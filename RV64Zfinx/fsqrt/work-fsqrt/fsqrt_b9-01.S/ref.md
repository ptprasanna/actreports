
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80001930')]      |
| SIG_REGION                | [('0x80003810', '0x80003e40', '198 dwords')]      |
| COV_LABELS                | fsqrt_b9      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV64Zfinx-rvopcodesdecoder/work-fsqrt/fsqrt_b9-01.S/ref.S    |
| Total Number of coverpoints| 261     |
| Total Coverpoints Hit     | 261      |
| Total Signature Updates   | 224      |
| STAT1                     | 0      |
| STAT2                     | 0      |
| STAT3                     | 195     |
| STAT4                     | 112     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x800003b8]:fsqrt.s t6, t6, dyn
[0x800003bc]:csrrs tp, fcsr, zero
[0x800003c0]:sd t6, 0(ra)
[0x800003c4]:sd tp, 8(ra)
[0x800003c8]:ld t4, 8(gp)
[0x800003cc]:addi sp, zero, 0
[0x800003d0]:csrrw zero, fcsr, sp
[0x800003d4]:fsqrt.s t5, t4, dyn

[0x800003d4]:fsqrt.s t5, t4, dyn
[0x800003d8]:csrrs tp, fcsr, zero
[0x800003dc]:sd t5, 16(ra)
[0x800003e0]:sd tp, 24(ra)
[0x800003e4]:ld t5, 16(gp)
[0x800003e8]:addi sp, zero, 0
[0x800003ec]:csrrw zero, fcsr, sp
[0x800003f0]:fsqrt.s t4, t5, dyn

[0x800003f0]:fsqrt.s t4, t5, dyn
[0x800003f4]:csrrs tp, fcsr, zero
[0x800003f8]:sd t4, 32(ra)
[0x800003fc]:sd tp, 40(ra)
[0x80000400]:ld s11, 24(gp)
[0x80000404]:addi sp, zero, 0
[0x80000408]:csrrw zero, fcsr, sp
[0x8000040c]:fsqrt.s t3, s11, dyn

[0x8000040c]:fsqrt.s t3, s11, dyn
[0x80000410]:csrrs tp, fcsr, zero
[0x80000414]:sd t3, 48(ra)
[0x80000418]:sd tp, 56(ra)
[0x8000041c]:ld t3, 32(gp)
[0x80000420]:addi sp, zero, 0
[0x80000424]:csrrw zero, fcsr, sp
[0x80000428]:fsqrt.s s11, t3, dyn

[0x80000428]:fsqrt.s s11, t3, dyn
[0x8000042c]:csrrs tp, fcsr, zero
[0x80000430]:sd s11, 64(ra)
[0x80000434]:sd tp, 72(ra)
[0x80000438]:ld s9, 40(gp)
[0x8000043c]:addi sp, zero, 0
[0x80000440]:csrrw zero, fcsr, sp
[0x80000444]:fsqrt.s s10, s9, dyn

[0x80000444]:fsqrt.s s10, s9, dyn
[0x80000448]:csrrs tp, fcsr, zero
[0x8000044c]:sd s10, 80(ra)
[0x80000450]:sd tp, 88(ra)
[0x80000454]:ld s10, 48(gp)
[0x80000458]:addi sp, zero, 0
[0x8000045c]:csrrw zero, fcsr, sp
[0x80000460]:fsqrt.s s9, s10, dyn

[0x80000460]:fsqrt.s s9, s10, dyn
[0x80000464]:csrrs tp, fcsr, zero
[0x80000468]:sd s9, 96(ra)
[0x8000046c]:sd tp, 104(ra)
[0x80000470]:ld s7, 56(gp)
[0x80000474]:addi sp, zero, 0
[0x80000478]:csrrw zero, fcsr, sp
[0x8000047c]:fsqrt.s s8, s7, dyn

[0x8000047c]:fsqrt.s s8, s7, dyn
[0x80000480]:csrrs tp, fcsr, zero
[0x80000484]:sd s8, 112(ra)
[0x80000488]:sd tp, 120(ra)
[0x8000048c]:ld s8, 64(gp)
[0x80000490]:addi sp, zero, 0
[0x80000494]:csrrw zero, fcsr, sp
[0x80000498]:fsqrt.s s7, s8, dyn

[0x80000498]:fsqrt.s s7, s8, dyn
[0x8000049c]:csrrs tp, fcsr, zero
[0x800004a0]:sd s7, 128(ra)
[0x800004a4]:sd tp, 136(ra)
[0x800004a8]:ld s5, 72(gp)
[0x800004ac]:addi sp, zero, 0
[0x800004b0]:csrrw zero, fcsr, sp
[0x800004b4]:fsqrt.s s6, s5, dyn

[0x800004b4]:fsqrt.s s6, s5, dyn
[0x800004b8]:csrrs tp, fcsr, zero
[0x800004bc]:sd s6, 144(ra)
[0x800004c0]:sd tp, 152(ra)
[0x800004c4]:ld s6, 80(gp)
[0x800004c8]:addi sp, zero, 0
[0x800004cc]:csrrw zero, fcsr, sp
[0x800004d0]:fsqrt.s s5, s6, dyn

[0x800004d0]:fsqrt.s s5, s6, dyn
[0x800004d4]:csrrs tp, fcsr, zero
[0x800004d8]:sd s5, 160(ra)
[0x800004dc]:sd tp, 168(ra)
[0x800004e0]:ld s3, 88(gp)
[0x800004e4]:addi sp, zero, 0
[0x800004e8]:csrrw zero, fcsr, sp
[0x800004ec]:fsqrt.s s4, s3, dyn

[0x800004ec]:fsqrt.s s4, s3, dyn
[0x800004f0]:csrrs tp, fcsr, zero
[0x800004f4]:sd s4, 176(ra)
[0x800004f8]:sd tp, 184(ra)
[0x800004fc]:ld s4, 96(gp)
[0x80000500]:addi sp, zero, 0
[0x80000504]:csrrw zero, fcsr, sp
[0x80000508]:fsqrt.s s3, s4, dyn

[0x80000508]:fsqrt.s s3, s4, dyn
[0x8000050c]:csrrs tp, fcsr, zero
[0x80000510]:sd s3, 192(ra)
[0x80000514]:sd tp, 200(ra)
[0x80000518]:ld a7, 104(gp)
[0x8000051c]:addi sp, zero, 0
[0x80000520]:csrrw zero, fcsr, sp
[0x80000524]:fsqrt.s s2, a7, dyn

[0x80000524]:fsqrt.s s2, a7, dyn
[0x80000528]:csrrs tp, fcsr, zero
[0x8000052c]:sd s2, 208(ra)
[0x80000530]:sd tp, 216(ra)
[0x80000534]:ld s2, 112(gp)
[0x80000538]:addi sp, zero, 0
[0x8000053c]:csrrw zero, fcsr, sp
[0x80000540]:fsqrt.s a7, s2, dyn

[0x80000540]:fsqrt.s a7, s2, dyn
[0x80000544]:csrrs tp, fcsr, zero
[0x80000548]:sd a7, 224(ra)
[0x8000054c]:sd tp, 232(ra)
[0x80000550]:ld a5, 120(gp)
[0x80000554]:addi sp, zero, 0
[0x80000558]:csrrw zero, fcsr, sp
[0x8000055c]:fsqrt.s a6, a5, dyn

[0x8000055c]:fsqrt.s a6, a5, dyn
[0x80000560]:csrrs tp, fcsr, zero
[0x80000564]:sd a6, 240(ra)
[0x80000568]:sd tp, 248(ra)
[0x8000056c]:ld a6, 128(gp)
[0x80000570]:addi sp, zero, 0
[0x80000574]:csrrw zero, fcsr, sp
[0x80000578]:fsqrt.s a5, a6, dyn

[0x80000578]:fsqrt.s a5, a6, dyn
[0x8000057c]:csrrs tp, fcsr, zero
[0x80000580]:sd a5, 256(ra)
[0x80000584]:sd tp, 264(ra)
[0x80000588]:ld a3, 136(gp)
[0x8000058c]:addi sp, zero, 0
[0x80000590]:csrrw zero, fcsr, sp
[0x80000594]:fsqrt.s a4, a3, dyn

[0x80000594]:fsqrt.s a4, a3, dyn
[0x80000598]:csrrs tp, fcsr, zero
[0x8000059c]:sd a4, 272(ra)
[0x800005a0]:sd tp, 280(ra)
[0x800005a4]:ld a4, 144(gp)
[0x800005a8]:addi sp, zero, 0
[0x800005ac]:csrrw zero, fcsr, sp
[0x800005b0]:fsqrt.s a3, a4, dyn

[0x800005b0]:fsqrt.s a3, a4, dyn
[0x800005b4]:csrrs tp, fcsr, zero
[0x800005b8]:sd a3, 288(ra)
[0x800005bc]:sd tp, 296(ra)
[0x800005c0]:ld a1, 152(gp)
[0x800005c4]:addi sp, zero, 0
[0x800005c8]:csrrw zero, fcsr, sp
[0x800005cc]:fsqrt.s a2, a1, dyn

[0x800005cc]:fsqrt.s a2, a1, dyn
[0x800005d0]:csrrs tp, fcsr, zero
[0x800005d4]:sd a2, 304(ra)
[0x800005d8]:sd tp, 312(ra)
[0x800005dc]:ld a2, 160(gp)
[0x800005e0]:addi sp, zero, 0
[0x800005e4]:csrrw zero, fcsr, sp
[0x800005e8]:fsqrt.s a1, a2, dyn

[0x800005e8]:fsqrt.s a1, a2, dyn
[0x800005ec]:csrrs tp, fcsr, zero
[0x800005f0]:sd a1, 320(ra)
[0x800005f4]:sd tp, 328(ra)
[0x800005f8]:ld s1, 168(gp)
[0x800005fc]:addi sp, zero, 0
[0x80000600]:csrrw zero, fcsr, sp
[0x80000604]:fsqrt.s a0, s1, dyn

[0x80000604]:fsqrt.s a0, s1, dyn
[0x80000608]:csrrs tp, fcsr, zero
[0x8000060c]:sd a0, 336(ra)
[0x80000610]:sd tp, 344(ra)
[0x80000614]:ld a0, 176(gp)
[0x80000618]:addi sp, zero, 0
[0x8000061c]:csrrw zero, fcsr, sp
[0x80000620]:fsqrt.s s1, a0, dyn

[0x80000620]:fsqrt.s s1, a0, dyn
[0x80000624]:csrrs tp, fcsr, zero
[0x80000628]:sd s1, 352(ra)
[0x8000062c]:sd tp, 360(ra)
[0x80000630]:ld t2, 184(gp)
[0x80000634]:addi sp, zero, 0
[0x80000638]:csrrw zero, fcsr, sp
[0x8000063c]:fsqrt.s fp, t2, dyn

[0x8000063c]:fsqrt.s fp, t2, dyn
[0x80000640]:csrrs tp, fcsr, zero
[0x80000644]:sd fp, 368(ra)
[0x80000648]:sd tp, 376(ra)
[0x8000064c]:auipc s1, 3
[0x80000650]:addi s1, s1, 2692
[0x80000654]:ld fp, 0(s1)
[0x80000658]:addi sp, zero, 0
[0x8000065c]:csrrw zero, fcsr, sp
[0x80000660]:fsqrt.s t2, fp, dyn

[0x80000660]:fsqrt.s t2, fp, dyn
[0x80000664]:csrrs a0, fcsr, zero
[0x80000668]:sd t2, 384(ra)
[0x8000066c]:sd a0, 392(ra)
[0x80000670]:ld t0, 8(s1)
[0x80000674]:addi sp, zero, 0
[0x80000678]:csrrw zero, fcsr, sp
[0x8000067c]:fsqrt.s t1, t0, dyn

[0x8000067c]:fsqrt.s t1, t0, dyn
[0x80000680]:csrrs a0, fcsr, zero
[0x80000684]:sd t1, 400(ra)
[0x80000688]:sd a0, 408(ra)
[0x8000068c]:auipc t2, 3
[0x80000690]:addi t2, t2, 604
[0x80000694]:ld t1, 16(s1)
[0x80000698]:addi fp, zero, 0
[0x8000069c]:csrrw zero, fcsr, fp
[0x800006a0]:fsqrt.s t0, t1, dyn

[0x800006a0]:fsqrt.s t0, t1, dyn
[0x800006a4]:csrrs a0, fcsr, zero
[0x800006a8]:sd t0, 0(t2)
[0x800006ac]:sd a0, 8(t2)
[0x800006b0]:ld gp, 24(s1)
[0x800006b4]:addi fp, zero, 0
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fsqrt.s tp, gp, dyn

[0x800006bc]:fsqrt.s tp, gp, dyn
[0x800006c0]:csrrs a0, fcsr, zero
[0x800006c4]:sd tp, 16(t2)
[0x800006c8]:sd a0, 24(t2)
[0x800006cc]:ld tp, 32(s1)
[0x800006d0]:addi fp, zero, 0
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fsqrt.s gp, tp, dyn

[0x800006d8]:fsqrt.s gp, tp, dyn
[0x800006dc]:csrrs a0, fcsr, zero
[0x800006e0]:sd gp, 32(t2)
[0x800006e4]:sd a0, 40(t2)
[0x800006e8]:ld ra, 40(s1)
[0x800006ec]:addi fp, zero, 0
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fsqrt.s sp, ra, dyn

[0x800006f4]:fsqrt.s sp, ra, dyn
[0x800006f8]:csrrs a0, fcsr, zero
[0x800006fc]:sd sp, 48(t2)
[0x80000700]:sd a0, 56(t2)
[0x80000704]:ld sp, 48(s1)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fsqrt.s ra, sp, dyn

[0x80000710]:fsqrt.s ra, sp, dyn
[0x80000714]:csrrs a0, fcsr, zero
[0x80000718]:sd ra, 64(t2)
[0x8000071c]:sd a0, 72(t2)
[0x80000720]:ld zero, 56(s1)
[0x80000724]:addi fp, zero, 0
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fsqrt.s t6, zero, dyn

[0x8000072c]:fsqrt.s t6, zero, dyn
[0x80000730]:csrrs a0, fcsr, zero
[0x80000734]:sd t6, 80(t2)
[0x80000738]:sd a0, 88(t2)
[0x8000073c]:ld t6, 64(s1)
[0x80000740]:addi fp, zero, 0
[0x80000744]:csrrw zero, fcsr, fp
[0x80000748]:fsqrt.s zero, t6, dyn

[0x80000748]:fsqrt.s zero, t6, dyn
[0x8000074c]:csrrs a0, fcsr, zero
[0x80000750]:sd zero, 96(t2)
[0x80000754]:sd a0, 104(t2)
[0x80000758]:ld t5, 72(s1)
[0x8000075c]:addi fp, zero, 0
[0x80000760]:csrrw zero, fcsr, fp
[0x80000764]:fsqrt.s t6, t5, dyn

[0x80000764]:fsqrt.s t6, t5, dyn
[0x80000768]:csrrs a0, fcsr, zero
[0x8000076c]:sd t6, 112(t2)
[0x80000770]:sd a0, 120(t2)
[0x80000774]:ld t5, 80(s1)
[0x80000778]:addi fp, zero, 0
[0x8000077c]:csrrw zero, fcsr, fp
[0x80000780]:fsqrt.s t6, t5, dyn

[0x80000780]:fsqrt.s t6, t5, dyn
[0x80000784]:csrrs a0, fcsr, zero
[0x80000788]:sd t6, 128(t2)
[0x8000078c]:sd a0, 136(t2)
[0x80000790]:ld t5, 88(s1)
[0x80000794]:addi fp, zero, 0
[0x80000798]:csrrw zero, fcsr, fp
[0x8000079c]:fsqrt.s t6, t5, dyn

[0x8000079c]:fsqrt.s t6, t5, dyn
[0x800007a0]:csrrs a0, fcsr, zero
[0x800007a4]:sd t6, 144(t2)
[0x800007a8]:sd a0, 152(t2)
[0x800007ac]:ld t5, 96(s1)
[0x800007b0]:addi fp, zero, 0
[0x800007b4]:csrrw zero, fcsr, fp
[0x800007b8]:fsqrt.s t6, t5, dyn

[0x800007b8]:fsqrt.s t6, t5, dyn
[0x800007bc]:csrrs a0, fcsr, zero
[0x800007c0]:sd t6, 160(t2)
[0x800007c4]:sd a0, 168(t2)
[0x800007c8]:ld t5, 104(s1)
[0x800007cc]:addi fp, zero, 0
[0x800007d0]:csrrw zero, fcsr, fp
[0x800007d4]:fsqrt.s t6, t5, dyn

[0x800007d4]:fsqrt.s t6, t5, dyn
[0x800007d8]:csrrs a0, fcsr, zero
[0x800007dc]:sd t6, 176(t2)
[0x800007e0]:sd a0, 184(t2)
[0x800007e4]:ld t5, 112(s1)
[0x800007e8]:addi fp, zero, 0
[0x800007ec]:csrrw zero, fcsr, fp
[0x800007f0]:fsqrt.s t6, t5, dyn

[0x800007f0]:fsqrt.s t6, t5, dyn
[0x800007f4]:csrrs a0, fcsr, zero
[0x800007f8]:sd t6, 192(t2)
[0x800007fc]:sd a0, 200(t2)
[0x80000800]:ld t5, 120(s1)
[0x80000804]:addi fp, zero, 0
[0x80000808]:csrrw zero, fcsr, fp
[0x8000080c]:fsqrt.s t6, t5, dyn

[0x8000080c]:fsqrt.s t6, t5, dyn
[0x80000810]:csrrs a0, fcsr, zero
[0x80000814]:sd t6, 208(t2)
[0x80000818]:sd a0, 216(t2)
[0x8000081c]:ld t5, 128(s1)
[0x80000820]:addi fp, zero, 0
[0x80000824]:csrrw zero, fcsr, fp
[0x80000828]:fsqrt.s t6, t5, dyn

[0x80000828]:fsqrt.s t6, t5, dyn
[0x8000082c]:csrrs a0, fcsr, zero
[0x80000830]:sd t6, 224(t2)
[0x80000834]:sd a0, 232(t2)
[0x80000838]:ld t5, 136(s1)
[0x8000083c]:addi fp, zero, 0
[0x80000840]:csrrw zero, fcsr, fp
[0x80000844]:fsqrt.s t6, t5, dyn

[0x80000844]:fsqrt.s t6, t5, dyn
[0x80000848]:csrrs a0, fcsr, zero
[0x8000084c]:sd t6, 240(t2)
[0x80000850]:sd a0, 248(t2)
[0x80000854]:ld t5, 144(s1)
[0x80000858]:addi fp, zero, 0
[0x8000085c]:csrrw zero, fcsr, fp
[0x80000860]:fsqrt.s t6, t5, dyn

[0x80000860]:fsqrt.s t6, t5, dyn
[0x80000864]:csrrs a0, fcsr, zero
[0x80000868]:sd t6, 256(t2)
[0x8000086c]:sd a0, 264(t2)
[0x80000870]:ld t5, 152(s1)
[0x80000874]:addi fp, zero, 0
[0x80000878]:csrrw zero, fcsr, fp
[0x8000087c]:fsqrt.s t6, t5, dyn

[0x8000087c]:fsqrt.s t6, t5, dyn
[0x80000880]:csrrs a0, fcsr, zero
[0x80000884]:sd t6, 272(t2)
[0x80000888]:sd a0, 280(t2)
[0x8000088c]:ld t5, 160(s1)
[0x80000890]:addi fp, zero, 0
[0x80000894]:csrrw zero, fcsr, fp
[0x80000898]:fsqrt.s t6, t5, dyn

[0x80000898]:fsqrt.s t6, t5, dyn
[0x8000089c]:csrrs a0, fcsr, zero
[0x800008a0]:sd t6, 288(t2)
[0x800008a4]:sd a0, 296(t2)
[0x800008a8]:ld t5, 168(s1)
[0x800008ac]:addi fp, zero, 0
[0x800008b0]:csrrw zero, fcsr, fp
[0x800008b4]:fsqrt.s t6, t5, dyn

[0x800008b4]:fsqrt.s t6, t5, dyn
[0x800008b8]:csrrs a0, fcsr, zero
[0x800008bc]:sd t6, 304(t2)
[0x800008c0]:sd a0, 312(t2)
[0x800008c4]:ld t5, 176(s1)
[0x800008c8]:addi fp, zero, 0
[0x800008cc]:csrrw zero, fcsr, fp
[0x800008d0]:fsqrt.s t6, t5, dyn

[0x800008d0]:fsqrt.s t6, t5, dyn
[0x800008d4]:csrrs a0, fcsr, zero
[0x800008d8]:sd t6, 320(t2)
[0x800008dc]:sd a0, 328(t2)
[0x800008e0]:ld t5, 184(s1)
[0x800008e4]:addi fp, zero, 0
[0x800008e8]:csrrw zero, fcsr, fp
[0x800008ec]:fsqrt.s t6, t5, dyn

[0x800008ec]:fsqrt.s t6, t5, dyn
[0x800008f0]:csrrs a0, fcsr, zero
[0x800008f4]:sd t6, 336(t2)
[0x800008f8]:sd a0, 344(t2)
[0x800008fc]:ld t5, 192(s1)
[0x80000900]:addi fp, zero, 0
[0x80000904]:csrrw zero, fcsr, fp
[0x80000908]:fsqrt.s t6, t5, dyn

[0x80000908]:fsqrt.s t6, t5, dyn
[0x8000090c]:csrrs a0, fcsr, zero
[0x80000910]:sd t6, 352(t2)
[0x80000914]:sd a0, 360(t2)
[0x80000918]:ld t5, 200(s1)
[0x8000091c]:addi fp, zero, 0
[0x80000920]:csrrw zero, fcsr, fp
[0x80000924]:fsqrt.s t6, t5, dyn

[0x80000924]:fsqrt.s t6, t5, dyn
[0x80000928]:csrrs a0, fcsr, zero
[0x8000092c]:sd t6, 368(t2)
[0x80000930]:sd a0, 376(t2)
[0x80000934]:ld t5, 208(s1)
[0x80000938]:addi fp, zero, 0
[0x8000093c]:csrrw zero, fcsr, fp
[0x80000940]:fsqrt.s t6, t5, dyn

[0x80000940]:fsqrt.s t6, t5, dyn
[0x80000944]:csrrs a0, fcsr, zero
[0x80000948]:sd t6, 384(t2)
[0x8000094c]:sd a0, 392(t2)
[0x80000950]:ld t5, 216(s1)
[0x80000954]:addi fp, zero, 0
[0x80000958]:csrrw zero, fcsr, fp
[0x8000095c]:fsqrt.s t6, t5, dyn

[0x8000095c]:fsqrt.s t6, t5, dyn
[0x80000960]:csrrs a0, fcsr, zero
[0x80000964]:sd t6, 400(t2)
[0x80000968]:sd a0, 408(t2)
[0x8000096c]:ld t5, 224(s1)
[0x80000970]:addi fp, zero, 0
[0x80000974]:csrrw zero, fcsr, fp
[0x80000978]:fsqrt.s t6, t5, dyn

[0x80000978]:fsqrt.s t6, t5, dyn
[0x8000097c]:csrrs a0, fcsr, zero
[0x80000980]:sd t6, 416(t2)
[0x80000984]:sd a0, 424(t2)
[0x80000988]:ld t5, 232(s1)
[0x8000098c]:addi fp, zero, 0
[0x80000990]:csrrw zero, fcsr, fp
[0x80000994]:fsqrt.s t6, t5, dyn

[0x80000994]:fsqrt.s t6, t5, dyn
[0x80000998]:csrrs a0, fcsr, zero
[0x8000099c]:sd t6, 432(t2)
[0x800009a0]:sd a0, 440(t2)
[0x800009a4]:ld t5, 240(s1)
[0x800009a8]:addi fp, zero, 0
[0x800009ac]:csrrw zero, fcsr, fp
[0x800009b0]:fsqrt.s t6, t5, dyn

[0x800009b0]:fsqrt.s t6, t5, dyn
[0x800009b4]:csrrs a0, fcsr, zero
[0x800009b8]:sd t6, 448(t2)
[0x800009bc]:sd a0, 456(t2)
[0x800009c0]:ld t5, 248(s1)
[0x800009c4]:addi fp, zero, 0
[0x800009c8]:csrrw zero, fcsr, fp
[0x800009cc]:fsqrt.s t6, t5, dyn

[0x800009cc]:fsqrt.s t6, t5, dyn
[0x800009d0]:csrrs a0, fcsr, zero
[0x800009d4]:sd t6, 464(t2)
[0x800009d8]:sd a0, 472(t2)
[0x800009dc]:ld t5, 256(s1)
[0x800009e0]:addi fp, zero, 0
[0x800009e4]:csrrw zero, fcsr, fp
[0x800009e8]:fsqrt.s t6, t5, dyn

[0x800009e8]:fsqrt.s t6, t5, dyn
[0x800009ec]:csrrs a0, fcsr, zero
[0x800009f0]:sd t6, 480(t2)
[0x800009f4]:sd a0, 488(t2)
[0x800009f8]:ld t5, 264(s1)
[0x800009fc]:addi fp, zero, 0
[0x80000a00]:csrrw zero, fcsr, fp
[0x80000a04]:fsqrt.s t6, t5, dyn

[0x80000a04]:fsqrt.s t6, t5, dyn
[0x80000a08]:csrrs a0, fcsr, zero
[0x80000a0c]:sd t6, 496(t2)
[0x80000a10]:sd a0, 504(t2)
[0x80000a14]:ld t5, 272(s1)
[0x80000a18]:addi fp, zero, 0
[0x80000a1c]:csrrw zero, fcsr, fp
[0x80000a20]:fsqrt.s t6, t5, dyn

[0x80000a20]:fsqrt.s t6, t5, dyn
[0x80000a24]:csrrs a0, fcsr, zero
[0x80000a28]:sd t6, 512(t2)
[0x80000a2c]:sd a0, 520(t2)
[0x80000a30]:ld t5, 280(s1)
[0x80000a34]:addi fp, zero, 0
[0x80000a38]:csrrw zero, fcsr, fp
[0x80000a3c]:fsqrt.s t6, t5, dyn

[0x80000a3c]:fsqrt.s t6, t5, dyn
[0x80000a40]:csrrs a0, fcsr, zero
[0x80000a44]:sd t6, 528(t2)
[0x80000a48]:sd a0, 536(t2)
[0x80000a4c]:ld t5, 288(s1)
[0x80000a50]:addi fp, zero, 0
[0x80000a54]:csrrw zero, fcsr, fp
[0x80000a58]:fsqrt.s t6, t5, dyn

[0x80000a58]:fsqrt.s t6, t5, dyn
[0x80000a5c]:csrrs a0, fcsr, zero
[0x80000a60]:sd t6, 544(t2)
[0x80000a64]:sd a0, 552(t2)
[0x80000a68]:ld t5, 296(s1)
[0x80000a6c]:addi fp, zero, 0
[0x80000a70]:csrrw zero, fcsr, fp
[0x80000a74]:fsqrt.s t6, t5, dyn

[0x80000a74]:fsqrt.s t6, t5, dyn
[0x80000a78]:csrrs a0, fcsr, zero
[0x80000a7c]:sd t6, 560(t2)
[0x80000a80]:sd a0, 568(t2)
[0x80000a84]:ld t5, 304(s1)
[0x80000a88]:addi fp, zero, 0
[0x80000a8c]:csrrw zero, fcsr, fp
[0x80000a90]:fsqrt.s t6, t5, dyn

[0x80000a90]:fsqrt.s t6, t5, dyn
[0x80000a94]:csrrs a0, fcsr, zero
[0x80000a98]:sd t6, 576(t2)
[0x80000a9c]:sd a0, 584(t2)
[0x80000aa0]:ld t5, 312(s1)
[0x80000aa4]:addi fp, zero, 0
[0x80000aa8]:csrrw zero, fcsr, fp
[0x80000aac]:fsqrt.s t6, t5, dyn

[0x80000aac]:fsqrt.s t6, t5, dyn
[0x80000ab0]:csrrs a0, fcsr, zero
[0x80000ab4]:sd t6, 592(t2)
[0x80000ab8]:sd a0, 600(t2)
[0x80000abc]:ld t5, 320(s1)
[0x80000ac0]:addi fp, zero, 0
[0x80000ac4]:csrrw zero, fcsr, fp
[0x80000ac8]:fsqrt.s t6, t5, dyn

[0x80000ac8]:fsqrt.s t6, t5, dyn
[0x80000acc]:csrrs a0, fcsr, zero
[0x80000ad0]:sd t6, 608(t2)
[0x80000ad4]:sd a0, 616(t2)
[0x80000ad8]:ld t5, 328(s1)
[0x80000adc]:addi fp, zero, 0
[0x80000ae0]:csrrw zero, fcsr, fp
[0x80000ae4]:fsqrt.s t6, t5, dyn

[0x80000ae4]:fsqrt.s t6, t5, dyn
[0x80000ae8]:csrrs a0, fcsr, zero
[0x80000aec]:sd t6, 624(t2)
[0x80000af0]:sd a0, 632(t2)
[0x80000af4]:ld t5, 336(s1)
[0x80000af8]:addi fp, zero, 0
[0x80000afc]:csrrw zero, fcsr, fp
[0x80000b00]:fsqrt.s t6, t5, dyn

[0x80000b00]:fsqrt.s t6, t5, dyn
[0x80000b04]:csrrs a0, fcsr, zero
[0x80000b08]:sd t6, 640(t2)
[0x80000b0c]:sd a0, 648(t2)
[0x80000b10]:ld t5, 344(s1)
[0x80000b14]:addi fp, zero, 0
[0x80000b18]:csrrw zero, fcsr, fp
[0x80000b1c]:fsqrt.s t6, t5, dyn

[0x80000b1c]:fsqrt.s t6, t5, dyn
[0x80000b20]:csrrs a0, fcsr, zero
[0x80000b24]:sd t6, 656(t2)
[0x80000b28]:sd a0, 664(t2)
[0x80000b2c]:ld t5, 352(s1)
[0x80000b30]:addi fp, zero, 0
[0x80000b34]:csrrw zero, fcsr, fp
[0x80000b38]:fsqrt.s t6, t5, dyn

[0x80000b38]:fsqrt.s t6, t5, dyn
[0x80000b3c]:csrrs a0, fcsr, zero
[0x80000b40]:sd t6, 672(t2)
[0x80000b44]:sd a0, 680(t2)
[0x80000b48]:ld t5, 360(s1)
[0x80000b4c]:addi fp, zero, 0
[0x80000b50]:csrrw zero, fcsr, fp
[0x80000b54]:fsqrt.s t6, t5, dyn

[0x80000b54]:fsqrt.s t6, t5, dyn
[0x80000b58]:csrrs a0, fcsr, zero
[0x80000b5c]:sd t6, 688(t2)
[0x80000b60]:sd a0, 696(t2)
[0x80000b64]:ld t5, 368(s1)
[0x80000b68]:addi fp, zero, 0
[0x80000b6c]:csrrw zero, fcsr, fp
[0x80000b70]:fsqrt.s t6, t5, dyn

[0x80000b70]:fsqrt.s t6, t5, dyn
[0x80000b74]:csrrs a0, fcsr, zero
[0x80000b78]:sd t6, 704(t2)
[0x80000b7c]:sd a0, 712(t2)
[0x80000b80]:ld t5, 376(s1)
[0x80000b84]:addi fp, zero, 0
[0x80000b88]:csrrw zero, fcsr, fp
[0x80000b8c]:fsqrt.s t6, t5, dyn

[0x80000b8c]:fsqrt.s t6, t5, dyn
[0x80000b90]:csrrs a0, fcsr, zero
[0x80000b94]:sd t6, 720(t2)
[0x80000b98]:sd a0, 728(t2)
[0x80000b9c]:ld t5, 384(s1)
[0x80000ba0]:addi fp, zero, 0
[0x80000ba4]:csrrw zero, fcsr, fp
[0x80000ba8]:fsqrt.s t6, t5, dyn

[0x80000ba8]:fsqrt.s t6, t5, dyn
[0x80000bac]:csrrs a0, fcsr, zero
[0x80000bb0]:sd t6, 736(t2)
[0x80000bb4]:sd a0, 744(t2)
[0x80000bb8]:ld t5, 392(s1)
[0x80000bbc]:addi fp, zero, 0
[0x80000bc0]:csrrw zero, fcsr, fp
[0x80000bc4]:fsqrt.s t6, t5, dyn

[0x80000bc4]:fsqrt.s t6, t5, dyn
[0x80000bc8]:csrrs a0, fcsr, zero
[0x80000bcc]:sd t6, 752(t2)
[0x80000bd0]:sd a0, 760(t2)
[0x80000bd4]:ld t5, 400(s1)
[0x80000bd8]:addi fp, zero, 0
[0x80000bdc]:csrrw zero, fcsr, fp
[0x80000be0]:fsqrt.s t6, t5, dyn

[0x80000be0]:fsqrt.s t6, t5, dyn
[0x80000be4]:csrrs a0, fcsr, zero
[0x80000be8]:sd t6, 768(t2)
[0x80000bec]:sd a0, 776(t2)
[0x80000bf0]:ld t5, 408(s1)
[0x80000bf4]:addi fp, zero, 0
[0x80000bf8]:csrrw zero, fcsr, fp
[0x80000bfc]:fsqrt.s t6, t5, dyn

[0x80000bfc]:fsqrt.s t6, t5, dyn
[0x80000c00]:csrrs a0, fcsr, zero
[0x80000c04]:sd t6, 784(t2)
[0x80000c08]:sd a0, 792(t2)
[0x80000c0c]:ld t5, 416(s1)
[0x80000c10]:addi fp, zero, 0
[0x80000c14]:csrrw zero, fcsr, fp
[0x80000c18]:fsqrt.s t6, t5, dyn

[0x80000c18]:fsqrt.s t6, t5, dyn
[0x80000c1c]:csrrs a0, fcsr, zero
[0x80000c20]:sd t6, 800(t2)
[0x80000c24]:sd a0, 808(t2)
[0x80000c28]:ld t5, 424(s1)
[0x80000c2c]:addi fp, zero, 0
[0x80000c30]:csrrw zero, fcsr, fp
[0x80000c34]:fsqrt.s t6, t5, dyn

[0x80000c34]:fsqrt.s t6, t5, dyn
[0x80000c38]:csrrs a0, fcsr, zero
[0x80000c3c]:sd t6, 816(t2)
[0x80000c40]:sd a0, 824(t2)
[0x80000c44]:ld t5, 432(s1)
[0x80000c48]:addi fp, zero, 0
[0x80000c4c]:csrrw zero, fcsr, fp
[0x80000c50]:fsqrt.s t6, t5, dyn

[0x80000c50]:fsqrt.s t6, t5, dyn
[0x80000c54]:csrrs a0, fcsr, zero
[0x80000c58]:sd t6, 832(t2)
[0x80000c5c]:sd a0, 840(t2)
[0x80000c60]:ld t5, 440(s1)
[0x80000c64]:addi fp, zero, 0
[0x80000c68]:csrrw zero, fcsr, fp
[0x80000c6c]:fsqrt.s t6, t5, dyn

[0x80000c6c]:fsqrt.s t6, t5, dyn
[0x80000c70]:csrrs a0, fcsr, zero
[0x80000c74]:sd t6, 848(t2)
[0x80000c78]:sd a0, 856(t2)
[0x80000c7c]:ld t5, 448(s1)
[0x80000c80]:addi fp, zero, 0
[0x80000c84]:csrrw zero, fcsr, fp
[0x80000c88]:fsqrt.s t6, t5, dyn

[0x80000c88]:fsqrt.s t6, t5, dyn
[0x80000c8c]:csrrs a0, fcsr, zero
[0x80000c90]:sd t6, 864(t2)
[0x80000c94]:sd a0, 872(t2)
[0x80000c98]:ld t5, 456(s1)
[0x80000c9c]:addi fp, zero, 0
[0x80000ca0]:csrrw zero, fcsr, fp
[0x80000ca4]:fsqrt.s t6, t5, dyn

[0x80000ca4]:fsqrt.s t6, t5, dyn
[0x80000ca8]:csrrs a0, fcsr, zero
[0x80000cac]:sd t6, 880(t2)
[0x80000cb0]:sd a0, 888(t2)
[0x80000cb4]:ld t5, 464(s1)
[0x80000cb8]:addi fp, zero, 0
[0x80000cbc]:csrrw zero, fcsr, fp
[0x80000cc0]:fsqrt.s t6, t5, dyn

[0x80000cc0]:fsqrt.s t6, t5, dyn
[0x80000cc4]:csrrs a0, fcsr, zero
[0x80000cc8]:sd t6, 896(t2)
[0x80000ccc]:sd a0, 904(t2)
[0x80000cd0]:ld t5, 472(s1)
[0x80000cd4]:addi fp, zero, 0
[0x80000cd8]:csrrw zero, fcsr, fp
[0x80000cdc]:fsqrt.s t6, t5, dyn

[0x80000cdc]:fsqrt.s t6, t5, dyn
[0x80000ce0]:csrrs a0, fcsr, zero
[0x80000ce4]:sd t6, 912(t2)
[0x80000ce8]:sd a0, 920(t2)
[0x80000cec]:ld t5, 480(s1)
[0x80000cf0]:addi fp, zero, 0
[0x80000cf4]:csrrw zero, fcsr, fp
[0x80000cf8]:fsqrt.s t6, t5, dyn

[0x80000cf8]:fsqrt.s t6, t5, dyn
[0x80000cfc]:csrrs a0, fcsr, zero
[0x80000d00]:sd t6, 928(t2)
[0x80000d04]:sd a0, 936(t2)
[0x80000d08]:ld t5, 488(s1)
[0x80000d0c]:addi fp, zero, 0
[0x80000d10]:csrrw zero, fcsr, fp
[0x80000d14]:fsqrt.s t6, t5, dyn

[0x80000d14]:fsqrt.s t6, t5, dyn
[0x80000d18]:csrrs a0, fcsr, zero
[0x80000d1c]:sd t6, 944(t2)
[0x80000d20]:sd a0, 952(t2)
[0x80000d24]:ld t5, 496(s1)
[0x80000d28]:addi fp, zero, 0
[0x80000d2c]:csrrw zero, fcsr, fp
[0x80000d30]:fsqrt.s t6, t5, dyn

[0x80000d30]:fsqrt.s t6, t5, dyn
[0x80000d34]:csrrs a0, fcsr, zero
[0x80000d38]:sd t6, 960(t2)
[0x80000d3c]:sd a0, 968(t2)
[0x80000d40]:ld t5, 504(s1)
[0x80000d44]:addi fp, zero, 0
[0x80000d48]:csrrw zero, fcsr, fp
[0x80000d4c]:fsqrt.s t6, t5, dyn

[0x80000d4c]:fsqrt.s t6, t5, dyn
[0x80000d50]:csrrs a0, fcsr, zero
[0x80000d54]:sd t6, 976(t2)
[0x80000d58]:sd a0, 984(t2)
[0x80000d5c]:ld t5, 512(s1)
[0x80000d60]:addi fp, zero, 0
[0x80000d64]:csrrw zero, fcsr, fp
[0x80000d68]:fsqrt.s t6, t5, dyn

[0x80000d68]:fsqrt.s t6, t5, dyn
[0x80000d6c]:csrrs a0, fcsr, zero
[0x80000d70]:sd t6, 992(t2)
[0x80000d74]:sd a0, 1000(t2)
[0x80000d78]:ld t5, 520(s1)
[0x80000d7c]:addi fp, zero, 0
[0x80000d80]:csrrw zero, fcsr, fp
[0x80000d84]:fsqrt.s t6, t5, dyn

[0x80000d84]:fsqrt.s t6, t5, dyn
[0x80000d88]:csrrs a0, fcsr, zero
[0x80000d8c]:sd t6, 1008(t2)
[0x80000d90]:sd a0, 1016(t2)
[0x80000d94]:ld t5, 528(s1)
[0x80000d98]:addi fp, zero, 0
[0x80000d9c]:csrrw zero, fcsr, fp
[0x80000da0]:fsqrt.s t6, t5, dyn

[0x80000da0]:fsqrt.s t6, t5, dyn
[0x80000da4]:csrrs a0, fcsr, zero
[0x80000da8]:sd t6, 1024(t2)
[0x80000dac]:sd a0, 1032(t2)
[0x80000db0]:ld t5, 536(s1)
[0x80000db4]:addi fp, zero, 0
[0x80000db8]:csrrw zero, fcsr, fp
[0x80000dbc]:fsqrt.s t6, t5, dyn

[0x80000dbc]:fsqrt.s t6, t5, dyn
[0x80000dc0]:csrrs a0, fcsr, zero
[0x80000dc4]:sd t6, 1040(t2)
[0x80000dc8]:sd a0, 1048(t2)
[0x80000dcc]:ld t5, 544(s1)
[0x80000dd0]:addi fp, zero, 0
[0x80000dd4]:csrrw zero, fcsr, fp
[0x80000dd8]:fsqrt.s t6, t5, dyn

[0x80000dd8]:fsqrt.s t6, t5, dyn
[0x80000ddc]:csrrs a0, fcsr, zero
[0x80000de0]:sd t6, 1056(t2)
[0x80000de4]:sd a0, 1064(t2)
[0x80000de8]:ld t5, 552(s1)
[0x80000dec]:addi fp, zero, 0
[0x80000df0]:csrrw zero, fcsr, fp
[0x80000df4]:fsqrt.s t6, t5, dyn

[0x80000df4]:fsqrt.s t6, t5, dyn
[0x80000df8]:csrrs a0, fcsr, zero
[0x80000dfc]:sd t6, 1072(t2)
[0x80000e00]:sd a0, 1080(t2)
[0x80000e04]:ld t5, 560(s1)
[0x80000e08]:addi fp, zero, 0
[0x80000e0c]:csrrw zero, fcsr, fp
[0x80000e10]:fsqrt.s t6, t5, dyn

[0x80000e10]:fsqrt.s t6, t5, dyn
[0x80000e14]:csrrs a0, fcsr, zero
[0x80000e18]:sd t6, 1088(t2)
[0x80000e1c]:sd a0, 1096(t2)
[0x80000e20]:ld t5, 568(s1)
[0x80000e24]:addi fp, zero, 0
[0x80000e28]:csrrw zero, fcsr, fp
[0x80000e2c]:fsqrt.s t6, t5, dyn

[0x80000e2c]:fsqrt.s t6, t5, dyn
[0x80000e30]:csrrs a0, fcsr, zero
[0x80000e34]:sd t6, 1104(t2)
[0x80000e38]:sd a0, 1112(t2)
[0x80000e3c]:ld t5, 576(s1)
[0x80000e40]:addi fp, zero, 0
[0x80000e44]:csrrw zero, fcsr, fp
[0x80000e48]:fsqrt.s t6, t5, dyn

[0x80000e48]:fsqrt.s t6, t5, dyn
[0x80000e4c]:csrrs a0, fcsr, zero
[0x80000e50]:sd t6, 1120(t2)
[0x80000e54]:sd a0, 1128(t2)
[0x80000e58]:ld t5, 584(s1)
[0x80000e5c]:addi fp, zero, 0
[0x80000e60]:csrrw zero, fcsr, fp
[0x80000e64]:fsqrt.s t6, t5, dyn

[0x80000e64]:fsqrt.s t6, t5, dyn
[0x80000e68]:csrrs a0, fcsr, zero
[0x80000e6c]:sd t6, 1136(t2)
[0x80000e70]:sd a0, 1144(t2)
[0x80000e74]:ld t5, 592(s1)
[0x80000e78]:addi fp, zero, 0
[0x80000e7c]:csrrw zero, fcsr, fp
[0x80000e80]:fsqrt.s t6, t5, dyn

[0x80000e80]:fsqrt.s t6, t5, dyn
[0x80000e84]:csrrs a0, fcsr, zero
[0x80000e88]:sd t6, 1152(t2)
[0x80000e8c]:sd a0, 1160(t2)
[0x80000e90]:ld t5, 600(s1)
[0x80000e94]:addi fp, zero, 0
[0x80000e98]:csrrw zero, fcsr, fp
[0x80000e9c]:fsqrt.s t6, t5, dyn

[0x80000e9c]:fsqrt.s t6, t5, dyn
[0x80000ea0]:csrrs a0, fcsr, zero
[0x80000ea4]:sd t6, 1168(t2)
[0x80000ea8]:sd a0, 1176(t2)
[0x80000eac]:ld t5, 608(s1)
[0x80000eb0]:addi fp, zero, 0
[0x80000eb4]:csrrw zero, fcsr, fp
[0x80000eb8]:fsqrt.s t6, t5, dyn

[0x80000eb8]:fsqrt.s t6, t5, dyn
[0x80000ebc]:csrrs a0, fcsr, zero
[0x80000ec0]:sd t6, 1184(t2)
[0x80000ec4]:sd a0, 1192(t2)
[0x80000ec8]:ld t5, 616(s1)
[0x80000ecc]:addi fp, zero, 0
[0x80000ed0]:csrrw zero, fcsr, fp
[0x80000ed4]:fsqrt.s t6, t5, dyn

[0x80000ed4]:fsqrt.s t6, t5, dyn
[0x80000ed8]:csrrs a0, fcsr, zero
[0x80000edc]:sd t6, 1200(t2)
[0x80000ee0]:sd a0, 1208(t2)
[0x80000ee4]:ld t5, 624(s1)
[0x80000ee8]:addi fp, zero, 0
[0x80000eec]:csrrw zero, fcsr, fp
[0x80000ef0]:fsqrt.s t6, t5, dyn

[0x80000ef0]:fsqrt.s t6, t5, dyn
[0x80000ef4]:csrrs a0, fcsr, zero
[0x80000ef8]:sd t6, 1216(t2)
[0x80000efc]:sd a0, 1224(t2)
[0x80000f00]:ld t5, 632(s1)
[0x80000f04]:addi fp, zero, 0
[0x80000f08]:csrrw zero, fcsr, fp
[0x80000f0c]:fsqrt.s t6, t5, dyn

[0x80000f0c]:fsqrt.s t6, t5, dyn
[0x80000f10]:csrrs a0, fcsr, zero
[0x80000f14]:sd t6, 1232(t2)
[0x80000f18]:sd a0, 1240(t2)
[0x80000f1c]:ld t5, 640(s1)
[0x80000f20]:addi fp, zero, 0
[0x80000f24]:csrrw zero, fcsr, fp
[0x80000f28]:fsqrt.s t6, t5, dyn

[0x80000f28]:fsqrt.s t6, t5, dyn
[0x80000f2c]:csrrs a0, fcsr, zero
[0x80000f30]:sd t6, 1248(t2)
[0x80000f34]:sd a0, 1256(t2)
[0x80000f38]:ld t5, 648(s1)
[0x80000f3c]:addi fp, zero, 0
[0x80000f40]:csrrw zero, fcsr, fp
[0x80000f44]:fsqrt.s t6, t5, dyn

[0x80000f44]:fsqrt.s t6, t5, dyn
[0x80000f48]:csrrs a0, fcsr, zero
[0x80000f4c]:sd t6, 1264(t2)
[0x80000f50]:sd a0, 1272(t2)
[0x80000f54]:ld t5, 656(s1)
[0x80000f58]:addi fp, zero, 0
[0x80000f5c]:csrrw zero, fcsr, fp
[0x80000f60]:fsqrt.s t6, t5, dyn

[0x80000f60]:fsqrt.s t6, t5, dyn
[0x80000f64]:csrrs a0, fcsr, zero
[0x80000f68]:sd t6, 1280(t2)
[0x80000f6c]:sd a0, 1288(t2)
[0x80000f70]:ld t5, 664(s1)
[0x80000f74]:addi fp, zero, 0
[0x80000f78]:csrrw zero, fcsr, fp
[0x80000f7c]:fsqrt.s t6, t5, dyn

[0x80000f7c]:fsqrt.s t6, t5, dyn
[0x80000f80]:csrrs a0, fcsr, zero
[0x80000f84]:sd t6, 1296(t2)
[0x80000f88]:sd a0, 1304(t2)
[0x80000f8c]:ld t5, 672(s1)
[0x80000f90]:addi fp, zero, 0
[0x80000f94]:csrrw zero, fcsr, fp
[0x80000f98]:fsqrt.s t6, t5, dyn

[0x80000f98]:fsqrt.s t6, t5, dyn
[0x80000f9c]:csrrs a0, fcsr, zero
[0x80000fa0]:sd t6, 1312(t2)
[0x80000fa4]:sd a0, 1320(t2)
[0x80000fa8]:ld t5, 680(s1)
[0x80000fac]:addi fp, zero, 0
[0x80000fb0]:csrrw zero, fcsr, fp
[0x80000fb4]:fsqrt.s t6, t5, dyn

[0x80000fb4]:fsqrt.s t6, t5, dyn
[0x80000fb8]:csrrs a0, fcsr, zero
[0x80000fbc]:sd t6, 1328(t2)
[0x80000fc0]:sd a0, 1336(t2)
[0x80000fc4]:ld t5, 688(s1)
[0x80000fc8]:addi fp, zero, 0
[0x80000fcc]:csrrw zero, fcsr, fp
[0x80000fd0]:fsqrt.s t6, t5, dyn

[0x80000fd0]:fsqrt.s t6, t5, dyn
[0x80000fd4]:csrrs a0, fcsr, zero
[0x80000fd8]:sd t6, 1344(t2)
[0x80000fdc]:sd a0, 1352(t2)
[0x80000fe0]:ld t5, 696(s1)
[0x80000fe4]:addi fp, zero, 0
[0x80000fe8]:csrrw zero, fcsr, fp
[0x80000fec]:fsqrt.s t6, t5, dyn

[0x80000fec]:fsqrt.s t6, t5, dyn
[0x80000ff0]:csrrs a0, fcsr, zero
[0x80000ff4]:sd t6, 1360(t2)
[0x80000ff8]:sd a0, 1368(t2)
[0x80000ffc]:ld t5, 704(s1)
[0x80001000]:addi fp, zero, 0
[0x80001004]:csrrw zero, fcsr, fp
[0x80001008]:fsqrt.s t6, t5, dyn

[0x80001008]:fsqrt.s t6, t5, dyn
[0x8000100c]:csrrs a0, fcsr, zero
[0x80001010]:sd t6, 1376(t2)
[0x80001014]:sd a0, 1384(t2)
[0x80001018]:ld t5, 712(s1)
[0x8000101c]:addi fp, zero, 0
[0x80001020]:csrrw zero, fcsr, fp
[0x80001024]:fsqrt.s t6, t5, dyn

[0x80001024]:fsqrt.s t6, t5, dyn
[0x80001028]:csrrs a0, fcsr, zero
[0x8000102c]:sd t6, 1392(t2)
[0x80001030]:sd a0, 1400(t2)
[0x80001034]:ld t5, 720(s1)
[0x80001038]:addi fp, zero, 0
[0x8000103c]:csrrw zero, fcsr, fp
[0x80001040]:fsqrt.s t6, t5, dyn

[0x80001040]:fsqrt.s t6, t5, dyn
[0x80001044]:csrrs a0, fcsr, zero
[0x80001048]:sd t6, 1408(t2)
[0x8000104c]:sd a0, 1416(t2)
[0x80001050]:ld t5, 728(s1)
[0x80001054]:addi fp, zero, 0
[0x80001058]:csrrw zero, fcsr, fp
[0x8000105c]:fsqrt.s t6, t5, dyn

[0x8000105c]:fsqrt.s t6, t5, dyn
[0x80001060]:csrrs a0, fcsr, zero
[0x80001064]:sd t6, 1424(t2)
[0x80001068]:sd a0, 1432(t2)
[0x8000106c]:ld t5, 736(s1)
[0x80001070]:addi fp, zero, 0
[0x80001074]:csrrw zero, fcsr, fp
[0x80001078]:fsqrt.s t6, t5, dyn

[0x80001078]:fsqrt.s t6, t5, dyn
[0x8000107c]:csrrs a0, fcsr, zero
[0x80001080]:sd t6, 1440(t2)
[0x80001084]:sd a0, 1448(t2)
[0x80001088]:ld t5, 744(s1)
[0x8000108c]:addi fp, zero, 0
[0x80001090]:csrrw zero, fcsr, fp
[0x80001094]:fsqrt.s t6, t5, dyn

[0x80001094]:fsqrt.s t6, t5, dyn
[0x80001098]:csrrs a0, fcsr, zero
[0x8000109c]:sd t6, 1456(t2)
[0x800010a0]:sd a0, 1464(t2)
[0x800010a4]:ld t5, 752(s1)
[0x800010a8]:addi fp, zero, 0
[0x800010ac]:csrrw zero, fcsr, fp
[0x800010b0]:fsqrt.s t6, t5, dyn

[0x800010b0]:fsqrt.s t6, t5, dyn
[0x800010b4]:csrrs a0, fcsr, zero
[0x800010b8]:sd t6, 1472(t2)
[0x800010bc]:sd a0, 1480(t2)
[0x800010c0]:ld t5, 760(s1)
[0x800010c4]:addi fp, zero, 0
[0x800010c8]:csrrw zero, fcsr, fp
[0x800010cc]:fsqrt.s t6, t5, dyn

[0x800010cc]:fsqrt.s t6, t5, dyn
[0x800010d0]:csrrs a0, fcsr, zero
[0x800010d4]:sd t6, 1488(t2)
[0x800010d8]:sd a0, 1496(t2)
[0x800010dc]:ld t5, 768(s1)
[0x800010e0]:addi fp, zero, 0
[0x800010e4]:csrrw zero, fcsr, fp
[0x800010e8]:fsqrt.s t6, t5, dyn

[0x800010e8]:fsqrt.s t6, t5, dyn
[0x800010ec]:csrrs a0, fcsr, zero
[0x800010f0]:sd t6, 1504(t2)
[0x800010f4]:sd a0, 1512(t2)
[0x800010f8]:ld t5, 776(s1)
[0x800010fc]:addi fp, zero, 0
[0x80001100]:csrrw zero, fcsr, fp
[0x80001104]:fsqrt.s t6, t5, dyn

[0x80001104]:fsqrt.s t6, t5, dyn
[0x80001108]:csrrs a0, fcsr, zero
[0x8000110c]:sd t6, 1520(t2)
[0x80001110]:sd a0, 1528(t2)
[0x80001114]:ld t5, 784(s1)
[0x80001118]:addi fp, zero, 0
[0x8000111c]:csrrw zero, fcsr, fp
[0x80001120]:fsqrt.s t6, t5, dyn

[0x80001120]:fsqrt.s t6, t5, dyn
[0x80001124]:csrrs a0, fcsr, zero
[0x80001128]:sd t6, 1536(t2)
[0x8000112c]:sd a0, 1544(t2)
[0x80001130]:ld t5, 792(s1)
[0x80001134]:addi fp, zero, 0
[0x80001138]:csrrw zero, fcsr, fp
[0x8000113c]:fsqrt.s t6, t5, dyn

[0x8000113c]:fsqrt.s t6, t5, dyn
[0x80001140]:csrrs a0, fcsr, zero
[0x80001144]:sd t6, 1552(t2)
[0x80001148]:sd a0, 1560(t2)
[0x8000114c]:ld t5, 800(s1)
[0x80001150]:addi fp, zero, 0
[0x80001154]:csrrw zero, fcsr, fp
[0x80001158]:fsqrt.s t6, t5, dyn

[0x80001158]:fsqrt.s t6, t5, dyn
[0x8000115c]:csrrs a0, fcsr, zero
[0x80001160]:sd t6, 1568(t2)
[0x80001164]:sd a0, 1576(t2)
[0x80001168]:ld t5, 808(s1)
[0x8000116c]:addi fp, zero, 0
[0x80001170]:csrrw zero, fcsr, fp
[0x80001174]:fsqrt.s t6, t5, dyn

[0x80001174]:fsqrt.s t6, t5, dyn
[0x80001178]:csrrs a0, fcsr, zero
[0x8000117c]:sd t6, 1584(t2)
[0x80001180]:sd a0, 1592(t2)
[0x80001184]:ld t5, 816(s1)
[0x80001188]:addi fp, zero, 0
[0x8000118c]:csrrw zero, fcsr, fp
[0x80001190]:fsqrt.s t6, t5, dyn

[0x80001190]:fsqrt.s t6, t5, dyn
[0x80001194]:csrrs a0, fcsr, zero
[0x80001198]:sd t6, 1600(t2)
[0x8000119c]:sd a0, 1608(t2)
[0x800011a0]:ld t5, 824(s1)
[0x800011a4]:addi fp, zero, 0
[0x800011a8]:csrrw zero, fcsr, fp
[0x800011ac]:fsqrt.s t6, t5, dyn

[0x800011ac]:fsqrt.s t6, t5, dyn
[0x800011b0]:csrrs a0, fcsr, zero
[0x800011b4]:sd t6, 1616(t2)
[0x800011b8]:sd a0, 1624(t2)
[0x800011bc]:ld t5, 832(s1)
[0x800011c0]:addi fp, zero, 0
[0x800011c4]:csrrw zero, fcsr, fp
[0x800011c8]:fsqrt.s t6, t5, dyn

[0x800011c8]:fsqrt.s t6, t5, dyn
[0x800011cc]:csrrs a0, fcsr, zero
[0x800011d0]:sd t6, 1632(t2)
[0x800011d4]:sd a0, 1640(t2)
[0x800011d8]:ld t5, 840(s1)
[0x800011dc]:addi fp, zero, 0
[0x800011e0]:csrrw zero, fcsr, fp
[0x800011e4]:fsqrt.s t6, t5, dyn

[0x800011e4]:fsqrt.s t6, t5, dyn
[0x800011e8]:csrrs a0, fcsr, zero
[0x800011ec]:sd t6, 1648(t2)
[0x800011f0]:sd a0, 1656(t2)
[0x800011f4]:ld t5, 848(s1)
[0x800011f8]:addi fp, zero, 0
[0x800011fc]:csrrw zero, fcsr, fp
[0x80001200]:fsqrt.s t6, t5, dyn

[0x80001200]:fsqrt.s t6, t5, dyn
[0x80001204]:csrrs a0, fcsr, zero
[0x80001208]:sd t6, 1664(t2)
[0x8000120c]:sd a0, 1672(t2)
[0x80001210]:ld t5, 856(s1)
[0x80001214]:addi fp, zero, 0
[0x80001218]:csrrw zero, fcsr, fp
[0x8000121c]:fsqrt.s t6, t5, dyn

[0x8000121c]:fsqrt.s t6, t5, dyn
[0x80001220]:csrrs a0, fcsr, zero
[0x80001224]:sd t6, 1680(t2)
[0x80001228]:sd a0, 1688(t2)
[0x8000122c]:ld t5, 864(s1)
[0x80001230]:addi fp, zero, 0
[0x80001234]:csrrw zero, fcsr, fp
[0x80001238]:fsqrt.s t6, t5, dyn

[0x80001238]:fsqrt.s t6, t5, dyn
[0x8000123c]:csrrs a0, fcsr, zero
[0x80001240]:sd t6, 1696(t2)
[0x80001244]:sd a0, 1704(t2)
[0x80001248]:ld t5, 872(s1)
[0x8000124c]:addi fp, zero, 0
[0x80001250]:csrrw zero, fcsr, fp
[0x80001254]:fsqrt.s t6, t5, dyn

[0x80001254]:fsqrt.s t6, t5, dyn
[0x80001258]:csrrs a0, fcsr, zero
[0x8000125c]:sd t6, 1712(t2)
[0x80001260]:sd a0, 1720(t2)
[0x80001264]:ld t5, 880(s1)
[0x80001268]:addi fp, zero, 0
[0x8000126c]:csrrw zero, fcsr, fp
[0x80001270]:fsqrt.s t6, t5, dyn

[0x80001270]:fsqrt.s t6, t5, dyn
[0x80001274]:csrrs a0, fcsr, zero
[0x80001278]:sd t6, 1728(t2)
[0x8000127c]:sd a0, 1736(t2)
[0x80001280]:ld t5, 888(s1)
[0x80001284]:addi fp, zero, 0
[0x80001288]:csrrw zero, fcsr, fp
[0x8000128c]:fsqrt.s t6, t5, dyn

[0x8000128c]:fsqrt.s t6, t5, dyn
[0x80001290]:csrrs a0, fcsr, zero
[0x80001294]:sd t6, 1744(t2)
[0x80001298]:sd a0, 1752(t2)
[0x8000129c]:ld t5, 896(s1)
[0x800012a0]:addi fp, zero, 0
[0x800012a4]:csrrw zero, fcsr, fp
[0x800012a8]:fsqrt.s t6, t5, dyn

[0x800012a8]:fsqrt.s t6, t5, dyn
[0x800012ac]:csrrs a0, fcsr, zero
[0x800012b0]:sd t6, 1760(t2)
[0x800012b4]:sd a0, 1768(t2)
[0x800012b8]:ld t5, 904(s1)
[0x800012bc]:addi fp, zero, 0
[0x800012c0]:csrrw zero, fcsr, fp
[0x800012c4]:fsqrt.s t6, t5, dyn

[0x800012c4]:fsqrt.s t6, t5, dyn
[0x800012c8]:csrrs a0, fcsr, zero
[0x800012cc]:sd t6, 1776(t2)
[0x800012d0]:sd a0, 1784(t2)
[0x800012d4]:ld t5, 912(s1)
[0x800012d8]:addi fp, zero, 0
[0x800012dc]:csrrw zero, fcsr, fp
[0x800012e0]:fsqrt.s t6, t5, dyn

[0x800012e0]:fsqrt.s t6, t5, dyn
[0x800012e4]:csrrs a0, fcsr, zero
[0x800012e8]:sd t6, 1792(t2)
[0x800012ec]:sd a0, 1800(t2)
[0x800012f0]:ld t5, 920(s1)
[0x800012f4]:addi fp, zero, 0
[0x800012f8]:csrrw zero, fcsr, fp
[0x800012fc]:fsqrt.s t6, t5, dyn

[0x800012fc]:fsqrt.s t6, t5, dyn
[0x80001300]:csrrs a0, fcsr, zero
[0x80001304]:sd t6, 1808(t2)
[0x80001308]:sd a0, 1816(t2)
[0x8000130c]:ld t5, 928(s1)
[0x80001310]:addi fp, zero, 0
[0x80001314]:csrrw zero, fcsr, fp
[0x80001318]:fsqrt.s t6, t5, dyn

[0x80001318]:fsqrt.s t6, t5, dyn
[0x8000131c]:csrrs a0, fcsr, zero
[0x80001320]:sd t6, 1824(t2)
[0x80001324]:sd a0, 1832(t2)
[0x80001328]:ld t5, 936(s1)
[0x8000132c]:addi fp, zero, 0
[0x80001330]:csrrw zero, fcsr, fp
[0x80001334]:fsqrt.s t6, t5, dyn

[0x80001334]:fsqrt.s t6, t5, dyn
[0x80001338]:csrrs a0, fcsr, zero
[0x8000133c]:sd t6, 1840(t2)
[0x80001340]:sd a0, 1848(t2)
[0x80001344]:ld t5, 944(s1)
[0x80001348]:addi fp, zero, 0
[0x8000134c]:csrrw zero, fcsr, fp
[0x80001350]:fsqrt.s t6, t5, dyn

[0x80001350]:fsqrt.s t6, t5, dyn
[0x80001354]:csrrs a0, fcsr, zero
[0x80001358]:sd t6, 1856(t2)
[0x8000135c]:sd a0, 1864(t2)
[0x80001360]:ld t5, 952(s1)
[0x80001364]:addi fp, zero, 0
[0x80001368]:csrrw zero, fcsr, fp
[0x8000136c]:fsqrt.s t6, t5, dyn

[0x8000136c]:fsqrt.s t6, t5, dyn
[0x80001370]:csrrs a0, fcsr, zero
[0x80001374]:sd t6, 1872(t2)
[0x80001378]:sd a0, 1880(t2)
[0x8000137c]:ld t5, 960(s1)
[0x80001380]:addi fp, zero, 0
[0x80001384]:csrrw zero, fcsr, fp
[0x80001388]:fsqrt.s t6, t5, dyn

[0x80001388]:fsqrt.s t6, t5, dyn
[0x8000138c]:csrrs a0, fcsr, zero
[0x80001390]:sd t6, 1888(t2)
[0x80001394]:sd a0, 1896(t2)
[0x80001398]:ld t5, 968(s1)
[0x8000139c]:addi fp, zero, 0
[0x800013a0]:csrrw zero, fcsr, fp
[0x800013a4]:fsqrt.s t6, t5, dyn

[0x800013a4]:fsqrt.s t6, t5, dyn
[0x800013a8]:csrrs a0, fcsr, zero
[0x800013ac]:sd t6, 1904(t2)
[0x800013b0]:sd a0, 1912(t2)
[0x800013b4]:ld t5, 976(s1)
[0x800013b8]:addi fp, zero, 0
[0x800013bc]:csrrw zero, fcsr, fp
[0x800013c0]:fsqrt.s t6, t5, dyn

[0x800013c0]:fsqrt.s t6, t5, dyn
[0x800013c4]:csrrs a0, fcsr, zero
[0x800013c8]:sd t6, 1920(t2)
[0x800013cc]:sd a0, 1928(t2)
[0x800013d0]:ld t5, 984(s1)
[0x800013d4]:addi fp, zero, 0
[0x800013d8]:csrrw zero, fcsr, fp
[0x800013dc]:fsqrt.s t6, t5, dyn

[0x800013dc]:fsqrt.s t6, t5, dyn
[0x800013e0]:csrrs a0, fcsr, zero
[0x800013e4]:sd t6, 1936(t2)
[0x800013e8]:sd a0, 1944(t2)
[0x800013ec]:ld t5, 992(s1)
[0x800013f0]:addi fp, zero, 0
[0x800013f4]:csrrw zero, fcsr, fp
[0x800013f8]:fsqrt.s t6, t5, dyn

[0x800013f8]:fsqrt.s t6, t5, dyn
[0x800013fc]:csrrs a0, fcsr, zero
[0x80001400]:sd t6, 1952(t2)
[0x80001404]:sd a0, 1960(t2)
[0x80001408]:ld t5, 1000(s1)
[0x8000140c]:addi fp, zero, 0
[0x80001410]:csrrw zero, fcsr, fp
[0x80001414]:fsqrt.s t6, t5, dyn

[0x80001414]:fsqrt.s t6, t5, dyn
[0x80001418]:csrrs a0, fcsr, zero
[0x8000141c]:sd t6, 1968(t2)
[0x80001420]:sd a0, 1976(t2)
[0x80001424]:ld t5, 1008(s1)
[0x80001428]:addi fp, zero, 0
[0x8000142c]:csrrw zero, fcsr, fp
[0x80001430]:fsqrt.s t6, t5, dyn

[0x80001430]:fsqrt.s t6, t5, dyn
[0x80001434]:csrrs a0, fcsr, zero
[0x80001438]:sd t6, 1984(t2)
[0x8000143c]:sd a0, 1992(t2)
[0x80001440]:ld t5, 1016(s1)
[0x80001444]:addi fp, zero, 0
[0x80001448]:csrrw zero, fcsr, fp
[0x8000144c]:fsqrt.s t6, t5, dyn

[0x8000144c]:fsqrt.s t6, t5, dyn
[0x80001450]:csrrs a0, fcsr, zero
[0x80001454]:sd t6, 2000(t2)
[0x80001458]:sd a0, 2008(t2)
[0x8000145c]:ld t5, 1024(s1)
[0x80001460]:addi fp, zero, 0
[0x80001464]:csrrw zero, fcsr, fp
[0x80001468]:fsqrt.s t6, t5, dyn

[0x80001468]:fsqrt.s t6, t5, dyn
[0x8000146c]:csrrs a0, fcsr, zero
[0x80001470]:sd t6, 2016(t2)
[0x80001474]:sd a0, 2024(t2)
[0x80001478]:ld t5, 1032(s1)
[0x8000147c]:addi fp, zero, 0
[0x80001480]:csrrw zero, fcsr, fp
[0x80001484]:fsqrt.s t6, t5, dyn

[0x80001484]:fsqrt.s t6, t5, dyn
[0x80001488]:csrrs a0, fcsr, zero
[0x8000148c]:sd t6, 2032(t2)
[0x80001490]:sd a0, 2040(t2)
[0x80001494]:ld t5, 1040(s1)
[0x80001498]:addi fp, zero, 0
[0x8000149c]:csrrw zero, fcsr, fp
[0x800014a0]:fsqrt.s t6, t5, dyn

[0x800014a0]:fsqrt.s t6, t5, dyn
[0x800014a4]:csrrs a0, fcsr, zero
[0x800014a8]:addi t2, t2, 2040
[0x800014ac]:sd t6, 8(t2)
[0x800014b0]:sd a0, 16(t2)
[0x800014b4]:ld t5, 1048(s1)
[0x800014b8]:addi fp, zero, 0
[0x800014bc]:csrrw zero, fcsr, fp
[0x800014c0]:fsqrt.s t6, t5, dyn

[0x800014c0]:fsqrt.s t6, t5, dyn
[0x800014c4]:csrrs a0, fcsr, zero
[0x800014c8]:sd t6, 24(t2)
[0x800014cc]:sd a0, 32(t2)
[0x800014d0]:ld t5, 1056(s1)
[0x800014d4]:addi fp, zero, 0
[0x800014d8]:csrrw zero, fcsr, fp
[0x800014dc]:fsqrt.s t6, t5, dyn

[0x800014dc]:fsqrt.s t6, t5, dyn
[0x800014e0]:csrrs a0, fcsr, zero
[0x800014e4]:sd t6, 40(t2)
[0x800014e8]:sd a0, 48(t2)
[0x800014ec]:ld t5, 1064(s1)
[0x800014f0]:addi fp, zero, 0
[0x800014f4]:csrrw zero, fcsr, fp
[0x800014f8]:fsqrt.s t6, t5, dyn

[0x800014f8]:fsqrt.s t6, t5, dyn
[0x800014fc]:csrrs a0, fcsr, zero
[0x80001500]:sd t6, 56(t2)
[0x80001504]:sd a0, 64(t2)
[0x80001508]:ld t5, 1072(s1)
[0x8000150c]:addi fp, zero, 0
[0x80001510]:csrrw zero, fcsr, fp
[0x80001514]:fsqrt.s t6, t5, dyn

[0x80001514]:fsqrt.s t6, t5, dyn
[0x80001518]:csrrs a0, fcsr, zero
[0x8000151c]:sd t6, 72(t2)
[0x80001520]:sd a0, 80(t2)
[0x80001524]:ld t5, 1080(s1)
[0x80001528]:addi fp, zero, 0
[0x8000152c]:csrrw zero, fcsr, fp
[0x80001530]:fsqrt.s t6, t5, dyn

[0x80001530]:fsqrt.s t6, t5, dyn
[0x80001534]:csrrs a0, fcsr, zero
[0x80001538]:sd t6, 88(t2)
[0x8000153c]:sd a0, 96(t2)
[0x80001540]:ld t5, 1088(s1)
[0x80001544]:addi fp, zero, 0
[0x80001548]:csrrw zero, fcsr, fp
[0x8000154c]:fsqrt.s t6, t5, dyn

[0x8000154c]:fsqrt.s t6, t5, dyn
[0x80001550]:csrrs a0, fcsr, zero
[0x80001554]:sd t6, 104(t2)
[0x80001558]:sd a0, 112(t2)
[0x8000155c]:ld t5, 1096(s1)
[0x80001560]:addi fp, zero, 0
[0x80001564]:csrrw zero, fcsr, fp
[0x80001568]:fsqrt.s t6, t5, dyn

[0x80001568]:fsqrt.s t6, t5, dyn
[0x8000156c]:csrrs a0, fcsr, zero
[0x80001570]:sd t6, 120(t2)
[0x80001574]:sd a0, 128(t2)
[0x80001578]:ld t5, 1104(s1)
[0x8000157c]:addi fp, zero, 0
[0x80001580]:csrrw zero, fcsr, fp
[0x80001584]:fsqrt.s t6, t5, dyn

[0x80001584]:fsqrt.s t6, t5, dyn
[0x80001588]:csrrs a0, fcsr, zero
[0x8000158c]:sd t6, 136(t2)
[0x80001590]:sd a0, 144(t2)
[0x80001594]:ld t5, 1112(s1)
[0x80001598]:addi fp, zero, 0
[0x8000159c]:csrrw zero, fcsr, fp
[0x800015a0]:fsqrt.s t6, t5, dyn

[0x800015a0]:fsqrt.s t6, t5, dyn
[0x800015a4]:csrrs a0, fcsr, zero
[0x800015a8]:sd t6, 152(t2)
[0x800015ac]:sd a0, 160(t2)
[0x800015b0]:ld t5, 1120(s1)
[0x800015b4]:addi fp, zero, 0
[0x800015b8]:csrrw zero, fcsr, fp
[0x800015bc]:fsqrt.s t6, t5, dyn

[0x800015bc]:fsqrt.s t6, t5, dyn
[0x800015c0]:csrrs a0, fcsr, zero
[0x800015c4]:sd t6, 168(t2)
[0x800015c8]:sd a0, 176(t2)
[0x800015cc]:ld t5, 1128(s1)
[0x800015d0]:addi fp, zero, 0
[0x800015d4]:csrrw zero, fcsr, fp
[0x800015d8]:fsqrt.s t6, t5, dyn

[0x800015d8]:fsqrt.s t6, t5, dyn
[0x800015dc]:csrrs a0, fcsr, zero
[0x800015e0]:sd t6, 184(t2)
[0x800015e4]:sd a0, 192(t2)
[0x800015e8]:ld t5, 1136(s1)
[0x800015ec]:addi fp, zero, 0
[0x800015f0]:csrrw zero, fcsr, fp
[0x800015f4]:fsqrt.s t6, t5, dyn

[0x800015f4]:fsqrt.s t6, t5, dyn
[0x800015f8]:csrrs a0, fcsr, zero
[0x800015fc]:sd t6, 200(t2)
[0x80001600]:sd a0, 208(t2)
[0x80001604]:ld t5, 1144(s1)
[0x80001608]:addi fp, zero, 0
[0x8000160c]:csrrw zero, fcsr, fp
[0x80001610]:fsqrt.s t6, t5, dyn

[0x80001610]:fsqrt.s t6, t5, dyn
[0x80001614]:csrrs a0, fcsr, zero
[0x80001618]:sd t6, 216(t2)
[0x8000161c]:sd a0, 224(t2)
[0x80001620]:ld t5, 1152(s1)
[0x80001624]:addi fp, zero, 0
[0x80001628]:csrrw zero, fcsr, fp
[0x8000162c]:fsqrt.s t6, t5, dyn

[0x8000162c]:fsqrt.s t6, t5, dyn
[0x80001630]:csrrs a0, fcsr, zero
[0x80001634]:sd t6, 232(t2)
[0x80001638]:sd a0, 240(t2)
[0x8000163c]:ld t5, 1160(s1)
[0x80001640]:addi fp, zero, 0
[0x80001644]:csrrw zero, fcsr, fp
[0x80001648]:fsqrt.s t6, t5, dyn

[0x80001648]:fsqrt.s t6, t5, dyn
[0x8000164c]:csrrs a0, fcsr, zero
[0x80001650]:sd t6, 248(t2)
[0x80001654]:sd a0, 256(t2)
[0x80001658]:ld t5, 1168(s1)
[0x8000165c]:addi fp, zero, 0
[0x80001660]:csrrw zero, fcsr, fp
[0x80001664]:fsqrt.s t6, t5, dyn

[0x80001664]:fsqrt.s t6, t5, dyn
[0x80001668]:csrrs a0, fcsr, zero
[0x8000166c]:sd t6, 264(t2)
[0x80001670]:sd a0, 272(t2)
[0x80001674]:ld t5, 1176(s1)
[0x80001678]:addi fp, zero, 0
[0x8000167c]:csrrw zero, fcsr, fp
[0x80001680]:fsqrt.s t6, t5, dyn

[0x80001680]:fsqrt.s t6, t5, dyn
[0x80001684]:csrrs a0, fcsr, zero
[0x80001688]:sd t6, 280(t2)
[0x8000168c]:sd a0, 288(t2)
[0x80001690]:ld t5, 1184(s1)
[0x80001694]:addi fp, zero, 0
[0x80001698]:csrrw zero, fcsr, fp
[0x8000169c]:fsqrt.s t6, t5, dyn

[0x8000169c]:fsqrt.s t6, t5, dyn
[0x800016a0]:csrrs a0, fcsr, zero
[0x800016a4]:sd t6, 296(t2)
[0x800016a8]:sd a0, 304(t2)
[0x800016ac]:ld t5, 1192(s1)
[0x800016b0]:addi fp, zero, 0
[0x800016b4]:csrrw zero, fcsr, fp
[0x800016b8]:fsqrt.s t6, t5, dyn

[0x800016b8]:fsqrt.s t6, t5, dyn
[0x800016bc]:csrrs a0, fcsr, zero
[0x800016c0]:sd t6, 312(t2)
[0x800016c4]:sd a0, 320(t2)
[0x800016c8]:ld t5, 1200(s1)
[0x800016cc]:addi fp, zero, 0
[0x800016d0]:csrrw zero, fcsr, fp
[0x800016d4]:fsqrt.s t6, t5, dyn

[0x800016d4]:fsqrt.s t6, t5, dyn
[0x800016d8]:csrrs a0, fcsr, zero
[0x800016dc]:sd t6, 328(t2)
[0x800016e0]:sd a0, 336(t2)
[0x800016e4]:ld t5, 1208(s1)
[0x800016e8]:addi fp, zero, 0
[0x800016ec]:csrrw zero, fcsr, fp
[0x800016f0]:fsqrt.s t6, t5, dyn

[0x800016f0]:fsqrt.s t6, t5, dyn
[0x800016f4]:csrrs a0, fcsr, zero
[0x800016f8]:sd t6, 344(t2)
[0x800016fc]:sd a0, 352(t2)
[0x80001700]:ld t5, 1216(s1)
[0x80001704]:addi fp, zero, 0
[0x80001708]:csrrw zero, fcsr, fp
[0x8000170c]:fsqrt.s t6, t5, dyn

[0x8000170c]:fsqrt.s t6, t5, dyn
[0x80001710]:csrrs a0, fcsr, zero
[0x80001714]:sd t6, 360(t2)
[0x80001718]:sd a0, 368(t2)
[0x8000171c]:ld t5, 1224(s1)
[0x80001720]:addi fp, zero, 0
[0x80001724]:csrrw zero, fcsr, fp
[0x80001728]:fsqrt.s t6, t5, dyn

[0x80001728]:fsqrt.s t6, t5, dyn
[0x8000172c]:csrrs a0, fcsr, zero
[0x80001730]:sd t6, 376(t2)
[0x80001734]:sd a0, 384(t2)
[0x80001738]:ld t5, 1232(s1)
[0x8000173c]:addi fp, zero, 0
[0x80001740]:csrrw zero, fcsr, fp
[0x80001744]:fsqrt.s t6, t5, dyn

[0x80001744]:fsqrt.s t6, t5, dyn
[0x80001748]:csrrs a0, fcsr, zero
[0x8000174c]:sd t6, 392(t2)
[0x80001750]:sd a0, 400(t2)
[0x80001754]:ld t5, 1240(s1)
[0x80001758]:addi fp, zero, 0
[0x8000175c]:csrrw zero, fcsr, fp
[0x80001760]:fsqrt.s t6, t5, dyn

[0x80001760]:fsqrt.s t6, t5, dyn
[0x80001764]:csrrs a0, fcsr, zero
[0x80001768]:sd t6, 408(t2)
[0x8000176c]:sd a0, 416(t2)
[0x80001770]:ld t5, 1248(s1)
[0x80001774]:addi fp, zero, 0
[0x80001778]:csrrw zero, fcsr, fp
[0x8000177c]:fsqrt.s t6, t5, dyn

[0x8000177c]:fsqrt.s t6, t5, dyn
[0x80001780]:csrrs a0, fcsr, zero
[0x80001784]:sd t6, 424(t2)
[0x80001788]:sd a0, 432(t2)
[0x8000178c]:ld t5, 1256(s1)
[0x80001790]:addi fp, zero, 0
[0x80001794]:csrrw zero, fcsr, fp
[0x80001798]:fsqrt.s t6, t5, dyn

[0x80001798]:fsqrt.s t6, t5, dyn
[0x8000179c]:csrrs a0, fcsr, zero
[0x800017a0]:sd t6, 440(t2)
[0x800017a4]:sd a0, 448(t2)
[0x800017a8]:ld t5, 1264(s1)
[0x800017ac]:addi fp, zero, 0
[0x800017b0]:csrrw zero, fcsr, fp
[0x800017b4]:fsqrt.s t6, t5, dyn

[0x800017b4]:fsqrt.s t6, t5, dyn
[0x800017b8]:csrrs a0, fcsr, zero
[0x800017bc]:sd t6, 456(t2)
[0x800017c0]:sd a0, 464(t2)
[0x800017c4]:ld t5, 1272(s1)
[0x800017c8]:addi fp, zero, 0
[0x800017cc]:csrrw zero, fcsr, fp
[0x800017d0]:fsqrt.s t6, t5, dyn

[0x800017d0]:fsqrt.s t6, t5, dyn
[0x800017d4]:csrrs a0, fcsr, zero
[0x800017d8]:sd t6, 472(t2)
[0x800017dc]:sd a0, 480(t2)
[0x800017e0]:ld t5, 1280(s1)
[0x800017e4]:addi fp, zero, 0
[0x800017e8]:csrrw zero, fcsr, fp
[0x800017ec]:fsqrt.s t6, t5, dyn

[0x800017ec]:fsqrt.s t6, t5, dyn
[0x800017f0]:csrrs a0, fcsr, zero
[0x800017f4]:sd t6, 488(t2)
[0x800017f8]:sd a0, 496(t2)
[0x800017fc]:ld t5, 1288(s1)
[0x80001800]:addi fp, zero, 0
[0x80001804]:csrrw zero, fcsr, fp
[0x80001808]:fsqrt.s t6, t5, dyn

[0x80001808]:fsqrt.s t6, t5, dyn
[0x8000180c]:csrrs a0, fcsr, zero
[0x80001810]:sd t6, 504(t2)
[0x80001814]:sd a0, 512(t2)
[0x80001818]:ld t5, 1296(s1)
[0x8000181c]:addi fp, zero, 0
[0x80001820]:csrrw zero, fcsr, fp
[0x80001824]:fsqrt.s t6, t5, dyn

[0x80001824]:fsqrt.s t6, t5, dyn
[0x80001828]:csrrs a0, fcsr, zero
[0x8000182c]:sd t6, 520(t2)
[0x80001830]:sd a0, 528(t2)
[0x80001834]:ld t5, 1304(s1)
[0x80001838]:addi fp, zero, 0
[0x8000183c]:csrrw zero, fcsr, fp
[0x80001840]:fsqrt.s t6, t5, dyn

[0x80001840]:fsqrt.s t6, t5, dyn
[0x80001844]:csrrs a0, fcsr, zero
[0x80001848]:sd t6, 536(t2)
[0x8000184c]:sd a0, 544(t2)
[0x80001850]:ld t5, 1312(s1)
[0x80001854]:addi fp, zero, 0
[0x80001858]:csrrw zero, fcsr, fp
[0x8000185c]:fsqrt.s t6, t5, dyn

[0x8000185c]:fsqrt.s t6, t5, dyn
[0x80001860]:csrrs a0, fcsr, zero
[0x80001864]:sd t6, 552(t2)
[0x80001868]:sd a0, 560(t2)
[0x8000186c]:ld t5, 1320(s1)
[0x80001870]:addi fp, zero, 0
[0x80001874]:csrrw zero, fcsr, fp
[0x80001878]:fsqrt.s t6, t5, dyn

[0x80001878]:fsqrt.s t6, t5, dyn
[0x8000187c]:csrrs a0, fcsr, zero
[0x80001880]:sd t6, 568(t2)
[0x80001884]:sd a0, 576(t2)
[0x80001888]:ld t5, 1328(s1)
[0x8000188c]:addi fp, zero, 0
[0x80001890]:csrrw zero, fcsr, fp
[0x80001894]:fsqrt.s t6, t5, dyn

[0x80001894]:fsqrt.s t6, t5, dyn
[0x80001898]:csrrs a0, fcsr, zero
[0x8000189c]:sd t6, 584(t2)
[0x800018a0]:sd a0, 592(t2)
[0x800018a4]:ld t5, 1336(s1)
[0x800018a8]:addi fp, zero, 0
[0x800018ac]:csrrw zero, fcsr, fp
[0x800018b0]:fsqrt.s t6, t5, dyn

[0x800018b0]:fsqrt.s t6, t5, dyn
[0x800018b4]:csrrs a0, fcsr, zero
[0x800018b8]:sd t6, 600(t2)
[0x800018bc]:sd a0, 608(t2)
[0x800018c0]:ld t5, 1344(s1)
[0x800018c4]:addi fp, zero, 0
[0x800018c8]:csrrw zero, fcsr, fp
[0x800018cc]:fsqrt.s t6, t5, dyn

[0x800018cc]:fsqrt.s t6, t5, dyn
[0x800018d0]:csrrs a0, fcsr, zero
[0x800018d4]:sd t6, 616(t2)
[0x800018d8]:sd a0, 624(t2)
[0x800018dc]:ld t5, 1352(s1)
[0x800018e0]:addi fp, zero, 0
[0x800018e4]:csrrw zero, fcsr, fp
[0x800018e8]:fsqrt.s t6, t5, dyn

[0x800018e8]:fsqrt.s t6, t5, dyn
[0x800018ec]:csrrs a0, fcsr, zero
[0x800018f0]:sd t6, 632(t2)
[0x800018f4]:sd a0, 640(t2)
[0x800018f8]:ld t5, 1360(s1)
[0x800018fc]:addi fp, zero, 0
[0x80001900]:csrrw zero, fcsr, fp
[0x80001904]:fsqrt.s t6, t5, dyn

[0x80001904]:fsqrt.s t6, t5, dyn
[0x80001908]:csrrs a0, fcsr, zero
[0x8000190c]:sd t6, 648(t2)
[0x80001910]:sd a0, 656(t2)
[0x80001914]:ld t5, 1368(s1)
[0x80001918]:addi fp, zero, 0
[0x8000191c]:csrrw zero, fcsr, fp
[0x80001920]:fsqrt.s t6, t5, dyn



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fsqrt.s', 'rs1 : x31', 'rd : x31', 'rs1 == rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800003b8]:fsqrt.s t6, t6, dyn
	-[0x800003bc]:csrrs tp, fcsr, zero
	-[0x800003c0]:sd t6, 0(ra)
	-[0x800003c4]:sd tp, 8(ra)
Current Store : [0x800003c4] : sd tp, 8(ra) -- Store: [0x80003820]:0x0000000000000000




Last Coverpoint : ['rs1 : x29', 'rd : x30', 'rs1 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800003d4]:fsqrt.s t5, t4, dyn
	-[0x800003d8]:csrrs tp, fcsr, zero
	-[0x800003dc]:sd t5, 16(ra)
	-[0x800003e0]:sd tp, 24(ra)
Current Store : [0x800003e0] : sd tp, 24(ra) -- Store: [0x80003830]:0x0000000000000001




Last Coverpoint : ['rs1 : x30', 'rd : x29', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x400000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800003f0]:fsqrt.s t4, t5, dyn
	-[0x800003f4]:csrrs tp, fcsr, zero
	-[0x800003f8]:sd t4, 32(ra)
	-[0x800003fc]:sd tp, 40(ra)
Current Store : [0x800003fc] : sd tp, 40(ra) -- Store: [0x80003840]:0x0000000000000001




Last Coverpoint : ['rs1 : x27', 'rd : x28', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000040c]:fsqrt.s t3, s11, dyn
	-[0x80000410]:csrrs tp, fcsr, zero
	-[0x80000414]:sd t3, 48(ra)
	-[0x80000418]:sd tp, 56(ra)
Current Store : [0x80000418] : sd tp, 56(ra) -- Store: [0x80003850]:0x0000000000000001




Last Coverpoint : ['rs1 : x28', 'rd : x27', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x600000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000428]:fsqrt.s s11, t3, dyn
	-[0x8000042c]:csrrs tp, fcsr, zero
	-[0x80000430]:sd s11, 64(ra)
	-[0x80000434]:sd tp, 72(ra)
Current Store : [0x80000434] : sd tp, 72(ra) -- Store: [0x80003860]:0x0000000000000001




Last Coverpoint : ['rs1 : x25', 'rd : x26', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x1fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000444]:fsqrt.s s10, s9, dyn
	-[0x80000448]:csrrs tp, fcsr, zero
	-[0x8000044c]:sd s10, 80(ra)
	-[0x80000450]:sd tp, 88(ra)
Current Store : [0x80000450] : sd tp, 88(ra) -- Store: [0x80003870]:0x0000000000000001




Last Coverpoint : ['rs1 : x26', 'rd : x25', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x700000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000460]:fsqrt.s s9, s10, dyn
	-[0x80000464]:csrrs tp, fcsr, zero
	-[0x80000468]:sd s9, 96(ra)
	-[0x8000046c]:sd tp, 104(ra)
Current Store : [0x8000046c] : sd tp, 104(ra) -- Store: [0x80003880]:0x0000000000000001




Last Coverpoint : ['rs1 : x23', 'rd : x24', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x0fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000047c]:fsqrt.s s8, s7, dyn
	-[0x80000480]:csrrs tp, fcsr, zero
	-[0x80000484]:sd s8, 112(ra)
	-[0x80000488]:sd tp, 120(ra)
Current Store : [0x80000488] : sd tp, 120(ra) -- Store: [0x80003890]:0x0000000000000001




Last Coverpoint : ['rs1 : x24', 'rd : x23', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x780000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000498]:fsqrt.s s7, s8, dyn
	-[0x8000049c]:csrrs tp, fcsr, zero
	-[0x800004a0]:sd s7, 128(ra)
	-[0x800004a4]:sd tp, 136(ra)
Current Store : [0x800004a4] : sd tp, 136(ra) -- Store: [0x800038a0]:0x0000000000000001




Last Coverpoint : ['rs1 : x21', 'rd : x22', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x07ffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800004b4]:fsqrt.s s6, s5, dyn
	-[0x800004b8]:csrrs tp, fcsr, zero
	-[0x800004bc]:sd s6, 144(ra)
	-[0x800004c0]:sd tp, 152(ra)
Current Store : [0x800004c0] : sd tp, 152(ra) -- Store: [0x800038b0]:0x0000000000000001




Last Coverpoint : ['rs1 : x22', 'rd : x21', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7c0000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800004d0]:fsqrt.s s5, s6, dyn
	-[0x800004d4]:csrrs tp, fcsr, zero
	-[0x800004d8]:sd s5, 160(ra)
	-[0x800004dc]:sd tp, 168(ra)
Current Store : [0x800004dc] : sd tp, 168(ra) -- Store: [0x800038c0]:0x0000000000000001




Last Coverpoint : ['rs1 : x19', 'rd : x20', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x03ffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800004ec]:fsqrt.s s4, s3, dyn
	-[0x800004f0]:csrrs tp, fcsr, zero
	-[0x800004f4]:sd s4, 176(ra)
	-[0x800004f8]:sd tp, 184(ra)
Current Store : [0x800004f8] : sd tp, 184(ra) -- Store: [0x800038d0]:0x0000000000000001




Last Coverpoint : ['rs1 : x20', 'rd : x19', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7e0000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000508]:fsqrt.s s3, s4, dyn
	-[0x8000050c]:csrrs tp, fcsr, zero
	-[0x80000510]:sd s3, 192(ra)
	-[0x80000514]:sd tp, 200(ra)
Current Store : [0x80000514] : sd tp, 200(ra) -- Store: [0x800038e0]:0x0000000000000001




Last Coverpoint : ['rs1 : x17', 'rd : x18', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x01ffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000524]:fsqrt.s s2, a7, dyn
	-[0x80000528]:csrrs tp, fcsr, zero
	-[0x8000052c]:sd s2, 208(ra)
	-[0x80000530]:sd tp, 216(ra)
Current Store : [0x80000530] : sd tp, 216(ra) -- Store: [0x800038f0]:0x0000000000000001




Last Coverpoint : ['rs1 : x18', 'rd : x17', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7f0000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000540]:fsqrt.s a7, s2, dyn
	-[0x80000544]:csrrs tp, fcsr, zero
	-[0x80000548]:sd a7, 224(ra)
	-[0x8000054c]:sd tp, 232(ra)
Current Store : [0x8000054c] : sd tp, 232(ra) -- Store: [0x80003900]:0x0000000000000001




Last Coverpoint : ['rs1 : x15', 'rd : x16', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x00ffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000055c]:fsqrt.s a6, a5, dyn
	-[0x80000560]:csrrs tp, fcsr, zero
	-[0x80000564]:sd a6, 240(ra)
	-[0x80000568]:sd tp, 248(ra)
Current Store : [0x80000568] : sd tp, 248(ra) -- Store: [0x80003910]:0x0000000000000001




Last Coverpoint : ['rs1 : x16', 'rd : x15', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7f8000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000578]:fsqrt.s a5, a6, dyn
	-[0x8000057c]:csrrs tp, fcsr, zero
	-[0x80000580]:sd a5, 256(ra)
	-[0x80000584]:sd tp, 264(ra)
Current Store : [0x80000584] : sd tp, 264(ra) -- Store: [0x80003920]:0x0000000000000001




Last Coverpoint : ['rs1 : x13', 'rd : x14', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x007fff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000594]:fsqrt.s a4, a3, dyn
	-[0x80000598]:csrrs tp, fcsr, zero
	-[0x8000059c]:sd a4, 272(ra)
	-[0x800005a0]:sd tp, 280(ra)
Current Store : [0x800005a0] : sd tp, 280(ra) -- Store: [0x80003930]:0x0000000000000001




Last Coverpoint : ['rs1 : x14', 'rd : x13', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fc000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800005b0]:fsqrt.s a3, a4, dyn
	-[0x800005b4]:csrrs tp, fcsr, zero
	-[0x800005b8]:sd a3, 288(ra)
	-[0x800005bc]:sd tp, 296(ra)
Current Store : [0x800005bc] : sd tp, 296(ra) -- Store: [0x80003940]:0x0000000000000001




Last Coverpoint : ['rs1 : x11', 'rd : x12', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x003fff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800005cc]:fsqrt.s a2, a1, dyn
	-[0x800005d0]:csrrs tp, fcsr, zero
	-[0x800005d4]:sd a2, 304(ra)
	-[0x800005d8]:sd tp, 312(ra)
Current Store : [0x800005d8] : sd tp, 312(ra) -- Store: [0x80003950]:0x0000000000000001




Last Coverpoint : ['rs1 : x12', 'rd : x11', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fe000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800005e8]:fsqrt.s a1, a2, dyn
	-[0x800005ec]:csrrs tp, fcsr, zero
	-[0x800005f0]:sd a1, 320(ra)
	-[0x800005f4]:sd tp, 328(ra)
Current Store : [0x800005f4] : sd tp, 328(ra) -- Store: [0x80003960]:0x0000000000000001




Last Coverpoint : ['rs1 : x9', 'rd : x10', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x001fff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000604]:fsqrt.s a0, s1, dyn
	-[0x80000608]:csrrs tp, fcsr, zero
	-[0x8000060c]:sd a0, 336(ra)
	-[0x80000610]:sd tp, 344(ra)
Current Store : [0x80000610] : sd tp, 344(ra) -- Store: [0x80003970]:0x0000000000000001




Last Coverpoint : ['rs1 : x10', 'rd : x9', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ff000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000620]:fsqrt.s s1, a0, dyn
	-[0x80000624]:csrrs tp, fcsr, zero
	-[0x80000628]:sd s1, 352(ra)
	-[0x8000062c]:sd tp, 360(ra)
Current Store : [0x8000062c] : sd tp, 360(ra) -- Store: [0x80003980]:0x0000000000000001




Last Coverpoint : ['rs1 : x7', 'rd : x8', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000fff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000063c]:fsqrt.s fp, t2, dyn
	-[0x80000640]:csrrs tp, fcsr, zero
	-[0x80000644]:sd fp, 368(ra)
	-[0x80000648]:sd tp, 376(ra)
Current Store : [0x80000648] : sd tp, 376(ra) -- Store: [0x80003990]:0x0000000000000001




Last Coverpoint : ['rs1 : x8', 'rd : x7', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ff800 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000660]:fsqrt.s t2, fp, dyn
	-[0x80000664]:csrrs a0, fcsr, zero
	-[0x80000668]:sd t2, 384(ra)
	-[0x8000066c]:sd a0, 392(ra)
Current Store : [0x8000066c] : sd a0, 392(ra) -- Store: [0x800039a0]:0x0000000000000001




Last Coverpoint : ['rs1 : x5', 'rd : x6', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007ff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000067c]:fsqrt.s t1, t0, dyn
	-[0x80000680]:csrrs a0, fcsr, zero
	-[0x80000684]:sd t1, 400(ra)
	-[0x80000688]:sd a0, 408(ra)
Current Store : [0x80000688] : sd a0, 408(ra) -- Store: [0x800039b0]:0x0000000000000001




Last Coverpoint : ['rs1 : x6', 'rd : x5', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ffc00 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800006a0]:fsqrt.s t0, t1, dyn
	-[0x800006a4]:csrrs a0, fcsr, zero
	-[0x800006a8]:sd t0, 0(t2)
	-[0x800006ac]:sd a0, 8(t2)
Current Store : [0x800006ac] : sd a0, 8(t2) -- Store: [0x800038f0]:0x0000000000000001




Last Coverpoint : ['rs1 : x3', 'rd : x4', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x0003ff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800006bc]:fsqrt.s tp, gp, dyn
	-[0x800006c0]:csrrs a0, fcsr, zero
	-[0x800006c4]:sd tp, 16(t2)
	-[0x800006c8]:sd a0, 24(t2)
Current Store : [0x800006c8] : sd a0, 24(t2) -- Store: [0x80003900]:0x0000000000000001




Last Coverpoint : ['rs1 : x4', 'rd : x3', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ffe00 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800006d8]:fsqrt.s gp, tp, dyn
	-[0x800006dc]:csrrs a0, fcsr, zero
	-[0x800006e0]:sd gp, 32(t2)
	-[0x800006e4]:sd a0, 40(t2)
Current Store : [0x800006e4] : sd a0, 40(t2) -- Store: [0x80003910]:0x0000000000000001




Last Coverpoint : ['rs1 : x1', 'rd : x2', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x0001ff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800006f4]:fsqrt.s sp, ra, dyn
	-[0x800006f8]:csrrs a0, fcsr, zero
	-[0x800006fc]:sd sp, 48(t2)
	-[0x80000700]:sd a0, 56(t2)
Current Store : [0x80000700] : sd a0, 56(t2) -- Store: [0x80003920]:0x0000000000000001




Last Coverpoint : ['rs1 : x2', 'rd : x1', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fff00 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000710]:fsqrt.s ra, sp, dyn
	-[0x80000714]:csrrs a0, fcsr, zero
	-[0x80000718]:sd ra, 64(t2)
	-[0x8000071c]:sd a0, 72(t2)
Current Store : [0x8000071c] : sd a0, 72(t2) -- Store: [0x80003930]:0x0000000000000001




Last Coverpoint : ['rs1 : x0']
Last Code Sequence : 
	-[0x8000072c]:fsqrt.s t6, zero, dyn
	-[0x80000730]:csrrs a0, fcsr, zero
	-[0x80000734]:sd t6, 80(t2)
	-[0x80000738]:sd a0, 88(t2)
Current Store : [0x80000738] : sd a0, 88(t2) -- Store: [0x80003940]:0x0000000000000000




Last Coverpoint : ['rd : x0', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fff80 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000748]:fsqrt.s zero, t6, dyn
	-[0x8000074c]:csrrs a0, fcsr, zero
	-[0x80000750]:sd zero, 96(t2)
	-[0x80000754]:sd a0, 104(t2)
Current Store : [0x80000754] : sd a0, 104(t2) -- Store: [0x80003950]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x00007f and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000764]:fsqrt.s t6, t5, dyn
	-[0x80000768]:csrrs a0, fcsr, zero
	-[0x8000076c]:sd t6, 112(t2)
	-[0x80000770]:sd a0, 120(t2)
Current Store : [0x80000770] : sd a0, 120(t2) -- Store: [0x80003960]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffc0 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000780]:fsqrt.s t6, t5, dyn
	-[0x80000784]:csrrs a0, fcsr, zero
	-[0x80000788]:sd t6, 128(t2)
	-[0x8000078c]:sd a0, 136(t2)
Current Store : [0x8000078c] : sd a0, 136(t2) -- Store: [0x80003970]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x00003f and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000079c]:fsqrt.s t6, t5, dyn
	-[0x800007a0]:csrrs a0, fcsr, zero
	-[0x800007a4]:sd t6, 144(t2)
	-[0x800007a8]:sd a0, 152(t2)
Current Store : [0x800007a8] : sd a0, 152(t2) -- Store: [0x80003980]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffe0 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800007b8]:fsqrt.s t6, t5, dyn
	-[0x800007bc]:csrrs a0, fcsr, zero
	-[0x800007c0]:sd t6, 160(t2)
	-[0x800007c4]:sd a0, 168(t2)
Current Store : [0x800007c4] : sd a0, 168(t2) -- Store: [0x80003990]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x00001f and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800007d4]:fsqrt.s t6, t5, dyn
	-[0x800007d8]:csrrs a0, fcsr, zero
	-[0x800007dc]:sd t6, 176(t2)
	-[0x800007e0]:sd a0, 184(t2)
Current Store : [0x800007e0] : sd a0, 184(t2) -- Store: [0x800039a0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ffff0 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800007f0]:fsqrt.s t6, t5, dyn
	-[0x800007f4]:csrrs a0, fcsr, zero
	-[0x800007f8]:sd t6, 192(t2)
	-[0x800007fc]:sd a0, 200(t2)
Current Store : [0x800007fc] : sd a0, 200(t2) -- Store: [0x800039b0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x00000f and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000080c]:fsqrt.s t6, t5, dyn
	-[0x80000810]:csrrs a0, fcsr, zero
	-[0x80000814]:sd t6, 208(t2)
	-[0x80000818]:sd a0, 216(t2)
Current Store : [0x80000818] : sd a0, 216(t2) -- Store: [0x800039c0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ffff8 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000828]:fsqrt.s t6, t5, dyn
	-[0x8000082c]:csrrs a0, fcsr, zero
	-[0x80000830]:sd t6, 224(t2)
	-[0x80000834]:sd a0, 232(t2)
Current Store : [0x80000834] : sd a0, 232(t2) -- Store: [0x800039d0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000007 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000844]:fsqrt.s t6, t5, dyn
	-[0x80000848]:csrrs a0, fcsr, zero
	-[0x8000084c]:sd t6, 240(t2)
	-[0x80000850]:sd a0, 248(t2)
Current Store : [0x80000850] : sd a0, 248(t2) -- Store: [0x800039e0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ffffc and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000860]:fsqrt.s t6, t5, dyn
	-[0x80000864]:csrrs a0, fcsr, zero
	-[0x80000868]:sd t6, 256(t2)
	-[0x8000086c]:sd a0, 264(t2)
Current Store : [0x8000086c] : sd a0, 264(t2) -- Store: [0x800039f0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000003 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000087c]:fsqrt.s t6, t5, dyn
	-[0x80000880]:csrrs a0, fcsr, zero
	-[0x80000884]:sd t6, 272(t2)
	-[0x80000888]:sd a0, 280(t2)
Current Store : [0x80000888] : sd a0, 280(t2) -- Store: [0x80003a00]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ffffe and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000898]:fsqrt.s t6, t5, dyn
	-[0x8000089c]:csrrs a0, fcsr, zero
	-[0x800008a0]:sd t6, 288(t2)
	-[0x800008a4]:sd a0, 296(t2)
Current Store : [0x800008a4] : sd a0, 296(t2) -- Store: [0x80003a10]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800008b4]:fsqrt.s t6, t5, dyn
	-[0x800008b8]:csrrs a0, fcsr, zero
	-[0x800008bc]:sd t6, 304(t2)
	-[0x800008c0]:sd a0, 312(t2)
Current Store : [0x800008c0] : sd a0, 312(t2) -- Store: [0x80003a20]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800008d0]:fsqrt.s t6, t5, dyn
	-[0x800008d4]:csrrs a0, fcsr, zero
	-[0x800008d8]:sd t6, 320(t2)
	-[0x800008dc]:sd a0, 328(t2)
Current Store : [0x800008dc] : sd a0, 328(t2) -- Store: [0x80003a30]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800008ec]:fsqrt.s t6, t5, dyn
	-[0x800008f0]:csrrs a0, fcsr, zero
	-[0x800008f4]:sd t6, 336(t2)
	-[0x800008f8]:sd a0, 344(t2)
Current Store : [0x800008f8] : sd a0, 344(t2) -- Store: [0x80003a40]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x400000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000908]:fsqrt.s t6, t5, dyn
	-[0x8000090c]:csrrs a0, fcsr, zero
	-[0x80000910]:sd t6, 352(t2)
	-[0x80000914]:sd a0, 360(t2)
Current Store : [0x80000914] : sd a0, 360(t2) -- Store: [0x80003a50]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x3fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000924]:fsqrt.s t6, t5, dyn
	-[0x80000928]:csrrs a0, fcsr, zero
	-[0x8000092c]:sd t6, 368(t2)
	-[0x80000930]:sd a0, 376(t2)
Current Store : [0x80000930] : sd a0, 376(t2) -- Store: [0x80003a60]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x600000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000940]:fsqrt.s t6, t5, dyn
	-[0x80000944]:csrrs a0, fcsr, zero
	-[0x80000948]:sd t6, 384(t2)
	-[0x8000094c]:sd a0, 392(t2)
Current Store : [0x8000094c] : sd a0, 392(t2) -- Store: [0x80003a70]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x1fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000095c]:fsqrt.s t6, t5, dyn
	-[0x80000960]:csrrs a0, fcsr, zero
	-[0x80000964]:sd t6, 400(t2)
	-[0x80000968]:sd a0, 408(t2)
Current Store : [0x80000968] : sd a0, 408(t2) -- Store: [0x80003a80]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x700000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000978]:fsqrt.s t6, t5, dyn
	-[0x8000097c]:csrrs a0, fcsr, zero
	-[0x80000980]:sd t6, 416(t2)
	-[0x80000984]:sd a0, 424(t2)
Current Store : [0x80000984] : sd a0, 424(t2) -- Store: [0x80003a90]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000994]:fsqrt.s t6, t5, dyn
	-[0x80000998]:csrrs a0, fcsr, zero
	-[0x8000099c]:sd t6, 432(t2)
	-[0x800009a0]:sd a0, 440(t2)
Current Store : [0x800009a0] : sd a0, 440(t2) -- Store: [0x80003aa0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x780000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800009b0]:fsqrt.s t6, t5, dyn
	-[0x800009b4]:csrrs a0, fcsr, zero
	-[0x800009b8]:sd t6, 448(t2)
	-[0x800009bc]:sd a0, 456(t2)
Current Store : [0x800009bc] : sd a0, 456(t2) -- Store: [0x80003ab0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x07ffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800009cc]:fsqrt.s t6, t5, dyn
	-[0x800009d0]:csrrs a0, fcsr, zero
	-[0x800009d4]:sd t6, 464(t2)
	-[0x800009d8]:sd a0, 472(t2)
Current Store : [0x800009d8] : sd a0, 472(t2) -- Store: [0x80003ac0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7c0000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800009e8]:fsqrt.s t6, t5, dyn
	-[0x800009ec]:csrrs a0, fcsr, zero
	-[0x800009f0]:sd t6, 480(t2)
	-[0x800009f4]:sd a0, 488(t2)
Current Store : [0x800009f4] : sd a0, 488(t2) -- Store: [0x80003ad0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x03ffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a04]:fsqrt.s t6, t5, dyn
	-[0x80000a08]:csrrs a0, fcsr, zero
	-[0x80000a0c]:sd t6, 496(t2)
	-[0x80000a10]:sd a0, 504(t2)
Current Store : [0x80000a10] : sd a0, 504(t2) -- Store: [0x80003ae0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7e0000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a20]:fsqrt.s t6, t5, dyn
	-[0x80000a24]:csrrs a0, fcsr, zero
	-[0x80000a28]:sd t6, 512(t2)
	-[0x80000a2c]:sd a0, 520(t2)
Current Store : [0x80000a2c] : sd a0, 520(t2) -- Store: [0x80003af0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x01ffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a3c]:fsqrt.s t6, t5, dyn
	-[0x80000a40]:csrrs a0, fcsr, zero
	-[0x80000a44]:sd t6, 528(t2)
	-[0x80000a48]:sd a0, 536(t2)
Current Store : [0x80000a48] : sd a0, 536(t2) -- Store: [0x80003b00]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7f0000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a58]:fsqrt.s t6, t5, dyn
	-[0x80000a5c]:csrrs a0, fcsr, zero
	-[0x80000a60]:sd t6, 544(t2)
	-[0x80000a64]:sd a0, 552(t2)
Current Store : [0x80000a64] : sd a0, 552(t2) -- Store: [0x80003b10]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x00ffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a74]:fsqrt.s t6, t5, dyn
	-[0x80000a78]:csrrs a0, fcsr, zero
	-[0x80000a7c]:sd t6, 560(t2)
	-[0x80000a80]:sd a0, 568(t2)
Current Store : [0x80000a80] : sd a0, 568(t2) -- Store: [0x80003b20]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7f8000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a90]:fsqrt.s t6, t5, dyn
	-[0x80000a94]:csrrs a0, fcsr, zero
	-[0x80000a98]:sd t6, 576(t2)
	-[0x80000a9c]:sd a0, 584(t2)
Current Store : [0x80000a9c] : sd a0, 584(t2) -- Store: [0x80003b30]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x007fff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000aac]:fsqrt.s t6, t5, dyn
	-[0x80000ab0]:csrrs a0, fcsr, zero
	-[0x80000ab4]:sd t6, 592(t2)
	-[0x80000ab8]:sd a0, 600(t2)
Current Store : [0x80000ab8] : sd a0, 600(t2) -- Store: [0x80003b40]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fc000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000ac8]:fsqrt.s t6, t5, dyn
	-[0x80000acc]:csrrs a0, fcsr, zero
	-[0x80000ad0]:sd t6, 608(t2)
	-[0x80000ad4]:sd a0, 616(t2)
Current Store : [0x80000ad4] : sd a0, 616(t2) -- Store: [0x80003b50]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x003fff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000ae4]:fsqrt.s t6, t5, dyn
	-[0x80000ae8]:csrrs a0, fcsr, zero
	-[0x80000aec]:sd t6, 624(t2)
	-[0x80000af0]:sd a0, 632(t2)
Current Store : [0x80000af0] : sd a0, 632(t2) -- Store: [0x80003b60]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fe000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000b00]:fsqrt.s t6, t5, dyn
	-[0x80000b04]:csrrs a0, fcsr, zero
	-[0x80000b08]:sd t6, 640(t2)
	-[0x80000b0c]:sd a0, 648(t2)
Current Store : [0x80000b0c] : sd a0, 648(t2) -- Store: [0x80003b70]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x001fff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000b1c]:fsqrt.s t6, t5, dyn
	-[0x80000b20]:csrrs a0, fcsr, zero
	-[0x80000b24]:sd t6, 656(t2)
	-[0x80000b28]:sd a0, 664(t2)
Current Store : [0x80000b28] : sd a0, 664(t2) -- Store: [0x80003b80]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ff000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000b38]:fsqrt.s t6, t5, dyn
	-[0x80000b3c]:csrrs a0, fcsr, zero
	-[0x80000b40]:sd t6, 672(t2)
	-[0x80000b44]:sd a0, 680(t2)
Current Store : [0x80000b44] : sd a0, 680(t2) -- Store: [0x80003b90]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000fff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000b54]:fsqrt.s t6, t5, dyn
	-[0x80000b58]:csrrs a0, fcsr, zero
	-[0x80000b5c]:sd t6, 688(t2)
	-[0x80000b60]:sd a0, 696(t2)
Current Store : [0x80000b60] : sd a0, 696(t2) -- Store: [0x80003ba0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ff800 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000b70]:fsqrt.s t6, t5, dyn
	-[0x80000b74]:csrrs a0, fcsr, zero
	-[0x80000b78]:sd t6, 704(t2)
	-[0x80000b7c]:sd a0, 712(t2)
Current Store : [0x80000b7c] : sd a0, 712(t2) -- Store: [0x80003bb0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0007ff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000b8c]:fsqrt.s t6, t5, dyn
	-[0x80000b90]:csrrs a0, fcsr, zero
	-[0x80000b94]:sd t6, 720(t2)
	-[0x80000b98]:sd a0, 728(t2)
Current Store : [0x80000b98] : sd a0, 728(t2) -- Store: [0x80003bc0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ffc00 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000ba8]:fsqrt.s t6, t5, dyn
	-[0x80000bac]:csrrs a0, fcsr, zero
	-[0x80000bb0]:sd t6, 736(t2)
	-[0x80000bb4]:sd a0, 744(t2)
Current Store : [0x80000bb4] : sd a0, 744(t2) -- Store: [0x80003bd0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0003ff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000bc4]:fsqrt.s t6, t5, dyn
	-[0x80000bc8]:csrrs a0, fcsr, zero
	-[0x80000bcc]:sd t6, 752(t2)
	-[0x80000bd0]:sd a0, 760(t2)
Current Store : [0x80000bd0] : sd a0, 760(t2) -- Store: [0x80003be0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ffe00 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000be0]:fsqrt.s t6, t5, dyn
	-[0x80000be4]:csrrs a0, fcsr, zero
	-[0x80000be8]:sd t6, 768(t2)
	-[0x80000bec]:sd a0, 776(t2)
Current Store : [0x80000bec] : sd a0, 776(t2) -- Store: [0x80003bf0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0001ff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000bfc]:fsqrt.s t6, t5, dyn
	-[0x80000c00]:csrrs a0, fcsr, zero
	-[0x80000c04]:sd t6, 784(t2)
	-[0x80000c08]:sd a0, 792(t2)
Current Store : [0x80000c08] : sd a0, 792(t2) -- Store: [0x80003c00]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fff00 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000c18]:fsqrt.s t6, t5, dyn
	-[0x80000c1c]:csrrs a0, fcsr, zero
	-[0x80000c20]:sd t6, 800(t2)
	-[0x80000c24]:sd a0, 808(t2)
Current Store : [0x80000c24] : sd a0, 808(t2) -- Store: [0x80003c10]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0000ff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000c34]:fsqrt.s t6, t5, dyn
	-[0x80000c38]:csrrs a0, fcsr, zero
	-[0x80000c3c]:sd t6, 816(t2)
	-[0x80000c40]:sd a0, 824(t2)
Current Store : [0x80000c40] : sd a0, 824(t2) -- Store: [0x80003c20]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fff80 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000c50]:fsqrt.s t6, t5, dyn
	-[0x80000c54]:csrrs a0, fcsr, zero
	-[0x80000c58]:sd t6, 832(t2)
	-[0x80000c5c]:sd a0, 840(t2)
Current Store : [0x80000c5c] : sd a0, 840(t2) -- Store: [0x80003c30]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x00007f and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000c6c]:fsqrt.s t6, t5, dyn
	-[0x80000c70]:csrrs a0, fcsr, zero
	-[0x80000c74]:sd t6, 848(t2)
	-[0x80000c78]:sd a0, 856(t2)
Current Store : [0x80000c78] : sd a0, 856(t2) -- Store: [0x80003c40]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fffc0 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000c88]:fsqrt.s t6, t5, dyn
	-[0x80000c8c]:csrrs a0, fcsr, zero
	-[0x80000c90]:sd t6, 864(t2)
	-[0x80000c94]:sd a0, 872(t2)
Current Store : [0x80000c94] : sd a0, 872(t2) -- Store: [0x80003c50]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x00003f and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000ca4]:fsqrt.s t6, t5, dyn
	-[0x80000ca8]:csrrs a0, fcsr, zero
	-[0x80000cac]:sd t6, 880(t2)
	-[0x80000cb0]:sd a0, 888(t2)
Current Store : [0x80000cb0] : sd a0, 888(t2) -- Store: [0x80003c60]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fffe0 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000cc0]:fsqrt.s t6, t5, dyn
	-[0x80000cc4]:csrrs a0, fcsr, zero
	-[0x80000cc8]:sd t6, 896(t2)
	-[0x80000ccc]:sd a0, 904(t2)
Current Store : [0x80000ccc] : sd a0, 904(t2) -- Store: [0x80003c70]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x00001f and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000cdc]:fsqrt.s t6, t5, dyn
	-[0x80000ce0]:csrrs a0, fcsr, zero
	-[0x80000ce4]:sd t6, 912(t2)
	-[0x80000ce8]:sd a0, 920(t2)
Current Store : [0x80000ce8] : sd a0, 920(t2) -- Store: [0x80003c80]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ffff0 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000cf8]:fsqrt.s t6, t5, dyn
	-[0x80000cfc]:csrrs a0, fcsr, zero
	-[0x80000d00]:sd t6, 928(t2)
	-[0x80000d04]:sd a0, 936(t2)
Current Store : [0x80000d04] : sd a0, 936(t2) -- Store: [0x80003c90]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x00000f and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d14]:fsqrt.s t6, t5, dyn
	-[0x80000d18]:csrrs a0, fcsr, zero
	-[0x80000d1c]:sd t6, 944(t2)
	-[0x80000d20]:sd a0, 952(t2)
Current Store : [0x80000d20] : sd a0, 952(t2) -- Store: [0x80003ca0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ffff8 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d30]:fsqrt.s t6, t5, dyn
	-[0x80000d34]:csrrs a0, fcsr, zero
	-[0x80000d38]:sd t6, 960(t2)
	-[0x80000d3c]:sd a0, 968(t2)
Current Store : [0x80000d3c] : sd a0, 968(t2) -- Store: [0x80003cb0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000007 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d4c]:fsqrt.s t6, t5, dyn
	-[0x80000d50]:csrrs a0, fcsr, zero
	-[0x80000d54]:sd t6, 976(t2)
	-[0x80000d58]:sd a0, 984(t2)
Current Store : [0x80000d58] : sd a0, 984(t2) -- Store: [0x80003cc0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ffffc and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d68]:fsqrt.s t6, t5, dyn
	-[0x80000d6c]:csrrs a0, fcsr, zero
	-[0x80000d70]:sd t6, 992(t2)
	-[0x80000d74]:sd a0, 1000(t2)
Current Store : [0x80000d74] : sd a0, 1000(t2) -- Store: [0x80003cd0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000003 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d84]:fsqrt.s t6, t5, dyn
	-[0x80000d88]:csrrs a0, fcsr, zero
	-[0x80000d8c]:sd t6, 1008(t2)
	-[0x80000d90]:sd a0, 1016(t2)
Current Store : [0x80000d90] : sd a0, 1016(t2) -- Store: [0x80003ce0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ffffe and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000da0]:fsqrt.s t6, t5, dyn
	-[0x80000da4]:csrrs a0, fcsr, zero
	-[0x80000da8]:sd t6, 1024(t2)
	-[0x80000dac]:sd a0, 1032(t2)
Current Store : [0x80000dac] : sd a0, 1032(t2) -- Store: [0x80003cf0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000001 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000dbc]:fsqrt.s t6, t5, dyn
	-[0x80000dc0]:csrrs a0, fcsr, zero
	-[0x80000dc4]:sd t6, 1040(t2)
	-[0x80000dc8]:sd a0, 1048(t2)
Current Store : [0x80000dc8] : sd a0, 1048(t2) -- Store: [0x80003d00]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000dd8]:fsqrt.s t6, t5, dyn
	-[0x80000ddc]:csrrs a0, fcsr, zero
	-[0x80000de0]:sd t6, 1056(t2)
	-[0x80000de4]:sd a0, 1064(t2)
Current Store : [0x80000de4] : sd a0, 1064(t2) -- Store: [0x80003d10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000df4]:fsqrt.s t6, t5, dyn
	-[0x80000df8]:csrrs a0, fcsr, zero
	-[0x80000dfc]:sd t6, 1072(t2)
	-[0x80000e00]:sd a0, 1080(t2)
Current Store : [0x80000e00] : sd a0, 1080(t2) -- Store: [0x80003d20]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x400000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000e10]:fsqrt.s t6, t5, dyn
	-[0x80000e14]:csrrs a0, fcsr, zero
	-[0x80000e18]:sd t6, 1088(t2)
	-[0x80000e1c]:sd a0, 1096(t2)
Current Store : [0x80000e1c] : sd a0, 1096(t2) -- Store: [0x80003d30]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x3fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000e2c]:fsqrt.s t6, t5, dyn
	-[0x80000e30]:csrrs a0, fcsr, zero
	-[0x80000e34]:sd t6, 1104(t2)
	-[0x80000e38]:sd a0, 1112(t2)
Current Store : [0x80000e38] : sd a0, 1112(t2) -- Store: [0x80003d40]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x600000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000e48]:fsqrt.s t6, t5, dyn
	-[0x80000e4c]:csrrs a0, fcsr, zero
	-[0x80000e50]:sd t6, 1120(t2)
	-[0x80000e54]:sd a0, 1128(t2)
Current Store : [0x80000e54] : sd a0, 1128(t2) -- Store: [0x80003d50]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x1fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000e64]:fsqrt.s t6, t5, dyn
	-[0x80000e68]:csrrs a0, fcsr, zero
	-[0x80000e6c]:sd t6, 1136(t2)
	-[0x80000e70]:sd a0, 1144(t2)
Current Store : [0x80000e70] : sd a0, 1144(t2) -- Store: [0x80003d60]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x700000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000e80]:fsqrt.s t6, t5, dyn
	-[0x80000e84]:csrrs a0, fcsr, zero
	-[0x80000e88]:sd t6, 1152(t2)
	-[0x80000e8c]:sd a0, 1160(t2)
Current Store : [0x80000e8c] : sd a0, 1160(t2) -- Store: [0x80003d70]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000e9c]:fsqrt.s t6, t5, dyn
	-[0x80000ea0]:csrrs a0, fcsr, zero
	-[0x80000ea4]:sd t6, 1168(t2)
	-[0x80000ea8]:sd a0, 1176(t2)
Current Store : [0x80000ea8] : sd a0, 1176(t2) -- Store: [0x80003d80]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x780000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000eb8]:fsqrt.s t6, t5, dyn
	-[0x80000ebc]:csrrs a0, fcsr, zero
	-[0x80000ec0]:sd t6, 1184(t2)
	-[0x80000ec4]:sd a0, 1192(t2)
Current Store : [0x80000ec4] : sd a0, 1192(t2) -- Store: [0x80003d90]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x07ffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000ed4]:fsqrt.s t6, t5, dyn
	-[0x80000ed8]:csrrs a0, fcsr, zero
	-[0x80000edc]:sd t6, 1200(t2)
	-[0x80000ee0]:sd a0, 1208(t2)
Current Store : [0x80000ee0] : sd a0, 1208(t2) -- Store: [0x80003da0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x7c0000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000ef0]:fsqrt.s t6, t5, dyn
	-[0x80000ef4]:csrrs a0, fcsr, zero
	-[0x80000ef8]:sd t6, 1216(t2)
	-[0x80000efc]:sd a0, 1224(t2)
Current Store : [0x80000efc] : sd a0, 1224(t2) -- Store: [0x80003db0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x03ffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000f0c]:fsqrt.s t6, t5, dyn
	-[0x80000f10]:csrrs a0, fcsr, zero
	-[0x80000f14]:sd t6, 1232(t2)
	-[0x80000f18]:sd a0, 1240(t2)
Current Store : [0x80000f18] : sd a0, 1240(t2) -- Store: [0x80003dc0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x7e0000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000f28]:fsqrt.s t6, t5, dyn
	-[0x80000f2c]:csrrs a0, fcsr, zero
	-[0x80000f30]:sd t6, 1248(t2)
	-[0x80000f34]:sd a0, 1256(t2)
Current Store : [0x80000f34] : sd a0, 1256(t2) -- Store: [0x80003dd0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x01ffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000f44]:fsqrt.s t6, t5, dyn
	-[0x80000f48]:csrrs a0, fcsr, zero
	-[0x80000f4c]:sd t6, 1264(t2)
	-[0x80000f50]:sd a0, 1272(t2)
Current Store : [0x80000f50] : sd a0, 1272(t2) -- Store: [0x80003de0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x7f0000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000f60]:fsqrt.s t6, t5, dyn
	-[0x80000f64]:csrrs a0, fcsr, zero
	-[0x80000f68]:sd t6, 1280(t2)
	-[0x80000f6c]:sd a0, 1288(t2)
Current Store : [0x80000f6c] : sd a0, 1288(t2) -- Store: [0x80003df0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x00ffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000f7c]:fsqrt.s t6, t5, dyn
	-[0x80000f80]:csrrs a0, fcsr, zero
	-[0x80000f84]:sd t6, 1296(t2)
	-[0x80000f88]:sd a0, 1304(t2)
Current Store : [0x80000f88] : sd a0, 1304(t2) -- Store: [0x80003e00]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x7f8000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000f98]:fsqrt.s t6, t5, dyn
	-[0x80000f9c]:csrrs a0, fcsr, zero
	-[0x80000fa0]:sd t6, 1312(t2)
	-[0x80000fa4]:sd a0, 1320(t2)
Current Store : [0x80000fa4] : sd a0, 1320(t2) -- Store: [0x80003e10]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x007fff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000fb4]:fsqrt.s t6, t5, dyn
	-[0x80000fb8]:csrrs a0, fcsr, zero
	-[0x80000fbc]:sd t6, 1328(t2)
	-[0x80000fc0]:sd a0, 1336(t2)
Current Store : [0x80000fc0] : sd a0, 1336(t2) -- Store: [0x80003e20]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x7fc000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000fd0]:fsqrt.s t6, t5, dyn
	-[0x80000fd4]:csrrs a0, fcsr, zero
	-[0x80000fd8]:sd t6, 1344(t2)
	-[0x80000fdc]:sd a0, 1352(t2)
Current Store : [0x80000fdc] : sd a0, 1352(t2) -- Store: [0x80003e30]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x003fff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000fec]:fsqrt.s t6, t5, dyn
	-[0x80000ff0]:csrrs a0, fcsr, zero
	-[0x80000ff4]:sd t6, 1360(t2)
	-[0x80000ff8]:sd a0, 1368(t2)
Current Store : [0x80000ff8] : sd a0, 1368(t2) -- Store: [0x80003e40]:0x0000000000000001





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|signature|coverpoints|code|
|----|---------|-----------|----|
