
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000760')]      |
| SIG_REGION                | [('0x80002310', '0x80002430', '36 dwords')]      |
| COV_LABELS                | fsqrt_b3      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV64Zfinx-rvopcodesdecoder/work-fsqrt/fsqrt_b3-01.S/ref.S    |
| Total Number of coverpoints| 82     |
| Total Coverpoints Hit     | 82      |
| Total Signature Updates   | 46      |
| STAT1                     | 0      |
| STAT2                     | 0      |
| STAT3                     | 33     |
| STAT4                     | 22     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x800003b8]:fsqrt.s t6, t6, dyn
[0x800003bc]:csrrs tp, fcsr, zero
[0x800003c0]:sd t6, 0(ra)
[0x800003c4]:sd tp, 8(ra)
[0x800003c8]:ld t4, 8(gp)
[0x800003cc]:addi sp, zero, 32
[0x800003d0]:csrrw zero, fcsr, sp
[0x800003d4]:fsqrt.s t5, t4, dyn

[0x800003d4]:fsqrt.s t5, t4, dyn
[0x800003d8]:csrrs tp, fcsr, zero
[0x800003dc]:sd t5, 16(ra)
[0x800003e0]:sd tp, 24(ra)
[0x800003e4]:ld t5, 16(gp)
[0x800003e8]:addi sp, zero, 64
[0x800003ec]:csrrw zero, fcsr, sp
[0x800003f0]:fsqrt.s t4, t5, dyn

[0x800003f0]:fsqrt.s t4, t5, dyn
[0x800003f4]:csrrs tp, fcsr, zero
[0x800003f8]:sd t4, 32(ra)
[0x800003fc]:sd tp, 40(ra)
[0x80000400]:ld s11, 24(gp)
[0x80000404]:addi sp, zero, 96
[0x80000408]:csrrw zero, fcsr, sp
[0x8000040c]:fsqrt.s t3, s11, dyn

[0x8000040c]:fsqrt.s t3, s11, dyn
[0x80000410]:csrrs tp, fcsr, zero
[0x80000414]:sd t3, 48(ra)
[0x80000418]:sd tp, 56(ra)
[0x8000041c]:ld t3, 32(gp)
[0x80000420]:addi sp, zero, 128
[0x80000424]:csrrw zero, fcsr, sp
[0x80000428]:fsqrt.s s11, t3, dyn

[0x80000428]:fsqrt.s s11, t3, dyn
[0x8000042c]:csrrs tp, fcsr, zero
[0x80000430]:sd s11, 64(ra)
[0x80000434]:sd tp, 72(ra)
[0x80000438]:ld s9, 40(gp)
[0x8000043c]:addi sp, zero, 0
[0x80000440]:csrrw zero, fcsr, sp
[0x80000444]:fsqrt.s s10, s9, dyn

[0x80000444]:fsqrt.s s10, s9, dyn
[0x80000448]:csrrs tp, fcsr, zero
[0x8000044c]:sd s10, 80(ra)
[0x80000450]:sd tp, 88(ra)
[0x80000454]:ld s10, 48(gp)
[0x80000458]:addi sp, zero, 32
[0x8000045c]:csrrw zero, fcsr, sp
[0x80000460]:fsqrt.s s9, s10, dyn

[0x80000460]:fsqrt.s s9, s10, dyn
[0x80000464]:csrrs tp, fcsr, zero
[0x80000468]:sd s9, 96(ra)
[0x8000046c]:sd tp, 104(ra)
[0x80000470]:ld s7, 56(gp)
[0x80000474]:addi sp, zero, 64
[0x80000478]:csrrw zero, fcsr, sp
[0x8000047c]:fsqrt.s s8, s7, dyn

[0x8000047c]:fsqrt.s s8, s7, dyn
[0x80000480]:csrrs tp, fcsr, zero
[0x80000484]:sd s8, 112(ra)
[0x80000488]:sd tp, 120(ra)
[0x8000048c]:ld s8, 64(gp)
[0x80000490]:addi sp, zero, 96
[0x80000494]:csrrw zero, fcsr, sp
[0x80000498]:fsqrt.s s7, s8, dyn

[0x80000498]:fsqrt.s s7, s8, dyn
[0x8000049c]:csrrs tp, fcsr, zero
[0x800004a0]:sd s7, 128(ra)
[0x800004a4]:sd tp, 136(ra)
[0x800004a8]:ld s5, 72(gp)
[0x800004ac]:addi sp, zero, 128
[0x800004b0]:csrrw zero, fcsr, sp
[0x800004b4]:fsqrt.s s6, s5, dyn

[0x800004b4]:fsqrt.s s6, s5, dyn
[0x800004b8]:csrrs tp, fcsr, zero
[0x800004bc]:sd s6, 144(ra)
[0x800004c0]:sd tp, 152(ra)
[0x800004c4]:ld s6, 80(gp)
[0x800004c8]:addi sp, zero, 0
[0x800004cc]:csrrw zero, fcsr, sp
[0x800004d0]:fsqrt.s s5, s6, dyn

[0x800004d0]:fsqrt.s s5, s6, dyn
[0x800004d4]:csrrs tp, fcsr, zero
[0x800004d8]:sd s5, 160(ra)
[0x800004dc]:sd tp, 168(ra)
[0x800004e0]:ld s3, 88(gp)
[0x800004e4]:addi sp, zero, 32
[0x800004e8]:csrrw zero, fcsr, sp
[0x800004ec]:fsqrt.s s4, s3, dyn

[0x800004ec]:fsqrt.s s4, s3, dyn
[0x800004f0]:csrrs tp, fcsr, zero
[0x800004f4]:sd s4, 176(ra)
[0x800004f8]:sd tp, 184(ra)
[0x800004fc]:ld s4, 96(gp)
[0x80000500]:addi sp, zero, 64
[0x80000504]:csrrw zero, fcsr, sp
[0x80000508]:fsqrt.s s3, s4, dyn

[0x80000508]:fsqrt.s s3, s4, dyn
[0x8000050c]:csrrs tp, fcsr, zero
[0x80000510]:sd s3, 192(ra)
[0x80000514]:sd tp, 200(ra)
[0x80000518]:ld a7, 104(gp)
[0x8000051c]:addi sp, zero, 96
[0x80000520]:csrrw zero, fcsr, sp
[0x80000524]:fsqrt.s s2, a7, dyn

[0x80000524]:fsqrt.s s2, a7, dyn
[0x80000528]:csrrs tp, fcsr, zero
[0x8000052c]:sd s2, 208(ra)
[0x80000530]:sd tp, 216(ra)
[0x80000534]:ld s2, 112(gp)
[0x80000538]:addi sp, zero, 128
[0x8000053c]:csrrw zero, fcsr, sp
[0x80000540]:fsqrt.s a7, s2, dyn

[0x80000540]:fsqrt.s a7, s2, dyn
[0x80000544]:csrrs tp, fcsr, zero
[0x80000548]:sd a7, 224(ra)
[0x8000054c]:sd tp, 232(ra)
[0x80000550]:ld a5, 120(gp)
[0x80000554]:addi sp, zero, 0
[0x80000558]:csrrw zero, fcsr, sp
[0x8000055c]:fsqrt.s a6, a5, dyn

[0x8000055c]:fsqrt.s a6, a5, dyn
[0x80000560]:csrrs tp, fcsr, zero
[0x80000564]:sd a6, 240(ra)
[0x80000568]:sd tp, 248(ra)
[0x8000056c]:ld a6, 128(gp)
[0x80000570]:addi sp, zero, 0
[0x80000574]:csrrw zero, fcsr, sp
[0x80000578]:fsqrt.s a5, a6, dyn

[0x80000578]:fsqrt.s a5, a6, dyn
[0x8000057c]:csrrs tp, fcsr, zero
[0x80000580]:sd a5, 256(ra)
[0x80000584]:sd tp, 264(ra)
[0x80000588]:ld a3, 136(gp)
[0x8000058c]:addi sp, zero, 0
[0x80000590]:csrrw zero, fcsr, sp
[0x80000594]:fsqrt.s a4, a3, dyn

[0x80000594]:fsqrt.s a4, a3, dyn
[0x80000598]:csrrs tp, fcsr, zero
[0x8000059c]:sd a4, 272(ra)
[0x800005a0]:sd tp, 280(ra)
[0x800005a4]:ld a4, 144(gp)
[0x800005a8]:addi sp, zero, 0
[0x800005ac]:csrrw zero, fcsr, sp
[0x800005b0]:fsqrt.s a3, a4, dyn

[0x80000604]:fsqrt.s a0, s1, dyn
[0x80000608]:csrrs tp, fcsr, zero
[0x8000060c]:sd a0, 336(ra)
[0x80000610]:sd tp, 344(ra)
[0x80000614]:ld a0, 176(gp)
[0x80000618]:addi sp, zero, 0
[0x8000061c]:csrrw zero, fcsr, sp
[0x80000620]:fsqrt.s s1, a0, dyn
[0x80000624]:csrrs tp, fcsr, zero
[0x80000628]:sd s1, 352(ra)
[0x8000062c]:sd tp, 360(ra)
[0x80000630]:ld t2, 184(gp)
[0x80000634]:addi sp, zero, 0
[0x80000638]:csrrw zero, fcsr, sp
[0x8000063c]:fsqrt.s fp, t2, dyn
[0x80000640]:csrrs tp, fcsr, zero
[0x80000644]:sd fp, 368(ra)
[0x80000648]:sd tp, 376(ra)
[0x8000064c]:auipc s1, 2
[0x80000650]:addi s1, s1, 2692
[0x80000654]:ld fp, 0(s1)
[0x80000658]:addi sp, zero, 0
[0x8000065c]:csrrw zero, fcsr, sp
[0x80000660]:fsqrt.s t2, fp, dyn
[0x80000664]:csrrs a0, fcsr, zero
[0x80000668]:sd t2, 384(ra)
[0x8000066c]:sd a0, 392(ra)
[0x80000670]:ld t0, 8(s1)
[0x80000674]:addi sp, zero, 0
[0x80000678]:csrrw zero, fcsr, sp
[0x8000067c]:fsqrt.s t1, t0, dyn
[0x80000680]:csrrs a0, fcsr, zero
[0x80000684]:sd t1, 400(ra)
[0x80000688]:sd a0, 408(ra)
[0x8000068c]:auipc t2, 2
[0x80000690]:addi t2, t2, 3420
[0x80000694]:ld t1, 16(s1)
[0x80000698]:addi fp, zero, 0
[0x8000069c]:csrrw zero, fcsr, fp
[0x800006a0]:fsqrt.s t0, t1, dyn
[0x800006a4]:csrrs a0, fcsr, zero
[0x800006a8]:sd t0, 0(t2)
[0x800006ac]:sd a0, 8(t2)

[0x800006a0]:fsqrt.s t0, t1, dyn
[0x800006a4]:csrrs a0, fcsr, zero
[0x800006a8]:sd t0, 0(t2)
[0x800006ac]:sd a0, 8(t2)
[0x800006b0]:ld gp, 24(s1)
[0x800006b4]:addi fp, zero, 0
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fsqrt.s tp, gp, dyn

[0x800006bc]:fsqrt.s tp, gp, dyn
[0x800006c0]:csrrs a0, fcsr, zero
[0x800006c4]:sd tp, 16(t2)
[0x800006c8]:sd a0, 24(t2)
[0x800006cc]:ld tp, 32(s1)
[0x800006d0]:addi fp, zero, 0
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fsqrt.s gp, tp, dyn

[0x800006d8]:fsqrt.s gp, tp, dyn
[0x800006dc]:csrrs a0, fcsr, zero
[0x800006e0]:sd gp, 32(t2)
[0x800006e4]:sd a0, 40(t2)
[0x800006e8]:ld ra, 40(s1)
[0x800006ec]:addi fp, zero, 0
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fsqrt.s sp, ra, dyn

[0x800006f4]:fsqrt.s sp, ra, dyn
[0x800006f8]:csrrs a0, fcsr, zero
[0x800006fc]:sd sp, 48(t2)
[0x80000700]:sd a0, 56(t2)
[0x80000704]:ld sp, 48(s1)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fsqrt.s ra, sp, dyn

[0x80000710]:fsqrt.s ra, sp, dyn
[0x80000714]:csrrs a0, fcsr, zero
[0x80000718]:sd ra, 64(t2)
[0x8000071c]:sd a0, 72(t2)
[0x80000720]:ld zero, 56(s1)
[0x80000724]:addi fp, zero, 0
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fsqrt.s t6, zero, dyn

[0x800005b0]:fsqrt.s a3, a4, dyn
[0x800005b4]:csrrs tp, fcsr, zero
[0x800005b8]:sd a3, 288(ra)
[0x800005bc]:sd tp, 296(ra)
[0x800005c0]:ld a1, 152(gp)
[0x800005c4]:addi sp, zero, 0
[0x800005c8]:csrrw zero, fcsr, sp
[0x800005cc]:fsqrt.s a2, a1, dyn
[0x800005d0]:csrrs tp, fcsr, zero
[0x800005d4]:sd a2, 304(ra)
[0x800005d8]:sd tp, 312(ra)
[0x800005dc]:ld a2, 160(gp)
[0x800005e0]:addi sp, zero, 0
[0x800005e4]:csrrw zero, fcsr, sp
[0x800005e8]:fsqrt.s a1, a2, dyn
[0x800005ec]:csrrs tp, fcsr, zero
[0x800005f0]:sd a1, 320(ra)
[0x800005f4]:sd tp, 328(ra)
[0x800005f8]:ld s1, 168(gp)
[0x800005fc]:addi sp, zero, 0
[0x80000600]:csrrw zero, fcsr, sp
[0x80000604]:fsqrt.s a0, s1, dyn
[0x80000608]:csrrs tp, fcsr, zero
[0x8000060c]:sd a0, 336(ra)
[0x80000610]:sd tp, 344(ra)
[0x80000614]:ld a0, 176(gp)
[0x80000618]:addi sp, zero, 0
[0x8000061c]:csrrw zero, fcsr, sp
[0x80000620]:fsqrt.s s1, a0, dyn
[0x80000624]:csrrs tp, fcsr, zero
[0x80000628]:sd s1, 352(ra)
[0x8000062c]:sd tp, 360(ra)
[0x80000630]:ld t2, 184(gp)
[0x80000634]:addi sp, zero, 0
[0x80000638]:csrrw zero, fcsr, sp
[0x8000063c]:fsqrt.s fp, t2, dyn
[0x80000640]:csrrs tp, fcsr, zero
[0x80000644]:sd fp, 368(ra)
[0x80000648]:sd tp, 376(ra)
[0x8000064c]:auipc s1, 2
[0x80000650]:addi s1, s1, 2692
[0x80000654]:ld fp, 0(s1)
[0x80000658]:addi sp, zero, 0
[0x8000065c]:csrrw zero, fcsr, sp
[0x80000660]:fsqrt.s t2, fp, dyn
[0x80000664]:csrrs a0, fcsr, zero
[0x80000668]:sd t2, 384(ra)
[0x8000066c]:sd a0, 392(ra)
[0x80000670]:ld t0, 8(s1)
[0x80000674]:addi sp, zero, 0
[0x80000678]:csrrw zero, fcsr, sp
[0x8000067c]:fsqrt.s t1, t0, dyn
[0x80000680]:csrrs a0, fcsr, zero
[0x80000684]:sd t1, 400(ra)
[0x80000688]:sd a0, 408(ra)
[0x8000068c]:auipc t2, 2
[0x80000690]:addi t2, t2, 3420
[0x80000694]:ld t1, 16(s1)
[0x80000698]:addi fp, zero, 0
[0x8000069c]:csrrw zero, fcsr, fp
[0x800006a0]:fsqrt.s t0, t1, dyn
[0x800006a4]:csrrs a0, fcsr, zero
[0x800006a8]:sd t0, 0(t2)
[0x800006ac]:sd a0, 8(t2)
[0x800006b0]:ld gp, 24(s1)
[0x800006b4]:addi fp, zero, 0
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fsqrt.s tp, gp, dyn
[0x800006c0]:csrrs a0, fcsr, zero
[0x800006c4]:sd tp, 16(t2)
[0x800006c8]:sd a0, 24(t2)
[0x800006cc]:ld tp, 32(s1)
[0x800006d0]:addi fp, zero, 0
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fsqrt.s gp, tp, dyn
[0x800006dc]:csrrs a0, fcsr, zero
[0x800006e0]:sd gp, 32(t2)
[0x800006e4]:sd a0, 40(t2)
[0x800006e8]:ld ra, 40(s1)
[0x800006ec]:addi fp, zero, 0
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fsqrt.s sp, ra, dyn
[0x800006f8]:csrrs a0, fcsr, zero
[0x800006fc]:sd sp, 48(t2)
[0x80000700]:sd a0, 56(t2)
[0x80000704]:ld sp, 48(s1)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fsqrt.s ra, sp, dyn
[0x80000714]:csrrs a0, fcsr, zero
[0x80000718]:sd ra, 64(t2)
[0x8000071c]:sd a0, 72(t2)
[0x80000720]:ld zero, 56(s1)
[0x80000724]:addi fp, zero, 0
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fsqrt.s t6, zero, dyn
[0x80000730]:csrrs a0, fcsr, zero
[0x80000734]:sd t6, 80(t2)
[0x80000738]:sd a0, 88(t2)
[0x8000073c]:ld t6, 64(s1)
[0x80000740]:addi fp, zero, 0
[0x80000744]:csrrw zero, fcsr, fp
[0x80000748]:fsqrt.s zero, t6, dyn
[0x8000074c]:csrrs a0, fcsr, zero
[0x80000750]:sd zero, 96(t2)
[0x80000754]:sd a0, 104(t2)
[0x80000758]:addi zero, zero, 0
[0x8000075c]:addi zero, zero, 0

[0x800005cc]:fsqrt.s a2, a1, dyn
[0x800005d0]:csrrs tp, fcsr, zero
[0x800005d4]:sd a2, 304(ra)
[0x800005d8]:sd tp, 312(ra)
[0x800005dc]:ld a2, 160(gp)
[0x800005e0]:addi sp, zero, 0
[0x800005e4]:csrrw zero, fcsr, sp
[0x800005e8]:fsqrt.s a1, a2, dyn
[0x800005ec]:csrrs tp, fcsr, zero
[0x800005f0]:sd a1, 320(ra)
[0x800005f4]:sd tp, 328(ra)
[0x800005f8]:ld s1, 168(gp)
[0x800005fc]:addi sp, zero, 0
[0x80000600]:csrrw zero, fcsr, sp
[0x80000604]:fsqrt.s a0, s1, dyn
[0x80000608]:csrrs tp, fcsr, zero
[0x8000060c]:sd a0, 336(ra)
[0x80000610]:sd tp, 344(ra)
[0x80000614]:ld a0, 176(gp)
[0x80000618]:addi sp, zero, 0
[0x8000061c]:csrrw zero, fcsr, sp
[0x80000620]:fsqrt.s s1, a0, dyn
[0x80000624]:csrrs tp, fcsr, zero
[0x80000628]:sd s1, 352(ra)
[0x8000062c]:sd tp, 360(ra)
[0x80000630]:ld t2, 184(gp)
[0x80000634]:addi sp, zero, 0
[0x80000638]:csrrw zero, fcsr, sp
[0x8000063c]:fsqrt.s fp, t2, dyn
[0x80000640]:csrrs tp, fcsr, zero
[0x80000644]:sd fp, 368(ra)
[0x80000648]:sd tp, 376(ra)
[0x8000064c]:auipc s1, 2
[0x80000650]:addi s1, s1, 2692
[0x80000654]:ld fp, 0(s1)
[0x80000658]:addi sp, zero, 0
[0x8000065c]:csrrw zero, fcsr, sp
[0x80000660]:fsqrt.s t2, fp, dyn
[0x80000664]:csrrs a0, fcsr, zero
[0x80000668]:sd t2, 384(ra)
[0x8000066c]:sd a0, 392(ra)
[0x80000670]:ld t0, 8(s1)
[0x80000674]:addi sp, zero, 0
[0x80000678]:csrrw zero, fcsr, sp
[0x8000067c]:fsqrt.s t1, t0, dyn
[0x80000680]:csrrs a0, fcsr, zero
[0x80000684]:sd t1, 400(ra)
[0x80000688]:sd a0, 408(ra)
[0x8000068c]:auipc t2, 2
[0x80000690]:addi t2, t2, 3420
[0x80000694]:ld t1, 16(s1)
[0x80000698]:addi fp, zero, 0
[0x8000069c]:csrrw zero, fcsr, fp
[0x800006a0]:fsqrt.s t0, t1, dyn
[0x800006a4]:csrrs a0, fcsr, zero
[0x800006a8]:sd t0, 0(t2)
[0x800006ac]:sd a0, 8(t2)
[0x800006b0]:ld gp, 24(s1)
[0x800006b4]:addi fp, zero, 0
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fsqrt.s tp, gp, dyn
[0x800006c0]:csrrs a0, fcsr, zero
[0x800006c4]:sd tp, 16(t2)
[0x800006c8]:sd a0, 24(t2)
[0x800006cc]:ld tp, 32(s1)
[0x800006d0]:addi fp, zero, 0
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fsqrt.s gp, tp, dyn
[0x800006dc]:csrrs a0, fcsr, zero
[0x800006e0]:sd gp, 32(t2)
[0x800006e4]:sd a0, 40(t2)
[0x800006e8]:ld ra, 40(s1)
[0x800006ec]:addi fp, zero, 0
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fsqrt.s sp, ra, dyn
[0x800006f8]:csrrs a0, fcsr, zero
[0x800006fc]:sd sp, 48(t2)
[0x80000700]:sd a0, 56(t2)
[0x80000704]:ld sp, 48(s1)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fsqrt.s ra, sp, dyn
[0x80000714]:csrrs a0, fcsr, zero
[0x80000718]:sd ra, 64(t2)
[0x8000071c]:sd a0, 72(t2)
[0x80000720]:ld zero, 56(s1)
[0x80000724]:addi fp, zero, 0
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fsqrt.s t6, zero, dyn
[0x80000730]:csrrs a0, fcsr, zero
[0x80000734]:sd t6, 80(t2)
[0x80000738]:sd a0, 88(t2)
[0x8000073c]:ld t6, 64(s1)
[0x80000740]:addi fp, zero, 0
[0x80000744]:csrrw zero, fcsr, fp
[0x80000748]:fsqrt.s zero, t6, dyn
[0x8000074c]:csrrs a0, fcsr, zero
[0x80000750]:sd zero, 96(t2)
[0x80000754]:sd a0, 104(t2)
[0x80000758]:addi zero, zero, 0
[0x8000075c]:addi zero, zero, 0

[0x800005e8]:fsqrt.s a1, a2, dyn
[0x800005ec]:csrrs tp, fcsr, zero
[0x800005f0]:sd a1, 320(ra)
[0x800005f4]:sd tp, 328(ra)
[0x800005f8]:ld s1, 168(gp)
[0x800005fc]:addi sp, zero, 0
[0x80000600]:csrrw zero, fcsr, sp
[0x80000604]:fsqrt.s a0, s1, dyn
[0x80000608]:csrrs tp, fcsr, zero
[0x8000060c]:sd a0, 336(ra)
[0x80000610]:sd tp, 344(ra)
[0x80000614]:ld a0, 176(gp)
[0x80000618]:addi sp, zero, 0
[0x8000061c]:csrrw zero, fcsr, sp
[0x80000620]:fsqrt.s s1, a0, dyn
[0x80000624]:csrrs tp, fcsr, zero
[0x80000628]:sd s1, 352(ra)
[0x8000062c]:sd tp, 360(ra)
[0x80000630]:ld t2, 184(gp)
[0x80000634]:addi sp, zero, 0
[0x80000638]:csrrw zero, fcsr, sp
[0x8000063c]:fsqrt.s fp, t2, dyn
[0x80000640]:csrrs tp, fcsr, zero
[0x80000644]:sd fp, 368(ra)
[0x80000648]:sd tp, 376(ra)
[0x8000064c]:auipc s1, 2
[0x80000650]:addi s1, s1, 2692
[0x80000654]:ld fp, 0(s1)
[0x80000658]:addi sp, zero, 0
[0x8000065c]:csrrw zero, fcsr, sp
[0x80000660]:fsqrt.s t2, fp, dyn
[0x80000664]:csrrs a0, fcsr, zero
[0x80000668]:sd t2, 384(ra)
[0x8000066c]:sd a0, 392(ra)
[0x80000670]:ld t0, 8(s1)
[0x80000674]:addi sp, zero, 0
[0x80000678]:csrrw zero, fcsr, sp
[0x8000067c]:fsqrt.s t1, t0, dyn
[0x80000680]:csrrs a0, fcsr, zero
[0x80000684]:sd t1, 400(ra)
[0x80000688]:sd a0, 408(ra)
[0x8000068c]:auipc t2, 2
[0x80000690]:addi t2, t2, 3420
[0x80000694]:ld t1, 16(s1)
[0x80000698]:addi fp, zero, 0
[0x8000069c]:csrrw zero, fcsr, fp
[0x800006a0]:fsqrt.s t0, t1, dyn
[0x800006a4]:csrrs a0, fcsr, zero
[0x800006a8]:sd t0, 0(t2)
[0x800006ac]:sd a0, 8(t2)
[0x800006b0]:ld gp, 24(s1)
[0x800006b4]:addi fp, zero, 0
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fsqrt.s tp, gp, dyn
[0x800006c0]:csrrs a0, fcsr, zero
[0x800006c4]:sd tp, 16(t2)
[0x800006c8]:sd a0, 24(t2)
[0x800006cc]:ld tp, 32(s1)
[0x800006d0]:addi fp, zero, 0
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fsqrt.s gp, tp, dyn
[0x800006dc]:csrrs a0, fcsr, zero
[0x800006e0]:sd gp, 32(t2)
[0x800006e4]:sd a0, 40(t2)
[0x800006e8]:ld ra, 40(s1)
[0x800006ec]:addi fp, zero, 0
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fsqrt.s sp, ra, dyn
[0x800006f8]:csrrs a0, fcsr, zero
[0x800006fc]:sd sp, 48(t2)
[0x80000700]:sd a0, 56(t2)
[0x80000704]:ld sp, 48(s1)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fsqrt.s ra, sp, dyn
[0x80000714]:csrrs a0, fcsr, zero
[0x80000718]:sd ra, 64(t2)
[0x8000071c]:sd a0, 72(t2)
[0x80000720]:ld zero, 56(s1)
[0x80000724]:addi fp, zero, 0
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fsqrt.s t6, zero, dyn
[0x80000730]:csrrs a0, fcsr, zero
[0x80000734]:sd t6, 80(t2)
[0x80000738]:sd a0, 88(t2)
[0x8000073c]:ld t6, 64(s1)
[0x80000740]:addi fp, zero, 0
[0x80000744]:csrrw zero, fcsr, fp
[0x80000748]:fsqrt.s zero, t6, dyn
[0x8000074c]:csrrs a0, fcsr, zero
[0x80000750]:sd zero, 96(t2)
[0x80000754]:sd a0, 104(t2)
[0x80000758]:addi zero, zero, 0
[0x8000075c]:addi zero, zero, 0

[0x80000620]:fsqrt.s s1, a0, dyn
[0x80000624]:csrrs tp, fcsr, zero
[0x80000628]:sd s1, 352(ra)
[0x8000062c]:sd tp, 360(ra)
[0x80000630]:ld t2, 184(gp)
[0x80000634]:addi sp, zero, 0
[0x80000638]:csrrw zero, fcsr, sp
[0x8000063c]:fsqrt.s fp, t2, dyn
[0x80000640]:csrrs tp, fcsr, zero
[0x80000644]:sd fp, 368(ra)
[0x80000648]:sd tp, 376(ra)
[0x8000064c]:auipc s1, 2
[0x80000650]:addi s1, s1, 2692
[0x80000654]:ld fp, 0(s1)
[0x80000658]:addi sp, zero, 0
[0x8000065c]:csrrw zero, fcsr, sp
[0x80000660]:fsqrt.s t2, fp, dyn
[0x80000664]:csrrs a0, fcsr, zero
[0x80000668]:sd t2, 384(ra)
[0x8000066c]:sd a0, 392(ra)
[0x80000670]:ld t0, 8(s1)
[0x80000674]:addi sp, zero, 0
[0x80000678]:csrrw zero, fcsr, sp
[0x8000067c]:fsqrt.s t1, t0, dyn
[0x80000680]:csrrs a0, fcsr, zero
[0x80000684]:sd t1, 400(ra)
[0x80000688]:sd a0, 408(ra)
[0x8000068c]:auipc t2, 2
[0x80000690]:addi t2, t2, 3420
[0x80000694]:ld t1, 16(s1)
[0x80000698]:addi fp, zero, 0
[0x8000069c]:csrrw zero, fcsr, fp
[0x800006a0]:fsqrt.s t0, t1, dyn
[0x800006a4]:csrrs a0, fcsr, zero
[0x800006a8]:sd t0, 0(t2)
[0x800006ac]:sd a0, 8(t2)
[0x800006b0]:ld gp, 24(s1)
[0x800006b4]:addi fp, zero, 0
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fsqrt.s tp, gp, dyn
[0x800006c0]:csrrs a0, fcsr, zero
[0x800006c4]:sd tp, 16(t2)
[0x800006c8]:sd a0, 24(t2)
[0x800006cc]:ld tp, 32(s1)
[0x800006d0]:addi fp, zero, 0
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fsqrt.s gp, tp, dyn
[0x800006dc]:csrrs a0, fcsr, zero
[0x800006e0]:sd gp, 32(t2)
[0x800006e4]:sd a0, 40(t2)
[0x800006e8]:ld ra, 40(s1)
[0x800006ec]:addi fp, zero, 0
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fsqrt.s sp, ra, dyn
[0x800006f8]:csrrs a0, fcsr, zero
[0x800006fc]:sd sp, 48(t2)
[0x80000700]:sd a0, 56(t2)
[0x80000704]:ld sp, 48(s1)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fsqrt.s ra, sp, dyn
[0x80000714]:csrrs a0, fcsr, zero
[0x80000718]:sd ra, 64(t2)
[0x8000071c]:sd a0, 72(t2)
[0x80000720]:ld zero, 56(s1)
[0x80000724]:addi fp, zero, 0
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fsqrt.s t6, zero, dyn
[0x80000730]:csrrs a0, fcsr, zero
[0x80000734]:sd t6, 80(t2)
[0x80000738]:sd a0, 88(t2)
[0x8000073c]:ld t6, 64(s1)
[0x80000740]:addi fp, zero, 0
[0x80000744]:csrrw zero, fcsr, fp
[0x80000748]:fsqrt.s zero, t6, dyn
[0x8000074c]:csrrs a0, fcsr, zero
[0x80000750]:sd zero, 96(t2)
[0x80000754]:sd a0, 104(t2)
[0x80000758]:addi zero, zero, 0
[0x8000075c]:addi zero, zero, 0

[0x8000063c]:fsqrt.s fp, t2, dyn
[0x80000640]:csrrs tp, fcsr, zero
[0x80000644]:sd fp, 368(ra)
[0x80000648]:sd tp, 376(ra)
[0x8000064c]:auipc s1, 2
[0x80000650]:addi s1, s1, 2692
[0x80000654]:ld fp, 0(s1)
[0x80000658]:addi sp, zero, 0
[0x8000065c]:csrrw zero, fcsr, sp
[0x80000660]:fsqrt.s t2, fp, dyn
[0x80000664]:csrrs a0, fcsr, zero
[0x80000668]:sd t2, 384(ra)
[0x8000066c]:sd a0, 392(ra)
[0x80000670]:ld t0, 8(s1)
[0x80000674]:addi sp, zero, 0
[0x80000678]:csrrw zero, fcsr, sp
[0x8000067c]:fsqrt.s t1, t0, dyn
[0x80000680]:csrrs a0, fcsr, zero
[0x80000684]:sd t1, 400(ra)
[0x80000688]:sd a0, 408(ra)
[0x8000068c]:auipc t2, 2
[0x80000690]:addi t2, t2, 3420
[0x80000694]:ld t1, 16(s1)
[0x80000698]:addi fp, zero, 0
[0x8000069c]:csrrw zero, fcsr, fp
[0x800006a0]:fsqrt.s t0, t1, dyn
[0x800006a4]:csrrs a0, fcsr, zero
[0x800006a8]:sd t0, 0(t2)
[0x800006ac]:sd a0, 8(t2)
[0x800006b0]:ld gp, 24(s1)
[0x800006b4]:addi fp, zero, 0
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fsqrt.s tp, gp, dyn
[0x800006c0]:csrrs a0, fcsr, zero
[0x800006c4]:sd tp, 16(t2)
[0x800006c8]:sd a0, 24(t2)
[0x800006cc]:ld tp, 32(s1)
[0x800006d0]:addi fp, zero, 0
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fsqrt.s gp, tp, dyn
[0x800006dc]:csrrs a0, fcsr, zero
[0x800006e0]:sd gp, 32(t2)
[0x800006e4]:sd a0, 40(t2)
[0x800006e8]:ld ra, 40(s1)
[0x800006ec]:addi fp, zero, 0
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fsqrt.s sp, ra, dyn
[0x800006f8]:csrrs a0, fcsr, zero
[0x800006fc]:sd sp, 48(t2)
[0x80000700]:sd a0, 56(t2)
[0x80000704]:ld sp, 48(s1)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fsqrt.s ra, sp, dyn
[0x80000714]:csrrs a0, fcsr, zero
[0x80000718]:sd ra, 64(t2)
[0x8000071c]:sd a0, 72(t2)
[0x80000720]:ld zero, 56(s1)
[0x80000724]:addi fp, zero, 0
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fsqrt.s t6, zero, dyn
[0x80000730]:csrrs a0, fcsr, zero
[0x80000734]:sd t6, 80(t2)
[0x80000738]:sd a0, 88(t2)
[0x8000073c]:ld t6, 64(s1)
[0x80000740]:addi fp, zero, 0
[0x80000744]:csrrw zero, fcsr, fp
[0x80000748]:fsqrt.s zero, t6, dyn
[0x8000074c]:csrrs a0, fcsr, zero
[0x80000750]:sd zero, 96(t2)
[0x80000754]:sd a0, 104(t2)
[0x80000758]:addi zero, zero, 0
[0x8000075c]:addi zero, zero, 0

[0x80000660]:fsqrt.s t2, fp, dyn
[0x80000664]:csrrs a0, fcsr, zero
[0x80000668]:sd t2, 384(ra)
[0x8000066c]:sd a0, 392(ra)
[0x80000670]:ld t0, 8(s1)
[0x80000674]:addi sp, zero, 0
[0x80000678]:csrrw zero, fcsr, sp
[0x8000067c]:fsqrt.s t1, t0, dyn
[0x80000680]:csrrs a0, fcsr, zero
[0x80000684]:sd t1, 400(ra)
[0x80000688]:sd a0, 408(ra)
[0x8000068c]:auipc t2, 2
[0x80000690]:addi t2, t2, 3420
[0x80000694]:ld t1, 16(s1)
[0x80000698]:addi fp, zero, 0
[0x8000069c]:csrrw zero, fcsr, fp
[0x800006a0]:fsqrt.s t0, t1, dyn
[0x800006a4]:csrrs a0, fcsr, zero
[0x800006a8]:sd t0, 0(t2)
[0x800006ac]:sd a0, 8(t2)
[0x800006b0]:ld gp, 24(s1)
[0x800006b4]:addi fp, zero, 0
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fsqrt.s tp, gp, dyn
[0x800006c0]:csrrs a0, fcsr, zero
[0x800006c4]:sd tp, 16(t2)
[0x800006c8]:sd a0, 24(t2)
[0x800006cc]:ld tp, 32(s1)
[0x800006d0]:addi fp, zero, 0
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fsqrt.s gp, tp, dyn
[0x800006dc]:csrrs a0, fcsr, zero
[0x800006e0]:sd gp, 32(t2)
[0x800006e4]:sd a0, 40(t2)
[0x800006e8]:ld ra, 40(s1)
[0x800006ec]:addi fp, zero, 0
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fsqrt.s sp, ra, dyn
[0x800006f8]:csrrs a0, fcsr, zero
[0x800006fc]:sd sp, 48(t2)
[0x80000700]:sd a0, 56(t2)
[0x80000704]:ld sp, 48(s1)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fsqrt.s ra, sp, dyn
[0x80000714]:csrrs a0, fcsr, zero
[0x80000718]:sd ra, 64(t2)
[0x8000071c]:sd a0, 72(t2)
[0x80000720]:ld zero, 56(s1)
[0x80000724]:addi fp, zero, 0
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fsqrt.s t6, zero, dyn
[0x80000730]:csrrs a0, fcsr, zero
[0x80000734]:sd t6, 80(t2)
[0x80000738]:sd a0, 88(t2)
[0x8000073c]:ld t6, 64(s1)
[0x80000740]:addi fp, zero, 0
[0x80000744]:csrrw zero, fcsr, fp
[0x80000748]:fsqrt.s zero, t6, dyn
[0x8000074c]:csrrs a0, fcsr, zero
[0x80000750]:sd zero, 96(t2)
[0x80000754]:sd a0, 104(t2)
[0x80000758]:addi zero, zero, 0
[0x8000075c]:addi zero, zero, 0

[0x8000067c]:fsqrt.s t1, t0, dyn
[0x80000680]:csrrs a0, fcsr, zero
[0x80000684]:sd t1, 400(ra)
[0x80000688]:sd a0, 408(ra)
[0x8000068c]:auipc t2, 2
[0x80000690]:addi t2, t2, 3420
[0x80000694]:ld t1, 16(s1)
[0x80000698]:addi fp, zero, 0
[0x8000069c]:csrrw zero, fcsr, fp
[0x800006a0]:fsqrt.s t0, t1, dyn
[0x800006a4]:csrrs a0, fcsr, zero
[0x800006a8]:sd t0, 0(t2)
[0x800006ac]:sd a0, 8(t2)
[0x800006b0]:ld gp, 24(s1)
[0x800006b4]:addi fp, zero, 0
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fsqrt.s tp, gp, dyn
[0x800006c0]:csrrs a0, fcsr, zero
[0x800006c4]:sd tp, 16(t2)
[0x800006c8]:sd a0, 24(t2)
[0x800006cc]:ld tp, 32(s1)
[0x800006d0]:addi fp, zero, 0
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fsqrt.s gp, tp, dyn
[0x800006dc]:csrrs a0, fcsr, zero
[0x800006e0]:sd gp, 32(t2)
[0x800006e4]:sd a0, 40(t2)
[0x800006e8]:ld ra, 40(s1)
[0x800006ec]:addi fp, zero, 0
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fsqrt.s sp, ra, dyn
[0x800006f8]:csrrs a0, fcsr, zero
[0x800006fc]:sd sp, 48(t2)
[0x80000700]:sd a0, 56(t2)
[0x80000704]:ld sp, 48(s1)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fsqrt.s ra, sp, dyn
[0x80000714]:csrrs a0, fcsr, zero
[0x80000718]:sd ra, 64(t2)
[0x8000071c]:sd a0, 72(t2)
[0x80000720]:ld zero, 56(s1)
[0x80000724]:addi fp, zero, 0
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fsqrt.s t6, zero, dyn
[0x80000730]:csrrs a0, fcsr, zero
[0x80000734]:sd t6, 80(t2)
[0x80000738]:sd a0, 88(t2)
[0x8000073c]:ld t6, 64(s1)
[0x80000740]:addi fp, zero, 0
[0x80000744]:csrrw zero, fcsr, fp
[0x80000748]:fsqrt.s zero, t6, dyn
[0x8000074c]:csrrs a0, fcsr, zero
[0x80000750]:sd zero, 96(t2)
[0x80000754]:sd a0, 104(t2)
[0x80000758]:addi zero, zero, 0
[0x8000075c]:addi zero, zero, 0

[0x8000072c]:fsqrt.s t6, zero, dyn
[0x80000730]:csrrs a0, fcsr, zero
[0x80000734]:sd t6, 80(t2)
[0x80000738]:sd a0, 88(t2)
[0x8000073c]:ld t6, 64(s1)
[0x80000740]:addi fp, zero, 0
[0x80000744]:csrrw zero, fcsr, fp
[0x80000748]:fsqrt.s zero, t6, dyn
[0x8000074c]:csrrs a0, fcsr, zero
[0x80000750]:sd zero, 96(t2)
[0x80000754]:sd a0, 104(t2)
[0x80000758]:addi zero, zero, 0
[0x8000075c]:addi zero, zero, 0

[0x80000748]:fsqrt.s zero, t6, dyn
[0x8000074c]:csrrs a0, fcsr, zero
[0x80000750]:sd zero, 96(t2)
[0x80000754]:sd a0, 104(t2)
[0x80000758]:addi zero, zero, 0
[0x8000075c]:addi zero, zero, 0



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fsqrt.s', 'rs1 : x31', 'rd : x31', 'rs1 == rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800003b8]:fsqrt.s t6, t6, dyn
	-[0x800003bc]:csrrs tp, fcsr, zero
	-[0x800003c0]:sd t6, 0(ra)
	-[0x800003c4]:sd tp, 8(ra)
Current Store : [0x800003c4] : sd tp, 8(ra) -- Store: [0x80002320]:0x0000000000000000




Last Coverpoint : ['rs1 : x29', 'rd : x30', 'rs1 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800003d4]:fsqrt.s t5, t4, dyn
	-[0x800003d8]:csrrs tp, fcsr, zero
	-[0x800003dc]:sd t5, 16(ra)
	-[0x800003e0]:sd tp, 24(ra)
Current Store : [0x800003e0] : sd tp, 24(ra) -- Store: [0x80002330]:0x0000000000000020




Last Coverpoint : ['rs1 : x30', 'rd : x29', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800003f0]:fsqrt.s t4, t5, dyn
	-[0x800003f4]:csrrs tp, fcsr, zero
	-[0x800003f8]:sd t4, 32(ra)
	-[0x800003fc]:sd tp, 40(ra)
Current Store : [0x800003fc] : sd tp, 40(ra) -- Store: [0x80002340]:0x0000000000000040




Last Coverpoint : ['rs1 : x27', 'rd : x28', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000040c]:fsqrt.s t3, s11, dyn
	-[0x80000410]:csrrs tp, fcsr, zero
	-[0x80000414]:sd t3, 48(ra)
	-[0x80000418]:sd tp, 56(ra)
Current Store : [0x80000418] : sd tp, 56(ra) -- Store: [0x80002350]:0x0000000000000060




Last Coverpoint : ['rs1 : x28', 'rd : x27', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000428]:fsqrt.s s11, t3, dyn
	-[0x8000042c]:csrrs tp, fcsr, zero
	-[0x80000430]:sd s11, 64(ra)
	-[0x80000434]:sd tp, 72(ra)
Current Store : [0x80000434] : sd tp, 72(ra) -- Store: [0x80002360]:0x0000000000000080




Last Coverpoint : ['rs1 : x25', 'rd : x26', 'fs1 == 0 and fe1 == 0xd5 and fm1 == 0x000000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000444]:fsqrt.s s10, s9, dyn
	-[0x80000448]:csrrs tp, fcsr, zero
	-[0x8000044c]:sd s10, 80(ra)
	-[0x80000450]:sd tp, 88(ra)
Current Store : [0x80000450] : sd tp, 88(ra) -- Store: [0x80002370]:0x0000000000000000




Last Coverpoint : ['rs1 : x26', 'rd : x25', 'fs1 == 0 and fe1 == 0xd5 and fm1 == 0x000000 and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000460]:fsqrt.s s9, s10, dyn
	-[0x80000464]:csrrs tp, fcsr, zero
	-[0x80000468]:sd s9, 96(ra)
	-[0x8000046c]:sd tp, 104(ra)
Current Store : [0x8000046c] : sd tp, 104(ra) -- Store: [0x80002380]:0x0000000000000020




Last Coverpoint : ['rs1 : x23', 'rd : x24', 'fs1 == 0 and fe1 == 0xd5 and fm1 == 0x000000 and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000047c]:fsqrt.s s8, s7, dyn
	-[0x80000480]:csrrs tp, fcsr, zero
	-[0x80000484]:sd s8, 112(ra)
	-[0x80000488]:sd tp, 120(ra)
Current Store : [0x80000488] : sd tp, 120(ra) -- Store: [0x80002390]:0x0000000000000040




Last Coverpoint : ['rs1 : x24', 'rd : x23', 'fs1 == 0 and fe1 == 0xd5 and fm1 == 0x000000 and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000498]:fsqrt.s s7, s8, dyn
	-[0x8000049c]:csrrs tp, fcsr, zero
	-[0x800004a0]:sd s7, 128(ra)
	-[0x800004a4]:sd tp, 136(ra)
Current Store : [0x800004a4] : sd tp, 136(ra) -- Store: [0x800023a0]:0x0000000000000060




Last Coverpoint : ['rs1 : x21', 'rd : x22', 'fs1 == 0 and fe1 == 0xd5 and fm1 == 0x000000 and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800004b4]:fsqrt.s s6, s5, dyn
	-[0x800004b8]:csrrs tp, fcsr, zero
	-[0x800004bc]:sd s6, 144(ra)
	-[0x800004c0]:sd tp, 152(ra)
Current Store : [0x800004c0] : sd tp, 152(ra) -- Store: [0x800023b0]:0x0000000000000080




Last Coverpoint : ['rs1 : x22', 'rd : x21', 'fs1 == 0 and fe1 == 0x29 and fm1 == 0x000000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800004d0]:fsqrt.s s5, s6, dyn
	-[0x800004d4]:csrrs tp, fcsr, zero
	-[0x800004d8]:sd s5, 160(ra)
	-[0x800004dc]:sd tp, 168(ra)
Current Store : [0x800004dc] : sd tp, 168(ra) -- Store: [0x800023c0]:0x0000000000000000




Last Coverpoint : ['rs1 : x19', 'rd : x20', 'fs1 == 0 and fe1 == 0x29 and fm1 == 0x000000 and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800004ec]:fsqrt.s s4, s3, dyn
	-[0x800004f0]:csrrs tp, fcsr, zero
	-[0x800004f4]:sd s4, 176(ra)
	-[0x800004f8]:sd tp, 184(ra)
Current Store : [0x800004f8] : sd tp, 184(ra) -- Store: [0x800023d0]:0x0000000000000020




Last Coverpoint : ['rs1 : x20', 'rd : x19', 'fs1 == 0 and fe1 == 0x29 and fm1 == 0x000000 and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000508]:fsqrt.s s3, s4, dyn
	-[0x8000050c]:csrrs tp, fcsr, zero
	-[0x80000510]:sd s3, 192(ra)
	-[0x80000514]:sd tp, 200(ra)
Current Store : [0x80000514] : sd tp, 200(ra) -- Store: [0x800023e0]:0x0000000000000040




Last Coverpoint : ['rs1 : x17', 'rd : x18', 'fs1 == 0 and fe1 == 0x29 and fm1 == 0x000000 and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000524]:fsqrt.s s2, a7, dyn
	-[0x80000528]:csrrs tp, fcsr, zero
	-[0x8000052c]:sd s2, 208(ra)
	-[0x80000530]:sd tp, 216(ra)
Current Store : [0x80000530] : sd tp, 216(ra) -- Store: [0x800023f0]:0x0000000000000060




Last Coverpoint : ['rs1 : x18', 'rd : x17', 'fs1 == 0 and fe1 == 0x29 and fm1 == 0x000000 and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000540]:fsqrt.s a7, s2, dyn
	-[0x80000544]:csrrs tp, fcsr, zero
	-[0x80000548]:sd a7, 224(ra)
	-[0x8000054c]:sd tp, 232(ra)
Current Store : [0x8000054c] : sd tp, 232(ra) -- Store: [0x80002400]:0x0000000000000080




Last Coverpoint : ['rs1 : x15', 'rd : x16']
Last Code Sequence : 
	-[0x8000055c]:fsqrt.s a6, a5, dyn
	-[0x80000560]:csrrs tp, fcsr, zero
	-[0x80000564]:sd a6, 240(ra)
	-[0x80000568]:sd tp, 248(ra)
Current Store : [0x80000568] : sd tp, 248(ra) -- Store: [0x80002410]:0x0000000000000000




Last Coverpoint : ['rs1 : x16', 'rd : x15']
Last Code Sequence : 
	-[0x80000578]:fsqrt.s a5, a6, dyn
	-[0x8000057c]:csrrs tp, fcsr, zero
	-[0x80000580]:sd a5, 256(ra)
	-[0x80000584]:sd tp, 264(ra)
Current Store : [0x80000584] : sd tp, 264(ra) -- Store: [0x80002420]:0x0000000000000000




Last Coverpoint : ['rs1 : x13', 'rd : x14']
Last Code Sequence : 
	-[0x80000594]:fsqrt.s a4, a3, dyn
	-[0x80000598]:csrrs tp, fcsr, zero
	-[0x8000059c]:sd a4, 272(ra)
	-[0x800005a0]:sd tp, 280(ra)
Current Store : [0x800005a0] : sd tp, 280(ra) -- Store: [0x80002430]:0x0000000000000000




Last Coverpoint : ['rs1 : x3', 'rd : x4']
Last Code Sequence : 
	-[0x800006bc]:fsqrt.s tp, gp, dyn
	-[0x800006c0]:csrrs a0, fcsr, zero
	-[0x800006c4]:sd tp, 16(t2)
	-[0x800006c8]:sd a0, 24(t2)
Current Store : [0x800006c8] : sd a0, 24(t2) -- Store: [0x80002400]:0x0000000000000000




Last Coverpoint : ['rs1 : x4', 'rd : x3']
Last Code Sequence : 
	-[0x800006d8]:fsqrt.s gp, tp, dyn
	-[0x800006dc]:csrrs a0, fcsr, zero
	-[0x800006e0]:sd gp, 32(t2)
	-[0x800006e4]:sd a0, 40(t2)
Current Store : [0x800006e4] : sd a0, 40(t2) -- Store: [0x80002410]:0x0000000000000000




Last Coverpoint : ['rs1 : x1', 'rd : x2']
Last Code Sequence : 
	-[0x800006f4]:fsqrt.s sp, ra, dyn
	-[0x800006f8]:csrrs a0, fcsr, zero
	-[0x800006fc]:sd sp, 48(t2)
	-[0x80000700]:sd a0, 56(t2)
Current Store : [0x80000700] : sd a0, 56(t2) -- Store: [0x80002420]:0x0000000000000000




Last Coverpoint : ['rs1 : x2', 'rd : x1']
Last Code Sequence : 
	-[0x80000710]:fsqrt.s ra, sp, dyn
	-[0x80000714]:csrrs a0, fcsr, zero
	-[0x80000718]:sd ra, 64(t2)
	-[0x8000071c]:sd a0, 72(t2)
Current Store : [0x8000071c] : sd a0, 72(t2) -- Store: [0x80002430]:0x0000000000000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|signature|coverpoints|code|
|----|---------|-----------|----|
