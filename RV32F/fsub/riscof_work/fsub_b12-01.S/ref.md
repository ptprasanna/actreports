
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800006d0')]      |
| SIG_REGION                | [('0x80002310', '0x800024b0', '104 words')]      |
| COV_LABELS                | fsub_b12      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch14/fsub/riscof_work/fsub_b12-01.S/ref.S    |
| Total Number of coverpoints| 158     |
| Total Coverpoints Hit     | 152      |
| Total Signature Updates   | 52      |
| STAT1                     | 52      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                      coverpoints                                                                                                       |                                                                          code                                                                          |
|---:|--------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002314]<br>0x00000001|- opcode : fsub.s<br> - rs1 : f3<br> - rs2 : f18<br> - rd : f3<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x08e8ca and rm_val == 0  #nosat<br>   |[0x80000120]:fsub.s ft3, ft3, fs2, dyn<br> [0x80000124]:csrrs a7, fflags, zero<br> [0x80000128]:fsw ft3, 0(a5)<br> [0x8000012c]:sw a7, 4(a5)<br>        |
|   2|[0x8000231c]<br>0x00000001|- rs1 : f23<br> - rs2 : f29<br> - rd : f8<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x578fb8 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4fc538 and rm_val == 0  #nosat<br> |[0x8000013c]:fsub.s fs0, fs7, ft9, dyn<br> [0x80000140]:csrrs a7, fflags, zero<br> [0x80000144]:fsw fs0, 8(a5)<br> [0x80000148]:sw a7, 12(a5)<br>       |
|   3|[0x80002324]<br>0x00000001|- rs1 : f9<br> - rs2 : f9<br> - rd : f12<br> - rs1 == rs2 != rd<br>                                                                                                                                                     |[0x80000158]:fsub.s fa2, fs1, fs1, dyn<br> [0x8000015c]:csrrs a7, fflags, zero<br> [0x80000160]:fsw fa2, 16(a5)<br> [0x80000164]:sw a7, 20(a5)<br>      |
|   4|[0x8000232c]<br>0x00000001|- rs1 : f16<br> - rs2 : f7<br> - rd : f7<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x2a257f and fs2 == 0 and fe2 == 0xfb and fm2 == 0x5fc232 and rm_val == 0  #nosat<br>                         |[0x80000174]:fsub.s ft7, fa6, ft7, dyn<br> [0x80000178]:csrrs a7, fflags, zero<br> [0x8000017c]:fsw ft7, 24(a5)<br> [0x80000180]:sw a7, 28(a5)<br>      |
|   5|[0x80002334]<br>0x00000001|- rs1 : f17<br> - rs2 : f17<br> - rd : f17<br> - rs1 == rs2 == rd<br>                                                                                                                                                   |[0x80000190]:fsub.s fa7, fa7, fa7, dyn<br> [0x80000194]:csrrs a7, fflags, zero<br> [0x80000198]:fsw fa7, 32(a5)<br> [0x8000019c]:sw a7, 36(a5)<br>      |
|   6|[0x8000233c]<br>0x00000001|- rs1 : f19<br> - rs2 : f8<br> - rd : f26<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x37c42d and fs2 == 0 and fe2 == 0xfe and fm2 == 0x36ab8f and rm_val == 0  #nosat<br>                                               |[0x800001ac]:fsub.s fs10, fs3, fs0, dyn<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:fsw fs10, 40(a5)<br> [0x800001b8]:sw a7, 44(a5)<br>    |
|   7|[0x80002344]<br>0x00000001|- rs1 : f26<br> - rs2 : f5<br> - rd : f21<br> - fs1 == 0 and fe1 == 0xfc and fm1 == 0x4ece7f and fs2 == 1 and fe2 == 0xfc and fm2 == 0x560df4 and rm_val == 0  #nosat<br>                                               |[0x800001c8]:fsub.s fs5, fs10, ft5, dyn<br> [0x800001cc]:csrrs a7, fflags, zero<br> [0x800001d0]:fsw fs5, 48(a5)<br> [0x800001d4]:sw a7, 52(a5)<br>     |
|   8|[0x8000234c]<br>0x00000001|- rs1 : f5<br> - rs2 : f16<br> - rd : f2<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x5ddf89 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x364437 and rm_val == 0  #nosat<br>                                                |[0x800001e4]:fsub.s ft2, ft5, fa6, dyn<br> [0x800001e8]:csrrs a7, fflags, zero<br> [0x800001ec]:fsw ft2, 56(a5)<br> [0x800001f0]:sw a7, 60(a5)<br>      |
|   9|[0x80002354]<br>0x00000001|- rs1 : f15<br> - rs2 : f28<br> - rd : f1<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x4549ce and fs2 == 1 and fe2 == 0xfd and fm2 == 0x28758a and rm_val == 0  #nosat<br>                                               |[0x80000200]:fsub.s ft1, fa5, ft8, dyn<br> [0x80000204]:csrrs a7, fflags, zero<br> [0x80000208]:fsw ft1, 64(a5)<br> [0x8000020c]:sw a7, 68(a5)<br>      |
|  10|[0x8000235c]<br>0x00000001|- rs1 : f21<br> - rs2 : f0<br> - rd : f29<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x252cf6 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x713214 and rm_val == 0  #nosat<br>                                               |[0x8000021c]:fsub.s ft9, fs5, ft0, dyn<br> [0x80000220]:csrrs a7, fflags, zero<br> [0x80000224]:fsw ft9, 72(a5)<br> [0x80000228]:sw a7, 76(a5)<br>      |
|  11|[0x80002364]<br>0x00000001|- rs1 : f18<br> - rs2 : f6<br> - rd : f24<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x13f0c0 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x3155e7 and rm_val == 0  #nosat<br>                                               |[0x80000238]:fsub.s fs8, fs2, ft6, dyn<br> [0x8000023c]:csrrs a7, fflags, zero<br> [0x80000240]:fsw fs8, 80(a5)<br> [0x80000244]:sw a7, 84(a5)<br>      |
|  12|[0x8000236c]<br>0x00000001|- rs1 : f20<br> - rs2 : f25<br> - rd : f6<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x40dc0e and fs2 == 1 and fe2 == 0xfd and fm2 == 0x384200 and rm_val == 0  #nosat<br>                                               |[0x80000254]:fsub.s ft6, fs4, fs9, dyn<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:fsw ft6, 88(a5)<br> [0x80000260]:sw a7, 92(a5)<br>      |
|  13|[0x80002374]<br>0x00000001|- rs1 : f30<br> - rs2 : f14<br> - rd : f11<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x17246c and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2b74d4 and rm_val == 0  #nosat<br>                                              |[0x80000270]:fsub.s fa1, ft10, fa4, dyn<br> [0x80000274]:csrrs a7, fflags, zero<br> [0x80000278]:fsw fa1, 96(a5)<br> [0x8000027c]:sw a7, 100(a5)<br>    |
|  14|[0x8000237c]<br>0x00000001|- rs1 : f25<br> - rs2 : f3<br> - rd : f9<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x3cc5a4 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x15c2f3 and rm_val == 0  #nosat<br>                                                |[0x8000028c]:fsub.s fs1, fs9, ft3, dyn<br> [0x80000290]:csrrs a7, fflags, zero<br> [0x80000294]:fsw fs1, 104(a5)<br> [0x80000298]:sw a7, 108(a5)<br>    |
|  15|[0x80002384]<br>0x00000001|- rs1 : f13<br> - rs2 : f1<br> - rd : f5<br> - fs1 == 0 and fe1 == 0xfa and fm1 == 0x0597cb and fs2 == 1 and fe2 == 0xfd and fm2 == 0x7d664b and rm_val == 0  #nosat<br>                                                |[0x800002a8]:fsub.s ft5, fa3, ft1, dyn<br> [0x800002ac]:csrrs a7, fflags, zero<br> [0x800002b0]:fsw ft5, 112(a5)<br> [0x800002b4]:sw a7, 116(a5)<br>    |
|  16|[0x8000238c]<br>0x00000001|- rs1 : f24<br> - rs2 : f4<br> - rd : f28<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x3c0ad4 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x30af7e and rm_val == 0  #nosat<br>                                               |[0x800002c4]:fsub.s ft8, fs8, ft4, dyn<br> [0x800002c8]:csrrs a7, fflags, zero<br> [0x800002cc]:fsw ft8, 120(a5)<br> [0x800002d0]:sw a7, 124(a5)<br>    |
|  17|[0x80002394]<br>0x00000001|- rs1 : f29<br> - rs2 : f22<br> - rd : f16<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x480a54 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x441f1f and rm_val == 0  #nosat<br>                                              |[0x800002e0]:fsub.s fa6, ft9, fs6, dyn<br> [0x800002e4]:csrrs a7, fflags, zero<br> [0x800002e8]:fsw fa6, 128(a5)<br> [0x800002ec]:sw a7, 132(a5)<br>    |
|  18|[0x8000239c]<br>0x00000001|- rs1 : f31<br> - rs2 : f11<br> - rd : f14<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x433c5b and fs2 == 1 and fe2 == 0xfc and fm2 == 0x4f5f54 and rm_val == 0  #nosat<br>                                              |[0x800002fc]:fsub.s fa4, ft11, fa1, dyn<br> [0x80000300]:csrrs a7, fflags, zero<br> [0x80000304]:fsw fa4, 136(a5)<br> [0x80000308]:sw a7, 140(a5)<br>   |
|  19|[0x800023a4]<br>0x00000001|- rs1 : f0<br> - rs2 : f10<br> - rd : f31<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x0fe2cd and fs2 == 1 and fe2 == 0xfa and fm2 == 0x456706 and rm_val == 0  #nosat<br>                                               |[0x80000318]:fsub.s ft11, ft0, fa0, dyn<br> [0x8000031c]:csrrs a7, fflags, zero<br> [0x80000320]:fsw ft11, 144(a5)<br> [0x80000324]:sw a7, 148(a5)<br>  |
|  20|[0x800023ac]<br>0x00000001|- rs1 : f28<br> - rs2 : f13<br> - rd : f23<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x06fbdb and fs2 == 0 and fe2 == 0xf9 and fm2 == 0x2f7105 and rm_val == 0  #nosat<br>                                              |[0x80000334]:fsub.s fs7, ft8, fa3, dyn<br> [0x80000338]:csrrs a7, fflags, zero<br> [0x8000033c]:fsw fs7, 152(a5)<br> [0x80000340]:sw a7, 156(a5)<br>    |
|  21|[0x800023b4]<br>0x00000001|- rs1 : f11<br> - rs2 : f20<br> - rd : f15<br> - fs1 == 0 and fe1 == 0xfc and fm1 == 0x04dea3 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x104dca and rm_val == 0  #nosat<br>                                              |[0x80000350]:fsub.s fa5, fa1, fs4, dyn<br> [0x80000354]:csrrs a7, fflags, zero<br> [0x80000358]:fsw fa5, 160(a5)<br> [0x8000035c]:sw a7, 164(a5)<br>    |
|  22|[0x800023bc]<br>0x00000001|- rs1 : f8<br> - rs2 : f27<br> - rd : f4<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x191a03 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x131b4d and rm_val == 0  #nosat<br>                                                |[0x8000036c]:fsub.s ft4, fs0, fs11, dyn<br> [0x80000370]:csrrs a7, fflags, zero<br> [0x80000374]:fsw ft4, 168(a5)<br> [0x80000378]:sw a7, 172(a5)<br>   |
|  23|[0x800023c4]<br>0x00000001|- rs1 : f7<br> - rs2 : f15<br> - rd : f25<br> - fs1 == 0 and fe1 == 0xf9 and fm1 == 0x54206e and fs2 == 1 and fe2 == 0xfc and fm2 == 0x1fe890 and rm_val == 0  #nosat<br>                                               |[0x80000388]:fsub.s fs9, ft7, fa5, dyn<br> [0x8000038c]:csrrs a7, fflags, zero<br> [0x80000390]:fsw fs9, 176(a5)<br> [0x80000394]:sw a7, 180(a5)<br>    |
|  24|[0x800023cc]<br>0x00000001|- rs1 : f10<br> - rs2 : f2<br> - rd : f13<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x42076b and fs2 == 1 and fe2 == 0xfb and fm2 == 0x00976d and rm_val == 0  #nosat<br>                                               |[0x800003a4]:fsub.s fa3, fa0, ft2, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:fsw fa3, 184(a5)<br> [0x800003b0]:sw a7, 188(a5)<br>    |
|  25|[0x800023d4]<br>0x00000001|- rs1 : f14<br> - rs2 : f31<br> - rd : f18<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x296f9b and fs2 == 0 and fe2 == 0xf9 and fm2 == 0x5ee9fe and rm_val == 0  #nosat<br>                                              |[0x800003c0]:fsub.s fs2, fa4, ft11, dyn<br> [0x800003c4]:csrrs a7, fflags, zero<br> [0x800003c8]:fsw fs2, 192(a5)<br> [0x800003cc]:sw a7, 196(a5)<br>   |
|  26|[0x800023dc]<br>0x00000001|- rs1 : f27<br> - rs2 : f21<br> - rd : f20<br> - fs1 == 0 and fe1 == 0xfc and fm1 == 0x68fcac and fs2 == 1 and fe2 == 0xfc and fm2 == 0x5a465e and rm_val == 0  #nosat<br>                                              |[0x800003dc]:fsub.s fs4, fs11, fs5, dyn<br> [0x800003e0]:csrrs a7, fflags, zero<br> [0x800003e4]:fsw fs4, 200(a5)<br> [0x800003e8]:sw a7, 204(a5)<br>   |
|  27|[0x800023e4]<br>0x00000001|- rs1 : f1<br> - rs2 : f23<br> - rd : f0<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x1f5de9 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x755870 and rm_val == 0  #nosat<br>                                                |[0x800003f8]:fsub.s ft0, ft1, fs7, dyn<br> [0x800003fc]:csrrs a7, fflags, zero<br> [0x80000400]:fsw ft0, 208(a5)<br> [0x80000404]:sw a7, 212(a5)<br>    |
|  28|[0x800023ec]<br>0x00000001|- rs1 : f12<br> - rs2 : f19<br> - rd : f30<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x33cbed and fs2 == 0 and fe2 == 0xfd and fm2 == 0x45810c and rm_val == 0  #nosat<br>                                              |[0x80000414]:fsub.s ft10, fa2, fs3, dyn<br> [0x80000418]:csrrs a7, fflags, zero<br> [0x8000041c]:fsw ft10, 216(a5)<br> [0x80000420]:sw a7, 220(a5)<br>  |
|  29|[0x800023f4]<br>0x00000001|- rs1 : f2<br> - rs2 : f26<br> - rd : f22<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a8666 and fs2 == 0 and fe2 == 0xf7 and fm2 == 0x7cf3ad and rm_val == 0  #nosat<br>                                               |[0x80000430]:fsub.s fs6, ft2, fs10, dyn<br> [0x80000434]:csrrs a7, fflags, zero<br> [0x80000438]:fsw fs6, 224(a5)<br> [0x8000043c]:sw a7, 228(a5)<br>   |
|  30|[0x800023fc]<br>0x00000001|- rs1 : f22<br> - rs2 : f30<br> - rd : f27<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x39afdd and fs2 == 1 and fe2 == 0xfc and fm2 == 0x22aa99 and rm_val == 0  #nosat<br>                                              |[0x8000044c]:fsub.s fs11, fs6, ft10, dyn<br> [0x80000450]:csrrs a7, fflags, zero<br> [0x80000454]:fsw fs11, 232(a5)<br> [0x80000458]:sw a7, 236(a5)<br> |
|  31|[0x80002404]<br>0x00000001|- rs1 : f6<br> - rs2 : f12<br> - rd : f10<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x3e917d and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4bab36 and rm_val == 0  #nosat<br>                                               |[0x80000468]:fsub.s fa0, ft6, fa2, dyn<br> [0x8000046c]:csrrs a7, fflags, zero<br> [0x80000470]:fsw fa0, 240(a5)<br> [0x80000474]:sw a7, 244(a5)<br>    |
|  32|[0x8000240c]<br>0x00000001|- rs1 : f4<br> - rs2 : f24<br> - rd : f19<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x76a41a and fs2 == 0 and fe2 == 0xfd and fm2 == 0x2f40c6 and rm_val == 0  #nosat<br>                                               |[0x80000484]:fsub.s fs3, ft4, fs8, dyn<br> [0x80000488]:csrrs a7, fflags, zero<br> [0x8000048c]:fsw fs3, 248(a5)<br> [0x80000490]:sw a7, 252(a5)<br>    |
|  33|[0x80002414]<br>0x00000001|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x20d0a1 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x57e728 and rm_val == 0  #nosat<br>                                                                                             |[0x800004a0]:fsub.s ft11, ft10, ft9, dyn<br> [0x800004a4]:csrrs a7, fflags, zero<br> [0x800004a8]:fsw ft11, 256(a5)<br> [0x800004ac]:sw a7, 260(a5)<br> |
|  34|[0x8000241c]<br>0x00000001|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x5e8d61 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x4e0c55 and rm_val == 0  #nosat<br>                                                                                             |[0x800004bc]:fsub.s ft11, ft10, ft9, dyn<br> [0x800004c0]:csrrs a7, fflags, zero<br> [0x800004c4]:fsw ft11, 264(a5)<br> [0x800004c8]:sw a7, 268(a5)<br> |
|  35|[0x80002424]<br>0x00000001|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x280619 and fs2 == 1 and fe2 == 0xfc and fm2 == 0x38f39d and rm_val == 0  #nosat<br>                                                                                             |[0x800004d8]:fsub.s ft11, ft10, ft9, dyn<br> [0x800004dc]:csrrs a7, fflags, zero<br> [0x800004e0]:fsw ft11, 272(a5)<br> [0x800004e4]:sw a7, 276(a5)<br> |
|  36|[0x8000242c]<br>0x00000001|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x330244 and fs2 == 0 and fe2 == 0xf5 and fm2 == 0x5a077f and rm_val == 0  #nosat<br>                                                                                             |[0x800004f4]:fsub.s ft11, ft10, ft9, dyn<br> [0x800004f8]:csrrs a7, fflags, zero<br> [0x800004fc]:fsw ft11, 280(a5)<br> [0x80000500]:sw a7, 284(a5)<br> |
|  37|[0x80002434]<br>0x00000001|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x282619 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x31f1cb and rm_val == 0  #nosat<br>                                                                                             |[0x80000510]:fsub.s ft11, ft10, ft9, dyn<br> [0x80000514]:csrrs a7, fflags, zero<br> [0x80000518]:fsw ft11, 288(a5)<br> [0x8000051c]:sw a7, 292(a5)<br> |
|  38|[0x8000243c]<br>0x00000001|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x326d35 and fs2 == 1 and fe2 == 0xfc and fm2 == 0x13f4b3 and rm_val == 0  #nosat<br>                                                                                             |[0x8000052c]:fsub.s ft11, ft10, ft9, dyn<br> [0x80000530]:csrrs a7, fflags, zero<br> [0x80000534]:fsw ft11, 296(a5)<br> [0x80000538]:sw a7, 300(a5)<br> |
|  39|[0x80002444]<br>0x00000001|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x26592c and fs2 == 1 and fe2 == 0xfc and fm2 == 0x1dd651 and rm_val == 0  #nosat<br>                                                                                             |[0x80000548]:fsub.s ft11, ft10, ft9, dyn<br> [0x8000054c]:csrrs a7, fflags, zero<br> [0x80000550]:fsw ft11, 304(a5)<br> [0x80000554]:sw a7, 308(a5)<br> |
|  40|[0x8000244c]<br>0x00000001|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x34510e and fs2 == 0 and fe2 == 0xfd and fm2 == 0x564037 and rm_val == 0  #nosat<br>                                                                                             |[0x80000564]:fsub.s ft11, ft10, ft9, dyn<br> [0x80000568]:csrrs a7, fflags, zero<br> [0x8000056c]:fsw ft11, 312(a5)<br> [0x80000570]:sw a7, 316(a5)<br> |
|  41|[0x80002454]<br>0x00000001|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x1c56e0 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x462194 and rm_val == 0  #nosat<br>                                                                                             |[0x80000580]:fsub.s ft11, ft10, ft9, dyn<br> [0x80000584]:csrrs a7, fflags, zero<br> [0x80000588]:fsw ft11, 320(a5)<br> [0x8000058c]:sw a7, 324(a5)<br> |
|  42|[0x8000245c]<br>0x00000001|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2d12f5 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x273366 and rm_val == 0  #nosat<br>                                                                                             |[0x8000059c]:fsub.s ft11, ft10, ft9, dyn<br> [0x800005a0]:csrrs a7, fflags, zero<br> [0x800005a4]:fsw ft11, 328(a5)<br> [0x800005a8]:sw a7, 332(a5)<br> |
|  43|[0x80002464]<br>0x00000001|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x0991d2 and rm_val == 0  #nosat<br>                                                                                             |[0x800005b8]:fsub.s ft11, ft10, ft9, dyn<br> [0x800005bc]:csrrs a7, fflags, zero<br> [0x800005c0]:fsw ft11, 336(a5)<br> [0x800005c4]:sw a7, 340(a5)<br> |
|  44|[0x8000246c]<br>0x00000001|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2b0757 and fs2 == 1 and fe2 == 0xfc and fm2 == 0x5415da and rm_val == 0  #nosat<br>                                                                                             |[0x800005d4]:fsub.s ft11, ft10, ft9, dyn<br> [0x800005d8]:csrrs a7, fflags, zero<br> [0x800005dc]:fsw ft11, 344(a5)<br> [0x800005e0]:sw a7, 348(a5)<br> |
|  45|[0x80002474]<br>0x00000001|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x11f412 and rm_val == 0  #nosat<br>                                                                                             |[0x800005f0]:fsub.s ft11, ft10, ft9, dyn<br> [0x800005f4]:csrrs a7, fflags, zero<br> [0x800005f8]:fsw ft11, 352(a5)<br> [0x800005fc]:sw a7, 356(a5)<br> |
|  46|[0x8000247c]<br>0x00000001|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x207786 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x1ac051 and rm_val == 0  #nosat<br>                                                                                             |[0x8000060c]:fsub.s ft11, ft10, ft9, dyn<br> [0x80000610]:csrrs a7, fflags, zero<br> [0x80000614]:fsw ft11, 360(a5)<br> [0x80000618]:sw a7, 364(a5)<br> |
|  47|[0x80002484]<br>0x00000001|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 0 and fe2 == 0xfa and fm2 == 0x4940d1 and rm_val == 0  #nosat<br>                                                                                             |[0x80000628]:fsub.s ft11, ft10, ft9, dyn<br> [0x8000062c]:csrrs a7, fflags, zero<br> [0x80000630]:fsw ft11, 368(a5)<br> [0x80000634]:sw a7, 372(a5)<br> |
|  48|[0x8000248c]<br>0x00000001|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7a3631 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x52a1db and rm_val == 0  #nosat<br>                                                                                             |[0x80000644]:fsub.s ft11, ft10, ft9, dyn<br> [0x80000648]:csrrs a7, fflags, zero<br> [0x8000064c]:fsw ft11, 376(a5)<br> [0x80000650]:sw a7, 380(a5)<br> |
|  49|[0x80002494]<br>0x00000001|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 0 and fe2 == 0xfa and fm2 == 0x0d23d9 and rm_val == 0  #nosat<br>                                                                                             |[0x80000660]:fsub.s ft11, ft10, ft9, dyn<br> [0x80000664]:csrrs a7, fflags, zero<br> [0x80000668]:fsw ft11, 384(a5)<br> [0x8000066c]:sw a7, 388(a5)<br> |
|  50|[0x8000249c]<br>0x00000001|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x12a50c and fs2 == 0 and fe2 == 0xfd and fm2 == 0x4357ca and rm_val == 0  #nosat<br>                                                                                             |[0x8000067c]:fsub.s ft11, ft10, ft9, dyn<br> [0x80000680]:csrrs a7, fflags, zero<br> [0x80000684]:fsw ft11, 392(a5)<br> [0x80000688]:sw a7, 396(a5)<br> |
|  51|[0x800024a4]<br>0x00000001|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1b5ad7 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x5f1313 and rm_val == 0  #nosat<br>                                                                                             |[0x80000698]:fsub.s ft11, ft10, ft9, dyn<br> [0x8000069c]:csrrs a7, fflags, zero<br> [0x800006a0]:fsw ft11, 400(a5)<br> [0x800006a4]:sw a7, 404(a5)<br> |
|  52|[0x800024ac]<br>0x00000001|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x167638 and fs2 == 1 and fe2 == 0xf9 and fm2 == 0x6249a5 and rm_val == 0  #nosat<br>                                                                                             |[0x800006b4]:fsub.s ft11, ft10, ft9, dyn<br> [0x800006b8]:csrrs a7, fflags, zero<br> [0x800006bc]:fsw ft11, 408(a5)<br> [0x800006c0]:sw a7, 412(a5)<br> |
