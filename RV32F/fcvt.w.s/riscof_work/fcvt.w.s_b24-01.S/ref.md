
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000b50')]      |
| SIG_REGION                | [('0x80002310', '0x80002660', '212 words')]      |
| COV_LABELS                | fcvt.w.s_b24      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch14/fcvt.w.s/riscof_work/fcvt.w.s_b24-01.S/ref.S    |
| Total Number of coverpoints| 174     |
| Total Coverpoints Hit     | 170      |
| Total Signature Updates   | 212      |
| STAT1                     | 105      |
| STAT2                     | 1      |
| STAT3                     | 0     |
| STAT4                     | 106     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000b3c]:fcvt.w.s t6, ft11, dyn
      [0x80000b40]:csrrs a7, fflags, zero
      [0x80000b44]:sw t6, 704(a5)
 -- Signature Address: 0x80002658 Data: 0x00000001
 -- Redundant Coverpoints hit by the op
      - opcode : fcvt.w.s
      - rd : x31
      - rs1 : f31
      - fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and rm_val == 3  #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['opcode : fcvt.w.s', 'rd : x6', 'rs1 : f24', 'fs1 == 1 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000011c]:fcvt.w.s t1, fs8, dyn
	-[0x80000120]:csrrs a7, fflags, zero
	-[0x80000124]:sw t1, 0(a5)
Current Store : [0x80000128] : sw a7, 4(a5) -- Store: [0x80002314]:0x00000001




Last Coverpoint : ['rd : x27', 'rs1 : f25', 'fs1 == 0 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000134]:fcvt.w.s s11, fs9, dyn
	-[0x80000138]:csrrs a7, fflags, zero
	-[0x8000013c]:sw s11, 8(a5)
Current Store : [0x80000140] : sw a7, 12(a5) -- Store: [0x8000231c]:0x00000001




Last Coverpoint : ['rd : x15', 'rs1 : f5', 'fs1 == 0 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000158]:fcvt.w.s a5, ft5, dyn
	-[0x8000015c]:csrrs s5, fflags, zero
	-[0x80000160]:sw a5, 0(s3)
Current Store : [0x80000164] : sw s5, 4(s3) -- Store: [0x80002324]:0x00000001




Last Coverpoint : ['rd : x19', 'rs1 : f18', 'fs1 == 0 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000017c]:fcvt.w.s s3, fs2, dyn
	-[0x80000180]:csrrs a7, fflags, zero
	-[0x80000184]:sw s3, 0(a5)
Current Store : [0x80000188] : sw a7, 4(a5) -- Store: [0x8000232c]:0x00000001




Last Coverpoint : ['rd : x3', 'rs1 : f8', 'fs1 == 0 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000194]:fcvt.w.s gp, fs0, dyn
	-[0x80000198]:csrrs a7, fflags, zero
	-[0x8000019c]:sw gp, 8(a5)
Current Store : [0x800001a0] : sw a7, 12(a5) -- Store: [0x80002334]:0x00000001




Last Coverpoint : ['rd : x22', 'rs1 : f27', 'fs1 == 0 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001ac]:fcvt.w.s s6, fs11, dyn
	-[0x800001b0]:csrrs a7, fflags, zero
	-[0x800001b4]:sw s6, 16(a5)
Current Store : [0x800001b8] : sw a7, 20(a5) -- Store: [0x8000233c]:0x00000001




Last Coverpoint : ['rd : x7', 'rs1 : f10', 'fs1 == 1 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800001c4]:fcvt.w.s t2, fa0, dyn
	-[0x800001c8]:csrrs a7, fflags, zero
	-[0x800001cc]:sw t2, 24(a5)
Current Store : [0x800001d0] : sw a7, 28(a5) -- Store: [0x80002344]:0x00000001




Last Coverpoint : ['rd : x28', 'rs1 : f6', 'fs1 == 1 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800001dc]:fcvt.w.s t3, ft6, dyn
	-[0x800001e0]:csrrs a7, fflags, zero
	-[0x800001e4]:sw t3, 32(a5)
Current Store : [0x800001e8] : sw a7, 36(a5) -- Store: [0x8000234c]:0x00000001




Last Coverpoint : ['rd : x5', 'rs1 : f28', 'fs1 == 1 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800001f4]:fcvt.w.s t0, ft8, dyn
	-[0x800001f8]:csrrs a7, fflags, zero
	-[0x800001fc]:sw t0, 40(a5)
Current Store : [0x80000200] : sw a7, 44(a5) -- Store: [0x80002354]:0x00000001




Last Coverpoint : ['rd : x14', 'rs1 : f31', 'fs1 == 1 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000020c]:fcvt.w.s a4, ft11, dyn
	-[0x80000210]:csrrs a7, fflags, zero
	-[0x80000214]:sw a4, 48(a5)
Current Store : [0x80000218] : sw a7, 52(a5) -- Store: [0x8000235c]:0x00000001




Last Coverpoint : ['rd : x2', 'rs1 : f26', 'fs1 == 1 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000224]:fcvt.w.s sp, fs10, dyn
	-[0x80000228]:csrrs a7, fflags, zero
	-[0x8000022c]:sw sp, 56(a5)
Current Store : [0x80000230] : sw a7, 60(a5) -- Store: [0x80002364]:0x00000001




Last Coverpoint : ['rd : x17', 'rs1 : f20', 'fs1 == 0 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000248]:fcvt.w.s a7, fs4, dyn
	-[0x8000024c]:csrrs s5, fflags, zero
	-[0x80000250]:sw a7, 0(s3)
Current Store : [0x80000254] : sw s5, 4(s3) -- Store: [0x8000236c]:0x00000001




Last Coverpoint : ['rd : x25', 'rs1 : f19', 'fs1 == 0 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x8000026c]:fcvt.w.s s9, fs3, dyn
	-[0x80000270]:csrrs a7, fflags, zero
	-[0x80000274]:sw s9, 0(a5)
Current Store : [0x80000278] : sw a7, 4(a5) -- Store: [0x80002374]:0x00000001




Last Coverpoint : ['rd : x12', 'rs1 : f9', 'fs1 == 0 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000284]:fcvt.w.s a2, fs1, dyn
	-[0x80000288]:csrrs a7, fflags, zero
	-[0x8000028c]:sw a2, 8(a5)
Current Store : [0x80000290] : sw a7, 12(a5) -- Store: [0x8000237c]:0x00000001




Last Coverpoint : ['rd : x10', 'rs1 : f14', 'fs1 == 0 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000029c]:fcvt.w.s a0, fa4, dyn
	-[0x800002a0]:csrrs a7, fflags, zero
	-[0x800002a4]:sw a0, 16(a5)
Current Store : [0x800002a8] : sw a7, 20(a5) -- Store: [0x80002384]:0x00000001




Last Coverpoint : ['rd : x21', 'rs1 : f4', 'fs1 == 0 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800002b4]:fcvt.w.s s5, ft4, dyn
	-[0x800002b8]:csrrs a7, fflags, zero
	-[0x800002bc]:sw s5, 24(a5)
Current Store : [0x800002c0] : sw a7, 28(a5) -- Store: [0x8000238c]:0x00000001




Last Coverpoint : ['rd : x16', 'rs1 : f11', 'fs1 == 0 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800002d8]:fcvt.w.s a6, fa1, dyn
	-[0x800002dc]:csrrs s5, fflags, zero
	-[0x800002e0]:sw a6, 0(s3)
Current Store : [0x800002e4] : sw s5, 4(s3) -- Store: [0x80002394]:0x00000001




Last Coverpoint : ['rd : x20', 'rs1 : f12', 'fs1 == 0 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800002fc]:fcvt.w.s s4, fa2, dyn
	-[0x80000300]:csrrs a7, fflags, zero
	-[0x80000304]:sw s4, 0(a5)
Current Store : [0x80000308] : sw a7, 4(a5) -- Store: [0x8000239c]:0x00000001




Last Coverpoint : ['rd : x13', 'rs1 : f2', 'fs1 == 0 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000314]:fcvt.w.s a3, ft2, dyn
	-[0x80000318]:csrrs a7, fflags, zero
	-[0x8000031c]:sw a3, 8(a5)
Current Store : [0x80000320] : sw a7, 12(a5) -- Store: [0x800023a4]:0x00000001




Last Coverpoint : ['rd : x31', 'rs1 : f15', 'fs1 == 0 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000032c]:fcvt.w.s t6, fa5, dyn
	-[0x80000330]:csrrs a7, fflags, zero
	-[0x80000334]:sw t6, 16(a5)
Current Store : [0x80000338] : sw a7, 20(a5) -- Store: [0x800023ac]:0x00000001




Last Coverpoint : ['rd : x29', 'rs1 : f3', 'fs1 == 0 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000344]:fcvt.w.s t4, ft3, dyn
	-[0x80000348]:csrrs a7, fflags, zero
	-[0x8000034c]:sw t4, 24(a5)
Current Store : [0x80000350] : sw a7, 28(a5) -- Store: [0x800023b4]:0x00000001




Last Coverpoint : ['rd : x1', 'rs1 : f23', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x8000035c]:fcvt.w.s ra, fs7, dyn
	-[0x80000360]:csrrs a7, fflags, zero
	-[0x80000364]:sw ra, 32(a5)
Current Store : [0x80000368] : sw a7, 36(a5) -- Store: [0x800023bc]:0x00000001




Last Coverpoint : ['rd : x0', 'rs1 : f30', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000374]:fcvt.w.s zero, ft10, dyn
	-[0x80000378]:csrrs a7, fflags, zero
	-[0x8000037c]:sw zero, 40(a5)
Current Store : [0x80000380] : sw a7, 44(a5) -- Store: [0x800023c4]:0x00000001




Last Coverpoint : ['rd : x9', 'rs1 : f7', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000038c]:fcvt.w.s s1, ft7, dyn
	-[0x80000390]:csrrs a7, fflags, zero
	-[0x80000394]:sw s1, 48(a5)
Current Store : [0x80000398] : sw a7, 52(a5) -- Store: [0x800023cc]:0x00000001




Last Coverpoint : ['rd : x11', 'rs1 : f13', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x800003a4]:fcvt.w.s a1, fa3, dyn
	-[0x800003a8]:csrrs a7, fflags, zero
	-[0x800003ac]:sw a1, 56(a5)
Current Store : [0x800003b0] : sw a7, 60(a5) -- Store: [0x800023d4]:0x00000001




Last Coverpoint : ['rd : x23', 'rs1 : f22', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003bc]:fcvt.w.s s7, fs6, dyn
	-[0x800003c0]:csrrs a7, fflags, zero
	-[0x800003c4]:sw s7, 64(a5)
Current Store : [0x800003c8] : sw a7, 68(a5) -- Store: [0x800023dc]:0x00000001




Last Coverpoint : ['rd : x24', 'rs1 : f0', 'fs1 == 1 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800003d4]:fcvt.w.s s8, ft0, dyn
	-[0x800003d8]:csrrs a7, fflags, zero
	-[0x800003dc]:sw s8, 72(a5)
Current Store : [0x800003e0] : sw a7, 76(a5) -- Store: [0x800023e4]:0x00000001




Last Coverpoint : ['rd : x18', 'rs1 : f17', 'fs1 == 1 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800003ec]:fcvt.w.s s2, fa7, dyn
	-[0x800003f0]:csrrs a7, fflags, zero
	-[0x800003f4]:sw s2, 80(a5)
Current Store : [0x800003f8] : sw a7, 84(a5) -- Store: [0x800023ec]:0x00000001




Last Coverpoint : ['rd : x8', 'rs1 : f16', 'fs1 == 1 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000404]:fcvt.w.s fp, fa6, dyn
	-[0x80000408]:csrrs a7, fflags, zero
	-[0x8000040c]:sw fp, 88(a5)
Current Store : [0x80000410] : sw a7, 92(a5) -- Store: [0x800023f4]:0x00000001




Last Coverpoint : ['rd : x26', 'rs1 : f29', 'fs1 == 1 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000041c]:fcvt.w.s s10, ft9, dyn
	-[0x80000420]:csrrs a7, fflags, zero
	-[0x80000424]:sw s10, 96(a5)
Current Store : [0x80000428] : sw a7, 100(a5) -- Store: [0x800023fc]:0x00000001




Last Coverpoint : ['rd : x30', 'rs1 : f1', 'fs1 == 1 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000434]:fcvt.w.s t5, ft1, dyn
	-[0x80000438]:csrrs a7, fflags, zero
	-[0x8000043c]:sw t5, 104(a5)
Current Store : [0x80000440] : sw a7, 108(a5) -- Store: [0x80002404]:0x00000001




Last Coverpoint : ['rd : x4', 'rs1 : f21', 'fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x8000044c]:fcvt.w.s tp, fs5, dyn
	-[0x80000450]:csrrs a7, fflags, zero
	-[0x80000454]:sw tp, 112(a5)
Current Store : [0x80000458] : sw a7, 116(a5) -- Store: [0x8000240c]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000464]:fcvt.w.s t6, ft11, dyn
	-[0x80000468]:csrrs a7, fflags, zero
	-[0x8000046c]:sw t6, 120(a5)
Current Store : [0x80000470] : sw a7, 124(a5) -- Store: [0x80002414]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000047c]:fcvt.w.s t6, ft11, dyn
	-[0x80000480]:csrrs a7, fflags, zero
	-[0x80000484]:sw t6, 128(a5)
Current Store : [0x80000488] : sw a7, 132(a5) -- Store: [0x8000241c]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000494]:fcvt.w.s t6, ft11, dyn
	-[0x80000498]:csrrs a7, fflags, zero
	-[0x8000049c]:sw t6, 136(a5)
Current Store : [0x800004a0] : sw a7, 140(a5) -- Store: [0x80002424]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004ac]:fcvt.w.s t6, ft11, dyn
	-[0x800004b0]:csrrs a7, fflags, zero
	-[0x800004b4]:sw t6, 144(a5)
Current Store : [0x800004b8] : sw a7, 148(a5) -- Store: [0x8000242c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800004c4]:fcvt.w.s t6, ft11, dyn
	-[0x800004c8]:csrrs a7, fflags, zero
	-[0x800004cc]:sw t6, 152(a5)
Current Store : [0x800004d0] : sw a7, 156(a5) -- Store: [0x80002434]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800004dc]:fcvt.w.s t6, ft11, dyn
	-[0x800004e0]:csrrs a7, fflags, zero
	-[0x800004e4]:sw t6, 160(a5)
Current Store : [0x800004e8] : sw a7, 164(a5) -- Store: [0x8000243c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800004f4]:fcvt.w.s t6, ft11, dyn
	-[0x800004f8]:csrrs a7, fflags, zero
	-[0x800004fc]:sw t6, 168(a5)
Current Store : [0x80000500] : sw a7, 172(a5) -- Store: [0x80002444]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000050c]:fcvt.w.s t6, ft11, dyn
	-[0x80000510]:csrrs a7, fflags, zero
	-[0x80000514]:sw t6, 176(a5)
Current Store : [0x80000518] : sw a7, 180(a5) -- Store: [0x8000244c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000524]:fcvt.w.s t6, ft11, dyn
	-[0x80000528]:csrrs a7, fflags, zero
	-[0x8000052c]:sw t6, 184(a5)
Current Store : [0x80000530] : sw a7, 188(a5) -- Store: [0x80002454]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x8000053c]:fcvt.w.s t6, ft11, dyn
	-[0x80000540]:csrrs a7, fflags, zero
	-[0x80000544]:sw t6, 192(a5)
Current Store : [0x80000548] : sw a7, 196(a5) -- Store: [0x8000245c]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000554]:fcvt.w.s t6, ft11, dyn
	-[0x80000558]:csrrs a7, fflags, zero
	-[0x8000055c]:sw t6, 200(a5)
Current Store : [0x80000560] : sw a7, 204(a5) -- Store: [0x80002464]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000056c]:fcvt.w.s t6, ft11, dyn
	-[0x80000570]:csrrs a7, fflags, zero
	-[0x80000574]:sw t6, 208(a5)
Current Store : [0x80000578] : sw a7, 212(a5) -- Store: [0x8000246c]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000584]:fcvt.w.s t6, ft11, dyn
	-[0x80000588]:csrrs a7, fflags, zero
	-[0x8000058c]:sw t6, 216(a5)
Current Store : [0x80000590] : sw a7, 220(a5) -- Store: [0x80002474]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x8000059c]:fcvt.w.s t6, ft11, dyn
	-[0x800005a0]:csrrs a7, fflags, zero
	-[0x800005a4]:sw t6, 224(a5)
Current Store : [0x800005a8] : sw a7, 228(a5) -- Store: [0x8000247c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800005b4]:fcvt.w.s t6, ft11, dyn
	-[0x800005b8]:csrrs a7, fflags, zero
	-[0x800005bc]:sw t6, 232(a5)
Current Store : [0x800005c0] : sw a7, 236(a5) -- Store: [0x80002484]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800005cc]:fcvt.w.s t6, ft11, dyn
	-[0x800005d0]:csrrs a7, fflags, zero
	-[0x800005d4]:sw t6, 240(a5)
Current Store : [0x800005d8] : sw a7, 244(a5) -- Store: [0x8000248c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x800005e4]:fcvt.w.s t6, ft11, dyn
	-[0x800005e8]:csrrs a7, fflags, zero
	-[0x800005ec]:sw t6, 248(a5)
Current Store : [0x800005f0] : sw a7, 252(a5) -- Store: [0x80002494]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800005fc]:fcvt.w.s t6, ft11, dyn
	-[0x80000600]:csrrs a7, fflags, zero
	-[0x80000604]:sw t6, 256(a5)
Current Store : [0x80000608] : sw a7, 260(a5) -- Store: [0x8000249c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000614]:fcvt.w.s t6, ft11, dyn
	-[0x80000618]:csrrs a7, fflags, zero
	-[0x8000061c]:sw t6, 264(a5)
Current Store : [0x80000620] : sw a7, 268(a5) -- Store: [0x800024a4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x8000062c]:fcvt.w.s t6, ft11, dyn
	-[0x80000630]:csrrs a7, fflags, zero
	-[0x80000634]:sw t6, 272(a5)
Current Store : [0x80000638] : sw a7, 276(a5) -- Store: [0x800024ac]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000644]:fcvt.w.s t6, ft11, dyn
	-[0x80000648]:csrrs a7, fflags, zero
	-[0x8000064c]:sw t6, 280(a5)
Current Store : [0x80000650] : sw a7, 284(a5) -- Store: [0x800024b4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000065c]:fcvt.w.s t6, ft11, dyn
	-[0x80000660]:csrrs a7, fflags, zero
	-[0x80000664]:sw t6, 288(a5)
Current Store : [0x80000668] : sw a7, 292(a5) -- Store: [0x800024bc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000674]:fcvt.w.s t6, ft11, dyn
	-[0x80000678]:csrrs a7, fflags, zero
	-[0x8000067c]:sw t6, 296(a5)
Current Store : [0x80000680] : sw a7, 300(a5) -- Store: [0x800024c4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x8000068c]:fcvt.w.s t6, ft11, dyn
	-[0x80000690]:csrrs a7, fflags, zero
	-[0x80000694]:sw t6, 304(a5)
Current Store : [0x80000698] : sw a7, 308(a5) -- Store: [0x800024cc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800006a4]:fcvt.w.s t6, ft11, dyn
	-[0x800006a8]:csrrs a7, fflags, zero
	-[0x800006ac]:sw t6, 312(a5)
Current Store : [0x800006b0] : sw a7, 316(a5) -- Store: [0x800024d4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800006bc]:fcvt.w.s t6, ft11, dyn
	-[0x800006c0]:csrrs a7, fflags, zero
	-[0x800006c4]:sw t6, 320(a5)
Current Store : [0x800006c8] : sw a7, 324(a5) -- Store: [0x800024dc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x800006d4]:fcvt.w.s t6, ft11, dyn
	-[0x800006d8]:csrrs a7, fflags, zero
	-[0x800006dc]:sw t6, 328(a5)
Current Store : [0x800006e0] : sw a7, 332(a5) -- Store: [0x800024e4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800006ec]:fcvt.w.s t6, ft11, dyn
	-[0x800006f0]:csrrs a7, fflags, zero
	-[0x800006f4]:sw t6, 336(a5)
Current Store : [0x800006f8] : sw a7, 340(a5) -- Store: [0x800024ec]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000704]:fcvt.w.s t6, ft11, dyn
	-[0x80000708]:csrrs a7, fflags, zero
	-[0x8000070c]:sw t6, 344(a5)
Current Store : [0x80000710] : sw a7, 348(a5) -- Store: [0x800024f4]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x8000071c]:fcvt.w.s t6, ft11, dyn
	-[0x80000720]:csrrs a7, fflags, zero
	-[0x80000724]:sw t6, 352(a5)
Current Store : [0x80000728] : sw a7, 356(a5) -- Store: [0x800024fc]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000734]:fcvt.w.s t6, ft11, dyn
	-[0x80000738]:csrrs a7, fflags, zero
	-[0x8000073c]:sw t6, 360(a5)
Current Store : [0x80000740] : sw a7, 364(a5) -- Store: [0x80002504]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000074c]:fcvt.w.s t6, ft11, dyn
	-[0x80000750]:csrrs a7, fflags, zero
	-[0x80000754]:sw t6, 368(a5)
Current Store : [0x80000758] : sw a7, 372(a5) -- Store: [0x8000250c]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000764]:fcvt.w.s t6, ft11, dyn
	-[0x80000768]:csrrs a7, fflags, zero
	-[0x8000076c]:sw t6, 376(a5)
Current Store : [0x80000770] : sw a7, 380(a5) -- Store: [0x80002514]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x8000077c]:fcvt.w.s t6, ft11, dyn
	-[0x80000780]:csrrs a7, fflags, zero
	-[0x80000784]:sw t6, 384(a5)
Current Store : [0x80000788] : sw a7, 388(a5) -- Store: [0x8000251c]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000794]:fcvt.w.s t6, ft11, dyn
	-[0x80000798]:csrrs a7, fflags, zero
	-[0x8000079c]:sw t6, 392(a5)
Current Store : [0x800007a0] : sw a7, 396(a5) -- Store: [0x80002524]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800007ac]:fcvt.w.s t6, ft11, dyn
	-[0x800007b0]:csrrs a7, fflags, zero
	-[0x800007b4]:sw t6, 400(a5)
Current Store : [0x800007b8] : sw a7, 404(a5) -- Store: [0x8000252c]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x800007c4]:fcvt.w.s t6, ft11, dyn
	-[0x800007c8]:csrrs a7, fflags, zero
	-[0x800007cc]:sw t6, 408(a5)
Current Store : [0x800007d0] : sw a7, 412(a5) -- Store: [0x80002534]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800007dc]:fcvt.w.s t6, ft11, dyn
	-[0x800007e0]:csrrs a7, fflags, zero
	-[0x800007e4]:sw t6, 416(a5)
Current Store : [0x800007e8] : sw a7, 420(a5) -- Store: [0x8000253c]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800007f4]:fcvt.w.s t6, ft11, dyn
	-[0x800007f8]:csrrs a7, fflags, zero
	-[0x800007fc]:sw t6, 424(a5)
Current Store : [0x80000800] : sw a7, 428(a5) -- Store: [0x80002544]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x8000080c]:fcvt.w.s t6, ft11, dyn
	-[0x80000810]:csrrs a7, fflags, zero
	-[0x80000814]:sw t6, 432(a5)
Current Store : [0x80000818] : sw a7, 436(a5) -- Store: [0x8000254c]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000824]:fcvt.w.s t6, ft11, dyn
	-[0x80000828]:csrrs a7, fflags, zero
	-[0x8000082c]:sw t6, 440(a5)
Current Store : [0x80000830] : sw a7, 444(a5) -- Store: [0x80002554]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000083c]:fcvt.w.s t6, ft11, dyn
	-[0x80000840]:csrrs a7, fflags, zero
	-[0x80000844]:sw t6, 448(a5)
Current Store : [0x80000848] : sw a7, 452(a5) -- Store: [0x8000255c]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000854]:fcvt.w.s t6, ft11, dyn
	-[0x80000858]:csrrs a7, fflags, zero
	-[0x8000085c]:sw t6, 456(a5)
Current Store : [0x80000860] : sw a7, 460(a5) -- Store: [0x80002564]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x8000086c]:fcvt.w.s t6, ft11, dyn
	-[0x80000870]:csrrs a7, fflags, zero
	-[0x80000874]:sw t6, 464(a5)
Current Store : [0x80000878] : sw a7, 468(a5) -- Store: [0x8000256c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000884]:fcvt.w.s t6, ft11, dyn
	-[0x80000888]:csrrs a7, fflags, zero
	-[0x8000088c]:sw t6, 472(a5)
Current Store : [0x80000890] : sw a7, 476(a5) -- Store: [0x80002574]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000089c]:fcvt.w.s t6, ft11, dyn
	-[0x800008a0]:csrrs a7, fflags, zero
	-[0x800008a4]:sw t6, 480(a5)
Current Store : [0x800008a8] : sw a7, 484(a5) -- Store: [0x8000257c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x800008b4]:fcvt.w.s t6, ft11, dyn
	-[0x800008b8]:csrrs a7, fflags, zero
	-[0x800008bc]:sw t6, 488(a5)
Current Store : [0x800008c0] : sw a7, 492(a5) -- Store: [0x80002584]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800008cc]:fcvt.w.s t6, ft11, dyn
	-[0x800008d0]:csrrs a7, fflags, zero
	-[0x800008d4]:sw t6, 496(a5)
Current Store : [0x800008d8] : sw a7, 500(a5) -- Store: [0x8000258c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800008e4]:fcvt.w.s t6, ft11, dyn
	-[0x800008e8]:csrrs a7, fflags, zero
	-[0x800008ec]:sw t6, 504(a5)
Current Store : [0x800008f0] : sw a7, 508(a5) -- Store: [0x80002594]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800008fc]:fcvt.w.s t6, ft11, dyn
	-[0x80000900]:csrrs a7, fflags, zero
	-[0x80000904]:sw t6, 512(a5)
Current Store : [0x80000908] : sw a7, 516(a5) -- Store: [0x8000259c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000914]:fcvt.w.s t6, ft11, dyn
	-[0x80000918]:csrrs a7, fflags, zero
	-[0x8000091c]:sw t6, 520(a5)
Current Store : [0x80000920] : sw a7, 524(a5) -- Store: [0x800025a4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000092c]:fcvt.w.s t6, ft11, dyn
	-[0x80000930]:csrrs a7, fflags, zero
	-[0x80000934]:sw t6, 528(a5)
Current Store : [0x80000938] : sw a7, 532(a5) -- Store: [0x800025ac]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000944]:fcvt.w.s t6, ft11, dyn
	-[0x80000948]:csrrs a7, fflags, zero
	-[0x8000094c]:sw t6, 536(a5)
Current Store : [0x80000950] : sw a7, 540(a5) -- Store: [0x800025b4]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x8000095c]:fcvt.w.s t6, ft11, dyn
	-[0x80000960]:csrrs a7, fflags, zero
	-[0x80000964]:sw t6, 544(a5)
Current Store : [0x80000968] : sw a7, 548(a5) -- Store: [0x800025bc]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000974]:fcvt.w.s t6, ft11, dyn
	-[0x80000978]:csrrs a7, fflags, zero
	-[0x8000097c]:sw t6, 552(a5)
Current Store : [0x80000980] : sw a7, 556(a5) -- Store: [0x800025c4]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000098c]:fcvt.w.s t6, ft11, dyn
	-[0x80000990]:csrrs a7, fflags, zero
	-[0x80000994]:sw t6, 560(a5)
Current Store : [0x80000998] : sw a7, 564(a5) -- Store: [0x800025cc]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x800009a4]:fcvt.w.s t6, ft11, dyn
	-[0x800009a8]:csrrs a7, fflags, zero
	-[0x800009ac]:sw t6, 568(a5)
Current Store : [0x800009b0] : sw a7, 572(a5) -- Store: [0x800025d4]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800009bc]:fcvt.w.s t6, ft11, dyn
	-[0x800009c0]:csrrs a7, fflags, zero
	-[0x800009c4]:sw t6, 576(a5)
Current Store : [0x800009c8] : sw a7, 580(a5) -- Store: [0x800025dc]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800009d4]:fcvt.w.s t6, ft11, dyn
	-[0x800009d8]:csrrs a7, fflags, zero
	-[0x800009dc]:sw t6, 584(a5)
Current Store : [0x800009e0] : sw a7, 588(a5) -- Store: [0x800025e4]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800009ec]:fcvt.w.s t6, ft11, dyn
	-[0x800009f0]:csrrs a7, fflags, zero
	-[0x800009f4]:sw t6, 592(a5)
Current Store : [0x800009f8] : sw a7, 596(a5) -- Store: [0x800025ec]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000a04]:fcvt.w.s t6, ft11, dyn
	-[0x80000a08]:csrrs a7, fflags, zero
	-[0x80000a0c]:sw t6, 600(a5)
Current Store : [0x80000a10] : sw a7, 604(a5) -- Store: [0x800025f4]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000a1c]:fcvt.w.s t6, ft11, dyn
	-[0x80000a20]:csrrs a7, fflags, zero
	-[0x80000a24]:sw t6, 608(a5)
Current Store : [0x80000a28] : sw a7, 612(a5) -- Store: [0x800025fc]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000a34]:fcvt.w.s t6, ft11, dyn
	-[0x80000a38]:csrrs a7, fflags, zero
	-[0x80000a3c]:sw t6, 616(a5)
Current Store : [0x80000a40] : sw a7, 620(a5) -- Store: [0x80002604]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000a4c]:fcvt.w.s t6, ft11, dyn
	-[0x80000a50]:csrrs a7, fflags, zero
	-[0x80000a54]:sw t6, 624(a5)
Current Store : [0x80000a58] : sw a7, 628(a5) -- Store: [0x8000260c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000a64]:fcvt.w.s t6, ft11, dyn
	-[0x80000a68]:csrrs a7, fflags, zero
	-[0x80000a6c]:sw t6, 632(a5)
Current Store : [0x80000a70] : sw a7, 636(a5) -- Store: [0x80002614]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000a7c]:fcvt.w.s t6, ft11, dyn
	-[0x80000a80]:csrrs a7, fflags, zero
	-[0x80000a84]:sw t6, 640(a5)
Current Store : [0x80000a88] : sw a7, 644(a5) -- Store: [0x8000261c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000a94]:fcvt.w.s t6, ft11, dyn
	-[0x80000a98]:csrrs a7, fflags, zero
	-[0x80000a9c]:sw t6, 648(a5)
Current Store : [0x80000aa0] : sw a7, 652(a5) -- Store: [0x80002624]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000aac]:fcvt.w.s t6, ft11, dyn
	-[0x80000ab0]:csrrs a7, fflags, zero
	-[0x80000ab4]:sw t6, 656(a5)
Current Store : [0x80000ab8] : sw a7, 660(a5) -- Store: [0x8000262c]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000ac4]:fcvt.w.s t6, ft11, dyn
	-[0x80000ac8]:csrrs a7, fflags, zero
	-[0x80000acc]:sw t6, 664(a5)
Current Store : [0x80000ad0] : sw a7, 668(a5) -- Store: [0x80002634]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000adc]:fcvt.w.s t6, ft11, dyn
	-[0x80000ae0]:csrrs a7, fflags, zero
	-[0x80000ae4]:sw t6, 672(a5)
Current Store : [0x80000ae8] : sw a7, 676(a5) -- Store: [0x8000263c]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000af4]:fcvt.w.s t6, ft11, dyn
	-[0x80000af8]:csrrs a7, fflags, zero
	-[0x80000afc]:sw t6, 680(a5)
Current Store : [0x80000b00] : sw a7, 684(a5) -- Store: [0x80002644]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000b0c]:fcvt.w.s t6, ft11, dyn
	-[0x80000b10]:csrrs a7, fflags, zero
	-[0x80000b14]:sw t6, 688(a5)
Current Store : [0x80000b18] : sw a7, 692(a5) -- Store: [0x8000264c]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000b24]:fcvt.w.s t6, ft11, dyn
	-[0x80000b28]:csrrs a7, fflags, zero
	-[0x80000b2c]:sw t6, 696(a5)
Current Store : [0x80000b30] : sw a7, 700(a5) -- Store: [0x80002654]:0x00000001




Last Coverpoint : ['opcode : fcvt.w.s', 'rd : x31', 'rs1 : f31', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000b3c]:fcvt.w.s t6, ft11, dyn
	-[0x80000b40]:csrrs a7, fflags, zero
	-[0x80000b44]:sw t6, 704(a5)
Current Store : [0x80000b48] : sw a7, 708(a5) -- Store: [0x8000265c]:0x00000001





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                           coverpoints                                                           |                                                       code                                                        |
|---:|--------------------------|---------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002310]<br>0x00000000|- opcode : fcvt.w.s<br> - rd : x6<br> - rs1 : f24<br> - fs1 == 1 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 0  #nosat<br> |[0x8000011c]:fcvt.w.s t1, fs8, dyn<br> [0x80000120]:csrrs a7, fflags, zero<br> [0x80000124]:sw t1, 0(a5)<br>       |
|   2|[0x80002318]<br>0x00000000|- rd : x27<br> - rs1 : f25<br> - fs1 == 0 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 4  #nosat<br>                        |[0x80000134]:fcvt.w.s s11, fs9, dyn<br> [0x80000138]:csrrs a7, fflags, zero<br> [0x8000013c]:sw s11, 8(a5)<br>     |
|   3|[0x80002320]<br>0x00000001|- rd : x15<br> - rs1 : f5<br> - fs1 == 0 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 3  #nosat<br>                         |[0x80000158]:fcvt.w.s a5, ft5, dyn<br> [0x8000015c]:csrrs s5, fflags, zero<br> [0x80000160]:sw a5, 0(s3)<br>       |
|   4|[0x80002328]<br>0x00000000|- rd : x19<br> - rs1 : f18<br> - fs1 == 0 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 2  #nosat<br>                        |[0x8000017c]:fcvt.w.s s3, fs2, dyn<br> [0x80000180]:csrrs a7, fflags, zero<br> [0x80000184]:sw s3, 0(a5)<br>       |
|   5|[0x80002330]<br>0x00000000|- rd : x3<br> - rs1 : f8<br> - fs1 == 0 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 1  #nosat<br>                          |[0x80000194]:fcvt.w.s gp, fs0, dyn<br> [0x80000198]:csrrs a7, fflags, zero<br> [0x8000019c]:sw gp, 8(a5)<br>       |
|   6|[0x80002338]<br>0x00000000|- rd : x22<br> - rs1 : f27<br> - fs1 == 0 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 0  #nosat<br>                        |[0x800001ac]:fcvt.w.s s6, fs11, dyn<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:sw s6, 16(a5)<br>     |
|   7|[0x80002340]<br>0x00000000|- rd : x7<br> - rs1 : f10<br> - fs1 == 1 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 4  #nosat<br>                         |[0x800001c4]:fcvt.w.s t2, fa0, dyn<br> [0x800001c8]:csrrs a7, fflags, zero<br> [0x800001cc]:sw t2, 24(a5)<br>      |
|   8|[0x80002348]<br>0x00000000|- rd : x28<br> - rs1 : f6<br> - fs1 == 1 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 3  #nosat<br>                         |[0x800001dc]:fcvt.w.s t3, ft6, dyn<br> [0x800001e0]:csrrs a7, fflags, zero<br> [0x800001e4]:sw t3, 32(a5)<br>      |
|   9|[0x80002350]<br>0xFFFFFFFF|- rd : x5<br> - rs1 : f28<br> - fs1 == 1 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 2  #nosat<br>                         |[0x800001f4]:fcvt.w.s t0, ft8, dyn<br> [0x800001f8]:csrrs a7, fflags, zero<br> [0x800001fc]:sw t0, 40(a5)<br>      |
|  10|[0x80002358]<br>0x00000000|- rd : x14<br> - rs1 : f31<br> - fs1 == 1 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 1  #nosat<br>                        |[0x8000020c]:fcvt.w.s a4, ft11, dyn<br> [0x80000210]:csrrs a7, fflags, zero<br> [0x80000214]:sw a4, 48(a5)<br>     |
|  11|[0x80002360]<br>0x00000000|- rd : x2<br> - rs1 : f26<br> - fs1 == 1 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 0  #nosat<br>                         |[0x80000224]:fcvt.w.s sp, fs10, dyn<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:sw sp, 56(a5)<br>     |
|  12|[0x80002368]<br>0x00000001|- rd : x17<br> - rs1 : f20<br> - fs1 == 0 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 4  #nosat<br>                        |[0x80000248]:fcvt.w.s a7, fs4, dyn<br> [0x8000024c]:csrrs s5, fflags, zero<br> [0x80000250]:sw a7, 0(s3)<br>       |
|  13|[0x80002370]<br>0x00000002|- rd : x25<br> - rs1 : f19<br> - fs1 == 0 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 3  #nosat<br>                        |[0x8000026c]:fcvt.w.s s9, fs3, dyn<br> [0x80000270]:csrrs a7, fflags, zero<br> [0x80000274]:sw s9, 0(a5)<br>       |
|  14|[0x80002378]<br>0x00000001|- rd : x12<br> - rs1 : f9<br> - fs1 == 0 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 2  #nosat<br>                         |[0x80000284]:fcvt.w.s a2, fs1, dyn<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:sw a2, 8(a5)<br>       |
|  15|[0x80002380]<br>0x00000001|- rd : x10<br> - rs1 : f14<br> - fs1 == 0 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 1  #nosat<br>                        |[0x8000029c]:fcvt.w.s a0, fa4, dyn<br> [0x800002a0]:csrrs a7, fflags, zero<br> [0x800002a4]:sw a0, 16(a5)<br>      |
|  16|[0x80002388]<br>0x00000001|- rd : x21<br> - rs1 : f4<br> - fs1 == 0 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 0  #nosat<br>                         |[0x800002b4]:fcvt.w.s s5, ft4, dyn<br> [0x800002b8]:csrrs a7, fflags, zero<br> [0x800002bc]:sw s5, 24(a5)<br>      |
|  17|[0x80002390]<br>0x00000001|- rd : x16<br> - rs1 : f11<br> - fs1 == 0 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 4  #nosat<br>                        |[0x800002d8]:fcvt.w.s a6, fa1, dyn<br> [0x800002dc]:csrrs s5, fflags, zero<br> [0x800002e0]:sw a6, 0(s3)<br>       |
|  18|[0x80002398]<br>0x00000001|- rd : x20<br> - rs1 : f12<br> - fs1 == 0 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 3  #nosat<br>                        |[0x800002fc]:fcvt.w.s s4, fa2, dyn<br> [0x80000300]:csrrs a7, fflags, zero<br> [0x80000304]:sw s4, 0(a5)<br>       |
|  19|[0x800023a0]<br>0x00000000|- rd : x13<br> - rs1 : f2<br> - fs1 == 0 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 2  #nosat<br>                         |[0x80000314]:fcvt.w.s a3, ft2, dyn<br> [0x80000318]:csrrs a7, fflags, zero<br> [0x8000031c]:sw a3, 8(a5)<br>       |
|  20|[0x800023a8]<br>0x00000000|- rd : x31<br> - rs1 : f15<br> - fs1 == 0 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 1  #nosat<br>                        |[0x8000032c]:fcvt.w.s t6, fa5, dyn<br> [0x80000330]:csrrs a7, fflags, zero<br> [0x80000334]:sw t6, 16(a5)<br>      |
|  21|[0x800023b0]<br>0x00000001|- rd : x29<br> - rs1 : f3<br> - fs1 == 0 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 0  #nosat<br>                         |[0x80000344]:fcvt.w.s t4, ft3, dyn<br> [0x80000348]:csrrs a7, fflags, zero<br> [0x8000034c]:sw t4, 24(a5)<br>      |
|  22|[0x800023b8]<br>0x00000000|- rd : x1<br> - rs1 : f23<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and rm_val == 4  #nosat<br>                         |[0x8000035c]:fcvt.w.s ra, fs7, dyn<br> [0x80000360]:csrrs a7, fflags, zero<br> [0x80000364]:sw ra, 32(a5)<br>      |
|  23|[0x800023c0]<br>0x00000000|- rd : x0<br> - rs1 : f30<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and rm_val == 3  #nosat<br>                         |[0x80000374]:fcvt.w.s zero, ft10, dyn<br> [0x80000378]:csrrs a7, fflags, zero<br> [0x8000037c]:sw zero, 40(a5)<br> |
|  24|[0x800023c8]<br>0x00000000|- rd : x9<br> - rs1 : f7<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and rm_val == 2  #nosat<br>                          |[0x8000038c]:fcvt.w.s s1, ft7, dyn<br> [0x80000390]:csrrs a7, fflags, zero<br> [0x80000394]:sw s1, 48(a5)<br>      |
|  25|[0x800023d0]<br>0x00000000|- rd : x11<br> - rs1 : f13<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and rm_val == 1  #nosat<br>                        |[0x800003a4]:fcvt.w.s a1, fa3, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:sw a1, 56(a5)<br>      |
|  26|[0x800023d8]<br>0x00000000|- rd : x23<br> - rs1 : f22<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and rm_val == 0  #nosat<br>                        |[0x800003bc]:fcvt.w.s s7, fs6, dyn<br> [0x800003c0]:csrrs a7, fflags, zero<br> [0x800003c4]:sw s7, 64(a5)<br>      |
|  27|[0x800023e0]<br>0xFFFFFFFF|- rd : x24<br> - rs1 : f0<br> - fs1 == 1 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 4  #nosat<br>                         |[0x800003d4]:fcvt.w.s s8, ft0, dyn<br> [0x800003d8]:csrrs a7, fflags, zero<br> [0x800003dc]:sw s8, 72(a5)<br>      |
|  28|[0x800023e8]<br>0x00000000|- rd : x18<br> - rs1 : f17<br> - fs1 == 1 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 3  #nosat<br>                        |[0x800003ec]:fcvt.w.s s2, fa7, dyn<br> [0x800003f0]:csrrs a7, fflags, zero<br> [0x800003f4]:sw s2, 80(a5)<br>      |
|  29|[0x800023f0]<br>0xFFFFFFFF|- rd : x8<br> - rs1 : f16<br> - fs1 == 1 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 2  #nosat<br>                         |[0x80000404]:fcvt.w.s fp, fa6, dyn<br> [0x80000408]:csrrs a7, fflags, zero<br> [0x8000040c]:sw fp, 88(a5)<br>      |
|  30|[0x800023f8]<br>0x00000000|- rd : x26<br> - rs1 : f29<br> - fs1 == 1 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 1  #nosat<br>                        |[0x8000041c]:fcvt.w.s s10, ft9, dyn<br> [0x80000420]:csrrs a7, fflags, zero<br> [0x80000424]:sw s10, 96(a5)<br>    |
|  31|[0x80002400]<br>0xFFFFFFFF|- rd : x30<br> - rs1 : f1<br> - fs1 == 1 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 0  #nosat<br>                         |[0x80000434]:fcvt.w.s t5, ft1, dyn<br> [0x80000438]:csrrs a7, fflags, zero<br> [0x8000043c]:sw t5, 104(a5)<br>     |
|  32|[0x80002408]<br>0xFFFFFFFF|- rd : x4<br> - rs1 : f21<br> - fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 4  #nosat<br>                         |[0x8000044c]:fcvt.w.s tp, fs5, dyn<br> [0x80000450]:csrrs a7, fflags, zero<br> [0x80000454]:sw tp, 112(a5)<br>     |
|  33|[0x80002410]<br>0xFFFFFFFF|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 3  #nosat<br>                                                       |[0x80000464]:fcvt.w.s t6, ft11, dyn<br> [0x80000468]:csrrs a7, fflags, zero<br> [0x8000046c]:sw t6, 120(a5)<br>    |
|  34|[0x80002418]<br>0xFFFFFFFF|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 2  #nosat<br>                                                       |[0x8000047c]:fcvt.w.s t6, ft11, dyn<br> [0x80000480]:csrrs a7, fflags, zero<br> [0x80000484]:sw t6, 128(a5)<br>    |
|  35|[0x80002420]<br>0xFFFFFFFF|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 1  #nosat<br>                                                       |[0x80000494]:fcvt.w.s t6, ft11, dyn<br> [0x80000498]:csrrs a7, fflags, zero<br> [0x8000049c]:sw t6, 136(a5)<br>    |
|  36|[0x80002428]<br>0xFFFFFFFF|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 0  #nosat<br>                                                       |[0x800004ac]:fcvt.w.s t6, ft11, dyn<br> [0x800004b0]:csrrs a7, fflags, zero<br> [0x800004b4]:sw t6, 144(a5)<br>    |
|  37|[0x80002430]<br>0x00000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 4  #nosat<br>                                                       |[0x800004c4]:fcvt.w.s t6, ft11, dyn<br> [0x800004c8]:csrrs a7, fflags, zero<br> [0x800004cc]:sw t6, 152(a5)<br>    |
|  38|[0x80002438]<br>0x00000002|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 3  #nosat<br>                                                       |[0x800004dc]:fcvt.w.s t6, ft11, dyn<br> [0x800004e0]:csrrs a7, fflags, zero<br> [0x800004e4]:sw t6, 160(a5)<br>    |
|  39|[0x80002440]<br>0x00000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 2  #nosat<br>                                                       |[0x800004f4]:fcvt.w.s t6, ft11, dyn<br> [0x800004f8]:csrrs a7, fflags, zero<br> [0x800004fc]:sw t6, 168(a5)<br>    |
|  40|[0x80002448]<br>0x00000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 1  #nosat<br>                                                       |[0x8000050c]:fcvt.w.s t6, ft11, dyn<br> [0x80000510]:csrrs a7, fflags, zero<br> [0x80000514]:sw t6, 176(a5)<br>    |
|  41|[0x80002450]<br>0x00000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 0  #nosat<br>                                                       |[0x80000524]:fcvt.w.s t6, ft11, dyn<br> [0x80000528]:csrrs a7, fflags, zero<br> [0x8000052c]:sw t6, 184(a5)<br>    |
|  42|[0x80002458]<br>0x00000000|- fs1 == 1 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 4  #nosat<br>                                                       |[0x8000053c]:fcvt.w.s t6, ft11, dyn<br> [0x80000540]:csrrs a7, fflags, zero<br> [0x80000544]:sw t6, 192(a5)<br>    |
|  43|[0x80002460]<br>0x00000000|- fs1 == 1 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 3  #nosat<br>                                                       |[0x80000554]:fcvt.w.s t6, ft11, dyn<br> [0x80000558]:csrrs a7, fflags, zero<br> [0x8000055c]:sw t6, 200(a5)<br>    |
|  44|[0x80002468]<br>0xFFFFFFFF|- fs1 == 1 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 2  #nosat<br>                                                       |[0x8000056c]:fcvt.w.s t6, ft11, dyn<br> [0x80000570]:csrrs a7, fflags, zero<br> [0x80000574]:sw t6, 208(a5)<br>    |
|  45|[0x80002470]<br>0x00000000|- fs1 == 1 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 1  #nosat<br>                                                       |[0x80000584]:fcvt.w.s t6, ft11, dyn<br> [0x80000588]:csrrs a7, fflags, zero<br> [0x8000058c]:sw t6, 216(a5)<br>    |
|  46|[0x80002478]<br>0x00000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 4  #nosat<br>                                                       |[0x8000059c]:fcvt.w.s t6, ft11, dyn<br> [0x800005a0]:csrrs a7, fflags, zero<br> [0x800005a4]:sw t6, 224(a5)<br>    |
|  47|[0x80002480]<br>0x00000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 3  #nosat<br>                                                       |[0x800005b4]:fcvt.w.s t6, ft11, dyn<br> [0x800005b8]:csrrs a7, fflags, zero<br> [0x800005bc]:sw t6, 232(a5)<br>    |
|  48|[0x80002488]<br>0x00000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 2  #nosat<br>                                                       |[0x800005cc]:fcvt.w.s t6, ft11, dyn<br> [0x800005d0]:csrrs a7, fflags, zero<br> [0x800005d4]:sw t6, 240(a5)<br>    |
|  49|[0x80002490]<br>0x00000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 1  #nosat<br>                                                       |[0x800005e4]:fcvt.w.s t6, ft11, dyn<br> [0x800005e8]:csrrs a7, fflags, zero<br> [0x800005ec]:sw t6, 248(a5)<br>    |
|  50|[0x80002498]<br>0x00000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 0  #nosat<br>                                                       |[0x800005fc]:fcvt.w.s t6, ft11, dyn<br> [0x80000600]:csrrs a7, fflags, zero<br> [0x80000604]:sw t6, 256(a5)<br>    |
|  51|[0x800024a0]<br>0x00000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 4  #nosat<br>                                                       |[0x80000614]:fcvt.w.s t6, ft11, dyn<br> [0x80000618]:csrrs a7, fflags, zero<br> [0x8000061c]:sw t6, 264(a5)<br>    |
|  52|[0x800024a8]<br>0x00000002|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 3  #nosat<br>                                                       |[0x8000062c]:fcvt.w.s t6, ft11, dyn<br> [0x80000630]:csrrs a7, fflags, zero<br> [0x80000634]:sw t6, 272(a5)<br>    |
|  53|[0x800024b0]<br>0x00000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 2  #nosat<br>                                                       |[0x80000644]:fcvt.w.s t6, ft11, dyn<br> [0x80000648]:csrrs a7, fflags, zero<br> [0x8000064c]:sw t6, 280(a5)<br>    |
|  54|[0x800024b8]<br>0x00000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 1  #nosat<br>                                                       |[0x8000065c]:fcvt.w.s t6, ft11, dyn<br> [0x80000660]:csrrs a7, fflags, zero<br> [0x80000664]:sw t6, 288(a5)<br>    |
|  55|[0x800024c0]<br>0x00000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 0  #nosat<br>                                                       |[0x80000674]:fcvt.w.s t6, ft11, dyn<br> [0x80000678]:csrrs a7, fflags, zero<br> [0x8000067c]:sw t6, 296(a5)<br>    |
|  56|[0x800024c8]<br>0x00000001|- fs1 == 0 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 4  #nosat<br>                                                       |[0x8000068c]:fcvt.w.s t6, ft11, dyn<br> [0x80000690]:csrrs a7, fflags, zero<br> [0x80000694]:sw t6, 304(a5)<br>    |
|  57|[0x800024d0]<br>0x00000001|- fs1 == 0 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 3  #nosat<br>                                                       |[0x800006a4]:fcvt.w.s t6, ft11, dyn<br> [0x800006a8]:csrrs a7, fflags, zero<br> [0x800006ac]:sw t6, 312(a5)<br>    |
|  58|[0x800024d8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 2  #nosat<br>                                                       |[0x800006bc]:fcvt.w.s t6, ft11, dyn<br> [0x800006c0]:csrrs a7, fflags, zero<br> [0x800006c4]:sw t6, 320(a5)<br>    |
|  59|[0x800024e0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 1  #nosat<br>                                                       |[0x800006d4]:fcvt.w.s t6, ft11, dyn<br> [0x800006d8]:csrrs a7, fflags, zero<br> [0x800006dc]:sw t6, 328(a5)<br>    |
|  60|[0x800024e8]<br>0x00000001|- fs1 == 0 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 0  #nosat<br>                                                       |[0x800006ec]:fcvt.w.s t6, ft11, dyn<br> [0x800006f0]:csrrs a7, fflags, zero<br> [0x800006f4]:sw t6, 336(a5)<br>    |
|  61|[0x800024f0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 4  #nosat<br>                                                       |[0x80000704]:fcvt.w.s t6, ft11, dyn<br> [0x80000708]:csrrs a7, fflags, zero<br> [0x8000070c]:sw t6, 344(a5)<br>    |
|  62|[0x800024f8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 3  #nosat<br>                                                       |[0x8000071c]:fcvt.w.s t6, ft11, dyn<br> [0x80000720]:csrrs a7, fflags, zero<br> [0x80000724]:sw t6, 352(a5)<br>    |
|  63|[0x80002500]<br>0xFFFFFFFF|- fs1 == 1 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 2  #nosat<br>                                                       |[0x80000734]:fcvt.w.s t6, ft11, dyn<br> [0x80000738]:csrrs a7, fflags, zero<br> [0x8000073c]:sw t6, 360(a5)<br>    |
|  64|[0x80002508]<br>0x00000000|- fs1 == 1 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 1  #nosat<br>                                                       |[0x8000074c]:fcvt.w.s t6, ft11, dyn<br> [0x80000750]:csrrs a7, fflags, zero<br> [0x80000754]:sw t6, 368(a5)<br>    |
|  65|[0x80002510]<br>0x00000000|- fs1 == 1 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 0  #nosat<br>                                                       |[0x80000764]:fcvt.w.s t6, ft11, dyn<br> [0x80000768]:csrrs a7, fflags, zero<br> [0x8000076c]:sw t6, 376(a5)<br>    |
|  66|[0x80002518]<br>0xFFFFFFFF|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 4  #nosat<br>                                                       |[0x8000077c]:fcvt.w.s t6, ft11, dyn<br> [0x80000780]:csrrs a7, fflags, zero<br> [0x80000784]:sw t6, 384(a5)<br>    |
|  67|[0x80002520]<br>0xFFFFFFFF|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 3  #nosat<br>                                                       |[0x80000794]:fcvt.w.s t6, ft11, dyn<br> [0x80000798]:csrrs a7, fflags, zero<br> [0x8000079c]:sw t6, 392(a5)<br>    |
|  68|[0x80002528]<br>0xFFFFFFFE|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 2  #nosat<br>                                                       |[0x800007ac]:fcvt.w.s t6, ft11, dyn<br> [0x800007b0]:csrrs a7, fflags, zero<br> [0x800007b4]:sw t6, 400(a5)<br>    |
|  69|[0x80002530]<br>0xFFFFFFFF|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 1  #nosat<br>                                                       |[0x800007c4]:fcvt.w.s t6, ft11, dyn<br> [0x800007c8]:csrrs a7, fflags, zero<br> [0x800007cc]:sw t6, 408(a5)<br>    |
|  70|[0x80002538]<br>0xFFFFFFFF|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 0  #nosat<br>                                                       |[0x800007dc]:fcvt.w.s t6, ft11, dyn<br> [0x800007e0]:csrrs a7, fflags, zero<br> [0x800007e4]:sw t6, 416(a5)<br>    |
|  71|[0x80002540]<br>0xFFFFFFFF|- fs1 == 1 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 4  #nosat<br>                                                       |[0x800007f4]:fcvt.w.s t6, ft11, dyn<br> [0x800007f8]:csrrs a7, fflags, zero<br> [0x800007fc]:sw t6, 424(a5)<br>    |
|  72|[0x80002548]<br>0x00000000|- fs1 == 1 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 3  #nosat<br>                                                       |[0x8000080c]:fcvt.w.s t6, ft11, dyn<br> [0x80000810]:csrrs a7, fflags, zero<br> [0x80000814]:sw t6, 432(a5)<br>    |
|  73|[0x80002550]<br>0xFFFFFFFF|- fs1 == 1 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 2  #nosat<br>                                                       |[0x80000824]:fcvt.w.s t6, ft11, dyn<br> [0x80000828]:csrrs a7, fflags, zero<br> [0x8000082c]:sw t6, 440(a5)<br>    |
|  74|[0x80002558]<br>0x00000000|- fs1 == 1 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 1  #nosat<br>                                                       |[0x8000083c]:fcvt.w.s t6, ft11, dyn<br> [0x80000840]:csrrs a7, fflags, zero<br> [0x80000844]:sw t6, 448(a5)<br>    |
|  75|[0x80002560]<br>0xFFFFFFFF|- fs1 == 1 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 0  #nosat<br>                                                       |[0x80000854]:fcvt.w.s t6, ft11, dyn<br> [0x80000858]:csrrs a7, fflags, zero<br> [0x8000085c]:sw t6, 456(a5)<br>    |
|  76|[0x80002568]<br>0x00000000|- fs1 == 0 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 4  #nosat<br>                                                       |[0x8000086c]:fcvt.w.s t6, ft11, dyn<br> [0x80000870]:csrrs a7, fflags, zero<br> [0x80000874]:sw t6, 464(a5)<br>    |
|  77|[0x80002570]<br>0x00000001|- fs1 == 0 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 3  #nosat<br>                                                       |[0x80000884]:fcvt.w.s t6, ft11, dyn<br> [0x80000888]:csrrs a7, fflags, zero<br> [0x8000088c]:sw t6, 472(a5)<br>    |
|  78|[0x80002578]<br>0x00000000|- fs1 == 0 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 2  #nosat<br>                                                       |[0x8000089c]:fcvt.w.s t6, ft11, dyn<br> [0x800008a0]:csrrs a7, fflags, zero<br> [0x800008a4]:sw t6, 480(a5)<br>    |
|  79|[0x80002580]<br>0x00000000|- fs1 == 0 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 1  #nosat<br>                                                       |[0x800008b4]:fcvt.w.s t6, ft11, dyn<br> [0x800008b8]:csrrs a7, fflags, zero<br> [0x800008bc]:sw t6, 488(a5)<br>    |
|  80|[0x80002588]<br>0x00000000|- fs1 == 0 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 0  #nosat<br>                                                       |[0x800008cc]:fcvt.w.s t6, ft11, dyn<br> [0x800008d0]:csrrs a7, fflags, zero<br> [0x800008d4]:sw t6, 496(a5)<br>    |
|  81|[0x80002590]<br>0x00000000|- fs1 == 0 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 4  #nosat<br>                                                       |[0x800008e4]:fcvt.w.s t6, ft11, dyn<br> [0x800008e8]:csrrs a7, fflags, zero<br> [0x800008ec]:sw t6, 504(a5)<br>    |
|  82|[0x80002598]<br>0x00000001|- fs1 == 0 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 3  #nosat<br>                                                       |[0x800008fc]:fcvt.w.s t6, ft11, dyn<br> [0x80000900]:csrrs a7, fflags, zero<br> [0x80000904]:sw t6, 512(a5)<br>    |
|  83|[0x800025a0]<br>0x00000000|- fs1 == 0 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 2  #nosat<br>                                                       |[0x80000914]:fcvt.w.s t6, ft11, dyn<br> [0x80000918]:csrrs a7, fflags, zero<br> [0x8000091c]:sw t6, 520(a5)<br>    |
|  84|[0x800025a8]<br>0x00000000|- fs1 == 0 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 1  #nosat<br>                                                       |[0x8000092c]:fcvt.w.s t6, ft11, dyn<br> [0x80000930]:csrrs a7, fflags, zero<br> [0x80000934]:sw t6, 528(a5)<br>    |
|  85|[0x800025b0]<br>0x00000000|- fs1 == 0 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 0  #nosat<br>                                                       |[0x80000944]:fcvt.w.s t6, ft11, dyn<br> [0x80000948]:csrrs a7, fflags, zero<br> [0x8000094c]:sw t6, 536(a5)<br>    |
|  86|[0x800025b8]<br>0xFFFFFFFF|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 4  #nosat<br>                                                       |[0x8000095c]:fcvt.w.s t6, ft11, dyn<br> [0x80000960]:csrrs a7, fflags, zero<br> [0x80000964]:sw t6, 544(a5)<br>    |
|  87|[0x800025c0]<br>0xFFFFFFFF|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 3  #nosat<br>                                                       |[0x80000974]:fcvt.w.s t6, ft11, dyn<br> [0x80000978]:csrrs a7, fflags, zero<br> [0x8000097c]:sw t6, 552(a5)<br>    |
|  88|[0x800025c8]<br>0xFFFFFFFE|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 2  #nosat<br>                                                       |[0x8000098c]:fcvt.w.s t6, ft11, dyn<br> [0x80000990]:csrrs a7, fflags, zero<br> [0x80000994]:sw t6, 560(a5)<br>    |
|  89|[0x800025d0]<br>0xFFFFFFFF|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 1  #nosat<br>                                                       |[0x800009a4]:fcvt.w.s t6, ft11, dyn<br> [0x800009a8]:csrrs a7, fflags, zero<br> [0x800009ac]:sw t6, 568(a5)<br>    |
|  90|[0x800025d8]<br>0xFFFFFFFF|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 0  #nosat<br>                                                       |[0x800009bc]:fcvt.w.s t6, ft11, dyn<br> [0x800009c0]:csrrs a7, fflags, zero<br> [0x800009c4]:sw t6, 576(a5)<br>    |
|  91|[0x800025e0]<br>0xFFFFFFFF|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 4  #nosat<br>                                                       |[0x800009d4]:fcvt.w.s t6, ft11, dyn<br> [0x800009d8]:csrrs a7, fflags, zero<br> [0x800009dc]:sw t6, 584(a5)<br>    |
|  92|[0x800025e8]<br>0xFFFFFFFF|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 3  #nosat<br>                                                       |[0x800009ec]:fcvt.w.s t6, ft11, dyn<br> [0x800009f0]:csrrs a7, fflags, zero<br> [0x800009f4]:sw t6, 592(a5)<br>    |
|  93|[0x800025f0]<br>0xFFFFFFFE|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 2  #nosat<br>                                                       |[0x80000a04]:fcvt.w.s t6, ft11, dyn<br> [0x80000a08]:csrrs a7, fflags, zero<br> [0x80000a0c]:sw t6, 600(a5)<br>    |
|  94|[0x800025f8]<br>0xFFFFFFFF|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 1  #nosat<br>                                                       |[0x80000a1c]:fcvt.w.s t6, ft11, dyn<br> [0x80000a20]:csrrs a7, fflags, zero<br> [0x80000a24]:sw t6, 608(a5)<br>    |
|  95|[0x80002600]<br>0xFFFFFFFF|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 0  #nosat<br>                                                       |[0x80000a34]:fcvt.w.s t6, ft11, dyn<br> [0x80000a38]:csrrs a7, fflags, zero<br> [0x80000a3c]:sw t6, 616(a5)<br>    |
|  96|[0x80002608]<br>0x00000001|- fs1 == 0 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 4  #nosat<br>                                                       |[0x80000a4c]:fcvt.w.s t6, ft11, dyn<br> [0x80000a50]:csrrs a7, fflags, zero<br> [0x80000a54]:sw t6, 624(a5)<br>    |
|  97|[0x80002610]<br>0x00000001|- fs1 == 0 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 3  #nosat<br>                                                       |[0x80000a64]:fcvt.w.s t6, ft11, dyn<br> [0x80000a68]:csrrs a7, fflags, zero<br> [0x80000a6c]:sw t6, 632(a5)<br>    |
|  98|[0x80002618]<br>0x00000000|- fs1 == 0 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 2  #nosat<br>                                                       |[0x80000a7c]:fcvt.w.s t6, ft11, dyn<br> [0x80000a80]:csrrs a7, fflags, zero<br> [0x80000a84]:sw t6, 640(a5)<br>    |
|  99|[0x80002620]<br>0x00000000|- fs1 == 0 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 1  #nosat<br>                                                       |[0x80000a94]:fcvt.w.s t6, ft11, dyn<br> [0x80000a98]:csrrs a7, fflags, zero<br> [0x80000a9c]:sw t6, 648(a5)<br>    |
| 100|[0x80002628]<br>0x00000001|- fs1 == 0 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 0  #nosat<br>                                                       |[0x80000aac]:fcvt.w.s t6, ft11, dyn<br> [0x80000ab0]:csrrs a7, fflags, zero<br> [0x80000ab4]:sw t6, 656(a5)<br>    |
| 101|[0x80002630]<br>0xFFFFFFFF|- fs1 == 1 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 4  #nosat<br>                                                       |[0x80000ac4]:fcvt.w.s t6, ft11, dyn<br> [0x80000ac8]:csrrs a7, fflags, zero<br> [0x80000acc]:sw t6, 664(a5)<br>    |
| 102|[0x80002638]<br>0x00000000|- fs1 == 1 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 3  #nosat<br>                                                       |[0x80000adc]:fcvt.w.s t6, ft11, dyn<br> [0x80000ae0]:csrrs a7, fflags, zero<br> [0x80000ae4]:sw t6, 672(a5)<br>    |
| 103|[0x80002640]<br>0xFFFFFFFF|- fs1 == 1 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 2  #nosat<br>                                                       |[0x80000af4]:fcvt.w.s t6, ft11, dyn<br> [0x80000af8]:csrrs a7, fflags, zero<br> [0x80000afc]:sw t6, 680(a5)<br>    |
| 104|[0x80002648]<br>0x00000000|- fs1 == 1 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 1  #nosat<br>                                                       |[0x80000b0c]:fcvt.w.s t6, ft11, dyn<br> [0x80000b10]:csrrs a7, fflags, zero<br> [0x80000b14]:sw t6, 688(a5)<br>    |
| 105|[0x80002650]<br>0xFFFFFFFF|- fs1 == 1 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 0  #nosat<br>                                                       |[0x80000b24]:fcvt.w.s t6, ft11, dyn<br> [0x80000b28]:csrrs a7, fflags, zero<br> [0x80000b2c]:sw t6, 696(a5)<br>    |
