
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000540')]      |
| SIG_REGION                | [('0x80002210', '0x80002360', '84 words')]      |
| COV_LABELS                | fcvt.w.s_b22      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch14/fcvt.w.s/riscof_work/fcvt.w.s_b22-01.S/ref.S    |
| Total Number of coverpoints| 109     |
| Total Coverpoints Hit     | 105      |
| Total Signature Updates   | 82      |
| STAT1                     | 40      |
| STAT2                     | 1      |
| STAT3                     | 0     |
| STAT4                     | 41     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000524]:fcvt.w.s t6, ft11, dyn
      [0x80000528]:csrrs a7, fflags, zero
      [0x8000052c]:sw t6, 120(a5)
 -- Signature Address: 0x80002350 Data: 0x0000D665
 -- Redundant Coverpoints hit by the op
      - opcode : fcvt.w.s
      - rd : x31
      - rs1 : f31
      - fs1 == 0 and fe1 == 0x8e and fm1 == 0x56653f and rm_val == 0  #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['opcode : fcvt.w.s', 'rd : x23', 'rs1 : f13', 'fs1 == 0 and fe1 == 0x7c and fm1 == 0x4923b8 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000011c]:fcvt.w.s s7, fa3, dyn
	-[0x80000120]:csrrs a7, fflags, zero
	-[0x80000124]:sw s7, 0(a5)
Current Store : [0x80000128] : sw a7, 4(a5) -- Store: [0x80002214]:0x00000001




Last Coverpoint : ['rd : x21', 'rs1 : f0', 'fs1 == 1 and fe1 == 0xb6 and fm1 == 0x403ed1 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000134]:fcvt.w.s s5, ft0, dyn
	-[0x80000138]:csrrs a7, fflags, zero
	-[0x8000013c]:sw s5, 8(a5)
Current Store : [0x80000140] : sw a7, 12(a5) -- Store: [0x8000221c]:0x00000011




Last Coverpoint : ['rd : x27', 'rs1 : f25', 'fs1 == 1 and fe1 == 0x29 and fm1 == 0x4ad269 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000014c]:fcvt.w.s s11, fs9, dyn
	-[0x80000150]:csrrs a7, fflags, zero
	-[0x80000154]:sw s11, 16(a5)
Current Store : [0x80000158] : sw a7, 20(a5) -- Store: [0x80002224]:0x00000011




Last Coverpoint : ['rd : x2', 'rs1 : f4', 'fs1 == 1 and fe1 == 0xa1 and fm1 == 0x0851ba and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000164]:fcvt.w.s sp, ft4, dyn
	-[0x80000168]:csrrs a7, fflags, zero
	-[0x8000016c]:sw sp, 24(a5)
Current Store : [0x80000170] : sw a7, 28(a5) -- Store: [0x8000222c]:0x00000011




Last Coverpoint : ['rd : x22', 'rs1 : f29', 'fs1 == 0 and fe1 == 0xa0 and fm1 == 0x37cfdc and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000017c]:fcvt.w.s s6, ft9, dyn
	-[0x80000180]:csrrs a7, fflags, zero
	-[0x80000184]:sw s6, 32(a5)
Current Store : [0x80000188] : sw a7, 36(a5) -- Store: [0x80002234]:0x00000011




Last Coverpoint : ['rd : x6', 'rs1 : f6', 'fs1 == 0 and fe1 == 0x9f and fm1 == 0x46450c and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000194]:fcvt.w.s t1, ft6, dyn
	-[0x80000198]:csrrs a7, fflags, zero
	-[0x8000019c]:sw t1, 40(a5)
Current Store : [0x800001a0] : sw a7, 44(a5) -- Store: [0x8000223c]:0x00000011




Last Coverpoint : ['rd : x17', 'rs1 : f16', 'fs1 == 1 and fe1 == 0x9e and fm1 == 0x283d12 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001b8]:fcvt.w.s a7, fa6, dyn
	-[0x800001bc]:csrrs s5, fflags, zero
	-[0x800001c0]:sw a7, 0(s3)
Current Store : [0x800001c4] : sw s5, 4(s3) -- Store: [0x80002244]:0x00000011




Last Coverpoint : ['rd : x11', 'rs1 : f2', 'fs1 == 0 and fe1 == 0x9d and fm1 == 0x72f818 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001dc]:fcvt.w.s a1, ft2, dyn
	-[0x800001e0]:csrrs a7, fflags, zero
	-[0x800001e4]:sw a1, 0(a5)
Current Store : [0x800001e8] : sw a7, 4(a5) -- Store: [0x8000224c]:0x00000011




Last Coverpoint : ['rd : x12', 'rs1 : f19', 'fs1 == 1 and fe1 == 0x9c and fm1 == 0x2edddb and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001f4]:fcvt.w.s a2, fs3, dyn
	-[0x800001f8]:csrrs a7, fflags, zero
	-[0x800001fc]:sw a2, 8(a5)
Current Store : [0x80000200] : sw a7, 12(a5) -- Store: [0x80002254]:0x00000011




Last Coverpoint : ['rd : x20', 'rs1 : f31', 'fs1 == 1 and fe1 == 0x9b and fm1 == 0x26c422 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000020c]:fcvt.w.s s4, ft11, dyn
	-[0x80000210]:csrrs a7, fflags, zero
	-[0x80000214]:sw s4, 16(a5)
Current Store : [0x80000218] : sw a7, 20(a5) -- Store: [0x8000225c]:0x00000011




Last Coverpoint : ['rd : x26', 'rs1 : f10', 'fs1 == 1 and fe1 == 0x9a and fm1 == 0x7872c3 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000224]:fcvt.w.s s10, fa0, dyn
	-[0x80000228]:csrrs a7, fflags, zero
	-[0x8000022c]:sw s10, 24(a5)
Current Store : [0x80000230] : sw a7, 28(a5) -- Store: [0x80002264]:0x00000011




Last Coverpoint : ['rd : x24', 'rs1 : f5', 'fs1 == 0 and fe1 == 0x99 and fm1 == 0x112603 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000023c]:fcvt.w.s s8, ft5, dyn
	-[0x80000240]:csrrs a7, fflags, zero
	-[0x80000244]:sw s8, 32(a5)
Current Store : [0x80000248] : sw a7, 36(a5) -- Store: [0x8000226c]:0x00000011




Last Coverpoint : ['rd : x18', 'rs1 : f14', 'fs1 == 0 and fe1 == 0x98 and fm1 == 0x0084e1 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000254]:fcvt.w.s s2, fa4, dyn
	-[0x80000258]:csrrs a7, fflags, zero
	-[0x8000025c]:sw s2, 40(a5)
Current Store : [0x80000260] : sw a7, 44(a5) -- Store: [0x80002274]:0x00000011




Last Coverpoint : ['rd : x3', 'rs1 : f17', 'fs1 == 1 and fe1 == 0x97 and fm1 == 0x05aa55 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000026c]:fcvt.w.s gp, fa7, dyn
	-[0x80000270]:csrrs a7, fflags, zero
	-[0x80000274]:sw gp, 48(a5)
Current Store : [0x80000278] : sw a7, 52(a5) -- Store: [0x8000227c]:0x00000011




Last Coverpoint : ['rd : x4', 'rs1 : f26', 'fs1 == 0 and fe1 == 0x96 and fm1 == 0x4e817e and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000284]:fcvt.w.s tp, fs10, dyn
	-[0x80000288]:csrrs a7, fflags, zero
	-[0x8000028c]:sw tp, 56(a5)
Current Store : [0x80000290] : sw a7, 60(a5) -- Store: [0x80002284]:0x00000011




Last Coverpoint : ['rd : x31', 'rs1 : f15', 'fs1 == 0 and fe1 == 0x95 and fm1 == 0x7c14e9 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000029c]:fcvt.w.s t6, fa5, dyn
	-[0x800002a0]:csrrs a7, fflags, zero
	-[0x800002a4]:sw t6, 64(a5)
Current Store : [0x800002a8] : sw a7, 68(a5) -- Store: [0x8000228c]:0x00000011




Last Coverpoint : ['rd : x9', 'rs1 : f27', 'fs1 == 0 and fe1 == 0x94 and fm1 == 0x7dbfb7 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800002b4]:fcvt.w.s s1, fs11, dyn
	-[0x800002b8]:csrrs a7, fflags, zero
	-[0x800002bc]:sw s1, 72(a5)
Current Store : [0x800002c0] : sw a7, 76(a5) -- Store: [0x80002294]:0x00000011




Last Coverpoint : ['rd : x1', 'rs1 : f11', 'fs1 == 1 and fe1 == 0x93 and fm1 == 0x624882 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800002cc]:fcvt.w.s ra, fa1, dyn
	-[0x800002d0]:csrrs a7, fflags, zero
	-[0x800002d4]:sw ra, 80(a5)
Current Store : [0x800002d8] : sw a7, 84(a5) -- Store: [0x8000229c]:0x00000011




Last Coverpoint : ['rd : x29', 'rs1 : f3', 'fs1 == 0 and fe1 == 0x92 and fm1 == 0x11056d and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800002e4]:fcvt.w.s t4, ft3, dyn
	-[0x800002e8]:csrrs a7, fflags, zero
	-[0x800002ec]:sw t4, 88(a5)
Current Store : [0x800002f0] : sw a7, 92(a5) -- Store: [0x800022a4]:0x00000011




Last Coverpoint : ['rd : x16', 'rs1 : f12', 'fs1 == 0 and fe1 == 0x91 and fm1 == 0x54b761 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000308]:fcvt.w.s a6, fa2, dyn
	-[0x8000030c]:csrrs s5, fflags, zero
	-[0x80000310]:sw a6, 0(s3)
Current Store : [0x80000314] : sw s5, 4(s3) -- Store: [0x800022ac]:0x00000011




Last Coverpoint : ['rd : x13', 'rs1 : f21', 'fs1 == 0 and fe1 == 0x90 and fm1 == 0x57ca4f and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000032c]:fcvt.w.s a3, fs5, dyn
	-[0x80000330]:csrrs a7, fflags, zero
	-[0x80000334]:sw a3, 0(a5)
Current Store : [0x80000338] : sw a7, 4(a5) -- Store: [0x800022b4]:0x00000011




Last Coverpoint : ['rd : x28', 'rs1 : f9', 'fs1 == 0 and fe1 == 0x8f and fm1 == 0x3a7971 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000344]:fcvt.w.s t3, fs1, dyn
	-[0x80000348]:csrrs a7, fflags, zero
	-[0x8000034c]:sw t3, 8(a5)
Current Store : [0x80000350] : sw a7, 12(a5) -- Store: [0x800022bc]:0x00000011




Last Coverpoint : ['rd : x0', 'rs1 : f18', 'fs1 == 0 and fe1 == 0x8e and fm1 == 0x56653f and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000035c]:fcvt.w.s zero, fs2, dyn
	-[0x80000360]:csrrs a7, fflags, zero
	-[0x80000364]:sw zero, 16(a5)
Current Store : [0x80000368] : sw a7, 20(a5) -- Store: [0x800022c4]:0x00000011




Last Coverpoint : ['rd : x14', 'rs1 : f7', 'fs1 == 1 and fe1 == 0x8d and fm1 == 0x244d9a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000374]:fcvt.w.s a4, ft7, dyn
	-[0x80000378]:csrrs a7, fflags, zero
	-[0x8000037c]:sw a4, 24(a5)
Current Store : [0x80000380] : sw a7, 28(a5) -- Store: [0x800022cc]:0x00000011




Last Coverpoint : ['rd : x15', 'rs1 : f22', 'fs1 == 0 and fe1 == 0x8c and fm1 == 0x30d877 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000398]:fcvt.w.s a5, fs6, dyn
	-[0x8000039c]:csrrs s5, fflags, zero
	-[0x800003a0]:sw a5, 0(s3)
Current Store : [0x800003a4] : sw s5, 4(s3) -- Store: [0x800022d4]:0x00000011




Last Coverpoint : ['rd : x8', 'rs1 : f8', 'fs1 == 0 and fe1 == 0x8b and fm1 == 0x4d3559 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003bc]:fcvt.w.s fp, fs0, dyn
	-[0x800003c0]:csrrs a7, fflags, zero
	-[0x800003c4]:sw fp, 0(a5)
Current Store : [0x800003c8] : sw a7, 4(a5) -- Store: [0x800022dc]:0x00000011




Last Coverpoint : ['rd : x30', 'rs1 : f30', 'fs1 == 0 and fe1 == 0x8a and fm1 == 0x6e19c1 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003d4]:fcvt.w.s t5, ft10, dyn
	-[0x800003d8]:csrrs a7, fflags, zero
	-[0x800003dc]:sw t5, 8(a5)
Current Store : [0x800003e0] : sw a7, 12(a5) -- Store: [0x800022e4]:0x00000011




Last Coverpoint : ['rd : x19', 'rs1 : f23', 'fs1 == 0 and fe1 == 0x89 and fm1 == 0x05b406 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003ec]:fcvt.w.s s3, fs7, dyn
	-[0x800003f0]:csrrs a7, fflags, zero
	-[0x800003f4]:sw s3, 16(a5)
Current Store : [0x800003f8] : sw a7, 20(a5) -- Store: [0x800022ec]:0x00000011




Last Coverpoint : ['rd : x5', 'rs1 : f28', 'fs1 == 0 and fe1 == 0x88 and fm1 == 0x7f8f2d and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000404]:fcvt.w.s t0, ft8, dyn
	-[0x80000408]:csrrs a7, fflags, zero
	-[0x8000040c]:sw t0, 24(a5)
Current Store : [0x80000410] : sw a7, 28(a5) -- Store: [0x800022f4]:0x00000011




Last Coverpoint : ['rd : x7', 'rs1 : f24', 'fs1 == 1 and fe1 == 0x87 and fm1 == 0x79f5e7 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000041c]:fcvt.w.s t2, fs8, dyn
	-[0x80000420]:csrrs a7, fflags, zero
	-[0x80000424]:sw t2, 32(a5)
Current Store : [0x80000428] : sw a7, 36(a5) -- Store: [0x800022fc]:0x00000011




Last Coverpoint : ['rd : x10', 'rs1 : f20', 'fs1 == 1 and fe1 == 0x86 and fm1 == 0x1fffe4 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000434]:fcvt.w.s a0, fs4, dyn
	-[0x80000438]:csrrs a7, fflags, zero
	-[0x8000043c]:sw a0, 40(a5)
Current Store : [0x80000440] : sw a7, 44(a5) -- Store: [0x80002304]:0x00000011




Last Coverpoint : ['rd : x25', 'rs1 : f1', 'fs1 == 0 and fe1 == 0x85 and fm1 == 0x29f475 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000044c]:fcvt.w.s s9, ft1, dyn
	-[0x80000450]:csrrs a7, fflags, zero
	-[0x80000454]:sw s9, 48(a5)
Current Store : [0x80000458] : sw a7, 52(a5) -- Store: [0x8000230c]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x84 and fm1 == 0x42a54b and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000464]:fcvt.w.s t6, ft11, dyn
	-[0x80000468]:csrrs a7, fflags, zero
	-[0x8000046c]:sw t6, 56(a5)
Current Store : [0x80000470] : sw a7, 60(a5) -- Store: [0x80002314]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x83 and fm1 == 0x148266 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000047c]:fcvt.w.s t6, ft11, dyn
	-[0x80000480]:csrrs a7, fflags, zero
	-[0x80000484]:sw t6, 64(a5)
Current Store : [0x80000488] : sw a7, 68(a5) -- Store: [0x8000231c]:0x00000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x82 and fm1 == 0x53a4fc and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000494]:fcvt.w.s t6, ft11, dyn
	-[0x80000498]:csrrs a7, fflags, zero
	-[0x8000049c]:sw t6, 72(a5)
Current Store : [0x800004a0] : sw a7, 76(a5) -- Store: [0x80002324]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x81 and fm1 == 0x696b5c and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004ac]:fcvt.w.s t6, ft11, dyn
	-[0x800004b0]:csrrs a7, fflags, zero
	-[0x800004b4]:sw t6, 80(a5)
Current Store : [0x800004b8] : sw a7, 84(a5) -- Store: [0x8000232c]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x80 and fm1 == 0x681ae9 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004c4]:fcvt.w.s t6, ft11, dyn
	-[0x800004c8]:csrrs a7, fflags, zero
	-[0x800004cc]:sw t6, 88(a5)
Current Store : [0x800004d0] : sw a7, 92(a5) -- Store: [0x80002334]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x1a616d and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004dc]:fcvt.w.s t6, ft11, dyn
	-[0x800004e0]:csrrs a7, fflags, zero
	-[0x800004e4]:sw t6, 96(a5)
Current Store : [0x800004e8] : sw a7, 100(a5) -- Store: [0x8000233c]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x49fee5 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004f4]:fcvt.w.s t6, ft11, dyn
	-[0x800004f8]:csrrs a7, fflags, zero
	-[0x800004fc]:sw t6, 104(a5)
Current Store : [0x80000500] : sw a7, 108(a5) -- Store: [0x80002344]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7d and fm1 == 0x36e5d6 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000050c]:fcvt.w.s t6, ft11, dyn
	-[0x80000510]:csrrs a7, fflags, zero
	-[0x80000514]:sw t6, 112(a5)
Current Store : [0x80000518] : sw a7, 116(a5) -- Store: [0x8000234c]:0x00000011




Last Coverpoint : ['opcode : fcvt.w.s', 'rd : x31', 'rs1 : f31', 'fs1 == 0 and fe1 == 0x8e and fm1 == 0x56653f and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000524]:fcvt.w.s t6, ft11, dyn
	-[0x80000528]:csrrs a7, fflags, zero
	-[0x8000052c]:sw t6, 120(a5)
Current Store : [0x80000530] : sw a7, 124(a5) -- Store: [0x80002354]:0x00000011





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                           coverpoints                                                            |                                                       code                                                       |
|---:|--------------------------|----------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002210]<br>0x00000000|- opcode : fcvt.w.s<br> - rd : x23<br> - rs1 : f13<br> - fs1 == 0 and fe1 == 0x7c and fm1 == 0x4923b8 and rm_val == 0  #nosat<br> |[0x8000011c]:fcvt.w.s s7, fa3, dyn<br> [0x80000120]:csrrs a7, fflags, zero<br> [0x80000124]:sw s7, 0(a5)<br>      |
|   2|[0x80002218]<br>0x80000000|- rd : x21<br> - rs1 : f0<br> - fs1 == 1 and fe1 == 0xb6 and fm1 == 0x403ed1 and rm_val == 0  #nosat<br>                          |[0x80000134]:fcvt.w.s s5, ft0, dyn<br> [0x80000138]:csrrs a7, fflags, zero<br> [0x8000013c]:sw s5, 8(a5)<br>      |
|   3|[0x80002220]<br>0x00000000|- rd : x27<br> - rs1 : f25<br> - fs1 == 1 and fe1 == 0x29 and fm1 == 0x4ad269 and rm_val == 0  #nosat<br>                         |[0x8000014c]:fcvt.w.s s11, fs9, dyn<br> [0x80000150]:csrrs a7, fflags, zero<br> [0x80000154]:sw s11, 16(a5)<br>   |
|   4|[0x80002228]<br>0x80000000|- rd : x2<br> - rs1 : f4<br> - fs1 == 1 and fe1 == 0xa1 and fm1 == 0x0851ba and rm_val == 0  #nosat<br>                           |[0x80000164]:fcvt.w.s sp, ft4, dyn<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:sw sp, 24(a5)<br>     |
|   5|[0x80002230]<br>0x7FFFFFFF|- rd : x22<br> - rs1 : f29<br> - fs1 == 0 and fe1 == 0xa0 and fm1 == 0x37cfdc and rm_val == 0  #nosat<br>                         |[0x8000017c]:fcvt.w.s s6, ft9, dyn<br> [0x80000180]:csrrs a7, fflags, zero<br> [0x80000184]:sw s6, 32(a5)<br>     |
|   6|[0x80002238]<br>0x7FFFFFFF|- rd : x6<br> - rs1 : f6<br> - fs1 == 0 and fe1 == 0x9f and fm1 == 0x46450c and rm_val == 0  #nosat<br>                           |[0x80000194]:fcvt.w.s t1, ft6, dyn<br> [0x80000198]:csrrs a7, fflags, zero<br> [0x8000019c]:sw t1, 40(a5)<br>     |
|   7|[0x80002240]<br>0x80000000|- rd : x17<br> - rs1 : f16<br> - fs1 == 1 and fe1 == 0x9e and fm1 == 0x283d12 and rm_val == 0  #nosat<br>                         |[0x800001b8]:fcvt.w.s a7, fa6, dyn<br> [0x800001bc]:csrrs s5, fflags, zero<br> [0x800001c0]:sw a7, 0(s3)<br>      |
|   8|[0x80002248]<br>0x797C0C00|- rd : x11<br> - rs1 : f2<br> - fs1 == 0 and fe1 == 0x9d and fm1 == 0x72f818 and rm_val == 0  #nosat<br>                          |[0x800001dc]:fcvt.w.s a1, ft2, dyn<br> [0x800001e0]:csrrs a7, fflags, zero<br> [0x800001e4]:sw a1, 0(a5)<br>      |
|   9|[0x80002250]<br>0xD4488940|- rd : x12<br> - rs1 : f19<br> - fs1 == 1 and fe1 == 0x9c and fm1 == 0x2edddb and rm_val == 0  #nosat<br>                         |[0x800001f4]:fcvt.w.s a2, fs3, dyn<br> [0x800001f8]:csrrs a7, fflags, zero<br> [0x800001fc]:sw a2, 8(a5)<br>      |
|  10|[0x80002258]<br>0xEB277BC0|- rd : x20<br> - rs1 : f31<br> - fs1 == 1 and fe1 == 0x9b and fm1 == 0x26c422 and rm_val == 0  #nosat<br>                         |[0x8000020c]:fcvt.w.s s4, ft11, dyn<br> [0x80000210]:csrrs a7, fflags, zero<br> [0x80000214]:sw s4, 16(a5)<br>    |
|  11|[0x80002260]<br>0xF078D3D0|- rd : x26<br> - rs1 : f10<br> - fs1 == 1 and fe1 == 0x9a and fm1 == 0x7872c3 and rm_val == 0  #nosat<br>                         |[0x80000224]:fcvt.w.s s10, fa0, dyn<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:sw s10, 24(a5)<br>   |
|  12|[0x80002268]<br>0x04893018|- rd : x24<br> - rs1 : f5<br> - fs1 == 0 and fe1 == 0x99 and fm1 == 0x112603 and rm_val == 0  #nosat<br>                          |[0x8000023c]:fcvt.w.s s8, ft5, dyn<br> [0x80000240]:csrrs a7, fflags, zero<br> [0x80000244]:sw s8, 32(a5)<br>     |
|  13|[0x80002270]<br>0x02021384|- rd : x18<br> - rs1 : f14<br> - fs1 == 0 and fe1 == 0x98 and fm1 == 0x0084e1 and rm_val == 0  #nosat<br>                         |[0x80000254]:fcvt.w.s s2, fa4, dyn<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:sw s2, 40(a5)<br>     |
|  14|[0x80002278]<br>0xFEF4AB56|- rd : x3<br> - rs1 : f17<br> - fs1 == 1 and fe1 == 0x97 and fm1 == 0x05aa55 and rm_val == 0  #nosat<br>                          |[0x8000026c]:fcvt.w.s gp, fa7, dyn<br> [0x80000270]:csrrs a7, fflags, zero<br> [0x80000274]:sw gp, 48(a5)<br>     |
|  15|[0x80002280]<br>0x00CE817E|- rd : x4<br> - rs1 : f26<br> - fs1 == 0 and fe1 == 0x96 and fm1 == 0x4e817e and rm_val == 0  #nosat<br>                          |[0x80000284]:fcvt.w.s tp, fs10, dyn<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:sw tp, 56(a5)<br>    |
|  16|[0x80002288]<br>0x007E0A74|- rd : x31<br> - rs1 : f15<br> - fs1 == 0 and fe1 == 0x95 and fm1 == 0x7c14e9 and rm_val == 0  #nosat<br>                         |[0x8000029c]:fcvt.w.s t6, fa5, dyn<br> [0x800002a0]:csrrs a7, fflags, zero<br> [0x800002a4]:sw t6, 64(a5)<br>     |
|  17|[0x80002290]<br>0x003F6FEE|- rd : x9<br> - rs1 : f27<br> - fs1 == 0 and fe1 == 0x94 and fm1 == 0x7dbfb7 and rm_val == 0  #nosat<br>                          |[0x800002b4]:fcvt.w.s s1, fs11, dyn<br> [0x800002b8]:csrrs a7, fflags, zero<br> [0x800002bc]:sw s1, 72(a5)<br>    |
|  18|[0x80002298]<br>0xFFE3B6F0|- rd : x1<br> - rs1 : f11<br> - fs1 == 1 and fe1 == 0x93 and fm1 == 0x624882 and rm_val == 0  #nosat<br>                          |[0x800002cc]:fcvt.w.s ra, fa1, dyn<br> [0x800002d0]:csrrs a7, fflags, zero<br> [0x800002d4]:sw ra, 80(a5)<br>     |
|  19|[0x800022a0]<br>0x00091057|- rd : x29<br> - rs1 : f3<br> - fs1 == 0 and fe1 == 0x92 and fm1 == 0x11056d and rm_val == 0  #nosat<br>                          |[0x800002e4]:fcvt.w.s t4, ft3, dyn<br> [0x800002e8]:csrrs a7, fflags, zero<br> [0x800002ec]:sw t4, 88(a5)<br>     |
|  20|[0x800022a8]<br>0x0006A5BB|- rd : x16<br> - rs1 : f12<br> - fs1 == 0 and fe1 == 0x91 and fm1 == 0x54b761 and rm_val == 0  #nosat<br>                         |[0x80000308]:fcvt.w.s a6, fa2, dyn<br> [0x8000030c]:csrrs s5, fflags, zero<br> [0x80000310]:sw a6, 0(s3)<br>      |
|  21|[0x800022b0]<br>0x00035F29|- rd : x13<br> - rs1 : f21<br> - fs1 == 0 and fe1 == 0x90 and fm1 == 0x57ca4f and rm_val == 0  #nosat<br>                         |[0x8000032c]:fcvt.w.s a3, fs5, dyn<br> [0x80000330]:csrrs a7, fflags, zero<br> [0x80000334]:sw a3, 0(a5)<br>      |
|  22|[0x800022b8]<br>0x000174F3|- rd : x28<br> - rs1 : f9<br> - fs1 == 0 and fe1 == 0x8f and fm1 == 0x3a7971 and rm_val == 0  #nosat<br>                          |[0x80000344]:fcvt.w.s t3, fs1, dyn<br> [0x80000348]:csrrs a7, fflags, zero<br> [0x8000034c]:sw t3, 8(a5)<br>      |
|  23|[0x800022c0]<br>0x00000000|- rd : x0<br> - rs1 : f18<br> - fs1 == 0 and fe1 == 0x8e and fm1 == 0x56653f and rm_val == 0  #nosat<br>                          |[0x8000035c]:fcvt.w.s zero, fs2, dyn<br> [0x80000360]:csrrs a7, fflags, zero<br> [0x80000364]:sw zero, 16(a5)<br> |
|  24|[0x800022c8]<br>0xFFFFADD9|- rd : x14<br> - rs1 : f7<br> - fs1 == 1 and fe1 == 0x8d and fm1 == 0x244d9a and rm_val == 0  #nosat<br>                          |[0x80000374]:fcvt.w.s a4, ft7, dyn<br> [0x80000378]:csrrs a7, fflags, zero<br> [0x8000037c]:sw a4, 24(a5)<br>     |
|  25|[0x800022d0]<br>0x00002C36|- rd : x15<br> - rs1 : f22<br> - fs1 == 0 and fe1 == 0x8c and fm1 == 0x30d877 and rm_val == 0  #nosat<br>                         |[0x80000398]:fcvt.w.s a5, fs6, dyn<br> [0x8000039c]:csrrs s5, fflags, zero<br> [0x800003a0]:sw a5, 0(s3)<br>      |
|  26|[0x800022d8]<br>0x000019A7|- rd : x8<br> - rs1 : f8<br> - fs1 == 0 and fe1 == 0x8b and fm1 == 0x4d3559 and rm_val == 0  #nosat<br>                           |[0x800003bc]:fcvt.w.s fp, fs0, dyn<br> [0x800003c0]:csrrs a7, fflags, zero<br> [0x800003c4]:sw fp, 0(a5)<br>      |
|  27|[0x800022e0]<br>0x00000EE2|- rd : x30<br> - rs1 : f30<br> - fs1 == 0 and fe1 == 0x8a and fm1 == 0x6e19c1 and rm_val == 0  #nosat<br>                         |[0x800003d4]:fcvt.w.s t5, ft10, dyn<br> [0x800003d8]:csrrs a7, fflags, zero<br> [0x800003dc]:sw t5, 8(a5)<br>     |
|  28|[0x800022e8]<br>0x0000042E|- rd : x19<br> - rs1 : f23<br> - fs1 == 0 and fe1 == 0x89 and fm1 == 0x05b406 and rm_val == 0  #nosat<br>                         |[0x800003ec]:fcvt.w.s s3, fs7, dyn<br> [0x800003f0]:csrrs a7, fflags, zero<br> [0x800003f4]:sw s3, 16(a5)<br>     |
|  29|[0x800022f0]<br>0x000003FE|- rd : x5<br> - rs1 : f28<br> - fs1 == 0 and fe1 == 0x88 and fm1 == 0x7f8f2d and rm_val == 0  #nosat<br>                          |[0x80000404]:fcvt.w.s t0, ft8, dyn<br> [0x80000408]:csrrs a7, fflags, zero<br> [0x8000040c]:sw t0, 24(a5)<br>     |
|  30|[0x800022f8]<br>0xFFFFFE0C|- rd : x7<br> - rs1 : f24<br> - fs1 == 1 and fe1 == 0x87 and fm1 == 0x79f5e7 and rm_val == 0  #nosat<br>                          |[0x8000041c]:fcvt.w.s t2, fs8, dyn<br> [0x80000420]:csrrs a7, fflags, zero<br> [0x80000424]:sw t2, 32(a5)<br>     |
|  31|[0x80002300]<br>0xFFFFFF60|- rd : x10<br> - rs1 : f20<br> - fs1 == 1 and fe1 == 0x86 and fm1 == 0x1fffe4 and rm_val == 0  #nosat<br>                         |[0x80000434]:fcvt.w.s a0, fs4, dyn<br> [0x80000438]:csrrs a7, fflags, zero<br> [0x8000043c]:sw a0, 40(a5)<br>     |
|  32|[0x80002308]<br>0x00000055|- rd : x25<br> - rs1 : f1<br> - fs1 == 0 and fe1 == 0x85 and fm1 == 0x29f475 and rm_val == 0  #nosat<br>                          |[0x8000044c]:fcvt.w.s s9, ft1, dyn<br> [0x80000450]:csrrs a7, fflags, zero<br> [0x80000454]:sw s9, 48(a5)<br>     |
|  33|[0x80002310]<br>0x00000031|- fs1 == 0 and fe1 == 0x84 and fm1 == 0x42a54b and rm_val == 0  #nosat<br>                                                        |[0x80000464]:fcvt.w.s t6, ft11, dyn<br> [0x80000468]:csrrs a7, fflags, zero<br> [0x8000046c]:sw t6, 56(a5)<br>    |
|  34|[0x80002318]<br>0x00000013|- fs1 == 0 and fe1 == 0x83 and fm1 == 0x148266 and rm_val == 0  #nosat<br>                                                        |[0x8000047c]:fcvt.w.s t6, ft11, dyn<br> [0x80000480]:csrrs a7, fflags, zero<br> [0x80000484]:sw t6, 64(a5)<br>    |
|  35|[0x80002320]<br>0xFFFFFFF3|- fs1 == 1 and fe1 == 0x82 and fm1 == 0x53a4fc and rm_val == 0  #nosat<br>                                                        |[0x80000494]:fcvt.w.s t6, ft11, dyn<br> [0x80000498]:csrrs a7, fflags, zero<br> [0x8000049c]:sw t6, 72(a5)<br>    |
|  36|[0x80002328]<br>0x00000007|- fs1 == 0 and fe1 == 0x81 and fm1 == 0x696b5c and rm_val == 0  #nosat<br>                                                        |[0x800004ac]:fcvt.w.s t6, ft11, dyn<br> [0x800004b0]:csrrs a7, fflags, zero<br> [0x800004b4]:sw t6, 80(a5)<br>    |
|  37|[0x80002330]<br>0x00000004|- fs1 == 0 and fe1 == 0x80 and fm1 == 0x681ae9 and rm_val == 0  #nosat<br>                                                        |[0x800004c4]:fcvt.w.s t6, ft11, dyn<br> [0x800004c8]:csrrs a7, fflags, zero<br> [0x800004cc]:sw t6, 88(a5)<br>    |
|  38|[0x80002338]<br>0x00000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x1a616d and rm_val == 0  #nosat<br>                                                        |[0x800004dc]:fcvt.w.s t6, ft11, dyn<br> [0x800004e0]:csrrs a7, fflags, zero<br> [0x800004e4]:sw t6, 96(a5)<br>    |
|  39|[0x80002340]<br>0x00000001|- fs1 == 0 and fe1 == 0x7e and fm1 == 0x49fee5 and rm_val == 0  #nosat<br>                                                        |[0x800004f4]:fcvt.w.s t6, ft11, dyn<br> [0x800004f8]:csrrs a7, fflags, zero<br> [0x800004fc]:sw t6, 104(a5)<br>   |
|  40|[0x80002348]<br>0x00000000|- fs1 == 0 and fe1 == 0x7d and fm1 == 0x36e5d6 and rm_val == 0  #nosat<br>                                                        |[0x8000050c]:fcvt.w.s t6, ft11, dyn<br> [0x80000510]:csrrs a7, fflags, zero<br> [0x80000514]:sw t6, 112(a5)<br>   |
