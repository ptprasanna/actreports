
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000420')]      |
| SIG_REGION                | [('0x80002210', '0x80002310', '64 words')]      |
| COV_LABELS                | fsqrt_b4      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch14/fsqrt/riscof_work/fsqrt_b4-01.S/ref.S    |
| Total Number of coverpoints| 77     |
| Total Coverpoints Hit     | 72      |
| Total Signature Updates   | 32      |
| STAT1                     | 32      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                  coverpoints                                                                  |                                                                       code                                                                        |
|---:|--------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002214]<br>0x00000001|- opcode : fsqrt.s<br> - rs1 : f8<br> - rd : f8<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and rm_val == 0  #nosat<br> |[0x8000011c]:fsqrt.s fs0, fs0, dyn<br> [0x80000120]:csrrs a7, fflags, zero<br> [0x80000124]:fsw fs0, 0(a5)<br> [0x80000128]:sw a7, 4(a5)<br>       |
|   2|[0x8000221c]<br>0x00000001|- rs1 : f11<br> - rd : f17<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and rm_val == 4  #nosat<br>                      |[0x80000134]:fsqrt.s fa7, fa1, dyn<br> [0x80000138]:csrrs a7, fflags, zero<br> [0x8000013c]:fsw fa7, 8(a5)<br> [0x80000140]:sw a7, 12(a5)<br>      |
|   3|[0x80002224]<br>0x00000001|- rs1 : f14<br> - rd : f19<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and rm_val == 3  #nosat<br>                                      |[0x8000014c]:fsqrt.s fs3, fa4, dyn<br> [0x80000150]:csrrs a7, fflags, zero<br> [0x80000154]:fsw fs3, 16(a5)<br> [0x80000158]:sw a7, 20(a5)<br>     |
|   4|[0x8000222c]<br>0x00000001|- rs1 : f10<br> - rd : f24<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and rm_val == 2  #nosat<br>                                      |[0x80000164]:fsqrt.s fs8, fa0, dyn<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:fsw fs8, 24(a5)<br> [0x80000170]:sw a7, 28(a5)<br>     |
|   5|[0x80002234]<br>0x00000001|- rs1 : f3<br> - rd : f30<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and rm_val == 1  #nosat<br>                                       |[0x8000017c]:fsqrt.s ft10, ft3, dyn<br> [0x80000180]:csrrs a7, fflags, zero<br> [0x80000184]:fsw ft10, 32(a5)<br> [0x80000188]:sw a7, 36(a5)<br>   |
|   6|[0x8000223c]<br>0x00000001|- rs1 : f5<br> - rd : f20<br>                                                                                                                  |[0x80000194]:fsqrt.s fs4, ft5, dyn<br> [0x80000198]:csrrs a7, fflags, zero<br> [0x8000019c]:fsw fs4, 40(a5)<br> [0x800001a0]:sw a7, 44(a5)<br>     |
|   7|[0x80002244]<br>0x00000001|- rs1 : f13<br> - rd : f1<br>                                                                                                                  |[0x800001ac]:fsqrt.s ft1, fa3, dyn<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:fsw ft1, 48(a5)<br> [0x800001b8]:sw a7, 52(a5)<br>     |
|   8|[0x8000224c]<br>0x00000001|- rs1 : f15<br> - rd : f23<br>                                                                                                                 |[0x800001c4]:fsqrt.s fs7, fa5, dyn<br> [0x800001c8]:csrrs a7, fflags, zero<br> [0x800001cc]:fsw fs7, 56(a5)<br> [0x800001d0]:sw a7, 60(a5)<br>     |
|   9|[0x80002254]<br>0x00000001|- rs1 : f7<br> - rd : f27<br>                                                                                                                  |[0x800001dc]:fsqrt.s fs11, ft7, dyn<br> [0x800001e0]:csrrs a7, fflags, zero<br> [0x800001e4]:fsw fs11, 64(a5)<br> [0x800001e8]:sw a7, 68(a5)<br>   |
|  10|[0x8000225c]<br>0x00000001|- rs1 : f17<br> - rd : f4<br>                                                                                                                  |[0x800001f4]:fsqrt.s ft4, fa7, dyn<br> [0x800001f8]:csrrs a7, fflags, zero<br> [0x800001fc]:fsw ft4, 72(a5)<br> [0x80000200]:sw a7, 76(a5)<br>     |
|  11|[0x80002264]<br>0x00000001|- rs1 : f28<br> - rd : f5<br>                                                                                                                  |[0x8000020c]:fsqrt.s ft5, ft8, dyn<br> [0x80000210]:csrrs a7, fflags, zero<br> [0x80000214]:fsw ft5, 80(a5)<br> [0x80000218]:sw a7, 84(a5)<br>     |
|  12|[0x8000226c]<br>0x00000001|- rs1 : f9<br> - rd : f2<br>                                                                                                                   |[0x80000224]:fsqrt.s ft2, fs1, dyn<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:fsw ft2, 88(a5)<br> [0x80000230]:sw a7, 92(a5)<br>     |
|  13|[0x80002274]<br>0x00000001|- rs1 : f26<br> - rd : f6<br>                                                                                                                  |[0x8000023c]:fsqrt.s ft6, fs10, dyn<br> [0x80000240]:csrrs a7, fflags, zero<br> [0x80000244]:fsw ft6, 96(a5)<br> [0x80000248]:sw a7, 100(a5)<br>   |
|  14|[0x8000227c]<br>0x00000001|- rs1 : f12<br> - rd : f0<br>                                                                                                                  |[0x80000254]:fsqrt.s ft0, fa2, dyn<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:fsw ft0, 104(a5)<br> [0x80000260]:sw a7, 108(a5)<br>   |
|  15|[0x80002284]<br>0x00000001|- rs1 : f0<br> - rd : f7<br>                                                                                                                   |[0x8000026c]:fsqrt.s ft7, ft0, dyn<br> [0x80000270]:csrrs a7, fflags, zero<br> [0x80000274]:fsw ft7, 112(a5)<br> [0x80000278]:sw a7, 116(a5)<br>   |
|  16|[0x8000228c]<br>0x00000001|- rs1 : f1<br> - rd : f21<br>                                                                                                                  |[0x80000284]:fsqrt.s fs5, ft1, dyn<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:fsw fs5, 120(a5)<br> [0x80000290]:sw a7, 124(a5)<br>   |
|  17|[0x80002294]<br>0x00000001|- rs1 : f16<br> - rd : f28<br>                                                                                                                 |[0x8000029c]:fsqrt.s ft8, fa6, dyn<br> [0x800002a0]:csrrs a7, fflags, zero<br> [0x800002a4]:fsw ft8, 128(a5)<br> [0x800002a8]:sw a7, 132(a5)<br>   |
|  18|[0x8000229c]<br>0x00000001|- rs1 : f24<br> - rd : f12<br>                                                                                                                 |[0x800002b4]:fsqrt.s fa2, fs8, dyn<br> [0x800002b8]:csrrs a7, fflags, zero<br> [0x800002bc]:fsw fa2, 136(a5)<br> [0x800002c0]:sw a7, 140(a5)<br>   |
|  19|[0x800022a4]<br>0x00000001|- rs1 : f25<br> - rd : f18<br>                                                                                                                 |[0x800002cc]:fsqrt.s fs2, fs9, dyn<br> [0x800002d0]:csrrs a7, fflags, zero<br> [0x800002d4]:fsw fs2, 144(a5)<br> [0x800002d8]:sw a7, 148(a5)<br>   |
|  20|[0x800022ac]<br>0x00000001|- rs1 : f27<br> - rd : f16<br>                                                                                                                 |[0x800002e4]:fsqrt.s fa6, fs11, dyn<br> [0x800002e8]:csrrs a7, fflags, zero<br> [0x800002ec]:fsw fa6, 152(a5)<br> [0x800002f0]:sw a7, 156(a5)<br>  |
|  21|[0x800022b4]<br>0x00000001|- rs1 : f21<br> - rd : f31<br>                                                                                                                 |[0x800002fc]:fsqrt.s ft11, fs5, dyn<br> [0x80000300]:csrrs a7, fflags, zero<br> [0x80000304]:fsw ft11, 160(a5)<br> [0x80000308]:sw a7, 164(a5)<br> |
|  22|[0x800022bc]<br>0x00000001|- rs1 : f31<br> - rd : f11<br>                                                                                                                 |[0x80000314]:fsqrt.s fa1, ft11, dyn<br> [0x80000318]:csrrs a7, fflags, zero<br> [0x8000031c]:fsw fa1, 168(a5)<br> [0x80000320]:sw a7, 172(a5)<br>  |
|  23|[0x800022c4]<br>0x00000001|- rs1 : f2<br> - rd : f26<br>                                                                                                                  |[0x8000032c]:fsqrt.s fs10, ft2, dyn<br> [0x80000330]:csrrs a7, fflags, zero<br> [0x80000334]:fsw fs10, 176(a5)<br> [0x80000338]:sw a7, 180(a5)<br> |
|  24|[0x800022cc]<br>0x00000001|- rs1 : f18<br> - rd : f14<br>                                                                                                                 |[0x80000344]:fsqrt.s fa4, fs2, dyn<br> [0x80000348]:csrrs a7, fflags, zero<br> [0x8000034c]:fsw fa4, 184(a5)<br> [0x80000350]:sw a7, 188(a5)<br>   |
|  25|[0x800022d4]<br>0x00000001|- rs1 : f19<br> - rd : f10<br>                                                                                                                 |[0x8000035c]:fsqrt.s fa0, fs3, dyn<br> [0x80000360]:csrrs a7, fflags, zero<br> [0x80000364]:fsw fa0, 192(a5)<br> [0x80000368]:sw a7, 196(a5)<br>   |
|  26|[0x800022dc]<br>0x00000001|- rs1 : f20<br> - rd : f13<br>                                                                                                                 |[0x80000374]:fsqrt.s fa3, fs4, dyn<br> [0x80000378]:csrrs a7, fflags, zero<br> [0x8000037c]:fsw fa3, 200(a5)<br> [0x80000380]:sw a7, 204(a5)<br>   |
|  27|[0x800022e4]<br>0x00000001|- rs1 : f30<br> - rd : f15<br>                                                                                                                 |[0x8000038c]:fsqrt.s fa5, ft10, dyn<br> [0x80000390]:csrrs a7, fflags, zero<br> [0x80000394]:fsw fa5, 208(a5)<br> [0x80000398]:sw a7, 212(a5)<br>  |
|  28|[0x800022ec]<br>0x00000001|- rs1 : f29<br> - rd : f9<br>                                                                                                                  |[0x800003a4]:fsqrt.s fs1, ft9, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:fsw fs1, 216(a5)<br> [0x800003b0]:sw a7, 220(a5)<br>   |
|  29|[0x800022f4]<br>0x00000001|- rs1 : f6<br> - rd : f29<br>                                                                                                                  |[0x800003bc]:fsqrt.s ft9, ft6, dyn<br> [0x800003c0]:csrrs a7, fflags, zero<br> [0x800003c4]:fsw ft9, 224(a5)<br> [0x800003c8]:sw a7, 228(a5)<br>   |
|  30|[0x800022fc]<br>0x00000001|- rs1 : f23<br> - rd : f25<br>                                                                                                                 |[0x800003d4]:fsqrt.s fs9, fs7, dyn<br> [0x800003d8]:csrrs a7, fflags, zero<br> [0x800003dc]:fsw fs9, 232(a5)<br> [0x800003e0]:sw a7, 236(a5)<br>   |
|  31|[0x80002304]<br>0x00000001|- rs1 : f22<br> - rd : f3<br>                                                                                                                  |[0x800003ec]:fsqrt.s ft3, fs6, dyn<br> [0x800003f0]:csrrs a7, fflags, zero<br> [0x800003f4]:fsw ft3, 240(a5)<br> [0x800003f8]:sw a7, 244(a5)<br>   |
|  32|[0x8000230c]<br>0x00000001|- rs1 : f4<br> - rd : f22<br>                                                                                                                  |[0x80000404]:fsqrt.s fs6, ft4, dyn<br> [0x80000408]:csrrs a7, fflags, zero<br> [0x8000040c]:fsw fs6, 248(a5)<br> [0x80000410]:sw a7, 252(a5)<br>   |
