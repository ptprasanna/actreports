
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000730')]      |
| SIG_REGION                | [('0x80002310', '0x80002520', '132 words')]      |
| COV_LABELS                | fsqrt_b20      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch14/fsqrt/riscof_work/fsqrt_b20-01.S/ref.S    |
| Total Number of coverpoints| 137     |
| Total Coverpoints Hit     | 132      |
| Total Signature Updates   | 65      |
| STAT1                     | 65      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                   coverpoints                                                                   |                                                                        code                                                                        |
|---:|--------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002314]<br>0x00000001|- opcode : fsqrt.s<br> - rs1 : f14<br> - rd : f14<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and rm_val == 0  #nosat<br> |[0x8000011c]:fsqrt.s fa4, fa4, dyn<br> [0x80000120]:csrrs a7, fflags, zero<br> [0x80000124]:fsw fa4, 0(a5)<br> [0x80000128]:sw a7, 4(a5)<br>        |
|   2|[0x8000231c]<br>0x00000001|- rs1 : f24<br> - rd : f12<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and rm_val == 0  #nosat<br>                        |[0x80000134]:fsqrt.s fa2, fs8, dyn<br> [0x80000138]:csrrs a7, fflags, zero<br> [0x8000013c]:fsw fa2, 8(a5)<br> [0x80000140]:sw a7, 12(a5)<br>       |
|   3|[0x80002324]<br>0x00000001|- rs1 : f29<br> - rd : f8<br> - fs1 == 0 and fe1 == 0x9e and fm1 == 0x38d874 and rm_val == 0  #nosat<br>                                         |[0x8000014c]:fsqrt.s fs0, ft9, dyn<br> [0x80000150]:csrrs a7, fflags, zero<br> [0x80000154]:fsw fs0, 16(a5)<br> [0x80000158]:sw a7, 20(a5)<br>      |
|   4|[0x8000232c]<br>0x00000001|- rs1 : f21<br> - rd : f9<br> - fs1 == 0 and fe1 == 0xd0 and fm1 == 0x010151 and rm_val == 0  #nosat<br>                                         |[0x80000164]:fsqrt.s fs1, fs5, dyn<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:fsw fs1, 24(a5)<br> [0x80000170]:sw a7, 28(a5)<br>      |
|   5|[0x80002334]<br>0x00000001|- rs1 : f3<br> - rd : f0<br> - fs1 == 0 and fe1 == 0xb7 and fm1 == 0x4bce51 and rm_val == 0  #nosat<br>                                          |[0x8000017c]:fsqrt.s ft0, ft3, dyn<br> [0x80000180]:csrrs a7, fflags, zero<br> [0x80000184]:fsw ft0, 32(a5)<br> [0x80000188]:sw a7, 36(a5)<br>      |
|   6|[0x8000233c]<br>0x00000001|- rs1 : f31<br> - rd : f11<br> - fs1 == 0 and fe1 == 0x30 and fm1 == 0x75cb89 and rm_val == 0  #nosat<br>                                        |[0x80000194]:fsqrt.s fa1, ft11, dyn<br> [0x80000198]:csrrs a7, fflags, zero<br> [0x8000019c]:fsw fa1, 40(a5)<br> [0x800001a0]:sw a7, 44(a5)<br>     |
|   7|[0x80002344]<br>0x00000001|- rs1 : f16<br> - rd : f6<br> - fs1 == 0 and fe1 == 0x79 and fm1 == 0x785c55 and rm_val == 0  #nosat<br>                                         |[0x800001ac]:fsqrt.s ft6, fa6, dyn<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:fsw ft6, 48(a5)<br> [0x800001b8]:sw a7, 52(a5)<br>      |
|   8|[0x8000234c]<br>0x00000001|- rs1 : f7<br> - rd : f3<br> - fs1 == 0 and fe1 == 0xac and fm1 == 0x13884e and rm_val == 0  #nosat<br>                                          |[0x800001c4]:fsqrt.s ft3, ft7, dyn<br> [0x800001c8]:csrrs a7, fflags, zero<br> [0x800001cc]:fsw ft3, 56(a5)<br> [0x800001d0]:sw a7, 60(a5)<br>      |
|   9|[0x80002354]<br>0x00000001|- rs1 : f25<br> - rd : f18<br> - fs1 == 0 and fe1 == 0x65 and fm1 == 0x064562 and rm_val == 0  #nosat<br>                                        |[0x800001dc]:fsqrt.s fs2, fs9, dyn<br> [0x800001e0]:csrrs a7, fflags, zero<br> [0x800001e4]:fsw fs2, 64(a5)<br> [0x800001e8]:sw a7, 68(a5)<br>      |
|  10|[0x8000235c]<br>0x00000001|- rs1 : f0<br> - rd : f29<br> - fs1 == 0 and fe1 == 0xd3 and fm1 == 0x190acf and rm_val == 0  #nosat<br>                                         |[0x800001f4]:fsqrt.s ft9, ft0, dyn<br> [0x800001f8]:csrrs a7, fflags, zero<br> [0x800001fc]:fsw ft9, 72(a5)<br> [0x80000200]:sw a7, 76(a5)<br>      |
|  11|[0x80002364]<br>0x00000001|- rs1 : f5<br> - rd : f2<br> - fs1 == 0 and fe1 == 0xea and fm1 == 0x284ae6 and rm_val == 0  #nosat<br>                                          |[0x8000020c]:fsqrt.s ft2, ft5, dyn<br> [0x80000210]:csrrs a7, fflags, zero<br> [0x80000214]:fsw ft2, 80(a5)<br> [0x80000218]:sw a7, 84(a5)<br>      |
|  12|[0x8000236c]<br>0x00000001|- rs1 : f28<br> - rd : f27<br> - fs1 == 0 and fe1 == 0x4e and fm1 == 0x454542 and rm_val == 0  #nosat<br>                                        |[0x80000224]:fsqrt.s fs11, ft8, dyn<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:fsw fs11, 88(a5)<br> [0x80000230]:sw a7, 92(a5)<br>    |
|  13|[0x80002374]<br>0x00000001|- rs1 : f27<br> - rd : f16<br> - fs1 == 0 and fe1 == 0xb6 and fm1 == 0x479816 and rm_val == 0  #nosat<br>                                        |[0x8000023c]:fsqrt.s fa6, fs11, dyn<br> [0x80000240]:csrrs a7, fflags, zero<br> [0x80000244]:fsw fa6, 96(a5)<br> [0x80000248]:sw a7, 100(a5)<br>    |
|  14|[0x8000237c]<br>0x00000001|- rs1 : f6<br> - rd : f31<br> - fs1 == 0 and fe1 == 0x65 and fm1 == 0x5b1e82 and rm_val == 0  #nosat<br>                                         |[0x80000254]:fsqrt.s ft11, ft6, dyn<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:fsw ft11, 104(a5)<br> [0x80000260]:sw a7, 108(a5)<br>  |
|  15|[0x80002384]<br>0x00000001|- rs1 : f15<br> - rd : f19<br> - fs1 == 0 and fe1 == 0x7b and fm1 == 0x64e1f0 and rm_val == 0  #nosat<br>                                        |[0x8000026c]:fsqrt.s fs3, fa5, dyn<br> [0x80000270]:csrrs a7, fflags, zero<br> [0x80000274]:fsw fs3, 112(a5)<br> [0x80000278]:sw a7, 116(a5)<br>    |
|  16|[0x8000238c]<br>0x00000001|- rs1 : f11<br> - rd : f5<br> - fs1 == 0 and fe1 == 0xc2 and fm1 == 0x26f9c3 and rm_val == 0  #nosat<br>                                         |[0x80000284]:fsqrt.s ft5, fa1, dyn<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:fsw ft5, 120(a5)<br> [0x80000290]:sw a7, 124(a5)<br>    |
|  17|[0x80002394]<br>0x00000001|- rs1 : f13<br> - rd : f7<br> - fs1 == 0 and fe1 == 0x31 and fm1 == 0x011313 and rm_val == 0  #nosat<br>                                         |[0x8000029c]:fsqrt.s ft7, fa3, dyn<br> [0x800002a0]:csrrs a7, fflags, zero<br> [0x800002a4]:fsw ft7, 128(a5)<br> [0x800002a8]:sw a7, 132(a5)<br>    |
|  18|[0x8000239c]<br>0x00000001|- rs1 : f12<br> - rd : f28<br> - fs1 == 0 and fe1 == 0xad and fm1 == 0x75bbd8 and rm_val == 0  #nosat<br>                                        |[0x800002b4]:fsqrt.s ft8, fa2, dyn<br> [0x800002b8]:csrrs a7, fflags, zero<br> [0x800002bc]:fsw ft8, 136(a5)<br> [0x800002c0]:sw a7, 140(a5)<br>    |
|  19|[0x800023a4]<br>0x00000001|- rs1 : f26<br> - rd : f24<br> - fs1 == 0 and fe1 == 0xe4 and fm1 == 0x1477dc and rm_val == 0  #nosat<br>                                        |[0x800002cc]:fsqrt.s fs8, fs10, dyn<br> [0x800002d0]:csrrs a7, fflags, zero<br> [0x800002d4]:fsw fs8, 144(a5)<br> [0x800002d8]:sw a7, 148(a5)<br>   |
|  20|[0x800023ac]<br>0x00000001|- rs1 : f10<br> - rd : f13<br> - fs1 == 0 and fe1 == 0xf9 and fm1 == 0x2b61ee and rm_val == 0  #nosat<br>                                        |[0x800002e4]:fsqrt.s fa3, fa0, dyn<br> [0x800002e8]:csrrs a7, fflags, zero<br> [0x800002ec]:fsw fa3, 152(a5)<br> [0x800002f0]:sw a7, 156(a5)<br>    |
|  21|[0x800023b4]<br>0x00000001|- rs1 : f9<br> - rd : f25<br> - fs1 == 0 and fe1 == 0x3f and fm1 == 0x0577a2 and rm_val == 0  #nosat<br>                                         |[0x800002fc]:fsqrt.s fs9, fs1, dyn<br> [0x80000300]:csrrs a7, fflags, zero<br> [0x80000304]:fsw fs9, 160(a5)<br> [0x80000308]:sw a7, 164(a5)<br>    |
|  22|[0x800023bc]<br>0x00000001|- rs1 : f22<br> - rd : f15<br> - fs1 == 0 and fe1 == 0xf5 and fm1 == 0x0aadc1 and rm_val == 0  #nosat<br>                                        |[0x80000314]:fsqrt.s fa5, fs6, dyn<br> [0x80000318]:csrrs a7, fflags, zero<br> [0x8000031c]:fsw fa5, 168(a5)<br> [0x80000320]:sw a7, 172(a5)<br>    |
|  23|[0x800023c4]<br>0x00000001|- rs1 : f4<br> - rd : f26<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x2bf296 and rm_val == 0  #nosat<br>                                         |[0x8000032c]:fsqrt.s fs10, ft4, dyn<br> [0x80000330]:csrrs a7, fflags, zero<br> [0x80000334]:fsw fs10, 176(a5)<br> [0x80000338]:sw a7, 180(a5)<br>  |
|  24|[0x800023cc]<br>0x00000001|- rs1 : f1<br> - rd : f10<br> - fs1 == 0 and fe1 == 0x39 and fm1 == 0x0ef3b1 and rm_val == 0  #nosat<br>                                         |[0x80000344]:fsqrt.s fa0, ft1, dyn<br> [0x80000348]:csrrs a7, fflags, zero<br> [0x8000034c]:fsw fa0, 184(a5)<br> [0x80000350]:sw a7, 188(a5)<br>    |
|  25|[0x800023d4]<br>0x00000001|- rs1 : f19<br> - rd : f21<br> - fs1 == 0 and fe1 == 0x59 and fm1 == 0x0fed85 and rm_val == 0  #nosat<br>                                        |[0x8000035c]:fsqrt.s fs5, fs3, dyn<br> [0x80000360]:csrrs a7, fflags, zero<br> [0x80000364]:fsw fs5, 192(a5)<br> [0x80000368]:sw a7, 196(a5)<br>    |
|  26|[0x800023dc]<br>0x00000001|- rs1 : f23<br> - rd : f1<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x0cd173 and rm_val == 0  #nosat<br>                                         |[0x80000374]:fsqrt.s ft1, fs7, dyn<br> [0x80000378]:csrrs a7, fflags, zero<br> [0x8000037c]:fsw ft1, 200(a5)<br> [0x80000380]:sw a7, 204(a5)<br>    |
|  27|[0x800023e4]<br>0x00000001|- rs1 : f18<br> - rd : f17<br> - fs1 == 0 and fe1 == 0xdd and fm1 == 0x4096e8 and rm_val == 0  #nosat<br>                                        |[0x8000038c]:fsqrt.s fa7, fs2, dyn<br> [0x80000390]:csrrs a7, fflags, zero<br> [0x80000394]:fsw fa7, 208(a5)<br> [0x80000398]:sw a7, 212(a5)<br>    |
|  28|[0x800023ec]<br>0x00000001|- rs1 : f17<br> - rd : f30<br> - fs1 == 0 and fe1 == 0x0b and fm1 == 0x0cd684 and rm_val == 0  #nosat<br>                                        |[0x800003a4]:fsqrt.s ft10, fa7, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:fsw ft10, 216(a5)<br> [0x800003b0]:sw a7, 220(a5)<br>  |
|  29|[0x800023f4]<br>0x00000001|- rs1 : f2<br> - rd : f23<br> - fs1 == 0 and fe1 == 0xa7 and fm1 == 0x0f78f8 and rm_val == 0  #nosat<br>                                         |[0x800003bc]:fsqrt.s fs7, ft2, dyn<br> [0x800003c0]:csrrs a7, fflags, zero<br> [0x800003c4]:fsw fs7, 224(a5)<br> [0x800003c8]:sw a7, 228(a5)<br>    |
|  30|[0x800023fc]<br>0x00000001|- rs1 : f30<br> - rd : f22<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x7f3827 and rm_val == 0  #nosat<br>                                        |[0x800003d4]:fsqrt.s fs6, ft10, dyn<br> [0x800003d8]:csrrs a7, fflags, zero<br> [0x800003dc]:fsw fs6, 232(a5)<br> [0x800003e0]:sw a7, 236(a5)<br>   |
|  31|[0x80002404]<br>0x00000001|- rs1 : f8<br> - rd : f4<br> - fs1 == 0 and fe1 == 0xf3 and fm1 == 0x6653ed and rm_val == 0  #nosat<br>                                          |[0x800003ec]:fsqrt.s ft4, fs0, dyn<br> [0x800003f0]:csrrs a7, fflags, zero<br> [0x800003f4]:fsw ft4, 240(a5)<br> [0x800003f8]:sw a7, 244(a5)<br>    |
|  32|[0x8000240c]<br>0x00000001|- rs1 : f20<br> - fs1 == 0 and fe1 == 0xc0 and fm1 == 0x3590aa and rm_val == 0  #nosat<br>                                                       |[0x80000404]:fsqrt.s fs7, fs4, dyn<br> [0x80000408]:csrrs a7, fflags, zero<br> [0x8000040c]:fsw fs7, 248(a5)<br> [0x80000410]:sw a7, 252(a5)<br>    |
|  33|[0x80002414]<br>0x00000001|- rd : f20<br> - fs1 == 0 and fe1 == 0x3c and fm1 == 0x124e58 and rm_val == 0  #nosat<br>                                                        |[0x8000041c]:fsqrt.s fs4, ft7, dyn<br> [0x80000420]:csrrs a7, fflags, zero<br> [0x80000424]:fsw fs4, 256(a5)<br> [0x80000428]:sw a7, 260(a5)<br>    |
|  34|[0x8000241c]<br>0x00000001|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000005 and rm_val == 0  #nosat<br>                                                                       |[0x80000434]:fsqrt.s ft11, ft10, dyn<br> [0x80000438]:csrrs a7, fflags, zero<br> [0x8000043c]:fsw ft11, 264(a5)<br> [0x80000440]:sw a7, 268(a5)<br> |
|  35|[0x80002424]<br>0x00000001|- fs1 == 0 and fe1 == 0x52 and fm1 == 0x216b44 and rm_val == 0  #nosat<br>                                                                       |[0x8000044c]:fsqrt.s ft11, ft10, dyn<br> [0x80000450]:csrrs a7, fflags, zero<br> [0x80000454]:fsw ft11, 272(a5)<br> [0x80000458]:sw a7, 276(a5)<br> |
|  36|[0x8000242c]<br>0x00000001|- fs1 == 0 and fe1 == 0xce and fm1 == 0x1168e1 and rm_val == 0  #nosat<br>                                                                       |[0x80000464]:fsqrt.s ft11, ft10, dyn<br> [0x80000468]:csrrs a7, fflags, zero<br> [0x8000046c]:fsw ft11, 280(a5)<br> [0x80000470]:sw a7, 284(a5)<br> |
|  37|[0x80002434]<br>0x00000001|- fs1 == 0 and fe1 == 0xa0 and fm1 == 0x10d851 and rm_val == 0  #nosat<br>                                                                       |[0x8000047c]:fsqrt.s ft11, ft10, dyn<br> [0x80000480]:csrrs a7, fflags, zero<br> [0x80000484]:fsw ft11, 288(a5)<br> [0x80000488]:sw a7, 292(a5)<br> |
|  38|[0x8000243c]<br>0x00000001|- fs1 == 0 and fe1 == 0xea and fm1 == 0x4f33d9 and rm_val == 0  #nosat<br>                                                                       |[0x80000494]:fsqrt.s ft11, ft10, dyn<br> [0x80000498]:csrrs a7, fflags, zero<br> [0x8000049c]:fsw ft11, 296(a5)<br> [0x800004a0]:sw a7, 300(a5)<br> |
|  39|[0x80002444]<br>0x00000001|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x5b5a62 and rm_val == 0  #nosat<br>                                                                       |[0x800004ac]:fsqrt.s ft11, ft10, dyn<br> [0x800004b0]:csrrs a7, fflags, zero<br> [0x800004b4]:fsw ft11, 304(a5)<br> [0x800004b8]:sw a7, 308(a5)<br> |
|  40|[0x8000244c]<br>0x00000001|- fs1 == 0 and fe1 == 0xbc and fm1 == 0x68cd04 and rm_val == 0  #nosat<br>                                                                       |[0x800004c4]:fsqrt.s ft11, ft10, dyn<br> [0x800004c8]:csrrs a7, fflags, zero<br> [0x800004cc]:fsw ft11, 312(a5)<br> [0x800004d0]:sw a7, 316(a5)<br> |
|  41|[0x80002454]<br>0x00000001|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000160 and rm_val == 0  #nosat<br>                                                                       |[0x800004dc]:fsqrt.s ft11, ft10, dyn<br> [0x800004e0]:csrrs a7, fflags, zero<br> [0x800004e4]:fsw ft11, 320(a5)<br> [0x800004e8]:sw a7, 324(a5)<br> |
|  42|[0x8000245c]<br>0x00000001|- fs1 == 0 and fe1 == 0x6a and fm1 == 0x3e2364 and rm_val == 0  #nosat<br>                                                                       |[0x800004f4]:fsqrt.s ft11, ft10, dyn<br> [0x800004f8]:csrrs a7, fflags, zero<br> [0x800004fc]:fsw ft11, 328(a5)<br> [0x80000500]:sw a7, 332(a5)<br> |
|  43|[0x80002464]<br>0x00000001|- fs1 == 0 and fe1 == 0x34 and fm1 == 0x08f690 and rm_val == 0  #nosat<br>                                                                       |[0x8000050c]:fsqrt.s ft11, ft10, dyn<br> [0x80000510]:csrrs a7, fflags, zero<br> [0x80000514]:fsw ft11, 336(a5)<br> [0x80000518]:sw a7, 340(a5)<br> |
|  44|[0x8000246c]<br>0x00000001|- fs1 == 0 and fe1 == 0xd3 and fm1 == 0x6a7f20 and rm_val == 0  #nosat<br>                                                                       |[0x80000524]:fsqrt.s ft11, ft10, dyn<br> [0x80000528]:csrrs a7, fflags, zero<br> [0x8000052c]:fsw ft11, 344(a5)<br> [0x80000530]:sw a7, 348(a5)<br> |
|  45|[0x80002474]<br>0x00000001|- fs1 == 0 and fe1 == 0x80 and fm1 == 0x6d5a40 and rm_val == 0  #nosat<br>                                                                       |[0x8000053c]:fsqrt.s ft11, ft10, dyn<br> [0x80000540]:csrrs a7, fflags, zero<br> [0x80000544]:fsw ft11, 352(a5)<br> [0x80000548]:sw a7, 356(a5)<br> |
|  46|[0x8000247c]<br>0x00000001|- fs1 == 0 and fe1 == 0x7b and fm1 == 0x46c080 and rm_val == 0  #nosat<br>                                                                       |[0x80000554]:fsqrt.s ft11, ft10, dyn<br> [0x80000558]:csrrs a7, fflags, zero<br> [0x8000055c]:fsw ft11, 360(a5)<br> [0x80000560]:sw a7, 364(a5)<br> |
|  47|[0x80002484]<br>0x00000001|- fs1 == 0 and fe1 == 0xd0 and fm1 == 0x095440 and rm_val == 0  #nosat<br>                                                                       |[0x8000056c]:fsqrt.s ft11, ft10, dyn<br> [0x80000570]:csrrs a7, fflags, zero<br> [0x80000574]:fsw ft11, 368(a5)<br> [0x80000578]:sw a7, 372(a5)<br> |
|  48|[0x8000248c]<br>0x00000001|- fs1 == 0 and fe1 == 0xef and fm1 == 0x7bb880 and rm_val == 0  #nosat<br>                                                                       |[0x80000584]:fsqrt.s ft11, ft10, dyn<br> [0x80000588]:csrrs a7, fflags, zero<br> [0x8000058c]:fsw ft11, 376(a5)<br> [0x80000590]:sw a7, 380(a5)<br> |
|  49|[0x80002494]<br>0x00000001|- fs1 == 0 and fe1 == 0x39 and fm1 == 0x69d200 and rm_val == 0  #nosat<br>                                                                       |[0x8000059c]:fsqrt.s ft11, ft10, dyn<br> [0x800005a0]:csrrs a7, fflags, zero<br> [0x800005a4]:fsw ft11, 384(a5)<br> [0x800005a8]:sw a7, 388(a5)<br> |
|  50|[0x8000249c]<br>0x00000001|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x7ff200 and rm_val == 0  #nosat<br>                                                                       |[0x800005b4]:fsqrt.s ft11, ft10, dyn<br> [0x800005b8]:csrrs a7, fflags, zero<br> [0x800005bc]:fsw ft11, 392(a5)<br> [0x800005c0]:sw a7, 396(a5)<br> |
|  51|[0x800024a4]<br>0x00000001|- fs1 == 0 and fe1 == 0x66 and fm1 == 0x64c400 and rm_val == 0  #nosat<br>                                                                       |[0x800005cc]:fsqrt.s ft11, ft10, dyn<br> [0x800005d0]:csrrs a7, fflags, zero<br> [0x800005d4]:fsw ft11, 400(a5)<br> [0x800005d8]:sw a7, 404(a5)<br> |
|  52|[0x800024ac]<br>0x00000001|- fs1 == 0 and fe1 == 0x88 and fm1 == 0x7c0400 and rm_val == 0  #nosat<br>                                                                       |[0x800005e4]:fsqrt.s ft11, ft10, dyn<br> [0x800005e8]:csrrs a7, fflags, zero<br> [0x800005ec]:fsw ft11, 408(a5)<br> [0x800005f0]:sw a7, 412(a5)<br> |
|  53|[0x800024b4]<br>0x00000001|- fs1 == 0 and fe1 == 0x86 and fm1 == 0x130400 and rm_val == 0  #nosat<br>                                                                       |[0x800005fc]:fsqrt.s ft11, ft10, dyn<br> [0x80000600]:csrrs a7, fflags, zero<br> [0x80000604]:fsw ft11, 416(a5)<br> [0x80000608]:sw a7, 420(a5)<br> |
|  54|[0x800024bc]<br>0x00000001|- fs1 == 0 and fe1 == 0x8e and fm1 == 0x689000 and rm_val == 0  #nosat<br>                                                                       |[0x80000614]:fsqrt.s ft11, ft10, dyn<br> [0x80000618]:csrrs a7, fflags, zero<br> [0x8000061c]:fsw ft11, 424(a5)<br> [0x80000620]:sw a7, 428(a5)<br> |
|  55|[0x800024c4]<br>0x00000001|- fs1 == 0 and fe1 == 0x59 and fm1 == 0x7d2000 and rm_val == 0  #nosat<br>                                                                       |[0x8000062c]:fsqrt.s ft11, ft10, dyn<br> [0x80000630]:csrrs a7, fflags, zero<br> [0x80000634]:fsw ft11, 432(a5)<br> [0x80000638]:sw a7, 436(a5)<br> |
|  56|[0x800024cc]<br>0x00000001|- fs1 == 0 and fe1 == 0x24 and fm1 == 0x689000 and rm_val == 0  #nosat<br>                                                                       |[0x80000644]:fsqrt.s ft11, ft10, dyn<br> [0x80000648]:csrrs a7, fflags, zero<br> [0x8000064c]:fsw ft11, 440(a5)<br> [0x80000650]:sw a7, 444(a5)<br> |
|  57|[0x800024d4]<br>0x00000001|- fs1 == 0 and fe1 == 0x3b and fm1 == 0x108000 and rm_val == 0  #nosat<br>                                                                       |[0x8000065c]:fsqrt.s ft11, ft10, dyn<br> [0x80000660]:csrrs a7, fflags, zero<br> [0x80000664]:fsw ft11, 448(a5)<br> [0x80000668]:sw a7, 452(a5)<br> |
|  58|[0x800024dc]<br>0x00000001|- fs1 == 0 and fe1 == 0x82 and fm1 == 0x044000 and rm_val == 0  #nosat<br>                                                                       |[0x80000674]:fsqrt.s ft11, ft10, dyn<br> [0x80000678]:csrrs a7, fflags, zero<br> [0x8000067c]:fsw ft11, 456(a5)<br> [0x80000680]:sw a7, 460(a5)<br> |
|  59|[0x800024e4]<br>0x00000001|- fs1 == 0 and fe1 == 0x97 and fm1 == 0x5c8000 and rm_val == 0  #nosat<br>                                                                       |[0x8000068c]:fsqrt.s ft11, ft10, dyn<br> [0x80000690]:csrrs a7, fflags, zero<br> [0x80000694]:fsw ft11, 464(a5)<br> [0x80000698]:sw a7, 468(a5)<br> |
|  60|[0x800024ec]<br>0x00000001|- fs1 == 0 and fe1 == 0x86 and fm1 == 0x704000 and rm_val == 0  #nosat<br>                                                                       |[0x800006a4]:fsqrt.s ft11, ft10, dyn<br> [0x800006a8]:csrrs a7, fflags, zero<br> [0x800006ac]:fsw ft11, 472(a5)<br> [0x800006b0]:sw a7, 476(a5)<br> |
|  61|[0x800024f4]<br>0x00000001|- fs1 == 0 and fe1 == 0xf4 and fm1 == 0x610000 and rm_val == 0  #nosat<br>                                                                       |[0x800006bc]:fsqrt.s ft11, ft10, dyn<br> [0x800006c0]:csrrs a7, fflags, zero<br> [0x800006c4]:fsw ft11, 480(a5)<br> [0x800006c8]:sw a7, 484(a5)<br> |
|  62|[0x800024fc]<br>0x00000001|- fs1 == 0 and fe1 == 0xc7 and fm1 == 0x720000 and rm_val == 0  #nosat<br>                                                                       |[0x800006d4]:fsqrt.s ft11, ft10, dyn<br> [0x800006d8]:csrrs a7, fflags, zero<br> [0x800006dc]:fsw ft11, 488(a5)<br> [0x800006e0]:sw a7, 492(a5)<br> |
|  63|[0x80002504]<br>0x00000001|- fs1 == 0 and fe1 == 0xb7 and fm1 == 0x720000 and rm_val == 0  #nosat<br>                                                                       |[0x800006ec]:fsqrt.s ft11, ft10, dyn<br> [0x800006f0]:csrrs a7, fflags, zero<br> [0x800006f4]:fsw ft11, 496(a5)<br> [0x800006f8]:sw a7, 500(a5)<br> |
|  64|[0x8000250c]<br>0x00000001|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x480000 and rm_val == 0  #nosat<br>                                                                       |[0x80000704]:fsqrt.s ft11, ft10, dyn<br> [0x80000708]:csrrs a7, fflags, zero<br> [0x8000070c]:fsw ft11, 504(a5)<br> [0x80000710]:sw a7, 508(a5)<br> |
|  65|[0x80002514]<br>0x00000001|- fs1 == 0 and fe1 == 0x39 and fm1 == 0x480000 and rm_val == 0  #nosat<br>                                                                       |[0x8000071c]:fsqrt.s ft11, ft10, dyn<br> [0x80000720]:csrrs a7, fflags, zero<br> [0x80000724]:fsw ft11, 512(a5)<br> [0x80000728]:sw a7, 516(a5)<br> |
