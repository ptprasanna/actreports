
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800006d0')]      |
| SIG_REGION                | [('0x80002310', '0x800024b0', '104 words')]      |
| COV_LABELS                | fadd_b12      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch14/fadd/riscof_work/fadd_b12-01.S/ref.S    |
| Total Number of coverpoints| 158     |
| Total Coverpoints Hit     | 152      |
| Total Signature Updates   | 52      |
| STAT1                     | 52      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                      coverpoints                                                                                                       |                                                                          code                                                                          |
|---:|--------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002314]<br>0x00000000|- opcode : fadd.s<br> - rs1 : f21<br> - rs2 : f11<br> - rd : f11<br> - rs2 == rd != rs1<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x222105 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br> |[0x80000120]:fadd.s fa1, fs5, fa1, dyn<br> [0x80000124]:csrrs a7, fflags, zero<br> [0x80000128]:fsw fa1, 0(a5)<br> [0x8000012c]:sw a7, 4(a5)<br>        |
|   2|[0x8000231c]<br>0x00000000|- rs1 : f16<br> - rs2 : f16<br> - rd : f14<br> - rs1 == rs2 != rd<br>                                                                                                                                                   |[0x8000013c]:fadd.s fa4, fa6, fa6, dyn<br> [0x80000140]:csrrs a7, fflags, zero<br> [0x80000144]:fsw fa4, 8(a5)<br> [0x80000148]:sw a7, 12(a5)<br>       |
|   3|[0x80002324]<br>0x00000000|- rs1 : f0<br> - rs2 : f29<br> - rd : f13<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x185183 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x28224f and rm_val == 0  #nosat<br> |[0x80000158]:fadd.s fa3, ft0, ft9, dyn<br> [0x8000015c]:csrrs a7, fflags, zero<br> [0x80000160]:fsw fa3, 16(a5)<br> [0x80000164]:sw a7, 20(a5)<br>      |
|   4|[0x8000232c]<br>0x00000005|- rs1 : f7<br> - rs2 : f7<br> - rd : f7<br> - rs1 == rs2 == rd<br>                                                                                                                                                      |[0x80000174]:fadd.s ft7, ft7, ft7, dyn<br> [0x80000178]:csrrs a7, fflags, zero<br> [0x8000017c]:fsw ft7, 24(a5)<br> [0x80000180]:sw a7, 28(a5)<br>      |
|   5|[0x80002334]<br>0x00000005|- rs1 : f30<br> - rs2 : f17<br> - rd : f30<br> - rs1 == rd != rs2<br> - fs1 == 1 and fe1 == 0xfd and fm1 == 0x2d0427 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x554674 and rm_val == 0  #nosat<br>                       |[0x80000190]:fadd.s ft10, ft10, fa7, dyn<br> [0x80000194]:csrrs a7, fflags, zero<br> [0x80000198]:fsw ft10, 32(a5)<br> [0x8000019c]:sw a7, 36(a5)<br>   |
|   6|[0x8000233c]<br>0x00000005|- rs1 : f10<br> - rs2 : f8<br> - rd : f3<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x365ad7 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                                |[0x800001ac]:fadd.s ft3, fa0, fs0, dyn<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:fsw ft3, 40(a5)<br> [0x800001b8]:sw a7, 44(a5)<br>      |
|   7|[0x80002344]<br>0x00000005|- rs1 : f9<br> - rs2 : f30<br> - rd : f5<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x2bd8f4 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                                |[0x800001c8]:fadd.s ft5, fs1, ft10, dyn<br> [0x800001cc]:csrrs a7, fflags, zero<br> [0x800001d0]:fsw ft5, 48(a5)<br> [0x800001d4]:sw a7, 52(a5)<br>     |
|   8|[0x8000234c]<br>0x00000005|- rs1 : f20<br> - rs2 : f0<br> - rd : f15<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x1bd52c and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                               |[0x800001e4]:fadd.s fa5, fs4, ft0, dyn<br> [0x800001e8]:csrrs a7, fflags, zero<br> [0x800001ec]:fsw fa5, 56(a5)<br> [0x800001f0]:sw a7, 60(a5)<br>      |
|   9|[0x80002354]<br>0x00000005|- rs1 : f25<br> - rs2 : f4<br> - rd : f9<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x076a16 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x3c560e and rm_val == 0  #nosat<br>                                                |[0x80000200]:fadd.s fs1, fs9, ft4, dyn<br> [0x80000204]:csrrs a7, fflags, zero<br> [0x80000208]:fsw fs1, 64(a5)<br> [0x8000020c]:sw a7, 68(a5)<br>      |
|  10|[0x8000235c]<br>0x00000005|- rs1 : f12<br> - rs2 : f6<br> - rd : f18<br> - fs1 == 1 and fe1 == 0xfd and fm1 == 0x4f9722 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x32ec8c and rm_val == 0  #nosat<br>                                               |[0x8000021c]:fadd.s fs2, fa2, ft6, dyn<br> [0x80000220]:csrrs a7, fflags, zero<br> [0x80000224]:fsw fs2, 72(a5)<br> [0x80000228]:sw a7, 76(a5)<br>      |
|  11|[0x80002364]<br>0x00000005|- rs1 : f29<br> - rs2 : f18<br> - rd : f26<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x2c7300 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                              |[0x80000238]:fadd.s fs10, ft9, fs2, dyn<br> [0x8000023c]:csrrs a7, fflags, zero<br> [0x80000240]:fsw fs10, 80(a5)<br> [0x80000244]:sw a7, 84(a5)<br>    |
|  12|[0x8000236c]<br>0x00000005|- rs1 : f27<br> - rs2 : f20<br> - rd : f22<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x314a05 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                              |[0x80000254]:fadd.s fs6, fs11, fs4, dyn<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:fsw fs6, 88(a5)<br> [0x80000260]:sw a7, 92(a5)<br>     |
|  13|[0x80002374]<br>0x00000005|- rs1 : f15<br> - rs2 : f28<br> - rd : f6<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x1175bf and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                               |[0x80000270]:fadd.s ft6, fa5, ft8, dyn<br> [0x80000274]:csrrs a7, fflags, zero<br> [0x80000278]:fsw ft6, 96(a5)<br> [0x8000027c]:sw a7, 100(a5)<br>     |
|  14|[0x8000237c]<br>0x00000005|- rs1 : f26<br> - rs2 : f27<br> - rd : f17<br> - fs1 == 1 and fe1 == 0xfd and fm1 == 0x36fce6 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x597afe and rm_val == 0  #nosat<br>                                              |[0x8000028c]:fadd.s fa7, fs10, fs11, dyn<br> [0x80000290]:csrrs a7, fflags, zero<br> [0x80000294]:fsw fa7, 104(a5)<br> [0x80000298]:sw a7, 108(a5)<br>  |
|  15|[0x80002384]<br>0x00000005|- rs1 : f11<br> - rs2 : f26<br> - rd : f0<br> - fs1 == 1 and fe1 == 0xfd and fm1 == 0x6b4e0d and fs2 == 0 and fe2 == 0xfe and fm2 == 0x73bb25 and rm_val == 0  #nosat<br>                                               |[0x800002a8]:fadd.s ft0, fa1, fs10, dyn<br> [0x800002ac]:csrrs a7, fflags, zero<br> [0x800002b0]:fsw ft0, 112(a5)<br> [0x800002b4]:sw a7, 116(a5)<br>   |
|  16|[0x8000238c]<br>0x00000005|- rs1 : f24<br> - rs2 : f12<br> - rd : f25<br> - fs1 == 1 and fe1 == 0xfc and fm1 == 0x1173d9 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x106e2e and rm_val == 0  #nosat<br>                                              |[0x800002c4]:fadd.s fs9, fs8, fa2, dyn<br> [0x800002c8]:csrrs a7, fflags, zero<br> [0x800002cc]:fsw fs9, 120(a5)<br> [0x800002d0]:sw a7, 124(a5)<br>    |
|  17|[0x80002394]<br>0x00000005|- rs1 : f22<br> - rs2 : f15<br> - rd : f8<br> - fs1 == 1 and fe1 == 0xfd and fm1 == 0x5d0ccb and fs2 == 0 and fe2 == 0xfe and fm2 == 0x2644ac and rm_val == 0  #nosat<br>                                               |[0x800002e0]:fadd.s fs0, fs6, fa5, dyn<br> [0x800002e4]:csrrs a7, fflags, zero<br> [0x800002e8]:fsw fs0, 128(a5)<br> [0x800002ec]:sw a7, 132(a5)<br>    |
|  18|[0x8000239c]<br>0x00000005|- rs1 : f3<br> - rs2 : f9<br> - rd : f23<br> - fs1 == 1 and fe1 == 0xfd and fm1 == 0x64f961 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x3c6359 and rm_val == 0  #nosat<br>                                                |[0x800002fc]:fadd.s fs7, ft3, fs1, dyn<br> [0x80000300]:csrrs a7, fflags, zero<br> [0x80000304]:fsw fs7, 136(a5)<br> [0x80000308]:sw a7, 140(a5)<br>    |
|  19|[0x800023a4]<br>0x00000005|- rs1 : f5<br> - rs2 : f23<br> - rd : f16<br> - fs1 == 1 and fe1 == 0xfd and fm1 == 0x61a51b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x4642a7 and rm_val == 0  #nosat<br>                                               |[0x80000318]:fadd.s fa6, ft5, fs7, dyn<br> [0x8000031c]:csrrs a7, fflags, zero<br> [0x80000320]:fsw fa6, 144(a5)<br> [0x80000324]:sw a7, 148(a5)<br>    |
|  20|[0x800023ac]<br>0x00000005|- rs1 : f1<br> - rs2 : f5<br> - rd : f28<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x390e97 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                                |[0x80000334]:fadd.s ft8, ft1, ft5, dyn<br> [0x80000338]:csrrs a7, fflags, zero<br> [0x8000033c]:fsw ft8, 152(a5)<br> [0x80000340]:sw a7, 156(a5)<br>    |
|  21|[0x800023b4]<br>0x00000005|- rs1 : f31<br> - rs2 : f25<br> - rd : f2<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x1c60ac and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                               |[0x80000350]:fadd.s ft2, ft11, fs9, dyn<br> [0x80000354]:csrrs a7, fflags, zero<br> [0x80000358]:fsw ft2, 160(a5)<br> [0x8000035c]:sw a7, 164(a5)<br>   |
|  22|[0x800023bc]<br>0x00000005|- rs1 : f23<br> - rs2 : f1<br> - rd : f12<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x07a8e7 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x70c4b8 and rm_val == 0  #nosat<br>                                               |[0x8000036c]:fadd.s fa2, fs7, ft1, dyn<br> [0x80000370]:csrrs a7, fflags, zero<br> [0x80000374]:fsw fa2, 168(a5)<br> [0x80000378]:sw a7, 172(a5)<br>    |
|  23|[0x800023c4]<br>0x00000005|- rs1 : f18<br> - rs2 : f21<br> - rd : f4<br> - fs1 == 1 and fe1 == 0xfb and fm1 == 0x278349 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x7d9098 and rm_val == 0  #nosat<br>                                               |[0x80000388]:fadd.s ft4, fs2, fs5, dyn<br> [0x8000038c]:csrrs a7, fflags, zero<br> [0x80000390]:fsw ft4, 176(a5)<br> [0x80000394]:sw a7, 180(a5)<br>    |
|  24|[0x800023cc]<br>0x00000005|- rs1 : f6<br> - rs2 : f2<br> - rd : f19<br> - fs1 == 1 and fe1 == 0xfd and fm1 == 0x430c98 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                                |[0x800003a4]:fadd.s fs3, ft6, ft2, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:fsw fs3, 184(a5)<br> [0x800003b0]:sw a7, 188(a5)<br>    |
|  25|[0x800023d4]<br>0x00000005|- rs1 : f13<br> - rs2 : f19<br> - rd : f27<br> - fs1 == 1 and fe1 == 0xfa and fm1 == 0x772129 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x19caca and rm_val == 0  #nosat<br>                                              |[0x800003c0]:fadd.s fs11, fa3, fs3, dyn<br> [0x800003c4]:csrrs a7, fflags, zero<br> [0x800003c8]:fsw fs11, 192(a5)<br> [0x800003cc]:sw a7, 196(a5)<br>  |
|  26|[0x800023dc]<br>0x00000005|- rs1 : f2<br> - rs2 : f24<br> - rd : f29<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x1a35e0 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x703879 and rm_val == 0  #nosat<br>                                               |[0x800003dc]:fadd.s ft9, ft2, fs8, dyn<br> [0x800003e0]:csrrs a7, fflags, zero<br> [0x800003e4]:fsw ft9, 200(a5)<br> [0x800003e8]:sw a7, 204(a5)<br>    |
|  27|[0x800023e4]<br>0x00000005|- rs1 : f19<br> - rs2 : f13<br> - rd : f20<br> - fs1 == 1 and fe1 == 0xfc and fm1 == 0x3741cc and fs2 == 0 and fe2 == 0xfe and fm2 == 0x6794fc and rm_val == 0  #nosat<br>                                              |[0x800003f8]:fadd.s fs4, fs3, fa3, dyn<br> [0x800003fc]:csrrs a7, fflags, zero<br> [0x80000400]:fsw fs4, 208(a5)<br> [0x80000404]:sw a7, 212(a5)<br>    |
|  28|[0x800023ec]<br>0x00000005|- rs1 : f14<br> - rs2 : f31<br> - rd : f21<br> - fs1 == 1 and fe1 == 0xfc and fm1 == 0x12bd51 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x11a59d and rm_val == 0  #nosat<br>                                              |[0x80000414]:fadd.s fs5, fa4, ft11, dyn<br> [0x80000418]:csrrs a7, fflags, zero<br> [0x8000041c]:fsw fs5, 216(a5)<br> [0x80000420]:sw a7, 220(a5)<br>   |
|  29|[0x800023f4]<br>0x00000005|- rs1 : f28<br> - rs2 : f3<br> - rd : f24<br> - fs1 == 1 and fe1 == 0xfc and fm1 == 0x79c1c6 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x6591d8 and rm_val == 0  #nosat<br>                                               |[0x80000430]:fadd.s fs8, ft8, ft3, dyn<br> [0x80000434]:csrrs a7, fflags, zero<br> [0x80000438]:fsw fs8, 224(a5)<br> [0x8000043c]:sw a7, 228(a5)<br>    |
|  30|[0x800023fc]<br>0x00000005|- rs1 : f17<br> - rs2 : f10<br> - rd : f1<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x269468 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x55691d and rm_val == 0  #nosat<br>                                               |[0x8000044c]:fadd.s ft1, fa7, fa0, dyn<br> [0x80000450]:csrrs a7, fflags, zero<br> [0x80000454]:fsw ft1, 232(a5)<br> [0x80000458]:sw a7, 236(a5)<br>    |
|  31|[0x80002404]<br>0x00000005|- rs1 : f4<br> - rs2 : f14<br> - rd : f10<br> - fs1 == 1 and fe1 == 0xf4 and fm1 == 0x60affa and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3df905 and rm_val == 0  #nosat<br>                                               |[0x80000468]:fadd.s fa0, ft4, fa4, dyn<br> [0x8000046c]:csrrs a7, fflags, zero<br> [0x80000470]:fsw fa0, 240(a5)<br> [0x80000474]:sw a7, 244(a5)<br>    |
|  32|[0x8000240c]<br>0x00000005|- rs1 : f8<br> - rs2 : f22<br> - rd : f31<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x1e5ec7 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                               |[0x80000484]:fadd.s ft11, fs0, fs6, dyn<br> [0x80000488]:csrrs a7, fflags, zero<br> [0x8000048c]:fsw ft11, 248(a5)<br> [0x80000490]:sw a7, 252(a5)<br>  |
|  33|[0x80002414]<br>0x00000005|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x0a2eec and fs2 == 0 and fe2 == 0xfe and fm2 == 0x56c1e5 and rm_val == 0  #nosat<br>                                                                                             |[0x800004a0]:fadd.s ft11, ft10, ft9, dyn<br> [0x800004a4]:csrrs a7, fflags, zero<br> [0x800004a8]:fsw ft11, 256(a5)<br> [0x800004ac]:sw a7, 260(a5)<br> |
|  34|[0x8000241c]<br>0x00000005|- fs1 == 1 and fe1 == 0xfd and fm1 == 0x52b355 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x5817b0 and rm_val == 0  #nosat<br>                                                                                             |[0x800004bc]:fadd.s ft11, ft10, ft9, dyn<br> [0x800004c0]:csrrs a7, fflags, zero<br> [0x800004c4]:fsw ft11, 264(a5)<br> [0x800004c8]:sw a7, 268(a5)<br> |
|  35|[0x80002424]<br>0x00000005|- fs1 == 1 and fe1 == 0xfc and fm1 == 0x480ede and fs2 == 0 and fe2 == 0xfe and fm2 == 0x4c9471 and rm_val == 0  #nosat<br>                                                                                             |[0x800004d8]:fadd.s ft11, ft10, ft9, dyn<br> [0x800004dc]:csrrs a7, fflags, zero<br> [0x800004e0]:fsw ft11, 272(a5)<br> [0x800004e4]:sw a7, 276(a5)<br> |
|  36|[0x8000242c]<br>0x00000005|- fs1 == 1 and fe1 == 0xfd and fm1 == 0x372bf7 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                                                                             |[0x800004f4]:fadd.s ft11, ft10, ft9, dyn<br> [0x800004f8]:csrrs a7, fflags, zero<br> [0x800004fc]:fsw ft11, 280(a5)<br> [0x80000500]:sw a7, 284(a5)<br> |
|  37|[0x80002434]<br>0x00000005|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x2f4c51 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                                                                             |[0x80000510]:fadd.s ft11, ft10, ft9, dyn<br> [0x80000514]:csrrs a7, fflags, zero<br> [0x80000518]:fsw ft11, 288(a5)<br> [0x8000051c]:sw a7, 292(a5)<br> |
|  38|[0x8000243c]<br>0x00000005|- fs1 == 1 and fe1 == 0xfd and fm1 == 0x26b8d3 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x48a6ba and rm_val == 0  #nosat<br>                                                                                             |[0x8000052c]:fadd.s ft11, ft10, ft9, dyn<br> [0x80000530]:csrrs a7, fflags, zero<br> [0x80000534]:fsw ft11, 296(a5)<br> [0x80000538]:sw a7, 300(a5)<br> |
|  39|[0x80002444]<br>0x00000005|- fs1 == 1 and fe1 == 0xfd and fm1 == 0x354d84 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x5bf8d8 and rm_val == 0  #nosat<br>                                                                                             |[0x80000548]:fadd.s ft11, ft10, ft9, dyn<br> [0x8000054c]:csrrs a7, fflags, zero<br> [0x80000550]:fsw ft11, 304(a5)<br> [0x80000554]:sw a7, 308(a5)<br> |
|  40|[0x8000244c]<br>0x00000005|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x2c93b2 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                                                                             |[0x80000564]:fadd.s ft11, ft10, ft9, dyn<br> [0x80000568]:csrrs a7, fflags, zero<br> [0x8000056c]:fsw ft11, 312(a5)<br> [0x80000570]:sw a7, 316(a5)<br> |
|  41|[0x80002454]<br>0x00000005|- fs1 == 1 and fe1 == 0xfc and fm1 == 0x6e317d and fs2 == 0 and fe2 == 0xfe and fm2 == 0x47ad0f and rm_val == 0  #nosat<br>                                                                                             |[0x80000580]:fadd.s ft11, ft10, ft9, dyn<br> [0x80000584]:csrrs a7, fflags, zero<br> [0x80000588]:fsw ft11, 320(a5)<br> [0x8000058c]:sw a7, 324(a5)<br> |
|  42|[0x8000245c]<br>0x00000005|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x1b8fcb and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                                                                             |[0x8000059c]:fadd.s ft11, ft10, ft9, dyn<br> [0x800005a0]:csrrs a7, fflags, zero<br> [0x800005a4]:fsw ft11, 328(a5)<br> [0x800005a8]:sw a7, 332(a5)<br> |
|  43|[0x80002464]<br>0x00000005|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x2eabd8 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                                                                             |[0x800005b8]:fadd.s ft11, ft10, ft9, dyn<br> [0x800005bc]:csrrs a7, fflags, zero<br> [0x800005c0]:fsw ft11, 336(a5)<br> [0x800005c4]:sw a7, 340(a5)<br> |
|  44|[0x8000246c]<br>0x00000005|- fs1 == 1 and fe1 == 0xfd and fm1 == 0x6d7424 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x26d2f5 and rm_val == 0  #nosat<br>                                                                                             |[0x800005d4]:fadd.s ft11, ft10, ft9, dyn<br> [0x800005d8]:csrrs a7, fflags, zero<br> [0x800005dc]:fsw ft11, 344(a5)<br> [0x800005e0]:sw a7, 348(a5)<br> |
|  45|[0x80002474]<br>0x00000005|- fs1 == 1 and fe1 == 0xfc and fm1 == 0x587392 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x473a2e and rm_val == 0  #nosat<br>                                                                                             |[0x800005f0]:fadd.s ft11, ft10, ft9, dyn<br> [0x800005f4]:csrrs a7, fflags, zero<br> [0x800005f8]:fsw ft11, 352(a5)<br> [0x800005fc]:sw a7, 356(a5)<br> |
|  46|[0x8000247c]<br>0x00000005|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x2e5b90 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                                                                             |[0x8000060c]:fadd.s ft11, ft10, ft9, dyn<br> [0x80000610]:csrrs a7, fflags, zero<br> [0x80000614]:fsw ft11, 360(a5)<br> [0x80000618]:sw a7, 364(a5)<br> |
|  47|[0x80002484]<br>0x00000005|- fs1 == 1 and fe1 == 0xfd and fm1 == 0x370362 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x4b8415 and rm_val == 0  #nosat<br>                                                                                             |[0x80000628]:fadd.s ft11, ft10, ft9, dyn<br> [0x8000062c]:csrrs a7, fflags, zero<br> [0x80000630]:fsw ft11, 368(a5)<br> [0x80000634]:sw a7, 372(a5)<br> |
|  48|[0x8000248c]<br>0x00000005|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x167d44 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x50b9b1 and rm_val == 0  #nosat<br>                                                                                             |[0x80000644]:fadd.s ft11, ft10, ft9, dyn<br> [0x80000648]:csrrs a7, fflags, zero<br> [0x8000064c]:fsw ft11, 376(a5)<br> [0x80000650]:sw a7, 380(a5)<br> |
|  49|[0x80002494]<br>0x00000005|- fs1 == 1 and fe1 == 0xfd and fm1 == 0x445459 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x2fe97e and rm_val == 0  #nosat<br>                                                                                             |[0x80000660]:fadd.s ft11, ft10, ft9, dyn<br> [0x80000664]:csrrs a7, fflags, zero<br> [0x80000668]:fsw ft11, 384(a5)<br> [0x8000066c]:sw a7, 388(a5)<br> |
|  50|[0x8000249c]<br>0x00000005|- fs1 == 1 and fe1 == 0xfd and fm1 == 0x217fdd and fs2 == 0 and fe2 == 0xfe and fm2 == 0x027635 and rm_val == 0  #nosat<br>                                                                                             |[0x8000067c]:fadd.s ft11, ft10, ft9, dyn<br> [0x80000680]:csrrs a7, fflags, zero<br> [0x80000684]:fsw ft11, 392(a5)<br> [0x80000688]:sw a7, 396(a5)<br> |
|  51|[0x800024a4]<br>0x00000005|- fs1 == 1 and fe1 == 0xfd and fm1 == 0x6b4f07 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x530d37 and rm_val == 0  #nosat<br>                                                                                             |[0x80000698]:fadd.s ft11, ft10, ft9, dyn<br> [0x8000069c]:csrrs a7, fflags, zero<br> [0x800006a0]:fsw ft11, 400(a5)<br> [0x800006a4]:sw a7, 404(a5)<br> |
|  52|[0x800024ac]<br>0x00000005|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f4810 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                                                                             |[0x800006b4]:fadd.s ft11, ft10, ft9, dyn<br> [0x800006b8]:csrrs a7, fflags, zero<br> [0x800006bc]:fsw ft11, 408(a5)<br> [0x800006c0]:sw a7, 412(a5)<br> |
