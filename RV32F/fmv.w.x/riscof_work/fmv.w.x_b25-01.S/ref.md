
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000450')]      |
| SIG_REGION                | [('0x80002210', '0x80002310', '64 words')]      |
| COV_LABELS                | fmv.w.x_b25      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch14/fmv.w.x/riscof_work/fmv.w.x_b25-01.S/ref.S    |
| Total Number of coverpoints| 76     |
| Total Coverpoints Hit     | 72      |
| Total Signature Updates   | 32      |
| STAT1                     | 32      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                           coverpoints                                           |                                                                    code                                                                    |
|---:|--------------------------|-------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002214]<br>0x00000000|- opcode : fmv.w.x<br> - rs1 : x14<br> - rd : f27<br> - rs1_val == 0 and rm_val == 0  #nosat<br> |[0x8000011c]:fmv.w.x fs11, a4<br> [0x80000120]:csrrs a7, fflags, zero<br> [0x80000124]:fsw fs11, 0(a5)<br> [0x80000128]:sw a7, 4(a5)<br>    |
|   2|[0x8000221c]<br>0x00000000|- rs1 : x26<br> - rd : f0<br> - rs1_val == -1227077728 and rm_val == 0  #nosat<br>               |[0x80000134]:fmv.w.x ft0, s10<br> [0x80000138]:csrrs a7, fflags, zero<br> [0x8000013c]:fsw ft0, 8(a5)<br> [0x80000140]:sw a7, 12(a5)<br>    |
|   3|[0x80002224]<br>0x00000000|- rs1 : x13<br> - rd : f31<br> - rs1_val == 1227077728 and rm_val == 0  #nosat<br>               |[0x8000014c]:fmv.w.x ft11, a3<br> [0x80000150]:csrrs a7, fflags, zero<br> [0x80000154]:fsw ft11, 16(a5)<br> [0x80000158]:sw a7, 20(a5)<br>  |
|   4|[0x8000222c]<br>0x00000000|- rs1 : x2<br> - rd : f11<br> - rs1_val == -2147483647 and rm_val == 0  #nosat<br>               |[0x80000164]:fmv.w.x fa1, sp<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:fsw fa1, 24(a5)<br> [0x80000170]:sw a7, 28(a5)<br>    |
|   5|[0x80002234]<br>0x00000000|- rs1 : x25<br> - rd : f15<br> - rs1_val == 2147483647 and rm_val == 0  #nosat<br>               |[0x8000017c]:fmv.w.x fa5, s9<br> [0x80000180]:csrrs a7, fflags, zero<br> [0x80000184]:fsw fa5, 32(a5)<br> [0x80000188]:sw a7, 36(a5)<br>    |
|   6|[0x8000223c]<br>0x00000000|- rs1 : x23<br> - rd : f29<br> - rs1_val == -1 and rm_val == 0  #nosat<br>                       |[0x80000194]:fmv.w.x ft9, s7<br> [0x80000198]:csrrs a7, fflags, zero<br> [0x8000019c]:fsw ft9, 40(a5)<br> [0x800001a0]:sw a7, 44(a5)<br>    |
|   7|[0x80002244]<br>0x00000000|- rs1 : x1<br> - rd : f9<br> - rs1_val == 1 and rm_val == 0  #nosat<br>                          |[0x800001ac]:fmv.w.x fs1, ra<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:fsw fs1, 48(a5)<br> [0x800001b8]:sw a7, 52(a5)<br>    |
|   8|[0x8000224c]<br>0x00000000|- rs1 : x20<br> - rd : f20<br>                                                                   |[0x800001c4]:fmv.w.x fs4, s4<br> [0x800001c8]:csrrs a7, fflags, zero<br> [0x800001cc]:fsw fs4, 56(a5)<br> [0x800001d0]:sw a7, 60(a5)<br>    |
|   9|[0x80002254]<br>0x00000000|- rs1 : x22<br> - rd : f8<br>                                                                    |[0x800001dc]:fmv.w.x fs0, s6<br> [0x800001e0]:csrrs a7, fflags, zero<br> [0x800001e4]:fsw fs0, 64(a5)<br> [0x800001e8]:sw a7, 68(a5)<br>    |
|  10|[0x8000225c]<br>0x00000000|- rs1 : x18<br> - rd : f23<br>                                                                   |[0x800001f4]:fmv.w.x fs7, s2<br> [0x800001f8]:csrrs a7, fflags, zero<br> [0x800001fc]:fsw fs7, 72(a5)<br> [0x80000200]:sw a7, 76(a5)<br>    |
|  11|[0x80002264]<br>0x00000000|- rs1 : x27<br> - rd : f4<br>                                                                    |[0x8000020c]:fmv.w.x ft4, s11<br> [0x80000210]:csrrs a7, fflags, zero<br> [0x80000214]:fsw ft4, 80(a5)<br> [0x80000218]:sw a7, 84(a5)<br>   |
|  12|[0x8000226c]<br>0x00000000|- rs1 : x19<br> - rd : f24<br>                                                                   |[0x80000224]:fmv.w.x fs8, s3<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:fsw fs8, 88(a5)<br> [0x80000230]:sw a7, 92(a5)<br>    |
|  13|[0x80002274]<br>0x00000000|- rs1 : x21<br> - rd : f26<br>                                                                   |[0x8000023c]:fmv.w.x fs10, s5<br> [0x80000240]:csrrs a7, fflags, zero<br> [0x80000244]:fsw fs10, 96(a5)<br> [0x80000248]:sw a7, 100(a5)<br> |
|  14|[0x8000227c]<br>0x00000000|- rs1 : x28<br> - rd : f5<br>                                                                    |[0x80000254]:fmv.w.x ft5, t3<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:fsw ft5, 104(a5)<br> [0x80000260]:sw a7, 108(a5)<br>  |
|  15|[0x80002284]<br>0x00000000|- rs1 : x4<br> - rd : f18<br>                                                                    |[0x8000026c]:fmv.w.x fs2, tp<br> [0x80000270]:csrrs a7, fflags, zero<br> [0x80000274]:fsw fs2, 112(a5)<br> [0x80000278]:sw a7, 116(a5)<br>  |
|  16|[0x8000228c]<br>0x00000000|- rs1 : x6<br> - rd : f21<br>                                                                    |[0x80000284]:fmv.w.x fs5, t1<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:fsw fs5, 120(a5)<br> [0x80000290]:sw a7, 124(a5)<br>  |
|  17|[0x80002294]<br>0x00000000|- rs1 : x15<br> - rd : f30<br>                                                                   |[0x800002a8]:fmv.w.x ft10, a5<br> [0x800002ac]:csrrs s5, fflags, zero<br> [0x800002b0]:fsw ft10, 0(s3)<br> [0x800002b4]:sw s5, 4(s3)<br>    |
|  18|[0x8000229c]<br>0x00000000|- rs1 : x29<br> - rd : f3<br>                                                                    |[0x800002cc]:fmv.w.x ft3, t4<br> [0x800002d0]:csrrs a7, fflags, zero<br> [0x800002d4]:fsw ft3, 0(a5)<br> [0x800002d8]:sw a7, 4(a5)<br>      |
|  19|[0x800022a4]<br>0x00000000|- rs1 : x30<br> - rd : f10<br>                                                                   |[0x800002e4]:fmv.w.x fa0, t5<br> [0x800002e8]:csrrs a7, fflags, zero<br> [0x800002ec]:fsw fa0, 8(a5)<br> [0x800002f0]:sw a7, 12(a5)<br>     |
|  20|[0x800022ac]<br>0x00000000|- rs1 : x0<br> - rd : f17<br>                                                                    |[0x800002fc]:fmv.w.x fa7, zero<br> [0x80000300]:csrrs a7, fflags, zero<br> [0x80000304]:fsw fa7, 16(a5)<br> [0x80000308]:sw a7, 20(a5)<br>  |
|  21|[0x800022b4]<br>0x00000000|- rs1 : x9<br> - rd : f13<br>                                                                    |[0x80000314]:fmv.w.x fa3, s1<br> [0x80000318]:csrrs a7, fflags, zero<br> [0x8000031c]:fsw fa3, 24(a5)<br> [0x80000320]:sw a7, 28(a5)<br>    |
|  22|[0x800022bc]<br>0x00000000|- rs1 : x17<br> - rd : f22<br>                                                                   |[0x80000338]:fmv.w.x fs6, a7<br> [0x8000033c]:csrrs s5, fflags, zero<br> [0x80000340]:fsw fs6, 0(s3)<br> [0x80000344]:sw s5, 4(s3)<br>      |
|  23|[0x800022c4]<br>0x00000000|- rs1 : x11<br> - rd : f1<br>                                                                    |[0x8000035c]:fmv.w.x ft1, a1<br> [0x80000360]:csrrs a7, fflags, zero<br> [0x80000364]:fsw ft1, 0(a5)<br> [0x80000368]:sw a7, 4(a5)<br>      |
|  24|[0x800022cc]<br>0x00000000|- rs1 : x3<br> - rd : f14<br>                                                                    |[0x80000374]:fmv.w.x fa4, gp<br> [0x80000378]:csrrs a7, fflags, zero<br> [0x8000037c]:fsw fa4, 8(a5)<br> [0x80000380]:sw a7, 12(a5)<br>     |
|  25|[0x800022d4]<br>0x00000000|- rs1 : x10<br> - rd : f2<br>                                                                    |[0x8000038c]:fmv.w.x ft2, a0<br> [0x80000390]:csrrs a7, fflags, zero<br> [0x80000394]:fsw ft2, 16(a5)<br> [0x80000398]:sw a7, 20(a5)<br>    |
|  26|[0x800022dc]<br>0x00000000|- rs1 : x31<br> - rd : f7<br>                                                                    |[0x800003a4]:fmv.w.x ft7, t6<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:fsw ft7, 24(a5)<br> [0x800003b0]:sw a7, 28(a5)<br>    |
|  27|[0x800022e4]<br>0x00000000|- rs1 : x12<br> - rd : f12<br>                                                                   |[0x800003bc]:fmv.w.x fa2, a2<br> [0x800003c0]:csrrs a7, fflags, zero<br> [0x800003c4]:fsw fa2, 32(a5)<br> [0x800003c8]:sw a7, 36(a5)<br>    |
|  28|[0x800022ec]<br>0x00000000|- rs1 : x5<br> - rd : f6<br>                                                                     |[0x800003d4]:fmv.w.x ft6, t0<br> [0x800003d8]:csrrs a7, fflags, zero<br> [0x800003dc]:fsw ft6, 40(a5)<br> [0x800003e0]:sw a7, 44(a5)<br>    |
|  29|[0x800022f4]<br>0x00000000|- rs1 : x8<br> - rd : f16<br>                                                                    |[0x800003ec]:fmv.w.x fa6, fp<br> [0x800003f0]:csrrs a7, fflags, zero<br> [0x800003f4]:fsw fa6, 48(a5)<br> [0x800003f8]:sw a7, 52(a5)<br>    |
|  30|[0x800022fc]<br>0x00000000|- rs1 : x7<br> - rd : f19<br>                                                                    |[0x80000404]:fmv.w.x fs3, t2<br> [0x80000408]:csrrs a7, fflags, zero<br> [0x8000040c]:fsw fs3, 56(a5)<br> [0x80000410]:sw a7, 60(a5)<br>    |
|  31|[0x80002304]<br>0x00000000|- rs1 : x24<br> - rd : f25<br>                                                                   |[0x8000041c]:fmv.w.x fs9, s8<br> [0x80000420]:csrrs a7, fflags, zero<br> [0x80000424]:fsw fs9, 64(a5)<br> [0x80000428]:sw a7, 68(a5)<br>    |
|  32|[0x8000230c]<br>0x00000000|- rs1 : x16<br> - rd : f28<br>                                                                   |[0x80000440]:fmv.w.x ft8, a6<br> [0x80000444]:csrrs s5, fflags, zero<br> [0x80000448]:fsw ft8, 0(s3)<br> [0x8000044c]:sw s5, 4(s3)<br>      |
