
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800004c0')]      |
| SIG_REGION                | [('0x80002210', '0x80002330', '72 words')]      |
| COV_LABELS                | fcvt.s.w_b25      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch14/fcvt.s.w/riscof_work/fcvt.s.w_b25-01.S/ref.S    |
| Total Number of coverpoints| 104     |
| Total Coverpoints Hit     | 100      |
| Total Signature Updates   | 36      |
| STAT1                     | 35      |
| STAT2                     | 1      |
| STAT3                     | 0     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000494]:fcvt.s.w ft11, t6, dyn
      [0x80000498]:csrrs a7, fflags, zero
      [0x8000049c]:fsw ft11, 48(a5)
      [0x800004a0]:sw a7, 52(a5)
 -- Signature Address: 0x80002324 Data: 0x00000001
 -- Redundant Coverpoints hit by the op
      - opcode : fcvt.s.w
      - rs1 : x31
      - rd : f31
      - rs1_val == 0 and rm_val == 1  #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                           coverpoints                                            |                                                                       code                                                                        |
|---:|--------------------------|--------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002214]<br>0x00000000|- opcode : fcvt.s.w<br> - rs1 : x11<br> - rd : f30<br> - rs1_val == 0 and rm_val == 0  #nosat<br> |[0x8000011c]:fcvt.s.w ft10, a1, dyn<br> [0x80000120]:csrrs a7, fflags, zero<br> [0x80000124]:fsw ft10, 0(a5)<br> [0x80000128]:sw a7, 4(a5)<br>     |
|   2|[0x8000221c]<br>0x00000001|- rs1 : x29<br> - rd : f21<br> - rs1_val == -1227077728 and rm_val == 4  #nosat<br>               |[0x80000134]:fcvt.s.w fs5, t4, dyn<br> [0x80000138]:csrrs a7, fflags, zero<br> [0x8000013c]:fsw fs5, 8(a5)<br> [0x80000140]:sw a7, 12(a5)<br>      |
|   3|[0x80002224]<br>0x00000001|- rs1 : x10<br> - rd : f20<br> - rs1_val == -1227077728 and rm_val == 3  #nosat<br>               |[0x8000014c]:fcvt.s.w fs4, a0, dyn<br> [0x80000150]:csrrs a7, fflags, zero<br> [0x80000154]:fsw fs4, 16(a5)<br> [0x80000158]:sw a7, 20(a5)<br>     |
|   4|[0x8000222c]<br>0x00000001|- rs1 : x27<br> - rd : f14<br> - rs1_val == -1227077728 and rm_val == 2  #nosat<br>               |[0x80000164]:fcvt.s.w fa4, s11, dyn<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:fsw fa4, 24(a5)<br> [0x80000170]:sw a7, 28(a5)<br>    |
|   5|[0x80002234]<br>0x00000001|- rs1 : x6<br> - rd : f28<br> - rs1_val == -1227077728 and rm_val == 1  #nosat<br>                |[0x8000017c]:fcvt.s.w ft8, t1, dyn<br> [0x80000180]:csrrs a7, fflags, zero<br> [0x80000184]:fsw ft8, 32(a5)<br> [0x80000188]:sw a7, 36(a5)<br>     |
|   6|[0x8000223c]<br>0x00000001|- rs1 : x26<br> - rd : f25<br> - rs1_val == -1227077728 and rm_val == 0  #nosat<br>               |[0x80000194]:fcvt.s.w fs9, s10, dyn<br> [0x80000198]:csrrs a7, fflags, zero<br> [0x8000019c]:fsw fs9, 40(a5)<br> [0x800001a0]:sw a7, 44(a5)<br>    |
|   7|[0x80002244]<br>0x00000001|- rs1 : x25<br> - rd : f13<br> - rs1_val == 1227077728 and rm_val == 4  #nosat<br>                |[0x800001ac]:fcvt.s.w fa3, s9, dyn<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:fsw fa3, 48(a5)<br> [0x800001b8]:sw a7, 52(a5)<br>     |
|   8|[0x8000224c]<br>0x00000001|- rs1 : x18<br> - rd : f11<br> - rs1_val == 1227077728 and rm_val == 3  #nosat<br>                |[0x800001c4]:fcvt.s.w fa1, s2, dyn<br> [0x800001c8]:csrrs a7, fflags, zero<br> [0x800001cc]:fsw fa1, 56(a5)<br> [0x800001d0]:sw a7, 60(a5)<br>     |
|   9|[0x80002254]<br>0x00000001|- rs1 : x31<br> - rd : f7<br> - rs1_val == 1227077728 and rm_val == 2  #nosat<br>                 |[0x800001dc]:fcvt.s.w ft7, t6, dyn<br> [0x800001e0]:csrrs a7, fflags, zero<br> [0x800001e4]:fsw ft7, 64(a5)<br> [0x800001e8]:sw a7, 68(a5)<br>     |
|  10|[0x8000225c]<br>0x00000001|- rs1 : x30<br> - rd : f2<br> - rs1_val == 1227077728 and rm_val == 1  #nosat<br>                 |[0x800001f4]:fcvt.s.w ft2, t5, dyn<br> [0x800001f8]:csrrs a7, fflags, zero<br> [0x800001fc]:fsw ft2, 72(a5)<br> [0x80000200]:sw a7, 76(a5)<br>     |
|  11|[0x80002264]<br>0x00000001|- rs1 : x21<br> - rd : f9<br> - rs1_val == 1227077728 and rm_val == 0  #nosat<br>                 |[0x8000020c]:fcvt.s.w fs1, s5, dyn<br> [0x80000210]:csrrs a7, fflags, zero<br> [0x80000214]:fsw fs1, 80(a5)<br> [0x80000218]:sw a7, 84(a5)<br>     |
|  12|[0x8000226c]<br>0x00000001|- rs1 : x12<br> - rd : f5<br> - rs1_val == -2147483647 and rm_val == 4  #nosat<br>                |[0x80000224]:fcvt.s.w ft5, a2, dyn<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:fsw ft5, 88(a5)<br> [0x80000230]:sw a7, 92(a5)<br>     |
|  13|[0x80002274]<br>0x00000001|- rs1 : x7<br> - rd : f15<br> - rs1_val == -2147483647 and rm_val == 3  #nosat<br>                |[0x8000023c]:fcvt.s.w fa5, t2, dyn<br> [0x80000240]:csrrs a7, fflags, zero<br> [0x80000244]:fsw fa5, 96(a5)<br> [0x80000248]:sw a7, 100(a5)<br>    |
|  14|[0x8000227c]<br>0x00000001|- rs1 : x5<br> - rd : f17<br> - rs1_val == -2147483647 and rm_val == 2  #nosat<br>                |[0x80000254]:fcvt.s.w fa7, t0, dyn<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:fsw fa7, 104(a5)<br> [0x80000260]:sw a7, 108(a5)<br>   |
|  15|[0x80002284]<br>0x00000001|- rs1 : x0<br> - rd : f6<br> - rs1_val == 0 and rm_val == 1  #nosat<br>                           |[0x8000026c]:fcvt.s.w ft6, zero, dyn<br> [0x80000270]:csrrs a7, fflags, zero<br> [0x80000274]:fsw ft6, 112(a5)<br> [0x80000278]:sw a7, 116(a5)<br> |
|  16|[0x8000228c]<br>0x00000001|- rs1 : x13<br> - rd : f8<br> - rs1_val == -2147483647 and rm_val == 0  #nosat<br>                |[0x80000284]:fcvt.s.w fs0, a3, dyn<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:fsw fs0, 120(a5)<br> [0x80000290]:sw a7, 124(a5)<br>   |
|  17|[0x80002294]<br>0x00000001|- rs1 : x19<br> - rd : f4<br> - rs1_val == 2147483647 and rm_val == 4  #nosat<br>                 |[0x8000029c]:fcvt.s.w ft4, s3, dyn<br> [0x800002a0]:csrrs a7, fflags, zero<br> [0x800002a4]:fsw ft4, 128(a5)<br> [0x800002a8]:sw a7, 132(a5)<br>   |
|  18|[0x8000229c]<br>0x00000001|- rs1 : x22<br> - rd : f12<br> - rs1_val == 2147483647 and rm_val == 3  #nosat<br>                |[0x800002b4]:fcvt.s.w fa2, s6, dyn<br> [0x800002b8]:csrrs a7, fflags, zero<br> [0x800002bc]:fsw fa2, 136(a5)<br> [0x800002c0]:sw a7, 140(a5)<br>   |
|  19|[0x800022a4]<br>0x00000001|- rs1 : x15<br> - rd : f29<br> - rs1_val == 2147483647 and rm_val == 2  #nosat<br>                |[0x800002d8]:fcvt.s.w ft9, a5, dyn<br> [0x800002dc]:csrrs s5, fflags, zero<br> [0x800002e0]:fsw ft9, 0(s3)<br> [0x800002e4]:sw s5, 4(s3)<br>       |
|  20|[0x800022ac]<br>0x00000001|- rs1 : x23<br> - rd : f23<br> - rs1_val == 2147483647 and rm_val == 1  #nosat<br>                |[0x800002fc]:fcvt.s.w fs7, s7, dyn<br> [0x80000300]:csrrs a7, fflags, zero<br> [0x80000304]:fsw fs7, 0(a5)<br> [0x80000308]:sw a7, 4(a5)<br>       |
|  21|[0x800022b4]<br>0x00000001|- rs1 : x28<br> - rd : f31<br> - rs1_val == 2147483647 and rm_val == 0  #nosat<br>                |[0x80000314]:fcvt.s.w ft11, t3, dyn<br> [0x80000318]:csrrs a7, fflags, zero<br> [0x8000031c]:fsw ft11, 8(a5)<br> [0x80000320]:sw a7, 12(a5)<br>    |
|  22|[0x800022bc]<br>0x00000001|- rs1 : x4<br> - rd : f10<br> - rs1_val == -1 and rm_val == 4  #nosat<br>                         |[0x8000032c]:fcvt.s.w fa0, tp, dyn<br> [0x80000330]:csrrs a7, fflags, zero<br> [0x80000334]:fsw fa0, 16(a5)<br> [0x80000338]:sw a7, 20(a5)<br>     |
|  23|[0x800022c4]<br>0x00000001|- rs1 : x17<br> - rd : f26<br> - rs1_val == -1 and rm_val == 3  #nosat<br>                        |[0x80000350]:fcvt.s.w fs10, a7, dyn<br> [0x80000354]:csrrs s5, fflags, zero<br> [0x80000358]:fsw fs10, 0(s3)<br> [0x8000035c]:sw s5, 4(s3)<br>     |
|  24|[0x800022cc]<br>0x00000001|- rs1 : x20<br> - rd : f16<br> - rs1_val == -1 and rm_val == 2  #nosat<br>                        |[0x80000374]:fcvt.s.w fa6, s4, dyn<br> [0x80000378]:csrrs a7, fflags, zero<br> [0x8000037c]:fsw fa6, 0(a5)<br> [0x80000380]:sw a7, 4(a5)<br>       |
|  25|[0x800022d4]<br>0x00000001|- rs1 : x3<br> - rd : f1<br> - rs1_val == -1 and rm_val == 1  #nosat<br>                          |[0x8000038c]:fcvt.s.w ft1, gp, dyn<br> [0x80000390]:csrrs a7, fflags, zero<br> [0x80000394]:fsw ft1, 8(a5)<br> [0x80000398]:sw a7, 12(a5)<br>      |
|  26|[0x800022dc]<br>0x00000001|- rs1 : x14<br> - rd : f19<br> - rs1_val == -1 and rm_val == 0  #nosat<br>                        |[0x800003a4]:fcvt.s.w fs3, a4, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:fsw fs3, 16(a5)<br> [0x800003b0]:sw a7, 20(a5)<br>     |
|  27|[0x800022e4]<br>0x00000001|- rs1 : x8<br> - rd : f0<br> - rs1_val == 1 and rm_val == 4  #nosat<br>                           |[0x800003bc]:fcvt.s.w ft0, fp, dyn<br> [0x800003c0]:csrrs a7, fflags, zero<br> [0x800003c4]:fsw ft0, 24(a5)<br> [0x800003c8]:sw a7, 28(a5)<br>     |
|  28|[0x800022ec]<br>0x00000001|- rs1 : x16<br> - rd : f24<br> - rs1_val == 1 and rm_val == 3  #nosat<br>                         |[0x800003e0]:fcvt.s.w fs8, a6, dyn<br> [0x800003e4]:csrrs s5, fflags, zero<br> [0x800003e8]:fsw fs8, 0(s3)<br> [0x800003ec]:sw s5, 4(s3)<br>       |
|  29|[0x800022f4]<br>0x00000001|- rs1 : x2<br> - rd : f18<br> - rs1_val == 1 and rm_val == 2  #nosat<br>                          |[0x80000404]:fcvt.s.w fs2, sp, dyn<br> [0x80000408]:csrrs a7, fflags, zero<br> [0x8000040c]:fsw fs2, 0(a5)<br> [0x80000410]:sw a7, 4(a5)<br>       |
|  30|[0x800022fc]<br>0x00000001|- rs1 : x1<br> - rd : f3<br> - rs1_val == 1 and rm_val == 1  #nosat<br>                           |[0x8000041c]:fcvt.s.w ft3, ra, dyn<br> [0x80000420]:csrrs a7, fflags, zero<br> [0x80000424]:fsw ft3, 8(a5)<br> [0x80000428]:sw a7, 12(a5)<br>      |
|  31|[0x80002304]<br>0x00000001|- rs1 : x24<br> - rd : f27<br> - rs1_val == 1 and rm_val == 0  #nosat<br>                         |[0x80000434]:fcvt.s.w fs11, s8, dyn<br> [0x80000438]:csrrs a7, fflags, zero<br> [0x8000043c]:fsw fs11, 16(a5)<br> [0x80000440]:sw a7, 20(a5)<br>   |
|  32|[0x8000230c]<br>0x00000001|- rs1 : x9<br> - rd : f22<br> - rs1_val == 0 and rm_val == 4  #nosat<br>                          |[0x8000044c]:fcvt.s.w fs6, s1, dyn<br> [0x80000450]:csrrs a7, fflags, zero<br> [0x80000454]:fsw fs6, 24(a5)<br> [0x80000458]:sw a7, 28(a5)<br>     |
|  33|[0x80002314]<br>0x00000001|- rs1_val == 0 and rm_val == 3  #nosat<br>                                                        |[0x80000464]:fcvt.s.w ft11, t6, dyn<br> [0x80000468]:csrrs a7, fflags, zero<br> [0x8000046c]:fsw ft11, 32(a5)<br> [0x80000470]:sw a7, 36(a5)<br>   |
|  34|[0x8000231c]<br>0x00000001|- rs1_val == 0 and rm_val == 2  #nosat<br>                                                        |[0x8000047c]:fcvt.s.w ft11, t6, dyn<br> [0x80000480]:csrrs a7, fflags, zero<br> [0x80000484]:fsw ft11, 40(a5)<br> [0x80000488]:sw a7, 44(a5)<br>   |
|  35|[0x8000232c]<br>0x00000001|- rs1_val == -2147483647 and rm_val == 1  #nosat<br>                                              |[0x800004ac]:fcvt.s.w ft11, t6, dyn<br> [0x800004b0]:csrrs a7, fflags, zero<br> [0x800004b4]:fsw ft11, 56(a5)<br> [0x800004b8]:sw a7, 60(a5)<br>   |
