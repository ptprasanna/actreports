
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000460')]      |
| SIG_REGION                | [('0x80002210', '0x80002310', '64 words')]      |
| COV_LABELS                | fmv.x.w_b22      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch14/fmv.x.w/riscof_work/fmv.x.w_b22-01.S/ref.S    |
| Total Number of coverpoints| 77     |
| Total Coverpoints Hit     | 73      |
| Total Signature Updates   | 64      |
| STAT1                     | 32      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 32     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['opcode : fmv.x.w', 'rd : x25', 'rs1 : f15', 'fs1 == 0 and fe1 == 0x7c and fm1 == 0x4923b8 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000011c]:fmv.x.w s9, fa5
	-[0x80000120]:csrrs a7, fflags, zero
	-[0x80000124]:sw s9, 0(a5)
Current Store : [0x80000128] : sw a7, 4(a5) -- Store: [0x80002214]:0x00000000




Last Coverpoint : ['rd : x20', 'rs1 : f20', 'fs1 == 0 and fe1 == 0xc4 and fm1 == 0x046756 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000134]:fmv.x.w s4, fs4
	-[0x80000138]:csrrs a7, fflags, zero
	-[0x8000013c]:sw s4, 8(a5)
Current Store : [0x80000140] : sw a7, 12(a5) -- Store: [0x8000221c]:0x00000000




Last Coverpoint : ['rd : x7', 'rs1 : f10', 'fs1 == 0 and fe1 == 0x67 and fm1 == 0x53a4fc and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000014c]:fmv.x.w t2, fa0
	-[0x80000150]:csrrs a7, fflags, zero
	-[0x80000154]:sw t2, 16(a5)
Current Store : [0x80000158] : sw a7, 20(a5) -- Store: [0x80002224]:0x00000000




Last Coverpoint : ['rd : x11', 'rs1 : f18', 'fs1 == 0 and fe1 == 0x81 and fm1 == 0x696b5c and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000164]:fmv.x.w a1, fs2
	-[0x80000168]:csrrs a7, fflags, zero
	-[0x8000016c]:sw a1, 24(a5)
Current Store : [0x80000170] : sw a7, 28(a5) -- Store: [0x8000222c]:0x00000000




Last Coverpoint : ['rd : x12', 'rs1 : f7', 'fs1 == 0 and fe1 == 0x80 and fm1 == 0x681ae9 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000017c]:fmv.x.w a2, ft7
	-[0x80000180]:csrrs a7, fflags, zero
	-[0x80000184]:sw a2, 32(a5)
Current Store : [0x80000188] : sw a7, 36(a5) -- Store: [0x80002234]:0x00000000




Last Coverpoint : ['rd : x21', 'rs1 : f27', 'fs1 == 0 and fe1 == 0x7f and fm1 == 0x1a616d and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000194]:fmv.x.w s5, fs11
	-[0x80000198]:csrrs a7, fflags, zero
	-[0x8000019c]:sw s5, 40(a5)
Current Store : [0x800001a0] : sw a7, 44(a5) -- Store: [0x8000223c]:0x00000000




Last Coverpoint : ['rd : x30', 'rs1 : f14', 'fs1 == 0 and fe1 == 0x7e and fm1 == 0x49fee5 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001ac]:fmv.x.w t5, fa4
	-[0x800001b0]:csrrs a7, fflags, zero
	-[0x800001b4]:sw t5, 48(a5)
Current Store : [0x800001b8] : sw a7, 52(a5) -- Store: [0x80002244]:0x00000000




Last Coverpoint : ['rd : x8', 'rs1 : f30', 'fs1 == 0 and fe1 == 0x7d and fm1 == 0x36e5d6 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001c4]:fmv.x.w fp, ft10
	-[0x800001c8]:csrrs a7, fflags, zero
	-[0x800001cc]:sw fp, 56(a5)
Current Store : [0x800001d0] : sw a7, 60(a5) -- Store: [0x8000224c]:0x00000000




Last Coverpoint : ['rd : x17', 'rs1 : f8']
Last Code Sequence : 
	-[0x800001e8]:fmv.x.w a7, fs0
	-[0x800001ec]:csrrs s5, fflags, zero
	-[0x800001f0]:sw a7, 0(s3)
Current Store : [0x800001f4] : sw s5, 4(s3) -- Store: [0x80002254]:0x00000000




Last Coverpoint : ['rd : x27', 'rs1 : f3']
Last Code Sequence : 
	-[0x8000020c]:fmv.x.w s11, ft3
	-[0x80000210]:csrrs a7, fflags, zero
	-[0x80000214]:sw s11, 0(a5)
Current Store : [0x80000218] : sw a7, 4(a5) -- Store: [0x8000225c]:0x00000000




Last Coverpoint : ['rd : x22', 'rs1 : f11']
Last Code Sequence : 
	-[0x80000224]:fmv.x.w s6, fa1
	-[0x80000228]:csrrs a7, fflags, zero
	-[0x8000022c]:sw s6, 8(a5)
Current Store : [0x80000230] : sw a7, 12(a5) -- Store: [0x80002264]:0x00000000




Last Coverpoint : ['rd : x2', 'rs1 : f12']
Last Code Sequence : 
	-[0x8000023c]:fmv.x.w sp, fa2
	-[0x80000240]:csrrs a7, fflags, zero
	-[0x80000244]:sw sp, 16(a5)
Current Store : [0x80000248] : sw a7, 20(a5) -- Store: [0x8000226c]:0x00000000




Last Coverpoint : ['rd : x5', 'rs1 : f28']
Last Code Sequence : 
	-[0x80000254]:fmv.x.w t0, ft8
	-[0x80000258]:csrrs a7, fflags, zero
	-[0x8000025c]:sw t0, 24(a5)
Current Store : [0x80000260] : sw a7, 28(a5) -- Store: [0x80002274]:0x00000000




Last Coverpoint : ['rd : x19', 'rs1 : f1']
Last Code Sequence : 
	-[0x8000026c]:fmv.x.w s3, ft1
	-[0x80000270]:csrrs a7, fflags, zero
	-[0x80000274]:sw s3, 32(a5)
Current Store : [0x80000278] : sw a7, 36(a5) -- Store: [0x8000227c]:0x00000000




Last Coverpoint : ['rd : x0', 'rs1 : f22']
Last Code Sequence : 
	-[0x80000284]:fmv.x.w zero, fs6
	-[0x80000288]:csrrs a7, fflags, zero
	-[0x8000028c]:sw zero, 40(a5)
Current Store : [0x80000290] : sw a7, 44(a5) -- Store: [0x80002284]:0x00000000




Last Coverpoint : ['rd : x23', 'rs1 : f23']
Last Code Sequence : 
	-[0x8000029c]:fmv.x.w s7, fs7
	-[0x800002a0]:csrrs a7, fflags, zero
	-[0x800002a4]:sw s7, 48(a5)
Current Store : [0x800002a8] : sw a7, 52(a5) -- Store: [0x8000228c]:0x00000000




Last Coverpoint : ['rd : x29', 'rs1 : f2']
Last Code Sequence : 
	-[0x800002b4]:fmv.x.w t4, ft2
	-[0x800002b8]:csrrs a7, fflags, zero
	-[0x800002bc]:sw t4, 56(a5)
Current Store : [0x800002c0] : sw a7, 60(a5) -- Store: [0x80002294]:0x00000000




Last Coverpoint : ['rd : x24', 'rs1 : f0']
Last Code Sequence : 
	-[0x800002cc]:fmv.x.w s8, ft0
	-[0x800002d0]:csrrs a7, fflags, zero
	-[0x800002d4]:sw s8, 64(a5)
Current Store : [0x800002d8] : sw a7, 68(a5) -- Store: [0x8000229c]:0x00000000




Last Coverpoint : ['rd : x15', 'rs1 : f24']
Last Code Sequence : 
	-[0x800002f0]:fmv.x.w a5, fs8
	-[0x800002f4]:csrrs s5, fflags, zero
	-[0x800002f8]:sw a5, 0(s3)
Current Store : [0x800002fc] : sw s5, 4(s3) -- Store: [0x800022a4]:0x00000000




Last Coverpoint : ['rd : x10', 'rs1 : f17']
Last Code Sequence : 
	-[0x80000314]:fmv.x.w a0, fa7
	-[0x80000318]:csrrs a7, fflags, zero
	-[0x8000031c]:sw a0, 0(a5)
Current Store : [0x80000320] : sw a7, 4(a5) -- Store: [0x800022ac]:0x00000000




Last Coverpoint : ['rd : x4', 'rs1 : f29']
Last Code Sequence : 
	-[0x8000032c]:fmv.x.w tp, ft9
	-[0x80000330]:csrrs a7, fflags, zero
	-[0x80000334]:sw tp, 8(a5)
Current Store : [0x80000338] : sw a7, 12(a5) -- Store: [0x800022b4]:0x00000000




Last Coverpoint : ['rd : x28', 'rs1 : f4']
Last Code Sequence : 
	-[0x80000344]:fmv.x.w t3, ft4
	-[0x80000348]:csrrs a7, fflags, zero
	-[0x8000034c]:sw t3, 16(a5)
Current Store : [0x80000350] : sw a7, 20(a5) -- Store: [0x800022bc]:0x00000000




Last Coverpoint : ['rd : x31', 'rs1 : f19']
Last Code Sequence : 
	-[0x8000035c]:fmv.x.w t6, fs3
	-[0x80000360]:csrrs a7, fflags, zero
	-[0x80000364]:sw t6, 24(a5)
Current Store : [0x80000368] : sw a7, 28(a5) -- Store: [0x800022c4]:0x00000000




Last Coverpoint : ['rd : x18', 'rs1 : f26']
Last Code Sequence : 
	-[0x80000374]:fmv.x.w s2, fs10
	-[0x80000378]:csrrs a7, fflags, zero
	-[0x8000037c]:sw s2, 32(a5)
Current Store : [0x80000380] : sw a7, 36(a5) -- Store: [0x800022cc]:0x00000000




Last Coverpoint : ['rd : x1', 'rs1 : f6']
Last Code Sequence : 
	-[0x8000038c]:fmv.x.w ra, ft6
	-[0x80000390]:csrrs a7, fflags, zero
	-[0x80000394]:sw ra, 40(a5)
Current Store : [0x80000398] : sw a7, 44(a5) -- Store: [0x800022d4]:0x00000000




Last Coverpoint : ['rd : x26', 'rs1 : f16']
Last Code Sequence : 
	-[0x800003a4]:fmv.x.w s10, fa6
	-[0x800003a8]:csrrs a7, fflags, zero
	-[0x800003ac]:sw s10, 48(a5)
Current Store : [0x800003b0] : sw a7, 52(a5) -- Store: [0x800022dc]:0x00000000




Last Coverpoint : ['rd : x3', 'rs1 : f13']
Last Code Sequence : 
	-[0x800003bc]:fmv.x.w gp, fa3
	-[0x800003c0]:csrrs a7, fflags, zero
	-[0x800003c4]:sw gp, 56(a5)
Current Store : [0x800003c8] : sw a7, 60(a5) -- Store: [0x800022e4]:0x00000000




Last Coverpoint : ['rd : x9', 'rs1 : f5']
Last Code Sequence : 
	-[0x800003d4]:fmv.x.w s1, ft5
	-[0x800003d8]:csrrs a7, fflags, zero
	-[0x800003dc]:sw s1, 64(a5)
Current Store : [0x800003e0] : sw a7, 68(a5) -- Store: [0x800022ec]:0x00000000




Last Coverpoint : ['rd : x16', 'rs1 : f31']
Last Code Sequence : 
	-[0x800003f8]:fmv.x.w a6, ft11
	-[0x800003fc]:csrrs s5, fflags, zero
	-[0x80000400]:sw a6, 0(s3)
Current Store : [0x80000404] : sw s5, 4(s3) -- Store: [0x800022f4]:0x00000000




Last Coverpoint : ['rd : x6', 'rs1 : f21']
Last Code Sequence : 
	-[0x8000041c]:fmv.x.w t1, fs5
	-[0x80000420]:csrrs a7, fflags, zero
	-[0x80000424]:sw t1, 0(a5)
Current Store : [0x80000428] : sw a7, 4(a5) -- Store: [0x800022fc]:0x00000000




Last Coverpoint : ['rd : x13', 'rs1 : f25']
Last Code Sequence : 
	-[0x80000434]:fmv.x.w a3, fs9
	-[0x80000438]:csrrs a7, fflags, zero
	-[0x8000043c]:sw a3, 8(a5)
Current Store : [0x80000440] : sw a7, 12(a5) -- Store: [0x80002304]:0x00000000




Last Coverpoint : ['rd : x14', 'rs1 : f9']
Last Code Sequence : 
	-[0x8000044c]:fmv.x.w a4, fs1
	-[0x80000450]:csrrs a7, fflags, zero
	-[0x80000454]:sw a4, 16(a5)
Current Store : [0x80000458] : sw a7, 20(a5) -- Store: [0x8000230c]:0x00000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                           coverpoints                                                           |                                                    code                                                    |
|---:|--------------------------|---------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------|
|   1|[0x80002210]<br>0x3E4923B8|- opcode : fmv.x.w<br> - rd : x25<br> - rs1 : f15<br> - fs1 == 0 and fe1 == 0x7c and fm1 == 0x4923b8 and rm_val == 0  #nosat<br> |[0x8000011c]:fmv.x.w s9, fa5<br> [0x80000120]:csrrs a7, fflags, zero<br> [0x80000124]:sw s9, 0(a5)<br>      |
|   2|[0x80002218]<br>0x62046756|- rd : x20<br> - rs1 : f20<br> - fs1 == 0 and fe1 == 0xc4 and fm1 == 0x046756 and rm_val == 0  #nosat<br>                        |[0x80000134]:fmv.x.w s4, fs4<br> [0x80000138]:csrrs a7, fflags, zero<br> [0x8000013c]:sw s4, 8(a5)<br>      |
|   3|[0x80002220]<br>0x33D3A4FC|- rd : x7<br> - rs1 : f10<br> - fs1 == 0 and fe1 == 0x67 and fm1 == 0x53a4fc and rm_val == 0  #nosat<br>                         |[0x8000014c]:fmv.x.w t2, fa0<br> [0x80000150]:csrrs a7, fflags, zero<br> [0x80000154]:sw t2, 16(a5)<br>     |
|   4|[0x80002228]<br>0x40E96B5C|- rd : x11<br> - rs1 : f18<br> - fs1 == 0 and fe1 == 0x81 and fm1 == 0x696b5c and rm_val == 0  #nosat<br>                        |[0x80000164]:fmv.x.w a1, fs2<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:sw a1, 24(a5)<br>     |
|   5|[0x80002230]<br>0x40681AE9|- rd : x12<br> - rs1 : f7<br> - fs1 == 0 and fe1 == 0x80 and fm1 == 0x681ae9 and rm_val == 0  #nosat<br>                         |[0x8000017c]:fmv.x.w a2, ft7<br> [0x80000180]:csrrs a7, fflags, zero<br> [0x80000184]:sw a2, 32(a5)<br>     |
|   6|[0x80002238]<br>0x3F9A616D|- rd : x21<br> - rs1 : f27<br> - fs1 == 0 and fe1 == 0x7f and fm1 == 0x1a616d and rm_val == 0  #nosat<br>                        |[0x80000194]:fmv.x.w s5, fs11<br> [0x80000198]:csrrs a7, fflags, zero<br> [0x8000019c]:sw s5, 40(a5)<br>    |
|   7|[0x80002240]<br>0x3F49FEE5|- rd : x30<br> - rs1 : f14<br> - fs1 == 0 and fe1 == 0x7e and fm1 == 0x49fee5 and rm_val == 0  #nosat<br>                        |[0x800001ac]:fmv.x.w t5, fa4<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:sw t5, 48(a5)<br>     |
|   8|[0x80002248]<br>0x3EB6E5D6|- rd : x8<br> - rs1 : f30<br> - fs1 == 0 and fe1 == 0x7d and fm1 == 0x36e5d6 and rm_val == 0  #nosat<br>                         |[0x800001c4]:fmv.x.w fp, ft10<br> [0x800001c8]:csrrs a7, fflags, zero<br> [0x800001cc]:sw fp, 56(a5)<br>    |
|   9|[0x80002250]<br>0x00000000|- rd : x17<br> - rs1 : f8<br>                                                                                                    |[0x800001e8]:fmv.x.w a7, fs0<br> [0x800001ec]:csrrs s5, fflags, zero<br> [0x800001f0]:sw a7, 0(s3)<br>      |
|  10|[0x80002258]<br>0x00000000|- rd : x27<br> - rs1 : f3<br>                                                                                                    |[0x8000020c]:fmv.x.w s11, ft3<br> [0x80000210]:csrrs a7, fflags, zero<br> [0x80000214]:sw s11, 0(a5)<br>    |
|  11|[0x80002260]<br>0x00000000|- rd : x22<br> - rs1 : f11<br>                                                                                                   |[0x80000224]:fmv.x.w s6, fa1<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:sw s6, 8(a5)<br>      |
|  12|[0x80002268]<br>0x00000000|- rd : x2<br> - rs1 : f12<br>                                                                                                    |[0x8000023c]:fmv.x.w sp, fa2<br> [0x80000240]:csrrs a7, fflags, zero<br> [0x80000244]:sw sp, 16(a5)<br>     |
|  13|[0x80002270]<br>0x00000000|- rd : x5<br> - rs1 : f28<br>                                                                                                    |[0x80000254]:fmv.x.w t0, ft8<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:sw t0, 24(a5)<br>     |
|  14|[0x80002278]<br>0x00000000|- rd : x19<br> - rs1 : f1<br>                                                                                                    |[0x8000026c]:fmv.x.w s3, ft1<br> [0x80000270]:csrrs a7, fflags, zero<br> [0x80000274]:sw s3, 32(a5)<br>     |
|  15|[0x80002280]<br>0x00000000|- rd : x0<br> - rs1 : f22<br>                                                                                                    |[0x80000284]:fmv.x.w zero, fs6<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:sw zero, 40(a5)<br> |
|  16|[0x80002288]<br>0x00000000|- rd : x23<br> - rs1 : f23<br>                                                                                                   |[0x8000029c]:fmv.x.w s7, fs7<br> [0x800002a0]:csrrs a7, fflags, zero<br> [0x800002a4]:sw s7, 48(a5)<br>     |
|  17|[0x80002290]<br>0x00000000|- rd : x29<br> - rs1 : f2<br>                                                                                                    |[0x800002b4]:fmv.x.w t4, ft2<br> [0x800002b8]:csrrs a7, fflags, zero<br> [0x800002bc]:sw t4, 56(a5)<br>     |
|  18|[0x80002298]<br>0x00000000|- rd : x24<br> - rs1 : f0<br>                                                                                                    |[0x800002cc]:fmv.x.w s8, ft0<br> [0x800002d0]:csrrs a7, fflags, zero<br> [0x800002d4]:sw s8, 64(a5)<br>     |
|  19|[0x800022a0]<br>0x00000000|- rd : x15<br> - rs1 : f24<br>                                                                                                   |[0x800002f0]:fmv.x.w a5, fs8<br> [0x800002f4]:csrrs s5, fflags, zero<br> [0x800002f8]:sw a5, 0(s3)<br>      |
|  20|[0x800022a8]<br>0x00000000|- rd : x10<br> - rs1 : f17<br>                                                                                                   |[0x80000314]:fmv.x.w a0, fa7<br> [0x80000318]:csrrs a7, fflags, zero<br> [0x8000031c]:sw a0, 0(a5)<br>      |
|  21|[0x800022b0]<br>0x00000000|- rd : x4<br> - rs1 : f29<br>                                                                                                    |[0x8000032c]:fmv.x.w tp, ft9<br> [0x80000330]:csrrs a7, fflags, zero<br> [0x80000334]:sw tp, 8(a5)<br>      |
|  22|[0x800022b8]<br>0x00000000|- rd : x28<br> - rs1 : f4<br>                                                                                                    |[0x80000344]:fmv.x.w t3, ft4<br> [0x80000348]:csrrs a7, fflags, zero<br> [0x8000034c]:sw t3, 16(a5)<br>     |
|  23|[0x800022c0]<br>0x00000000|- rd : x31<br> - rs1 : f19<br>                                                                                                   |[0x8000035c]:fmv.x.w t6, fs3<br> [0x80000360]:csrrs a7, fflags, zero<br> [0x80000364]:sw t6, 24(a5)<br>     |
|  24|[0x800022c8]<br>0x00000000|- rd : x18<br> - rs1 : f26<br>                                                                                                   |[0x80000374]:fmv.x.w s2, fs10<br> [0x80000378]:csrrs a7, fflags, zero<br> [0x8000037c]:sw s2, 32(a5)<br>    |
|  25|[0x800022d0]<br>0x00000000|- rd : x1<br> - rs1 : f6<br>                                                                                                     |[0x8000038c]:fmv.x.w ra, ft6<br> [0x80000390]:csrrs a7, fflags, zero<br> [0x80000394]:sw ra, 40(a5)<br>     |
|  26|[0x800022d8]<br>0x00000000|- rd : x26<br> - rs1 : f16<br>                                                                                                   |[0x800003a4]:fmv.x.w s10, fa6<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:sw s10, 48(a5)<br>   |
|  27|[0x800022e0]<br>0x00000000|- rd : x3<br> - rs1 : f13<br>                                                                                                    |[0x800003bc]:fmv.x.w gp, fa3<br> [0x800003c0]:csrrs a7, fflags, zero<br> [0x800003c4]:sw gp, 56(a5)<br>     |
|  28|[0x800022e8]<br>0x00000000|- rd : x9<br> - rs1 : f5<br>                                                                                                     |[0x800003d4]:fmv.x.w s1, ft5<br> [0x800003d8]:csrrs a7, fflags, zero<br> [0x800003dc]:sw s1, 64(a5)<br>     |
|  29|[0x800022f0]<br>0x00000000|- rd : x16<br> - rs1 : f31<br>                                                                                                   |[0x800003f8]:fmv.x.w a6, ft11<br> [0x800003fc]:csrrs s5, fflags, zero<br> [0x80000400]:sw a6, 0(s3)<br>     |
|  30|[0x800022f8]<br>0x00000000|- rd : x6<br> - rs1 : f21<br>                                                                                                    |[0x8000041c]:fmv.x.w t1, fs5<br> [0x80000420]:csrrs a7, fflags, zero<br> [0x80000424]:sw t1, 0(a5)<br>      |
|  31|[0x80002300]<br>0x00000000|- rd : x13<br> - rs1 : f25<br>                                                                                                   |[0x80000434]:fmv.x.w a3, fs9<br> [0x80000438]:csrrs a7, fflags, zero<br> [0x8000043c]:sw a3, 8(a5)<br>      |
|  32|[0x80002308]<br>0x00000000|- rd : x14<br> - rs1 : f9<br>                                                                                                    |[0x8000044c]:fmv.x.w a4, fs1<br> [0x80000450]:csrrs a7, fflags, zero<br> [0x80000454]:sw a4, 16(a5)<br>     |
