
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800004b0')]      |
| SIG_REGION                | [('0x80002210', '0x80002330', '72 words')]      |
| COV_LABELS                | fcvt.s.wu_b25      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch14/fcvt.s.wu/riscof_work/fcvt.s.wu_b25-01.S/ref.S    |
| Total Number of coverpoints| 104     |
| Total Coverpoints Hit     | 100      |
| Total Signature Updates   | 36      |
| STAT1                     | 35      |
| STAT2                     | 1      |
| STAT3                     | 0     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x8000047c]:fcvt.s.wu ft11, t6, dyn
      [0x80000480]:csrrs a7, fflags, zero
      [0x80000484]:fsw ft11, 96(a5)
      [0x80000488]:sw a7, 100(a5)
 -- Signature Address: 0x80002324 Data: 0x00000001
 -- Redundant Coverpoints hit by the op
      - opcode : fcvt.s.wu
      - rs1 : x31
      - rd : f31
      - rs1_val == 0 and rm_val == 1  #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                           coverpoints                                            |                                                                        code                                                                        |
|---:|--------------------------|--------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002214]<br>0x00000000|- opcode : fcvt.s.wu<br> - rs1 : x31<br> - rd : f7<br> - rs1_val == 0 and rm_val == 0  #nosat<br> |[0x8000011c]:fcvt.s.wu ft7, t6, dyn<br> [0x80000120]:csrrs a7, fflags, zero<br> [0x80000124]:fsw ft7, 0(a5)<br> [0x80000128]:sw a7, 4(a5)<br>       |
|   2|[0x8000221c]<br>0x00000001|- rs1 : x20<br> - rd : f6<br> - rs1_val == -1227077728 and rm_val == 4  #nosat<br>                |[0x80000134]:fcvt.s.wu ft6, s4, dyn<br> [0x80000138]:csrrs a7, fflags, zero<br> [0x8000013c]:fsw ft6, 8(a5)<br> [0x80000140]:sw a7, 12(a5)<br>      |
|   3|[0x80002224]<br>0x00000001|- rs1 : x10<br> - rd : f17<br> - rs1_val == -1227077728 and rm_val == 3  #nosat<br>               |[0x8000014c]:fcvt.s.wu fa7, a0, dyn<br> [0x80000150]:csrrs a7, fflags, zero<br> [0x80000154]:fsw fa7, 16(a5)<br> [0x80000158]:sw a7, 20(a5)<br>     |
|   4|[0x8000222c]<br>0x00000001|- rs1 : x6<br> - rd : f1<br> - rs1_val == -1227077728 and rm_val == 2  #nosat<br>                 |[0x80000164]:fcvt.s.wu ft1, t1, dyn<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:fsw ft1, 24(a5)<br> [0x80000170]:sw a7, 28(a5)<br>     |
|   5|[0x80002234]<br>0x00000001|- rs1 : x1<br> - rd : f22<br> - rs1_val == -1227077728 and rm_val == 1  #nosat<br>                |[0x8000017c]:fcvt.s.wu fs6, ra, dyn<br> [0x80000180]:csrrs a7, fflags, zero<br> [0x80000184]:fsw fs6, 32(a5)<br> [0x80000188]:sw a7, 36(a5)<br>     |
|   6|[0x8000223c]<br>0x00000001|- rs1 : x29<br> - rd : f11<br> - rs1_val == -1227077728 and rm_val == 0  #nosat<br>               |[0x80000194]:fcvt.s.wu fa1, t4, dyn<br> [0x80000198]:csrrs a7, fflags, zero<br> [0x8000019c]:fsw fa1, 40(a5)<br> [0x800001a0]:sw a7, 44(a5)<br>     |
|   7|[0x80002244]<br>0x00000001|- rs1 : x27<br> - rd : f12<br> - rs1_val == 1227077728 and rm_val == 4  #nosat<br>                |[0x800001ac]:fcvt.s.wu fa2, s11, dyn<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:fsw fa2, 48(a5)<br> [0x800001b8]:sw a7, 52(a5)<br>    |
|   8|[0x8000224c]<br>0x00000001|- rs1 : x9<br> - rd : f24<br> - rs1_val == 1227077728 and rm_val == 3  #nosat<br>                 |[0x800001c4]:fcvt.s.wu fs8, s1, dyn<br> [0x800001c8]:csrrs a7, fflags, zero<br> [0x800001cc]:fsw fs8, 56(a5)<br> [0x800001d0]:sw a7, 60(a5)<br>     |
|   9|[0x80002254]<br>0x00000001|- rs1 : x3<br> - rd : f9<br> - rs1_val == 1227077728 and rm_val == 2  #nosat<br>                  |[0x800001dc]:fcvt.s.wu fs1, gp, dyn<br> [0x800001e0]:csrrs a7, fflags, zero<br> [0x800001e4]:fsw fs1, 64(a5)<br> [0x800001e8]:sw a7, 68(a5)<br>     |
|  10|[0x8000225c]<br>0x00000001|- rs1 : x30<br> - rd : f19<br> - rs1_val == 1227077728 and rm_val == 1  #nosat<br>                |[0x800001f4]:fcvt.s.wu fs3, t5, dyn<br> [0x800001f8]:csrrs a7, fflags, zero<br> [0x800001fc]:fsw fs3, 72(a5)<br> [0x80000200]:sw a7, 76(a5)<br>     |
|  11|[0x80002264]<br>0x00000001|- rs1 : x5<br> - rd : f2<br> - rs1_val == 1227077728 and rm_val == 0  #nosat<br>                  |[0x8000020c]:fcvt.s.wu ft2, t0, dyn<br> [0x80000210]:csrrs a7, fflags, zero<br> [0x80000214]:fsw ft2, 80(a5)<br> [0x80000218]:sw a7, 84(a5)<br>     |
|  12|[0x8000226c]<br>0x00000001|- rs1 : x24<br> - rd : f0<br> - rs1_val == -2147483647 and rm_val == 4  #nosat<br>                |[0x80000224]:fcvt.s.wu ft0, s8, dyn<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:fsw ft0, 88(a5)<br> [0x80000230]:sw a7, 92(a5)<br>     |
|  13|[0x80002274]<br>0x00000001|- rs1 : x18<br> - rd : f15<br> - rs1_val == -2147483647 and rm_val == 3  #nosat<br>               |[0x8000023c]:fcvt.s.wu fa5, s2, dyn<br> [0x80000240]:csrrs a7, fflags, zero<br> [0x80000244]:fsw fa5, 96(a5)<br> [0x80000248]:sw a7, 100(a5)<br>    |
|  14|[0x8000227c]<br>0x00000001|- rs1 : x23<br> - rd : f31<br> - rs1_val == -2147483647 and rm_val == 2  #nosat<br>               |[0x80000254]:fcvt.s.wu ft11, s7, dyn<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:fsw ft11, 104(a5)<br> [0x80000260]:sw a7, 108(a5)<br> |
|  15|[0x80002284]<br>0x00000001|- rs1 : x4<br> - rd : f23<br> - rs1_val == -2147483647 and rm_val == 1  #nosat<br>                |[0x8000026c]:fcvt.s.wu fs7, tp, dyn<br> [0x80000270]:csrrs a7, fflags, zero<br> [0x80000274]:fsw fs7, 112(a5)<br> [0x80000278]:sw a7, 116(a5)<br>   |
|  16|[0x8000228c]<br>0x00000001|- rs1 : x2<br> - rd : f16<br> - rs1_val == -2147483647 and rm_val == 0  #nosat<br>                |[0x80000284]:fcvt.s.wu fa6, sp, dyn<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:fsw fa6, 120(a5)<br> [0x80000290]:sw a7, 124(a5)<br>   |
|  17|[0x80002294]<br>0x00000001|- rs1 : x16<br> - rd : f21<br> - rs1_val == 2147483647 and rm_val == 4  #nosat<br>                |[0x800002a8]:fcvt.s.wu fs5, a6, dyn<br> [0x800002ac]:csrrs s5, fflags, zero<br> [0x800002b0]:fsw fs5, 0(s3)<br> [0x800002b4]:sw s5, 4(s3)<br>       |
|  18|[0x8000229c]<br>0x00000001|- rs1 : x14<br> - rd : f14<br> - rs1_val == 2147483647 and rm_val == 3  #nosat<br>                |[0x800002cc]:fcvt.s.wu fa4, a4, dyn<br> [0x800002d0]:csrrs a7, fflags, zero<br> [0x800002d4]:fsw fa4, 0(a5)<br> [0x800002d8]:sw a7, 4(a5)<br>       |
|  19|[0x800022a4]<br>0x00000001|- rs1 : x7<br> - rd : f25<br> - rs1_val == 2147483647 and rm_val == 2  #nosat<br>                 |[0x800002e4]:fcvt.s.wu fs9, t2, dyn<br> [0x800002e8]:csrrs a7, fflags, zero<br> [0x800002ec]:fsw fs9, 8(a5)<br> [0x800002f0]:sw a7, 12(a5)<br>      |
|  20|[0x800022ac]<br>0x00000001|- rs1 : x12<br> - rd : f4<br> - rs1_val == 2147483647 and rm_val == 1  #nosat<br>                 |[0x800002fc]:fcvt.s.wu ft4, a2, dyn<br> [0x80000300]:csrrs a7, fflags, zero<br> [0x80000304]:fsw ft4, 16(a5)<br> [0x80000308]:sw a7, 20(a5)<br>     |
|  21|[0x800022b4]<br>0x00000001|- rs1 : x15<br> - rd : f26<br> - rs1_val == 2147483647 and rm_val == 0  #nosat<br>                |[0x80000320]:fcvt.s.wu fs10, a5, dyn<br> [0x80000324]:csrrs s5, fflags, zero<br> [0x80000328]:fsw fs10, 0(s3)<br> [0x8000032c]:sw s5, 4(s3)<br>     |
|  22|[0x800022bc]<br>0x00000001|- rs1 : x17<br> - rd : f18<br> - rs1_val == -1 and rm_val == 4  #nosat<br>                        |[0x80000338]:fcvt.s.wu fs2, a7, dyn<br> [0x8000033c]:csrrs s5, fflags, zero<br> [0x80000340]:fsw fs2, 8(s3)<br> [0x80000344]:sw s5, 12(s3)<br>      |
|  23|[0x800022c4]<br>0x00000001|- rs1 : x8<br> - rd : f8<br> - rs1_val == -1 and rm_val == 3  #nosat<br>                          |[0x8000035c]:fcvt.s.wu fs0, fp, dyn<br> [0x80000360]:csrrs a7, fflags, zero<br> [0x80000364]:fsw fs0, 0(a5)<br> [0x80000368]:sw a7, 4(a5)<br>       |
|  24|[0x800022cc]<br>0x00000001|- rs1 : x25<br> - rd : f29<br> - rs1_val == -1 and rm_val == 2  #nosat<br>                        |[0x80000374]:fcvt.s.wu ft9, s9, dyn<br> [0x80000378]:csrrs a7, fflags, zero<br> [0x8000037c]:fsw ft9, 8(a5)<br> [0x80000380]:sw a7, 12(a5)<br>      |
|  25|[0x800022d4]<br>0x00000001|- rs1 : x26<br> - rd : f20<br> - rs1_val == -1 and rm_val == 1  #nosat<br>                        |[0x8000038c]:fcvt.s.wu fs4, s10, dyn<br> [0x80000390]:csrrs a7, fflags, zero<br> [0x80000394]:fsw fs4, 16(a5)<br> [0x80000398]:sw a7, 20(a5)<br>    |
|  26|[0x800022dc]<br>0x00000001|- rs1 : x28<br> - rd : f28<br> - rs1_val == -1 and rm_val == 0  #nosat<br>                        |[0x800003a4]:fcvt.s.wu ft8, t3, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:fsw ft8, 24(a5)<br> [0x800003b0]:sw a7, 28(a5)<br>     |
|  27|[0x800022e4]<br>0x00000001|- rs1 : x21<br> - rd : f27<br> - rs1_val == 1 and rm_val == 4  #nosat<br>                         |[0x800003bc]:fcvt.s.wu fs11, s5, dyn<br> [0x800003c0]:csrrs a7, fflags, zero<br> [0x800003c4]:fsw fs11, 32(a5)<br> [0x800003c8]:sw a7, 36(a5)<br>   |
|  28|[0x800022ec]<br>0x00000001|- rs1 : x13<br> - rd : f3<br> - rs1_val == 1 and rm_val == 3  #nosat<br>                          |[0x800003d4]:fcvt.s.wu ft3, a3, dyn<br> [0x800003d8]:csrrs a7, fflags, zero<br> [0x800003dc]:fsw ft3, 40(a5)<br> [0x800003e0]:sw a7, 44(a5)<br>     |
|  29|[0x800022f4]<br>0x00000001|- rs1 : x11<br> - rd : f13<br> - rs1_val == 1 and rm_val == 2  #nosat<br>                         |[0x800003ec]:fcvt.s.wu fa3, a1, dyn<br> [0x800003f0]:csrrs a7, fflags, zero<br> [0x800003f4]:fsw fa3, 48(a5)<br> [0x800003f8]:sw a7, 52(a5)<br>     |
|  30|[0x800022fc]<br>0x00000001|- rs1 : x0<br> - rd : f10<br> - rs1_val == 0 and rm_val == 1  #nosat<br>                          |[0x80000404]:fcvt.s.wu fa0, zero, dyn<br> [0x80000408]:csrrs a7, fflags, zero<br> [0x8000040c]:fsw fa0, 56(a5)<br> [0x80000410]:sw a7, 60(a5)<br>   |
|  31|[0x80002304]<br>0x00000001|- rs1 : x19<br> - rd : f5<br> - rs1_val == 1 and rm_val == 0  #nosat<br>                          |[0x8000041c]:fcvt.s.wu ft5, s3, dyn<br> [0x80000420]:csrrs a7, fflags, zero<br> [0x80000424]:fsw ft5, 64(a5)<br> [0x80000428]:sw a7, 68(a5)<br>     |
|  32|[0x8000230c]<br>0x00000001|- rs1 : x22<br> - rd : f30<br> - rs1_val == 0 and rm_val == 4  #nosat<br>                         |[0x80000434]:fcvt.s.wu ft10, s6, dyn<br> [0x80000438]:csrrs a7, fflags, zero<br> [0x8000043c]:fsw ft10, 72(a5)<br> [0x80000440]:sw a7, 76(a5)<br>   |
|  33|[0x80002314]<br>0x00000001|- rs1_val == 0 and rm_val == 3  #nosat<br>                                                        |[0x8000044c]:fcvt.s.wu ft11, t6, dyn<br> [0x80000450]:csrrs a7, fflags, zero<br> [0x80000454]:fsw ft11, 80(a5)<br> [0x80000458]:sw a7, 84(a5)<br>   |
|  34|[0x8000231c]<br>0x00000001|- rs1_val == 0 and rm_val == 2  #nosat<br>                                                        |[0x80000464]:fcvt.s.wu ft11, t6, dyn<br> [0x80000468]:csrrs a7, fflags, zero<br> [0x8000046c]:fsw ft11, 88(a5)<br> [0x80000470]:sw a7, 92(a5)<br>   |
|  35|[0x8000232c]<br>0x00000001|- rs1_val == 1 and rm_val == 1  #nosat<br>                                                        |[0x80000494]:fcvt.s.wu ft11, t6, dyn<br> [0x80000498]:csrrs a7, fflags, zero<br> [0x8000049c]:fsw ft11, 104(a5)<br> [0x800004a0]:sw a7, 108(a5)<br> |
