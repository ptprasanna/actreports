
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000530')]      |
| SIG_REGION                | [('0x80002210', '0x80002310', '64 words')]      |
| COV_LABELS                | flw-align      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch14/flw/riscof_work/flw-align-01.S/ref.S    |
| Total Number of coverpoints| 75     |
| Total Coverpoints Hit     | 71      |
| Total Signature Updates   | 32      |
| STAT1                     | 32      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                  coverpoints                                                  |                                                                                                         code                                                                                                          |
|---:|--------------------------|---------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002214]<br>0x00000000|- opcode : flw<br> - rs1 : x20<br> - rd : f15<br> - ea_align == 0 and (imm_val % 4) == 0<br> - imm_val < 0<br> |[0x80000114]:flw fa5, 2048(s4)<br> [0x80000118]:addi zero, zero, 0<br> [0x8000011c]:addi zero, zero, 0<br> [0x80000120]:csrrs a7, fflags, zero<br> [0x80000124]:fsw fa5, 0(a5)<br> [0x80000128]:sw a7, 4(a5)<br>       |
|   2|[0x8000221c]<br>0x00000000|- rs1 : x24<br> - rd : f2<br> - ea_align == 0 and (imm_val % 4) == 1<br> - imm_val > 0<br>                     |[0x80000134]:flw ft2, 9(s8)<br> [0x80000138]:addi zero, zero, 0<br> [0x8000013c]:addi zero, zero, 0<br> [0x80000140]:csrrs a7, fflags, zero<br> [0x80000144]:fsw ft2, 8(a5)<br> [0x80000148]:sw a7, 12(a5)<br>         |
|   3|[0x80002224]<br>0x00000000|- rs1 : x1<br> - rd : f19<br> - ea_align == 0 and (imm_val % 4) == 2<br>                                       |[0x80000154]:flw fs3, 4090(ra)<br> [0x80000158]:addi zero, zero, 0<br> [0x8000015c]:addi zero, zero, 0<br> [0x80000160]:csrrs a7, fflags, zero<br> [0x80000164]:fsw fs3, 16(a5)<br> [0x80000168]:sw a7, 20(a5)<br>     |
|   4|[0x8000222c]<br>0x00000000|- rs1 : x30<br> - rd : f16<br> - ea_align == 0 and (imm_val % 4) == 3<br>                                      |[0x80000174]:flw fa6, 2047(t5)<br> [0x80000178]:addi zero, zero, 0<br> [0x8000017c]:addi zero, zero, 0<br> [0x80000180]:csrrs a7, fflags, zero<br> [0x80000184]:fsw fa6, 24(a5)<br> [0x80000188]:sw a7, 28(a5)<br>     |
|   5|[0x80002234]<br>0x00000000|- rs1 : x25<br> - rd : f3<br> - imm_val == 0<br>                                                               |[0x80000194]:flw ft3, 0(s9)<br> [0x80000198]:addi zero, zero, 0<br> [0x8000019c]:addi zero, zero, 0<br> [0x800001a0]:csrrs a7, fflags, zero<br> [0x800001a4]:fsw ft3, 32(a5)<br> [0x800001a8]:sw a7, 36(a5)<br>        |
|   6|[0x8000223c]<br>0x00000000|- rs1 : x8<br> - rd : f0<br>                                                                                   |[0x800001b4]:flw ft0, 2048(fp)<br> [0x800001b8]:addi zero, zero, 0<br> [0x800001bc]:addi zero, zero, 0<br> [0x800001c0]:csrrs a7, fflags, zero<br> [0x800001c4]:fsw ft0, 40(a5)<br> [0x800001c8]:sw a7, 44(a5)<br>     |
|   7|[0x80002244]<br>0x00000000|- rs1 : x15<br> - rd : f12<br>                                                                                 |[0x800001dc]:flw fa2, 2048(a5)<br> [0x800001e0]:addi zero, zero, 0<br> [0x800001e4]:addi zero, zero, 0<br> [0x800001e8]:csrrs s5, fflags, zero<br> [0x800001ec]:fsw fa2, 0(s3)<br> [0x800001f0]:sw s5, 4(s3)<br>       |
|   8|[0x8000224c]<br>0x00000000|- rs1 : x23<br> - rd : f29<br>                                                                                 |[0x80000204]:flw ft9, 2048(s7)<br> [0x80000208]:addi zero, zero, 0<br> [0x8000020c]:addi zero, zero, 0<br> [0x80000210]:csrrs a7, fflags, zero<br> [0x80000214]:fsw ft9, 0(a5)<br> [0x80000218]:sw a7, 4(a5)<br>       |
|   9|[0x80002254]<br>0x00000000|- rs1 : x29<br> - rd : f8<br>                                                                                  |[0x80000224]:flw fs0, 2048(t4)<br> [0x80000228]:addi zero, zero, 0<br> [0x8000022c]:addi zero, zero, 0<br> [0x80000230]:csrrs a7, fflags, zero<br> [0x80000234]:fsw fs0, 8(a5)<br> [0x80000238]:sw a7, 12(a5)<br>      |
|  10|[0x8000225c]<br>0x00000000|- rs1 : x26<br> - rd : f13<br>                                                                                 |[0x80000244]:flw fa3, 2048(s10)<br> [0x80000248]:addi zero, zero, 0<br> [0x8000024c]:addi zero, zero, 0<br> [0x80000250]:csrrs a7, fflags, zero<br> [0x80000254]:fsw fa3, 16(a5)<br> [0x80000258]:sw a7, 20(a5)<br>    |
|  11|[0x80002264]<br>0x00000000|- rs1 : x28<br> - rd : f20<br>                                                                                 |[0x80000264]:flw fs4, 2048(t3)<br> [0x80000268]:addi zero, zero, 0<br> [0x8000026c]:addi zero, zero, 0<br> [0x80000270]:csrrs a7, fflags, zero<br> [0x80000274]:fsw fs4, 24(a5)<br> [0x80000278]:sw a7, 28(a5)<br>     |
|  12|[0x8000226c]<br>0x00000000|- rs1 : x31<br> - rd : f5<br>                                                                                  |[0x80000284]:flw ft5, 2048(t6)<br> [0x80000288]:addi zero, zero, 0<br> [0x8000028c]:addi zero, zero, 0<br> [0x80000290]:csrrs a7, fflags, zero<br> [0x80000294]:fsw ft5, 32(a5)<br> [0x80000298]:sw a7, 36(a5)<br>     |
|  13|[0x80002274]<br>0x00000000|- rs1 : x27<br> - rd : f25<br>                                                                                 |[0x800002a4]:flw fs9, 2048(s11)<br> [0x800002a8]:addi zero, zero, 0<br> [0x800002ac]:addi zero, zero, 0<br> [0x800002b0]:csrrs a7, fflags, zero<br> [0x800002b4]:fsw fs9, 40(a5)<br> [0x800002b8]:sw a7, 44(a5)<br>    |
|  14|[0x8000227c]<br>0x00000000|- rs1 : x2<br> - rd : f18<br>                                                                                  |[0x800002c4]:flw fs2, 2048(sp)<br> [0x800002c8]:addi zero, zero, 0<br> [0x800002cc]:addi zero, zero, 0<br> [0x800002d0]:csrrs a7, fflags, zero<br> [0x800002d4]:fsw fs2, 48(a5)<br> [0x800002d8]:sw a7, 52(a5)<br>     |
|  15|[0x80002284]<br>0x00000000|- rs1 : x22<br> - rd : f7<br>                                                                                  |[0x800002e4]:flw ft7, 2048(s6)<br> [0x800002e8]:addi zero, zero, 0<br> [0x800002ec]:addi zero, zero, 0<br> [0x800002f0]:csrrs a7, fflags, zero<br> [0x800002f4]:fsw ft7, 56(a5)<br> [0x800002f8]:sw a7, 60(a5)<br>     |
|  16|[0x8000228c]<br>0x00000000|- rs1 : x12<br> - rd : f26<br>                                                                                 |[0x80000304]:flw fs10, 2048(a2)<br> [0x80000308]:addi zero, zero, 0<br> [0x8000030c]:addi zero, zero, 0<br> [0x80000310]:csrrs a7, fflags, zero<br> [0x80000314]:fsw fs10, 64(a5)<br> [0x80000318]:sw a7, 68(a5)<br>   |
|  17|[0x80002294]<br>0x00000000|- rs1 : x6<br> - rd : f17<br>                                                                                  |[0x80000324]:flw fa7, 2048(t1)<br> [0x80000328]:addi zero, zero, 0<br> [0x8000032c]:addi zero, zero, 0<br> [0x80000330]:csrrs a7, fflags, zero<br> [0x80000334]:fsw fa7, 72(a5)<br> [0x80000338]:sw a7, 76(a5)<br>     |
|  18|[0x8000229c]<br>0x00000000|- rs1 : x21<br> - rd : f14<br>                                                                                 |[0x80000344]:flw fa4, 2048(s5)<br> [0x80000348]:addi zero, zero, 0<br> [0x8000034c]:addi zero, zero, 0<br> [0x80000350]:csrrs a7, fflags, zero<br> [0x80000354]:fsw fa4, 80(a5)<br> [0x80000358]:sw a7, 84(a5)<br>     |
|  19|[0x800022a4]<br>0x00000000|- rs1 : x7<br> - rd : f6<br>                                                                                   |[0x80000364]:flw ft6, 2048(t2)<br> [0x80000368]:addi zero, zero, 0<br> [0x8000036c]:addi zero, zero, 0<br> [0x80000370]:csrrs a7, fflags, zero<br> [0x80000374]:fsw ft6, 88(a5)<br> [0x80000378]:sw a7, 92(a5)<br>     |
|  20|[0x800022ac]<br>0x00000000|- rs1 : x14<br> - rd : f1<br>                                                                                  |[0x80000384]:flw ft1, 2048(a4)<br> [0x80000388]:addi zero, zero, 0<br> [0x8000038c]:addi zero, zero, 0<br> [0x80000390]:csrrs a7, fflags, zero<br> [0x80000394]:fsw ft1, 96(a5)<br> [0x80000398]:sw a7, 100(a5)<br>    |
|  21|[0x800022b4]<br>0x00000000|- rs1 : x3<br> - rd : f30<br>                                                                                  |[0x800003a4]:flw ft10, 2048(gp)<br> [0x800003a8]:addi zero, zero, 0<br> [0x800003ac]:addi zero, zero, 0<br> [0x800003b0]:csrrs a7, fflags, zero<br> [0x800003b4]:fsw ft10, 104(a5)<br> [0x800003b8]:sw a7, 108(a5)<br> |
|  22|[0x800022bc]<br>0x00000000|- rs1 : x4<br> - rd : f23<br>                                                                                  |[0x800003c4]:flw fs7, 2048(tp)<br> [0x800003c8]:addi zero, zero, 0<br> [0x800003cc]:addi zero, zero, 0<br> [0x800003d0]:csrrs a7, fflags, zero<br> [0x800003d4]:fsw fs7, 112(a5)<br> [0x800003d8]:sw a7, 116(a5)<br>   |
|  23|[0x800022c4]<br>0x00000000|- rs1 : x19<br> - rd : f11<br>                                                                                 |[0x800003e4]:flw fa1, 2048(s3)<br> [0x800003e8]:addi zero, zero, 0<br> [0x800003ec]:addi zero, zero, 0<br> [0x800003f0]:csrrs a7, fflags, zero<br> [0x800003f4]:fsw fa1, 120(a5)<br> [0x800003f8]:sw a7, 124(a5)<br>   |
|  24|[0x800022cc]<br>0x00000000|- rs1 : x13<br> - rd : f24<br>                                                                                 |[0x80000404]:flw fs8, 2048(a3)<br> [0x80000408]:addi zero, zero, 0<br> [0x8000040c]:addi zero, zero, 0<br> [0x80000410]:csrrs a7, fflags, zero<br> [0x80000414]:fsw fs8, 128(a5)<br> [0x80000418]:sw a7, 132(a5)<br>   |
|  25|[0x800022d4]<br>0x00000000|- rs1 : x11<br> - rd : f28<br>                                                                                 |[0x80000424]:flw ft8, 2048(a1)<br> [0x80000428]:addi zero, zero, 0<br> [0x8000042c]:addi zero, zero, 0<br> [0x80000430]:csrrs a7, fflags, zero<br> [0x80000434]:fsw ft8, 136(a5)<br> [0x80000438]:sw a7, 140(a5)<br>   |
|  26|[0x800022dc]<br>0x00000000|- rs1 : x9<br> - rd : f4<br>                                                                                   |[0x80000444]:flw ft4, 2048(s1)<br> [0x80000448]:addi zero, zero, 0<br> [0x8000044c]:addi zero, zero, 0<br> [0x80000450]:csrrs a7, fflags, zero<br> [0x80000454]:fsw ft4, 144(a5)<br> [0x80000458]:sw a7, 148(a5)<br>   |
|  27|[0x800022e4]<br>0x00000000|- rs1 : x5<br> - rd : f9<br>                                                                                   |[0x80000464]:flw fs1, 2048(t0)<br> [0x80000468]:addi zero, zero, 0<br> [0x8000046c]:addi zero, zero, 0<br> [0x80000470]:csrrs a7, fflags, zero<br> [0x80000474]:fsw fs1, 152(a5)<br> [0x80000478]:sw a7, 156(a5)<br>   |
|  28|[0x800022ec]<br>0x00000000|- rs1 : x18<br> - rd : f10<br>                                                                                 |[0x80000484]:flw fa0, 2048(s2)<br> [0x80000488]:addi zero, zero, 0<br> [0x8000048c]:addi zero, zero, 0<br> [0x80000490]:csrrs a7, fflags, zero<br> [0x80000494]:fsw fa0, 160(a5)<br> [0x80000498]:sw a7, 164(a5)<br>   |
|  29|[0x800022f4]<br>0x00000000|- rs1 : x10<br> - rd : f22<br>                                                                                 |[0x800004a4]:flw fs6, 2048(a0)<br> [0x800004a8]:addi zero, zero, 0<br> [0x800004ac]:addi zero, zero, 0<br> [0x800004b0]:csrrs a7, fflags, zero<br> [0x800004b4]:fsw fs6, 168(a5)<br> [0x800004b8]:sw a7, 172(a5)<br>   |
|  30|[0x800022fc]<br>0x00000000|- rs1 : x16<br> - rd : f31<br>                                                                                 |[0x800004cc]:flw ft11, 2048(a6)<br> [0x800004d0]:addi zero, zero, 0<br> [0x800004d4]:addi zero, zero, 0<br> [0x800004d8]:csrrs s5, fflags, zero<br> [0x800004dc]:fsw ft11, 0(s3)<br> [0x800004e0]:sw s5, 4(s3)<br>     |
|  31|[0x80002304]<br>0x00000000|- rs1 : x17<br> - rd : f21<br>                                                                                 |[0x800004ec]:flw fs5, 2048(a7)<br> [0x800004f0]:addi zero, zero, 0<br> [0x800004f4]:addi zero, zero, 0<br> [0x800004f8]:csrrs s5, fflags, zero<br> [0x800004fc]:fsw fs5, 8(s3)<br> [0x80000500]:sw s5, 12(s3)<br>      |
|  32|[0x8000230c]<br>0x00000000|- rd : f27<br>                                                                                                 |[0x80000514]:flw fs11, 2048(s2)<br> [0x80000518]:addi zero, zero, 0<br> [0x8000051c]:addi zero, zero, 0<br> [0x80000520]:csrrs a7, fflags, zero<br> [0x80000524]:fsw fs11, 0(a5)<br> [0x80000528]:sw a7, 4(a5)<br>     |
