
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000640')]      |
| SIG_REGION                | [('0x80002210', '0x80002310', '64 words')]      |
| COV_LABELS                | fsw-align      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch14/fsw/riscof_work/fsw-align-01.S/ref.S    |
| Total Number of coverpoints| 75     |
| Total Coverpoints Hit     | 71      |
| Total Signature Updates   | 32      |
| STAT1                     | 32      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                  coverpoints                                                   |                                                                                       code                                                                                        |
|---:|--------------------------|----------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002210]<br>0x00000000|- opcode : fsw<br> - rs1 : x30<br> - rs2 : f14<br> - ea_align == 0 and (imm_val % 4) == 0<br> - imm_val > 0<br> |[0x80000124]:fsw fa4, 8(t5)<br> [0x80000128]:addi zero, zero, 0<br> [0x8000012c]:addi zero, zero, 0<br> [0x80000130]:csrrs a7, fflags, zero<br> [0x80000134]:sw a7, 0(a5)<br>      |
|   2|[0x80002218]<br>0x00000000|- rs1 : x21<br> - rs2 : f18<br> - ea_align == 0 and (imm_val % 4) == 1<br>                                      |[0x8000014c]:fsw fs2, 1365(s5)<br> [0x80000150]:addi zero, zero, 0<br> [0x80000154]:addi zero, zero, 0<br> [0x80000158]:csrrs a7, fflags, zero<br> [0x8000015c]:sw a7, 8(a5)<br>   |
|   3|[0x80002220]<br>0x00000000|- rs1 : x13<br> - rs2 : f10<br> - ea_align == 0 and (imm_val % 4) == 2<br>                                      |[0x80000174]:fsw fa0, 2(a3)<br> [0x80000178]:addi zero, zero, 0<br> [0x8000017c]:addi zero, zero, 0<br> [0x80000180]:csrrs a7, fflags, zero<br> [0x80000184]:sw a7, 16(a5)<br>     |
|   4|[0x80002228]<br>0x00000000|- rs1 : x11<br> - rs2 : f8<br> - ea_align == 0 and (imm_val % 4) == 3<br> - imm_val < 0<br>                     |[0x8000019c]:fsw fs0, 3583(a1)<br> [0x800001a0]:addi zero, zero, 0<br> [0x800001a4]:addi zero, zero, 0<br> [0x800001a8]:csrrs a7, fflags, zero<br> [0x800001ac]:sw a7, 24(a5)<br>  |
|   5|[0x80002230]<br>0x00000000|- rs1 : x1<br> - rs2 : f24<br> - imm_val == 0<br>                                                               |[0x800001c4]:fsw fs8, 0(ra)<br> [0x800001c8]:addi zero, zero, 0<br> [0x800001cc]:addi zero, zero, 0<br> [0x800001d0]:csrrs a7, fflags, zero<br> [0x800001d4]:sw a7, 32(a5)<br>     |
|   6|[0x80002238]<br>0x00000000|- rs1 : x3<br> - rs2 : f5<br>                                                                                   |[0x800001ec]:fsw ft5, 2048(gp)<br> [0x800001f0]:addi zero, zero, 0<br> [0x800001f4]:addi zero, zero, 0<br> [0x800001f8]:csrrs a7, fflags, zero<br> [0x800001fc]:sw a7, 40(a5)<br>  |
|   7|[0x80002240]<br>0x00000000|- rs1 : x19<br> - rs2 : f28<br>                                                                                 |[0x80000214]:fsw ft8, 2048(s3)<br> [0x80000218]:addi zero, zero, 0<br> [0x8000021c]:addi zero, zero, 0<br> [0x80000220]:csrrs a7, fflags, zero<br> [0x80000224]:sw a7, 48(a5)<br>  |
|   8|[0x80002248]<br>0x00000000|- rs1 : x18<br> - rs2 : f22<br>                                                                                 |[0x8000023c]:fsw fs6, 2048(s2)<br> [0x80000240]:addi zero, zero, 0<br> [0x80000244]:addi zero, zero, 0<br> [0x80000248]:csrrs a7, fflags, zero<br> [0x8000024c]:sw a7, 56(a5)<br>  |
|   9|[0x80002250]<br>0x00000000|- rs1 : x7<br> - rs2 : f15<br>                                                                                  |[0x80000264]:fsw fa5, 2048(t2)<br> [0x80000268]:addi zero, zero, 0<br> [0x8000026c]:addi zero, zero, 0<br> [0x80000270]:csrrs a7, fflags, zero<br> [0x80000274]:sw a7, 64(a5)<br>  |
|  10|[0x80002258]<br>0x00000000|- rs1 : x9<br> - rs2 : f12<br>                                                                                  |[0x8000028c]:fsw fa2, 2048(s1)<br> [0x80000290]:addi zero, zero, 0<br> [0x80000294]:addi zero, zero, 0<br> [0x80000298]:csrrs a7, fflags, zero<br> [0x8000029c]:sw a7, 72(a5)<br>  |
|  11|[0x80002260]<br>0x00000000|- rs1 : x15<br> - rs2 : f3<br>                                                                                  |[0x800002bc]:fsw ft3, 2048(a5)<br> [0x800002c0]:addi zero, zero, 0<br> [0x800002c4]:addi zero, zero, 0<br> [0x800002c8]:csrrs s5, fflags, zero<br> [0x800002cc]:sw s5, 0(s3)<br>   |
|  12|[0x80002268]<br>0x00000000|- rs1 : x24<br> - rs2 : f25<br>                                                                                 |[0x800002ec]:fsw fs9, 2048(s8)<br> [0x800002f0]:addi zero, zero, 0<br> [0x800002f4]:addi zero, zero, 0<br> [0x800002f8]:csrrs a7, fflags, zero<br> [0x800002fc]:sw a7, 0(a5)<br>   |
|  13|[0x80002270]<br>0x00000000|- rs1 : x23<br> - rs2 : f31<br>                                                                                 |[0x80000314]:fsw ft11, 2048(s7)<br> [0x80000318]:addi zero, zero, 0<br> [0x8000031c]:addi zero, zero, 0<br> [0x80000320]:csrrs a7, fflags, zero<br> [0x80000324]:sw a7, 8(a5)<br>  |
|  14|[0x80002278]<br>0x00000000|- rs1 : x27<br> - rs2 : f7<br>                                                                                  |[0x8000033c]:fsw ft7, 2048(s11)<br> [0x80000340]:addi zero, zero, 0<br> [0x80000344]:addi zero, zero, 0<br> [0x80000348]:csrrs a7, fflags, zero<br> [0x8000034c]:sw a7, 16(a5)<br> |
|  15|[0x80002280]<br>0x00000000|- rs1 : x14<br> - rs2 : f1<br>                                                                                  |[0x80000364]:fsw ft1, 2048(a4)<br> [0x80000368]:addi zero, zero, 0<br> [0x8000036c]:addi zero, zero, 0<br> [0x80000370]:csrrs a7, fflags, zero<br> [0x80000374]:sw a7, 24(a5)<br>  |
|  16|[0x80002288]<br>0x00000000|- rs1 : x20<br> - rs2 : f27<br>                                                                                 |[0x8000038c]:fsw fs11, 2048(s4)<br> [0x80000390]:addi zero, zero, 0<br> [0x80000394]:addi zero, zero, 0<br> [0x80000398]:csrrs a7, fflags, zero<br> [0x8000039c]:sw a7, 32(a5)<br> |
|  17|[0x80002290]<br>0x00000000|- rs1 : x8<br> - rs2 : f20<br>                                                                                  |[0x800003b4]:fsw fs4, 2048(fp)<br> [0x800003b8]:addi zero, zero, 0<br> [0x800003bc]:addi zero, zero, 0<br> [0x800003c0]:csrrs a7, fflags, zero<br> [0x800003c4]:sw a7, 40(a5)<br>  |
|  18|[0x80002298]<br>0x00000000|- rs1 : x16<br> - rs2 : f4<br>                                                                                  |[0x800003e4]:fsw ft4, 2048(a6)<br> [0x800003e8]:addi zero, zero, 0<br> [0x800003ec]:addi zero, zero, 0<br> [0x800003f0]:csrrs s5, fflags, zero<br> [0x800003f4]:sw s5, 0(s3)<br>   |
|  19|[0x800022a0]<br>0x00000000|- rs1 : x26<br> - rs2 : f23<br>                                                                                 |[0x80000414]:fsw fs7, 2048(s10)<br> [0x80000418]:addi zero, zero, 0<br> [0x8000041c]:addi zero, zero, 0<br> [0x80000420]:csrrs a7, fflags, zero<br> [0x80000424]:sw a7, 0(a5)<br>  |
|  20|[0x800022a8]<br>0x00000000|- rs1 : x28<br> - rs2 : f13<br>                                                                                 |[0x8000043c]:fsw fa3, 2048(t3)<br> [0x80000440]:addi zero, zero, 0<br> [0x80000444]:addi zero, zero, 0<br> [0x80000448]:csrrs a7, fflags, zero<br> [0x8000044c]:sw a7, 8(a5)<br>   |
|  21|[0x800022b0]<br>0x00000000|- rs1 : x12<br> - rs2 : f21<br>                                                                                 |[0x80000464]:fsw fs5, 2048(a2)<br> [0x80000468]:addi zero, zero, 0<br> [0x8000046c]:addi zero, zero, 0<br> [0x80000470]:csrrs a7, fflags, zero<br> [0x80000474]:sw a7, 16(a5)<br>  |
|  22|[0x800022b8]<br>0x00000000|- rs1 : x29<br> - rs2 : f6<br>                                                                                  |[0x8000048c]:fsw ft6, 2048(t4)<br> [0x80000490]:addi zero, zero, 0<br> [0x80000494]:addi zero, zero, 0<br> [0x80000498]:csrrs a7, fflags, zero<br> [0x8000049c]:sw a7, 24(a5)<br>  |
|  23|[0x800022c0]<br>0x00000000|- rs1 : x2<br> - rs2 : f19<br>                                                                                  |[0x800004b4]:fsw fs3, 2048(sp)<br> [0x800004b8]:addi zero, zero, 0<br> [0x800004bc]:addi zero, zero, 0<br> [0x800004c0]:csrrs a7, fflags, zero<br> [0x800004c4]:sw a7, 32(a5)<br>  |
|  24|[0x800022c8]<br>0x00000000|- rs1 : x25<br> - rs2 : f0<br>                                                                                  |[0x800004dc]:fsw ft0, 2048(s9)<br> [0x800004e0]:addi zero, zero, 0<br> [0x800004e4]:addi zero, zero, 0<br> [0x800004e8]:csrrs a7, fflags, zero<br> [0x800004ec]:sw a7, 40(a5)<br>  |
|  25|[0x800022d0]<br>0x00000000|- rs1 : x5<br> - rs2 : f30<br>                                                                                  |[0x80000504]:fsw ft10, 2048(t0)<br> [0x80000508]:addi zero, zero, 0<br> [0x8000050c]:addi zero, zero, 0<br> [0x80000510]:csrrs a7, fflags, zero<br> [0x80000514]:sw a7, 48(a5)<br> |
|  26|[0x800022d8]<br>0x00000000|- rs1 : x22<br> - rs2 : f16<br>                                                                                 |[0x8000052c]:fsw fa6, 2048(s6)<br> [0x80000530]:addi zero, zero, 0<br> [0x80000534]:addi zero, zero, 0<br> [0x80000538]:csrrs a7, fflags, zero<br> [0x8000053c]:sw a7, 56(a5)<br>  |
|  27|[0x800022e0]<br>0x00000000|- rs1 : x31<br> - rs2 : f2<br>                                                                                  |[0x80000554]:fsw ft2, 2048(t6)<br> [0x80000558]:addi zero, zero, 0<br> [0x8000055c]:addi zero, zero, 0<br> [0x80000560]:csrrs a7, fflags, zero<br> [0x80000564]:sw a7, 64(a5)<br>  |
|  28|[0x800022e8]<br>0x00000000|- rs1 : x4<br> - rs2 : f17<br>                                                                                  |[0x8000057c]:fsw fa7, 2048(tp)<br> [0x80000580]:addi zero, zero, 0<br> [0x80000584]:addi zero, zero, 0<br> [0x80000588]:csrrs a7, fflags, zero<br> [0x8000058c]:sw a7, 72(a5)<br>  |
|  29|[0x800022f0]<br>0x00000000|- rs1 : x17<br> - rs2 : f26<br>                                                                                 |[0x800005ac]:fsw fs10, 2048(a7)<br> [0x800005b0]:addi zero, zero, 0<br> [0x800005b4]:addi zero, zero, 0<br> [0x800005b8]:csrrs s5, fflags, zero<br> [0x800005bc]:sw s5, 0(s3)<br>  |
|  30|[0x800022f8]<br>0x00000000|- rs1 : x10<br> - rs2 : f29<br>                                                                                 |[0x800005dc]:fsw ft9, 2048(a0)<br> [0x800005e0]:addi zero, zero, 0<br> [0x800005e4]:addi zero, zero, 0<br> [0x800005e8]:csrrs a7, fflags, zero<br> [0x800005ec]:sw a7, 0(a5)<br>   |
|  31|[0x80002300]<br>0x00000000|- rs1 : x6<br> - rs2 : f11<br>                                                                                  |[0x80000604]:fsw fa1, 2048(t1)<br> [0x80000608]:addi zero, zero, 0<br> [0x8000060c]:addi zero, zero, 0<br> [0x80000610]:csrrs a7, fflags, zero<br> [0x80000614]:sw a7, 8(a5)<br>   |
|  32|[0x80002308]<br>0x00000000|- rs2 : f9<br>                                                                                                  |[0x8000062c]:fsw fs1, 2048(t6)<br> [0x80000630]:addi zero, zero, 0<br> [0x80000634]:addi zero, zero, 0<br> [0x80000638]:csrrs a7, fflags, zero<br> [0x8000063c]:sw a7, 16(a5)<br>  |
