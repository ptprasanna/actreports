
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000ed0')]      |
| SIG_REGION                | [('0x80002510', '0x80002880', '220 words')]      |
| COV_LABELS                | fdiv_b20      |
| TEST_NAME                 | /home/reg/work/zfinx/fdiv.s/work/fdiv_b20-01.S/ref.S    |
| Total Number of coverpoints| 204     |
| Total Coverpoints Hit     | 204      |
| Total Signature Updates   | 218      |
| STAT1                     | 106      |
| STAT2                     | 3      |
| STAT3                     | 0     |
| STAT4                     | 109     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000df4]:fdiv.s t6, t5, t4, dyn
      [0x80000df8]:csrrs a2, fcsr, zero
      [0x80000dfc]:sw t6, 616(fp)
 -- Signature Address: 0x80002840 Data: 0xC8A00000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fdiv.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0xfb and fm1 == 0x60f718 and fs2 == 1 and fe2 == 0xe9 and fm2 == 0x33f8e0 and  fcsr == 0x0 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000e14]:fdiv.s t6, t5, t4, dyn
      [0x80000e18]:csrrs a2, fcsr, zero
      [0x80000e1c]:sw t6, 624(fp)
 -- Signature Address: 0x80002848 Data: 0x61E00000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fdiv.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a0433 and fs2 == 0 and fe2 == 0xb9 and fm2 == 0x1dbba8 and  fcsr == 0x0 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000eb4]:fdiv.s t6, t5, t4, dyn
      [0x80000eb8]:csrrs a2, fcsr, zero
      [0x80000ebc]:sw t6, 664(fp)
 -- Signature Address: 0x80002870 Data: 0x3E03B9A2
 -- Redundant Coverpoints hit by the op
      - mnemonic : fdiv.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0xfc and fm1 == 0x03b9a1 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fdiv.s', 'rs1 : x30', 'rs2 : x31', 'rd : x31', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x60f718 and fs2 == 1 and fe2 == 0xe9 and fm2 == 0x33f8e0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000124]:fdiv.s t6, t5, t6, dyn
	-[0x80000128]:csrrs tp, fcsr, zero
	-[0x8000012c]:sw t6, 0(ra)
Current Store : [0x80000130] : sw tp, 4(ra) -- Store: [0x80002514]:0x00000000




Last Coverpoint : ['rs1 : x29', 'rs2 : x30', 'rd : x29', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a0433 and fs2 == 0 and fe2 == 0xb9 and fm2 == 0x1dbba8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000144]:fdiv.s t4, t4, t5, dyn
	-[0x80000148]:csrrs tp, fcsr, zero
	-[0x8000014c]:sw t4, 8(ra)
Current Store : [0x80000150] : sw tp, 12(ra) -- Store: [0x8000251c]:0x00000000




Last Coverpoint : ['rs1 : x31', 'rs2 : x29', 'rd : x30', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x761c0c and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000164]:fdiv.s t5, t6, t4, dyn
	-[0x80000168]:csrrs tp, fcsr, zero
	-[0x8000016c]:sw t5, 16(ra)
Current Store : [0x80000170] : sw tp, 20(ra) -- Store: [0x80002524]:0x00000001




Last Coverpoint : ['rs1 : x27', 'rs2 : x27', 'rd : x28', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x80000184]:fdiv.s t3, s11, s11, dyn
	-[0x80000188]:csrrs tp, fcsr, zero
	-[0x8000018c]:sw t3, 24(ra)
Current Store : [0x80000190] : sw tp, 28(ra) -- Store: [0x8000252c]:0x00000000




Last Coverpoint : ['rs1 : x26', 'rs2 : x26', 'rd : x26', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x800001a4]:fdiv.s s10, s10, s10, dyn
	-[0x800001a8]:csrrs tp, fcsr, zero
	-[0x800001ac]:sw s10, 32(ra)
Current Store : [0x800001b0] : sw tp, 36(ra) -- Store: [0x80002534]:0x00000000




Last Coverpoint : ['rs1 : x28', 'rs2 : x25', 'rd : x27', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x512a66 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001c4]:fdiv.s s11, t3, s9, dyn
	-[0x800001c8]:csrrs tp, fcsr, zero
	-[0x800001cc]:sw s11, 40(ra)
Current Store : [0x800001d0] : sw tp, 44(ra) -- Store: [0x8000253c]:0x00000001




Last Coverpoint : ['rs1 : x24', 'rs2 : x28', 'rd : x25', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x296bac and fs2 == 1 and fe2 == 0x97 and fm2 == 0x169899 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001e4]:fdiv.s s9, s8, t3, dyn
	-[0x800001e8]:csrrs tp, fcsr, zero
	-[0x800001ec]:sw s9, 48(ra)
Current Store : [0x800001f0] : sw tp, 52(ra) -- Store: [0x80002544]:0x00000001




Last Coverpoint : ['rs1 : x25', 'rs2 : x23', 'rd : x24', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x3b506b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000204]:fdiv.s s8, s9, s7, dyn
	-[0x80000208]:csrrs tp, fcsr, zero
	-[0x8000020c]:sw s8, 56(ra)
Current Store : [0x80000210] : sw tp, 60(ra) -- Store: [0x8000254c]:0x00000001




Last Coverpoint : ['rs1 : x22', 'rs2 : x24', 'rd : x23', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x192dff and fs2 == 0 and fe2 == 0xb8 and fm2 == 0x236443 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000224]:fdiv.s s7, s6, s8, dyn
	-[0x80000228]:csrrs tp, fcsr, zero
	-[0x8000022c]:sw s7, 64(ra)
Current Store : [0x80000230] : sw tp, 68(ra) -- Store: [0x80002554]:0x00000001




Last Coverpoint : ['rs1 : x23', 'rs2 : x21', 'rd : x22', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x465fcc and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000244]:fdiv.s s6, s7, s5, dyn
	-[0x80000248]:csrrs tp, fcsr, zero
	-[0x8000024c]:sw s6, 72(ra)
Current Store : [0x80000250] : sw tp, 76(ra) -- Store: [0x8000255c]:0x00000001




Last Coverpoint : ['rs1 : x20', 'rs2 : x22', 'rd : x21', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x2b6a13 and fs2 == 0 and fe2 == 0xad and fm2 == 0x4b2862 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000264]:fdiv.s s5, s4, s6, dyn
	-[0x80000268]:csrrs tp, fcsr, zero
	-[0x8000026c]:sw s5, 80(ra)
Current Store : [0x80000270] : sw tp, 84(ra) -- Store: [0x80002564]:0x00000001




Last Coverpoint : ['rs1 : x21', 'rs2 : x19', 'rd : x20', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x7906c5 and fs2 == 0 and fe2 == 0x8b and fm2 == 0x1f607e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000284]:fdiv.s s4, s5, s3, dyn
	-[0x80000288]:csrrs tp, fcsr, zero
	-[0x8000028c]:sw s4, 88(ra)
Current Store : [0x80000290] : sw tp, 92(ra) -- Store: [0x8000256c]:0x00000001




Last Coverpoint : ['rs1 : x18', 'rs2 : x20', 'rd : x19', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x17a40d and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002a4]:fdiv.s s3, s2, s4, dyn
	-[0x800002a8]:csrrs tp, fcsr, zero
	-[0x800002ac]:sw s3, 96(ra)
Current Store : [0x800002b0] : sw tp, 100(ra) -- Store: [0x80002574]:0x00000001




Last Coverpoint : ['rs1 : x19', 'rs2 : x17', 'rd : x18', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x5d1ff5 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002c4]:fdiv.s s2, s3, a7, dyn
	-[0x800002c8]:csrrs tp, fcsr, zero
	-[0x800002cc]:sw s2, 104(ra)
Current Store : [0x800002d0] : sw tp, 108(ra) -- Store: [0x8000257c]:0x00000001




Last Coverpoint : ['rs1 : x16', 'rs2 : x18', 'rd : x17', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x76b77e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002e4]:fdiv.s a7, a6, s2, dyn
	-[0x800002e8]:csrrs tp, fcsr, zero
	-[0x800002ec]:sw a7, 112(ra)
Current Store : [0x800002f0] : sw tp, 116(ra) -- Store: [0x80002584]:0x00000001




Last Coverpoint : ['rs1 : x17', 'rs2 : x15', 'rd : x16', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x7248b2 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000304]:fdiv.s a6, a7, a5, dyn
	-[0x80000308]:csrrs tp, fcsr, zero
	-[0x8000030c]:sw a6, 120(ra)
Current Store : [0x80000310] : sw tp, 124(ra) -- Store: [0x8000258c]:0x00000001




Last Coverpoint : ['rs1 : x14', 'rs2 : x16', 'rd : x15', 'fs1 == 0 and fe1 == 0xfa and fm1 == 0x34967e and fs2 == 0 and fe2 == 0x81 and fm2 == 0x142cb6 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000324]:fdiv.s a5, a4, a6, dyn
	-[0x80000328]:csrrs tp, fcsr, zero
	-[0x8000032c]:sw a5, 128(ra)
Current Store : [0x80000330] : sw tp, 132(ra) -- Store: [0x80002594]:0x00000001




Last Coverpoint : ['rs1 : x15', 'rs2 : x13', 'rd : x14', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x655450 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000344]:fdiv.s a4, a5, a3, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:sw a4, 136(ra)
Current Store : [0x80000350] : sw tp, 140(ra) -- Store: [0x8000259c]:0x00000001




Last Coverpoint : ['rs1 : x12', 'rs2 : x14', 'rd : x13', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x39d661 and fs2 == 0 and fe2 == 0xba and fm2 == 0x187b63 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000364]:fdiv.s a3, a2, a4, dyn
	-[0x80000368]:csrrs tp, fcsr, zero
	-[0x8000036c]:sw a3, 144(ra)
Current Store : [0x80000370] : sw tp, 148(ra) -- Store: [0x800025a4]:0x00000001




Last Coverpoint : ['rs1 : x13', 'rs2 : x11', 'rd : x12', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x281a41 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x2ac557 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000384]:fdiv.s a2, a3, a1, dyn
	-[0x80000388]:csrrs tp, fcsr, zero
	-[0x8000038c]:sw a2, 152(ra)
Current Store : [0x80000390] : sw tp, 156(ra) -- Store: [0x800025ac]:0x00000001




Last Coverpoint : ['rs1 : x10', 'rs2 : x12', 'rd : x11', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x6b4e0e and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003a4]:fdiv.s a1, a0, a2, dyn
	-[0x800003a8]:csrrs tp, fcsr, zero
	-[0x800003ac]:sw a1, 160(ra)
Current Store : [0x800003b0] : sw tp, 164(ra) -- Store: [0x800025b4]:0x00000001




Last Coverpoint : ['rs1 : x11', 'rs2 : x9', 'rd : x10', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x24d5b2 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003c4]:fdiv.s a0, a1, s1, dyn
	-[0x800003c8]:csrrs tp, fcsr, zero
	-[0x800003cc]:sw a0, 168(ra)
Current Store : [0x800003d0] : sw tp, 172(ra) -- Store: [0x800025bc]:0x00000001




Last Coverpoint : ['rs1 : x8', 'rs2 : x10', 'rd : x9', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x6e5bf8 and fs2 == 0 and fe2 == 0x88 and fm2 == 0x24083c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003ec]:fdiv.s s1, fp, a0, dyn
	-[0x800003f0]:csrrs a2, fcsr, zero
	-[0x800003f4]:sw s1, 176(ra)
Current Store : [0x800003f8] : sw a2, 180(ra) -- Store: [0x800025c4]:0x00000001




Last Coverpoint : ['rs1 : x9', 'rs2 : x7', 'rd : x8', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x3457e7 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000040c]:fdiv.s fp, s1, t2, dyn
	-[0x80000410]:csrrs a2, fcsr, zero
	-[0x80000414]:sw fp, 184(ra)
Current Store : [0x80000418] : sw a2, 188(ra) -- Store: [0x800025cc]:0x00000001




Last Coverpoint : ['rs1 : x6', 'rs2 : x8', 'rd : x7', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x0ed153 and fs2 == 1 and fe2 == 0x92 and fm2 == 0x1c3eb7 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000042c]:fdiv.s t2, t1, fp, dyn
	-[0x80000430]:csrrs a2, fcsr, zero
	-[0x80000434]:sw t2, 192(ra)
Current Store : [0x80000438] : sw a2, 196(ra) -- Store: [0x800025d4]:0x00000001




Last Coverpoint : ['rs1 : x7', 'rs2 : x5', 'rd : x6', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x1fcf65 and fs2 == 0 and fe2 == 0xd7 and fm2 == 0x25a218 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000454]:fdiv.s t1, t2, t0, dyn
	-[0x80000458]:csrrs a2, fcsr, zero
	-[0x8000045c]:sw t1, 0(fp)
Current Store : [0x80000460] : sw a2, 4(fp) -- Store: [0x800025dc]:0x00000001




Last Coverpoint : ['rs1 : x4', 'rs2 : x6', 'rd : x5', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x41cf9e and fs2 == 1 and fe2 == 0xda and fm2 == 0x244a46 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000474]:fdiv.s t0, tp, t1, dyn
	-[0x80000478]:csrrs a2, fcsr, zero
	-[0x8000047c]:sw t0, 8(fp)
Current Store : [0x80000480] : sw a2, 12(fp) -- Store: [0x800025e4]:0x00000001




Last Coverpoint : ['rs1 : x5', 'rs2 : x3', 'rd : x4', 'fs1 == 0 and fe1 == 0xf7 and fm1 == 0x506932 and fs2 == 0 and fe2 == 0x99 and fm2 == 0x716acc and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000494]:fdiv.s tp, t0, gp, dyn
	-[0x80000498]:csrrs a2, fcsr, zero
	-[0x8000049c]:sw tp, 16(fp)
Current Store : [0x800004a0] : sw a2, 20(fp) -- Store: [0x800025ec]:0x00000001




Last Coverpoint : ['rs1 : x2', 'rs2 : x4', 'rd : x3', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x28b6bd and fs2 == 0 and fe2 == 0xbf and fm2 == 0x17043e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004b4]:fdiv.s gp, sp, tp, dyn
	-[0x800004b8]:csrrs a2, fcsr, zero
	-[0x800004bc]:sw gp, 24(fp)
Current Store : [0x800004c0] : sw a2, 28(fp) -- Store: [0x800025f4]:0x00000001




Last Coverpoint : ['rs1 : x3', 'rs2 : x1', 'rd : x2', 'fs1 == 0 and fe1 == 0xf9 and fm1 == 0x68f58b and fs2 == 1 and fe2 == 0xbf and fm2 == 0x756c07 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004d4]:fdiv.s sp, gp, ra, dyn
	-[0x800004d8]:csrrs a2, fcsr, zero
	-[0x800004dc]:sw sp, 32(fp)
Current Store : [0x800004e0] : sw a2, 36(fp) -- Store: [0x800025fc]:0x00000001




Last Coverpoint : ['rs1 : x0', 'rs2 : x2', 'rd : x1']
Last Code Sequence : 
	-[0x800004f4]:fdiv.s ra, zero, sp, dyn
	-[0x800004f8]:csrrs a2, fcsr, zero
	-[0x800004fc]:sw ra, 40(fp)
Current Store : [0x80000500] : sw a2, 44(fp) -- Store: [0x80002604]:0x00000000




Last Coverpoint : ['rs1 : x1', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x38be1b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000514]:fdiv.s t6, ra, t5, dyn
	-[0x80000518]:csrrs a2, fcsr, zero
	-[0x8000051c]:sw t6, 48(fp)
Current Store : [0x80000520] : sw a2, 52(fp) -- Store: [0x8000260c]:0x00000001




Last Coverpoint : ['rs2 : x0']
Last Code Sequence : 
	-[0x80000534]:fdiv.s t6, t5, zero, dyn
	-[0x80000538]:csrrs a2, fcsr, zero
	-[0x8000053c]:sw t6, 56(fp)
Current Store : [0x80000540] : sw a2, 60(fp) -- Store: [0x80002614]:0x00000008




Last Coverpoint : ['rd : x0', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x03b9a1 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000554]:fdiv.s zero, t6, t5, dyn
	-[0x80000558]:csrrs a2, fcsr, zero
	-[0x8000055c]:sw zero, 64(fp)
Current Store : [0x80000560] : sw a2, 68(fp) -- Store: [0x8000261c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x269d2c and fs2 == 1 and fe2 == 0x83 and fm2 == 0x519910 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000574]:fdiv.s t6, t5, t4, dyn
	-[0x80000578]:csrrs a2, fcsr, zero
	-[0x8000057c]:sw t6, 72(fp)
Current Store : [0x80000580] : sw a2, 76(fp) -- Store: [0x80002624]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3af6ff and fs2 == 1 and fe2 == 0xa4 and fm2 == 0x799c89 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000594]:fdiv.s t6, t5, t4, dyn
	-[0x80000598]:csrrs a2, fcsr, zero
	-[0x8000059c]:sw t6, 80(fp)
Current Store : [0x800005a0] : sw a2, 84(fp) -- Store: [0x8000262c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x072c24 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005b4]:fdiv.s t6, t5, t4, dyn
	-[0x800005b8]:csrrs a2, fcsr, zero
	-[0x800005bc]:sw t6, 88(fp)
Current Store : [0x800005c0] : sw a2, 92(fp) -- Store: [0x80002634]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x436852 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005d4]:fdiv.s t6, t5, t4, dyn
	-[0x800005d8]:csrrs a2, fcsr, zero
	-[0x800005dc]:sw t6, 96(fp)
Current Store : [0x800005e0] : sw a2, 100(fp) -- Store: [0x8000263c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x1123d8 and fs2 == 1 and fe2 == 0xbd and fm2 == 0x695156 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005f4]:fdiv.s t6, t5, t4, dyn
	-[0x800005f8]:csrrs a2, fcsr, zero
	-[0x800005fc]:sw t6, 104(fp)
Current Store : [0x80000600] : sw a2, 108(fp) -- Store: [0x80002644]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x0538b1 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000614]:fdiv.s t6, t5, t4, dyn
	-[0x80000618]:csrrs a2, fcsr, zero
	-[0x8000061c]:sw t6, 112(fp)
Current Store : [0x80000620] : sw a2, 116(fp) -- Store: [0x8000264c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1e0667 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000634]:fdiv.s t6, t5, t4, dyn
	-[0x80000638]:csrrs a2, fcsr, zero
	-[0x8000063c]:sw t6, 120(fp)
Current Store : [0x80000640] : sw a2, 124(fp) -- Store: [0x80002654]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x016ff7 and fs2 == 1 and fe2 == 0xc9 and fm2 == 0x0990a3 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000654]:fdiv.s t6, t5, t4, dyn
	-[0x80000658]:csrrs a2, fcsr, zero
	-[0x8000065c]:sw t6, 128(fp)
Current Store : [0x80000660] : sw a2, 132(fp) -- Store: [0x8000265c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2640ba and fs2 == 0 and fe2 == 0xbf and fm2 == 0x4688b4 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000674]:fdiv.s t6, t5, t4, dyn
	-[0x80000678]:csrrs a2, fcsr, zero
	-[0x8000067c]:sw t6, 136(fp)
Current Store : [0x80000680] : sw a2, 140(fp) -- Store: [0x80002664]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x151546 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000694]:fdiv.s t6, t5, t4, dyn
	-[0x80000698]:csrrs a2, fcsr, zero
	-[0x8000069c]:sw t6, 144(fp)
Current Store : [0x800006a0] : sw a2, 148(fp) -- Store: [0x8000266c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x206546 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006b4]:fdiv.s t6, t5, t4, dyn
	-[0x800006b8]:csrrs a2, fcsr, zero
	-[0x800006bc]:sw t6, 152(fp)
Current Store : [0x800006c0] : sw a2, 156(fp) -- Store: [0x80002674]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7fba49 and fs2 == 1 and fe2 == 0xf7 and fm2 == 0x5bc718 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006d4]:fdiv.s t6, t5, t4, dyn
	-[0x800006d8]:csrrs a2, fcsr, zero
	-[0x800006dc]:sw t6, 160(fp)
Current Store : [0x800006e0] : sw a2, 164(fp) -- Store: [0x8000267c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x636240 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006f4]:fdiv.s t6, t5, t4, dyn
	-[0x800006f8]:csrrs a2, fcsr, zero
	-[0x800006fc]:sw t6, 168(fp)
Current Store : [0x80000700] : sw a2, 172(fp) -- Store: [0x80002684]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3fec54 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000714]:fdiv.s t6, t5, t4, dyn
	-[0x80000718]:csrrs a2, fcsr, zero
	-[0x8000071c]:sw t6, 176(fp)
Current Store : [0x80000720] : sw a2, 180(fp) -- Store: [0x8000268c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x79dd8e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000734]:fdiv.s t6, t5, t4, dyn
	-[0x80000738]:csrrs a2, fcsr, zero
	-[0x8000073c]:sw t6, 184(fp)
Current Store : [0x80000740] : sw a2, 188(fp) -- Store: [0x80002694]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2759f0 and fs2 == 1 and fe2 == 0x81 and fm2 == 0x351a1b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000754]:fdiv.s t6, t5, t4, dyn
	-[0x80000758]:csrrs a2, fcsr, zero
	-[0x8000075c]:sw t6, 192(fp)
Current Store : [0x80000760] : sw a2, 196(fp) -- Store: [0x8000269c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2cbcfc and fs2 == 0 and fe2 == 0xe8 and fm2 == 0x58de83 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000774]:fdiv.s t6, t5, t4, dyn
	-[0x80000778]:csrrs a2, fcsr, zero
	-[0x8000077c]:sw t6, 200(fp)
Current Store : [0x80000780] : sw a2, 204(fp) -- Store: [0x800026a4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x5e539a and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000794]:fdiv.s t6, t5, t4, dyn
	-[0x80000798]:csrrs a2, fcsr, zero
	-[0x8000079c]:sw t6, 208(fp)
Current Store : [0x800007a0] : sw a2, 212(fp) -- Store: [0x800026ac]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1a414e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007b4]:fdiv.s t6, t5, t4, dyn
	-[0x800007b8]:csrrs a2, fcsr, zero
	-[0x800007bc]:sw t6, 216(fp)
Current Store : [0x800007c0] : sw a2, 220(fp) -- Store: [0x800026b4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x7bb471 and fs2 == 0 and fe2 == 0xb6 and fm2 == 0x789bff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007d4]:fdiv.s t6, t5, t4, dyn
	-[0x800007d8]:csrrs a2, fcsr, zero
	-[0x800007dc]:sw t6, 224(fp)
Current Store : [0x800007e0] : sw a2, 228(fp) -- Store: [0x800026bc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3d8377 and fs2 == 1 and fe2 == 0x91 and fm2 == 0x76277d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007f4]:fdiv.s t6, t5, t4, dyn
	-[0x800007f8]:csrrs a2, fcsr, zero
	-[0x800007fc]:sw t6, 232(fp)
Current Store : [0x80000800] : sw a2, 236(fp) -- Store: [0x800026c4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x164749 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000814]:fdiv.s t6, t5, t4, dyn
	-[0x80000818]:csrrs a2, fcsr, zero
	-[0x8000081c]:sw t6, 240(fp)
Current Store : [0x80000820] : sw a2, 244(fp) -- Store: [0x800026cc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x023675 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000834]:fdiv.s t6, t5, t4, dyn
	-[0x80000838]:csrrs a2, fcsr, zero
	-[0x8000083c]:sw t6, 248(fp)
Current Store : [0x80000840] : sw a2, 252(fp) -- Store: [0x800026d4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x239b5c and fs2 == 0 and fe2 == 0xe7 and fm2 == 0x311bbd and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000854]:fdiv.s t6, t5, t4, dyn
	-[0x80000858]:csrrs a2, fcsr, zero
	-[0x8000085c]:sw t6, 256(fp)
Current Store : [0x80000860] : sw a2, 260(fp) -- Store: [0x800026dc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x017ed0 and fs2 == 0 and fe2 == 0x94 and fm2 == 0x317fe2 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000874]:fdiv.s t6, t5, t4, dyn
	-[0x80000878]:csrrs a2, fcsr, zero
	-[0x8000087c]:sw t6, 264(fp)
Current Store : [0x80000880] : sw a2, 268(fp) -- Store: [0x800026e4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x20d6ae and fs2 == 1 and fe2 == 0xf0 and fm2 == 0x41dcec and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000894]:fdiv.s t6, t5, t4, dyn
	-[0x80000898]:csrrs a2, fcsr, zero
	-[0x8000089c]:sw t6, 272(fp)
Current Store : [0x800008a0] : sw a2, 276(fp) -- Store: [0x800026ec]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x32fae0 and fs2 == 0 and fe2 == 0xc2 and fm2 == 0x36e76b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008b4]:fdiv.s t6, t5, t4, dyn
	-[0x800008b8]:csrrs a2, fcsr, zero
	-[0x800008bc]:sw t6, 280(fp)
Current Store : [0x800008c0] : sw a2, 284(fp) -- Store: [0x800026f4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x0409cf and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x3ed825 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008d4]:fdiv.s t6, t5, t4, dyn
	-[0x800008d8]:csrrs a2, fcsr, zero
	-[0x800008dc]:sw t6, 288(fp)
Current Store : [0x800008e0] : sw a2, 292(fp) -- Store: [0x800026fc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x296b63 and fs2 == 1 and fe2 == 0x88 and fm2 == 0x3a1fe4 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008f4]:fdiv.s t6, t5, t4, dyn
	-[0x800008f8]:csrrs a2, fcsr, zero
	-[0x800008fc]:sw t6, 296(fp)
Current Store : [0x80000900] : sw a2, 300(fp) -- Store: [0x80002704]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x03ecd6 and fs2 == 1 and fe2 == 0xbf and fm2 == 0x081dd1 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000914]:fdiv.s t6, t5, t4, dyn
	-[0x80000918]:csrrs a2, fcsr, zero
	-[0x8000091c]:sw t6, 304(fp)
Current Store : [0x80000920] : sw a2, 308(fp) -- Store: [0x8000270c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a0c29 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000934]:fdiv.s t6, t5, t4, dyn
	-[0x80000938]:csrrs a2, fcsr, zero
	-[0x8000093c]:sw t6, 312(fp)
Current Store : [0x80000940] : sw a2, 316(fp) -- Store: [0x80002714]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3d37b2 and fs2 == 0 and fe2 == 0xed and fm2 == 0x54eb08 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000954]:fdiv.s t6, t5, t4, dyn
	-[0x80000958]:csrrs a2, fcsr, zero
	-[0x8000095c]:sw t6, 320(fp)
Current Store : [0x80000960] : sw a2, 324(fp) -- Store: [0x8000271c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x34342f and fs2 == 0 and fe2 == 0x87 and fm2 == 0x7cb837 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000974]:fdiv.s t6, t5, t4, dyn
	-[0x80000978]:csrrs a2, fcsr, zero
	-[0x8000097c]:sw t6, 328(fp)
Current Store : [0x80000980] : sw a2, 332(fp) -- Store: [0x80002724]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0b4a10 and fs2 == 0 and fe2 == 0x9e and fm2 == 0x3c1678 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000994]:fdiv.s t6, t5, t4, dyn
	-[0x80000998]:csrrs a2, fcsr, zero
	-[0x8000099c]:sw t6, 336(fp)
Current Store : [0x800009a0] : sw a2, 340(fp) -- Store: [0x8000272c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x02119e and fs2 == 0 and fe2 == 0xec and fm2 == 0x25b1fa and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009b4]:fdiv.s t6, t5, t4, dyn
	-[0x800009b8]:csrrs a2, fcsr, zero
	-[0x800009bc]:sw t6, 344(fp)
Current Store : [0x800009c0] : sw a2, 348(fp) -- Store: [0x80002734]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x025339 and fs2 == 0 and fe2 == 0xf9 and fm2 == 0x064930 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009d4]:fdiv.s t6, t5, t4, dyn
	-[0x800009d8]:csrrs a2, fcsr, zero
	-[0x800009dc]:sw t6, 352(fp)
Current Store : [0x800009e0] : sw a2, 356(fp) -- Store: [0x8000273c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2ee8de and fs2 == 0 and fe2 == 0xac and fm2 == 0x6b9400 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009f4]:fdiv.s t6, t5, t4, dyn
	-[0x800009f8]:csrrs a2, fcsr, zero
	-[0x800009fc]:sw t6, 360(fp)
Current Store : [0x80000a00] : sw a2, 364(fp) -- Store: [0x80002744]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x238f3f and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a14]:fdiv.s t6, t5, t4, dyn
	-[0x80000a18]:csrrs a2, fcsr, zero
	-[0x80000a1c]:sw t6, 368(fp)
Current Store : [0x80000a20] : sw a2, 372(fp) -- Store: [0x8000274c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x11c013 and fs2 == 1 and fe2 == 0xf2 and fm2 == 0x3d7983 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a34]:fdiv.s t6, t5, t4, dyn
	-[0x80000a38]:csrrs a2, fcsr, zero
	-[0x80000a3c]:sw t6, 376(fp)
Current Store : [0x80000a40] : sw a2, 380(fp) -- Store: [0x80002754]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x667aed and fs2 == 0 and fe2 == 0xe0 and fm2 == 0x0103f8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a54]:fdiv.s t6, t5, t4, dyn
	-[0x80000a58]:csrrs a2, fcsr, zero
	-[0x80000a5c]:sw t6, 384(fp)
Current Store : [0x80000a60] : sw a2, 388(fp) -- Store: [0x8000275c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x217f53 and fs2 == 0 and fe2 == 0xe3 and fm2 == 0x5d82bd and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a74]:fdiv.s t6, t5, t4, dyn
	-[0x80000a78]:csrrs a2, fcsr, zero
	-[0x80000a7c]:sw t6, 392(fp)
Current Store : [0x80000a80] : sw a2, 396(fp) -- Store: [0x80002764]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x0d5a7c and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a94]:fdiv.s t6, t5, t4, dyn
	-[0x80000a98]:csrrs a2, fcsr, zero
	-[0x80000a9c]:sw t6, 400(fp)
Current Store : [0x80000aa0] : sw a2, 404(fp) -- Store: [0x8000276c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x02ab65 and fs2 == 1 and fe2 == 0xef and fm2 == 0x5dffc1 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ab4]:fdiv.s t6, t5, t4, dyn
	-[0x80000ab8]:csrrs a2, fcsr, zero
	-[0x80000abc]:sw t6, 408(fp)
Current Store : [0x80000ac0] : sw a2, 412(fp) -- Store: [0x80002774]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x5fa740 and fs2 == 0 and fe2 == 0x9f and fm2 == 0x3d6968 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ad4]:fdiv.s t6, t5, t4, dyn
	-[0x80000ad8]:csrrs a2, fcsr, zero
	-[0x80000adc]:sw t6, 416(fp)
Current Store : [0x80000ae0] : sw a2, 420(fp) -- Store: [0x8000277c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x268b6a and fs2 == 1 and fe2 == 0xef and fm2 == 0x3935cd and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000af4]:fdiv.s t6, t5, t4, dyn
	-[0x80000af8]:csrrs a2, fcsr, zero
	-[0x80000afc]:sw t6, 424(fp)
Current Store : [0x80000b00] : sw a2, 428(fp) -- Store: [0x80002784]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x67ede5 and fs2 == 1 and fe2 == 0xaa and fm2 == 0x7a8f9b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b14]:fdiv.s t6, t5, t4, dyn
	-[0x80000b18]:csrrs a2, fcsr, zero
	-[0x80000b1c]:sw t6, 432(fp)
Current Store : [0x80000b20] : sw a2, 436(fp) -- Store: [0x8000278c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x373a1e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b34]:fdiv.s t6, t5, t4, dyn
	-[0x80000b38]:csrrs a2, fcsr, zero
	-[0x80000b3c]:sw t6, 440(fp)
Current Store : [0x80000b40] : sw a2, 444(fp) -- Store: [0x80002794]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2a5ada and fs2 == 0 and fe2 == 0xd0 and fm2 == 0x2f4d13 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b54]:fdiv.s t6, t5, t4, dyn
	-[0x80000b58]:csrrs a2, fcsr, zero
	-[0x80000b5c]:sw t6, 448(fp)
Current Store : [0x80000b60] : sw a2, 452(fp) -- Store: [0x8000279c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4fe433 and fs2 == 0 and fe2 == 0x9c and fm2 == 0x3feae5 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b74]:fdiv.s t6, t5, t4, dyn
	-[0x80000b78]:csrrs a2, fcsr, zero
	-[0x80000b7c]:sw t6, 456(fp)
Current Store : [0x80000b80] : sw a2, 460(fp) -- Store: [0x800027a4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x53a642 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b94]:fdiv.s t6, t5, t4, dyn
	-[0x80000b98]:csrrs a2, fcsr, zero
	-[0x80000b9c]:sw t6, 464(fp)
Current Store : [0x80000ba0] : sw a2, 468(fp) -- Store: [0x800027ac]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x202a98 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bb4]:fdiv.s t6, t5, t4, dyn
	-[0x80000bb8]:csrrs a2, fcsr, zero
	-[0x80000bbc]:sw t6, 472(fp)
Current Store : [0x80000bc0] : sw a2, 476(fp) -- Store: [0x800027b4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x70766e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bd4]:fdiv.s t6, t5, t4, dyn
	-[0x80000bd8]:csrrs a2, fcsr, zero
	-[0x80000bdc]:sw t6, 480(fp)
Current Store : [0x80000be0] : sw a2, 484(fp) -- Store: [0x800027bc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x60d9a4 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bf4]:fdiv.s t6, t5, t4, dyn
	-[0x80000bf8]:csrrs a2, fcsr, zero
	-[0x80000bfc]:sw t6, 488(fp)
Current Store : [0x80000c00] : sw a2, 492(fp) -- Store: [0x800027c4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x264de7 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c14]:fdiv.s t6, t5, t4, dyn
	-[0x80000c18]:csrrs a2, fcsr, zero
	-[0x80000c1c]:sw t6, 496(fp)
Current Store : [0x80000c20] : sw a2, 500(fp) -- Store: [0x800027cc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x17517f and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c34]:fdiv.s t6, t5, t4, dyn
	-[0x80000c38]:csrrs a2, fcsr, zero
	-[0x80000c3c]:sw t6, 504(fp)
Current Store : [0x80000c40] : sw a2, 508(fp) -- Store: [0x800027d4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1f12b9 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c54]:fdiv.s t6, t5, t4, dyn
	-[0x80000c58]:csrrs a2, fcsr, zero
	-[0x80000c5c]:sw t6, 512(fp)
Current Store : [0x80000c60] : sw a2, 516(fp) -- Store: [0x800027dc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x02e795 and fs2 == 1 and fe2 == 0xd6 and fm2 == 0x4dad56 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c74]:fdiv.s t6, t5, t4, dyn
	-[0x80000c78]:csrrs a2, fcsr, zero
	-[0x80000c7c]:sw t6, 520(fp)
Current Store : [0x80000c80] : sw a2, 524(fp) -- Store: [0x800027e4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6c3b3e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c94]:fdiv.s t6, t5, t4, dyn
	-[0x80000c98]:csrrs a2, fcsr, zero
	-[0x80000c9c]:sw t6, 528(fp)
Current Store : [0x80000ca0] : sw a2, 532(fp) -- Store: [0x800027ec]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x16201f and fs2 == 0 and fe2 == 0xfd and fm2 == 0x631e55 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cb4]:fdiv.s t6, t5, t4, dyn
	-[0x80000cb8]:csrrs a2, fcsr, zero
	-[0x80000cbc]:sw t6, 536(fp)
Current Store : [0x80000cc0] : sw a2, 540(fp) -- Store: [0x800027f4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x112ace and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cd4]:fdiv.s t6, t5, t4, dyn
	-[0x80000cd8]:csrrs a2, fcsr, zero
	-[0x80000cdc]:sw t6, 544(fp)
Current Store : [0x80000ce0] : sw a2, 548(fp) -- Store: [0x800027fc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x5b2e1a and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cf4]:fdiv.s t6, t5, t4, dyn
	-[0x80000cf8]:csrrs a2, fcsr, zero
	-[0x80000cfc]:sw t6, 552(fp)
Current Store : [0x80000d00] : sw a2, 556(fp) -- Store: [0x80002804]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2bcff9 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d14]:fdiv.s t6, t5, t4, dyn
	-[0x80000d18]:csrrs a2, fcsr, zero
	-[0x80000d1c]:sw t6, 560(fp)
Current Store : [0x80000d20] : sw a2, 564(fp) -- Store: [0x8000280c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x7b1d83 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d34]:fdiv.s t6, t5, t4, dyn
	-[0x80000d38]:csrrs a2, fcsr, zero
	-[0x80000d3c]:sw t6, 568(fp)
Current Store : [0x80000d40] : sw a2, 572(fp) -- Store: [0x80002814]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x4cd7ff and fs2 == 1 and fe2 == 0xf8 and fm2 == 0x6ccaee and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d54]:fdiv.s t6, t5, t4, dyn
	-[0x80000d58]:csrrs a2, fcsr, zero
	-[0x80000d5c]:sw t6, 576(fp)
Current Store : [0x80000d60] : sw a2, 580(fp) -- Store: [0x8000281c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2b0708 and fs2 == 1 and fe2 == 0x86 and fm2 == 0x0d7389 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d74]:fdiv.s t6, t5, t4, dyn
	-[0x80000d78]:csrrs a2, fcsr, zero
	-[0x80000d7c]:sw t6, 584(fp)
Current Store : [0x80000d80] : sw a2, 588(fp) -- Store: [0x80002824]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x58bf61 and fs2 == 1 and fe2 == 0xab and fm2 == 0x17657f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d94]:fdiv.s t6, t5, t4, dyn
	-[0x80000d98]:csrrs a2, fcsr, zero
	-[0x80000d9c]:sw t6, 592(fp)
Current Store : [0x80000da0] : sw a2, 596(fp) -- Store: [0x8000282c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x319ce6 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000db4]:fdiv.s t6, t5, t4, dyn
	-[0x80000db8]:csrrs a2, fcsr, zero
	-[0x80000dbc]:sw t6, 600(fp)
Current Store : [0x80000dc0] : sw a2, 604(fp) -- Store: [0x80002834]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2fc88c and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000dd4]:fdiv.s t6, t5, t4, dyn
	-[0x80000dd8]:csrrs a2, fcsr, zero
	-[0x80000ddc]:sw t6, 608(fp)
Current Store : [0x80000de0] : sw a2, 612(fp) -- Store: [0x8000283c]:0x00000001




Last Coverpoint : ['mnemonic : fdiv.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x60f718 and fs2 == 1 and fe2 == 0xe9 and fm2 == 0x33f8e0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000df4]:fdiv.s t6, t5, t4, dyn
	-[0x80000df8]:csrrs a2, fcsr, zero
	-[0x80000dfc]:sw t6, 616(fp)
Current Store : [0x80000e00] : sw a2, 620(fp) -- Store: [0x80002844]:0x00000000




Last Coverpoint : ['mnemonic : fdiv.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a0433 and fs2 == 0 and fe2 == 0xb9 and fm2 == 0x1dbba8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e14]:fdiv.s t6, t5, t4, dyn
	-[0x80000e18]:csrrs a2, fcsr, zero
	-[0x80000e1c]:sw t6, 624(fp)
Current Store : [0x80000e20] : sw a2, 628(fp) -- Store: [0x8000284c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0748c6 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e34]:fdiv.s t6, t5, t4, dyn
	-[0x80000e38]:csrrs a2, fcsr, zero
	-[0x80000e3c]:sw t6, 632(fp)
Current Store : [0x80000e40] : sw a2, 636(fp) -- Store: [0x80002854]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0235b2 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e54]:fdiv.s t6, t5, t4, dyn
	-[0x80000e58]:csrrs a2, fcsr, zero
	-[0x80000e5c]:sw t6, 640(fp)
Current Store : [0x80000e60] : sw a2, 644(fp) -- Store: [0x8000285c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d53d7 and fs2 == 1 and fe2 == 0xfc and fm2 == 0x377d30 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e74]:fdiv.s t6, t5, t4, dyn
	-[0x80000e78]:csrrs a2, fcsr, zero
	-[0x80000e7c]:sw t6, 648(fp)
Current Store : [0x80000e80] : sw a2, 652(fp) -- Store: [0x80002864]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x5afcdb and fs2 == 1 and fe2 == 0x92 and fm2 == 0x6f10c4 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e94]:fdiv.s t6, t5, t4, dyn
	-[0x80000e98]:csrrs a2, fcsr, zero
	-[0x80000e9c]:sw t6, 656(fp)
Current Store : [0x80000ea0] : sw a2, 660(fp) -- Store: [0x8000286c]:0x00000001




Last Coverpoint : ['mnemonic : fdiv.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x03b9a1 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000eb4]:fdiv.s t6, t5, t4, dyn
	-[0x80000eb8]:csrrs a2, fcsr, zero
	-[0x80000ebc]:sw t6, 664(fp)
Current Store : [0x80000ec0] : sw a2, 668(fp) -- Store: [0x80002874]:0x00000001





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                                coverpoints                                                                                                                 |                                                      code                                                       |
|---:|--------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|
|   1|[0x80002510]<br>0xC8A00000|- mnemonic : fdiv.s<br> - rs1 : x30<br> - rs2 : x31<br> - rd : x31<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x60f718 and fs2 == 1 and fe2 == 0xe9 and fm2 == 0x33f8e0 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x80000124]:fdiv.s t6, t5, t6, dyn<br> [0x80000128]:csrrs tp, fcsr, zero<br> [0x8000012c]:sw t6, 0(ra)<br>      |
|   2|[0x80002518]<br>0x61E00000|- rs1 : x29<br> - rs2 : x30<br> - rd : x29<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a0433 and fs2 == 0 and fe2 == 0xb9 and fm2 == 0x1dbba8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                         |[0x80000144]:fdiv.s t4, t4, t5, dyn<br> [0x80000148]:csrrs tp, fcsr, zero<br> [0x8000014c]:sw t4, 8(ra)<br>      |
|   3|[0x80002520]<br>0x3DF61C0D|- rs1 : x31<br> - rs2 : x29<br> - rd : x30<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x761c0c and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>  |[0x80000164]:fdiv.s t5, t6, t4, dyn<br> [0x80000168]:csrrs tp, fcsr, zero<br> [0x8000016c]:sw t5, 16(ra)<br>     |
|   4|[0x80002528]<br>0x3F800000|- rs1 : x27<br> - rs2 : x27<br> - rd : x28<br> - rs1 == rs2 != rd<br>                                                                                                                                                                       |[0x80000184]:fdiv.s t3, s11, s11, dyn<br> [0x80000188]:csrrs tp, fcsr, zero<br> [0x8000018c]:sw t3, 24(ra)<br>   |
|   5|[0x80002530]<br>0x3F800000|- rs1 : x26<br> - rs2 : x26<br> - rd : x26<br> - rs1 == rs2 == rd<br>                                                                                                                                                                       |[0x800001a4]:fdiv.s s10, s10, s10, dyn<br> [0x800001a8]:csrrs tp, fcsr, zero<br> [0x800001ac]:sw s10, 32(ra)<br> |
|   6|[0x80002538]<br>0x3ED12A67|- rs1 : x28<br> - rs2 : x25<br> - rd : x27<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x512a66 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x800001c4]:fdiv.s s11, t3, s9, dyn<br> [0x800001c8]:csrrs tp, fcsr, zero<br> [0x800001cc]:sw s11, 40(ra)<br>   |
|   7|[0x80002540]<br>0xF2100000|- rs1 : x24<br> - rs2 : x28<br> - rd : x25<br> - fs1 == 0 and fe1 == 0xfc and fm1 == 0x296bac and fs2 == 1 and fe2 == 0x97 and fm2 == 0x169899 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x800001e4]:fdiv.s s9, s8, t3, dyn<br> [0x800001e8]:csrrs tp, fcsr, zero<br> [0x800001ec]:sw s9, 48(ra)<br>     |
|   8|[0x80002548]<br>0xBF3B506C|- rs1 : x25<br> - rs2 : x23<br> - rd : x24<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x3b506b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000204]:fdiv.s s8, s9, s7, dyn<br> [0x80000208]:csrrs tp, fcsr, zero<br> [0x8000020c]:sw s8, 56(ra)<br>     |
|   9|[0x80002550]<br>0x62700000|- rs1 : x22<br> - rs2 : x24<br> - rd : x23<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x192dff and fs2 == 0 and fe2 == 0xb8 and fm2 == 0x236443 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000224]:fdiv.s s7, s6, s8, dyn<br> [0x80000228]:csrrs tp, fcsr, zero<br> [0x8000022c]:sw s7, 64(ra)<br>     |
|  10|[0x80002558]<br>0x3EC65FCD|- rs1 : x23<br> - rs2 : x21<br> - rd : x22<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x465fcc and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000244]:fdiv.s s6, s7, s5, dyn<br> [0x80000248]:csrrs tp, fcsr, zero<br> [0x8000024c]:sw s6, 72(ra)<br>     |
|  11|[0x80002560]<br>0x66D80000|- rs1 : x20<br> - rs2 : x22<br> - rd : x21<br> - fs1 == 0 and fe1 == 0xfc and fm1 == 0x2b6a13 and fs2 == 0 and fe2 == 0xad and fm2 == 0x4b2862 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000264]:fdiv.s s5, s4, s6, dyn<br> [0x80000268]:csrrs tp, fcsr, zero<br> [0x8000026c]:sw s5, 80(ra)<br>     |
|  12|[0x80002568]<br>0x78C80000|- rs1 : x21<br> - rs2 : x19<br> - rd : x20<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x7906c5 and fs2 == 0 and fe2 == 0x8b and fm2 == 0x1f607e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000284]:fdiv.s s4, s5, s3, dyn<br> [0x80000288]:csrrs tp, fcsr, zero<br> [0x8000028c]:sw s4, 88(ra)<br>     |
|  13|[0x80002570]<br>0x3E97A40E|- rs1 : x18<br> - rs2 : x20<br> - rd : x19<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x17a40d and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x800002a4]:fdiv.s s3, s2, s4, dyn<br> [0x800002a8]:csrrs tp, fcsr, zero<br> [0x800002ac]:sw s3, 96(ra)<br>     |
|  14|[0x80002578]<br>0xBEDD1FF6|- rs1 : x19<br> - rs2 : x17<br> - rd : x18<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x5d1ff5 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x800002c4]:fdiv.s s2, s3, a7, dyn<br> [0x800002c8]:csrrs tp, fcsr, zero<br> [0x800002cc]:sw s2, 104(ra)<br>    |
|  15|[0x80002580]<br>0xBE76B77F|- rs1 : x16<br> - rs2 : x18<br> - rd : x17<br> - fs1 == 0 and fe1 == 0xfc and fm1 == 0x76b77e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x800002e4]:fdiv.s a7, a6, s2, dyn<br> [0x800002e8]:csrrs tp, fcsr, zero<br> [0x800002ec]:sw a7, 112(ra)<br>    |
|  16|[0x80002588]<br>0xBEF248B3|- rs1 : x17<br> - rs2 : x15<br> - rd : x16<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x7248b2 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000304]:fdiv.s a6, a7, a5, dyn<br> [0x80000308]:csrrs tp, fcsr, zero<br> [0x8000030c]:sw a6, 120(ra)<br>    |
|  17|[0x80002590]<br>0x7C1C0000|- rs1 : x14<br> - rs2 : x16<br> - rd : x15<br> - fs1 == 0 and fe1 == 0xfa and fm1 == 0x34967e and fs2 == 0 and fe2 == 0x81 and fm2 == 0x142cb6 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000324]:fdiv.s a5, a4, a6, dyn<br> [0x80000328]:csrrs tp, fcsr, zero<br> [0x8000032c]:sw a5, 128(ra)<br>    |
|  18|[0x80002598]<br>0xBE655451|- rs1 : x15<br> - rs2 : x13<br> - rd : x14<br> - fs1 == 0 and fe1 == 0xfc and fm1 == 0x655450 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000344]:fdiv.s a4, a5, a3, dyn<br> [0x80000348]:csrrs tp, fcsr, zero<br> [0x8000034c]:sw a4, 136(ra)<br>    |
|  19|[0x800025a0]<br>0x619C0000|- rs1 : x12<br> - rs2 : x14<br> - rd : x13<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x39d661 and fs2 == 0 and fe2 == 0xba and fm2 == 0x187b63 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000364]:fdiv.s a3, a2, a4, dyn<br> [0x80000368]:csrrs tp, fcsr, zero<br> [0x8000036c]:sw a3, 144(ra)<br>    |
|  20|[0x800025a8]<br>0xBF7BFFFF|- rs1 : x13<br> - rs2 : x11<br> - rd : x12<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x281a41 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x2ac557 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000384]:fdiv.s a2, a3, a1, dyn<br> [0x80000388]:csrrs tp, fcsr, zero<br> [0x8000038c]:sw a2, 152(ra)<br>    |
|  21|[0x800025b0]<br>0x3E6B4E0F|- rs1 : x10<br> - rs2 : x12<br> - rd : x11<br> - fs1 == 0 and fe1 == 0xfc and fm1 == 0x6b4e0e and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x800003a4]:fdiv.s a1, a0, a2, dyn<br> [0x800003a8]:csrrs tp, fcsr, zero<br> [0x800003ac]:sw a1, 160(ra)<br>    |
|  22|[0x800025b8]<br>0xBF24D5B3|- rs1 : x11<br> - rs2 : x9<br> - rd : x10<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x24d5b2 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                 |[0x800003c4]:fdiv.s a0, a1, s1, dyn<br> [0x800003c8]:csrrs tp, fcsr, zero<br> [0x800003cc]:sw a0, 168(ra)<br>    |
|  23|[0x800025c0]<br>0x79BA0001|- rs1 : x8<br> - rs2 : x10<br> - rd : x9<br> - fs1 == 0 and fe1 == 0xfc and fm1 == 0x6e5bf8 and fs2 == 0 and fe2 == 0x88 and fm2 == 0x24083c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                  |[0x800003ec]:fdiv.s s1, fp, a0, dyn<br> [0x800003f0]:csrrs a2, fcsr, zero<br> [0x800003f4]:sw s1, 176(ra)<br>    |
|  24|[0x800025c8]<br>0x3F3457E8|- rs1 : x9<br> - rs2 : x7<br> - rd : x8<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x3457e7 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                   |[0x8000040c]:fdiv.s fp, s1, t2, dyn<br> [0x80000410]:csrrs a2, fcsr, zero<br> [0x80000414]:sw fp, 184(ra)<br>    |
|  25|[0x800025d0]<br>0xF56A0000|- rs1 : x6<br> - rs2 : x8<br> - rd : x7<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x0ed153 and fs2 == 1 and fe2 == 0x92 and fm2 == 0x1c3eb7 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                   |[0x8000042c]:fdiv.s t2, t1, fp, dyn<br> [0x80000430]:csrrs a2, fcsr, zero<br> [0x80000434]:sw t2, 192(ra)<br>    |
|  26|[0x800025d8]<br>0x52770000|- rs1 : x7<br> - rs2 : x5<br> - rd : x6<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x1fcf65 and fs2 == 0 and fe2 == 0xd7 and fm2 == 0x25a218 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                   |[0x80000454]:fdiv.s t1, t2, t0, dyn<br> [0x80000458]:csrrs a2, fcsr, zero<br> [0x8000045c]:sw t1, 0(fp)<br>      |
|  27|[0x800025e0]<br>0xD0970000|- rs1 : x4<br> - rs2 : x6<br> - rd : x5<br> - fs1 == 0 and fe1 == 0xfc and fm1 == 0x41cf9e and fs2 == 1 and fe2 == 0xda and fm2 == 0x244a46 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                   |[0x80000474]:fdiv.s t0, tp, t1, dyn<br> [0x80000478]:csrrs a2, fcsr, zero<br> [0x8000047c]:sw t0, 8(fp)<br>      |
|  28|[0x800025e8]<br>0x6E5D0000|- rs1 : x5<br> - rs2 : x3<br> - rd : x4<br> - fs1 == 0 and fe1 == 0xf7 and fm1 == 0x506932 and fs2 == 0 and fe2 == 0x99 and fm2 == 0x716acc and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                   |[0x80000494]:fdiv.s tp, t0, gp, dyn<br> [0x80000498]:csrrs a2, fcsr, zero<br> [0x8000049c]:sw tp, 16(fp)<br>     |
|  29|[0x800025f0]<br>0x5F0F0000|- rs1 : x2<br> - rs2 : x4<br> - rd : x3<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x28b6bd and fs2 == 0 and fe2 == 0xbf and fm2 == 0x17043e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                   |[0x800004b4]:fdiv.s gp, sp, tp, dyn<br> [0x800004b8]:csrrs a2, fcsr, zero<br> [0x800004bc]:sw gp, 24(fp)<br>     |
|  30|[0x800025f8]<br>0xDC730000|- rs1 : x3<br> - rs2 : x1<br> - rd : x2<br> - fs1 == 0 and fe1 == 0xf9 and fm1 == 0x68f58b and fs2 == 1 and fe2 == 0xbf and fm2 == 0x756c07 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                   |[0x800004d4]:fdiv.s sp, gp, ra, dyn<br> [0x800004d8]:csrrs a2, fcsr, zero<br> [0x800004dc]:sw sp, 32(fp)<br>     |
|  31|[0x80002600]<br>0x80000000|- rs1 : x0<br> - rs2 : x2<br> - rd : x1<br>                                                                                                                                                                                                 |[0x800004f4]:fdiv.s ra, zero, sp, dyn<br> [0x800004f8]:csrrs a2, fcsr, zero<br> [0x800004fc]:sw ra, 40(fp)<br>   |
|  32|[0x80002608]<br>0xBF38BE1C|- rs1 : x1<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x38be1b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                |[0x80000514]:fdiv.s t6, ra, t5, dyn<br> [0x80000518]:csrrs a2, fcsr, zero<br> [0x8000051c]:sw t6, 48(fp)<br>     |
|  33|[0x80002610]<br>0x7F800000|- rs2 : x0<br>                                                                                                                                                                                                                              |[0x80000534]:fdiv.s t6, t5, zero, dyn<br> [0x80000538]:csrrs a2, fcsr, zero<br> [0x8000053c]:sw t6, 56(fp)<br>   |
|  34|[0x80002618]<br>0x00000000|- rd : x0<br> - fs1 == 0 and fe1 == 0xfc and fm1 == 0x03b9a1 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                 |[0x80000554]:fdiv.s zero, t6, t5, dyn<br> [0x80000558]:csrrs a2, fcsr, zero<br> [0x8000055c]:sw zero, 64(fp)<br> |
|  35|[0x80002620]<br>0xFCCB8000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x269d2c and fs2 == 1 and fe2 == 0x83 and fm2 == 0x519910 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000574]:fdiv.s t6, t5, t4, dyn<br> [0x80000578]:csrrs a2, fcsr, zero<br> [0x8000057c]:sw t6, 72(fp)<br>     |
|  36|[0x80002628]<br>0xEC3FBFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3af6ff and fs2 == 1 and fe2 == 0xa4 and fm2 == 0x799c89 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000594]:fdiv.s t6, t5, t4, dyn<br> [0x80000598]:csrrs a2, fcsr, zero<br> [0x8000059c]:sw t6, 80(fp)<br>     |
|  37|[0x80002630]<br>0xBF072C25|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x072c24 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800005b4]:fdiv.s t6, t5, t4, dyn<br> [0x800005b8]:csrrs a2, fcsr, zero<br> [0x800005bc]:sw t6, 88(fp)<br>     |
|  38|[0x80002638]<br>0x3EC36853|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x436852 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800005d4]:fdiv.s t6, t5, t4, dyn<br> [0x800005d8]:csrrs a2, fcsr, zero<br> [0x800005dc]:sw t6, 96(fp)<br>     |
|  39|[0x80002640]<br>0xDF1F3FFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x1123d8 and fs2 == 1 and fe2 == 0xbd and fm2 == 0x695156 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800005f4]:fdiv.s t6, t5, t4, dyn<br> [0x800005f8]:csrrs a2, fcsr, zero<br> [0x800005fc]:sw t6, 104(fp)<br>    |
|  40|[0x80002648]<br>0xBE8538B2|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x0538b1 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000614]:fdiv.s t6, t5, t4, dyn<br> [0x80000618]:csrrs a2, fcsr, zero<br> [0x8000061c]:sw t6, 112(fp)<br>    |
|  41|[0x80002650]<br>0xBE1E0668|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x1e0667 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000634]:fdiv.s t6, t5, t4, dyn<br> [0x80000638]:csrrs a2, fcsr, zero<br> [0x8000063c]:sw t6, 120(fp)<br>    |
|  42|[0x80002658]<br>0xD9F0DFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x016ff7 and fs2 == 1 and fe2 == 0xc9 and fm2 == 0x0990a3 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000654]:fdiv.s t6, t5, t4, dyn<br> [0x80000658]:csrrs a2, fcsr, zero<br> [0x8000065c]:sw t6, 128(fp)<br>    |
|  43|[0x80002660]<br>0x5E566000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2640ba and fs2 == 0 and fe2 == 0xbf and fm2 == 0x4688b4 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000674]:fdiv.s t6, t5, t4, dyn<br> [0x80000678]:csrrs a2, fcsr, zero<br> [0x8000067c]:sw t6, 136(fp)<br>    |
|  44|[0x80002668]<br>0xBE151547|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x151546 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000694]:fdiv.s t6, t5, t4, dyn<br> [0x80000698]:csrrs a2, fcsr, zero<br> [0x8000069c]:sw t6, 144(fp)<br>    |
|  45|[0x80002670]<br>0xBDA06547|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x206546 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800006b4]:fdiv.s t6, t5, t4, dyn<br> [0x800006b8]:csrrs a2, fcsr, zero<br> [0x800006bc]:sw t6, 152(fp)<br>    |
|  46|[0x80002678]<br>0xC294F000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7fba49 and fs2 == 1 and fe2 == 0xf7 and fm2 == 0x5bc718 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800006d4]:fdiv.s t6, t5, t4, dyn<br> [0x800006d8]:csrrs a2, fcsr, zero<br> [0x800006dc]:sw t6, 160(fp)<br>    |
|  47|[0x80002680]<br>0x3E636241|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x636240 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800006f4]:fdiv.s t6, t5, t4, dyn<br> [0x800006f8]:csrrs a2, fcsr, zero<br> [0x800006fc]:sw t6, 168(fp)<br>    |
|  48|[0x80002688]<br>0x3EBFEC55|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3fec54 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000714]:fdiv.s t6, t5, t4, dyn<br> [0x80000718]:csrrs a2, fcsr, zero<br> [0x8000071c]:sw t6, 176(fp)<br>    |
|  49|[0x80002690]<br>0xBE79DD8F|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x79dd8e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000734]:fdiv.s t6, t5, t4, dyn<br> [0x80000738]:csrrs a2, fcsr, zero<br> [0x8000073c]:sw t6, 184(fp)<br>    |
|  50|[0x80002698]<br>0xFDEC9001|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2759f0 and fs2 == 1 and fe2 == 0x81 and fm2 == 0x351a1b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000754]:fdiv.s t6, t5, t4, dyn<br> [0x80000758]:csrrs a2, fcsr, zero<br> [0x8000075c]:sw t6, 192(fp)<br>    |
|  51|[0x800026a0]<br>0x4A4BE801|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2cbcfc and fs2 == 0 and fe2 == 0xe8 and fm2 == 0x58de83 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000774]:fdiv.s t6, t5, t4, dyn<br> [0x80000778]:csrrs a2, fcsr, zero<br> [0x8000077c]:sw t6, 200(fp)<br>    |
|  52|[0x800026a8]<br>0x3C5E539B|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x5e539a and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000794]:fdiv.s t6, t5, t4, dyn<br> [0x80000798]:csrrs a2, fcsr, zero<br> [0x8000079c]:sw t6, 208(fp)<br>    |
|  53|[0x800026b0]<br>0xBE1A414F|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x1a414e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800007b4]:fdiv.s t6, t5, t4, dyn<br> [0x800007b8]:csrrs a2, fcsr, zero<br> [0x800007bc]:sw t6, 216(fp)<br>    |
|  54|[0x800026b8]<br>0x62819800|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x7bb471 and fs2 == 0 and fe2 == 0xb6 and fm2 == 0x789bff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800007d4]:fdiv.s t6, t5, t4, dyn<br> [0x800007d8]:csrrs a2, fcsr, zero<br> [0x800007dc]:sw t6, 224(fp)<br>    |
|  55|[0x800026c0]<br>0xF5C51800|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3d8377 and fs2 == 1 and fe2 == 0x91 and fm2 == 0x76277d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800007f4]:fdiv.s t6, t5, t4, dyn<br> [0x800007f8]:csrrs a2, fcsr, zero<br> [0x800007fc]:sw t6, 232(fp)<br>    |
|  56|[0x800026c8]<br>0x3F16474A|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x164749 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000814]:fdiv.s t6, t5, t4, dyn<br> [0x80000818]:csrrs a2, fcsr, zero<br> [0x8000081c]:sw t6, 240(fp)<br>    |
|  57|[0x800026d0]<br>0xBE823676|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x023675 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000834]:fdiv.s t6, t5, t4, dyn<br> [0x80000838]:csrrs a2, fcsr, zero<br> [0x8000083c]:sw t6, 248(fp)<br>    |
|  58|[0x800026d8]<br>0x49EC7C00|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x239b5c and fs2 == 0 and fe2 == 0xe7 and fm2 == 0x311bbd and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000854]:fdiv.s t6, t5, t4, dyn<br> [0x80000858]:csrrs a2, fcsr, zero<br> [0x8000085c]:sw t6, 256(fp)<br>    |
|  59|[0x800026e0]<br>0x743AC400|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x017ed0 and fs2 == 0 and fe2 == 0x94 and fm2 == 0x317fe2 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000874]:fdiv.s t6, t5, t4, dyn<br> [0x80000878]:csrrs a2, fcsr, zero<br> [0x8000087c]:sw t6, 264(fp)<br>    |
|  60|[0x800026e8]<br>0xC6546400|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x20d6ae and fs2 == 1 and fe2 == 0xf0 and fm2 == 0x41dcec and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000894]:fdiv.s t6, t5, t4, dyn<br> [0x80000898]:csrrs a2, fcsr, zero<br> [0x8000089c]:sw t6, 272(fp)<br>    |
|  61|[0x800026f0]<br>0x5D7A8200|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x32fae0 and fs2 == 0 and fe2 == 0xc2 and fm2 == 0x36e76b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800008b4]:fdiv.s t6, t5, t4, dyn<br> [0x800008b8]:csrrs a2, fcsr, zero<br> [0x800008bc]:sw t6, 280(fp)<br>    |
|  62|[0x800026f8]<br>0xE4311E00|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x0409cf and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x3ed825 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800008d4]:fdiv.s t6, t5, t4, dyn<br> [0x800008d8]:csrrs a2, fcsr, zero<br> [0x800008dc]:sw t6, 288(fp)<br>    |
|  63|[0x80002700]<br>0xFA690600|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x296b63 and fs2 == 1 and fe2 == 0x88 and fm2 == 0x3a1fe4 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800008f4]:fdiv.s t6, t5, t4, dyn<br> [0x800008f8]:csrrs a2, fcsr, zero<br> [0x800008fc]:sw t6, 296(fp)<br>    |
|  64|[0x80002708]<br>0xDEF81E00|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x03ecd6 and fs2 == 1 and fe2 == 0xbf and fm2 == 0x081dd1 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000914]:fdiv.s t6, t5, t4, dyn<br> [0x80000918]:csrrs a2, fcsr, zero<br> [0x8000091c]:sw t6, 304(fp)<br>    |
|  65|[0x80002710]<br>0xBEBA0C2A|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a0c29 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000934]:fdiv.s t6, t5, t4, dyn<br> [0x80000938]:csrrs a2, fcsr, zero<br> [0x8000093c]:sw t6, 312(fp)<br>    |
|  66|[0x80002718]<br>0x47E380FF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3d37b2 and fs2 == 0 and fe2 == 0xed and fm2 == 0x54eb08 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000954]:fdiv.s t6, t5, t4, dyn<br> [0x80000958]:csrrs a2, fcsr, zero<br> [0x8000095c]:sw t6, 320(fp)<br>    |
|  67|[0x80002720]<br>0x79B68B00|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x34342f and fs2 == 0 and fe2 == 0x87 and fm2 == 0x7cb837 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000974]:fdiv.s t6, t5, t4, dyn<br> [0x80000978]:csrrs a2, fcsr, zero<br> [0x8000097c]:sw t6, 328(fp)<br>    |
|  68|[0x80002728]<br>0x6F3D9500|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0b4a10 and fs2 == 0 and fe2 == 0x9e and fm2 == 0x3c1678 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000994]:fdiv.s t6, t5, t4, dyn<br> [0x80000998]:csrrs a2, fcsr, zero<br> [0x8000099c]:sw t6, 336(fp)<br>    |
|  69|[0x80002730]<br>0x46C8F4FF|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x02119e and fs2 == 0 and fe2 == 0xec and fm2 == 0x25b1fa and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800009b4]:fdiv.s t6, t5, t4, dyn<br> [0x800009b8]:csrrs a2, fcsr, zero<br> [0x800009bc]:sw t6, 344(fp)<br>    |
|  70|[0x80002738]<br>0x40F872FF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x025339 and fs2 == 0 and fe2 == 0xf9 and fm2 == 0x064930 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800009d4]:fdiv.s t6, t5, t4, dyn<br> [0x800009d8]:csrrs a2, fcsr, zero<br> [0x800009dc]:sw t6, 352(fp)<br>    |
|  71|[0x80002740]<br>0x683E1280|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2ee8de and fs2 == 0 and fe2 == 0xac and fm2 == 0x6b9400 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800009f4]:fdiv.s t6, t5, t4, dyn<br> [0x800009f8]:csrrs a2, fcsr, zero<br> [0x800009fc]:sw t6, 360(fp)<br>    |
|  72|[0x80002748]<br>0x3E238F40|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x238f3f and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000a14]:fdiv.s t6, t5, t4, dyn<br> [0x80000a18]:csrrs a2, fcsr, zero<br> [0x80000a1c]:sw t6, 368(fp)<br>    |
|  73|[0x80002750]<br>0xC544EC80|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x11c013 and fs2 == 1 and fe2 == 0xf2 and fm2 == 0x3d7983 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000a34]:fdiv.s t6, t5, t4, dyn<br> [0x80000a38]:csrrs a2, fcsr, zero<br> [0x80000a3c]:sw t6, 376(fp)<br>    |
|  74|[0x80002758]<br>0x4E64AA81|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x667aed and fs2 == 0 and fe2 == 0xe0 and fm2 == 0x0103f8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000a54]:fdiv.s t6, t5, t4, dyn<br> [0x80000a58]:csrrs a2, fcsr, zero<br> [0x80000a5c]:sw t6, 384(fp)<br>    |
|  75|[0x80002760]<br>0x4CBAA47F|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x217f53 and fs2 == 0 and fe2 == 0xe3 and fm2 == 0x5d82bd and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000a74]:fdiv.s t6, t5, t4, dyn<br> [0x80000a78]:csrrs a2, fcsr, zero<br> [0x80000a7c]:sw t6, 392(fp)<br>    |
|  76|[0x80002768]<br>0xBE8D5A7D|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x0d5a7c and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000a94]:fdiv.s t6, t5, t4, dyn<br> [0x80000a98]:csrrs a2, fcsr, zero<br> [0x80000a9c]:sw t6, 400(fp)<br>    |
|  77|[0x80002770]<br>0xC616AEC0|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x02ab65 and fs2 == 1 and fe2 == 0xef and fm2 == 0x5dffc1 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000ab4]:fdiv.s t6, t5, t4, dyn<br> [0x80000ab8]:csrrs a2, fcsr, zero<br> [0x80000abc]:sw t6, 408(fp)<br>    |
|  78|[0x80002778]<br>0x6E1723C0|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x5fa740 and fs2 == 0 and fe2 == 0x9f and fm2 == 0x3d6968 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000ad4]:fdiv.s t6, t5, t4, dyn<br> [0x80000ad8]:csrrs a2, fcsr, zero<br> [0x80000adc]:sw t6, 416(fp)<br>    |
|  79|[0x80002780]<br>0xC6E63340|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x268b6a and fs2 == 1 and fe2 == 0xef and fm2 == 0x3935cd and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000af4]:fdiv.s t6, t5, t4, dyn<br> [0x80000af8]:csrrs a2, fcsr, zero<br> [0x80000afc]:sw t6, 424(fp)<br>    |
|  80|[0x80002788]<br>0xE8ECF6C0|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x67ede5 and fs2 == 1 and fe2 == 0xaa and fm2 == 0x7a8f9b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000b14]:fdiv.s t6, t5, t4, dyn<br> [0x80000b18]:csrrs a2, fcsr, zero<br> [0x80000b1c]:sw t6, 432(fp)<br>    |
|  81|[0x80002790]<br>0xBF373A1F|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x373a1e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000b34]:fdiv.s t6, t5, t4, dyn<br> [0x80000b38]:csrrs a2, fcsr, zero<br> [0x80000b3c]:sw t6, 440(fp)<br>    |
|  82|[0x80002798]<br>0x5678C6E1|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2a5ada and fs2 == 0 and fe2 == 0xd0 and fm2 == 0x2f4d13 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000b54]:fdiv.s t6, t5, t4, dyn<br> [0x80000b58]:csrrs a2, fcsr, zero<br> [0x80000b5c]:sw t6, 448(fp)<br>    |
|  83|[0x800027a0]<br>0x6F0AA760|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4fe433 and fs2 == 0 and fe2 == 0x9c and fm2 == 0x3feae5 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000b74]:fdiv.s t6, t5, t4, dyn<br> [0x80000b78]:csrrs a2, fcsr, zero<br> [0x80000b7c]:sw t6, 456(fp)<br>    |
|  84|[0x800027a8]<br>0xBED3A643|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x53a642 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000b94]:fdiv.s t6, t5, t4, dyn<br> [0x80000b98]:csrrs a2, fcsr, zero<br> [0x80000b9c]:sw t6, 464(fp)<br>    |
|  85|[0x800027b0]<br>0x3DA02A99|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x202a98 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000bb4]:fdiv.s t6, t5, t4, dyn<br> [0x80000bb8]:csrrs a2, fcsr, zero<br> [0x80000bbc]:sw t6, 472(fp)<br>    |
|  86|[0x800027b8]<br>0xBCF0766F|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x70766e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000bd4]:fdiv.s t6, t5, t4, dyn<br> [0x80000bd8]:csrrs a2, fcsr, zero<br> [0x80000bdc]:sw t6, 480(fp)<br>    |
|  87|[0x800027c0]<br>0x3D60D9A5|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x60d9a4 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000bf4]:fdiv.s t6, t5, t4, dyn<br> [0x80000bf8]:csrrs a2, fcsr, zero<br> [0x80000bfc]:sw t6, 488(fp)<br>    |
|  88|[0x800027c8]<br>0x3F264DE8|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x264de7 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000c14]:fdiv.s t6, t5, t4, dyn<br> [0x80000c18]:csrrs a2, fcsr, zero<br> [0x80000c1c]:sw t6, 496(fp)<br>    |
|  89|[0x800027d0]<br>0xBF175180|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x17517f and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000c34]:fdiv.s t6, t5, t4, dyn<br> [0x80000c38]:csrrs a2, fcsr, zero<br> [0x80000c3c]:sw t6, 504(fp)<br>    |
|  90|[0x800027d8]<br>0xBF1F12BA|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1f12b9 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000c54]:fdiv.s t6, t5, t4, dyn<br> [0x80000c58]:csrrs a2, fcsr, zero<br> [0x80000c5c]:sw t6, 512(fp)<br>    |
|  91|[0x800027e0]<br>0xD2A2EED8|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x02e795 and fs2 == 1 and fe2 == 0xd6 and fm2 == 0x4dad56 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000c74]:fdiv.s t6, t5, t4, dyn<br> [0x80000c78]:csrrs a2, fcsr, zero<br> [0x80000c7c]:sw t6, 520(fp)<br>    |
|  92|[0x800027e8]<br>0xBEEC3B3F|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6c3b3e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000c94]:fdiv.s t6, t5, t4, dyn<br> [0x80000c98]:csrrs a2, fcsr, zero<br> [0x80000c9c]:sw t6, 528(fp)<br>    |
|  93|[0x800027f0]<br>0x3FA93757|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x16201f and fs2 == 0 and fe2 == 0xfd and fm2 == 0x631e55 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000cb4]:fdiv.s t6, t5, t4, dyn<br> [0x80000cb8]:csrrs a2, fcsr, zero<br> [0x80000cbc]:sw t6, 536(fp)<br>    |
|  94|[0x800027f8]<br>0x3E912ACF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x112ace and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000cd4]:fdiv.s t6, t5, t4, dyn<br> [0x80000cd8]:csrrs a2, fcsr, zero<br> [0x80000cdc]:sw t6, 544(fp)<br>    |
|  95|[0x80002800]<br>0x3EDB2E1B|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x5b2e1a and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000cf4]:fdiv.s t6, t5, t4, dyn<br> [0x80000cf8]:csrrs a2, fcsr, zero<br> [0x80000cfc]:sw t6, 552(fp)<br>    |
|  96|[0x80002808]<br>0x3E2BCFFA|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x2bcff9 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000d14]:fdiv.s t6, t5, t4, dyn<br> [0x80000d18]:csrrs a2, fcsr, zero<br> [0x80000d1c]:sw t6, 560(fp)<br>    |
|  97|[0x80002810]<br>0xBD7B1D84|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x7b1d83 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000d34]:fdiv.s t6, t5, t4, dyn<br> [0x80000d38]:csrrs a2, fcsr, zero<br> [0x80000d3c]:sw t6, 568(fp)<br>    |
|  98|[0x80002818]<br>0xC15D75A3|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x4cd7ff and fs2 == 1 and fe2 == 0xf8 and fm2 == 0x6ccaee and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000d54]:fdiv.s t6, t5, t4, dyn<br> [0x80000d58]:csrrs a2, fcsr, zero<br> [0x80000d5c]:sw t6, 576(fp)<br>    |
|  99|[0x80002820]<br>0xFB9AC37C|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2b0708 and fs2 == 1 and fe2 == 0x86 and fm2 == 0x0d7389 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000d74]:fdiv.s t6, t5, t4, dyn<br> [0x80000d78]:csrrs a2, fcsr, zero<br> [0x80000d7c]:sw t6, 584(fp)<br>    |
| 100|[0x80002828]<br>0xE8B7407C|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x58bf61 and fs2 == 1 and fe2 == 0xab and fm2 == 0x17657f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000d94]:fdiv.s t6, t5, t4, dyn<br> [0x80000d98]:csrrs a2, fcsr, zero<br> [0x80000d9c]:sw t6, 592(fp)<br>    |
| 101|[0x80002830]<br>0x3F319CE7|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x319ce6 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000db4]:fdiv.s t6, t5, t4, dyn<br> [0x80000db8]:csrrs a2, fcsr, zero<br> [0x80000dbc]:sw t6, 600(fp)<br>    |
| 102|[0x80002838]<br>0x3EAFC88D|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2fc88c and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000dd4]:fdiv.s t6, t5, t4, dyn<br> [0x80000dd8]:csrrs a2, fcsr, zero<br> [0x80000ddc]:sw t6, 608(fp)<br>    |
| 103|[0x80002850]<br>0x3F0748C7|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0748c6 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000e34]:fdiv.s t6, t5, t4, dyn<br> [0x80000e38]:csrrs a2, fcsr, zero<br> [0x80000e3c]:sw t6, 632(fp)<br>    |
| 104|[0x80002858]<br>0xBF0235B3|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0235b2 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000e54]:fdiv.s t6, t5, t4, dyn<br> [0x80000e58]:csrrs a2, fcsr, zero<br> [0x80000e5c]:sw t6, 640(fp)<br>    |
| 105|[0x80002860]<br>0xC05B8000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d53d7 and fs2 == 1 and fe2 == 0xfc and fm2 == 0x377d30 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000e74]:fdiv.s t6, t5, t4, dyn<br> [0x80000e78]:csrrs a2, fcsr, zero<br> [0x80000e7c]:sw t6, 648(fp)<br>    |
| 106|[0x80002868]<br>0xF4EA7FFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x5afcdb and fs2 == 1 and fe2 == 0x92 and fm2 == 0x6f10c4 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000e94]:fdiv.s t6, t5, t4, dyn<br> [0x80000e98]:csrrs a2, fcsr, zero<br> [0x80000e9c]:sw t6, 656(fp)<br>    |
