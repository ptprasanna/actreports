
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80001390')]      |
| SIG_REGION                | [('0x80003610', '0x80003ab0', '296 words')]      |
| COV_LABELS                | fdiv_b6      |
| TEST_NAME                 | /home/reg/work/zfinx/fdiv.s/work/fdiv_b6-01.S/ref.S    |
| Total Number of coverpoints| 242     |
| Total Coverpoints Hit     | 242      |
| Total Signature Updates   | 294      |
| STAT1                     | 144      |
| STAT2                     | 3      |
| STAT3                     | 0     |
| STAT4                     | 147     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x800012b4]:fdiv.s t6, t5, t4, dyn
      [0x800012b8]:csrrs a2, fcsr, zero
      [0x800012bc]:sw t6, 920(fp)
 -- Signature Address: 0x80003a70 Data: 0x80000000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fdiv.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0x1a and fm1 == 0x20c723 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x2c5b71 and  fcsr == 0x0 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x800012d4]:fdiv.s t6, t5, t4, dyn
      [0x800012d8]:csrrs a2, fcsr, zero
      [0x800012dc]:sw t6, 928(fp)
 -- Signature Address: 0x80003a78 Data: 0x80000000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fdiv.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0x1a and fm1 == 0x20c723 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x2c5b71 and  fcsr == 0x20 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80001374]:fdiv.s t6, t5, t4, dyn
      [0x80001378]:csrrs a2, fcsr, zero
      [0x8000137c]:sw t6, 968(fp)
 -- Signature Address: 0x80003aa0 Data: 0x80000000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fdiv.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0x19 and fm1 == 0x1479a4 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x1f2b1f and  fcsr == 0x60 and rm_val == 7   #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fdiv.s', 'rs1 : x30', 'rs2 : x31', 'rd : x31', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x20c723 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x2c5b71 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000124]:fdiv.s t6, t5, t6, dyn
	-[0x80000128]:csrrs tp, fcsr, zero
	-[0x8000012c]:sw t6, 0(ra)
Current Store : [0x80000130] : sw tp, 4(ra) -- Store: [0x80003614]:0x00000003




Last Coverpoint : ['rs1 : x29', 'rs2 : x30', 'rd : x29', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x20c723 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x2c5b71 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000144]:fdiv.s t4, t4, t5, dyn
	-[0x80000148]:csrrs tp, fcsr, zero
	-[0x8000014c]:sw t4, 8(ra)
Current Store : [0x80000150] : sw tp, 12(ra) -- Store: [0x8000361c]:0x00000023




Last Coverpoint : ['rs1 : x31', 'rs2 : x29', 'rd : x30', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x20c723 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x2c5b71 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000164]:fdiv.s t5, t6, t4, dyn
	-[0x80000168]:csrrs tp, fcsr, zero
	-[0x8000016c]:sw t5, 16(ra)
Current Store : [0x80000170] : sw tp, 20(ra) -- Store: [0x80003624]:0x00000043




Last Coverpoint : ['rs1 : x27', 'rs2 : x27', 'rd : x28', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x80000184]:fdiv.s t3, s11, s11, dyn
	-[0x80000188]:csrrs tp, fcsr, zero
	-[0x8000018c]:sw t3, 24(ra)
Current Store : [0x80000190] : sw tp, 28(ra) -- Store: [0x8000362c]:0x00000060




Last Coverpoint : ['rs1 : x26', 'rs2 : x26', 'rd : x26', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x800001a4]:fdiv.s s10, s10, s10, dyn
	-[0x800001a8]:csrrs tp, fcsr, zero
	-[0x800001ac]:sw s10, 32(ra)
Current Store : [0x800001b0] : sw tp, 36(ra) -- Store: [0x80003634]:0x00000080




Last Coverpoint : ['rs1 : x28', 'rs2 : x25', 'rd : x27', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x537439 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x62aedc and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001c4]:fdiv.s s11, t3, s9, dyn
	-[0x800001c8]:csrrs tp, fcsr, zero
	-[0x800001cc]:sw s11, 40(ra)
Current Store : [0x800001d0] : sw tp, 44(ra) -- Store: [0x8000363c]:0x00000003




Last Coverpoint : ['rs1 : x24', 'rs2 : x28', 'rd : x25', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x537439 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x62aedc and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001e4]:fdiv.s s9, s8, t3, dyn
	-[0x800001e8]:csrrs tp, fcsr, zero
	-[0x800001ec]:sw s9, 48(ra)
Current Store : [0x800001f0] : sw tp, 52(ra) -- Store: [0x80003644]:0x00000023




Last Coverpoint : ['rs1 : x25', 'rs2 : x23', 'rd : x24', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x537439 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x62aedc and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000204]:fdiv.s s8, s9, s7, dyn
	-[0x80000208]:csrrs tp, fcsr, zero
	-[0x8000020c]:sw s8, 56(ra)
Current Store : [0x80000210] : sw tp, 60(ra) -- Store: [0x8000364c]:0x00000043




Last Coverpoint : ['rs1 : x22', 'rs2 : x24', 'rd : x23', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x537439 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x62aedc and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000224]:fdiv.s s7, s6, s8, dyn
	-[0x80000228]:csrrs tp, fcsr, zero
	-[0x8000022c]:sw s7, 64(ra)
Current Store : [0x80000230] : sw tp, 68(ra) -- Store: [0x80003654]:0x00000063




Last Coverpoint : ['rs1 : x23', 'rs2 : x21', 'rd : x22', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x537439 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x62aedc and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000244]:fdiv.s s6, s7, s5, dyn
	-[0x80000248]:csrrs tp, fcsr, zero
	-[0x8000024c]:sw s6, 72(ra)
Current Store : [0x80000250] : sw tp, 76(ra) -- Store: [0x8000365c]:0x00000083




Last Coverpoint : ['rs1 : x20', 'rs2 : x22', 'rd : x21', 'fs1 == 0 and fe1 == 0x17 and fm1 == 0x73ac65 and fs2 == 1 and fe2 == 0xaf and fm2 == 0x029c88 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000264]:fdiv.s s5, s4, s6, dyn
	-[0x80000268]:csrrs tp, fcsr, zero
	-[0x8000026c]:sw s5, 80(ra)
Current Store : [0x80000270] : sw tp, 84(ra) -- Store: [0x80003664]:0x00000003




Last Coverpoint : ['rs1 : x21', 'rs2 : x19', 'rd : x20', 'fs1 == 0 and fe1 == 0x17 and fm1 == 0x73ac65 and fs2 == 1 and fe2 == 0xaf and fm2 == 0x029c88 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000284]:fdiv.s s4, s5, s3, dyn
	-[0x80000288]:csrrs tp, fcsr, zero
	-[0x8000028c]:sw s4, 88(ra)
Current Store : [0x80000290] : sw tp, 92(ra) -- Store: [0x8000366c]:0x00000023




Last Coverpoint : ['rs1 : x18', 'rs2 : x20', 'rd : x19', 'fs1 == 0 and fe1 == 0x17 and fm1 == 0x73ac65 and fs2 == 1 and fe2 == 0xaf and fm2 == 0x029c88 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002a4]:fdiv.s s3, s2, s4, dyn
	-[0x800002a8]:csrrs tp, fcsr, zero
	-[0x800002ac]:sw s3, 96(ra)
Current Store : [0x800002b0] : sw tp, 100(ra) -- Store: [0x80003674]:0x00000043




Last Coverpoint : ['rs1 : x19', 'rs2 : x17', 'rd : x18', 'fs1 == 0 and fe1 == 0x17 and fm1 == 0x73ac65 and fs2 == 1 and fe2 == 0xaf and fm2 == 0x029c88 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002c4]:fdiv.s s2, s3, a7, dyn
	-[0x800002c8]:csrrs tp, fcsr, zero
	-[0x800002cc]:sw s2, 104(ra)
Current Store : [0x800002d0] : sw tp, 108(ra) -- Store: [0x8000367c]:0x00000063




Last Coverpoint : ['rs1 : x16', 'rs2 : x18', 'rd : x17', 'fs1 == 0 and fe1 == 0x17 and fm1 == 0x73ac65 and fs2 == 1 and fe2 == 0xaf and fm2 == 0x029c88 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002e4]:fdiv.s a7, a6, s2, dyn
	-[0x800002e8]:csrrs tp, fcsr, zero
	-[0x800002ec]:sw a7, 112(ra)
Current Store : [0x800002f0] : sw tp, 116(ra) -- Store: [0x80003684]:0x00000083




Last Coverpoint : ['rs1 : x17', 'rs2 : x15', 'rd : x16', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x079c52 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x11609d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000304]:fdiv.s a6, a7, a5, dyn
	-[0x80000308]:csrrs tp, fcsr, zero
	-[0x8000030c]:sw a6, 120(ra)
Current Store : [0x80000310] : sw tp, 124(ra) -- Store: [0x8000368c]:0x00000003




Last Coverpoint : ['rs1 : x14', 'rs2 : x16', 'rd : x15', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x079c52 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x11609d and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000324]:fdiv.s a5, a4, a6, dyn
	-[0x80000328]:csrrs tp, fcsr, zero
	-[0x8000032c]:sw a5, 128(ra)
Current Store : [0x80000330] : sw tp, 132(ra) -- Store: [0x80003694]:0x00000023




Last Coverpoint : ['rs1 : x15', 'rs2 : x13', 'rd : x14', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x079c52 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x11609d and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000344]:fdiv.s a4, a5, a3, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:sw a4, 136(ra)
Current Store : [0x80000350] : sw tp, 140(ra) -- Store: [0x8000369c]:0x00000043




Last Coverpoint : ['rs1 : x12', 'rs2 : x14', 'rd : x13', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x079c52 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x11609d and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000364]:fdiv.s a3, a2, a4, dyn
	-[0x80000368]:csrrs tp, fcsr, zero
	-[0x8000036c]:sw a3, 144(ra)
Current Store : [0x80000370] : sw tp, 148(ra) -- Store: [0x800036a4]:0x00000063




Last Coverpoint : ['rs1 : x13', 'rs2 : x11', 'rd : x12', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x079c52 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x11609d and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000384]:fdiv.s a2, a3, a1, dyn
	-[0x80000388]:csrrs tp, fcsr, zero
	-[0x8000038c]:sw a2, 152(ra)
Current Store : [0x80000390] : sw tp, 156(ra) -- Store: [0x800036ac]:0x00000083




Last Coverpoint : ['rs1 : x10', 'rs2 : x12', 'rd : x11', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x775fc0 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x049853 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003a4]:fdiv.s a1, a0, a2, dyn
	-[0x800003a8]:csrrs tp, fcsr, zero
	-[0x800003ac]:sw a1, 160(ra)
Current Store : [0x800003b0] : sw tp, 164(ra) -- Store: [0x800036b4]:0x00000003




Last Coverpoint : ['rs1 : x11', 'rs2 : x9', 'rd : x10', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x775fc0 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x049853 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003c4]:fdiv.s a0, a1, s1, dyn
	-[0x800003c8]:csrrs tp, fcsr, zero
	-[0x800003cc]:sw a0, 168(ra)
Current Store : [0x800003d0] : sw tp, 172(ra) -- Store: [0x800036bc]:0x00000023




Last Coverpoint : ['rs1 : x8', 'rs2 : x10', 'rd : x9', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x775fc0 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x049853 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003ec]:fdiv.s s1, fp, a0, dyn
	-[0x800003f0]:csrrs a2, fcsr, zero
	-[0x800003f4]:sw s1, 176(ra)
Current Store : [0x800003f8] : sw a2, 180(ra) -- Store: [0x800036c4]:0x00000043




Last Coverpoint : ['rs1 : x9', 'rs2 : x7', 'rd : x8', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x775fc0 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x049853 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000040c]:fdiv.s fp, s1, t2, dyn
	-[0x80000410]:csrrs a2, fcsr, zero
	-[0x80000414]:sw fp, 184(ra)
Current Store : [0x80000418] : sw a2, 188(ra) -- Store: [0x800036cc]:0x00000063




Last Coverpoint : ['rs1 : x6', 'rs2 : x8', 'rd : x7', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x775fc0 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x049853 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000042c]:fdiv.s t2, t1, fp, dyn
	-[0x80000430]:csrrs a2, fcsr, zero
	-[0x80000434]:sw t2, 192(ra)
Current Store : [0x80000438] : sw a2, 196(ra) -- Store: [0x800036d4]:0x00000083




Last Coverpoint : ['rs1 : x7', 'rs2 : x5', 'rd : x6', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x108952 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x1af22d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000454]:fdiv.s t1, t2, t0, dyn
	-[0x80000458]:csrrs a2, fcsr, zero
	-[0x8000045c]:sw t1, 0(fp)
Current Store : [0x80000460] : sw a2, 4(fp) -- Store: [0x800036dc]:0x00000003




Last Coverpoint : ['rs1 : x4', 'rs2 : x6', 'rd : x5', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x108952 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x1af22d and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000474]:fdiv.s t0, tp, t1, dyn
	-[0x80000478]:csrrs a2, fcsr, zero
	-[0x8000047c]:sw t0, 8(fp)
Current Store : [0x80000480] : sw a2, 12(fp) -- Store: [0x800036e4]:0x00000023




Last Coverpoint : ['rs1 : x5', 'rs2 : x3', 'rd : x4', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x108952 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x1af22d and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000494]:fdiv.s tp, t0, gp, dyn
	-[0x80000498]:csrrs a2, fcsr, zero
	-[0x8000049c]:sw tp, 16(fp)
Current Store : [0x800004a0] : sw a2, 20(fp) -- Store: [0x800036ec]:0x00000043




Last Coverpoint : ['rs1 : x2', 'rs2 : x4', 'rd : x3', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x108952 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x1af22d and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004b4]:fdiv.s gp, sp, tp, dyn
	-[0x800004b8]:csrrs a2, fcsr, zero
	-[0x800004bc]:sw gp, 24(fp)
Current Store : [0x800004c0] : sw a2, 28(fp) -- Store: [0x800036f4]:0x00000063




Last Coverpoint : ['rs1 : x3', 'rs2 : x1', 'rd : x2', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x108952 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x1af22d and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004d4]:fdiv.s sp, gp, ra, dyn
	-[0x800004d8]:csrrs a2, fcsr, zero
	-[0x800004dc]:sw sp, 32(fp)
Current Store : [0x800004e0] : sw a2, 36(fp) -- Store: [0x800036fc]:0x00000083




Last Coverpoint : ['rs1 : x0', 'rs2 : x2', 'rd : x1']
Last Code Sequence : 
	-[0x800004f4]:fdiv.s ra, zero, sp, dyn
	-[0x800004f8]:csrrs a2, fcsr, zero
	-[0x800004fc]:sw ra, 40(fp)
Current Store : [0x80000500] : sw a2, 44(fp) -- Store: [0x80003704]:0x00000000




Last Coverpoint : ['rs1 : x1', 'fs1 == 0 and fe1 == 0x19 and fm1 == 0x1479a4 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x1f2b1f and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000514]:fdiv.s t6, ra, t5, dyn
	-[0x80000518]:csrrs a2, fcsr, zero
	-[0x8000051c]:sw t6, 48(fp)
Current Store : [0x80000520] : sw a2, 52(fp) -- Store: [0x8000370c]:0x00000023




Last Coverpoint : ['rs2 : x0']
Last Code Sequence : 
	-[0x80000534]:fdiv.s t6, t5, zero, dyn
	-[0x80000538]:csrrs a2, fcsr, zero
	-[0x8000053c]:sw t6, 56(fp)
Current Store : [0x80000540] : sw a2, 60(fp) -- Store: [0x80003714]:0x00000048




Last Coverpoint : ['rd : x0', 'fs1 == 0 and fe1 == 0x19 and fm1 == 0x1479a4 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x1f2b1f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000554]:fdiv.s zero, t6, t5, dyn
	-[0x80000558]:csrrs a2, fcsr, zero
	-[0x8000055c]:sw zero, 64(fp)
Current Store : [0x80000560] : sw a2, 68(fp) -- Store: [0x8000371c]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x1479a4 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x1f2b1f and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000574]:fdiv.s t6, t5, t4, dyn
	-[0x80000578]:csrrs a2, fcsr, zero
	-[0x8000057c]:sw t6, 72(fp)
Current Store : [0x80000580] : sw a2, 76(fp) -- Store: [0x80003724]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x1243d4 and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x6facab and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000594]:fdiv.s t6, t5, t4, dyn
	-[0x80000598]:csrrs a2, fcsr, zero
	-[0x8000059c]:sw t6, 80(fp)
Current Store : [0x800005a0] : sw a2, 84(fp) -- Store: [0x8000372c]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x1243d4 and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x6facab and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005b4]:fdiv.s t6, t5, t4, dyn
	-[0x800005b8]:csrrs a2, fcsr, zero
	-[0x800005bc]:sw t6, 88(fp)
Current Store : [0x800005c0] : sw a2, 92(fp) -- Store: [0x80003734]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x1243d4 and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x6facab and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005d4]:fdiv.s t6, t5, t4, dyn
	-[0x800005d8]:csrrs a2, fcsr, zero
	-[0x800005dc]:sw t6, 96(fp)
Current Store : [0x800005e0] : sw a2, 100(fp) -- Store: [0x8000373c]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x1243d4 and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x6facab and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005f4]:fdiv.s t6, t5, t4, dyn
	-[0x800005f8]:csrrs a2, fcsr, zero
	-[0x800005fc]:sw t6, 104(fp)
Current Store : [0x80000600] : sw a2, 108(fp) -- Store: [0x80003744]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x1243d4 and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x6facab and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000614]:fdiv.s t6, t5, t4, dyn
	-[0x80000618]:csrrs a2, fcsr, zero
	-[0x8000061c]:sw t6, 112(fp)
Current Store : [0x80000620] : sw a2, 116(fp) -- Store: [0x8000374c]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x041f59 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x588008 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000634]:fdiv.s t6, t5, t4, dyn
	-[0x80000638]:csrrs a2, fcsr, zero
	-[0x8000063c]:sw t6, 120(fp)
Current Store : [0x80000640] : sw a2, 124(fp) -- Store: [0x80003754]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x041f59 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x588008 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000654]:fdiv.s t6, t5, t4, dyn
	-[0x80000658]:csrrs a2, fcsr, zero
	-[0x8000065c]:sw t6, 128(fp)
Current Store : [0x80000660] : sw a2, 132(fp) -- Store: [0x8000375c]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x041f59 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x588008 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000674]:fdiv.s t6, t5, t4, dyn
	-[0x80000678]:csrrs a2, fcsr, zero
	-[0x8000067c]:sw t6, 136(fp)
Current Store : [0x80000680] : sw a2, 140(fp) -- Store: [0x80003764]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x041f59 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x588008 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000694]:fdiv.s t6, t5, t4, dyn
	-[0x80000698]:csrrs a2, fcsr, zero
	-[0x8000069c]:sw t6, 144(fp)
Current Store : [0x800006a0] : sw a2, 148(fp) -- Store: [0x8000376c]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x041f59 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x588008 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006b4]:fdiv.s t6, t5, t4, dyn
	-[0x800006b8]:csrrs a2, fcsr, zero
	-[0x800006bc]:sw t6, 152(fp)
Current Store : [0x800006c0] : sw a2, 156(fp) -- Store: [0x80003774]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x1862ba and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x79b43d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006d4]:fdiv.s t6, t5, t4, dyn
	-[0x800006d8]:csrrs a2, fcsr, zero
	-[0x800006dc]:sw t6, 160(fp)
Current Store : [0x800006e0] : sw a2, 164(fp) -- Store: [0x8000377c]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x1862ba and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x79b43d and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006f4]:fdiv.s t6, t5, t4, dyn
	-[0x800006f8]:csrrs a2, fcsr, zero
	-[0x800006fc]:sw t6, 168(fp)
Current Store : [0x80000700] : sw a2, 172(fp) -- Store: [0x80003784]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x1862ba and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x79b43d and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000714]:fdiv.s t6, t5, t4, dyn
	-[0x80000718]:csrrs a2, fcsr, zero
	-[0x8000071c]:sw t6, 176(fp)
Current Store : [0x80000720] : sw a2, 180(fp) -- Store: [0x8000378c]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x1862ba and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x79b43d and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000734]:fdiv.s t6, t5, t4, dyn
	-[0x80000738]:csrrs a2, fcsr, zero
	-[0x8000073c]:sw t6, 184(fp)
Current Store : [0x80000740] : sw a2, 188(fp) -- Store: [0x80003794]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x1862ba and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x79b43d and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000754]:fdiv.s t6, t5, t4, dyn
	-[0x80000758]:csrrs a2, fcsr, zero
	-[0x8000075c]:sw t6, 192(fp)
Current Store : [0x80000760] : sw a2, 196(fp) -- Store: [0x8000379c]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x0c94bf and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x665c52 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000774]:fdiv.s t6, t5, t4, dyn
	-[0x80000778]:csrrs a2, fcsr, zero
	-[0x8000077c]:sw t6, 200(fp)
Current Store : [0x80000780] : sw a2, 204(fp) -- Store: [0x800037a4]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x0c94bf and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x665c52 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000794]:fdiv.s t6, t5, t4, dyn
	-[0x80000798]:csrrs a2, fcsr, zero
	-[0x8000079c]:sw t6, 208(fp)
Current Store : [0x800007a0] : sw a2, 212(fp) -- Store: [0x800037ac]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x0c94bf and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x665c52 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007b4]:fdiv.s t6, t5, t4, dyn
	-[0x800007b8]:csrrs a2, fcsr, zero
	-[0x800007bc]:sw t6, 216(fp)
Current Store : [0x800007c0] : sw a2, 220(fp) -- Store: [0x800037b4]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x0c94bf and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x665c52 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007d4]:fdiv.s t6, t5, t4, dyn
	-[0x800007d8]:csrrs a2, fcsr, zero
	-[0x800007dc]:sw t6, 224(fp)
Current Store : [0x800007e0] : sw a2, 228(fp) -- Store: [0x800037bc]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x0c94bf and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x665c52 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007f4]:fdiv.s t6, t5, t4, dyn
	-[0x800007f8]:csrrs a2, fcsr, zero
	-[0x800007fc]:sw t6, 232(fp)
Current Store : [0x80000800] : sw a2, 236(fp) -- Store: [0x800037c4]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x16c9d3 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x771633 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000814]:fdiv.s t6, t5, t4, dyn
	-[0x80000818]:csrrs a2, fcsr, zero
	-[0x8000081c]:sw t6, 240(fp)
Current Store : [0x80000820] : sw a2, 244(fp) -- Store: [0x800037cc]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x16c9d3 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x771633 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000834]:fdiv.s t6, t5, t4, dyn
	-[0x80000838]:csrrs a2, fcsr, zero
	-[0x8000083c]:sw t6, 248(fp)
Current Store : [0x80000840] : sw a2, 252(fp) -- Store: [0x800037d4]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x16c9d3 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x771633 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000854]:fdiv.s t6, t5, t4, dyn
	-[0x80000858]:csrrs a2, fcsr, zero
	-[0x8000085c]:sw t6, 256(fp)
Current Store : [0x80000860] : sw a2, 260(fp) -- Store: [0x800037dc]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x16c9d3 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x771633 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000874]:fdiv.s t6, t5, t4, dyn
	-[0x80000878]:csrrs a2, fcsr, zero
	-[0x8000087c]:sw t6, 264(fp)
Current Store : [0x80000880] : sw a2, 268(fp) -- Store: [0x800037e4]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x16c9d3 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x771633 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000894]:fdiv.s t6, t5, t4, dyn
	-[0x80000898]:csrrs a2, fcsr, zero
	-[0x8000089c]:sw t6, 272(fp)
Current Store : [0x800008a0] : sw a2, 276(fp) -- Store: [0x800037ec]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x0e1a47 and fs2 == 1 and fe2 == 0xb4 and fm2 == 0x68da9e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008b4]:fdiv.s t6, t5, t4, dyn
	-[0x800008b8]:csrrs a2, fcsr, zero
	-[0x800008bc]:sw t6, 280(fp)
Current Store : [0x800008c0] : sw a2, 284(fp) -- Store: [0x800037f4]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x0e1a47 and fs2 == 1 and fe2 == 0xb4 and fm2 == 0x68da9e and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008d4]:fdiv.s t6, t5, t4, dyn
	-[0x800008d8]:csrrs a2, fcsr, zero
	-[0x800008dc]:sw t6, 288(fp)
Current Store : [0x800008e0] : sw a2, 292(fp) -- Store: [0x800037fc]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x0e1a47 and fs2 == 1 and fe2 == 0xb4 and fm2 == 0x68da9e and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008f4]:fdiv.s t6, t5, t4, dyn
	-[0x800008f8]:csrrs a2, fcsr, zero
	-[0x800008fc]:sw t6, 296(fp)
Current Store : [0x80000900] : sw a2, 300(fp) -- Store: [0x80003804]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x0e1a47 and fs2 == 1 and fe2 == 0xb4 and fm2 == 0x68da9e and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000914]:fdiv.s t6, t5, t4, dyn
	-[0x80000918]:csrrs a2, fcsr, zero
	-[0x8000091c]:sw t6, 304(fp)
Current Store : [0x80000920] : sw a2, 308(fp) -- Store: [0x8000380c]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x0e1a47 and fs2 == 1 and fe2 == 0xb4 and fm2 == 0x68da9e and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000934]:fdiv.s t6, t5, t4, dyn
	-[0x80000938]:csrrs a2, fcsr, zero
	-[0x8000093c]:sw t6, 312(fp)
Current Store : [0x80000940] : sw a2, 316(fp) -- Store: [0x80003814]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x1747cc and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x77e49e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000954]:fdiv.s t6, t5, t4, dyn
	-[0x80000958]:csrrs a2, fcsr, zero
	-[0x8000095c]:sw t6, 320(fp)
Current Store : [0x80000960] : sw a2, 324(fp) -- Store: [0x8000381c]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x1747cc and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x77e49e and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000974]:fdiv.s t6, t5, t4, dyn
	-[0x80000978]:csrrs a2, fcsr, zero
	-[0x8000097c]:sw t6, 328(fp)
Current Store : [0x80000980] : sw a2, 332(fp) -- Store: [0x80003824]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x1747cc and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x77e49e and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000994]:fdiv.s t6, t5, t4, dyn
	-[0x80000998]:csrrs a2, fcsr, zero
	-[0x8000099c]:sw t6, 336(fp)
Current Store : [0x800009a0] : sw a2, 340(fp) -- Store: [0x8000382c]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x1747cc and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x77e49e and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009b4]:fdiv.s t6, t5, t4, dyn
	-[0x800009b8]:csrrs a2, fcsr, zero
	-[0x800009bc]:sw t6, 344(fp)
Current Store : [0x800009c0] : sw a2, 348(fp) -- Store: [0x80003834]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x1747cc and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x77e49e and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009d4]:fdiv.s t6, t5, t4, dyn
	-[0x800009d8]:csrrs a2, fcsr, zero
	-[0x800009dc]:sw t6, 352(fp)
Current Store : [0x800009e0] : sw a2, 356(fp) -- Store: [0x8000383c]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x0dff3c and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x39ea30 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009f4]:fdiv.s t6, t5, t4, dyn
	-[0x800009f8]:csrrs a2, fcsr, zero
	-[0x800009fc]:sw t6, 360(fp)
Current Store : [0x80000a00] : sw a2, 364(fp) -- Store: [0x80003844]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x0dff3c and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x39ea30 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a14]:fdiv.s t6, t5, t4, dyn
	-[0x80000a18]:csrrs a2, fcsr, zero
	-[0x80000a1c]:sw t6, 368(fp)
Current Store : [0x80000a20] : sw a2, 372(fp) -- Store: [0x8000384c]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x0dff3c and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x39ea30 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a34]:fdiv.s t6, t5, t4, dyn
	-[0x80000a38]:csrrs a2, fcsr, zero
	-[0x80000a3c]:sw t6, 376(fp)
Current Store : [0x80000a40] : sw a2, 380(fp) -- Store: [0x80003854]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x0dff3c and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x39ea30 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a54]:fdiv.s t6, t5, t4, dyn
	-[0x80000a58]:csrrs a2, fcsr, zero
	-[0x80000a5c]:sw t6, 384(fp)
Current Store : [0x80000a60] : sw a2, 388(fp) -- Store: [0x8000385c]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x0dff3c and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x39ea30 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a74]:fdiv.s t6, t5, t4, dyn
	-[0x80000a78]:csrrs a2, fcsr, zero
	-[0x80000a7c]:sw t6, 392(fp)
Current Store : [0x80000a80] : sw a2, 396(fp) -- Store: [0x80003864]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x3c143a and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x763fd6 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a94]:fdiv.s t6, t5, t4, dyn
	-[0x80000a98]:csrrs a2, fcsr, zero
	-[0x80000a9c]:sw t6, 400(fp)
Current Store : [0x80000aa0] : sw a2, 404(fp) -- Store: [0x8000386c]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x3c143a and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x763fd6 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ab4]:fdiv.s t6, t5, t4, dyn
	-[0x80000ab8]:csrrs a2, fcsr, zero
	-[0x80000abc]:sw t6, 408(fp)
Current Store : [0x80000ac0] : sw a2, 412(fp) -- Store: [0x80003874]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x3c143a and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x763fd6 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ad4]:fdiv.s t6, t5, t4, dyn
	-[0x80000ad8]:csrrs a2, fcsr, zero
	-[0x80000adc]:sw t6, 416(fp)
Current Store : [0x80000ae0] : sw a2, 420(fp) -- Store: [0x8000387c]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x3c143a and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x763fd6 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000af4]:fdiv.s t6, t5, t4, dyn
	-[0x80000af8]:csrrs a2, fcsr, zero
	-[0x80000afc]:sw t6, 424(fp)
Current Store : [0x80000b00] : sw a2, 428(fp) -- Store: [0x80003884]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x3c143a and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x763fd6 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b14]:fdiv.s t6, t5, t4, dyn
	-[0x80000b18]:csrrs a2, fcsr, zero
	-[0x80000b1c]:sw t6, 432(fp)
Current Store : [0x80000b20] : sw a2, 436(fp) -- Store: [0x8000388c]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x07e7a0 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x31f037 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b34]:fdiv.s t6, t5, t4, dyn
	-[0x80000b38]:csrrs a2, fcsr, zero
	-[0x80000b3c]:sw t6, 440(fp)
Current Store : [0x80000b40] : sw a2, 444(fp) -- Store: [0x80003894]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x07e7a0 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x31f037 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b54]:fdiv.s t6, t5, t4, dyn
	-[0x80000b58]:csrrs a2, fcsr, zero
	-[0x80000b5c]:sw t6, 448(fp)
Current Store : [0x80000b60] : sw a2, 452(fp) -- Store: [0x8000389c]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x07e7a0 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x31f037 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b74]:fdiv.s t6, t5, t4, dyn
	-[0x80000b78]:csrrs a2, fcsr, zero
	-[0x80000b7c]:sw t6, 456(fp)
Current Store : [0x80000b80] : sw a2, 460(fp) -- Store: [0x800038a4]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x07e7a0 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x31f037 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b94]:fdiv.s t6, t5, t4, dyn
	-[0x80000b98]:csrrs a2, fcsr, zero
	-[0x80000b9c]:sw t6, 464(fp)
Current Store : [0x80000ba0] : sw a2, 468(fp) -- Store: [0x800038ac]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x07e7a0 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x31f037 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bb4]:fdiv.s t6, t5, t4, dyn
	-[0x80000bb8]:csrrs a2, fcsr, zero
	-[0x80000bbc]:sw t6, 472(fp)
Current Store : [0x80000bc0] : sw a2, 476(fp) -- Store: [0x800038b4]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x506fba and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x087397 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bd4]:fdiv.s t6, t5, t4, dyn
	-[0x80000bd8]:csrrs a2, fcsr, zero
	-[0x80000bdc]:sw t6, 480(fp)
Current Store : [0x80000be0] : sw a2, 484(fp) -- Store: [0x800038bc]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x506fba and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x087397 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bf4]:fdiv.s t6, t5, t4, dyn
	-[0x80000bf8]:csrrs a2, fcsr, zero
	-[0x80000bfc]:sw t6, 488(fp)
Current Store : [0x80000c00] : sw a2, 492(fp) -- Store: [0x800038c4]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x506fba and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x087397 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c14]:fdiv.s t6, t5, t4, dyn
	-[0x80000c18]:csrrs a2, fcsr, zero
	-[0x80000c1c]:sw t6, 496(fp)
Current Store : [0x80000c20] : sw a2, 500(fp) -- Store: [0x800038cc]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x506fba and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x087397 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c34]:fdiv.s t6, t5, t4, dyn
	-[0x80000c38]:csrrs a2, fcsr, zero
	-[0x80000c3c]:sw t6, 504(fp)
Current Store : [0x80000c40] : sw a2, 508(fp) -- Store: [0x800038d4]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x506fba and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x087397 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c54]:fdiv.s t6, t5, t4, dyn
	-[0x80000c58]:csrrs a2, fcsr, zero
	-[0x80000c5c]:sw t6, 512(fp)
Current Store : [0x80000c60] : sw a2, 516(fp) -- Store: [0x800038dc]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x210ca3 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x52dc1c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c74]:fdiv.s t6, t5, t4, dyn
	-[0x80000c78]:csrrs a2, fcsr, zero
	-[0x80000c7c]:sw t6, 520(fp)
Current Store : [0x80000c80] : sw a2, 524(fp) -- Store: [0x800038e4]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x210ca3 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x52dc1c and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c94]:fdiv.s t6, t5, t4, dyn
	-[0x80000c98]:csrrs a2, fcsr, zero
	-[0x80000c9c]:sw t6, 528(fp)
Current Store : [0x80000ca0] : sw a2, 532(fp) -- Store: [0x800038ec]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x210ca3 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x52dc1c and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cb4]:fdiv.s t6, t5, t4, dyn
	-[0x80000cb8]:csrrs a2, fcsr, zero
	-[0x80000cbc]:sw t6, 536(fp)
Current Store : [0x80000cc0] : sw a2, 540(fp) -- Store: [0x800038f4]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x210ca3 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x52dc1c and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cd4]:fdiv.s t6, t5, t4, dyn
	-[0x80000cd8]:csrrs a2, fcsr, zero
	-[0x80000cdc]:sw t6, 544(fp)
Current Store : [0x80000ce0] : sw a2, 548(fp) -- Store: [0x800038fc]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x210ca3 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x52dc1c and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cf4]:fdiv.s t6, t5, t4, dyn
	-[0x80000cf8]:csrrs a2, fcsr, zero
	-[0x80000cfc]:sw t6, 552(fp)
Current Store : [0x80000d00] : sw a2, 556(fp) -- Store: [0x80003904]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x18 and fm1 == 0x1cec94 and fs2 == 0 and fe2 == 0xb0 and fm2 == 0x4d756d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d14]:fdiv.s t6, t5, t4, dyn
	-[0x80000d18]:csrrs a2, fcsr, zero
	-[0x80000d1c]:sw t6, 560(fp)
Current Store : [0x80000d20] : sw a2, 564(fp) -- Store: [0x8000390c]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x18 and fm1 == 0x1cec94 and fs2 == 0 and fe2 == 0xb0 and fm2 == 0x4d756d and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d34]:fdiv.s t6, t5, t4, dyn
	-[0x80000d38]:csrrs a2, fcsr, zero
	-[0x80000d3c]:sw t6, 568(fp)
Current Store : [0x80000d40] : sw a2, 572(fp) -- Store: [0x80003914]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x18 and fm1 == 0x1cec94 and fs2 == 0 and fe2 == 0xb0 and fm2 == 0x4d756d and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d54]:fdiv.s t6, t5, t4, dyn
	-[0x80000d58]:csrrs a2, fcsr, zero
	-[0x80000d5c]:sw t6, 576(fp)
Current Store : [0x80000d60] : sw a2, 580(fp) -- Store: [0x8000391c]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x18 and fm1 == 0x1cec94 and fs2 == 0 and fe2 == 0xb0 and fm2 == 0x4d756d and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d74]:fdiv.s t6, t5, t4, dyn
	-[0x80000d78]:csrrs a2, fcsr, zero
	-[0x80000d7c]:sw t6, 584(fp)
Current Store : [0x80000d80] : sw a2, 588(fp) -- Store: [0x80003924]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x18 and fm1 == 0x1cec94 and fs2 == 0 and fe2 == 0xb0 and fm2 == 0x4d756d and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d94]:fdiv.s t6, t5, t4, dyn
	-[0x80000d98]:csrrs a2, fcsr, zero
	-[0x80000d9c]:sw t6, 592(fp)
Current Store : [0x80000da0] : sw a2, 596(fp) -- Store: [0x8000392c]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x6a2294 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x194664 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000db4]:fdiv.s t6, t5, t4, dyn
	-[0x80000db8]:csrrs a2, fcsr, zero
	-[0x80000dbc]:sw t6, 600(fp)
Current Store : [0x80000dc0] : sw a2, 604(fp) -- Store: [0x80003934]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x6a2294 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x194664 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000dd4]:fdiv.s t6, t5, t4, dyn
	-[0x80000dd8]:csrrs a2, fcsr, zero
	-[0x80000ddc]:sw t6, 608(fp)
Current Store : [0x80000de0] : sw a2, 612(fp) -- Store: [0x8000393c]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x6a2294 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x194664 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000df4]:fdiv.s t6, t5, t4, dyn
	-[0x80000df8]:csrrs a2, fcsr, zero
	-[0x80000dfc]:sw t6, 616(fp)
Current Store : [0x80000e00] : sw a2, 620(fp) -- Store: [0x80003944]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x6a2294 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x194664 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e14]:fdiv.s t6, t5, t4, dyn
	-[0x80000e18]:csrrs a2, fcsr, zero
	-[0x80000e1c]:sw t6, 624(fp)
Current Store : [0x80000e20] : sw a2, 628(fp) -- Store: [0x8000394c]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x6a2294 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x194664 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e34]:fdiv.s t6, t5, t4, dyn
	-[0x80000e38]:csrrs a2, fcsr, zero
	-[0x80000e3c]:sw t6, 632(fp)
Current Store : [0x80000e40] : sw a2, 636(fp) -- Store: [0x80003954]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x17f238 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x7221c9 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e54]:fdiv.s t6, t5, t4, dyn
	-[0x80000e58]:csrrs a2, fcsr, zero
	-[0x80000e5c]:sw t6, 640(fp)
Current Store : [0x80000e60] : sw a2, 644(fp) -- Store: [0x8000395c]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x17f238 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x7221c9 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e74]:fdiv.s t6, t5, t4, dyn
	-[0x80000e78]:csrrs a2, fcsr, zero
	-[0x80000e7c]:sw t6, 648(fp)
Current Store : [0x80000e80] : sw a2, 652(fp) -- Store: [0x80003964]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x17f238 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x7221c9 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e94]:fdiv.s t6, t5, t4, dyn
	-[0x80000e98]:csrrs a2, fcsr, zero
	-[0x80000e9c]:sw t6, 656(fp)
Current Store : [0x80000ea0] : sw a2, 660(fp) -- Store: [0x8000396c]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x17f238 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x7221c9 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000eb4]:fdiv.s t6, t5, t4, dyn
	-[0x80000eb8]:csrrs a2, fcsr, zero
	-[0x80000ebc]:sw t6, 664(fp)
Current Store : [0x80000ec0] : sw a2, 668(fp) -- Store: [0x80003974]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x17f238 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x7221c9 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ed4]:fdiv.s t6, t5, t4, dyn
	-[0x80000ed8]:csrrs a2, fcsr, zero
	-[0x80000edc]:sw t6, 672(fp)
Current Store : [0x80000ee0] : sw a2, 676(fp) -- Store: [0x8000397c]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x06ae55 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x569e87 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ef4]:fdiv.s t6, t5, t4, dyn
	-[0x80000ef8]:csrrs a2, fcsr, zero
	-[0x80000efc]:sw t6, 680(fp)
Current Store : [0x80000f00] : sw a2, 684(fp) -- Store: [0x80003984]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x06ae55 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x569e87 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f14]:fdiv.s t6, t5, t4, dyn
	-[0x80000f18]:csrrs a2, fcsr, zero
	-[0x80000f1c]:sw t6, 688(fp)
Current Store : [0x80000f20] : sw a2, 692(fp) -- Store: [0x8000398c]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x06ae55 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x569e87 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f34]:fdiv.s t6, t5, t4, dyn
	-[0x80000f38]:csrrs a2, fcsr, zero
	-[0x80000f3c]:sw t6, 696(fp)
Current Store : [0x80000f40] : sw a2, 700(fp) -- Store: [0x80003994]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x06ae55 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x569e87 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f54]:fdiv.s t6, t5, t4, dyn
	-[0x80000f58]:csrrs a2, fcsr, zero
	-[0x80000f5c]:sw t6, 704(fp)
Current Store : [0x80000f60] : sw a2, 708(fp) -- Store: [0x8000399c]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x06ae55 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x569e87 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f74]:fdiv.s t6, t5, t4, dyn
	-[0x80000f78]:csrrs a2, fcsr, zero
	-[0x80000f7c]:sw t6, 712(fp)
Current Store : [0x80000f80] : sw a2, 716(fp) -- Store: [0x800039a4]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x44e5bd and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x1ce1ba and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f94]:fdiv.s t6, t5, t4, dyn
	-[0x80000f98]:csrrs a2, fcsr, zero
	-[0x80000f9c]:sw t6, 720(fp)
Current Store : [0x80000fa0] : sw a2, 724(fp) -- Store: [0x800039ac]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x44e5bd and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x1ce1ba and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fb4]:fdiv.s t6, t5, t4, dyn
	-[0x80000fb8]:csrrs a2, fcsr, zero
	-[0x80000fbc]:sw t6, 728(fp)
Current Store : [0x80000fc0] : sw a2, 732(fp) -- Store: [0x800039b4]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x44e5bd and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x1ce1ba and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fd4]:fdiv.s t6, t5, t4, dyn
	-[0x80000fd8]:csrrs a2, fcsr, zero
	-[0x80000fdc]:sw t6, 736(fp)
Current Store : [0x80000fe0] : sw a2, 740(fp) -- Store: [0x800039bc]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x44e5bd and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x1ce1ba and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ff4]:fdiv.s t6, t5, t4, dyn
	-[0x80000ff8]:csrrs a2, fcsr, zero
	-[0x80000ffc]:sw t6, 744(fp)
Current Store : [0x80001000] : sw a2, 748(fp) -- Store: [0x800039c4]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x44e5bd and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x1ce1ba and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001014]:fdiv.s t6, t5, t4, dyn
	-[0x80001018]:csrrs a2, fcsr, zero
	-[0x8000101c]:sw t6, 752(fp)
Current Store : [0x80001020] : sw a2, 756(fp) -- Store: [0x800039cc]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0f30be and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x642de8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001034]:fdiv.s t6, t5, t4, dyn
	-[0x80001038]:csrrs a2, fcsr, zero
	-[0x8000103c]:sw t6, 760(fp)
Current Store : [0x80001040] : sw a2, 764(fp) -- Store: [0x800039d4]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0f30be and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x642de8 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001054]:fdiv.s t6, t5, t4, dyn
	-[0x80001058]:csrrs a2, fcsr, zero
	-[0x8000105c]:sw t6, 768(fp)
Current Store : [0x80001060] : sw a2, 772(fp) -- Store: [0x800039dc]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0f30be and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x642de8 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001074]:fdiv.s t6, t5, t4, dyn
	-[0x80001078]:csrrs a2, fcsr, zero
	-[0x8000107c]:sw t6, 776(fp)
Current Store : [0x80001080] : sw a2, 780(fp) -- Store: [0x800039e4]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0f30be and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x642de8 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001094]:fdiv.s t6, t5, t4, dyn
	-[0x80001098]:csrrs a2, fcsr, zero
	-[0x8000109c]:sw t6, 784(fp)
Current Store : [0x800010a0] : sw a2, 788(fp) -- Store: [0x800039ec]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0f30be and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x642de8 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010b4]:fdiv.s t6, t5, t4, dyn
	-[0x800010b8]:csrrs a2, fcsr, zero
	-[0x800010bc]:sw t6, 792(fp)
Current Store : [0x800010c0] : sw a2, 796(fp) -- Store: [0x800039f4]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x23f956 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x02a63c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010d4]:fdiv.s t6, t5, t4, dyn
	-[0x800010d8]:csrrs a2, fcsr, zero
	-[0x800010dc]:sw t6, 800(fp)
Current Store : [0x800010e0] : sw a2, 804(fp) -- Store: [0x800039fc]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x23f956 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x02a63c and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010f4]:fdiv.s t6, t5, t4, dyn
	-[0x800010f8]:csrrs a2, fcsr, zero
	-[0x800010fc]:sw t6, 808(fp)
Current Store : [0x80001100] : sw a2, 812(fp) -- Store: [0x80003a04]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x23f956 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x02a63c and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001114]:fdiv.s t6, t5, t4, dyn
	-[0x80001118]:csrrs a2, fcsr, zero
	-[0x8000111c]:sw t6, 816(fp)
Current Store : [0x80001120] : sw a2, 820(fp) -- Store: [0x80003a0c]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x23f956 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x02a63c and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001134]:fdiv.s t6, t5, t4, dyn
	-[0x80001138]:csrrs a2, fcsr, zero
	-[0x8000113c]:sw t6, 824(fp)
Current Store : [0x80001140] : sw a2, 828(fp) -- Store: [0x80003a14]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x23f956 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x02a63c and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001154]:fdiv.s t6, t5, t4, dyn
	-[0x80001158]:csrrs a2, fcsr, zero
	-[0x8000115c]:sw t6, 832(fp)
Current Store : [0x80001160] : sw a2, 836(fp) -- Store: [0x80003a1c]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x3344b3 and fs2 == 0 and fe2 == 0xae and fm2 == 0x0ed5e0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001174]:fdiv.s t6, t5, t4, dyn
	-[0x80001178]:csrrs a2, fcsr, zero
	-[0x8000117c]:sw t6, 840(fp)
Current Store : [0x80001180] : sw a2, 844(fp) -- Store: [0x80003a24]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x3344b3 and fs2 == 0 and fe2 == 0xae and fm2 == 0x0ed5e0 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001194]:fdiv.s t6, t5, t4, dyn
	-[0x80001198]:csrrs a2, fcsr, zero
	-[0x8000119c]:sw t6, 848(fp)
Current Store : [0x800011a0] : sw a2, 852(fp) -- Store: [0x80003a2c]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x3344b3 and fs2 == 0 and fe2 == 0xae and fm2 == 0x0ed5e0 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011b4]:fdiv.s t6, t5, t4, dyn
	-[0x800011b8]:csrrs a2, fcsr, zero
	-[0x800011bc]:sw t6, 856(fp)
Current Store : [0x800011c0] : sw a2, 860(fp) -- Store: [0x80003a34]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x3344b3 and fs2 == 0 and fe2 == 0xae and fm2 == 0x0ed5e0 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011d4]:fdiv.s t6, t5, t4, dyn
	-[0x800011d8]:csrrs a2, fcsr, zero
	-[0x800011dc]:sw t6, 864(fp)
Current Store : [0x800011e0] : sw a2, 868(fp) -- Store: [0x80003a3c]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x3344b3 and fs2 == 0 and fe2 == 0xae and fm2 == 0x0ed5e0 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011f4]:fdiv.s t6, t5, t4, dyn
	-[0x800011f8]:csrrs a2, fcsr, zero
	-[0x800011fc]:sw t6, 872(fp)
Current Store : [0x80001200] : sw a2, 876(fp) -- Store: [0x80003a44]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0162e0 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x4e2e8e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001214]:fdiv.s t6, t5, t4, dyn
	-[0x80001218]:csrrs a2, fcsr, zero
	-[0x8000121c]:sw t6, 880(fp)
Current Store : [0x80001220] : sw a2, 884(fp) -- Store: [0x80003a4c]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0162e0 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x4e2e8e and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001234]:fdiv.s t6, t5, t4, dyn
	-[0x80001238]:csrrs a2, fcsr, zero
	-[0x8000123c]:sw t6, 888(fp)
Current Store : [0x80001240] : sw a2, 892(fp) -- Store: [0x80003a54]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0162e0 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x4e2e8e and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001254]:fdiv.s t6, t5, t4, dyn
	-[0x80001258]:csrrs a2, fcsr, zero
	-[0x8000125c]:sw t6, 896(fp)
Current Store : [0x80001260] : sw a2, 900(fp) -- Store: [0x80003a5c]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0162e0 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x4e2e8e and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001274]:fdiv.s t6, t5, t4, dyn
	-[0x80001278]:csrrs a2, fcsr, zero
	-[0x8000127c]:sw t6, 904(fp)
Current Store : [0x80001280] : sw a2, 908(fp) -- Store: [0x80003a64]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0162e0 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x4e2e8e and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001294]:fdiv.s t6, t5, t4, dyn
	-[0x80001298]:csrrs a2, fcsr, zero
	-[0x8000129c]:sw t6, 912(fp)
Current Store : [0x800012a0] : sw a2, 916(fp) -- Store: [0x80003a6c]:0x00000083




Last Coverpoint : ['mnemonic : fdiv.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x20c723 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x2c5b71 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012b4]:fdiv.s t6, t5, t4, dyn
	-[0x800012b8]:csrrs a2, fcsr, zero
	-[0x800012bc]:sw t6, 920(fp)
Current Store : [0x800012c0] : sw a2, 924(fp) -- Store: [0x80003a74]:0x00000003




Last Coverpoint : ['mnemonic : fdiv.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x20c723 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x2c5b71 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012d4]:fdiv.s t6, t5, t4, dyn
	-[0x800012d8]:csrrs a2, fcsr, zero
	-[0x800012dc]:sw t6, 928(fp)
Current Store : [0x800012e0] : sw a2, 932(fp) -- Store: [0x80003a7c]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x20c723 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x2c5b71 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012f4]:fdiv.s t6, t5, t4, dyn
	-[0x800012f8]:csrrs a2, fcsr, zero
	-[0x800012fc]:sw t6, 936(fp)
Current Store : [0x80001300] : sw a2, 940(fp) -- Store: [0x80003a84]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x20c723 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x2c5b71 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001314]:fdiv.s t6, t5, t4, dyn
	-[0x80001318]:csrrs a2, fcsr, zero
	-[0x8000131c]:sw t6, 944(fp)
Current Store : [0x80001320] : sw a2, 948(fp) -- Store: [0x80003a8c]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x1479a4 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x1f2b1f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001334]:fdiv.s t6, t5, t4, dyn
	-[0x80001338]:csrrs a2, fcsr, zero
	-[0x8000133c]:sw t6, 952(fp)
Current Store : [0x80001340] : sw a2, 956(fp) -- Store: [0x80003a94]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x1479a4 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x1f2b1f and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001354]:fdiv.s t6, t5, t4, dyn
	-[0x80001358]:csrrs a2, fcsr, zero
	-[0x8000135c]:sw t6, 960(fp)
Current Store : [0x80001360] : sw a2, 964(fp) -- Store: [0x80003a9c]:0x00000043




Last Coverpoint : ['mnemonic : fdiv.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x19 and fm1 == 0x1479a4 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x1f2b1f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001374]:fdiv.s t6, t5, t4, dyn
	-[0x80001378]:csrrs a2, fcsr, zero
	-[0x8000137c]:sw t6, 968(fp)
Current Store : [0x80001380] : sw a2, 972(fp) -- Store: [0x80003aa4]:0x00000063





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                                coverpoints                                                                                                                 |                                                      code                                                       |
|---:|--------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|
|   1|[0x80003610]<br>0x80000000|- mnemonic : fdiv.s<br> - rs1 : x30<br> - rs2 : x31<br> - rd : x31<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x20c723 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x2c5b71 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x80000124]:fdiv.s t6, t5, t6, dyn<br> [0x80000128]:csrrs tp, fcsr, zero<br> [0x8000012c]:sw t6, 0(ra)<br>      |
|   2|[0x80003618]<br>0x80000000|- rs1 : x29<br> - rs2 : x30<br> - rd : x29<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x20c723 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x2c5b71 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                        |[0x80000144]:fdiv.s t4, t4, t5, dyn<br> [0x80000148]:csrrs tp, fcsr, zero<br> [0x8000014c]:sw t4, 8(ra)<br>      |
|   3|[0x80003620]<br>0x80000001|- rs1 : x31<br> - rs2 : x29<br> - rd : x30<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x20c723 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x2c5b71 and  fcsr == 0x40 and rm_val == 7   #nosat<br> |[0x80000164]:fdiv.s t5, t6, t4, dyn<br> [0x80000168]:csrrs tp, fcsr, zero<br> [0x8000016c]:sw t5, 16(ra)<br>     |
|   4|[0x80003628]<br>0x3F800000|- rs1 : x27<br> - rs2 : x27<br> - rd : x28<br> - rs1 == rs2 != rd<br>                                                                                                                                                                       |[0x80000184]:fdiv.s t3, s11, s11, dyn<br> [0x80000188]:csrrs tp, fcsr, zero<br> [0x8000018c]:sw t3, 24(ra)<br>   |
|   5|[0x80003630]<br>0x3F800000|- rs1 : x26<br> - rs2 : x26<br> - rd : x26<br> - rs1 == rs2 == rd<br>                                                                                                                                                                       |[0x800001a4]:fdiv.s s10, s10, s10, dyn<br> [0x800001a8]:csrrs tp, fcsr, zero<br> [0x800001ac]:sw s10, 32(ra)<br> |
|   6|[0x80003638]<br>0x80000000|- rs1 : x28<br> - rs2 : x25<br> - rd : x27<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x537439 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x62aedc and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x800001c4]:fdiv.s s11, t3, s9, dyn<br> [0x800001c8]:csrrs tp, fcsr, zero<br> [0x800001cc]:sw s11, 40(ra)<br>   |
|   7|[0x80003640]<br>0x80000000|- rs1 : x24<br> - rs2 : x28<br> - rd : x25<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x537439 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x62aedc and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                               |[0x800001e4]:fdiv.s s9, s8, t3, dyn<br> [0x800001e8]:csrrs tp, fcsr, zero<br> [0x800001ec]:sw s9, 48(ra)<br>     |
|   8|[0x80003648]<br>0x80000001|- rs1 : x25<br> - rs2 : x23<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x537439 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x62aedc and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                               |[0x80000204]:fdiv.s s8, s9, s7, dyn<br> [0x80000208]:csrrs tp, fcsr, zero<br> [0x8000020c]:sw s8, 56(ra)<br>     |
|   9|[0x80003650]<br>0x80000000|- rs1 : x22<br> - rs2 : x24<br> - rd : x23<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x537439 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x62aedc and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                               |[0x80000224]:fdiv.s s7, s6, s8, dyn<br> [0x80000228]:csrrs tp, fcsr, zero<br> [0x8000022c]:sw s7, 64(ra)<br>     |
|  10|[0x80003658]<br>0x80000000|- rs1 : x23<br> - rs2 : x21<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x537439 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x62aedc and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                               |[0x80000244]:fdiv.s s6, s7, s5, dyn<br> [0x80000248]:csrrs tp, fcsr, zero<br> [0x8000024c]:sw s6, 72(ra)<br>     |
|  11|[0x80003660]<br>0x80000000|- rs1 : x20<br> - rs2 : x22<br> - rd : x21<br> - fs1 == 0 and fe1 == 0x17 and fm1 == 0x73ac65 and fs2 == 1 and fe2 == 0xaf and fm2 == 0x029c88 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000264]:fdiv.s s5, s4, s6, dyn<br> [0x80000268]:csrrs tp, fcsr, zero<br> [0x8000026c]:sw s5, 80(ra)<br>     |
|  12|[0x80003668]<br>0x80000000|- rs1 : x21<br> - rs2 : x19<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x17 and fm1 == 0x73ac65 and fs2 == 1 and fe2 == 0xaf and fm2 == 0x029c88 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                               |[0x80000284]:fdiv.s s4, s5, s3, dyn<br> [0x80000288]:csrrs tp, fcsr, zero<br> [0x8000028c]:sw s4, 88(ra)<br>     |
|  13|[0x80003670]<br>0x80000001|- rs1 : x18<br> - rs2 : x20<br> - rd : x19<br> - fs1 == 0 and fe1 == 0x17 and fm1 == 0x73ac65 and fs2 == 1 and fe2 == 0xaf and fm2 == 0x029c88 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                               |[0x800002a4]:fdiv.s s3, s2, s4, dyn<br> [0x800002a8]:csrrs tp, fcsr, zero<br> [0x800002ac]:sw s3, 96(ra)<br>     |
|  14|[0x80003678]<br>0x80000000|- rs1 : x19<br> - rs2 : x17<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x17 and fm1 == 0x73ac65 and fs2 == 1 and fe2 == 0xaf and fm2 == 0x029c88 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                               |[0x800002c4]:fdiv.s s2, s3, a7, dyn<br> [0x800002c8]:csrrs tp, fcsr, zero<br> [0x800002cc]:sw s2, 104(ra)<br>    |
|  15|[0x80003680]<br>0x80000000|- rs1 : x16<br> - rs2 : x18<br> - rd : x17<br> - fs1 == 0 and fe1 == 0x17 and fm1 == 0x73ac65 and fs2 == 1 and fe2 == 0xaf and fm2 == 0x029c88 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                               |[0x800002e4]:fdiv.s a7, a6, s2, dyn<br> [0x800002e8]:csrrs tp, fcsr, zero<br> [0x800002ec]:sw a7, 112(ra)<br>    |
|  16|[0x80003688]<br>0x80000000|- rs1 : x17<br> - rs2 : x15<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x1b and fm1 == 0x079c52 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x11609d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000304]:fdiv.s a6, a7, a5, dyn<br> [0x80000308]:csrrs tp, fcsr, zero<br> [0x8000030c]:sw a6, 120(ra)<br>    |
|  17|[0x80003690]<br>0x80000000|- rs1 : x14<br> - rs2 : x16<br> - rd : x15<br> - fs1 == 0 and fe1 == 0x1b and fm1 == 0x079c52 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x11609d and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                               |[0x80000324]:fdiv.s a5, a4, a6, dyn<br> [0x80000328]:csrrs tp, fcsr, zero<br> [0x8000032c]:sw a5, 128(ra)<br>    |
|  18|[0x80003698]<br>0x80000001|- rs1 : x15<br> - rs2 : x13<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x1b and fm1 == 0x079c52 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x11609d and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                               |[0x80000344]:fdiv.s a4, a5, a3, dyn<br> [0x80000348]:csrrs tp, fcsr, zero<br> [0x8000034c]:sw a4, 136(ra)<br>    |
|  19|[0x800036a0]<br>0x80000000|- rs1 : x12<br> - rs2 : x14<br> - rd : x13<br> - fs1 == 0 and fe1 == 0x1b and fm1 == 0x079c52 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x11609d and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                               |[0x80000364]:fdiv.s a3, a2, a4, dyn<br> [0x80000368]:csrrs tp, fcsr, zero<br> [0x8000036c]:sw a3, 144(ra)<br>    |
|  20|[0x800036a8]<br>0x80000000|- rs1 : x13<br> - rs2 : x11<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x1b and fm1 == 0x079c52 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x11609d and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                               |[0x80000384]:fdiv.s a2, a3, a1, dyn<br> [0x80000388]:csrrs tp, fcsr, zero<br> [0x8000038c]:sw a2, 152(ra)<br>    |
|  21|[0x800036b0]<br>0x80000000|- rs1 : x10<br> - rs2 : x12<br> - rd : x11<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x775fc0 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x049853 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x800003a4]:fdiv.s a1, a0, a2, dyn<br> [0x800003a8]:csrrs tp, fcsr, zero<br> [0x800003ac]:sw a1, 160(ra)<br>    |
|  22|[0x800036b8]<br>0x80000000|- rs1 : x11<br> - rs2 : x9<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x775fc0 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x049853 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                |[0x800003c4]:fdiv.s a0, a1, s1, dyn<br> [0x800003c8]:csrrs tp, fcsr, zero<br> [0x800003cc]:sw a0, 168(ra)<br>    |
|  23|[0x800036c0]<br>0x80000001|- rs1 : x8<br> - rs2 : x10<br> - rd : x9<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x775fc0 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x049853 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                 |[0x800003ec]:fdiv.s s1, fp, a0, dyn<br> [0x800003f0]:csrrs a2, fcsr, zero<br> [0x800003f4]:sw s1, 176(ra)<br>    |
|  24|[0x800036c8]<br>0x80000000|- rs1 : x9<br> - rs2 : x7<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x775fc0 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x049853 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                  |[0x8000040c]:fdiv.s fp, s1, t2, dyn<br> [0x80000410]:csrrs a2, fcsr, zero<br> [0x80000414]:sw fp, 184(ra)<br>    |
|  25|[0x800036d0]<br>0x80000000|- rs1 : x6<br> - rs2 : x8<br> - rd : x7<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x775fc0 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x049853 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                  |[0x8000042c]:fdiv.s t2, t1, fp, dyn<br> [0x80000430]:csrrs a2, fcsr, zero<br> [0x80000434]:sw t2, 192(ra)<br>    |
|  26|[0x800036d8]<br>0x80000000|- rs1 : x7<br> - rs2 : x5<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x108952 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x1af22d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                   |[0x80000454]:fdiv.s t1, t2, t0, dyn<br> [0x80000458]:csrrs a2, fcsr, zero<br> [0x8000045c]:sw t1, 0(fp)<br>      |
|  27|[0x800036e0]<br>0x80000000|- rs1 : x4<br> - rs2 : x6<br> - rd : x5<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x108952 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x1af22d and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                  |[0x80000474]:fdiv.s t0, tp, t1, dyn<br> [0x80000478]:csrrs a2, fcsr, zero<br> [0x8000047c]:sw t0, 8(fp)<br>      |
|  28|[0x800036e8]<br>0x80000001|- rs1 : x5<br> - rs2 : x3<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x108952 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x1af22d and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                  |[0x80000494]:fdiv.s tp, t0, gp, dyn<br> [0x80000498]:csrrs a2, fcsr, zero<br> [0x8000049c]:sw tp, 16(fp)<br>     |
|  29|[0x800036f0]<br>0x80000000|- rs1 : x2<br> - rs2 : x4<br> - rd : x3<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x108952 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x1af22d and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                  |[0x800004b4]:fdiv.s gp, sp, tp, dyn<br> [0x800004b8]:csrrs a2, fcsr, zero<br> [0x800004bc]:sw gp, 24(fp)<br>     |
|  30|[0x800036f8]<br>0x80000000|- rs1 : x3<br> - rs2 : x1<br> - rd : x2<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x108952 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x1af22d and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                  |[0x800004d4]:fdiv.s sp, gp, ra, dyn<br> [0x800004d8]:csrrs a2, fcsr, zero<br> [0x800004dc]:sw sp, 32(fp)<br>     |
|  31|[0x80003700]<br>0x80000000|- rs1 : x0<br> - rs2 : x2<br> - rd : x1<br>                                                                                                                                                                                                 |[0x800004f4]:fdiv.s ra, zero, sp, dyn<br> [0x800004f8]:csrrs a2, fcsr, zero<br> [0x800004fc]:sw ra, 40(fp)<br>   |
|  32|[0x80003708]<br>0x80000000|- rs1 : x1<br> - fs1 == 0 and fe1 == 0x19 and fm1 == 0x1479a4 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x1f2b1f and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                               |[0x80000514]:fdiv.s t6, ra, t5, dyn<br> [0x80000518]:csrrs a2, fcsr, zero<br> [0x8000051c]:sw t6, 48(fp)<br>     |
|  33|[0x80003710]<br>0x7F800000|- rs2 : x0<br>                                                                                                                                                                                                                              |[0x80000534]:fdiv.s t6, t5, zero, dyn<br> [0x80000538]:csrrs a2, fcsr, zero<br> [0x8000053c]:sw t6, 56(fp)<br>   |
|  34|[0x80003718]<br>0x00000000|- rd : x0<br> - fs1 == 0 and fe1 == 0x19 and fm1 == 0x1479a4 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x1f2b1f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                |[0x80000554]:fdiv.s zero, t6, t5, dyn<br> [0x80000558]:csrrs a2, fcsr, zero<br> [0x8000055c]:sw zero, 64(fp)<br> |
|  35|[0x80003720]<br>0x80000000|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x1479a4 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x1f2b1f and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000574]:fdiv.s t6, t5, t4, dyn<br> [0x80000578]:csrrs a2, fcsr, zero<br> [0x8000057c]:sw t6, 72(fp)<br>     |
|  36|[0x80003728]<br>0x80000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x1243d4 and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x6facab and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000594]:fdiv.s t6, t5, t4, dyn<br> [0x80000598]:csrrs a2, fcsr, zero<br> [0x8000059c]:sw t6, 80(fp)<br>     |
|  37|[0x80003730]<br>0x80000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x1243d4 and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x6facab and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800005b4]:fdiv.s t6, t5, t4, dyn<br> [0x800005b8]:csrrs a2, fcsr, zero<br> [0x800005bc]:sw t6, 88(fp)<br>     |
|  38|[0x80003738]<br>0x80000001|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x1243d4 and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x6facab and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800005d4]:fdiv.s t6, t5, t4, dyn<br> [0x800005d8]:csrrs a2, fcsr, zero<br> [0x800005dc]:sw t6, 96(fp)<br>     |
|  39|[0x80003740]<br>0x80000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x1243d4 and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x6facab and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800005f4]:fdiv.s t6, t5, t4, dyn<br> [0x800005f8]:csrrs a2, fcsr, zero<br> [0x800005fc]:sw t6, 104(fp)<br>    |
|  40|[0x80003748]<br>0x80000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x1243d4 and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x6facab and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000614]:fdiv.s t6, t5, t4, dyn<br> [0x80000618]:csrrs a2, fcsr, zero<br> [0x8000061c]:sw t6, 112(fp)<br>    |
|  41|[0x80003750]<br>0x80000000|- fs1 == 0 and fe1 == 0x16 and fm1 == 0x041f59 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x588008 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000634]:fdiv.s t6, t5, t4, dyn<br> [0x80000638]:csrrs a2, fcsr, zero<br> [0x8000063c]:sw t6, 120(fp)<br>    |
|  42|[0x80003758]<br>0x80000000|- fs1 == 0 and fe1 == 0x16 and fm1 == 0x041f59 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x588008 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000654]:fdiv.s t6, t5, t4, dyn<br> [0x80000658]:csrrs a2, fcsr, zero<br> [0x8000065c]:sw t6, 128(fp)<br>    |
|  43|[0x80003760]<br>0x80000001|- fs1 == 0 and fe1 == 0x16 and fm1 == 0x041f59 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x588008 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000674]:fdiv.s t6, t5, t4, dyn<br> [0x80000678]:csrrs a2, fcsr, zero<br> [0x8000067c]:sw t6, 136(fp)<br>    |
|  44|[0x80003768]<br>0x80000000|- fs1 == 0 and fe1 == 0x16 and fm1 == 0x041f59 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x588008 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000694]:fdiv.s t6, t5, t4, dyn<br> [0x80000698]:csrrs a2, fcsr, zero<br> [0x8000069c]:sw t6, 144(fp)<br>    |
|  45|[0x80003770]<br>0x80000000|- fs1 == 0 and fe1 == 0x16 and fm1 == 0x041f59 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x588008 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800006b4]:fdiv.s t6, t5, t4, dyn<br> [0x800006b8]:csrrs a2, fcsr, zero<br> [0x800006bc]:sw t6, 152(fp)<br>    |
|  46|[0x80003778]<br>0x80000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x1862ba and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x79b43d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800006d4]:fdiv.s t6, t5, t4, dyn<br> [0x800006d8]:csrrs a2, fcsr, zero<br> [0x800006dc]:sw t6, 160(fp)<br>    |
|  47|[0x80003780]<br>0x80000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x1862ba and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x79b43d and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800006f4]:fdiv.s t6, t5, t4, dyn<br> [0x800006f8]:csrrs a2, fcsr, zero<br> [0x800006fc]:sw t6, 168(fp)<br>    |
|  48|[0x80003788]<br>0x80000001|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x1862ba and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x79b43d and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000714]:fdiv.s t6, t5, t4, dyn<br> [0x80000718]:csrrs a2, fcsr, zero<br> [0x8000071c]:sw t6, 176(fp)<br>    |
|  49|[0x80003790]<br>0x80000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x1862ba and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x79b43d and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000734]:fdiv.s t6, t5, t4, dyn<br> [0x80000738]:csrrs a2, fcsr, zero<br> [0x8000073c]:sw t6, 184(fp)<br>    |
|  50|[0x80003798]<br>0x80000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x1862ba and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x79b43d and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000754]:fdiv.s t6, t5, t4, dyn<br> [0x80000758]:csrrs a2, fcsr, zero<br> [0x8000075c]:sw t6, 192(fp)<br>    |
|  51|[0x800037a0]<br>0x80000000|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x0c94bf and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x665c52 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000774]:fdiv.s t6, t5, t4, dyn<br> [0x80000778]:csrrs a2, fcsr, zero<br> [0x8000077c]:sw t6, 200(fp)<br>    |
|  52|[0x800037a8]<br>0x80000000|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x0c94bf and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x665c52 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000794]:fdiv.s t6, t5, t4, dyn<br> [0x80000798]:csrrs a2, fcsr, zero<br> [0x8000079c]:sw t6, 208(fp)<br>    |
|  53|[0x800037b0]<br>0x80000001|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x0c94bf and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x665c52 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800007b4]:fdiv.s t6, t5, t4, dyn<br> [0x800007b8]:csrrs a2, fcsr, zero<br> [0x800007bc]:sw t6, 216(fp)<br>    |
|  54|[0x800037b8]<br>0x80000000|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x0c94bf and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x665c52 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800007d4]:fdiv.s t6, t5, t4, dyn<br> [0x800007d8]:csrrs a2, fcsr, zero<br> [0x800007dc]:sw t6, 224(fp)<br>    |
|  55|[0x800037c0]<br>0x80000000|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x0c94bf and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x665c52 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800007f4]:fdiv.s t6, t5, t4, dyn<br> [0x800007f8]:csrrs a2, fcsr, zero<br> [0x800007fc]:sw t6, 232(fp)<br>    |
|  56|[0x800037c8]<br>0x80000000|- fs1 == 0 and fe1 == 0x16 and fm1 == 0x16c9d3 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x771633 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000814]:fdiv.s t6, t5, t4, dyn<br> [0x80000818]:csrrs a2, fcsr, zero<br> [0x8000081c]:sw t6, 240(fp)<br>    |
|  57|[0x800037d0]<br>0x80000000|- fs1 == 0 and fe1 == 0x16 and fm1 == 0x16c9d3 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x771633 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000834]:fdiv.s t6, t5, t4, dyn<br> [0x80000838]:csrrs a2, fcsr, zero<br> [0x8000083c]:sw t6, 248(fp)<br>    |
|  58|[0x800037d8]<br>0x80000001|- fs1 == 0 and fe1 == 0x16 and fm1 == 0x16c9d3 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x771633 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000854]:fdiv.s t6, t5, t4, dyn<br> [0x80000858]:csrrs a2, fcsr, zero<br> [0x8000085c]:sw t6, 256(fp)<br>    |
|  59|[0x800037e0]<br>0x80000000|- fs1 == 0 and fe1 == 0x16 and fm1 == 0x16c9d3 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x771633 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000874]:fdiv.s t6, t5, t4, dyn<br> [0x80000878]:csrrs a2, fcsr, zero<br> [0x8000087c]:sw t6, 264(fp)<br>    |
|  60|[0x800037e8]<br>0x80000000|- fs1 == 0 and fe1 == 0x16 and fm1 == 0x16c9d3 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x771633 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000894]:fdiv.s t6, t5, t4, dyn<br> [0x80000898]:csrrs a2, fcsr, zero<br> [0x8000089c]:sw t6, 272(fp)<br>    |
|  61|[0x800037f0]<br>0x80000000|- fs1 == 0 and fe1 == 0x1a and fm1 == 0x0e1a47 and fs2 == 1 and fe2 == 0xb4 and fm2 == 0x68da9e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800008b4]:fdiv.s t6, t5, t4, dyn<br> [0x800008b8]:csrrs a2, fcsr, zero<br> [0x800008bc]:sw t6, 280(fp)<br>    |
|  62|[0x800037f8]<br>0x80000000|- fs1 == 0 and fe1 == 0x1a and fm1 == 0x0e1a47 and fs2 == 1 and fe2 == 0xb4 and fm2 == 0x68da9e and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800008d4]:fdiv.s t6, t5, t4, dyn<br> [0x800008d8]:csrrs a2, fcsr, zero<br> [0x800008dc]:sw t6, 288(fp)<br>    |
|  63|[0x80003800]<br>0x80000001|- fs1 == 0 and fe1 == 0x1a and fm1 == 0x0e1a47 and fs2 == 1 and fe2 == 0xb4 and fm2 == 0x68da9e and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800008f4]:fdiv.s t6, t5, t4, dyn<br> [0x800008f8]:csrrs a2, fcsr, zero<br> [0x800008fc]:sw t6, 296(fp)<br>    |
|  64|[0x80003808]<br>0x80000000|- fs1 == 0 and fe1 == 0x1a and fm1 == 0x0e1a47 and fs2 == 1 and fe2 == 0xb4 and fm2 == 0x68da9e and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000914]:fdiv.s t6, t5, t4, dyn<br> [0x80000918]:csrrs a2, fcsr, zero<br> [0x8000091c]:sw t6, 304(fp)<br>    |
|  65|[0x80003810]<br>0x80000000|- fs1 == 0 and fe1 == 0x1a and fm1 == 0x0e1a47 and fs2 == 1 and fe2 == 0xb4 and fm2 == 0x68da9e and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000934]:fdiv.s t6, t5, t4, dyn<br> [0x80000938]:csrrs a2, fcsr, zero<br> [0x8000093c]:sw t6, 312(fp)<br>    |
|  66|[0x80003818]<br>0x80000000|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x1747cc and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x77e49e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000954]:fdiv.s t6, t5, t4, dyn<br> [0x80000958]:csrrs a2, fcsr, zero<br> [0x8000095c]:sw t6, 320(fp)<br>    |
|  67|[0x80003820]<br>0x80000000|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x1747cc and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x77e49e and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000974]:fdiv.s t6, t5, t4, dyn<br> [0x80000978]:csrrs a2, fcsr, zero<br> [0x8000097c]:sw t6, 328(fp)<br>    |
|  68|[0x80003828]<br>0x80000001|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x1747cc and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x77e49e and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000994]:fdiv.s t6, t5, t4, dyn<br> [0x80000998]:csrrs a2, fcsr, zero<br> [0x8000099c]:sw t6, 336(fp)<br>    |
|  69|[0x80003830]<br>0x80000000|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x1747cc and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x77e49e and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800009b4]:fdiv.s t6, t5, t4, dyn<br> [0x800009b8]:csrrs a2, fcsr, zero<br> [0x800009bc]:sw t6, 344(fp)<br>    |
|  70|[0x80003838]<br>0x80000000|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x1747cc and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x77e49e and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800009d4]:fdiv.s t6, t5, t4, dyn<br> [0x800009d8]:csrrs a2, fcsr, zero<br> [0x800009dc]:sw t6, 352(fp)<br>    |
|  71|[0x80003840]<br>0x00000000|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x0dff3c and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x39ea30 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800009f4]:fdiv.s t6, t5, t4, dyn<br> [0x800009f8]:csrrs a2, fcsr, zero<br> [0x800009fc]:sw t6, 360(fp)<br>    |
|  72|[0x80003848]<br>0x00000000|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x0dff3c and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x39ea30 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000a14]:fdiv.s t6, t5, t4, dyn<br> [0x80000a18]:csrrs a2, fcsr, zero<br> [0x80000a1c]:sw t6, 368(fp)<br>    |
|  73|[0x80003850]<br>0x00000000|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x0dff3c and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x39ea30 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000a34]:fdiv.s t6, t5, t4, dyn<br> [0x80000a38]:csrrs a2, fcsr, zero<br> [0x80000a3c]:sw t6, 376(fp)<br>    |
|  74|[0x80003858]<br>0x00000001|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x0dff3c and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x39ea30 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000a54]:fdiv.s t6, t5, t4, dyn<br> [0x80000a58]:csrrs a2, fcsr, zero<br> [0x80000a5c]:sw t6, 384(fp)<br>    |
|  75|[0x80003860]<br>0x00000000|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x0dff3c and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x39ea30 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000a74]:fdiv.s t6, t5, t4, dyn<br> [0x80000a78]:csrrs a2, fcsr, zero<br> [0x80000a7c]:sw t6, 392(fp)<br>    |
|  76|[0x80003868]<br>0x00000000|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x3c143a and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x763fd6 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000a94]:fdiv.s t6, t5, t4, dyn<br> [0x80000a98]:csrrs a2, fcsr, zero<br> [0x80000a9c]:sw t6, 400(fp)<br>    |
|  77|[0x80003870]<br>0x00000000|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x3c143a and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x763fd6 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000ab4]:fdiv.s t6, t5, t4, dyn<br> [0x80000ab8]:csrrs a2, fcsr, zero<br> [0x80000abc]:sw t6, 408(fp)<br>    |
|  78|[0x80003878]<br>0x00000000|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x3c143a and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x763fd6 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000ad4]:fdiv.s t6, t5, t4, dyn<br> [0x80000ad8]:csrrs a2, fcsr, zero<br> [0x80000adc]:sw t6, 416(fp)<br>    |
|  79|[0x80003880]<br>0x00000001|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x3c143a and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x763fd6 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000af4]:fdiv.s t6, t5, t4, dyn<br> [0x80000af8]:csrrs a2, fcsr, zero<br> [0x80000afc]:sw t6, 424(fp)<br>    |
|  80|[0x80003888]<br>0x00000000|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x3c143a and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x763fd6 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000b14]:fdiv.s t6, t5, t4, dyn<br> [0x80000b18]:csrrs a2, fcsr, zero<br> [0x80000b1c]:sw t6, 432(fp)<br>    |
|  81|[0x80003890]<br>0x00000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x07e7a0 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x31f037 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000b34]:fdiv.s t6, t5, t4, dyn<br> [0x80000b38]:csrrs a2, fcsr, zero<br> [0x80000b3c]:sw t6, 440(fp)<br>    |
|  82|[0x80003898]<br>0x00000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x07e7a0 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x31f037 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000b54]:fdiv.s t6, t5, t4, dyn<br> [0x80000b58]:csrrs a2, fcsr, zero<br> [0x80000b5c]:sw t6, 448(fp)<br>    |
|  83|[0x800038a0]<br>0x00000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x07e7a0 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x31f037 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000b74]:fdiv.s t6, t5, t4, dyn<br> [0x80000b78]:csrrs a2, fcsr, zero<br> [0x80000b7c]:sw t6, 456(fp)<br>    |
|  84|[0x800038a8]<br>0x00000001|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x07e7a0 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x31f037 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000b94]:fdiv.s t6, t5, t4, dyn<br> [0x80000b98]:csrrs a2, fcsr, zero<br> [0x80000b9c]:sw t6, 464(fp)<br>    |
|  85|[0x800038b0]<br>0x00000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x07e7a0 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x31f037 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000bb4]:fdiv.s t6, t5, t4, dyn<br> [0x80000bb8]:csrrs a2, fcsr, zero<br> [0x80000bbc]:sw t6, 472(fp)<br>    |
|  86|[0x800038b8]<br>0x00000000|- fs1 == 0 and fe1 == 0x1a and fm1 == 0x506fba and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x087397 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000bd4]:fdiv.s t6, t5, t4, dyn<br> [0x80000bd8]:csrrs a2, fcsr, zero<br> [0x80000bdc]:sw t6, 480(fp)<br>    |
|  87|[0x800038c0]<br>0x00000000|- fs1 == 0 and fe1 == 0x1a and fm1 == 0x506fba and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x087397 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000bf4]:fdiv.s t6, t5, t4, dyn<br> [0x80000bf8]:csrrs a2, fcsr, zero<br> [0x80000bfc]:sw t6, 488(fp)<br>    |
|  88|[0x800038c8]<br>0x00000000|- fs1 == 0 and fe1 == 0x1a and fm1 == 0x506fba and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x087397 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000c14]:fdiv.s t6, t5, t4, dyn<br> [0x80000c18]:csrrs a2, fcsr, zero<br> [0x80000c1c]:sw t6, 496(fp)<br>    |
|  89|[0x800038d0]<br>0x00000001|- fs1 == 0 and fe1 == 0x1a and fm1 == 0x506fba and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x087397 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000c34]:fdiv.s t6, t5, t4, dyn<br> [0x80000c38]:csrrs a2, fcsr, zero<br> [0x80000c3c]:sw t6, 504(fp)<br>    |
|  90|[0x800038d8]<br>0x00000000|- fs1 == 0 and fe1 == 0x1a and fm1 == 0x506fba and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x087397 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000c54]:fdiv.s t6, t5, t4, dyn<br> [0x80000c58]:csrrs a2, fcsr, zero<br> [0x80000c5c]:sw t6, 512(fp)<br>    |
|  91|[0x800038e0]<br>0x00000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x210ca3 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x52dc1c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000c74]:fdiv.s t6, t5, t4, dyn<br> [0x80000c78]:csrrs a2, fcsr, zero<br> [0x80000c7c]:sw t6, 520(fp)<br>    |
|  92|[0x800038e8]<br>0x00000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x210ca3 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x52dc1c and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000c94]:fdiv.s t6, t5, t4, dyn<br> [0x80000c98]:csrrs a2, fcsr, zero<br> [0x80000c9c]:sw t6, 528(fp)<br>    |
|  93|[0x800038f0]<br>0x00000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x210ca3 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x52dc1c and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000cb4]:fdiv.s t6, t5, t4, dyn<br> [0x80000cb8]:csrrs a2, fcsr, zero<br> [0x80000cbc]:sw t6, 536(fp)<br>    |
|  94|[0x800038f8]<br>0x00000001|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x210ca3 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x52dc1c and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000cd4]:fdiv.s t6, t5, t4, dyn<br> [0x80000cd8]:csrrs a2, fcsr, zero<br> [0x80000cdc]:sw t6, 544(fp)<br>    |
|  95|[0x80003900]<br>0x00000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x210ca3 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x52dc1c and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000cf4]:fdiv.s t6, t5, t4, dyn<br> [0x80000cf8]:csrrs a2, fcsr, zero<br> [0x80000cfc]:sw t6, 552(fp)<br>    |
|  96|[0x80003908]<br>0x00000000|- fs1 == 0 and fe1 == 0x18 and fm1 == 0x1cec94 and fs2 == 0 and fe2 == 0xb0 and fm2 == 0x4d756d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000d14]:fdiv.s t6, t5, t4, dyn<br> [0x80000d18]:csrrs a2, fcsr, zero<br> [0x80000d1c]:sw t6, 560(fp)<br>    |
|  97|[0x80003910]<br>0x00000000|- fs1 == 0 and fe1 == 0x18 and fm1 == 0x1cec94 and fs2 == 0 and fe2 == 0xb0 and fm2 == 0x4d756d and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000d34]:fdiv.s t6, t5, t4, dyn<br> [0x80000d38]:csrrs a2, fcsr, zero<br> [0x80000d3c]:sw t6, 568(fp)<br>    |
|  98|[0x80003918]<br>0x00000000|- fs1 == 0 and fe1 == 0x18 and fm1 == 0x1cec94 and fs2 == 0 and fe2 == 0xb0 and fm2 == 0x4d756d and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000d54]:fdiv.s t6, t5, t4, dyn<br> [0x80000d58]:csrrs a2, fcsr, zero<br> [0x80000d5c]:sw t6, 576(fp)<br>    |
|  99|[0x80003920]<br>0x00000001|- fs1 == 0 and fe1 == 0x18 and fm1 == 0x1cec94 and fs2 == 0 and fe2 == 0xb0 and fm2 == 0x4d756d and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000d74]:fdiv.s t6, t5, t4, dyn<br> [0x80000d78]:csrrs a2, fcsr, zero<br> [0x80000d7c]:sw t6, 584(fp)<br>    |
| 100|[0x80003928]<br>0x00000000|- fs1 == 0 and fe1 == 0x18 and fm1 == 0x1cec94 and fs2 == 0 and fe2 == 0xb0 and fm2 == 0x4d756d and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000d94]:fdiv.s t6, t5, t4, dyn<br> [0x80000d98]:csrrs a2, fcsr, zero<br> [0x80000d9c]:sw t6, 592(fp)<br>    |
| 101|[0x80003930]<br>0x00000000|- fs1 == 0 and fe1 == 0x1a and fm1 == 0x6a2294 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x194664 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000db4]:fdiv.s t6, t5, t4, dyn<br> [0x80000db8]:csrrs a2, fcsr, zero<br> [0x80000dbc]:sw t6, 600(fp)<br>    |
| 102|[0x80003938]<br>0x00000000|- fs1 == 0 and fe1 == 0x1a and fm1 == 0x6a2294 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x194664 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000dd4]:fdiv.s t6, t5, t4, dyn<br> [0x80000dd8]:csrrs a2, fcsr, zero<br> [0x80000ddc]:sw t6, 608(fp)<br>    |
| 103|[0x80003940]<br>0x00000000|- fs1 == 0 and fe1 == 0x1a and fm1 == 0x6a2294 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x194664 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000df4]:fdiv.s t6, t5, t4, dyn<br> [0x80000df8]:csrrs a2, fcsr, zero<br> [0x80000dfc]:sw t6, 616(fp)<br>    |
| 104|[0x80003948]<br>0x00000001|- fs1 == 0 and fe1 == 0x1a and fm1 == 0x6a2294 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x194664 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000e14]:fdiv.s t6, t5, t4, dyn<br> [0x80000e18]:csrrs a2, fcsr, zero<br> [0x80000e1c]:sw t6, 624(fp)<br>    |
| 105|[0x80003950]<br>0x00000000|- fs1 == 0 and fe1 == 0x1a and fm1 == 0x6a2294 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x194664 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000e34]:fdiv.s t6, t5, t4, dyn<br> [0x80000e38]:csrrs a2, fcsr, zero<br> [0x80000e3c]:sw t6, 632(fp)<br>    |
| 106|[0x80003958]<br>0x00000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x17f238 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x7221c9 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000e54]:fdiv.s t6, t5, t4, dyn<br> [0x80000e58]:csrrs a2, fcsr, zero<br> [0x80000e5c]:sw t6, 640(fp)<br>    |
| 107|[0x80003960]<br>0x00000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x17f238 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x7221c9 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000e74]:fdiv.s t6, t5, t4, dyn<br> [0x80000e78]:csrrs a2, fcsr, zero<br> [0x80000e7c]:sw t6, 648(fp)<br>    |
| 108|[0x80003968]<br>0x00000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x17f238 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x7221c9 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000e94]:fdiv.s t6, t5, t4, dyn<br> [0x80000e98]:csrrs a2, fcsr, zero<br> [0x80000e9c]:sw t6, 656(fp)<br>    |
| 109|[0x80003970]<br>0x00000001|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x17f238 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x7221c9 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000eb4]:fdiv.s t6, t5, t4, dyn<br> [0x80000eb8]:csrrs a2, fcsr, zero<br> [0x80000ebc]:sw t6, 664(fp)<br>    |
| 110|[0x80003978]<br>0x00000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x17f238 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x7221c9 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000ed4]:fdiv.s t6, t5, t4, dyn<br> [0x80000ed8]:csrrs a2, fcsr, zero<br> [0x80000edc]:sw t6, 672(fp)<br>    |
| 111|[0x80003980]<br>0x00000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x06ae55 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x569e87 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000ef4]:fdiv.s t6, t5, t4, dyn<br> [0x80000ef8]:csrrs a2, fcsr, zero<br> [0x80000efc]:sw t6, 680(fp)<br>    |
| 112|[0x80003988]<br>0x00000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x06ae55 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x569e87 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000f14]:fdiv.s t6, t5, t4, dyn<br> [0x80000f18]:csrrs a2, fcsr, zero<br> [0x80000f1c]:sw t6, 688(fp)<br>    |
| 113|[0x80003990]<br>0x00000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x06ae55 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x569e87 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000f34]:fdiv.s t6, t5, t4, dyn<br> [0x80000f38]:csrrs a2, fcsr, zero<br> [0x80000f3c]:sw t6, 696(fp)<br>    |
| 114|[0x80003998]<br>0x00000001|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x06ae55 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x569e87 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000f54]:fdiv.s t6, t5, t4, dyn<br> [0x80000f58]:csrrs a2, fcsr, zero<br> [0x80000f5c]:sw t6, 704(fp)<br>    |
| 115|[0x800039a0]<br>0x00000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x06ae55 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x569e87 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000f74]:fdiv.s t6, t5, t4, dyn<br> [0x80000f78]:csrrs a2, fcsr, zero<br> [0x80000f7c]:sw t6, 712(fp)<br>    |
| 116|[0x800039a8]<br>0x00000000|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x44e5bd and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x1ce1ba and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000f94]:fdiv.s t6, t5, t4, dyn<br> [0x80000f98]:csrrs a2, fcsr, zero<br> [0x80000f9c]:sw t6, 720(fp)<br>    |
| 117|[0x800039b0]<br>0x00000000|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x44e5bd and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x1ce1ba and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000fb4]:fdiv.s t6, t5, t4, dyn<br> [0x80000fb8]:csrrs a2, fcsr, zero<br> [0x80000fbc]:sw t6, 728(fp)<br>    |
| 118|[0x800039b8]<br>0x00000000|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x44e5bd and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x1ce1ba and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000fd4]:fdiv.s t6, t5, t4, dyn<br> [0x80000fd8]:csrrs a2, fcsr, zero<br> [0x80000fdc]:sw t6, 736(fp)<br>    |
| 119|[0x800039c0]<br>0x00000001|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x44e5bd and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x1ce1ba and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000ff4]:fdiv.s t6, t5, t4, dyn<br> [0x80000ff8]:csrrs a2, fcsr, zero<br> [0x80000ffc]:sw t6, 744(fp)<br>    |
| 120|[0x800039c8]<br>0x00000000|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x44e5bd and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x1ce1ba and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80001014]:fdiv.s t6, t5, t4, dyn<br> [0x80001018]:csrrs a2, fcsr, zero<br> [0x8000101c]:sw t6, 752(fp)<br>    |
| 121|[0x800039d0]<br>0x00000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x0f30be and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x642de8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001034]:fdiv.s t6, t5, t4, dyn<br> [0x80001038]:csrrs a2, fcsr, zero<br> [0x8000103c]:sw t6, 760(fp)<br>    |
| 122|[0x800039d8]<br>0x00000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x0f30be and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x642de8 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80001054]:fdiv.s t6, t5, t4, dyn<br> [0x80001058]:csrrs a2, fcsr, zero<br> [0x8000105c]:sw t6, 768(fp)<br>    |
| 123|[0x800039e0]<br>0x00000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x0f30be and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x642de8 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80001074]:fdiv.s t6, t5, t4, dyn<br> [0x80001078]:csrrs a2, fcsr, zero<br> [0x8000107c]:sw t6, 776(fp)<br>    |
| 124|[0x800039e8]<br>0x00000001|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x0f30be and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x642de8 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80001094]:fdiv.s t6, t5, t4, dyn<br> [0x80001098]:csrrs a2, fcsr, zero<br> [0x8000109c]:sw t6, 784(fp)<br>    |
| 125|[0x800039f0]<br>0x00000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x0f30be and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x642de8 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800010b4]:fdiv.s t6, t5, t4, dyn<br> [0x800010b8]:csrrs a2, fcsr, zero<br> [0x800010bc]:sw t6, 792(fp)<br>    |
| 126|[0x800039f8]<br>0x00000000|- fs1 == 0 and fe1 == 0x1a and fm1 == 0x23f956 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x02a63c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800010d4]:fdiv.s t6, t5, t4, dyn<br> [0x800010d8]:csrrs a2, fcsr, zero<br> [0x800010dc]:sw t6, 800(fp)<br>    |
| 127|[0x80003a00]<br>0x00000000|- fs1 == 0 and fe1 == 0x1a and fm1 == 0x23f956 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x02a63c and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800010f4]:fdiv.s t6, t5, t4, dyn<br> [0x800010f8]:csrrs a2, fcsr, zero<br> [0x800010fc]:sw t6, 808(fp)<br>    |
| 128|[0x80003a08]<br>0x00000000|- fs1 == 0 and fe1 == 0x1a and fm1 == 0x23f956 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x02a63c and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80001114]:fdiv.s t6, t5, t4, dyn<br> [0x80001118]:csrrs a2, fcsr, zero<br> [0x8000111c]:sw t6, 816(fp)<br>    |
| 129|[0x80003a10]<br>0x00000001|- fs1 == 0 and fe1 == 0x1a and fm1 == 0x23f956 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x02a63c and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80001134]:fdiv.s t6, t5, t4, dyn<br> [0x80001138]:csrrs a2, fcsr, zero<br> [0x8000113c]:sw t6, 824(fp)<br>    |
| 130|[0x80003a18]<br>0x00000000|- fs1 == 0 and fe1 == 0x1a and fm1 == 0x23f956 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x02a63c and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80001154]:fdiv.s t6, t5, t4, dyn<br> [0x80001158]:csrrs a2, fcsr, zero<br> [0x8000115c]:sw t6, 832(fp)<br>    |
| 131|[0x80003a20]<br>0x00000000|- fs1 == 0 and fe1 == 0x16 and fm1 == 0x3344b3 and fs2 == 0 and fe2 == 0xae and fm2 == 0x0ed5e0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001174]:fdiv.s t6, t5, t4, dyn<br> [0x80001178]:csrrs a2, fcsr, zero<br> [0x8000117c]:sw t6, 840(fp)<br>    |
| 132|[0x80003a28]<br>0x00000000|- fs1 == 0 and fe1 == 0x16 and fm1 == 0x3344b3 and fs2 == 0 and fe2 == 0xae and fm2 == 0x0ed5e0 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80001194]:fdiv.s t6, t5, t4, dyn<br> [0x80001198]:csrrs a2, fcsr, zero<br> [0x8000119c]:sw t6, 848(fp)<br>    |
| 133|[0x80003a30]<br>0x00000000|- fs1 == 0 and fe1 == 0x16 and fm1 == 0x3344b3 and fs2 == 0 and fe2 == 0xae and fm2 == 0x0ed5e0 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800011b4]:fdiv.s t6, t5, t4, dyn<br> [0x800011b8]:csrrs a2, fcsr, zero<br> [0x800011bc]:sw t6, 856(fp)<br>    |
| 134|[0x80003a38]<br>0x00000001|- fs1 == 0 and fe1 == 0x16 and fm1 == 0x3344b3 and fs2 == 0 and fe2 == 0xae and fm2 == 0x0ed5e0 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800011d4]:fdiv.s t6, t5, t4, dyn<br> [0x800011d8]:csrrs a2, fcsr, zero<br> [0x800011dc]:sw t6, 864(fp)<br>    |
| 135|[0x80003a40]<br>0x00000000|- fs1 == 0 and fe1 == 0x16 and fm1 == 0x3344b3 and fs2 == 0 and fe2 == 0xae and fm2 == 0x0ed5e0 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800011f4]:fdiv.s t6, t5, t4, dyn<br> [0x800011f8]:csrrs a2, fcsr, zero<br> [0x800011fc]:sw t6, 872(fp)<br>    |
| 136|[0x80003a48]<br>0x00000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x0162e0 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x4e2e8e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001214]:fdiv.s t6, t5, t4, dyn<br> [0x80001218]:csrrs a2, fcsr, zero<br> [0x8000121c]:sw t6, 880(fp)<br>    |
| 137|[0x80003a50]<br>0x00000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x0162e0 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x4e2e8e and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80001234]:fdiv.s t6, t5, t4, dyn<br> [0x80001238]:csrrs a2, fcsr, zero<br> [0x8000123c]:sw t6, 888(fp)<br>    |
| 138|[0x80003a58]<br>0x00000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x0162e0 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x4e2e8e and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80001254]:fdiv.s t6, t5, t4, dyn<br> [0x80001258]:csrrs a2, fcsr, zero<br> [0x8000125c]:sw t6, 896(fp)<br>    |
| 139|[0x80003a60]<br>0x00000001|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x0162e0 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x4e2e8e and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80001274]:fdiv.s t6, t5, t4, dyn<br> [0x80001278]:csrrs a2, fcsr, zero<br> [0x8000127c]:sw t6, 904(fp)<br>    |
| 140|[0x80003a68]<br>0x00000000|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x0162e0 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x4e2e8e and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80001294]:fdiv.s t6, t5, t4, dyn<br> [0x80001298]:csrrs a2, fcsr, zero<br> [0x8000129c]:sw t6, 912(fp)<br>    |
| 141|[0x80003a80]<br>0x80000000|- fs1 == 0 and fe1 == 0x1a and fm1 == 0x20c723 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x2c5b71 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800012f4]:fdiv.s t6, t5, t4, dyn<br> [0x800012f8]:csrrs a2, fcsr, zero<br> [0x800012fc]:sw t6, 936(fp)<br>    |
| 142|[0x80003a88]<br>0x80000000|- fs1 == 0 and fe1 == 0x1a and fm1 == 0x20c723 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x2c5b71 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80001314]:fdiv.s t6, t5, t4, dyn<br> [0x80001318]:csrrs a2, fcsr, zero<br> [0x8000131c]:sw t6, 944(fp)<br>    |
| 143|[0x80003a90]<br>0x80000000|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x1479a4 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x1f2b1f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001334]:fdiv.s t6, t5, t4, dyn<br> [0x80001338]:csrrs a2, fcsr, zero<br> [0x8000133c]:sw t6, 952(fp)<br>    |
| 144|[0x80003a98]<br>0x80000001|- fs1 == 0 and fe1 == 0x19 and fm1 == 0x1479a4 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x1f2b1f and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80001354]:fdiv.s t6, t5, t4, dyn<br> [0x80001358]:csrrs a2, fcsr, zero<br> [0x8000135c]:sw t6, 960(fp)<br>    |
