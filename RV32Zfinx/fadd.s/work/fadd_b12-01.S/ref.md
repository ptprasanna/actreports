
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000850')]      |
| SIG_REGION                | [('0x80002310', '0x800024e0', '116 words')]      |
| COV_LABELS                | fadd_b12      |
| TEST_NAME                 | /home/reg/work/zfinx/fadd.s/work/fadd_b12-01.S/ref.S    |
| Total Number of coverpoints| 152     |
| Total Coverpoints Hit     | 152      |
| Total Signature Updates   | 114      |
| STAT1                     | 54      |
| STAT2                     | 3      |
| STAT3                     | 0     |
| STAT4                     | 57     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000794]:fadd.s t6, t5, t4, dyn
      [0x80000798]:csrrs a3, fcsr, zero
      [0x8000079c]:sw t6, 216(s1)
 -- Signature Address: 0x800024a8 Data: 0x7E46D91A
 -- Redundant Coverpoints hit by the op
      - mnemonic : fadd.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 1 and fe1 == 0xfd and fm1 == 0x217fdd and fs2 == 0 and fe2 == 0xfe and fm2 == 0x027635 and  fcsr == 0x0 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x800007b4]:fadd.s t6, t5, t4, dyn
      [0x800007b8]:csrrs a3, fcsr, zero
      [0x800007bc]:sw t6, 224(s1)
 -- Signature Address: 0x800024b0 Data: 0x7E9B7EA3
 -- Redundant Coverpoints hit by the op
      - mnemonic : fadd.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 1 and fe1 == 0xfd and fm1 == 0x445459 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x2fe97e and  fcsr == 0x0 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000834]:fadd.s t6, t5, t4, dyn
      [0x80000838]:csrrs a3, fcsr, zero
      [0x8000083c]:sw t6, 256(s1)
 -- Signature Address: 0x800024d0 Data: 0x7E8DE2D0
 -- Redundant Coverpoints hit by the op
      - mnemonic : fadd.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 1 and fe1 == 0xfe and fm1 == 0x390e97 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fadd.s', 'rs1 : x30', 'rs2 : x30', 'rd : x31', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x80000124]:fadd.s t6, t5, t5, dyn
	-[0x80000128]:csrrs tp, fcsr, zero
	-[0x8000012c]:sw t6, 0(ra)
Current Store : [0x80000130] : sw tp, 4(ra) -- Store: [0x80002314]:0x00000005




Last Coverpoint : ['rs1 : x31', 'rs2 : x29', 'rd : x29', 'rs2 == rd != rs1', 'fs1 == 1 and fe1 == 0xfd and fm1 == 0x217fdd and fs2 == 0 and fe2 == 0xfe and fm2 == 0x027635 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000144]:fadd.s t4, t6, t4, dyn
	-[0x80000148]:csrrs tp, fcsr, zero
	-[0x8000014c]:sw t4, 8(ra)
Current Store : [0x80000150] : sw tp, 12(ra) -- Store: [0x8000231c]:0x00000000




Last Coverpoint : ['rs1 : x28', 'rs2 : x31', 'rd : x28', 'rs1 == rd != rs2', 'fs1 == 1 and fe1 == 0xfd and fm1 == 0x445459 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x2fe97e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000164]:fadd.s t3, t3, t6, dyn
	-[0x80000168]:csrrs tp, fcsr, zero
	-[0x8000016c]:sw t3, 16(ra)
Current Store : [0x80000170] : sw tp, 20(ra) -- Store: [0x80002324]:0x00000000




Last Coverpoint : ['rs1 : x27', 'rs2 : x27', 'rd : x27', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x80000184]:fadd.s s11, s11, s11, dyn
	-[0x80000188]:csrrs tp, fcsr, zero
	-[0x8000018c]:sw s11, 24(ra)
Current Store : [0x80000190] : sw tp, 28(ra) -- Store: [0x8000232c]:0x00000005




Last Coverpoint : ['rs1 : x29', 'rs2 : x28', 'rd : x30', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 1 and fe1 == 0xfd and fm1 == 0x370362 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x4b8415 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001a4]:fadd.s t5, t4, t3, dyn
	-[0x800001a8]:csrrs tp, fcsr, zero
	-[0x800001ac]:sw t5, 32(ra)
Current Store : [0x800001b0] : sw tp, 36(ra) -- Store: [0x80002334]:0x00000000




Last Coverpoint : ['rs1 : x25', 'rs2 : x24', 'rd : x26', 'fs1 == 1 and fe1 == 0xfe and fm1 == 0x2e5b90 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001c4]:fadd.s s10, s9, s8, dyn
	-[0x800001c8]:csrrs tp, fcsr, zero
	-[0x800001cc]:sw s10, 40(ra)
Current Store : [0x800001d0] : sw tp, 44(ra) -- Store: [0x8000233c]:0x00000000




Last Coverpoint : ['rs1 : x24', 'rs2 : x26', 'rd : x25', 'fs1 == 1 and fe1 == 0xfc and fm1 == 0x587392 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x473a2e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001e4]:fadd.s s9, s8, s10, dyn
	-[0x800001e8]:csrrs tp, fcsr, zero
	-[0x800001ec]:sw s9, 48(ra)
Current Store : [0x800001f0] : sw tp, 52(ra) -- Store: [0x80002344]:0x00000001




Last Coverpoint : ['rs1 : x26', 'rs2 : x25', 'rd : x24', 'fs1 == 1 and fe1 == 0xfd and fm1 == 0x6d7424 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x26d2f5 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000204]:fadd.s s8, s10, s9, dyn
	-[0x80000208]:csrrs tp, fcsr, zero
	-[0x8000020c]:sw s8, 56(ra)
Current Store : [0x80000210] : sw tp, 60(ra) -- Store: [0x8000234c]:0x00000000




Last Coverpoint : ['rs1 : x22', 'rs2 : x21', 'rd : x23', 'fs1 == 1 and fe1 == 0xfe and fm1 == 0x2eabd8 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000224]:fadd.s s7, s6, s5, dyn
	-[0x80000228]:csrrs tp, fcsr, zero
	-[0x8000022c]:sw s7, 64(ra)
Current Store : [0x80000230] : sw tp, 68(ra) -- Store: [0x80002354]:0x00000000




Last Coverpoint : ['rs1 : x21', 'rs2 : x23', 'rd : x22', 'fs1 == 1 and fe1 == 0xfe and fm1 == 0x1b8fcb and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000244]:fadd.s s6, s5, s7, dyn
	-[0x80000248]:csrrs tp, fcsr, zero
	-[0x8000024c]:sw s6, 72(ra)
Current Store : [0x80000250] : sw tp, 76(ra) -- Store: [0x8000235c]:0x00000000




Last Coverpoint : ['rs1 : x23', 'rs2 : x22', 'rd : x21', 'fs1 == 1 and fe1 == 0xfc and fm1 == 0x6e317d and fs2 == 0 and fe2 == 0xfe and fm2 == 0x47ad0f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000264]:fadd.s s5, s7, s6, dyn
	-[0x80000268]:csrrs tp, fcsr, zero
	-[0x8000026c]:sw s5, 80(ra)
Current Store : [0x80000270] : sw tp, 84(ra) -- Store: [0x80002364]:0x00000001




Last Coverpoint : ['rs1 : x19', 'rs2 : x18', 'rd : x20', 'fs1 == 1 and fe1 == 0xfe and fm1 == 0x2c93b2 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000284]:fadd.s s4, s3, s2, dyn
	-[0x80000288]:csrrs tp, fcsr, zero
	-[0x8000028c]:sw s4, 88(ra)
Current Store : [0x80000290] : sw tp, 92(ra) -- Store: [0x8000236c]:0x00000000




Last Coverpoint : ['rs1 : x18', 'rs2 : x20', 'rd : x19', 'fs1 == 1 and fe1 == 0xfd and fm1 == 0x354d84 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x5bf8d8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002a4]:fadd.s s3, s2, s4, dyn
	-[0x800002a8]:csrrs tp, fcsr, zero
	-[0x800002ac]:sw s3, 96(ra)
Current Store : [0x800002b0] : sw tp, 100(ra) -- Store: [0x80002374]:0x00000000




Last Coverpoint : ['rs1 : x20', 'rs2 : x19', 'rd : x18', 'fs1 == 1 and fe1 == 0xfd and fm1 == 0x26b8d3 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x48a6ba and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002c4]:fadd.s s2, s4, s3, dyn
	-[0x800002c8]:csrrs tp, fcsr, zero
	-[0x800002cc]:sw s2, 104(ra)
Current Store : [0x800002d0] : sw tp, 108(ra) -- Store: [0x8000237c]:0x00000000




Last Coverpoint : ['rs1 : x16', 'rs2 : x15', 'rd : x17', 'fs1 == 1 and fe1 == 0xfe and fm1 == 0x2f4c51 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002e4]:fadd.s a7, a6, a5, dyn
	-[0x800002e8]:csrrs tp, fcsr, zero
	-[0x800002ec]:sw a7, 112(ra)
Current Store : [0x800002f0] : sw tp, 116(ra) -- Store: [0x80002384]:0x00000000




Last Coverpoint : ['rs1 : x15', 'rs2 : x17', 'rd : x16', 'fs1 == 1 and fe1 == 0xfd and fm1 == 0x372bf7 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000304]:fadd.s a6, a5, a7, dyn
	-[0x80000308]:csrrs tp, fcsr, zero
	-[0x8000030c]:sw a6, 120(ra)
Current Store : [0x80000310] : sw tp, 124(ra) -- Store: [0x8000238c]:0x00000001




Last Coverpoint : ['rs1 : x17', 'rs2 : x16', 'rd : x15', 'fs1 == 1 and fe1 == 0xfc and fm1 == 0x480ede and fs2 == 0 and fe2 == 0xfe and fm2 == 0x4c9471 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000324]:fadd.s a5, a7, a6, dyn
	-[0x80000328]:csrrs tp, fcsr, zero
	-[0x8000032c]:sw a5, 128(ra)
Current Store : [0x80000330] : sw tp, 132(ra) -- Store: [0x80002394]:0x00000001




Last Coverpoint : ['rs1 : x13', 'rs2 : x12', 'rd : x14', 'fs1 == 1 and fe1 == 0xfd and fm1 == 0x52b355 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x5817b0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000344]:fadd.s a4, a3, a2, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:sw a4, 136(ra)
Current Store : [0x80000350] : sw tp, 140(ra) -- Store: [0x8000239c]:0x00000000




Last Coverpoint : ['rs1 : x12', 'rs2 : x14', 'rd : x13', 'fs1 == 1 and fe1 == 0xfe and fm1 == 0x0a2eec and fs2 == 0 and fe2 == 0xfe and fm2 == 0x56c1e5 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000364]:fadd.s a3, a2, a4, dyn
	-[0x80000368]:csrrs tp, fcsr, zero
	-[0x8000036c]:sw a3, 144(ra)
Current Store : [0x80000370] : sw tp, 148(ra) -- Store: [0x800023a4]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rs2 : x13', 'rd : x12', 'fs1 == 1 and fe1 == 0xfe and fm1 == 0x1e5ec7 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000384]:fadd.s a2, a4, a3, dyn
	-[0x80000388]:csrrs tp, fcsr, zero
	-[0x8000038c]:sw a2, 152(ra)
Current Store : [0x80000390] : sw tp, 156(ra) -- Store: [0x800023ac]:0x00000000




Last Coverpoint : ['rs1 : x10', 'rs2 : x9', 'rd : x11', 'fs1 == 1 and fe1 == 0xf4 and fm1 == 0x60affa and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3df905 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003a4]:fadd.s a1, a0, s1, dyn
	-[0x800003a8]:csrrs tp, fcsr, zero
	-[0x800003ac]:sw a1, 160(ra)
Current Store : [0x800003b0] : sw tp, 164(ra) -- Store: [0x800023b4]:0x00000001




Last Coverpoint : ['rs1 : x9', 'rs2 : x11', 'rd : x10', 'fs1 == 1 and fe1 == 0xfe and fm1 == 0x269468 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x55691d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003cc]:fadd.s a0, s1, a1, dyn
	-[0x800003d0]:csrrs a3, fcsr, zero
	-[0x800003d4]:sw a0, 168(ra)
Current Store : [0x800003d8] : sw a3, 172(ra) -- Store: [0x800023bc]:0x00000000




Last Coverpoint : ['rs1 : x11', 'rs2 : x10', 'rd : x9', 'fs1 == 1 and fe1 == 0xfc and fm1 == 0x79c1c6 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x6591d8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003ec]:fadd.s s1, a1, a0, dyn
	-[0x800003f0]:csrrs a3, fcsr, zero
	-[0x800003f4]:sw s1, 176(ra)
Current Store : [0x800003f8] : sw a3, 180(ra) -- Store: [0x800023c4]:0x00000001




Last Coverpoint : ['rs1 : x7', 'rs2 : x6', 'rd : x8', 'fs1 == 1 and fe1 == 0xfc and fm1 == 0x12bd51 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x11a59d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000040c]:fadd.s fp, t2, t1, dyn
	-[0x80000410]:csrrs a3, fcsr, zero
	-[0x80000414]:sw fp, 184(ra)
Current Store : [0x80000418] : sw a3, 188(ra) -- Store: [0x800023cc]:0x00000001




Last Coverpoint : ['rs1 : x6', 'rs2 : x8', 'rd : x7', 'fs1 == 1 and fe1 == 0xfc and fm1 == 0x3741cc and fs2 == 0 and fe2 == 0xfe and fm2 == 0x6794fc and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000434]:fadd.s t2, t1, fp, dyn
	-[0x80000438]:csrrs a3, fcsr, zero
	-[0x8000043c]:sw t2, 0(s1)
Current Store : [0x80000440] : sw a3, 4(s1) -- Store: [0x800023d4]:0x00000000




Last Coverpoint : ['rs1 : x8', 'rs2 : x7', 'rd : x6', 'fs1 == 1 and fe1 == 0xfe and fm1 == 0x1a35e0 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x703879 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000454]:fadd.s t1, fp, t2, dyn
	-[0x80000458]:csrrs a3, fcsr, zero
	-[0x8000045c]:sw t1, 8(s1)
Current Store : [0x80000460] : sw a3, 12(s1) -- Store: [0x800023dc]:0x00000000




Last Coverpoint : ['rs1 : x4', 'rs2 : x3', 'rd : x5', 'fs1 == 1 and fe1 == 0xfa and fm1 == 0x772129 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x19caca and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000474]:fadd.s t0, tp, gp, dyn
	-[0x80000478]:csrrs a3, fcsr, zero
	-[0x8000047c]:sw t0, 16(s1)
Current Store : [0x80000480] : sw a3, 20(s1) -- Store: [0x800023e4]:0x00000001




Last Coverpoint : ['rs1 : x3', 'rs2 : x5', 'rd : x4', 'fs1 == 1 and fe1 == 0xfd and fm1 == 0x430c98 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000494]:fadd.s tp, gp, t0, dyn
	-[0x80000498]:csrrs a3, fcsr, zero
	-[0x8000049c]:sw tp, 24(s1)
Current Store : [0x800004a0] : sw a3, 28(s1) -- Store: [0x800023ec]:0x00000000




Last Coverpoint : ['rs1 : x5', 'rs2 : x4', 'rd : x3', 'fs1 == 1 and fe1 == 0xfb and fm1 == 0x278349 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x7d9098 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004b4]:fadd.s gp, t0, tp, dyn
	-[0x800004b8]:csrrs a3, fcsr, zero
	-[0x800004bc]:sw gp, 32(s1)
Current Store : [0x800004c0] : sw a3, 36(s1) -- Store: [0x800023f4]:0x00000001




Last Coverpoint : ['rs1 : x1', 'rs2 : x0', 'rd : x2']
Last Code Sequence : 
	-[0x800004d4]:fadd.s sp, ra, zero, dyn
	-[0x800004d8]:csrrs a3, fcsr, zero
	-[0x800004dc]:sw sp, 40(s1)
Current Store : [0x800004e0] : sw a3, 44(s1) -- Store: [0x800023fc]:0x00000000




Last Coverpoint : ['rs1 : x0', 'rs2 : x2', 'rd : x1']
Last Code Sequence : 
	-[0x800004f4]:fadd.s ra, zero, sp, dyn
	-[0x800004f8]:csrrs a3, fcsr, zero
	-[0x800004fc]:sw ra, 48(s1)
Current Store : [0x80000500] : sw a3, 52(s1) -- Store: [0x80002404]:0x00000000




Last Coverpoint : ['rs1 : x2', 'rs2 : x1', 'rd : x0', 'fs1 == 1 and fe1 == 0xfe and fm1 == 0x390e97 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000514]:fadd.s zero, sp, ra, dyn
	-[0x80000518]:csrrs a3, fcsr, zero
	-[0x8000051c]:sw zero, 56(s1)
Current Store : [0x80000520] : sw a3, 60(s1) -- Store: [0x8000240c]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x61a51b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x4642a7 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000534]:fadd.s t6, t5, t4, dyn
	-[0x80000538]:csrrs a3, fcsr, zero
	-[0x8000053c]:sw t6, 64(s1)
Current Store : [0x80000540] : sw a3, 68(s1) -- Store: [0x80002414]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x64f961 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x3c6359 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000554]:fadd.s t6, t5, t4, dyn
	-[0x80000558]:csrrs a3, fcsr, zero
	-[0x8000055c]:sw t6, 72(s1)
Current Store : [0x80000560] : sw a3, 76(s1) -- Store: [0x8000241c]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x5d0ccb and fs2 == 0 and fe2 == 0xfe and fm2 == 0x2644ac and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000574]:fadd.s t6, t5, t4, dyn
	-[0x80000578]:csrrs a3, fcsr, zero
	-[0x8000057c]:sw t6, 80(s1)
Current Store : [0x80000580] : sw a3, 84(s1) -- Store: [0x80002424]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfc and fm1 == 0x1173d9 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x106e2e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000594]:fadd.s t6, t5, t4, dyn
	-[0x80000598]:csrrs a3, fcsr, zero
	-[0x8000059c]:sw t6, 88(s1)
Current Store : [0x800005a0] : sw a3, 92(s1) -- Store: [0x8000242c]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x6b4e0d and fs2 == 0 and fe2 == 0xfe and fm2 == 0x73bb25 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005b4]:fadd.s t6, t5, t4, dyn
	-[0x800005b8]:csrrs a3, fcsr, zero
	-[0x800005bc]:sw t6, 96(s1)
Current Store : [0x800005c0] : sw a3, 100(s1) -- Store: [0x80002434]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x36fce6 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x597afe and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005d4]:fadd.s t6, t5, t4, dyn
	-[0x800005d8]:csrrs a3, fcsr, zero
	-[0x800005dc]:sw t6, 104(s1)
Current Store : [0x800005e0] : sw a3, 108(s1) -- Store: [0x8000243c]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x1175bf and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005f4]:fadd.s t6, t5, t4, dyn
	-[0x800005f8]:csrrs a3, fcsr, zero
	-[0x800005fc]:sw t6, 112(s1)
Current Store : [0x80000600] : sw a3, 116(s1) -- Store: [0x80002444]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x314a05 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000614]:fadd.s t6, t5, t4, dyn
	-[0x80000618]:csrrs a3, fcsr, zero
	-[0x8000061c]:sw t6, 120(s1)
Current Store : [0x80000620] : sw a3, 124(s1) -- Store: [0x8000244c]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x2c7300 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000634]:fadd.s t6, t5, t4, dyn
	-[0x80000638]:csrrs a3, fcsr, zero
	-[0x8000063c]:sw t6, 128(s1)
Current Store : [0x80000640] : sw a3, 132(s1) -- Store: [0x80002454]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x4f9722 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x32ec8c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000654]:fadd.s t6, t5, t4, dyn
	-[0x80000658]:csrrs a3, fcsr, zero
	-[0x8000065c]:sw t6, 136(s1)
Current Store : [0x80000660] : sw a3, 140(s1) -- Store: [0x8000245c]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x076a16 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x3c560e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000674]:fadd.s t6, t5, t4, dyn
	-[0x80000678]:csrrs a3, fcsr, zero
	-[0x8000067c]:sw t6, 144(s1)
Current Store : [0x80000680] : sw a3, 148(s1) -- Store: [0x80002464]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x1bd52c and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000694]:fadd.s t6, t5, t4, dyn
	-[0x80000698]:csrrs a3, fcsr, zero
	-[0x8000069c]:sw t6, 152(s1)
Current Store : [0x800006a0] : sw a3, 156(s1) -- Store: [0x8000246c]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x2bd8f4 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006b4]:fadd.s t6, t5, t4, dyn
	-[0x800006b8]:csrrs a3, fcsr, zero
	-[0x800006bc]:sw t6, 160(s1)
Current Store : [0x800006c0] : sw a3, 164(s1) -- Store: [0x80002474]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x365ad7 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006d4]:fadd.s t6, t5, t4, dyn
	-[0x800006d8]:csrrs a3, fcsr, zero
	-[0x800006dc]:sw t6, 168(s1)
Current Store : [0x800006e0] : sw a3, 172(s1) -- Store: [0x8000247c]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x2d0427 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x554674 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006f4]:fadd.s t6, t5, t4, dyn
	-[0x800006f8]:csrrs a3, fcsr, zero
	-[0x800006fc]:sw t6, 176(s1)
Current Store : [0x80000700] : sw a3, 180(s1) -- Store: [0x80002484]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f4810 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000714]:fadd.s t6, t5, t4, dyn
	-[0x80000718]:csrrs a3, fcsr, zero
	-[0x8000071c]:sw t6, 184(s1)
Current Store : [0x80000720] : sw a3, 188(s1) -- Store: [0x8000248c]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x185183 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x28224f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000734]:fadd.s t6, t5, t4, dyn
	-[0x80000738]:csrrs a3, fcsr, zero
	-[0x8000073c]:sw t6, 192(s1)
Current Store : [0x80000740] : sw a3, 196(s1) -- Store: [0x80002494]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x6b4f07 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x530d37 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000754]:fadd.s t6, t5, t4, dyn
	-[0x80000758]:csrrs a3, fcsr, zero
	-[0x8000075c]:sw t6, 200(s1)
Current Store : [0x80000760] : sw a3, 204(s1) -- Store: [0x8000249c]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x222105 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000774]:fadd.s t6, t5, t4, dyn
	-[0x80000778]:csrrs a3, fcsr, zero
	-[0x8000077c]:sw t6, 208(s1)
Current Store : [0x80000780] : sw a3, 212(s1) -- Store: [0x800024a4]:0x00000000




Last Coverpoint : ['mnemonic : fadd.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 1 and fe1 == 0xfd and fm1 == 0x217fdd and fs2 == 0 and fe2 == 0xfe and fm2 == 0x027635 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000794]:fadd.s t6, t5, t4, dyn
	-[0x80000798]:csrrs a3, fcsr, zero
	-[0x8000079c]:sw t6, 216(s1)
Current Store : [0x800007a0] : sw a3, 220(s1) -- Store: [0x800024ac]:0x00000000




Last Coverpoint : ['mnemonic : fadd.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 1 and fe1 == 0xfd and fm1 == 0x445459 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x2fe97e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007b4]:fadd.s t6, t5, t4, dyn
	-[0x800007b8]:csrrs a3, fcsr, zero
	-[0x800007bc]:sw t6, 224(s1)
Current Store : [0x800007c0] : sw a3, 228(s1) -- Store: [0x800024b4]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x167d44 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x50b9b1 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007d4]:fadd.s t6, t5, t4, dyn
	-[0x800007d8]:csrrs a3, fcsr, zero
	-[0x800007dc]:sw t6, 232(s1)
Current Store : [0x800007e0] : sw a3, 236(s1) -- Store: [0x800024bc]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x07a8e7 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x70c4b8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007f4]:fadd.s t6, t5, t4, dyn
	-[0x800007f8]:csrrs a3, fcsr, zero
	-[0x800007fc]:sw t6, 240(s1)
Current Store : [0x80000800] : sw a3, 244(s1) -- Store: [0x800024c4]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x1c60ac and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000814]:fadd.s t6, t5, t4, dyn
	-[0x80000818]:csrrs a3, fcsr, zero
	-[0x8000081c]:sw t6, 248(s1)
Current Store : [0x80000820] : sw a3, 252(s1) -- Store: [0x800024cc]:0x00000000




Last Coverpoint : ['mnemonic : fadd.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 1 and fe1 == 0xfe and fm1 == 0x390e97 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000834]:fadd.s t6, t5, t4, dyn
	-[0x80000838]:csrrs a3, fcsr, zero
	-[0x8000083c]:sw t6, 256(s1)
Current Store : [0x80000840] : sw a3, 260(s1) -- Store: [0x800024d4]:0x00000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                                coverpoints                                                                                                                |                                                      code                                                       |
|---:|--------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|
|   1|[0x80002310]<br>0xFF800000|- mnemonic : fadd.s<br> - rs1 : x30<br> - rs2 : x30<br> - rd : x31<br> - rs1 == rs2 != rd<br>                                                                                                                                              |[0x80000124]:fadd.s t6, t5, t5, dyn<br> [0x80000128]:csrrs tp, fcsr, zero<br> [0x8000012c]:sw t6, 0(ra)<br>      |
|   2|[0x80002318]<br>0x7E46D91A|- rs1 : x31<br> - rs2 : x29<br> - rd : x29<br> - rs2 == rd != rs1<br> - fs1 == 1 and fe1 == 0xfd and fm1 == 0x217fdd and fs2 == 0 and fe2 == 0xfe and fm2 == 0x027635 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                        |[0x80000144]:fadd.s t4, t6, t4, dyn<br> [0x80000148]:csrrs tp, fcsr, zero<br> [0x8000014c]:sw t4, 8(ra)<br>      |
|   3|[0x80002320]<br>0x7E9B7EA3|- rs1 : x28<br> - rs2 : x31<br> - rd : x28<br> - rs1 == rd != rs2<br> - fs1 == 1 and fe1 == 0xfd and fm1 == 0x445459 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x2fe97e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                        |[0x80000164]:fadd.s t3, t3, t6, dyn<br> [0x80000168]:csrrs tp, fcsr, zero<br> [0x8000016c]:sw t3, 16(ra)<br>     |
|   4|[0x80002328]<br>0xFF800000|- rs1 : x27<br> - rs2 : x27<br> - rd : x27<br> - rs1 == rs2 == rd<br>                                                                                                                                                                      |[0x80000184]:fadd.s s11, s11, s11, dyn<br> [0x80000188]:csrrs tp, fcsr, zero<br> [0x8000018c]:sw s11, 24(ra)<br> |
|   5|[0x80002330]<br>0x7EE004C8|- rs1 : x29<br> - rs2 : x28<br> - rd : x30<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 1 and fe1 == 0xfd and fm1 == 0x370362 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x4b8415 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x800001a4]:fadd.s t5, t4, t3, dyn<br> [0x800001a8]:csrrs tp, fcsr, zero<br> [0x800001ac]:sw t5, 32(ra)<br>     |
|   6|[0x80002338]<br>0x7EA348DE|- rs1 : x25<br> - rs2 : x24<br> - rd : x26<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x2e5b90 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x800001c4]:fadd.s s10, s9, s8, dyn<br> [0x800001c8]:csrrs tp, fcsr, zero<br> [0x800001cc]:sw s10, 40(ra)<br>   |
|   7|[0x80002340]<br>0x7F111D4A|- rs1 : x24<br> - rs2 : x26<br> - rd : x25<br> - fs1 == 1 and fe1 == 0xfc and fm1 == 0x587392 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x473a2e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x800001e4]:fadd.s s9, s8, s10, dyn<br> [0x800001e8]:csrrs tp, fcsr, zero<br> [0x800001ec]:sw s9, 48(ra)<br>    |
|   8|[0x80002348]<br>0x7E40638C|- rs1 : x26<br> - rs2 : x25<br> - rd : x24<br> - fs1 == 1 and fe1 == 0xfd and fm1 == 0x6d7424 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x26d2f5 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x80000204]:fadd.s s8, s10, s9, dyn<br> [0x80000208]:csrrs tp, fcsr, zero<br> [0x8000020c]:sw s8, 56(ra)<br>    |
|   9|[0x80002350]<br>0x7EA2A84E|- rs1 : x22<br> - rs2 : x21<br> - rd : x23<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x2eabd8 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x80000224]:fadd.s s7, s6, s5, dyn<br> [0x80000228]:csrrs tp, fcsr, zero<br> [0x8000022c]:sw s7, 64(ra)<br>     |
|  10|[0x80002358]<br>0x7EC8E068|- rs1 : x21<br> - rs2 : x23<br> - rd : x22<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x1b8fcb and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x80000244]:fadd.s s6, s5, s7, dyn<br> [0x80000248]:csrrs tp, fcsr, zero<br> [0x8000024c]:sw s6, 72(ra)<br>     |
|  11|[0x80002360]<br>0x7F0C20B0|- rs1 : x23<br> - rs2 : x22<br> - rd : x21<br> - fs1 == 1 and fe1 == 0xfc and fm1 == 0x6e317d and fs2 == 0 and fe2 == 0xfe and fm2 == 0x47ad0f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x80000264]:fadd.s s5, s7, s6, dyn<br> [0x80000268]:csrrs tp, fcsr, zero<br> [0x8000026c]:sw s5, 80(ra)<br>     |
|  12|[0x80002368]<br>0x7EA6D89A|- rs1 : x19<br> - rs2 : x18<br> - rd : x20<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x2c93b2 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x80000284]:fadd.s s4, s3, s2, dyn<br> [0x80000288]:csrrs tp, fcsr, zero<br> [0x8000028c]:sw s4, 88(ra)<br>     |
|  13|[0x80002370]<br>0x7D9AAD50|- rs1 : x18<br> - rs2 : x20<br> - rd : x19<br> - fs1 == 1 and fe1 == 0xfd and fm1 == 0x354d84 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x5bf8d8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x800002a4]:fadd.s s3, s2, s4, dyn<br> [0x800002a8]:csrrs tp, fcsr, zero<br> [0x800002ac]:sw s3, 96(ra)<br>     |
|  14|[0x80002378]<br>0x7EEA94A1|- rs1 : x20<br> - rs2 : x19<br> - rd : x18<br> - fs1 == 1 and fe1 == 0xfd and fm1 == 0x26b8d3 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x48a6ba and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x800002c4]:fadd.s s2, s4, s3, dyn<br> [0x800002c8]:csrrs tp, fcsr, zero<br> [0x800002cc]:sw s2, 104(ra)<br>    |
|  15|[0x80002380]<br>0x7EA1675C|- rs1 : x16<br> - rs2 : x15<br> - rd : x17<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x2f4c51 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x800002e4]:fadd.s a7, a6, a5, dyn<br> [0x800002e8]:csrrs tp, fcsr, zero<br> [0x800002ec]:sw a7, 112(ra)<br>    |
|  16|[0x80002388]<br>0x7F246A04|- rs1 : x15<br> - rs2 : x17<br> - rd : x16<br> - fs1 == 1 and fe1 == 0xfd and fm1 == 0x372bf7 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x80000304]:fadd.s a6, a5, a7, dyn<br> [0x80000308]:csrrs tp, fcsr, zero<br> [0x8000030c]:sw a6, 120(ra)<br>    |
|  17|[0x80002390]<br>0x7F1A90BA|- rs1 : x17<br> - rs2 : x16<br> - rd : x15<br> - fs1 == 1 and fe1 == 0xfc and fm1 == 0x480ede and fs2 == 0 and fe2 == 0xfe and fm2 == 0x4c9471 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x80000324]:fadd.s a5, a7, a6, dyn<br> [0x80000328]:csrrs tp, fcsr, zero<br> [0x8000032c]:sw a5, 128(ra)<br>    |
|  18|[0x80002398]<br>0x7C2C8B60|- rs1 : x13<br> - rs2 : x12<br> - rd : x14<br> - fs1 == 1 and fe1 == 0xfd and fm1 == 0x52b355 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x5817b0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x80000344]:fadd.s a4, a3, a2, dyn<br> [0x80000348]:csrrs tp, fcsr, zero<br> [0x8000034c]:sw a4, 136(ra)<br>    |
|  19|[0x800023a0]<br>0x7E9925F2|- rs1 : x12<br> - rs2 : x14<br> - rd : x13<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x0a2eec and fs2 == 0 and fe2 == 0xfe and fm2 == 0x56c1e5 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x80000364]:fadd.s a3, a2, a4, dyn<br> [0x80000368]:csrrs tp, fcsr, zero<br> [0x8000036c]:sw a3, 144(ra)<br>    |
|  20|[0x800023a8]<br>0x7EC34270|- rs1 : x14<br> - rs2 : x13<br> - rd : x12<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x1e5ec7 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x80000384]:fadd.s a2, a4, a3, dyn<br> [0x80000388]:csrrs tp, fcsr, zero<br> [0x8000038c]:sw a2, 152(ra)<br>    |
|  21|[0x800023b0]<br>0x7EBD88AD|- rs1 : x10<br> - rs2 : x9<br> - rd : x11<br> - fs1 == 1 and fe1 == 0xf4 and fm1 == 0x60affa and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3df905 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x800003a4]:fadd.s a1, a0, s1, dyn<br> [0x800003a8]:csrrs tp, fcsr, zero<br> [0x800003ac]:sw a1, 160(ra)<br>    |
|  22|[0x800023b8]<br>0x7E3B52D4|- rs1 : x9<br> - rs2 : x11<br> - rd : x10<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x269468 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x55691d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x800003cc]:fadd.s a0, s1, a1, dyn<br> [0x800003d0]:csrrs a3, fcsr, zero<br> [0x800003d4]:sw a0, 168(ra)<br>    |
|  23|[0x800023c0]<br>0x7F272166|- rs1 : x11<br> - rs2 : x10<br> - rd : x9<br> - fs1 == 1 and fe1 == 0xfc and fm1 == 0x79c1c6 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x6591d8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x800003ec]:fadd.s s1, a1, a0, dyn<br> [0x800003f0]:csrrs a3, fcsr, zero<br> [0x800003f4]:sw s1, 176(ra)<br>    |
|  24|[0x800023c8]<br>0x7ED9EC92|- rs1 : x7<br> - rs2 : x6<br> - rd : x8<br> - fs1 == 1 and fe1 == 0xfc and fm1 == 0x12bd51 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x11a59d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                  |[0x8000040c]:fadd.s fp, t2, t1, dyn<br> [0x80000410]:csrrs a3, fcsr, zero<br> [0x80000414]:sw fp, 184(ra)<br>    |
|  25|[0x800023d0]<br>0x7F39C489|- rs1 : x6<br> - rs2 : x8<br> - rd : x7<br> - fs1 == 1 and fe1 == 0xfc and fm1 == 0x3741cc and fs2 == 0 and fe2 == 0xfe and fm2 == 0x6794fc and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                  |[0x80000434]:fadd.s t2, t1, fp, dyn<br> [0x80000438]:csrrs a3, fcsr, zero<br> [0x8000043c]:sw t2, 0(s1)<br>      |
|  26|[0x800023d8]<br>0x7EAC0532|- rs1 : x8<br> - rs2 : x7<br> - rd : x6<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x1a35e0 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x703879 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                  |[0x80000454]:fadd.s t1, fp, t2, dyn<br> [0x80000458]:csrrs a3, fcsr, zero<br> [0x8000045c]:sw t1, 8(s1)<br>      |
|  27|[0x800023e0]<br>0x7E75CD4A|- rs1 : x4<br> - rs2 : x3<br> - rd : x5<br> - fs1 == 1 and fe1 == 0xfa and fm1 == 0x772129 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x19caca and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                  |[0x80000474]:fadd.s t0, tp, gp, dyn<br> [0x80000478]:csrrs a3, fcsr, zero<br> [0x8000047c]:sw t0, 16(s1)<br>     |
|  28|[0x800023e8]<br>0x7F1E79B3|- rs1 : x3<br> - rs2 : x5<br> - rd : x4<br> - fs1 == 1 and fe1 == 0xfd and fm1 == 0x430c98 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                  |[0x80000494]:fadd.s tp, gp, t0, dyn<br> [0x80000498]:csrrs a3, fcsr, zero<br> [0x8000049c]:sw tp, 24(s1)<br>     |
|  29|[0x800023f0]<br>0x7ED3AFC6|- rs1 : x5<br> - rs2 : x4<br> - rd : x3<br> - fs1 == 1 and fe1 == 0xfb and fm1 == 0x278349 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x7d9098 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                  |[0x800004b4]:fadd.s gp, t0, tp, dyn<br> [0x800004b8]:csrrs a3, fcsr, zero<br> [0x800004bc]:sw gp, 32(s1)<br>     |
|  30|[0x800023f8]<br>0xFF07A8E7|- rs1 : x1<br> - rs2 : x0<br> - rd : x2<br>                                                                                                                                                                                                |[0x800004d4]:fadd.s sp, ra, zero, dyn<br> [0x800004d8]:csrrs a3, fcsr, zero<br> [0x800004dc]:sw sp, 40(s1)<br>   |
|  31|[0x80002400]<br>0x7F7FFFFF|- rs1 : x0<br> - rs2 : x2<br> - rd : x1<br>                                                                                                                                                                                                |[0x800004f4]:fadd.s ra, zero, sp, dyn<br> [0x800004f8]:csrrs a3, fcsr, zero<br> [0x800004fc]:sw ra, 48(s1)<br>   |
|  32|[0x80002408]<br>0x00000000|- rs1 : x2<br> - rs2 : x1<br> - rd : x0<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x390e97 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                  |[0x80000514]:fadd.s zero, sp, ra, dyn<br> [0x80000518]:csrrs a3, fcsr, zero<br> [0x8000051c]:sw zero, 56(s1)<br> |
|  33|[0x80002410]<br>0x7EAAE033|- fs1 == 1 and fe1 == 0xfd and fm1 == 0x61a51b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x4642a7 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000534]:fadd.s t6, t5, t4, dyn<br> [0x80000538]:csrrs a3, fcsr, zero<br> [0x8000053c]:sw t6, 64(s1)<br>     |
|  34|[0x80002418]<br>0x7E93CD51|- fs1 == 1 and fe1 == 0xfd and fm1 == 0x64f961 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x3c6359 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000554]:fadd.s t6, t5, t4, dyn<br> [0x80000558]:csrrs a3, fcsr, zero<br> [0x8000055c]:sw t6, 72(s1)<br>     |
|  35|[0x80002420]<br>0x7E5EF91A|- fs1 == 1 and fe1 == 0xfd and fm1 == 0x5d0ccb and fs2 == 0 and fe2 == 0xfe and fm2 == 0x2644ac and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000574]:fadd.s t6, t5, t4, dyn<br> [0x80000578]:csrrs a3, fcsr, zero<br> [0x8000057c]:sw t6, 80(s1)<br>     |
|  36|[0x80002428]<br>0x7E0F6883|- fs1 == 1 and fe1 == 0xfc and fm1 == 0x1173d9 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x106e2e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000594]:fadd.s t6, t5, t4, dyn<br> [0x80000598]:csrrs a3, fcsr, zero<br> [0x8000059c]:sw t6, 88(s1)<br>     |
|  37|[0x80002430]<br>0x7EFC283D|- fs1 == 1 and fe1 == 0xfd and fm1 == 0x6b4e0d and fs2 == 0 and fe2 == 0xfe and fm2 == 0x73bb25 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800005b4]:fadd.s t6, t5, t4, dyn<br> [0x800005b8]:csrrs a3, fcsr, zero<br> [0x800005bc]:sw t6, 96(s1)<br>     |
|  38|[0x80002438]<br>0x7D89F860|- fs1 == 1 and fe1 == 0xfd and fm1 == 0x36fce6 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x597afe and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800005d4]:fadd.s t6, t5, t4, dyn<br> [0x800005d8]:csrrs a3, fcsr, zero<br> [0x800005dc]:sw t6, 104(s1)<br>    |
|  39|[0x80002440]<br>0x7EDD1480|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x1175bf and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800005f4]:fadd.s t6, t5, t4, dyn<br> [0x800005f8]:csrrs a3, fcsr, zero<br> [0x800005fc]:sw t6, 112(s1)<br>    |
|  40|[0x80002448]<br>0x7E9D6BF4|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x314a05 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000614]:fadd.s t6, t5, t4, dyn<br> [0x80000618]:csrrs a3, fcsr, zero<br> [0x8000061c]:sw t6, 120(s1)<br>    |
|  41|[0x80002450]<br>0x7EA719FE|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x2c7300 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000634]:fadd.s t6, t5, t4, dyn<br> [0x80000638]:csrrs a3, fcsr, zero<br> [0x8000063c]:sw t6, 128(s1)<br>    |
|  42|[0x80002458]<br>0x7E9641F6|- fs1 == 1 and fe1 == 0xfd and fm1 == 0x4f9722 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x32ec8c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000654]:fadd.s t6, t5, t4, dyn<br> [0x80000658]:csrrs a3, fcsr, zero<br> [0x8000065c]:sw t6, 136(s1)<br>    |
|  43|[0x80002460]<br>0x7E53AFE0|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x076a16 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x3c560e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000674]:fadd.s t6, t5, t4, dyn<br> [0x80000678]:csrrs a3, fcsr, zero<br> [0x8000067c]:sw t6, 144(s1)<br>    |
|  44|[0x80002468]<br>0x7EC855A6|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x1bd52c and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000694]:fadd.s t6, t5, t4, dyn<br> [0x80000698]:csrrs a3, fcsr, zero<br> [0x8000069c]:sw t6, 152(s1)<br>    |
|  45|[0x80002470]<br>0x7EA84E16|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x2bd8f4 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800006b4]:fadd.s t6, t5, t4, dyn<br> [0x800006b8]:csrrs a3, fcsr, zero<br> [0x800006bc]:sw t6, 160(s1)<br>    |
|  46|[0x80002478]<br>0x7E934A50|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x365ad7 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800006d4]:fadd.s t6, t5, t4, dyn<br> [0x800006d8]:csrrs a3, fcsr, zero<br> [0x800006dc]:sw t6, 168(s1)<br>    |
|  47|[0x80002480]<br>0x7EFD88C1|- fs1 == 1 and fe1 == 0xfd and fm1 == 0x2d0427 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x554674 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800006f4]:fadd.s t6, t5, t4, dyn<br> [0x800006f8]:csrrs a3, fcsr, zero<br> [0x800006fc]:sw t6, 176(s1)<br>    |
|  48|[0x80002488]<br>0x7E816FDE|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f4810 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000714]:fadd.s t6, t5, t4, dyn<br> [0x80000718]:csrrs a3, fcsr, zero<br> [0x8000071c]:sw t6, 184(s1)<br>    |
|  49|[0x80002490]<br>0x7D7D0CC0|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x185183 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x28224f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000734]:fadd.s t6, t5, t4, dyn<br> [0x80000738]:csrrs a3, fcsr, zero<br> [0x8000073c]:sw t6, 192(s1)<br>    |
|  50|[0x80002498]<br>0x7EBACB67|- fs1 == 1 and fe1 == 0xfd and fm1 == 0x6b4f07 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x530d37 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000754]:fadd.s t6, t5, t4, dyn<br> [0x80000758]:csrrs a3, fcsr, zero<br> [0x8000075c]:sw t6, 200(s1)<br>    |
|  51|[0x800024a0]<br>0x7EBBBDF4|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x222105 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000774]:fadd.s t6, t5, t4, dyn<br> [0x80000778]:csrrs a3, fcsr, zero<br> [0x8000077c]:sw t6, 208(s1)<br>    |
|  52|[0x800024b8]<br>0x7E68F1B4|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x167d44 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x50b9b1 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800007d4]:fadd.s t6, t5, t4, dyn<br> [0x800007d8]:csrrs a3, fcsr, zero<br> [0x800007dc]:sw t6, 232(s1)<br>    |
|  53|[0x800024c0]<br>0x7ED237A2|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x07a8e7 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x70c4b8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800007f4]:fadd.s t6, t5, t4, dyn<br> [0x800007f8]:csrrs a3, fcsr, zero<br> [0x800007fc]:sw t6, 240(s1)<br>    |
|  54|[0x800024c8]<br>0x7EC73EA6|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x1c60ac and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000814]:fadd.s t6, t5, t4, dyn<br> [0x80000818]:csrrs a3, fcsr, zero<br> [0x8000081c]:sw t6, 248(s1)<br>    |
