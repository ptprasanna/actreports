
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x8000ede0')]      |
| SIG_REGION                | [('0x80012510', '0x80014850', '2256 words')]      |
| COV_LABELS                | fmul_b3      |
| TEST_NAME                 | /home/reg/work/zfinx/fmul.s/work/fmul_b3-01.S/ref.S    |
| Total Number of coverpoints| 1222     |
| Total Coverpoints Hit     | 1222      |
| Total Signature Updates   | 2254      |
| STAT1                     | 1123      |
| STAT2                     | 4      |
| STAT3                     | 0     |
| STAT4                     | 1127     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x8000ec7c]:fmul.s t6, t5, t4, dyn
      [0x8000ec80]:csrrs a2, fcsr, zero
      [0x8000ec84]:sw t6, 568(fp)
 -- Signature Address: 0x80014810 Data: 0x00000000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fmul.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0xfe and fm1 == 0x378efe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x8000ed24]:fmul.s t6, t5, t4, dyn
      [0x8000ed28]:csrrs a2, fcsr, zero
      [0x8000ed2c]:sw t6, 592(fp)
 -- Signature Address: 0x80014828 Data: 0x00000000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fmul.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0xfe and fm1 == 0x378efe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x8000ed94]:fmul.s t6, t5, t4, dyn
      [0x8000ed98]:csrrs a2, fcsr, zero
      [0x8000ed9c]:sw t6, 608(fp)
 -- Signature Address: 0x80014838 Data: 0x00000000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fmul.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0xfd and fm1 == 0x25608b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x8000edcc]:fmul.s t6, t5, t4, dyn
      [0x8000edd0]:csrrs a2, fcsr, zero
      [0x8000edd4]:sw t6, 616(fp)
 -- Signature Address: 0x80014840 Data: 0x80000000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fmul.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0xfe and fm1 == 0x0ad17d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fmul.s', 'rs1 : x30', 'rs2 : x31', 'rd : x31', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x378efe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000124]:fmul.s t6, t5, t6, dyn
	-[0x80000128]:csrrs tp, fcsr, zero
	-[0x8000012c]:sw t6, 0(ra)
Current Store : [0x80000130] : sw tp, 4(ra) -- Store: [0x80012514]:0x00000000




Last Coverpoint : ['rs1 : x31', 'rs2 : x29', 'rd : x30', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x378efe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000144]:fmul.s t5, t6, t4, dyn
	-[0x80000148]:csrrs tp, fcsr, zero
	-[0x8000014c]:sw t5, 8(ra)
Current Store : [0x80000150] : sw tp, 12(ra) -- Store: [0x8001251c]:0x00000020




Last Coverpoint : ['rs1 : x28', 'rs2 : x28', 'rd : x28', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x80000164]:fmul.s t3, t3, t3, dyn
	-[0x80000168]:csrrs tp, fcsr, zero
	-[0x8000016c]:sw t3, 16(ra)
Current Store : [0x80000170] : sw tp, 20(ra) -- Store: [0x80012524]:0x00000045




Last Coverpoint : ['rs1 : x27', 'rs2 : x27', 'rd : x29', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x80000184]:fmul.s t4, s11, s11, dyn
	-[0x80000188]:csrrs tp, fcsr, zero
	-[0x8000018c]:sw t4, 24(ra)
Current Store : [0x80000190] : sw tp, 28(ra) -- Store: [0x8001252c]:0x00000065




Last Coverpoint : ['rs1 : x26', 'rs2 : x30', 'rd : x26', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x378efe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001a4]:fmul.s s10, s10, t5, dyn
	-[0x800001a8]:csrrs tp, fcsr, zero
	-[0x800001ac]:sw s10, 32(ra)
Current Store : [0x800001b0] : sw tp, 36(ra) -- Store: [0x80012534]:0x00000080




Last Coverpoint : ['rs1 : x29', 'rs2 : x26', 'rd : x27', 'fs1 == 0 and fe1 == 0xfa and fm1 == 0x2db9cd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001c4]:fmul.s s11, t4, s10, dyn
	-[0x800001c8]:csrrs tp, fcsr, zero
	-[0x800001cc]:sw s11, 40(ra)
Current Store : [0x800001d0] : sw tp, 44(ra) -- Store: [0x8001253c]:0x00000000




Last Coverpoint : ['rs1 : x24', 'rs2 : x23', 'rd : x25', 'fs1 == 0 and fe1 == 0xfa and fm1 == 0x2db9cd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001e4]:fmul.s s9, s8, s7, dyn
	-[0x800001e8]:csrrs tp, fcsr, zero
	-[0x800001ec]:sw s9, 48(ra)
Current Store : [0x800001f0] : sw tp, 52(ra) -- Store: [0x80012544]:0x00000020




Last Coverpoint : ['rs1 : x23', 'rs2 : x25', 'rd : x24', 'fs1 == 0 and fe1 == 0xfa and fm1 == 0x2db9cd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000204]:fmul.s s8, s7, s9, dyn
	-[0x80000208]:csrrs tp, fcsr, zero
	-[0x8000020c]:sw s8, 56(ra)
Current Store : [0x80000210] : sw tp, 60(ra) -- Store: [0x8001254c]:0x00000040




Last Coverpoint : ['rs1 : x25', 'rs2 : x24', 'rd : x23', 'fs1 == 0 and fe1 == 0xfa and fm1 == 0x2db9cd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000224]:fmul.s s7, s9, s8, dyn
	-[0x80000228]:csrrs tp, fcsr, zero
	-[0x8000022c]:sw s7, 64(ra)
Current Store : [0x80000230] : sw tp, 68(ra) -- Store: [0x80012554]:0x00000060




Last Coverpoint : ['rs1 : x21', 'rs2 : x20', 'rd : x22', 'fs1 == 0 and fe1 == 0xfa and fm1 == 0x2db9cd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000244]:fmul.s s6, s5, s4, dyn
	-[0x80000248]:csrrs tp, fcsr, zero
	-[0x8000024c]:sw s6, 72(ra)
Current Store : [0x80000250] : sw tp, 76(ra) -- Store: [0x8001255c]:0x00000080




Last Coverpoint : ['rs1 : x20', 'rs2 : x22', 'rd : x21', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x206a70 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000264]:fmul.s s5, s4, s6, dyn
	-[0x80000268]:csrrs tp, fcsr, zero
	-[0x8000026c]:sw s5, 80(ra)
Current Store : [0x80000270] : sw tp, 84(ra) -- Store: [0x80012564]:0x00000000




Last Coverpoint : ['rs1 : x22', 'rs2 : x21', 'rd : x20', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x206a70 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000284]:fmul.s s4, s6, s5, dyn
	-[0x80000288]:csrrs tp, fcsr, zero
	-[0x8000028c]:sw s4, 88(ra)
Current Store : [0x80000290] : sw tp, 92(ra) -- Store: [0x8001256c]:0x00000020




Last Coverpoint : ['rs1 : x18', 'rs2 : x17', 'rd : x19', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x206a70 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002a4]:fmul.s s3, s2, a7, dyn
	-[0x800002a8]:csrrs tp, fcsr, zero
	-[0x800002ac]:sw s3, 96(ra)
Current Store : [0x800002b0] : sw tp, 100(ra) -- Store: [0x80012574]:0x00000040




Last Coverpoint : ['rs1 : x17', 'rs2 : x19', 'rd : x18', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x206a70 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002c4]:fmul.s s2, a7, s3, dyn
	-[0x800002c8]:csrrs tp, fcsr, zero
	-[0x800002cc]:sw s2, 104(ra)
Current Store : [0x800002d0] : sw tp, 108(ra) -- Store: [0x8001257c]:0x00000060




Last Coverpoint : ['rs1 : x19', 'rs2 : x18', 'rd : x17', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x206a70 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002e4]:fmul.s a7, s3, s2, dyn
	-[0x800002e8]:csrrs tp, fcsr, zero
	-[0x800002ec]:sw a7, 112(ra)
Current Store : [0x800002f0] : sw tp, 116(ra) -- Store: [0x80012584]:0x00000080




Last Coverpoint : ['rs1 : x15', 'rs2 : x14', 'rd : x16', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x009696 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000304]:fmul.s a6, a5, a4, dyn
	-[0x80000308]:csrrs tp, fcsr, zero
	-[0x8000030c]:sw a6, 120(ra)
Current Store : [0x80000310] : sw tp, 124(ra) -- Store: [0x8001258c]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rs2 : x16', 'rd : x15', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x009696 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000324]:fmul.s a5, a4, a6, dyn
	-[0x80000328]:csrrs tp, fcsr, zero
	-[0x8000032c]:sw a5, 128(ra)
Current Store : [0x80000330] : sw tp, 132(ra) -- Store: [0x80012594]:0x00000020




Last Coverpoint : ['rs1 : x16', 'rs2 : x15', 'rd : x14', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x009696 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000344]:fmul.s a4, a6, a5, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:sw a4, 136(ra)
Current Store : [0x80000350] : sw tp, 140(ra) -- Store: [0x8001259c]:0x00000040




Last Coverpoint : ['rs1 : x12', 'rs2 : x11', 'rd : x13', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x009696 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000364]:fmul.s a3, a2, a1, dyn
	-[0x80000368]:csrrs tp, fcsr, zero
	-[0x8000036c]:sw a3, 144(ra)
Current Store : [0x80000370] : sw tp, 148(ra) -- Store: [0x800125a4]:0x00000060




Last Coverpoint : ['rs1 : x11', 'rs2 : x13', 'rd : x12', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x009696 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000384]:fmul.s a2, a1, a3, dyn
	-[0x80000388]:csrrs tp, fcsr, zero
	-[0x8000038c]:sw a2, 152(ra)
Current Store : [0x80000390] : sw tp, 156(ra) -- Store: [0x800125ac]:0x00000080




Last Coverpoint : ['rs1 : x13', 'rs2 : x12', 'rd : x11', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x68aebb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003a4]:fmul.s a1, a3, a2, dyn
	-[0x800003a8]:csrrs tp, fcsr, zero
	-[0x800003ac]:sw a1, 160(ra)
Current Store : [0x800003b0] : sw tp, 164(ra) -- Store: [0x800125b4]:0x00000000




Last Coverpoint : ['rs1 : x9', 'rs2 : x8', 'rd : x10', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x68aebb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003c4]:fmul.s a0, s1, fp, dyn
	-[0x800003c8]:csrrs tp, fcsr, zero
	-[0x800003cc]:sw a0, 168(ra)
Current Store : [0x800003d0] : sw tp, 172(ra) -- Store: [0x800125bc]:0x00000020




Last Coverpoint : ['rs1 : x8', 'rs2 : x10', 'rd : x9', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x68aebb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003ec]:fmul.s s1, fp, a0, dyn
	-[0x800003f0]:csrrs a2, fcsr, zero
	-[0x800003f4]:sw s1, 176(ra)
Current Store : [0x800003f8] : sw a2, 180(ra) -- Store: [0x800125c4]:0x00000040




Last Coverpoint : ['rs1 : x10', 'rs2 : x9', 'rd : x8', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x68aebb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000040c]:fmul.s fp, a0, s1, dyn
	-[0x80000410]:csrrs a2, fcsr, zero
	-[0x80000414]:sw fp, 184(ra)
Current Store : [0x80000418] : sw a2, 188(ra) -- Store: [0x800125cc]:0x00000060




Last Coverpoint : ['rs1 : x6', 'rs2 : x5', 'rd : x7', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x68aebb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000042c]:fmul.s t2, t1, t0, dyn
	-[0x80000430]:csrrs a2, fcsr, zero
	-[0x80000434]:sw t2, 192(ra)
Current Store : [0x80000438] : sw a2, 196(ra) -- Store: [0x800125d4]:0x00000080




Last Coverpoint : ['rs1 : x5', 'rs2 : x7', 'rd : x6', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x5f2ead and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000454]:fmul.s t1, t0, t2, dyn
	-[0x80000458]:csrrs a2, fcsr, zero
	-[0x8000045c]:sw t1, 0(fp)
Current Store : [0x80000460] : sw a2, 4(fp) -- Store: [0x800125dc]:0x00000000




Last Coverpoint : ['rs1 : x7', 'rs2 : x6', 'rd : x5', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x5f2ead and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000474]:fmul.s t0, t2, t1, dyn
	-[0x80000478]:csrrs a2, fcsr, zero
	-[0x8000047c]:sw t0, 8(fp)
Current Store : [0x80000480] : sw a2, 12(fp) -- Store: [0x800125e4]:0x00000020




Last Coverpoint : ['rs1 : x3', 'rs2 : x2', 'rd : x4', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x5f2ead and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000494]:fmul.s tp, gp, sp, dyn
	-[0x80000498]:csrrs a2, fcsr, zero
	-[0x8000049c]:sw tp, 16(fp)
Current Store : [0x800004a0] : sw a2, 20(fp) -- Store: [0x800125ec]:0x00000040




Last Coverpoint : ['rs1 : x2', 'rs2 : x4', 'rd : x3', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x5f2ead and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004b4]:fmul.s gp, sp, tp, dyn
	-[0x800004b8]:csrrs a2, fcsr, zero
	-[0x800004bc]:sw gp, 24(fp)
Current Store : [0x800004c0] : sw a2, 28(fp) -- Store: [0x800125f4]:0x00000060




Last Coverpoint : ['rs1 : x4', 'rs2 : x3', 'rd : x2', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x5f2ead and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004d4]:fmul.s sp, tp, gp, dyn
	-[0x800004d8]:csrrs a2, fcsr, zero
	-[0x800004dc]:sw sp, 32(fp)
Current Store : [0x800004e0] : sw a2, 36(fp) -- Store: [0x800125fc]:0x00000080




Last Coverpoint : ['rs1 : x1', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x25608b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004f4]:fmul.s t6, ra, t5, dyn
	-[0x800004f8]:csrrs a2, fcsr, zero
	-[0x800004fc]:sw t6, 40(fp)
Current Store : [0x80000500] : sw a2, 44(fp) -- Store: [0x80012604]:0x00000000




Last Coverpoint : ['rs1 : x0']
Last Code Sequence : 
	-[0x80000514]:fmul.s t6, zero, t5, dyn
	-[0x80000518]:csrrs a2, fcsr, zero
	-[0x8000051c]:sw t6, 48(fp)
Current Store : [0x80000520] : sw a2, 52(fp) -- Store: [0x8001260c]:0x00000020




Last Coverpoint : ['rs2 : x1', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x25608b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000534]:fmul.s t6, t5, ra, dyn
	-[0x80000538]:csrrs a2, fcsr, zero
	-[0x8000053c]:sw t6, 56(fp)
Current Store : [0x80000540] : sw a2, 60(fp) -- Store: [0x80012614]:0x00000040




Last Coverpoint : ['rs2 : x0', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x25608b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000554]:fmul.s t6, t5, zero, dyn
	-[0x80000558]:csrrs a2, fcsr, zero
	-[0x8000055c]:sw t6, 64(fp)
Current Store : [0x80000560] : sw a2, 68(fp) -- Store: [0x8001261c]:0x00000060




Last Coverpoint : ['rd : x1', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x25608b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000574]:fmul.s ra, t6, t5, dyn
	-[0x80000578]:csrrs a2, fcsr, zero
	-[0x8000057c]:sw ra, 72(fp)
Current Store : [0x80000580] : sw a2, 76(fp) -- Store: [0x80012624]:0x00000080




Last Coverpoint : ['rd : x0', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x0ad17d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000594]:fmul.s zero, t6, t5, dyn
	-[0x80000598]:csrrs a2, fcsr, zero
	-[0x8000059c]:sw zero, 80(fp)
Current Store : [0x800005a0] : sw a2, 84(fp) -- Store: [0x8001262c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0ad17d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005b4]:fmul.s t6, t5, t4, dyn
	-[0x800005b8]:csrrs a2, fcsr, zero
	-[0x800005bc]:sw t6, 88(fp)
Current Store : [0x800005c0] : sw a2, 92(fp) -- Store: [0x80012634]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0ad17d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005d4]:fmul.s t6, t5, t4, dyn
	-[0x800005d8]:csrrs a2, fcsr, zero
	-[0x800005dc]:sw t6, 96(fp)
Current Store : [0x800005e0] : sw a2, 100(fp) -- Store: [0x8001263c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0ad17d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005f4]:fmul.s t6, t5, t4, dyn
	-[0x800005f8]:csrrs a2, fcsr, zero
	-[0x800005fc]:sw t6, 104(fp)
Current Store : [0x80000600] : sw a2, 108(fp) -- Store: [0x80012644]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0ad17d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000614]:fmul.s t6, t5, t4, dyn
	-[0x80000618]:csrrs a2, fcsr, zero
	-[0x8000061c]:sw t6, 112(fp)
Current Store : [0x80000620] : sw a2, 116(fp) -- Store: [0x8001264c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3648af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000634]:fmul.s t6, t5, t4, dyn
	-[0x80000638]:csrrs a2, fcsr, zero
	-[0x8000063c]:sw t6, 120(fp)
Current Store : [0x80000640] : sw a2, 124(fp) -- Store: [0x80012654]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3648af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000654]:fmul.s t6, t5, t4, dyn
	-[0x80000658]:csrrs a2, fcsr, zero
	-[0x8000065c]:sw t6, 128(fp)
Current Store : [0x80000660] : sw a2, 132(fp) -- Store: [0x8001265c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3648af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000674]:fmul.s t6, t5, t4, dyn
	-[0x80000678]:csrrs a2, fcsr, zero
	-[0x8000067c]:sw t6, 136(fp)
Current Store : [0x80000680] : sw a2, 140(fp) -- Store: [0x80012664]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3648af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000694]:fmul.s t6, t5, t4, dyn
	-[0x80000698]:csrrs a2, fcsr, zero
	-[0x8000069c]:sw t6, 144(fp)
Current Store : [0x800006a0] : sw a2, 148(fp) -- Store: [0x8001266c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3648af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006b4]:fmul.s t6, t5, t4, dyn
	-[0x800006b8]:csrrs a2, fcsr, zero
	-[0x800006bc]:sw t6, 152(fp)
Current Store : [0x800006c0] : sw a2, 156(fp) -- Store: [0x80012674]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2ad2f1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006d4]:fmul.s t6, t5, t4, dyn
	-[0x800006d8]:csrrs a2, fcsr, zero
	-[0x800006dc]:sw t6, 160(fp)
Current Store : [0x800006e0] : sw a2, 164(fp) -- Store: [0x8001267c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2ad2f1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006f4]:fmul.s t6, t5, t4, dyn
	-[0x800006f8]:csrrs a2, fcsr, zero
	-[0x800006fc]:sw t6, 168(fp)
Current Store : [0x80000700] : sw a2, 172(fp) -- Store: [0x80012684]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2ad2f1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000714]:fmul.s t6, t5, t4, dyn
	-[0x80000718]:csrrs a2, fcsr, zero
	-[0x8000071c]:sw t6, 176(fp)
Current Store : [0x80000720] : sw a2, 180(fp) -- Store: [0x8001268c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2ad2f1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000734]:fmul.s t6, t5, t4, dyn
	-[0x80000738]:csrrs a2, fcsr, zero
	-[0x8000073c]:sw t6, 184(fp)
Current Store : [0x80000740] : sw a2, 188(fp) -- Store: [0x80012694]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2ad2f1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000754]:fmul.s t6, t5, t4, dyn
	-[0x80000758]:csrrs a2, fcsr, zero
	-[0x8000075c]:sw t6, 192(fp)
Current Store : [0x80000760] : sw a2, 196(fp) -- Store: [0x8001269c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x5cb815 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000774]:fmul.s t6, t5, t4, dyn
	-[0x80000778]:csrrs a2, fcsr, zero
	-[0x8000077c]:sw t6, 200(fp)
Current Store : [0x80000780] : sw a2, 204(fp) -- Store: [0x800126a4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x5cb815 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000794]:fmul.s t6, t5, t4, dyn
	-[0x80000798]:csrrs a2, fcsr, zero
	-[0x8000079c]:sw t6, 208(fp)
Current Store : [0x800007a0] : sw a2, 212(fp) -- Store: [0x800126ac]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x5cb815 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007b4]:fmul.s t6, t5, t4, dyn
	-[0x800007b8]:csrrs a2, fcsr, zero
	-[0x800007bc]:sw t6, 216(fp)
Current Store : [0x800007c0] : sw a2, 220(fp) -- Store: [0x800126b4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x5cb815 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007d4]:fmul.s t6, t5, t4, dyn
	-[0x800007d8]:csrrs a2, fcsr, zero
	-[0x800007dc]:sw t6, 224(fp)
Current Store : [0x800007e0] : sw a2, 228(fp) -- Store: [0x800126bc]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x5cb815 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007f4]:fmul.s t6, t5, t4, dyn
	-[0x800007f8]:csrrs a2, fcsr, zero
	-[0x800007fc]:sw t6, 232(fp)
Current Store : [0x80000800] : sw a2, 236(fp) -- Store: [0x800126c4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3284ec and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000814]:fmul.s t6, t5, t4, dyn
	-[0x80000818]:csrrs a2, fcsr, zero
	-[0x8000081c]:sw t6, 240(fp)
Current Store : [0x80000820] : sw a2, 244(fp) -- Store: [0x800126cc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3284ec and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000834]:fmul.s t6, t5, t4, dyn
	-[0x80000838]:csrrs a2, fcsr, zero
	-[0x8000083c]:sw t6, 248(fp)
Current Store : [0x80000840] : sw a2, 252(fp) -- Store: [0x800126d4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3284ec and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000854]:fmul.s t6, t5, t4, dyn
	-[0x80000858]:csrrs a2, fcsr, zero
	-[0x8000085c]:sw t6, 256(fp)
Current Store : [0x80000860] : sw a2, 260(fp) -- Store: [0x800126dc]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3284ec and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000874]:fmul.s t6, t5, t4, dyn
	-[0x80000878]:csrrs a2, fcsr, zero
	-[0x8000087c]:sw t6, 264(fp)
Current Store : [0x80000880] : sw a2, 268(fp) -- Store: [0x800126e4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3284ec and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000894]:fmul.s t6, t5, t4, dyn
	-[0x80000898]:csrrs a2, fcsr, zero
	-[0x8000089c]:sw t6, 272(fp)
Current Store : [0x800008a0] : sw a2, 276(fp) -- Store: [0x800126ec]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x11ecfc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008b4]:fmul.s t6, t5, t4, dyn
	-[0x800008b8]:csrrs a2, fcsr, zero
	-[0x800008bc]:sw t6, 280(fp)
Current Store : [0x800008c0] : sw a2, 284(fp) -- Store: [0x800126f4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x11ecfc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008d4]:fmul.s t6, t5, t4, dyn
	-[0x800008d8]:csrrs a2, fcsr, zero
	-[0x800008dc]:sw t6, 288(fp)
Current Store : [0x800008e0] : sw a2, 292(fp) -- Store: [0x800126fc]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x11ecfc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008f4]:fmul.s t6, t5, t4, dyn
	-[0x800008f8]:csrrs a2, fcsr, zero
	-[0x800008fc]:sw t6, 296(fp)
Current Store : [0x80000900] : sw a2, 300(fp) -- Store: [0x80012704]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x11ecfc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000914]:fmul.s t6, t5, t4, dyn
	-[0x80000918]:csrrs a2, fcsr, zero
	-[0x8000091c]:sw t6, 304(fp)
Current Store : [0x80000920] : sw a2, 308(fp) -- Store: [0x8001270c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x11ecfc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000934]:fmul.s t6, t5, t4, dyn
	-[0x80000938]:csrrs a2, fcsr, zero
	-[0x8000093c]:sw t6, 312(fp)
Current Store : [0x80000940] : sw a2, 316(fp) -- Store: [0x80012714]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x49e399 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000954]:fmul.s t6, t5, t4, dyn
	-[0x80000958]:csrrs a2, fcsr, zero
	-[0x8000095c]:sw t6, 320(fp)
Current Store : [0x80000960] : sw a2, 324(fp) -- Store: [0x8001271c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x49e399 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000974]:fmul.s t6, t5, t4, dyn
	-[0x80000978]:csrrs a2, fcsr, zero
	-[0x8000097c]:sw t6, 328(fp)
Current Store : [0x80000980] : sw a2, 332(fp) -- Store: [0x80012724]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x49e399 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000994]:fmul.s t6, t5, t4, dyn
	-[0x80000998]:csrrs a2, fcsr, zero
	-[0x8000099c]:sw t6, 336(fp)
Current Store : [0x800009a0] : sw a2, 340(fp) -- Store: [0x8001272c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x49e399 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009b4]:fmul.s t6, t5, t4, dyn
	-[0x800009b8]:csrrs a2, fcsr, zero
	-[0x800009bc]:sw t6, 344(fp)
Current Store : [0x800009c0] : sw a2, 348(fp) -- Store: [0x80012734]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x49e399 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009d4]:fmul.s t6, t5, t4, dyn
	-[0x800009d8]:csrrs a2, fcsr, zero
	-[0x800009dc]:sw t6, 352(fp)
Current Store : [0x800009e0] : sw a2, 356(fp) -- Store: [0x8001273c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x3557bf and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009f4]:fmul.s t6, t5, t4, dyn
	-[0x800009f8]:csrrs a2, fcsr, zero
	-[0x800009fc]:sw t6, 360(fp)
Current Store : [0x80000a00] : sw a2, 364(fp) -- Store: [0x80012744]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x3557bf and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a14]:fmul.s t6, t5, t4, dyn
	-[0x80000a18]:csrrs a2, fcsr, zero
	-[0x80000a1c]:sw t6, 368(fp)
Current Store : [0x80000a20] : sw a2, 372(fp) -- Store: [0x8001274c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x3557bf and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a34]:fmul.s t6, t5, t4, dyn
	-[0x80000a38]:csrrs a2, fcsr, zero
	-[0x80000a3c]:sw t6, 376(fp)
Current Store : [0x80000a40] : sw a2, 380(fp) -- Store: [0x80012754]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x3557bf and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a54]:fmul.s t6, t5, t4, dyn
	-[0x80000a58]:csrrs a2, fcsr, zero
	-[0x80000a5c]:sw t6, 384(fp)
Current Store : [0x80000a60] : sw a2, 388(fp) -- Store: [0x8001275c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x3557bf and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a74]:fmul.s t6, t5, t4, dyn
	-[0x80000a78]:csrrs a2, fcsr, zero
	-[0x80000a7c]:sw t6, 392(fp)
Current Store : [0x80000a80] : sw a2, 396(fp) -- Store: [0x80012764]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x79b5b2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a94]:fmul.s t6, t5, t4, dyn
	-[0x80000a98]:csrrs a2, fcsr, zero
	-[0x80000a9c]:sw t6, 400(fp)
Current Store : [0x80000aa0] : sw a2, 404(fp) -- Store: [0x8001276c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x79b5b2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ab4]:fmul.s t6, t5, t4, dyn
	-[0x80000ab8]:csrrs a2, fcsr, zero
	-[0x80000abc]:sw t6, 408(fp)
Current Store : [0x80000ac0] : sw a2, 412(fp) -- Store: [0x80012774]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x79b5b2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ad4]:fmul.s t6, t5, t4, dyn
	-[0x80000ad8]:csrrs a2, fcsr, zero
	-[0x80000adc]:sw t6, 416(fp)
Current Store : [0x80000ae0] : sw a2, 420(fp) -- Store: [0x8001277c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x79b5b2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000af4]:fmul.s t6, t5, t4, dyn
	-[0x80000af8]:csrrs a2, fcsr, zero
	-[0x80000afc]:sw t6, 424(fp)
Current Store : [0x80000b00] : sw a2, 428(fp) -- Store: [0x80012784]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x79b5b2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b14]:fmul.s t6, t5, t4, dyn
	-[0x80000b18]:csrrs a2, fcsr, zero
	-[0x80000b1c]:sw t6, 432(fp)
Current Store : [0x80000b20] : sw a2, 436(fp) -- Store: [0x8001278c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x43ed0a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b34]:fmul.s t6, t5, t4, dyn
	-[0x80000b38]:csrrs a2, fcsr, zero
	-[0x80000b3c]:sw t6, 440(fp)
Current Store : [0x80000b40] : sw a2, 444(fp) -- Store: [0x80012794]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x43ed0a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b54]:fmul.s t6, t5, t4, dyn
	-[0x80000b58]:csrrs a2, fcsr, zero
	-[0x80000b5c]:sw t6, 448(fp)
Current Store : [0x80000b60] : sw a2, 452(fp) -- Store: [0x8001279c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x43ed0a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b74]:fmul.s t6, t5, t4, dyn
	-[0x80000b78]:csrrs a2, fcsr, zero
	-[0x80000b7c]:sw t6, 456(fp)
Current Store : [0x80000b80] : sw a2, 460(fp) -- Store: [0x800127a4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x43ed0a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b94]:fmul.s t6, t5, t4, dyn
	-[0x80000b98]:csrrs a2, fcsr, zero
	-[0x80000b9c]:sw t6, 464(fp)
Current Store : [0x80000ba0] : sw a2, 468(fp) -- Store: [0x800127ac]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x43ed0a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bb4]:fmul.s t6, t5, t4, dyn
	-[0x80000bb8]:csrrs a2, fcsr, zero
	-[0x80000bbc]:sw t6, 472(fp)
Current Store : [0x80000bc0] : sw a2, 476(fp) -- Store: [0x800127b4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x018006 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bd4]:fmul.s t6, t5, t4, dyn
	-[0x80000bd8]:csrrs a2, fcsr, zero
	-[0x80000bdc]:sw t6, 480(fp)
Current Store : [0x80000be0] : sw a2, 484(fp) -- Store: [0x800127bc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x018006 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bf4]:fmul.s t6, t5, t4, dyn
	-[0x80000bf8]:csrrs a2, fcsr, zero
	-[0x80000bfc]:sw t6, 488(fp)
Current Store : [0x80000c00] : sw a2, 492(fp) -- Store: [0x800127c4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x018006 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c14]:fmul.s t6, t5, t4, dyn
	-[0x80000c18]:csrrs a2, fcsr, zero
	-[0x80000c1c]:sw t6, 496(fp)
Current Store : [0x80000c20] : sw a2, 500(fp) -- Store: [0x800127cc]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x018006 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c34]:fmul.s t6, t5, t4, dyn
	-[0x80000c38]:csrrs a2, fcsr, zero
	-[0x80000c3c]:sw t6, 504(fp)
Current Store : [0x80000c40] : sw a2, 508(fp) -- Store: [0x800127d4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x018006 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c54]:fmul.s t6, t5, t4, dyn
	-[0x80000c58]:csrrs a2, fcsr, zero
	-[0x80000c5c]:sw t6, 512(fp)
Current Store : [0x80000c60] : sw a2, 516(fp) -- Store: [0x800127dc]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2b90d3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c74]:fmul.s t6, t5, t4, dyn
	-[0x80000c78]:csrrs a2, fcsr, zero
	-[0x80000c7c]:sw t6, 520(fp)
Current Store : [0x80000c80] : sw a2, 524(fp) -- Store: [0x800127e4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2b90d3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c94]:fmul.s t6, t5, t4, dyn
	-[0x80000c98]:csrrs a2, fcsr, zero
	-[0x80000c9c]:sw t6, 528(fp)
Current Store : [0x80000ca0] : sw a2, 532(fp) -- Store: [0x800127ec]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2b90d3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cb4]:fmul.s t6, t5, t4, dyn
	-[0x80000cb8]:csrrs a2, fcsr, zero
	-[0x80000cbc]:sw t6, 536(fp)
Current Store : [0x80000cc0] : sw a2, 540(fp) -- Store: [0x800127f4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2b90d3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cd4]:fmul.s t6, t5, t4, dyn
	-[0x80000cd8]:csrrs a2, fcsr, zero
	-[0x80000cdc]:sw t6, 544(fp)
Current Store : [0x80000ce0] : sw a2, 548(fp) -- Store: [0x800127fc]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2b90d3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cf4]:fmul.s t6, t5, t4, dyn
	-[0x80000cf8]:csrrs a2, fcsr, zero
	-[0x80000cfc]:sw t6, 552(fp)
Current Store : [0x80000d00] : sw a2, 556(fp) -- Store: [0x80012804]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0d014f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d14]:fmul.s t6, t5, t4, dyn
	-[0x80000d18]:csrrs a2, fcsr, zero
	-[0x80000d1c]:sw t6, 560(fp)
Current Store : [0x80000d20] : sw a2, 564(fp) -- Store: [0x8001280c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0d014f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d34]:fmul.s t6, t5, t4, dyn
	-[0x80000d38]:csrrs a2, fcsr, zero
	-[0x80000d3c]:sw t6, 568(fp)
Current Store : [0x80000d40] : sw a2, 572(fp) -- Store: [0x80012814]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0d014f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d54]:fmul.s t6, t5, t4, dyn
	-[0x80000d58]:csrrs a2, fcsr, zero
	-[0x80000d5c]:sw t6, 576(fp)
Current Store : [0x80000d60] : sw a2, 580(fp) -- Store: [0x8001281c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0d014f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d74]:fmul.s t6, t5, t4, dyn
	-[0x80000d78]:csrrs a2, fcsr, zero
	-[0x80000d7c]:sw t6, 584(fp)
Current Store : [0x80000d80] : sw a2, 588(fp) -- Store: [0x80012824]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0d014f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d94]:fmul.s t6, t5, t4, dyn
	-[0x80000d98]:csrrs a2, fcsr, zero
	-[0x80000d9c]:sw t6, 592(fp)
Current Store : [0x80000da0] : sw a2, 596(fp) -- Store: [0x8001282c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x127958 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000db4]:fmul.s t6, t5, t4, dyn
	-[0x80000db8]:csrrs a2, fcsr, zero
	-[0x80000dbc]:sw t6, 600(fp)
Current Store : [0x80000dc0] : sw a2, 604(fp) -- Store: [0x80012834]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x127958 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000dd4]:fmul.s t6, t5, t4, dyn
	-[0x80000dd8]:csrrs a2, fcsr, zero
	-[0x80000ddc]:sw t6, 608(fp)
Current Store : [0x80000de0] : sw a2, 612(fp) -- Store: [0x8001283c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x127958 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000df4]:fmul.s t6, t5, t4, dyn
	-[0x80000df8]:csrrs a2, fcsr, zero
	-[0x80000dfc]:sw t6, 616(fp)
Current Store : [0x80000e00] : sw a2, 620(fp) -- Store: [0x80012844]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x127958 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e14]:fmul.s t6, t5, t4, dyn
	-[0x80000e18]:csrrs a2, fcsr, zero
	-[0x80000e1c]:sw t6, 624(fp)
Current Store : [0x80000e20] : sw a2, 628(fp) -- Store: [0x8001284c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x127958 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e34]:fmul.s t6, t5, t4, dyn
	-[0x80000e38]:csrrs a2, fcsr, zero
	-[0x80000e3c]:sw t6, 632(fp)
Current Store : [0x80000e40] : sw a2, 636(fp) -- Store: [0x80012854]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x07daac and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e54]:fmul.s t6, t5, t4, dyn
	-[0x80000e58]:csrrs a2, fcsr, zero
	-[0x80000e5c]:sw t6, 640(fp)
Current Store : [0x80000e60] : sw a2, 644(fp) -- Store: [0x8001285c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x07daac and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e74]:fmul.s t6, t5, t4, dyn
	-[0x80000e78]:csrrs a2, fcsr, zero
	-[0x80000e7c]:sw t6, 648(fp)
Current Store : [0x80000e80] : sw a2, 652(fp) -- Store: [0x80012864]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x07daac and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e94]:fmul.s t6, t5, t4, dyn
	-[0x80000e98]:csrrs a2, fcsr, zero
	-[0x80000e9c]:sw t6, 656(fp)
Current Store : [0x80000ea0] : sw a2, 660(fp) -- Store: [0x8001286c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x07daac and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000eb4]:fmul.s t6, t5, t4, dyn
	-[0x80000eb8]:csrrs a2, fcsr, zero
	-[0x80000ebc]:sw t6, 664(fp)
Current Store : [0x80000ec0] : sw a2, 668(fp) -- Store: [0x80012874]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x07daac and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ed4]:fmul.s t6, t5, t4, dyn
	-[0x80000ed8]:csrrs a2, fcsr, zero
	-[0x80000edc]:sw t6, 672(fp)
Current Store : [0x80000ee0] : sw a2, 676(fp) -- Store: [0x8001287c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x38af5a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ef4]:fmul.s t6, t5, t4, dyn
	-[0x80000ef8]:csrrs a2, fcsr, zero
	-[0x80000efc]:sw t6, 680(fp)
Current Store : [0x80000f00] : sw a2, 684(fp) -- Store: [0x80012884]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x38af5a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f14]:fmul.s t6, t5, t4, dyn
	-[0x80000f18]:csrrs a2, fcsr, zero
	-[0x80000f1c]:sw t6, 688(fp)
Current Store : [0x80000f20] : sw a2, 692(fp) -- Store: [0x8001288c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x38af5a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f34]:fmul.s t6, t5, t4, dyn
	-[0x80000f38]:csrrs a2, fcsr, zero
	-[0x80000f3c]:sw t6, 696(fp)
Current Store : [0x80000f40] : sw a2, 700(fp) -- Store: [0x80012894]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x38af5a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f54]:fmul.s t6, t5, t4, dyn
	-[0x80000f58]:csrrs a2, fcsr, zero
	-[0x80000f5c]:sw t6, 704(fp)
Current Store : [0x80000f60] : sw a2, 708(fp) -- Store: [0x8001289c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x38af5a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f74]:fmul.s t6, t5, t4, dyn
	-[0x80000f78]:csrrs a2, fcsr, zero
	-[0x80000f7c]:sw t6, 712(fp)
Current Store : [0x80000f80] : sw a2, 716(fp) -- Store: [0x800128a4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x10c4ce and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f94]:fmul.s t6, t5, t4, dyn
	-[0x80000f98]:csrrs a2, fcsr, zero
	-[0x80000f9c]:sw t6, 720(fp)
Current Store : [0x80000fa0] : sw a2, 724(fp) -- Store: [0x800128ac]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x10c4ce and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fb4]:fmul.s t6, t5, t4, dyn
	-[0x80000fb8]:csrrs a2, fcsr, zero
	-[0x80000fbc]:sw t6, 728(fp)
Current Store : [0x80000fc0] : sw a2, 732(fp) -- Store: [0x800128b4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x10c4ce and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fd4]:fmul.s t6, t5, t4, dyn
	-[0x80000fd8]:csrrs a2, fcsr, zero
	-[0x80000fdc]:sw t6, 736(fp)
Current Store : [0x80000fe0] : sw a2, 740(fp) -- Store: [0x800128bc]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x10c4ce and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ff4]:fmul.s t6, t5, t4, dyn
	-[0x80000ff8]:csrrs a2, fcsr, zero
	-[0x80000ffc]:sw t6, 744(fp)
Current Store : [0x80001000] : sw a2, 748(fp) -- Store: [0x800128c4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x10c4ce and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001014]:fmul.s t6, t5, t4, dyn
	-[0x80001018]:csrrs a2, fcsr, zero
	-[0x8000101c]:sw t6, 752(fp)
Current Store : [0x80001020] : sw a2, 756(fp) -- Store: [0x800128cc]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x312e1f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001034]:fmul.s t6, t5, t4, dyn
	-[0x80001038]:csrrs a2, fcsr, zero
	-[0x8000103c]:sw t6, 760(fp)
Current Store : [0x80001040] : sw a2, 764(fp) -- Store: [0x800128d4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x312e1f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001054]:fmul.s t6, t5, t4, dyn
	-[0x80001058]:csrrs a2, fcsr, zero
	-[0x8000105c]:sw t6, 768(fp)
Current Store : [0x80001060] : sw a2, 772(fp) -- Store: [0x800128dc]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x312e1f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001074]:fmul.s t6, t5, t4, dyn
	-[0x80001078]:csrrs a2, fcsr, zero
	-[0x8000107c]:sw t6, 776(fp)
Current Store : [0x80001080] : sw a2, 780(fp) -- Store: [0x800128e4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x312e1f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001094]:fmul.s t6, t5, t4, dyn
	-[0x80001098]:csrrs a2, fcsr, zero
	-[0x8000109c]:sw t6, 784(fp)
Current Store : [0x800010a0] : sw a2, 788(fp) -- Store: [0x800128ec]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x312e1f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010b4]:fmul.s t6, t5, t4, dyn
	-[0x800010b8]:csrrs a2, fcsr, zero
	-[0x800010bc]:sw t6, 792(fp)
Current Store : [0x800010c0] : sw a2, 796(fp) -- Store: [0x800128f4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3c2a53 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010d4]:fmul.s t6, t5, t4, dyn
	-[0x800010d8]:csrrs a2, fcsr, zero
	-[0x800010dc]:sw t6, 800(fp)
Current Store : [0x800010e0] : sw a2, 804(fp) -- Store: [0x800128fc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3c2a53 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010f4]:fmul.s t6, t5, t4, dyn
	-[0x800010f8]:csrrs a2, fcsr, zero
	-[0x800010fc]:sw t6, 808(fp)
Current Store : [0x80001100] : sw a2, 812(fp) -- Store: [0x80012904]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3c2a53 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001114]:fmul.s t6, t5, t4, dyn
	-[0x80001118]:csrrs a2, fcsr, zero
	-[0x8000111c]:sw t6, 816(fp)
Current Store : [0x80001120] : sw a2, 820(fp) -- Store: [0x8001290c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3c2a53 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001134]:fmul.s t6, t5, t4, dyn
	-[0x80001138]:csrrs a2, fcsr, zero
	-[0x8000113c]:sw t6, 824(fp)
Current Store : [0x80001140] : sw a2, 828(fp) -- Store: [0x80012914]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3c2a53 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001154]:fmul.s t6, t5, t4, dyn
	-[0x80001158]:csrrs a2, fcsr, zero
	-[0x8000115c]:sw t6, 832(fp)
Current Store : [0x80001160] : sw a2, 836(fp) -- Store: [0x8001291c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4052ad and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001174]:fmul.s t6, t5, t4, dyn
	-[0x80001178]:csrrs a2, fcsr, zero
	-[0x8000117c]:sw t6, 840(fp)
Current Store : [0x80001180] : sw a2, 844(fp) -- Store: [0x80012924]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4052ad and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001194]:fmul.s t6, t5, t4, dyn
	-[0x80001198]:csrrs a2, fcsr, zero
	-[0x8000119c]:sw t6, 848(fp)
Current Store : [0x800011a0] : sw a2, 852(fp) -- Store: [0x8001292c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4052ad and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011b4]:fmul.s t6, t5, t4, dyn
	-[0x800011b8]:csrrs a2, fcsr, zero
	-[0x800011bc]:sw t6, 856(fp)
Current Store : [0x800011c0] : sw a2, 860(fp) -- Store: [0x80012934]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4052ad and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011d4]:fmul.s t6, t5, t4, dyn
	-[0x800011d8]:csrrs a2, fcsr, zero
	-[0x800011dc]:sw t6, 864(fp)
Current Store : [0x800011e0] : sw a2, 868(fp) -- Store: [0x8001293c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4052ad and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011f4]:fmul.s t6, t5, t4, dyn
	-[0x800011f8]:csrrs a2, fcsr, zero
	-[0x800011fc]:sw t6, 872(fp)
Current Store : [0x80001200] : sw a2, 876(fp) -- Store: [0x80012944]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x07e829 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001214]:fmul.s t6, t5, t4, dyn
	-[0x80001218]:csrrs a2, fcsr, zero
	-[0x8000121c]:sw t6, 880(fp)
Current Store : [0x80001220] : sw a2, 884(fp) -- Store: [0x8001294c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x07e829 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001234]:fmul.s t6, t5, t4, dyn
	-[0x80001238]:csrrs a2, fcsr, zero
	-[0x8000123c]:sw t6, 888(fp)
Current Store : [0x80001240] : sw a2, 892(fp) -- Store: [0x80012954]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x07e829 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001254]:fmul.s t6, t5, t4, dyn
	-[0x80001258]:csrrs a2, fcsr, zero
	-[0x8000125c]:sw t6, 896(fp)
Current Store : [0x80001260] : sw a2, 900(fp) -- Store: [0x8001295c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x07e829 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001274]:fmul.s t6, t5, t4, dyn
	-[0x80001278]:csrrs a2, fcsr, zero
	-[0x8000127c]:sw t6, 904(fp)
Current Store : [0x80001280] : sw a2, 908(fp) -- Store: [0x80012964]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x07e829 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001294]:fmul.s t6, t5, t4, dyn
	-[0x80001298]:csrrs a2, fcsr, zero
	-[0x8000129c]:sw t6, 912(fp)
Current Store : [0x800012a0] : sw a2, 916(fp) -- Store: [0x8001296c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2cbe14 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012b4]:fmul.s t6, t5, t4, dyn
	-[0x800012b8]:csrrs a2, fcsr, zero
	-[0x800012bc]:sw t6, 920(fp)
Current Store : [0x800012c0] : sw a2, 924(fp) -- Store: [0x80012974]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2cbe14 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012d4]:fmul.s t6, t5, t4, dyn
	-[0x800012d8]:csrrs a2, fcsr, zero
	-[0x800012dc]:sw t6, 928(fp)
Current Store : [0x800012e0] : sw a2, 932(fp) -- Store: [0x8001297c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2cbe14 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012f4]:fmul.s t6, t5, t4, dyn
	-[0x800012f8]:csrrs a2, fcsr, zero
	-[0x800012fc]:sw t6, 936(fp)
Current Store : [0x80001300] : sw a2, 940(fp) -- Store: [0x80012984]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2cbe14 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001314]:fmul.s t6, t5, t4, dyn
	-[0x80001318]:csrrs a2, fcsr, zero
	-[0x8000131c]:sw t6, 944(fp)
Current Store : [0x80001320] : sw a2, 948(fp) -- Store: [0x8001298c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2cbe14 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001334]:fmul.s t6, t5, t4, dyn
	-[0x80001338]:csrrs a2, fcsr, zero
	-[0x8000133c]:sw t6, 952(fp)
Current Store : [0x80001340] : sw a2, 956(fp) -- Store: [0x80012994]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x59ffad and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001354]:fmul.s t6, t5, t4, dyn
	-[0x80001358]:csrrs a2, fcsr, zero
	-[0x8000135c]:sw t6, 960(fp)
Current Store : [0x80001360] : sw a2, 964(fp) -- Store: [0x8001299c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x59ffad and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001374]:fmul.s t6, t5, t4, dyn
	-[0x80001378]:csrrs a2, fcsr, zero
	-[0x8000137c]:sw t6, 968(fp)
Current Store : [0x80001380] : sw a2, 972(fp) -- Store: [0x800129a4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x59ffad and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001394]:fmul.s t6, t5, t4, dyn
	-[0x80001398]:csrrs a2, fcsr, zero
	-[0x8000139c]:sw t6, 976(fp)
Current Store : [0x800013a0] : sw a2, 980(fp) -- Store: [0x800129ac]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x59ffad and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013b4]:fmul.s t6, t5, t4, dyn
	-[0x800013b8]:csrrs a2, fcsr, zero
	-[0x800013bc]:sw t6, 984(fp)
Current Store : [0x800013c0] : sw a2, 988(fp) -- Store: [0x800129b4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x59ffad and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013d4]:fmul.s t6, t5, t4, dyn
	-[0x800013d8]:csrrs a2, fcsr, zero
	-[0x800013dc]:sw t6, 992(fp)
Current Store : [0x800013e0] : sw a2, 996(fp) -- Store: [0x800129bc]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0af6e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013f4]:fmul.s t6, t5, t4, dyn
	-[0x800013f8]:csrrs a2, fcsr, zero
	-[0x800013fc]:sw t6, 1000(fp)
Current Store : [0x80001400] : sw a2, 1004(fp) -- Store: [0x800129c4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0af6e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001414]:fmul.s t6, t5, t4, dyn
	-[0x80001418]:csrrs a2, fcsr, zero
	-[0x8000141c]:sw t6, 1008(fp)
Current Store : [0x80001420] : sw a2, 1012(fp) -- Store: [0x800129cc]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0af6e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001434]:fmul.s t6, t5, t4, dyn
	-[0x80001438]:csrrs a2, fcsr, zero
	-[0x8000143c]:sw t6, 1016(fp)
Current Store : [0x80001440] : sw a2, 1020(fp) -- Store: [0x800129d4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0af6e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000145c]:fmul.s t6, t5, t4, dyn
	-[0x80001460]:csrrs a2, fcsr, zero
	-[0x80001464]:sw t6, 0(fp)
Current Store : [0x80001468] : sw a2, 4(fp) -- Store: [0x800129dc]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0af6e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000147c]:fmul.s t6, t5, t4, dyn
	-[0x80001480]:csrrs a2, fcsr, zero
	-[0x80001484]:sw t6, 8(fp)
Current Store : [0x80001488] : sw a2, 12(fp) -- Store: [0x800129e4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2a59d1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000149c]:fmul.s t6, t5, t4, dyn
	-[0x800014a0]:csrrs a2, fcsr, zero
	-[0x800014a4]:sw t6, 16(fp)
Current Store : [0x800014a8] : sw a2, 20(fp) -- Store: [0x800129ec]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2a59d1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014bc]:fmul.s t6, t5, t4, dyn
	-[0x800014c0]:csrrs a2, fcsr, zero
	-[0x800014c4]:sw t6, 24(fp)
Current Store : [0x800014c8] : sw a2, 28(fp) -- Store: [0x800129f4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2a59d1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014dc]:fmul.s t6, t5, t4, dyn
	-[0x800014e0]:csrrs a2, fcsr, zero
	-[0x800014e4]:sw t6, 32(fp)
Current Store : [0x800014e8] : sw a2, 36(fp) -- Store: [0x800129fc]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2a59d1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014fc]:fmul.s t6, t5, t4, dyn
	-[0x80001500]:csrrs a2, fcsr, zero
	-[0x80001504]:sw t6, 40(fp)
Current Store : [0x80001508] : sw a2, 44(fp) -- Store: [0x80012a04]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2a59d1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000151c]:fmul.s t6, t5, t4, dyn
	-[0x80001520]:csrrs a2, fcsr, zero
	-[0x80001524]:sw t6, 48(fp)
Current Store : [0x80001528] : sw a2, 52(fp) -- Store: [0x80012a0c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x065158 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000153c]:fmul.s t6, t5, t4, dyn
	-[0x80001540]:csrrs a2, fcsr, zero
	-[0x80001544]:sw t6, 56(fp)
Current Store : [0x80001548] : sw a2, 60(fp) -- Store: [0x80012a14]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x065158 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000155c]:fmul.s t6, t5, t4, dyn
	-[0x80001560]:csrrs a2, fcsr, zero
	-[0x80001564]:sw t6, 64(fp)
Current Store : [0x80001568] : sw a2, 68(fp) -- Store: [0x80012a1c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x065158 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000157c]:fmul.s t6, t5, t4, dyn
	-[0x80001580]:csrrs a2, fcsr, zero
	-[0x80001584]:sw t6, 72(fp)
Current Store : [0x80001588] : sw a2, 76(fp) -- Store: [0x80012a24]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x065158 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000159c]:fmul.s t6, t5, t4, dyn
	-[0x800015a0]:csrrs a2, fcsr, zero
	-[0x800015a4]:sw t6, 80(fp)
Current Store : [0x800015a8] : sw a2, 84(fp) -- Store: [0x80012a2c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x065158 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015bc]:fmul.s t6, t5, t4, dyn
	-[0x800015c0]:csrrs a2, fcsr, zero
	-[0x800015c4]:sw t6, 88(fp)
Current Store : [0x800015c8] : sw a2, 92(fp) -- Store: [0x80012a34]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2e5316 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015dc]:fmul.s t6, t5, t4, dyn
	-[0x800015e0]:csrrs a2, fcsr, zero
	-[0x800015e4]:sw t6, 96(fp)
Current Store : [0x800015e8] : sw a2, 100(fp) -- Store: [0x80012a3c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2e5316 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015fc]:fmul.s t6, t5, t4, dyn
	-[0x80001600]:csrrs a2, fcsr, zero
	-[0x80001604]:sw t6, 104(fp)
Current Store : [0x80001608] : sw a2, 108(fp) -- Store: [0x80012a44]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2e5316 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000161c]:fmul.s t6, t5, t4, dyn
	-[0x80001620]:csrrs a2, fcsr, zero
	-[0x80001624]:sw t6, 112(fp)
Current Store : [0x80001628] : sw a2, 116(fp) -- Store: [0x80012a4c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2e5316 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000163c]:fmul.s t6, t5, t4, dyn
	-[0x80001640]:csrrs a2, fcsr, zero
	-[0x80001644]:sw t6, 120(fp)
Current Store : [0x80001648] : sw a2, 124(fp) -- Store: [0x80012a54]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2e5316 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000165c]:fmul.s t6, t5, t4, dyn
	-[0x80001660]:csrrs a2, fcsr, zero
	-[0x80001664]:sw t6, 128(fp)
Current Store : [0x80001668] : sw a2, 132(fp) -- Store: [0x80012a5c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2efc0a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000167c]:fmul.s t6, t5, t4, dyn
	-[0x80001680]:csrrs a2, fcsr, zero
	-[0x80001684]:sw t6, 136(fp)
Current Store : [0x80001688] : sw a2, 140(fp) -- Store: [0x80012a64]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2efc0a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000169c]:fmul.s t6, t5, t4, dyn
	-[0x800016a0]:csrrs a2, fcsr, zero
	-[0x800016a4]:sw t6, 144(fp)
Current Store : [0x800016a8] : sw a2, 148(fp) -- Store: [0x80012a6c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2efc0a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016bc]:fmul.s t6, t5, t4, dyn
	-[0x800016c0]:csrrs a2, fcsr, zero
	-[0x800016c4]:sw t6, 152(fp)
Current Store : [0x800016c8] : sw a2, 156(fp) -- Store: [0x80012a74]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2efc0a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016dc]:fmul.s t6, t5, t4, dyn
	-[0x800016e0]:csrrs a2, fcsr, zero
	-[0x800016e4]:sw t6, 160(fp)
Current Store : [0x800016e8] : sw a2, 164(fp) -- Store: [0x80012a7c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2efc0a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016fc]:fmul.s t6, t5, t4, dyn
	-[0x80001700]:csrrs a2, fcsr, zero
	-[0x80001704]:sw t6, 168(fp)
Current Store : [0x80001708] : sw a2, 172(fp) -- Store: [0x80012a84]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x37d03d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000171c]:fmul.s t6, t5, t4, dyn
	-[0x80001720]:csrrs a2, fcsr, zero
	-[0x80001724]:sw t6, 176(fp)
Current Store : [0x80001728] : sw a2, 180(fp) -- Store: [0x80012a8c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x37d03d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000173c]:fmul.s t6, t5, t4, dyn
	-[0x80001740]:csrrs a2, fcsr, zero
	-[0x80001744]:sw t6, 184(fp)
Current Store : [0x80001748] : sw a2, 188(fp) -- Store: [0x80012a94]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x37d03d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000175c]:fmul.s t6, t5, t4, dyn
	-[0x80001760]:csrrs a2, fcsr, zero
	-[0x80001764]:sw t6, 192(fp)
Current Store : [0x80001768] : sw a2, 196(fp) -- Store: [0x80012a9c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x37d03d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000177c]:fmul.s t6, t5, t4, dyn
	-[0x80001780]:csrrs a2, fcsr, zero
	-[0x80001784]:sw t6, 200(fp)
Current Store : [0x80001788] : sw a2, 204(fp) -- Store: [0x80012aa4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x37d03d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000179c]:fmul.s t6, t5, t4, dyn
	-[0x800017a0]:csrrs a2, fcsr, zero
	-[0x800017a4]:sw t6, 208(fp)
Current Store : [0x800017a8] : sw a2, 212(fp) -- Store: [0x80012aac]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x41a1ac and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017bc]:fmul.s t6, t5, t4, dyn
	-[0x800017c0]:csrrs a2, fcsr, zero
	-[0x800017c4]:sw t6, 216(fp)
Current Store : [0x800017c8] : sw a2, 220(fp) -- Store: [0x80012ab4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x41a1ac and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017dc]:fmul.s t6, t5, t4, dyn
	-[0x800017e0]:csrrs a2, fcsr, zero
	-[0x800017e4]:sw t6, 224(fp)
Current Store : [0x800017e8] : sw a2, 228(fp) -- Store: [0x80012abc]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x41a1ac and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017fc]:fmul.s t6, t5, t4, dyn
	-[0x80001800]:csrrs a2, fcsr, zero
	-[0x80001804]:sw t6, 232(fp)
Current Store : [0x80001808] : sw a2, 236(fp) -- Store: [0x80012ac4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x41a1ac and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000181c]:fmul.s t6, t5, t4, dyn
	-[0x80001820]:csrrs a2, fcsr, zero
	-[0x80001824]:sw t6, 240(fp)
Current Store : [0x80001828] : sw a2, 244(fp) -- Store: [0x80012acc]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x41a1ac and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000183c]:fmul.s t6, t5, t4, dyn
	-[0x80001840]:csrrs a2, fcsr, zero
	-[0x80001844]:sw t6, 248(fp)
Current Store : [0x80001848] : sw a2, 252(fp) -- Store: [0x80012ad4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7a24a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000185c]:fmul.s t6, t5, t4, dyn
	-[0x80001860]:csrrs a2, fcsr, zero
	-[0x80001864]:sw t6, 256(fp)
Current Store : [0x80001868] : sw a2, 260(fp) -- Store: [0x80012adc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7a24a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000187c]:fmul.s t6, t5, t4, dyn
	-[0x80001880]:csrrs a2, fcsr, zero
	-[0x80001884]:sw t6, 264(fp)
Current Store : [0x80001888] : sw a2, 268(fp) -- Store: [0x80012ae4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7a24a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000189c]:fmul.s t6, t5, t4, dyn
	-[0x800018a0]:csrrs a2, fcsr, zero
	-[0x800018a4]:sw t6, 272(fp)
Current Store : [0x800018a8] : sw a2, 276(fp) -- Store: [0x80012aec]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7a24a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018bc]:fmul.s t6, t5, t4, dyn
	-[0x800018c0]:csrrs a2, fcsr, zero
	-[0x800018c4]:sw t6, 280(fp)
Current Store : [0x800018c8] : sw a2, 284(fp) -- Store: [0x80012af4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7a24a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018dc]:fmul.s t6, t5, t4, dyn
	-[0x800018e0]:csrrs a2, fcsr, zero
	-[0x800018e4]:sw t6, 288(fp)
Current Store : [0x800018e8] : sw a2, 292(fp) -- Store: [0x80012afc]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6f7f16 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018fc]:fmul.s t6, t5, t4, dyn
	-[0x80001900]:csrrs a2, fcsr, zero
	-[0x80001904]:sw t6, 296(fp)
Current Store : [0x80001908] : sw a2, 300(fp) -- Store: [0x80012b04]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6f7f16 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000191c]:fmul.s t6, t5, t4, dyn
	-[0x80001920]:csrrs a2, fcsr, zero
	-[0x80001924]:sw t6, 304(fp)
Current Store : [0x80001928] : sw a2, 308(fp) -- Store: [0x80012b0c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6f7f16 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000193c]:fmul.s t6, t5, t4, dyn
	-[0x80001940]:csrrs a2, fcsr, zero
	-[0x80001944]:sw t6, 312(fp)
Current Store : [0x80001948] : sw a2, 316(fp) -- Store: [0x80012b14]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6f7f16 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000195c]:fmul.s t6, t5, t4, dyn
	-[0x80001960]:csrrs a2, fcsr, zero
	-[0x80001964]:sw t6, 320(fp)
Current Store : [0x80001968] : sw a2, 324(fp) -- Store: [0x80012b1c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6f7f16 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000197c]:fmul.s t6, t5, t4, dyn
	-[0x80001980]:csrrs a2, fcsr, zero
	-[0x80001984]:sw t6, 328(fp)
Current Store : [0x80001988] : sw a2, 332(fp) -- Store: [0x80012b24]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4490fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000199c]:fmul.s t6, t5, t4, dyn
	-[0x800019a0]:csrrs a2, fcsr, zero
	-[0x800019a4]:sw t6, 336(fp)
Current Store : [0x800019a8] : sw a2, 340(fp) -- Store: [0x80012b2c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4490fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800019bc]:fmul.s t6, t5, t4, dyn
	-[0x800019c0]:csrrs a2, fcsr, zero
	-[0x800019c4]:sw t6, 344(fp)
Current Store : [0x800019c8] : sw a2, 348(fp) -- Store: [0x80012b34]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4490fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800019dc]:fmul.s t6, t5, t4, dyn
	-[0x800019e0]:csrrs a2, fcsr, zero
	-[0x800019e4]:sw t6, 352(fp)
Current Store : [0x800019e8] : sw a2, 356(fp) -- Store: [0x80012b3c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4490fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800019fc]:fmul.s t6, t5, t4, dyn
	-[0x80001a00]:csrrs a2, fcsr, zero
	-[0x80001a04]:sw t6, 360(fp)
Current Store : [0x80001a08] : sw a2, 364(fp) -- Store: [0x80012b44]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4490fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a1c]:fmul.s t6, t5, t4, dyn
	-[0x80001a20]:csrrs a2, fcsr, zero
	-[0x80001a24]:sw t6, 368(fp)
Current Store : [0x80001a28] : sw a2, 372(fp) -- Store: [0x80012b4c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6f5572 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a3c]:fmul.s t6, t5, t4, dyn
	-[0x80001a40]:csrrs a2, fcsr, zero
	-[0x80001a44]:sw t6, 376(fp)
Current Store : [0x80001a48] : sw a2, 380(fp) -- Store: [0x80012b54]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6f5572 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a5c]:fmul.s t6, t5, t4, dyn
	-[0x80001a60]:csrrs a2, fcsr, zero
	-[0x80001a64]:sw t6, 384(fp)
Current Store : [0x80001a68] : sw a2, 388(fp) -- Store: [0x80012b5c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6f5572 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a7c]:fmul.s t6, t5, t4, dyn
	-[0x80001a80]:csrrs a2, fcsr, zero
	-[0x80001a84]:sw t6, 392(fp)
Current Store : [0x80001a88] : sw a2, 396(fp) -- Store: [0x80012b64]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6f5572 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a9c]:fmul.s t6, t5, t4, dyn
	-[0x80001aa0]:csrrs a2, fcsr, zero
	-[0x80001aa4]:sw t6, 400(fp)
Current Store : [0x80001aa8] : sw a2, 404(fp) -- Store: [0x80012b6c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6f5572 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001abc]:fmul.s t6, t5, t4, dyn
	-[0x80001ac0]:csrrs a2, fcsr, zero
	-[0x80001ac4]:sw t6, 408(fp)
Current Store : [0x80001ac8] : sw a2, 412(fp) -- Store: [0x80012b74]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d844c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001adc]:fmul.s t6, t5, t4, dyn
	-[0x80001ae0]:csrrs a2, fcsr, zero
	-[0x80001ae4]:sw t6, 416(fp)
Current Store : [0x80001ae8] : sw a2, 420(fp) -- Store: [0x80012b7c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d844c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001afc]:fmul.s t6, t5, t4, dyn
	-[0x80001b00]:csrrs a2, fcsr, zero
	-[0x80001b04]:sw t6, 424(fp)
Current Store : [0x80001b08] : sw a2, 428(fp) -- Store: [0x80012b84]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d844c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b1c]:fmul.s t6, t5, t4, dyn
	-[0x80001b20]:csrrs a2, fcsr, zero
	-[0x80001b24]:sw t6, 432(fp)
Current Store : [0x80001b28] : sw a2, 436(fp) -- Store: [0x80012b8c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d844c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b3c]:fmul.s t6, t5, t4, dyn
	-[0x80001b40]:csrrs a2, fcsr, zero
	-[0x80001b44]:sw t6, 440(fp)
Current Store : [0x80001b48] : sw a2, 444(fp) -- Store: [0x80012b94]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d844c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b5c]:fmul.s t6, t5, t4, dyn
	-[0x80001b60]:csrrs a2, fcsr, zero
	-[0x80001b64]:sw t6, 448(fp)
Current Store : [0x80001b68] : sw a2, 452(fp) -- Store: [0x80012b9c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2e44fd and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b7c]:fmul.s t6, t5, t4, dyn
	-[0x80001b80]:csrrs a2, fcsr, zero
	-[0x80001b84]:sw t6, 456(fp)
Current Store : [0x80001b88] : sw a2, 460(fp) -- Store: [0x80012ba4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2e44fd and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b9c]:fmul.s t6, t5, t4, dyn
	-[0x80001ba0]:csrrs a2, fcsr, zero
	-[0x80001ba4]:sw t6, 464(fp)
Current Store : [0x80001ba8] : sw a2, 468(fp) -- Store: [0x80012bac]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2e44fd and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001bbc]:fmul.s t6, t5, t4, dyn
	-[0x80001bc0]:csrrs a2, fcsr, zero
	-[0x80001bc4]:sw t6, 472(fp)
Current Store : [0x80001bc8] : sw a2, 476(fp) -- Store: [0x80012bb4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2e44fd and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001bdc]:fmul.s t6, t5, t4, dyn
	-[0x80001be0]:csrrs a2, fcsr, zero
	-[0x80001be4]:sw t6, 480(fp)
Current Store : [0x80001be8] : sw a2, 484(fp) -- Store: [0x80012bbc]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2e44fd and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001bfc]:fmul.s t6, t5, t4, dyn
	-[0x80001c00]:csrrs a2, fcsr, zero
	-[0x80001c04]:sw t6, 488(fp)
Current Store : [0x80001c08] : sw a2, 492(fp) -- Store: [0x80012bc4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0eff8f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c1c]:fmul.s t6, t5, t4, dyn
	-[0x80001c20]:csrrs a2, fcsr, zero
	-[0x80001c24]:sw t6, 496(fp)
Current Store : [0x80001c28] : sw a2, 500(fp) -- Store: [0x80012bcc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0eff8f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c3c]:fmul.s t6, t5, t4, dyn
	-[0x80001c40]:csrrs a2, fcsr, zero
	-[0x80001c44]:sw t6, 504(fp)
Current Store : [0x80001c48] : sw a2, 508(fp) -- Store: [0x80012bd4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0eff8f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c5c]:fmul.s t6, t5, t4, dyn
	-[0x80001c60]:csrrs a2, fcsr, zero
	-[0x80001c64]:sw t6, 512(fp)
Current Store : [0x80001c68] : sw a2, 516(fp) -- Store: [0x80012bdc]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0eff8f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c7c]:fmul.s t6, t5, t4, dyn
	-[0x80001c80]:csrrs a2, fcsr, zero
	-[0x80001c84]:sw t6, 520(fp)
Current Store : [0x80001c88] : sw a2, 524(fp) -- Store: [0x80012be4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0eff8f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c9c]:fmul.s t6, t5, t4, dyn
	-[0x80001ca0]:csrrs a2, fcsr, zero
	-[0x80001ca4]:sw t6, 528(fp)
Current Store : [0x80001ca8] : sw a2, 532(fp) -- Store: [0x80012bec]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7ab7a7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001cbc]:fmul.s t6, t5, t4, dyn
	-[0x80001cc0]:csrrs a2, fcsr, zero
	-[0x80001cc4]:sw t6, 536(fp)
Current Store : [0x80001cc8] : sw a2, 540(fp) -- Store: [0x80012bf4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7ab7a7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001cdc]:fmul.s t6, t5, t4, dyn
	-[0x80001ce0]:csrrs a2, fcsr, zero
	-[0x80001ce4]:sw t6, 544(fp)
Current Store : [0x80001ce8] : sw a2, 548(fp) -- Store: [0x80012bfc]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7ab7a7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001cfc]:fmul.s t6, t5, t4, dyn
	-[0x80001d00]:csrrs a2, fcsr, zero
	-[0x80001d04]:sw t6, 552(fp)
Current Store : [0x80001d08] : sw a2, 556(fp) -- Store: [0x80012c04]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7ab7a7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d1c]:fmul.s t6, t5, t4, dyn
	-[0x80001d20]:csrrs a2, fcsr, zero
	-[0x80001d24]:sw t6, 560(fp)
Current Store : [0x80001d28] : sw a2, 564(fp) -- Store: [0x80012c0c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7ab7a7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d3c]:fmul.s t6, t5, t4, dyn
	-[0x80001d40]:csrrs a2, fcsr, zero
	-[0x80001d44]:sw t6, 568(fp)
Current Store : [0x80001d48] : sw a2, 572(fp) -- Store: [0x80012c14]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2e0a9e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d5c]:fmul.s t6, t5, t4, dyn
	-[0x80001d60]:csrrs a2, fcsr, zero
	-[0x80001d64]:sw t6, 576(fp)
Current Store : [0x80001d68] : sw a2, 580(fp) -- Store: [0x80012c1c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2e0a9e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d7c]:fmul.s t6, t5, t4, dyn
	-[0x80001d80]:csrrs a2, fcsr, zero
	-[0x80001d84]:sw t6, 584(fp)
Current Store : [0x80001d88] : sw a2, 588(fp) -- Store: [0x80012c24]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2e0a9e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d9c]:fmul.s t6, t5, t4, dyn
	-[0x80001da0]:csrrs a2, fcsr, zero
	-[0x80001da4]:sw t6, 592(fp)
Current Store : [0x80001da8] : sw a2, 596(fp) -- Store: [0x80012c2c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2e0a9e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001dbc]:fmul.s t6, t5, t4, dyn
	-[0x80001dc0]:csrrs a2, fcsr, zero
	-[0x80001dc4]:sw t6, 600(fp)
Current Store : [0x80001dc8] : sw a2, 604(fp) -- Store: [0x80012c34]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2e0a9e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ddc]:fmul.s t6, t5, t4, dyn
	-[0x80001de0]:csrrs a2, fcsr, zero
	-[0x80001de4]:sw t6, 608(fp)
Current Store : [0x80001de8] : sw a2, 612(fp) -- Store: [0x80012c3c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x23397b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001dfc]:fmul.s t6, t5, t4, dyn
	-[0x80001e00]:csrrs a2, fcsr, zero
	-[0x80001e04]:sw t6, 616(fp)
Current Store : [0x80001e08] : sw a2, 620(fp) -- Store: [0x80012c44]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x23397b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e1c]:fmul.s t6, t5, t4, dyn
	-[0x80001e20]:csrrs a2, fcsr, zero
	-[0x80001e24]:sw t6, 624(fp)
Current Store : [0x80001e28] : sw a2, 628(fp) -- Store: [0x80012c4c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x23397b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e3c]:fmul.s t6, t5, t4, dyn
	-[0x80001e40]:csrrs a2, fcsr, zero
	-[0x80001e44]:sw t6, 632(fp)
Current Store : [0x80001e48] : sw a2, 636(fp) -- Store: [0x80012c54]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x23397b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e5c]:fmul.s t6, t5, t4, dyn
	-[0x80001e60]:csrrs a2, fcsr, zero
	-[0x80001e64]:sw t6, 640(fp)
Current Store : [0x80001e68] : sw a2, 644(fp) -- Store: [0x80012c5c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x23397b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e7c]:fmul.s t6, t5, t4, dyn
	-[0x80001e80]:csrrs a2, fcsr, zero
	-[0x80001e84]:sw t6, 648(fp)
Current Store : [0x80001e88] : sw a2, 652(fp) -- Store: [0x80012c64]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x23f501 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e9c]:fmul.s t6, t5, t4, dyn
	-[0x80001ea0]:csrrs a2, fcsr, zero
	-[0x80001ea4]:sw t6, 656(fp)
Current Store : [0x80001ea8] : sw a2, 660(fp) -- Store: [0x80012c6c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x23f501 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ebc]:fmul.s t6, t5, t4, dyn
	-[0x80001ec0]:csrrs a2, fcsr, zero
	-[0x80001ec4]:sw t6, 664(fp)
Current Store : [0x80001ec8] : sw a2, 668(fp) -- Store: [0x80012c74]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x23f501 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001edc]:fmul.s t6, t5, t4, dyn
	-[0x80001ee0]:csrrs a2, fcsr, zero
	-[0x80001ee4]:sw t6, 672(fp)
Current Store : [0x80001ee8] : sw a2, 676(fp) -- Store: [0x80012c7c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x23f501 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001efc]:fmul.s t6, t5, t4, dyn
	-[0x80001f00]:csrrs a2, fcsr, zero
	-[0x80001f04]:sw t6, 680(fp)
Current Store : [0x80001f08] : sw a2, 684(fp) -- Store: [0x80012c84]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x23f501 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f1c]:fmul.s t6, t5, t4, dyn
	-[0x80001f20]:csrrs a2, fcsr, zero
	-[0x80001f24]:sw t6, 688(fp)
Current Store : [0x80001f28] : sw a2, 692(fp) -- Store: [0x80012c8c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x21a5d7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f3c]:fmul.s t6, t5, t4, dyn
	-[0x80001f40]:csrrs a2, fcsr, zero
	-[0x80001f44]:sw t6, 696(fp)
Current Store : [0x80001f48] : sw a2, 700(fp) -- Store: [0x80012c94]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x21a5d7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f5c]:fmul.s t6, t5, t4, dyn
	-[0x80001f60]:csrrs a2, fcsr, zero
	-[0x80001f64]:sw t6, 704(fp)
Current Store : [0x80001f68] : sw a2, 708(fp) -- Store: [0x80012c9c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x21a5d7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f7c]:fmul.s t6, t5, t4, dyn
	-[0x80001f80]:csrrs a2, fcsr, zero
	-[0x80001f84]:sw t6, 712(fp)
Current Store : [0x80001f88] : sw a2, 716(fp) -- Store: [0x80012ca4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x21a5d7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f9c]:fmul.s t6, t5, t4, dyn
	-[0x80001fa0]:csrrs a2, fcsr, zero
	-[0x80001fa4]:sw t6, 720(fp)
Current Store : [0x80001fa8] : sw a2, 724(fp) -- Store: [0x80012cac]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x21a5d7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001fbc]:fmul.s t6, t5, t4, dyn
	-[0x80001fc0]:csrrs a2, fcsr, zero
	-[0x80001fc4]:sw t6, 728(fp)
Current Store : [0x80001fc8] : sw a2, 732(fp) -- Store: [0x80012cb4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x22667e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001fdc]:fmul.s t6, t5, t4, dyn
	-[0x80001fe0]:csrrs a2, fcsr, zero
	-[0x80001fe4]:sw t6, 736(fp)
Current Store : [0x80001fe8] : sw a2, 740(fp) -- Store: [0x80012cbc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x22667e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ffc]:fmul.s t6, t5, t4, dyn
	-[0x80002000]:csrrs a2, fcsr, zero
	-[0x80002004]:sw t6, 744(fp)
Current Store : [0x80002008] : sw a2, 748(fp) -- Store: [0x80012cc4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x22667e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000201c]:fmul.s t6, t5, t4, dyn
	-[0x80002020]:csrrs a2, fcsr, zero
	-[0x80002024]:sw t6, 752(fp)
Current Store : [0x80002028] : sw a2, 756(fp) -- Store: [0x80012ccc]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x22667e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000203c]:fmul.s t6, t5, t4, dyn
	-[0x80002040]:csrrs a2, fcsr, zero
	-[0x80002044]:sw t6, 760(fp)
Current Store : [0x80002048] : sw a2, 764(fp) -- Store: [0x80012cd4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x22667e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000205c]:fmul.s t6, t5, t4, dyn
	-[0x80002060]:csrrs a2, fcsr, zero
	-[0x80002064]:sw t6, 768(fp)
Current Store : [0x80002068] : sw a2, 772(fp) -- Store: [0x80012cdc]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x43d400 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000207c]:fmul.s t6, t5, t4, dyn
	-[0x80002080]:csrrs a2, fcsr, zero
	-[0x80002084]:sw t6, 776(fp)
Current Store : [0x80002088] : sw a2, 780(fp) -- Store: [0x80012ce4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x43d400 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000209c]:fmul.s t6, t5, t4, dyn
	-[0x800020a0]:csrrs a2, fcsr, zero
	-[0x800020a4]:sw t6, 784(fp)
Current Store : [0x800020a8] : sw a2, 788(fp) -- Store: [0x80012cec]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x43d400 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800020bc]:fmul.s t6, t5, t4, dyn
	-[0x800020c0]:csrrs a2, fcsr, zero
	-[0x800020c4]:sw t6, 792(fp)
Current Store : [0x800020c8] : sw a2, 796(fp) -- Store: [0x80012cf4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x43d400 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800020dc]:fmul.s t6, t5, t4, dyn
	-[0x800020e0]:csrrs a2, fcsr, zero
	-[0x800020e4]:sw t6, 800(fp)
Current Store : [0x800020e8] : sw a2, 804(fp) -- Store: [0x80012cfc]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x43d400 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800020fc]:fmul.s t6, t5, t4, dyn
	-[0x80002100]:csrrs a2, fcsr, zero
	-[0x80002104]:sw t6, 808(fp)
Current Store : [0x80002108] : sw a2, 812(fp) -- Store: [0x80012d04]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x04012d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000211c]:fmul.s t6, t5, t4, dyn
	-[0x80002120]:csrrs a2, fcsr, zero
	-[0x80002124]:sw t6, 816(fp)
Current Store : [0x80002128] : sw a2, 820(fp) -- Store: [0x80012d0c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x04012d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000213c]:fmul.s t6, t5, t4, dyn
	-[0x80002140]:csrrs a2, fcsr, zero
	-[0x80002144]:sw t6, 824(fp)
Current Store : [0x80002148] : sw a2, 828(fp) -- Store: [0x80012d14]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x04012d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000215c]:fmul.s t6, t5, t4, dyn
	-[0x80002160]:csrrs a2, fcsr, zero
	-[0x80002164]:sw t6, 832(fp)
Current Store : [0x80002168] : sw a2, 836(fp) -- Store: [0x80012d1c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x04012d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000217c]:fmul.s t6, t5, t4, dyn
	-[0x80002180]:csrrs a2, fcsr, zero
	-[0x80002184]:sw t6, 840(fp)
Current Store : [0x80002188] : sw a2, 844(fp) -- Store: [0x80012d24]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x04012d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000219c]:fmul.s t6, t5, t4, dyn
	-[0x800021a0]:csrrs a2, fcsr, zero
	-[0x800021a4]:sw t6, 848(fp)
Current Store : [0x800021a8] : sw a2, 852(fp) -- Store: [0x80012d2c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x7eadb5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800021bc]:fmul.s t6, t5, t4, dyn
	-[0x800021c0]:csrrs a2, fcsr, zero
	-[0x800021c4]:sw t6, 856(fp)
Current Store : [0x800021c8] : sw a2, 860(fp) -- Store: [0x80012d34]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x7eadb5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800021dc]:fmul.s t6, t5, t4, dyn
	-[0x800021e0]:csrrs a2, fcsr, zero
	-[0x800021e4]:sw t6, 864(fp)
Current Store : [0x800021e8] : sw a2, 868(fp) -- Store: [0x80012d3c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x7eadb5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800021fc]:fmul.s t6, t5, t4, dyn
	-[0x80002200]:csrrs a2, fcsr, zero
	-[0x80002204]:sw t6, 872(fp)
Current Store : [0x80002208] : sw a2, 876(fp) -- Store: [0x80012d44]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x7eadb5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000221c]:fmul.s t6, t5, t4, dyn
	-[0x80002220]:csrrs a2, fcsr, zero
	-[0x80002224]:sw t6, 880(fp)
Current Store : [0x80002228] : sw a2, 884(fp) -- Store: [0x80012d4c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x7eadb5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000223c]:fmul.s t6, t5, t4, dyn
	-[0x80002240]:csrrs a2, fcsr, zero
	-[0x80002244]:sw t6, 888(fp)
Current Store : [0x80002248] : sw a2, 892(fp) -- Store: [0x80012d54]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x1bb7c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000225c]:fmul.s t6, t5, t4, dyn
	-[0x80002260]:csrrs a2, fcsr, zero
	-[0x80002264]:sw t6, 896(fp)
Current Store : [0x80002268] : sw a2, 900(fp) -- Store: [0x80012d5c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x1bb7c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000227c]:fmul.s t6, t5, t4, dyn
	-[0x80002280]:csrrs a2, fcsr, zero
	-[0x80002284]:sw t6, 904(fp)
Current Store : [0x80002288] : sw a2, 908(fp) -- Store: [0x80012d64]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x1bb7c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000229c]:fmul.s t6, t5, t4, dyn
	-[0x800022a0]:csrrs a2, fcsr, zero
	-[0x800022a4]:sw t6, 912(fp)
Current Store : [0x800022a8] : sw a2, 916(fp) -- Store: [0x80012d6c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x1bb7c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800022bc]:fmul.s t6, t5, t4, dyn
	-[0x800022c0]:csrrs a2, fcsr, zero
	-[0x800022c4]:sw t6, 920(fp)
Current Store : [0x800022c8] : sw a2, 924(fp) -- Store: [0x80012d74]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x1bb7c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800022dc]:fmul.s t6, t5, t4, dyn
	-[0x800022e0]:csrrs a2, fcsr, zero
	-[0x800022e4]:sw t6, 928(fp)
Current Store : [0x800022e8] : sw a2, 932(fp) -- Store: [0x80012d7c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x01ea00 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800022fc]:fmul.s t6, t5, t4, dyn
	-[0x80002300]:csrrs a2, fcsr, zero
	-[0x80002304]:sw t6, 936(fp)
Current Store : [0x80002308] : sw a2, 940(fp) -- Store: [0x80012d84]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x01ea00 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000231c]:fmul.s t6, t5, t4, dyn
	-[0x80002320]:csrrs a2, fcsr, zero
	-[0x80002324]:sw t6, 944(fp)
Current Store : [0x80002328] : sw a2, 948(fp) -- Store: [0x80012d8c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x01ea00 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000233c]:fmul.s t6, t5, t4, dyn
	-[0x80002340]:csrrs a2, fcsr, zero
	-[0x80002344]:sw t6, 952(fp)
Current Store : [0x80002348] : sw a2, 956(fp) -- Store: [0x80012d94]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x01ea00 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000235c]:fmul.s t6, t5, t4, dyn
	-[0x80002360]:csrrs a2, fcsr, zero
	-[0x80002364]:sw t6, 960(fp)
Current Store : [0x80002368] : sw a2, 964(fp) -- Store: [0x80012d9c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x01ea00 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000237c]:fmul.s t6, t5, t4, dyn
	-[0x80002380]:csrrs a2, fcsr, zero
	-[0x80002384]:sw t6, 968(fp)
Current Store : [0x80002388] : sw a2, 972(fp) -- Store: [0x80012da4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1c29fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000239c]:fmul.s t6, t5, t4, dyn
	-[0x800023a0]:csrrs a2, fcsr, zero
	-[0x800023a4]:sw t6, 976(fp)
Current Store : [0x800023a8] : sw a2, 980(fp) -- Store: [0x80012dac]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1c29fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800023bc]:fmul.s t6, t5, t4, dyn
	-[0x800023c0]:csrrs a2, fcsr, zero
	-[0x800023c4]:sw t6, 984(fp)
Current Store : [0x800023c8] : sw a2, 988(fp) -- Store: [0x80012db4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1c29fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800023dc]:fmul.s t6, t5, t4, dyn
	-[0x800023e0]:csrrs a2, fcsr, zero
	-[0x800023e4]:sw t6, 992(fp)
Current Store : [0x800023e8] : sw a2, 996(fp) -- Store: [0x80012dbc]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1c29fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000241c]:fmul.s t6, t5, t4, dyn
	-[0x80002420]:csrrs a2, fcsr, zero
	-[0x80002424]:sw t6, 1000(fp)
Current Store : [0x80002428] : sw a2, 1004(fp) -- Store: [0x80012dc4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1c29fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000245c]:fmul.s t6, t5, t4, dyn
	-[0x80002460]:csrrs a2, fcsr, zero
	-[0x80002464]:sw t6, 1008(fp)
Current Store : [0x80002468] : sw a2, 1012(fp) -- Store: [0x80012dcc]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x0d1c84 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000249c]:fmul.s t6, t5, t4, dyn
	-[0x800024a0]:csrrs a2, fcsr, zero
	-[0x800024a4]:sw t6, 1016(fp)
Current Store : [0x800024a8] : sw a2, 1020(fp) -- Store: [0x80012dd4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x0d1c84 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800024e4]:fmul.s t6, t5, t4, dyn
	-[0x800024e8]:csrrs a2, fcsr, zero
	-[0x800024ec]:sw t6, 0(fp)
Current Store : [0x800024f0] : sw a2, 4(fp) -- Store: [0x80012ddc]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x0d1c84 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002524]:fmul.s t6, t5, t4, dyn
	-[0x80002528]:csrrs a2, fcsr, zero
	-[0x8000252c]:sw t6, 8(fp)
Current Store : [0x80002530] : sw a2, 12(fp) -- Store: [0x80012de4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x0d1c84 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002564]:fmul.s t6, t5, t4, dyn
	-[0x80002568]:csrrs a2, fcsr, zero
	-[0x8000256c]:sw t6, 16(fp)
Current Store : [0x80002570] : sw a2, 20(fp) -- Store: [0x80012dec]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x0d1c84 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800025a4]:fmul.s t6, t5, t4, dyn
	-[0x800025a8]:csrrs a2, fcsr, zero
	-[0x800025ac]:sw t6, 24(fp)
Current Store : [0x800025b0] : sw a2, 28(fp) -- Store: [0x80012df4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x140eaa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800025e4]:fmul.s t6, t5, t4, dyn
	-[0x800025e8]:csrrs a2, fcsr, zero
	-[0x800025ec]:sw t6, 32(fp)
Current Store : [0x800025f0] : sw a2, 36(fp) -- Store: [0x80012dfc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x140eaa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002624]:fmul.s t6, t5, t4, dyn
	-[0x80002628]:csrrs a2, fcsr, zero
	-[0x8000262c]:sw t6, 40(fp)
Current Store : [0x80002630] : sw a2, 44(fp) -- Store: [0x80012e04]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x140eaa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002664]:fmul.s t6, t5, t4, dyn
	-[0x80002668]:csrrs a2, fcsr, zero
	-[0x8000266c]:sw t6, 48(fp)
Current Store : [0x80002670] : sw a2, 52(fp) -- Store: [0x80012e0c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x140eaa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800026a4]:fmul.s t6, t5, t4, dyn
	-[0x800026a8]:csrrs a2, fcsr, zero
	-[0x800026ac]:sw t6, 56(fp)
Current Store : [0x800026b0] : sw a2, 60(fp) -- Store: [0x80012e14]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x140eaa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800026e4]:fmul.s t6, t5, t4, dyn
	-[0x800026e8]:csrrs a2, fcsr, zero
	-[0x800026ec]:sw t6, 64(fp)
Current Store : [0x800026f0] : sw a2, 68(fp) -- Store: [0x80012e1c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3e148d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002724]:fmul.s t6, t5, t4, dyn
	-[0x80002728]:csrrs a2, fcsr, zero
	-[0x8000272c]:sw t6, 72(fp)
Current Store : [0x80002730] : sw a2, 76(fp) -- Store: [0x80012e24]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3e148d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002764]:fmul.s t6, t5, t4, dyn
	-[0x80002768]:csrrs a2, fcsr, zero
	-[0x8000276c]:sw t6, 80(fp)
Current Store : [0x80002770] : sw a2, 84(fp) -- Store: [0x80012e2c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3e148d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800027a4]:fmul.s t6, t5, t4, dyn
	-[0x800027a8]:csrrs a2, fcsr, zero
	-[0x800027ac]:sw t6, 88(fp)
Current Store : [0x800027b0] : sw a2, 92(fp) -- Store: [0x80012e34]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3e148d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800027e4]:fmul.s t6, t5, t4, dyn
	-[0x800027e8]:csrrs a2, fcsr, zero
	-[0x800027ec]:sw t6, 96(fp)
Current Store : [0x800027f0] : sw a2, 100(fp) -- Store: [0x80012e3c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3e148d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002824]:fmul.s t6, t5, t4, dyn
	-[0x80002828]:csrrs a2, fcsr, zero
	-[0x8000282c]:sw t6, 104(fp)
Current Store : [0x80002830] : sw a2, 108(fp) -- Store: [0x80012e44]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x5ed631 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002864]:fmul.s t6, t5, t4, dyn
	-[0x80002868]:csrrs a2, fcsr, zero
	-[0x8000286c]:sw t6, 112(fp)
Current Store : [0x80002870] : sw a2, 116(fp) -- Store: [0x80012e4c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x5ed631 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800028a4]:fmul.s t6, t5, t4, dyn
	-[0x800028a8]:csrrs a2, fcsr, zero
	-[0x800028ac]:sw t6, 120(fp)
Current Store : [0x800028b0] : sw a2, 124(fp) -- Store: [0x80012e54]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x5ed631 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800028e4]:fmul.s t6, t5, t4, dyn
	-[0x800028e8]:csrrs a2, fcsr, zero
	-[0x800028ec]:sw t6, 128(fp)
Current Store : [0x800028f0] : sw a2, 132(fp) -- Store: [0x80012e5c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x5ed631 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002924]:fmul.s t6, t5, t4, dyn
	-[0x80002928]:csrrs a2, fcsr, zero
	-[0x8000292c]:sw t6, 136(fp)
Current Store : [0x80002930] : sw a2, 140(fp) -- Store: [0x80012e64]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x5ed631 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002964]:fmul.s t6, t5, t4, dyn
	-[0x80002968]:csrrs a2, fcsr, zero
	-[0x8000296c]:sw t6, 144(fp)
Current Store : [0x80002970] : sw a2, 148(fp) -- Store: [0x80012e6c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6c407f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800029a4]:fmul.s t6, t5, t4, dyn
	-[0x800029a8]:csrrs a2, fcsr, zero
	-[0x800029ac]:sw t6, 152(fp)
Current Store : [0x800029b0] : sw a2, 156(fp) -- Store: [0x80012e74]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6c407f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800029e4]:fmul.s t6, t5, t4, dyn
	-[0x800029e8]:csrrs a2, fcsr, zero
	-[0x800029ec]:sw t6, 160(fp)
Current Store : [0x800029f0] : sw a2, 164(fp) -- Store: [0x80012e7c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6c407f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002a24]:fmul.s t6, t5, t4, dyn
	-[0x80002a28]:csrrs a2, fcsr, zero
	-[0x80002a2c]:sw t6, 168(fp)
Current Store : [0x80002a30] : sw a2, 172(fp) -- Store: [0x80012e84]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6c407f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002a64]:fmul.s t6, t5, t4, dyn
	-[0x80002a68]:csrrs a2, fcsr, zero
	-[0x80002a6c]:sw t6, 176(fp)
Current Store : [0x80002a70] : sw a2, 180(fp) -- Store: [0x80012e8c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6c407f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002aa4]:fmul.s t6, t5, t4, dyn
	-[0x80002aa8]:csrrs a2, fcsr, zero
	-[0x80002aac]:sw t6, 184(fp)
Current Store : [0x80002ab0] : sw a2, 188(fp) -- Store: [0x80012e94]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x2dbe96 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002ae4]:fmul.s t6, t5, t4, dyn
	-[0x80002ae8]:csrrs a2, fcsr, zero
	-[0x80002aec]:sw t6, 192(fp)
Current Store : [0x80002af0] : sw a2, 196(fp) -- Store: [0x80012e9c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x2dbe96 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002b24]:fmul.s t6, t5, t4, dyn
	-[0x80002b28]:csrrs a2, fcsr, zero
	-[0x80002b2c]:sw t6, 200(fp)
Current Store : [0x80002b30] : sw a2, 204(fp) -- Store: [0x80012ea4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x2dbe96 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002b64]:fmul.s t6, t5, t4, dyn
	-[0x80002b68]:csrrs a2, fcsr, zero
	-[0x80002b6c]:sw t6, 208(fp)
Current Store : [0x80002b70] : sw a2, 212(fp) -- Store: [0x80012eac]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x2dbe96 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002ba4]:fmul.s t6, t5, t4, dyn
	-[0x80002ba8]:csrrs a2, fcsr, zero
	-[0x80002bac]:sw t6, 216(fp)
Current Store : [0x80002bb0] : sw a2, 220(fp) -- Store: [0x80012eb4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x2dbe96 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002be4]:fmul.s t6, t5, t4, dyn
	-[0x80002be8]:csrrs a2, fcsr, zero
	-[0x80002bec]:sw t6, 224(fp)
Current Store : [0x80002bf0] : sw a2, 228(fp) -- Store: [0x80012ebc]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x3d4d49 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002c24]:fmul.s t6, t5, t4, dyn
	-[0x80002c28]:csrrs a2, fcsr, zero
	-[0x80002c2c]:sw t6, 232(fp)
Current Store : [0x80002c30] : sw a2, 236(fp) -- Store: [0x80012ec4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x3d4d49 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002c64]:fmul.s t6, t5, t4, dyn
	-[0x80002c68]:csrrs a2, fcsr, zero
	-[0x80002c6c]:sw t6, 240(fp)
Current Store : [0x80002c70] : sw a2, 244(fp) -- Store: [0x80012ecc]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x3d4d49 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002ca4]:fmul.s t6, t5, t4, dyn
	-[0x80002ca8]:csrrs a2, fcsr, zero
	-[0x80002cac]:sw t6, 248(fp)
Current Store : [0x80002cb0] : sw a2, 252(fp) -- Store: [0x80012ed4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x3d4d49 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002ce4]:fmul.s t6, t5, t4, dyn
	-[0x80002ce8]:csrrs a2, fcsr, zero
	-[0x80002cec]:sw t6, 256(fp)
Current Store : [0x80002cf0] : sw a2, 260(fp) -- Store: [0x80012edc]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x3d4d49 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002d24]:fmul.s t6, t5, t4, dyn
	-[0x80002d28]:csrrs a2, fcsr, zero
	-[0x80002d2c]:sw t6, 264(fp)
Current Store : [0x80002d30] : sw a2, 268(fp) -- Store: [0x80012ee4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1310f3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002d64]:fmul.s t6, t5, t4, dyn
	-[0x80002d68]:csrrs a2, fcsr, zero
	-[0x80002d6c]:sw t6, 272(fp)
Current Store : [0x80002d70] : sw a2, 276(fp) -- Store: [0x80012eec]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1310f3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002da4]:fmul.s t6, t5, t4, dyn
	-[0x80002da8]:csrrs a2, fcsr, zero
	-[0x80002dac]:sw t6, 280(fp)
Current Store : [0x80002db0] : sw a2, 284(fp) -- Store: [0x80012ef4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1310f3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002de4]:fmul.s t6, t5, t4, dyn
	-[0x80002de8]:csrrs a2, fcsr, zero
	-[0x80002dec]:sw t6, 288(fp)
Current Store : [0x80002df0] : sw a2, 292(fp) -- Store: [0x80012efc]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1310f3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002e24]:fmul.s t6, t5, t4, dyn
	-[0x80002e28]:csrrs a2, fcsr, zero
	-[0x80002e2c]:sw t6, 296(fp)
Current Store : [0x80002e30] : sw a2, 300(fp) -- Store: [0x80012f04]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1310f3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002e64]:fmul.s t6, t5, t4, dyn
	-[0x80002e68]:csrrs a2, fcsr, zero
	-[0x80002e6c]:sw t6, 304(fp)
Current Store : [0x80002e70] : sw a2, 308(fp) -- Store: [0x80012f0c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2d3017 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002ea4]:fmul.s t6, t5, t4, dyn
	-[0x80002ea8]:csrrs a2, fcsr, zero
	-[0x80002eac]:sw t6, 312(fp)
Current Store : [0x80002eb0] : sw a2, 316(fp) -- Store: [0x80012f14]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2d3017 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002ee4]:fmul.s t6, t5, t4, dyn
	-[0x80002ee8]:csrrs a2, fcsr, zero
	-[0x80002eec]:sw t6, 320(fp)
Current Store : [0x80002ef0] : sw a2, 324(fp) -- Store: [0x80012f1c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2d3017 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002f24]:fmul.s t6, t5, t4, dyn
	-[0x80002f28]:csrrs a2, fcsr, zero
	-[0x80002f2c]:sw t6, 328(fp)
Current Store : [0x80002f30] : sw a2, 332(fp) -- Store: [0x80012f24]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2d3017 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002f64]:fmul.s t6, t5, t4, dyn
	-[0x80002f68]:csrrs a2, fcsr, zero
	-[0x80002f6c]:sw t6, 336(fp)
Current Store : [0x80002f70] : sw a2, 340(fp) -- Store: [0x80012f2c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2d3017 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002fa4]:fmul.s t6, t5, t4, dyn
	-[0x80002fa8]:csrrs a2, fcsr, zero
	-[0x80002fac]:sw t6, 344(fp)
Current Store : [0x80002fb0] : sw a2, 348(fp) -- Store: [0x80012f34]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2596ee and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002fe4]:fmul.s t6, t5, t4, dyn
	-[0x80002fe8]:csrrs a2, fcsr, zero
	-[0x80002fec]:sw t6, 352(fp)
Current Store : [0x80002ff0] : sw a2, 356(fp) -- Store: [0x80012f3c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2596ee and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003024]:fmul.s t6, t5, t4, dyn
	-[0x80003028]:csrrs a2, fcsr, zero
	-[0x8000302c]:sw t6, 360(fp)
Current Store : [0x80003030] : sw a2, 364(fp) -- Store: [0x80012f44]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2596ee and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003064]:fmul.s t6, t5, t4, dyn
	-[0x80003068]:csrrs a2, fcsr, zero
	-[0x8000306c]:sw t6, 368(fp)
Current Store : [0x80003070] : sw a2, 372(fp) -- Store: [0x80012f4c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2596ee and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800030a4]:fmul.s t6, t5, t4, dyn
	-[0x800030a8]:csrrs a2, fcsr, zero
	-[0x800030ac]:sw t6, 376(fp)
Current Store : [0x800030b0] : sw a2, 380(fp) -- Store: [0x80012f54]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2596ee and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800030e4]:fmul.s t6, t5, t4, dyn
	-[0x800030e8]:csrrs a2, fcsr, zero
	-[0x800030ec]:sw t6, 384(fp)
Current Store : [0x800030f0] : sw a2, 388(fp) -- Store: [0x80012f5c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x358c1d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003124]:fmul.s t6, t5, t4, dyn
	-[0x80003128]:csrrs a2, fcsr, zero
	-[0x8000312c]:sw t6, 392(fp)
Current Store : [0x80003130] : sw a2, 396(fp) -- Store: [0x80012f64]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x358c1d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003164]:fmul.s t6, t5, t4, dyn
	-[0x80003168]:csrrs a2, fcsr, zero
	-[0x8000316c]:sw t6, 400(fp)
Current Store : [0x80003170] : sw a2, 404(fp) -- Store: [0x80012f6c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x358c1d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800031a4]:fmul.s t6, t5, t4, dyn
	-[0x800031a8]:csrrs a2, fcsr, zero
	-[0x800031ac]:sw t6, 408(fp)
Current Store : [0x800031b0] : sw a2, 412(fp) -- Store: [0x80012f74]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x358c1d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800031e4]:fmul.s t6, t5, t4, dyn
	-[0x800031e8]:csrrs a2, fcsr, zero
	-[0x800031ec]:sw t6, 416(fp)
Current Store : [0x800031f0] : sw a2, 420(fp) -- Store: [0x80012f7c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x358c1d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003224]:fmul.s t6, t5, t4, dyn
	-[0x80003228]:csrrs a2, fcsr, zero
	-[0x8000322c]:sw t6, 424(fp)
Current Store : [0x80003230] : sw a2, 428(fp) -- Store: [0x80012f84]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7dc215 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003264]:fmul.s t6, t5, t4, dyn
	-[0x80003268]:csrrs a2, fcsr, zero
	-[0x8000326c]:sw t6, 432(fp)
Current Store : [0x80003270] : sw a2, 436(fp) -- Store: [0x80012f8c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7dc215 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800032a4]:fmul.s t6, t5, t4, dyn
	-[0x800032a8]:csrrs a2, fcsr, zero
	-[0x800032ac]:sw t6, 440(fp)
Current Store : [0x800032b0] : sw a2, 444(fp) -- Store: [0x80012f94]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7dc215 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800032e4]:fmul.s t6, t5, t4, dyn
	-[0x800032e8]:csrrs a2, fcsr, zero
	-[0x800032ec]:sw t6, 448(fp)
Current Store : [0x800032f0] : sw a2, 452(fp) -- Store: [0x80012f9c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7dc215 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003324]:fmul.s t6, t5, t4, dyn
	-[0x80003328]:csrrs a2, fcsr, zero
	-[0x8000332c]:sw t6, 456(fp)
Current Store : [0x80003330] : sw a2, 460(fp) -- Store: [0x80012fa4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7dc215 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003364]:fmul.s t6, t5, t4, dyn
	-[0x80003368]:csrrs a2, fcsr, zero
	-[0x8000336c]:sw t6, 464(fp)
Current Store : [0x80003370] : sw a2, 468(fp) -- Store: [0x80012fac]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x1cbf56 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800033a4]:fmul.s t6, t5, t4, dyn
	-[0x800033a8]:csrrs a2, fcsr, zero
	-[0x800033ac]:sw t6, 472(fp)
Current Store : [0x800033b0] : sw a2, 476(fp) -- Store: [0x80012fb4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x1cbf56 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800033e4]:fmul.s t6, t5, t4, dyn
	-[0x800033e8]:csrrs a2, fcsr, zero
	-[0x800033ec]:sw t6, 480(fp)
Current Store : [0x800033f0] : sw a2, 484(fp) -- Store: [0x80012fbc]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x1cbf56 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003424]:fmul.s t6, t5, t4, dyn
	-[0x80003428]:csrrs a2, fcsr, zero
	-[0x8000342c]:sw t6, 488(fp)
Current Store : [0x80003430] : sw a2, 492(fp) -- Store: [0x80012fc4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x1cbf56 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003464]:fmul.s t6, t5, t4, dyn
	-[0x80003468]:csrrs a2, fcsr, zero
	-[0x8000346c]:sw t6, 496(fp)
Current Store : [0x80003470] : sw a2, 500(fp) -- Store: [0x80012fcc]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x1cbf56 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800034a4]:fmul.s t6, t5, t4, dyn
	-[0x800034a8]:csrrs a2, fcsr, zero
	-[0x800034ac]:sw t6, 504(fp)
Current Store : [0x800034b0] : sw a2, 508(fp) -- Store: [0x80012fd4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x27f459 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800034e4]:fmul.s t6, t5, t4, dyn
	-[0x800034e8]:csrrs a2, fcsr, zero
	-[0x800034ec]:sw t6, 512(fp)
Current Store : [0x800034f0] : sw a2, 516(fp) -- Store: [0x80012fdc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x27f459 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003524]:fmul.s t6, t5, t4, dyn
	-[0x80003528]:csrrs a2, fcsr, zero
	-[0x8000352c]:sw t6, 520(fp)
Current Store : [0x80003530] : sw a2, 524(fp) -- Store: [0x80012fe4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x27f459 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003564]:fmul.s t6, t5, t4, dyn
	-[0x80003568]:csrrs a2, fcsr, zero
	-[0x8000356c]:sw t6, 528(fp)
Current Store : [0x80003570] : sw a2, 532(fp) -- Store: [0x80012fec]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x27f459 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800035a4]:fmul.s t6, t5, t4, dyn
	-[0x800035a8]:csrrs a2, fcsr, zero
	-[0x800035ac]:sw t6, 536(fp)
Current Store : [0x800035b0] : sw a2, 540(fp) -- Store: [0x80012ff4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x27f459 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800035e4]:fmul.s t6, t5, t4, dyn
	-[0x800035e8]:csrrs a2, fcsr, zero
	-[0x800035ec]:sw t6, 544(fp)
Current Store : [0x800035f0] : sw a2, 548(fp) -- Store: [0x80012ffc]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x609f7b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003624]:fmul.s t6, t5, t4, dyn
	-[0x80003628]:csrrs a2, fcsr, zero
	-[0x8000362c]:sw t6, 552(fp)
Current Store : [0x80003630] : sw a2, 556(fp) -- Store: [0x80013004]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x609f7b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003664]:fmul.s t6, t5, t4, dyn
	-[0x80003668]:csrrs a2, fcsr, zero
	-[0x8000366c]:sw t6, 560(fp)
Current Store : [0x80003670] : sw a2, 564(fp) -- Store: [0x8001300c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x609f7b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800036a4]:fmul.s t6, t5, t4, dyn
	-[0x800036a8]:csrrs a2, fcsr, zero
	-[0x800036ac]:sw t6, 568(fp)
Current Store : [0x800036b0] : sw a2, 572(fp) -- Store: [0x80013014]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x609f7b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800036e4]:fmul.s t6, t5, t4, dyn
	-[0x800036e8]:csrrs a2, fcsr, zero
	-[0x800036ec]:sw t6, 576(fp)
Current Store : [0x800036f0] : sw a2, 580(fp) -- Store: [0x8001301c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x609f7b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003724]:fmul.s t6, t5, t4, dyn
	-[0x80003728]:csrrs a2, fcsr, zero
	-[0x8000372c]:sw t6, 584(fp)
Current Store : [0x80003730] : sw a2, 588(fp) -- Store: [0x80013024]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x45af29 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003764]:fmul.s t6, t5, t4, dyn
	-[0x80003768]:csrrs a2, fcsr, zero
	-[0x8000376c]:sw t6, 592(fp)
Current Store : [0x80003770] : sw a2, 596(fp) -- Store: [0x8001302c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x45af29 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800037a4]:fmul.s t6, t5, t4, dyn
	-[0x800037a8]:csrrs a2, fcsr, zero
	-[0x800037ac]:sw t6, 600(fp)
Current Store : [0x800037b0] : sw a2, 604(fp) -- Store: [0x80013034]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x45af29 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800037e4]:fmul.s t6, t5, t4, dyn
	-[0x800037e8]:csrrs a2, fcsr, zero
	-[0x800037ec]:sw t6, 608(fp)
Current Store : [0x800037f0] : sw a2, 612(fp) -- Store: [0x8001303c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x45af29 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003824]:fmul.s t6, t5, t4, dyn
	-[0x80003828]:csrrs a2, fcsr, zero
	-[0x8000382c]:sw t6, 616(fp)
Current Store : [0x80003830] : sw a2, 620(fp) -- Store: [0x80013044]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x45af29 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003864]:fmul.s t6, t5, t4, dyn
	-[0x80003868]:csrrs a2, fcsr, zero
	-[0x8000386c]:sw t6, 624(fp)
Current Store : [0x80003870] : sw a2, 628(fp) -- Store: [0x8001304c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3844b4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800038a4]:fmul.s t6, t5, t4, dyn
	-[0x800038a8]:csrrs a2, fcsr, zero
	-[0x800038ac]:sw t6, 632(fp)
Current Store : [0x800038b0] : sw a2, 636(fp) -- Store: [0x80013054]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3844b4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800038e4]:fmul.s t6, t5, t4, dyn
	-[0x800038e8]:csrrs a2, fcsr, zero
	-[0x800038ec]:sw t6, 640(fp)
Current Store : [0x800038f0] : sw a2, 644(fp) -- Store: [0x8001305c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3844b4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003924]:fmul.s t6, t5, t4, dyn
	-[0x80003928]:csrrs a2, fcsr, zero
	-[0x8000392c]:sw t6, 648(fp)
Current Store : [0x80003930] : sw a2, 652(fp) -- Store: [0x80013064]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3844b4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003964]:fmul.s t6, t5, t4, dyn
	-[0x80003968]:csrrs a2, fcsr, zero
	-[0x8000396c]:sw t6, 656(fp)
Current Store : [0x80003970] : sw a2, 660(fp) -- Store: [0x8001306c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3844b4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800039a4]:fmul.s t6, t5, t4, dyn
	-[0x800039a8]:csrrs a2, fcsr, zero
	-[0x800039ac]:sw t6, 664(fp)
Current Store : [0x800039b0] : sw a2, 668(fp) -- Store: [0x80013074]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x68bbe2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800039e4]:fmul.s t6, t5, t4, dyn
	-[0x800039e8]:csrrs a2, fcsr, zero
	-[0x800039ec]:sw t6, 672(fp)
Current Store : [0x800039f0] : sw a2, 676(fp) -- Store: [0x8001307c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x68bbe2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003a24]:fmul.s t6, t5, t4, dyn
	-[0x80003a28]:csrrs a2, fcsr, zero
	-[0x80003a2c]:sw t6, 680(fp)
Current Store : [0x80003a30] : sw a2, 684(fp) -- Store: [0x80013084]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x68bbe2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003a64]:fmul.s t6, t5, t4, dyn
	-[0x80003a68]:csrrs a2, fcsr, zero
	-[0x80003a6c]:sw t6, 688(fp)
Current Store : [0x80003a70] : sw a2, 692(fp) -- Store: [0x8001308c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x68bbe2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003aa4]:fmul.s t6, t5, t4, dyn
	-[0x80003aa8]:csrrs a2, fcsr, zero
	-[0x80003aac]:sw t6, 696(fp)
Current Store : [0x80003ab0] : sw a2, 700(fp) -- Store: [0x80013094]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x68bbe2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003ae4]:fmul.s t6, t5, t4, dyn
	-[0x80003ae8]:csrrs a2, fcsr, zero
	-[0x80003aec]:sw t6, 704(fp)
Current Store : [0x80003af0] : sw a2, 708(fp) -- Store: [0x8001309c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x5d9799 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003b24]:fmul.s t6, t5, t4, dyn
	-[0x80003b28]:csrrs a2, fcsr, zero
	-[0x80003b2c]:sw t6, 712(fp)
Current Store : [0x80003b30] : sw a2, 716(fp) -- Store: [0x800130a4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x5d9799 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003b64]:fmul.s t6, t5, t4, dyn
	-[0x80003b68]:csrrs a2, fcsr, zero
	-[0x80003b6c]:sw t6, 720(fp)
Current Store : [0x80003b70] : sw a2, 724(fp) -- Store: [0x800130ac]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x5d9799 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003ba4]:fmul.s t6, t5, t4, dyn
	-[0x80003ba8]:csrrs a2, fcsr, zero
	-[0x80003bac]:sw t6, 728(fp)
Current Store : [0x80003bb0] : sw a2, 732(fp) -- Store: [0x800130b4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x5d9799 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003be4]:fmul.s t6, t5, t4, dyn
	-[0x80003be8]:csrrs a2, fcsr, zero
	-[0x80003bec]:sw t6, 736(fp)
Current Store : [0x80003bf0] : sw a2, 740(fp) -- Store: [0x800130bc]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x5d9799 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003c24]:fmul.s t6, t5, t4, dyn
	-[0x80003c28]:csrrs a2, fcsr, zero
	-[0x80003c2c]:sw t6, 744(fp)
Current Store : [0x80003c30] : sw a2, 748(fp) -- Store: [0x800130c4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x57ea20 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003c64]:fmul.s t6, t5, t4, dyn
	-[0x80003c68]:csrrs a2, fcsr, zero
	-[0x80003c6c]:sw t6, 752(fp)
Current Store : [0x80003c70] : sw a2, 756(fp) -- Store: [0x800130cc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x57ea20 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003ca4]:fmul.s t6, t5, t4, dyn
	-[0x80003ca8]:csrrs a2, fcsr, zero
	-[0x80003cac]:sw t6, 760(fp)
Current Store : [0x80003cb0] : sw a2, 764(fp) -- Store: [0x800130d4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x57ea20 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003ce4]:fmul.s t6, t5, t4, dyn
	-[0x80003ce8]:csrrs a2, fcsr, zero
	-[0x80003cec]:sw t6, 768(fp)
Current Store : [0x80003cf0] : sw a2, 772(fp) -- Store: [0x800130dc]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x57ea20 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003d24]:fmul.s t6, t5, t4, dyn
	-[0x80003d28]:csrrs a2, fcsr, zero
	-[0x80003d2c]:sw t6, 776(fp)
Current Store : [0x80003d30] : sw a2, 780(fp) -- Store: [0x800130e4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x57ea20 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003d64]:fmul.s t6, t5, t4, dyn
	-[0x80003d68]:csrrs a2, fcsr, zero
	-[0x80003d6c]:sw t6, 784(fp)
Current Store : [0x80003d70] : sw a2, 788(fp) -- Store: [0x800130ec]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x4e2b68 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003da4]:fmul.s t6, t5, t4, dyn
	-[0x80003da8]:csrrs a2, fcsr, zero
	-[0x80003dac]:sw t6, 792(fp)
Current Store : [0x80003db0] : sw a2, 796(fp) -- Store: [0x800130f4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x4e2b68 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003de4]:fmul.s t6, t5, t4, dyn
	-[0x80003de8]:csrrs a2, fcsr, zero
	-[0x80003dec]:sw t6, 800(fp)
Current Store : [0x80003df0] : sw a2, 804(fp) -- Store: [0x800130fc]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x4e2b68 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003e24]:fmul.s t6, t5, t4, dyn
	-[0x80003e28]:csrrs a2, fcsr, zero
	-[0x80003e2c]:sw t6, 808(fp)
Current Store : [0x80003e30] : sw a2, 812(fp) -- Store: [0x80013104]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x4e2b68 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003e64]:fmul.s t6, t5, t4, dyn
	-[0x80003e68]:csrrs a2, fcsr, zero
	-[0x80003e6c]:sw t6, 816(fp)
Current Store : [0x80003e70] : sw a2, 820(fp) -- Store: [0x8001310c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x4e2b68 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003ea4]:fmul.s t6, t5, t4, dyn
	-[0x80003ea8]:csrrs a2, fcsr, zero
	-[0x80003eac]:sw t6, 824(fp)
Current Store : [0x80003eb0] : sw a2, 828(fp) -- Store: [0x80013114]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x134261 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003ee4]:fmul.s t6, t5, t4, dyn
	-[0x80003ee8]:csrrs a2, fcsr, zero
	-[0x80003eec]:sw t6, 832(fp)
Current Store : [0x80003ef0] : sw a2, 836(fp) -- Store: [0x8001311c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x134261 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003f24]:fmul.s t6, t5, t4, dyn
	-[0x80003f28]:csrrs a2, fcsr, zero
	-[0x80003f2c]:sw t6, 840(fp)
Current Store : [0x80003f30] : sw a2, 844(fp) -- Store: [0x80013124]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x134261 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003f64]:fmul.s t6, t5, t4, dyn
	-[0x80003f68]:csrrs a2, fcsr, zero
	-[0x80003f6c]:sw t6, 848(fp)
Current Store : [0x80003f70] : sw a2, 852(fp) -- Store: [0x8001312c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x134261 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003fa4]:fmul.s t6, t5, t4, dyn
	-[0x80003fa8]:csrrs a2, fcsr, zero
	-[0x80003fac]:sw t6, 856(fp)
Current Store : [0x80003fb0] : sw a2, 860(fp) -- Store: [0x80013134]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x134261 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003fe4]:fmul.s t6, t5, t4, dyn
	-[0x80003fe8]:csrrs a2, fcsr, zero
	-[0x80003fec]:sw t6, 864(fp)
Current Store : [0x80003ff0] : sw a2, 868(fp) -- Store: [0x8001313c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x432be8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004024]:fmul.s t6, t5, t4, dyn
	-[0x80004028]:csrrs a2, fcsr, zero
	-[0x8000402c]:sw t6, 872(fp)
Current Store : [0x80004030] : sw a2, 876(fp) -- Store: [0x80013144]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x432be8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004064]:fmul.s t6, t5, t4, dyn
	-[0x80004068]:csrrs a2, fcsr, zero
	-[0x8000406c]:sw t6, 880(fp)
Current Store : [0x80004070] : sw a2, 884(fp) -- Store: [0x8001314c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x432be8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800040a4]:fmul.s t6, t5, t4, dyn
	-[0x800040a8]:csrrs a2, fcsr, zero
	-[0x800040ac]:sw t6, 888(fp)
Current Store : [0x800040b0] : sw a2, 892(fp) -- Store: [0x80013154]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x432be8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800040e4]:fmul.s t6, t5, t4, dyn
	-[0x800040e8]:csrrs a2, fcsr, zero
	-[0x800040ec]:sw t6, 896(fp)
Current Store : [0x800040f0] : sw a2, 900(fp) -- Store: [0x8001315c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x432be8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004124]:fmul.s t6, t5, t4, dyn
	-[0x80004128]:csrrs a2, fcsr, zero
	-[0x8000412c]:sw t6, 904(fp)
Current Store : [0x80004130] : sw a2, 908(fp) -- Store: [0x80013164]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x335a5f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004164]:fmul.s t6, t5, t4, dyn
	-[0x80004168]:csrrs a2, fcsr, zero
	-[0x8000416c]:sw t6, 912(fp)
Current Store : [0x80004170] : sw a2, 916(fp) -- Store: [0x8001316c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x335a5f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800041a4]:fmul.s t6, t5, t4, dyn
	-[0x800041a8]:csrrs a2, fcsr, zero
	-[0x800041ac]:sw t6, 920(fp)
Current Store : [0x800041b0] : sw a2, 924(fp) -- Store: [0x80013174]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x335a5f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800041e4]:fmul.s t6, t5, t4, dyn
	-[0x800041e8]:csrrs a2, fcsr, zero
	-[0x800041ec]:sw t6, 928(fp)
Current Store : [0x800041f0] : sw a2, 932(fp) -- Store: [0x8001317c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x335a5f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004224]:fmul.s t6, t5, t4, dyn
	-[0x80004228]:csrrs a2, fcsr, zero
	-[0x8000422c]:sw t6, 936(fp)
Current Store : [0x80004230] : sw a2, 940(fp) -- Store: [0x80013184]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x335a5f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004264]:fmul.s t6, t5, t4, dyn
	-[0x80004268]:csrrs a2, fcsr, zero
	-[0x8000426c]:sw t6, 944(fp)
Current Store : [0x80004270] : sw a2, 948(fp) -- Store: [0x8001318c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x328a37 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800042a4]:fmul.s t6, t5, t4, dyn
	-[0x800042a8]:csrrs a2, fcsr, zero
	-[0x800042ac]:sw t6, 952(fp)
Current Store : [0x800042b0] : sw a2, 956(fp) -- Store: [0x80013194]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x328a37 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800042e4]:fmul.s t6, t5, t4, dyn
	-[0x800042e8]:csrrs a2, fcsr, zero
	-[0x800042ec]:sw t6, 960(fp)
Current Store : [0x800042f0] : sw a2, 964(fp) -- Store: [0x8001319c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x328a37 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004324]:fmul.s t6, t5, t4, dyn
	-[0x80004328]:csrrs a2, fcsr, zero
	-[0x8000432c]:sw t6, 968(fp)
Current Store : [0x80004330] : sw a2, 972(fp) -- Store: [0x800131a4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x328a37 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004364]:fmul.s t6, t5, t4, dyn
	-[0x80004368]:csrrs a2, fcsr, zero
	-[0x8000436c]:sw t6, 976(fp)
Current Store : [0x80004370] : sw a2, 980(fp) -- Store: [0x800131ac]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x328a37 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800043a4]:fmul.s t6, t5, t4, dyn
	-[0x800043a8]:csrrs a2, fcsr, zero
	-[0x800043ac]:sw t6, 984(fp)
Current Store : [0x800043b0] : sw a2, 988(fp) -- Store: [0x800131b4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x409980 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800043e4]:fmul.s t6, t5, t4, dyn
	-[0x800043e8]:csrrs a2, fcsr, zero
	-[0x800043ec]:sw t6, 992(fp)
Current Store : [0x800043f0] : sw a2, 996(fp) -- Store: [0x800131bc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x409980 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004424]:fmul.s t6, t5, t4, dyn
	-[0x80004428]:csrrs a2, fcsr, zero
	-[0x8000442c]:sw t6, 1000(fp)
Current Store : [0x80004430] : sw a2, 1004(fp) -- Store: [0x800131c4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x409980 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004464]:fmul.s t6, t5, t4, dyn
	-[0x80004468]:csrrs a2, fcsr, zero
	-[0x8000446c]:sw t6, 1008(fp)
Current Store : [0x80004470] : sw a2, 1012(fp) -- Store: [0x800131cc]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x409980 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800044a4]:fmul.s t6, t5, t4, dyn
	-[0x800044a8]:csrrs a2, fcsr, zero
	-[0x800044ac]:sw t6, 1016(fp)
Current Store : [0x800044b0] : sw a2, 1020(fp) -- Store: [0x800131d4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x409980 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800044ec]:fmul.s t6, t5, t4, dyn
	-[0x800044f0]:csrrs a2, fcsr, zero
	-[0x800044f4]:sw t6, 0(fp)
Current Store : [0x800044f8] : sw a2, 4(fp) -- Store: [0x800131dc]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6fadd2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000452c]:fmul.s t6, t5, t4, dyn
	-[0x80004530]:csrrs a2, fcsr, zero
	-[0x80004534]:sw t6, 8(fp)
Current Store : [0x80004538] : sw a2, 12(fp) -- Store: [0x800131e4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6fadd2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000456c]:fmul.s t6, t5, t4, dyn
	-[0x80004570]:csrrs a2, fcsr, zero
	-[0x80004574]:sw t6, 16(fp)
Current Store : [0x80004578] : sw a2, 20(fp) -- Store: [0x800131ec]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6fadd2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800045ac]:fmul.s t6, t5, t4, dyn
	-[0x800045b0]:csrrs a2, fcsr, zero
	-[0x800045b4]:sw t6, 24(fp)
Current Store : [0x800045b8] : sw a2, 28(fp) -- Store: [0x800131f4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6fadd2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800045ec]:fmul.s t6, t5, t4, dyn
	-[0x800045f0]:csrrs a2, fcsr, zero
	-[0x800045f4]:sw t6, 32(fp)
Current Store : [0x800045f8] : sw a2, 36(fp) -- Store: [0x800131fc]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6fadd2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000462c]:fmul.s t6, t5, t4, dyn
	-[0x80004630]:csrrs a2, fcsr, zero
	-[0x80004634]:sw t6, 40(fp)
Current Store : [0x80004638] : sw a2, 44(fp) -- Store: [0x80013204]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x083942 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000466c]:fmul.s t6, t5, t4, dyn
	-[0x80004670]:csrrs a2, fcsr, zero
	-[0x80004674]:sw t6, 48(fp)
Current Store : [0x80004678] : sw a2, 52(fp) -- Store: [0x8001320c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x083942 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800046ac]:fmul.s t6, t5, t4, dyn
	-[0x800046b0]:csrrs a2, fcsr, zero
	-[0x800046b4]:sw t6, 56(fp)
Current Store : [0x800046b8] : sw a2, 60(fp) -- Store: [0x80013214]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x083942 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800046ec]:fmul.s t6, t5, t4, dyn
	-[0x800046f0]:csrrs a2, fcsr, zero
	-[0x800046f4]:sw t6, 64(fp)
Current Store : [0x800046f8] : sw a2, 68(fp) -- Store: [0x8001321c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x083942 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000472c]:fmul.s t6, t5, t4, dyn
	-[0x80004730]:csrrs a2, fcsr, zero
	-[0x80004734]:sw t6, 72(fp)
Current Store : [0x80004738] : sw a2, 76(fp) -- Store: [0x80013224]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x083942 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000476c]:fmul.s t6, t5, t4, dyn
	-[0x80004770]:csrrs a2, fcsr, zero
	-[0x80004774]:sw t6, 80(fp)
Current Store : [0x80004778] : sw a2, 84(fp) -- Store: [0x8001322c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x227041 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800047ac]:fmul.s t6, t5, t4, dyn
	-[0x800047b0]:csrrs a2, fcsr, zero
	-[0x800047b4]:sw t6, 88(fp)
Current Store : [0x800047b8] : sw a2, 92(fp) -- Store: [0x80013234]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x227041 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800047ec]:fmul.s t6, t5, t4, dyn
	-[0x800047f0]:csrrs a2, fcsr, zero
	-[0x800047f4]:sw t6, 96(fp)
Current Store : [0x800047f8] : sw a2, 100(fp) -- Store: [0x8001323c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x227041 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000482c]:fmul.s t6, t5, t4, dyn
	-[0x80004830]:csrrs a2, fcsr, zero
	-[0x80004834]:sw t6, 104(fp)
Current Store : [0x80004838] : sw a2, 108(fp) -- Store: [0x80013244]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x227041 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000486c]:fmul.s t6, t5, t4, dyn
	-[0x80004870]:csrrs a2, fcsr, zero
	-[0x80004874]:sw t6, 112(fp)
Current Store : [0x80004878] : sw a2, 116(fp) -- Store: [0x8001324c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x227041 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800048ac]:fmul.s t6, t5, t4, dyn
	-[0x800048b0]:csrrs a2, fcsr, zero
	-[0x800048b4]:sw t6, 120(fp)
Current Store : [0x800048b8] : sw a2, 124(fp) -- Store: [0x80013254]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf6 and fm1 == 0x6d25cb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800048ec]:fmul.s t6, t5, t4, dyn
	-[0x800048f0]:csrrs a2, fcsr, zero
	-[0x800048f4]:sw t6, 128(fp)
Current Store : [0x800048f8] : sw a2, 132(fp) -- Store: [0x8001325c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf6 and fm1 == 0x6d25cb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000492c]:fmul.s t6, t5, t4, dyn
	-[0x80004930]:csrrs a2, fcsr, zero
	-[0x80004934]:sw t6, 136(fp)
Current Store : [0x80004938] : sw a2, 140(fp) -- Store: [0x80013264]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf6 and fm1 == 0x6d25cb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000496c]:fmul.s t6, t5, t4, dyn
	-[0x80004970]:csrrs a2, fcsr, zero
	-[0x80004974]:sw t6, 144(fp)
Current Store : [0x80004978] : sw a2, 148(fp) -- Store: [0x8001326c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf6 and fm1 == 0x6d25cb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800049ac]:fmul.s t6, t5, t4, dyn
	-[0x800049b0]:csrrs a2, fcsr, zero
	-[0x800049b4]:sw t6, 152(fp)
Current Store : [0x800049b8] : sw a2, 156(fp) -- Store: [0x80013274]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf6 and fm1 == 0x6d25cb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800049ec]:fmul.s t6, t5, t4, dyn
	-[0x800049f0]:csrrs a2, fcsr, zero
	-[0x800049f4]:sw t6, 160(fp)
Current Store : [0x800049f8] : sw a2, 164(fp) -- Store: [0x8001327c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x1cf370 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004a2c]:fmul.s t6, t5, t4, dyn
	-[0x80004a30]:csrrs a2, fcsr, zero
	-[0x80004a34]:sw t6, 168(fp)
Current Store : [0x80004a38] : sw a2, 172(fp) -- Store: [0x80013284]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x1cf370 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004a6c]:fmul.s t6, t5, t4, dyn
	-[0x80004a70]:csrrs a2, fcsr, zero
	-[0x80004a74]:sw t6, 176(fp)
Current Store : [0x80004a78] : sw a2, 180(fp) -- Store: [0x8001328c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x1cf370 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004aac]:fmul.s t6, t5, t4, dyn
	-[0x80004ab0]:csrrs a2, fcsr, zero
	-[0x80004ab4]:sw t6, 184(fp)
Current Store : [0x80004ab8] : sw a2, 188(fp) -- Store: [0x80013294]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x1cf370 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004aec]:fmul.s t6, t5, t4, dyn
	-[0x80004af0]:csrrs a2, fcsr, zero
	-[0x80004af4]:sw t6, 192(fp)
Current Store : [0x80004af8] : sw a2, 196(fp) -- Store: [0x8001329c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x1cf370 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004b2c]:fmul.s t6, t5, t4, dyn
	-[0x80004b30]:csrrs a2, fcsr, zero
	-[0x80004b34]:sw t6, 200(fp)
Current Store : [0x80004b38] : sw a2, 204(fp) -- Store: [0x800132a4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0af6b9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004b6c]:fmul.s t6, t5, t4, dyn
	-[0x80004b70]:csrrs a2, fcsr, zero
	-[0x80004b74]:sw t6, 208(fp)
Current Store : [0x80004b78] : sw a2, 212(fp) -- Store: [0x800132ac]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0af6b9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004bac]:fmul.s t6, t5, t4, dyn
	-[0x80004bb0]:csrrs a2, fcsr, zero
	-[0x80004bb4]:sw t6, 216(fp)
Current Store : [0x80004bb8] : sw a2, 220(fp) -- Store: [0x800132b4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0af6b9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004bec]:fmul.s t6, t5, t4, dyn
	-[0x80004bf0]:csrrs a2, fcsr, zero
	-[0x80004bf4]:sw t6, 224(fp)
Current Store : [0x80004bf8] : sw a2, 228(fp) -- Store: [0x800132bc]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0af6b9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004c2c]:fmul.s t6, t5, t4, dyn
	-[0x80004c30]:csrrs a2, fcsr, zero
	-[0x80004c34]:sw t6, 232(fp)
Current Store : [0x80004c38] : sw a2, 236(fp) -- Store: [0x800132c4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0af6b9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004c6c]:fmul.s t6, t5, t4, dyn
	-[0x80004c70]:csrrs a2, fcsr, zero
	-[0x80004c74]:sw t6, 240(fp)
Current Store : [0x80004c78] : sw a2, 244(fp) -- Store: [0x800132cc]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x19405f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004cac]:fmul.s t6, t5, t4, dyn
	-[0x80004cb0]:csrrs a2, fcsr, zero
	-[0x80004cb4]:sw t6, 248(fp)
Current Store : [0x80004cb8] : sw a2, 252(fp) -- Store: [0x800132d4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x19405f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004cec]:fmul.s t6, t5, t4, dyn
	-[0x80004cf0]:csrrs a2, fcsr, zero
	-[0x80004cf4]:sw t6, 256(fp)
Current Store : [0x80004cf8] : sw a2, 260(fp) -- Store: [0x800132dc]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x19405f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004d2c]:fmul.s t6, t5, t4, dyn
	-[0x80004d30]:csrrs a2, fcsr, zero
	-[0x80004d34]:sw t6, 264(fp)
Current Store : [0x80004d38] : sw a2, 268(fp) -- Store: [0x800132e4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x19405f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004d6c]:fmul.s t6, t5, t4, dyn
	-[0x80004d70]:csrrs a2, fcsr, zero
	-[0x80004d74]:sw t6, 272(fp)
Current Store : [0x80004d78] : sw a2, 276(fp) -- Store: [0x800132ec]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x19405f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004dac]:fmul.s t6, t5, t4, dyn
	-[0x80004db0]:csrrs a2, fcsr, zero
	-[0x80004db4]:sw t6, 280(fp)
Current Store : [0x80004db8] : sw a2, 284(fp) -- Store: [0x800132f4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x31cfbf and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004dec]:fmul.s t6, t5, t4, dyn
	-[0x80004df0]:csrrs a2, fcsr, zero
	-[0x80004df4]:sw t6, 288(fp)
Current Store : [0x80004df8] : sw a2, 292(fp) -- Store: [0x800132fc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x31cfbf and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004e2c]:fmul.s t6, t5, t4, dyn
	-[0x80004e30]:csrrs a2, fcsr, zero
	-[0x80004e34]:sw t6, 296(fp)
Current Store : [0x80004e38] : sw a2, 300(fp) -- Store: [0x80013304]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x31cfbf and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004e6c]:fmul.s t6, t5, t4, dyn
	-[0x80004e70]:csrrs a2, fcsr, zero
	-[0x80004e74]:sw t6, 304(fp)
Current Store : [0x80004e78] : sw a2, 308(fp) -- Store: [0x8001330c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x31cfbf and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004eac]:fmul.s t6, t5, t4, dyn
	-[0x80004eb0]:csrrs a2, fcsr, zero
	-[0x80004eb4]:sw t6, 312(fp)
Current Store : [0x80004eb8] : sw a2, 316(fp) -- Store: [0x80013314]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x31cfbf and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004eec]:fmul.s t6, t5, t4, dyn
	-[0x80004ef0]:csrrs a2, fcsr, zero
	-[0x80004ef4]:sw t6, 320(fp)
Current Store : [0x80004ef8] : sw a2, 324(fp) -- Store: [0x8001331c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x47f677 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004f2c]:fmul.s t6, t5, t4, dyn
	-[0x80004f30]:csrrs a2, fcsr, zero
	-[0x80004f34]:sw t6, 328(fp)
Current Store : [0x80004f38] : sw a2, 332(fp) -- Store: [0x80013324]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x47f677 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004f6c]:fmul.s t6, t5, t4, dyn
	-[0x80004f70]:csrrs a2, fcsr, zero
	-[0x80004f74]:sw t6, 336(fp)
Current Store : [0x80004f78] : sw a2, 340(fp) -- Store: [0x8001332c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x47f677 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004fac]:fmul.s t6, t5, t4, dyn
	-[0x80004fb0]:csrrs a2, fcsr, zero
	-[0x80004fb4]:sw t6, 344(fp)
Current Store : [0x80004fb8] : sw a2, 348(fp) -- Store: [0x80013334]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x47f677 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004fec]:fmul.s t6, t5, t4, dyn
	-[0x80004ff0]:csrrs a2, fcsr, zero
	-[0x80004ff4]:sw t6, 352(fp)
Current Store : [0x80004ff8] : sw a2, 356(fp) -- Store: [0x8001333c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x47f677 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000502c]:fmul.s t6, t5, t4, dyn
	-[0x80005030]:csrrs a2, fcsr, zero
	-[0x80005034]:sw t6, 360(fp)
Current Store : [0x80005038] : sw a2, 364(fp) -- Store: [0x80013344]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6dde9e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000506c]:fmul.s t6, t5, t4, dyn
	-[0x80005070]:csrrs a2, fcsr, zero
	-[0x80005074]:sw t6, 368(fp)
Current Store : [0x80005078] : sw a2, 372(fp) -- Store: [0x8001334c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6dde9e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800050ac]:fmul.s t6, t5, t4, dyn
	-[0x800050b0]:csrrs a2, fcsr, zero
	-[0x800050b4]:sw t6, 376(fp)
Current Store : [0x800050b8] : sw a2, 380(fp) -- Store: [0x80013354]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6dde9e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800050ec]:fmul.s t6, t5, t4, dyn
	-[0x800050f0]:csrrs a2, fcsr, zero
	-[0x800050f4]:sw t6, 384(fp)
Current Store : [0x800050f8] : sw a2, 388(fp) -- Store: [0x8001335c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6dde9e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000512c]:fmul.s t6, t5, t4, dyn
	-[0x80005130]:csrrs a2, fcsr, zero
	-[0x80005134]:sw t6, 392(fp)
Current Store : [0x80005138] : sw a2, 396(fp) -- Store: [0x80013364]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6dde9e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000516c]:fmul.s t6, t5, t4, dyn
	-[0x80005170]:csrrs a2, fcsr, zero
	-[0x80005174]:sw t6, 400(fp)
Current Store : [0x80005178] : sw a2, 404(fp) -- Store: [0x8001336c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x232951 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800051ac]:fmul.s t6, t5, t4, dyn
	-[0x800051b0]:csrrs a2, fcsr, zero
	-[0x800051b4]:sw t6, 408(fp)
Current Store : [0x800051b8] : sw a2, 412(fp) -- Store: [0x80013374]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x232951 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800051ec]:fmul.s t6, t5, t4, dyn
	-[0x800051f0]:csrrs a2, fcsr, zero
	-[0x800051f4]:sw t6, 416(fp)
Current Store : [0x800051f8] : sw a2, 420(fp) -- Store: [0x8001337c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x232951 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000522c]:fmul.s t6, t5, t4, dyn
	-[0x80005230]:csrrs a2, fcsr, zero
	-[0x80005234]:sw t6, 424(fp)
Current Store : [0x80005238] : sw a2, 428(fp) -- Store: [0x80013384]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x232951 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000526c]:fmul.s t6, t5, t4, dyn
	-[0x80005270]:csrrs a2, fcsr, zero
	-[0x80005274]:sw t6, 432(fp)
Current Store : [0x80005278] : sw a2, 436(fp) -- Store: [0x8001338c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x232951 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800052ac]:fmul.s t6, t5, t4, dyn
	-[0x800052b0]:csrrs a2, fcsr, zero
	-[0x800052b4]:sw t6, 440(fp)
Current Store : [0x800052b8] : sw a2, 444(fp) -- Store: [0x80013394]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x38eb1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800052ec]:fmul.s t6, t5, t4, dyn
	-[0x800052f0]:csrrs a2, fcsr, zero
	-[0x800052f4]:sw t6, 448(fp)
Current Store : [0x800052f8] : sw a2, 452(fp) -- Store: [0x8001339c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x38eb1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000532c]:fmul.s t6, t5, t4, dyn
	-[0x80005330]:csrrs a2, fcsr, zero
	-[0x80005334]:sw t6, 456(fp)
Current Store : [0x80005338] : sw a2, 460(fp) -- Store: [0x800133a4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x38eb1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000536c]:fmul.s t6, t5, t4, dyn
	-[0x80005370]:csrrs a2, fcsr, zero
	-[0x80005374]:sw t6, 464(fp)
Current Store : [0x80005378] : sw a2, 468(fp) -- Store: [0x800133ac]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x38eb1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800053ac]:fmul.s t6, t5, t4, dyn
	-[0x800053b0]:csrrs a2, fcsr, zero
	-[0x800053b4]:sw t6, 472(fp)
Current Store : [0x800053b8] : sw a2, 476(fp) -- Store: [0x800133b4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x38eb1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800053ec]:fmul.s t6, t5, t4, dyn
	-[0x800053f0]:csrrs a2, fcsr, zero
	-[0x800053f4]:sw t6, 480(fp)
Current Store : [0x800053f8] : sw a2, 484(fp) -- Store: [0x800133bc]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x268dc5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000542c]:fmul.s t6, t5, t4, dyn
	-[0x80005430]:csrrs a2, fcsr, zero
	-[0x80005434]:sw t6, 488(fp)
Current Store : [0x80005438] : sw a2, 492(fp) -- Store: [0x800133c4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x268dc5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000546c]:fmul.s t6, t5, t4, dyn
	-[0x80005470]:csrrs a2, fcsr, zero
	-[0x80005474]:sw t6, 496(fp)
Current Store : [0x80005478] : sw a2, 500(fp) -- Store: [0x800133cc]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x268dc5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800054ac]:fmul.s t6, t5, t4, dyn
	-[0x800054b0]:csrrs a2, fcsr, zero
	-[0x800054b4]:sw t6, 504(fp)
Current Store : [0x800054b8] : sw a2, 508(fp) -- Store: [0x800133d4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x268dc5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800054ec]:fmul.s t6, t5, t4, dyn
	-[0x800054f0]:csrrs a2, fcsr, zero
	-[0x800054f4]:sw t6, 512(fp)
Current Store : [0x800054f8] : sw a2, 516(fp) -- Store: [0x800133dc]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x268dc5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000552c]:fmul.s t6, t5, t4, dyn
	-[0x80005530]:csrrs a2, fcsr, zero
	-[0x80005534]:sw t6, 520(fp)
Current Store : [0x80005538] : sw a2, 524(fp) -- Store: [0x800133e4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x5f221f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000556c]:fmul.s t6, t5, t4, dyn
	-[0x80005570]:csrrs a2, fcsr, zero
	-[0x80005574]:sw t6, 528(fp)
Current Store : [0x80005578] : sw a2, 532(fp) -- Store: [0x800133ec]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x5f221f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800055ac]:fmul.s t6, t5, t4, dyn
	-[0x800055b0]:csrrs a2, fcsr, zero
	-[0x800055b4]:sw t6, 536(fp)
Current Store : [0x800055b8] : sw a2, 540(fp) -- Store: [0x800133f4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x5f221f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800055ec]:fmul.s t6, t5, t4, dyn
	-[0x800055f0]:csrrs a2, fcsr, zero
	-[0x800055f4]:sw t6, 544(fp)
Current Store : [0x800055f8] : sw a2, 548(fp) -- Store: [0x800133fc]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x5f221f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000562c]:fmul.s t6, t5, t4, dyn
	-[0x80005630]:csrrs a2, fcsr, zero
	-[0x80005634]:sw t6, 552(fp)
Current Store : [0x80005638] : sw a2, 556(fp) -- Store: [0x80013404]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x5f221f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000566c]:fmul.s t6, t5, t4, dyn
	-[0x80005670]:csrrs a2, fcsr, zero
	-[0x80005674]:sw t6, 560(fp)
Current Store : [0x80005678] : sw a2, 564(fp) -- Store: [0x8001340c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2084ae and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800056ac]:fmul.s t6, t5, t4, dyn
	-[0x800056b0]:csrrs a2, fcsr, zero
	-[0x800056b4]:sw t6, 568(fp)
Current Store : [0x800056b8] : sw a2, 572(fp) -- Store: [0x80013414]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2084ae and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800056ec]:fmul.s t6, t5, t4, dyn
	-[0x800056f0]:csrrs a2, fcsr, zero
	-[0x800056f4]:sw t6, 576(fp)
Current Store : [0x800056f8] : sw a2, 580(fp) -- Store: [0x8001341c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2084ae and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000572c]:fmul.s t6, t5, t4, dyn
	-[0x80005730]:csrrs a2, fcsr, zero
	-[0x80005734]:sw t6, 584(fp)
Current Store : [0x80005738] : sw a2, 588(fp) -- Store: [0x80013424]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2084ae and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000576c]:fmul.s t6, t5, t4, dyn
	-[0x80005770]:csrrs a2, fcsr, zero
	-[0x80005774]:sw t6, 592(fp)
Current Store : [0x80005778] : sw a2, 596(fp) -- Store: [0x8001342c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2084ae and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800057ac]:fmul.s t6, t5, t4, dyn
	-[0x800057b0]:csrrs a2, fcsr, zero
	-[0x800057b4]:sw t6, 600(fp)
Current Store : [0x800057b8] : sw a2, 604(fp) -- Store: [0x80013434]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x0b2e4b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800057ec]:fmul.s t6, t5, t4, dyn
	-[0x800057f0]:csrrs a2, fcsr, zero
	-[0x800057f4]:sw t6, 608(fp)
Current Store : [0x800057f8] : sw a2, 612(fp) -- Store: [0x8001343c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x0b2e4b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000582c]:fmul.s t6, t5, t4, dyn
	-[0x80005830]:csrrs a2, fcsr, zero
	-[0x80005834]:sw t6, 616(fp)
Current Store : [0x80005838] : sw a2, 620(fp) -- Store: [0x80013444]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x0b2e4b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000586c]:fmul.s t6, t5, t4, dyn
	-[0x80005870]:csrrs a2, fcsr, zero
	-[0x80005874]:sw t6, 624(fp)
Current Store : [0x80005878] : sw a2, 628(fp) -- Store: [0x8001344c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x0b2e4b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800058ac]:fmul.s t6, t5, t4, dyn
	-[0x800058b0]:csrrs a2, fcsr, zero
	-[0x800058b4]:sw t6, 632(fp)
Current Store : [0x800058b8] : sw a2, 636(fp) -- Store: [0x80013454]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x0b2e4b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800058ec]:fmul.s t6, t5, t4, dyn
	-[0x800058f0]:csrrs a2, fcsr, zero
	-[0x800058f4]:sw t6, 640(fp)
Current Store : [0x800058f8] : sw a2, 644(fp) -- Store: [0x8001345c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a1c1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000592c]:fmul.s t6, t5, t4, dyn
	-[0x80005930]:csrrs a2, fcsr, zero
	-[0x80005934]:sw t6, 648(fp)
Current Store : [0x80005938] : sw a2, 652(fp) -- Store: [0x80013464]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a1c1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000596c]:fmul.s t6, t5, t4, dyn
	-[0x80005970]:csrrs a2, fcsr, zero
	-[0x80005974]:sw t6, 656(fp)
Current Store : [0x80005978] : sw a2, 660(fp) -- Store: [0x8001346c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a1c1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800059ac]:fmul.s t6, t5, t4, dyn
	-[0x800059b0]:csrrs a2, fcsr, zero
	-[0x800059b4]:sw t6, 664(fp)
Current Store : [0x800059b8] : sw a2, 668(fp) -- Store: [0x80013474]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a1c1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800059ec]:fmul.s t6, t5, t4, dyn
	-[0x800059f0]:csrrs a2, fcsr, zero
	-[0x800059f4]:sw t6, 672(fp)
Current Store : [0x800059f8] : sw a2, 676(fp) -- Store: [0x8001347c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a1c1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005a2c]:fmul.s t6, t5, t4, dyn
	-[0x80005a30]:csrrs a2, fcsr, zero
	-[0x80005a34]:sw t6, 680(fp)
Current Store : [0x80005a38] : sw a2, 684(fp) -- Store: [0x80013484]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x476063 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005a6c]:fmul.s t6, t5, t4, dyn
	-[0x80005a70]:csrrs a2, fcsr, zero
	-[0x80005a74]:sw t6, 688(fp)
Current Store : [0x80005a78] : sw a2, 692(fp) -- Store: [0x8001348c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x476063 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005aac]:fmul.s t6, t5, t4, dyn
	-[0x80005ab0]:csrrs a2, fcsr, zero
	-[0x80005ab4]:sw t6, 696(fp)
Current Store : [0x80005ab8] : sw a2, 700(fp) -- Store: [0x80013494]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x476063 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005aec]:fmul.s t6, t5, t4, dyn
	-[0x80005af0]:csrrs a2, fcsr, zero
	-[0x80005af4]:sw t6, 704(fp)
Current Store : [0x80005af8] : sw a2, 708(fp) -- Store: [0x8001349c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x476063 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005b2c]:fmul.s t6, t5, t4, dyn
	-[0x80005b30]:csrrs a2, fcsr, zero
	-[0x80005b34]:sw t6, 712(fp)
Current Store : [0x80005b38] : sw a2, 716(fp) -- Store: [0x800134a4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x476063 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005b6c]:fmul.s t6, t5, t4, dyn
	-[0x80005b70]:csrrs a2, fcsr, zero
	-[0x80005b74]:sw t6, 720(fp)
Current Store : [0x80005b78] : sw a2, 724(fp) -- Store: [0x800134ac]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6a6f9b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005bac]:fmul.s t6, t5, t4, dyn
	-[0x80005bb0]:csrrs a2, fcsr, zero
	-[0x80005bb4]:sw t6, 728(fp)
Current Store : [0x80005bb8] : sw a2, 732(fp) -- Store: [0x800134b4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6a6f9b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005bec]:fmul.s t6, t5, t4, dyn
	-[0x80005bf0]:csrrs a2, fcsr, zero
	-[0x80005bf4]:sw t6, 736(fp)
Current Store : [0x80005bf8] : sw a2, 740(fp) -- Store: [0x800134bc]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6a6f9b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005c2c]:fmul.s t6, t5, t4, dyn
	-[0x80005c30]:csrrs a2, fcsr, zero
	-[0x80005c34]:sw t6, 744(fp)
Current Store : [0x80005c38] : sw a2, 748(fp) -- Store: [0x800134c4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6a6f9b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005c6c]:fmul.s t6, t5, t4, dyn
	-[0x80005c70]:csrrs a2, fcsr, zero
	-[0x80005c74]:sw t6, 752(fp)
Current Store : [0x80005c78] : sw a2, 756(fp) -- Store: [0x800134cc]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6a6f9b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005cac]:fmul.s t6, t5, t4, dyn
	-[0x80005cb0]:csrrs a2, fcsr, zero
	-[0x80005cb4]:sw t6, 760(fp)
Current Store : [0x80005cb8] : sw a2, 764(fp) -- Store: [0x800134d4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3cd606 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005cec]:fmul.s t6, t5, t4, dyn
	-[0x80005cf0]:csrrs a2, fcsr, zero
	-[0x80005cf4]:sw t6, 768(fp)
Current Store : [0x80005cf8] : sw a2, 772(fp) -- Store: [0x800134dc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3cd606 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005d2c]:fmul.s t6, t5, t4, dyn
	-[0x80005d30]:csrrs a2, fcsr, zero
	-[0x80005d34]:sw t6, 776(fp)
Current Store : [0x80005d38] : sw a2, 780(fp) -- Store: [0x800134e4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3cd606 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005d6c]:fmul.s t6, t5, t4, dyn
	-[0x80005d70]:csrrs a2, fcsr, zero
	-[0x80005d74]:sw t6, 784(fp)
Current Store : [0x80005d78] : sw a2, 788(fp) -- Store: [0x800134ec]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3cd606 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005dac]:fmul.s t6, t5, t4, dyn
	-[0x80005db0]:csrrs a2, fcsr, zero
	-[0x80005db4]:sw t6, 792(fp)
Current Store : [0x80005db8] : sw a2, 796(fp) -- Store: [0x800134f4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3cd606 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005dec]:fmul.s t6, t5, t4, dyn
	-[0x80005df0]:csrrs a2, fcsr, zero
	-[0x80005df4]:sw t6, 800(fp)
Current Store : [0x80005df8] : sw a2, 804(fp) -- Store: [0x800134fc]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x6a262c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005e2c]:fmul.s t6, t5, t4, dyn
	-[0x80005e30]:csrrs a2, fcsr, zero
	-[0x80005e34]:sw t6, 808(fp)
Current Store : [0x80005e38] : sw a2, 812(fp) -- Store: [0x80013504]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x6a262c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005e6c]:fmul.s t6, t5, t4, dyn
	-[0x80005e70]:csrrs a2, fcsr, zero
	-[0x80005e74]:sw t6, 816(fp)
Current Store : [0x80005e78] : sw a2, 820(fp) -- Store: [0x8001350c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x6a262c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005eac]:fmul.s t6, t5, t4, dyn
	-[0x80005eb0]:csrrs a2, fcsr, zero
	-[0x80005eb4]:sw t6, 824(fp)
Current Store : [0x80005eb8] : sw a2, 828(fp) -- Store: [0x80013514]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x6a262c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005eec]:fmul.s t6, t5, t4, dyn
	-[0x80005ef0]:csrrs a2, fcsr, zero
	-[0x80005ef4]:sw t6, 832(fp)
Current Store : [0x80005ef8] : sw a2, 836(fp) -- Store: [0x8001351c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x6a262c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005f2c]:fmul.s t6, t5, t4, dyn
	-[0x80005f30]:csrrs a2, fcsr, zero
	-[0x80005f34]:sw t6, 840(fp)
Current Store : [0x80005f38] : sw a2, 844(fp) -- Store: [0x80013524]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x1b6277 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005f6c]:fmul.s t6, t5, t4, dyn
	-[0x80005f70]:csrrs a2, fcsr, zero
	-[0x80005f74]:sw t6, 848(fp)
Current Store : [0x80005f78] : sw a2, 852(fp) -- Store: [0x8001352c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x1b6277 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005fac]:fmul.s t6, t5, t4, dyn
	-[0x80005fb0]:csrrs a2, fcsr, zero
	-[0x80005fb4]:sw t6, 856(fp)
Current Store : [0x80005fb8] : sw a2, 860(fp) -- Store: [0x80013534]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x1b6277 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005fec]:fmul.s t6, t5, t4, dyn
	-[0x80005ff0]:csrrs a2, fcsr, zero
	-[0x80005ff4]:sw t6, 864(fp)
Current Store : [0x80005ff8] : sw a2, 868(fp) -- Store: [0x8001353c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x1b6277 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000602c]:fmul.s t6, t5, t4, dyn
	-[0x80006030]:csrrs a2, fcsr, zero
	-[0x80006034]:sw t6, 872(fp)
Current Store : [0x80006038] : sw a2, 876(fp) -- Store: [0x80013544]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x1b6277 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000606c]:fmul.s t6, t5, t4, dyn
	-[0x80006070]:csrrs a2, fcsr, zero
	-[0x80006074]:sw t6, 880(fp)
Current Store : [0x80006078] : sw a2, 884(fp) -- Store: [0x8001354c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2165be and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800060ac]:fmul.s t6, t5, t4, dyn
	-[0x800060b0]:csrrs a2, fcsr, zero
	-[0x800060b4]:sw t6, 888(fp)
Current Store : [0x800060b8] : sw a2, 892(fp) -- Store: [0x80013554]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2165be and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800060ec]:fmul.s t6, t5, t4, dyn
	-[0x800060f0]:csrrs a2, fcsr, zero
	-[0x800060f4]:sw t6, 896(fp)
Current Store : [0x800060f8] : sw a2, 900(fp) -- Store: [0x8001355c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2165be and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000612c]:fmul.s t6, t5, t4, dyn
	-[0x80006130]:csrrs a2, fcsr, zero
	-[0x80006134]:sw t6, 904(fp)
Current Store : [0x80006138] : sw a2, 908(fp) -- Store: [0x80013564]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2165be and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000616c]:fmul.s t6, t5, t4, dyn
	-[0x80006170]:csrrs a2, fcsr, zero
	-[0x80006174]:sw t6, 912(fp)
Current Store : [0x80006178] : sw a2, 916(fp) -- Store: [0x8001356c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2165be and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800061ac]:fmul.s t6, t5, t4, dyn
	-[0x800061b0]:csrrs a2, fcsr, zero
	-[0x800061b4]:sw t6, 920(fp)
Current Store : [0x800061b8] : sw a2, 924(fp) -- Store: [0x80013574]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x41315c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800061ec]:fmul.s t6, t5, t4, dyn
	-[0x800061f0]:csrrs a2, fcsr, zero
	-[0x800061f4]:sw t6, 928(fp)
Current Store : [0x800061f8] : sw a2, 932(fp) -- Store: [0x8001357c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x41315c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000622c]:fmul.s t6, t5, t4, dyn
	-[0x80006230]:csrrs a2, fcsr, zero
	-[0x80006234]:sw t6, 936(fp)
Current Store : [0x80006238] : sw a2, 940(fp) -- Store: [0x80013584]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x41315c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000626c]:fmul.s t6, t5, t4, dyn
	-[0x80006270]:csrrs a2, fcsr, zero
	-[0x80006274]:sw t6, 944(fp)
Current Store : [0x80006278] : sw a2, 948(fp) -- Store: [0x8001358c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x41315c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800062ac]:fmul.s t6, t5, t4, dyn
	-[0x800062b0]:csrrs a2, fcsr, zero
	-[0x800062b4]:sw t6, 952(fp)
Current Store : [0x800062b8] : sw a2, 956(fp) -- Store: [0x80013594]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x41315c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800062ec]:fmul.s t6, t5, t4, dyn
	-[0x800062f0]:csrrs a2, fcsr, zero
	-[0x800062f4]:sw t6, 960(fp)
Current Store : [0x800062f8] : sw a2, 964(fp) -- Store: [0x8001359c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x1db2ee and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000632c]:fmul.s t6, t5, t4, dyn
	-[0x80006330]:csrrs a2, fcsr, zero
	-[0x80006334]:sw t6, 968(fp)
Current Store : [0x80006338] : sw a2, 972(fp) -- Store: [0x800135a4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x1db2ee and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000636c]:fmul.s t6, t5, t4, dyn
	-[0x80006370]:csrrs a2, fcsr, zero
	-[0x80006374]:sw t6, 976(fp)
Current Store : [0x80006378] : sw a2, 980(fp) -- Store: [0x800135ac]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x1db2ee and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800063ac]:fmul.s t6, t5, t4, dyn
	-[0x800063b0]:csrrs a2, fcsr, zero
	-[0x800063b4]:sw t6, 984(fp)
Current Store : [0x800063b8] : sw a2, 988(fp) -- Store: [0x800135b4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x1db2ee and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800063ec]:fmul.s t6, t5, t4, dyn
	-[0x800063f0]:csrrs a2, fcsr, zero
	-[0x800063f4]:sw t6, 992(fp)
Current Store : [0x800063f8] : sw a2, 996(fp) -- Store: [0x800135bc]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x1db2ee and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006424]:fmul.s t6, t5, t4, dyn
	-[0x80006428]:csrrs a2, fcsr, zero
	-[0x8000642c]:sw t6, 1000(fp)
Current Store : [0x80006430] : sw a2, 1004(fp) -- Store: [0x800135c4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1fef00 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000645c]:fmul.s t6, t5, t4, dyn
	-[0x80006460]:csrrs a2, fcsr, zero
	-[0x80006464]:sw t6, 1008(fp)
Current Store : [0x80006468] : sw a2, 1012(fp) -- Store: [0x800135cc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1fef00 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006494]:fmul.s t6, t5, t4, dyn
	-[0x80006498]:csrrs a2, fcsr, zero
	-[0x8000649c]:sw t6, 1016(fp)
Current Store : [0x800064a0] : sw a2, 1020(fp) -- Store: [0x800135d4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1fef00 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800064d4]:fmul.s t6, t5, t4, dyn
	-[0x800064d8]:csrrs a2, fcsr, zero
	-[0x800064dc]:sw t6, 0(fp)
Current Store : [0x800064e0] : sw a2, 4(fp) -- Store: [0x800135dc]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1fef00 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000650c]:fmul.s t6, t5, t4, dyn
	-[0x80006510]:csrrs a2, fcsr, zero
	-[0x80006514]:sw t6, 8(fp)
Current Store : [0x80006518] : sw a2, 12(fp) -- Store: [0x800135e4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1fef00 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006544]:fmul.s t6, t5, t4, dyn
	-[0x80006548]:csrrs a2, fcsr, zero
	-[0x8000654c]:sw t6, 16(fp)
Current Store : [0x80006550] : sw a2, 20(fp) -- Store: [0x800135ec]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x62ae46 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000657c]:fmul.s t6, t5, t4, dyn
	-[0x80006580]:csrrs a2, fcsr, zero
	-[0x80006584]:sw t6, 24(fp)
Current Store : [0x80006588] : sw a2, 28(fp) -- Store: [0x800135f4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x62ae46 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800065b4]:fmul.s t6, t5, t4, dyn
	-[0x800065b8]:csrrs a2, fcsr, zero
	-[0x800065bc]:sw t6, 32(fp)
Current Store : [0x800065c0] : sw a2, 36(fp) -- Store: [0x800135fc]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x62ae46 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800065ec]:fmul.s t6, t5, t4, dyn
	-[0x800065f0]:csrrs a2, fcsr, zero
	-[0x800065f4]:sw t6, 40(fp)
Current Store : [0x800065f8] : sw a2, 44(fp) -- Store: [0x80013604]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x62ae46 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006624]:fmul.s t6, t5, t4, dyn
	-[0x80006628]:csrrs a2, fcsr, zero
	-[0x8000662c]:sw t6, 48(fp)
Current Store : [0x80006630] : sw a2, 52(fp) -- Store: [0x8001360c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x62ae46 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000665c]:fmul.s t6, t5, t4, dyn
	-[0x80006660]:csrrs a2, fcsr, zero
	-[0x80006664]:sw t6, 56(fp)
Current Store : [0x80006668] : sw a2, 60(fp) -- Store: [0x80013614]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x0f31c4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006694]:fmul.s t6, t5, t4, dyn
	-[0x80006698]:csrrs a2, fcsr, zero
	-[0x8000669c]:sw t6, 64(fp)
Current Store : [0x800066a0] : sw a2, 68(fp) -- Store: [0x8001361c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x0f31c4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800066cc]:fmul.s t6, t5, t4, dyn
	-[0x800066d0]:csrrs a2, fcsr, zero
	-[0x800066d4]:sw t6, 72(fp)
Current Store : [0x800066d8] : sw a2, 76(fp) -- Store: [0x80013624]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x0f31c4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006704]:fmul.s t6, t5, t4, dyn
	-[0x80006708]:csrrs a2, fcsr, zero
	-[0x8000670c]:sw t6, 80(fp)
Current Store : [0x80006710] : sw a2, 84(fp) -- Store: [0x8001362c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x0f31c4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000673c]:fmul.s t6, t5, t4, dyn
	-[0x80006740]:csrrs a2, fcsr, zero
	-[0x80006744]:sw t6, 88(fp)
Current Store : [0x80006748] : sw a2, 92(fp) -- Store: [0x80013634]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x0f31c4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006774]:fmul.s t6, t5, t4, dyn
	-[0x80006778]:csrrs a2, fcsr, zero
	-[0x8000677c]:sw t6, 96(fp)
Current Store : [0x80006780] : sw a2, 100(fp) -- Store: [0x8001363c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x6b27f7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800067ac]:fmul.s t6, t5, t4, dyn
	-[0x800067b0]:csrrs a2, fcsr, zero
	-[0x800067b4]:sw t6, 104(fp)
Current Store : [0x800067b8] : sw a2, 108(fp) -- Store: [0x80013644]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x6b27f7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800067e4]:fmul.s t6, t5, t4, dyn
	-[0x800067e8]:csrrs a2, fcsr, zero
	-[0x800067ec]:sw t6, 112(fp)
Current Store : [0x800067f0] : sw a2, 116(fp) -- Store: [0x8001364c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x6b27f7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000681c]:fmul.s t6, t5, t4, dyn
	-[0x80006820]:csrrs a2, fcsr, zero
	-[0x80006824]:sw t6, 120(fp)
Current Store : [0x80006828] : sw a2, 124(fp) -- Store: [0x80013654]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x6b27f7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006854]:fmul.s t6, t5, t4, dyn
	-[0x80006858]:csrrs a2, fcsr, zero
	-[0x8000685c]:sw t6, 128(fp)
Current Store : [0x80006860] : sw a2, 132(fp) -- Store: [0x8001365c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x6b27f7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000688c]:fmul.s t6, t5, t4, dyn
	-[0x80006890]:csrrs a2, fcsr, zero
	-[0x80006894]:sw t6, 136(fp)
Current Store : [0x80006898] : sw a2, 140(fp) -- Store: [0x80013664]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x13bb57 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800068c4]:fmul.s t6, t5, t4, dyn
	-[0x800068c8]:csrrs a2, fcsr, zero
	-[0x800068cc]:sw t6, 144(fp)
Current Store : [0x800068d0] : sw a2, 148(fp) -- Store: [0x8001366c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x13bb57 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800068fc]:fmul.s t6, t5, t4, dyn
	-[0x80006900]:csrrs a2, fcsr, zero
	-[0x80006904]:sw t6, 152(fp)
Current Store : [0x80006908] : sw a2, 156(fp) -- Store: [0x80013674]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x13bb57 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006934]:fmul.s t6, t5, t4, dyn
	-[0x80006938]:csrrs a2, fcsr, zero
	-[0x8000693c]:sw t6, 160(fp)
Current Store : [0x80006940] : sw a2, 164(fp) -- Store: [0x8001367c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x13bb57 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000696c]:fmul.s t6, t5, t4, dyn
	-[0x80006970]:csrrs a2, fcsr, zero
	-[0x80006974]:sw t6, 168(fp)
Current Store : [0x80006978] : sw a2, 172(fp) -- Store: [0x80013684]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x13bb57 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800069a4]:fmul.s t6, t5, t4, dyn
	-[0x800069a8]:csrrs a2, fcsr, zero
	-[0x800069ac]:sw t6, 176(fp)
Current Store : [0x800069b0] : sw a2, 180(fp) -- Store: [0x8001368c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x1671a2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800069dc]:fmul.s t6, t5, t4, dyn
	-[0x800069e0]:csrrs a2, fcsr, zero
	-[0x800069e4]:sw t6, 184(fp)
Current Store : [0x800069e8] : sw a2, 188(fp) -- Store: [0x80013694]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x1671a2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006a14]:fmul.s t6, t5, t4, dyn
	-[0x80006a18]:csrrs a2, fcsr, zero
	-[0x80006a1c]:sw t6, 192(fp)
Current Store : [0x80006a20] : sw a2, 196(fp) -- Store: [0x8001369c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x1671a2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006a4c]:fmul.s t6, t5, t4, dyn
	-[0x80006a50]:csrrs a2, fcsr, zero
	-[0x80006a54]:sw t6, 200(fp)
Current Store : [0x80006a58] : sw a2, 204(fp) -- Store: [0x800136a4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x1671a2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006a84]:fmul.s t6, t5, t4, dyn
	-[0x80006a88]:csrrs a2, fcsr, zero
	-[0x80006a8c]:sw t6, 208(fp)
Current Store : [0x80006a90] : sw a2, 212(fp) -- Store: [0x800136ac]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x1671a2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006abc]:fmul.s t6, t5, t4, dyn
	-[0x80006ac0]:csrrs a2, fcsr, zero
	-[0x80006ac4]:sw t6, 216(fp)
Current Store : [0x80006ac8] : sw a2, 220(fp) -- Store: [0x800136b4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x10affc and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006af4]:fmul.s t6, t5, t4, dyn
	-[0x80006af8]:csrrs a2, fcsr, zero
	-[0x80006afc]:sw t6, 224(fp)
Current Store : [0x80006b00] : sw a2, 228(fp) -- Store: [0x800136bc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x10affc and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006b2c]:fmul.s t6, t5, t4, dyn
	-[0x80006b30]:csrrs a2, fcsr, zero
	-[0x80006b34]:sw t6, 232(fp)
Current Store : [0x80006b38] : sw a2, 236(fp) -- Store: [0x800136c4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x10affc and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006b64]:fmul.s t6, t5, t4, dyn
	-[0x80006b68]:csrrs a2, fcsr, zero
	-[0x80006b6c]:sw t6, 240(fp)
Current Store : [0x80006b70] : sw a2, 244(fp) -- Store: [0x800136cc]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x10affc and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006b9c]:fmul.s t6, t5, t4, dyn
	-[0x80006ba0]:csrrs a2, fcsr, zero
	-[0x80006ba4]:sw t6, 248(fp)
Current Store : [0x80006ba8] : sw a2, 252(fp) -- Store: [0x800136d4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x10affc and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006bd4]:fmul.s t6, t5, t4, dyn
	-[0x80006bd8]:csrrs a2, fcsr, zero
	-[0x80006bdc]:sw t6, 256(fp)
Current Store : [0x80006be0] : sw a2, 260(fp) -- Store: [0x800136dc]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x3af9fa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006c0c]:fmul.s t6, t5, t4, dyn
	-[0x80006c10]:csrrs a2, fcsr, zero
	-[0x80006c14]:sw t6, 264(fp)
Current Store : [0x80006c18] : sw a2, 268(fp) -- Store: [0x800136e4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x3af9fa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006c44]:fmul.s t6, t5, t4, dyn
	-[0x80006c48]:csrrs a2, fcsr, zero
	-[0x80006c4c]:sw t6, 272(fp)
Current Store : [0x80006c50] : sw a2, 276(fp) -- Store: [0x800136ec]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x3af9fa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006c7c]:fmul.s t6, t5, t4, dyn
	-[0x80006c80]:csrrs a2, fcsr, zero
	-[0x80006c84]:sw t6, 280(fp)
Current Store : [0x80006c88] : sw a2, 284(fp) -- Store: [0x800136f4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x3af9fa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006cb4]:fmul.s t6, t5, t4, dyn
	-[0x80006cb8]:csrrs a2, fcsr, zero
	-[0x80006cbc]:sw t6, 288(fp)
Current Store : [0x80006cc0] : sw a2, 292(fp) -- Store: [0x800136fc]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x3af9fa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006cec]:fmul.s t6, t5, t4, dyn
	-[0x80006cf0]:csrrs a2, fcsr, zero
	-[0x80006cf4]:sw t6, 296(fp)
Current Store : [0x80006cf8] : sw a2, 300(fp) -- Store: [0x80013704]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x775433 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006d24]:fmul.s t6, t5, t4, dyn
	-[0x80006d28]:csrrs a2, fcsr, zero
	-[0x80006d2c]:sw t6, 304(fp)
Current Store : [0x80006d30] : sw a2, 308(fp) -- Store: [0x8001370c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x775433 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006d5c]:fmul.s t6, t5, t4, dyn
	-[0x80006d60]:csrrs a2, fcsr, zero
	-[0x80006d64]:sw t6, 312(fp)
Current Store : [0x80006d68] : sw a2, 316(fp) -- Store: [0x80013714]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x775433 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006d94]:fmul.s t6, t5, t4, dyn
	-[0x80006d98]:csrrs a2, fcsr, zero
	-[0x80006d9c]:sw t6, 320(fp)
Current Store : [0x80006da0] : sw a2, 324(fp) -- Store: [0x8001371c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x775433 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006dcc]:fmul.s t6, t5, t4, dyn
	-[0x80006dd0]:csrrs a2, fcsr, zero
	-[0x80006dd4]:sw t6, 328(fp)
Current Store : [0x80006dd8] : sw a2, 332(fp) -- Store: [0x80013724]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x775433 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006e04]:fmul.s t6, t5, t4, dyn
	-[0x80006e08]:csrrs a2, fcsr, zero
	-[0x80006e0c]:sw t6, 336(fp)
Current Store : [0x80006e10] : sw a2, 340(fp) -- Store: [0x8001372c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x762408 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006e3c]:fmul.s t6, t5, t4, dyn
	-[0x80006e40]:csrrs a2, fcsr, zero
	-[0x80006e44]:sw t6, 344(fp)
Current Store : [0x80006e48] : sw a2, 348(fp) -- Store: [0x80013734]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x762408 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006e74]:fmul.s t6, t5, t4, dyn
	-[0x80006e78]:csrrs a2, fcsr, zero
	-[0x80006e7c]:sw t6, 352(fp)
Current Store : [0x80006e80] : sw a2, 356(fp) -- Store: [0x8001373c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x762408 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006eac]:fmul.s t6, t5, t4, dyn
	-[0x80006eb0]:csrrs a2, fcsr, zero
	-[0x80006eb4]:sw t6, 360(fp)
Current Store : [0x80006eb8] : sw a2, 364(fp) -- Store: [0x80013744]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x762408 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006ee4]:fmul.s t6, t5, t4, dyn
	-[0x80006ee8]:csrrs a2, fcsr, zero
	-[0x80006eec]:sw t6, 368(fp)
Current Store : [0x80006ef0] : sw a2, 372(fp) -- Store: [0x8001374c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x762408 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006f1c]:fmul.s t6, t5, t4, dyn
	-[0x80006f20]:csrrs a2, fcsr, zero
	-[0x80006f24]:sw t6, 376(fp)
Current Store : [0x80006f28] : sw a2, 380(fp) -- Store: [0x80013754]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x04fd41 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006f54]:fmul.s t6, t5, t4, dyn
	-[0x80006f58]:csrrs a2, fcsr, zero
	-[0x80006f5c]:sw t6, 384(fp)
Current Store : [0x80006f60] : sw a2, 388(fp) -- Store: [0x8001375c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x04fd41 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006f8c]:fmul.s t6, t5, t4, dyn
	-[0x80006f90]:csrrs a2, fcsr, zero
	-[0x80006f94]:sw t6, 392(fp)
Current Store : [0x80006f98] : sw a2, 396(fp) -- Store: [0x80013764]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x04fd41 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006fc4]:fmul.s t6, t5, t4, dyn
	-[0x80006fc8]:csrrs a2, fcsr, zero
	-[0x80006fcc]:sw t6, 400(fp)
Current Store : [0x80006fd0] : sw a2, 404(fp) -- Store: [0x8001376c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x04fd41 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006ffc]:fmul.s t6, t5, t4, dyn
	-[0x80007000]:csrrs a2, fcsr, zero
	-[0x80007004]:sw t6, 408(fp)
Current Store : [0x80007008] : sw a2, 412(fp) -- Store: [0x80013774]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x04fd41 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007034]:fmul.s t6, t5, t4, dyn
	-[0x80007038]:csrrs a2, fcsr, zero
	-[0x8000703c]:sw t6, 416(fp)
Current Store : [0x80007040] : sw a2, 420(fp) -- Store: [0x8001377c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7d1e07 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000706c]:fmul.s t6, t5, t4, dyn
	-[0x80007070]:csrrs a2, fcsr, zero
	-[0x80007074]:sw t6, 424(fp)
Current Store : [0x80007078] : sw a2, 428(fp) -- Store: [0x80013784]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7d1e07 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800070a4]:fmul.s t6, t5, t4, dyn
	-[0x800070a8]:csrrs a2, fcsr, zero
	-[0x800070ac]:sw t6, 432(fp)
Current Store : [0x800070b0] : sw a2, 436(fp) -- Store: [0x8001378c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7d1e07 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800070dc]:fmul.s t6, t5, t4, dyn
	-[0x800070e0]:csrrs a2, fcsr, zero
	-[0x800070e4]:sw t6, 440(fp)
Current Store : [0x800070e8] : sw a2, 444(fp) -- Store: [0x80013794]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7d1e07 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007114]:fmul.s t6, t5, t4, dyn
	-[0x80007118]:csrrs a2, fcsr, zero
	-[0x8000711c]:sw t6, 448(fp)
Current Store : [0x80007120] : sw a2, 452(fp) -- Store: [0x8001379c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7d1e07 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000714c]:fmul.s t6, t5, t4, dyn
	-[0x80007150]:csrrs a2, fcsr, zero
	-[0x80007154]:sw t6, 456(fp)
Current Store : [0x80007158] : sw a2, 460(fp) -- Store: [0x800137a4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0aea5e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007184]:fmul.s t6, t5, t4, dyn
	-[0x80007188]:csrrs a2, fcsr, zero
	-[0x8000718c]:sw t6, 464(fp)
Current Store : [0x80007190] : sw a2, 468(fp) -- Store: [0x800137ac]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0aea5e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800071bc]:fmul.s t6, t5, t4, dyn
	-[0x800071c0]:csrrs a2, fcsr, zero
	-[0x800071c4]:sw t6, 472(fp)
Current Store : [0x800071c8] : sw a2, 476(fp) -- Store: [0x800137b4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0aea5e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800071f4]:fmul.s t6, t5, t4, dyn
	-[0x800071f8]:csrrs a2, fcsr, zero
	-[0x800071fc]:sw t6, 480(fp)
Current Store : [0x80007200] : sw a2, 484(fp) -- Store: [0x800137bc]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0aea5e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000722c]:fmul.s t6, t5, t4, dyn
	-[0x80007230]:csrrs a2, fcsr, zero
	-[0x80007234]:sw t6, 488(fp)
Current Store : [0x80007238] : sw a2, 492(fp) -- Store: [0x800137c4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0aea5e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007264]:fmul.s t6, t5, t4, dyn
	-[0x80007268]:csrrs a2, fcsr, zero
	-[0x8000726c]:sw t6, 496(fp)
Current Store : [0x80007270] : sw a2, 500(fp) -- Store: [0x800137cc]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x094fff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000729c]:fmul.s t6, t5, t4, dyn
	-[0x800072a0]:csrrs a2, fcsr, zero
	-[0x800072a4]:sw t6, 504(fp)
Current Store : [0x800072a8] : sw a2, 508(fp) -- Store: [0x800137d4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x094fff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800072d4]:fmul.s t6, t5, t4, dyn
	-[0x800072d8]:csrrs a2, fcsr, zero
	-[0x800072dc]:sw t6, 512(fp)
Current Store : [0x800072e0] : sw a2, 516(fp) -- Store: [0x800137dc]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x094fff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000730c]:fmul.s t6, t5, t4, dyn
	-[0x80007310]:csrrs a2, fcsr, zero
	-[0x80007314]:sw t6, 520(fp)
Current Store : [0x80007318] : sw a2, 524(fp) -- Store: [0x800137e4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x094fff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007344]:fmul.s t6, t5, t4, dyn
	-[0x80007348]:csrrs a2, fcsr, zero
	-[0x8000734c]:sw t6, 528(fp)
Current Store : [0x80007350] : sw a2, 532(fp) -- Store: [0x800137ec]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x094fff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000737c]:fmul.s t6, t5, t4, dyn
	-[0x80007380]:csrrs a2, fcsr, zero
	-[0x80007384]:sw t6, 536(fp)
Current Store : [0x80007388] : sw a2, 540(fp) -- Store: [0x800137f4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x195ac9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800073b4]:fmul.s t6, t5, t4, dyn
	-[0x800073b8]:csrrs a2, fcsr, zero
	-[0x800073bc]:sw t6, 544(fp)
Current Store : [0x800073c0] : sw a2, 548(fp) -- Store: [0x800137fc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x195ac9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800073ec]:fmul.s t6, t5, t4, dyn
	-[0x800073f0]:csrrs a2, fcsr, zero
	-[0x800073f4]:sw t6, 552(fp)
Current Store : [0x800073f8] : sw a2, 556(fp) -- Store: [0x80013804]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x195ac9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007424]:fmul.s t6, t5, t4, dyn
	-[0x80007428]:csrrs a2, fcsr, zero
	-[0x8000742c]:sw t6, 560(fp)
Current Store : [0x80007430] : sw a2, 564(fp) -- Store: [0x8001380c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x195ac9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000745c]:fmul.s t6, t5, t4, dyn
	-[0x80007460]:csrrs a2, fcsr, zero
	-[0x80007464]:sw t6, 568(fp)
Current Store : [0x80007468] : sw a2, 572(fp) -- Store: [0x80013814]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x195ac9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007494]:fmul.s t6, t5, t4, dyn
	-[0x80007498]:csrrs a2, fcsr, zero
	-[0x8000749c]:sw t6, 576(fp)
Current Store : [0x800074a0] : sw a2, 580(fp) -- Store: [0x8001381c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4a19c2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800074cc]:fmul.s t6, t5, t4, dyn
	-[0x800074d0]:csrrs a2, fcsr, zero
	-[0x800074d4]:sw t6, 584(fp)
Current Store : [0x800074d8] : sw a2, 588(fp) -- Store: [0x80013824]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4a19c2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007504]:fmul.s t6, t5, t4, dyn
	-[0x80007508]:csrrs a2, fcsr, zero
	-[0x8000750c]:sw t6, 592(fp)
Current Store : [0x80007510] : sw a2, 596(fp) -- Store: [0x8001382c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4a19c2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000753c]:fmul.s t6, t5, t4, dyn
	-[0x80007540]:csrrs a2, fcsr, zero
	-[0x80007544]:sw t6, 600(fp)
Current Store : [0x80007548] : sw a2, 604(fp) -- Store: [0x80013834]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4a19c2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007574]:fmul.s t6, t5, t4, dyn
	-[0x80007578]:csrrs a2, fcsr, zero
	-[0x8000757c]:sw t6, 608(fp)
Current Store : [0x80007580] : sw a2, 612(fp) -- Store: [0x8001383c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4a19c2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800075ac]:fmul.s t6, t5, t4, dyn
	-[0x800075b0]:csrrs a2, fcsr, zero
	-[0x800075b4]:sw t6, 616(fp)
Current Store : [0x800075b8] : sw a2, 620(fp) -- Store: [0x80013844]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x374d41 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800075e4]:fmul.s t6, t5, t4, dyn
	-[0x800075e8]:csrrs a2, fcsr, zero
	-[0x800075ec]:sw t6, 624(fp)
Current Store : [0x800075f0] : sw a2, 628(fp) -- Store: [0x8001384c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x374d41 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000761c]:fmul.s t6, t5, t4, dyn
	-[0x80007620]:csrrs a2, fcsr, zero
	-[0x80007624]:sw t6, 632(fp)
Current Store : [0x80007628] : sw a2, 636(fp) -- Store: [0x80013854]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x374d41 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007654]:fmul.s t6, t5, t4, dyn
	-[0x80007658]:csrrs a2, fcsr, zero
	-[0x8000765c]:sw t6, 640(fp)
Current Store : [0x80007660] : sw a2, 644(fp) -- Store: [0x8001385c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x374d41 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000768c]:fmul.s t6, t5, t4, dyn
	-[0x80007690]:csrrs a2, fcsr, zero
	-[0x80007694]:sw t6, 648(fp)
Current Store : [0x80007698] : sw a2, 652(fp) -- Store: [0x80013864]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x374d41 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800076c4]:fmul.s t6, t5, t4, dyn
	-[0x800076c8]:csrrs a2, fcsr, zero
	-[0x800076cc]:sw t6, 656(fp)
Current Store : [0x800076d0] : sw a2, 660(fp) -- Store: [0x8001386c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x32f44f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800076fc]:fmul.s t6, t5, t4, dyn
	-[0x80007700]:csrrs a2, fcsr, zero
	-[0x80007704]:sw t6, 664(fp)
Current Store : [0x80007708] : sw a2, 668(fp) -- Store: [0x80013874]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x32f44f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007734]:fmul.s t6, t5, t4, dyn
	-[0x80007738]:csrrs a2, fcsr, zero
	-[0x8000773c]:sw t6, 672(fp)
Current Store : [0x80007740] : sw a2, 676(fp) -- Store: [0x8001387c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x32f44f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000776c]:fmul.s t6, t5, t4, dyn
	-[0x80007770]:csrrs a2, fcsr, zero
	-[0x80007774]:sw t6, 680(fp)
Current Store : [0x80007778] : sw a2, 684(fp) -- Store: [0x80013884]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x32f44f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800077a4]:fmul.s t6, t5, t4, dyn
	-[0x800077a8]:csrrs a2, fcsr, zero
	-[0x800077ac]:sw t6, 688(fp)
Current Store : [0x800077b0] : sw a2, 692(fp) -- Store: [0x8001388c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x32f44f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800077dc]:fmul.s t6, t5, t4, dyn
	-[0x800077e0]:csrrs a2, fcsr, zero
	-[0x800077e4]:sw t6, 696(fp)
Current Store : [0x800077e8] : sw a2, 700(fp) -- Store: [0x80013894]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x63ea1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007814]:fmul.s t6, t5, t4, dyn
	-[0x80007818]:csrrs a2, fcsr, zero
	-[0x8000781c]:sw t6, 704(fp)
Current Store : [0x80007820] : sw a2, 708(fp) -- Store: [0x8001389c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x63ea1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000784c]:fmul.s t6, t5, t4, dyn
	-[0x80007850]:csrrs a2, fcsr, zero
	-[0x80007854]:sw t6, 712(fp)
Current Store : [0x80007858] : sw a2, 716(fp) -- Store: [0x800138a4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x63ea1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007884]:fmul.s t6, t5, t4, dyn
	-[0x80007888]:csrrs a2, fcsr, zero
	-[0x8000788c]:sw t6, 720(fp)
Current Store : [0x80007890] : sw a2, 724(fp) -- Store: [0x800138ac]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x63ea1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800078bc]:fmul.s t6, t5, t4, dyn
	-[0x800078c0]:csrrs a2, fcsr, zero
	-[0x800078c4]:sw t6, 728(fp)
Current Store : [0x800078c8] : sw a2, 732(fp) -- Store: [0x800138b4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x63ea1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800078f4]:fmul.s t6, t5, t4, dyn
	-[0x800078f8]:csrrs a2, fcsr, zero
	-[0x800078fc]:sw t6, 736(fp)
Current Store : [0x80007900] : sw a2, 740(fp) -- Store: [0x800138bc]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3bb1f5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000792c]:fmul.s t6, t5, t4, dyn
	-[0x80007930]:csrrs a2, fcsr, zero
	-[0x80007934]:sw t6, 744(fp)
Current Store : [0x80007938] : sw a2, 748(fp) -- Store: [0x800138c4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3bb1f5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007964]:fmul.s t6, t5, t4, dyn
	-[0x80007968]:csrrs a2, fcsr, zero
	-[0x8000796c]:sw t6, 752(fp)
Current Store : [0x80007970] : sw a2, 756(fp) -- Store: [0x800138cc]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3bb1f5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000799c]:fmul.s t6, t5, t4, dyn
	-[0x800079a0]:csrrs a2, fcsr, zero
	-[0x800079a4]:sw t6, 760(fp)
Current Store : [0x800079a8] : sw a2, 764(fp) -- Store: [0x800138d4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3bb1f5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800079d4]:fmul.s t6, t5, t4, dyn
	-[0x800079d8]:csrrs a2, fcsr, zero
	-[0x800079dc]:sw t6, 768(fp)
Current Store : [0x800079e0] : sw a2, 772(fp) -- Store: [0x800138dc]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3bb1f5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007a0c]:fmul.s t6, t5, t4, dyn
	-[0x80007a10]:csrrs a2, fcsr, zero
	-[0x80007a14]:sw t6, 776(fp)
Current Store : [0x80007a18] : sw a2, 780(fp) -- Store: [0x800138e4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x243814 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007a44]:fmul.s t6, t5, t4, dyn
	-[0x80007a48]:csrrs a2, fcsr, zero
	-[0x80007a4c]:sw t6, 784(fp)
Current Store : [0x80007a50] : sw a2, 788(fp) -- Store: [0x800138ec]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x243814 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007a7c]:fmul.s t6, t5, t4, dyn
	-[0x80007a80]:csrrs a2, fcsr, zero
	-[0x80007a84]:sw t6, 792(fp)
Current Store : [0x80007a88] : sw a2, 796(fp) -- Store: [0x800138f4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x243814 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007ab4]:fmul.s t6, t5, t4, dyn
	-[0x80007ab8]:csrrs a2, fcsr, zero
	-[0x80007abc]:sw t6, 800(fp)
Current Store : [0x80007ac0] : sw a2, 804(fp) -- Store: [0x800138fc]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x243814 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007aec]:fmul.s t6, t5, t4, dyn
	-[0x80007af0]:csrrs a2, fcsr, zero
	-[0x80007af4]:sw t6, 808(fp)
Current Store : [0x80007af8] : sw a2, 812(fp) -- Store: [0x80013904]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x243814 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007b24]:fmul.s t6, t5, t4, dyn
	-[0x80007b28]:csrrs a2, fcsr, zero
	-[0x80007b2c]:sw t6, 816(fp)
Current Store : [0x80007b30] : sw a2, 820(fp) -- Store: [0x8001390c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x305e0d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007b5c]:fmul.s t6, t5, t4, dyn
	-[0x80007b60]:csrrs a2, fcsr, zero
	-[0x80007b64]:sw t6, 824(fp)
Current Store : [0x80007b68] : sw a2, 828(fp) -- Store: [0x80013914]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x305e0d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007b94]:fmul.s t6, t5, t4, dyn
	-[0x80007b98]:csrrs a2, fcsr, zero
	-[0x80007b9c]:sw t6, 832(fp)
Current Store : [0x80007ba0] : sw a2, 836(fp) -- Store: [0x8001391c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x305e0d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007bcc]:fmul.s t6, t5, t4, dyn
	-[0x80007bd0]:csrrs a2, fcsr, zero
	-[0x80007bd4]:sw t6, 840(fp)
Current Store : [0x80007bd8] : sw a2, 844(fp) -- Store: [0x80013924]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x305e0d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007c04]:fmul.s t6, t5, t4, dyn
	-[0x80007c08]:csrrs a2, fcsr, zero
	-[0x80007c0c]:sw t6, 848(fp)
Current Store : [0x80007c10] : sw a2, 852(fp) -- Store: [0x8001392c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x305e0d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007c3c]:fmul.s t6, t5, t4, dyn
	-[0x80007c40]:csrrs a2, fcsr, zero
	-[0x80007c44]:sw t6, 856(fp)
Current Store : [0x80007c48] : sw a2, 860(fp) -- Store: [0x80013934]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d071f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007c74]:fmul.s t6, t5, t4, dyn
	-[0x80007c78]:csrrs a2, fcsr, zero
	-[0x80007c7c]:sw t6, 864(fp)
Current Store : [0x80007c80] : sw a2, 868(fp) -- Store: [0x8001393c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d071f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007cac]:fmul.s t6, t5, t4, dyn
	-[0x80007cb0]:csrrs a2, fcsr, zero
	-[0x80007cb4]:sw t6, 872(fp)
Current Store : [0x80007cb8] : sw a2, 876(fp) -- Store: [0x80013944]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d071f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007ce4]:fmul.s t6, t5, t4, dyn
	-[0x80007ce8]:csrrs a2, fcsr, zero
	-[0x80007cec]:sw t6, 880(fp)
Current Store : [0x80007cf0] : sw a2, 884(fp) -- Store: [0x8001394c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d071f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007d1c]:fmul.s t6, t5, t4, dyn
	-[0x80007d20]:csrrs a2, fcsr, zero
	-[0x80007d24]:sw t6, 888(fp)
Current Store : [0x80007d28] : sw a2, 892(fp) -- Store: [0x80013954]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d071f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007d54]:fmul.s t6, t5, t4, dyn
	-[0x80007d58]:csrrs a2, fcsr, zero
	-[0x80007d5c]:sw t6, 896(fp)
Current Store : [0x80007d60] : sw a2, 900(fp) -- Store: [0x8001395c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x08b9d9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007d8c]:fmul.s t6, t5, t4, dyn
	-[0x80007d90]:csrrs a2, fcsr, zero
	-[0x80007d94]:sw t6, 904(fp)
Current Store : [0x80007d98] : sw a2, 908(fp) -- Store: [0x80013964]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x08b9d9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007dc4]:fmul.s t6, t5, t4, dyn
	-[0x80007dc8]:csrrs a2, fcsr, zero
	-[0x80007dcc]:sw t6, 912(fp)
Current Store : [0x80007dd0] : sw a2, 916(fp) -- Store: [0x8001396c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x08b9d9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007dfc]:fmul.s t6, t5, t4, dyn
	-[0x80007e00]:csrrs a2, fcsr, zero
	-[0x80007e04]:sw t6, 920(fp)
Current Store : [0x80007e08] : sw a2, 924(fp) -- Store: [0x80013974]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x08b9d9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007e34]:fmul.s t6, t5, t4, dyn
	-[0x80007e38]:csrrs a2, fcsr, zero
	-[0x80007e3c]:sw t6, 928(fp)
Current Store : [0x80007e40] : sw a2, 932(fp) -- Store: [0x8001397c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x08b9d9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007e6c]:fmul.s t6, t5, t4, dyn
	-[0x80007e70]:csrrs a2, fcsr, zero
	-[0x80007e74]:sw t6, 936(fp)
Current Store : [0x80007e78] : sw a2, 940(fp) -- Store: [0x80013984]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x61068d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007ea4]:fmul.s t6, t5, t4, dyn
	-[0x80007ea8]:csrrs a2, fcsr, zero
	-[0x80007eac]:sw t6, 944(fp)
Current Store : [0x80007eb0] : sw a2, 948(fp) -- Store: [0x8001398c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x61068d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007edc]:fmul.s t6, t5, t4, dyn
	-[0x80007ee0]:csrrs a2, fcsr, zero
	-[0x80007ee4]:sw t6, 952(fp)
Current Store : [0x80007ee8] : sw a2, 956(fp) -- Store: [0x80013994]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x61068d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007f14]:fmul.s t6, t5, t4, dyn
	-[0x80007f18]:csrrs a2, fcsr, zero
	-[0x80007f1c]:sw t6, 960(fp)
Current Store : [0x80007f20] : sw a2, 964(fp) -- Store: [0x8001399c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x61068d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007f4c]:fmul.s t6, t5, t4, dyn
	-[0x80007f50]:csrrs a2, fcsr, zero
	-[0x80007f54]:sw t6, 968(fp)
Current Store : [0x80007f58] : sw a2, 972(fp) -- Store: [0x800139a4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x61068d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007f84]:fmul.s t6, t5, t4, dyn
	-[0x80007f88]:csrrs a2, fcsr, zero
	-[0x80007f8c]:sw t6, 976(fp)
Current Store : [0x80007f90] : sw a2, 980(fp) -- Store: [0x800139ac]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x4d54a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007fbc]:fmul.s t6, t5, t4, dyn
	-[0x80007fc0]:csrrs a2, fcsr, zero
	-[0x80007fc4]:sw t6, 984(fp)
Current Store : [0x80007fc8] : sw a2, 988(fp) -- Store: [0x800139b4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x4d54a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007ff4]:fmul.s t6, t5, t4, dyn
	-[0x80007ff8]:csrrs a2, fcsr, zero
	-[0x80007ffc]:sw t6, 992(fp)
Current Store : [0x80008000] : sw a2, 996(fp) -- Store: [0x800139bc]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x4d54a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000802c]:fmul.s t6, t5, t4, dyn
	-[0x80008030]:csrrs a2, fcsr, zero
	-[0x80008034]:sw t6, 1000(fp)
Current Store : [0x80008038] : sw a2, 1004(fp) -- Store: [0x800139c4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x4d54a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008064]:fmul.s t6, t5, t4, dyn
	-[0x80008068]:csrrs a2, fcsr, zero
	-[0x8000806c]:sw t6, 1008(fp)
Current Store : [0x80008070] : sw a2, 1012(fp) -- Store: [0x800139cc]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x4d54a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000809c]:fmul.s t6, t5, t4, dyn
	-[0x800080a0]:csrrs a2, fcsr, zero
	-[0x800080a4]:sw t6, 1016(fp)
Current Store : [0x800080a8] : sw a2, 1020(fp) -- Store: [0x800139d4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2befa1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800080dc]:fmul.s t6, t5, t4, dyn
	-[0x800080e0]:csrrs a2, fcsr, zero
	-[0x800080e4]:sw t6, 0(fp)
Current Store : [0x800080e8] : sw a2, 4(fp) -- Store: [0x800139dc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2befa1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008114]:fmul.s t6, t5, t4, dyn
	-[0x80008118]:csrrs a2, fcsr, zero
	-[0x8000811c]:sw t6, 8(fp)
Current Store : [0x80008120] : sw a2, 12(fp) -- Store: [0x800139e4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2befa1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000814c]:fmul.s t6, t5, t4, dyn
	-[0x80008150]:csrrs a2, fcsr, zero
	-[0x80008154]:sw t6, 16(fp)
Current Store : [0x80008158] : sw a2, 20(fp) -- Store: [0x800139ec]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2befa1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008184]:fmul.s t6, t5, t4, dyn
	-[0x80008188]:csrrs a2, fcsr, zero
	-[0x8000818c]:sw t6, 24(fp)
Current Store : [0x80008190] : sw a2, 28(fp) -- Store: [0x800139f4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2befa1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800081bc]:fmul.s t6, t5, t4, dyn
	-[0x800081c0]:csrrs a2, fcsr, zero
	-[0x800081c4]:sw t6, 32(fp)
Current Store : [0x800081c8] : sw a2, 36(fp) -- Store: [0x800139fc]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x78dd0e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800081f4]:fmul.s t6, t5, t4, dyn
	-[0x800081f8]:csrrs a2, fcsr, zero
	-[0x800081fc]:sw t6, 40(fp)
Current Store : [0x80008200] : sw a2, 44(fp) -- Store: [0x80013a04]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x78dd0e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000822c]:fmul.s t6, t5, t4, dyn
	-[0x80008230]:csrrs a2, fcsr, zero
	-[0x80008234]:sw t6, 48(fp)
Current Store : [0x80008238] : sw a2, 52(fp) -- Store: [0x80013a0c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x78dd0e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008264]:fmul.s t6, t5, t4, dyn
	-[0x80008268]:csrrs a2, fcsr, zero
	-[0x8000826c]:sw t6, 56(fp)
Current Store : [0x80008270] : sw a2, 60(fp) -- Store: [0x80013a14]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x78dd0e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000829c]:fmul.s t6, t5, t4, dyn
	-[0x800082a0]:csrrs a2, fcsr, zero
	-[0x800082a4]:sw t6, 64(fp)
Current Store : [0x800082a8] : sw a2, 68(fp) -- Store: [0x80013a1c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x78dd0e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800082d4]:fmul.s t6, t5, t4, dyn
	-[0x800082d8]:csrrs a2, fcsr, zero
	-[0x800082dc]:sw t6, 72(fp)
Current Store : [0x800082e0] : sw a2, 76(fp) -- Store: [0x80013a24]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x639f2c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000830c]:fmul.s t6, t5, t4, dyn
	-[0x80008310]:csrrs a2, fcsr, zero
	-[0x80008314]:sw t6, 80(fp)
Current Store : [0x80008318] : sw a2, 84(fp) -- Store: [0x80013a2c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x639f2c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008344]:fmul.s t6, t5, t4, dyn
	-[0x80008348]:csrrs a2, fcsr, zero
	-[0x8000834c]:sw t6, 88(fp)
Current Store : [0x80008350] : sw a2, 92(fp) -- Store: [0x80013a34]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x639f2c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000837c]:fmul.s t6, t5, t4, dyn
	-[0x80008380]:csrrs a2, fcsr, zero
	-[0x80008384]:sw t6, 96(fp)
Current Store : [0x80008388] : sw a2, 100(fp) -- Store: [0x80013a3c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x639f2c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800083b4]:fmul.s t6, t5, t4, dyn
	-[0x800083b8]:csrrs a2, fcsr, zero
	-[0x800083bc]:sw t6, 104(fp)
Current Store : [0x800083c0] : sw a2, 108(fp) -- Store: [0x80013a44]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x639f2c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800083ec]:fmul.s t6, t5, t4, dyn
	-[0x800083f0]:csrrs a2, fcsr, zero
	-[0x800083f4]:sw t6, 112(fp)
Current Store : [0x800083f8] : sw a2, 116(fp) -- Store: [0x80013a4c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00c7d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008424]:fmul.s t6, t5, t4, dyn
	-[0x80008428]:csrrs a2, fcsr, zero
	-[0x8000842c]:sw t6, 120(fp)
Current Store : [0x80008430] : sw a2, 124(fp) -- Store: [0x80013a54]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00c7d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000845c]:fmul.s t6, t5, t4, dyn
	-[0x80008460]:csrrs a2, fcsr, zero
	-[0x80008464]:sw t6, 128(fp)
Current Store : [0x80008468] : sw a2, 132(fp) -- Store: [0x80013a5c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00c7d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008494]:fmul.s t6, t5, t4, dyn
	-[0x80008498]:csrrs a2, fcsr, zero
	-[0x8000849c]:sw t6, 136(fp)
Current Store : [0x800084a0] : sw a2, 140(fp) -- Store: [0x80013a64]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00c7d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800084cc]:fmul.s t6, t5, t4, dyn
	-[0x800084d0]:csrrs a2, fcsr, zero
	-[0x800084d4]:sw t6, 144(fp)
Current Store : [0x800084d8] : sw a2, 148(fp) -- Store: [0x80013a6c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00c7d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008504]:fmul.s t6, t5, t4, dyn
	-[0x80008508]:csrrs a2, fcsr, zero
	-[0x8000850c]:sw t6, 152(fp)
Current Store : [0x80008510] : sw a2, 156(fp) -- Store: [0x80013a74]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x28844c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000853c]:fmul.s t6, t5, t4, dyn
	-[0x80008540]:csrrs a2, fcsr, zero
	-[0x80008544]:sw t6, 160(fp)
Current Store : [0x80008548] : sw a2, 164(fp) -- Store: [0x80013a7c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x28844c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008574]:fmul.s t6, t5, t4, dyn
	-[0x80008578]:csrrs a2, fcsr, zero
	-[0x8000857c]:sw t6, 168(fp)
Current Store : [0x80008580] : sw a2, 172(fp) -- Store: [0x80013a84]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x28844c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800085ac]:fmul.s t6, t5, t4, dyn
	-[0x800085b0]:csrrs a2, fcsr, zero
	-[0x800085b4]:sw t6, 176(fp)
Current Store : [0x800085b8] : sw a2, 180(fp) -- Store: [0x80013a8c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x28844c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800085e4]:fmul.s t6, t5, t4, dyn
	-[0x800085e8]:csrrs a2, fcsr, zero
	-[0x800085ec]:sw t6, 184(fp)
Current Store : [0x800085f0] : sw a2, 188(fp) -- Store: [0x80013a94]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x28844c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000861c]:fmul.s t6, t5, t4, dyn
	-[0x80008620]:csrrs a2, fcsr, zero
	-[0x80008624]:sw t6, 192(fp)
Current Store : [0x80008628] : sw a2, 196(fp) -- Store: [0x80013a9c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x2c23d1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008654]:fmul.s t6, t5, t4, dyn
	-[0x80008658]:csrrs a2, fcsr, zero
	-[0x8000865c]:sw t6, 200(fp)
Current Store : [0x80008660] : sw a2, 204(fp) -- Store: [0x80013aa4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x2c23d1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000868c]:fmul.s t6, t5, t4, dyn
	-[0x80008690]:csrrs a2, fcsr, zero
	-[0x80008694]:sw t6, 208(fp)
Current Store : [0x80008698] : sw a2, 212(fp) -- Store: [0x80013aac]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x2c23d1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800086c4]:fmul.s t6, t5, t4, dyn
	-[0x800086c8]:csrrs a2, fcsr, zero
	-[0x800086cc]:sw t6, 216(fp)
Current Store : [0x800086d0] : sw a2, 220(fp) -- Store: [0x80013ab4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x2c23d1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800086fc]:fmul.s t6, t5, t4, dyn
	-[0x80008700]:csrrs a2, fcsr, zero
	-[0x80008704]:sw t6, 224(fp)
Current Store : [0x80008708] : sw a2, 228(fp) -- Store: [0x80013abc]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x2c23d1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008734]:fmul.s t6, t5, t4, dyn
	-[0x80008738]:csrrs a2, fcsr, zero
	-[0x8000873c]:sw t6, 232(fp)
Current Store : [0x80008740] : sw a2, 236(fp) -- Store: [0x80013ac4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x366362 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000876c]:fmul.s t6, t5, t4, dyn
	-[0x80008770]:csrrs a2, fcsr, zero
	-[0x80008774]:sw t6, 240(fp)
Current Store : [0x80008778] : sw a2, 244(fp) -- Store: [0x80013acc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x366362 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800087a4]:fmul.s t6, t5, t4, dyn
	-[0x800087a8]:csrrs a2, fcsr, zero
	-[0x800087ac]:sw t6, 248(fp)
Current Store : [0x800087b0] : sw a2, 252(fp) -- Store: [0x80013ad4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x366362 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800087dc]:fmul.s t6, t5, t4, dyn
	-[0x800087e0]:csrrs a2, fcsr, zero
	-[0x800087e4]:sw t6, 256(fp)
Current Store : [0x800087e8] : sw a2, 260(fp) -- Store: [0x80013adc]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x366362 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008814]:fmul.s t6, t5, t4, dyn
	-[0x80008818]:csrrs a2, fcsr, zero
	-[0x8000881c]:sw t6, 264(fp)
Current Store : [0x80008820] : sw a2, 268(fp) -- Store: [0x80013ae4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x366362 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000884c]:fmul.s t6, t5, t4, dyn
	-[0x80008850]:csrrs a2, fcsr, zero
	-[0x80008854]:sw t6, 272(fp)
Current Store : [0x80008858] : sw a2, 276(fp) -- Store: [0x80013aec]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x13b178 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008884]:fmul.s t6, t5, t4, dyn
	-[0x80008888]:csrrs a2, fcsr, zero
	-[0x8000888c]:sw t6, 280(fp)
Current Store : [0x80008890] : sw a2, 284(fp) -- Store: [0x80013af4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x13b178 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800088bc]:fmul.s t6, t5, t4, dyn
	-[0x800088c0]:csrrs a2, fcsr, zero
	-[0x800088c4]:sw t6, 288(fp)
Current Store : [0x800088c8] : sw a2, 292(fp) -- Store: [0x80013afc]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x13b178 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800088f4]:fmul.s t6, t5, t4, dyn
	-[0x800088f8]:csrrs a2, fcsr, zero
	-[0x800088fc]:sw t6, 296(fp)
Current Store : [0x80008900] : sw a2, 300(fp) -- Store: [0x80013b04]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x13b178 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000892c]:fmul.s t6, t5, t4, dyn
	-[0x80008930]:csrrs a2, fcsr, zero
	-[0x80008934]:sw t6, 304(fp)
Current Store : [0x80008938] : sw a2, 308(fp) -- Store: [0x80013b0c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x13b178 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008964]:fmul.s t6, t5, t4, dyn
	-[0x80008968]:csrrs a2, fcsr, zero
	-[0x8000896c]:sw t6, 312(fp)
Current Store : [0x80008970] : sw a2, 316(fp) -- Store: [0x80013b14]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x09599c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000899c]:fmul.s t6, t5, t4, dyn
	-[0x800089a0]:csrrs a2, fcsr, zero
	-[0x800089a4]:sw t6, 320(fp)
Current Store : [0x800089a8] : sw a2, 324(fp) -- Store: [0x80013b1c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x09599c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800089d4]:fmul.s t6, t5, t4, dyn
	-[0x800089d8]:csrrs a2, fcsr, zero
	-[0x800089dc]:sw t6, 328(fp)
Current Store : [0x800089e0] : sw a2, 332(fp) -- Store: [0x80013b24]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x09599c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008a0c]:fmul.s t6, t5, t4, dyn
	-[0x80008a10]:csrrs a2, fcsr, zero
	-[0x80008a14]:sw t6, 336(fp)
Current Store : [0x80008a18] : sw a2, 340(fp) -- Store: [0x80013b2c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x09599c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008a44]:fmul.s t6, t5, t4, dyn
	-[0x80008a48]:csrrs a2, fcsr, zero
	-[0x80008a4c]:sw t6, 344(fp)
Current Store : [0x80008a50] : sw a2, 348(fp) -- Store: [0x80013b34]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x09599c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008a7c]:fmul.s t6, t5, t4, dyn
	-[0x80008a80]:csrrs a2, fcsr, zero
	-[0x80008a84]:sw t6, 352(fp)
Current Store : [0x80008a88] : sw a2, 356(fp) -- Store: [0x80013b3c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2bd1f5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008ab4]:fmul.s t6, t5, t4, dyn
	-[0x80008ab8]:csrrs a2, fcsr, zero
	-[0x80008abc]:sw t6, 360(fp)
Current Store : [0x80008ac0] : sw a2, 364(fp) -- Store: [0x80013b44]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2bd1f5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008aec]:fmul.s t6, t5, t4, dyn
	-[0x80008af0]:csrrs a2, fcsr, zero
	-[0x80008af4]:sw t6, 368(fp)
Current Store : [0x80008af8] : sw a2, 372(fp) -- Store: [0x80013b4c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2bd1f5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008b24]:fmul.s t6, t5, t4, dyn
	-[0x80008b28]:csrrs a2, fcsr, zero
	-[0x80008b2c]:sw t6, 376(fp)
Current Store : [0x80008b30] : sw a2, 380(fp) -- Store: [0x80013b54]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2bd1f5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008b5c]:fmul.s t6, t5, t4, dyn
	-[0x80008b60]:csrrs a2, fcsr, zero
	-[0x80008b64]:sw t6, 384(fp)
Current Store : [0x80008b68] : sw a2, 388(fp) -- Store: [0x80013b5c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2bd1f5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008b94]:fmul.s t6, t5, t4, dyn
	-[0x80008b98]:csrrs a2, fcsr, zero
	-[0x80008b9c]:sw t6, 392(fp)
Current Store : [0x80008ba0] : sw a2, 396(fp) -- Store: [0x80013b64]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x070538 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008bcc]:fmul.s t6, t5, t4, dyn
	-[0x80008bd0]:csrrs a2, fcsr, zero
	-[0x80008bd4]:sw t6, 400(fp)
Current Store : [0x80008bd8] : sw a2, 404(fp) -- Store: [0x80013b6c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x070538 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008c04]:fmul.s t6, t5, t4, dyn
	-[0x80008c08]:csrrs a2, fcsr, zero
	-[0x80008c0c]:sw t6, 408(fp)
Current Store : [0x80008c10] : sw a2, 412(fp) -- Store: [0x80013b74]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x070538 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008c3c]:fmul.s t6, t5, t4, dyn
	-[0x80008c40]:csrrs a2, fcsr, zero
	-[0x80008c44]:sw t6, 416(fp)
Current Store : [0x80008c48] : sw a2, 420(fp) -- Store: [0x80013b7c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x070538 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008c74]:fmul.s t6, t5, t4, dyn
	-[0x80008c78]:csrrs a2, fcsr, zero
	-[0x80008c7c]:sw t6, 424(fp)
Current Store : [0x80008c80] : sw a2, 428(fp) -- Store: [0x80013b84]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x070538 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008cac]:fmul.s t6, t5, t4, dyn
	-[0x80008cb0]:csrrs a2, fcsr, zero
	-[0x80008cb4]:sw t6, 432(fp)
Current Store : [0x80008cb8] : sw a2, 436(fp) -- Store: [0x80013b8c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3f10c6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008ce4]:fmul.s t6, t5, t4, dyn
	-[0x80008ce8]:csrrs a2, fcsr, zero
	-[0x80008cec]:sw t6, 440(fp)
Current Store : [0x80008cf0] : sw a2, 444(fp) -- Store: [0x80013b94]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3f10c6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008d1c]:fmul.s t6, t5, t4, dyn
	-[0x80008d20]:csrrs a2, fcsr, zero
	-[0x80008d24]:sw t6, 448(fp)
Current Store : [0x80008d28] : sw a2, 452(fp) -- Store: [0x80013b9c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3f10c6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008d54]:fmul.s t6, t5, t4, dyn
	-[0x80008d58]:csrrs a2, fcsr, zero
	-[0x80008d5c]:sw t6, 456(fp)
Current Store : [0x80008d60] : sw a2, 460(fp) -- Store: [0x80013ba4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3f10c6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008d8c]:fmul.s t6, t5, t4, dyn
	-[0x80008d90]:csrrs a2, fcsr, zero
	-[0x80008d94]:sw t6, 464(fp)
Current Store : [0x80008d98] : sw a2, 468(fp) -- Store: [0x80013bac]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3f10c6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008dc4]:fmul.s t6, t5, t4, dyn
	-[0x80008dc8]:csrrs a2, fcsr, zero
	-[0x80008dcc]:sw t6, 472(fp)
Current Store : [0x80008dd0] : sw a2, 476(fp) -- Store: [0x80013bb4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x5c337b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008dfc]:fmul.s t6, t5, t4, dyn
	-[0x80008e00]:csrrs a2, fcsr, zero
	-[0x80008e04]:sw t6, 480(fp)
Current Store : [0x80008e08] : sw a2, 484(fp) -- Store: [0x80013bbc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x5c337b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008e34]:fmul.s t6, t5, t4, dyn
	-[0x80008e38]:csrrs a2, fcsr, zero
	-[0x80008e3c]:sw t6, 488(fp)
Current Store : [0x80008e40] : sw a2, 492(fp) -- Store: [0x80013bc4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x5c337b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008e6c]:fmul.s t6, t5, t4, dyn
	-[0x80008e70]:csrrs a2, fcsr, zero
	-[0x80008e74]:sw t6, 496(fp)
Current Store : [0x80008e78] : sw a2, 500(fp) -- Store: [0x80013bcc]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x5c337b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008ea4]:fmul.s t6, t5, t4, dyn
	-[0x80008ea8]:csrrs a2, fcsr, zero
	-[0x80008eac]:sw t6, 504(fp)
Current Store : [0x80008eb0] : sw a2, 508(fp) -- Store: [0x80013bd4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x5c337b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008edc]:fmul.s t6, t5, t4, dyn
	-[0x80008ee0]:csrrs a2, fcsr, zero
	-[0x80008ee4]:sw t6, 512(fp)
Current Store : [0x80008ee8] : sw a2, 516(fp) -- Store: [0x80013bdc]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x296a13 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008f14]:fmul.s t6, t5, t4, dyn
	-[0x80008f18]:csrrs a2, fcsr, zero
	-[0x80008f1c]:sw t6, 520(fp)
Current Store : [0x80008f20] : sw a2, 524(fp) -- Store: [0x80013be4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x296a13 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008f4c]:fmul.s t6, t5, t4, dyn
	-[0x80008f50]:csrrs a2, fcsr, zero
	-[0x80008f54]:sw t6, 528(fp)
Current Store : [0x80008f58] : sw a2, 532(fp) -- Store: [0x80013bec]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x296a13 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008f84]:fmul.s t6, t5, t4, dyn
	-[0x80008f88]:csrrs a2, fcsr, zero
	-[0x80008f8c]:sw t6, 536(fp)
Current Store : [0x80008f90] : sw a2, 540(fp) -- Store: [0x80013bf4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x296a13 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008fbc]:fmul.s t6, t5, t4, dyn
	-[0x80008fc0]:csrrs a2, fcsr, zero
	-[0x80008fc4]:sw t6, 544(fp)
Current Store : [0x80008fc8] : sw a2, 548(fp) -- Store: [0x80013bfc]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x296a13 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008ff4]:fmul.s t6, t5, t4, dyn
	-[0x80008ff8]:csrrs a2, fcsr, zero
	-[0x80008ffc]:sw t6, 552(fp)
Current Store : [0x80009000] : sw a2, 556(fp) -- Store: [0x80013c04]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x648b04 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000902c]:fmul.s t6, t5, t4, dyn
	-[0x80009030]:csrrs a2, fcsr, zero
	-[0x80009034]:sw t6, 560(fp)
Current Store : [0x80009038] : sw a2, 564(fp) -- Store: [0x80013c0c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x648b04 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009064]:fmul.s t6, t5, t4, dyn
	-[0x80009068]:csrrs a2, fcsr, zero
	-[0x8000906c]:sw t6, 568(fp)
Current Store : [0x80009070] : sw a2, 572(fp) -- Store: [0x80013c14]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x648b04 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000909c]:fmul.s t6, t5, t4, dyn
	-[0x800090a0]:csrrs a2, fcsr, zero
	-[0x800090a4]:sw t6, 576(fp)
Current Store : [0x800090a8] : sw a2, 580(fp) -- Store: [0x80013c1c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x648b04 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800090d4]:fmul.s t6, t5, t4, dyn
	-[0x800090d8]:csrrs a2, fcsr, zero
	-[0x800090dc]:sw t6, 584(fp)
Current Store : [0x800090e0] : sw a2, 588(fp) -- Store: [0x80013c24]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x648b04 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000910c]:fmul.s t6, t5, t4, dyn
	-[0x80009110]:csrrs a2, fcsr, zero
	-[0x80009114]:sw t6, 592(fp)
Current Store : [0x80009118] : sw a2, 596(fp) -- Store: [0x80013c2c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x0a90e5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009144]:fmul.s t6, t5, t4, dyn
	-[0x80009148]:csrrs a2, fcsr, zero
	-[0x8000914c]:sw t6, 600(fp)
Current Store : [0x80009150] : sw a2, 604(fp) -- Store: [0x80013c34]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x0a90e5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000917c]:fmul.s t6, t5, t4, dyn
	-[0x80009180]:csrrs a2, fcsr, zero
	-[0x80009184]:sw t6, 608(fp)
Current Store : [0x80009188] : sw a2, 612(fp) -- Store: [0x80013c3c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x0a90e5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800091b4]:fmul.s t6, t5, t4, dyn
	-[0x800091b8]:csrrs a2, fcsr, zero
	-[0x800091bc]:sw t6, 616(fp)
Current Store : [0x800091c0] : sw a2, 620(fp) -- Store: [0x80013c44]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x0a90e5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800091ec]:fmul.s t6, t5, t4, dyn
	-[0x800091f0]:csrrs a2, fcsr, zero
	-[0x800091f4]:sw t6, 624(fp)
Current Store : [0x800091f8] : sw a2, 628(fp) -- Store: [0x80013c4c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x0a90e5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009224]:fmul.s t6, t5, t4, dyn
	-[0x80009228]:csrrs a2, fcsr, zero
	-[0x8000922c]:sw t6, 632(fp)
Current Store : [0x80009230] : sw a2, 636(fp) -- Store: [0x80013c54]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x394394 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000925c]:fmul.s t6, t5, t4, dyn
	-[0x80009260]:csrrs a2, fcsr, zero
	-[0x80009264]:sw t6, 640(fp)
Current Store : [0x80009268] : sw a2, 644(fp) -- Store: [0x80013c5c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x394394 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009294]:fmul.s t6, t5, t4, dyn
	-[0x80009298]:csrrs a2, fcsr, zero
	-[0x8000929c]:sw t6, 648(fp)
Current Store : [0x800092a0] : sw a2, 652(fp) -- Store: [0x80013c64]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x394394 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800092cc]:fmul.s t6, t5, t4, dyn
	-[0x800092d0]:csrrs a2, fcsr, zero
	-[0x800092d4]:sw t6, 656(fp)
Current Store : [0x800092d8] : sw a2, 660(fp) -- Store: [0x80013c6c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x394394 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009304]:fmul.s t6, t5, t4, dyn
	-[0x80009308]:csrrs a2, fcsr, zero
	-[0x8000930c]:sw t6, 664(fp)
Current Store : [0x80009310] : sw a2, 668(fp) -- Store: [0x80013c74]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x394394 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000933c]:fmul.s t6, t5, t4, dyn
	-[0x80009340]:csrrs a2, fcsr, zero
	-[0x80009344]:sw t6, 672(fp)
Current Store : [0x80009348] : sw a2, 676(fp) -- Store: [0x80013c7c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x086888 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009374]:fmul.s t6, t5, t4, dyn
	-[0x80009378]:csrrs a2, fcsr, zero
	-[0x8000937c]:sw t6, 680(fp)
Current Store : [0x80009380] : sw a2, 684(fp) -- Store: [0x80013c84]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x086888 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800093ac]:fmul.s t6, t5, t4, dyn
	-[0x800093b0]:csrrs a2, fcsr, zero
	-[0x800093b4]:sw t6, 688(fp)
Current Store : [0x800093b8] : sw a2, 692(fp) -- Store: [0x80013c8c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x086888 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800093e4]:fmul.s t6, t5, t4, dyn
	-[0x800093e8]:csrrs a2, fcsr, zero
	-[0x800093ec]:sw t6, 696(fp)
Current Store : [0x800093f0] : sw a2, 700(fp) -- Store: [0x80013c94]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x086888 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000941c]:fmul.s t6, t5, t4, dyn
	-[0x80009420]:csrrs a2, fcsr, zero
	-[0x80009424]:sw t6, 704(fp)
Current Store : [0x80009428] : sw a2, 708(fp) -- Store: [0x80013c9c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x086888 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009454]:fmul.s t6, t5, t4, dyn
	-[0x80009458]:csrrs a2, fcsr, zero
	-[0x8000945c]:sw t6, 712(fp)
Current Store : [0x80009460] : sw a2, 716(fp) -- Store: [0x80013ca4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x076f73 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000948c]:fmul.s t6, t5, t4, dyn
	-[0x80009490]:csrrs a2, fcsr, zero
	-[0x80009494]:sw t6, 720(fp)
Current Store : [0x80009498] : sw a2, 724(fp) -- Store: [0x80013cac]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x076f73 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800094c4]:fmul.s t6, t5, t4, dyn
	-[0x800094c8]:csrrs a2, fcsr, zero
	-[0x800094cc]:sw t6, 728(fp)
Current Store : [0x800094d0] : sw a2, 732(fp) -- Store: [0x80013cb4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x076f73 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800094fc]:fmul.s t6, t5, t4, dyn
	-[0x80009500]:csrrs a2, fcsr, zero
	-[0x80009504]:sw t6, 736(fp)
Current Store : [0x80009508] : sw a2, 740(fp) -- Store: [0x80013cbc]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x076f73 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009534]:fmul.s t6, t5, t4, dyn
	-[0x80009538]:csrrs a2, fcsr, zero
	-[0x8000953c]:sw t6, 744(fp)
Current Store : [0x80009540] : sw a2, 748(fp) -- Store: [0x80013cc4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x076f73 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000956c]:fmul.s t6, t5, t4, dyn
	-[0x80009570]:csrrs a2, fcsr, zero
	-[0x80009574]:sw t6, 752(fp)
Current Store : [0x80009578] : sw a2, 756(fp) -- Store: [0x80013ccc]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x38b31c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800095a4]:fmul.s t6, t5, t4, dyn
	-[0x800095a8]:csrrs a2, fcsr, zero
	-[0x800095ac]:sw t6, 760(fp)
Current Store : [0x800095b0] : sw a2, 764(fp) -- Store: [0x80013cd4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x38b31c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800095dc]:fmul.s t6, t5, t4, dyn
	-[0x800095e0]:csrrs a2, fcsr, zero
	-[0x800095e4]:sw t6, 768(fp)
Current Store : [0x800095e8] : sw a2, 772(fp) -- Store: [0x80013cdc]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x38b31c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009614]:fmul.s t6, t5, t4, dyn
	-[0x80009618]:csrrs a2, fcsr, zero
	-[0x8000961c]:sw t6, 776(fp)
Current Store : [0x80009620] : sw a2, 780(fp) -- Store: [0x80013ce4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x38b31c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000964c]:fmul.s t6, t5, t4, dyn
	-[0x80009650]:csrrs a2, fcsr, zero
	-[0x80009654]:sw t6, 784(fp)
Current Store : [0x80009658] : sw a2, 788(fp) -- Store: [0x80013cec]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x38b31c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009684]:fmul.s t6, t5, t4, dyn
	-[0x80009688]:csrrs a2, fcsr, zero
	-[0x8000968c]:sw t6, 792(fp)
Current Store : [0x80009690] : sw a2, 796(fp) -- Store: [0x80013cf4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0f285b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800096bc]:fmul.s t6, t5, t4, dyn
	-[0x800096c0]:csrrs a2, fcsr, zero
	-[0x800096c4]:sw t6, 800(fp)
Current Store : [0x800096c8] : sw a2, 804(fp) -- Store: [0x80013cfc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0f285b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800096f4]:fmul.s t6, t5, t4, dyn
	-[0x800096f8]:csrrs a2, fcsr, zero
	-[0x800096fc]:sw t6, 808(fp)
Current Store : [0x80009700] : sw a2, 812(fp) -- Store: [0x80013d04]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0f285b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000972c]:fmul.s t6, t5, t4, dyn
	-[0x80009730]:csrrs a2, fcsr, zero
	-[0x80009734]:sw t6, 816(fp)
Current Store : [0x80009738] : sw a2, 820(fp) -- Store: [0x80013d0c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0f285b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009764]:fmul.s t6, t5, t4, dyn
	-[0x80009768]:csrrs a2, fcsr, zero
	-[0x8000976c]:sw t6, 824(fp)
Current Store : [0x80009770] : sw a2, 828(fp) -- Store: [0x80013d14]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0f285b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000979c]:fmul.s t6, t5, t4, dyn
	-[0x800097a0]:csrrs a2, fcsr, zero
	-[0x800097a4]:sw t6, 832(fp)
Current Store : [0x800097a8] : sw a2, 836(fp) -- Store: [0x80013d1c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x123a99 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800097d4]:fmul.s t6, t5, t4, dyn
	-[0x800097d8]:csrrs a2, fcsr, zero
	-[0x800097dc]:sw t6, 840(fp)
Current Store : [0x800097e0] : sw a2, 844(fp) -- Store: [0x80013d24]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x123a99 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000980c]:fmul.s t6, t5, t4, dyn
	-[0x80009810]:csrrs a2, fcsr, zero
	-[0x80009814]:sw t6, 848(fp)
Current Store : [0x80009818] : sw a2, 852(fp) -- Store: [0x80013d2c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x123a99 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009844]:fmul.s t6, t5, t4, dyn
	-[0x80009848]:csrrs a2, fcsr, zero
	-[0x8000984c]:sw t6, 856(fp)
Current Store : [0x80009850] : sw a2, 860(fp) -- Store: [0x80013d34]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x123a99 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000987c]:fmul.s t6, t5, t4, dyn
	-[0x80009880]:csrrs a2, fcsr, zero
	-[0x80009884]:sw t6, 864(fp)
Current Store : [0x80009888] : sw a2, 868(fp) -- Store: [0x80013d3c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x123a99 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800098b4]:fmul.s t6, t5, t4, dyn
	-[0x800098b8]:csrrs a2, fcsr, zero
	-[0x800098bc]:sw t6, 872(fp)
Current Store : [0x800098c0] : sw a2, 876(fp) -- Store: [0x80013d44]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2ccc93 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800098ec]:fmul.s t6, t5, t4, dyn
	-[0x800098f0]:csrrs a2, fcsr, zero
	-[0x800098f4]:sw t6, 880(fp)
Current Store : [0x800098f8] : sw a2, 884(fp) -- Store: [0x80013d4c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2ccc93 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009924]:fmul.s t6, t5, t4, dyn
	-[0x80009928]:csrrs a2, fcsr, zero
	-[0x8000992c]:sw t6, 888(fp)
Current Store : [0x80009930] : sw a2, 892(fp) -- Store: [0x80013d54]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2ccc93 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000995c]:fmul.s t6, t5, t4, dyn
	-[0x80009960]:csrrs a2, fcsr, zero
	-[0x80009964]:sw t6, 896(fp)
Current Store : [0x80009968] : sw a2, 900(fp) -- Store: [0x80013d5c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2ccc93 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009994]:fmul.s t6, t5, t4, dyn
	-[0x80009998]:csrrs a2, fcsr, zero
	-[0x8000999c]:sw t6, 904(fp)
Current Store : [0x800099a0] : sw a2, 908(fp) -- Store: [0x80013d64]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2ccc93 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800099cc]:fmul.s t6, t5, t4, dyn
	-[0x800099d0]:csrrs a2, fcsr, zero
	-[0x800099d4]:sw t6, 912(fp)
Current Store : [0x800099d8] : sw a2, 916(fp) -- Store: [0x80013d6c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x1aa55e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009a04]:fmul.s t6, t5, t4, dyn
	-[0x80009a08]:csrrs a2, fcsr, zero
	-[0x80009a0c]:sw t6, 920(fp)
Current Store : [0x80009a10] : sw a2, 924(fp) -- Store: [0x80013d74]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x1aa55e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009a3c]:fmul.s t6, t5, t4, dyn
	-[0x80009a40]:csrrs a2, fcsr, zero
	-[0x80009a44]:sw t6, 928(fp)
Current Store : [0x80009a48] : sw a2, 932(fp) -- Store: [0x80013d7c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x1aa55e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009a74]:fmul.s t6, t5, t4, dyn
	-[0x80009a78]:csrrs a2, fcsr, zero
	-[0x80009a7c]:sw t6, 936(fp)
Current Store : [0x80009a80] : sw a2, 940(fp) -- Store: [0x80013d84]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x1aa55e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009aac]:fmul.s t6, t5, t4, dyn
	-[0x80009ab0]:csrrs a2, fcsr, zero
	-[0x80009ab4]:sw t6, 944(fp)
Current Store : [0x80009ab8] : sw a2, 948(fp) -- Store: [0x80013d8c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x1aa55e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009ae4]:fmul.s t6, t5, t4, dyn
	-[0x80009ae8]:csrrs a2, fcsr, zero
	-[0x80009aec]:sw t6, 952(fp)
Current Store : [0x80009af0] : sw a2, 956(fp) -- Store: [0x80013d94]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3ba101 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009b1c]:fmul.s t6, t5, t4, dyn
	-[0x80009b20]:csrrs a2, fcsr, zero
	-[0x80009b24]:sw t6, 960(fp)
Current Store : [0x80009b28] : sw a2, 964(fp) -- Store: [0x80013d9c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3ba101 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009b54]:fmul.s t6, t5, t4, dyn
	-[0x80009b58]:csrrs a2, fcsr, zero
	-[0x80009b5c]:sw t6, 968(fp)
Current Store : [0x80009b60] : sw a2, 972(fp) -- Store: [0x80013da4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3ba101 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009b8c]:fmul.s t6, t5, t4, dyn
	-[0x80009b90]:csrrs a2, fcsr, zero
	-[0x80009b94]:sw t6, 976(fp)
Current Store : [0x80009b98] : sw a2, 980(fp) -- Store: [0x80013dac]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3ba101 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009bc4]:fmul.s t6, t5, t4, dyn
	-[0x80009bc8]:csrrs a2, fcsr, zero
	-[0x80009bcc]:sw t6, 984(fp)
Current Store : [0x80009bd0] : sw a2, 988(fp) -- Store: [0x80013db4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3ba101 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009bfc]:fmul.s t6, t5, t4, dyn
	-[0x80009c00]:csrrs a2, fcsr, zero
	-[0x80009c04]:sw t6, 992(fp)
Current Store : [0x80009c08] : sw a2, 996(fp) -- Store: [0x80013dbc]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x0e9cab and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009c3c]:fmul.s t6, t5, t4, dyn
	-[0x80009c40]:csrrs a2, fcsr, zero
	-[0x80009c44]:sw t6, 1000(fp)
Current Store : [0x80009c48] : sw a2, 1004(fp) -- Store: [0x80013dc4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x0e9cab and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009c7c]:fmul.s t6, t5, t4, dyn
	-[0x80009c80]:csrrs a2, fcsr, zero
	-[0x80009c84]:sw t6, 1008(fp)
Current Store : [0x80009c88] : sw a2, 1012(fp) -- Store: [0x80013dcc]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x0e9cab and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009cbc]:fmul.s t6, t5, t4, dyn
	-[0x80009cc0]:csrrs a2, fcsr, zero
	-[0x80009cc4]:sw t6, 1016(fp)
Current Store : [0x80009cc8] : sw a2, 1020(fp) -- Store: [0x80013dd4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x0e9cab and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009d04]:fmul.s t6, t5, t4, dyn
	-[0x80009d08]:csrrs a2, fcsr, zero
	-[0x80009d0c]:sw t6, 0(fp)
Current Store : [0x80009d10] : sw a2, 4(fp) -- Store: [0x80013ddc]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x0e9cab and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009d44]:fmul.s t6, t5, t4, dyn
	-[0x80009d48]:csrrs a2, fcsr, zero
	-[0x80009d4c]:sw t6, 8(fp)
Current Store : [0x80009d50] : sw a2, 12(fp) -- Store: [0x80013de4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x089f67 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009d84]:fmul.s t6, t5, t4, dyn
	-[0x80009d88]:csrrs a2, fcsr, zero
	-[0x80009d8c]:sw t6, 16(fp)
Current Store : [0x80009d90] : sw a2, 20(fp) -- Store: [0x80013dec]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x089f67 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009dc4]:fmul.s t6, t5, t4, dyn
	-[0x80009dc8]:csrrs a2, fcsr, zero
	-[0x80009dcc]:sw t6, 24(fp)
Current Store : [0x80009dd0] : sw a2, 28(fp) -- Store: [0x80013df4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x089f67 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009e04]:fmul.s t6, t5, t4, dyn
	-[0x80009e08]:csrrs a2, fcsr, zero
	-[0x80009e0c]:sw t6, 32(fp)
Current Store : [0x80009e10] : sw a2, 36(fp) -- Store: [0x80013dfc]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x089f67 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009e44]:fmul.s t6, t5, t4, dyn
	-[0x80009e48]:csrrs a2, fcsr, zero
	-[0x80009e4c]:sw t6, 40(fp)
Current Store : [0x80009e50] : sw a2, 44(fp) -- Store: [0x80013e04]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x089f67 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009e84]:fmul.s t6, t5, t4, dyn
	-[0x80009e88]:csrrs a2, fcsr, zero
	-[0x80009e8c]:sw t6, 48(fp)
Current Store : [0x80009e90] : sw a2, 52(fp) -- Store: [0x80013e0c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x79a1f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009ec4]:fmul.s t6, t5, t4, dyn
	-[0x80009ec8]:csrrs a2, fcsr, zero
	-[0x80009ecc]:sw t6, 56(fp)
Current Store : [0x80009ed0] : sw a2, 60(fp) -- Store: [0x80013e14]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x79a1f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009f04]:fmul.s t6, t5, t4, dyn
	-[0x80009f08]:csrrs a2, fcsr, zero
	-[0x80009f0c]:sw t6, 64(fp)
Current Store : [0x80009f10] : sw a2, 68(fp) -- Store: [0x80013e1c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x79a1f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009f44]:fmul.s t6, t5, t4, dyn
	-[0x80009f48]:csrrs a2, fcsr, zero
	-[0x80009f4c]:sw t6, 72(fp)
Current Store : [0x80009f50] : sw a2, 76(fp) -- Store: [0x80013e24]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x79a1f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009f84]:fmul.s t6, t5, t4, dyn
	-[0x80009f88]:csrrs a2, fcsr, zero
	-[0x80009f8c]:sw t6, 80(fp)
Current Store : [0x80009f90] : sw a2, 84(fp) -- Store: [0x80013e2c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x79a1f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009fc4]:fmul.s t6, t5, t4, dyn
	-[0x80009fc8]:csrrs a2, fcsr, zero
	-[0x80009fcc]:sw t6, 88(fp)
Current Store : [0x80009fd0] : sw a2, 92(fp) -- Store: [0x80013e34]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x529595 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a004]:fmul.s t6, t5, t4, dyn
	-[0x8000a008]:csrrs a2, fcsr, zero
	-[0x8000a00c]:sw t6, 96(fp)
Current Store : [0x8000a010] : sw a2, 100(fp) -- Store: [0x80013e3c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x529595 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a044]:fmul.s t6, t5, t4, dyn
	-[0x8000a048]:csrrs a2, fcsr, zero
	-[0x8000a04c]:sw t6, 104(fp)
Current Store : [0x8000a050] : sw a2, 108(fp) -- Store: [0x80013e44]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x529595 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a084]:fmul.s t6, t5, t4, dyn
	-[0x8000a088]:csrrs a2, fcsr, zero
	-[0x8000a08c]:sw t6, 112(fp)
Current Store : [0x8000a090] : sw a2, 116(fp) -- Store: [0x80013e4c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x529595 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a0c4]:fmul.s t6, t5, t4, dyn
	-[0x8000a0c8]:csrrs a2, fcsr, zero
	-[0x8000a0cc]:sw t6, 120(fp)
Current Store : [0x8000a0d0] : sw a2, 124(fp) -- Store: [0x80013e54]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x529595 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a104]:fmul.s t6, t5, t4, dyn
	-[0x8000a108]:csrrs a2, fcsr, zero
	-[0x8000a10c]:sw t6, 128(fp)
Current Store : [0x8000a110] : sw a2, 132(fp) -- Store: [0x80013e5c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0c0e98 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a144]:fmul.s t6, t5, t4, dyn
	-[0x8000a148]:csrrs a2, fcsr, zero
	-[0x8000a14c]:sw t6, 136(fp)
Current Store : [0x8000a150] : sw a2, 140(fp) -- Store: [0x80013e64]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0c0e98 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a184]:fmul.s t6, t5, t4, dyn
	-[0x8000a188]:csrrs a2, fcsr, zero
	-[0x8000a18c]:sw t6, 144(fp)
Current Store : [0x8000a190] : sw a2, 148(fp) -- Store: [0x80013e6c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0c0e98 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a1c4]:fmul.s t6, t5, t4, dyn
	-[0x8000a1c8]:csrrs a2, fcsr, zero
	-[0x8000a1cc]:sw t6, 152(fp)
Current Store : [0x8000a1d0] : sw a2, 156(fp) -- Store: [0x80013e74]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0c0e98 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a204]:fmul.s t6, t5, t4, dyn
	-[0x8000a208]:csrrs a2, fcsr, zero
	-[0x8000a20c]:sw t6, 160(fp)
Current Store : [0x8000a210] : sw a2, 164(fp) -- Store: [0x80013e7c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0c0e98 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a244]:fmul.s t6, t5, t4, dyn
	-[0x8000a248]:csrrs a2, fcsr, zero
	-[0x8000a24c]:sw t6, 168(fp)
Current Store : [0x8000a250] : sw a2, 172(fp) -- Store: [0x80013e84]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2dd26f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a284]:fmul.s t6, t5, t4, dyn
	-[0x8000a288]:csrrs a2, fcsr, zero
	-[0x8000a28c]:sw t6, 176(fp)
Current Store : [0x8000a290] : sw a2, 180(fp) -- Store: [0x80013e8c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2dd26f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a2c4]:fmul.s t6, t5, t4, dyn
	-[0x8000a2c8]:csrrs a2, fcsr, zero
	-[0x8000a2cc]:sw t6, 184(fp)
Current Store : [0x8000a2d0] : sw a2, 188(fp) -- Store: [0x80013e94]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2dd26f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a304]:fmul.s t6, t5, t4, dyn
	-[0x8000a308]:csrrs a2, fcsr, zero
	-[0x8000a30c]:sw t6, 192(fp)
Current Store : [0x8000a310] : sw a2, 196(fp) -- Store: [0x80013e9c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2dd26f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a344]:fmul.s t6, t5, t4, dyn
	-[0x8000a348]:csrrs a2, fcsr, zero
	-[0x8000a34c]:sw t6, 200(fp)
Current Store : [0x8000a350] : sw a2, 204(fp) -- Store: [0x80013ea4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2dd26f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a384]:fmul.s t6, t5, t4, dyn
	-[0x8000a388]:csrrs a2, fcsr, zero
	-[0x8000a38c]:sw t6, 208(fp)
Current Store : [0x8000a390] : sw a2, 212(fp) -- Store: [0x80013eac]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x1a4d72 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a3c4]:fmul.s t6, t5, t4, dyn
	-[0x8000a3c8]:csrrs a2, fcsr, zero
	-[0x8000a3cc]:sw t6, 216(fp)
Current Store : [0x8000a3d0] : sw a2, 220(fp) -- Store: [0x80013eb4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x1a4d72 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a404]:fmul.s t6, t5, t4, dyn
	-[0x8000a408]:csrrs a2, fcsr, zero
	-[0x8000a40c]:sw t6, 224(fp)
Current Store : [0x8000a410] : sw a2, 228(fp) -- Store: [0x80013ebc]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x1a4d72 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a444]:fmul.s t6, t5, t4, dyn
	-[0x8000a448]:csrrs a2, fcsr, zero
	-[0x8000a44c]:sw t6, 232(fp)
Current Store : [0x8000a450] : sw a2, 236(fp) -- Store: [0x80013ec4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x1a4d72 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a484]:fmul.s t6, t5, t4, dyn
	-[0x8000a488]:csrrs a2, fcsr, zero
	-[0x8000a48c]:sw t6, 240(fp)
Current Store : [0x8000a490] : sw a2, 244(fp) -- Store: [0x80013ecc]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x1a4d72 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a4c4]:fmul.s t6, t5, t4, dyn
	-[0x8000a4c8]:csrrs a2, fcsr, zero
	-[0x8000a4cc]:sw t6, 248(fp)
Current Store : [0x8000a4d0] : sw a2, 252(fp) -- Store: [0x80013ed4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x13439e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a504]:fmul.s t6, t5, t4, dyn
	-[0x8000a508]:csrrs a2, fcsr, zero
	-[0x8000a50c]:sw t6, 256(fp)
Current Store : [0x8000a510] : sw a2, 260(fp) -- Store: [0x80013edc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x13439e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a544]:fmul.s t6, t5, t4, dyn
	-[0x8000a548]:csrrs a2, fcsr, zero
	-[0x8000a54c]:sw t6, 264(fp)
Current Store : [0x8000a550] : sw a2, 268(fp) -- Store: [0x80013ee4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x13439e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a584]:fmul.s t6, t5, t4, dyn
	-[0x8000a588]:csrrs a2, fcsr, zero
	-[0x8000a58c]:sw t6, 272(fp)
Current Store : [0x8000a590] : sw a2, 276(fp) -- Store: [0x80013eec]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x13439e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a5c4]:fmul.s t6, t5, t4, dyn
	-[0x8000a5c8]:csrrs a2, fcsr, zero
	-[0x8000a5cc]:sw t6, 280(fp)
Current Store : [0x8000a5d0] : sw a2, 284(fp) -- Store: [0x80013ef4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x13439e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a604]:fmul.s t6, t5, t4, dyn
	-[0x8000a608]:csrrs a2, fcsr, zero
	-[0x8000a60c]:sw t6, 288(fp)
Current Store : [0x8000a610] : sw a2, 292(fp) -- Store: [0x80013efc]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0ee529 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a644]:fmul.s t6, t5, t4, dyn
	-[0x8000a648]:csrrs a2, fcsr, zero
	-[0x8000a64c]:sw t6, 296(fp)
Current Store : [0x8000a650] : sw a2, 300(fp) -- Store: [0x80013f04]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0ee529 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a684]:fmul.s t6, t5, t4, dyn
	-[0x8000a688]:csrrs a2, fcsr, zero
	-[0x8000a68c]:sw t6, 304(fp)
Current Store : [0x8000a690] : sw a2, 308(fp) -- Store: [0x80013f0c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0ee529 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a6c4]:fmul.s t6, t5, t4, dyn
	-[0x8000a6c8]:csrrs a2, fcsr, zero
	-[0x8000a6cc]:sw t6, 312(fp)
Current Store : [0x8000a6d0] : sw a2, 316(fp) -- Store: [0x80013f14]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0ee529 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a704]:fmul.s t6, t5, t4, dyn
	-[0x8000a708]:csrrs a2, fcsr, zero
	-[0x8000a70c]:sw t6, 320(fp)
Current Store : [0x8000a710] : sw a2, 324(fp) -- Store: [0x80013f1c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0ee529 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a744]:fmul.s t6, t5, t4, dyn
	-[0x8000a748]:csrrs a2, fcsr, zero
	-[0x8000a74c]:sw t6, 328(fp)
Current Store : [0x8000a750] : sw a2, 332(fp) -- Store: [0x80013f24]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x131155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a784]:fmul.s t6, t5, t4, dyn
	-[0x8000a788]:csrrs a2, fcsr, zero
	-[0x8000a78c]:sw t6, 336(fp)
Current Store : [0x8000a790] : sw a2, 340(fp) -- Store: [0x80013f2c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x131155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a7c4]:fmul.s t6, t5, t4, dyn
	-[0x8000a7c8]:csrrs a2, fcsr, zero
	-[0x8000a7cc]:sw t6, 344(fp)
Current Store : [0x8000a7d0] : sw a2, 348(fp) -- Store: [0x80013f34]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x131155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a804]:fmul.s t6, t5, t4, dyn
	-[0x8000a808]:csrrs a2, fcsr, zero
	-[0x8000a80c]:sw t6, 352(fp)
Current Store : [0x8000a810] : sw a2, 356(fp) -- Store: [0x80013f3c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x131155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a844]:fmul.s t6, t5, t4, dyn
	-[0x8000a848]:csrrs a2, fcsr, zero
	-[0x8000a84c]:sw t6, 360(fp)
Current Store : [0x8000a850] : sw a2, 364(fp) -- Store: [0x80013f44]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x131155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a884]:fmul.s t6, t5, t4, dyn
	-[0x8000a888]:csrrs a2, fcsr, zero
	-[0x8000a88c]:sw t6, 368(fp)
Current Store : [0x8000a890] : sw a2, 372(fp) -- Store: [0x80013f4c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x53c5ea and fs2 == 0 and fe2 == 0x2d and fm2 == 0x1abb4c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a8c4]:fmul.s t6, t5, t4, dyn
	-[0x8000a8c8]:csrrs a2, fcsr, zero
	-[0x8000a8cc]:sw t6, 376(fp)
Current Store : [0x8000a8d0] : sw a2, 380(fp) -- Store: [0x80013f54]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x53c5ea and fs2 == 0 and fe2 == 0x2d and fm2 == 0x1abb4c and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a904]:fmul.s t6, t5, t4, dyn
	-[0x8000a908]:csrrs a2, fcsr, zero
	-[0x8000a90c]:sw t6, 384(fp)
Current Store : [0x8000a910] : sw a2, 388(fp) -- Store: [0x80013f5c]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x53c5ea and fs2 == 0 and fe2 == 0x2d and fm2 == 0x1abb4c and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a944]:fmul.s t6, t5, t4, dyn
	-[0x8000a948]:csrrs a2, fcsr, zero
	-[0x8000a94c]:sw t6, 392(fp)
Current Store : [0x8000a950] : sw a2, 396(fp) -- Store: [0x80013f64]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x53c5ea and fs2 == 0 and fe2 == 0x2d and fm2 == 0x1abb4c and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a984]:fmul.s t6, t5, t4, dyn
	-[0x8000a988]:csrrs a2, fcsr, zero
	-[0x8000a98c]:sw t6, 400(fp)
Current Store : [0x8000a990] : sw a2, 404(fp) -- Store: [0x80013f6c]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x53c5ea and fs2 == 0 and fe2 == 0x2d and fm2 == 0x1abb4c and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000a9c4]:fmul.s t6, t5, t4, dyn
	-[0x8000a9c8]:csrrs a2, fcsr, zero
	-[0x8000a9cc]:sw t6, 408(fp)
Current Store : [0x8000a9d0] : sw a2, 412(fp) -- Store: [0x80013f74]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x26cf0e and fs2 == 0 and fe2 == 0x2b and fm2 == 0x4470c1 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000aa04]:fmul.s t6, t5, t4, dyn
	-[0x8000aa08]:csrrs a2, fcsr, zero
	-[0x8000aa0c]:sw t6, 416(fp)
Current Store : [0x8000aa10] : sw a2, 420(fp) -- Store: [0x80013f7c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x26cf0e and fs2 == 0 and fe2 == 0x2b and fm2 == 0x4470c1 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000aa44]:fmul.s t6, t5, t4, dyn
	-[0x8000aa48]:csrrs a2, fcsr, zero
	-[0x8000aa4c]:sw t6, 424(fp)
Current Store : [0x8000aa50] : sw a2, 428(fp) -- Store: [0x80013f84]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x26cf0e and fs2 == 0 and fe2 == 0x2b and fm2 == 0x4470c1 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000aa84]:fmul.s t6, t5, t4, dyn
	-[0x8000aa88]:csrrs a2, fcsr, zero
	-[0x8000aa8c]:sw t6, 432(fp)
Current Store : [0x8000aa90] : sw a2, 436(fp) -- Store: [0x80013f8c]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x26cf0e and fs2 == 0 and fe2 == 0x2b and fm2 == 0x4470c1 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000aac4]:fmul.s t6, t5, t4, dyn
	-[0x8000aac8]:csrrs a2, fcsr, zero
	-[0x8000aacc]:sw t6, 440(fp)
Current Store : [0x8000aad0] : sw a2, 444(fp) -- Store: [0x80013f94]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x26cf0e and fs2 == 0 and fe2 == 0x2b and fm2 == 0x4470c1 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ab04]:fmul.s t6, t5, t4, dyn
	-[0x8000ab08]:csrrs a2, fcsr, zero
	-[0x8000ab0c]:sw t6, 448(fp)
Current Store : [0x8000ab10] : sw a2, 452(fp) -- Store: [0x80013f9c]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x59615d and fs2 == 0 and fe2 == 0x2b and fm2 == 0x16bd8b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ab44]:fmul.s t6, t5, t4, dyn
	-[0x8000ab48]:csrrs a2, fcsr, zero
	-[0x8000ab4c]:sw t6, 456(fp)
Current Store : [0x8000ab50] : sw a2, 460(fp) -- Store: [0x80013fa4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x59615d and fs2 == 0 and fe2 == 0x2b and fm2 == 0x16bd8b and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ab84]:fmul.s t6, t5, t4, dyn
	-[0x8000ab88]:csrrs a2, fcsr, zero
	-[0x8000ab8c]:sw t6, 464(fp)
Current Store : [0x8000ab90] : sw a2, 468(fp) -- Store: [0x80013fac]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x59615d and fs2 == 0 and fe2 == 0x2b and fm2 == 0x16bd8b and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000abc4]:fmul.s t6, t5, t4, dyn
	-[0x8000abc8]:csrrs a2, fcsr, zero
	-[0x8000abcc]:sw t6, 472(fp)
Current Store : [0x8000abd0] : sw a2, 476(fp) -- Store: [0x80013fb4]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x59615d and fs2 == 0 and fe2 == 0x2b and fm2 == 0x16bd8b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ac04]:fmul.s t6, t5, t4, dyn
	-[0x8000ac08]:csrrs a2, fcsr, zero
	-[0x8000ac0c]:sw t6, 480(fp)
Current Store : [0x8000ac10] : sw a2, 484(fp) -- Store: [0x80013fbc]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x59615d and fs2 == 0 and fe2 == 0x2b and fm2 == 0x16bd8b and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ac44]:fmul.s t6, t5, t4, dyn
	-[0x8000ac48]:csrrs a2, fcsr, zero
	-[0x8000ac4c]:sw t6, 488(fp)
Current Store : [0x8000ac50] : sw a2, 492(fp) -- Store: [0x80013fc4]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1d7025 and fs2 == 0 and fe2 == 0x2c and fm2 == 0x5021f4 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ac84]:fmul.s t6, t5, t4, dyn
	-[0x8000ac88]:csrrs a2, fcsr, zero
	-[0x8000ac8c]:sw t6, 496(fp)
Current Store : [0x8000ac90] : sw a2, 500(fp) -- Store: [0x80013fcc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1d7025 and fs2 == 0 and fe2 == 0x2c and fm2 == 0x5021f4 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000acc4]:fmul.s t6, t5, t4, dyn
	-[0x8000acc8]:csrrs a2, fcsr, zero
	-[0x8000accc]:sw t6, 504(fp)
Current Store : [0x8000acd0] : sw a2, 508(fp) -- Store: [0x80013fd4]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1d7025 and fs2 == 0 and fe2 == 0x2c and fm2 == 0x5021f4 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ad04]:fmul.s t6, t5, t4, dyn
	-[0x8000ad08]:csrrs a2, fcsr, zero
	-[0x8000ad0c]:sw t6, 512(fp)
Current Store : [0x8000ad10] : sw a2, 516(fp) -- Store: [0x80013fdc]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1d7025 and fs2 == 0 and fe2 == 0x2c and fm2 == 0x5021f4 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ad44]:fmul.s t6, t5, t4, dyn
	-[0x8000ad48]:csrrs a2, fcsr, zero
	-[0x8000ad4c]:sw t6, 520(fp)
Current Store : [0x8000ad50] : sw a2, 524(fp) -- Store: [0x80013fe4]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1d7025 and fs2 == 0 and fe2 == 0x2c and fm2 == 0x5021f4 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ad84]:fmul.s t6, t5, t4, dyn
	-[0x8000ad88]:csrrs a2, fcsr, zero
	-[0x8000ad8c]:sw t6, 528(fp)
Current Store : [0x8000ad90] : sw a2, 532(fp) -- Store: [0x80013fec]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x5fb832 and fs2 == 0 and fe2 == 0x2e and fm2 == 0x127817 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000adc4]:fmul.s t6, t5, t4, dyn
	-[0x8000adc8]:csrrs a2, fcsr, zero
	-[0x8000adcc]:sw t6, 536(fp)
Current Store : [0x8000add0] : sw a2, 540(fp) -- Store: [0x80013ff4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x5fb832 and fs2 == 0 and fe2 == 0x2e and fm2 == 0x127817 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ae04]:fmul.s t6, t5, t4, dyn
	-[0x8000ae08]:csrrs a2, fcsr, zero
	-[0x8000ae0c]:sw t6, 544(fp)
Current Store : [0x8000ae10] : sw a2, 548(fp) -- Store: [0x80013ffc]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x5fb832 and fs2 == 0 and fe2 == 0x2e and fm2 == 0x127817 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ae44]:fmul.s t6, t5, t4, dyn
	-[0x8000ae48]:csrrs a2, fcsr, zero
	-[0x8000ae4c]:sw t6, 552(fp)
Current Store : [0x8000ae50] : sw a2, 556(fp) -- Store: [0x80014004]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x5fb832 and fs2 == 0 and fe2 == 0x2e and fm2 == 0x127817 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ae84]:fmul.s t6, t5, t4, dyn
	-[0x8000ae88]:csrrs a2, fcsr, zero
	-[0x8000ae8c]:sw t6, 560(fp)
Current Store : [0x8000ae90] : sw a2, 564(fp) -- Store: [0x8001400c]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x5fb832 and fs2 == 0 and fe2 == 0x2e and fm2 == 0x127817 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000aec4]:fmul.s t6, t5, t4, dyn
	-[0x8000aec8]:csrrs a2, fcsr, zero
	-[0x8000aecc]:sw t6, 568(fp)
Current Store : [0x8000aed0] : sw a2, 572(fp) -- Store: [0x80014014]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a6708 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x2fcaaa and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000af04]:fmul.s t6, t5, t4, dyn
	-[0x8000af08]:csrrs a2, fcsr, zero
	-[0x8000af0c]:sw t6, 576(fp)
Current Store : [0x8000af10] : sw a2, 580(fp) -- Store: [0x8001401c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a6708 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x2fcaaa and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000af44]:fmul.s t6, t5, t4, dyn
	-[0x8000af48]:csrrs a2, fcsr, zero
	-[0x8000af4c]:sw t6, 584(fp)
Current Store : [0x8000af50] : sw a2, 588(fp) -- Store: [0x80014024]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a6708 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x2fcaaa and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000af84]:fmul.s t6, t5, t4, dyn
	-[0x8000af88]:csrrs a2, fcsr, zero
	-[0x8000af8c]:sw t6, 592(fp)
Current Store : [0x8000af90] : sw a2, 596(fp) -- Store: [0x8001402c]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a6708 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x2fcaaa and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000afc4]:fmul.s t6, t5, t4, dyn
	-[0x8000afc8]:csrrs a2, fcsr, zero
	-[0x8000afcc]:sw t6, 600(fp)
Current Store : [0x8000afd0] : sw a2, 604(fp) -- Store: [0x80014034]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a6708 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x2fcaaa and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b004]:fmul.s t6, t5, t4, dyn
	-[0x8000b008]:csrrs a2, fcsr, zero
	-[0x8000b00c]:sw t6, 608(fp)
Current Store : [0x8000b010] : sw a2, 612(fp) -- Store: [0x8001403c]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7cd38d and fs2 == 0 and fe2 == 0x2b and fm2 == 0x019b52 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b044]:fmul.s t6, t5, t4, dyn
	-[0x8000b048]:csrrs a2, fcsr, zero
	-[0x8000b04c]:sw t6, 616(fp)
Current Store : [0x8000b050] : sw a2, 620(fp) -- Store: [0x80014044]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7cd38d and fs2 == 0 and fe2 == 0x2b and fm2 == 0x019b52 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b084]:fmul.s t6, t5, t4, dyn
	-[0x8000b088]:csrrs a2, fcsr, zero
	-[0x8000b08c]:sw t6, 624(fp)
Current Store : [0x8000b090] : sw a2, 628(fp) -- Store: [0x8001404c]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7cd38d and fs2 == 0 and fe2 == 0x2b and fm2 == 0x019b52 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b0c4]:fmul.s t6, t5, t4, dyn
	-[0x8000b0c8]:csrrs a2, fcsr, zero
	-[0x8000b0cc]:sw t6, 632(fp)
Current Store : [0x8000b0d0] : sw a2, 636(fp) -- Store: [0x80014054]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7cd38d and fs2 == 0 and fe2 == 0x2b and fm2 == 0x019b52 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b104]:fmul.s t6, t5, t4, dyn
	-[0x8000b108]:csrrs a2, fcsr, zero
	-[0x8000b10c]:sw t6, 640(fp)
Current Store : [0x8000b110] : sw a2, 644(fp) -- Store: [0x8001405c]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7cd38d and fs2 == 0 and fe2 == 0x2b and fm2 == 0x019b52 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b144]:fmul.s t6, t5, t4, dyn
	-[0x8000b148]:csrrs a2, fcsr, zero
	-[0x8000b14c]:sw t6, 648(fp)
Current Store : [0x8000b150] : sw a2, 652(fp) -- Store: [0x80014064]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a3d7b and fs2 == 1 and fe2 == 0x2b and fm2 == 0x2ff1e2 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b184]:fmul.s t6, t5, t4, dyn
	-[0x8000b188]:csrrs a2, fcsr, zero
	-[0x8000b18c]:sw t6, 656(fp)
Current Store : [0x8000b190] : sw a2, 660(fp) -- Store: [0x8001406c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a3d7b and fs2 == 1 and fe2 == 0x2b and fm2 == 0x2ff1e2 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b1c4]:fmul.s t6, t5, t4, dyn
	-[0x8000b1c8]:csrrs a2, fcsr, zero
	-[0x8000b1cc]:sw t6, 664(fp)
Current Store : [0x8000b1d0] : sw a2, 668(fp) -- Store: [0x80014074]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a3d7b and fs2 == 1 and fe2 == 0x2b and fm2 == 0x2ff1e2 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b204]:fmul.s t6, t5, t4, dyn
	-[0x8000b208]:csrrs a2, fcsr, zero
	-[0x8000b20c]:sw t6, 672(fp)
Current Store : [0x8000b210] : sw a2, 676(fp) -- Store: [0x8001407c]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a3d7b and fs2 == 1 and fe2 == 0x2b and fm2 == 0x2ff1e2 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b244]:fmul.s t6, t5, t4, dyn
	-[0x8000b248]:csrrs a2, fcsr, zero
	-[0x8000b24c]:sw t6, 680(fp)
Current Store : [0x8000b250] : sw a2, 684(fp) -- Store: [0x80014084]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a3d7b and fs2 == 1 and fe2 == 0x2b and fm2 == 0x2ff1e2 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b284]:fmul.s t6, t5, t4, dyn
	-[0x8000b288]:csrrs a2, fcsr, zero
	-[0x8000b28c]:sw t6, 688(fp)
Current Store : [0x8000b290] : sw a2, 692(fp) -- Store: [0x8001408c]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x009916 and fs2 == 1 and fe2 == 0x2b and fm2 == 0x7ecf3f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b2c4]:fmul.s t6, t5, t4, dyn
	-[0x8000b2c8]:csrrs a2, fcsr, zero
	-[0x8000b2cc]:sw t6, 696(fp)
Current Store : [0x8000b2d0] : sw a2, 700(fp) -- Store: [0x80014094]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x009916 and fs2 == 1 and fe2 == 0x2b and fm2 == 0x7ecf3f and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b304]:fmul.s t6, t5, t4, dyn
	-[0x8000b308]:csrrs a2, fcsr, zero
	-[0x8000b30c]:sw t6, 704(fp)
Current Store : [0x8000b310] : sw a2, 708(fp) -- Store: [0x8001409c]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x009916 and fs2 == 1 and fe2 == 0x2b and fm2 == 0x7ecf3f and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b344]:fmul.s t6, t5, t4, dyn
	-[0x8000b348]:csrrs a2, fcsr, zero
	-[0x8000b34c]:sw t6, 712(fp)
Current Store : [0x8000b350] : sw a2, 716(fp) -- Store: [0x800140a4]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x009916 and fs2 == 1 and fe2 == 0x2b and fm2 == 0x7ecf3f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b384]:fmul.s t6, t5, t4, dyn
	-[0x8000b388]:csrrs a2, fcsr, zero
	-[0x8000b38c]:sw t6, 720(fp)
Current Store : [0x8000b390] : sw a2, 724(fp) -- Store: [0x800140ac]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x009916 and fs2 == 1 and fe2 == 0x2b and fm2 == 0x7ecf3f and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b3c4]:fmul.s t6, t5, t4, dyn
	-[0x8000b3c8]:csrrs a2, fcsr, zero
	-[0x8000b3cc]:sw t6, 728(fp)
Current Store : [0x8000b3d0] : sw a2, 732(fp) -- Store: [0x800140b4]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x411d26 and fs2 == 1 and fe2 == 0x2b and fm2 == 0x29aea9 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b404]:fmul.s t6, t5, t4, dyn
	-[0x8000b408]:csrrs a2, fcsr, zero
	-[0x8000b40c]:sw t6, 736(fp)
Current Store : [0x8000b410] : sw a2, 740(fp) -- Store: [0x800140bc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x411d26 and fs2 == 1 and fe2 == 0x2b and fm2 == 0x29aea9 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b444]:fmul.s t6, t5, t4, dyn
	-[0x8000b448]:csrrs a2, fcsr, zero
	-[0x8000b44c]:sw t6, 744(fp)
Current Store : [0x8000b450] : sw a2, 748(fp) -- Store: [0x800140c4]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x411d26 and fs2 == 1 and fe2 == 0x2b and fm2 == 0x29aea9 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b484]:fmul.s t6, t5, t4, dyn
	-[0x8000b488]:csrrs a2, fcsr, zero
	-[0x8000b48c]:sw t6, 752(fp)
Current Store : [0x8000b490] : sw a2, 756(fp) -- Store: [0x800140cc]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x411d26 and fs2 == 1 and fe2 == 0x2b and fm2 == 0x29aea9 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b4c4]:fmul.s t6, t5, t4, dyn
	-[0x8000b4c8]:csrrs a2, fcsr, zero
	-[0x8000b4cc]:sw t6, 760(fp)
Current Store : [0x8000b4d0] : sw a2, 764(fp) -- Store: [0x800140d4]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x411d26 and fs2 == 1 and fe2 == 0x2b and fm2 == 0x29aea9 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b504]:fmul.s t6, t5, t4, dyn
	-[0x8000b508]:csrrs a2, fcsr, zero
	-[0x8000b50c]:sw t6, 768(fp)
Current Store : [0x8000b510] : sw a2, 772(fp) -- Store: [0x800140dc]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x752c06 and fs2 == 1 and fe2 == 0x2e and fm2 == 0x05a733 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b544]:fmul.s t6, t5, t4, dyn
	-[0x8000b548]:csrrs a2, fcsr, zero
	-[0x8000b54c]:sw t6, 776(fp)
Current Store : [0x8000b550] : sw a2, 780(fp) -- Store: [0x800140e4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x752c06 and fs2 == 1 and fe2 == 0x2e and fm2 == 0x05a733 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b584]:fmul.s t6, t5, t4, dyn
	-[0x8000b588]:csrrs a2, fcsr, zero
	-[0x8000b58c]:sw t6, 784(fp)
Current Store : [0x8000b590] : sw a2, 788(fp) -- Store: [0x800140ec]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x752c06 and fs2 == 1 and fe2 == 0x2e and fm2 == 0x05a733 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b5c4]:fmul.s t6, t5, t4, dyn
	-[0x8000b5c8]:csrrs a2, fcsr, zero
	-[0x8000b5cc]:sw t6, 792(fp)
Current Store : [0x8000b5d0] : sw a2, 796(fp) -- Store: [0x800140f4]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x752c06 and fs2 == 1 and fe2 == 0x2e and fm2 == 0x05a733 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b604]:fmul.s t6, t5, t4, dyn
	-[0x8000b608]:csrrs a2, fcsr, zero
	-[0x8000b60c]:sw t6, 800(fp)
Current Store : [0x8000b610] : sw a2, 804(fp) -- Store: [0x800140fc]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x752c06 and fs2 == 1 and fe2 == 0x2e and fm2 == 0x05a733 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b644]:fmul.s t6, t5, t4, dyn
	-[0x8000b648]:csrrs a2, fcsr, zero
	-[0x8000b64c]:sw t6, 808(fp)
Current Store : [0x8000b650] : sw a2, 812(fp) -- Store: [0x80014104]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x0566bc and fs2 == 1 and fe2 == 0x2c and fm2 == 0x75a27f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b684]:fmul.s t6, t5, t4, dyn
	-[0x8000b688]:csrrs a2, fcsr, zero
	-[0x8000b68c]:sw t6, 816(fp)
Current Store : [0x8000b690] : sw a2, 820(fp) -- Store: [0x8001410c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x0566bc and fs2 == 1 and fe2 == 0x2c and fm2 == 0x75a27f and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b6c4]:fmul.s t6, t5, t4, dyn
	-[0x8000b6c8]:csrrs a2, fcsr, zero
	-[0x8000b6cc]:sw t6, 824(fp)
Current Store : [0x8000b6d0] : sw a2, 828(fp) -- Store: [0x80014114]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x0566bc and fs2 == 1 and fe2 == 0x2c and fm2 == 0x75a27f and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b704]:fmul.s t6, t5, t4, dyn
	-[0x8000b708]:csrrs a2, fcsr, zero
	-[0x8000b70c]:sw t6, 832(fp)
Current Store : [0x8000b710] : sw a2, 836(fp) -- Store: [0x8001411c]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x0566bc and fs2 == 1 and fe2 == 0x2c and fm2 == 0x75a27f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b744]:fmul.s t6, t5, t4, dyn
	-[0x8000b748]:csrrs a2, fcsr, zero
	-[0x8000b74c]:sw t6, 840(fp)
Current Store : [0x8000b750] : sw a2, 844(fp) -- Store: [0x80014124]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x0566bc and fs2 == 1 and fe2 == 0x2c and fm2 == 0x75a27f and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b784]:fmul.s t6, t5, t4, dyn
	-[0x8000b788]:csrrs a2, fcsr, zero
	-[0x8000b78c]:sw t6, 848(fp)
Current Store : [0x8000b790] : sw a2, 852(fp) -- Store: [0x8001412c]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1699a5 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x59952e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b7c4]:fmul.s t6, t5, t4, dyn
	-[0x8000b7c8]:csrrs a2, fcsr, zero
	-[0x8000b7cc]:sw t6, 856(fp)
Current Store : [0x8000b7d0] : sw a2, 860(fp) -- Store: [0x80014134]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1699a5 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x59952e and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b804]:fmul.s t6, t5, t4, dyn
	-[0x8000b808]:csrrs a2, fcsr, zero
	-[0x8000b80c]:sw t6, 864(fp)
Current Store : [0x8000b810] : sw a2, 868(fp) -- Store: [0x8001413c]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1699a5 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x59952e and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b844]:fmul.s t6, t5, t4, dyn
	-[0x8000b848]:csrrs a2, fcsr, zero
	-[0x8000b84c]:sw t6, 872(fp)
Current Store : [0x8000b850] : sw a2, 876(fp) -- Store: [0x80014144]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1699a5 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x59952e and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b884]:fmul.s t6, t5, t4, dyn
	-[0x8000b888]:csrrs a2, fcsr, zero
	-[0x8000b88c]:sw t6, 880(fp)
Current Store : [0x8000b890] : sw a2, 884(fp) -- Store: [0x8001414c]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1699a5 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x59952e and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b8c4]:fmul.s t6, t5, t4, dyn
	-[0x8000b8c8]:csrrs a2, fcsr, zero
	-[0x8000b8cc]:sw t6, 888(fp)
Current Store : [0x8000b8d0] : sw a2, 892(fp) -- Store: [0x80014154]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x019943 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x7cd791 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b904]:fmul.s t6, t5, t4, dyn
	-[0x8000b908]:csrrs a2, fcsr, zero
	-[0x8000b90c]:sw t6, 896(fp)
Current Store : [0x8000b910] : sw a2, 900(fp) -- Store: [0x8001415c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x019943 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x7cd791 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b944]:fmul.s t6, t5, t4, dyn
	-[0x8000b948]:csrrs a2, fcsr, zero
	-[0x8000b94c]:sw t6, 904(fp)
Current Store : [0x8000b950] : sw a2, 908(fp) -- Store: [0x80014164]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x019943 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x7cd791 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b984]:fmul.s t6, t5, t4, dyn
	-[0x8000b988]:csrrs a2, fcsr, zero
	-[0x8000b98c]:sw t6, 912(fp)
Current Store : [0x8000b990] : sw a2, 916(fp) -- Store: [0x8001416c]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x019943 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x7cd791 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000b9c4]:fmul.s t6, t5, t4, dyn
	-[0x8000b9c8]:csrrs a2, fcsr, zero
	-[0x8000b9cc]:sw t6, 920(fp)
Current Store : [0x8000b9d0] : sw a2, 924(fp) -- Store: [0x80014174]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x019943 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x7cd791 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ba04]:fmul.s t6, t5, t4, dyn
	-[0x8000ba08]:csrrs a2, fcsr, zero
	-[0x8000ba0c]:sw t6, 928(fp)
Current Store : [0x8000ba10] : sw a2, 932(fp) -- Store: [0x8001417c]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x25e482 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x45867e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ba44]:fmul.s t6, t5, t4, dyn
	-[0x8000ba48]:csrrs a2, fcsr, zero
	-[0x8000ba4c]:sw t6, 936(fp)
Current Store : [0x8000ba50] : sw a2, 940(fp) -- Store: [0x80014184]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x25e482 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x45867e and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ba84]:fmul.s t6, t5, t4, dyn
	-[0x8000ba88]:csrrs a2, fcsr, zero
	-[0x8000ba8c]:sw t6, 944(fp)
Current Store : [0x8000ba90] : sw a2, 948(fp) -- Store: [0x8001418c]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x25e482 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x45867e and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000bac4]:fmul.s t6, t5, t4, dyn
	-[0x8000bac8]:csrrs a2, fcsr, zero
	-[0x8000bacc]:sw t6, 952(fp)
Current Store : [0x8000bad0] : sw a2, 956(fp) -- Store: [0x80014194]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x25e482 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x45867e and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000bb04]:fmul.s t6, t5, t4, dyn
	-[0x8000bb08]:csrrs a2, fcsr, zero
	-[0x8000bb0c]:sw t6, 960(fp)
Current Store : [0x8000bb10] : sw a2, 964(fp) -- Store: [0x8001419c]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x25e482 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x45867e and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000bb44]:fmul.s t6, t5, t4, dyn
	-[0x8000bb48]:csrrs a2, fcsr, zero
	-[0x8000bb4c]:sw t6, 968(fp)
Current Store : [0x8000bb50] : sw a2, 972(fp) -- Store: [0x800141a4]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x79b52d and fs2 == 1 and fe2 == 0x2d and fm2 == 0x0339b4 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000bb84]:fmul.s t6, t5, t4, dyn
	-[0x8000bb88]:csrrs a2, fcsr, zero
	-[0x8000bb8c]:sw t6, 976(fp)
Current Store : [0x8000bb90] : sw a2, 980(fp) -- Store: [0x800141ac]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x79b52d and fs2 == 1 and fe2 == 0x2d and fm2 == 0x0339b4 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000bbc4]:fmul.s t6, t5, t4, dyn
	-[0x8000bbc8]:csrrs a2, fcsr, zero
	-[0x8000bbcc]:sw t6, 984(fp)
Current Store : [0x8000bbd0] : sw a2, 988(fp) -- Store: [0x800141b4]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x79b52d and fs2 == 1 and fe2 == 0x2d and fm2 == 0x0339b4 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000bc04]:fmul.s t6, t5, t4, dyn
	-[0x8000bc08]:csrrs a2, fcsr, zero
	-[0x8000bc0c]:sw t6, 992(fp)
Current Store : [0x8000bc10] : sw a2, 996(fp) -- Store: [0x800141bc]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x79b52d and fs2 == 1 and fe2 == 0x2d and fm2 == 0x0339b4 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000bc44]:fmul.s t6, t5, t4, dyn
	-[0x8000bc48]:csrrs a2, fcsr, zero
	-[0x8000bc4c]:sw t6, 1000(fp)
Current Store : [0x8000bc50] : sw a2, 1004(fp) -- Store: [0x800141c4]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x79b52d and fs2 == 1 and fe2 == 0x2d and fm2 == 0x0339b4 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000bc84]:fmul.s t6, t5, t4, dyn
	-[0x8000bc88]:csrrs a2, fcsr, zero
	-[0x8000bc8c]:sw t6, 1008(fp)
Current Store : [0x8000bc90] : sw a2, 1012(fp) -- Store: [0x800141cc]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x326d9c and fs2 == 1 and fe2 == 0x2b and fm2 == 0x37a5ec and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000bcc4]:fmul.s t6, t5, t4, dyn
	-[0x8000bcc8]:csrrs a2, fcsr, zero
	-[0x8000bccc]:sw t6, 1016(fp)
Current Store : [0x8000bcd0] : sw a2, 1020(fp) -- Store: [0x800141d4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x326d9c and fs2 == 1 and fe2 == 0x2b and fm2 == 0x37a5ec and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000bd0c]:fmul.s t6, t5, t4, dyn
	-[0x8000bd10]:csrrs a2, fcsr, zero
	-[0x8000bd14]:sw t6, 0(fp)
Current Store : [0x8000bd18] : sw a2, 4(fp) -- Store: [0x800141dc]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x326d9c and fs2 == 1 and fe2 == 0x2b and fm2 == 0x37a5ec and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000bd4c]:fmul.s t6, t5, t4, dyn
	-[0x8000bd50]:csrrs a2, fcsr, zero
	-[0x8000bd54]:sw t6, 8(fp)
Current Store : [0x8000bd58] : sw a2, 12(fp) -- Store: [0x800141e4]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x326d9c and fs2 == 1 and fe2 == 0x2b and fm2 == 0x37a5ec and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000bd8c]:fmul.s t6, t5, t4, dyn
	-[0x8000bd90]:csrrs a2, fcsr, zero
	-[0x8000bd94]:sw t6, 16(fp)
Current Store : [0x8000bd98] : sw a2, 20(fp) -- Store: [0x800141ec]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x326d9c and fs2 == 1 and fe2 == 0x2b and fm2 == 0x37a5ec and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000bdcc]:fmul.s t6, t5, t4, dyn
	-[0x8000bdd0]:csrrs a2, fcsr, zero
	-[0x8000bdd4]:sw t6, 24(fp)
Current Store : [0x8000bdd8] : sw a2, 28(fp) -- Store: [0x800141f4]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf7 and fm1 == 0x7f98a2 and fs2 == 1 and fe2 == 0x31 and fm2 == 0x0033c3 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000be0c]:fmul.s t6, t5, t4, dyn
	-[0x8000be10]:csrrs a2, fcsr, zero
	-[0x8000be14]:sw t6, 32(fp)
Current Store : [0x8000be18] : sw a2, 36(fp) -- Store: [0x800141fc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf7 and fm1 == 0x7f98a2 and fs2 == 1 and fe2 == 0x31 and fm2 == 0x0033c3 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000be4c]:fmul.s t6, t5, t4, dyn
	-[0x8000be50]:csrrs a2, fcsr, zero
	-[0x8000be54]:sw t6, 40(fp)
Current Store : [0x8000be58] : sw a2, 44(fp) -- Store: [0x80014204]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf7 and fm1 == 0x7f98a2 and fs2 == 1 and fe2 == 0x31 and fm2 == 0x0033c3 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000be8c]:fmul.s t6, t5, t4, dyn
	-[0x8000be90]:csrrs a2, fcsr, zero
	-[0x8000be94]:sw t6, 48(fp)
Current Store : [0x8000be98] : sw a2, 52(fp) -- Store: [0x8001420c]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf7 and fm1 == 0x7f98a2 and fs2 == 1 and fe2 == 0x31 and fm2 == 0x0033c3 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000becc]:fmul.s t6, t5, t4, dyn
	-[0x8000bed0]:csrrs a2, fcsr, zero
	-[0x8000bed4]:sw t6, 56(fp)
Current Store : [0x8000bed8] : sw a2, 60(fp) -- Store: [0x80014214]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf7 and fm1 == 0x7f98a2 and fs2 == 1 and fe2 == 0x31 and fm2 == 0x0033c3 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000bf0c]:fmul.s t6, t5, t4, dyn
	-[0x8000bf10]:csrrs a2, fcsr, zero
	-[0x8000bf14]:sw t6, 64(fp)
Current Store : [0x8000bf18] : sw a2, 68(fp) -- Store: [0x8001421c]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x39a5be and fs2 == 1 and fe2 == 0x2a and fm2 == 0x3081b1 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000bf4c]:fmul.s t6, t5, t4, dyn
	-[0x8000bf50]:csrrs a2, fcsr, zero
	-[0x8000bf54]:sw t6, 72(fp)
Current Store : [0x8000bf58] : sw a2, 76(fp) -- Store: [0x80014224]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x39a5be and fs2 == 1 and fe2 == 0x2a and fm2 == 0x3081b1 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000bf8c]:fmul.s t6, t5, t4, dyn
	-[0x8000bf90]:csrrs a2, fcsr, zero
	-[0x8000bf94]:sw t6, 80(fp)
Current Store : [0x8000bf98] : sw a2, 84(fp) -- Store: [0x8001422c]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x39a5be and fs2 == 1 and fe2 == 0x2a and fm2 == 0x3081b1 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000bfcc]:fmul.s t6, t5, t4, dyn
	-[0x8000bfd0]:csrrs a2, fcsr, zero
	-[0x8000bfd4]:sw t6, 88(fp)
Current Store : [0x8000bfd8] : sw a2, 92(fp) -- Store: [0x80014234]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x39a5be and fs2 == 1 and fe2 == 0x2a and fm2 == 0x3081b1 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c00c]:fmul.s t6, t5, t4, dyn
	-[0x8000c010]:csrrs a2, fcsr, zero
	-[0x8000c014]:sw t6, 96(fp)
Current Store : [0x8000c018] : sw a2, 100(fp) -- Store: [0x8001423c]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x39a5be and fs2 == 1 and fe2 == 0x2a and fm2 == 0x3081b1 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c04c]:fmul.s t6, t5, t4, dyn
	-[0x8000c050]:csrrs a2, fcsr, zero
	-[0x8000c054]:sw t6, 104(fp)
Current Store : [0x8000c058] : sw a2, 108(fp) -- Store: [0x80014244]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4e977e and fs2 == 1 and fe2 == 0x2b and fm2 == 0x1e9cc1 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c08c]:fmul.s t6, t5, t4, dyn
	-[0x8000c090]:csrrs a2, fcsr, zero
	-[0x8000c094]:sw t6, 112(fp)
Current Store : [0x8000c098] : sw a2, 116(fp) -- Store: [0x8001424c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4e977e and fs2 == 1 and fe2 == 0x2b and fm2 == 0x1e9cc1 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c0cc]:fmul.s t6, t5, t4, dyn
	-[0x8000c0d0]:csrrs a2, fcsr, zero
	-[0x8000c0d4]:sw t6, 120(fp)
Current Store : [0x8000c0d8] : sw a2, 124(fp) -- Store: [0x80014254]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4e977e and fs2 == 1 and fe2 == 0x2b and fm2 == 0x1e9cc1 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c10c]:fmul.s t6, t5, t4, dyn
	-[0x8000c110]:csrrs a2, fcsr, zero
	-[0x8000c114]:sw t6, 128(fp)
Current Store : [0x8000c118] : sw a2, 132(fp) -- Store: [0x8001425c]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4e977e and fs2 == 1 and fe2 == 0x2b and fm2 == 0x1e9cc1 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c14c]:fmul.s t6, t5, t4, dyn
	-[0x8000c150]:csrrs a2, fcsr, zero
	-[0x8000c154]:sw t6, 136(fp)
Current Store : [0x8000c158] : sw a2, 140(fp) -- Store: [0x80014264]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4e977e and fs2 == 1 and fe2 == 0x2b and fm2 == 0x1e9cc1 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c18c]:fmul.s t6, t5, t4, dyn
	-[0x8000c190]:csrrs a2, fcsr, zero
	-[0x8000c194]:sw t6, 144(fp)
Current Store : [0x8000c198] : sw a2, 148(fp) -- Store: [0x8001426c]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2a090e and fs2 == 1 and fe2 == 0x2b and fm2 == 0x40b67c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c1cc]:fmul.s t6, t5, t4, dyn
	-[0x8000c1d0]:csrrs a2, fcsr, zero
	-[0x8000c1d4]:sw t6, 152(fp)
Current Store : [0x8000c1d8] : sw a2, 156(fp) -- Store: [0x80014274]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2a090e and fs2 == 1 and fe2 == 0x2b and fm2 == 0x40b67c and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c20c]:fmul.s t6, t5, t4, dyn
	-[0x8000c210]:csrrs a2, fcsr, zero
	-[0x8000c214]:sw t6, 160(fp)
Current Store : [0x8000c218] : sw a2, 164(fp) -- Store: [0x8001427c]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2a090e and fs2 == 1 and fe2 == 0x2b and fm2 == 0x40b67c and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c24c]:fmul.s t6, t5, t4, dyn
	-[0x8000c250]:csrrs a2, fcsr, zero
	-[0x8000c254]:sw t6, 168(fp)
Current Store : [0x8000c258] : sw a2, 172(fp) -- Store: [0x80014284]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2a090e and fs2 == 1 and fe2 == 0x2b and fm2 == 0x40b67c and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c28c]:fmul.s t6, t5, t4, dyn
	-[0x8000c290]:csrrs a2, fcsr, zero
	-[0x8000c294]:sw t6, 176(fp)
Current Store : [0x8000c298] : sw a2, 180(fp) -- Store: [0x8001428c]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2a090e and fs2 == 1 and fe2 == 0x2b and fm2 == 0x40b67c and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c2cc]:fmul.s t6, t5, t4, dyn
	-[0x8000c2d0]:csrrs a2, fcsr, zero
	-[0x8000c2d4]:sw t6, 184(fp)
Current Store : [0x8000c2d8] : sw a2, 188(fp) -- Store: [0x80014294]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6cdfe3 and fs2 == 0 and fe2 == 0x2c and fm2 == 0x0a55b6 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c30c]:fmul.s t6, t5, t4, dyn
	-[0x8000c310]:csrrs a2, fcsr, zero
	-[0x8000c314]:sw t6, 192(fp)
Current Store : [0x8000c318] : sw a2, 196(fp) -- Store: [0x8001429c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6cdfe3 and fs2 == 0 and fe2 == 0x2c and fm2 == 0x0a55b6 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c34c]:fmul.s t6, t5, t4, dyn
	-[0x8000c350]:csrrs a2, fcsr, zero
	-[0x8000c354]:sw t6, 200(fp)
Current Store : [0x8000c358] : sw a2, 204(fp) -- Store: [0x800142a4]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6cdfe3 and fs2 == 0 and fe2 == 0x2c and fm2 == 0x0a55b6 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c38c]:fmul.s t6, t5, t4, dyn
	-[0x8000c390]:csrrs a2, fcsr, zero
	-[0x8000c394]:sw t6, 208(fp)
Current Store : [0x8000c398] : sw a2, 212(fp) -- Store: [0x800142ac]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6cdfe3 and fs2 == 0 and fe2 == 0x2c and fm2 == 0x0a55b6 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c3cc]:fmul.s t6, t5, t4, dyn
	-[0x8000c3d0]:csrrs a2, fcsr, zero
	-[0x8000c3d4]:sw t6, 216(fp)
Current Store : [0x8000c3d8] : sw a2, 220(fp) -- Store: [0x800142b4]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6cdfe3 and fs2 == 0 and fe2 == 0x2c and fm2 == 0x0a55b6 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c40c]:fmul.s t6, t5, t4, dyn
	-[0x8000c410]:csrrs a2, fcsr, zero
	-[0x8000c414]:sw t6, 224(fp)
Current Store : [0x8000c418] : sw a2, 228(fp) -- Store: [0x800142bc]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x39c626 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x3062e7 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c44c]:fmul.s t6, t5, t4, dyn
	-[0x8000c450]:csrrs a2, fcsr, zero
	-[0x8000c454]:sw t6, 232(fp)
Current Store : [0x8000c458] : sw a2, 236(fp) -- Store: [0x800142c4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x39c626 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x3062e7 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c48c]:fmul.s t6, t5, t4, dyn
	-[0x8000c490]:csrrs a2, fcsr, zero
	-[0x8000c494]:sw t6, 240(fp)
Current Store : [0x8000c498] : sw a2, 244(fp) -- Store: [0x800142cc]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x39c626 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x3062e7 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c4cc]:fmul.s t6, t5, t4, dyn
	-[0x8000c4d0]:csrrs a2, fcsr, zero
	-[0x8000c4d4]:sw t6, 248(fp)
Current Store : [0x8000c4d8] : sw a2, 252(fp) -- Store: [0x800142d4]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x39c626 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x3062e7 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c50c]:fmul.s t6, t5, t4, dyn
	-[0x8000c510]:csrrs a2, fcsr, zero
	-[0x8000c514]:sw t6, 256(fp)
Current Store : [0x8000c518] : sw a2, 260(fp) -- Store: [0x800142dc]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x39c626 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x3062e7 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c54c]:fmul.s t6, t5, t4, dyn
	-[0x8000c550]:csrrs a2, fcsr, zero
	-[0x8000c554]:sw t6, 264(fp)
Current Store : [0x8000c558] : sw a2, 268(fp) -- Store: [0x800142e4]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2f9f7c and fs2 == 0 and fe2 == 0x2a and fm2 == 0x3a94dc and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c58c]:fmul.s t6, t5, t4, dyn
	-[0x8000c590]:csrrs a2, fcsr, zero
	-[0x8000c594]:sw t6, 272(fp)
Current Store : [0x8000c598] : sw a2, 276(fp) -- Store: [0x800142ec]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2f9f7c and fs2 == 0 and fe2 == 0x2a and fm2 == 0x3a94dc and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c5cc]:fmul.s t6, t5, t4, dyn
	-[0x8000c5d0]:csrrs a2, fcsr, zero
	-[0x8000c5d4]:sw t6, 280(fp)
Current Store : [0x8000c5d8] : sw a2, 284(fp) -- Store: [0x800142f4]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2f9f7c and fs2 == 0 and fe2 == 0x2a and fm2 == 0x3a94dc and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c60c]:fmul.s t6, t5, t4, dyn
	-[0x8000c610]:csrrs a2, fcsr, zero
	-[0x8000c614]:sw t6, 288(fp)
Current Store : [0x8000c618] : sw a2, 292(fp) -- Store: [0x800142fc]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2f9f7c and fs2 == 0 and fe2 == 0x2a and fm2 == 0x3a94dc and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c64c]:fmul.s t6, t5, t4, dyn
	-[0x8000c650]:csrrs a2, fcsr, zero
	-[0x8000c654]:sw t6, 296(fp)
Current Store : [0x8000c658] : sw a2, 300(fp) -- Store: [0x80014304]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2f9f7c and fs2 == 0 and fe2 == 0x2a and fm2 == 0x3a94dc and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c68c]:fmul.s t6, t5, t4, dyn
	-[0x8000c690]:csrrs a2, fcsr, zero
	-[0x8000c694]:sw t6, 304(fp)
Current Store : [0x8000c698] : sw a2, 308(fp) -- Store: [0x8001430c]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1e4774 and fs2 == 0 and fe2 == 0x2a and fm2 == 0x4f06d4 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c6cc]:fmul.s t6, t5, t4, dyn
	-[0x8000c6d0]:csrrs a2, fcsr, zero
	-[0x8000c6d4]:sw t6, 312(fp)
Current Store : [0x8000c6d8] : sw a2, 316(fp) -- Store: [0x80014314]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1e4774 and fs2 == 0 and fe2 == 0x2a and fm2 == 0x4f06d4 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c70c]:fmul.s t6, t5, t4, dyn
	-[0x8000c710]:csrrs a2, fcsr, zero
	-[0x8000c714]:sw t6, 320(fp)
Current Store : [0x8000c718] : sw a2, 324(fp) -- Store: [0x8001431c]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1e4774 and fs2 == 0 and fe2 == 0x2a and fm2 == 0x4f06d4 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c74c]:fmul.s t6, t5, t4, dyn
	-[0x8000c750]:csrrs a2, fcsr, zero
	-[0x8000c754]:sw t6, 328(fp)
Current Store : [0x8000c758] : sw a2, 332(fp) -- Store: [0x80014324]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1e4774 and fs2 == 0 and fe2 == 0x2a and fm2 == 0x4f06d4 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c78c]:fmul.s t6, t5, t4, dyn
	-[0x8000c790]:csrrs a2, fcsr, zero
	-[0x8000c794]:sw t6, 336(fp)
Current Store : [0x8000c798] : sw a2, 340(fp) -- Store: [0x8001432c]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1e4774 and fs2 == 0 and fe2 == 0x2a and fm2 == 0x4f06d4 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c7cc]:fmul.s t6, t5, t4, dyn
	-[0x8000c7d0]:csrrs a2, fcsr, zero
	-[0x8000c7d4]:sw t6, 344(fp)
Current Store : [0x8000c7d8] : sw a2, 348(fp) -- Store: [0x80014334]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x782eaf and fs2 == 0 and fe2 == 0x2b and fm2 == 0x04082d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c80c]:fmul.s t6, t5, t4, dyn
	-[0x8000c810]:csrrs a2, fcsr, zero
	-[0x8000c814]:sw t6, 352(fp)
Current Store : [0x8000c818] : sw a2, 356(fp) -- Store: [0x8001433c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x782eaf and fs2 == 0 and fe2 == 0x2b and fm2 == 0x04082d and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c84c]:fmul.s t6, t5, t4, dyn
	-[0x8000c850]:csrrs a2, fcsr, zero
	-[0x8000c854]:sw t6, 360(fp)
Current Store : [0x8000c858] : sw a2, 364(fp) -- Store: [0x80014344]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x782eaf and fs2 == 0 and fe2 == 0x2b and fm2 == 0x04082d and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c88c]:fmul.s t6, t5, t4, dyn
	-[0x8000c890]:csrrs a2, fcsr, zero
	-[0x8000c894]:sw t6, 368(fp)
Current Store : [0x8000c898] : sw a2, 372(fp) -- Store: [0x8001434c]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x782eaf and fs2 == 0 and fe2 == 0x2b and fm2 == 0x04082d and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c8cc]:fmul.s t6, t5, t4, dyn
	-[0x8000c8d0]:csrrs a2, fcsr, zero
	-[0x8000c8d4]:sw t6, 376(fp)
Current Store : [0x8000c8d8] : sw a2, 380(fp) -- Store: [0x80014354]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x782eaf and fs2 == 0 and fe2 == 0x2b and fm2 == 0x04082d and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c90c]:fmul.s t6, t5, t4, dyn
	-[0x8000c910]:csrrs a2, fcsr, zero
	-[0x8000c914]:sw t6, 384(fp)
Current Store : [0x8000c918] : sw a2, 388(fp) -- Store: [0x8001435c]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7ae8c2 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x0298d7 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c94c]:fmul.s t6, t5, t4, dyn
	-[0x8000c950]:csrrs a2, fcsr, zero
	-[0x8000c954]:sw t6, 392(fp)
Current Store : [0x8000c958] : sw a2, 396(fp) -- Store: [0x80014364]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7ae8c2 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x0298d7 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c98c]:fmul.s t6, t5, t4, dyn
	-[0x8000c990]:csrrs a2, fcsr, zero
	-[0x8000c994]:sw t6, 400(fp)
Current Store : [0x8000c998] : sw a2, 404(fp) -- Store: [0x8001436c]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7ae8c2 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x0298d7 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c9cc]:fmul.s t6, t5, t4, dyn
	-[0x8000c9d0]:csrrs a2, fcsr, zero
	-[0x8000c9d4]:sw t6, 408(fp)
Current Store : [0x8000c9d8] : sw a2, 412(fp) -- Store: [0x80014374]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7ae8c2 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x0298d7 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ca0c]:fmul.s t6, t5, t4, dyn
	-[0x8000ca10]:csrrs a2, fcsr, zero
	-[0x8000ca14]:sw t6, 416(fp)
Current Store : [0x8000ca18] : sw a2, 420(fp) -- Store: [0x8001437c]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7ae8c2 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x0298d7 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ca4c]:fmul.s t6, t5, t4, dyn
	-[0x8000ca50]:csrrs a2, fcsr, zero
	-[0x8000ca54]:sw t6, 424(fp)
Current Store : [0x8000ca58] : sw a2, 428(fp) -- Store: [0x80014384]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x216a51 and fs2 == 0 and fe2 == 0x2a and fm2 == 0x4b0119 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ca8c]:fmul.s t6, t5, t4, dyn
	-[0x8000ca90]:csrrs a2, fcsr, zero
	-[0x8000ca94]:sw t6, 432(fp)
Current Store : [0x8000ca98] : sw a2, 436(fp) -- Store: [0x8001438c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x216a51 and fs2 == 0 and fe2 == 0x2a and fm2 == 0x4b0119 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cacc]:fmul.s t6, t5, t4, dyn
	-[0x8000cad0]:csrrs a2, fcsr, zero
	-[0x8000cad4]:sw t6, 440(fp)
Current Store : [0x8000cad8] : sw a2, 444(fp) -- Store: [0x80014394]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x216a51 and fs2 == 0 and fe2 == 0x2a and fm2 == 0x4b0119 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cb0c]:fmul.s t6, t5, t4, dyn
	-[0x8000cb10]:csrrs a2, fcsr, zero
	-[0x8000cb14]:sw t6, 448(fp)
Current Store : [0x8000cb18] : sw a2, 452(fp) -- Store: [0x8001439c]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x216a51 and fs2 == 0 and fe2 == 0x2a and fm2 == 0x4b0119 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cb4c]:fmul.s t6, t5, t4, dyn
	-[0x8000cb50]:csrrs a2, fcsr, zero
	-[0x8000cb54]:sw t6, 456(fp)
Current Store : [0x8000cb58] : sw a2, 460(fp) -- Store: [0x800143a4]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x216a51 and fs2 == 0 and fe2 == 0x2a and fm2 == 0x4b0119 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cb8c]:fmul.s t6, t5, t4, dyn
	-[0x8000cb90]:csrrs a2, fcsr, zero
	-[0x8000cb94]:sw t6, 464(fp)
Current Store : [0x8000cb98] : sw a2, 468(fp) -- Store: [0x800143ac]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x733156 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cbcc]:fmul.s t6, t5, t4, dyn
	-[0x8000cbd0]:csrrs a2, fcsr, zero
	-[0x8000cbd4]:sw t6, 472(fp)
Current Store : [0x8000cbd8] : sw a2, 476(fp) -- Store: [0x800143b4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x733156 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cc0c]:fmul.s t6, t5, t4, dyn
	-[0x8000cc10]:csrrs a2, fcsr, zero
	-[0x8000cc14]:sw t6, 480(fp)
Current Store : [0x8000cc18] : sw a2, 484(fp) -- Store: [0x800143bc]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x733156 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cc4c]:fmul.s t6, t5, t4, dyn
	-[0x8000cc50]:csrrs a2, fcsr, zero
	-[0x8000cc54]:sw t6, 488(fp)
Current Store : [0x8000cc58] : sw a2, 492(fp) -- Store: [0x800143c4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x733156 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cc8c]:fmul.s t6, t5, t4, dyn
	-[0x8000cc90]:csrrs a2, fcsr, zero
	-[0x8000cc94]:sw t6, 496(fp)
Current Store : [0x8000cc98] : sw a2, 500(fp) -- Store: [0x800143cc]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x733156 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cccc]:fmul.s t6, t5, t4, dyn
	-[0x8000ccd0]:csrrs a2, fcsr, zero
	-[0x8000ccd4]:sw t6, 504(fp)
Current Store : [0x8000ccd8] : sw a2, 508(fp) -- Store: [0x800143d4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4bb2ed and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cd0c]:fmul.s t6, t5, t4, dyn
	-[0x8000cd10]:csrrs a2, fcsr, zero
	-[0x8000cd14]:sw t6, 512(fp)
Current Store : [0x8000cd18] : sw a2, 516(fp) -- Store: [0x800143dc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4bb2ed and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cd4c]:fmul.s t6, t5, t4, dyn
	-[0x8000cd50]:csrrs a2, fcsr, zero
	-[0x8000cd54]:sw t6, 520(fp)
Current Store : [0x8000cd58] : sw a2, 524(fp) -- Store: [0x800143e4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4bb2ed and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cd8c]:fmul.s t6, t5, t4, dyn
	-[0x8000cd90]:csrrs a2, fcsr, zero
	-[0x8000cd94]:sw t6, 528(fp)
Current Store : [0x8000cd98] : sw a2, 532(fp) -- Store: [0x800143ec]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4bb2ed and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cdcc]:fmul.s t6, t5, t4, dyn
	-[0x8000cdd0]:csrrs a2, fcsr, zero
	-[0x8000cdd4]:sw t6, 536(fp)
Current Store : [0x8000cdd8] : sw a2, 540(fp) -- Store: [0x800143f4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4bb2ed and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ce0c]:fmul.s t6, t5, t4, dyn
	-[0x8000ce10]:csrrs a2, fcsr, zero
	-[0x8000ce14]:sw t6, 544(fp)
Current Store : [0x8000ce18] : sw a2, 548(fp) -- Store: [0x800143fc]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1930a7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ce4c]:fmul.s t6, t5, t4, dyn
	-[0x8000ce50]:csrrs a2, fcsr, zero
	-[0x8000ce54]:sw t6, 552(fp)
Current Store : [0x8000ce58] : sw a2, 556(fp) -- Store: [0x80014404]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1930a7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ce8c]:fmul.s t6, t5, t4, dyn
	-[0x8000ce90]:csrrs a2, fcsr, zero
	-[0x8000ce94]:sw t6, 560(fp)
Current Store : [0x8000ce98] : sw a2, 564(fp) -- Store: [0x8001440c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1930a7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cecc]:fmul.s t6, t5, t4, dyn
	-[0x8000ced0]:csrrs a2, fcsr, zero
	-[0x8000ced4]:sw t6, 568(fp)
Current Store : [0x8000ced8] : sw a2, 572(fp) -- Store: [0x80014414]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1930a7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cf0c]:fmul.s t6, t5, t4, dyn
	-[0x8000cf10]:csrrs a2, fcsr, zero
	-[0x8000cf14]:sw t6, 576(fp)
Current Store : [0x8000cf18] : sw a2, 580(fp) -- Store: [0x8001441c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1930a7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cf4c]:fmul.s t6, t5, t4, dyn
	-[0x8000cf50]:csrrs a2, fcsr, zero
	-[0x8000cf54]:sw t6, 584(fp)
Current Store : [0x8000cf58] : sw a2, 588(fp) -- Store: [0x80014424]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6c9ad9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cf8c]:fmul.s t6, t5, t4, dyn
	-[0x8000cf90]:csrrs a2, fcsr, zero
	-[0x8000cf94]:sw t6, 592(fp)
Current Store : [0x8000cf98] : sw a2, 596(fp) -- Store: [0x8001442c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6c9ad9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cfcc]:fmul.s t6, t5, t4, dyn
	-[0x8000cfd0]:csrrs a2, fcsr, zero
	-[0x8000cfd4]:sw t6, 600(fp)
Current Store : [0x8000cfd8] : sw a2, 604(fp) -- Store: [0x80014434]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6c9ad9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d00c]:fmul.s t6, t5, t4, dyn
	-[0x8000d010]:csrrs a2, fcsr, zero
	-[0x8000d014]:sw t6, 608(fp)
Current Store : [0x8000d018] : sw a2, 612(fp) -- Store: [0x8001443c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6c9ad9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d04c]:fmul.s t6, t5, t4, dyn
	-[0x8000d050]:csrrs a2, fcsr, zero
	-[0x8000d054]:sw t6, 616(fp)
Current Store : [0x8000d058] : sw a2, 620(fp) -- Store: [0x80014444]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6c9ad9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d08c]:fmul.s t6, t5, t4, dyn
	-[0x8000d090]:csrrs a2, fcsr, zero
	-[0x8000d094]:sw t6, 624(fp)
Current Store : [0x8000d098] : sw a2, 628(fp) -- Store: [0x8001444c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2fb1d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d0cc]:fmul.s t6, t5, t4, dyn
	-[0x8000d0d0]:csrrs a2, fcsr, zero
	-[0x8000d0d4]:sw t6, 632(fp)
Current Store : [0x8000d0d8] : sw a2, 636(fp) -- Store: [0x80014454]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2fb1d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d10c]:fmul.s t6, t5, t4, dyn
	-[0x8000d110]:csrrs a2, fcsr, zero
	-[0x8000d114]:sw t6, 640(fp)
Current Store : [0x8000d118] : sw a2, 644(fp) -- Store: [0x8001445c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2fb1d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d14c]:fmul.s t6, t5, t4, dyn
	-[0x8000d150]:csrrs a2, fcsr, zero
	-[0x8000d154]:sw t6, 648(fp)
Current Store : [0x8000d158] : sw a2, 652(fp) -- Store: [0x80014464]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2fb1d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d18c]:fmul.s t6, t5, t4, dyn
	-[0x8000d190]:csrrs a2, fcsr, zero
	-[0x8000d194]:sw t6, 656(fp)
Current Store : [0x8000d198] : sw a2, 660(fp) -- Store: [0x8001446c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2fb1d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d1cc]:fmul.s t6, t5, t4, dyn
	-[0x8000d1d0]:csrrs a2, fcsr, zero
	-[0x8000d1d4]:sw t6, 664(fp)
Current Store : [0x8000d1d8] : sw a2, 668(fp) -- Store: [0x80014474]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x551c11 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d20c]:fmul.s t6, t5, t4, dyn
	-[0x8000d210]:csrrs a2, fcsr, zero
	-[0x8000d214]:sw t6, 672(fp)
Current Store : [0x8000d218] : sw a2, 676(fp) -- Store: [0x8001447c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x551c11 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d24c]:fmul.s t6, t5, t4, dyn
	-[0x8000d250]:csrrs a2, fcsr, zero
	-[0x8000d254]:sw t6, 680(fp)
Current Store : [0x8000d258] : sw a2, 684(fp) -- Store: [0x80014484]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x551c11 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d28c]:fmul.s t6, t5, t4, dyn
	-[0x8000d290]:csrrs a2, fcsr, zero
	-[0x8000d294]:sw t6, 688(fp)
Current Store : [0x8000d298] : sw a2, 692(fp) -- Store: [0x8001448c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x551c11 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d2cc]:fmul.s t6, t5, t4, dyn
	-[0x8000d2d0]:csrrs a2, fcsr, zero
	-[0x8000d2d4]:sw t6, 696(fp)
Current Store : [0x8000d2d8] : sw a2, 700(fp) -- Store: [0x80014494]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x551c11 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d30c]:fmul.s t6, t5, t4, dyn
	-[0x8000d310]:csrrs a2, fcsr, zero
	-[0x8000d314]:sw t6, 704(fp)
Current Store : [0x8000d318] : sw a2, 708(fp) -- Store: [0x8001449c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x2bfad8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d34c]:fmul.s t6, t5, t4, dyn
	-[0x8000d350]:csrrs a2, fcsr, zero
	-[0x8000d354]:sw t6, 712(fp)
Current Store : [0x8000d358] : sw a2, 716(fp) -- Store: [0x800144a4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x2bfad8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d38c]:fmul.s t6, t5, t4, dyn
	-[0x8000d390]:csrrs a2, fcsr, zero
	-[0x8000d394]:sw t6, 720(fp)
Current Store : [0x8000d398] : sw a2, 724(fp) -- Store: [0x800144ac]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x2bfad8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d3cc]:fmul.s t6, t5, t4, dyn
	-[0x8000d3d0]:csrrs a2, fcsr, zero
	-[0x8000d3d4]:sw t6, 728(fp)
Current Store : [0x8000d3d8] : sw a2, 732(fp) -- Store: [0x800144b4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x2bfad8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d40c]:fmul.s t6, t5, t4, dyn
	-[0x8000d410]:csrrs a2, fcsr, zero
	-[0x8000d414]:sw t6, 736(fp)
Current Store : [0x8000d418] : sw a2, 740(fp) -- Store: [0x800144bc]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x2bfad8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d44c]:fmul.s t6, t5, t4, dyn
	-[0x8000d450]:csrrs a2, fcsr, zero
	-[0x8000d454]:sw t6, 744(fp)
Current Store : [0x8000d458] : sw a2, 748(fp) -- Store: [0x800144c4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x119e42 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d48c]:fmul.s t6, t5, t4, dyn
	-[0x8000d490]:csrrs a2, fcsr, zero
	-[0x8000d494]:sw t6, 752(fp)
Current Store : [0x8000d498] : sw a2, 756(fp) -- Store: [0x800144cc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x119e42 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d4cc]:fmul.s t6, t5, t4, dyn
	-[0x8000d4d0]:csrrs a2, fcsr, zero
	-[0x8000d4d4]:sw t6, 760(fp)
Current Store : [0x8000d4d8] : sw a2, 764(fp) -- Store: [0x800144d4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x119e42 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d50c]:fmul.s t6, t5, t4, dyn
	-[0x8000d510]:csrrs a2, fcsr, zero
	-[0x8000d514]:sw t6, 768(fp)
Current Store : [0x8000d518] : sw a2, 772(fp) -- Store: [0x800144dc]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x119e42 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d54c]:fmul.s t6, t5, t4, dyn
	-[0x8000d550]:csrrs a2, fcsr, zero
	-[0x8000d554]:sw t6, 776(fp)
Current Store : [0x8000d558] : sw a2, 780(fp) -- Store: [0x800144e4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x119e42 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d58c]:fmul.s t6, t5, t4, dyn
	-[0x8000d590]:csrrs a2, fcsr, zero
	-[0x8000d594]:sw t6, 784(fp)
Current Store : [0x8000d598] : sw a2, 788(fp) -- Store: [0x800144ec]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x748650 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d5cc]:fmul.s t6, t5, t4, dyn
	-[0x8000d5d0]:csrrs a2, fcsr, zero
	-[0x8000d5d4]:sw t6, 792(fp)
Current Store : [0x8000d5d8] : sw a2, 796(fp) -- Store: [0x800144f4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x748650 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d60c]:fmul.s t6, t5, t4, dyn
	-[0x8000d610]:csrrs a2, fcsr, zero
	-[0x8000d614]:sw t6, 800(fp)
Current Store : [0x8000d618] : sw a2, 804(fp) -- Store: [0x800144fc]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x748650 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d64c]:fmul.s t6, t5, t4, dyn
	-[0x8000d650]:csrrs a2, fcsr, zero
	-[0x8000d654]:sw t6, 808(fp)
Current Store : [0x8000d658] : sw a2, 812(fp) -- Store: [0x80014504]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x748650 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d68c]:fmul.s t6, t5, t4, dyn
	-[0x8000d690]:csrrs a2, fcsr, zero
	-[0x8000d694]:sw t6, 816(fp)
Current Store : [0x8000d698] : sw a2, 820(fp) -- Store: [0x8001450c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x748650 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d6cc]:fmul.s t6, t5, t4, dyn
	-[0x8000d6d0]:csrrs a2, fcsr, zero
	-[0x8000d6d4]:sw t6, 824(fp)
Current Store : [0x8000d6d8] : sw a2, 828(fp) -- Store: [0x80014514]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2fa24d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d70c]:fmul.s t6, t5, t4, dyn
	-[0x8000d710]:csrrs a2, fcsr, zero
	-[0x8000d714]:sw t6, 832(fp)
Current Store : [0x8000d718] : sw a2, 836(fp) -- Store: [0x8001451c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2fa24d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d74c]:fmul.s t6, t5, t4, dyn
	-[0x8000d750]:csrrs a2, fcsr, zero
	-[0x8000d754]:sw t6, 840(fp)
Current Store : [0x8000d758] : sw a2, 844(fp) -- Store: [0x80014524]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2fa24d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d78c]:fmul.s t6, t5, t4, dyn
	-[0x8000d790]:csrrs a2, fcsr, zero
	-[0x8000d794]:sw t6, 848(fp)
Current Store : [0x8000d798] : sw a2, 852(fp) -- Store: [0x8001452c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2fa24d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d7cc]:fmul.s t6, t5, t4, dyn
	-[0x8000d7d0]:csrrs a2, fcsr, zero
	-[0x8000d7d4]:sw t6, 856(fp)
Current Store : [0x8000d7d8] : sw a2, 860(fp) -- Store: [0x80014534]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2fa24d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d80c]:fmul.s t6, t5, t4, dyn
	-[0x8000d810]:csrrs a2, fcsr, zero
	-[0x8000d814]:sw t6, 864(fp)
Current Store : [0x8000d818] : sw a2, 868(fp) -- Store: [0x8001453c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x29bb3f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d84c]:fmul.s t6, t5, t4, dyn
	-[0x8000d850]:csrrs a2, fcsr, zero
	-[0x8000d854]:sw t6, 872(fp)
Current Store : [0x8000d858] : sw a2, 876(fp) -- Store: [0x80014544]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x29bb3f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d88c]:fmul.s t6, t5, t4, dyn
	-[0x8000d890]:csrrs a2, fcsr, zero
	-[0x8000d894]:sw t6, 880(fp)
Current Store : [0x8000d898] : sw a2, 884(fp) -- Store: [0x8001454c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x29bb3f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d8cc]:fmul.s t6, t5, t4, dyn
	-[0x8000d8d0]:csrrs a2, fcsr, zero
	-[0x8000d8d4]:sw t6, 888(fp)
Current Store : [0x8000d8d8] : sw a2, 892(fp) -- Store: [0x80014554]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x29bb3f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d90c]:fmul.s t6, t5, t4, dyn
	-[0x8000d910]:csrrs a2, fcsr, zero
	-[0x8000d914]:sw t6, 896(fp)
Current Store : [0x8000d918] : sw a2, 900(fp) -- Store: [0x8001455c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x29bb3f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d94c]:fmul.s t6, t5, t4, dyn
	-[0x8000d950]:csrrs a2, fcsr, zero
	-[0x8000d954]:sw t6, 904(fp)
Current Store : [0x8000d958] : sw a2, 908(fp) -- Store: [0x80014564]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2bc20f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d98c]:fmul.s t6, t5, t4, dyn
	-[0x8000d990]:csrrs a2, fcsr, zero
	-[0x8000d994]:sw t6, 912(fp)
Current Store : [0x8000d998] : sw a2, 916(fp) -- Store: [0x8001456c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2bc20f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000d9cc]:fmul.s t6, t5, t4, dyn
	-[0x8000d9d0]:csrrs a2, fcsr, zero
	-[0x8000d9d4]:sw t6, 920(fp)
Current Store : [0x8000d9d8] : sw a2, 924(fp) -- Store: [0x80014574]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2bc20f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000da0c]:fmul.s t6, t5, t4, dyn
	-[0x8000da10]:csrrs a2, fcsr, zero
	-[0x8000da14]:sw t6, 928(fp)
Current Store : [0x8000da18] : sw a2, 932(fp) -- Store: [0x8001457c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2bc20f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000da4c]:fmul.s t6, t5, t4, dyn
	-[0x8000da50]:csrrs a2, fcsr, zero
	-[0x8000da54]:sw t6, 936(fp)
Current Store : [0x8000da58] : sw a2, 940(fp) -- Store: [0x80014584]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2bc20f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000da8c]:fmul.s t6, t5, t4, dyn
	-[0x8000da90]:csrrs a2, fcsr, zero
	-[0x8000da94]:sw t6, 944(fp)
Current Store : [0x8000da98] : sw a2, 948(fp) -- Store: [0x8001458c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x29ba05 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000dacc]:fmul.s t6, t5, t4, dyn
	-[0x8000dad0]:csrrs a2, fcsr, zero
	-[0x8000dad4]:sw t6, 952(fp)
Current Store : [0x8000dad8] : sw a2, 956(fp) -- Store: [0x80014594]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x29ba05 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000db0c]:fmul.s t6, t5, t4, dyn
	-[0x8000db10]:csrrs a2, fcsr, zero
	-[0x8000db14]:sw t6, 960(fp)
Current Store : [0x8000db18] : sw a2, 964(fp) -- Store: [0x8001459c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x29ba05 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000db4c]:fmul.s t6, t5, t4, dyn
	-[0x8000db50]:csrrs a2, fcsr, zero
	-[0x8000db54]:sw t6, 968(fp)
Current Store : [0x8000db58] : sw a2, 972(fp) -- Store: [0x800145a4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x29ba05 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000db8c]:fmul.s t6, t5, t4, dyn
	-[0x8000db90]:csrrs a2, fcsr, zero
	-[0x8000db94]:sw t6, 976(fp)
Current Store : [0x8000db98] : sw a2, 980(fp) -- Store: [0x800145ac]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x29ba05 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000dbcc]:fmul.s t6, t5, t4, dyn
	-[0x8000dbd0]:csrrs a2, fcsr, zero
	-[0x8000dbd4]:sw t6, 984(fp)
Current Store : [0x8000dbd8] : sw a2, 988(fp) -- Store: [0x800145b4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x327dc5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000dc0c]:fmul.s t6, t5, t4, dyn
	-[0x8000dc10]:csrrs a2, fcsr, zero
	-[0x8000dc14]:sw t6, 992(fp)
Current Store : [0x8000dc18] : sw a2, 996(fp) -- Store: [0x800145bc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x327dc5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000dc44]:fmul.s t6, t5, t4, dyn
	-[0x8000dc48]:csrrs a2, fcsr, zero
	-[0x8000dc4c]:sw t6, 1000(fp)
Current Store : [0x8000dc50] : sw a2, 1004(fp) -- Store: [0x800145c4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x327dc5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000dc7c]:fmul.s t6, t5, t4, dyn
	-[0x8000dc80]:csrrs a2, fcsr, zero
	-[0x8000dc84]:sw t6, 1008(fp)
Current Store : [0x8000dc88] : sw a2, 1012(fp) -- Store: [0x800145cc]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x327dc5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000dcb4]:fmul.s t6, t5, t4, dyn
	-[0x8000dcb8]:csrrs a2, fcsr, zero
	-[0x8000dcbc]:sw t6, 1016(fp)
Current Store : [0x8000dcc0] : sw a2, 1020(fp) -- Store: [0x800145d4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x327dc5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000dcf4]:fmul.s t6, t5, t4, dyn
	-[0x8000dcf8]:csrrs a2, fcsr, zero
	-[0x8000dcfc]:sw t6, 0(fp)
Current Store : [0x8000dd00] : sw a2, 4(fp) -- Store: [0x800145dc]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x18ea60 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000dd2c]:fmul.s t6, t5, t4, dyn
	-[0x8000dd30]:csrrs a2, fcsr, zero
	-[0x8000dd34]:sw t6, 8(fp)
Current Store : [0x8000dd38] : sw a2, 12(fp) -- Store: [0x800145e4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x18ea60 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000dd64]:fmul.s t6, t5, t4, dyn
	-[0x8000dd68]:csrrs a2, fcsr, zero
	-[0x8000dd6c]:sw t6, 16(fp)
Current Store : [0x8000dd70] : sw a2, 20(fp) -- Store: [0x800145ec]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x18ea60 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000dd9c]:fmul.s t6, t5, t4, dyn
	-[0x8000dda0]:csrrs a2, fcsr, zero
	-[0x8000dda4]:sw t6, 24(fp)
Current Store : [0x8000dda8] : sw a2, 28(fp) -- Store: [0x800145f4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x18ea60 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ddd4]:fmul.s t6, t5, t4, dyn
	-[0x8000ddd8]:csrrs a2, fcsr, zero
	-[0x8000dddc]:sw t6, 32(fp)
Current Store : [0x8000dde0] : sw a2, 36(fp) -- Store: [0x800145fc]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x18ea60 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000de0c]:fmul.s t6, t5, t4, dyn
	-[0x8000de10]:csrrs a2, fcsr, zero
	-[0x8000de14]:sw t6, 40(fp)
Current Store : [0x8000de18] : sw a2, 44(fp) -- Store: [0x80014604]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x11924c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000de44]:fmul.s t6, t5, t4, dyn
	-[0x8000de48]:csrrs a2, fcsr, zero
	-[0x8000de4c]:sw t6, 48(fp)
Current Store : [0x8000de50] : sw a2, 52(fp) -- Store: [0x8001460c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x11924c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000de7c]:fmul.s t6, t5, t4, dyn
	-[0x8000de80]:csrrs a2, fcsr, zero
	-[0x8000de84]:sw t6, 56(fp)
Current Store : [0x8000de88] : sw a2, 60(fp) -- Store: [0x80014614]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x11924c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000deb4]:fmul.s t6, t5, t4, dyn
	-[0x8000deb8]:csrrs a2, fcsr, zero
	-[0x8000debc]:sw t6, 64(fp)
Current Store : [0x8000dec0] : sw a2, 68(fp) -- Store: [0x8001461c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x11924c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000deec]:fmul.s t6, t5, t4, dyn
	-[0x8000def0]:csrrs a2, fcsr, zero
	-[0x8000def4]:sw t6, 72(fp)
Current Store : [0x8000def8] : sw a2, 76(fp) -- Store: [0x80014624]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x11924c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000df24]:fmul.s t6, t5, t4, dyn
	-[0x8000df28]:csrrs a2, fcsr, zero
	-[0x8000df2c]:sw t6, 80(fp)
Current Store : [0x8000df30] : sw a2, 84(fp) -- Store: [0x8001462c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x0f88e2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000df5c]:fmul.s t6, t5, t4, dyn
	-[0x8000df60]:csrrs a2, fcsr, zero
	-[0x8000df64]:sw t6, 88(fp)
Current Store : [0x8000df68] : sw a2, 92(fp) -- Store: [0x80014634]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x0f88e2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000df94]:fmul.s t6, t5, t4, dyn
	-[0x8000df98]:csrrs a2, fcsr, zero
	-[0x8000df9c]:sw t6, 96(fp)
Current Store : [0x8000dfa0] : sw a2, 100(fp) -- Store: [0x8001463c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x0f88e2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000dfcc]:fmul.s t6, t5, t4, dyn
	-[0x8000dfd0]:csrrs a2, fcsr, zero
	-[0x8000dfd4]:sw t6, 104(fp)
Current Store : [0x8000dfd8] : sw a2, 108(fp) -- Store: [0x80014644]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x0f88e2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e004]:fmul.s t6, t5, t4, dyn
	-[0x8000e008]:csrrs a2, fcsr, zero
	-[0x8000e00c]:sw t6, 112(fp)
Current Store : [0x8000e010] : sw a2, 116(fp) -- Store: [0x8001464c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x0f88e2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e03c]:fmul.s t6, t5, t4, dyn
	-[0x8000e040]:csrrs a2, fcsr, zero
	-[0x8000e044]:sw t6, 120(fp)
Current Store : [0x8000e048] : sw a2, 124(fp) -- Store: [0x80014654]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x18a8a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e074]:fmul.s t6, t5, t4, dyn
	-[0x8000e078]:csrrs a2, fcsr, zero
	-[0x8000e07c]:sw t6, 128(fp)
Current Store : [0x8000e080] : sw a2, 132(fp) -- Store: [0x8001465c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x18a8a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e0ac]:fmul.s t6, t5, t4, dyn
	-[0x8000e0b0]:csrrs a2, fcsr, zero
	-[0x8000e0b4]:sw t6, 136(fp)
Current Store : [0x8000e0b8] : sw a2, 140(fp) -- Store: [0x80014664]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x18a8a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e0e4]:fmul.s t6, t5, t4, dyn
	-[0x8000e0e8]:csrrs a2, fcsr, zero
	-[0x8000e0ec]:sw t6, 144(fp)
Current Store : [0x8000e0f0] : sw a2, 148(fp) -- Store: [0x8001466c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x18a8a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e11c]:fmul.s t6, t5, t4, dyn
	-[0x8000e120]:csrrs a2, fcsr, zero
	-[0x8000e124]:sw t6, 152(fp)
Current Store : [0x8000e128] : sw a2, 156(fp) -- Store: [0x80014674]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x18a8a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e154]:fmul.s t6, t5, t4, dyn
	-[0x8000e158]:csrrs a2, fcsr, zero
	-[0x8000e15c]:sw t6, 160(fp)
Current Store : [0x8000e160] : sw a2, 164(fp) -- Store: [0x8001467c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1b3d2d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e18c]:fmul.s t6, t5, t4, dyn
	-[0x8000e190]:csrrs a2, fcsr, zero
	-[0x8000e194]:sw t6, 168(fp)
Current Store : [0x8000e198] : sw a2, 172(fp) -- Store: [0x80014684]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1b3d2d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e1c4]:fmul.s t6, t5, t4, dyn
	-[0x8000e1c8]:csrrs a2, fcsr, zero
	-[0x8000e1cc]:sw t6, 176(fp)
Current Store : [0x8000e1d0] : sw a2, 180(fp) -- Store: [0x8001468c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1b3d2d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e1fc]:fmul.s t6, t5, t4, dyn
	-[0x8000e200]:csrrs a2, fcsr, zero
	-[0x8000e204]:sw t6, 184(fp)
Current Store : [0x8000e208] : sw a2, 188(fp) -- Store: [0x80014694]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1b3d2d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e234]:fmul.s t6, t5, t4, dyn
	-[0x8000e238]:csrrs a2, fcsr, zero
	-[0x8000e23c]:sw t6, 192(fp)
Current Store : [0x8000e240] : sw a2, 196(fp) -- Store: [0x8001469c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1b3d2d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e26c]:fmul.s t6, t5, t4, dyn
	-[0x8000e270]:csrrs a2, fcsr, zero
	-[0x8000e274]:sw t6, 200(fp)
Current Store : [0x8000e278] : sw a2, 204(fp) -- Store: [0x800146a4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x19b32b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e2a4]:fmul.s t6, t5, t4, dyn
	-[0x8000e2a8]:csrrs a2, fcsr, zero
	-[0x8000e2ac]:sw t6, 208(fp)
Current Store : [0x8000e2b0] : sw a2, 212(fp) -- Store: [0x800146ac]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x19b32b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e2dc]:fmul.s t6, t5, t4, dyn
	-[0x8000e2e0]:csrrs a2, fcsr, zero
	-[0x8000e2e4]:sw t6, 216(fp)
Current Store : [0x8000e2e8] : sw a2, 220(fp) -- Store: [0x800146b4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x19b32b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e314]:fmul.s t6, t5, t4, dyn
	-[0x8000e318]:csrrs a2, fcsr, zero
	-[0x8000e31c]:sw t6, 224(fp)
Current Store : [0x8000e320] : sw a2, 228(fp) -- Store: [0x800146bc]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x19b32b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e34c]:fmul.s t6, t5, t4, dyn
	-[0x8000e350]:csrrs a2, fcsr, zero
	-[0x8000e354]:sw t6, 232(fp)
Current Store : [0x8000e358] : sw a2, 236(fp) -- Store: [0x800146c4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x19b32b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e384]:fmul.s t6, t5, t4, dyn
	-[0x8000e388]:csrrs a2, fcsr, zero
	-[0x8000e38c]:sw t6, 240(fp)
Current Store : [0x8000e390] : sw a2, 244(fp) -- Store: [0x800146cc]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x036cdc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e3bc]:fmul.s t6, t5, t4, dyn
	-[0x8000e3c0]:csrrs a2, fcsr, zero
	-[0x8000e3c4]:sw t6, 248(fp)
Current Store : [0x8000e3c8] : sw a2, 252(fp) -- Store: [0x800146d4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x036cdc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e3f4]:fmul.s t6, t5, t4, dyn
	-[0x8000e3f8]:csrrs a2, fcsr, zero
	-[0x8000e3fc]:sw t6, 256(fp)
Current Store : [0x8000e400] : sw a2, 260(fp) -- Store: [0x800146dc]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x036cdc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e42c]:fmul.s t6, t5, t4, dyn
	-[0x8000e430]:csrrs a2, fcsr, zero
	-[0x8000e434]:sw t6, 264(fp)
Current Store : [0x8000e438] : sw a2, 268(fp) -- Store: [0x800146e4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x036cdc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e464]:fmul.s t6, t5, t4, dyn
	-[0x8000e468]:csrrs a2, fcsr, zero
	-[0x8000e46c]:sw t6, 272(fp)
Current Store : [0x8000e470] : sw a2, 276(fp) -- Store: [0x800146ec]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x036cdc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e49c]:fmul.s t6, t5, t4, dyn
	-[0x8000e4a0]:csrrs a2, fcsr, zero
	-[0x8000e4a4]:sw t6, 280(fp)
Current Store : [0x8000e4a8] : sw a2, 284(fp) -- Store: [0x800146f4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x31c71b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e4d4]:fmul.s t6, t5, t4, dyn
	-[0x8000e4d8]:csrrs a2, fcsr, zero
	-[0x8000e4dc]:sw t6, 288(fp)
Current Store : [0x8000e4e0] : sw a2, 292(fp) -- Store: [0x800146fc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x31c71b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e50c]:fmul.s t6, t5, t4, dyn
	-[0x8000e510]:csrrs a2, fcsr, zero
	-[0x8000e514]:sw t6, 296(fp)
Current Store : [0x8000e518] : sw a2, 300(fp) -- Store: [0x80014704]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x31c71b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e544]:fmul.s t6, t5, t4, dyn
	-[0x8000e548]:csrrs a2, fcsr, zero
	-[0x8000e54c]:sw t6, 304(fp)
Current Store : [0x8000e550] : sw a2, 308(fp) -- Store: [0x8001470c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x31c71b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e57c]:fmul.s t6, t5, t4, dyn
	-[0x8000e580]:csrrs a2, fcsr, zero
	-[0x8000e584]:sw t6, 312(fp)
Current Store : [0x8000e588] : sw a2, 316(fp) -- Store: [0x80014714]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x31c71b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e5b4]:fmul.s t6, t5, t4, dyn
	-[0x8000e5b8]:csrrs a2, fcsr, zero
	-[0x8000e5bc]:sw t6, 320(fp)
Current Store : [0x8000e5c0] : sw a2, 324(fp) -- Store: [0x8001471c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x047a0e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e5ec]:fmul.s t6, t5, t4, dyn
	-[0x8000e5f0]:csrrs a2, fcsr, zero
	-[0x8000e5f4]:sw t6, 328(fp)
Current Store : [0x8000e5f8] : sw a2, 332(fp) -- Store: [0x80014724]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x047a0e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e624]:fmul.s t6, t5, t4, dyn
	-[0x8000e628]:csrrs a2, fcsr, zero
	-[0x8000e62c]:sw t6, 336(fp)
Current Store : [0x8000e630] : sw a2, 340(fp) -- Store: [0x8001472c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x047a0e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e65c]:fmul.s t6, t5, t4, dyn
	-[0x8000e660]:csrrs a2, fcsr, zero
	-[0x8000e664]:sw t6, 344(fp)
Current Store : [0x8000e668] : sw a2, 348(fp) -- Store: [0x80014734]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x047a0e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e694]:fmul.s t6, t5, t4, dyn
	-[0x8000e698]:csrrs a2, fcsr, zero
	-[0x8000e69c]:sw t6, 352(fp)
Current Store : [0x8000e6a0] : sw a2, 356(fp) -- Store: [0x8001473c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x047a0e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e6cc]:fmul.s t6, t5, t4, dyn
	-[0x8000e6d0]:csrrs a2, fcsr, zero
	-[0x8000e6d4]:sw t6, 360(fp)
Current Store : [0x8000e6d8] : sw a2, 364(fp) -- Store: [0x80014744]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0e924d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e704]:fmul.s t6, t5, t4, dyn
	-[0x8000e708]:csrrs a2, fcsr, zero
	-[0x8000e70c]:sw t6, 368(fp)
Current Store : [0x8000e710] : sw a2, 372(fp) -- Store: [0x8001474c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0e924d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e73c]:fmul.s t6, t5, t4, dyn
	-[0x8000e740]:csrrs a2, fcsr, zero
	-[0x8000e744]:sw t6, 376(fp)
Current Store : [0x8000e748] : sw a2, 380(fp) -- Store: [0x80014754]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0e924d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e774]:fmul.s t6, t5, t4, dyn
	-[0x8000e778]:csrrs a2, fcsr, zero
	-[0x8000e77c]:sw t6, 384(fp)
Current Store : [0x8000e780] : sw a2, 388(fp) -- Store: [0x8001475c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0e924d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e7ac]:fmul.s t6, t5, t4, dyn
	-[0x8000e7b0]:csrrs a2, fcsr, zero
	-[0x8000e7b4]:sw t6, 392(fp)
Current Store : [0x8000e7b8] : sw a2, 396(fp) -- Store: [0x80014764]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0e924d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e7e4]:fmul.s t6, t5, t4, dyn
	-[0x8000e7e8]:csrrs a2, fcsr, zero
	-[0x8000e7ec]:sw t6, 400(fp)
Current Store : [0x8000e7f0] : sw a2, 404(fp) -- Store: [0x8001476c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x26e854 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e81c]:fmul.s t6, t5, t4, dyn
	-[0x8000e820]:csrrs a2, fcsr, zero
	-[0x8000e824]:sw t6, 408(fp)
Current Store : [0x8000e828] : sw a2, 412(fp) -- Store: [0x80014774]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x26e854 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e854]:fmul.s t6, t5, t4, dyn
	-[0x8000e858]:csrrs a2, fcsr, zero
	-[0x8000e85c]:sw t6, 416(fp)
Current Store : [0x8000e860] : sw a2, 420(fp) -- Store: [0x8001477c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x26e854 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e88c]:fmul.s t6, t5, t4, dyn
	-[0x8000e890]:csrrs a2, fcsr, zero
	-[0x8000e894]:sw t6, 424(fp)
Current Store : [0x8000e898] : sw a2, 428(fp) -- Store: [0x80014784]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x26e854 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e8c4]:fmul.s t6, t5, t4, dyn
	-[0x8000e8c8]:csrrs a2, fcsr, zero
	-[0x8000e8cc]:sw t6, 432(fp)
Current Store : [0x8000e8d0] : sw a2, 436(fp) -- Store: [0x8001478c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x26e854 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e8fc]:fmul.s t6, t5, t4, dyn
	-[0x8000e900]:csrrs a2, fcsr, zero
	-[0x8000e904]:sw t6, 440(fp)
Current Store : [0x8000e908] : sw a2, 444(fp) -- Store: [0x80014794]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x10adb7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e934]:fmul.s t6, t5, t4, dyn
	-[0x8000e938]:csrrs a2, fcsr, zero
	-[0x8000e93c]:sw t6, 448(fp)
Current Store : [0x8000e940] : sw a2, 452(fp) -- Store: [0x8001479c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x10adb7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e96c]:fmul.s t6, t5, t4, dyn
	-[0x8000e970]:csrrs a2, fcsr, zero
	-[0x8000e974]:sw t6, 456(fp)
Current Store : [0x8000e978] : sw a2, 460(fp) -- Store: [0x800147a4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x10adb7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e9a4]:fmul.s t6, t5, t4, dyn
	-[0x8000e9a8]:csrrs a2, fcsr, zero
	-[0x8000e9ac]:sw t6, 464(fp)
Current Store : [0x8000e9b0] : sw a2, 468(fp) -- Store: [0x800147ac]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x10adb7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000e9dc]:fmul.s t6, t5, t4, dyn
	-[0x8000e9e0]:csrrs a2, fcsr, zero
	-[0x8000e9e4]:sw t6, 472(fp)
Current Store : [0x8000e9e8] : sw a2, 476(fp) -- Store: [0x800147b4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x10adb7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ea14]:fmul.s t6, t5, t4, dyn
	-[0x8000ea18]:csrrs a2, fcsr, zero
	-[0x8000ea1c]:sw t6, 480(fp)
Current Store : [0x8000ea20] : sw a2, 484(fp) -- Store: [0x800147bc]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x5ff410 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ea4c]:fmul.s t6, t5, t4, dyn
	-[0x8000ea50]:csrrs a2, fcsr, zero
	-[0x8000ea54]:sw t6, 488(fp)
Current Store : [0x8000ea58] : sw a2, 492(fp) -- Store: [0x800147c4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x5ff410 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ea84]:fmul.s t6, t5, t4, dyn
	-[0x8000ea88]:csrrs a2, fcsr, zero
	-[0x8000ea8c]:sw t6, 496(fp)
Current Store : [0x8000ea90] : sw a2, 500(fp) -- Store: [0x800147cc]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x5ff410 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000eabc]:fmul.s t6, t5, t4, dyn
	-[0x8000eac0]:csrrs a2, fcsr, zero
	-[0x8000eac4]:sw t6, 504(fp)
Current Store : [0x8000eac8] : sw a2, 508(fp) -- Store: [0x800147d4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x5ff410 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000eaf4]:fmul.s t6, t5, t4, dyn
	-[0x8000eaf8]:csrrs a2, fcsr, zero
	-[0x8000eafc]:sw t6, 512(fp)
Current Store : [0x8000eb00] : sw a2, 516(fp) -- Store: [0x800147dc]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x5ff410 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000eb2c]:fmul.s t6, t5, t4, dyn
	-[0x8000eb30]:csrrs a2, fcsr, zero
	-[0x8000eb34]:sw t6, 520(fp)
Current Store : [0x8000eb38] : sw a2, 524(fp) -- Store: [0x800147e4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00c42d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000eb64]:fmul.s t6, t5, t4, dyn
	-[0x8000eb68]:csrrs a2, fcsr, zero
	-[0x8000eb6c]:sw t6, 528(fp)
Current Store : [0x8000eb70] : sw a2, 532(fp) -- Store: [0x800147ec]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00c42d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000eb9c]:fmul.s t6, t5, t4, dyn
	-[0x8000eba0]:csrrs a2, fcsr, zero
	-[0x8000eba4]:sw t6, 536(fp)
Current Store : [0x8000eba8] : sw a2, 540(fp) -- Store: [0x800147f4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00c42d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ebd4]:fmul.s t6, t5, t4, dyn
	-[0x8000ebd8]:csrrs a2, fcsr, zero
	-[0x8000ebdc]:sw t6, 544(fp)
Current Store : [0x8000ebe0] : sw a2, 548(fp) -- Store: [0x800147fc]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00c42d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ec0c]:fmul.s t6, t5, t4, dyn
	-[0x8000ec10]:csrrs a2, fcsr, zero
	-[0x8000ec14]:sw t6, 552(fp)
Current Store : [0x8000ec18] : sw a2, 556(fp) -- Store: [0x80014804]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00c42d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ec44]:fmul.s t6, t5, t4, dyn
	-[0x8000ec48]:csrrs a2, fcsr, zero
	-[0x8000ec4c]:sw t6, 560(fp)
Current Store : [0x8000ec50] : sw a2, 564(fp) -- Store: [0x8001480c]:0x00000080




Last Coverpoint : ['mnemonic : fmul.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x378efe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ec7c]:fmul.s t6, t5, t4, dyn
	-[0x8000ec80]:csrrs a2, fcsr, zero
	-[0x8000ec84]:sw t6, 568(fp)
Current Store : [0x8000ec88] : sw a2, 572(fp) -- Store: [0x80014814]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x378efe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ecb4]:fmul.s t6, t5, t4, dyn
	-[0x8000ecb8]:csrrs a2, fcsr, zero
	-[0x8000ecbc]:sw t6, 576(fp)
Current Store : [0x8000ecc0] : sw a2, 580(fp) -- Store: [0x8001481c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x378efe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ecec]:fmul.s t6, t5, t4, dyn
	-[0x8000ecf0]:csrrs a2, fcsr, zero
	-[0x8000ecf4]:sw t6, 584(fp)
Current Store : [0x8000ecf8] : sw a2, 588(fp) -- Store: [0x80014824]:0x00000060




Last Coverpoint : ['mnemonic : fmul.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x378efe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ed24]:fmul.s t6, t5, t4, dyn
	-[0x8000ed28]:csrrs a2, fcsr, zero
	-[0x8000ed2c]:sw t6, 592(fp)
Current Store : [0x8000ed30] : sw a2, 596(fp) -- Store: [0x8001482c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x25608b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ed5c]:fmul.s t6, t5, t4, dyn
	-[0x8000ed60]:csrrs a2, fcsr, zero
	-[0x8000ed64]:sw t6, 600(fp)
Current Store : [0x8000ed68] : sw a2, 604(fp) -- Store: [0x80014834]:0x00000020




Last Coverpoint : ['mnemonic : fmul.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x25608b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ed94]:fmul.s t6, t5, t4, dyn
	-[0x8000ed98]:csrrs a2, fcsr, zero
	-[0x8000ed9c]:sw t6, 608(fp)
Current Store : [0x8000eda0] : sw a2, 612(fp) -- Store: [0x8001483c]:0x00000060




Last Coverpoint : ['mnemonic : fmul.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x0ad17d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000edcc]:fmul.s t6, t5, t4, dyn
	-[0x8000edd0]:csrrs a2, fcsr, zero
	-[0x8000edd4]:sw t6, 616(fp)
Current Store : [0x8000edd8] : sw a2, 620(fp) -- Store: [0x80014844]:0x00000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                                coverpoints                                                                                                                 |                                                      code                                                       |
|---:|--------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|
|   1|[0x80012510]<br>0x00000000|- mnemonic : fmul.s<br> - rs1 : x30<br> - rs2 : x31<br> - rd : x31<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x378efe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x80000124]:fmul.s t6, t5, t6, dyn<br> [0x80000128]:csrrs tp, fcsr, zero<br> [0x8000012c]:sw t6, 0(ra)<br>      |
|   2|[0x80012518]<br>0x00000000|- rs1 : x31<br> - rs2 : x29<br> - rd : x30<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x378efe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br> |[0x80000144]:fmul.s t5, t6, t4, dyn<br> [0x80000148]:csrrs tp, fcsr, zero<br> [0x8000014c]:sw t5, 8(ra)<br>      |
|   3|[0x80012520]<br>0x7F7FFFFF|- rs1 : x28<br> - rs2 : x28<br> - rd : x28<br> - rs1 == rs2 == rd<br>                                                                                                                                                                       |[0x80000164]:fmul.s t3, t3, t3, dyn<br> [0x80000168]:csrrs tp, fcsr, zero<br> [0x8000016c]:sw t3, 16(ra)<br>     |
|   4|[0x80012528]<br>0x7F800000|- rs1 : x27<br> - rs2 : x27<br> - rd : x29<br> - rs1 == rs2 != rd<br>                                                                                                                                                                       |[0x80000184]:fmul.s t4, s11, s11, dyn<br> [0x80000188]:csrrs tp, fcsr, zero<br> [0x8000018c]:sw t4, 24(ra)<br>   |
|   5|[0x80012530]<br>0x00000000|- rs1 : x26<br> - rs2 : x30<br> - rd : x26<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x378efe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                        |[0x800001a4]:fmul.s s10, s10, t5, dyn<br> [0x800001a8]:csrrs tp, fcsr, zero<br> [0x800001ac]:sw s10, 32(ra)<br>  |
|   6|[0x80012538]<br>0x00000000|- rs1 : x29<br> - rs2 : x26<br> - rd : x27<br> - fs1 == 0 and fe1 == 0xfa and fm1 == 0x2db9cd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x800001c4]:fmul.s s11, t4, s10, dyn<br> [0x800001c8]:csrrs tp, fcsr, zero<br> [0x800001cc]:sw s11, 40(ra)<br>  |
|   7|[0x80012540]<br>0x00000000|- rs1 : x24<br> - rs2 : x23<br> - rd : x25<br> - fs1 == 0 and fe1 == 0xfa and fm1 == 0x2db9cd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                               |[0x800001e4]:fmul.s s9, s8, s7, dyn<br> [0x800001e8]:csrrs tp, fcsr, zero<br> [0x800001ec]:sw s9, 48(ra)<br>     |
|   8|[0x80012548]<br>0x00000000|- rs1 : x23<br> - rs2 : x25<br> - rd : x24<br> - fs1 == 0 and fe1 == 0xfa and fm1 == 0x2db9cd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                               |[0x80000204]:fmul.s s8, s7, s9, dyn<br> [0x80000208]:csrrs tp, fcsr, zero<br> [0x8000020c]:sw s8, 56(ra)<br>     |
|   9|[0x80012550]<br>0x00000000|- rs1 : x25<br> - rs2 : x24<br> - rd : x23<br> - fs1 == 0 and fe1 == 0xfa and fm1 == 0x2db9cd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                               |[0x80000224]:fmul.s s7, s9, s8, dyn<br> [0x80000228]:csrrs tp, fcsr, zero<br> [0x8000022c]:sw s7, 64(ra)<br>     |
|  10|[0x80012558]<br>0x00000000|- rs1 : x21<br> - rs2 : x20<br> - rd : x22<br> - fs1 == 0 and fe1 == 0xfa and fm1 == 0x2db9cd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                               |[0x80000244]:fmul.s s6, s5, s4, dyn<br> [0x80000248]:csrrs tp, fcsr, zero<br> [0x8000024c]:sw s6, 72(ra)<br>     |
|  11|[0x80012560]<br>0x00000000|- rs1 : x20<br> - rs2 : x22<br> - rd : x21<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x206a70 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000264]:fmul.s s5, s4, s6, dyn<br> [0x80000268]:csrrs tp, fcsr, zero<br> [0x8000026c]:sw s5, 80(ra)<br>     |
|  12|[0x80012568]<br>0x00000000|- rs1 : x22<br> - rs2 : x21<br> - rd : x20<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x206a70 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                               |[0x80000284]:fmul.s s4, s6, s5, dyn<br> [0x80000288]:csrrs tp, fcsr, zero<br> [0x8000028c]:sw s4, 88(ra)<br>     |
|  13|[0x80012570]<br>0x00000000|- rs1 : x18<br> - rs2 : x17<br> - rd : x19<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x206a70 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                               |[0x800002a4]:fmul.s s3, s2, a7, dyn<br> [0x800002a8]:csrrs tp, fcsr, zero<br> [0x800002ac]:sw s3, 96(ra)<br>     |
|  14|[0x80012578]<br>0x00000000|- rs1 : x17<br> - rs2 : x19<br> - rd : x18<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x206a70 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                               |[0x800002c4]:fmul.s s2, a7, s3, dyn<br> [0x800002c8]:csrrs tp, fcsr, zero<br> [0x800002cc]:sw s2, 104(ra)<br>    |
|  15|[0x80012580]<br>0x00000000|- rs1 : x19<br> - rs2 : x18<br> - rd : x17<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x206a70 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                               |[0x800002e4]:fmul.s a7, s3, s2, dyn<br> [0x800002e8]:csrrs tp, fcsr, zero<br> [0x800002ec]:sw a7, 112(ra)<br>    |
|  16|[0x80012588]<br>0x00000000|- rs1 : x15<br> - rs2 : x14<br> - rd : x16<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x009696 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000304]:fmul.s a6, a5, a4, dyn<br> [0x80000308]:csrrs tp, fcsr, zero<br> [0x8000030c]:sw a6, 120(ra)<br>    |
|  17|[0x80012590]<br>0x00000000|- rs1 : x14<br> - rs2 : x16<br> - rd : x15<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x009696 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                               |[0x80000324]:fmul.s a5, a4, a6, dyn<br> [0x80000328]:csrrs tp, fcsr, zero<br> [0x8000032c]:sw a5, 128(ra)<br>    |
|  18|[0x80012598]<br>0x00000000|- rs1 : x16<br> - rs2 : x15<br> - rd : x14<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x009696 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                               |[0x80000344]:fmul.s a4, a6, a5, dyn<br> [0x80000348]:csrrs tp, fcsr, zero<br> [0x8000034c]:sw a4, 136(ra)<br>    |
|  19|[0x800125a0]<br>0x00000000|- rs1 : x12<br> - rs2 : x11<br> - rd : x13<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x009696 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                               |[0x80000364]:fmul.s a3, a2, a1, dyn<br> [0x80000368]:csrrs tp, fcsr, zero<br> [0x8000036c]:sw a3, 144(ra)<br>    |
|  20|[0x800125a8]<br>0x00000000|- rs1 : x11<br> - rs2 : x13<br> - rd : x12<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x009696 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                               |[0x80000384]:fmul.s a2, a1, a3, dyn<br> [0x80000388]:csrrs tp, fcsr, zero<br> [0x8000038c]:sw a2, 152(ra)<br>    |
|  21|[0x800125b0]<br>0x00000000|- rs1 : x13<br> - rs2 : x12<br> - rd : x11<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x68aebb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x800003a4]:fmul.s a1, a3, a2, dyn<br> [0x800003a8]:csrrs tp, fcsr, zero<br> [0x800003ac]:sw a1, 160(ra)<br>    |
|  22|[0x800125b8]<br>0x00000000|- rs1 : x9<br> - rs2 : x8<br> - rd : x10<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x68aebb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                 |[0x800003c4]:fmul.s a0, s1, fp, dyn<br> [0x800003c8]:csrrs tp, fcsr, zero<br> [0x800003cc]:sw a0, 168(ra)<br>    |
|  23|[0x800125c0]<br>0x00000000|- rs1 : x8<br> - rs2 : x10<br> - rd : x9<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x68aebb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                 |[0x800003ec]:fmul.s s1, fp, a0, dyn<br> [0x800003f0]:csrrs a2, fcsr, zero<br> [0x800003f4]:sw s1, 176(ra)<br>    |
|  24|[0x800125c8]<br>0x00000000|- rs1 : x10<br> - rs2 : x9<br> - rd : x8<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x68aebb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                 |[0x8000040c]:fmul.s fp, a0, s1, dyn<br> [0x80000410]:csrrs a2, fcsr, zero<br> [0x80000414]:sw fp, 184(ra)<br>    |
|  25|[0x800125d0]<br>0x00000000|- rs1 : x6<br> - rs2 : x5<br> - rd : x7<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x68aebb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                  |[0x8000042c]:fmul.s t2, t1, t0, dyn<br> [0x80000430]:csrrs a2, fcsr, zero<br> [0x80000434]:sw t2, 192(ra)<br>    |
|  26|[0x800125d8]<br>0x00000000|- rs1 : x5<br> - rs2 : x7<br> - rd : x6<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x5f2ead and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                   |[0x80000454]:fmul.s t1, t0, t2, dyn<br> [0x80000458]:csrrs a2, fcsr, zero<br> [0x8000045c]:sw t1, 0(fp)<br>      |
|  27|[0x800125e0]<br>0x00000000|- rs1 : x7<br> - rs2 : x6<br> - rd : x5<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x5f2ead and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                  |[0x80000474]:fmul.s t0, t2, t1, dyn<br> [0x80000478]:csrrs a2, fcsr, zero<br> [0x8000047c]:sw t0, 8(fp)<br>      |
|  28|[0x800125e8]<br>0x00000000|- rs1 : x3<br> - rs2 : x2<br> - rd : x4<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x5f2ead and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                  |[0x80000494]:fmul.s tp, gp, sp, dyn<br> [0x80000498]:csrrs a2, fcsr, zero<br> [0x8000049c]:sw tp, 16(fp)<br>     |
|  29|[0x800125f0]<br>0x00000000|- rs1 : x2<br> - rs2 : x4<br> - rd : x3<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x5f2ead and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                  |[0x800004b4]:fmul.s gp, sp, tp, dyn<br> [0x800004b8]:csrrs a2, fcsr, zero<br> [0x800004bc]:sw gp, 24(fp)<br>     |
|  30|[0x800125f8]<br>0x00000000|- rs1 : x4<br> - rs2 : x3<br> - rd : x2<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x5f2ead and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                  |[0x800004d4]:fmul.s sp, tp, gp, dyn<br> [0x800004d8]:csrrs a2, fcsr, zero<br> [0x800004dc]:sw sp, 32(fp)<br>     |
|  31|[0x80012600]<br>0x00000000|- rs1 : x1<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x25608b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                |[0x800004f4]:fmul.s t6, ra, t5, dyn<br> [0x800004f8]:csrrs a2, fcsr, zero<br> [0x800004fc]:sw t6, 40(fp)<br>     |
|  32|[0x80012608]<br>0x00000000|- rs1 : x0<br>                                                                                                                                                                                                                              |[0x80000514]:fmul.s t6, zero, t5, dyn<br> [0x80000518]:csrrs a2, fcsr, zero<br> [0x8000051c]:sw t6, 48(fp)<br>   |
|  33|[0x80012610]<br>0x00000000|- rs2 : x1<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x25608b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                               |[0x80000534]:fmul.s t6, t5, ra, dyn<br> [0x80000538]:csrrs a2, fcsr, zero<br> [0x8000053c]:sw t6, 56(fp)<br>     |
|  34|[0x80012618]<br>0x00000000|- rs2 : x0<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x25608b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                               |[0x80000554]:fmul.s t6, t5, zero, dyn<br> [0x80000558]:csrrs a2, fcsr, zero<br> [0x8000055c]:sw t6, 64(fp)<br>   |
|  35|[0x80012620]<br>0x00000000|- rd : x1<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x25608b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                |[0x80000574]:fmul.s ra, t6, t5, dyn<br> [0x80000578]:csrrs a2, fcsr, zero<br> [0x8000057c]:sw ra, 72(fp)<br>     |
|  36|[0x80012628]<br>0x00000000|- rd : x0<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x0ad17d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                 |[0x80000594]:fmul.s zero, t6, t5, dyn<br> [0x80000598]:csrrs a2, fcsr, zero<br> [0x8000059c]:sw zero, 80(fp)<br> |
|  37|[0x80012630]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0ad17d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800005b4]:fmul.s t6, t5, t4, dyn<br> [0x800005b8]:csrrs a2, fcsr, zero<br> [0x800005bc]:sw t6, 88(fp)<br>     |
|  38|[0x80012638]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0ad17d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800005d4]:fmul.s t6, t5, t4, dyn<br> [0x800005d8]:csrrs a2, fcsr, zero<br> [0x800005dc]:sw t6, 96(fp)<br>     |
|  39|[0x80012640]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0ad17d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800005f4]:fmul.s t6, t5, t4, dyn<br> [0x800005f8]:csrrs a2, fcsr, zero<br> [0x800005fc]:sw t6, 104(fp)<br>    |
|  40|[0x80012648]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0ad17d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000614]:fmul.s t6, t5, t4, dyn<br> [0x80000618]:csrrs a2, fcsr, zero<br> [0x8000061c]:sw t6, 112(fp)<br>    |
|  41|[0x80012650]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3648af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000634]:fmul.s t6, t5, t4, dyn<br> [0x80000638]:csrrs a2, fcsr, zero<br> [0x8000063c]:sw t6, 120(fp)<br>    |
|  42|[0x80012658]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3648af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000654]:fmul.s t6, t5, t4, dyn<br> [0x80000658]:csrrs a2, fcsr, zero<br> [0x8000065c]:sw t6, 128(fp)<br>    |
|  43|[0x80012660]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3648af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000674]:fmul.s t6, t5, t4, dyn<br> [0x80000678]:csrrs a2, fcsr, zero<br> [0x8000067c]:sw t6, 136(fp)<br>    |
|  44|[0x80012668]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3648af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000694]:fmul.s t6, t5, t4, dyn<br> [0x80000698]:csrrs a2, fcsr, zero<br> [0x8000069c]:sw t6, 144(fp)<br>    |
|  45|[0x80012670]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3648af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800006b4]:fmul.s t6, t5, t4, dyn<br> [0x800006b8]:csrrs a2, fcsr, zero<br> [0x800006bc]:sw t6, 152(fp)<br>    |
|  46|[0x80012678]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2ad2f1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800006d4]:fmul.s t6, t5, t4, dyn<br> [0x800006d8]:csrrs a2, fcsr, zero<br> [0x800006dc]:sw t6, 160(fp)<br>    |
|  47|[0x80012680]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2ad2f1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800006f4]:fmul.s t6, t5, t4, dyn<br> [0x800006f8]:csrrs a2, fcsr, zero<br> [0x800006fc]:sw t6, 168(fp)<br>    |
|  48|[0x80012688]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2ad2f1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000714]:fmul.s t6, t5, t4, dyn<br> [0x80000718]:csrrs a2, fcsr, zero<br> [0x8000071c]:sw t6, 176(fp)<br>    |
|  49|[0x80012690]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2ad2f1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000734]:fmul.s t6, t5, t4, dyn<br> [0x80000738]:csrrs a2, fcsr, zero<br> [0x8000073c]:sw t6, 184(fp)<br>    |
|  50|[0x80012698]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2ad2f1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000754]:fmul.s t6, t5, t4, dyn<br> [0x80000758]:csrrs a2, fcsr, zero<br> [0x8000075c]:sw t6, 192(fp)<br>    |
|  51|[0x800126a0]<br>0x80000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x5cb815 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000774]:fmul.s t6, t5, t4, dyn<br> [0x80000778]:csrrs a2, fcsr, zero<br> [0x8000077c]:sw t6, 200(fp)<br>    |
|  52|[0x800126a8]<br>0x80000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x5cb815 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000794]:fmul.s t6, t5, t4, dyn<br> [0x80000798]:csrrs a2, fcsr, zero<br> [0x8000079c]:sw t6, 208(fp)<br>    |
|  53|[0x800126b0]<br>0x80000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x5cb815 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800007b4]:fmul.s t6, t5, t4, dyn<br> [0x800007b8]:csrrs a2, fcsr, zero<br> [0x800007bc]:sw t6, 216(fp)<br>    |
|  54|[0x800126b8]<br>0x80000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x5cb815 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800007d4]:fmul.s t6, t5, t4, dyn<br> [0x800007d8]:csrrs a2, fcsr, zero<br> [0x800007dc]:sw t6, 224(fp)<br>    |
|  55|[0x800126c0]<br>0x80000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x5cb815 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800007f4]:fmul.s t6, t5, t4, dyn<br> [0x800007f8]:csrrs a2, fcsr, zero<br> [0x800007fc]:sw t6, 232(fp)<br>    |
|  56|[0x800126c8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3284ec and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000814]:fmul.s t6, t5, t4, dyn<br> [0x80000818]:csrrs a2, fcsr, zero<br> [0x8000081c]:sw t6, 240(fp)<br>    |
|  57|[0x800126d0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3284ec and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000834]:fmul.s t6, t5, t4, dyn<br> [0x80000838]:csrrs a2, fcsr, zero<br> [0x8000083c]:sw t6, 248(fp)<br>    |
|  58|[0x800126d8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3284ec and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000854]:fmul.s t6, t5, t4, dyn<br> [0x80000858]:csrrs a2, fcsr, zero<br> [0x8000085c]:sw t6, 256(fp)<br>    |
|  59|[0x800126e0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3284ec and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000874]:fmul.s t6, t5, t4, dyn<br> [0x80000878]:csrrs a2, fcsr, zero<br> [0x8000087c]:sw t6, 264(fp)<br>    |
|  60|[0x800126e8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3284ec and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000894]:fmul.s t6, t5, t4, dyn<br> [0x80000898]:csrrs a2, fcsr, zero<br> [0x8000089c]:sw t6, 272(fp)<br>    |
|  61|[0x800126f0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x11ecfc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800008b4]:fmul.s t6, t5, t4, dyn<br> [0x800008b8]:csrrs a2, fcsr, zero<br> [0x800008bc]:sw t6, 280(fp)<br>    |
|  62|[0x800126f8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x11ecfc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800008d4]:fmul.s t6, t5, t4, dyn<br> [0x800008d8]:csrrs a2, fcsr, zero<br> [0x800008dc]:sw t6, 288(fp)<br>    |
|  63|[0x80012700]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x11ecfc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800008f4]:fmul.s t6, t5, t4, dyn<br> [0x800008f8]:csrrs a2, fcsr, zero<br> [0x800008fc]:sw t6, 296(fp)<br>    |
|  64|[0x80012708]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x11ecfc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000914]:fmul.s t6, t5, t4, dyn<br> [0x80000918]:csrrs a2, fcsr, zero<br> [0x8000091c]:sw t6, 304(fp)<br>    |
|  65|[0x80012710]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x11ecfc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000934]:fmul.s t6, t5, t4, dyn<br> [0x80000938]:csrrs a2, fcsr, zero<br> [0x8000093c]:sw t6, 312(fp)<br>    |
|  66|[0x80012718]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x49e399 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000954]:fmul.s t6, t5, t4, dyn<br> [0x80000958]:csrrs a2, fcsr, zero<br> [0x8000095c]:sw t6, 320(fp)<br>    |
|  67|[0x80012720]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x49e399 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000974]:fmul.s t6, t5, t4, dyn<br> [0x80000978]:csrrs a2, fcsr, zero<br> [0x8000097c]:sw t6, 328(fp)<br>    |
|  68|[0x80012728]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x49e399 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000994]:fmul.s t6, t5, t4, dyn<br> [0x80000998]:csrrs a2, fcsr, zero<br> [0x8000099c]:sw t6, 336(fp)<br>    |
|  69|[0x80012730]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x49e399 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800009b4]:fmul.s t6, t5, t4, dyn<br> [0x800009b8]:csrrs a2, fcsr, zero<br> [0x800009bc]:sw t6, 344(fp)<br>    |
|  70|[0x80012738]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x49e399 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800009d4]:fmul.s t6, t5, t4, dyn<br> [0x800009d8]:csrrs a2, fcsr, zero<br> [0x800009dc]:sw t6, 352(fp)<br>    |
|  71|[0x80012740]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x3557bf and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800009f4]:fmul.s t6, t5, t4, dyn<br> [0x800009f8]:csrrs a2, fcsr, zero<br> [0x800009fc]:sw t6, 360(fp)<br>    |
|  72|[0x80012748]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x3557bf and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000a14]:fmul.s t6, t5, t4, dyn<br> [0x80000a18]:csrrs a2, fcsr, zero<br> [0x80000a1c]:sw t6, 368(fp)<br>    |
|  73|[0x80012750]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x3557bf and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000a34]:fmul.s t6, t5, t4, dyn<br> [0x80000a38]:csrrs a2, fcsr, zero<br> [0x80000a3c]:sw t6, 376(fp)<br>    |
|  74|[0x80012758]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x3557bf and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000a54]:fmul.s t6, t5, t4, dyn<br> [0x80000a58]:csrrs a2, fcsr, zero<br> [0x80000a5c]:sw t6, 384(fp)<br>    |
|  75|[0x80012760]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x3557bf and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000a74]:fmul.s t6, t5, t4, dyn<br> [0x80000a78]:csrrs a2, fcsr, zero<br> [0x80000a7c]:sw t6, 392(fp)<br>    |
|  76|[0x80012768]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x79b5b2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000a94]:fmul.s t6, t5, t4, dyn<br> [0x80000a98]:csrrs a2, fcsr, zero<br> [0x80000a9c]:sw t6, 400(fp)<br>    |
|  77|[0x80012770]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x79b5b2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000ab4]:fmul.s t6, t5, t4, dyn<br> [0x80000ab8]:csrrs a2, fcsr, zero<br> [0x80000abc]:sw t6, 408(fp)<br>    |
|  78|[0x80012778]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x79b5b2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000ad4]:fmul.s t6, t5, t4, dyn<br> [0x80000ad8]:csrrs a2, fcsr, zero<br> [0x80000adc]:sw t6, 416(fp)<br>    |
|  79|[0x80012780]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x79b5b2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000af4]:fmul.s t6, t5, t4, dyn<br> [0x80000af8]:csrrs a2, fcsr, zero<br> [0x80000afc]:sw t6, 424(fp)<br>    |
|  80|[0x80012788]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x79b5b2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000b14]:fmul.s t6, t5, t4, dyn<br> [0x80000b18]:csrrs a2, fcsr, zero<br> [0x80000b1c]:sw t6, 432(fp)<br>    |
|  81|[0x80012790]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x43ed0a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000b34]:fmul.s t6, t5, t4, dyn<br> [0x80000b38]:csrrs a2, fcsr, zero<br> [0x80000b3c]:sw t6, 440(fp)<br>    |
|  82|[0x80012798]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x43ed0a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000b54]:fmul.s t6, t5, t4, dyn<br> [0x80000b58]:csrrs a2, fcsr, zero<br> [0x80000b5c]:sw t6, 448(fp)<br>    |
|  83|[0x800127a0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x43ed0a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000b74]:fmul.s t6, t5, t4, dyn<br> [0x80000b78]:csrrs a2, fcsr, zero<br> [0x80000b7c]:sw t6, 456(fp)<br>    |
|  84|[0x800127a8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x43ed0a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000b94]:fmul.s t6, t5, t4, dyn<br> [0x80000b98]:csrrs a2, fcsr, zero<br> [0x80000b9c]:sw t6, 464(fp)<br>    |
|  85|[0x800127b0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x43ed0a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000bb4]:fmul.s t6, t5, t4, dyn<br> [0x80000bb8]:csrrs a2, fcsr, zero<br> [0x80000bbc]:sw t6, 472(fp)<br>    |
|  86|[0x800127b8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x018006 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000bd4]:fmul.s t6, t5, t4, dyn<br> [0x80000bd8]:csrrs a2, fcsr, zero<br> [0x80000bdc]:sw t6, 480(fp)<br>    |
|  87|[0x800127c0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x018006 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000bf4]:fmul.s t6, t5, t4, dyn<br> [0x80000bf8]:csrrs a2, fcsr, zero<br> [0x80000bfc]:sw t6, 488(fp)<br>    |
|  88|[0x800127c8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x018006 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000c14]:fmul.s t6, t5, t4, dyn<br> [0x80000c18]:csrrs a2, fcsr, zero<br> [0x80000c1c]:sw t6, 496(fp)<br>    |
|  89|[0x800127d0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x018006 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000c34]:fmul.s t6, t5, t4, dyn<br> [0x80000c38]:csrrs a2, fcsr, zero<br> [0x80000c3c]:sw t6, 504(fp)<br>    |
|  90|[0x800127d8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x018006 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000c54]:fmul.s t6, t5, t4, dyn<br> [0x80000c58]:csrrs a2, fcsr, zero<br> [0x80000c5c]:sw t6, 512(fp)<br>    |
|  91|[0x800127e0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2b90d3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000c74]:fmul.s t6, t5, t4, dyn<br> [0x80000c78]:csrrs a2, fcsr, zero<br> [0x80000c7c]:sw t6, 520(fp)<br>    |
|  92|[0x800127e8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2b90d3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000c94]:fmul.s t6, t5, t4, dyn<br> [0x80000c98]:csrrs a2, fcsr, zero<br> [0x80000c9c]:sw t6, 528(fp)<br>    |
|  93|[0x800127f0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2b90d3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000cb4]:fmul.s t6, t5, t4, dyn<br> [0x80000cb8]:csrrs a2, fcsr, zero<br> [0x80000cbc]:sw t6, 536(fp)<br>    |
|  94|[0x800127f8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2b90d3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000cd4]:fmul.s t6, t5, t4, dyn<br> [0x80000cd8]:csrrs a2, fcsr, zero<br> [0x80000cdc]:sw t6, 544(fp)<br>    |
|  95|[0x80012800]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2b90d3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000cf4]:fmul.s t6, t5, t4, dyn<br> [0x80000cf8]:csrrs a2, fcsr, zero<br> [0x80000cfc]:sw t6, 552(fp)<br>    |
|  96|[0x80012808]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0d014f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000d14]:fmul.s t6, t5, t4, dyn<br> [0x80000d18]:csrrs a2, fcsr, zero<br> [0x80000d1c]:sw t6, 560(fp)<br>    |
|  97|[0x80012810]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0d014f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000d34]:fmul.s t6, t5, t4, dyn<br> [0x80000d38]:csrrs a2, fcsr, zero<br> [0x80000d3c]:sw t6, 568(fp)<br>    |
|  98|[0x80012818]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0d014f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000d54]:fmul.s t6, t5, t4, dyn<br> [0x80000d58]:csrrs a2, fcsr, zero<br> [0x80000d5c]:sw t6, 576(fp)<br>    |
|  99|[0x80012820]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0d014f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000d74]:fmul.s t6, t5, t4, dyn<br> [0x80000d78]:csrrs a2, fcsr, zero<br> [0x80000d7c]:sw t6, 584(fp)<br>    |
| 100|[0x80012828]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0d014f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000d94]:fmul.s t6, t5, t4, dyn<br> [0x80000d98]:csrrs a2, fcsr, zero<br> [0x80000d9c]:sw t6, 592(fp)<br>    |
| 101|[0x80012830]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x127958 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000db4]:fmul.s t6, t5, t4, dyn<br> [0x80000db8]:csrrs a2, fcsr, zero<br> [0x80000dbc]:sw t6, 600(fp)<br>    |
| 102|[0x80012838]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x127958 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000dd4]:fmul.s t6, t5, t4, dyn<br> [0x80000dd8]:csrrs a2, fcsr, zero<br> [0x80000ddc]:sw t6, 608(fp)<br>    |
| 103|[0x80012840]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x127958 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000df4]:fmul.s t6, t5, t4, dyn<br> [0x80000df8]:csrrs a2, fcsr, zero<br> [0x80000dfc]:sw t6, 616(fp)<br>    |
| 104|[0x80012848]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x127958 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000e14]:fmul.s t6, t5, t4, dyn<br> [0x80000e18]:csrrs a2, fcsr, zero<br> [0x80000e1c]:sw t6, 624(fp)<br>    |
| 105|[0x80012850]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x127958 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000e34]:fmul.s t6, t5, t4, dyn<br> [0x80000e38]:csrrs a2, fcsr, zero<br> [0x80000e3c]:sw t6, 632(fp)<br>    |
| 106|[0x80012858]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x07daac and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000e54]:fmul.s t6, t5, t4, dyn<br> [0x80000e58]:csrrs a2, fcsr, zero<br> [0x80000e5c]:sw t6, 640(fp)<br>    |
| 107|[0x80012860]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x07daac and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000e74]:fmul.s t6, t5, t4, dyn<br> [0x80000e78]:csrrs a2, fcsr, zero<br> [0x80000e7c]:sw t6, 648(fp)<br>    |
| 108|[0x80012868]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x07daac and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000e94]:fmul.s t6, t5, t4, dyn<br> [0x80000e98]:csrrs a2, fcsr, zero<br> [0x80000e9c]:sw t6, 656(fp)<br>    |
| 109|[0x80012870]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x07daac and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000eb4]:fmul.s t6, t5, t4, dyn<br> [0x80000eb8]:csrrs a2, fcsr, zero<br> [0x80000ebc]:sw t6, 664(fp)<br>    |
| 110|[0x80012878]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x07daac and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000ed4]:fmul.s t6, t5, t4, dyn<br> [0x80000ed8]:csrrs a2, fcsr, zero<br> [0x80000edc]:sw t6, 672(fp)<br>    |
| 111|[0x80012880]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x38af5a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000ef4]:fmul.s t6, t5, t4, dyn<br> [0x80000ef8]:csrrs a2, fcsr, zero<br> [0x80000efc]:sw t6, 680(fp)<br>    |
| 112|[0x80012888]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x38af5a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000f14]:fmul.s t6, t5, t4, dyn<br> [0x80000f18]:csrrs a2, fcsr, zero<br> [0x80000f1c]:sw t6, 688(fp)<br>    |
| 113|[0x80012890]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x38af5a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000f34]:fmul.s t6, t5, t4, dyn<br> [0x80000f38]:csrrs a2, fcsr, zero<br> [0x80000f3c]:sw t6, 696(fp)<br>    |
| 114|[0x80012898]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x38af5a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000f54]:fmul.s t6, t5, t4, dyn<br> [0x80000f58]:csrrs a2, fcsr, zero<br> [0x80000f5c]:sw t6, 704(fp)<br>    |
| 115|[0x800128a0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x38af5a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000f74]:fmul.s t6, t5, t4, dyn<br> [0x80000f78]:csrrs a2, fcsr, zero<br> [0x80000f7c]:sw t6, 712(fp)<br>    |
| 116|[0x800128a8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x10c4ce and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000f94]:fmul.s t6, t5, t4, dyn<br> [0x80000f98]:csrrs a2, fcsr, zero<br> [0x80000f9c]:sw t6, 720(fp)<br>    |
| 117|[0x800128b0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x10c4ce and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000fb4]:fmul.s t6, t5, t4, dyn<br> [0x80000fb8]:csrrs a2, fcsr, zero<br> [0x80000fbc]:sw t6, 728(fp)<br>    |
| 118|[0x800128b8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x10c4ce and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000fd4]:fmul.s t6, t5, t4, dyn<br> [0x80000fd8]:csrrs a2, fcsr, zero<br> [0x80000fdc]:sw t6, 736(fp)<br>    |
| 119|[0x800128c0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x10c4ce and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000ff4]:fmul.s t6, t5, t4, dyn<br> [0x80000ff8]:csrrs a2, fcsr, zero<br> [0x80000ffc]:sw t6, 744(fp)<br>    |
| 120|[0x800128c8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x10c4ce and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80001014]:fmul.s t6, t5, t4, dyn<br> [0x80001018]:csrrs a2, fcsr, zero<br> [0x8000101c]:sw t6, 752(fp)<br>    |
| 121|[0x800128d0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x312e1f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001034]:fmul.s t6, t5, t4, dyn<br> [0x80001038]:csrrs a2, fcsr, zero<br> [0x8000103c]:sw t6, 760(fp)<br>    |
| 122|[0x800128d8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x312e1f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80001054]:fmul.s t6, t5, t4, dyn<br> [0x80001058]:csrrs a2, fcsr, zero<br> [0x8000105c]:sw t6, 768(fp)<br>    |
| 123|[0x800128e0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x312e1f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80001074]:fmul.s t6, t5, t4, dyn<br> [0x80001078]:csrrs a2, fcsr, zero<br> [0x8000107c]:sw t6, 776(fp)<br>    |
| 124|[0x800128e8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x312e1f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80001094]:fmul.s t6, t5, t4, dyn<br> [0x80001098]:csrrs a2, fcsr, zero<br> [0x8000109c]:sw t6, 784(fp)<br>    |
| 125|[0x800128f0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x312e1f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800010b4]:fmul.s t6, t5, t4, dyn<br> [0x800010b8]:csrrs a2, fcsr, zero<br> [0x800010bc]:sw t6, 792(fp)<br>    |
| 126|[0x800128f8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3c2a53 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800010d4]:fmul.s t6, t5, t4, dyn<br> [0x800010d8]:csrrs a2, fcsr, zero<br> [0x800010dc]:sw t6, 800(fp)<br>    |
| 127|[0x80012900]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3c2a53 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800010f4]:fmul.s t6, t5, t4, dyn<br> [0x800010f8]:csrrs a2, fcsr, zero<br> [0x800010fc]:sw t6, 808(fp)<br>    |
| 128|[0x80012908]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3c2a53 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80001114]:fmul.s t6, t5, t4, dyn<br> [0x80001118]:csrrs a2, fcsr, zero<br> [0x8000111c]:sw t6, 816(fp)<br>    |
| 129|[0x80012910]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3c2a53 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80001134]:fmul.s t6, t5, t4, dyn<br> [0x80001138]:csrrs a2, fcsr, zero<br> [0x8000113c]:sw t6, 824(fp)<br>    |
| 130|[0x80012918]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3c2a53 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80001154]:fmul.s t6, t5, t4, dyn<br> [0x80001158]:csrrs a2, fcsr, zero<br> [0x8000115c]:sw t6, 832(fp)<br>    |
| 131|[0x80012920]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4052ad and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001174]:fmul.s t6, t5, t4, dyn<br> [0x80001178]:csrrs a2, fcsr, zero<br> [0x8000117c]:sw t6, 840(fp)<br>    |
| 132|[0x80012928]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4052ad and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80001194]:fmul.s t6, t5, t4, dyn<br> [0x80001198]:csrrs a2, fcsr, zero<br> [0x8000119c]:sw t6, 848(fp)<br>    |
| 133|[0x80012930]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4052ad and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800011b4]:fmul.s t6, t5, t4, dyn<br> [0x800011b8]:csrrs a2, fcsr, zero<br> [0x800011bc]:sw t6, 856(fp)<br>    |
| 134|[0x80012938]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4052ad and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800011d4]:fmul.s t6, t5, t4, dyn<br> [0x800011d8]:csrrs a2, fcsr, zero<br> [0x800011dc]:sw t6, 864(fp)<br>    |
| 135|[0x80012940]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4052ad and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800011f4]:fmul.s t6, t5, t4, dyn<br> [0x800011f8]:csrrs a2, fcsr, zero<br> [0x800011fc]:sw t6, 872(fp)<br>    |
| 136|[0x80012948]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x07e829 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001214]:fmul.s t6, t5, t4, dyn<br> [0x80001218]:csrrs a2, fcsr, zero<br> [0x8000121c]:sw t6, 880(fp)<br>    |
| 137|[0x80012950]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x07e829 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80001234]:fmul.s t6, t5, t4, dyn<br> [0x80001238]:csrrs a2, fcsr, zero<br> [0x8000123c]:sw t6, 888(fp)<br>    |
| 138|[0x80012958]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x07e829 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80001254]:fmul.s t6, t5, t4, dyn<br> [0x80001258]:csrrs a2, fcsr, zero<br> [0x8000125c]:sw t6, 896(fp)<br>    |
| 139|[0x80012960]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x07e829 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80001274]:fmul.s t6, t5, t4, dyn<br> [0x80001278]:csrrs a2, fcsr, zero<br> [0x8000127c]:sw t6, 904(fp)<br>    |
| 140|[0x80012968]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x07e829 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80001294]:fmul.s t6, t5, t4, dyn<br> [0x80001298]:csrrs a2, fcsr, zero<br> [0x8000129c]:sw t6, 912(fp)<br>    |
| 141|[0x80012970]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2cbe14 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800012b4]:fmul.s t6, t5, t4, dyn<br> [0x800012b8]:csrrs a2, fcsr, zero<br> [0x800012bc]:sw t6, 920(fp)<br>    |
| 142|[0x80012978]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2cbe14 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800012d4]:fmul.s t6, t5, t4, dyn<br> [0x800012d8]:csrrs a2, fcsr, zero<br> [0x800012dc]:sw t6, 928(fp)<br>    |
| 143|[0x80012980]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2cbe14 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800012f4]:fmul.s t6, t5, t4, dyn<br> [0x800012f8]:csrrs a2, fcsr, zero<br> [0x800012fc]:sw t6, 936(fp)<br>    |
| 144|[0x80012988]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2cbe14 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80001314]:fmul.s t6, t5, t4, dyn<br> [0x80001318]:csrrs a2, fcsr, zero<br> [0x8000131c]:sw t6, 944(fp)<br>    |
| 145|[0x80012990]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2cbe14 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80001334]:fmul.s t6, t5, t4, dyn<br> [0x80001338]:csrrs a2, fcsr, zero<br> [0x8000133c]:sw t6, 952(fp)<br>    |
| 146|[0x80012998]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x59ffad and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001354]:fmul.s t6, t5, t4, dyn<br> [0x80001358]:csrrs a2, fcsr, zero<br> [0x8000135c]:sw t6, 960(fp)<br>    |
| 147|[0x800129a0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x59ffad and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80001374]:fmul.s t6, t5, t4, dyn<br> [0x80001378]:csrrs a2, fcsr, zero<br> [0x8000137c]:sw t6, 968(fp)<br>    |
| 148|[0x800129a8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x59ffad and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80001394]:fmul.s t6, t5, t4, dyn<br> [0x80001398]:csrrs a2, fcsr, zero<br> [0x8000139c]:sw t6, 976(fp)<br>    |
| 149|[0x800129b0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x59ffad and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800013b4]:fmul.s t6, t5, t4, dyn<br> [0x800013b8]:csrrs a2, fcsr, zero<br> [0x800013bc]:sw t6, 984(fp)<br>    |
| 150|[0x800129b8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x59ffad and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800013d4]:fmul.s t6, t5, t4, dyn<br> [0x800013d8]:csrrs a2, fcsr, zero<br> [0x800013dc]:sw t6, 992(fp)<br>    |
| 151|[0x800129c0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0af6e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800013f4]:fmul.s t6, t5, t4, dyn<br> [0x800013f8]:csrrs a2, fcsr, zero<br> [0x800013fc]:sw t6, 1000(fp)<br>   |
| 152|[0x800129c8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0af6e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80001414]:fmul.s t6, t5, t4, dyn<br> [0x80001418]:csrrs a2, fcsr, zero<br> [0x8000141c]:sw t6, 1008(fp)<br>   |
| 153|[0x800129d0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0af6e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80001434]:fmul.s t6, t5, t4, dyn<br> [0x80001438]:csrrs a2, fcsr, zero<br> [0x8000143c]:sw t6, 1016(fp)<br>   |
| 154|[0x800129d8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0af6e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000145c]:fmul.s t6, t5, t4, dyn<br> [0x80001460]:csrrs a2, fcsr, zero<br> [0x80001464]:sw t6, 0(fp)<br>      |
| 155|[0x800129e0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0af6e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000147c]:fmul.s t6, t5, t4, dyn<br> [0x80001480]:csrrs a2, fcsr, zero<br> [0x80001484]:sw t6, 8(fp)<br>      |
| 156|[0x800129e8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x2a59d1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000149c]:fmul.s t6, t5, t4, dyn<br> [0x800014a0]:csrrs a2, fcsr, zero<br> [0x800014a4]:sw t6, 16(fp)<br>     |
| 157|[0x800129f0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x2a59d1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800014bc]:fmul.s t6, t5, t4, dyn<br> [0x800014c0]:csrrs a2, fcsr, zero<br> [0x800014c4]:sw t6, 24(fp)<br>     |
| 158|[0x800129f8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x2a59d1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800014dc]:fmul.s t6, t5, t4, dyn<br> [0x800014e0]:csrrs a2, fcsr, zero<br> [0x800014e4]:sw t6, 32(fp)<br>     |
| 159|[0x80012a00]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x2a59d1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800014fc]:fmul.s t6, t5, t4, dyn<br> [0x80001500]:csrrs a2, fcsr, zero<br> [0x80001504]:sw t6, 40(fp)<br>     |
| 160|[0x80012a08]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x2a59d1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000151c]:fmul.s t6, t5, t4, dyn<br> [0x80001520]:csrrs a2, fcsr, zero<br> [0x80001524]:sw t6, 48(fp)<br>     |
| 161|[0x80012a10]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x065158 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000153c]:fmul.s t6, t5, t4, dyn<br> [0x80001540]:csrrs a2, fcsr, zero<br> [0x80001544]:sw t6, 56(fp)<br>     |
| 162|[0x80012a18]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x065158 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000155c]:fmul.s t6, t5, t4, dyn<br> [0x80001560]:csrrs a2, fcsr, zero<br> [0x80001564]:sw t6, 64(fp)<br>     |
| 163|[0x80012a20]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x065158 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000157c]:fmul.s t6, t5, t4, dyn<br> [0x80001580]:csrrs a2, fcsr, zero<br> [0x80001584]:sw t6, 72(fp)<br>     |
| 164|[0x80012a28]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x065158 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000159c]:fmul.s t6, t5, t4, dyn<br> [0x800015a0]:csrrs a2, fcsr, zero<br> [0x800015a4]:sw t6, 80(fp)<br>     |
| 165|[0x80012a30]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x065158 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800015bc]:fmul.s t6, t5, t4, dyn<br> [0x800015c0]:csrrs a2, fcsr, zero<br> [0x800015c4]:sw t6, 88(fp)<br>     |
| 166|[0x80012a38]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2e5316 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800015dc]:fmul.s t6, t5, t4, dyn<br> [0x800015e0]:csrrs a2, fcsr, zero<br> [0x800015e4]:sw t6, 96(fp)<br>     |
| 167|[0x80012a40]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2e5316 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800015fc]:fmul.s t6, t5, t4, dyn<br> [0x80001600]:csrrs a2, fcsr, zero<br> [0x80001604]:sw t6, 104(fp)<br>    |
| 168|[0x80012a48]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2e5316 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000161c]:fmul.s t6, t5, t4, dyn<br> [0x80001620]:csrrs a2, fcsr, zero<br> [0x80001624]:sw t6, 112(fp)<br>    |
| 169|[0x80012a50]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2e5316 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000163c]:fmul.s t6, t5, t4, dyn<br> [0x80001640]:csrrs a2, fcsr, zero<br> [0x80001644]:sw t6, 120(fp)<br>    |
| 170|[0x80012a58]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2e5316 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000165c]:fmul.s t6, t5, t4, dyn<br> [0x80001660]:csrrs a2, fcsr, zero<br> [0x80001664]:sw t6, 128(fp)<br>    |
| 171|[0x80012a60]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2efc0a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000167c]:fmul.s t6, t5, t4, dyn<br> [0x80001680]:csrrs a2, fcsr, zero<br> [0x80001684]:sw t6, 136(fp)<br>    |
| 172|[0x80012a68]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2efc0a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000169c]:fmul.s t6, t5, t4, dyn<br> [0x800016a0]:csrrs a2, fcsr, zero<br> [0x800016a4]:sw t6, 144(fp)<br>    |
| 173|[0x80012a70]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2efc0a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800016bc]:fmul.s t6, t5, t4, dyn<br> [0x800016c0]:csrrs a2, fcsr, zero<br> [0x800016c4]:sw t6, 152(fp)<br>    |
| 174|[0x80012a78]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2efc0a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800016dc]:fmul.s t6, t5, t4, dyn<br> [0x800016e0]:csrrs a2, fcsr, zero<br> [0x800016e4]:sw t6, 160(fp)<br>    |
| 175|[0x80012a80]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2efc0a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800016fc]:fmul.s t6, t5, t4, dyn<br> [0x80001700]:csrrs a2, fcsr, zero<br> [0x80001704]:sw t6, 168(fp)<br>    |
| 176|[0x80012a88]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x37d03d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000171c]:fmul.s t6, t5, t4, dyn<br> [0x80001720]:csrrs a2, fcsr, zero<br> [0x80001724]:sw t6, 176(fp)<br>    |
| 177|[0x80012a90]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x37d03d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000173c]:fmul.s t6, t5, t4, dyn<br> [0x80001740]:csrrs a2, fcsr, zero<br> [0x80001744]:sw t6, 184(fp)<br>    |
| 178|[0x80012a98]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x37d03d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000175c]:fmul.s t6, t5, t4, dyn<br> [0x80001760]:csrrs a2, fcsr, zero<br> [0x80001764]:sw t6, 192(fp)<br>    |
| 179|[0x80012aa0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x37d03d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000177c]:fmul.s t6, t5, t4, dyn<br> [0x80001780]:csrrs a2, fcsr, zero<br> [0x80001784]:sw t6, 200(fp)<br>    |
| 180|[0x80012aa8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x37d03d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000179c]:fmul.s t6, t5, t4, dyn<br> [0x800017a0]:csrrs a2, fcsr, zero<br> [0x800017a4]:sw t6, 208(fp)<br>    |
| 181|[0x80012ab0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x41a1ac and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800017bc]:fmul.s t6, t5, t4, dyn<br> [0x800017c0]:csrrs a2, fcsr, zero<br> [0x800017c4]:sw t6, 216(fp)<br>    |
| 182|[0x80012ab8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x41a1ac and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800017dc]:fmul.s t6, t5, t4, dyn<br> [0x800017e0]:csrrs a2, fcsr, zero<br> [0x800017e4]:sw t6, 224(fp)<br>    |
| 183|[0x80012ac0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x41a1ac and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800017fc]:fmul.s t6, t5, t4, dyn<br> [0x80001800]:csrrs a2, fcsr, zero<br> [0x80001804]:sw t6, 232(fp)<br>    |
| 184|[0x80012ac8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x41a1ac and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000181c]:fmul.s t6, t5, t4, dyn<br> [0x80001820]:csrrs a2, fcsr, zero<br> [0x80001824]:sw t6, 240(fp)<br>    |
| 185|[0x80012ad0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x41a1ac and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000183c]:fmul.s t6, t5, t4, dyn<br> [0x80001840]:csrrs a2, fcsr, zero<br> [0x80001844]:sw t6, 248(fp)<br>    |
| 186|[0x80012ad8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7a24a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000185c]:fmul.s t6, t5, t4, dyn<br> [0x80001860]:csrrs a2, fcsr, zero<br> [0x80001864]:sw t6, 256(fp)<br>    |
| 187|[0x80012ae0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7a24a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000187c]:fmul.s t6, t5, t4, dyn<br> [0x80001880]:csrrs a2, fcsr, zero<br> [0x80001884]:sw t6, 264(fp)<br>    |
| 188|[0x80012ae8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7a24a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000189c]:fmul.s t6, t5, t4, dyn<br> [0x800018a0]:csrrs a2, fcsr, zero<br> [0x800018a4]:sw t6, 272(fp)<br>    |
| 189|[0x80012af0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7a24a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800018bc]:fmul.s t6, t5, t4, dyn<br> [0x800018c0]:csrrs a2, fcsr, zero<br> [0x800018c4]:sw t6, 280(fp)<br>    |
| 190|[0x80012af8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7a24a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800018dc]:fmul.s t6, t5, t4, dyn<br> [0x800018e0]:csrrs a2, fcsr, zero<br> [0x800018e4]:sw t6, 288(fp)<br>    |
| 191|[0x80012b00]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x6f7f16 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800018fc]:fmul.s t6, t5, t4, dyn<br> [0x80001900]:csrrs a2, fcsr, zero<br> [0x80001904]:sw t6, 296(fp)<br>    |
| 192|[0x80012b08]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x6f7f16 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000191c]:fmul.s t6, t5, t4, dyn<br> [0x80001920]:csrrs a2, fcsr, zero<br> [0x80001924]:sw t6, 304(fp)<br>    |
| 193|[0x80012b10]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x6f7f16 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000193c]:fmul.s t6, t5, t4, dyn<br> [0x80001940]:csrrs a2, fcsr, zero<br> [0x80001944]:sw t6, 312(fp)<br>    |
| 194|[0x80012b18]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x6f7f16 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000195c]:fmul.s t6, t5, t4, dyn<br> [0x80001960]:csrrs a2, fcsr, zero<br> [0x80001964]:sw t6, 320(fp)<br>    |
| 195|[0x80012b20]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x6f7f16 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000197c]:fmul.s t6, t5, t4, dyn<br> [0x80001980]:csrrs a2, fcsr, zero<br> [0x80001984]:sw t6, 328(fp)<br>    |
| 196|[0x80012b28]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4490fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000199c]:fmul.s t6, t5, t4, dyn<br> [0x800019a0]:csrrs a2, fcsr, zero<br> [0x800019a4]:sw t6, 336(fp)<br>    |
| 197|[0x80012b30]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4490fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800019bc]:fmul.s t6, t5, t4, dyn<br> [0x800019c0]:csrrs a2, fcsr, zero<br> [0x800019c4]:sw t6, 344(fp)<br>    |
| 198|[0x80012b38]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4490fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800019dc]:fmul.s t6, t5, t4, dyn<br> [0x800019e0]:csrrs a2, fcsr, zero<br> [0x800019e4]:sw t6, 352(fp)<br>    |
| 199|[0x80012b40]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4490fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800019fc]:fmul.s t6, t5, t4, dyn<br> [0x80001a00]:csrrs a2, fcsr, zero<br> [0x80001a04]:sw t6, 360(fp)<br>    |
| 200|[0x80012b48]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4490fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80001a1c]:fmul.s t6, t5, t4, dyn<br> [0x80001a20]:csrrs a2, fcsr, zero<br> [0x80001a24]:sw t6, 368(fp)<br>    |
| 201|[0x80012b50]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6f5572 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001a3c]:fmul.s t6, t5, t4, dyn<br> [0x80001a40]:csrrs a2, fcsr, zero<br> [0x80001a44]:sw t6, 376(fp)<br>    |
| 202|[0x80012b58]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6f5572 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80001a5c]:fmul.s t6, t5, t4, dyn<br> [0x80001a60]:csrrs a2, fcsr, zero<br> [0x80001a64]:sw t6, 384(fp)<br>    |
| 203|[0x80012b60]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6f5572 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80001a7c]:fmul.s t6, t5, t4, dyn<br> [0x80001a80]:csrrs a2, fcsr, zero<br> [0x80001a84]:sw t6, 392(fp)<br>    |
| 204|[0x80012b68]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6f5572 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80001a9c]:fmul.s t6, t5, t4, dyn<br> [0x80001aa0]:csrrs a2, fcsr, zero<br> [0x80001aa4]:sw t6, 400(fp)<br>    |
| 205|[0x80012b70]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6f5572 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80001abc]:fmul.s t6, t5, t4, dyn<br> [0x80001ac0]:csrrs a2, fcsr, zero<br> [0x80001ac4]:sw t6, 408(fp)<br>    |
| 206|[0x80012b78]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d844c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001adc]:fmul.s t6, t5, t4, dyn<br> [0x80001ae0]:csrrs a2, fcsr, zero<br> [0x80001ae4]:sw t6, 416(fp)<br>    |
| 207|[0x80012b80]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d844c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80001afc]:fmul.s t6, t5, t4, dyn<br> [0x80001b00]:csrrs a2, fcsr, zero<br> [0x80001b04]:sw t6, 424(fp)<br>    |
| 208|[0x80012b88]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d844c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80001b1c]:fmul.s t6, t5, t4, dyn<br> [0x80001b20]:csrrs a2, fcsr, zero<br> [0x80001b24]:sw t6, 432(fp)<br>    |
| 209|[0x80012b90]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d844c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80001b3c]:fmul.s t6, t5, t4, dyn<br> [0x80001b40]:csrrs a2, fcsr, zero<br> [0x80001b44]:sw t6, 440(fp)<br>    |
| 210|[0x80012b98]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d844c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80001b5c]:fmul.s t6, t5, t4, dyn<br> [0x80001b60]:csrrs a2, fcsr, zero<br> [0x80001b64]:sw t6, 448(fp)<br>    |
| 211|[0x80012ba0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2e44fd and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001b7c]:fmul.s t6, t5, t4, dyn<br> [0x80001b80]:csrrs a2, fcsr, zero<br> [0x80001b84]:sw t6, 456(fp)<br>    |
| 212|[0x80012ba8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2e44fd and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80001b9c]:fmul.s t6, t5, t4, dyn<br> [0x80001ba0]:csrrs a2, fcsr, zero<br> [0x80001ba4]:sw t6, 464(fp)<br>    |
| 213|[0x80012bb0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2e44fd and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80001bbc]:fmul.s t6, t5, t4, dyn<br> [0x80001bc0]:csrrs a2, fcsr, zero<br> [0x80001bc4]:sw t6, 472(fp)<br>    |
| 214|[0x80012bb8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2e44fd and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80001bdc]:fmul.s t6, t5, t4, dyn<br> [0x80001be0]:csrrs a2, fcsr, zero<br> [0x80001be4]:sw t6, 480(fp)<br>    |
| 215|[0x80012bc0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2e44fd and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80001bfc]:fmul.s t6, t5, t4, dyn<br> [0x80001c00]:csrrs a2, fcsr, zero<br> [0x80001c04]:sw t6, 488(fp)<br>    |
| 216|[0x80012bc8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0eff8f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001c1c]:fmul.s t6, t5, t4, dyn<br> [0x80001c20]:csrrs a2, fcsr, zero<br> [0x80001c24]:sw t6, 496(fp)<br>    |
| 217|[0x80012bd0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0eff8f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80001c3c]:fmul.s t6, t5, t4, dyn<br> [0x80001c40]:csrrs a2, fcsr, zero<br> [0x80001c44]:sw t6, 504(fp)<br>    |
| 218|[0x80012bd8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0eff8f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80001c5c]:fmul.s t6, t5, t4, dyn<br> [0x80001c60]:csrrs a2, fcsr, zero<br> [0x80001c64]:sw t6, 512(fp)<br>    |
| 219|[0x80012be0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0eff8f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80001c7c]:fmul.s t6, t5, t4, dyn<br> [0x80001c80]:csrrs a2, fcsr, zero<br> [0x80001c84]:sw t6, 520(fp)<br>    |
| 220|[0x80012be8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0eff8f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80001c9c]:fmul.s t6, t5, t4, dyn<br> [0x80001ca0]:csrrs a2, fcsr, zero<br> [0x80001ca4]:sw t6, 528(fp)<br>    |
| 221|[0x80012bf0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7ab7a7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001cbc]:fmul.s t6, t5, t4, dyn<br> [0x80001cc0]:csrrs a2, fcsr, zero<br> [0x80001cc4]:sw t6, 536(fp)<br>    |
| 222|[0x80012bf8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7ab7a7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80001cdc]:fmul.s t6, t5, t4, dyn<br> [0x80001ce0]:csrrs a2, fcsr, zero<br> [0x80001ce4]:sw t6, 544(fp)<br>    |
| 223|[0x80012c00]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7ab7a7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80001cfc]:fmul.s t6, t5, t4, dyn<br> [0x80001d00]:csrrs a2, fcsr, zero<br> [0x80001d04]:sw t6, 552(fp)<br>    |
| 224|[0x80012c08]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7ab7a7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80001d1c]:fmul.s t6, t5, t4, dyn<br> [0x80001d20]:csrrs a2, fcsr, zero<br> [0x80001d24]:sw t6, 560(fp)<br>    |
| 225|[0x80012c10]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7ab7a7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80001d3c]:fmul.s t6, t5, t4, dyn<br> [0x80001d40]:csrrs a2, fcsr, zero<br> [0x80001d44]:sw t6, 568(fp)<br>    |
| 226|[0x80012c18]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x2e0a9e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001d5c]:fmul.s t6, t5, t4, dyn<br> [0x80001d60]:csrrs a2, fcsr, zero<br> [0x80001d64]:sw t6, 576(fp)<br>    |
| 227|[0x80012c20]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x2e0a9e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80001d7c]:fmul.s t6, t5, t4, dyn<br> [0x80001d80]:csrrs a2, fcsr, zero<br> [0x80001d84]:sw t6, 584(fp)<br>    |
| 228|[0x80012c28]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x2e0a9e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80001d9c]:fmul.s t6, t5, t4, dyn<br> [0x80001da0]:csrrs a2, fcsr, zero<br> [0x80001da4]:sw t6, 592(fp)<br>    |
| 229|[0x80012c30]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x2e0a9e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80001dbc]:fmul.s t6, t5, t4, dyn<br> [0x80001dc0]:csrrs a2, fcsr, zero<br> [0x80001dc4]:sw t6, 600(fp)<br>    |
| 230|[0x80012c38]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x2e0a9e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80001ddc]:fmul.s t6, t5, t4, dyn<br> [0x80001de0]:csrrs a2, fcsr, zero<br> [0x80001de4]:sw t6, 608(fp)<br>    |
| 231|[0x80012c40]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x23397b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001dfc]:fmul.s t6, t5, t4, dyn<br> [0x80001e00]:csrrs a2, fcsr, zero<br> [0x80001e04]:sw t6, 616(fp)<br>    |
| 232|[0x80012c48]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x23397b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80001e1c]:fmul.s t6, t5, t4, dyn<br> [0x80001e20]:csrrs a2, fcsr, zero<br> [0x80001e24]:sw t6, 624(fp)<br>    |
| 233|[0x80012c50]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x23397b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80001e3c]:fmul.s t6, t5, t4, dyn<br> [0x80001e40]:csrrs a2, fcsr, zero<br> [0x80001e44]:sw t6, 632(fp)<br>    |
| 234|[0x80012c58]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x23397b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80001e5c]:fmul.s t6, t5, t4, dyn<br> [0x80001e60]:csrrs a2, fcsr, zero<br> [0x80001e64]:sw t6, 640(fp)<br>    |
| 235|[0x80012c60]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x23397b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80001e7c]:fmul.s t6, t5, t4, dyn<br> [0x80001e80]:csrrs a2, fcsr, zero<br> [0x80001e84]:sw t6, 648(fp)<br>    |
| 236|[0x80012c68]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x23f501 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001e9c]:fmul.s t6, t5, t4, dyn<br> [0x80001ea0]:csrrs a2, fcsr, zero<br> [0x80001ea4]:sw t6, 656(fp)<br>    |
| 237|[0x80012c70]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x23f501 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80001ebc]:fmul.s t6, t5, t4, dyn<br> [0x80001ec0]:csrrs a2, fcsr, zero<br> [0x80001ec4]:sw t6, 664(fp)<br>    |
| 238|[0x80012c78]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x23f501 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80001edc]:fmul.s t6, t5, t4, dyn<br> [0x80001ee0]:csrrs a2, fcsr, zero<br> [0x80001ee4]:sw t6, 672(fp)<br>    |
| 239|[0x80012c80]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x23f501 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80001efc]:fmul.s t6, t5, t4, dyn<br> [0x80001f00]:csrrs a2, fcsr, zero<br> [0x80001f04]:sw t6, 680(fp)<br>    |
| 240|[0x80012c88]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x23f501 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80001f1c]:fmul.s t6, t5, t4, dyn<br> [0x80001f20]:csrrs a2, fcsr, zero<br> [0x80001f24]:sw t6, 688(fp)<br>    |
| 241|[0x80012c90]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x21a5d7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001f3c]:fmul.s t6, t5, t4, dyn<br> [0x80001f40]:csrrs a2, fcsr, zero<br> [0x80001f44]:sw t6, 696(fp)<br>    |
| 242|[0x80012c98]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x21a5d7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80001f5c]:fmul.s t6, t5, t4, dyn<br> [0x80001f60]:csrrs a2, fcsr, zero<br> [0x80001f64]:sw t6, 704(fp)<br>    |
| 243|[0x80012ca0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x21a5d7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80001f7c]:fmul.s t6, t5, t4, dyn<br> [0x80001f80]:csrrs a2, fcsr, zero<br> [0x80001f84]:sw t6, 712(fp)<br>    |
| 244|[0x80012ca8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x21a5d7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80001f9c]:fmul.s t6, t5, t4, dyn<br> [0x80001fa0]:csrrs a2, fcsr, zero<br> [0x80001fa4]:sw t6, 720(fp)<br>    |
| 245|[0x80012cb0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x21a5d7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80001fbc]:fmul.s t6, t5, t4, dyn<br> [0x80001fc0]:csrrs a2, fcsr, zero<br> [0x80001fc4]:sw t6, 728(fp)<br>    |
| 246|[0x80012cb8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x22667e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001fdc]:fmul.s t6, t5, t4, dyn<br> [0x80001fe0]:csrrs a2, fcsr, zero<br> [0x80001fe4]:sw t6, 736(fp)<br>    |
| 247|[0x80012cc0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x22667e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80001ffc]:fmul.s t6, t5, t4, dyn<br> [0x80002000]:csrrs a2, fcsr, zero<br> [0x80002004]:sw t6, 744(fp)<br>    |
| 248|[0x80012cc8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x22667e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000201c]:fmul.s t6, t5, t4, dyn<br> [0x80002020]:csrrs a2, fcsr, zero<br> [0x80002024]:sw t6, 752(fp)<br>    |
| 249|[0x80012cd0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x22667e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000203c]:fmul.s t6, t5, t4, dyn<br> [0x80002040]:csrrs a2, fcsr, zero<br> [0x80002044]:sw t6, 760(fp)<br>    |
| 250|[0x80012cd8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x22667e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000205c]:fmul.s t6, t5, t4, dyn<br> [0x80002060]:csrrs a2, fcsr, zero<br> [0x80002064]:sw t6, 768(fp)<br>    |
| 251|[0x80012ce0]<br>0x00000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x43d400 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000207c]:fmul.s t6, t5, t4, dyn<br> [0x80002080]:csrrs a2, fcsr, zero<br> [0x80002084]:sw t6, 776(fp)<br>    |
| 252|[0x80012ce8]<br>0x00000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x43d400 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000209c]:fmul.s t6, t5, t4, dyn<br> [0x800020a0]:csrrs a2, fcsr, zero<br> [0x800020a4]:sw t6, 784(fp)<br>    |
| 253|[0x80012cf0]<br>0x00000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x43d400 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800020bc]:fmul.s t6, t5, t4, dyn<br> [0x800020c0]:csrrs a2, fcsr, zero<br> [0x800020c4]:sw t6, 792(fp)<br>    |
| 254|[0x80012cf8]<br>0x00000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x43d400 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800020dc]:fmul.s t6, t5, t4, dyn<br> [0x800020e0]:csrrs a2, fcsr, zero<br> [0x800020e4]:sw t6, 800(fp)<br>    |
| 255|[0x80012d00]<br>0x00000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x43d400 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800020fc]:fmul.s t6, t5, t4, dyn<br> [0x80002100]:csrrs a2, fcsr, zero<br> [0x80002104]:sw t6, 808(fp)<br>    |
| 256|[0x80012d08]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x04012d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000211c]:fmul.s t6, t5, t4, dyn<br> [0x80002120]:csrrs a2, fcsr, zero<br> [0x80002124]:sw t6, 816(fp)<br>    |
| 257|[0x80012d10]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x04012d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000213c]:fmul.s t6, t5, t4, dyn<br> [0x80002140]:csrrs a2, fcsr, zero<br> [0x80002144]:sw t6, 824(fp)<br>    |
| 258|[0x80012d18]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x04012d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000215c]:fmul.s t6, t5, t4, dyn<br> [0x80002160]:csrrs a2, fcsr, zero<br> [0x80002164]:sw t6, 832(fp)<br>    |
| 259|[0x80012d20]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x04012d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000217c]:fmul.s t6, t5, t4, dyn<br> [0x80002180]:csrrs a2, fcsr, zero<br> [0x80002184]:sw t6, 840(fp)<br>    |
| 260|[0x80012d28]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x04012d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000219c]:fmul.s t6, t5, t4, dyn<br> [0x800021a0]:csrrs a2, fcsr, zero<br> [0x800021a4]:sw t6, 848(fp)<br>    |
| 261|[0x80012d30]<br>0x00000000|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x7eadb5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800021bc]:fmul.s t6, t5, t4, dyn<br> [0x800021c0]:csrrs a2, fcsr, zero<br> [0x800021c4]:sw t6, 856(fp)<br>    |
| 262|[0x80012d38]<br>0x00000000|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x7eadb5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800021dc]:fmul.s t6, t5, t4, dyn<br> [0x800021e0]:csrrs a2, fcsr, zero<br> [0x800021e4]:sw t6, 864(fp)<br>    |
| 263|[0x80012d40]<br>0x00000000|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x7eadb5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800021fc]:fmul.s t6, t5, t4, dyn<br> [0x80002200]:csrrs a2, fcsr, zero<br> [0x80002204]:sw t6, 872(fp)<br>    |
| 264|[0x80012d48]<br>0x00000000|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x7eadb5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000221c]:fmul.s t6, t5, t4, dyn<br> [0x80002220]:csrrs a2, fcsr, zero<br> [0x80002224]:sw t6, 880(fp)<br>    |
| 265|[0x80012d50]<br>0x00000000|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x7eadb5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000223c]:fmul.s t6, t5, t4, dyn<br> [0x80002240]:csrrs a2, fcsr, zero<br> [0x80002244]:sw t6, 888(fp)<br>    |
| 266|[0x80012d58]<br>0x00000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x1bb7c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000225c]:fmul.s t6, t5, t4, dyn<br> [0x80002260]:csrrs a2, fcsr, zero<br> [0x80002264]:sw t6, 896(fp)<br>    |
| 267|[0x80012d60]<br>0x00000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x1bb7c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000227c]:fmul.s t6, t5, t4, dyn<br> [0x80002280]:csrrs a2, fcsr, zero<br> [0x80002284]:sw t6, 904(fp)<br>    |
| 268|[0x80012d68]<br>0x00000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x1bb7c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000229c]:fmul.s t6, t5, t4, dyn<br> [0x800022a0]:csrrs a2, fcsr, zero<br> [0x800022a4]:sw t6, 912(fp)<br>    |
| 269|[0x80012d70]<br>0x00000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x1bb7c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800022bc]:fmul.s t6, t5, t4, dyn<br> [0x800022c0]:csrrs a2, fcsr, zero<br> [0x800022c4]:sw t6, 920(fp)<br>    |
| 270|[0x80012d78]<br>0x00000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x1bb7c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800022dc]:fmul.s t6, t5, t4, dyn<br> [0x800022e0]:csrrs a2, fcsr, zero<br> [0x800022e4]:sw t6, 928(fp)<br>    |
| 271|[0x80012d80]<br>0x00000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x01ea00 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800022fc]:fmul.s t6, t5, t4, dyn<br> [0x80002300]:csrrs a2, fcsr, zero<br> [0x80002304]:sw t6, 936(fp)<br>    |
| 272|[0x80012d88]<br>0x00000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x01ea00 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000231c]:fmul.s t6, t5, t4, dyn<br> [0x80002320]:csrrs a2, fcsr, zero<br> [0x80002324]:sw t6, 944(fp)<br>    |
| 273|[0x80012d90]<br>0x00000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x01ea00 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000233c]:fmul.s t6, t5, t4, dyn<br> [0x80002340]:csrrs a2, fcsr, zero<br> [0x80002344]:sw t6, 952(fp)<br>    |
| 274|[0x80012d98]<br>0x00000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x01ea00 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000235c]:fmul.s t6, t5, t4, dyn<br> [0x80002360]:csrrs a2, fcsr, zero<br> [0x80002364]:sw t6, 960(fp)<br>    |
| 275|[0x80012da0]<br>0x00000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x01ea00 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000237c]:fmul.s t6, t5, t4, dyn<br> [0x80002380]:csrrs a2, fcsr, zero<br> [0x80002384]:sw t6, 968(fp)<br>    |
| 276|[0x80012da8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1c29fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000239c]:fmul.s t6, t5, t4, dyn<br> [0x800023a0]:csrrs a2, fcsr, zero<br> [0x800023a4]:sw t6, 976(fp)<br>    |
| 277|[0x80012db0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1c29fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800023bc]:fmul.s t6, t5, t4, dyn<br> [0x800023c0]:csrrs a2, fcsr, zero<br> [0x800023c4]:sw t6, 984(fp)<br>    |
| 278|[0x80012db8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1c29fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800023dc]:fmul.s t6, t5, t4, dyn<br> [0x800023e0]:csrrs a2, fcsr, zero<br> [0x800023e4]:sw t6, 992(fp)<br>    |
| 279|[0x80012dc0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1c29fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000241c]:fmul.s t6, t5, t4, dyn<br> [0x80002420]:csrrs a2, fcsr, zero<br> [0x80002424]:sw t6, 1000(fp)<br>   |
| 280|[0x80012dc8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1c29fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000245c]:fmul.s t6, t5, t4, dyn<br> [0x80002460]:csrrs a2, fcsr, zero<br> [0x80002464]:sw t6, 1008(fp)<br>   |
| 281|[0x80012dd0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x0d1c84 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000249c]:fmul.s t6, t5, t4, dyn<br> [0x800024a0]:csrrs a2, fcsr, zero<br> [0x800024a4]:sw t6, 1016(fp)<br>   |
| 282|[0x80012dd8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x0d1c84 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800024e4]:fmul.s t6, t5, t4, dyn<br> [0x800024e8]:csrrs a2, fcsr, zero<br> [0x800024ec]:sw t6, 0(fp)<br>      |
| 283|[0x80012de0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x0d1c84 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80002524]:fmul.s t6, t5, t4, dyn<br> [0x80002528]:csrrs a2, fcsr, zero<br> [0x8000252c]:sw t6, 8(fp)<br>      |
| 284|[0x80012de8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x0d1c84 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80002564]:fmul.s t6, t5, t4, dyn<br> [0x80002568]:csrrs a2, fcsr, zero<br> [0x8000256c]:sw t6, 16(fp)<br>     |
| 285|[0x80012df0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x0d1c84 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800025a4]:fmul.s t6, t5, t4, dyn<br> [0x800025a8]:csrrs a2, fcsr, zero<br> [0x800025ac]:sw t6, 24(fp)<br>     |
| 286|[0x80012df8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x140eaa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800025e4]:fmul.s t6, t5, t4, dyn<br> [0x800025e8]:csrrs a2, fcsr, zero<br> [0x800025ec]:sw t6, 32(fp)<br>     |
| 287|[0x80012e00]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x140eaa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80002624]:fmul.s t6, t5, t4, dyn<br> [0x80002628]:csrrs a2, fcsr, zero<br> [0x8000262c]:sw t6, 40(fp)<br>     |
| 288|[0x80012e08]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x140eaa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80002664]:fmul.s t6, t5, t4, dyn<br> [0x80002668]:csrrs a2, fcsr, zero<br> [0x8000266c]:sw t6, 48(fp)<br>     |
| 289|[0x80012e10]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x140eaa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800026a4]:fmul.s t6, t5, t4, dyn<br> [0x800026a8]:csrrs a2, fcsr, zero<br> [0x800026ac]:sw t6, 56(fp)<br>     |
| 290|[0x80012e18]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x140eaa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800026e4]:fmul.s t6, t5, t4, dyn<br> [0x800026e8]:csrrs a2, fcsr, zero<br> [0x800026ec]:sw t6, 64(fp)<br>     |
| 291|[0x80012e20]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3e148d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002724]:fmul.s t6, t5, t4, dyn<br> [0x80002728]:csrrs a2, fcsr, zero<br> [0x8000272c]:sw t6, 72(fp)<br>     |
| 292|[0x80012e28]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3e148d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80002764]:fmul.s t6, t5, t4, dyn<br> [0x80002768]:csrrs a2, fcsr, zero<br> [0x8000276c]:sw t6, 80(fp)<br>     |
| 293|[0x80012e30]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3e148d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800027a4]:fmul.s t6, t5, t4, dyn<br> [0x800027a8]:csrrs a2, fcsr, zero<br> [0x800027ac]:sw t6, 88(fp)<br>     |
| 294|[0x80012e38]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3e148d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800027e4]:fmul.s t6, t5, t4, dyn<br> [0x800027e8]:csrrs a2, fcsr, zero<br> [0x800027ec]:sw t6, 96(fp)<br>     |
| 295|[0x80012e40]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3e148d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80002824]:fmul.s t6, t5, t4, dyn<br> [0x80002828]:csrrs a2, fcsr, zero<br> [0x8000282c]:sw t6, 104(fp)<br>    |
| 296|[0x80012e48]<br>0x00000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x5ed631 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002864]:fmul.s t6, t5, t4, dyn<br> [0x80002868]:csrrs a2, fcsr, zero<br> [0x8000286c]:sw t6, 112(fp)<br>    |
| 297|[0x80012e50]<br>0x00000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x5ed631 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800028a4]:fmul.s t6, t5, t4, dyn<br> [0x800028a8]:csrrs a2, fcsr, zero<br> [0x800028ac]:sw t6, 120(fp)<br>    |
| 298|[0x80012e58]<br>0x00000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x5ed631 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800028e4]:fmul.s t6, t5, t4, dyn<br> [0x800028e8]:csrrs a2, fcsr, zero<br> [0x800028ec]:sw t6, 128(fp)<br>    |
| 299|[0x80012e60]<br>0x00000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x5ed631 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80002924]:fmul.s t6, t5, t4, dyn<br> [0x80002928]:csrrs a2, fcsr, zero<br> [0x8000292c]:sw t6, 136(fp)<br>    |
| 300|[0x80012e68]<br>0x00000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x5ed631 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80002964]:fmul.s t6, t5, t4, dyn<br> [0x80002968]:csrrs a2, fcsr, zero<br> [0x8000296c]:sw t6, 144(fp)<br>    |
| 301|[0x80012e70]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6c407f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800029a4]:fmul.s t6, t5, t4, dyn<br> [0x800029a8]:csrrs a2, fcsr, zero<br> [0x800029ac]:sw t6, 152(fp)<br>    |
| 302|[0x80012e78]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6c407f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800029e4]:fmul.s t6, t5, t4, dyn<br> [0x800029e8]:csrrs a2, fcsr, zero<br> [0x800029ec]:sw t6, 160(fp)<br>    |
| 303|[0x80012e80]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6c407f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80002a24]:fmul.s t6, t5, t4, dyn<br> [0x80002a28]:csrrs a2, fcsr, zero<br> [0x80002a2c]:sw t6, 168(fp)<br>    |
| 304|[0x80012e88]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6c407f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80002a64]:fmul.s t6, t5, t4, dyn<br> [0x80002a68]:csrrs a2, fcsr, zero<br> [0x80002a6c]:sw t6, 176(fp)<br>    |
| 305|[0x80012e90]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6c407f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80002aa4]:fmul.s t6, t5, t4, dyn<br> [0x80002aa8]:csrrs a2, fcsr, zero<br> [0x80002aac]:sw t6, 184(fp)<br>    |
| 306|[0x80012e98]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x2dbe96 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002ae4]:fmul.s t6, t5, t4, dyn<br> [0x80002ae8]:csrrs a2, fcsr, zero<br> [0x80002aec]:sw t6, 192(fp)<br>    |
| 307|[0x80012ea0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x2dbe96 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80002b24]:fmul.s t6, t5, t4, dyn<br> [0x80002b28]:csrrs a2, fcsr, zero<br> [0x80002b2c]:sw t6, 200(fp)<br>    |
| 308|[0x80012ea8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x2dbe96 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80002b64]:fmul.s t6, t5, t4, dyn<br> [0x80002b68]:csrrs a2, fcsr, zero<br> [0x80002b6c]:sw t6, 208(fp)<br>    |
| 309|[0x80012eb0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x2dbe96 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80002ba4]:fmul.s t6, t5, t4, dyn<br> [0x80002ba8]:csrrs a2, fcsr, zero<br> [0x80002bac]:sw t6, 216(fp)<br>    |
| 310|[0x80012eb8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x2dbe96 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80002be4]:fmul.s t6, t5, t4, dyn<br> [0x80002be8]:csrrs a2, fcsr, zero<br> [0x80002bec]:sw t6, 224(fp)<br>    |
| 311|[0x80012ec0]<br>0x00000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x3d4d49 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002c24]:fmul.s t6, t5, t4, dyn<br> [0x80002c28]:csrrs a2, fcsr, zero<br> [0x80002c2c]:sw t6, 232(fp)<br>    |
| 312|[0x80012ec8]<br>0x00000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x3d4d49 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80002c64]:fmul.s t6, t5, t4, dyn<br> [0x80002c68]:csrrs a2, fcsr, zero<br> [0x80002c6c]:sw t6, 240(fp)<br>    |
| 313|[0x80012ed0]<br>0x00000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x3d4d49 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80002ca4]:fmul.s t6, t5, t4, dyn<br> [0x80002ca8]:csrrs a2, fcsr, zero<br> [0x80002cac]:sw t6, 248(fp)<br>    |
| 314|[0x80012ed8]<br>0x00000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x3d4d49 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80002ce4]:fmul.s t6, t5, t4, dyn<br> [0x80002ce8]:csrrs a2, fcsr, zero<br> [0x80002cec]:sw t6, 256(fp)<br>    |
| 315|[0x80012ee0]<br>0x00000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x3d4d49 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80002d24]:fmul.s t6, t5, t4, dyn<br> [0x80002d28]:csrrs a2, fcsr, zero<br> [0x80002d2c]:sw t6, 264(fp)<br>    |
| 316|[0x80012ee8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1310f3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002d64]:fmul.s t6, t5, t4, dyn<br> [0x80002d68]:csrrs a2, fcsr, zero<br> [0x80002d6c]:sw t6, 272(fp)<br>    |
| 317|[0x80012ef0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1310f3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80002da4]:fmul.s t6, t5, t4, dyn<br> [0x80002da8]:csrrs a2, fcsr, zero<br> [0x80002dac]:sw t6, 280(fp)<br>    |
| 318|[0x80012ef8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1310f3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80002de4]:fmul.s t6, t5, t4, dyn<br> [0x80002de8]:csrrs a2, fcsr, zero<br> [0x80002dec]:sw t6, 288(fp)<br>    |
| 319|[0x80012f00]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1310f3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80002e24]:fmul.s t6, t5, t4, dyn<br> [0x80002e28]:csrrs a2, fcsr, zero<br> [0x80002e2c]:sw t6, 296(fp)<br>    |
| 320|[0x80012f08]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1310f3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80002e64]:fmul.s t6, t5, t4, dyn<br> [0x80002e68]:csrrs a2, fcsr, zero<br> [0x80002e6c]:sw t6, 304(fp)<br>    |
| 321|[0x80012f10]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2d3017 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002ea4]:fmul.s t6, t5, t4, dyn<br> [0x80002ea8]:csrrs a2, fcsr, zero<br> [0x80002eac]:sw t6, 312(fp)<br>    |
| 322|[0x80012f18]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2d3017 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80002ee4]:fmul.s t6, t5, t4, dyn<br> [0x80002ee8]:csrrs a2, fcsr, zero<br> [0x80002eec]:sw t6, 320(fp)<br>    |
| 323|[0x80012f20]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2d3017 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80002f24]:fmul.s t6, t5, t4, dyn<br> [0x80002f28]:csrrs a2, fcsr, zero<br> [0x80002f2c]:sw t6, 328(fp)<br>    |
| 324|[0x80012f28]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2d3017 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80002f64]:fmul.s t6, t5, t4, dyn<br> [0x80002f68]:csrrs a2, fcsr, zero<br> [0x80002f6c]:sw t6, 336(fp)<br>    |
| 325|[0x80012f30]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2d3017 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80002fa4]:fmul.s t6, t5, t4, dyn<br> [0x80002fa8]:csrrs a2, fcsr, zero<br> [0x80002fac]:sw t6, 344(fp)<br>    |
| 326|[0x80012f38]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2596ee and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002fe4]:fmul.s t6, t5, t4, dyn<br> [0x80002fe8]:csrrs a2, fcsr, zero<br> [0x80002fec]:sw t6, 352(fp)<br>    |
| 327|[0x80012f40]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2596ee and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80003024]:fmul.s t6, t5, t4, dyn<br> [0x80003028]:csrrs a2, fcsr, zero<br> [0x8000302c]:sw t6, 360(fp)<br>    |
| 328|[0x80012f48]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2596ee and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80003064]:fmul.s t6, t5, t4, dyn<br> [0x80003068]:csrrs a2, fcsr, zero<br> [0x8000306c]:sw t6, 368(fp)<br>    |
| 329|[0x80012f50]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2596ee and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800030a4]:fmul.s t6, t5, t4, dyn<br> [0x800030a8]:csrrs a2, fcsr, zero<br> [0x800030ac]:sw t6, 376(fp)<br>    |
| 330|[0x80012f58]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2596ee and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800030e4]:fmul.s t6, t5, t4, dyn<br> [0x800030e8]:csrrs a2, fcsr, zero<br> [0x800030ec]:sw t6, 384(fp)<br>    |
| 331|[0x80012f60]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x358c1d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80003124]:fmul.s t6, t5, t4, dyn<br> [0x80003128]:csrrs a2, fcsr, zero<br> [0x8000312c]:sw t6, 392(fp)<br>    |
| 332|[0x80012f68]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x358c1d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80003164]:fmul.s t6, t5, t4, dyn<br> [0x80003168]:csrrs a2, fcsr, zero<br> [0x8000316c]:sw t6, 400(fp)<br>    |
| 333|[0x80012f70]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x358c1d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800031a4]:fmul.s t6, t5, t4, dyn<br> [0x800031a8]:csrrs a2, fcsr, zero<br> [0x800031ac]:sw t6, 408(fp)<br>    |
| 334|[0x80012f78]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x358c1d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800031e4]:fmul.s t6, t5, t4, dyn<br> [0x800031e8]:csrrs a2, fcsr, zero<br> [0x800031ec]:sw t6, 416(fp)<br>    |
| 335|[0x80012f80]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x358c1d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80003224]:fmul.s t6, t5, t4, dyn<br> [0x80003228]:csrrs a2, fcsr, zero<br> [0x8000322c]:sw t6, 424(fp)<br>    |
| 336|[0x80012f88]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7dc215 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80003264]:fmul.s t6, t5, t4, dyn<br> [0x80003268]:csrrs a2, fcsr, zero<br> [0x8000326c]:sw t6, 432(fp)<br>    |
| 337|[0x80012f90]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7dc215 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800032a4]:fmul.s t6, t5, t4, dyn<br> [0x800032a8]:csrrs a2, fcsr, zero<br> [0x800032ac]:sw t6, 440(fp)<br>    |
| 338|[0x80012f98]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7dc215 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800032e4]:fmul.s t6, t5, t4, dyn<br> [0x800032e8]:csrrs a2, fcsr, zero<br> [0x800032ec]:sw t6, 448(fp)<br>    |
| 339|[0x80012fa0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7dc215 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80003324]:fmul.s t6, t5, t4, dyn<br> [0x80003328]:csrrs a2, fcsr, zero<br> [0x8000332c]:sw t6, 456(fp)<br>    |
| 340|[0x80012fa8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7dc215 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80003364]:fmul.s t6, t5, t4, dyn<br> [0x80003368]:csrrs a2, fcsr, zero<br> [0x8000336c]:sw t6, 464(fp)<br>    |
| 341|[0x80012fb0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x1cbf56 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800033a4]:fmul.s t6, t5, t4, dyn<br> [0x800033a8]:csrrs a2, fcsr, zero<br> [0x800033ac]:sw t6, 472(fp)<br>    |
| 342|[0x80012fb8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x1cbf56 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800033e4]:fmul.s t6, t5, t4, dyn<br> [0x800033e8]:csrrs a2, fcsr, zero<br> [0x800033ec]:sw t6, 480(fp)<br>    |
| 343|[0x80012fc0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x1cbf56 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80003424]:fmul.s t6, t5, t4, dyn<br> [0x80003428]:csrrs a2, fcsr, zero<br> [0x8000342c]:sw t6, 488(fp)<br>    |
| 344|[0x80012fc8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x1cbf56 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80003464]:fmul.s t6, t5, t4, dyn<br> [0x80003468]:csrrs a2, fcsr, zero<br> [0x8000346c]:sw t6, 496(fp)<br>    |
| 345|[0x80012fd0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x1cbf56 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800034a4]:fmul.s t6, t5, t4, dyn<br> [0x800034a8]:csrrs a2, fcsr, zero<br> [0x800034ac]:sw t6, 504(fp)<br>    |
| 346|[0x80012fd8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x27f459 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800034e4]:fmul.s t6, t5, t4, dyn<br> [0x800034e8]:csrrs a2, fcsr, zero<br> [0x800034ec]:sw t6, 512(fp)<br>    |
| 347|[0x80012fe0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x27f459 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80003524]:fmul.s t6, t5, t4, dyn<br> [0x80003528]:csrrs a2, fcsr, zero<br> [0x8000352c]:sw t6, 520(fp)<br>    |
| 348|[0x80012fe8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x27f459 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80003564]:fmul.s t6, t5, t4, dyn<br> [0x80003568]:csrrs a2, fcsr, zero<br> [0x8000356c]:sw t6, 528(fp)<br>    |
| 349|[0x80012ff0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x27f459 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800035a4]:fmul.s t6, t5, t4, dyn<br> [0x800035a8]:csrrs a2, fcsr, zero<br> [0x800035ac]:sw t6, 536(fp)<br>    |
| 350|[0x80012ff8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x27f459 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800035e4]:fmul.s t6, t5, t4, dyn<br> [0x800035e8]:csrrs a2, fcsr, zero<br> [0x800035ec]:sw t6, 544(fp)<br>    |
| 351|[0x80013000]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x609f7b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80003624]:fmul.s t6, t5, t4, dyn<br> [0x80003628]:csrrs a2, fcsr, zero<br> [0x8000362c]:sw t6, 552(fp)<br>    |
| 352|[0x80013008]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x609f7b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80003664]:fmul.s t6, t5, t4, dyn<br> [0x80003668]:csrrs a2, fcsr, zero<br> [0x8000366c]:sw t6, 560(fp)<br>    |
| 353|[0x80013010]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x609f7b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800036a4]:fmul.s t6, t5, t4, dyn<br> [0x800036a8]:csrrs a2, fcsr, zero<br> [0x800036ac]:sw t6, 568(fp)<br>    |
| 354|[0x80013018]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x609f7b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800036e4]:fmul.s t6, t5, t4, dyn<br> [0x800036e8]:csrrs a2, fcsr, zero<br> [0x800036ec]:sw t6, 576(fp)<br>    |
| 355|[0x80013020]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x609f7b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80003724]:fmul.s t6, t5, t4, dyn<br> [0x80003728]:csrrs a2, fcsr, zero<br> [0x8000372c]:sw t6, 584(fp)<br>    |
| 356|[0x80013028]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x45af29 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80003764]:fmul.s t6, t5, t4, dyn<br> [0x80003768]:csrrs a2, fcsr, zero<br> [0x8000376c]:sw t6, 592(fp)<br>    |
| 357|[0x80013030]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x45af29 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800037a4]:fmul.s t6, t5, t4, dyn<br> [0x800037a8]:csrrs a2, fcsr, zero<br> [0x800037ac]:sw t6, 600(fp)<br>    |
| 358|[0x80013038]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x45af29 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800037e4]:fmul.s t6, t5, t4, dyn<br> [0x800037e8]:csrrs a2, fcsr, zero<br> [0x800037ec]:sw t6, 608(fp)<br>    |
| 359|[0x80013040]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x45af29 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80003824]:fmul.s t6, t5, t4, dyn<br> [0x80003828]:csrrs a2, fcsr, zero<br> [0x8000382c]:sw t6, 616(fp)<br>    |
| 360|[0x80013048]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x45af29 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80003864]:fmul.s t6, t5, t4, dyn<br> [0x80003868]:csrrs a2, fcsr, zero<br> [0x8000386c]:sw t6, 624(fp)<br>    |
| 361|[0x80013050]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3844b4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800038a4]:fmul.s t6, t5, t4, dyn<br> [0x800038a8]:csrrs a2, fcsr, zero<br> [0x800038ac]:sw t6, 632(fp)<br>    |
| 362|[0x80013058]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3844b4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800038e4]:fmul.s t6, t5, t4, dyn<br> [0x800038e8]:csrrs a2, fcsr, zero<br> [0x800038ec]:sw t6, 640(fp)<br>    |
| 363|[0x80013060]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3844b4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80003924]:fmul.s t6, t5, t4, dyn<br> [0x80003928]:csrrs a2, fcsr, zero<br> [0x8000392c]:sw t6, 648(fp)<br>    |
| 364|[0x80013068]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3844b4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80003964]:fmul.s t6, t5, t4, dyn<br> [0x80003968]:csrrs a2, fcsr, zero<br> [0x8000396c]:sw t6, 656(fp)<br>    |
| 365|[0x80013070]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3844b4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800039a4]:fmul.s t6, t5, t4, dyn<br> [0x800039a8]:csrrs a2, fcsr, zero<br> [0x800039ac]:sw t6, 664(fp)<br>    |
| 366|[0x80013078]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x68bbe2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800039e4]:fmul.s t6, t5, t4, dyn<br> [0x800039e8]:csrrs a2, fcsr, zero<br> [0x800039ec]:sw t6, 672(fp)<br>    |
| 367|[0x80013080]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x68bbe2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80003a24]:fmul.s t6, t5, t4, dyn<br> [0x80003a28]:csrrs a2, fcsr, zero<br> [0x80003a2c]:sw t6, 680(fp)<br>    |
| 368|[0x80013088]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x68bbe2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80003a64]:fmul.s t6, t5, t4, dyn<br> [0x80003a68]:csrrs a2, fcsr, zero<br> [0x80003a6c]:sw t6, 688(fp)<br>    |
| 369|[0x80013090]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x68bbe2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80003aa4]:fmul.s t6, t5, t4, dyn<br> [0x80003aa8]:csrrs a2, fcsr, zero<br> [0x80003aac]:sw t6, 696(fp)<br>    |
| 370|[0x80013098]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x68bbe2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80003ae4]:fmul.s t6, t5, t4, dyn<br> [0x80003ae8]:csrrs a2, fcsr, zero<br> [0x80003aec]:sw t6, 704(fp)<br>    |
| 371|[0x800130a0]<br>0x80000000|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x5d9799 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80003b24]:fmul.s t6, t5, t4, dyn<br> [0x80003b28]:csrrs a2, fcsr, zero<br> [0x80003b2c]:sw t6, 712(fp)<br>    |
| 372|[0x800130a8]<br>0x80000000|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x5d9799 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80003b64]:fmul.s t6, t5, t4, dyn<br> [0x80003b68]:csrrs a2, fcsr, zero<br> [0x80003b6c]:sw t6, 720(fp)<br>    |
| 373|[0x800130b0]<br>0x80000000|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x5d9799 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80003ba4]:fmul.s t6, t5, t4, dyn<br> [0x80003ba8]:csrrs a2, fcsr, zero<br> [0x80003bac]:sw t6, 728(fp)<br>    |
| 374|[0x800130b8]<br>0x80000000|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x5d9799 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80003be4]:fmul.s t6, t5, t4, dyn<br> [0x80003be8]:csrrs a2, fcsr, zero<br> [0x80003bec]:sw t6, 736(fp)<br>    |
| 375|[0x800130c0]<br>0x80000000|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x5d9799 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80003c24]:fmul.s t6, t5, t4, dyn<br> [0x80003c28]:csrrs a2, fcsr, zero<br> [0x80003c2c]:sw t6, 744(fp)<br>    |
| 376|[0x800130c8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x57ea20 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80003c64]:fmul.s t6, t5, t4, dyn<br> [0x80003c68]:csrrs a2, fcsr, zero<br> [0x80003c6c]:sw t6, 752(fp)<br>    |
| 377|[0x800130d0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x57ea20 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80003ca4]:fmul.s t6, t5, t4, dyn<br> [0x80003ca8]:csrrs a2, fcsr, zero<br> [0x80003cac]:sw t6, 760(fp)<br>    |
| 378|[0x800130d8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x57ea20 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80003ce4]:fmul.s t6, t5, t4, dyn<br> [0x80003ce8]:csrrs a2, fcsr, zero<br> [0x80003cec]:sw t6, 768(fp)<br>    |
| 379|[0x800130e0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x57ea20 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80003d24]:fmul.s t6, t5, t4, dyn<br> [0x80003d28]:csrrs a2, fcsr, zero<br> [0x80003d2c]:sw t6, 776(fp)<br>    |
| 380|[0x800130e8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x57ea20 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80003d64]:fmul.s t6, t5, t4, dyn<br> [0x80003d68]:csrrs a2, fcsr, zero<br> [0x80003d6c]:sw t6, 784(fp)<br>    |
| 381|[0x800130f0]<br>0x80000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x4e2b68 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80003da4]:fmul.s t6, t5, t4, dyn<br> [0x80003da8]:csrrs a2, fcsr, zero<br> [0x80003dac]:sw t6, 792(fp)<br>    |
| 382|[0x800130f8]<br>0x80000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x4e2b68 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80003de4]:fmul.s t6, t5, t4, dyn<br> [0x80003de8]:csrrs a2, fcsr, zero<br> [0x80003dec]:sw t6, 800(fp)<br>    |
| 383|[0x80013100]<br>0x80000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x4e2b68 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80003e24]:fmul.s t6, t5, t4, dyn<br> [0x80003e28]:csrrs a2, fcsr, zero<br> [0x80003e2c]:sw t6, 808(fp)<br>    |
| 384|[0x80013108]<br>0x80000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x4e2b68 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80003e64]:fmul.s t6, t5, t4, dyn<br> [0x80003e68]:csrrs a2, fcsr, zero<br> [0x80003e6c]:sw t6, 816(fp)<br>    |
| 385|[0x80013110]<br>0x80000000|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x4e2b68 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80003ea4]:fmul.s t6, t5, t4, dyn<br> [0x80003ea8]:csrrs a2, fcsr, zero<br> [0x80003eac]:sw t6, 824(fp)<br>    |
| 386|[0x80013118]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x134261 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80003ee4]:fmul.s t6, t5, t4, dyn<br> [0x80003ee8]:csrrs a2, fcsr, zero<br> [0x80003eec]:sw t6, 832(fp)<br>    |
| 387|[0x80013120]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x134261 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80003f24]:fmul.s t6, t5, t4, dyn<br> [0x80003f28]:csrrs a2, fcsr, zero<br> [0x80003f2c]:sw t6, 840(fp)<br>    |
| 388|[0x80013128]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x134261 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80003f64]:fmul.s t6, t5, t4, dyn<br> [0x80003f68]:csrrs a2, fcsr, zero<br> [0x80003f6c]:sw t6, 848(fp)<br>    |
| 389|[0x80013130]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x134261 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80003fa4]:fmul.s t6, t5, t4, dyn<br> [0x80003fa8]:csrrs a2, fcsr, zero<br> [0x80003fac]:sw t6, 856(fp)<br>    |
| 390|[0x80013138]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x134261 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80003fe4]:fmul.s t6, t5, t4, dyn<br> [0x80003fe8]:csrrs a2, fcsr, zero<br> [0x80003fec]:sw t6, 864(fp)<br>    |
| 391|[0x80013140]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x432be8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004024]:fmul.s t6, t5, t4, dyn<br> [0x80004028]:csrrs a2, fcsr, zero<br> [0x8000402c]:sw t6, 872(fp)<br>    |
| 392|[0x80013148]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x432be8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80004064]:fmul.s t6, t5, t4, dyn<br> [0x80004068]:csrrs a2, fcsr, zero<br> [0x8000406c]:sw t6, 880(fp)<br>    |
| 393|[0x80013150]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x432be8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800040a4]:fmul.s t6, t5, t4, dyn<br> [0x800040a8]:csrrs a2, fcsr, zero<br> [0x800040ac]:sw t6, 888(fp)<br>    |
| 394|[0x80013158]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x432be8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800040e4]:fmul.s t6, t5, t4, dyn<br> [0x800040e8]:csrrs a2, fcsr, zero<br> [0x800040ec]:sw t6, 896(fp)<br>    |
| 395|[0x80013160]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x432be8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80004124]:fmul.s t6, t5, t4, dyn<br> [0x80004128]:csrrs a2, fcsr, zero<br> [0x8000412c]:sw t6, 904(fp)<br>    |
| 396|[0x80013168]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x335a5f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004164]:fmul.s t6, t5, t4, dyn<br> [0x80004168]:csrrs a2, fcsr, zero<br> [0x8000416c]:sw t6, 912(fp)<br>    |
| 397|[0x80013170]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x335a5f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800041a4]:fmul.s t6, t5, t4, dyn<br> [0x800041a8]:csrrs a2, fcsr, zero<br> [0x800041ac]:sw t6, 920(fp)<br>    |
| 398|[0x80013178]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x335a5f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800041e4]:fmul.s t6, t5, t4, dyn<br> [0x800041e8]:csrrs a2, fcsr, zero<br> [0x800041ec]:sw t6, 928(fp)<br>    |
| 399|[0x80013180]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x335a5f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80004224]:fmul.s t6, t5, t4, dyn<br> [0x80004228]:csrrs a2, fcsr, zero<br> [0x8000422c]:sw t6, 936(fp)<br>    |
| 400|[0x80013188]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x335a5f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80004264]:fmul.s t6, t5, t4, dyn<br> [0x80004268]:csrrs a2, fcsr, zero<br> [0x8000426c]:sw t6, 944(fp)<br>    |
| 401|[0x80013190]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x328a37 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800042a4]:fmul.s t6, t5, t4, dyn<br> [0x800042a8]:csrrs a2, fcsr, zero<br> [0x800042ac]:sw t6, 952(fp)<br>    |
| 402|[0x80013198]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x328a37 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800042e4]:fmul.s t6, t5, t4, dyn<br> [0x800042e8]:csrrs a2, fcsr, zero<br> [0x800042ec]:sw t6, 960(fp)<br>    |
| 403|[0x800131a0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x328a37 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80004324]:fmul.s t6, t5, t4, dyn<br> [0x80004328]:csrrs a2, fcsr, zero<br> [0x8000432c]:sw t6, 968(fp)<br>    |
| 404|[0x800131a8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x328a37 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80004364]:fmul.s t6, t5, t4, dyn<br> [0x80004368]:csrrs a2, fcsr, zero<br> [0x8000436c]:sw t6, 976(fp)<br>    |
| 405|[0x800131b0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x328a37 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800043a4]:fmul.s t6, t5, t4, dyn<br> [0x800043a8]:csrrs a2, fcsr, zero<br> [0x800043ac]:sw t6, 984(fp)<br>    |
| 406|[0x800131b8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x409980 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800043e4]:fmul.s t6, t5, t4, dyn<br> [0x800043e8]:csrrs a2, fcsr, zero<br> [0x800043ec]:sw t6, 992(fp)<br>    |
| 407|[0x800131c0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x409980 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80004424]:fmul.s t6, t5, t4, dyn<br> [0x80004428]:csrrs a2, fcsr, zero<br> [0x8000442c]:sw t6, 1000(fp)<br>   |
| 408|[0x800131c8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x409980 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80004464]:fmul.s t6, t5, t4, dyn<br> [0x80004468]:csrrs a2, fcsr, zero<br> [0x8000446c]:sw t6, 1008(fp)<br>   |
| 409|[0x800131d0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x409980 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800044a4]:fmul.s t6, t5, t4, dyn<br> [0x800044a8]:csrrs a2, fcsr, zero<br> [0x800044ac]:sw t6, 1016(fp)<br>   |
| 410|[0x800131d8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x409980 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800044ec]:fmul.s t6, t5, t4, dyn<br> [0x800044f0]:csrrs a2, fcsr, zero<br> [0x800044f4]:sw t6, 0(fp)<br>      |
| 411|[0x800131e0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6fadd2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000452c]:fmul.s t6, t5, t4, dyn<br> [0x80004530]:csrrs a2, fcsr, zero<br> [0x80004534]:sw t6, 8(fp)<br>      |
| 412|[0x800131e8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6fadd2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000456c]:fmul.s t6, t5, t4, dyn<br> [0x80004570]:csrrs a2, fcsr, zero<br> [0x80004574]:sw t6, 16(fp)<br>     |
| 413|[0x800131f0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6fadd2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800045ac]:fmul.s t6, t5, t4, dyn<br> [0x800045b0]:csrrs a2, fcsr, zero<br> [0x800045b4]:sw t6, 24(fp)<br>     |
| 414|[0x800131f8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6fadd2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800045ec]:fmul.s t6, t5, t4, dyn<br> [0x800045f0]:csrrs a2, fcsr, zero<br> [0x800045f4]:sw t6, 32(fp)<br>     |
| 415|[0x80013200]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6fadd2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000462c]:fmul.s t6, t5, t4, dyn<br> [0x80004630]:csrrs a2, fcsr, zero<br> [0x80004634]:sw t6, 40(fp)<br>     |
| 416|[0x80013208]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x083942 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000466c]:fmul.s t6, t5, t4, dyn<br> [0x80004670]:csrrs a2, fcsr, zero<br> [0x80004674]:sw t6, 48(fp)<br>     |
| 417|[0x80013210]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x083942 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800046ac]:fmul.s t6, t5, t4, dyn<br> [0x800046b0]:csrrs a2, fcsr, zero<br> [0x800046b4]:sw t6, 56(fp)<br>     |
| 418|[0x80013218]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x083942 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800046ec]:fmul.s t6, t5, t4, dyn<br> [0x800046f0]:csrrs a2, fcsr, zero<br> [0x800046f4]:sw t6, 64(fp)<br>     |
| 419|[0x80013220]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x083942 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000472c]:fmul.s t6, t5, t4, dyn<br> [0x80004730]:csrrs a2, fcsr, zero<br> [0x80004734]:sw t6, 72(fp)<br>     |
| 420|[0x80013228]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x083942 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000476c]:fmul.s t6, t5, t4, dyn<br> [0x80004770]:csrrs a2, fcsr, zero<br> [0x80004774]:sw t6, 80(fp)<br>     |
| 421|[0x80013230]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x227041 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800047ac]:fmul.s t6, t5, t4, dyn<br> [0x800047b0]:csrrs a2, fcsr, zero<br> [0x800047b4]:sw t6, 88(fp)<br>     |
| 422|[0x80013238]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x227041 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800047ec]:fmul.s t6, t5, t4, dyn<br> [0x800047f0]:csrrs a2, fcsr, zero<br> [0x800047f4]:sw t6, 96(fp)<br>     |
| 423|[0x80013240]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x227041 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000482c]:fmul.s t6, t5, t4, dyn<br> [0x80004830]:csrrs a2, fcsr, zero<br> [0x80004834]:sw t6, 104(fp)<br>    |
| 424|[0x80013248]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x227041 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000486c]:fmul.s t6, t5, t4, dyn<br> [0x80004870]:csrrs a2, fcsr, zero<br> [0x80004874]:sw t6, 112(fp)<br>    |
| 425|[0x80013250]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x227041 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800048ac]:fmul.s t6, t5, t4, dyn<br> [0x800048b0]:csrrs a2, fcsr, zero<br> [0x800048b4]:sw t6, 120(fp)<br>    |
| 426|[0x80013258]<br>0x00000000|- fs1 == 0 and fe1 == 0xf6 and fm1 == 0x6d25cb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800048ec]:fmul.s t6, t5, t4, dyn<br> [0x800048f0]:csrrs a2, fcsr, zero<br> [0x800048f4]:sw t6, 128(fp)<br>    |
| 427|[0x80013260]<br>0x00000000|- fs1 == 0 and fe1 == 0xf6 and fm1 == 0x6d25cb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000492c]:fmul.s t6, t5, t4, dyn<br> [0x80004930]:csrrs a2, fcsr, zero<br> [0x80004934]:sw t6, 136(fp)<br>    |
| 428|[0x80013268]<br>0x00000000|- fs1 == 0 and fe1 == 0xf6 and fm1 == 0x6d25cb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000496c]:fmul.s t6, t5, t4, dyn<br> [0x80004970]:csrrs a2, fcsr, zero<br> [0x80004974]:sw t6, 144(fp)<br>    |
| 429|[0x80013270]<br>0x00000000|- fs1 == 0 and fe1 == 0xf6 and fm1 == 0x6d25cb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800049ac]:fmul.s t6, t5, t4, dyn<br> [0x800049b0]:csrrs a2, fcsr, zero<br> [0x800049b4]:sw t6, 152(fp)<br>    |
| 430|[0x80013278]<br>0x00000000|- fs1 == 0 and fe1 == 0xf6 and fm1 == 0x6d25cb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800049ec]:fmul.s t6, t5, t4, dyn<br> [0x800049f0]:csrrs a2, fcsr, zero<br> [0x800049f4]:sw t6, 160(fp)<br>    |
| 431|[0x80013280]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x1cf370 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004a2c]:fmul.s t6, t5, t4, dyn<br> [0x80004a30]:csrrs a2, fcsr, zero<br> [0x80004a34]:sw t6, 168(fp)<br>    |
| 432|[0x80013288]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x1cf370 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80004a6c]:fmul.s t6, t5, t4, dyn<br> [0x80004a70]:csrrs a2, fcsr, zero<br> [0x80004a74]:sw t6, 176(fp)<br>    |
| 433|[0x80013290]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x1cf370 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80004aac]:fmul.s t6, t5, t4, dyn<br> [0x80004ab0]:csrrs a2, fcsr, zero<br> [0x80004ab4]:sw t6, 184(fp)<br>    |
| 434|[0x80013298]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x1cf370 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80004aec]:fmul.s t6, t5, t4, dyn<br> [0x80004af0]:csrrs a2, fcsr, zero<br> [0x80004af4]:sw t6, 192(fp)<br>    |
| 435|[0x800132a0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x1cf370 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80004b2c]:fmul.s t6, t5, t4, dyn<br> [0x80004b30]:csrrs a2, fcsr, zero<br> [0x80004b34]:sw t6, 200(fp)<br>    |
| 436|[0x800132a8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0af6b9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004b6c]:fmul.s t6, t5, t4, dyn<br> [0x80004b70]:csrrs a2, fcsr, zero<br> [0x80004b74]:sw t6, 208(fp)<br>    |
| 437|[0x800132b0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0af6b9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80004bac]:fmul.s t6, t5, t4, dyn<br> [0x80004bb0]:csrrs a2, fcsr, zero<br> [0x80004bb4]:sw t6, 216(fp)<br>    |
| 438|[0x800132b8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0af6b9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80004bec]:fmul.s t6, t5, t4, dyn<br> [0x80004bf0]:csrrs a2, fcsr, zero<br> [0x80004bf4]:sw t6, 224(fp)<br>    |
| 439|[0x800132c0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0af6b9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80004c2c]:fmul.s t6, t5, t4, dyn<br> [0x80004c30]:csrrs a2, fcsr, zero<br> [0x80004c34]:sw t6, 232(fp)<br>    |
| 440|[0x800132c8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0af6b9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80004c6c]:fmul.s t6, t5, t4, dyn<br> [0x80004c70]:csrrs a2, fcsr, zero<br> [0x80004c74]:sw t6, 240(fp)<br>    |
| 441|[0x800132d0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x19405f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004cac]:fmul.s t6, t5, t4, dyn<br> [0x80004cb0]:csrrs a2, fcsr, zero<br> [0x80004cb4]:sw t6, 248(fp)<br>    |
| 442|[0x800132d8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x19405f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80004cec]:fmul.s t6, t5, t4, dyn<br> [0x80004cf0]:csrrs a2, fcsr, zero<br> [0x80004cf4]:sw t6, 256(fp)<br>    |
| 443|[0x800132e0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x19405f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80004d2c]:fmul.s t6, t5, t4, dyn<br> [0x80004d30]:csrrs a2, fcsr, zero<br> [0x80004d34]:sw t6, 264(fp)<br>    |
| 444|[0x800132e8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x19405f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80004d6c]:fmul.s t6, t5, t4, dyn<br> [0x80004d70]:csrrs a2, fcsr, zero<br> [0x80004d74]:sw t6, 272(fp)<br>    |
| 445|[0x800132f0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x19405f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80004dac]:fmul.s t6, t5, t4, dyn<br> [0x80004db0]:csrrs a2, fcsr, zero<br> [0x80004db4]:sw t6, 280(fp)<br>    |
| 446|[0x800132f8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x31cfbf and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004dec]:fmul.s t6, t5, t4, dyn<br> [0x80004df0]:csrrs a2, fcsr, zero<br> [0x80004df4]:sw t6, 288(fp)<br>    |
| 447|[0x80013300]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x31cfbf and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80004e2c]:fmul.s t6, t5, t4, dyn<br> [0x80004e30]:csrrs a2, fcsr, zero<br> [0x80004e34]:sw t6, 296(fp)<br>    |
| 448|[0x80013308]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x31cfbf and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80004e6c]:fmul.s t6, t5, t4, dyn<br> [0x80004e70]:csrrs a2, fcsr, zero<br> [0x80004e74]:sw t6, 304(fp)<br>    |
| 449|[0x80013310]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x31cfbf and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80004eac]:fmul.s t6, t5, t4, dyn<br> [0x80004eb0]:csrrs a2, fcsr, zero<br> [0x80004eb4]:sw t6, 312(fp)<br>    |
| 450|[0x80013318]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x31cfbf and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80004eec]:fmul.s t6, t5, t4, dyn<br> [0x80004ef0]:csrrs a2, fcsr, zero<br> [0x80004ef4]:sw t6, 320(fp)<br>    |
| 451|[0x80013320]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x47f677 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004f2c]:fmul.s t6, t5, t4, dyn<br> [0x80004f30]:csrrs a2, fcsr, zero<br> [0x80004f34]:sw t6, 328(fp)<br>    |
| 452|[0x80013328]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x47f677 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80004f6c]:fmul.s t6, t5, t4, dyn<br> [0x80004f70]:csrrs a2, fcsr, zero<br> [0x80004f74]:sw t6, 336(fp)<br>    |
| 453|[0x80013330]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x47f677 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80004fac]:fmul.s t6, t5, t4, dyn<br> [0x80004fb0]:csrrs a2, fcsr, zero<br> [0x80004fb4]:sw t6, 344(fp)<br>    |
| 454|[0x80013338]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x47f677 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80004fec]:fmul.s t6, t5, t4, dyn<br> [0x80004ff0]:csrrs a2, fcsr, zero<br> [0x80004ff4]:sw t6, 352(fp)<br>    |
| 455|[0x80013340]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x47f677 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000502c]:fmul.s t6, t5, t4, dyn<br> [0x80005030]:csrrs a2, fcsr, zero<br> [0x80005034]:sw t6, 360(fp)<br>    |
| 456|[0x80013348]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x6dde9e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000506c]:fmul.s t6, t5, t4, dyn<br> [0x80005070]:csrrs a2, fcsr, zero<br> [0x80005074]:sw t6, 368(fp)<br>    |
| 457|[0x80013350]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x6dde9e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800050ac]:fmul.s t6, t5, t4, dyn<br> [0x800050b0]:csrrs a2, fcsr, zero<br> [0x800050b4]:sw t6, 376(fp)<br>    |
| 458|[0x80013358]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x6dde9e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800050ec]:fmul.s t6, t5, t4, dyn<br> [0x800050f0]:csrrs a2, fcsr, zero<br> [0x800050f4]:sw t6, 384(fp)<br>    |
| 459|[0x80013360]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x6dde9e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000512c]:fmul.s t6, t5, t4, dyn<br> [0x80005130]:csrrs a2, fcsr, zero<br> [0x80005134]:sw t6, 392(fp)<br>    |
| 460|[0x80013368]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x6dde9e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000516c]:fmul.s t6, t5, t4, dyn<br> [0x80005170]:csrrs a2, fcsr, zero<br> [0x80005174]:sw t6, 400(fp)<br>    |
| 461|[0x80013370]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x232951 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800051ac]:fmul.s t6, t5, t4, dyn<br> [0x800051b0]:csrrs a2, fcsr, zero<br> [0x800051b4]:sw t6, 408(fp)<br>    |
| 462|[0x80013378]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x232951 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800051ec]:fmul.s t6, t5, t4, dyn<br> [0x800051f0]:csrrs a2, fcsr, zero<br> [0x800051f4]:sw t6, 416(fp)<br>    |
| 463|[0x80013380]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x232951 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000522c]:fmul.s t6, t5, t4, dyn<br> [0x80005230]:csrrs a2, fcsr, zero<br> [0x80005234]:sw t6, 424(fp)<br>    |
| 464|[0x80013388]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x232951 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000526c]:fmul.s t6, t5, t4, dyn<br> [0x80005270]:csrrs a2, fcsr, zero<br> [0x80005274]:sw t6, 432(fp)<br>    |
| 465|[0x80013390]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x232951 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800052ac]:fmul.s t6, t5, t4, dyn<br> [0x800052b0]:csrrs a2, fcsr, zero<br> [0x800052b4]:sw t6, 440(fp)<br>    |
| 466|[0x80013398]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x38eb1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800052ec]:fmul.s t6, t5, t4, dyn<br> [0x800052f0]:csrrs a2, fcsr, zero<br> [0x800052f4]:sw t6, 448(fp)<br>    |
| 467|[0x800133a0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x38eb1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000532c]:fmul.s t6, t5, t4, dyn<br> [0x80005330]:csrrs a2, fcsr, zero<br> [0x80005334]:sw t6, 456(fp)<br>    |
| 468|[0x800133a8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x38eb1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000536c]:fmul.s t6, t5, t4, dyn<br> [0x80005370]:csrrs a2, fcsr, zero<br> [0x80005374]:sw t6, 464(fp)<br>    |
| 469|[0x800133b0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x38eb1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800053ac]:fmul.s t6, t5, t4, dyn<br> [0x800053b0]:csrrs a2, fcsr, zero<br> [0x800053b4]:sw t6, 472(fp)<br>    |
| 470|[0x800133b8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x38eb1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800053ec]:fmul.s t6, t5, t4, dyn<br> [0x800053f0]:csrrs a2, fcsr, zero<br> [0x800053f4]:sw t6, 480(fp)<br>    |
| 471|[0x800133c0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x268dc5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000542c]:fmul.s t6, t5, t4, dyn<br> [0x80005430]:csrrs a2, fcsr, zero<br> [0x80005434]:sw t6, 488(fp)<br>    |
| 472|[0x800133c8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x268dc5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000546c]:fmul.s t6, t5, t4, dyn<br> [0x80005470]:csrrs a2, fcsr, zero<br> [0x80005474]:sw t6, 496(fp)<br>    |
| 473|[0x800133d0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x268dc5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800054ac]:fmul.s t6, t5, t4, dyn<br> [0x800054b0]:csrrs a2, fcsr, zero<br> [0x800054b4]:sw t6, 504(fp)<br>    |
| 474|[0x800133d8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x268dc5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800054ec]:fmul.s t6, t5, t4, dyn<br> [0x800054f0]:csrrs a2, fcsr, zero<br> [0x800054f4]:sw t6, 512(fp)<br>    |
| 475|[0x800133e0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x268dc5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000552c]:fmul.s t6, t5, t4, dyn<br> [0x80005530]:csrrs a2, fcsr, zero<br> [0x80005534]:sw t6, 520(fp)<br>    |
| 476|[0x800133e8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x5f221f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000556c]:fmul.s t6, t5, t4, dyn<br> [0x80005570]:csrrs a2, fcsr, zero<br> [0x80005574]:sw t6, 528(fp)<br>    |
| 477|[0x800133f0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x5f221f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800055ac]:fmul.s t6, t5, t4, dyn<br> [0x800055b0]:csrrs a2, fcsr, zero<br> [0x800055b4]:sw t6, 536(fp)<br>    |
| 478|[0x800133f8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x5f221f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800055ec]:fmul.s t6, t5, t4, dyn<br> [0x800055f0]:csrrs a2, fcsr, zero<br> [0x800055f4]:sw t6, 544(fp)<br>    |
| 479|[0x80013400]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x5f221f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000562c]:fmul.s t6, t5, t4, dyn<br> [0x80005630]:csrrs a2, fcsr, zero<br> [0x80005634]:sw t6, 552(fp)<br>    |
| 480|[0x80013408]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x5f221f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000566c]:fmul.s t6, t5, t4, dyn<br> [0x80005670]:csrrs a2, fcsr, zero<br> [0x80005674]:sw t6, 560(fp)<br>    |
| 481|[0x80013410]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2084ae and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800056ac]:fmul.s t6, t5, t4, dyn<br> [0x800056b0]:csrrs a2, fcsr, zero<br> [0x800056b4]:sw t6, 568(fp)<br>    |
| 482|[0x80013418]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2084ae and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800056ec]:fmul.s t6, t5, t4, dyn<br> [0x800056f0]:csrrs a2, fcsr, zero<br> [0x800056f4]:sw t6, 576(fp)<br>    |
| 483|[0x80013420]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2084ae and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000572c]:fmul.s t6, t5, t4, dyn<br> [0x80005730]:csrrs a2, fcsr, zero<br> [0x80005734]:sw t6, 584(fp)<br>    |
| 484|[0x80013428]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2084ae and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000576c]:fmul.s t6, t5, t4, dyn<br> [0x80005770]:csrrs a2, fcsr, zero<br> [0x80005774]:sw t6, 592(fp)<br>    |
| 485|[0x80013430]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2084ae and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800057ac]:fmul.s t6, t5, t4, dyn<br> [0x800057b0]:csrrs a2, fcsr, zero<br> [0x800057b4]:sw t6, 600(fp)<br>    |
| 486|[0x80013438]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x0b2e4b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800057ec]:fmul.s t6, t5, t4, dyn<br> [0x800057f0]:csrrs a2, fcsr, zero<br> [0x800057f4]:sw t6, 608(fp)<br>    |
| 487|[0x80013440]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x0b2e4b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000582c]:fmul.s t6, t5, t4, dyn<br> [0x80005830]:csrrs a2, fcsr, zero<br> [0x80005834]:sw t6, 616(fp)<br>    |
| 488|[0x80013448]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x0b2e4b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000586c]:fmul.s t6, t5, t4, dyn<br> [0x80005870]:csrrs a2, fcsr, zero<br> [0x80005874]:sw t6, 624(fp)<br>    |
| 489|[0x80013450]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x0b2e4b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800058ac]:fmul.s t6, t5, t4, dyn<br> [0x800058b0]:csrrs a2, fcsr, zero<br> [0x800058b4]:sw t6, 632(fp)<br>    |
| 490|[0x80013458]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x0b2e4b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800058ec]:fmul.s t6, t5, t4, dyn<br> [0x800058f0]:csrrs a2, fcsr, zero<br> [0x800058f4]:sw t6, 640(fp)<br>    |
| 491|[0x80013460]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a1c1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000592c]:fmul.s t6, t5, t4, dyn<br> [0x80005930]:csrrs a2, fcsr, zero<br> [0x80005934]:sw t6, 648(fp)<br>    |
| 492|[0x80013468]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a1c1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000596c]:fmul.s t6, t5, t4, dyn<br> [0x80005970]:csrrs a2, fcsr, zero<br> [0x80005974]:sw t6, 656(fp)<br>    |
| 493|[0x80013470]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a1c1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800059ac]:fmul.s t6, t5, t4, dyn<br> [0x800059b0]:csrrs a2, fcsr, zero<br> [0x800059b4]:sw t6, 664(fp)<br>    |
| 494|[0x80013478]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a1c1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800059ec]:fmul.s t6, t5, t4, dyn<br> [0x800059f0]:csrrs a2, fcsr, zero<br> [0x800059f4]:sw t6, 672(fp)<br>    |
| 495|[0x80013480]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a1c1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80005a2c]:fmul.s t6, t5, t4, dyn<br> [0x80005a30]:csrrs a2, fcsr, zero<br> [0x80005a34]:sw t6, 680(fp)<br>    |
| 496|[0x80013488]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x476063 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80005a6c]:fmul.s t6, t5, t4, dyn<br> [0x80005a70]:csrrs a2, fcsr, zero<br> [0x80005a74]:sw t6, 688(fp)<br>    |
| 497|[0x80013490]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x476063 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80005aac]:fmul.s t6, t5, t4, dyn<br> [0x80005ab0]:csrrs a2, fcsr, zero<br> [0x80005ab4]:sw t6, 696(fp)<br>    |
| 498|[0x80013498]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x476063 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80005aec]:fmul.s t6, t5, t4, dyn<br> [0x80005af0]:csrrs a2, fcsr, zero<br> [0x80005af4]:sw t6, 704(fp)<br>    |
| 499|[0x800134a0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x476063 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80005b2c]:fmul.s t6, t5, t4, dyn<br> [0x80005b30]:csrrs a2, fcsr, zero<br> [0x80005b34]:sw t6, 712(fp)<br>    |
| 500|[0x800134a8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x476063 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80005b6c]:fmul.s t6, t5, t4, dyn<br> [0x80005b70]:csrrs a2, fcsr, zero<br> [0x80005b74]:sw t6, 720(fp)<br>    |
| 501|[0x800134b0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6a6f9b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80005bac]:fmul.s t6, t5, t4, dyn<br> [0x80005bb0]:csrrs a2, fcsr, zero<br> [0x80005bb4]:sw t6, 728(fp)<br>    |
| 502|[0x800134b8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6a6f9b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80005bec]:fmul.s t6, t5, t4, dyn<br> [0x80005bf0]:csrrs a2, fcsr, zero<br> [0x80005bf4]:sw t6, 736(fp)<br>    |
| 503|[0x800134c0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6a6f9b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80005c2c]:fmul.s t6, t5, t4, dyn<br> [0x80005c30]:csrrs a2, fcsr, zero<br> [0x80005c34]:sw t6, 744(fp)<br>    |
| 504|[0x800134c8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6a6f9b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80005c6c]:fmul.s t6, t5, t4, dyn<br> [0x80005c70]:csrrs a2, fcsr, zero<br> [0x80005c74]:sw t6, 752(fp)<br>    |
| 505|[0x800134d0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6a6f9b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80005cac]:fmul.s t6, t5, t4, dyn<br> [0x80005cb0]:csrrs a2, fcsr, zero<br> [0x80005cb4]:sw t6, 760(fp)<br>    |
| 506|[0x800134d8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3cd606 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80005cec]:fmul.s t6, t5, t4, dyn<br> [0x80005cf0]:csrrs a2, fcsr, zero<br> [0x80005cf4]:sw t6, 768(fp)<br>    |
| 507|[0x800134e0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3cd606 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80005d2c]:fmul.s t6, t5, t4, dyn<br> [0x80005d30]:csrrs a2, fcsr, zero<br> [0x80005d34]:sw t6, 776(fp)<br>    |
| 508|[0x800134e8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3cd606 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80005d6c]:fmul.s t6, t5, t4, dyn<br> [0x80005d70]:csrrs a2, fcsr, zero<br> [0x80005d74]:sw t6, 784(fp)<br>    |
| 509|[0x800134f0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3cd606 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80005dac]:fmul.s t6, t5, t4, dyn<br> [0x80005db0]:csrrs a2, fcsr, zero<br> [0x80005db4]:sw t6, 792(fp)<br>    |
| 510|[0x800134f8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3cd606 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80005dec]:fmul.s t6, t5, t4, dyn<br> [0x80005df0]:csrrs a2, fcsr, zero<br> [0x80005df4]:sw t6, 800(fp)<br>    |
| 511|[0x80013500]<br>0x80000000|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x6a262c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80005e2c]:fmul.s t6, t5, t4, dyn<br> [0x80005e30]:csrrs a2, fcsr, zero<br> [0x80005e34]:sw t6, 808(fp)<br>    |
| 512|[0x80013508]<br>0x80000000|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x6a262c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80005e6c]:fmul.s t6, t5, t4, dyn<br> [0x80005e70]:csrrs a2, fcsr, zero<br> [0x80005e74]:sw t6, 816(fp)<br>    |
| 513|[0x80013510]<br>0x80000000|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x6a262c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80005eac]:fmul.s t6, t5, t4, dyn<br> [0x80005eb0]:csrrs a2, fcsr, zero<br> [0x80005eb4]:sw t6, 824(fp)<br>    |
| 514|[0x80013518]<br>0x80000000|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x6a262c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80005eec]:fmul.s t6, t5, t4, dyn<br> [0x80005ef0]:csrrs a2, fcsr, zero<br> [0x80005ef4]:sw t6, 832(fp)<br>    |
| 515|[0x80013520]<br>0x80000000|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x6a262c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80005f2c]:fmul.s t6, t5, t4, dyn<br> [0x80005f30]:csrrs a2, fcsr, zero<br> [0x80005f34]:sw t6, 840(fp)<br>    |
| 516|[0x80013528]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x1b6277 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80005f6c]:fmul.s t6, t5, t4, dyn<br> [0x80005f70]:csrrs a2, fcsr, zero<br> [0x80005f74]:sw t6, 848(fp)<br>    |
| 517|[0x80013530]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x1b6277 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80005fac]:fmul.s t6, t5, t4, dyn<br> [0x80005fb0]:csrrs a2, fcsr, zero<br> [0x80005fb4]:sw t6, 856(fp)<br>    |
| 518|[0x80013538]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x1b6277 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80005fec]:fmul.s t6, t5, t4, dyn<br> [0x80005ff0]:csrrs a2, fcsr, zero<br> [0x80005ff4]:sw t6, 864(fp)<br>    |
| 519|[0x80013540]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x1b6277 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000602c]:fmul.s t6, t5, t4, dyn<br> [0x80006030]:csrrs a2, fcsr, zero<br> [0x80006034]:sw t6, 872(fp)<br>    |
| 520|[0x80013548]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x1b6277 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000606c]:fmul.s t6, t5, t4, dyn<br> [0x80006070]:csrrs a2, fcsr, zero<br> [0x80006074]:sw t6, 880(fp)<br>    |
| 521|[0x80013550]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2165be and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800060ac]:fmul.s t6, t5, t4, dyn<br> [0x800060b0]:csrrs a2, fcsr, zero<br> [0x800060b4]:sw t6, 888(fp)<br>    |
| 522|[0x80013558]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2165be and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800060ec]:fmul.s t6, t5, t4, dyn<br> [0x800060f0]:csrrs a2, fcsr, zero<br> [0x800060f4]:sw t6, 896(fp)<br>    |
| 523|[0x80013560]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2165be and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000612c]:fmul.s t6, t5, t4, dyn<br> [0x80006130]:csrrs a2, fcsr, zero<br> [0x80006134]:sw t6, 904(fp)<br>    |
| 524|[0x80013568]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2165be and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000616c]:fmul.s t6, t5, t4, dyn<br> [0x80006170]:csrrs a2, fcsr, zero<br> [0x80006174]:sw t6, 912(fp)<br>    |
| 525|[0x80013570]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2165be and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800061ac]:fmul.s t6, t5, t4, dyn<br> [0x800061b0]:csrrs a2, fcsr, zero<br> [0x800061b4]:sw t6, 920(fp)<br>    |
| 526|[0x80013578]<br>0x00000000|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x41315c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800061ec]:fmul.s t6, t5, t4, dyn<br> [0x800061f0]:csrrs a2, fcsr, zero<br> [0x800061f4]:sw t6, 928(fp)<br>    |
| 527|[0x80013580]<br>0x00000000|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x41315c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000622c]:fmul.s t6, t5, t4, dyn<br> [0x80006230]:csrrs a2, fcsr, zero<br> [0x80006234]:sw t6, 936(fp)<br>    |
| 528|[0x80013588]<br>0x00000000|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x41315c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000626c]:fmul.s t6, t5, t4, dyn<br> [0x80006270]:csrrs a2, fcsr, zero<br> [0x80006274]:sw t6, 944(fp)<br>    |
| 529|[0x80013590]<br>0x00000000|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x41315c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800062ac]:fmul.s t6, t5, t4, dyn<br> [0x800062b0]:csrrs a2, fcsr, zero<br> [0x800062b4]:sw t6, 952(fp)<br>    |
| 530|[0x80013598]<br>0x00000000|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x41315c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800062ec]:fmul.s t6, t5, t4, dyn<br> [0x800062f0]:csrrs a2, fcsr, zero<br> [0x800062f4]:sw t6, 960(fp)<br>    |
| 531|[0x800135a0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x1db2ee and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000632c]:fmul.s t6, t5, t4, dyn<br> [0x80006330]:csrrs a2, fcsr, zero<br> [0x80006334]:sw t6, 968(fp)<br>    |
| 532|[0x800135a8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x1db2ee and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000636c]:fmul.s t6, t5, t4, dyn<br> [0x80006370]:csrrs a2, fcsr, zero<br> [0x80006374]:sw t6, 976(fp)<br>    |
| 533|[0x800135b0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x1db2ee and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800063ac]:fmul.s t6, t5, t4, dyn<br> [0x800063b0]:csrrs a2, fcsr, zero<br> [0x800063b4]:sw t6, 984(fp)<br>    |
| 534|[0x800135b8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x1db2ee and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800063ec]:fmul.s t6, t5, t4, dyn<br> [0x800063f0]:csrrs a2, fcsr, zero<br> [0x800063f4]:sw t6, 992(fp)<br>    |
| 535|[0x800135c0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x1db2ee and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80006424]:fmul.s t6, t5, t4, dyn<br> [0x80006428]:csrrs a2, fcsr, zero<br> [0x8000642c]:sw t6, 1000(fp)<br>   |
| 536|[0x800135c8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x1fef00 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000645c]:fmul.s t6, t5, t4, dyn<br> [0x80006460]:csrrs a2, fcsr, zero<br> [0x80006464]:sw t6, 1008(fp)<br>   |
| 537|[0x800135d0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x1fef00 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80006494]:fmul.s t6, t5, t4, dyn<br> [0x80006498]:csrrs a2, fcsr, zero<br> [0x8000649c]:sw t6, 1016(fp)<br>   |
| 538|[0x800135d8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x1fef00 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800064d4]:fmul.s t6, t5, t4, dyn<br> [0x800064d8]:csrrs a2, fcsr, zero<br> [0x800064dc]:sw t6, 0(fp)<br>      |
| 539|[0x800135e0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x1fef00 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000650c]:fmul.s t6, t5, t4, dyn<br> [0x80006510]:csrrs a2, fcsr, zero<br> [0x80006514]:sw t6, 8(fp)<br>      |
| 540|[0x800135e8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x1fef00 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80006544]:fmul.s t6, t5, t4, dyn<br> [0x80006548]:csrrs a2, fcsr, zero<br> [0x8000654c]:sw t6, 16(fp)<br>     |
| 541|[0x800135f0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x62ae46 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000657c]:fmul.s t6, t5, t4, dyn<br> [0x80006580]:csrrs a2, fcsr, zero<br> [0x80006584]:sw t6, 24(fp)<br>     |
| 542|[0x800135f8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x62ae46 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800065b4]:fmul.s t6, t5, t4, dyn<br> [0x800065b8]:csrrs a2, fcsr, zero<br> [0x800065bc]:sw t6, 32(fp)<br>     |
| 543|[0x80013600]<br>0x00000000|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x62ae46 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800065ec]:fmul.s t6, t5, t4, dyn<br> [0x800065f0]:csrrs a2, fcsr, zero<br> [0x800065f4]:sw t6, 40(fp)<br>     |
| 544|[0x80013608]<br>0x00000000|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x62ae46 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80006624]:fmul.s t6, t5, t4, dyn<br> [0x80006628]:csrrs a2, fcsr, zero<br> [0x8000662c]:sw t6, 48(fp)<br>     |
| 545|[0x80013610]<br>0x00000000|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x62ae46 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000665c]:fmul.s t6, t5, t4, dyn<br> [0x80006660]:csrrs a2, fcsr, zero<br> [0x80006664]:sw t6, 56(fp)<br>     |
| 546|[0x80013618]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x0f31c4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006694]:fmul.s t6, t5, t4, dyn<br> [0x80006698]:csrrs a2, fcsr, zero<br> [0x8000669c]:sw t6, 64(fp)<br>     |
| 547|[0x80013620]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x0f31c4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800066cc]:fmul.s t6, t5, t4, dyn<br> [0x800066d0]:csrrs a2, fcsr, zero<br> [0x800066d4]:sw t6, 72(fp)<br>     |
| 548|[0x80013628]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x0f31c4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80006704]:fmul.s t6, t5, t4, dyn<br> [0x80006708]:csrrs a2, fcsr, zero<br> [0x8000670c]:sw t6, 80(fp)<br>     |
| 549|[0x80013630]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x0f31c4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000673c]:fmul.s t6, t5, t4, dyn<br> [0x80006740]:csrrs a2, fcsr, zero<br> [0x80006744]:sw t6, 88(fp)<br>     |
| 550|[0x80013638]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x0f31c4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80006774]:fmul.s t6, t5, t4, dyn<br> [0x80006778]:csrrs a2, fcsr, zero<br> [0x8000677c]:sw t6, 96(fp)<br>     |
| 551|[0x80013640]<br>0x00000000|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x6b27f7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800067ac]:fmul.s t6, t5, t4, dyn<br> [0x800067b0]:csrrs a2, fcsr, zero<br> [0x800067b4]:sw t6, 104(fp)<br>    |
| 552|[0x80013648]<br>0x00000000|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x6b27f7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800067e4]:fmul.s t6, t5, t4, dyn<br> [0x800067e8]:csrrs a2, fcsr, zero<br> [0x800067ec]:sw t6, 112(fp)<br>    |
| 553|[0x80013650]<br>0x00000000|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x6b27f7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000681c]:fmul.s t6, t5, t4, dyn<br> [0x80006820]:csrrs a2, fcsr, zero<br> [0x80006824]:sw t6, 120(fp)<br>    |
| 554|[0x80013658]<br>0x00000000|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x6b27f7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80006854]:fmul.s t6, t5, t4, dyn<br> [0x80006858]:csrrs a2, fcsr, zero<br> [0x8000685c]:sw t6, 128(fp)<br>    |
| 555|[0x80013660]<br>0x00000000|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x6b27f7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000688c]:fmul.s t6, t5, t4, dyn<br> [0x80006890]:csrrs a2, fcsr, zero<br> [0x80006894]:sw t6, 136(fp)<br>    |
| 556|[0x80013668]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x13bb57 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800068c4]:fmul.s t6, t5, t4, dyn<br> [0x800068c8]:csrrs a2, fcsr, zero<br> [0x800068cc]:sw t6, 144(fp)<br>    |
| 557|[0x80013670]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x13bb57 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800068fc]:fmul.s t6, t5, t4, dyn<br> [0x80006900]:csrrs a2, fcsr, zero<br> [0x80006904]:sw t6, 152(fp)<br>    |
| 558|[0x80013678]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x13bb57 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80006934]:fmul.s t6, t5, t4, dyn<br> [0x80006938]:csrrs a2, fcsr, zero<br> [0x8000693c]:sw t6, 160(fp)<br>    |
| 559|[0x80013680]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x13bb57 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000696c]:fmul.s t6, t5, t4, dyn<br> [0x80006970]:csrrs a2, fcsr, zero<br> [0x80006974]:sw t6, 168(fp)<br>    |
| 560|[0x80013688]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x13bb57 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800069a4]:fmul.s t6, t5, t4, dyn<br> [0x800069a8]:csrrs a2, fcsr, zero<br> [0x800069ac]:sw t6, 176(fp)<br>    |
| 561|[0x80013690]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x1671a2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800069dc]:fmul.s t6, t5, t4, dyn<br> [0x800069e0]:csrrs a2, fcsr, zero<br> [0x800069e4]:sw t6, 184(fp)<br>    |
| 562|[0x80013698]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x1671a2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80006a14]:fmul.s t6, t5, t4, dyn<br> [0x80006a18]:csrrs a2, fcsr, zero<br> [0x80006a1c]:sw t6, 192(fp)<br>    |
| 563|[0x800136a0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x1671a2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80006a4c]:fmul.s t6, t5, t4, dyn<br> [0x80006a50]:csrrs a2, fcsr, zero<br> [0x80006a54]:sw t6, 200(fp)<br>    |
| 564|[0x800136a8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x1671a2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80006a84]:fmul.s t6, t5, t4, dyn<br> [0x80006a88]:csrrs a2, fcsr, zero<br> [0x80006a8c]:sw t6, 208(fp)<br>    |
| 565|[0x800136b0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x1671a2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80006abc]:fmul.s t6, t5, t4, dyn<br> [0x80006ac0]:csrrs a2, fcsr, zero<br> [0x80006ac4]:sw t6, 216(fp)<br>    |
| 566|[0x800136b8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x10affc and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006af4]:fmul.s t6, t5, t4, dyn<br> [0x80006af8]:csrrs a2, fcsr, zero<br> [0x80006afc]:sw t6, 224(fp)<br>    |
| 567|[0x800136c0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x10affc and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80006b2c]:fmul.s t6, t5, t4, dyn<br> [0x80006b30]:csrrs a2, fcsr, zero<br> [0x80006b34]:sw t6, 232(fp)<br>    |
| 568|[0x800136c8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x10affc and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80006b64]:fmul.s t6, t5, t4, dyn<br> [0x80006b68]:csrrs a2, fcsr, zero<br> [0x80006b6c]:sw t6, 240(fp)<br>    |
| 569|[0x800136d0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x10affc and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80006b9c]:fmul.s t6, t5, t4, dyn<br> [0x80006ba0]:csrrs a2, fcsr, zero<br> [0x80006ba4]:sw t6, 248(fp)<br>    |
| 570|[0x800136d8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x10affc and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80006bd4]:fmul.s t6, t5, t4, dyn<br> [0x80006bd8]:csrrs a2, fcsr, zero<br> [0x80006bdc]:sw t6, 256(fp)<br>    |
| 571|[0x800136e0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x3af9fa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006c0c]:fmul.s t6, t5, t4, dyn<br> [0x80006c10]:csrrs a2, fcsr, zero<br> [0x80006c14]:sw t6, 264(fp)<br>    |
| 572|[0x800136e8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x3af9fa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80006c44]:fmul.s t6, t5, t4, dyn<br> [0x80006c48]:csrrs a2, fcsr, zero<br> [0x80006c4c]:sw t6, 272(fp)<br>    |
| 573|[0x800136f0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x3af9fa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80006c7c]:fmul.s t6, t5, t4, dyn<br> [0x80006c80]:csrrs a2, fcsr, zero<br> [0x80006c84]:sw t6, 280(fp)<br>    |
| 574|[0x800136f8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x3af9fa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80006cb4]:fmul.s t6, t5, t4, dyn<br> [0x80006cb8]:csrrs a2, fcsr, zero<br> [0x80006cbc]:sw t6, 288(fp)<br>    |
| 575|[0x80013700]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x3af9fa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80006cec]:fmul.s t6, t5, t4, dyn<br> [0x80006cf0]:csrrs a2, fcsr, zero<br> [0x80006cf4]:sw t6, 296(fp)<br>    |
| 576|[0x80013708]<br>0x00000000|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x775433 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006d24]:fmul.s t6, t5, t4, dyn<br> [0x80006d28]:csrrs a2, fcsr, zero<br> [0x80006d2c]:sw t6, 304(fp)<br>    |
| 577|[0x80013710]<br>0x00000000|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x775433 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80006d5c]:fmul.s t6, t5, t4, dyn<br> [0x80006d60]:csrrs a2, fcsr, zero<br> [0x80006d64]:sw t6, 312(fp)<br>    |
| 578|[0x80013718]<br>0x00000000|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x775433 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80006d94]:fmul.s t6, t5, t4, dyn<br> [0x80006d98]:csrrs a2, fcsr, zero<br> [0x80006d9c]:sw t6, 320(fp)<br>    |
| 579|[0x80013720]<br>0x00000000|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x775433 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80006dcc]:fmul.s t6, t5, t4, dyn<br> [0x80006dd0]:csrrs a2, fcsr, zero<br> [0x80006dd4]:sw t6, 328(fp)<br>    |
| 580|[0x80013728]<br>0x00000000|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x775433 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80006e04]:fmul.s t6, t5, t4, dyn<br> [0x80006e08]:csrrs a2, fcsr, zero<br> [0x80006e0c]:sw t6, 336(fp)<br>    |
| 581|[0x80013730]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x762408 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006e3c]:fmul.s t6, t5, t4, dyn<br> [0x80006e40]:csrrs a2, fcsr, zero<br> [0x80006e44]:sw t6, 344(fp)<br>    |
| 582|[0x80013738]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x762408 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80006e74]:fmul.s t6, t5, t4, dyn<br> [0x80006e78]:csrrs a2, fcsr, zero<br> [0x80006e7c]:sw t6, 352(fp)<br>    |
| 583|[0x80013740]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x762408 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80006eac]:fmul.s t6, t5, t4, dyn<br> [0x80006eb0]:csrrs a2, fcsr, zero<br> [0x80006eb4]:sw t6, 360(fp)<br>    |
| 584|[0x80013748]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x762408 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80006ee4]:fmul.s t6, t5, t4, dyn<br> [0x80006ee8]:csrrs a2, fcsr, zero<br> [0x80006eec]:sw t6, 368(fp)<br>    |
| 585|[0x80013750]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x762408 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80006f1c]:fmul.s t6, t5, t4, dyn<br> [0x80006f20]:csrrs a2, fcsr, zero<br> [0x80006f24]:sw t6, 376(fp)<br>    |
| 586|[0x80013758]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x04fd41 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006f54]:fmul.s t6, t5, t4, dyn<br> [0x80006f58]:csrrs a2, fcsr, zero<br> [0x80006f5c]:sw t6, 384(fp)<br>    |
| 587|[0x80013760]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x04fd41 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80006f8c]:fmul.s t6, t5, t4, dyn<br> [0x80006f90]:csrrs a2, fcsr, zero<br> [0x80006f94]:sw t6, 392(fp)<br>    |
| 588|[0x80013768]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x04fd41 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80006fc4]:fmul.s t6, t5, t4, dyn<br> [0x80006fc8]:csrrs a2, fcsr, zero<br> [0x80006fcc]:sw t6, 400(fp)<br>    |
| 589|[0x80013770]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x04fd41 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80006ffc]:fmul.s t6, t5, t4, dyn<br> [0x80007000]:csrrs a2, fcsr, zero<br> [0x80007004]:sw t6, 408(fp)<br>    |
| 590|[0x80013778]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x04fd41 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80007034]:fmul.s t6, t5, t4, dyn<br> [0x80007038]:csrrs a2, fcsr, zero<br> [0x8000703c]:sw t6, 416(fp)<br>    |
| 591|[0x80013780]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7d1e07 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000706c]:fmul.s t6, t5, t4, dyn<br> [0x80007070]:csrrs a2, fcsr, zero<br> [0x80007074]:sw t6, 424(fp)<br>    |
| 592|[0x80013788]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7d1e07 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800070a4]:fmul.s t6, t5, t4, dyn<br> [0x800070a8]:csrrs a2, fcsr, zero<br> [0x800070ac]:sw t6, 432(fp)<br>    |
| 593|[0x80013790]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7d1e07 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800070dc]:fmul.s t6, t5, t4, dyn<br> [0x800070e0]:csrrs a2, fcsr, zero<br> [0x800070e4]:sw t6, 440(fp)<br>    |
| 594|[0x80013798]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7d1e07 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80007114]:fmul.s t6, t5, t4, dyn<br> [0x80007118]:csrrs a2, fcsr, zero<br> [0x8000711c]:sw t6, 448(fp)<br>    |
| 595|[0x800137a0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7d1e07 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000714c]:fmul.s t6, t5, t4, dyn<br> [0x80007150]:csrrs a2, fcsr, zero<br> [0x80007154]:sw t6, 456(fp)<br>    |
| 596|[0x800137a8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0aea5e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007184]:fmul.s t6, t5, t4, dyn<br> [0x80007188]:csrrs a2, fcsr, zero<br> [0x8000718c]:sw t6, 464(fp)<br>    |
| 597|[0x800137b0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0aea5e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800071bc]:fmul.s t6, t5, t4, dyn<br> [0x800071c0]:csrrs a2, fcsr, zero<br> [0x800071c4]:sw t6, 472(fp)<br>    |
| 598|[0x800137b8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0aea5e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800071f4]:fmul.s t6, t5, t4, dyn<br> [0x800071f8]:csrrs a2, fcsr, zero<br> [0x800071fc]:sw t6, 480(fp)<br>    |
| 599|[0x800137c0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0aea5e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000722c]:fmul.s t6, t5, t4, dyn<br> [0x80007230]:csrrs a2, fcsr, zero<br> [0x80007234]:sw t6, 488(fp)<br>    |
| 600|[0x800137c8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0aea5e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80007264]:fmul.s t6, t5, t4, dyn<br> [0x80007268]:csrrs a2, fcsr, zero<br> [0x8000726c]:sw t6, 496(fp)<br>    |
| 601|[0x800137d0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x094fff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000729c]:fmul.s t6, t5, t4, dyn<br> [0x800072a0]:csrrs a2, fcsr, zero<br> [0x800072a4]:sw t6, 504(fp)<br>    |
| 602|[0x800137d8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x094fff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800072d4]:fmul.s t6, t5, t4, dyn<br> [0x800072d8]:csrrs a2, fcsr, zero<br> [0x800072dc]:sw t6, 512(fp)<br>    |
| 603|[0x800137e0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x094fff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000730c]:fmul.s t6, t5, t4, dyn<br> [0x80007310]:csrrs a2, fcsr, zero<br> [0x80007314]:sw t6, 520(fp)<br>    |
| 604|[0x800137e8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x094fff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80007344]:fmul.s t6, t5, t4, dyn<br> [0x80007348]:csrrs a2, fcsr, zero<br> [0x8000734c]:sw t6, 528(fp)<br>    |
| 605|[0x800137f0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x094fff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000737c]:fmul.s t6, t5, t4, dyn<br> [0x80007380]:csrrs a2, fcsr, zero<br> [0x80007384]:sw t6, 536(fp)<br>    |
| 606|[0x800137f8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x195ac9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800073b4]:fmul.s t6, t5, t4, dyn<br> [0x800073b8]:csrrs a2, fcsr, zero<br> [0x800073bc]:sw t6, 544(fp)<br>    |
| 607|[0x80013800]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x195ac9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800073ec]:fmul.s t6, t5, t4, dyn<br> [0x800073f0]:csrrs a2, fcsr, zero<br> [0x800073f4]:sw t6, 552(fp)<br>    |
| 608|[0x80013808]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x195ac9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80007424]:fmul.s t6, t5, t4, dyn<br> [0x80007428]:csrrs a2, fcsr, zero<br> [0x8000742c]:sw t6, 560(fp)<br>    |
| 609|[0x80013810]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x195ac9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000745c]:fmul.s t6, t5, t4, dyn<br> [0x80007460]:csrrs a2, fcsr, zero<br> [0x80007464]:sw t6, 568(fp)<br>    |
| 610|[0x80013818]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x195ac9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80007494]:fmul.s t6, t5, t4, dyn<br> [0x80007498]:csrrs a2, fcsr, zero<br> [0x8000749c]:sw t6, 576(fp)<br>    |
| 611|[0x80013820]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4a19c2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800074cc]:fmul.s t6, t5, t4, dyn<br> [0x800074d0]:csrrs a2, fcsr, zero<br> [0x800074d4]:sw t6, 584(fp)<br>    |
| 612|[0x80013828]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4a19c2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80007504]:fmul.s t6, t5, t4, dyn<br> [0x80007508]:csrrs a2, fcsr, zero<br> [0x8000750c]:sw t6, 592(fp)<br>    |
| 613|[0x80013830]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4a19c2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000753c]:fmul.s t6, t5, t4, dyn<br> [0x80007540]:csrrs a2, fcsr, zero<br> [0x80007544]:sw t6, 600(fp)<br>    |
| 614|[0x80013838]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4a19c2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80007574]:fmul.s t6, t5, t4, dyn<br> [0x80007578]:csrrs a2, fcsr, zero<br> [0x8000757c]:sw t6, 608(fp)<br>    |
| 615|[0x80013840]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4a19c2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800075ac]:fmul.s t6, t5, t4, dyn<br> [0x800075b0]:csrrs a2, fcsr, zero<br> [0x800075b4]:sw t6, 616(fp)<br>    |
| 616|[0x80013848]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x374d41 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800075e4]:fmul.s t6, t5, t4, dyn<br> [0x800075e8]:csrrs a2, fcsr, zero<br> [0x800075ec]:sw t6, 624(fp)<br>    |
| 617|[0x80013850]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x374d41 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000761c]:fmul.s t6, t5, t4, dyn<br> [0x80007620]:csrrs a2, fcsr, zero<br> [0x80007624]:sw t6, 632(fp)<br>    |
| 618|[0x80013858]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x374d41 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80007654]:fmul.s t6, t5, t4, dyn<br> [0x80007658]:csrrs a2, fcsr, zero<br> [0x8000765c]:sw t6, 640(fp)<br>    |
| 619|[0x80013860]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x374d41 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000768c]:fmul.s t6, t5, t4, dyn<br> [0x80007690]:csrrs a2, fcsr, zero<br> [0x80007694]:sw t6, 648(fp)<br>    |
| 620|[0x80013868]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x374d41 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800076c4]:fmul.s t6, t5, t4, dyn<br> [0x800076c8]:csrrs a2, fcsr, zero<br> [0x800076cc]:sw t6, 656(fp)<br>    |
| 621|[0x80013870]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x32f44f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800076fc]:fmul.s t6, t5, t4, dyn<br> [0x80007700]:csrrs a2, fcsr, zero<br> [0x80007704]:sw t6, 664(fp)<br>    |
| 622|[0x80013878]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x32f44f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80007734]:fmul.s t6, t5, t4, dyn<br> [0x80007738]:csrrs a2, fcsr, zero<br> [0x8000773c]:sw t6, 672(fp)<br>    |
| 623|[0x80013880]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x32f44f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000776c]:fmul.s t6, t5, t4, dyn<br> [0x80007770]:csrrs a2, fcsr, zero<br> [0x80007774]:sw t6, 680(fp)<br>    |
| 624|[0x80013888]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x32f44f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800077a4]:fmul.s t6, t5, t4, dyn<br> [0x800077a8]:csrrs a2, fcsr, zero<br> [0x800077ac]:sw t6, 688(fp)<br>    |
| 625|[0x80013890]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x32f44f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800077dc]:fmul.s t6, t5, t4, dyn<br> [0x800077e0]:csrrs a2, fcsr, zero<br> [0x800077e4]:sw t6, 696(fp)<br>    |
| 626|[0x80013898]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x63ea1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007814]:fmul.s t6, t5, t4, dyn<br> [0x80007818]:csrrs a2, fcsr, zero<br> [0x8000781c]:sw t6, 704(fp)<br>    |
| 627|[0x800138a0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x63ea1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000784c]:fmul.s t6, t5, t4, dyn<br> [0x80007850]:csrrs a2, fcsr, zero<br> [0x80007854]:sw t6, 712(fp)<br>    |
| 628|[0x800138a8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x63ea1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80007884]:fmul.s t6, t5, t4, dyn<br> [0x80007888]:csrrs a2, fcsr, zero<br> [0x8000788c]:sw t6, 720(fp)<br>    |
| 629|[0x800138b0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x63ea1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800078bc]:fmul.s t6, t5, t4, dyn<br> [0x800078c0]:csrrs a2, fcsr, zero<br> [0x800078c4]:sw t6, 728(fp)<br>    |
| 630|[0x800138b8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x63ea1b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800078f4]:fmul.s t6, t5, t4, dyn<br> [0x800078f8]:csrrs a2, fcsr, zero<br> [0x800078fc]:sw t6, 736(fp)<br>    |
| 631|[0x800138c0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3bb1f5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000792c]:fmul.s t6, t5, t4, dyn<br> [0x80007930]:csrrs a2, fcsr, zero<br> [0x80007934]:sw t6, 744(fp)<br>    |
| 632|[0x800138c8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3bb1f5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80007964]:fmul.s t6, t5, t4, dyn<br> [0x80007968]:csrrs a2, fcsr, zero<br> [0x8000796c]:sw t6, 752(fp)<br>    |
| 633|[0x800138d0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3bb1f5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000799c]:fmul.s t6, t5, t4, dyn<br> [0x800079a0]:csrrs a2, fcsr, zero<br> [0x800079a4]:sw t6, 760(fp)<br>    |
| 634|[0x800138d8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3bb1f5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800079d4]:fmul.s t6, t5, t4, dyn<br> [0x800079d8]:csrrs a2, fcsr, zero<br> [0x800079dc]:sw t6, 768(fp)<br>    |
| 635|[0x800138e0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3bb1f5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80007a0c]:fmul.s t6, t5, t4, dyn<br> [0x80007a10]:csrrs a2, fcsr, zero<br> [0x80007a14]:sw t6, 776(fp)<br>    |
| 636|[0x800138e8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x243814 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007a44]:fmul.s t6, t5, t4, dyn<br> [0x80007a48]:csrrs a2, fcsr, zero<br> [0x80007a4c]:sw t6, 784(fp)<br>    |
| 637|[0x800138f0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x243814 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80007a7c]:fmul.s t6, t5, t4, dyn<br> [0x80007a80]:csrrs a2, fcsr, zero<br> [0x80007a84]:sw t6, 792(fp)<br>    |
| 638|[0x800138f8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x243814 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80007ab4]:fmul.s t6, t5, t4, dyn<br> [0x80007ab8]:csrrs a2, fcsr, zero<br> [0x80007abc]:sw t6, 800(fp)<br>    |
| 639|[0x80013900]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x243814 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80007aec]:fmul.s t6, t5, t4, dyn<br> [0x80007af0]:csrrs a2, fcsr, zero<br> [0x80007af4]:sw t6, 808(fp)<br>    |
| 640|[0x80013908]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x243814 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80007b24]:fmul.s t6, t5, t4, dyn<br> [0x80007b28]:csrrs a2, fcsr, zero<br> [0x80007b2c]:sw t6, 816(fp)<br>    |
| 641|[0x80013910]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x305e0d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007b5c]:fmul.s t6, t5, t4, dyn<br> [0x80007b60]:csrrs a2, fcsr, zero<br> [0x80007b64]:sw t6, 824(fp)<br>    |
| 642|[0x80013918]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x305e0d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80007b94]:fmul.s t6, t5, t4, dyn<br> [0x80007b98]:csrrs a2, fcsr, zero<br> [0x80007b9c]:sw t6, 832(fp)<br>    |
| 643|[0x80013920]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x305e0d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80007bcc]:fmul.s t6, t5, t4, dyn<br> [0x80007bd0]:csrrs a2, fcsr, zero<br> [0x80007bd4]:sw t6, 840(fp)<br>    |
| 644|[0x80013928]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x305e0d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80007c04]:fmul.s t6, t5, t4, dyn<br> [0x80007c08]:csrrs a2, fcsr, zero<br> [0x80007c0c]:sw t6, 848(fp)<br>    |
| 645|[0x80013930]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x305e0d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80007c3c]:fmul.s t6, t5, t4, dyn<br> [0x80007c40]:csrrs a2, fcsr, zero<br> [0x80007c44]:sw t6, 856(fp)<br>    |
| 646|[0x80013938]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d071f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007c74]:fmul.s t6, t5, t4, dyn<br> [0x80007c78]:csrrs a2, fcsr, zero<br> [0x80007c7c]:sw t6, 864(fp)<br>    |
| 647|[0x80013940]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d071f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80007cac]:fmul.s t6, t5, t4, dyn<br> [0x80007cb0]:csrrs a2, fcsr, zero<br> [0x80007cb4]:sw t6, 872(fp)<br>    |
| 648|[0x80013948]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d071f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80007ce4]:fmul.s t6, t5, t4, dyn<br> [0x80007ce8]:csrrs a2, fcsr, zero<br> [0x80007cec]:sw t6, 880(fp)<br>    |
| 649|[0x80013950]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d071f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80007d1c]:fmul.s t6, t5, t4, dyn<br> [0x80007d20]:csrrs a2, fcsr, zero<br> [0x80007d24]:sw t6, 888(fp)<br>    |
| 650|[0x80013958]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d071f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80007d54]:fmul.s t6, t5, t4, dyn<br> [0x80007d58]:csrrs a2, fcsr, zero<br> [0x80007d5c]:sw t6, 896(fp)<br>    |
| 651|[0x80013960]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x08b9d9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007d8c]:fmul.s t6, t5, t4, dyn<br> [0x80007d90]:csrrs a2, fcsr, zero<br> [0x80007d94]:sw t6, 904(fp)<br>    |
| 652|[0x80013968]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x08b9d9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80007dc4]:fmul.s t6, t5, t4, dyn<br> [0x80007dc8]:csrrs a2, fcsr, zero<br> [0x80007dcc]:sw t6, 912(fp)<br>    |
| 653|[0x80013970]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x08b9d9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80007dfc]:fmul.s t6, t5, t4, dyn<br> [0x80007e00]:csrrs a2, fcsr, zero<br> [0x80007e04]:sw t6, 920(fp)<br>    |
| 654|[0x80013978]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x08b9d9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80007e34]:fmul.s t6, t5, t4, dyn<br> [0x80007e38]:csrrs a2, fcsr, zero<br> [0x80007e3c]:sw t6, 928(fp)<br>    |
| 655|[0x80013980]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x08b9d9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80007e6c]:fmul.s t6, t5, t4, dyn<br> [0x80007e70]:csrrs a2, fcsr, zero<br> [0x80007e74]:sw t6, 936(fp)<br>    |
| 656|[0x80013988]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x61068d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007ea4]:fmul.s t6, t5, t4, dyn<br> [0x80007ea8]:csrrs a2, fcsr, zero<br> [0x80007eac]:sw t6, 944(fp)<br>    |
| 657|[0x80013990]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x61068d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80007edc]:fmul.s t6, t5, t4, dyn<br> [0x80007ee0]:csrrs a2, fcsr, zero<br> [0x80007ee4]:sw t6, 952(fp)<br>    |
| 658|[0x80013998]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x61068d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80007f14]:fmul.s t6, t5, t4, dyn<br> [0x80007f18]:csrrs a2, fcsr, zero<br> [0x80007f1c]:sw t6, 960(fp)<br>    |
| 659|[0x800139a0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x61068d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80007f4c]:fmul.s t6, t5, t4, dyn<br> [0x80007f50]:csrrs a2, fcsr, zero<br> [0x80007f54]:sw t6, 968(fp)<br>    |
| 660|[0x800139a8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x61068d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80007f84]:fmul.s t6, t5, t4, dyn<br> [0x80007f88]:csrrs a2, fcsr, zero<br> [0x80007f8c]:sw t6, 976(fp)<br>    |
| 661|[0x800139b0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x4d54a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007fbc]:fmul.s t6, t5, t4, dyn<br> [0x80007fc0]:csrrs a2, fcsr, zero<br> [0x80007fc4]:sw t6, 984(fp)<br>    |
| 662|[0x800139b8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x4d54a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80007ff4]:fmul.s t6, t5, t4, dyn<br> [0x80007ff8]:csrrs a2, fcsr, zero<br> [0x80007ffc]:sw t6, 992(fp)<br>    |
| 663|[0x800139c0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x4d54a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000802c]:fmul.s t6, t5, t4, dyn<br> [0x80008030]:csrrs a2, fcsr, zero<br> [0x80008034]:sw t6, 1000(fp)<br>   |
| 664|[0x800139c8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x4d54a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80008064]:fmul.s t6, t5, t4, dyn<br> [0x80008068]:csrrs a2, fcsr, zero<br> [0x8000806c]:sw t6, 1008(fp)<br>   |
| 665|[0x800139d0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x4d54a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000809c]:fmul.s t6, t5, t4, dyn<br> [0x800080a0]:csrrs a2, fcsr, zero<br> [0x800080a4]:sw t6, 1016(fp)<br>   |
| 666|[0x800139d8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2befa1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800080dc]:fmul.s t6, t5, t4, dyn<br> [0x800080e0]:csrrs a2, fcsr, zero<br> [0x800080e4]:sw t6, 0(fp)<br>      |
| 667|[0x800139e0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2befa1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80008114]:fmul.s t6, t5, t4, dyn<br> [0x80008118]:csrrs a2, fcsr, zero<br> [0x8000811c]:sw t6, 8(fp)<br>      |
| 668|[0x800139e8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2befa1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000814c]:fmul.s t6, t5, t4, dyn<br> [0x80008150]:csrrs a2, fcsr, zero<br> [0x80008154]:sw t6, 16(fp)<br>     |
| 669|[0x800139f0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2befa1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80008184]:fmul.s t6, t5, t4, dyn<br> [0x80008188]:csrrs a2, fcsr, zero<br> [0x8000818c]:sw t6, 24(fp)<br>     |
| 670|[0x800139f8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2befa1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800081bc]:fmul.s t6, t5, t4, dyn<br> [0x800081c0]:csrrs a2, fcsr, zero<br> [0x800081c4]:sw t6, 32(fp)<br>     |
| 671|[0x80013a00]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x78dd0e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800081f4]:fmul.s t6, t5, t4, dyn<br> [0x800081f8]:csrrs a2, fcsr, zero<br> [0x800081fc]:sw t6, 40(fp)<br>     |
| 672|[0x80013a08]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x78dd0e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000822c]:fmul.s t6, t5, t4, dyn<br> [0x80008230]:csrrs a2, fcsr, zero<br> [0x80008234]:sw t6, 48(fp)<br>     |
| 673|[0x80013a10]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x78dd0e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80008264]:fmul.s t6, t5, t4, dyn<br> [0x80008268]:csrrs a2, fcsr, zero<br> [0x8000826c]:sw t6, 56(fp)<br>     |
| 674|[0x80013a18]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x78dd0e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000829c]:fmul.s t6, t5, t4, dyn<br> [0x800082a0]:csrrs a2, fcsr, zero<br> [0x800082a4]:sw t6, 64(fp)<br>     |
| 675|[0x80013a20]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x78dd0e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800082d4]:fmul.s t6, t5, t4, dyn<br> [0x800082d8]:csrrs a2, fcsr, zero<br> [0x800082dc]:sw t6, 72(fp)<br>     |
| 676|[0x80013a28]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x639f2c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000830c]:fmul.s t6, t5, t4, dyn<br> [0x80008310]:csrrs a2, fcsr, zero<br> [0x80008314]:sw t6, 80(fp)<br>     |
| 677|[0x80013a30]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x639f2c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80008344]:fmul.s t6, t5, t4, dyn<br> [0x80008348]:csrrs a2, fcsr, zero<br> [0x8000834c]:sw t6, 88(fp)<br>     |
| 678|[0x80013a38]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x639f2c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000837c]:fmul.s t6, t5, t4, dyn<br> [0x80008380]:csrrs a2, fcsr, zero<br> [0x80008384]:sw t6, 96(fp)<br>     |
| 679|[0x80013a40]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x639f2c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800083b4]:fmul.s t6, t5, t4, dyn<br> [0x800083b8]:csrrs a2, fcsr, zero<br> [0x800083bc]:sw t6, 104(fp)<br>    |
| 680|[0x80013a48]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x639f2c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800083ec]:fmul.s t6, t5, t4, dyn<br> [0x800083f0]:csrrs a2, fcsr, zero<br> [0x800083f4]:sw t6, 112(fp)<br>    |
| 681|[0x80013a50]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x00c7d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008424]:fmul.s t6, t5, t4, dyn<br> [0x80008428]:csrrs a2, fcsr, zero<br> [0x8000842c]:sw t6, 120(fp)<br>    |
| 682|[0x80013a58]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x00c7d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000845c]:fmul.s t6, t5, t4, dyn<br> [0x80008460]:csrrs a2, fcsr, zero<br> [0x80008464]:sw t6, 128(fp)<br>    |
| 683|[0x80013a60]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x00c7d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80008494]:fmul.s t6, t5, t4, dyn<br> [0x80008498]:csrrs a2, fcsr, zero<br> [0x8000849c]:sw t6, 136(fp)<br>    |
| 684|[0x80013a68]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x00c7d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800084cc]:fmul.s t6, t5, t4, dyn<br> [0x800084d0]:csrrs a2, fcsr, zero<br> [0x800084d4]:sw t6, 144(fp)<br>    |
| 685|[0x80013a70]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x00c7d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80008504]:fmul.s t6, t5, t4, dyn<br> [0x80008508]:csrrs a2, fcsr, zero<br> [0x8000850c]:sw t6, 152(fp)<br>    |
| 686|[0x80013a78]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x28844c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000853c]:fmul.s t6, t5, t4, dyn<br> [0x80008540]:csrrs a2, fcsr, zero<br> [0x80008544]:sw t6, 160(fp)<br>    |
| 687|[0x80013a80]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x28844c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80008574]:fmul.s t6, t5, t4, dyn<br> [0x80008578]:csrrs a2, fcsr, zero<br> [0x8000857c]:sw t6, 168(fp)<br>    |
| 688|[0x80013a88]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x28844c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800085ac]:fmul.s t6, t5, t4, dyn<br> [0x800085b0]:csrrs a2, fcsr, zero<br> [0x800085b4]:sw t6, 176(fp)<br>    |
| 689|[0x80013a90]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x28844c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800085e4]:fmul.s t6, t5, t4, dyn<br> [0x800085e8]:csrrs a2, fcsr, zero<br> [0x800085ec]:sw t6, 184(fp)<br>    |
| 690|[0x80013a98]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x28844c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000861c]:fmul.s t6, t5, t4, dyn<br> [0x80008620]:csrrs a2, fcsr, zero<br> [0x80008624]:sw t6, 192(fp)<br>    |
| 691|[0x80013aa0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x2c23d1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008654]:fmul.s t6, t5, t4, dyn<br> [0x80008658]:csrrs a2, fcsr, zero<br> [0x8000865c]:sw t6, 200(fp)<br>    |
| 692|[0x80013aa8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x2c23d1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000868c]:fmul.s t6, t5, t4, dyn<br> [0x80008690]:csrrs a2, fcsr, zero<br> [0x80008694]:sw t6, 208(fp)<br>    |
| 693|[0x80013ab0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x2c23d1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800086c4]:fmul.s t6, t5, t4, dyn<br> [0x800086c8]:csrrs a2, fcsr, zero<br> [0x800086cc]:sw t6, 216(fp)<br>    |
| 694|[0x80013ab8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x2c23d1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800086fc]:fmul.s t6, t5, t4, dyn<br> [0x80008700]:csrrs a2, fcsr, zero<br> [0x80008704]:sw t6, 224(fp)<br>    |
| 695|[0x80013ac0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x2c23d1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80008734]:fmul.s t6, t5, t4, dyn<br> [0x80008738]:csrrs a2, fcsr, zero<br> [0x8000873c]:sw t6, 232(fp)<br>    |
| 696|[0x80013ac8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x366362 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000876c]:fmul.s t6, t5, t4, dyn<br> [0x80008770]:csrrs a2, fcsr, zero<br> [0x80008774]:sw t6, 240(fp)<br>    |
| 697|[0x80013ad0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x366362 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800087a4]:fmul.s t6, t5, t4, dyn<br> [0x800087a8]:csrrs a2, fcsr, zero<br> [0x800087ac]:sw t6, 248(fp)<br>    |
| 698|[0x80013ad8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x366362 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800087dc]:fmul.s t6, t5, t4, dyn<br> [0x800087e0]:csrrs a2, fcsr, zero<br> [0x800087e4]:sw t6, 256(fp)<br>    |
| 699|[0x80013ae0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x366362 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80008814]:fmul.s t6, t5, t4, dyn<br> [0x80008818]:csrrs a2, fcsr, zero<br> [0x8000881c]:sw t6, 264(fp)<br>    |
| 700|[0x80013ae8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x366362 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000884c]:fmul.s t6, t5, t4, dyn<br> [0x80008850]:csrrs a2, fcsr, zero<br> [0x80008854]:sw t6, 272(fp)<br>    |
| 701|[0x80013af0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x13b178 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008884]:fmul.s t6, t5, t4, dyn<br> [0x80008888]:csrrs a2, fcsr, zero<br> [0x8000888c]:sw t6, 280(fp)<br>    |
| 702|[0x80013af8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x13b178 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800088bc]:fmul.s t6, t5, t4, dyn<br> [0x800088c0]:csrrs a2, fcsr, zero<br> [0x800088c4]:sw t6, 288(fp)<br>    |
| 703|[0x80013b00]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x13b178 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800088f4]:fmul.s t6, t5, t4, dyn<br> [0x800088f8]:csrrs a2, fcsr, zero<br> [0x800088fc]:sw t6, 296(fp)<br>    |
| 704|[0x80013b08]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x13b178 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000892c]:fmul.s t6, t5, t4, dyn<br> [0x80008930]:csrrs a2, fcsr, zero<br> [0x80008934]:sw t6, 304(fp)<br>    |
| 705|[0x80013b10]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x13b178 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80008964]:fmul.s t6, t5, t4, dyn<br> [0x80008968]:csrrs a2, fcsr, zero<br> [0x8000896c]:sw t6, 312(fp)<br>    |
| 706|[0x80013b18]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x09599c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000899c]:fmul.s t6, t5, t4, dyn<br> [0x800089a0]:csrrs a2, fcsr, zero<br> [0x800089a4]:sw t6, 320(fp)<br>    |
| 707|[0x80013b20]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x09599c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800089d4]:fmul.s t6, t5, t4, dyn<br> [0x800089d8]:csrrs a2, fcsr, zero<br> [0x800089dc]:sw t6, 328(fp)<br>    |
| 708|[0x80013b28]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x09599c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80008a0c]:fmul.s t6, t5, t4, dyn<br> [0x80008a10]:csrrs a2, fcsr, zero<br> [0x80008a14]:sw t6, 336(fp)<br>    |
| 709|[0x80013b30]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x09599c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80008a44]:fmul.s t6, t5, t4, dyn<br> [0x80008a48]:csrrs a2, fcsr, zero<br> [0x80008a4c]:sw t6, 344(fp)<br>    |
| 710|[0x80013b38]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x09599c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80008a7c]:fmul.s t6, t5, t4, dyn<br> [0x80008a80]:csrrs a2, fcsr, zero<br> [0x80008a84]:sw t6, 352(fp)<br>    |
| 711|[0x80013b40]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2bd1f5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008ab4]:fmul.s t6, t5, t4, dyn<br> [0x80008ab8]:csrrs a2, fcsr, zero<br> [0x80008abc]:sw t6, 360(fp)<br>    |
| 712|[0x80013b48]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2bd1f5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80008aec]:fmul.s t6, t5, t4, dyn<br> [0x80008af0]:csrrs a2, fcsr, zero<br> [0x80008af4]:sw t6, 368(fp)<br>    |
| 713|[0x80013b50]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2bd1f5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80008b24]:fmul.s t6, t5, t4, dyn<br> [0x80008b28]:csrrs a2, fcsr, zero<br> [0x80008b2c]:sw t6, 376(fp)<br>    |
| 714|[0x80013b58]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2bd1f5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80008b5c]:fmul.s t6, t5, t4, dyn<br> [0x80008b60]:csrrs a2, fcsr, zero<br> [0x80008b64]:sw t6, 384(fp)<br>    |
| 715|[0x80013b60]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2bd1f5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80008b94]:fmul.s t6, t5, t4, dyn<br> [0x80008b98]:csrrs a2, fcsr, zero<br> [0x80008b9c]:sw t6, 392(fp)<br>    |
| 716|[0x80013b68]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x070538 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008bcc]:fmul.s t6, t5, t4, dyn<br> [0x80008bd0]:csrrs a2, fcsr, zero<br> [0x80008bd4]:sw t6, 400(fp)<br>    |
| 717|[0x80013b70]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x070538 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80008c04]:fmul.s t6, t5, t4, dyn<br> [0x80008c08]:csrrs a2, fcsr, zero<br> [0x80008c0c]:sw t6, 408(fp)<br>    |
| 718|[0x80013b78]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x070538 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80008c3c]:fmul.s t6, t5, t4, dyn<br> [0x80008c40]:csrrs a2, fcsr, zero<br> [0x80008c44]:sw t6, 416(fp)<br>    |
| 719|[0x80013b80]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x070538 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80008c74]:fmul.s t6, t5, t4, dyn<br> [0x80008c78]:csrrs a2, fcsr, zero<br> [0x80008c7c]:sw t6, 424(fp)<br>    |
| 720|[0x80013b88]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x070538 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80008cac]:fmul.s t6, t5, t4, dyn<br> [0x80008cb0]:csrrs a2, fcsr, zero<br> [0x80008cb4]:sw t6, 432(fp)<br>    |
| 721|[0x80013b90]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3f10c6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008ce4]:fmul.s t6, t5, t4, dyn<br> [0x80008ce8]:csrrs a2, fcsr, zero<br> [0x80008cec]:sw t6, 440(fp)<br>    |
| 722|[0x80013b98]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3f10c6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80008d1c]:fmul.s t6, t5, t4, dyn<br> [0x80008d20]:csrrs a2, fcsr, zero<br> [0x80008d24]:sw t6, 448(fp)<br>    |
| 723|[0x80013ba0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3f10c6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80008d54]:fmul.s t6, t5, t4, dyn<br> [0x80008d58]:csrrs a2, fcsr, zero<br> [0x80008d5c]:sw t6, 456(fp)<br>    |
| 724|[0x80013ba8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3f10c6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80008d8c]:fmul.s t6, t5, t4, dyn<br> [0x80008d90]:csrrs a2, fcsr, zero<br> [0x80008d94]:sw t6, 464(fp)<br>    |
| 725|[0x80013bb0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3f10c6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80008dc4]:fmul.s t6, t5, t4, dyn<br> [0x80008dc8]:csrrs a2, fcsr, zero<br> [0x80008dcc]:sw t6, 472(fp)<br>    |
| 726|[0x80013bb8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x5c337b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008dfc]:fmul.s t6, t5, t4, dyn<br> [0x80008e00]:csrrs a2, fcsr, zero<br> [0x80008e04]:sw t6, 480(fp)<br>    |
| 727|[0x80013bc0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x5c337b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80008e34]:fmul.s t6, t5, t4, dyn<br> [0x80008e38]:csrrs a2, fcsr, zero<br> [0x80008e3c]:sw t6, 488(fp)<br>    |
| 728|[0x80013bc8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x5c337b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80008e6c]:fmul.s t6, t5, t4, dyn<br> [0x80008e70]:csrrs a2, fcsr, zero<br> [0x80008e74]:sw t6, 496(fp)<br>    |
| 729|[0x80013bd0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x5c337b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80008ea4]:fmul.s t6, t5, t4, dyn<br> [0x80008ea8]:csrrs a2, fcsr, zero<br> [0x80008eac]:sw t6, 504(fp)<br>    |
| 730|[0x80013bd8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x5c337b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80008edc]:fmul.s t6, t5, t4, dyn<br> [0x80008ee0]:csrrs a2, fcsr, zero<br> [0x80008ee4]:sw t6, 512(fp)<br>    |
| 731|[0x80013be0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x296a13 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008f14]:fmul.s t6, t5, t4, dyn<br> [0x80008f18]:csrrs a2, fcsr, zero<br> [0x80008f1c]:sw t6, 520(fp)<br>    |
| 732|[0x80013be8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x296a13 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80008f4c]:fmul.s t6, t5, t4, dyn<br> [0x80008f50]:csrrs a2, fcsr, zero<br> [0x80008f54]:sw t6, 528(fp)<br>    |
| 733|[0x80013bf0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x296a13 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80008f84]:fmul.s t6, t5, t4, dyn<br> [0x80008f88]:csrrs a2, fcsr, zero<br> [0x80008f8c]:sw t6, 536(fp)<br>    |
| 734|[0x80013bf8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x296a13 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80008fbc]:fmul.s t6, t5, t4, dyn<br> [0x80008fc0]:csrrs a2, fcsr, zero<br> [0x80008fc4]:sw t6, 544(fp)<br>    |
| 735|[0x80013c00]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x296a13 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80008ff4]:fmul.s t6, t5, t4, dyn<br> [0x80008ff8]:csrrs a2, fcsr, zero<br> [0x80008ffc]:sw t6, 552(fp)<br>    |
| 736|[0x80013c08]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x648b04 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000902c]:fmul.s t6, t5, t4, dyn<br> [0x80009030]:csrrs a2, fcsr, zero<br> [0x80009034]:sw t6, 560(fp)<br>    |
| 737|[0x80013c10]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x648b04 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80009064]:fmul.s t6, t5, t4, dyn<br> [0x80009068]:csrrs a2, fcsr, zero<br> [0x8000906c]:sw t6, 568(fp)<br>    |
| 738|[0x80013c18]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x648b04 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000909c]:fmul.s t6, t5, t4, dyn<br> [0x800090a0]:csrrs a2, fcsr, zero<br> [0x800090a4]:sw t6, 576(fp)<br>    |
| 739|[0x80013c20]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x648b04 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800090d4]:fmul.s t6, t5, t4, dyn<br> [0x800090d8]:csrrs a2, fcsr, zero<br> [0x800090dc]:sw t6, 584(fp)<br>    |
| 740|[0x80013c28]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x648b04 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000910c]:fmul.s t6, t5, t4, dyn<br> [0x80009110]:csrrs a2, fcsr, zero<br> [0x80009114]:sw t6, 592(fp)<br>    |
| 741|[0x80013c30]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x0a90e5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80009144]:fmul.s t6, t5, t4, dyn<br> [0x80009148]:csrrs a2, fcsr, zero<br> [0x8000914c]:sw t6, 600(fp)<br>    |
| 742|[0x80013c38]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x0a90e5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000917c]:fmul.s t6, t5, t4, dyn<br> [0x80009180]:csrrs a2, fcsr, zero<br> [0x80009184]:sw t6, 608(fp)<br>    |
| 743|[0x80013c40]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x0a90e5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800091b4]:fmul.s t6, t5, t4, dyn<br> [0x800091b8]:csrrs a2, fcsr, zero<br> [0x800091bc]:sw t6, 616(fp)<br>    |
| 744|[0x80013c48]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x0a90e5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800091ec]:fmul.s t6, t5, t4, dyn<br> [0x800091f0]:csrrs a2, fcsr, zero<br> [0x800091f4]:sw t6, 624(fp)<br>    |
| 745|[0x80013c50]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x0a90e5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80009224]:fmul.s t6, t5, t4, dyn<br> [0x80009228]:csrrs a2, fcsr, zero<br> [0x8000922c]:sw t6, 632(fp)<br>    |
| 746|[0x80013c58]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x394394 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000925c]:fmul.s t6, t5, t4, dyn<br> [0x80009260]:csrrs a2, fcsr, zero<br> [0x80009264]:sw t6, 640(fp)<br>    |
| 747|[0x80013c60]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x394394 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80009294]:fmul.s t6, t5, t4, dyn<br> [0x80009298]:csrrs a2, fcsr, zero<br> [0x8000929c]:sw t6, 648(fp)<br>    |
| 748|[0x80013c68]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x394394 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800092cc]:fmul.s t6, t5, t4, dyn<br> [0x800092d0]:csrrs a2, fcsr, zero<br> [0x800092d4]:sw t6, 656(fp)<br>    |
| 749|[0x80013c70]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x394394 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80009304]:fmul.s t6, t5, t4, dyn<br> [0x80009308]:csrrs a2, fcsr, zero<br> [0x8000930c]:sw t6, 664(fp)<br>    |
| 750|[0x80013c78]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x394394 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000933c]:fmul.s t6, t5, t4, dyn<br> [0x80009340]:csrrs a2, fcsr, zero<br> [0x80009344]:sw t6, 672(fp)<br>    |
| 751|[0x80013c80]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x086888 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80009374]:fmul.s t6, t5, t4, dyn<br> [0x80009378]:csrrs a2, fcsr, zero<br> [0x8000937c]:sw t6, 680(fp)<br>    |
| 752|[0x80013c88]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x086888 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800093ac]:fmul.s t6, t5, t4, dyn<br> [0x800093b0]:csrrs a2, fcsr, zero<br> [0x800093b4]:sw t6, 688(fp)<br>    |
| 753|[0x80013c90]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x086888 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800093e4]:fmul.s t6, t5, t4, dyn<br> [0x800093e8]:csrrs a2, fcsr, zero<br> [0x800093ec]:sw t6, 696(fp)<br>    |
| 754|[0x80013c98]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x086888 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000941c]:fmul.s t6, t5, t4, dyn<br> [0x80009420]:csrrs a2, fcsr, zero<br> [0x80009424]:sw t6, 704(fp)<br>    |
| 755|[0x80013ca0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x086888 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80009454]:fmul.s t6, t5, t4, dyn<br> [0x80009458]:csrrs a2, fcsr, zero<br> [0x8000945c]:sw t6, 712(fp)<br>    |
| 756|[0x80013ca8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x076f73 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000948c]:fmul.s t6, t5, t4, dyn<br> [0x80009490]:csrrs a2, fcsr, zero<br> [0x80009494]:sw t6, 720(fp)<br>    |
| 757|[0x80013cb0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x076f73 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800094c4]:fmul.s t6, t5, t4, dyn<br> [0x800094c8]:csrrs a2, fcsr, zero<br> [0x800094cc]:sw t6, 728(fp)<br>    |
| 758|[0x80013cb8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x076f73 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800094fc]:fmul.s t6, t5, t4, dyn<br> [0x80009500]:csrrs a2, fcsr, zero<br> [0x80009504]:sw t6, 736(fp)<br>    |
| 759|[0x80013cc0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x076f73 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80009534]:fmul.s t6, t5, t4, dyn<br> [0x80009538]:csrrs a2, fcsr, zero<br> [0x8000953c]:sw t6, 744(fp)<br>    |
| 760|[0x80013cc8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x076f73 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000956c]:fmul.s t6, t5, t4, dyn<br> [0x80009570]:csrrs a2, fcsr, zero<br> [0x80009574]:sw t6, 752(fp)<br>    |
| 761|[0x80013cd0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x38b31c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800095a4]:fmul.s t6, t5, t4, dyn<br> [0x800095a8]:csrrs a2, fcsr, zero<br> [0x800095ac]:sw t6, 760(fp)<br>    |
| 762|[0x80013cd8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x38b31c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800095dc]:fmul.s t6, t5, t4, dyn<br> [0x800095e0]:csrrs a2, fcsr, zero<br> [0x800095e4]:sw t6, 768(fp)<br>    |
| 763|[0x80013ce0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x38b31c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80009614]:fmul.s t6, t5, t4, dyn<br> [0x80009618]:csrrs a2, fcsr, zero<br> [0x8000961c]:sw t6, 776(fp)<br>    |
| 764|[0x80013ce8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x38b31c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000964c]:fmul.s t6, t5, t4, dyn<br> [0x80009650]:csrrs a2, fcsr, zero<br> [0x80009654]:sw t6, 784(fp)<br>    |
| 765|[0x80013cf0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x38b31c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80009684]:fmul.s t6, t5, t4, dyn<br> [0x80009688]:csrrs a2, fcsr, zero<br> [0x8000968c]:sw t6, 792(fp)<br>    |
| 766|[0x80013cf8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0f285b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800096bc]:fmul.s t6, t5, t4, dyn<br> [0x800096c0]:csrrs a2, fcsr, zero<br> [0x800096c4]:sw t6, 800(fp)<br>    |
| 767|[0x80013d00]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0f285b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800096f4]:fmul.s t6, t5, t4, dyn<br> [0x800096f8]:csrrs a2, fcsr, zero<br> [0x800096fc]:sw t6, 808(fp)<br>    |
| 768|[0x80013d08]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0f285b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000972c]:fmul.s t6, t5, t4, dyn<br> [0x80009730]:csrrs a2, fcsr, zero<br> [0x80009734]:sw t6, 816(fp)<br>    |
| 769|[0x80013d10]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0f285b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80009764]:fmul.s t6, t5, t4, dyn<br> [0x80009768]:csrrs a2, fcsr, zero<br> [0x8000976c]:sw t6, 824(fp)<br>    |
| 770|[0x80013d18]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0f285b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000979c]:fmul.s t6, t5, t4, dyn<br> [0x800097a0]:csrrs a2, fcsr, zero<br> [0x800097a4]:sw t6, 832(fp)<br>    |
| 771|[0x80013d20]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x123a99 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800097d4]:fmul.s t6, t5, t4, dyn<br> [0x800097d8]:csrrs a2, fcsr, zero<br> [0x800097dc]:sw t6, 840(fp)<br>    |
| 772|[0x80013d28]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x123a99 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000980c]:fmul.s t6, t5, t4, dyn<br> [0x80009810]:csrrs a2, fcsr, zero<br> [0x80009814]:sw t6, 848(fp)<br>    |
| 773|[0x80013d30]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x123a99 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80009844]:fmul.s t6, t5, t4, dyn<br> [0x80009848]:csrrs a2, fcsr, zero<br> [0x8000984c]:sw t6, 856(fp)<br>    |
| 774|[0x80013d38]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x123a99 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000987c]:fmul.s t6, t5, t4, dyn<br> [0x80009880]:csrrs a2, fcsr, zero<br> [0x80009884]:sw t6, 864(fp)<br>    |
| 775|[0x80013d40]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x123a99 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800098b4]:fmul.s t6, t5, t4, dyn<br> [0x800098b8]:csrrs a2, fcsr, zero<br> [0x800098bc]:sw t6, 872(fp)<br>    |
| 776|[0x80013d48]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x2ccc93 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800098ec]:fmul.s t6, t5, t4, dyn<br> [0x800098f0]:csrrs a2, fcsr, zero<br> [0x800098f4]:sw t6, 880(fp)<br>    |
| 777|[0x80013d50]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x2ccc93 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80009924]:fmul.s t6, t5, t4, dyn<br> [0x80009928]:csrrs a2, fcsr, zero<br> [0x8000992c]:sw t6, 888(fp)<br>    |
| 778|[0x80013d58]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x2ccc93 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000995c]:fmul.s t6, t5, t4, dyn<br> [0x80009960]:csrrs a2, fcsr, zero<br> [0x80009964]:sw t6, 896(fp)<br>    |
| 779|[0x80013d60]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x2ccc93 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80009994]:fmul.s t6, t5, t4, dyn<br> [0x80009998]:csrrs a2, fcsr, zero<br> [0x8000999c]:sw t6, 904(fp)<br>    |
| 780|[0x80013d68]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x2ccc93 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800099cc]:fmul.s t6, t5, t4, dyn<br> [0x800099d0]:csrrs a2, fcsr, zero<br> [0x800099d4]:sw t6, 912(fp)<br>    |
| 781|[0x80013d70]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x1aa55e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80009a04]:fmul.s t6, t5, t4, dyn<br> [0x80009a08]:csrrs a2, fcsr, zero<br> [0x80009a0c]:sw t6, 920(fp)<br>    |
| 782|[0x80013d78]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x1aa55e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80009a3c]:fmul.s t6, t5, t4, dyn<br> [0x80009a40]:csrrs a2, fcsr, zero<br> [0x80009a44]:sw t6, 928(fp)<br>    |
| 783|[0x80013d80]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x1aa55e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80009a74]:fmul.s t6, t5, t4, dyn<br> [0x80009a78]:csrrs a2, fcsr, zero<br> [0x80009a7c]:sw t6, 936(fp)<br>    |
| 784|[0x80013d88]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x1aa55e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80009aac]:fmul.s t6, t5, t4, dyn<br> [0x80009ab0]:csrrs a2, fcsr, zero<br> [0x80009ab4]:sw t6, 944(fp)<br>    |
| 785|[0x80013d90]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x1aa55e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80009ae4]:fmul.s t6, t5, t4, dyn<br> [0x80009ae8]:csrrs a2, fcsr, zero<br> [0x80009aec]:sw t6, 952(fp)<br>    |
| 786|[0x80013d98]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3ba101 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80009b1c]:fmul.s t6, t5, t4, dyn<br> [0x80009b20]:csrrs a2, fcsr, zero<br> [0x80009b24]:sw t6, 960(fp)<br>    |
| 787|[0x80013da0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3ba101 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80009b54]:fmul.s t6, t5, t4, dyn<br> [0x80009b58]:csrrs a2, fcsr, zero<br> [0x80009b5c]:sw t6, 968(fp)<br>    |
| 788|[0x80013da8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3ba101 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80009b8c]:fmul.s t6, t5, t4, dyn<br> [0x80009b90]:csrrs a2, fcsr, zero<br> [0x80009b94]:sw t6, 976(fp)<br>    |
| 789|[0x80013db0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3ba101 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80009bc4]:fmul.s t6, t5, t4, dyn<br> [0x80009bc8]:csrrs a2, fcsr, zero<br> [0x80009bcc]:sw t6, 984(fp)<br>    |
| 790|[0x80013db8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3ba101 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80009bfc]:fmul.s t6, t5, t4, dyn<br> [0x80009c00]:csrrs a2, fcsr, zero<br> [0x80009c04]:sw t6, 992(fp)<br>    |
| 791|[0x80013dc0]<br>0x80000000|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x0e9cab and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80009c3c]:fmul.s t6, t5, t4, dyn<br> [0x80009c40]:csrrs a2, fcsr, zero<br> [0x80009c44]:sw t6, 1000(fp)<br>   |
| 792|[0x80013dc8]<br>0x80000000|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x0e9cab and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80009c7c]:fmul.s t6, t5, t4, dyn<br> [0x80009c80]:csrrs a2, fcsr, zero<br> [0x80009c84]:sw t6, 1008(fp)<br>   |
| 793|[0x80013dd0]<br>0x80000000|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x0e9cab and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80009cbc]:fmul.s t6, t5, t4, dyn<br> [0x80009cc0]:csrrs a2, fcsr, zero<br> [0x80009cc4]:sw t6, 1016(fp)<br>   |
| 794|[0x80013dd8]<br>0x80000000|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x0e9cab and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80009d04]:fmul.s t6, t5, t4, dyn<br> [0x80009d08]:csrrs a2, fcsr, zero<br> [0x80009d0c]:sw t6, 0(fp)<br>      |
| 795|[0x80013de0]<br>0x80000000|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x0e9cab and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80009d44]:fmul.s t6, t5, t4, dyn<br> [0x80009d48]:csrrs a2, fcsr, zero<br> [0x80009d4c]:sw t6, 8(fp)<br>      |
| 796|[0x80013de8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x089f67 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80009d84]:fmul.s t6, t5, t4, dyn<br> [0x80009d88]:csrrs a2, fcsr, zero<br> [0x80009d8c]:sw t6, 16(fp)<br>     |
| 797|[0x80013df0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x089f67 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80009dc4]:fmul.s t6, t5, t4, dyn<br> [0x80009dc8]:csrrs a2, fcsr, zero<br> [0x80009dcc]:sw t6, 24(fp)<br>     |
| 798|[0x80013df8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x089f67 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80009e04]:fmul.s t6, t5, t4, dyn<br> [0x80009e08]:csrrs a2, fcsr, zero<br> [0x80009e0c]:sw t6, 32(fp)<br>     |
| 799|[0x80013e00]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x089f67 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80009e44]:fmul.s t6, t5, t4, dyn<br> [0x80009e48]:csrrs a2, fcsr, zero<br> [0x80009e4c]:sw t6, 40(fp)<br>     |
| 800|[0x80013e08]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x089f67 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80009e84]:fmul.s t6, t5, t4, dyn<br> [0x80009e88]:csrrs a2, fcsr, zero<br> [0x80009e8c]:sw t6, 48(fp)<br>     |
| 801|[0x80013e10]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x79a1f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80009ec4]:fmul.s t6, t5, t4, dyn<br> [0x80009ec8]:csrrs a2, fcsr, zero<br> [0x80009ecc]:sw t6, 56(fp)<br>     |
| 802|[0x80013e18]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x79a1f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80009f04]:fmul.s t6, t5, t4, dyn<br> [0x80009f08]:csrrs a2, fcsr, zero<br> [0x80009f0c]:sw t6, 64(fp)<br>     |
| 803|[0x80013e20]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x79a1f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80009f44]:fmul.s t6, t5, t4, dyn<br> [0x80009f48]:csrrs a2, fcsr, zero<br> [0x80009f4c]:sw t6, 72(fp)<br>     |
| 804|[0x80013e28]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x79a1f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80009f84]:fmul.s t6, t5, t4, dyn<br> [0x80009f88]:csrrs a2, fcsr, zero<br> [0x80009f8c]:sw t6, 80(fp)<br>     |
| 805|[0x80013e30]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x79a1f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80009fc4]:fmul.s t6, t5, t4, dyn<br> [0x80009fc8]:csrrs a2, fcsr, zero<br> [0x80009fcc]:sw t6, 88(fp)<br>     |
| 806|[0x80013e38]<br>0x00000000|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x529595 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000a004]:fmul.s t6, t5, t4, dyn<br> [0x8000a008]:csrrs a2, fcsr, zero<br> [0x8000a00c]:sw t6, 96(fp)<br>     |
| 807|[0x80013e40]<br>0x00000000|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x529595 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a044]:fmul.s t6, t5, t4, dyn<br> [0x8000a048]:csrrs a2, fcsr, zero<br> [0x8000a04c]:sw t6, 104(fp)<br>    |
| 808|[0x80013e48]<br>0x00000000|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x529595 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a084]:fmul.s t6, t5, t4, dyn<br> [0x8000a088]:csrrs a2, fcsr, zero<br> [0x8000a08c]:sw t6, 112(fp)<br>    |
| 809|[0x80013e50]<br>0x00000000|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x529595 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a0c4]:fmul.s t6, t5, t4, dyn<br> [0x8000a0c8]:csrrs a2, fcsr, zero<br> [0x8000a0cc]:sw t6, 120(fp)<br>    |
| 810|[0x80013e58]<br>0x00000000|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x529595 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a104]:fmul.s t6, t5, t4, dyn<br> [0x8000a108]:csrrs a2, fcsr, zero<br> [0x8000a10c]:sw t6, 128(fp)<br>    |
| 811|[0x80013e60]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0c0e98 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000a144]:fmul.s t6, t5, t4, dyn<br> [0x8000a148]:csrrs a2, fcsr, zero<br> [0x8000a14c]:sw t6, 136(fp)<br>    |
| 812|[0x80013e68]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0c0e98 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a184]:fmul.s t6, t5, t4, dyn<br> [0x8000a188]:csrrs a2, fcsr, zero<br> [0x8000a18c]:sw t6, 144(fp)<br>    |
| 813|[0x80013e70]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0c0e98 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a1c4]:fmul.s t6, t5, t4, dyn<br> [0x8000a1c8]:csrrs a2, fcsr, zero<br> [0x8000a1cc]:sw t6, 152(fp)<br>    |
| 814|[0x80013e78]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0c0e98 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a204]:fmul.s t6, t5, t4, dyn<br> [0x8000a208]:csrrs a2, fcsr, zero<br> [0x8000a20c]:sw t6, 160(fp)<br>    |
| 815|[0x80013e80]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0c0e98 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a244]:fmul.s t6, t5, t4, dyn<br> [0x8000a248]:csrrs a2, fcsr, zero<br> [0x8000a24c]:sw t6, 168(fp)<br>    |
| 816|[0x80013e88]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2dd26f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000a284]:fmul.s t6, t5, t4, dyn<br> [0x8000a288]:csrrs a2, fcsr, zero<br> [0x8000a28c]:sw t6, 176(fp)<br>    |
| 817|[0x80013e90]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2dd26f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a2c4]:fmul.s t6, t5, t4, dyn<br> [0x8000a2c8]:csrrs a2, fcsr, zero<br> [0x8000a2cc]:sw t6, 184(fp)<br>    |
| 818|[0x80013e98]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2dd26f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a304]:fmul.s t6, t5, t4, dyn<br> [0x8000a308]:csrrs a2, fcsr, zero<br> [0x8000a30c]:sw t6, 192(fp)<br>    |
| 819|[0x80013ea0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2dd26f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a344]:fmul.s t6, t5, t4, dyn<br> [0x8000a348]:csrrs a2, fcsr, zero<br> [0x8000a34c]:sw t6, 200(fp)<br>    |
| 820|[0x80013ea8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2dd26f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a384]:fmul.s t6, t5, t4, dyn<br> [0x8000a388]:csrrs a2, fcsr, zero<br> [0x8000a38c]:sw t6, 208(fp)<br>    |
| 821|[0x80013eb0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x1a4d72 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000a3c4]:fmul.s t6, t5, t4, dyn<br> [0x8000a3c8]:csrrs a2, fcsr, zero<br> [0x8000a3cc]:sw t6, 216(fp)<br>    |
| 822|[0x80013eb8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x1a4d72 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a404]:fmul.s t6, t5, t4, dyn<br> [0x8000a408]:csrrs a2, fcsr, zero<br> [0x8000a40c]:sw t6, 224(fp)<br>    |
| 823|[0x80013ec0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x1a4d72 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a444]:fmul.s t6, t5, t4, dyn<br> [0x8000a448]:csrrs a2, fcsr, zero<br> [0x8000a44c]:sw t6, 232(fp)<br>    |
| 824|[0x80013ec8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x1a4d72 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a484]:fmul.s t6, t5, t4, dyn<br> [0x8000a488]:csrrs a2, fcsr, zero<br> [0x8000a48c]:sw t6, 240(fp)<br>    |
| 825|[0x80013ed0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x1a4d72 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a4c4]:fmul.s t6, t5, t4, dyn<br> [0x8000a4c8]:csrrs a2, fcsr, zero<br> [0x8000a4cc]:sw t6, 248(fp)<br>    |
| 826|[0x80013ed8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x13439e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000a504]:fmul.s t6, t5, t4, dyn<br> [0x8000a508]:csrrs a2, fcsr, zero<br> [0x8000a50c]:sw t6, 256(fp)<br>    |
| 827|[0x80013ee0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x13439e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a544]:fmul.s t6, t5, t4, dyn<br> [0x8000a548]:csrrs a2, fcsr, zero<br> [0x8000a54c]:sw t6, 264(fp)<br>    |
| 828|[0x80013ee8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x13439e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a584]:fmul.s t6, t5, t4, dyn<br> [0x8000a588]:csrrs a2, fcsr, zero<br> [0x8000a58c]:sw t6, 272(fp)<br>    |
| 829|[0x80013ef0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x13439e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a5c4]:fmul.s t6, t5, t4, dyn<br> [0x8000a5c8]:csrrs a2, fcsr, zero<br> [0x8000a5cc]:sw t6, 280(fp)<br>    |
| 830|[0x80013ef8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x13439e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a604]:fmul.s t6, t5, t4, dyn<br> [0x8000a608]:csrrs a2, fcsr, zero<br> [0x8000a60c]:sw t6, 288(fp)<br>    |
| 831|[0x80013f00]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0ee529 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000a644]:fmul.s t6, t5, t4, dyn<br> [0x8000a648]:csrrs a2, fcsr, zero<br> [0x8000a64c]:sw t6, 296(fp)<br>    |
| 832|[0x80013f08]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0ee529 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a684]:fmul.s t6, t5, t4, dyn<br> [0x8000a688]:csrrs a2, fcsr, zero<br> [0x8000a68c]:sw t6, 304(fp)<br>    |
| 833|[0x80013f10]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0ee529 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a6c4]:fmul.s t6, t5, t4, dyn<br> [0x8000a6c8]:csrrs a2, fcsr, zero<br> [0x8000a6cc]:sw t6, 312(fp)<br>    |
| 834|[0x80013f18]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0ee529 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a704]:fmul.s t6, t5, t4, dyn<br> [0x8000a708]:csrrs a2, fcsr, zero<br> [0x8000a70c]:sw t6, 320(fp)<br>    |
| 835|[0x80013f20]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0ee529 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a744]:fmul.s t6, t5, t4, dyn<br> [0x8000a748]:csrrs a2, fcsr, zero<br> [0x8000a74c]:sw t6, 328(fp)<br>    |
| 836|[0x80013f28]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x131155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000a784]:fmul.s t6, t5, t4, dyn<br> [0x8000a788]:csrrs a2, fcsr, zero<br> [0x8000a78c]:sw t6, 336(fp)<br>    |
| 837|[0x80013f30]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x131155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a7c4]:fmul.s t6, t5, t4, dyn<br> [0x8000a7c8]:csrrs a2, fcsr, zero<br> [0x8000a7cc]:sw t6, 344(fp)<br>    |
| 838|[0x80013f38]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x131155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a804]:fmul.s t6, t5, t4, dyn<br> [0x8000a808]:csrrs a2, fcsr, zero<br> [0x8000a80c]:sw t6, 352(fp)<br>    |
| 839|[0x80013f40]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x131155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a844]:fmul.s t6, t5, t4, dyn<br> [0x8000a848]:csrrs a2, fcsr, zero<br> [0x8000a84c]:sw t6, 360(fp)<br>    |
| 840|[0x80013f48]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x131155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a884]:fmul.s t6, t5, t4, dyn<br> [0x8000a888]:csrrs a2, fcsr, zero<br> [0x8000a88c]:sw t6, 368(fp)<br>    |
| 841|[0x80013f50]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x53c5ea and fs2 == 0 and fe2 == 0x2d and fm2 == 0x1abb4c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000a8c4]:fmul.s t6, t5, t4, dyn<br> [0x8000a8c8]:csrrs a2, fcsr, zero<br> [0x8000a8cc]:sw t6, 376(fp)<br>    |
| 842|[0x80013f58]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x53c5ea and fs2 == 0 and fe2 == 0x2d and fm2 == 0x1abb4c and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a904]:fmul.s t6, t5, t4, dyn<br> [0x8000a908]:csrrs a2, fcsr, zero<br> [0x8000a90c]:sw t6, 384(fp)<br>    |
| 843|[0x80013f60]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x53c5ea and fs2 == 0 and fe2 == 0x2d and fm2 == 0x1abb4c and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a944]:fmul.s t6, t5, t4, dyn<br> [0x8000a948]:csrrs a2, fcsr, zero<br> [0x8000a94c]:sw t6, 392(fp)<br>    |
| 844|[0x80013f68]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x53c5ea and fs2 == 0 and fe2 == 0x2d and fm2 == 0x1abb4c and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a984]:fmul.s t6, t5, t4, dyn<br> [0x8000a988]:csrrs a2, fcsr, zero<br> [0x8000a98c]:sw t6, 400(fp)<br>    |
| 845|[0x80013f70]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x53c5ea and fs2 == 0 and fe2 == 0x2d and fm2 == 0x1abb4c and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000a9c4]:fmul.s t6, t5, t4, dyn<br> [0x8000a9c8]:csrrs a2, fcsr, zero<br> [0x8000a9cc]:sw t6, 408(fp)<br>    |
| 846|[0x80013f78]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x26cf0e and fs2 == 0 and fe2 == 0x2b and fm2 == 0x4470c1 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000aa04]:fmul.s t6, t5, t4, dyn<br> [0x8000aa08]:csrrs a2, fcsr, zero<br> [0x8000aa0c]:sw t6, 416(fp)<br>    |
| 847|[0x80013f80]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x26cf0e and fs2 == 0 and fe2 == 0x2b and fm2 == 0x4470c1 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000aa44]:fmul.s t6, t5, t4, dyn<br> [0x8000aa48]:csrrs a2, fcsr, zero<br> [0x8000aa4c]:sw t6, 424(fp)<br>    |
| 848|[0x80013f88]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x26cf0e and fs2 == 0 and fe2 == 0x2b and fm2 == 0x4470c1 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000aa84]:fmul.s t6, t5, t4, dyn<br> [0x8000aa88]:csrrs a2, fcsr, zero<br> [0x8000aa8c]:sw t6, 432(fp)<br>    |
| 849|[0x80013f90]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x26cf0e and fs2 == 0 and fe2 == 0x2b and fm2 == 0x4470c1 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000aac4]:fmul.s t6, t5, t4, dyn<br> [0x8000aac8]:csrrs a2, fcsr, zero<br> [0x8000aacc]:sw t6, 440(fp)<br>    |
| 850|[0x80013f98]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x26cf0e and fs2 == 0 and fe2 == 0x2b and fm2 == 0x4470c1 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000ab04]:fmul.s t6, t5, t4, dyn<br> [0x8000ab08]:csrrs a2, fcsr, zero<br> [0x8000ab0c]:sw t6, 448(fp)<br>    |
| 851|[0x80013fa0]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x59615d and fs2 == 0 and fe2 == 0x2b and fm2 == 0x16bd8b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000ab44]:fmul.s t6, t5, t4, dyn<br> [0x8000ab48]:csrrs a2, fcsr, zero<br> [0x8000ab4c]:sw t6, 456(fp)<br>    |
| 852|[0x80013fa8]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x59615d and fs2 == 0 and fe2 == 0x2b and fm2 == 0x16bd8b and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000ab84]:fmul.s t6, t5, t4, dyn<br> [0x8000ab88]:csrrs a2, fcsr, zero<br> [0x8000ab8c]:sw t6, 464(fp)<br>    |
| 853|[0x80013fb0]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x59615d and fs2 == 0 and fe2 == 0x2b and fm2 == 0x16bd8b and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000abc4]:fmul.s t6, t5, t4, dyn<br> [0x8000abc8]:csrrs a2, fcsr, zero<br> [0x8000abcc]:sw t6, 472(fp)<br>    |
| 854|[0x80013fb8]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x59615d and fs2 == 0 and fe2 == 0x2b and fm2 == 0x16bd8b and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000ac04]:fmul.s t6, t5, t4, dyn<br> [0x8000ac08]:csrrs a2, fcsr, zero<br> [0x8000ac0c]:sw t6, 480(fp)<br>    |
| 855|[0x80013fc0]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x59615d and fs2 == 0 and fe2 == 0x2b and fm2 == 0x16bd8b and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000ac44]:fmul.s t6, t5, t4, dyn<br> [0x8000ac48]:csrrs a2, fcsr, zero<br> [0x8000ac4c]:sw t6, 488(fp)<br>    |
| 856|[0x80013fc8]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x1d7025 and fs2 == 0 and fe2 == 0x2c and fm2 == 0x5021f4 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000ac84]:fmul.s t6, t5, t4, dyn<br> [0x8000ac88]:csrrs a2, fcsr, zero<br> [0x8000ac8c]:sw t6, 496(fp)<br>    |
| 857|[0x80013fd0]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x1d7025 and fs2 == 0 and fe2 == 0x2c and fm2 == 0x5021f4 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000acc4]:fmul.s t6, t5, t4, dyn<br> [0x8000acc8]:csrrs a2, fcsr, zero<br> [0x8000accc]:sw t6, 504(fp)<br>    |
| 858|[0x80013fd8]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x1d7025 and fs2 == 0 and fe2 == 0x2c and fm2 == 0x5021f4 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000ad04]:fmul.s t6, t5, t4, dyn<br> [0x8000ad08]:csrrs a2, fcsr, zero<br> [0x8000ad0c]:sw t6, 512(fp)<br>    |
| 859|[0x80013fe0]<br>0x55000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x1d7025 and fs2 == 0 and fe2 == 0x2c and fm2 == 0x5021f4 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000ad44]:fmul.s t6, t5, t4, dyn<br> [0x8000ad48]:csrrs a2, fcsr, zero<br> [0x8000ad4c]:sw t6, 520(fp)<br>    |
| 860|[0x80013fe8]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x1d7025 and fs2 == 0 and fe2 == 0x2c and fm2 == 0x5021f4 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000ad84]:fmul.s t6, t5, t4, dyn<br> [0x8000ad88]:csrrs a2, fcsr, zero<br> [0x8000ad8c]:sw t6, 528(fp)<br>    |
| 861|[0x80013ff0]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x5fb832 and fs2 == 0 and fe2 == 0x2e and fm2 == 0x127817 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000adc4]:fmul.s t6, t5, t4, dyn<br> [0x8000adc8]:csrrs a2, fcsr, zero<br> [0x8000adcc]:sw t6, 536(fp)<br>    |
| 862|[0x80013ff8]<br>0x54FFFFFD|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x5fb832 and fs2 == 0 and fe2 == 0x2e and fm2 == 0x127817 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000ae04]:fmul.s t6, t5, t4, dyn<br> [0x8000ae08]:csrrs a2, fcsr, zero<br> [0x8000ae0c]:sw t6, 544(fp)<br>    |
| 863|[0x80014000]<br>0x54FFFFFD|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x5fb832 and fs2 == 0 and fe2 == 0x2e and fm2 == 0x127817 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000ae44]:fmul.s t6, t5, t4, dyn<br> [0x8000ae48]:csrrs a2, fcsr, zero<br> [0x8000ae4c]:sw t6, 552(fp)<br>    |
| 864|[0x80014008]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x5fb832 and fs2 == 0 and fe2 == 0x2e and fm2 == 0x127817 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000ae84]:fmul.s t6, t5, t4, dyn<br> [0x8000ae88]:csrrs a2, fcsr, zero<br> [0x8000ae8c]:sw t6, 560(fp)<br>    |
| 865|[0x80014010]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x5fb832 and fs2 == 0 and fe2 == 0x2e and fm2 == 0x127817 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000aec4]:fmul.s t6, t5, t4, dyn<br> [0x8000aec8]:csrrs a2, fcsr, zero<br> [0x8000aecc]:sw t6, 568(fp)<br>    |
| 866|[0x80014018]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a6708 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x2fcaaa and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000af04]:fmul.s t6, t5, t4, dyn<br> [0x8000af08]:csrrs a2, fcsr, zero<br> [0x8000af0c]:sw t6, 576(fp)<br>    |
| 867|[0x80014020]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a6708 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x2fcaaa and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000af44]:fmul.s t6, t5, t4, dyn<br> [0x8000af48]:csrrs a2, fcsr, zero<br> [0x8000af4c]:sw t6, 584(fp)<br>    |
| 868|[0x80014028]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a6708 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x2fcaaa and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000af84]:fmul.s t6, t5, t4, dyn<br> [0x8000af88]:csrrs a2, fcsr, zero<br> [0x8000af8c]:sw t6, 592(fp)<br>    |
| 869|[0x80014030]<br>0x55000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a6708 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x2fcaaa and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000afc4]:fmul.s t6, t5, t4, dyn<br> [0x8000afc8]:csrrs a2, fcsr, zero<br> [0x8000afcc]:sw t6, 600(fp)<br>    |
| 870|[0x80014038]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a6708 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x2fcaaa and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b004]:fmul.s t6, t5, t4, dyn<br> [0x8000b008]:csrrs a2, fcsr, zero<br> [0x8000b00c]:sw t6, 608(fp)<br>    |
| 871|[0x80014040]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7cd38d and fs2 == 0 and fe2 == 0x2b and fm2 == 0x019b52 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000b044]:fmul.s t6, t5, t4, dyn<br> [0x8000b048]:csrrs a2, fcsr, zero<br> [0x8000b04c]:sw t6, 616(fp)<br>    |
| 872|[0x80014048]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7cd38d and fs2 == 0 and fe2 == 0x2b and fm2 == 0x019b52 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b084]:fmul.s t6, t5, t4, dyn<br> [0x8000b088]:csrrs a2, fcsr, zero<br> [0x8000b08c]:sw t6, 624(fp)<br>    |
| 873|[0x80014050]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7cd38d and fs2 == 0 and fe2 == 0x2b and fm2 == 0x019b52 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b0c4]:fmul.s t6, t5, t4, dyn<br> [0x8000b0c8]:csrrs a2, fcsr, zero<br> [0x8000b0cc]:sw t6, 632(fp)<br>    |
| 874|[0x80014058]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7cd38d and fs2 == 0 and fe2 == 0x2b and fm2 == 0x019b52 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b104]:fmul.s t6, t5, t4, dyn<br> [0x8000b108]:csrrs a2, fcsr, zero<br> [0x8000b10c]:sw t6, 640(fp)<br>    |
| 875|[0x80014060]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7cd38d and fs2 == 0 and fe2 == 0x2b and fm2 == 0x019b52 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b144]:fmul.s t6, t5, t4, dyn<br> [0x8000b148]:csrrs a2, fcsr, zero<br> [0x8000b14c]:sw t6, 648(fp)<br>    |
| 876|[0x80014068]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a3d7b and fs2 == 1 and fe2 == 0x2b and fm2 == 0x2ff1e2 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000b184]:fmul.s t6, t5, t4, dyn<br> [0x8000b188]:csrrs a2, fcsr, zero<br> [0x8000b18c]:sw t6, 656(fp)<br>    |
| 877|[0x80014070]<br>0xD4FFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a3d7b and fs2 == 1 and fe2 == 0x2b and fm2 == 0x2ff1e2 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b1c4]:fmul.s t6, t5, t4, dyn<br> [0x8000b1c8]:csrrs a2, fcsr, zero<br> [0x8000b1cc]:sw t6, 664(fp)<br>    |
| 878|[0x80014078]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a3d7b and fs2 == 1 and fe2 == 0x2b and fm2 == 0x2ff1e2 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b204]:fmul.s t6, t5, t4, dyn<br> [0x8000b208]:csrrs a2, fcsr, zero<br> [0x8000b20c]:sw t6, 672(fp)<br>    |
| 879|[0x80014080]<br>0xD4FFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a3d7b and fs2 == 1 and fe2 == 0x2b and fm2 == 0x2ff1e2 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b244]:fmul.s t6, t5, t4, dyn<br> [0x8000b248]:csrrs a2, fcsr, zero<br> [0x8000b24c]:sw t6, 680(fp)<br>    |
| 880|[0x80014088]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a3d7b and fs2 == 1 and fe2 == 0x2b and fm2 == 0x2ff1e2 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b284]:fmul.s t6, t5, t4, dyn<br> [0x8000b288]:csrrs a2, fcsr, zero<br> [0x8000b28c]:sw t6, 688(fp)<br>    |
| 881|[0x80014090]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x009916 and fs2 == 1 and fe2 == 0x2b and fm2 == 0x7ecf3f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000b2c4]:fmul.s t6, t5, t4, dyn<br> [0x8000b2c8]:csrrs a2, fcsr, zero<br> [0x8000b2cc]:sw t6, 696(fp)<br>    |
| 882|[0x80014098]<br>0xD4FFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x009916 and fs2 == 1 and fe2 == 0x2b and fm2 == 0x7ecf3f and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b304]:fmul.s t6, t5, t4, dyn<br> [0x8000b308]:csrrs a2, fcsr, zero<br> [0x8000b30c]:sw t6, 704(fp)<br>    |
| 883|[0x800140a0]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x009916 and fs2 == 1 and fe2 == 0x2b and fm2 == 0x7ecf3f and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b344]:fmul.s t6, t5, t4, dyn<br> [0x8000b348]:csrrs a2, fcsr, zero<br> [0x8000b34c]:sw t6, 712(fp)<br>    |
| 884|[0x800140a8]<br>0xD4FFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x009916 and fs2 == 1 and fe2 == 0x2b and fm2 == 0x7ecf3f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b384]:fmul.s t6, t5, t4, dyn<br> [0x8000b388]:csrrs a2, fcsr, zero<br> [0x8000b38c]:sw t6, 720(fp)<br>    |
| 885|[0x800140b0]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x009916 and fs2 == 1 and fe2 == 0x2b and fm2 == 0x7ecf3f and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b3c4]:fmul.s t6, t5, t4, dyn<br> [0x8000b3c8]:csrrs a2, fcsr, zero<br> [0x8000b3cc]:sw t6, 728(fp)<br>    |
| 886|[0x800140b8]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x411d26 and fs2 == 1 and fe2 == 0x2b and fm2 == 0x29aea9 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000b404]:fmul.s t6, t5, t4, dyn<br> [0x8000b408]:csrrs a2, fcsr, zero<br> [0x8000b40c]:sw t6, 736(fp)<br>    |
| 887|[0x800140c0]<br>0xD4FFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x411d26 and fs2 == 1 and fe2 == 0x2b and fm2 == 0x29aea9 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b444]:fmul.s t6, t5, t4, dyn<br> [0x8000b448]:csrrs a2, fcsr, zero<br> [0x8000b44c]:sw t6, 744(fp)<br>    |
| 888|[0x800140c8]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x411d26 and fs2 == 1 and fe2 == 0x2b and fm2 == 0x29aea9 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b484]:fmul.s t6, t5, t4, dyn<br> [0x8000b488]:csrrs a2, fcsr, zero<br> [0x8000b48c]:sw t6, 752(fp)<br>    |
| 889|[0x800140d0]<br>0xD4FFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x411d26 and fs2 == 1 and fe2 == 0x2b and fm2 == 0x29aea9 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b4c4]:fmul.s t6, t5, t4, dyn<br> [0x8000b4c8]:csrrs a2, fcsr, zero<br> [0x8000b4cc]:sw t6, 760(fp)<br>    |
| 890|[0x800140d8]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x411d26 and fs2 == 1 and fe2 == 0x2b and fm2 == 0x29aea9 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b504]:fmul.s t6, t5, t4, dyn<br> [0x8000b508]:csrrs a2, fcsr, zero<br> [0x8000b50c]:sw t6, 768(fp)<br>    |
| 891|[0x800140e0]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x752c06 and fs2 == 1 and fe2 == 0x2e and fm2 == 0x05a733 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000b544]:fmul.s t6, t5, t4, dyn<br> [0x8000b548]:csrrs a2, fcsr, zero<br> [0x8000b54c]:sw t6, 776(fp)<br>    |
| 892|[0x800140e8]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x752c06 and fs2 == 1 and fe2 == 0x2e and fm2 == 0x05a733 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b584]:fmul.s t6, t5, t4, dyn<br> [0x8000b588]:csrrs a2, fcsr, zero<br> [0x8000b58c]:sw t6, 784(fp)<br>    |
| 893|[0x800140f0]<br>0xD5000000|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x752c06 and fs2 == 1 and fe2 == 0x2e and fm2 == 0x05a733 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b5c4]:fmul.s t6, t5, t4, dyn<br> [0x8000b5c8]:csrrs a2, fcsr, zero<br> [0x8000b5cc]:sw t6, 792(fp)<br>    |
| 894|[0x800140f8]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x752c06 and fs2 == 1 and fe2 == 0x2e and fm2 == 0x05a733 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b604]:fmul.s t6, t5, t4, dyn<br> [0x8000b608]:csrrs a2, fcsr, zero<br> [0x8000b60c]:sw t6, 800(fp)<br>    |
| 895|[0x80014100]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfa and fm1 == 0x752c06 and fs2 == 1 and fe2 == 0x2e and fm2 == 0x05a733 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b644]:fmul.s t6, t5, t4, dyn<br> [0x8000b648]:csrrs a2, fcsr, zero<br> [0x8000b64c]:sw t6, 808(fp)<br>    |
| 896|[0x80014108]<br>0xD4FFFFFE|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x0566bc and fs2 == 1 and fe2 == 0x2c and fm2 == 0x75a27f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000b684]:fmul.s t6, t5, t4, dyn<br> [0x8000b688]:csrrs a2, fcsr, zero<br> [0x8000b68c]:sw t6, 816(fp)<br>    |
| 897|[0x80014110]<br>0xD4FFFFFE|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x0566bc and fs2 == 1 and fe2 == 0x2c and fm2 == 0x75a27f and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b6c4]:fmul.s t6, t5, t4, dyn<br> [0x8000b6c8]:csrrs a2, fcsr, zero<br> [0x8000b6cc]:sw t6, 824(fp)<br>    |
| 898|[0x80014118]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x0566bc and fs2 == 1 and fe2 == 0x2c and fm2 == 0x75a27f and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b704]:fmul.s t6, t5, t4, dyn<br> [0x8000b708]:csrrs a2, fcsr, zero<br> [0x8000b70c]:sw t6, 832(fp)<br>    |
| 899|[0x80014120]<br>0xD4FFFFFE|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x0566bc and fs2 == 1 and fe2 == 0x2c and fm2 == 0x75a27f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b744]:fmul.s t6, t5, t4, dyn<br> [0x8000b748]:csrrs a2, fcsr, zero<br> [0x8000b74c]:sw t6, 840(fp)<br>    |
| 900|[0x80014128]<br>0xD4FFFFFE|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x0566bc and fs2 == 1 and fe2 == 0x2c and fm2 == 0x75a27f and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b784]:fmul.s t6, t5, t4, dyn<br> [0x8000b788]:csrrs a2, fcsr, zero<br> [0x8000b78c]:sw t6, 848(fp)<br>    |
| 901|[0x80014130]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1699a5 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x59952e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000b7c4]:fmul.s t6, t5, t4, dyn<br> [0x8000b7c8]:csrrs a2, fcsr, zero<br> [0x8000b7cc]:sw t6, 856(fp)<br>    |
| 902|[0x80014138]<br>0xD4FFFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1699a5 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x59952e and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b804]:fmul.s t6, t5, t4, dyn<br> [0x8000b808]:csrrs a2, fcsr, zero<br> [0x8000b80c]:sw t6, 864(fp)<br>    |
| 903|[0x80014140]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1699a5 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x59952e and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b844]:fmul.s t6, t5, t4, dyn<br> [0x8000b848]:csrrs a2, fcsr, zero<br> [0x8000b84c]:sw t6, 872(fp)<br>    |
| 904|[0x80014148]<br>0xD4FFFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1699a5 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x59952e and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b884]:fmul.s t6, t5, t4, dyn<br> [0x8000b888]:csrrs a2, fcsr, zero<br> [0x8000b88c]:sw t6, 880(fp)<br>    |
| 905|[0x80014150]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1699a5 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x59952e and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b8c4]:fmul.s t6, t5, t4, dyn<br> [0x8000b8c8]:csrrs a2, fcsr, zero<br> [0x8000b8cc]:sw t6, 888(fp)<br>    |
| 906|[0x80014158]<br>0xD4FFFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x019943 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x7cd791 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000b904]:fmul.s t6, t5, t4, dyn<br> [0x8000b908]:csrrs a2, fcsr, zero<br> [0x8000b90c]:sw t6, 896(fp)<br>    |
| 907|[0x80014160]<br>0xD4FFFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x019943 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x7cd791 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b944]:fmul.s t6, t5, t4, dyn<br> [0x8000b948]:csrrs a2, fcsr, zero<br> [0x8000b94c]:sw t6, 904(fp)<br>    |
| 908|[0x80014168]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x019943 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x7cd791 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b984]:fmul.s t6, t5, t4, dyn<br> [0x8000b988]:csrrs a2, fcsr, zero<br> [0x8000b98c]:sw t6, 912(fp)<br>    |
| 909|[0x80014170]<br>0xD4FFFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x019943 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x7cd791 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000b9c4]:fmul.s t6, t5, t4, dyn<br> [0x8000b9c8]:csrrs a2, fcsr, zero<br> [0x8000b9cc]:sw t6, 920(fp)<br>    |
| 910|[0x80014178]<br>0xD4FFFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x019943 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x7cd791 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000ba04]:fmul.s t6, t5, t4, dyn<br> [0x8000ba08]:csrrs a2, fcsr, zero<br> [0x8000ba0c]:sw t6, 928(fp)<br>    |
| 911|[0x80014180]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x25e482 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x45867e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000ba44]:fmul.s t6, t5, t4, dyn<br> [0x8000ba48]:csrrs a2, fcsr, zero<br> [0x8000ba4c]:sw t6, 936(fp)<br>    |
| 912|[0x80014188]<br>0xD4FFFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x25e482 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x45867e and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000ba84]:fmul.s t6, t5, t4, dyn<br> [0x8000ba88]:csrrs a2, fcsr, zero<br> [0x8000ba8c]:sw t6, 944(fp)<br>    |
| 913|[0x80014190]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x25e482 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x45867e and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000bac4]:fmul.s t6, t5, t4, dyn<br> [0x8000bac8]:csrrs a2, fcsr, zero<br> [0x8000bacc]:sw t6, 952(fp)<br>    |
| 914|[0x80014198]<br>0xD4FFFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x25e482 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x45867e and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000bb04]:fmul.s t6, t5, t4, dyn<br> [0x8000bb08]:csrrs a2, fcsr, zero<br> [0x8000bb0c]:sw t6, 960(fp)<br>    |
| 915|[0x800141a0]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x25e482 and fs2 == 1 and fe2 == 0x2a and fm2 == 0x45867e and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000bb44]:fmul.s t6, t5, t4, dyn<br> [0x8000bb48]:csrrs a2, fcsr, zero<br> [0x8000bb4c]:sw t6, 968(fp)<br>    |
| 916|[0x800141a8]<br>0xD4FFFFFE|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x79b52d and fs2 == 1 and fe2 == 0x2d and fm2 == 0x0339b4 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000bb84]:fmul.s t6, t5, t4, dyn<br> [0x8000bb88]:csrrs a2, fcsr, zero<br> [0x8000bb8c]:sw t6, 976(fp)<br>    |
| 917|[0x800141b0]<br>0xD4FFFFFD|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x79b52d and fs2 == 1 and fe2 == 0x2d and fm2 == 0x0339b4 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000bbc4]:fmul.s t6, t5, t4, dyn<br> [0x8000bbc8]:csrrs a2, fcsr, zero<br> [0x8000bbcc]:sw t6, 984(fp)<br>    |
| 918|[0x800141b8]<br>0xD4FFFFFE|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x79b52d and fs2 == 1 and fe2 == 0x2d and fm2 == 0x0339b4 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000bc04]:fmul.s t6, t5, t4, dyn<br> [0x8000bc08]:csrrs a2, fcsr, zero<br> [0x8000bc0c]:sw t6, 992(fp)<br>    |
| 919|[0x800141c0]<br>0xD4FFFFFD|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x79b52d and fs2 == 1 and fe2 == 0x2d and fm2 == 0x0339b4 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000bc44]:fmul.s t6, t5, t4, dyn<br> [0x8000bc48]:csrrs a2, fcsr, zero<br> [0x8000bc4c]:sw t6, 1000(fp)<br>   |
| 920|[0x800141c8]<br>0xD4FFFFFE|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x79b52d and fs2 == 1 and fe2 == 0x2d and fm2 == 0x0339b4 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000bc84]:fmul.s t6, t5, t4, dyn<br> [0x8000bc88]:csrrs a2, fcsr, zero<br> [0x8000bc8c]:sw t6, 1008(fp)<br>   |
| 921|[0x800141d0]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x326d9c and fs2 == 1 and fe2 == 0x2b and fm2 == 0x37a5ec and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000bcc4]:fmul.s t6, t5, t4, dyn<br> [0x8000bcc8]:csrrs a2, fcsr, zero<br> [0x8000bccc]:sw t6, 1016(fp)<br>   |
| 922|[0x800141d8]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x326d9c and fs2 == 1 and fe2 == 0x2b and fm2 == 0x37a5ec and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000bd0c]:fmul.s t6, t5, t4, dyn<br> [0x8000bd10]:csrrs a2, fcsr, zero<br> [0x8000bd14]:sw t6, 0(fp)<br>      |
| 923|[0x800141e0]<br>0xD5000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x326d9c and fs2 == 1 and fe2 == 0x2b and fm2 == 0x37a5ec and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000bd4c]:fmul.s t6, t5, t4, dyn<br> [0x8000bd50]:csrrs a2, fcsr, zero<br> [0x8000bd54]:sw t6, 8(fp)<br>      |
| 924|[0x800141e8]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x326d9c and fs2 == 1 and fe2 == 0x2b and fm2 == 0x37a5ec and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000bd8c]:fmul.s t6, t5, t4, dyn<br> [0x8000bd90]:csrrs a2, fcsr, zero<br> [0x8000bd94]:sw t6, 16(fp)<br>     |
| 925|[0x800141f0]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x326d9c and fs2 == 1 and fe2 == 0x2b and fm2 == 0x37a5ec and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000bdcc]:fmul.s t6, t5, t4, dyn<br> [0x8000bdd0]:csrrs a2, fcsr, zero<br> [0x8000bdd4]:sw t6, 24(fp)<br>     |
| 926|[0x800141f8]<br>0xD4FFFFFE|- fs1 == 0 and fe1 == 0xf7 and fm1 == 0x7f98a2 and fs2 == 1 and fe2 == 0x31 and fm2 == 0x0033c3 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000be0c]:fmul.s t6, t5, t4, dyn<br> [0x8000be10]:csrrs a2, fcsr, zero<br> [0x8000be14]:sw t6, 32(fp)<br>     |
| 927|[0x80014200]<br>0xD4FFFFFE|- fs1 == 0 and fe1 == 0xf7 and fm1 == 0x7f98a2 and fs2 == 1 and fe2 == 0x31 and fm2 == 0x0033c3 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000be4c]:fmul.s t6, t5, t4, dyn<br> [0x8000be50]:csrrs a2, fcsr, zero<br> [0x8000be54]:sw t6, 40(fp)<br>     |
| 928|[0x80014208]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xf7 and fm1 == 0x7f98a2 and fs2 == 1 and fe2 == 0x31 and fm2 == 0x0033c3 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000be8c]:fmul.s t6, t5, t4, dyn<br> [0x8000be90]:csrrs a2, fcsr, zero<br> [0x8000be94]:sw t6, 48(fp)<br>     |
| 929|[0x80014210]<br>0xD4FFFFFE|- fs1 == 0 and fe1 == 0xf7 and fm1 == 0x7f98a2 and fs2 == 1 and fe2 == 0x31 and fm2 == 0x0033c3 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000becc]:fmul.s t6, t5, t4, dyn<br> [0x8000bed0]:csrrs a2, fcsr, zero<br> [0x8000bed4]:sw t6, 56(fp)<br>     |
| 930|[0x80014218]<br>0xD4FFFFFE|- fs1 == 0 and fe1 == 0xf7 and fm1 == 0x7f98a2 and fs2 == 1 and fe2 == 0x31 and fm2 == 0x0033c3 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000bf0c]:fmul.s t6, t5, t4, dyn<br> [0x8000bf10]:csrrs a2, fcsr, zero<br> [0x8000bf14]:sw t6, 64(fp)<br>     |
| 931|[0x80014220]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x39a5be and fs2 == 1 and fe2 == 0x2a and fm2 == 0x3081b1 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000bf4c]:fmul.s t6, t5, t4, dyn<br> [0x8000bf50]:csrrs a2, fcsr, zero<br> [0x8000bf54]:sw t6, 72(fp)<br>     |
| 932|[0x80014228]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x39a5be and fs2 == 1 and fe2 == 0x2a and fm2 == 0x3081b1 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000bf8c]:fmul.s t6, t5, t4, dyn<br> [0x8000bf90]:csrrs a2, fcsr, zero<br> [0x8000bf94]:sw t6, 80(fp)<br>     |
| 933|[0x80014230]<br>0xD5000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x39a5be and fs2 == 1 and fe2 == 0x2a and fm2 == 0x3081b1 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000bfcc]:fmul.s t6, t5, t4, dyn<br> [0x8000bfd0]:csrrs a2, fcsr, zero<br> [0x8000bfd4]:sw t6, 88(fp)<br>     |
| 934|[0x80014238]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x39a5be and fs2 == 1 and fe2 == 0x2a and fm2 == 0x3081b1 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c00c]:fmul.s t6, t5, t4, dyn<br> [0x8000c010]:csrrs a2, fcsr, zero<br> [0x8000c014]:sw t6, 96(fp)<br>     |
| 935|[0x80014240]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x39a5be and fs2 == 1 and fe2 == 0x2a and fm2 == 0x3081b1 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c04c]:fmul.s t6, t5, t4, dyn<br> [0x8000c050]:csrrs a2, fcsr, zero<br> [0x8000c054]:sw t6, 104(fp)<br>    |
| 936|[0x80014248]<br>0xD5000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4e977e and fs2 == 1 and fe2 == 0x2b and fm2 == 0x1e9cc1 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000c08c]:fmul.s t6, t5, t4, dyn<br> [0x8000c090]:csrrs a2, fcsr, zero<br> [0x8000c094]:sw t6, 112(fp)<br>    |
| 937|[0x80014250]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4e977e and fs2 == 1 and fe2 == 0x2b and fm2 == 0x1e9cc1 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c0cc]:fmul.s t6, t5, t4, dyn<br> [0x8000c0d0]:csrrs a2, fcsr, zero<br> [0x8000c0d4]:sw t6, 120(fp)<br>    |
| 938|[0x80014258]<br>0xD5000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4e977e and fs2 == 1 and fe2 == 0x2b and fm2 == 0x1e9cc1 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c10c]:fmul.s t6, t5, t4, dyn<br> [0x8000c110]:csrrs a2, fcsr, zero<br> [0x8000c114]:sw t6, 128(fp)<br>    |
| 939|[0x80014260]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4e977e and fs2 == 1 and fe2 == 0x2b and fm2 == 0x1e9cc1 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c14c]:fmul.s t6, t5, t4, dyn<br> [0x8000c150]:csrrs a2, fcsr, zero<br> [0x8000c154]:sw t6, 136(fp)<br>    |
| 940|[0x80014268]<br>0xD5000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4e977e and fs2 == 1 and fe2 == 0x2b and fm2 == 0x1e9cc1 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c18c]:fmul.s t6, t5, t4, dyn<br> [0x8000c190]:csrrs a2, fcsr, zero<br> [0x8000c194]:sw t6, 144(fp)<br>    |
| 941|[0x80014270]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2a090e and fs2 == 1 and fe2 == 0x2b and fm2 == 0x40b67c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000c1cc]:fmul.s t6, t5, t4, dyn<br> [0x8000c1d0]:csrrs a2, fcsr, zero<br> [0x8000c1d4]:sw t6, 152(fp)<br>    |
| 942|[0x80014278]<br>0xD4FFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2a090e and fs2 == 1 and fe2 == 0x2b and fm2 == 0x40b67c and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c20c]:fmul.s t6, t5, t4, dyn<br> [0x8000c210]:csrrs a2, fcsr, zero<br> [0x8000c214]:sw t6, 160(fp)<br>    |
| 943|[0x80014280]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2a090e and fs2 == 1 and fe2 == 0x2b and fm2 == 0x40b67c and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c24c]:fmul.s t6, t5, t4, dyn<br> [0x8000c250]:csrrs a2, fcsr, zero<br> [0x8000c254]:sw t6, 168(fp)<br>    |
| 944|[0x80014288]<br>0xD4FFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2a090e and fs2 == 1 and fe2 == 0x2b and fm2 == 0x40b67c and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c28c]:fmul.s t6, t5, t4, dyn<br> [0x8000c290]:csrrs a2, fcsr, zero<br> [0x8000c294]:sw t6, 176(fp)<br>    |
| 945|[0x80014290]<br>0xD4FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2a090e and fs2 == 1 and fe2 == 0x2b and fm2 == 0x40b67c and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c2cc]:fmul.s t6, t5, t4, dyn<br> [0x8000c2d0]:csrrs a2, fcsr, zero<br> [0x8000c2d4]:sw t6, 184(fp)<br>    |
| 946|[0x80014298]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x6cdfe3 and fs2 == 0 and fe2 == 0x2c and fm2 == 0x0a55b6 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000c30c]:fmul.s t6, t5, t4, dyn<br> [0x8000c310]:csrrs a2, fcsr, zero<br> [0x8000c314]:sw t6, 192(fp)<br>    |
| 947|[0x800142a0]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x6cdfe3 and fs2 == 0 and fe2 == 0x2c and fm2 == 0x0a55b6 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c34c]:fmul.s t6, t5, t4, dyn<br> [0x8000c350]:csrrs a2, fcsr, zero<br> [0x8000c354]:sw t6, 200(fp)<br>    |
| 948|[0x800142a8]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x6cdfe3 and fs2 == 0 and fe2 == 0x2c and fm2 == 0x0a55b6 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c38c]:fmul.s t6, t5, t4, dyn<br> [0x8000c390]:csrrs a2, fcsr, zero<br> [0x8000c394]:sw t6, 208(fp)<br>    |
| 949|[0x800142b0]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x6cdfe3 and fs2 == 0 and fe2 == 0x2c and fm2 == 0x0a55b6 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c3cc]:fmul.s t6, t5, t4, dyn<br> [0x8000c3d0]:csrrs a2, fcsr, zero<br> [0x8000c3d4]:sw t6, 216(fp)<br>    |
| 950|[0x800142b8]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x6cdfe3 and fs2 == 0 and fe2 == 0x2c and fm2 == 0x0a55b6 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c40c]:fmul.s t6, t5, t4, dyn<br> [0x8000c410]:csrrs a2, fcsr, zero<br> [0x8000c414]:sw t6, 224(fp)<br>    |
| 951|[0x800142c0]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x39c626 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x3062e7 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000c44c]:fmul.s t6, t5, t4, dyn<br> [0x8000c450]:csrrs a2, fcsr, zero<br> [0x8000c454]:sw t6, 232(fp)<br>    |
| 952|[0x800142c8]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x39c626 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x3062e7 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c48c]:fmul.s t6, t5, t4, dyn<br> [0x8000c490]:csrrs a2, fcsr, zero<br> [0x8000c494]:sw t6, 240(fp)<br>    |
| 953|[0x800142d0]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x39c626 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x3062e7 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c4cc]:fmul.s t6, t5, t4, dyn<br> [0x8000c4d0]:csrrs a2, fcsr, zero<br> [0x8000c4d4]:sw t6, 248(fp)<br>    |
| 954|[0x800142d8]<br>0x55000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x39c626 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x3062e7 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c50c]:fmul.s t6, t5, t4, dyn<br> [0x8000c510]:csrrs a2, fcsr, zero<br> [0x8000c514]:sw t6, 256(fp)<br>    |
| 955|[0x800142e0]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x39c626 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x3062e7 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c54c]:fmul.s t6, t5, t4, dyn<br> [0x8000c550]:csrrs a2, fcsr, zero<br> [0x8000c554]:sw t6, 264(fp)<br>    |
| 956|[0x800142e8]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2f9f7c and fs2 == 0 and fe2 == 0x2a and fm2 == 0x3a94dc and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000c58c]:fmul.s t6, t5, t4, dyn<br> [0x8000c590]:csrrs a2, fcsr, zero<br> [0x8000c594]:sw t6, 272(fp)<br>    |
| 957|[0x800142f0]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2f9f7c and fs2 == 0 and fe2 == 0x2a and fm2 == 0x3a94dc and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c5cc]:fmul.s t6, t5, t4, dyn<br> [0x8000c5d0]:csrrs a2, fcsr, zero<br> [0x8000c5d4]:sw t6, 280(fp)<br>    |
| 958|[0x800142f8]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2f9f7c and fs2 == 0 and fe2 == 0x2a and fm2 == 0x3a94dc and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c60c]:fmul.s t6, t5, t4, dyn<br> [0x8000c610]:csrrs a2, fcsr, zero<br> [0x8000c614]:sw t6, 288(fp)<br>    |
| 959|[0x80014300]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2f9f7c and fs2 == 0 and fe2 == 0x2a and fm2 == 0x3a94dc and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c64c]:fmul.s t6, t5, t4, dyn<br> [0x8000c650]:csrrs a2, fcsr, zero<br> [0x8000c654]:sw t6, 296(fp)<br>    |
| 960|[0x80014308]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2f9f7c and fs2 == 0 and fe2 == 0x2a and fm2 == 0x3a94dc and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c68c]:fmul.s t6, t5, t4, dyn<br> [0x8000c690]:csrrs a2, fcsr, zero<br> [0x8000c694]:sw t6, 304(fp)<br>    |
| 961|[0x80014310]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1e4774 and fs2 == 0 and fe2 == 0x2a and fm2 == 0x4f06d4 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000c6cc]:fmul.s t6, t5, t4, dyn<br> [0x8000c6d0]:csrrs a2, fcsr, zero<br> [0x8000c6d4]:sw t6, 312(fp)<br>    |
| 962|[0x80014318]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1e4774 and fs2 == 0 and fe2 == 0x2a and fm2 == 0x4f06d4 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c70c]:fmul.s t6, t5, t4, dyn<br> [0x8000c710]:csrrs a2, fcsr, zero<br> [0x8000c714]:sw t6, 320(fp)<br>    |
| 963|[0x80014320]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1e4774 and fs2 == 0 and fe2 == 0x2a and fm2 == 0x4f06d4 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c74c]:fmul.s t6, t5, t4, dyn<br> [0x8000c750]:csrrs a2, fcsr, zero<br> [0x8000c754]:sw t6, 328(fp)<br>    |
| 964|[0x80014328]<br>0x55000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1e4774 and fs2 == 0 and fe2 == 0x2a and fm2 == 0x4f06d4 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c78c]:fmul.s t6, t5, t4, dyn<br> [0x8000c790]:csrrs a2, fcsr, zero<br> [0x8000c794]:sw t6, 336(fp)<br>    |
| 965|[0x80014330]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1e4774 and fs2 == 0 and fe2 == 0x2a and fm2 == 0x4f06d4 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c7cc]:fmul.s t6, t5, t4, dyn<br> [0x8000c7d0]:csrrs a2, fcsr, zero<br> [0x8000c7d4]:sw t6, 344(fp)<br>    |
| 966|[0x80014338]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x782eaf and fs2 == 0 and fe2 == 0x2b and fm2 == 0x04082d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000c80c]:fmul.s t6, t5, t4, dyn<br> [0x8000c810]:csrrs a2, fcsr, zero<br> [0x8000c814]:sw t6, 352(fp)<br>    |
| 967|[0x80014340]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x782eaf and fs2 == 0 and fe2 == 0x2b and fm2 == 0x04082d and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c84c]:fmul.s t6, t5, t4, dyn<br> [0x8000c850]:csrrs a2, fcsr, zero<br> [0x8000c854]:sw t6, 360(fp)<br>    |
| 968|[0x80014348]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x782eaf and fs2 == 0 and fe2 == 0x2b and fm2 == 0x04082d and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c88c]:fmul.s t6, t5, t4, dyn<br> [0x8000c890]:csrrs a2, fcsr, zero<br> [0x8000c894]:sw t6, 368(fp)<br>    |
| 969|[0x80014350]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x782eaf and fs2 == 0 and fe2 == 0x2b and fm2 == 0x04082d and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c8cc]:fmul.s t6, t5, t4, dyn<br> [0x8000c8d0]:csrrs a2, fcsr, zero<br> [0x8000c8d4]:sw t6, 376(fp)<br>    |
| 970|[0x80014358]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x782eaf and fs2 == 0 and fe2 == 0x2b and fm2 == 0x04082d and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c90c]:fmul.s t6, t5, t4, dyn<br> [0x8000c910]:csrrs a2, fcsr, zero<br> [0x8000c914]:sw t6, 384(fp)<br>    |
| 971|[0x80014360]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7ae8c2 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x0298d7 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000c94c]:fmul.s t6, t5, t4, dyn<br> [0x8000c950]:csrrs a2, fcsr, zero<br> [0x8000c954]:sw t6, 392(fp)<br>    |
| 972|[0x80014368]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7ae8c2 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x0298d7 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c98c]:fmul.s t6, t5, t4, dyn<br> [0x8000c990]:csrrs a2, fcsr, zero<br> [0x8000c994]:sw t6, 400(fp)<br>    |
| 973|[0x80014370]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7ae8c2 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x0298d7 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000c9cc]:fmul.s t6, t5, t4, dyn<br> [0x8000c9d0]:csrrs a2, fcsr, zero<br> [0x8000c9d4]:sw t6, 408(fp)<br>    |
| 974|[0x80014378]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7ae8c2 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x0298d7 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000ca0c]:fmul.s t6, t5, t4, dyn<br> [0x8000ca10]:csrrs a2, fcsr, zero<br> [0x8000ca14]:sw t6, 416(fp)<br>    |
| 975|[0x80014380]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x7ae8c2 and fs2 == 0 and fe2 == 0x2b and fm2 == 0x0298d7 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000ca4c]:fmul.s t6, t5, t4, dyn<br> [0x8000ca50]:csrrs a2, fcsr, zero<br> [0x8000ca54]:sw t6, 424(fp)<br>    |
| 976|[0x80014388]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x216a51 and fs2 == 0 and fe2 == 0x2a and fm2 == 0x4b0119 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000ca8c]:fmul.s t6, t5, t4, dyn<br> [0x8000ca90]:csrrs a2, fcsr, zero<br> [0x8000ca94]:sw t6, 432(fp)<br>    |
| 977|[0x80014390]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x216a51 and fs2 == 0 and fe2 == 0x2a and fm2 == 0x4b0119 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000cacc]:fmul.s t6, t5, t4, dyn<br> [0x8000cad0]:csrrs a2, fcsr, zero<br> [0x8000cad4]:sw t6, 440(fp)<br>    |
| 978|[0x80014398]<br>0x54FFFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x216a51 and fs2 == 0 and fe2 == 0x2a and fm2 == 0x4b0119 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000cb0c]:fmul.s t6, t5, t4, dyn<br> [0x8000cb10]:csrrs a2, fcsr, zero<br> [0x8000cb14]:sw t6, 448(fp)<br>    |
| 979|[0x800143a0]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x216a51 and fs2 == 0 and fe2 == 0x2a and fm2 == 0x4b0119 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000cb4c]:fmul.s t6, t5, t4, dyn<br> [0x8000cb50]:csrrs a2, fcsr, zero<br> [0x8000cb54]:sw t6, 456(fp)<br>    |
| 980|[0x800143a8]<br>0x54FFFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x216a51 and fs2 == 0 and fe2 == 0x2a and fm2 == 0x4b0119 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000cb8c]:fmul.s t6, t5, t4, dyn<br> [0x8000cb90]:csrrs a2, fcsr, zero<br> [0x8000cb94]:sw t6, 464(fp)<br>    |
| 981|[0x800143b0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x733156 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000cbcc]:fmul.s t6, t5, t4, dyn<br> [0x8000cbd0]:csrrs a2, fcsr, zero<br> [0x8000cbd4]:sw t6, 472(fp)<br>    |
| 982|[0x800143b8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x733156 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000cc0c]:fmul.s t6, t5, t4, dyn<br> [0x8000cc10]:csrrs a2, fcsr, zero<br> [0x8000cc14]:sw t6, 480(fp)<br>    |
| 983|[0x800143c0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x733156 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000cc4c]:fmul.s t6, t5, t4, dyn<br> [0x8000cc50]:csrrs a2, fcsr, zero<br> [0x8000cc54]:sw t6, 488(fp)<br>    |
| 984|[0x800143c8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x733156 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000cc8c]:fmul.s t6, t5, t4, dyn<br> [0x8000cc90]:csrrs a2, fcsr, zero<br> [0x8000cc94]:sw t6, 496(fp)<br>    |
| 985|[0x800143d0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x733156 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000cccc]:fmul.s t6, t5, t4, dyn<br> [0x8000ccd0]:csrrs a2, fcsr, zero<br> [0x8000ccd4]:sw t6, 504(fp)<br>    |
| 986|[0x800143d8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4bb2ed and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000cd0c]:fmul.s t6, t5, t4, dyn<br> [0x8000cd10]:csrrs a2, fcsr, zero<br> [0x8000cd14]:sw t6, 512(fp)<br>    |
| 987|[0x800143e0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4bb2ed and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000cd4c]:fmul.s t6, t5, t4, dyn<br> [0x8000cd50]:csrrs a2, fcsr, zero<br> [0x8000cd54]:sw t6, 520(fp)<br>    |
| 988|[0x800143e8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4bb2ed and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000cd8c]:fmul.s t6, t5, t4, dyn<br> [0x8000cd90]:csrrs a2, fcsr, zero<br> [0x8000cd94]:sw t6, 528(fp)<br>    |
| 989|[0x800143f0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4bb2ed and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000cdcc]:fmul.s t6, t5, t4, dyn<br> [0x8000cdd0]:csrrs a2, fcsr, zero<br> [0x8000cdd4]:sw t6, 536(fp)<br>    |
| 990|[0x800143f8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4bb2ed and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000ce0c]:fmul.s t6, t5, t4, dyn<br> [0x8000ce10]:csrrs a2, fcsr, zero<br> [0x8000ce14]:sw t6, 544(fp)<br>    |
| 991|[0x80014400]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1930a7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000ce4c]:fmul.s t6, t5, t4, dyn<br> [0x8000ce50]:csrrs a2, fcsr, zero<br> [0x8000ce54]:sw t6, 552(fp)<br>    |
| 992|[0x80014408]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1930a7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000ce8c]:fmul.s t6, t5, t4, dyn<br> [0x8000ce90]:csrrs a2, fcsr, zero<br> [0x8000ce94]:sw t6, 560(fp)<br>    |
| 993|[0x80014410]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1930a7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000cecc]:fmul.s t6, t5, t4, dyn<br> [0x8000ced0]:csrrs a2, fcsr, zero<br> [0x8000ced4]:sw t6, 568(fp)<br>    |
| 994|[0x80014418]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1930a7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000cf0c]:fmul.s t6, t5, t4, dyn<br> [0x8000cf10]:csrrs a2, fcsr, zero<br> [0x8000cf14]:sw t6, 576(fp)<br>    |
| 995|[0x80014420]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1930a7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000cf4c]:fmul.s t6, t5, t4, dyn<br> [0x8000cf50]:csrrs a2, fcsr, zero<br> [0x8000cf54]:sw t6, 584(fp)<br>    |
| 996|[0x80014428]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x6c9ad9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000cf8c]:fmul.s t6, t5, t4, dyn<br> [0x8000cf90]:csrrs a2, fcsr, zero<br> [0x8000cf94]:sw t6, 592(fp)<br>    |
| 997|[0x80014430]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x6c9ad9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000cfcc]:fmul.s t6, t5, t4, dyn<br> [0x8000cfd0]:csrrs a2, fcsr, zero<br> [0x8000cfd4]:sw t6, 600(fp)<br>    |
| 998|[0x80014438]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x6c9ad9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d00c]:fmul.s t6, t5, t4, dyn<br> [0x8000d010]:csrrs a2, fcsr, zero<br> [0x8000d014]:sw t6, 608(fp)<br>    |
| 999|[0x80014440]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x6c9ad9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d04c]:fmul.s t6, t5, t4, dyn<br> [0x8000d050]:csrrs a2, fcsr, zero<br> [0x8000d054]:sw t6, 616(fp)<br>    |
|1000|[0x80014448]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x6c9ad9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d08c]:fmul.s t6, t5, t4, dyn<br> [0x8000d090]:csrrs a2, fcsr, zero<br> [0x8000d094]:sw t6, 624(fp)<br>    |
|1001|[0x80014450]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2fb1d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000d0cc]:fmul.s t6, t5, t4, dyn<br> [0x8000d0d0]:csrrs a2, fcsr, zero<br> [0x8000d0d4]:sw t6, 632(fp)<br>    |
|1002|[0x80014458]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2fb1d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d10c]:fmul.s t6, t5, t4, dyn<br> [0x8000d110]:csrrs a2, fcsr, zero<br> [0x8000d114]:sw t6, 640(fp)<br>    |
|1003|[0x80014460]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2fb1d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d14c]:fmul.s t6, t5, t4, dyn<br> [0x8000d150]:csrrs a2, fcsr, zero<br> [0x8000d154]:sw t6, 648(fp)<br>    |
|1004|[0x80014468]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2fb1d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d18c]:fmul.s t6, t5, t4, dyn<br> [0x8000d190]:csrrs a2, fcsr, zero<br> [0x8000d194]:sw t6, 656(fp)<br>    |
|1005|[0x80014470]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2fb1d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d1cc]:fmul.s t6, t5, t4, dyn<br> [0x8000d1d0]:csrrs a2, fcsr, zero<br> [0x8000d1d4]:sw t6, 664(fp)<br>    |
|1006|[0x80014478]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x551c11 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000d20c]:fmul.s t6, t5, t4, dyn<br> [0x8000d210]:csrrs a2, fcsr, zero<br> [0x8000d214]:sw t6, 672(fp)<br>    |
|1007|[0x80014480]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x551c11 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d24c]:fmul.s t6, t5, t4, dyn<br> [0x8000d250]:csrrs a2, fcsr, zero<br> [0x8000d254]:sw t6, 680(fp)<br>    |
|1008|[0x80014488]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x551c11 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d28c]:fmul.s t6, t5, t4, dyn<br> [0x8000d290]:csrrs a2, fcsr, zero<br> [0x8000d294]:sw t6, 688(fp)<br>    |
|1009|[0x80014490]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x551c11 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d2cc]:fmul.s t6, t5, t4, dyn<br> [0x8000d2d0]:csrrs a2, fcsr, zero<br> [0x8000d2d4]:sw t6, 696(fp)<br>    |
|1010|[0x80014498]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x551c11 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d30c]:fmul.s t6, t5, t4, dyn<br> [0x8000d310]:csrrs a2, fcsr, zero<br> [0x8000d314]:sw t6, 704(fp)<br>    |
|1011|[0x800144a0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x2bfad8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000d34c]:fmul.s t6, t5, t4, dyn<br> [0x8000d350]:csrrs a2, fcsr, zero<br> [0x8000d354]:sw t6, 712(fp)<br>    |
|1012|[0x800144a8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x2bfad8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d38c]:fmul.s t6, t5, t4, dyn<br> [0x8000d390]:csrrs a2, fcsr, zero<br> [0x8000d394]:sw t6, 720(fp)<br>    |
|1013|[0x800144b0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x2bfad8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d3cc]:fmul.s t6, t5, t4, dyn<br> [0x8000d3d0]:csrrs a2, fcsr, zero<br> [0x8000d3d4]:sw t6, 728(fp)<br>    |
|1014|[0x800144b8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x2bfad8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d40c]:fmul.s t6, t5, t4, dyn<br> [0x8000d410]:csrrs a2, fcsr, zero<br> [0x8000d414]:sw t6, 736(fp)<br>    |
|1015|[0x800144c0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x2bfad8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d44c]:fmul.s t6, t5, t4, dyn<br> [0x8000d450]:csrrs a2, fcsr, zero<br> [0x8000d454]:sw t6, 744(fp)<br>    |
|1016|[0x800144c8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x119e42 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000d48c]:fmul.s t6, t5, t4, dyn<br> [0x8000d490]:csrrs a2, fcsr, zero<br> [0x8000d494]:sw t6, 752(fp)<br>    |
|1017|[0x800144d0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x119e42 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d4cc]:fmul.s t6, t5, t4, dyn<br> [0x8000d4d0]:csrrs a2, fcsr, zero<br> [0x8000d4d4]:sw t6, 760(fp)<br>    |
|1018|[0x800144d8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x119e42 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d50c]:fmul.s t6, t5, t4, dyn<br> [0x8000d510]:csrrs a2, fcsr, zero<br> [0x8000d514]:sw t6, 768(fp)<br>    |
|1019|[0x800144e0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x119e42 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d54c]:fmul.s t6, t5, t4, dyn<br> [0x8000d550]:csrrs a2, fcsr, zero<br> [0x8000d554]:sw t6, 776(fp)<br>    |
|1020|[0x800144e8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x119e42 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d58c]:fmul.s t6, t5, t4, dyn<br> [0x8000d590]:csrrs a2, fcsr, zero<br> [0x8000d594]:sw t6, 784(fp)<br>    |
|1021|[0x800144f0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x748650 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000d5cc]:fmul.s t6, t5, t4, dyn<br> [0x8000d5d0]:csrrs a2, fcsr, zero<br> [0x8000d5d4]:sw t6, 792(fp)<br>    |
|1022|[0x800144f8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x748650 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d60c]:fmul.s t6, t5, t4, dyn<br> [0x8000d610]:csrrs a2, fcsr, zero<br> [0x8000d614]:sw t6, 800(fp)<br>    |
|1023|[0x80014500]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x748650 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d64c]:fmul.s t6, t5, t4, dyn<br> [0x8000d650]:csrrs a2, fcsr, zero<br> [0x8000d654]:sw t6, 808(fp)<br>    |
|1024|[0x80014508]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x748650 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d68c]:fmul.s t6, t5, t4, dyn<br> [0x8000d690]:csrrs a2, fcsr, zero<br> [0x8000d694]:sw t6, 816(fp)<br>    |
|1025|[0x80014510]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x748650 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d6cc]:fmul.s t6, t5, t4, dyn<br> [0x8000d6d0]:csrrs a2, fcsr, zero<br> [0x8000d6d4]:sw t6, 824(fp)<br>    |
|1026|[0x80014518]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2fa24d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000d70c]:fmul.s t6, t5, t4, dyn<br> [0x8000d710]:csrrs a2, fcsr, zero<br> [0x8000d714]:sw t6, 832(fp)<br>    |
|1027|[0x80014520]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2fa24d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d74c]:fmul.s t6, t5, t4, dyn<br> [0x8000d750]:csrrs a2, fcsr, zero<br> [0x8000d754]:sw t6, 840(fp)<br>    |
|1028|[0x80014528]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2fa24d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d78c]:fmul.s t6, t5, t4, dyn<br> [0x8000d790]:csrrs a2, fcsr, zero<br> [0x8000d794]:sw t6, 848(fp)<br>    |
|1029|[0x80014530]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2fa24d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d7cc]:fmul.s t6, t5, t4, dyn<br> [0x8000d7d0]:csrrs a2, fcsr, zero<br> [0x8000d7d4]:sw t6, 856(fp)<br>    |
|1030|[0x80014538]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2fa24d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d80c]:fmul.s t6, t5, t4, dyn<br> [0x8000d810]:csrrs a2, fcsr, zero<br> [0x8000d814]:sw t6, 864(fp)<br>    |
|1031|[0x80014540]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x29bb3f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000d84c]:fmul.s t6, t5, t4, dyn<br> [0x8000d850]:csrrs a2, fcsr, zero<br> [0x8000d854]:sw t6, 872(fp)<br>    |
|1032|[0x80014548]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x29bb3f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d88c]:fmul.s t6, t5, t4, dyn<br> [0x8000d890]:csrrs a2, fcsr, zero<br> [0x8000d894]:sw t6, 880(fp)<br>    |
|1033|[0x80014550]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x29bb3f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d8cc]:fmul.s t6, t5, t4, dyn<br> [0x8000d8d0]:csrrs a2, fcsr, zero<br> [0x8000d8d4]:sw t6, 888(fp)<br>    |
|1034|[0x80014558]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x29bb3f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d90c]:fmul.s t6, t5, t4, dyn<br> [0x8000d910]:csrrs a2, fcsr, zero<br> [0x8000d914]:sw t6, 896(fp)<br>    |
|1035|[0x80014560]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x29bb3f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d94c]:fmul.s t6, t5, t4, dyn<br> [0x8000d950]:csrrs a2, fcsr, zero<br> [0x8000d954]:sw t6, 904(fp)<br>    |
|1036|[0x80014568]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2bc20f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000d98c]:fmul.s t6, t5, t4, dyn<br> [0x8000d990]:csrrs a2, fcsr, zero<br> [0x8000d994]:sw t6, 912(fp)<br>    |
|1037|[0x80014570]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2bc20f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000d9cc]:fmul.s t6, t5, t4, dyn<br> [0x8000d9d0]:csrrs a2, fcsr, zero<br> [0x8000d9d4]:sw t6, 920(fp)<br>    |
|1038|[0x80014578]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2bc20f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000da0c]:fmul.s t6, t5, t4, dyn<br> [0x8000da10]:csrrs a2, fcsr, zero<br> [0x8000da14]:sw t6, 928(fp)<br>    |
|1039|[0x80014580]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2bc20f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000da4c]:fmul.s t6, t5, t4, dyn<br> [0x8000da50]:csrrs a2, fcsr, zero<br> [0x8000da54]:sw t6, 936(fp)<br>    |
|1040|[0x80014588]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2bc20f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000da8c]:fmul.s t6, t5, t4, dyn<br> [0x8000da90]:csrrs a2, fcsr, zero<br> [0x8000da94]:sw t6, 944(fp)<br>    |
|1041|[0x80014590]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x29ba05 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000dacc]:fmul.s t6, t5, t4, dyn<br> [0x8000dad0]:csrrs a2, fcsr, zero<br> [0x8000dad4]:sw t6, 952(fp)<br>    |
|1042|[0x80014598]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x29ba05 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000db0c]:fmul.s t6, t5, t4, dyn<br> [0x8000db10]:csrrs a2, fcsr, zero<br> [0x8000db14]:sw t6, 960(fp)<br>    |
|1043|[0x800145a0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x29ba05 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000db4c]:fmul.s t6, t5, t4, dyn<br> [0x8000db50]:csrrs a2, fcsr, zero<br> [0x8000db54]:sw t6, 968(fp)<br>    |
|1044|[0x800145a8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x29ba05 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000db8c]:fmul.s t6, t5, t4, dyn<br> [0x8000db90]:csrrs a2, fcsr, zero<br> [0x8000db94]:sw t6, 976(fp)<br>    |
|1045|[0x800145b0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x29ba05 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000dbcc]:fmul.s t6, t5, t4, dyn<br> [0x8000dbd0]:csrrs a2, fcsr, zero<br> [0x8000dbd4]:sw t6, 984(fp)<br>    |
|1046|[0x800145b8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x327dc5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000dc0c]:fmul.s t6, t5, t4, dyn<br> [0x8000dc10]:csrrs a2, fcsr, zero<br> [0x8000dc14]:sw t6, 992(fp)<br>    |
|1047|[0x800145c0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x327dc5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000dc44]:fmul.s t6, t5, t4, dyn<br> [0x8000dc48]:csrrs a2, fcsr, zero<br> [0x8000dc4c]:sw t6, 1000(fp)<br>   |
|1048|[0x800145c8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x327dc5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000dc7c]:fmul.s t6, t5, t4, dyn<br> [0x8000dc80]:csrrs a2, fcsr, zero<br> [0x8000dc84]:sw t6, 1008(fp)<br>   |
|1049|[0x800145d0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x327dc5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000dcb4]:fmul.s t6, t5, t4, dyn<br> [0x8000dcb8]:csrrs a2, fcsr, zero<br> [0x8000dcbc]:sw t6, 1016(fp)<br>   |
|1050|[0x800145d8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x327dc5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000dcf4]:fmul.s t6, t5, t4, dyn<br> [0x8000dcf8]:csrrs a2, fcsr, zero<br> [0x8000dcfc]:sw t6, 0(fp)<br>      |
|1051|[0x800145e0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x18ea60 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000dd2c]:fmul.s t6, t5, t4, dyn<br> [0x8000dd30]:csrrs a2, fcsr, zero<br> [0x8000dd34]:sw t6, 8(fp)<br>      |
|1052|[0x800145e8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x18ea60 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000dd64]:fmul.s t6, t5, t4, dyn<br> [0x8000dd68]:csrrs a2, fcsr, zero<br> [0x8000dd6c]:sw t6, 16(fp)<br>     |
|1053|[0x800145f0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x18ea60 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000dd9c]:fmul.s t6, t5, t4, dyn<br> [0x8000dda0]:csrrs a2, fcsr, zero<br> [0x8000dda4]:sw t6, 24(fp)<br>     |
|1054|[0x800145f8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x18ea60 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000ddd4]:fmul.s t6, t5, t4, dyn<br> [0x8000ddd8]:csrrs a2, fcsr, zero<br> [0x8000dddc]:sw t6, 32(fp)<br>     |
|1055|[0x80014600]<br>0x80000000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x18ea60 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000de0c]:fmul.s t6, t5, t4, dyn<br> [0x8000de10]:csrrs a2, fcsr, zero<br> [0x8000de14]:sw t6, 40(fp)<br>     |
|1056|[0x80014608]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x11924c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000de44]:fmul.s t6, t5, t4, dyn<br> [0x8000de48]:csrrs a2, fcsr, zero<br> [0x8000de4c]:sw t6, 48(fp)<br>     |
|1057|[0x80014610]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x11924c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000de7c]:fmul.s t6, t5, t4, dyn<br> [0x8000de80]:csrrs a2, fcsr, zero<br> [0x8000de84]:sw t6, 56(fp)<br>     |
|1058|[0x80014618]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x11924c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000deb4]:fmul.s t6, t5, t4, dyn<br> [0x8000deb8]:csrrs a2, fcsr, zero<br> [0x8000debc]:sw t6, 64(fp)<br>     |
|1059|[0x80014620]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x11924c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000deec]:fmul.s t6, t5, t4, dyn<br> [0x8000def0]:csrrs a2, fcsr, zero<br> [0x8000def4]:sw t6, 72(fp)<br>     |
|1060|[0x80014628]<br>0x80000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x11924c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000df24]:fmul.s t6, t5, t4, dyn<br> [0x8000df28]:csrrs a2, fcsr, zero<br> [0x8000df2c]:sw t6, 80(fp)<br>     |
|1061|[0x80014630]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x0f88e2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000df5c]:fmul.s t6, t5, t4, dyn<br> [0x8000df60]:csrrs a2, fcsr, zero<br> [0x8000df64]:sw t6, 88(fp)<br>     |
|1062|[0x80014638]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x0f88e2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000df94]:fmul.s t6, t5, t4, dyn<br> [0x8000df98]:csrrs a2, fcsr, zero<br> [0x8000df9c]:sw t6, 96(fp)<br>     |
|1063|[0x80014640]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x0f88e2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000dfcc]:fmul.s t6, t5, t4, dyn<br> [0x8000dfd0]:csrrs a2, fcsr, zero<br> [0x8000dfd4]:sw t6, 104(fp)<br>    |
|1064|[0x80014648]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x0f88e2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e004]:fmul.s t6, t5, t4, dyn<br> [0x8000e008]:csrrs a2, fcsr, zero<br> [0x8000e00c]:sw t6, 112(fp)<br>    |
|1065|[0x80014650]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x0f88e2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e03c]:fmul.s t6, t5, t4, dyn<br> [0x8000e040]:csrrs a2, fcsr, zero<br> [0x8000e044]:sw t6, 120(fp)<br>    |
|1066|[0x80014658]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x18a8a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000e074]:fmul.s t6, t5, t4, dyn<br> [0x8000e078]:csrrs a2, fcsr, zero<br> [0x8000e07c]:sw t6, 128(fp)<br>    |
|1067|[0x80014660]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x18a8a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e0ac]:fmul.s t6, t5, t4, dyn<br> [0x8000e0b0]:csrrs a2, fcsr, zero<br> [0x8000e0b4]:sw t6, 136(fp)<br>    |
|1068|[0x80014668]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x18a8a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e0e4]:fmul.s t6, t5, t4, dyn<br> [0x8000e0e8]:csrrs a2, fcsr, zero<br> [0x8000e0ec]:sw t6, 144(fp)<br>    |
|1069|[0x80014670]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x18a8a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e11c]:fmul.s t6, t5, t4, dyn<br> [0x8000e120]:csrrs a2, fcsr, zero<br> [0x8000e124]:sw t6, 152(fp)<br>    |
|1070|[0x80014678]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x18a8a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e154]:fmul.s t6, t5, t4, dyn<br> [0x8000e158]:csrrs a2, fcsr, zero<br> [0x8000e15c]:sw t6, 160(fp)<br>    |
|1071|[0x80014680]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1b3d2d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000e18c]:fmul.s t6, t5, t4, dyn<br> [0x8000e190]:csrrs a2, fcsr, zero<br> [0x8000e194]:sw t6, 168(fp)<br>    |
|1072|[0x80014688]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1b3d2d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e1c4]:fmul.s t6, t5, t4, dyn<br> [0x8000e1c8]:csrrs a2, fcsr, zero<br> [0x8000e1cc]:sw t6, 176(fp)<br>    |
|1073|[0x80014690]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1b3d2d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e1fc]:fmul.s t6, t5, t4, dyn<br> [0x8000e200]:csrrs a2, fcsr, zero<br> [0x8000e204]:sw t6, 184(fp)<br>    |
|1074|[0x80014698]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1b3d2d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e234]:fmul.s t6, t5, t4, dyn<br> [0x8000e238]:csrrs a2, fcsr, zero<br> [0x8000e23c]:sw t6, 192(fp)<br>    |
|1075|[0x800146a0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1b3d2d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e26c]:fmul.s t6, t5, t4, dyn<br> [0x8000e270]:csrrs a2, fcsr, zero<br> [0x8000e274]:sw t6, 200(fp)<br>    |
|1076|[0x800146a8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x19b32b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000e2a4]:fmul.s t6, t5, t4, dyn<br> [0x8000e2a8]:csrrs a2, fcsr, zero<br> [0x8000e2ac]:sw t6, 208(fp)<br>    |
|1077|[0x800146b0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x19b32b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e2dc]:fmul.s t6, t5, t4, dyn<br> [0x8000e2e0]:csrrs a2, fcsr, zero<br> [0x8000e2e4]:sw t6, 216(fp)<br>    |
|1078|[0x800146b8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x19b32b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e314]:fmul.s t6, t5, t4, dyn<br> [0x8000e318]:csrrs a2, fcsr, zero<br> [0x8000e31c]:sw t6, 224(fp)<br>    |
|1079|[0x800146c0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x19b32b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e34c]:fmul.s t6, t5, t4, dyn<br> [0x8000e350]:csrrs a2, fcsr, zero<br> [0x8000e354]:sw t6, 232(fp)<br>    |
|1080|[0x800146c8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x19b32b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e384]:fmul.s t6, t5, t4, dyn<br> [0x8000e388]:csrrs a2, fcsr, zero<br> [0x8000e38c]:sw t6, 240(fp)<br>    |
|1081|[0x800146d0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x036cdc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000e3bc]:fmul.s t6, t5, t4, dyn<br> [0x8000e3c0]:csrrs a2, fcsr, zero<br> [0x8000e3c4]:sw t6, 248(fp)<br>    |
|1082|[0x800146d8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x036cdc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e3f4]:fmul.s t6, t5, t4, dyn<br> [0x8000e3f8]:csrrs a2, fcsr, zero<br> [0x8000e3fc]:sw t6, 256(fp)<br>    |
|1083|[0x800146e0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x036cdc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e42c]:fmul.s t6, t5, t4, dyn<br> [0x8000e430]:csrrs a2, fcsr, zero<br> [0x8000e434]:sw t6, 264(fp)<br>    |
|1084|[0x800146e8]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x036cdc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e464]:fmul.s t6, t5, t4, dyn<br> [0x8000e468]:csrrs a2, fcsr, zero<br> [0x8000e46c]:sw t6, 272(fp)<br>    |
|1085|[0x800146f0]<br>0x80000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x036cdc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e49c]:fmul.s t6, t5, t4, dyn<br> [0x8000e4a0]:csrrs a2, fcsr, zero<br> [0x8000e4a4]:sw t6, 280(fp)<br>    |
|1086|[0x800146f8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x31c71b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000e4d4]:fmul.s t6, t5, t4, dyn<br> [0x8000e4d8]:csrrs a2, fcsr, zero<br> [0x8000e4dc]:sw t6, 288(fp)<br>    |
|1087|[0x80014700]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x31c71b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e50c]:fmul.s t6, t5, t4, dyn<br> [0x8000e510]:csrrs a2, fcsr, zero<br> [0x8000e514]:sw t6, 296(fp)<br>    |
|1088|[0x80014708]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x31c71b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e544]:fmul.s t6, t5, t4, dyn<br> [0x8000e548]:csrrs a2, fcsr, zero<br> [0x8000e54c]:sw t6, 304(fp)<br>    |
|1089|[0x80014710]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x31c71b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e57c]:fmul.s t6, t5, t4, dyn<br> [0x8000e580]:csrrs a2, fcsr, zero<br> [0x8000e584]:sw t6, 312(fp)<br>    |
|1090|[0x80014718]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x31c71b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e5b4]:fmul.s t6, t5, t4, dyn<br> [0x8000e5b8]:csrrs a2, fcsr, zero<br> [0x8000e5bc]:sw t6, 320(fp)<br>    |
|1091|[0x80014720]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x047a0e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000e5ec]:fmul.s t6, t5, t4, dyn<br> [0x8000e5f0]:csrrs a2, fcsr, zero<br> [0x8000e5f4]:sw t6, 328(fp)<br>    |
|1092|[0x80014728]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x047a0e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e624]:fmul.s t6, t5, t4, dyn<br> [0x8000e628]:csrrs a2, fcsr, zero<br> [0x8000e62c]:sw t6, 336(fp)<br>    |
|1093|[0x80014730]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x047a0e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e65c]:fmul.s t6, t5, t4, dyn<br> [0x8000e660]:csrrs a2, fcsr, zero<br> [0x8000e664]:sw t6, 344(fp)<br>    |
|1094|[0x80014738]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x047a0e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e694]:fmul.s t6, t5, t4, dyn<br> [0x8000e698]:csrrs a2, fcsr, zero<br> [0x8000e69c]:sw t6, 352(fp)<br>    |
|1095|[0x80014740]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x047a0e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e6cc]:fmul.s t6, t5, t4, dyn<br> [0x8000e6d0]:csrrs a2, fcsr, zero<br> [0x8000e6d4]:sw t6, 360(fp)<br>    |
|1096|[0x80014748]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0e924d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000e704]:fmul.s t6, t5, t4, dyn<br> [0x8000e708]:csrrs a2, fcsr, zero<br> [0x8000e70c]:sw t6, 368(fp)<br>    |
|1097|[0x80014750]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0e924d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e73c]:fmul.s t6, t5, t4, dyn<br> [0x8000e740]:csrrs a2, fcsr, zero<br> [0x8000e744]:sw t6, 376(fp)<br>    |
|1098|[0x80014758]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0e924d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e774]:fmul.s t6, t5, t4, dyn<br> [0x8000e778]:csrrs a2, fcsr, zero<br> [0x8000e77c]:sw t6, 384(fp)<br>    |
|1099|[0x80014760]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0e924d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e7ac]:fmul.s t6, t5, t4, dyn<br> [0x8000e7b0]:csrrs a2, fcsr, zero<br> [0x8000e7b4]:sw t6, 392(fp)<br>    |
|1100|[0x80014768]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0e924d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e7e4]:fmul.s t6, t5, t4, dyn<br> [0x8000e7e8]:csrrs a2, fcsr, zero<br> [0x8000e7ec]:sw t6, 400(fp)<br>    |
|1101|[0x80014770]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x26e854 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000e81c]:fmul.s t6, t5, t4, dyn<br> [0x8000e820]:csrrs a2, fcsr, zero<br> [0x8000e824]:sw t6, 408(fp)<br>    |
|1102|[0x80014778]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x26e854 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e854]:fmul.s t6, t5, t4, dyn<br> [0x8000e858]:csrrs a2, fcsr, zero<br> [0x8000e85c]:sw t6, 416(fp)<br>    |
|1103|[0x80014780]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x26e854 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e88c]:fmul.s t6, t5, t4, dyn<br> [0x8000e890]:csrrs a2, fcsr, zero<br> [0x8000e894]:sw t6, 424(fp)<br>    |
|1104|[0x80014788]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x26e854 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e8c4]:fmul.s t6, t5, t4, dyn<br> [0x8000e8c8]:csrrs a2, fcsr, zero<br> [0x8000e8cc]:sw t6, 432(fp)<br>    |
|1105|[0x80014790]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x26e854 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e8fc]:fmul.s t6, t5, t4, dyn<br> [0x8000e900]:csrrs a2, fcsr, zero<br> [0x8000e904]:sw t6, 440(fp)<br>    |
|1106|[0x80014798]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x10adb7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000e934]:fmul.s t6, t5, t4, dyn<br> [0x8000e938]:csrrs a2, fcsr, zero<br> [0x8000e93c]:sw t6, 448(fp)<br>    |
|1107|[0x800147a0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x10adb7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e96c]:fmul.s t6, t5, t4, dyn<br> [0x8000e970]:csrrs a2, fcsr, zero<br> [0x8000e974]:sw t6, 456(fp)<br>    |
|1108|[0x800147a8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x10adb7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e9a4]:fmul.s t6, t5, t4, dyn<br> [0x8000e9a8]:csrrs a2, fcsr, zero<br> [0x8000e9ac]:sw t6, 464(fp)<br>    |
|1109|[0x800147b0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x10adb7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000e9dc]:fmul.s t6, t5, t4, dyn<br> [0x8000e9e0]:csrrs a2, fcsr, zero<br> [0x8000e9e4]:sw t6, 472(fp)<br>    |
|1110|[0x800147b8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x10adb7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000ea14]:fmul.s t6, t5, t4, dyn<br> [0x8000ea18]:csrrs a2, fcsr, zero<br> [0x8000ea1c]:sw t6, 480(fp)<br>    |
|1111|[0x800147c0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x5ff410 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000ea4c]:fmul.s t6, t5, t4, dyn<br> [0x8000ea50]:csrrs a2, fcsr, zero<br> [0x8000ea54]:sw t6, 488(fp)<br>    |
|1112|[0x800147c8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x5ff410 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000ea84]:fmul.s t6, t5, t4, dyn<br> [0x8000ea88]:csrrs a2, fcsr, zero<br> [0x8000ea8c]:sw t6, 496(fp)<br>    |
|1113|[0x800147d0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x5ff410 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000eabc]:fmul.s t6, t5, t4, dyn<br> [0x8000eac0]:csrrs a2, fcsr, zero<br> [0x8000eac4]:sw t6, 504(fp)<br>    |
|1114|[0x800147d8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x5ff410 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000eaf4]:fmul.s t6, t5, t4, dyn<br> [0x8000eaf8]:csrrs a2, fcsr, zero<br> [0x8000eafc]:sw t6, 512(fp)<br>    |
|1115|[0x800147e0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x5ff410 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000eb2c]:fmul.s t6, t5, t4, dyn<br> [0x8000eb30]:csrrs a2, fcsr, zero<br> [0x8000eb34]:sw t6, 520(fp)<br>    |
|1116|[0x800147e8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x00c42d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000eb64]:fmul.s t6, t5, t4, dyn<br> [0x8000eb68]:csrrs a2, fcsr, zero<br> [0x8000eb6c]:sw t6, 528(fp)<br>    |
|1117|[0x800147f0]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x00c42d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000eb9c]:fmul.s t6, t5, t4, dyn<br> [0x8000eba0]:csrrs a2, fcsr, zero<br> [0x8000eba4]:sw t6, 536(fp)<br>    |
|1118|[0x800147f8]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x00c42d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000ebd4]:fmul.s t6, t5, t4, dyn<br> [0x8000ebd8]:csrrs a2, fcsr, zero<br> [0x8000ebdc]:sw t6, 544(fp)<br>    |
|1119|[0x80014800]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x00c42d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000ec0c]:fmul.s t6, t5, t4, dyn<br> [0x8000ec10]:csrrs a2, fcsr, zero<br> [0x8000ec14]:sw t6, 552(fp)<br>    |
|1120|[0x80014808]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x00c42d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000ec44]:fmul.s t6, t5, t4, dyn<br> [0x8000ec48]:csrrs a2, fcsr, zero<br> [0x8000ec4c]:sw t6, 560(fp)<br>    |
|1121|[0x80014818]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x378efe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000ecb4]:fmul.s t6, t5, t4, dyn<br> [0x8000ecb8]:csrrs a2, fcsr, zero<br> [0x8000ecbc]:sw t6, 576(fp)<br>    |
|1122|[0x80014820]<br>0x00000000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x378efe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000ecec]:fmul.s t6, t5, t4, dyn<br> [0x8000ecf0]:csrrs a2, fcsr, zero<br> [0x8000ecf4]:sw t6, 584(fp)<br>    |
|1123|[0x80014830]<br>0x00000000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x25608b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000ed5c]:fmul.s t6, t5, t4, dyn<br> [0x8000ed60]:csrrs a2, fcsr, zero<br> [0x8000ed64]:sw t6, 600(fp)<br>    |
