
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80003c90')]      |
| SIG_REGION                | [('0x80005f10', '0x80006830', '584 words')]      |
| COV_LABELS                | fmadd_b2      |
| TEST_NAME                 | /home/reg/work/zfinx/RV32Zfinx/fmadd.s/work/fmadd_b2-01.S/ref.S    |
| Total Number of coverpoints| 417     |
| Total Coverpoints Hit     | 417      |
| Total Signature Updates   | 582      |
| STAT1                     | 286      |
| STAT2                     | 5      |
| STAT3                     | 0     |
| STAT4                     | 291     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80003a34]:fmadd.s t6, t5, t4, t3, dyn
      [0x80003a38]:csrrs a4, fcsr, zero
      [0x80003a3c]:sw t6, 40(a1)
 -- Signature Address: 0x800067e8 Data: 0x0000004A
 -- Redundant Coverpoints hit by the op
      - mnemonic : fmadd.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs3 : x28
      - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd
      - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x86 and fm2 == 0x7f094c and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004a and  fcsr == 0x0 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80003a88]:fmadd.s t6, t5, t4, t3, dyn
      [0x80003a8c]:csrrs a4, fcsr, zero
      [0x80003a90]:sw t6, 48(a1)
 -- Signature Address: 0x800067f0 Data: 0x00000002
 -- Redundant Coverpoints hit by the op
      - mnemonic : fmadd.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs3 : x28
      - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd
      - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x88 and fm2 == 0x32aa62 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000002 and  fcsr == 0x0 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80003adc]:fmadd.s t6, t5, t4, t3, dyn
      [0x80003ae0]:csrrs a4, fcsr, zero
      [0x80003ae4]:sw t6, 56(a1)
 -- Signature Address: 0x800067f8 Data: 0x00000035
 -- Redundant Coverpoints hit by the op
      - mnemonic : fmadd.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs3 : x28
      - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd
      - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x89 and fm2 == 0x2a1525 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000035 and  fcsr == 0x0 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80003b84]:fmadd.s t6, t5, t4, t3, dyn
      [0x80003b88]:csrrs a4, fcsr, zero
      [0x80003b8c]:sw t6, 72(a1)
 -- Signature Address: 0x80006808 Data: 0x00000018
 -- Redundant Coverpoints hit by the op
      - mnemonic : fmadd.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs3 : x28
      - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd
      - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x87 and fm2 == 0x442e89 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000018 and  fcsr == 0x0 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80003c80]:fmadd.s t6, t5, t4, t3, dyn
      [0x80003c84]:csrrs a4, fcsr, zero
      [0x80003c88]:sw t6, 96(a1)
 -- Signature Address: 0x80006820 Data: 0x0000005B
 -- Redundant Coverpoints hit by the op
      - mnemonic : fmadd.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs3 : x28
      - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd
      - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x90 and fm2 == 0x337da1 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005b and  fcsr == 0x0 and rm_val == 7   #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fmadd.s', 'rs1 : x30', 'rs2 : x30', 'rd : x31', 'rs3 : x29', 'rs1 == rs2 != rs3 and rs1 == rs2 != rd and rd != rs3']
Last Code Sequence : 
	-[0x80000128]:fmadd.s t6, t5, t5, t4, dyn
	-[0x8000012c]:csrrs tp, fcsr, zero
	-[0x80000130]:sw t6, 0(ra)
Current Store : [0x80000134] : sw tp, 4(ra) -- Store: [0x80005f14]:0x00000000




Last Coverpoint : ['rs1 : x31', 'rs2 : x29', 'rd : x30', 'rs3 : x28', 'rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x85 and fm2 == 0x73d370 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000059 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000014c]:fmadd.s t5, t6, t4, t3, dyn
	-[0x80000150]:csrrs tp, fcsr, zero
	-[0x80000154]:sw t5, 8(ra)
Current Store : [0x80000158] : sw tp, 12(ra) -- Store: [0x80005f1c]:0x00000000




Last Coverpoint : ['rs1 : x28', 'rs2 : x28', 'rd : x28', 'rs3 : x31', 'rs1 == rs2 == rd != rs3']
Last Code Sequence : 
	-[0x80000170]:fmadd.s t3, t3, t3, t6, dyn
	-[0x80000174]:csrrs tp, fcsr, zero
	-[0x80000178]:sw t3, 16(ra)
Current Store : [0x8000017c] : sw tp, 20(ra) -- Store: [0x80005f24]:0x00000000




Last Coverpoint : ['rs1 : x29', 'rs2 : x27', 'rd : x27', 'rs3 : x27', 'rd == rs2 == rs3 != rs1']
Last Code Sequence : 
	-[0x80000194]:fmadd.s s11, t4, s11, s11, dyn
	-[0x80000198]:csrrs tp, fcsr, zero
	-[0x8000019c]:sw s11, 24(ra)
Current Store : [0x800001a0] : sw tp, 28(ra) -- Store: [0x80005f2c]:0x00000000




Last Coverpoint : ['rs1 : x27', 'rs2 : x26', 'rd : x29', 'rs3 : x26', 'rs2 == rs3 != rs1 and rs2 == rs3 != rd and rd != rs1']
Last Code Sequence : 
	-[0x800001b8]:fmadd.s t4, s11, s10, s10, dyn
	-[0x800001bc]:csrrs tp, fcsr, zero
	-[0x800001c0]:sw t4, 32(ra)
Current Store : [0x800001c4] : sw tp, 36(ra) -- Store: [0x80005f34]:0x00000000




Last Coverpoint : ['rs1 : x25', 'rs2 : x25', 'rd : x26', 'rs3 : x25', 'rs1 == rs2 == rs3 != rd']
Last Code Sequence : 
	-[0x800001dc]:fmadd.s s10, s9, s9, s9, dyn
	-[0x800001e0]:csrrs tp, fcsr, zero
	-[0x800001e4]:sw s10, 40(ra)
Current Store : [0x800001e8] : sw tp, 44(ra) -- Store: [0x80005f3c]:0x00000000




Last Coverpoint : ['rs1 : x24', 'rs2 : x24', 'rd : x24', 'rs3 : x24', 'rs1 == rs2 == rs3 == rd']
Last Code Sequence : 
	-[0x80000200]:fmadd.s s8, s8, s8, s8, dyn
	-[0x80000204]:csrrs tp, fcsr, zero
	-[0x80000208]:sw s8, 48(ra)
Current Store : [0x8000020c] : sw tp, 52(ra) -- Store: [0x80005f44]:0x00000000




Last Coverpoint : ['rs1 : x23', 'rs2 : x31', 'rd : x25', 'rs3 : x23', 'rs1 == rs3 != rs2 and rs1 == rs3 != rd and rd != rs2']
Last Code Sequence : 
	-[0x80000224]:fmadd.s s9, s7, t6, s7, dyn
	-[0x80000228]:csrrs tp, fcsr, zero
	-[0x8000022c]:sw s9, 56(ra)
Current Store : [0x80000230] : sw tp, 60(ra) -- Store: [0x80005f4c]:0x00000000




Last Coverpoint : ['rs1 : x22', 'rs2 : x23', 'rd : x22', 'rs3 : x30', 'rs1 == rd != rs2 and rs1 == rd != rs3 and rs3 != rs2', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x86 and fm2 == 0x7f094c and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000248]:fmadd.s s6, s6, s7, t5, dyn
	-[0x8000024c]:csrrs tp, fcsr, zero
	-[0x80000250]:sw s6, 64(ra)
Current Store : [0x80000254] : sw tp, 68(ra) -- Store: [0x80005f54]:0x00000000




Last Coverpoint : ['rs1 : x26', 'rs2 : x21', 'rd : x21', 'rs3 : x22', 'rs2 == rd != rs1 and rs2 == rd != rs3 and rs3 != rs1', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x88 and fm2 == 0x32aa62 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000026c]:fmadd.s s5, s10, s5, s6, dyn
	-[0x80000270]:csrrs tp, fcsr, zero
	-[0x80000274]:sw s5, 72(ra)
Current Store : [0x80000278] : sw tp, 76(ra) -- Store: [0x80005f5c]:0x00000000




Last Coverpoint : ['rs1 : x21', 'rs2 : x22', 'rd : x20', 'rs3 : x20', 'rs3 == rd != rs1 and rs3 == rd != rs2 and rs2 != rs1', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x89 and fm2 == 0x2a1525 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000035 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000290]:fmadd.s s4, s5, s6, s4, dyn
	-[0x80000294]:csrrs tp, fcsr, zero
	-[0x80000298]:sw s4, 80(ra)
Current Store : [0x8000029c] : sw tp, 84(ra) -- Store: [0x80005f64]:0x00000000




Last Coverpoint : ['rs1 : x19', 'rs2 : x20', 'rd : x19', 'rs3 : x19', 'rs1 == rd == rs3 != rs2']
Last Code Sequence : 
	-[0x800002b4]:fmadd.s s3, s3, s4, s3, dyn
	-[0x800002b8]:csrrs tp, fcsr, zero
	-[0x800002bc]:sw s3, 88(ra)
Current Store : [0x800002c0] : sw tp, 92(ra) -- Store: [0x80005f6c]:0x00000000




Last Coverpoint : ['rs1 : x20', 'rs2 : x19', 'rd : x23', 'rs3 : x21', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8b and fm2 == 0x327254 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000015 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002d8]:fmadd.s s7, s4, s3, s5, dyn
	-[0x800002dc]:csrrs tp, fcsr, zero
	-[0x800002e0]:sw s7, 96(ra)
Current Store : [0x800002e4] : sw tp, 100(ra) -- Store: [0x80005f74]:0x00000000




Last Coverpoint : ['rs1 : x17', 'rs2 : x16', 'rd : x18', 'rs3 : x15', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8c and fm2 == 0x32f8db and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000012 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002fc]:fmadd.s s2, a7, a6, a5, dyn
	-[0x80000300]:csrrs tp, fcsr, zero
	-[0x80000304]:sw s2, 104(ra)
Current Store : [0x80000308] : sw tp, 108(ra) -- Store: [0x80005f7c]:0x00000000




Last Coverpoint : ['rs1 : x15', 'rs2 : x18', 'rd : x17', 'rs3 : x16', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8d and fm2 == 0x332e1b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000011 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000320]:fmadd.s a7, a5, s2, a6, dyn
	-[0x80000324]:csrrs tp, fcsr, zero
	-[0x80000328]:sw a7, 112(ra)
Current Store : [0x8000032c] : sw tp, 116(ra) -- Store: [0x80005f84]:0x00000000




Last Coverpoint : ['rs1 : x18', 'rs2 : x15', 'rd : x16', 'rs3 : x17', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8e and fm2 == 0x335c59 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000344]:fmadd.s a6, s2, a5, a7, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:sw a6, 120(ra)
Current Store : [0x80000350] : sw tp, 124(ra) -- Store: [0x80005f8c]:0x00000000




Last Coverpoint : ['rs1 : x16', 'rs2 : x17', 'rd : x15', 'rs3 : x18', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8f and fm2 == 0x334ad5 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000368]:fmadd.s a5, a6, a7, s2, dyn
	-[0x8000036c]:csrrs tp, fcsr, zero
	-[0x80000370]:sw a5, 128(ra)
Current Store : [0x80000374] : sw tp, 132(ra) -- Store: [0x80005f94]:0x00000000




Last Coverpoint : ['rs1 : x13', 'rs2 : x12', 'rd : x14', 'rs3 : x11', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x90 and fm2 == 0x33560a and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000016 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000038c]:fmadd.s a4, a3, a2, a1, dyn
	-[0x80000390]:csrrs tp, fcsr, zero
	-[0x80000394]:sw a4, 136(ra)
Current Store : [0x80000398] : sw tp, 140(ra) -- Store: [0x80005f9c]:0x00000000




Last Coverpoint : ['rs1 : x11', 'rs2 : x14', 'rd : x13', 'rs3 : x12', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x91 and fm2 == 0x335718 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000026 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003b0]:fmadd.s a3, a1, a4, a2, dyn
	-[0x800003b4]:csrrs tp, fcsr, zero
	-[0x800003b8]:sw a3, 144(ra)
Current Store : [0x800003bc] : sw tp, 148(ra) -- Store: [0x80005fa4]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rs2 : x11', 'rd : x12', 'rs3 : x13', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x92 and fm2 == 0x335b79 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003d4]:fmadd.s a2, a4, a1, a3, dyn
	-[0x800003d8]:csrrs tp, fcsr, zero
	-[0x800003dc]:sw a2, 152(ra)
Current Store : [0x800003e0] : sw tp, 156(ra) -- Store: [0x80005fac]:0x00000000




Last Coverpoint : ['rs1 : x12', 'rs2 : x13', 'rd : x11', 'rs3 : x14', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x93 and fm2 == 0x335cb2 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000018 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003f8]:fmadd.s a1, a2, a3, a4, dyn
	-[0x800003fc]:csrrs tp, fcsr, zero
	-[0x80000400]:sw a1, 160(ra)
Current Store : [0x80000404] : sw tp, 164(ra) -- Store: [0x80005fb4]:0x00000000




Last Coverpoint : ['rs1 : x9', 'rs2 : x8', 'rd : x10', 'rs3 : x7', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x94 and fm2 == 0x335ca7 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000032 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000041c]:fmadd.s a0, s1, fp, t2, dyn
	-[0x80000420]:csrrs tp, fcsr, zero
	-[0x80000424]:sw a0, 168(ra)
Current Store : [0x80000428] : sw tp, 172(ra) -- Store: [0x80005fbc]:0x00000000




Last Coverpoint : ['rs1 : x7', 'rs2 : x10', 'rd : x9', 'rs3 : x8', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x95 and fm2 == 0x335db7 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000003 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000450]:fmadd.s s1, t2, a0, fp, dyn
	-[0x80000454]:csrrs a4, fcsr, zero
	-[0x80000458]:sw s1, 0(a1)
Current Store : [0x8000045c] : sw a4, 4(a1) -- Store: [0x80005fc4]:0x00000000




Last Coverpoint : ['rs1 : x10', 'rs2 : x7', 'rd : x8', 'rs3 : x9', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x85 and fm2 == 0x1a2491 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000036 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000474]:fmadd.s fp, a0, t2, s1, dyn
	-[0x80000478]:csrrs a4, fcsr, zero
	-[0x8000047c]:sw fp, 8(a1)
Current Store : [0x80000480] : sw a4, 12(a1) -- Store: [0x80005fcc]:0x00000000




Last Coverpoint : ['rs1 : x8', 'rs2 : x9', 'rd : x7', 'rs3 : x10', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x83 and fm2 == 0x6b6b0b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000013 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000498]:fmadd.s t2, fp, s1, a0, dyn
	-[0x8000049c]:csrrs a4, fcsr, zero
	-[0x800004a0]:sw t2, 16(a1)
Current Store : [0x800004a4] : sw a4, 20(a1) -- Store: [0x80005fd4]:0x00000000




Last Coverpoint : ['rs1 : x5', 'rs2 : x4', 'rd : x6', 'rs3 : x3', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x83 and fm2 == 0x11bc2c and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000009 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004bc]:fmadd.s t1, t0, tp, gp, dyn
	-[0x800004c0]:csrrs a4, fcsr, zero
	-[0x800004c4]:sw t1, 24(a1)
Current Store : [0x800004c8] : sw a4, 28(a1) -- Store: [0x80005fdc]:0x00000000




Last Coverpoint : ['rs1 : x3', 'rs2 : x6', 'rd : x5', 'rs3 : x4', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x85 and fm2 == 0x03b8d9 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000027 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004e0]:fmadd.s t0, gp, t1, tp, dyn
	-[0x800004e4]:csrrs a4, fcsr, zero
	-[0x800004e8]:sw t0, 32(a1)
Current Store : [0x800004ec] : sw a4, 36(a1) -- Store: [0x80005fe4]:0x00000000




Last Coverpoint : ['rs1 : x6', 'rs2 : x3', 'rd : x4', 'rs3 : x5', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x86 and fm2 == 0x02521d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000504]:fmadd.s tp, t1, gp, t0, dyn
	-[0x80000508]:csrrs a4, fcsr, zero
	-[0x8000050c]:sw tp, 40(a1)
Current Store : [0x80000510] : sw a4, 44(a1) -- Store: [0x80005fec]:0x00000000




Last Coverpoint : ['rs1 : x4', 'rs2 : x5', 'rd : x3', 'rs3 : x6', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x84 and fm2 == 0x65d01d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000009 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000528]:fmadd.s gp, tp, t0, t1, dyn
	-[0x8000052c]:csrrs a4, fcsr, zero
	-[0x80000530]:sw gp, 48(a1)
Current Store : [0x80000534] : sw a4, 52(a1) -- Store: [0x80005ff4]:0x00000000




Last Coverpoint : ['rs1 : x2', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x86 and fm2 == 0x1a2491 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000054c]:fmadd.s t6, sp, t5, t4, dyn
	-[0x80000550]:csrrs a4, fcsr, zero
	-[0x80000554]:sw t6, 56(a1)
Current Store : [0x80000558] : sw a4, 60(a1) -- Store: [0x80005ffc]:0x00000000




Last Coverpoint : ['rs1 : x1', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x87 and fm2 == 0x051f94 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000570]:fmadd.s t6, ra, t5, t4, dyn
	-[0x80000574]:csrrs a4, fcsr, zero
	-[0x80000578]:sw t6, 64(a1)
Current Store : [0x8000057c] : sw a4, 68(a1) -- Store: [0x80006004]:0x00000000




Last Coverpoint : ['rs1 : x0', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x87 and fm2 == 0x442e89 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000018 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000594]:fmadd.s t6, zero, t5, t4, dyn
	-[0x80000598]:csrrs a4, fcsr, zero
	-[0x8000059c]:sw t6, 72(a1)
Current Store : [0x800005a0] : sw a4, 76(a1) -- Store: [0x8000600c]:0x00000000




Last Coverpoint : ['rs2 : x2', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x88 and fm2 == 0x48bc6b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005b8]:fmadd.s t6, t5, sp, t4, dyn
	-[0x800005bc]:csrrs a4, fcsr, zero
	-[0x800005c0]:sw t6, 80(a1)
Current Store : [0x800005c4] : sw a4, 84(a1) -- Store: [0x80006014]:0x00000000




Last Coverpoint : ['rs2 : x1', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x89 and fm2 == 0x34c47b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000008 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005dc]:fmadd.s t6, t5, ra, t4, dyn
	-[0x800005e0]:csrrs a4, fcsr, zero
	-[0x800005e4]:sw t6, 88(a1)
Current Store : [0x800005e8] : sw a4, 92(a1) -- Store: [0x8000601c]:0x00000000




Last Coverpoint : ['rs2 : x0']
Last Code Sequence : 
	-[0x80000600]:fmadd.s t6, t5, zero, t4, dyn
	-[0x80000604]:csrrs a4, fcsr, zero
	-[0x80000608]:sw t6, 96(a1)
Current Store : [0x8000060c] : sw a4, 100(a1) -- Store: [0x80006024]:0x00000000




Last Coverpoint : ['rs3 : x2', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8b and fm2 == 0x35a4b0 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000034 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000624]:fmadd.s t6, t5, t4, sp, dyn
	-[0x80000628]:csrrs a4, fcsr, zero
	-[0x8000062c]:sw t6, 104(a1)
Current Store : [0x80000630] : sw a4, 108(a1) -- Store: [0x8000602c]:0x00000000




Last Coverpoint : ['rs3 : x1', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8c and fm2 == 0x34ebb8 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000047 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000648]:fmadd.s t6, t5, t4, ra, dyn
	-[0x8000064c]:csrrs a4, fcsr, zero
	-[0x80000650]:sw t6, 112(a1)
Current Store : [0x80000654] : sw a4, 116(a1) -- Store: [0x80006034]:0x00000000




Last Coverpoint : ['rs3 : x0']
Last Code Sequence : 
	-[0x8000066c]:fmadd.s t6, t5, t4, zero, dyn
	-[0x80000670]:csrrs a4, fcsr, zero
	-[0x80000674]:sw t6, 120(a1)
Current Store : [0x80000678] : sw a4, 124(a1) -- Store: [0x8000603c]:0x00000000




Last Coverpoint : ['rd : x2', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8e and fm2 == 0x33c6d8 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000690]:fmadd.s sp, t6, t5, t4, dyn
	-[0x80000694]:csrrs a4, fcsr, zero
	-[0x80000698]:sw sp, 128(a1)
Current Store : [0x8000069c] : sw a4, 132(a1) -- Store: [0x80006044]:0x00000000




Last Coverpoint : ['rd : x1', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8f and fm2 == 0x338663 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006b4]:fmadd.s ra, t6, t5, t4, dyn
	-[0x800006b8]:csrrs a4, fcsr, zero
	-[0x800006bc]:sw ra, 136(a1)
Current Store : [0x800006c0] : sw a4, 140(a1) -- Store: [0x8000604c]:0x00000000




Last Coverpoint : ['rd : x0', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x90 and fm2 == 0x337da1 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006d8]:fmadd.s zero, t6, t5, t4, dyn
	-[0x800006dc]:csrrs a4, fcsr, zero
	-[0x800006e0]:sw zero, 144(a1)
Current Store : [0x800006e4] : sw a4, 148(a1) -- Store: [0x80006054]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x91 and fm2 == 0x336bc3 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000050 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006fc]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000700]:csrrs a4, fcsr, zero
	-[0x80000704]:sw t6, 152(a1)
Current Store : [0x80000708] : sw a4, 156(a1) -- Store: [0x8000605c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x92 and fm2 == 0x335f26 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000010 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000720]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000724]:csrrs a4, fcsr, zero
	-[0x80000728]:sw t6, 160(a1)
Current Store : [0x8000072c] : sw a4, 164(a1) -- Store: [0x80006064]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x93 and fm2 == 0x336060 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000744]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000748]:csrrs a4, fcsr, zero
	-[0x8000074c]:sw t6, 168(a1)
Current Store : [0x80000750] : sw a4, 172(a1) -- Store: [0x8000606c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x94 and fm2 == 0x335f31 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000042 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000768]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000076c]:csrrs a4, fcsr, zero
	-[0x80000770]:sw t6, 176(a1)
Current Store : [0x80000774] : sw a4, 180(a1) -- Store: [0x80006074]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x95 and fm2 == 0x335e7e and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000044 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000078c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000790]:csrrs a4, fcsr, zero
	-[0x80000794]:sw t6, 184(a1)
Current Store : [0x80000798] : sw a4, 188(a1) -- Store: [0x8000607c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6d and fm2 == 0x6c0000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00003c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007b0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800007b4]:csrrs a4, fcsr, zero
	-[0x800007b8]:sw t6, 192(a1)
Current Store : [0x800007bc] : sw a4, 196(a1) -- Store: [0x80006084]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6e and fm2 == 0x140000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00004c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007d4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800007d8]:csrrs a4, fcsr, zero
	-[0x800007dc]:sw t6, 200(a1)
Current Store : [0x800007e0] : sw a4, 204(a1) -- Store: [0x8000608c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6d and fm2 == 0x080000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000026 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007f8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800007fc]:csrrs a4, fcsr, zero
	-[0x80000800]:sw t6, 208(a1)
Current Store : [0x80000804] : sw a4, 212(a1) -- Store: [0x80006094]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6d and fm2 == 0x400000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000038 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000081c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000820]:csrrs a4, fcsr, zero
	-[0x80000824]:sw t6, 216(a1)
Current Store : [0x80000828] : sw a4, 220(a1) -- Store: [0x8000609c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6b and fm2 == 0x300000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00001b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000840]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000844]:csrrs a4, fcsr, zero
	-[0x80000848]:sw t6, 224(a1)
Current Store : [0x8000084c] : sw a4, 228(a1) -- Store: [0x800060a4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6d and fm2 == 0x080000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000042 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000864]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000868]:csrrs a4, fcsr, zero
	-[0x8000086c]:sw t6, 232(a1)
Current Store : [0x80000870] : sw a4, 236(a1) -- Store: [0x800060ac]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6d and fm2 == 0x340000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000013 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000888]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000088c]:csrrs a4, fcsr, zero
	-[0x80000890]:sw t6, 240(a1)
Current Store : [0x80000894] : sw a4, 244(a1) -- Store: [0x800060b4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6e and fm2 == 0x380000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000024 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008ac]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800008b0]:csrrs a4, fcsr, zero
	-[0x800008b4]:sw t6, 248(a1)
Current Store : [0x800008b8] : sw a4, 252(a1) -- Store: [0x800060bc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6f and fm2 == 0x580000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000028 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008d0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800008d4]:csrrs a4, fcsr, zero
	-[0x800008d8]:sw t6, 256(a1)
Current Store : [0x800008dc] : sw a4, 260(a1) -- Store: [0x800060c4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x70 and fm2 == 0x6c0000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000028 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008f4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800008f8]:csrrs a4, fcsr, zero
	-[0x800008fc]:sw t6, 264(a1)
Current Store : [0x80000900] : sw a4, 268(a1) -- Store: [0x800060cc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x71 and fm2 == 0x7d4000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00000b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000918]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000091c]:csrrs a4, fcsr, zero
	-[0x80000920]:sw t6, 272(a1)
Current Store : [0x80000924] : sw a4, 276(a1) -- Store: [0x800060d4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x72 and fm2 == 0x746000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00005d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000093c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000940]:csrrs a4, fcsr, zero
	-[0x80000944]:sw t6, 280(a1)
Current Store : [0x80000948] : sw a4, 284(a1) -- Store: [0x800060dc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x73 and fm2 == 0x7c2000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00003e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000960]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000964]:csrrs a4, fcsr, zero
	-[0x80000968]:sw t6, 288(a1)
Current Store : [0x8000096c] : sw a4, 292(a1) -- Store: [0x800060e4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x74 and fm2 == 0x7d1800 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00005d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000984]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000988]:csrrs a4, fcsr, zero
	-[0x8000098c]:sw t6, 296(a1)
Current Store : [0x80000990] : sw a4, 300(a1) -- Store: [0x800060ec]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x75 and fm2 == 0x7fd400 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00000b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009a8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800009ac]:csrrs a4, fcsr, zero
	-[0x800009b0]:sw t6, 304(a1)
Current Store : [0x800009b4] : sw a4, 308(a1) -- Store: [0x800060f4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x76 and fm2 == 0x7ff600 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000005 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009cc]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800009d0]:csrrs a4, fcsr, zero
	-[0x800009d4]:sw t6, 312(a1)
Current Store : [0x800009d8] : sw a4, 316(a1) -- Store: [0x800060fc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x77 and fm2 == 0x7fa100 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00005f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009f0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800009f4]:csrrs a4, fcsr, zero
	-[0x800009f8]:sw t6, 320(a1)
Current Store : [0x800009fc] : sw a4, 324(a1) -- Store: [0x80006104]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x78 and fm2 == 0x7fef80 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000021 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a14]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000a18]:csrrs a4, fcsr, zero
	-[0x80000a1c]:sw t6, 328(a1)
Current Store : [0x80000a20] : sw a4, 332(a1) -- Store: [0x8000610c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x79 and fm2 == 0x7feb00 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000054 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a38]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000a3c]:csrrs a4, fcsr, zero
	-[0x80000a40]:sw t6, 336(a1)
Current Store : [0x80000a44] : sw a4, 340(a1) -- Store: [0x80006114]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7a and fm2 == 0x7ffda0 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000013 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a5c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000a60]:csrrs a4, fcsr, zero
	-[0x80000a64]:sw t6, 344(a1)
Current Store : [0x80000a68] : sw a4, 348(a1) -- Store: [0x8000611c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7b and fm2 == 0x7ffae0 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000052 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a80]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000a84]:csrrs a4, fcsr, zero
	-[0x80000a88]:sw t6, 352(a1)
Current Store : [0x80000a8c] : sw a4, 356(a1) -- Store: [0x80006124]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7c and fm2 == 0x7ffe00 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000040 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000aa4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000aa8]:csrrs a4, fcsr, zero
	-[0x80000aac]:sw t6, 360(a1)
Current Store : [0x80000ab0] : sw a4, 364(a1) -- Store: [0x8000612c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x7fff94 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00001b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ac8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000acc]:csrrs a4, fcsr, zero
	-[0x80000ad0]:sw t6, 368(a1)
Current Store : [0x80000ad4] : sw a4, 372(a1) -- Store: [0x80006134]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000030 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00005e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000aec]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000af0]:csrrs a4, fcsr, zero
	-[0x80000af4]:sw t6, 376(a1)
Current Store : [0x80000af8] : sw a4, 380(a1) -- Store: [0x8000613c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x00002a and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000051 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b10]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000b14]:csrrs a4, fcsr, zero
	-[0x80000b18]:sw t6, 384(a1)
Current Store : [0x80000b1c] : sw a4, 388(a1) -- Store: [0x80006144]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000009 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00000e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b34]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000b38]:csrrs a4, fcsr, zero
	-[0x80000b3c]:sw t6, 392(a1)
Current Store : [0x80000b40] : sw a4, 396(a1) -- Store: [0x8000614c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000020 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000038 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b58]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000b5c]:csrrs a4, fcsr, zero
	-[0x80000b60]:sw t6, 400(a1)
Current Store : [0x80000b64] : sw a4, 404(a1) -- Store: [0x80006154]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000012 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000014 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b7c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000b80]:csrrs a4, fcsr, zero
	-[0x80000b84]:sw t6, 408(a1)
Current Store : [0x80000b88] : sw a4, 412(a1) -- Store: [0x8000615c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x00002b and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000036 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ba0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000ba4]:csrrs a4, fcsr, zero
	-[0x80000ba8]:sw t6, 416(a1)
Current Store : [0x80000bac] : sw a4, 420(a1) -- Store: [0x80006164]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x00002a and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000013 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bc4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000bc8]:csrrs a4, fcsr, zero
	-[0x80000bcc]:sw t6, 424(a1)
Current Store : [0x80000bd0] : sw a4, 428(a1) -- Store: [0x8000616c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000068 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000050 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000be8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000bec]:csrrs a4, fcsr, zero
	-[0x80000bf0]:sw t6, 432(a1)
Current Store : [0x80000bf4] : sw a4, 436(a1) -- Store: [0x80006174]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x0000a2 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000043 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c0c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000c10]:csrrs a4, fcsr, zero
	-[0x80000c14]:sw t6, 440(a1)
Current Store : [0x80000c18] : sw a4, 444(a1) -- Store: [0x8000617c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000120 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c30]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000c34]:csrrs a4, fcsr, zero
	-[0x80000c38]:sw t6, 448(a1)
Current Store : [0x80000c3c] : sw a4, 452(a1) -- Store: [0x80006184]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x00021f and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00003e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c54]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000c58]:csrrs a4, fcsr, zero
	-[0x80000c5c]:sw t6, 456(a1)
Current Store : [0x80000c60] : sw a4, 460(a1) -- Store: [0x8000618c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000413 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000026 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c78]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000c7c]:csrrs a4, fcsr, zero
	-[0x80000c80]:sw t6, 464(a1)
Current Store : [0x80000c84] : sw a4, 468(a1) -- Store: [0x80006194]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x00081a and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000034 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c9c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000ca0]:csrrs a4, fcsr, zero
	-[0x80000ca4]:sw t6, 472(a1)
Current Store : [0x80000ca8] : sw a4, 476(a1) -- Store: [0x8000619c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x001008 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00000f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cc0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000cc4]:csrrs a4, fcsr, zero
	-[0x80000cc8]:sw t6, 480(a1)
Current Store : [0x80000ccc] : sw a4, 484(a1) -- Store: [0x800061a4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x002022 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000045 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ce4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000ce8]:csrrs a4, fcsr, zero
	-[0x80000cec]:sw t6, 488(a1)
Current Store : [0x80000cf0] : sw a4, 492(a1) -- Store: [0x800061ac]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x004028 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000051 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d08]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000d0c]:csrrs a4, fcsr, zero
	-[0x80000d10]:sw t6, 496(a1)
Current Store : [0x80000d14] : sw a4, 500(a1) -- Store: [0x800061b4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x008016 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00002c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d2c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000d30]:csrrs a4, fcsr, zero
	-[0x80000d34]:sw t6, 504(a1)
Current Store : [0x80000d38] : sw a4, 508(a1) -- Store: [0x800061bc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x010006 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00000c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d50]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000d54]:csrrs a4, fcsr, zero
	-[0x80000d58]:sw t6, 512(a1)
Current Store : [0x80000d5c] : sw a4, 516(a1) -- Store: [0x800061c4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x020012 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000023 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d74]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000d78]:csrrs a4, fcsr, zero
	-[0x80000d7c]:sw t6, 520(a1)
Current Store : [0x80000d80] : sw a4, 524(a1) -- Store: [0x800061cc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x040004 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000009 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d98]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000d9c]:csrrs a4, fcsr, zero
	-[0x80000da0]:sw t6, 528(a1)
Current Store : [0x80000da4] : sw a4, 532(a1) -- Store: [0x800061d4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x08002c and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000059 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000dbc]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000dc0]:csrrs a4, fcsr, zero
	-[0x80000dc4]:sw t6, 536(a1)
Current Store : [0x80000dc8] : sw a4, 540(a1) -- Store: [0x800061dc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x100031 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000062 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000de0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000de4]:csrrs a4, fcsr, zero
	-[0x80000de8]:sw t6, 544(a1)
Current Store : [0x80000dec] : sw a4, 548(a1) -- Store: [0x800061e4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x200018 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00002f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e04]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000e08]:csrrs a4, fcsr, zero
	-[0x80000e0c]:sw t6, 552(a1)
Current Store : [0x80000e10] : sw a4, 556(a1) -- Store: [0x800061ec]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x85 and fm2 == 0x2e9049 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e28]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000e2c]:csrrs a4, fcsr, zero
	-[0x80000e30]:sw t6, 560(a1)
Current Store : [0x80000e34] : sw a4, 564(a1) -- Store: [0x800061f4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x86 and fm2 == 0x068650 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000063 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e4c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000e50]:csrrs a4, fcsr, zero
	-[0x80000e54]:sw t6, 568(a1)
Current Store : [0x80000e58] : sw a4, 572(a1) -- Store: [0x800061fc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x84 and fm2 == 0x5a9a41 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e70]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000e74]:csrrs a4, fcsr, zero
	-[0x80000e78]:sw t6, 576(a1)
Current Store : [0x80000e7c] : sw a4, 580(a1) -- Store: [0x80006204]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x85 and fm2 == 0x38f8ad and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e94]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000e98]:csrrs a4, fcsr, zero
	-[0x80000e9c]:sw t6, 584(a1)
Current Store : [0x80000ea0] : sw a4, 588(a1) -- Store: [0x8000620c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x84 and fm2 == 0x76a0e7 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000eb8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000ebc]:csrrs a4, fcsr, zero
	-[0x80000ec0]:sw t6, 592(a1)
Current Store : [0x80000ec4] : sw a4, 596(a1) -- Store: [0x80006214]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x82 and fm2 == 0x49c977 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000edc]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000ee0]:csrrs a4, fcsr, zero
	-[0x80000ee4]:sw t6, 600(a1)
Current Store : [0x80000ee8] : sw a4, 604(a1) -- Store: [0x8000621c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x60352f and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f00]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000f04]:csrrs a4, fcsr, zero
	-[0x80000f08]:sw t6, 608(a1)
Current Store : [0x80000f0c] : sw a4, 612(a1) -- Store: [0x80006224]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x335dc0 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000041 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f24]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000f28]:csrrs a4, fcsr, zero
	-[0x80000f2c]:sw t6, 616(a1)
Current Store : [0x80000f30] : sw a4, 620(a1) -- Store: [0x8000622c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x87 and fm2 == 0x1489a3 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f48]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000f4c]:csrrs a4, fcsr, zero
	-[0x80000f50]:sw t6, 624(a1)
Current Store : [0x80000f54] : sw a4, 628(a1) -- Store: [0x80006234]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x88 and fm2 == 0x16a3bc and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000053 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f6c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000f70]:csrrs a4, fcsr, zero
	-[0x80000f74]:sw t6, 632(a1)
Current Store : [0x80000f78] : sw a4, 636(a1) -- Store: [0x8000623c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x89 and fm2 == 0x22e6a4 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f90]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000f94]:csrrs a4, fcsr, zero
	-[0x80000f98]:sw t6, 640(a1)
Current Store : [0x80000f9c] : sw a4, 644(a1) -- Store: [0x80006244]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8a and fm2 == 0x2f833c and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fb4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000fb8]:csrrs a4, fcsr, zero
	-[0x80000fbc]:sw t6, 648(a1)
Current Store : [0x80000fc0] : sw a4, 652(a1) -- Store: [0x8000624c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8b and fm2 == 0x2f833c and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000059 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fd8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000fdc]:csrrs a4, fcsr, zero
	-[0x80000fe0]:sw t6, 656(a1)
Current Store : [0x80000fe4] : sw a4, 660(a1) -- Store: [0x80006254]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8c and fm2 == 0x325be9 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ffc]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001000]:csrrs a4, fcsr, zero
	-[0x80001004]:sw t6, 664(a1)
Current Store : [0x80001008] : sw a4, 668(a1) -- Store: [0x8000625c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8d and fm2 == 0x32a4c7 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000043 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001020]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001024]:csrrs a4, fcsr, zero
	-[0x80001028]:sw t6, 672(a1)
Current Store : [0x8000102c] : sw a4, 676(a1) -- Store: [0x80006264]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8e and fm2 == 0x32ffdc and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000044 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001044]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001048]:csrrs a4, fcsr, zero
	-[0x8000104c]:sw t6, 680(a1)
Current Store : [0x80001050] : sw a4, 684(a1) -- Store: [0x8000626c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8f and fm2 == 0x334c3b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001068]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000106c]:csrrs a4, fcsr, zero
	-[0x80001070]:sw t6, 688(a1)
Current Store : [0x80001074] : sw a4, 692(a1) -- Store: [0x80006274]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x90 and fm2 == 0x334861 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000108c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001090]:csrrs a4, fcsr, zero
	-[0x80001094]:sw t6, 696(a1)
Current Store : [0x80001098] : sw a4, 700(a1) -- Store: [0x8000627c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x91 and fm2 == 0x334e55 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000059 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010b0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800010b4]:csrrs a4, fcsr, zero
	-[0x800010b8]:sw t6, 704(a1)
Current Store : [0x800010bc] : sw a4, 708(a1) -- Store: [0x80006284]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x92 and fm2 == 0x3355b1 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010d4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800010d8]:csrrs a4, fcsr, zero
	-[0x800010dc]:sw t6, 712(a1)
Current Store : [0x800010e0] : sw a4, 716(a1) -- Store: [0x8000628c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x93 and fm2 == 0x335cd4 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000016 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010f8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800010fc]:csrrs a4, fcsr, zero
	-[0x80001100]:sw t6, 720(a1)
Current Store : [0x80001104] : sw a4, 724(a1) -- Store: [0x80006294]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x94 and fm2 == 0x335ceb and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000027 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000111c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001120]:csrrs a4, fcsr, zero
	-[0x80001124]:sw t6, 728(a1)
Current Store : [0x80001128] : sw a4, 732(a1) -- Store: [0x8000629c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x95 and fm2 == 0x335cbe and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001140]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001144]:csrrs a4, fcsr, zero
	-[0x80001148]:sw t6, 736(a1)
Current Store : [0x8000114c] : sw a4, 740(a1) -- Store: [0x800062a4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x84 and fm2 == 0x15bc2c and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001164]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001168]:csrrs a4, fcsr, zero
	-[0x8000116c]:sw t6, 744(a1)
Current Store : [0x80001170] : sw a4, 748(a1) -- Store: [0x800062ac]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x85 and fm2 == 0x5d67b8 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001188]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000118c]:csrrs a4, fcsr, zero
	-[0x80001190]:sw t6, 752(a1)
Current Store : [0x80001194] : sw a4, 756(a1) -- Store: [0x800062b4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x85 and fm2 == 0x6e3882 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000050 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011ac]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800011b0]:csrrs a4, fcsr, zero
	-[0x800011b4]:sw t6, 760(a1)
Current Store : [0x800011b8] : sw a4, 764(a1) -- Store: [0x800062bc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x84 and fm2 == 0x54ff53 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011d0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800011d4]:csrrs a4, fcsr, zero
	-[0x800011d8]:sw t6, 768(a1)
Current Store : [0x800011dc] : sw a4, 772(a1) -- Store: [0x800062c4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x86 and fm2 == 0x0953c7 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000051 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011f4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800011f8]:csrrs a4, fcsr, zero
	-[0x800011fc]:sw t6, 776(a1)
Current Store : [0x80001200] : sw a4, 780(a1) -- Store: [0x800062cc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x86 and fm2 == 0x03b8d9 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001218]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000121c]:csrrs a4, fcsr, zero
	-[0x80001220]:sw t6, 784(a1)
Current Store : [0x80001224] : sw a4, 788(a1) -- Store: [0x800062d4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x85 and fm2 == 0x73d370 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000016 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000123c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001240]:csrrs a4, fcsr, zero
	-[0x80001244]:sw t6, 792(a1)
Current Store : [0x80001248] : sw a4, 796(a1) -- Store: [0x800062dc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x86 and fm2 == 0x4dfdaa and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000012 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001260]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001264]:csrrs a4, fcsr, zero
	-[0x80001268]:sw t6, 800(a1)
Current Store : [0x8000126c] : sw a4, 804(a1) -- Store: [0x800062e4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x87 and fm2 == 0x50cb21 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000029 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001284]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001288]:csrrs a4, fcsr, zero
	-[0x8000128c]:sw t6, 808(a1)
Current Store : [0x80001290] : sw a4, 812(a1) -- Store: [0x800062ec]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x88 and fm2 == 0x496fc9 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012a8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800012ac]:csrrs a4, fcsr, zero
	-[0x800012b0]:sw t6, 816(a1)
Current Store : [0x800012b4] : sw a4, 820(a1) -- Store: [0x800062f4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x89 and fm2 == 0x3ffa57 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000047 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012cc]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800012d0]:csrrs a4, fcsr, zero
	-[0x800012d4]:sw t6, 824(a1)
Current Store : [0x800012d8] : sw a4, 828(a1) -- Store: [0x800062fc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8a and fm2 == 0x382ee4 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000036 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012f0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800012f4]:csrrs a4, fcsr, zero
	-[0x800012f8]:sw t6, 832(a1)
Current Store : [0x800012fc] : sw a4, 836(a1) -- Store: [0x80006304]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8b and fm2 == 0x35616d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001314]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001318]:csrrs a4, fcsr, zero
	-[0x8000131c]:sw t6, 840(a1)
Current Store : [0x80001320] : sw a4, 844(a1) -- Store: [0x8000630c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8c and fm2 == 0x353fcb and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000055 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001338]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000133c]:csrrs a4, fcsr, zero
	-[0x80001340]:sw t6, 848(a1)
Current Store : [0x80001344] : sw a4, 852(a1) -- Store: [0x80006314]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8d and fm2 == 0x34358c and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000135c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001360]:csrrs a4, fcsr, zero
	-[0x80001364]:sw t6, 856(a1)
Current Store : [0x80001368] : sw a4, 860(a1) -- Store: [0x8000631c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8e and fm2 == 0x338a97 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001380]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001384]:csrrs a4, fcsr, zero
	-[0x80001388]:sw t6, 864(a1)
Current Store : [0x8000138c] : sw a4, 868(a1) -- Store: [0x80006324]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8f and fm2 == 0x335f26 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013a4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800013a8]:csrrs a4, fcsr, zero
	-[0x800013ac]:sw t6, 872(a1)
Current Store : [0x800013b0] : sw a4, 876(a1) -- Store: [0x8000632c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x90 and fm2 == 0x337051 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000034 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013c8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800013cc]:csrrs a4, fcsr, zero
	-[0x800013d0]:sw t6, 880(a1)
Current Store : [0x800013d4] : sw a4, 884(a1) -- Store: [0x80006334]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x91 and fm2 == 0x336735 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000035 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013ec]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800013f0]:csrrs a4, fcsr, zero
	-[0x800013f4]:sw t6, 888(a1)
Current Store : [0x800013f8] : sw a4, 892(a1) -- Store: [0x8000633c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x92 and fm2 == 0x3365a1 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000059 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001410]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001414]:csrrs a4, fcsr, zero
	-[0x80001418]:sw t6, 896(a1)
Current Store : [0x8000141c] : sw a4, 900(a1) -- Store: [0x80006344]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x93 and fm2 == 0x335e03 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000005 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001434]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001438]:csrrs a4, fcsr, zero
	-[0x8000143c]:sw t6, 904(a1)
Current Store : [0x80001440] : sw a4, 908(a1) -- Store: [0x8000634c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x94 and fm2 == 0x335e0e and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00000d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001458]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000145c]:csrrs a4, fcsr, zero
	-[0x80001460]:sw t6, 912(a1)
Current Store : [0x80001464] : sw a4, 916(a1) -- Store: [0x80006354]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x95 and fm2 == 0x335e86 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000046 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000147c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001480]:csrrs a4, fcsr, zero
	-[0x80001484]:sw t6, 920(a1)
Current Store : [0x80001488] : sw a4, 924(a1) -- Store: [0x8000635c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335d54 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014a0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800014a4]:csrrs a4, fcsr, zero
	-[0x800014a8]:sw t6, 928(a1)
Current Store : [0x800014ac] : sw a4, 932(a1) -- Store: [0x80006364]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335d7b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014c4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800014c8]:csrrs a4, fcsr, zero
	-[0x800014cc]:sw t6, 936(a1)
Current Store : [0x800014d0] : sw a4, 940(a1) -- Store: [0x8000636c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335d6d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000036 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014e8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800014ec]:csrrs a4, fcsr, zero
	-[0x800014f0]:sw t6, 944(a1)
Current Store : [0x800014f4] : sw a4, 948(a1) -- Store: [0x80006374]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335d4d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000049 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000150c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001510]:csrrs a4, fcsr, zero
	-[0x80001514]:sw t6, 952(a1)
Current Store : [0x80001518] : sw a4, 956(a1) -- Store: [0x8000637c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335d54 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001530]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001534]:csrrs a4, fcsr, zero
	-[0x80001538]:sw t6, 960(a1)
Current Store : [0x8000153c] : sw a4, 964(a1) -- Store: [0x80006384]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335d74 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000015 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001554]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001558]:csrrs a4, fcsr, zero
	-[0x8000155c]:sw t6, 968(a1)
Current Store : [0x80001560] : sw a4, 972(a1) -- Store: [0x8000638c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335cfe and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000049 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001578]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000157c]:csrrs a4, fcsr, zero
	-[0x80001580]:sw t6, 976(a1)
Current Store : [0x80001584] : sw a4, 980(a1) -- Store: [0x80006394]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335ce6 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000159c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800015a0]:csrrs a4, fcsr, zero
	-[0x800015a4]:sw t6, 984(a1)
Current Store : [0x800015a8] : sw a4, 988(a1) -- Store: [0x8000639c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335bea and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015c0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800015c4]:csrrs a4, fcsr, zero
	-[0x800015c8]:sw t6, 992(a1)
Current Store : [0x800015cc] : sw a4, 996(a1) -- Store: [0x800063a4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335ab2 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015e4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800015e8]:csrrs a4, fcsr, zero
	-[0x800015ec]:sw t6, 1000(a1)
Current Store : [0x800015f0] : sw a4, 1004(a1) -- Store: [0x800063ac]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x3357df and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000031 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001608]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000160c]:csrrs a4, fcsr, zero
	-[0x80001610]:sw t6, 1008(a1)
Current Store : [0x80001614] : sw a4, 1012(a1) -- Store: [0x800063b4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x33524d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000162c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001630]:csrrs a4, fcsr, zero
	-[0x80001634]:sw t6, 1016(a1)
Current Store : [0x80001638] : sw a4, 1020(a1) -- Store: [0x800063bc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x334718 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001658]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000165c]:csrrs a4, fcsr, zero
	-[0x80001660]:sw t6, 0(a1)
Current Store : [0x80001664] : sw a4, 4(a1) -- Store: [0x800063c4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x33309a and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000037 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000167c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001680]:csrrs a4, fcsr, zero
	-[0x80001684]:sw t6, 8(a1)
Current Store : [0x80001688] : sw a4, 12(a1) -- Store: [0x800063cc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x3303e7 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016a0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800016a4]:csrrs a4, fcsr, zero
	-[0x800016a8]:sw t6, 16(a1)
Current Store : [0x800016ac] : sw a4, 20(a1) -- Store: [0x800063d4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x32aa2b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000026 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016c4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800016c8]:csrrs a4, fcsr, zero
	-[0x800016cc]:sw t6, 24(a1)
Current Store : [0x800016d0] : sw a4, 28(a1) -- Store: [0x800063dc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x31f683 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016e8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800016ec]:csrrs a4, fcsr, zero
	-[0x800016f0]:sw t6, 32(a1)
Current Store : [0x800016f4] : sw a4, 36(a1) -- Store: [0x800063e4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x309001 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000032 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000170c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001710]:csrrs a4, fcsr, zero
	-[0x80001714]:sw t6, 40(a1)
Current Store : [0x80001718] : sw a4, 44(a1) -- Store: [0x800063ec]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x2dc264 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001730]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001734]:csrrs a4, fcsr, zero
	-[0x80001738]:sw t6, 48(a1)
Current Store : [0x8000173c] : sw a4, 52(a1) -- Store: [0x800063f4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x28275d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001754]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001758]:csrrs a4, fcsr, zero
	-[0x8000175c]:sw t6, 56(a1)
Current Store : [0x80001760] : sw a4, 60(a1) -- Store: [0x800063fc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x1cf1ac and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000040 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001778]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000177c]:csrrs a4, fcsr, zero
	-[0x80001780]:sw t6, 64(a1)
Current Store : [0x80001784] : sw a4, 68(a1) -- Store: [0x80006404]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x068603 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000036 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000179c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800017a0]:csrrs a4, fcsr, zero
	-[0x800017a4]:sw t6, 72(a1)
Current Store : [0x800017a8] : sw a4, 76(a1) -- Store: [0x8000640c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x95 and fm2 == 0x335d4a and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000029 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017c0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800017c4]:csrrs a4, fcsr, zero
	-[0x800017c8]:sw t6, 80(a1)
Current Store : [0x800017cc] : sw a4, 84(a1) -- Store: [0x80006414]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335e15 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017e4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800017e8]:csrrs a4, fcsr, zero
	-[0x800017ec]:sw t6, 88(a1)
Current Store : [0x800017f0] : sw a4, 92(a1) -- Store: [0x8000641c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335dd6 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000013 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001808]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000180c]:csrrs a4, fcsr, zero
	-[0x80001810]:sw t6, 96(a1)
Current Store : [0x80001814] : sw a4, 100(a1) -- Store: [0x80006424]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335dbe and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000004 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000182c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001830]:csrrs a4, fcsr, zero
	-[0x80001834]:sw t6, 104(a1)
Current Store : [0x80001838] : sw a4, 108(a1) -- Store: [0x8000642c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335e2a and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000055 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001850]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001854]:csrrs a4, fcsr, zero
	-[0x80001858]:sw t6, 112(a1)
Current Store : [0x8000185c] : sw a4, 116(a1) -- Store: [0x80006434]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335dd5 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000020 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001874]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001878]:csrrs a4, fcsr, zero
	-[0x8000187c]:sw t6, 120(a1)
Current Store : [0x80001880] : sw a4, 124(a1) -- Store: [0x8000643c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335d9a and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000006 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001898]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000189c]:csrrs a4, fcsr, zero
	-[0x800018a0]:sw t6, 128(a1)
Current Store : [0x800018a4] : sw a4, 132(a1) -- Store: [0x80006444]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335dac and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000033 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018bc]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800018c0]:csrrs a4, fcsr, zero
	-[0x800018c4]:sw t6, 136(a1)
Current Store : [0x800018c8] : sw a4, 140(a1) -- Store: [0x8000644c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335d62 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018e0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800018e4]:csrrs a4, fcsr, zero
	-[0x800018e8]:sw t6, 144(a1)
Current Store : [0x800018ec] : sw a4, 148(a1) -- Store: [0x80006454]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335c84 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000020 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001904]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001908]:csrrs a4, fcsr, zero
	-[0x8000190c]:sw t6, 152(a1)
Current Store : [0x80001910] : sw a4, 156(a1) -- Store: [0x8000645c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335af8 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000005 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001928]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000192c]:csrrs a4, fcsr, zero
	-[0x80001930]:sw t6, 160(a1)
Current Store : [0x80001934] : sw a4, 164(a1) -- Store: [0x80006464]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x33587b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000194c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001950]:csrrs a4, fcsr, zero
	-[0x80001954]:sw t6, 168(a1)
Current Store : [0x80001958] : sw a4, 172(a1) -- Store: [0x8000646c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x3352a4 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000014 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001970]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001974]:csrrs a4, fcsr, zero
	-[0x80001978]:sw t6, 176(a1)
Current Store : [0x8000197c] : sw a4, 180(a1) -- Store: [0x80006474]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x334788 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000026 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001994]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001998]:csrrs a4, fcsr, zero
	-[0x8000199c]:sw t6, 184(a1)
Current Store : [0x800019a0] : sw a4, 188(a1) -- Store: [0x8000647c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x333154 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800019b8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800019bc]:csrrs a4, fcsr, zero
	-[0x800019c0]:sw t6, 192(a1)
Current Store : [0x800019c4] : sw a4, 196(a1) -- Store: [0x80006484]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x33046d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000043 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800019dc]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800019e0]:csrrs a4, fcsr, zero
	-[0x800019e4]:sw t6, 200(a1)
Current Store : [0x800019e8] : sw a4, 204(a1) -- Store: [0x8000648c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x32aa65 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000003 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a00]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001a04]:csrrs a4, fcsr, zero
	-[0x80001a08]:sw t6, 208(a1)
Current Store : [0x80001a0c] : sw a4, 212(a1) -- Store: [0x80006494]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x31f739 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000027 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a24]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001a28]:csrrs a4, fcsr, zero
	-[0x80001a2c]:sw t6, 216(a1)
Current Store : [0x80001a30] : sw a4, 220(a1) -- Store: [0x8000649c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x3090a8 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000045 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a48]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001a4c]:csrrs a4, fcsr, zero
	-[0x80001a50]:sw t6, 224(a1)
Current Store : [0x80001a54] : sw a4, 228(a1) -- Store: [0x800064a4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x2dc33f and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a6c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001a70]:csrrs a4, fcsr, zero
	-[0x80001a74]:sw t6, 232(a1)
Current Store : [0x80001a78] : sw a4, 236(a1) -- Store: [0x800064ac]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x282866 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a90]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001a94]:csrrs a4, fcsr, zero
	-[0x80001a98]:sw t6, 240(a1)
Current Store : [0x80001a9c] : sw a4, 244(a1) -- Store: [0x800064b4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x1cf25a and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ab4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001ab8]:csrrs a4, fcsr, zero
	-[0x80001abc]:sw t6, 248(a1)
Current Store : [0x80001ac0] : sw a4, 252(a1) -- Store: [0x800064bc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x068690 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ad8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001adc]:csrrs a4, fcsr, zero
	-[0x80001ae0]:sw t6, 256(a1)
Current Store : [0x80001ae4] : sw a4, 260(a1) -- Store: [0x800064c4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x95 and fm2 == 0x335dcb and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000005 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001afc]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001b00]:csrrs a4, fcsr, zero
	-[0x80001b04]:sw t6, 264(a1)
Current Store : [0x80001b08] : sw a4, 268(a1) -- Store: [0x800064cc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6d and fm2 == 0x000000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000021 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b20]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001b24]:csrrs a4, fcsr, zero
	-[0x80001b28]:sw t6, 272(a1)
Current Store : [0x80001b2c] : sw a4, 276(a1) -- Store: [0x800064d4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6e and fm2 == 0x2c0000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000058 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b44]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001b48]:csrrs a4, fcsr, zero
	-[0x80001b4c]:sw t6, 280(a1)
Current Store : [0x80001b50] : sw a4, 284(a1) -- Store: [0x800064dc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6e and fm2 == 0x280000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000058 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b68]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001b6c]:csrrs a4, fcsr, zero
	-[0x80001b70]:sw t6, 288(a1)
Current Store : [0x80001b74] : sw a4, 292(a1) -- Store: [0x800064e4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6a and fm2 == 0x400000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b8c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001b90]:csrrs a4, fcsr, zero
	-[0x80001b94]:sw t6, 296(a1)
Current Store : [0x80001b98] : sw a4, 300(a1) -- Store: [0x800064ec]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6d and fm2 == 0x1c0000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000037 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001bb0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001bb4]:csrrs a4, fcsr, zero
	-[0x80001bb8]:sw t6, 304(a1)
Current Store : [0x80001bbc] : sw a4, 308(a1) -- Store: [0x800064f4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6c and fm2 == 0x100000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000032 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001bd4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001bd8]:csrrs a4, fcsr, zero
	-[0x80001bdc]:sw t6, 312(a1)
Current Store : [0x80001be0] : sw a4, 316(a1) -- Store: [0x800064fc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6b and fm2 == 0x500000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000033 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001bf8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001bfc]:csrrs a4, fcsr, zero
	-[0x80001c00]:sw t6, 320(a1)
Current Store : [0x80001c04] : sw a4, 324(a1) -- Store: [0x80006504]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6e and fm2 == 0x340000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000026 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c1c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001c20]:csrrs a4, fcsr, zero
	-[0x80001c24]:sw t6, 328(a1)
Current Store : [0x80001c28] : sw a4, 332(a1) -- Store: [0x8000650c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6f and fm2 == 0x1e0000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000062 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c50]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001c54]:csrrs a4, fcsr, zero
	-[0x80001c58]:sw t6, 336(a1)
Current Store : [0x80001c5c] : sw a4, 340(a1) -- Store: [0x80006514]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x70 and fm2 == 0x6c8000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000027 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ca4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001ca8]:csrrs a4, fcsr, zero
	-[0x80001cac]:sw t6, 344(a1)
Current Store : [0x80001cb0] : sw a4, 348(a1) -- Store: [0x8000651c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x71 and fm2 == 0x69c000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000059 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001cf8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001cfc]:csrrs a4, fcsr, zero
	-[0x80001d00]:sw t6, 352(a1)
Current Store : [0x80001d04] : sw a4, 356(a1) -- Store: [0x80006524]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x72 and fm2 == 0x76a000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00004b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d4c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001d50]:csrrs a4, fcsr, zero
	-[0x80001d54]:sw t6, 360(a1)
Current Store : [0x80001d58] : sw a4, 364(a1) -- Store: [0x8000652c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x73 and fm2 == 0x7ad000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000053 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001da0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001da4]:csrrs a4, fcsr, zero
	-[0x80001da8]:sw t6, 368(a1)
Current Store : [0x80001dac] : sw a4, 372(a1) -- Store: [0x80006534]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x74 and fm2 == 0x7ec000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000028 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001df4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001df8]:csrrs a4, fcsr, zero
	-[0x80001dfc]:sw t6, 376(a1)
Current Store : [0x80001e00] : sw a4, 380(a1) -- Store: [0x8000653c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x75 and fm2 == 0x7fa800 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000016 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e48]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001e4c]:csrrs a4, fcsr, zero
	-[0x80001e50]:sw t6, 384(a1)
Current Store : [0x80001e54] : sw a4, 388(a1) -- Store: [0x80006544]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x76 and fm2 == 0x7f4e00 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000059 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e9c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001ea0]:csrrs a4, fcsr, zero
	-[0x80001ea4]:sw t6, 392(a1)
Current Store : [0x80001ea8] : sw a4, 396(a1) -- Store: [0x8000654c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x77 and fm2 == 0x7fd500 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00002b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ef0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001ef4]:csrrs a4, fcsr, zero
	-[0x80001ef8]:sw t6, 400(a1)
Current Store : [0x80001efc] : sw a4, 404(a1) -- Store: [0x80006554]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x78 and fm2 == 0x7fe480 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000037 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f44]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001f48]:csrrs a4, fcsr, zero
	-[0x80001f4c]:sw t6, 408(a1)
Current Store : [0x80001f50] : sw a4, 412(a1) -- Store: [0x8000655c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x79 and fm2 == 0x7ffdc0 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000009 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f98]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001f9c]:csrrs a4, fcsr, zero
	-[0x80001fa0]:sw t6, 416(a1)
Current Store : [0x80001fa4] : sw a4, 420(a1) -- Store: [0x80006564]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7a and fm2 == 0x7ffba0 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000023 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001fec]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001ff0]:csrrs a4, fcsr, zero
	-[0x80001ff4]:sw t6, 424(a1)
Current Store : [0x80001ff8] : sw a4, 428(a1) -- Store: [0x8000656c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7b and fm2 == 0x7fff70 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000009 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002040]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002044]:csrrs a4, fcsr, zero
	-[0x80002048]:sw t6, 432(a1)
Current Store : [0x8000204c] : sw a4, 436(a1) -- Store: [0x80006574]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7c and fm2 == 0x7ffea8 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00002b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002094]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002098]:csrrs a4, fcsr, zero
	-[0x8000209c]:sw t6, 440(a1)
Current Store : [0x800020a0] : sw a4, 444(a1) -- Store: [0x8000657c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x7fffdc and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000009 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800020e8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800020ec]:csrrs a4, fcsr, zero
	-[0x800020f0]:sw t6, 448(a1)
Current Store : [0x800020f4] : sw a4, 452(a1) -- Store: [0x80006584]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x00002e and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00005a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000213c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002140]:csrrs a4, fcsr, zero
	-[0x80002144]:sw t6, 456(a1)
Current Store : [0x80002148] : sw a4, 460(a1) -- Store: [0x8000658c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000004 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000007 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002190]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002194]:csrrs a4, fcsr, zero
	-[0x80002198]:sw t6, 464(a1)
Current Store : [0x8000219c] : sw a4, 468(a1) -- Store: [0x80006594]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x00000a and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000010 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800021e4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800021e8]:csrrs a4, fcsr, zero
	-[0x800021ec]:sw t6, 472(a1)
Current Store : [0x800021f0] : sw a4, 476(a1) -- Store: [0x8000659c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x00002c and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00004f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002238]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000223c]:csrrs a4, fcsr, zero
	-[0x80002240]:sw t6, 480(a1)
Current Store : [0x80002244] : sw a4, 484(a1) -- Store: [0x800065a4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000036 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00005c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000228c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002290]:csrrs a4, fcsr, zero
	-[0x80002294]:sw t6, 488(a1)
Current Store : [0x80002298] : sw a4, 492(a1) -- Store: [0x800065ac]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000023 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000026 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800022e0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800022e4]:csrrs a4, fcsr, zero
	-[0x800022e8]:sw t6, 496(a1)
Current Store : [0x800022ec] : sw a4, 500(a1) -- Store: [0x800065b4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x00002a and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000014 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002334]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002338]:csrrs a4, fcsr, zero
	-[0x8000233c]:sw t6, 504(a1)
Current Store : [0x80002340] : sw a4, 508(a1) -- Store: [0x800065bc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000068 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00004f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002388]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000238c]:csrrs a4, fcsr, zero
	-[0x80002390]:sw t6, 512(a1)
Current Store : [0x80002394] : sw a4, 516(a1) -- Store: [0x800065c4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x00009a and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000035 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800023dc]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800023e0]:csrrs a4, fcsr, zero
	-[0x800023e4]:sw t6, 520(a1)
Current Store : [0x800023e8] : sw a4, 524(a1) -- Store: [0x800065cc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000104 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000009 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002430]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002434]:csrrs a4, fcsr, zero
	-[0x80002438]:sw t6, 528(a1)
Current Store : [0x8000243c] : sw a4, 532(a1) -- Store: [0x800065d4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000203 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000006 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002484]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002488]:csrrs a4, fcsr, zero
	-[0x8000248c]:sw t6, 536(a1)
Current Store : [0x80002490] : sw a4, 540(a1) -- Store: [0x800065dc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x00042a and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000055 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800024d8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800024dc]:csrrs a4, fcsr, zero
	-[0x800024e0]:sw t6, 544(a1)
Current Store : [0x800024e4] : sw a4, 548(a1) -- Store: [0x800065e4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000830 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000060 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000252c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002530]:csrrs a4, fcsr, zero
	-[0x80002534]:sw t6, 552(a1)
Current Store : [0x80002538] : sw a4, 556(a1) -- Store: [0x800065ec]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x001012 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000024 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002580]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002584]:csrrs a4, fcsr, zero
	-[0x80002588]:sw t6, 560(a1)
Current Store : [0x8000258c] : sw a4, 564(a1) -- Store: [0x800065f4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x002025 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00004a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800025d4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800025d8]:csrrs a4, fcsr, zero
	-[0x800025dc]:sw t6, 568(a1)
Current Store : [0x800025e0] : sw a4, 572(a1) -- Store: [0x800065fc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x004030 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000060 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002628]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000262c]:csrrs a4, fcsr, zero
	-[0x80002630]:sw t6, 576(a1)
Current Store : [0x80002634] : sw a4, 580(a1) -- Store: [0x80006604]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x008028 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000050 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000267c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002680]:csrrs a4, fcsr, zero
	-[0x80002684]:sw t6, 584(a1)
Current Store : [0x80002688] : sw a4, 588(a1) -- Store: [0x8000660c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x010001 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800026d0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800026d4]:csrrs a4, fcsr, zero
	-[0x800026d8]:sw t6, 592(a1)
Current Store : [0x800026dc] : sw a4, 596(a1) -- Store: [0x80006614]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x020006 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00000c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002724]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002728]:csrrs a4, fcsr, zero
	-[0x8000272c]:sw t6, 600(a1)
Current Store : [0x80002730] : sw a4, 604(a1) -- Store: [0x8000661c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x04002c and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000058 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002778]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000277c]:csrrs a4, fcsr, zero
	-[0x80002780]:sw t6, 608(a1)
Current Store : [0x80002784] : sw a4, 612(a1) -- Store: [0x80006624]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x080020 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800027cc]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800027d0]:csrrs a4, fcsr, zero
	-[0x800027d4]:sw t6, 616(a1)
Current Store : [0x800027d8] : sw a4, 620(a1) -- Store: [0x8000662c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x10002a and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000054 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002820]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002824]:csrrs a4, fcsr, zero
	-[0x80002828]:sw t6, 624(a1)
Current Store : [0x8000282c] : sw a4, 628(a1) -- Store: [0x80006634]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x200004 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000008 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002874]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002878]:csrrs a4, fcsr, zero
	-[0x8000287c]:sw t6, 632(a1)
Current Store : [0x80002880] : sw a4, 636(a1) -- Store: [0x8000663c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7fff48 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00005a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800028c8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800028cc]:csrrs a4, fcsr, zero
	-[0x800028d0]:sw t6, 640(a1)
Current Store : [0x800028d4] : sw a4, 644(a1) -- Store: [0x80006644]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7fffd2 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000014 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000291c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002920]:csrrs a4, fcsr, zero
	-[0x80002924]:sw t6, 648(a1)
Current Store : [0x80002928] : sw a4, 652(a1) -- Store: [0x8000664c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7fff66 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000048 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002970]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002974]:csrrs a4, fcsr, zero
	-[0x80002978]:sw t6, 656(a1)
Current Store : [0x8000297c] : sw a4, 660(a1) -- Store: [0x80006654]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7fffaa and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000022 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800029c4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800029c8]:csrrs a4, fcsr, zero
	-[0x800029cc]:sw t6, 664(a1)
Current Store : [0x800029d0] : sw a4, 668(a1) -- Store: [0x8000665c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7fff50 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000047 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002a18]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002a1c]:csrrs a4, fcsr, zero
	-[0x80002a20]:sw t6, 672(a1)
Current Store : [0x80002a24] : sw a4, 676(a1) -- Store: [0x80006664]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7fff7c and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000021 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002a6c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002a70]:csrrs a4, fcsr, zero
	-[0x80002a74]:sw t6, 680(a1)
Current Store : [0x80002a78] : sw a4, 684(a1) -- Store: [0x8000666c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7ffef8 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000043 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002ac0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002ac4]:csrrs a4, fcsr, zero
	-[0x80002ac8]:sw t6, 688(a1)
Current Store : [0x80002acc] : sw a4, 692(a1) -- Store: [0x80006674]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7ffec0 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00001f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002b14]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002b18]:csrrs a4, fcsr, zero
	-[0x80002b1c]:sw t6, 696(a1)
Current Store : [0x80002b20] : sw a4, 700(a1) -- Store: [0x8000667c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7ffd88 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00003b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002b68]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002b6c]:csrrs a4, fcsr, zero
	-[0x80002b70]:sw t6, 704(a1)
Current Store : [0x80002b74] : sw a4, 708(a1) -- Store: [0x80006684]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7ffbd0 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000017 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002bbc]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002bc0]:csrrs a4, fcsr, zero
	-[0x80002bc4]:sw t6, 712(a1)
Current Store : [0x80002bc8] : sw a4, 716(a1) -- Store: [0x8000668c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7ff746 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00005c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002c10]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002c14]:csrrs a4, fcsr, zero
	-[0x80002c18]:sw t6, 720(a1)
Current Store : [0x80002c1c] : sw a4, 724(a1) -- Store: [0x80006694]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7fef56 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000054 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002c64]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002c68]:csrrs a4, fcsr, zero
	-[0x80002c6c]:sw t6, 728(a1)
Current Store : [0x80002c70] : sw a4, 732(a1) -- Store: [0x8000669c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7fdfe8 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00000b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002cb8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002cbc]:csrrs a4, fcsr, zero
	-[0x80002cc0]:sw t6, 736(a1)
Current Store : [0x80002cc4] : sw a4, 740(a1) -- Store: [0x800066a4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7fbfe6 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00000c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002d0c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002d10]:csrrs a4, fcsr, zero
	-[0x80002d14]:sw t6, 744(a1)
Current Store : [0x80002d18] : sw a4, 748(a1) -- Store: [0x800066ac]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7f7f7c and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000041 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002d60]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002d64]:csrrs a4, fcsr, zero
	-[0x80002d68]:sw t6, 752(a1)
Current Store : [0x80002d6c] : sw a4, 756(a1) -- Store: [0x800066b4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7effc0 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00001f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002db4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002db8]:csrrs a4, fcsr, zero
	-[0x80002dbc]:sw t6, 760(a1)
Current Store : [0x80002dc0] : sw a4, 764(a1) -- Store: [0x800066bc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7dff86 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00003c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002e08]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002e0c]:csrrs a4, fcsr, zero
	-[0x80002e10]:sw t6, 768(a1)
Current Store : [0x80002e14] : sw a4, 772(a1) -- Store: [0x800066c4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7bffaa and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00002a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002e5c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002e60]:csrrs a4, fcsr, zero
	-[0x80002e64]:sw t6, 776(a1)
Current Store : [0x80002e68] : sw a4, 780(a1) -- Store: [0x800066cc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x77ff76 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000044 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002eb0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002eb4]:csrrs a4, fcsr, zero
	-[0x80002eb8]:sw t6, 784(a1)
Current Store : [0x80002ebc] : sw a4, 788(a1) -- Store: [0x800066d4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x6fff72 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000046 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002f04]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002f08]:csrrs a4, fcsr, zero
	-[0x80002f0c]:sw t6, 792(a1)
Current Store : [0x80002f10] : sw a4, 796(a1) -- Store: [0x800066dc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x5ffff0 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000007 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002f58]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002f5c]:csrrs a4, fcsr, zero
	-[0x80002f60]:sw t6, 800(a1)
Current Store : [0x80002f64] : sw a4, 804(a1) -- Store: [0x800066e4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x3fff90 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000037 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002fac]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80002fb0]:csrrs a4, fcsr, zero
	-[0x80002fb4]:sw t6, 808(a1)
Current Store : [0x80002fb8] : sw a4, 812(a1) -- Store: [0x800066ec]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x7ffe7c and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000060 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003000]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80003004]:csrrs a4, fcsr, zero
	-[0x80003008]:sw t6, 816(a1)
Current Store : [0x8000300c] : sw a4, 820(a1) -- Store: [0x800066f4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x400004 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00000b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003054]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80003058]:csrrs a4, fcsr, zero
	-[0x8000305c]:sw t6, 824(a1)
Current Store : [0x80003060] : sw a4, 828(a1) -- Store: [0x800066fc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x40000c and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00001c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800030a8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800030ac]:csrrs a4, fcsr, zero
	-[0x800030b0]:sw t6, 832(a1)
Current Store : [0x800030b4] : sw a4, 836(a1) -- Store: [0x80006704]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x40002a and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00005a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800030fc]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80003100]:csrrs a4, fcsr, zero
	-[0x80003104]:sw t6, 840(a1)
Current Store : [0x80003108] : sw a4, 844(a1) -- Store: [0x8000670c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x400012 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00002e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003150]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80003154]:csrrs a4, fcsr, zero
	-[0x80003158]:sw t6, 848(a1)
Current Store : [0x8000315c] : sw a4, 852(a1) -- Store: [0x80006714]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x40000a and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000026 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800031a4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800031a8]:csrrs a4, fcsr, zero
	-[0x800031ac]:sw t6, 856(a1)
Current Store : [0x800031b0] : sw a4, 860(a1) -- Store: [0x8000671c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x400017 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00004f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800031f8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800031fc]:csrrs a4, fcsr, zero
	-[0x80003200]:sw t6, 864(a1)
Current Store : [0x80003204] : sw a4, 868(a1) -- Store: [0x80006724]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3ffff8 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000032 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000324c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80003250]:csrrs a4, fcsr, zero
	-[0x80003254]:sw t6, 872(a1)
Current Store : [0x80003258] : sw a4, 876(a1) -- Store: [0x8000672c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3fffc8 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000012 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800032a0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800032a4]:csrrs a4, fcsr, zero
	-[0x800032a8]:sw t6, 880(a1)
Current Store : [0x800032ac] : sw a4, 884(a1) -- Store: [0x80006734]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3fffa9 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000053 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800032f4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800032f8]:csrrs a4, fcsr, zero
	-[0x800032fc]:sw t6, 888(a1)
Current Store : [0x80003300] : sw a4, 892(a1) -- Store: [0x8000673c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3fff28 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000051 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003348]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000334c]:csrrs a4, fcsr, zero
	-[0x80003350]:sw t6, 896(a1)
Current Store : [0x80003354] : sw a4, 900(a1) -- Store: [0x80006744]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3ffe17 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00002f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000339c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800033a0]:csrrs a4, fcsr, zero
	-[0x800033a4]:sw t6, 904(a1)
Current Store : [0x800033a8] : sw a4, 908(a1) -- Store: [0x8000674c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3ffc26 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00004c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800033f0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800033f4]:csrrs a4, fcsr, zero
	-[0x800033f8]:sw t6, 912(a1)
Current Store : [0x800033fc] : sw a4, 916(a1) -- Store: [0x80006754]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3ff82f and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00005f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003444]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80003448]:csrrs a4, fcsr, zero
	-[0x8000344c]:sw t6, 920(a1)
Current Store : [0x80003450] : sw a4, 924(a1) -- Store: [0x8000675c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3ff019 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000033 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003498]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000349c]:csrrs a4, fcsr, zero
	-[0x800034a0]:sw t6, 928(a1)
Current Store : [0x800034a4] : sw a4, 932(a1) -- Store: [0x80006764]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3fe006 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00000d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800034ec]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800034f0]:csrrs a4, fcsr, zero
	-[0x800034f4]:sw t6, 936(a1)
Current Store : [0x800034f8] : sw a4, 940(a1) -- Store: [0x8000676c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3fc002 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000005 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003540]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80003544]:csrrs a4, fcsr, zero
	-[0x80003548]:sw t6, 944(a1)
Current Store : [0x8000354c] : sw a4, 948(a1) -- Store: [0x80006774]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3f800c and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00001a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003594]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80003598]:csrrs a4, fcsr, zero
	-[0x8000359c]:sw t6, 952(a1)
Current Store : [0x800035a0] : sw a4, 956(a1) -- Store: [0x8000677c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3f0002 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000006 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800035e8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800035ec]:csrrs a4, fcsr, zero
	-[0x800035f0]:sw t6, 960(a1)
Current Store : [0x800035f4] : sw a4, 964(a1) -- Store: [0x80006784]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3e001e and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00003e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000363c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80003640]:csrrs a4, fcsr, zero
	-[0x80003644]:sw t6, 968(a1)
Current Store : [0x80003648] : sw a4, 972(a1) -- Store: [0x8000678c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3c0000 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003690]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80003694]:csrrs a4, fcsr, zero
	-[0x80003698]:sw t6, 976(a1)
Current Store : [0x8000369c] : sw a4, 980(a1) -- Store: [0x80006794]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x38001e and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00003d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800036e4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800036e8]:csrrs a4, fcsr, zero
	-[0x800036ec]:sw t6, 984(a1)
Current Store : [0x800036f0] : sw a4, 988(a1) -- Store: [0x8000679c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x300028 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000050 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003738]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000373c]:csrrs a4, fcsr, zero
	-[0x80003740]:sw t6, 992(a1)
Current Store : [0x80003744] : sw a4, 996(a1) -- Store: [0x800067a4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x200014 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00002a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000378c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80003790]:csrrs a4, fcsr, zero
	-[0x80003794]:sw t6, 1000(a1)
Current Store : [0x80003798] : sw a4, 1004(a1) -- Store: [0x800067ac]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x86 and fm2 == 0x03b8d9 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800037e0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800037e4]:csrrs a4, fcsr, zero
	-[0x800037e8]:sw t6, 1008(a1)
Current Store : [0x800037ec] : sw a4, 1012(a1) -- Store: [0x800067b4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x85 and fm2 == 0x1cf208 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003834]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80003838]:csrrs a4, fcsr, zero
	-[0x8000383c]:sw t6, 1016(a1)
Current Store : [0x80003840] : sw a4, 1020(a1) -- Store: [0x800067bc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x85 and fm2 == 0x54ff53 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000054 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003890]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80003894]:csrrs a4, fcsr, zero
	-[0x80003898]:sw t6, 0(a1)
Current Store : [0x8000389c] : sw a4, 4(a1) -- Store: [0x800067c4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x81 and fm2 == 0x60352f and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000015 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800038e4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800038e8]:csrrs a4, fcsr, zero
	-[0x800038ec]:sw t6, 8(a1)
Current Store : [0x800038f0] : sw a4, 12(a1) -- Store: [0x800067cc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x83 and fm2 == 0x335dc0 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000030 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003938]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000393c]:csrrs a4, fcsr, zero
	-[0x80003940]:sw t6, 16(a1)
Current Store : [0x80003944] : sw a4, 20(a1) -- Store: [0x800067d4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x84 and fm2 == 0x335dc0 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000020 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000398c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80003990]:csrrs a4, fcsr, zero
	-[0x80003994]:sw t6, 24(a1)
Current Store : [0x80003998] : sw a4, 28(a1) -- Store: [0x800067dc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x228cf6 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000046 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800039e0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800039e4]:csrrs a4, fcsr, zero
	-[0x800039e8]:sw t6, 32(a1)
Current Store : [0x800039ec] : sw a4, 36(a1) -- Store: [0x800067e4]:0x00000000




Last Coverpoint : ['mnemonic : fmadd.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs3 : x28', 'rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x86 and fm2 == 0x7f094c and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003a34]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80003a38]:csrrs a4, fcsr, zero
	-[0x80003a3c]:sw t6, 40(a1)
Current Store : [0x80003a40] : sw a4, 44(a1) -- Store: [0x800067ec]:0x00000000




Last Coverpoint : ['mnemonic : fmadd.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs3 : x28', 'rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x88 and fm2 == 0x32aa62 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003a88]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80003a8c]:csrrs a4, fcsr, zero
	-[0x80003a90]:sw t6, 48(a1)
Current Store : [0x80003a94] : sw a4, 52(a1) -- Store: [0x800067f4]:0x00000000




Last Coverpoint : ['mnemonic : fmadd.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs3 : x28', 'rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x89 and fm2 == 0x2a1525 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000035 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003adc]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80003ae0]:csrrs a4, fcsr, zero
	-[0x80003ae4]:sw t6, 56(a1)
Current Store : [0x80003ae8] : sw a4, 60(a1) -- Store: [0x800067fc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8a and fm2 == 0x3143a6 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000018 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003b30]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80003b34]:csrrs a4, fcsr, zero
	-[0x80003b38]:sw t6, 64(a1)
Current Store : [0x80003b3c] : sw a4, 68(a1) -- Store: [0x80006804]:0x00000000




Last Coverpoint : ['mnemonic : fmadd.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs3 : x28', 'rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x87 and fm2 == 0x442e89 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000018 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003b84]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80003b88]:csrrs a4, fcsr, zero
	-[0x80003b8c]:sw t6, 72(a1)
Current Store : [0x80003b90] : sw a4, 76(a1) -- Store: [0x8000680c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8a and fm2 == 0x33a103 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000003 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003bd8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80003bdc]:csrrs a4, fcsr, zero
	-[0x80003be0]:sw t6, 80(a1)
Current Store : [0x80003be4] : sw a4, 84(a1) -- Store: [0x80006814]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8d and fm2 == 0x33e178 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003c2c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80003c30]:csrrs a4, fcsr, zero
	-[0x80003c34]:sw t6, 88(a1)
Current Store : [0x80003c38] : sw a4, 92(a1) -- Store: [0x8000681c]:0x00000000




Last Coverpoint : ['mnemonic : fmadd.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs3 : x28', 'rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x90 and fm2 == 0x337da1 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003c80]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80003c84]:csrrs a4, fcsr, zero
	-[0x80003c88]:sw t6, 96(a1)
Current Store : [0x80003c8c] : sw a4, 100(a1) -- Store: [0x80006824]:0x00000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                                                                                      coverpoints                                                                                                                                                                      |                                                         code                                                          |
|---:|--------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------|
|   1|[0x80005f10]<br>0x0000005F|- mnemonic : fmadd.s<br> - rs1 : x30<br> - rs2 : x30<br> - rd : x31<br> - rs3 : x29<br> - rs1 == rs2 != rs3 and rs1 == rs2 != rd and rd != rs3<br>                                                                                                                                                                                                     |[0x80000128]:fmadd.s t6, t5, t5, t4, dyn<br> [0x8000012c]:csrrs tp, fcsr, zero<br> [0x80000130]:sw t6, 0(ra)<br>       |
|   2|[0x80005f18]<br>0x00000059|- rs1 : x31<br> - rs2 : x29<br> - rd : x30<br> - rs3 : x28<br> - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x85 and fm2 == 0x73d370 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000059 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x8000014c]:fmadd.s t5, t6, t4, t3, dyn<br> [0x80000150]:csrrs tp, fcsr, zero<br> [0x80000154]:sw t5, 8(ra)<br>       |
|   3|[0x80005f20]<br>0x0000003C|- rs1 : x28<br> - rs2 : x28<br> - rd : x28<br> - rs3 : x31<br> - rs1 == rs2 == rd != rs3<br>                                                                                                                                                                                                                                                           |[0x80000170]:fmadd.s t3, t3, t3, t6, dyn<br> [0x80000174]:csrrs tp, fcsr, zero<br> [0x80000178]:sw t3, 16(ra)<br>      |
|   4|[0x80005f28]<br>0xC2D4FF53|- rs1 : x29<br> - rs2 : x27<br> - rd : x27<br> - rs3 : x27<br> - rd == rs2 == rs3 != rs1<br>                                                                                                                                                                                                                                                           |[0x80000194]:fmadd.s s11, t4, s11, s11, dyn<br> [0x80000198]:csrrs tp, fcsr, zero<br> [0x8000019c]:sw s11, 24(ra)<br>  |
|   5|[0x80005f30]<br>0xC0E0352F|- rs1 : x27<br> - rs2 : x26<br> - rd : x29<br> - rs3 : x26<br> - rs2 == rs3 != rs1 and rs2 == rs3 != rd and rd != rs1<br>                                                                                                                                                                                                                              |[0x800001b8]:fmadd.s t4, s11, s10, s10, dyn<br> [0x800001bc]:csrrs tp, fcsr, zero<br> [0x800001c0]:sw t4, 32(ra)<br>   |
|   6|[0x80005f38]<br>0x00000000|- rs1 : x25<br> - rs2 : x25<br> - rd : x26<br> - rs3 : x25<br> - rs1 == rs2 == rs3 != rd<br>                                                                                                                                                                                                                                                           |[0x800001dc]:fmadd.s s10, s9, s9, s9, dyn<br> [0x800001e0]:csrrs tp, fcsr, zero<br> [0x800001e4]:sw s10, 40(ra)<br>    |
|   7|[0x80005f40]<br>0x00000000|- rs1 : x24<br> - rs2 : x24<br> - rd : x24<br> - rs3 : x24<br> - rs1 == rs2 == rs3 == rd<br>                                                                                                                                                                                                                                                           |[0x80000200]:fmadd.s s8, s8, s8, s8, dyn<br> [0x80000204]:csrrs tp, fcsr, zero<br> [0x80000208]:sw s8, 48(ra)<br>      |
|   8|[0x80005f48]<br>0x00000000|- rs1 : x23<br> - rs2 : x31<br> - rd : x25<br> - rs3 : x23<br> - rs1 == rs3 != rs2 and rs1 == rs3 != rd and rd != rs2<br>                                                                                                                                                                                                                              |[0x80000224]:fmadd.s s9, s7, t6, s7, dyn<br> [0x80000228]:csrrs tp, fcsr, zero<br> [0x8000022c]:sw s9, 56(ra)<br>      |
|   9|[0x80005f50]<br>0x0000004A|- rs1 : x22<br> - rs2 : x23<br> - rd : x22<br> - rs3 : x30<br> - rs1 == rd != rs2 and rs1 == rd != rs3 and rs3 != rs2<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x86 and fm2 == 0x7f094c and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                               |[0x80000248]:fmadd.s s6, s6, s7, t5, dyn<br> [0x8000024c]:csrrs tp, fcsr, zero<br> [0x80000250]:sw s6, 64(ra)<br>      |
|  10|[0x80005f58]<br>0x00000002|- rs1 : x26<br> - rs2 : x21<br> - rd : x21<br> - rs3 : x22<br> - rs2 == rd != rs1 and rs2 == rd != rs3 and rs3 != rs1<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x88 and fm2 == 0x32aa62 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                               |[0x8000026c]:fmadd.s s5, s10, s5, s6, dyn<br> [0x80000270]:csrrs tp, fcsr, zero<br> [0x80000274]:sw s5, 72(ra)<br>     |
|  11|[0x80005f60]<br>0x00000035|- rs1 : x21<br> - rs2 : x22<br> - rd : x20<br> - rs3 : x20<br> - rs3 == rd != rs1 and rs3 == rd != rs2 and rs2 != rs1<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x89 and fm2 == 0x2a1525 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000035 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                               |[0x80000290]:fmadd.s s4, s5, s6, s4, dyn<br> [0x80000294]:csrrs tp, fcsr, zero<br> [0x80000298]:sw s4, 80(ra)<br>      |
|  12|[0x80005f68]<br>0x00000000|- rs1 : x19<br> - rs2 : x20<br> - rd : x19<br> - rs3 : x19<br> - rs1 == rd == rs3 != rs2<br>                                                                                                                                                                                                                                                           |[0x800002b4]:fmadd.s s3, s3, s4, s3, dyn<br> [0x800002b8]:csrrs tp, fcsr, zero<br> [0x800002bc]:sw s3, 88(ra)<br>      |
|  13|[0x80005f70]<br>0x00000015|- rs1 : x20<br> - rs2 : x19<br> - rd : x23<br> - rs3 : x21<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8b and fm2 == 0x327254 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000015 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                          |[0x800002d8]:fmadd.s s7, s4, s3, s5, dyn<br> [0x800002dc]:csrrs tp, fcsr, zero<br> [0x800002e0]:sw s7, 96(ra)<br>      |
|  14|[0x80005f78]<br>0x00000012|- rs1 : x17<br> - rs2 : x16<br> - rd : x18<br> - rs3 : x15<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8c and fm2 == 0x32f8db and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000012 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                          |[0x800002fc]:fmadd.s s2, a7, a6, a5, dyn<br> [0x80000300]:csrrs tp, fcsr, zero<br> [0x80000304]:sw s2, 104(ra)<br>     |
|  15|[0x80005f80]<br>0x00000011|- rs1 : x15<br> - rs2 : x18<br> - rd : x17<br> - rs3 : x16<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8d and fm2 == 0x332e1b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000011 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                          |[0x80000320]:fmadd.s a7, a5, s2, a6, dyn<br> [0x80000324]:csrrs tp, fcsr, zero<br> [0x80000328]:sw a7, 112(ra)<br>     |
|  16|[0x80005f88]<br>0x00000001|- rs1 : x18<br> - rs2 : x15<br> - rd : x16<br> - rs3 : x17<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8e and fm2 == 0x335c59 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                          |[0x80000344]:fmadd.s a6, s2, a5, a7, dyn<br> [0x80000348]:csrrs tp, fcsr, zero<br> [0x8000034c]:sw a6, 120(ra)<br>     |
|  17|[0x80005f90]<br>0x0000001B|- rs1 : x16<br> - rs2 : x17<br> - rd : x15<br> - rs3 : x18<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8f and fm2 == 0x334ad5 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                          |[0x80000368]:fmadd.s a5, a6, a7, s2, dyn<br> [0x8000036c]:csrrs tp, fcsr, zero<br> [0x80000370]:sw a5, 128(ra)<br>     |
|  18|[0x80005f98]<br>0x00000016|- rs1 : x13<br> - rs2 : x12<br> - rd : x14<br> - rs3 : x11<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x90 and fm2 == 0x33560a and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000016 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                          |[0x8000038c]:fmadd.s a4, a3, a2, a1, dyn<br> [0x80000390]:csrrs tp, fcsr, zero<br> [0x80000394]:sw a4, 136(ra)<br>     |
|  19|[0x80005fa0]<br>0x00000026|- rs1 : x11<br> - rs2 : x14<br> - rd : x13<br> - rs3 : x12<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x91 and fm2 == 0x335718 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000026 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                          |[0x800003b0]:fmadd.s a3, a1, a4, a2, dyn<br> [0x800003b4]:csrrs tp, fcsr, zero<br> [0x800003b8]:sw a3, 144(ra)<br>     |
|  20|[0x80005fa8]<br>0x0000001A|- rs1 : x14<br> - rs2 : x11<br> - rd : x12<br> - rs3 : x13<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x92 and fm2 == 0x335b79 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                          |[0x800003d4]:fmadd.s a2, a4, a1, a3, dyn<br> [0x800003d8]:csrrs tp, fcsr, zero<br> [0x800003dc]:sw a2, 152(ra)<br>     |
|  21|[0x80005fb0]<br>0x00000018|- rs1 : x12<br> - rs2 : x13<br> - rd : x11<br> - rs3 : x14<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x93 and fm2 == 0x335cb2 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000018 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                          |[0x800003f8]:fmadd.s a1, a2, a3, a4, dyn<br> [0x800003fc]:csrrs tp, fcsr, zero<br> [0x80000400]:sw a1, 160(ra)<br>     |
|  22|[0x80005fb8]<br>0x00000032|- rs1 : x9<br> - rs2 : x8<br> - rd : x10<br> - rs3 : x7<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x94 and fm2 == 0x335ca7 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000032 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                             |[0x8000041c]:fmadd.s a0, s1, fp, t2, dyn<br> [0x80000420]:csrrs tp, fcsr, zero<br> [0x80000424]:sw a0, 168(ra)<br>     |
|  23|[0x80005fc0]<br>0x00000003|- rs1 : x7<br> - rs2 : x10<br> - rd : x9<br> - rs3 : x8<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x95 and fm2 == 0x335db7 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000003 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                             |[0x80000450]:fmadd.s s1, t2, a0, fp, dyn<br> [0x80000454]:csrrs a4, fcsr, zero<br> [0x80000458]:sw s1, 0(a1)<br>       |
|  24|[0x80005fc8]<br>0x00000036|- rs1 : x10<br> - rs2 : x7<br> - rd : x8<br> - rs3 : x9<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x85 and fm2 == 0x1a2491 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000036 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                             |[0x80000474]:fmadd.s fp, a0, t2, s1, dyn<br> [0x80000478]:csrrs a4, fcsr, zero<br> [0x8000047c]:sw fp, 8(a1)<br>       |
|  25|[0x80005fd0]<br>0x00000013|- rs1 : x8<br> - rs2 : x9<br> - rd : x7<br> - rs3 : x10<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x83 and fm2 == 0x6b6b0b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000013 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                             |[0x80000498]:fmadd.s t2, fp, s1, a0, dyn<br> [0x8000049c]:csrrs a4, fcsr, zero<br> [0x800004a0]:sw t2, 16(a1)<br>      |
|  26|[0x80005fd8]<br>0x00000009|- rs1 : x5<br> - rs2 : x4<br> - rd : x6<br> - rs3 : x3<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x83 and fm2 == 0x11bc2c and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000009 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800004bc]:fmadd.s t1, t0, tp, gp, dyn<br> [0x800004c0]:csrrs a4, fcsr, zero<br> [0x800004c4]:sw t1, 24(a1)<br>      |
|  27|[0x80005fe0]<br>0x00000027|- rs1 : x3<br> - rs2 : x6<br> - rd : x5<br> - rs3 : x4<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x85 and fm2 == 0x03b8d9 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000027 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800004e0]:fmadd.s t0, gp, t1, tp, dyn<br> [0x800004e4]:csrrs a4, fcsr, zero<br> [0x800004e8]:sw t0, 32(a1)<br>      |
|  28|[0x80005fe8]<br>0x0000004D|- rs1 : x6<br> - rs2 : x3<br> - rd : x4<br> - rs3 : x5<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x86 and fm2 == 0x02521d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000504]:fmadd.s tp, t1, gp, t0, dyn<br> [0x80000508]:csrrs a4, fcsr, zero<br> [0x8000050c]:sw tp, 40(a1)<br>      |
|  29|[0x80005ff0]<br>0x00000009|- rs1 : x4<br> - rs2 : x5<br> - rd : x3<br> - rs3 : x6<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x84 and fm2 == 0x65d01d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000009 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000528]:fmadd.s gp, tp, t0, t1, dyn<br> [0x8000052c]:csrrs a4, fcsr, zero<br> [0x80000530]:sw gp, 48(a1)<br>      |
|  30|[0x80005ff8]<br>0x0000002E|- rs1 : x2<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x86 and fm2 == 0x1a2491 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                          |[0x8000054c]:fmadd.s t6, sp, t5, t4, dyn<br> [0x80000550]:csrrs a4, fcsr, zero<br> [0x80000554]:sw t6, 56(a1)<br>      |
|  31|[0x80006000]<br>0x0000003E|- rs1 : x1<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x87 and fm2 == 0x051f94 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                          |[0x80000570]:fmadd.s t6, ra, t5, t4, dyn<br> [0x80000574]:csrrs a4, fcsr, zero<br> [0x80000578]:sw t6, 64(a1)<br>      |
|  32|[0x80006008]<br>0x00000018|- rs1 : x0<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x87 and fm2 == 0x442e89 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000018 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                          |[0x80000594]:fmadd.s t6, zero, t5, t4, dyn<br> [0x80000598]:csrrs a4, fcsr, zero<br> [0x8000059c]:sw t6, 72(a1)<br>    |
|  33|[0x80006010]<br>0x0000003D|- rs2 : x2<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x88 and fm2 == 0x48bc6b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                          |[0x800005b8]:fmadd.s t6, t5, sp, t4, dyn<br> [0x800005bc]:csrrs a4, fcsr, zero<br> [0x800005c0]:sw t6, 80(a1)<br>      |
|  34|[0x80006018]<br>0x00000008|- rs2 : x1<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x89 and fm2 == 0x34c47b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000008 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                          |[0x800005dc]:fmadd.s t6, t5, ra, t4, dyn<br> [0x800005e0]:csrrs a4, fcsr, zero<br> [0x800005e4]:sw t6, 88(a1)<br>      |
|  35|[0x80006020]<br>0x00000003|- rs2 : x0<br>                                                                                                                                                                                                                                                                                                                                         |[0x80000600]:fmadd.s t6, t5, zero, t4, dyn<br> [0x80000604]:csrrs a4, fcsr, zero<br> [0x80000608]:sw t6, 96(a1)<br>    |
|  36|[0x80006028]<br>0x00000034|- rs3 : x2<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8b and fm2 == 0x35a4b0 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000034 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                          |[0x80000624]:fmadd.s t6, t5, t4, sp, dyn<br> [0x80000628]:csrrs a4, fcsr, zero<br> [0x8000062c]:sw t6, 104(a1)<br>     |
|  37|[0x80006030]<br>0x00000047|- rs3 : x1<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8c and fm2 == 0x34ebb8 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000047 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                          |[0x80000648]:fmadd.s t6, t5, t4, ra, dyn<br> [0x8000064c]:csrrs a4, fcsr, zero<br> [0x80000650]:sw t6, 112(a1)<br>     |
|  38|[0x80006038]<br>0x00000000|- rs3 : x0<br>                                                                                                                                                                                                                                                                                                                                         |[0x8000066c]:fmadd.s t6, t5, t4, zero, dyn<br> [0x80000670]:csrrs a4, fcsr, zero<br> [0x80000674]:sw t6, 120(a1)<br>   |
|  39|[0x80006040]<br>0x0000004B|- rd : x2<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8e and fm2 == 0x33c6d8 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                           |[0x80000690]:fmadd.s sp, t6, t5, t4, dyn<br> [0x80000694]:csrrs a4, fcsr, zero<br> [0x80000698]:sw sp, 128(a1)<br>     |
|  40|[0x80006048]<br>0x0000003A|- rd : x1<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8f and fm2 == 0x338663 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                           |[0x800006b4]:fmadd.s ra, t6, t5, t4, dyn<br> [0x800006b8]:csrrs a4, fcsr, zero<br> [0x800006bc]:sw ra, 136(a1)<br>     |
|  41|[0x80006050]<br>0x00000000|- rd : x0<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x90 and fm2 == 0x337da1 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                           |[0x800006d8]:fmadd.s zero, t6, t5, t4, dyn<br> [0x800006dc]:csrrs a4, fcsr, zero<br> [0x800006e0]:sw zero, 144(a1)<br> |
|  42|[0x80006058]<br>0x00000050|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x91 and fm2 == 0x336bc3 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000050 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800006fc]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000700]:csrrs a4, fcsr, zero<br> [0x80000704]:sw t6, 152(a1)<br>     |
|  43|[0x80006060]<br>0x00000010|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x92 and fm2 == 0x335f26 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000010 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000720]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000724]:csrrs a4, fcsr, zero<br> [0x80000728]:sw t6, 160(a1)<br>     |
|  44|[0x80006068]<br>0x0000003C|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x93 and fm2 == 0x336060 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000744]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000748]:csrrs a4, fcsr, zero<br> [0x8000074c]:sw t6, 168(a1)<br>     |
|  45|[0x80006070]<br>0x00000042|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x94 and fm2 == 0x335f31 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000042 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000768]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000076c]:csrrs a4, fcsr, zero<br> [0x80000770]:sw t6, 176(a1)<br>     |
|  46|[0x80006078]<br>0x00000044|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x95 and fm2 == 0x335e7e and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000044 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000078c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000790]:csrrs a4, fcsr, zero<br> [0x80000794]:sw t6, 184(a1)<br>     |
|  47|[0x80006080]<br>0x3F800001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6d and fm2 == 0x6c0000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00003c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800007b0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800007b4]:csrrs a4, fcsr, zero<br> [0x800007b8]:sw t6, 192(a1)<br>     |
|  48|[0x80006088]<br>0x3F800002|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6e and fm2 == 0x140000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00004c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800007d4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800007d8]:csrrs a4, fcsr, zero<br> [0x800007dc]:sw t6, 200(a1)<br>     |
|  49|[0x80006090]<br>0x3F800004|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6d and fm2 == 0x080000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000026 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800007f8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800007fc]:csrrs a4, fcsr, zero<br> [0x80000800]:sw t6, 208(a1)<br>     |
|  50|[0x80006098]<br>0x3F800008|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6d and fm2 == 0x400000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000038 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000081c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000820]:csrrs a4, fcsr, zero<br> [0x80000824]:sw t6, 216(a1)<br>     |
|  51|[0x800060a0]<br>0x3F800010|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6b and fm2 == 0x300000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00001b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000840]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000844]:csrrs a4, fcsr, zero<br> [0x80000848]:sw t6, 224(a1)<br>     |
|  52|[0x800060a8]<br>0x3F800020|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6d and fm2 == 0x080000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000042 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000864]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000868]:csrrs a4, fcsr, zero<br> [0x8000086c]:sw t6, 232(a1)<br>     |
|  53|[0x800060b0]<br>0x3F800040|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6d and fm2 == 0x340000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000013 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000888]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000088c]:csrrs a4, fcsr, zero<br> [0x80000890]:sw t6, 240(a1)<br>     |
|  54|[0x800060b8]<br>0x3F800080|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6e and fm2 == 0x380000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000024 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800008ac]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800008b0]:csrrs a4, fcsr, zero<br> [0x800008b4]:sw t6, 248(a1)<br>     |
|  55|[0x800060c0]<br>0x3F800100|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6f and fm2 == 0x580000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000028 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800008d0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800008d4]:csrrs a4, fcsr, zero<br> [0x800008d8]:sw t6, 256(a1)<br>     |
|  56|[0x800060c8]<br>0x3F800200|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x70 and fm2 == 0x6c0000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000028 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800008f4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800008f8]:csrrs a4, fcsr, zero<br> [0x800008fc]:sw t6, 264(a1)<br>     |
|  57|[0x800060d0]<br>0x3F800400|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x71 and fm2 == 0x7d4000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00000b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000918]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000091c]:csrrs a4, fcsr, zero<br> [0x80000920]:sw t6, 272(a1)<br>     |
|  58|[0x800060d8]<br>0x3F800800|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x72 and fm2 == 0x746000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00005d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000093c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000940]:csrrs a4, fcsr, zero<br> [0x80000944]:sw t6, 280(a1)<br>     |
|  59|[0x800060e0]<br>0x3F801000|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x73 and fm2 == 0x7c2000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00003e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000960]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000964]:csrrs a4, fcsr, zero<br> [0x80000968]:sw t6, 288(a1)<br>     |
|  60|[0x800060e8]<br>0x3F802000|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x74 and fm2 == 0x7d1800 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00005d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000984]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000988]:csrrs a4, fcsr, zero<br> [0x8000098c]:sw t6, 296(a1)<br>     |
|  61|[0x800060f0]<br>0x3F804000|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x75 and fm2 == 0x7fd400 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00000b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800009a8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800009ac]:csrrs a4, fcsr, zero<br> [0x800009b0]:sw t6, 304(a1)<br>     |
|  62|[0x800060f8]<br>0x3F808000|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x76 and fm2 == 0x7ff600 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000005 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800009cc]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800009d0]:csrrs a4, fcsr, zero<br> [0x800009d4]:sw t6, 312(a1)<br>     |
|  63|[0x80006100]<br>0x3F810000|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x77 and fm2 == 0x7fa100 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00005f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800009f0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800009f4]:csrrs a4, fcsr, zero<br> [0x800009f8]:sw t6, 320(a1)<br>     |
|  64|[0x80006108]<br>0x3F820000|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x78 and fm2 == 0x7fef80 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000021 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000a14]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000a18]:csrrs a4, fcsr, zero<br> [0x80000a1c]:sw t6, 328(a1)<br>     |
|  65|[0x80006110]<br>0x3F840000|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x79 and fm2 == 0x7feb00 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000054 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000a38]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000a3c]:csrrs a4, fcsr, zero<br> [0x80000a40]:sw t6, 336(a1)<br>     |
|  66|[0x80006118]<br>0x3F880000|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7a and fm2 == 0x7ffda0 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000013 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000a5c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000a60]:csrrs a4, fcsr, zero<br> [0x80000a64]:sw t6, 344(a1)<br>     |
|  67|[0x80006120]<br>0x3F900000|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7b and fm2 == 0x7ffae0 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000052 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000a80]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000a84]:csrrs a4, fcsr, zero<br> [0x80000a88]:sw t6, 352(a1)<br>     |
|  68|[0x80006128]<br>0x3FA00000|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7c and fm2 == 0x7ffe00 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000040 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000aa4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000aa8]:csrrs a4, fcsr, zero<br> [0x80000aac]:sw t6, 360(a1)<br>     |
|  69|[0x80006130]<br>0x3FC00000|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x7fff94 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00001b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000ac8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000acc]:csrrs a4, fcsr, zero<br> [0x80000ad0]:sw t6, 368(a1)<br>     |
|  70|[0x80006138]<br>0xBF800002|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000030 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00005e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000aec]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000af0]:csrrs a4, fcsr, zero<br> [0x80000af4]:sw t6, 376(a1)<br>     |
|  71|[0x80006140]<br>0xBF800003|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x00002a and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000051 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000b10]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000b14]:csrrs a4, fcsr, zero<br> [0x80000b18]:sw t6, 384(a1)<br>     |
|  72|[0x80006148]<br>0xBF800004|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000009 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00000e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000b34]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000b38]:csrrs a4, fcsr, zero<br> [0x80000b3c]:sw t6, 392(a1)<br>     |
|  73|[0x80006150]<br>0xBF800008|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000020 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000038 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000b58]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000b5c]:csrrs a4, fcsr, zero<br> [0x80000b60]:sw t6, 400(a1)<br>     |
|  74|[0x80006158]<br>0xBF800010|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000012 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000014 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000b7c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000b80]:csrrs a4, fcsr, zero<br> [0x80000b84]:sw t6, 408(a1)<br>     |
|  75|[0x80006160]<br>0xBF800020|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x00002b and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000036 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000ba0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000ba4]:csrrs a4, fcsr, zero<br> [0x80000ba8]:sw t6, 416(a1)<br>     |
|  76|[0x80006168]<br>0xBF800041|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x00002a and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000013 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000bc4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000bc8]:csrrs a4, fcsr, zero<br> [0x80000bcc]:sw t6, 424(a1)<br>     |
|  77|[0x80006170]<br>0xBF800080|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000068 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000050 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000be8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000bec]:csrrs a4, fcsr, zero<br> [0x80000bf0]:sw t6, 432(a1)<br>     |
|  78|[0x80006178]<br>0xBF800101|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x0000a2 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000043 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000c0c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000c10]:csrrs a4, fcsr, zero<br> [0x80000c14]:sw t6, 440(a1)<br>     |
|  79|[0x80006180]<br>0xBF800201|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000120 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000c30]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000c34]:csrrs a4, fcsr, zero<br> [0x80000c38]:sw t6, 448(a1)<br>     |
|  80|[0x80006188]<br>0xBF800400|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x00021f and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00003e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000c54]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000c58]:csrrs a4, fcsr, zero<br> [0x80000c5c]:sw t6, 456(a1)<br>     |
|  81|[0x80006190]<br>0xBF800800|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000413 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000026 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000c78]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000c7c]:csrrs a4, fcsr, zero<br> [0x80000c80]:sw t6, 464(a1)<br>     |
|  82|[0x80006198]<br>0xBF801000|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x00081a and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000034 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000c9c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000ca0]:csrrs a4, fcsr, zero<br> [0x80000ca4]:sw t6, 472(a1)<br>     |
|  83|[0x800061a0]<br>0xBF802001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x001008 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00000f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000cc0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000cc4]:csrrs a4, fcsr, zero<br> [0x80000cc8]:sw t6, 480(a1)<br>     |
|  84|[0x800061a8]<br>0xBF803FFF|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x002022 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000045 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000ce4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000ce8]:csrrs a4, fcsr, zero<br> [0x80000cec]:sw t6, 488(a1)<br>     |
|  85|[0x800061b0]<br>0xBF807FFF|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x004028 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000051 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000d08]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000d0c]:csrrs a4, fcsr, zero<br> [0x80000d10]:sw t6, 496(a1)<br>     |
|  86|[0x800061b8]<br>0xBF810000|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x008016 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00002c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000d2c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000d30]:csrrs a4, fcsr, zero<br> [0x80000d34]:sw t6, 504(a1)<br>     |
|  87|[0x800061c0]<br>0xBF820000|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x010006 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00000c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000d50]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000d54]:csrrs a4, fcsr, zero<br> [0x80000d58]:sw t6, 512(a1)<br>     |
|  88|[0x800061c8]<br>0xBF840001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x020012 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000023 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000d74]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000d78]:csrrs a4, fcsr, zero<br> [0x80000d7c]:sw t6, 520(a1)<br>     |
|  89|[0x800061d0]<br>0xBF87FFFF|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x040004 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000009 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000d98]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000d9c]:csrrs a4, fcsr, zero<br> [0x80000da0]:sw t6, 528(a1)<br>     |
|  90|[0x800061d8]<br>0xBF8FFFFF|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x08002c and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000059 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000dbc]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000dc0]:csrrs a4, fcsr, zero<br> [0x80000dc4]:sw t6, 536(a1)<br>     |
|  91|[0x800061e0]<br>0xBFA00000|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x100031 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000062 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000de0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000de4]:csrrs a4, fcsr, zero<br> [0x80000de8]:sw t6, 544(a1)<br>     |
|  92|[0x800061e8]<br>0xBFC00001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x200018 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00002f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000e04]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000e08]:csrrs a4, fcsr, zero<br> [0x80000e0c]:sw t6, 552(a1)<br>     |
|  93|[0x800061f0]<br>0x0000003F|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x85 and fm2 == 0x2e9049 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000e28]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000e2c]:csrrs a4, fcsr, zero<br> [0x80000e30]:sw t6, 560(a1)<br>     |
|  94|[0x800061f8]<br>0x00000063|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x86 and fm2 == 0x068650 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000063 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000e4c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000e50]:csrrs a4, fcsr, zero<br> [0x80000e54]:sw t6, 568(a1)<br>     |
|  95|[0x80006200]<br>0x0000002C|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x84 and fm2 == 0x5a9a41 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000e70]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000e74]:csrrs a4, fcsr, zero<br> [0x80000e78]:sw t6, 576(a1)<br>     |
|  96|[0x80006208]<br>0x0000004B|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x85 and fm2 == 0x38f8ad and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000e94]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000e98]:csrrs a4, fcsr, zero<br> [0x80000e9c]:sw t6, 584(a1)<br>     |
|  97|[0x80006210]<br>0x0000003D|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x84 and fm2 == 0x76a0e7 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000eb8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000ebc]:csrrs a4, fcsr, zero<br> [0x80000ec0]:sw t6, 592(a1)<br>     |
|  98|[0x80006218]<br>0x0000002A|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x82 and fm2 == 0x49c977 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000edc]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000ee0]:csrrs a4, fcsr, zero<br> [0x80000ee4]:sw t6, 600(a1)<br>     |
|  99|[0x80006220]<br>0x0000003C|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x60352f and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000f00]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000f04]:csrrs a4, fcsr, zero<br> [0x80000f08]:sw t6, 608(a1)<br>     |
| 100|[0x80006228]<br>0x00000041|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x335dc0 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000041 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000f24]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000f28]:csrrs a4, fcsr, zero<br> [0x80000f2c]:sw t6, 616(a1)<br>     |
| 101|[0x80006230]<br>0x0000002D|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x87 and fm2 == 0x1489a3 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000f48]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000f4c]:csrrs a4, fcsr, zero<br> [0x80000f50]:sw t6, 624(a1)<br>     |
| 102|[0x80006238]<br>0x00000053|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x88 and fm2 == 0x16a3bc and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000053 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000f6c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000f70]:csrrs a4, fcsr, zero<br> [0x80000f74]:sw t6, 632(a1)<br>     |
| 103|[0x80006240]<br>0x0000005F|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x89 and fm2 == 0x22e6a4 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000f90]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000f94]:csrrs a4, fcsr, zero<br> [0x80000f98]:sw t6, 640(a1)<br>     |
| 104|[0x80006248]<br>0x0000002D|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8a and fm2 == 0x2f833c and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000fb4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000fb8]:csrrs a4, fcsr, zero<br> [0x80000fbc]:sw t6, 648(a1)<br>     |
| 105|[0x80006250]<br>0x00000059|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8b and fm2 == 0x2f833c and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000059 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000fd8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000fdc]:csrrs a4, fcsr, zero<br> [0x80000fe0]:sw t6, 656(a1)<br>     |
| 106|[0x80006258]<br>0x0000002F|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8c and fm2 == 0x325be9 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000ffc]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001000]:csrrs a4, fcsr, zero<br> [0x80001004]:sw t6, 664(a1)<br>     |
| 107|[0x80006260]<br>0x00000043|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8d and fm2 == 0x32a4c7 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000043 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001020]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001024]:csrrs a4, fcsr, zero<br> [0x80001028]:sw t6, 672(a1)<br>     |
| 108|[0x80006268]<br>0x00000044|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8e and fm2 == 0x32ffdc and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000044 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001044]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001048]:csrrs a4, fcsr, zero<br> [0x8000104c]:sw t6, 680(a1)<br>     |
| 109|[0x80006270]<br>0x0000001A|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8f and fm2 == 0x334c3b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001068]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000106c]:csrrs a4, fcsr, zero<br> [0x80001070]:sw t6, 688(a1)<br>     |
| 110|[0x80006278]<br>0x0000003E|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x90 and fm2 == 0x334861 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000108c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001090]:csrrs a4, fcsr, zero<br> [0x80001094]:sw t6, 696(a1)<br>     |
| 111|[0x80006280]<br>0x00000059|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x91 and fm2 == 0x334e55 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000059 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800010b0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800010b4]:csrrs a4, fcsr, zero<br> [0x800010b8]:sw t6, 704(a1)<br>     |
| 112|[0x80006288]<br>0x0000005D|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x92 and fm2 == 0x3355b1 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800010d4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800010d8]:csrrs a4, fcsr, zero<br> [0x800010dc]:sw t6, 712(a1)<br>     |
| 113|[0x80006290]<br>0x00000016|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x93 and fm2 == 0x335cd4 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000016 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800010f8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800010fc]:csrrs a4, fcsr, zero<br> [0x80001100]:sw t6, 720(a1)<br>     |
| 114|[0x80006298]<br>0x00000027|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x94 and fm2 == 0x335ceb and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000027 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000111c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001120]:csrrs a4, fcsr, zero<br> [0x80001124]:sw t6, 728(a1)<br>     |
| 115|[0x800062a0]<br>0x0000005D|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x95 and fm2 == 0x335cbe and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001140]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001144]:csrrs a4, fcsr, zero<br> [0x80001148]:sw t6, 736(a1)<br>     |
| 116|[0x800062a8]<br>0x0000001A|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x84 and fm2 == 0x15bc2c and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001164]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001168]:csrrs a4, fcsr, zero<br> [0x8000116c]:sw t6, 744(a1)<br>     |
| 117|[0x800062b0]<br>0x0000004C|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x85 and fm2 == 0x5d67b8 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001188]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000118c]:csrrs a4, fcsr, zero<br> [0x80001190]:sw t6, 752(a1)<br>     |
| 118|[0x800062b8]<br>0x00000050|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x85 and fm2 == 0x6e3882 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000050 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800011ac]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800011b0]:csrrs a4, fcsr, zero<br> [0x800011b4]:sw t6, 760(a1)<br>     |
| 119|[0x800062c0]<br>0x0000001D|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x84 and fm2 == 0x54ff53 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800011d0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800011d4]:csrrs a4, fcsr, zero<br> [0x800011d8]:sw t6, 768(a1)<br>     |
| 120|[0x800062c8]<br>0x00000051|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x86 and fm2 == 0x0953c7 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000051 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800011f4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800011f8]:csrrs a4, fcsr, zero<br> [0x800011fc]:sw t6, 776(a1)<br>     |
| 121|[0x800062d0]<br>0x0000003D|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x86 and fm2 == 0x03b8d9 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001218]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000121c]:csrrs a4, fcsr, zero<br> [0x80001220]:sw t6, 784(a1)<br>     |
| 122|[0x800062d8]<br>0x00000016|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x85 and fm2 == 0x73d370 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000016 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000123c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001240]:csrrs a4, fcsr, zero<br> [0x80001244]:sw t6, 792(a1)<br>     |
| 123|[0x800062e0]<br>0x00000012|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x86 and fm2 == 0x4dfdaa and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000012 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001260]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001264]:csrrs a4, fcsr, zero<br> [0x80001268]:sw t6, 800(a1)<br>     |
| 124|[0x800062e8]<br>0x00000029|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x87 and fm2 == 0x50cb21 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000029 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001284]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001288]:csrrs a4, fcsr, zero<br> [0x8000128c]:sw t6, 808(a1)<br>     |
| 125|[0x800062f0]<br>0x0000003E|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x88 and fm2 == 0x496fc9 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800012a8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800012ac]:csrrs a4, fcsr, zero<br> [0x800012b0]:sw t6, 816(a1)<br>     |
| 126|[0x800062f8]<br>0x00000047|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x89 and fm2 == 0x3ffa57 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000047 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800012cc]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800012d0]:csrrs a4, fcsr, zero<br> [0x800012d4]:sw t6, 824(a1)<br>     |
| 127|[0x80006300]<br>0x00000036|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8a and fm2 == 0x382ee4 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000036 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800012f0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800012f4]:csrrs a4, fcsr, zero<br> [0x800012f8]:sw t6, 832(a1)<br>     |
| 128|[0x80006308]<br>0x0000002D|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8b and fm2 == 0x35616d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001314]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001318]:csrrs a4, fcsr, zero<br> [0x8000131c]:sw t6, 840(a1)<br>     |
| 129|[0x80006310]<br>0x00000055|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8c and fm2 == 0x353fcb and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000055 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001338]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000133c]:csrrs a4, fcsr, zero<br> [0x80001340]:sw t6, 848(a1)<br>     |
| 130|[0x80006318]<br>0x0000004C|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8d and fm2 == 0x34358c and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000135c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001360]:csrrs a4, fcsr, zero<br> [0x80001364]:sw t6, 856(a1)<br>     |
| 131|[0x80006320]<br>0x0000001F|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8e and fm2 == 0x338a97 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001380]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001384]:csrrs a4, fcsr, zero<br> [0x80001388]:sw t6, 864(a1)<br>     |
| 132|[0x80006328]<br>0x00000001|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8f and fm2 == 0x335f26 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800013a4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800013a8]:csrrs a4, fcsr, zero<br> [0x800013ac]:sw t6, 872(a1)<br>     |
| 133|[0x80006330]<br>0x00000034|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x90 and fm2 == 0x337051 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000034 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800013c8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800013cc]:csrrs a4, fcsr, zero<br> [0x800013d0]:sw t6, 880(a1)<br>     |
| 134|[0x80006338]<br>0x00000035|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x91 and fm2 == 0x336735 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000035 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800013ec]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800013f0]:csrrs a4, fcsr, zero<br> [0x800013f4]:sw t6, 888(a1)<br>     |
| 135|[0x80006340]<br>0x00000059|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x92 and fm2 == 0x3365a1 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000059 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001410]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001414]:csrrs a4, fcsr, zero<br> [0x80001418]:sw t6, 896(a1)<br>     |
| 136|[0x80006348]<br>0x00000005|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x93 and fm2 == 0x335e03 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000005 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001434]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001438]:csrrs a4, fcsr, zero<br> [0x8000143c]:sw t6, 904(a1)<br>     |
| 137|[0x80006350]<br>0x0000000D|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x94 and fm2 == 0x335e0e and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00000d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001458]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000145c]:csrrs a4, fcsr, zero<br> [0x80001460]:sw t6, 912(a1)<br>     |
| 138|[0x80006358]<br>0x00000046|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x95 and fm2 == 0x335e86 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000046 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000147c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001480]:csrrs a4, fcsr, zero<br> [0x80001484]:sw t6, 920(a1)<br>     |
| 139|[0x80006360]<br>0x0000004B|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335d54 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800014a0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800014a4]:csrrs a4, fcsr, zero<br> [0x800014a8]:sw t6, 928(a1)<br>     |
| 140|[0x80006368]<br>0x0000002E|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335d7b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800014c4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800014c8]:csrrs a4, fcsr, zero<br> [0x800014cc]:sw t6, 936(a1)<br>     |
| 141|[0x80006370]<br>0x00000036|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335d6d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000036 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800014e8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800014ec]:csrrs a4, fcsr, zero<br> [0x800014f0]:sw t6, 944(a1)<br>     |
| 142|[0x80006378]<br>0x00000049|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335d4d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000049 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000150c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001510]:csrrs a4, fcsr, zero<br> [0x80001514]:sw t6, 952(a1)<br>     |
| 143|[0x80006380]<br>0x0000003C|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335d54 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001530]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001534]:csrrs a4, fcsr, zero<br> [0x80001538]:sw t6, 960(a1)<br>     |
| 144|[0x80006388]<br>0x00000015|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335d74 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000015 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001554]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001558]:csrrs a4, fcsr, zero<br> [0x8000155c]:sw t6, 968(a1)<br>     |
| 145|[0x80006390]<br>0x00000049|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335cfe and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000049 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001578]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000157c]:csrrs a4, fcsr, zero<br> [0x80001580]:sw t6, 976(a1)<br>     |
| 146|[0x80006398]<br>0x0000001A|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335ce6 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000159c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800015a0]:csrrs a4, fcsr, zero<br> [0x800015a4]:sw t6, 984(a1)<br>     |
| 147|[0x800063a0]<br>0x0000004E|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335bea and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800015c0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800015c4]:csrrs a4, fcsr, zero<br> [0x800015c8]:sw t6, 992(a1)<br>     |
| 148|[0x800063a8]<br>0x0000002D|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335ab2 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800015e4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800015e8]:csrrs a4, fcsr, zero<br> [0x800015ec]:sw t6, 1000(a1)<br>    |
| 149|[0x800063b0]<br>0x00000031|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x3357df and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000031 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001608]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000160c]:csrrs a4, fcsr, zero<br> [0x80001610]:sw t6, 1008(a1)<br>    |
| 150|[0x800063b8]<br>0x0000002A|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x33524d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000162c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001630]:csrrs a4, fcsr, zero<br> [0x80001634]:sw t6, 1016(a1)<br>    |
| 151|[0x800063c0]<br>0x0000002A|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x334718 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001658]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000165c]:csrrs a4, fcsr, zero<br> [0x80001660]:sw t6, 0(a1)<br>       |
| 152|[0x800063c8]<br>0x00000037|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x33309a and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000037 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000167c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001680]:csrrs a4, fcsr, zero<br> [0x80001684]:sw t6, 8(a1)<br>       |
| 153|[0x800063d0]<br>0x0000001D|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x3303e7 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800016a0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800016a4]:csrrs a4, fcsr, zero<br> [0x800016a8]:sw t6, 16(a1)<br>      |
| 154|[0x800063d8]<br>0x00000026|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x32aa2b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000026 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800016c4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800016c8]:csrrs a4, fcsr, zero<br> [0x800016cc]:sw t6, 24(a1)<br>      |
| 155|[0x800063e0]<br>0x0000005B|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x31f683 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800016e8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800016ec]:csrrs a4, fcsr, zero<br> [0x800016f0]:sw t6, 32(a1)<br>      |
| 156|[0x800063e8]<br>0x00000032|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x309001 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000032 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000170c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001710]:csrrs a4, fcsr, zero<br> [0x80001714]:sw t6, 40(a1)<br>      |
| 157|[0x800063f0]<br>0x0000004D|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x2dc264 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001730]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001734]:csrrs a4, fcsr, zero<br> [0x80001738]:sw t6, 48(a1)<br>      |
| 158|[0x800063f8]<br>0x0000005F|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x28275d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001754]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001758]:csrrs a4, fcsr, zero<br> [0x8000175c]:sw t6, 56(a1)<br>      |
| 159|[0x80006400]<br>0x00000040|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x1cf1ac and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000040 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001778]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000177c]:csrrs a4, fcsr, zero<br> [0x80001780]:sw t6, 64(a1)<br>      |
| 160|[0x80006408]<br>0x00000036|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x068603 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000036 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000179c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800017a0]:csrrs a4, fcsr, zero<br> [0x800017a4]:sw t6, 72(a1)<br>      |
| 161|[0x80006410]<br>0x00000029|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x95 and fm2 == 0x335d4a and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000029 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800017c0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800017c4]:csrrs a4, fcsr, zero<br> [0x800017c8]:sw t6, 80(a1)<br>      |
| 162|[0x80006418]<br>0x0000003F|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335e15 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800017e4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800017e8]:csrrs a4, fcsr, zero<br> [0x800017ec]:sw t6, 88(a1)<br>      |
| 163|[0x80006420]<br>0x00000013|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335dd6 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000013 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001808]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000180c]:csrrs a4, fcsr, zero<br> [0x80001810]:sw t6, 96(a1)<br>      |
| 164|[0x80006428]<br>0x00000004|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335dbe and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000004 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000182c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001830]:csrrs a4, fcsr, zero<br> [0x80001834]:sw t6, 104(a1)<br>     |
| 165|[0x80006430]<br>0x00000055|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335e2a and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000055 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001850]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001854]:csrrs a4, fcsr, zero<br> [0x80001858]:sw t6, 112(a1)<br>     |
| 166|[0x80006438]<br>0x00000020|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335dd5 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000020 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001874]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001878]:csrrs a4, fcsr, zero<br> [0x8000187c]:sw t6, 120(a1)<br>     |
| 167|[0x80006440]<br>0x00000006|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335d9a and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000006 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001898]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000189c]:csrrs a4, fcsr, zero<br> [0x800018a0]:sw t6, 128(a1)<br>     |
| 168|[0x80006448]<br>0x00000033|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335dac and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000033 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800018bc]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800018c0]:csrrs a4, fcsr, zero<br> [0x800018c4]:sw t6, 136(a1)<br>     |
| 169|[0x80006450]<br>0x0000003E|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335d62 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800018e0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800018e4]:csrrs a4, fcsr, zero<br> [0x800018e8]:sw t6, 144(a1)<br>     |
| 170|[0x80006458]<br>0x00000020|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335c84 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000020 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001904]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001908]:csrrs a4, fcsr, zero<br> [0x8000190c]:sw t6, 152(a1)<br>     |
| 171|[0x80006460]<br>0x00000005|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335af8 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000005 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001928]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000192c]:csrrs a4, fcsr, zero<br> [0x80001930]:sw t6, 160(a1)<br>     |
| 172|[0x80006468]<br>0x0000003F|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x33587b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000194c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001950]:csrrs a4, fcsr, zero<br> [0x80001954]:sw t6, 168(a1)<br>     |
| 173|[0x80006470]<br>0x00000014|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x3352a4 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000014 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001970]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001974]:csrrs a4, fcsr, zero<br> [0x80001978]:sw t6, 176(a1)<br>     |
| 174|[0x80006478]<br>0x00000026|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x334788 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000026 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001994]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001998]:csrrs a4, fcsr, zero<br> [0x8000199c]:sw t6, 184(a1)<br>     |
| 175|[0x80006480]<br>0x0000004E|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x333154 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800019b8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800019bc]:csrrs a4, fcsr, zero<br> [0x800019c0]:sw t6, 192(a1)<br>     |
| 176|[0x80006488]<br>0x00000043|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x33046d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000043 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800019dc]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800019e0]:csrrs a4, fcsr, zero<br> [0x800019e4]:sw t6, 200(a1)<br>     |
| 177|[0x80006490]<br>0x00000003|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x32aa65 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000003 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001a00]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001a04]:csrrs a4, fcsr, zero<br> [0x80001a08]:sw t6, 208(a1)<br>     |
| 178|[0x80006498]<br>0x00000027|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x31f739 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000027 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001a24]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001a28]:csrrs a4, fcsr, zero<br> [0x80001a2c]:sw t6, 216(a1)<br>     |
| 179|[0x800064a0]<br>0x00000045|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x3090a8 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000045 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001a48]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001a4c]:csrrs a4, fcsr, zero<br> [0x80001a50]:sw t6, 224(a1)<br>     |
| 180|[0x800064a8]<br>0x0000004F|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x2dc33f and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001a6c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001a70]:csrrs a4, fcsr, zero<br> [0x80001a74]:sw t6, 232(a1)<br>     |
| 181|[0x800064b0]<br>0x0000005E|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x282866 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001a90]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001a94]:csrrs a4, fcsr, zero<br> [0x80001a98]:sw t6, 240(a1)<br>     |
| 182|[0x800064b8]<br>0x0000003C|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x1cf25a and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001ab4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001ab8]:csrrs a4, fcsr, zero<br> [0x80001abc]:sw t6, 248(a1)<br>     |
| 183|[0x800064c0]<br>0x0000002F|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x068690 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001ad8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001adc]:csrrs a4, fcsr, zero<br> [0x80001ae0]:sw t6, 256(a1)<br>     |
| 184|[0x800064c8]<br>0x00000005|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x95 and fm2 == 0x335dcb and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000005 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001afc]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001b00]:csrrs a4, fcsr, zero<br> [0x80001b04]:sw t6, 264(a1)<br>     |
| 185|[0x800064d0]<br>0x00800001|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6d and fm2 == 0x000000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000021 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001b20]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001b24]:csrrs a4, fcsr, zero<br> [0x80001b28]:sw t6, 272(a1)<br>     |
| 186|[0x800064d8]<br>0x00800002|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6e and fm2 == 0x2c0000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000058 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001b44]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001b48]:csrrs a4, fcsr, zero<br> [0x80001b4c]:sw t6, 280(a1)<br>     |
| 187|[0x800064e0]<br>0x00800004|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6e and fm2 == 0x280000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000058 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001b68]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001b6c]:csrrs a4, fcsr, zero<br> [0x80001b70]:sw t6, 288(a1)<br>     |
| 188|[0x800064e8]<br>0x00800008|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6a and fm2 == 0x400000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001b8c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001b90]:csrrs a4, fcsr, zero<br> [0x80001b94]:sw t6, 296(a1)<br>     |
| 189|[0x800064f0]<br>0x00800010|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6d and fm2 == 0x1c0000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000037 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001bb0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001bb4]:csrrs a4, fcsr, zero<br> [0x80001bb8]:sw t6, 304(a1)<br>     |
| 190|[0x800064f8]<br>0x00800020|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6c and fm2 == 0x100000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000032 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001bd4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001bd8]:csrrs a4, fcsr, zero<br> [0x80001bdc]:sw t6, 312(a1)<br>     |
| 191|[0x80006500]<br>0x00800040|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6b and fm2 == 0x500000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000033 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001bf8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001bfc]:csrrs a4, fcsr, zero<br> [0x80001c00]:sw t6, 320(a1)<br>     |
| 192|[0x80006508]<br>0x00800080|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6e and fm2 == 0x340000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000026 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001c1c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001c20]:csrrs a4, fcsr, zero<br> [0x80001c24]:sw t6, 328(a1)<br>     |
| 193|[0x80006510]<br>0x00800100|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6f and fm2 == 0x1e0000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000062 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001c50]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001c54]:csrrs a4, fcsr, zero<br> [0x80001c58]:sw t6, 336(a1)<br>     |
| 194|[0x80006518]<br>0x00800200|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x70 and fm2 == 0x6c8000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000027 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001ca4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001ca8]:csrrs a4, fcsr, zero<br> [0x80001cac]:sw t6, 344(a1)<br>     |
| 195|[0x80006520]<br>0x00800400|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x71 and fm2 == 0x69c000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000059 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001cf8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001cfc]:csrrs a4, fcsr, zero<br> [0x80001d00]:sw t6, 352(a1)<br>     |
| 196|[0x80006528]<br>0x00800800|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x72 and fm2 == 0x76a000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00004b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001d4c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001d50]:csrrs a4, fcsr, zero<br> [0x80001d54]:sw t6, 360(a1)<br>     |
| 197|[0x80006530]<br>0x00801000|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x73 and fm2 == 0x7ad000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000053 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001da0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001da4]:csrrs a4, fcsr, zero<br> [0x80001da8]:sw t6, 368(a1)<br>     |
| 198|[0x80006538]<br>0x00802000|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x74 and fm2 == 0x7ec000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000028 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001df4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001df8]:csrrs a4, fcsr, zero<br> [0x80001dfc]:sw t6, 376(a1)<br>     |
| 199|[0x80006540]<br>0x00804000|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x75 and fm2 == 0x7fa800 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000016 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001e48]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001e4c]:csrrs a4, fcsr, zero<br> [0x80001e50]:sw t6, 384(a1)<br>     |
| 200|[0x80006548]<br>0x00808000|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x76 and fm2 == 0x7f4e00 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000059 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001e9c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001ea0]:csrrs a4, fcsr, zero<br> [0x80001ea4]:sw t6, 392(a1)<br>     |
| 201|[0x80006550]<br>0x00810000|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x77 and fm2 == 0x7fd500 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00002b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001ef0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001ef4]:csrrs a4, fcsr, zero<br> [0x80001ef8]:sw t6, 400(a1)<br>     |
| 202|[0x80006558]<br>0x00820000|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x78 and fm2 == 0x7fe480 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000037 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001f44]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001f48]:csrrs a4, fcsr, zero<br> [0x80001f4c]:sw t6, 408(a1)<br>     |
| 203|[0x80006560]<br>0x00840000|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x79 and fm2 == 0x7ffdc0 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000009 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001f98]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001f9c]:csrrs a4, fcsr, zero<br> [0x80001fa0]:sw t6, 416(a1)<br>     |
| 204|[0x80006568]<br>0x00880000|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7a and fm2 == 0x7ffba0 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000023 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001fec]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001ff0]:csrrs a4, fcsr, zero<br> [0x80001ff4]:sw t6, 424(a1)<br>     |
| 205|[0x80006570]<br>0x00900000|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7b and fm2 == 0x7fff70 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000009 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002040]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002044]:csrrs a4, fcsr, zero<br> [0x80002048]:sw t6, 432(a1)<br>     |
| 206|[0x80006578]<br>0x00A00000|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7c and fm2 == 0x7ffea8 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00002b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002094]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002098]:csrrs a4, fcsr, zero<br> [0x8000209c]:sw t6, 440(a1)<br>     |
| 207|[0x80006580]<br>0x00C00000|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x7fffdc and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000009 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800020e8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800020ec]:csrrs a4, fcsr, zero<br> [0x800020f0]:sw t6, 448(a1)<br>     |
| 208|[0x80006588]<br>0x80800002|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x00002e and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00005a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000213c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002140]:csrrs a4, fcsr, zero<br> [0x80002144]:sw t6, 456(a1)<br>     |
| 209|[0x80006590]<br>0x80800001|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000004 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000007 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002190]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002194]:csrrs a4, fcsr, zero<br> [0x80002198]:sw t6, 464(a1)<br>     |
| 210|[0x80006598]<br>0x80800004|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x00000a and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000010 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800021e4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800021e8]:csrrs a4, fcsr, zero<br> [0x800021ec]:sw t6, 472(a1)<br>     |
| 211|[0x800065a0]<br>0x80800009|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x00002c and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00004f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002238]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000223c]:csrrs a4, fcsr, zero<br> [0x80002240]:sw t6, 480(a1)<br>     |
| 212|[0x800065a8]<br>0x80800010|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000036 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00005c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000228c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002290]:csrrs a4, fcsr, zero<br> [0x80002294]:sw t6, 488(a1)<br>     |
| 213|[0x800065b0]<br>0x80800020|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000023 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000026 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800022e0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800022e4]:csrrs a4, fcsr, zero<br> [0x800022e8]:sw t6, 496(a1)<br>     |
| 214|[0x800065b8]<br>0x80800040|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x00002a and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000014 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002334]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002338]:csrrs a4, fcsr, zero<br> [0x8000233c]:sw t6, 504(a1)<br>     |
| 215|[0x800065c0]<br>0x80800081|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000068 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00004f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002388]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000238c]:csrrs a4, fcsr, zero<br> [0x80002390]:sw t6, 512(a1)<br>     |
| 216|[0x800065c8]<br>0x808000FF|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x00009a and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000035 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800023dc]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800023e0]:csrrs a4, fcsr, zero<br> [0x800023e4]:sw t6, 520(a1)<br>     |
| 217|[0x800065d0]<br>0x808001FF|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000104 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000009 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002430]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002434]:csrrs a4, fcsr, zero<br> [0x80002438]:sw t6, 528(a1)<br>     |
| 218|[0x800065d8]<br>0x80800400|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000203 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000006 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002484]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002488]:csrrs a4, fcsr, zero<br> [0x8000248c]:sw t6, 536(a1)<br>     |
| 219|[0x800065e0]<br>0x808007FF|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x00042a and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000055 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800024d8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800024dc]:csrrs a4, fcsr, zero<br> [0x800024e0]:sw t6, 544(a1)<br>     |
| 220|[0x800065e8]<br>0x80801000|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x000830 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000060 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000252c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002530]:csrrs a4, fcsr, zero<br> [0x80002534]:sw t6, 552(a1)<br>     |
| 221|[0x800065f0]<br>0x80802000|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x001012 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000024 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002580]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002584]:csrrs a4, fcsr, zero<br> [0x80002588]:sw t6, 560(a1)<br>     |
| 222|[0x800065f8]<br>0x80804000|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x002025 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00004a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800025d4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800025d8]:csrrs a4, fcsr, zero<br> [0x800025dc]:sw t6, 568(a1)<br>     |
| 223|[0x80006600]<br>0x80808000|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x004030 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000060 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002628]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000262c]:csrrs a4, fcsr, zero<br> [0x80002630]:sw t6, 576(a1)<br>     |
| 224|[0x80006608]<br>0x80810000|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x008028 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000050 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000267c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002680]:csrrs a4, fcsr, zero<br> [0x80002684]:sw t6, 584(a1)<br>     |
| 225|[0x80006610]<br>0x80820000|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x010001 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800026d0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800026d4]:csrrs a4, fcsr, zero<br> [0x800026d8]:sw t6, 592(a1)<br>     |
| 226|[0x80006618]<br>0x80840000|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x020006 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00000c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002724]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002728]:csrrs a4, fcsr, zero<br> [0x8000272c]:sw t6, 600(a1)<br>     |
| 227|[0x80006620]<br>0x80880000|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x04002c and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000058 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002778]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000277c]:csrrs a4, fcsr, zero<br> [0x80002780]:sw t6, 608(a1)<br>     |
| 228|[0x80006628]<br>0x80900001|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x080020 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800027cc]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800027d0]:csrrs a4, fcsr, zero<br> [0x800027d4]:sw t6, 616(a1)<br>     |
| 229|[0x80006630]<br>0x80A00000|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x10002a and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000054 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002820]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002824]:csrrs a4, fcsr, zero<br> [0x80002828]:sw t6, 624(a1)<br>     |
| 230|[0x80006638]<br>0x80C00000|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x200004 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000008 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002874]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002878]:csrrs a4, fcsr, zero<br> [0x8000287c]:sw t6, 632(a1)<br>     |
| 231|[0x80006640]<br>0x7F7FFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7fff48 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00005a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800028c8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800028cc]:csrrs a4, fcsr, zero<br> [0x800028d0]:sw t6, 640(a1)<br>     |
| 232|[0x80006648]<br>0x7F7FFFFD|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7fffd2 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000014 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000291c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002920]:csrrs a4, fcsr, zero<br> [0x80002924]:sw t6, 648(a1)<br>     |
| 233|[0x80006650]<br>0x7F7FFFFB|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7fff66 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000048 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002970]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002974]:csrrs a4, fcsr, zero<br> [0x80002978]:sw t6, 656(a1)<br>     |
| 234|[0x80006658]<br>0x7F7FFFF7|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7fffaa and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000022 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800029c4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800029c8]:csrrs a4, fcsr, zero<br> [0x800029cc]:sw t6, 664(a1)<br>     |
| 235|[0x80006660]<br>0x7F7FFFEF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7fff50 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000047 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002a18]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002a1c]:csrrs a4, fcsr, zero<br> [0x80002a20]:sw t6, 672(a1)<br>     |
| 236|[0x80006668]<br>0x7F7FFFDF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7fff7c and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000021 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002a6c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002a70]:csrrs a4, fcsr, zero<br> [0x80002a74]:sw t6, 680(a1)<br>     |
| 237|[0x80006670]<br>0x7F7FFFBF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7ffef8 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000043 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002ac0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002ac4]:csrrs a4, fcsr, zero<br> [0x80002ac8]:sw t6, 688(a1)<br>     |
| 238|[0x80006678]<br>0x7F7FFF7F|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7ffec0 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00001f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002b14]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002b18]:csrrs a4, fcsr, zero<br> [0x80002b1c]:sw t6, 696(a1)<br>     |
| 239|[0x80006680]<br>0x7F7FFEFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7ffd88 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00003b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002b68]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002b6c]:csrrs a4, fcsr, zero<br> [0x80002b70]:sw t6, 704(a1)<br>     |
| 240|[0x80006688]<br>0x7F7FFDFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7ffbd0 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000017 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002bbc]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002bc0]:csrrs a4, fcsr, zero<br> [0x80002bc4]:sw t6, 712(a1)<br>     |
| 241|[0x80006690]<br>0x7F7FFBFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7ff746 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00005c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002c10]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002c14]:csrrs a4, fcsr, zero<br> [0x80002c18]:sw t6, 720(a1)<br>     |
| 242|[0x80006698]<br>0x7F7FF7FF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7fef56 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000054 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002c64]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002c68]:csrrs a4, fcsr, zero<br> [0x80002c6c]:sw t6, 728(a1)<br>     |
| 243|[0x800066a0]<br>0x7F7FEFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7fdfe8 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00000b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002cb8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002cbc]:csrrs a4, fcsr, zero<br> [0x80002cc0]:sw t6, 736(a1)<br>     |
| 244|[0x800066a8]<br>0x7F7FDFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7fbfe6 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00000c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002d0c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002d10]:csrrs a4, fcsr, zero<br> [0x80002d14]:sw t6, 744(a1)<br>     |
| 245|[0x800066b0]<br>0x7F7FBFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7f7f7c and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000041 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002d60]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002d64]:csrrs a4, fcsr, zero<br> [0x80002d68]:sw t6, 752(a1)<br>     |
| 246|[0x800066b8]<br>0x7F7F7FFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7effc0 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00001f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002db4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002db8]:csrrs a4, fcsr, zero<br> [0x80002dbc]:sw t6, 760(a1)<br>     |
| 247|[0x800066c0]<br>0x7F7EFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7dff86 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00003c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002e08]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002e0c]:csrrs a4, fcsr, zero<br> [0x80002e10]:sw t6, 768(a1)<br>     |
| 248|[0x800066c8]<br>0x7F7DFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7bffaa and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00002a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002e5c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002e60]:csrrs a4, fcsr, zero<br> [0x80002e64]:sw t6, 776(a1)<br>     |
| 249|[0x800066d0]<br>0x7F7BFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x77ff76 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000044 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002eb0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002eb4]:csrrs a4, fcsr, zero<br> [0x80002eb8]:sw t6, 784(a1)<br>     |
| 250|[0x800066d8]<br>0x7F77FFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x6fff72 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000046 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002f04]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002f08]:csrrs a4, fcsr, zero<br> [0x80002f0c]:sw t6, 792(a1)<br>     |
| 251|[0x800066e0]<br>0x7F6FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x5ffff0 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000007 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002f58]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002f5c]:csrrs a4, fcsr, zero<br> [0x80002f60]:sw t6, 800(a1)<br>     |
| 252|[0x800066e8]<br>0x7F5FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x3fff90 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000037 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002fac]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80002fb0]:csrrs a4, fcsr, zero<br> [0x80002fb4]:sw t6, 808(a1)<br>     |
| 253|[0x800066f0]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x7ffe7c and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000060 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003000]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80003004]:csrrs a4, fcsr, zero<br> [0x80003008]:sw t6, 816(a1)<br>     |
| 254|[0x800066f8]<br>0xFF7FFFFD|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x400004 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00000b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003054]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80003058]:csrrs a4, fcsr, zero<br> [0x8000305c]:sw t6, 824(a1)<br>     |
| 255|[0x80006700]<br>0xFF7FFFFC|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x40000c and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00001c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800030a8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800030ac]:csrrs a4, fcsr, zero<br> [0x800030b0]:sw t6, 832(a1)<br>     |
| 256|[0x80006708]<br>0xFF7FFFFA|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x40002a and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00005a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800030fc]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80003100]:csrrs a4, fcsr, zero<br> [0x80003104]:sw t6, 840(a1)<br>     |
| 257|[0x80006710]<br>0xFF7FFFF6|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x400012 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00002e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003150]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80003154]:csrrs a4, fcsr, zero<br> [0x80003158]:sw t6, 848(a1)<br>     |
| 258|[0x80006718]<br>0xFF7FFFEE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x40000a and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000026 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800031a4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800031a8]:csrrs a4, fcsr, zero<br> [0x800031ac]:sw t6, 856(a1)<br>     |
| 259|[0x80006720]<br>0xFF7FFFDF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x400017 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00004f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800031f8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800031fc]:csrrs a4, fcsr, zero<br> [0x80003200]:sw t6, 864(a1)<br>     |
| 260|[0x80006728]<br>0xFF7FFFBE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3ffff8 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000032 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000324c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80003250]:csrrs a4, fcsr, zero<br> [0x80003254]:sw t6, 872(a1)<br>     |
| 261|[0x80006730]<br>0xFF7FFF7E|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3fffc8 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000012 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800032a0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800032a4]:csrrs a4, fcsr, zero<br> [0x800032a8]:sw t6, 880(a1)<br>     |
| 262|[0x80006738]<br>0xFF7FFEFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3fffa9 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000053 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800032f4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800032f8]:csrrs a4, fcsr, zero<br> [0x800032fc]:sw t6, 888(a1)<br>     |
| 263|[0x80006740]<br>0xFF7FFDFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3fff28 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000051 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003348]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000334c]:csrrs a4, fcsr, zero<br> [0x80003350]:sw t6, 896(a1)<br>     |
| 264|[0x80006748]<br>0xFF7FFBFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3ffe17 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00002f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000339c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800033a0]:csrrs a4, fcsr, zero<br> [0x800033a4]:sw t6, 904(a1)<br>     |
| 265|[0x80006750]<br>0xFF7FF800|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3ffc26 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00004c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800033f0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800033f4]:csrrs a4, fcsr, zero<br> [0x800033f8]:sw t6, 912(a1)<br>     |
| 266|[0x80006758]<br>0xFF7FEFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3ff82f and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00005f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003444]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80003448]:csrrs a4, fcsr, zero<br> [0x8000344c]:sw t6, 920(a1)<br>     |
| 267|[0x80006760]<br>0xFF7FDFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3ff019 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000033 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003498]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000349c]:csrrs a4, fcsr, zero<br> [0x800034a0]:sw t6, 928(a1)<br>     |
| 268|[0x80006768]<br>0xFF7FBFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3fe006 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00000d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800034ec]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800034f0]:csrrs a4, fcsr, zero<br> [0x800034f4]:sw t6, 936(a1)<br>     |
| 269|[0x80006770]<br>0xFF7F7FFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3fc002 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000005 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003540]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80003544]:csrrs a4, fcsr, zero<br> [0x80003548]:sw t6, 944(a1)<br>     |
| 270|[0x80006778]<br>0xFF7EFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3f800c and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00001a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003594]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80003598]:csrrs a4, fcsr, zero<br> [0x8000359c]:sw t6, 952(a1)<br>     |
| 271|[0x80006780]<br>0xFF7DFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3f0002 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000006 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800035e8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800035ec]:csrrs a4, fcsr, zero<br> [0x800035f0]:sw t6, 960(a1)<br>     |
| 272|[0x80006788]<br>0xFF7BFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3e001e and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00003e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000363c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80003640]:csrrs a4, fcsr, zero<br> [0x80003644]:sw t6, 968(a1)<br>     |
| 273|[0x80006790]<br>0xFF77FFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x3c0000 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003690]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80003694]:csrrs a4, fcsr, zero<br> [0x80003698]:sw t6, 976(a1)<br>     |
| 274|[0x80006798]<br>0xFF6FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x38001e and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00003d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800036e4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800036e8]:csrrs a4, fcsr, zero<br> [0x800036ec]:sw t6, 984(a1)<br>     |
| 275|[0x800067a0]<br>0xFF600000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x300028 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000050 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003738]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000373c]:csrrs a4, fcsr, zero<br> [0x80003740]:sw t6, 992(a1)<br>     |
| 276|[0x800067a8]<br>0xFF3FFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x200014 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00002a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000378c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80003790]:csrrs a4, fcsr, zero<br> [0x80003794]:sw t6, 1000(a1)<br>    |
| 277|[0x800067b0]<br>0x0000005F|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x86 and fm2 == 0x03b8d9 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800037e0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800037e4]:csrrs a4, fcsr, zero<br> [0x800037e8]:sw t6, 1008(a1)<br>    |
| 278|[0x800067b8]<br>0x0000003C|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x85 and fm2 == 0x1cf208 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003834]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80003838]:csrrs a4, fcsr, zero<br> [0x8000383c]:sw t6, 1016(a1)<br>    |
| 279|[0x800067c0]<br>0x00000054|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x85 and fm2 == 0x54ff53 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000054 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003890]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80003894]:csrrs a4, fcsr, zero<br> [0x80003898]:sw t6, 0(a1)<br>       |
| 280|[0x800067c8]<br>0x00000015|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x81 and fm2 == 0x60352f and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000015 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800038e4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800038e8]:csrrs a4, fcsr, zero<br> [0x800038ec]:sw t6, 8(a1)<br>       |
| 281|[0x800067d0]<br>0x00000030|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x83 and fm2 == 0x335dc0 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000030 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003938]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000393c]:csrrs a4, fcsr, zero<br> [0x80003940]:sw t6, 16(a1)<br>      |
| 282|[0x800067d8]<br>0x00000020|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x84 and fm2 == 0x335dc0 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000020 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000398c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80003990]:csrrs a4, fcsr, zero<br> [0x80003994]:sw t6, 24(a1)<br>      |
| 283|[0x800067e0]<br>0x00000046|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x228cf6 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000046 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800039e0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800039e4]:csrrs a4, fcsr, zero<br> [0x800039e8]:sw t6, 32(a1)<br>      |
| 284|[0x80006800]<br>0x00000018|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8a and fm2 == 0x3143a6 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000018 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003b30]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80003b34]:csrrs a4, fcsr, zero<br> [0x80003b38]:sw t6, 64(a1)<br>      |
| 285|[0x80006810]<br>0x00000003|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8a and fm2 == 0x33a103 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000003 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003bd8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80003bdc]:csrrs a4, fcsr, zero<br> [0x80003be0]:sw t6, 80(a1)<br>      |
| 286|[0x80006818]<br>0x0000002F|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8d and fm2 == 0x33e178 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003c2c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80003c30]:csrrs a4, fcsr, zero<br> [0x80003c34]:sw t6, 88(a1)<br>      |
