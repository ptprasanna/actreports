
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80001700')]      |
| SIG_REGION                | [('0x80003910', '0x80003df0', '312 words')]      |
| COV_LABELS                | fmadd_b4      |
| TEST_NAME                 | /home/reg/work/zfinx/RV32Zfinx/fmadd.s/work/fmadd_b4-01.S/ref.S    |
| Total Number of coverpoints| 281     |
| Total Coverpoints Hit     | 281      |
| Total Signature Updates   | 310      |
| STAT1                     | 151      |
| STAT2                     | 4      |
| STAT3                     | 0     |
| STAT4                     | 155     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x800015e4]:fmadd.s t6, t5, t4, t3, dyn
      [0x800015e8]:csrrs a4, fcsr, zero
      [0x800015ec]:sw t6, 1000(a1)
 -- Signature Address: 0x80003da8 Data: 0x7F3FFFFF
 -- Redundant Coverpoints hit by the op
      - mnemonic : fmadd.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs3 : x28
      - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd
      - fs1 == 0 and fe1 == 0xfe and fm1 == 0x18ad5a and fs2 == 0 and fe2 == 0x7b and fm2 == 0x1437ba and fs3 == 0 and fe3 == 0xfe and fm3 == 0x34f350 and  fcsr == 0x60 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80001608]:fmadd.s t6, t5, t4, t3, dyn
      [0x8000160c]:csrrs a4, fcsr, zero
      [0x80001610]:sw t6, 1008(a1)
 -- Signature Address: 0x80003db0 Data: 0x7F3FFFFF
 -- Redundant Coverpoints hit by the op
      - mnemonic : fmadd.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs3 : x28
      - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd
      - fs1 == 0 and fe1 == 0xfe and fm1 == 0x18ad5a and fs2 == 0 and fe2 == 0x7b and fm2 == 0x1437ba and fs3 == 0 and fe3 == 0xfe and fm3 == 0x34f350 and  fcsr == 0x80 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x8000162c]:fmadd.s t6, t5, t4, t3, dyn
      [0x80001630]:csrrs a4, fcsr, zero
      [0x80001634]:sw t6, 1016(a1)
 -- Signature Address: 0x80003db8 Data: 0x7F3FFFFF
 -- Redundant Coverpoints hit by the op
      - mnemonic : fmadd.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs3 : x28
      - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd
      - fs1 == 0 and fe1 == 0xfe and fm1 == 0x0e0f7e and fs2 == 0 and fe2 == 0x7b and fm2 == 0x570014 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x31161d and  fcsr == 0x0 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x800016e8]:fmadd.s t6, t5, t4, t3, dyn
      [0x800016ec]:csrrs a4, fcsr, zero
      [0x800016f0]:sw t6, 32(a1)
 -- Signature Address: 0x80003de0 Data: 0x7F3FFFFE
 -- Redundant Coverpoints hit by the op
      - mnemonic : fmadd.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs3 : x28
      - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd
      - fs1 == 0 and fe1 == 0xf8 and fm1 == 0x2125ab and fs2 == 0 and fe2 == 0x84 and fm2 == 0x6ee85e and fs3 == 0 and fe3 == 0xfc and fm3 == 0x2672cf and  fcsr == 0x0 and rm_val == 7   #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fmadd.s', 'rs1 : x30', 'rs2 : x30', 'rd : x31', 'rs3 : x29', 'rs1 == rs2 != rs3 and rs1 == rs2 != rd and rd != rs3']
Last Code Sequence : 
	-[0x80000128]:fmadd.s t6, t5, t5, t4, dyn
	-[0x8000012c]:csrrs tp, fcsr, zero
	-[0x80000130]:sw t6, 0(ra)
Current Store : [0x80000134] : sw tp, 4(ra) -- Store: [0x80003914]:0x00000005




Last Coverpoint : ['rs1 : x31', 'rs2 : x29', 'rd : x30', 'rs3 : x28', 'rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x6f31b9 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x543d9b and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0e6c50 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000014c]:fmadd.s t5, t6, t4, t3, dyn
	-[0x80000150]:csrrs tp, fcsr, zero
	-[0x80000154]:sw t5, 8(ra)
Current Store : [0x80000158] : sw tp, 12(ra) -- Store: [0x8000391c]:0x00000021




Last Coverpoint : ['rs1 : x28', 'rs2 : x28', 'rd : x28', 'rs3 : x31', 'rs1 == rs2 == rd != rs3']
Last Code Sequence : 
	-[0x80000170]:fmadd.s t3, t3, t3, t6, dyn
	-[0x80000174]:csrrs tp, fcsr, zero
	-[0x80000178]:sw t3, 16(ra)
Current Store : [0x8000017c] : sw tp, 20(ra) -- Store: [0x80003924]:0x00000045




Last Coverpoint : ['rs1 : x29', 'rs2 : x27', 'rd : x27', 'rs3 : x27', 'rd == rs2 == rs3 != rs1']
Last Code Sequence : 
	-[0x80000194]:fmadd.s s11, t4, s11, s11, dyn
	-[0x80000198]:csrrs tp, fcsr, zero
	-[0x8000019c]:sw s11, 24(ra)
Current Store : [0x800001a0] : sw tp, 28(ra) -- Store: [0x8000392c]:0x00000061




Last Coverpoint : ['rs1 : x27', 'rs2 : x26', 'rd : x29', 'rs3 : x26', 'rs2 == rs3 != rs1 and rs2 == rs3 != rd and rd != rs1']
Last Code Sequence : 
	-[0x800001b8]:fmadd.s t4, s11, s10, s10, dyn
	-[0x800001bc]:csrrs tp, fcsr, zero
	-[0x800001c0]:sw t4, 32(ra)
Current Store : [0x800001c4] : sw tp, 36(ra) -- Store: [0x80003934]:0x00000081




Last Coverpoint : ['rs1 : x25', 'rs2 : x25', 'rd : x26', 'rs3 : x25', 'rs1 == rs2 == rs3 != rd']
Last Code Sequence : 
	-[0x800001dc]:fmadd.s s10, s9, s9, s9, dyn
	-[0x800001e0]:csrrs tp, fcsr, zero
	-[0x800001e4]:sw s10, 40(ra)
Current Store : [0x800001e8] : sw tp, 44(ra) -- Store: [0x8000393c]:0x00000005




Last Coverpoint : ['rs1 : x24', 'rs2 : x24', 'rd : x24', 'rs3 : x24', 'rs1 == rs2 == rs3 == rd']
Last Code Sequence : 
	-[0x80000200]:fmadd.s s8, s8, s8, s8, dyn
	-[0x80000204]:csrrs tp, fcsr, zero
	-[0x80000208]:sw s8, 48(ra)
Current Store : [0x8000020c] : sw tp, 52(ra) -- Store: [0x80003944]:0x00000025




Last Coverpoint : ['rs1 : x23', 'rs2 : x31', 'rd : x25', 'rs3 : x23', 'rs1 == rs3 != rs2 and rs1 == rs3 != rd and rd != rs2']
Last Code Sequence : 
	-[0x80000224]:fmadd.s s9, s7, t6, s7, dyn
	-[0x80000228]:csrrs tp, fcsr, zero
	-[0x8000022c]:sw s9, 56(ra)
Current Store : [0x80000230] : sw tp, 60(ra) -- Store: [0x8000394c]:0x00000041




Last Coverpoint : ['rs1 : x22', 'rs2 : x23', 'rd : x22', 'rs3 : x30', 'rs1 == rd != rs2 and rs1 == rd != rs3 and rs3 != rs2', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x18ad5a and fs2 == 0 and fe2 == 0x7b and fm2 == 0x1437ba and fs3 == 0 and fe3 == 0xfe and fm3 == 0x34f350 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000248]:fmadd.s s6, s6, s7, t5, dyn
	-[0x8000024c]:csrrs tp, fcsr, zero
	-[0x80000250]:sw s6, 64(ra)
Current Store : [0x80000254] : sw tp, 68(ra) -- Store: [0x80003954]:0x00000061




Last Coverpoint : ['rs1 : x26', 'rs2 : x21', 'rd : x21', 'rs3 : x22', 'rs2 == rd != rs1 and rs2 == rd != rs3 and rs3 != rs1', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x18ad5a and fs2 == 0 and fe2 == 0x7b and fm2 == 0x1437ba and fs3 == 0 and fe3 == 0xfe and fm3 == 0x34f350 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000026c]:fmadd.s s5, s10, s5, s6, dyn
	-[0x80000270]:csrrs tp, fcsr, zero
	-[0x80000274]:sw s5, 72(ra)
Current Store : [0x80000278] : sw tp, 76(ra) -- Store: [0x8000395c]:0x00000081




Last Coverpoint : ['rs1 : x21', 'rs2 : x22', 'rd : x20', 'rs3 : x20', 'rs3 == rd != rs1 and rs3 == rd != rs2 and rs2 != rs1', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x0e0f7e and fs2 == 0 and fe2 == 0x7b and fm2 == 0x570014 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x31161d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000290]:fmadd.s s4, s5, s6, s4, dyn
	-[0x80000294]:csrrs tp, fcsr, zero
	-[0x80000298]:sw s4, 80(ra)
Current Store : [0x8000029c] : sw tp, 84(ra) -- Store: [0x80003964]:0x00000001




Last Coverpoint : ['rs1 : x19', 'rs2 : x20', 'rd : x19', 'rs3 : x19', 'rs1 == rd == rs3 != rs2']
Last Code Sequence : 
	-[0x800002b4]:fmadd.s s3, s3, s4, s3, dyn
	-[0x800002b8]:csrrs tp, fcsr, zero
	-[0x800002bc]:sw s3, 88(ra)
Current Store : [0x800002c0] : sw tp, 92(ra) -- Store: [0x8000396c]:0x00000021




Last Coverpoint : ['rs1 : x20', 'rs2 : x19', 'rd : x23', 'rs3 : x21', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x0e0f7e and fs2 == 0 and fe2 == 0x7b and fm2 == 0x570014 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x31161d and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002d8]:fmadd.s s7, s4, s3, s5, dyn
	-[0x800002dc]:csrrs tp, fcsr, zero
	-[0x800002e0]:sw s7, 96(ra)
Current Store : [0x800002e4] : sw tp, 100(ra) -- Store: [0x80003974]:0x00000041




Last Coverpoint : ['rs1 : x17', 'rs2 : x16', 'rd : x18', 'rs3 : x15', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x0e0f7e and fs2 == 0 and fe2 == 0x7b and fm2 == 0x570014 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x31161d and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002fc]:fmadd.s s2, a7, a6, a5, dyn
	-[0x80000300]:csrrs tp, fcsr, zero
	-[0x80000304]:sw s2, 104(ra)
Current Store : [0x80000308] : sw tp, 108(ra) -- Store: [0x8000397c]:0x00000061




Last Coverpoint : ['rs1 : x15', 'rs2 : x18', 'rd : x17', 'rs3 : x16', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x0e0f7e and fs2 == 0 and fe2 == 0x7b and fm2 == 0x570014 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x31161d and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000320]:fmadd.s a7, a5, s2, a6, dyn
	-[0x80000324]:csrrs tp, fcsr, zero
	-[0x80000328]:sw a7, 112(ra)
Current Store : [0x8000032c] : sw tp, 116(ra) -- Store: [0x80003984]:0x00000081




Last Coverpoint : ['rs1 : x18', 'rs2 : x15', 'rd : x16', 'rs3 : x17', 'fs1 == 0 and fe1 == 0xf9 and fm1 == 0x323547 and fs2 == 0 and fe2 == 0x83 and fm2 == 0x13634a and fs3 == 0 and fe3 == 0xfd and fm3 == 0x32cc91 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000344]:fmadd.s a6, s2, a5, a7, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:sw a6, 120(ra)
Current Store : [0x80000350] : sw tp, 124(ra) -- Store: [0x8000398c]:0x00000001




Last Coverpoint : ['rs1 : x16', 'rs2 : x17', 'rd : x15', 'rs3 : x18', 'fs1 == 0 and fe1 == 0xf9 and fm1 == 0x323547 and fs2 == 0 and fe2 == 0x83 and fm2 == 0x13634a and fs3 == 0 and fe3 == 0xfd and fm3 == 0x32cc91 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000368]:fmadd.s a5, a6, a7, s2, dyn
	-[0x8000036c]:csrrs tp, fcsr, zero
	-[0x80000370]:sw a5, 128(ra)
Current Store : [0x80000374] : sw tp, 132(ra) -- Store: [0x80003994]:0x00000021




Last Coverpoint : ['rs1 : x13', 'rs2 : x12', 'rd : x14', 'rs3 : x11', 'fs1 == 0 and fe1 == 0xf9 and fm1 == 0x323547 and fs2 == 0 and fe2 == 0x83 and fm2 == 0x13634a and fs3 == 0 and fe3 == 0xfd and fm3 == 0x32cc91 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000038c]:fmadd.s a4, a3, a2, a1, dyn
	-[0x80000390]:csrrs tp, fcsr, zero
	-[0x80000394]:sw a4, 136(ra)
Current Store : [0x80000398] : sw tp, 140(ra) -- Store: [0x8000399c]:0x00000041




Last Coverpoint : ['rs1 : x11', 'rs2 : x14', 'rd : x13', 'rs3 : x12', 'fs1 == 0 and fe1 == 0xf9 and fm1 == 0x323547 and fs2 == 0 and fe2 == 0x83 and fm2 == 0x13634a and fs3 == 0 and fe3 == 0xfd and fm3 == 0x32cc91 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003b0]:fmadd.s a3, a1, a4, a2, dyn
	-[0x800003b4]:csrrs tp, fcsr, zero
	-[0x800003b8]:sw a3, 144(ra)
Current Store : [0x800003bc] : sw tp, 148(ra) -- Store: [0x800039a4]:0x00000061




Last Coverpoint : ['rs1 : x14', 'rs2 : x11', 'rd : x12', 'rs3 : x13', 'fs1 == 0 and fe1 == 0xf9 and fm1 == 0x323547 and fs2 == 0 and fe2 == 0x83 and fm2 == 0x13634a and fs3 == 0 and fe3 == 0xfd and fm3 == 0x32cc91 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003d4]:fmadd.s a2, a4, a1, a3, dyn
	-[0x800003d8]:csrrs tp, fcsr, zero
	-[0x800003dc]:sw a2, 152(ra)
Current Store : [0x800003e0] : sw tp, 156(ra) -- Store: [0x800039ac]:0x00000081




Last Coverpoint : ['rs1 : x12', 'rs2 : x13', 'rd : x11', 'rs3 : x14', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x351fde and fs2 == 0 and fe2 == 0x7d and fm2 == 0x3e843a and fs3 == 0 and fe3 == 0xfd and fm3 == 0x7934ca and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003f8]:fmadd.s a1, a2, a3, a4, dyn
	-[0x800003fc]:csrrs tp, fcsr, zero
	-[0x80000400]:sw a1, 160(ra)
Current Store : [0x80000404] : sw tp, 164(ra) -- Store: [0x800039b4]:0x00000001




Last Coverpoint : ['rs1 : x9', 'rs2 : x8', 'rd : x10', 'rs3 : x7', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x351fde and fs2 == 0 and fe2 == 0x7d and fm2 == 0x3e843a and fs3 == 0 and fe3 == 0xfd and fm3 == 0x7934ca and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000041c]:fmadd.s a0, s1, fp, t2, dyn
	-[0x80000420]:csrrs tp, fcsr, zero
	-[0x80000424]:sw a0, 168(ra)
Current Store : [0x80000428] : sw tp, 172(ra) -- Store: [0x800039bc]:0x00000021




Last Coverpoint : ['rs1 : x7', 'rs2 : x10', 'rd : x9', 'rs3 : x8', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x351fde and fs2 == 0 and fe2 == 0x7d and fm2 == 0x3e843a and fs3 == 0 and fe3 == 0xfd and fm3 == 0x7934ca and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000450]:fmadd.s s1, t2, a0, fp, dyn
	-[0x80000454]:csrrs a4, fcsr, zero
	-[0x80000458]:sw s1, 0(a1)
Current Store : [0x8000045c] : sw a4, 4(a1) -- Store: [0x800039c4]:0x00000041




Last Coverpoint : ['rs1 : x10', 'rs2 : x7', 'rd : x8', 'rs3 : x9', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x351fde and fs2 == 0 and fe2 == 0x7d and fm2 == 0x3e843a and fs3 == 0 and fe3 == 0xfd and fm3 == 0x7934ca and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000474]:fmadd.s fp, a0, t2, s1, dyn
	-[0x80000478]:csrrs a4, fcsr, zero
	-[0x8000047c]:sw fp, 8(a1)
Current Store : [0x80000480] : sw a4, 12(a1) -- Store: [0x800039cc]:0x00000061




Last Coverpoint : ['rs1 : x8', 'rs2 : x9', 'rd : x7', 'rs3 : x10', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x351fde and fs2 == 0 and fe2 == 0x7d and fm2 == 0x3e843a and fs3 == 0 and fe3 == 0xfd and fm3 == 0x7934ca and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000498]:fmadd.s t2, fp, s1, a0, dyn
	-[0x8000049c]:csrrs a4, fcsr, zero
	-[0x800004a0]:sw t2, 16(a1)
Current Store : [0x800004a4] : sw a4, 20(a1) -- Store: [0x800039d4]:0x00000081




Last Coverpoint : ['rs1 : x5', 'rs2 : x4', 'rd : x6', 'rs3 : x3', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x2cf90f and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7bfdd2 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x2de265 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004bc]:fmadd.s t1, t0, tp, gp, dyn
	-[0x800004c0]:csrrs a4, fcsr, zero
	-[0x800004c4]:sw t1, 24(a1)
Current Store : [0x800004c8] : sw a4, 28(a1) -- Store: [0x800039dc]:0x00000001




Last Coverpoint : ['rs1 : x3', 'rs2 : x6', 'rd : x5', 'rs3 : x4', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x2cf90f and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7bfdd2 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x2de265 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004e0]:fmadd.s t0, gp, t1, tp, dyn
	-[0x800004e4]:csrrs a4, fcsr, zero
	-[0x800004e8]:sw t0, 32(a1)
Current Store : [0x800004ec] : sw a4, 36(a1) -- Store: [0x800039e4]:0x00000021




Last Coverpoint : ['rs1 : x6', 'rs2 : x3', 'rd : x4', 'rs3 : x5', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x2cf90f and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7bfdd2 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x2de265 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000504]:fmadd.s tp, t1, gp, t0, dyn
	-[0x80000508]:csrrs a4, fcsr, zero
	-[0x8000050c]:sw tp, 40(a1)
Current Store : [0x80000510] : sw a4, 44(a1) -- Store: [0x800039ec]:0x00000041




Last Coverpoint : ['rs1 : x4', 'rs2 : x5', 'rd : x3', 'rs3 : x6', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x2cf90f and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7bfdd2 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x2de265 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000528]:fmadd.s gp, tp, t0, t1, dyn
	-[0x8000052c]:csrrs a4, fcsr, zero
	-[0x80000530]:sw gp, 48(a1)
Current Store : [0x80000534] : sw a4, 52(a1) -- Store: [0x800039f4]:0x00000061




Last Coverpoint : ['rs1 : x2', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x2cf90f and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7bfdd2 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x2de265 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000054c]:fmadd.s t6, sp, t5, t4, dyn
	-[0x80000550]:csrrs a4, fcsr, zero
	-[0x80000554]:sw t6, 56(a1)
Current Store : [0x80000558] : sw a4, 60(a1) -- Store: [0x800039fc]:0x00000081




Last Coverpoint : ['rs1 : x1', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x341f5c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x4d988d and fs3 == 0 and fe3 == 0xfc and fm3 == 0x3d5e30 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000570]:fmadd.s t6, ra, t5, t4, dyn
	-[0x80000574]:csrrs a4, fcsr, zero
	-[0x80000578]:sw t6, 64(a1)
Current Store : [0x8000057c] : sw a4, 68(a1) -- Store: [0x80003a04]:0x00000001




Last Coverpoint : ['rs1 : x0']
Last Code Sequence : 
	-[0x80000594]:fmadd.s t6, zero, t5, t4, dyn
	-[0x80000598]:csrrs a4, fcsr, zero
	-[0x8000059c]:sw t6, 72(a1)
Current Store : [0x800005a0] : sw a4, 76(a1) -- Store: [0x80003a0c]:0x00000020




Last Coverpoint : ['rs2 : x2', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x341f5c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x4d988d and fs3 == 0 and fe3 == 0xfc and fm3 == 0x3d5e30 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005b8]:fmadd.s t6, t5, sp, t4, dyn
	-[0x800005bc]:csrrs a4, fcsr, zero
	-[0x800005c0]:sw t6, 80(a1)
Current Store : [0x800005c4] : sw a4, 84(a1) -- Store: [0x80003a14]:0x00000041




Last Coverpoint : ['rs2 : x1', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x341f5c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x4d988d and fs3 == 0 and fe3 == 0xfc and fm3 == 0x3d5e30 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005dc]:fmadd.s t6, t5, ra, t4, dyn
	-[0x800005e0]:csrrs a4, fcsr, zero
	-[0x800005e4]:sw t6, 88(a1)
Current Store : [0x800005e8] : sw a4, 92(a1) -- Store: [0x80003a1c]:0x00000061




Last Coverpoint : ['rs2 : x0']
Last Code Sequence : 
	-[0x80000600]:fmadd.s t6, t5, zero, t4, dyn
	-[0x80000604]:csrrs a4, fcsr, zero
	-[0x80000608]:sw t6, 96(a1)
Current Store : [0x8000060c] : sw a4, 100(a1) -- Store: [0x80003a24]:0x00000080




Last Coverpoint : ['rs3 : x2', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x50cddd and fs2 == 0 and fe2 == 0x7e and fm2 == 0x489620 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x5c64b6 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000624]:fmadd.s t6, t5, t4, sp, dyn
	-[0x80000628]:csrrs a4, fcsr, zero
	-[0x8000062c]:sw t6, 104(a1)
Current Store : [0x80000630] : sw a4, 108(a1) -- Store: [0x80003a2c]:0x00000001




Last Coverpoint : ['rs3 : x1', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x50cddd and fs2 == 0 and fe2 == 0x7e and fm2 == 0x489620 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x5c64b6 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000648]:fmadd.s t6, t5, t4, ra, dyn
	-[0x8000064c]:csrrs a4, fcsr, zero
	-[0x80000650]:sw t6, 112(a1)
Current Store : [0x80000654] : sw a4, 116(a1) -- Store: [0x80003a34]:0x00000021




Last Coverpoint : ['rs3 : x0']
Last Code Sequence : 
	-[0x8000066c]:fmadd.s t6, t5, t4, zero, dyn
	-[0x80000670]:csrrs a4, fcsr, zero
	-[0x80000674]:sw t6, 120(a1)
Current Store : [0x80000678] : sw a4, 124(a1) -- Store: [0x80003a3c]:0x00000041




Last Coverpoint : ['rd : x2', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x50cddd and fs2 == 0 and fe2 == 0x7e and fm2 == 0x489620 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x5c64b6 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000690]:fmadd.s sp, t6, t5, t4, dyn
	-[0x80000694]:csrrs a4, fcsr, zero
	-[0x80000698]:sw sp, 128(a1)
Current Store : [0x8000069c] : sw a4, 132(a1) -- Store: [0x80003a44]:0x00000061




Last Coverpoint : ['rd : x1', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x50cddd and fs2 == 0 and fe2 == 0x7e and fm2 == 0x489620 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x5c64b6 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006b4]:fmadd.s ra, t6, t5, t4, dyn
	-[0x800006b8]:csrrs a4, fcsr, zero
	-[0x800006bc]:sw ra, 136(a1)
Current Store : [0x800006c0] : sw a4, 140(a1) -- Store: [0x80003a4c]:0x00000081




Last Coverpoint : ['rd : x0', 'fs1 == 0 and fe1 == 0xf8 and fm1 == 0x2125ab and fs2 == 0 and fe2 == 0x84 and fm2 == 0x6ee85e and fs3 == 0 and fe3 == 0xfc and fm3 == 0x2672cf and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006d8]:fmadd.s zero, t6, t5, t4, dyn
	-[0x800006dc]:csrrs a4, fcsr, zero
	-[0x800006e0]:sw zero, 144(a1)
Current Store : [0x800006e4] : sw a4, 148(a1) -- Store: [0x80003a54]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x2125ab and fs2 == 0 and fe2 == 0x84 and fm2 == 0x6ee85e and fs3 == 0 and fe3 == 0xfc and fm3 == 0x2672cf and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006fc]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000700]:csrrs a4, fcsr, zero
	-[0x80000704]:sw t6, 152(a1)
Current Store : [0x80000708] : sw a4, 156(a1) -- Store: [0x80003a5c]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x2125ab and fs2 == 0 and fe2 == 0x84 and fm2 == 0x6ee85e and fs3 == 0 and fe3 == 0xfc and fm3 == 0x2672cf and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000720]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000724]:csrrs a4, fcsr, zero
	-[0x80000728]:sw t6, 160(a1)
Current Store : [0x8000072c] : sw a4, 164(a1) -- Store: [0x80003a64]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x2125ab and fs2 == 0 and fe2 == 0x84 and fm2 == 0x6ee85e and fs3 == 0 and fe3 == 0xfc and fm3 == 0x2672cf and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000744]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000748]:csrrs a4, fcsr, zero
	-[0x8000074c]:sw t6, 168(a1)
Current Store : [0x80000750] : sw a4, 172(a1) -- Store: [0x80003a6c]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x2125ab and fs2 == 0 and fe2 == 0x84 and fm2 == 0x6ee85e and fs3 == 0 and fe3 == 0xfc and fm3 == 0x2672cf and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000768]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000076c]:csrrs a4, fcsr, zero
	-[0x80000770]:sw t6, 176(a1)
Current Store : [0x80000774] : sw a4, 180(a1) -- Store: [0x80003a74]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x56a477 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x194070 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x2ff034 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000078c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000790]:csrrs a4, fcsr, zero
	-[0x80000794]:sw t6, 184(a1)
Current Store : [0x80000798] : sw a4, 188(a1) -- Store: [0x80003a7c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x56a477 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x194070 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x2ff034 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007b0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800007b4]:csrrs a4, fcsr, zero
	-[0x800007b8]:sw t6, 192(a1)
Current Store : [0x800007bc] : sw a4, 196(a1) -- Store: [0x80003a84]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x56a477 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x194070 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x2ff034 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007d4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800007d8]:csrrs a4, fcsr, zero
	-[0x800007dc]:sw t6, 200(a1)
Current Store : [0x800007e0] : sw a4, 204(a1) -- Store: [0x80003a8c]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x56a477 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x194070 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x2ff034 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007f8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800007fc]:csrrs a4, fcsr, zero
	-[0x80000800]:sw t6, 208(a1)
Current Store : [0x80000804] : sw a4, 212(a1) -- Store: [0x80003a94]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x56a477 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x194070 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x2ff034 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000081c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000820]:csrrs a4, fcsr, zero
	-[0x80000824]:sw t6, 216(a1)
Current Store : [0x80000828] : sw a4, 220(a1) -- Store: [0x80003a9c]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1304ef and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0c7b65 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x7526ed and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000840]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000844]:csrrs a4, fcsr, zero
	-[0x80000848]:sw t6, 224(a1)
Current Store : [0x8000084c] : sw a4, 228(a1) -- Store: [0x80003aa4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1304ef and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0c7b65 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x7526ed and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000864]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000868]:csrrs a4, fcsr, zero
	-[0x8000086c]:sw t6, 232(a1)
Current Store : [0x80000870] : sw a4, 236(a1) -- Store: [0x80003aac]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1304ef and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0c7b65 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x7526ed and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000888]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000088c]:csrrs a4, fcsr, zero
	-[0x80000890]:sw t6, 240(a1)
Current Store : [0x80000894] : sw a4, 244(a1) -- Store: [0x80003ab4]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1304ef and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0c7b65 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x7526ed and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008ac]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800008b0]:csrrs a4, fcsr, zero
	-[0x800008b4]:sw t6, 248(a1)
Current Store : [0x800008b8] : sw a4, 252(a1) -- Store: [0x80003abc]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1304ef and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0c7b65 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x7526ed and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008d0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800008d4]:csrrs a4, fcsr, zero
	-[0x800008d8]:sw t6, 256(a1)
Current Store : [0x800008dc] : sw a4, 260(a1) -- Store: [0x80003ac4]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x190d5e and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0a4a56 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x552591 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008f4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800008f8]:csrrs a4, fcsr, zero
	-[0x800008fc]:sw t6, 264(a1)
Current Store : [0x80000900] : sw a4, 268(a1) -- Store: [0x80003acc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x190d5e and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0a4a56 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x552591 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000918]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000091c]:csrrs a4, fcsr, zero
	-[0x80000920]:sw t6, 272(a1)
Current Store : [0x80000924] : sw a4, 276(a1) -- Store: [0x80003ad4]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x190d5e and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0a4a56 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x552591 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000093c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000940]:csrrs a4, fcsr, zero
	-[0x80000944]:sw t6, 280(a1)
Current Store : [0x80000948] : sw a4, 284(a1) -- Store: [0x80003adc]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x190d5e and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0a4a56 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x552591 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000960]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000964]:csrrs a4, fcsr, zero
	-[0x80000968]:sw t6, 288(a1)
Current Store : [0x8000096c] : sw a4, 292(a1) -- Store: [0x80003ae4]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x190d5e and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0a4a56 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x552591 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000984]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000988]:csrrs a4, fcsr, zero
	-[0x8000098c]:sw t6, 296(a1)
Current Store : [0x80000990] : sw a4, 300(a1) -- Store: [0x80003aec]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6d1a0c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x3509c8 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x429c29 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009a8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800009ac]:csrrs a4, fcsr, zero
	-[0x800009b0]:sw t6, 304(a1)
Current Store : [0x800009b4] : sw a4, 308(a1) -- Store: [0x80003af4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6d1a0c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x3509c8 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x429c29 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009cc]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800009d0]:csrrs a4, fcsr, zero
	-[0x800009d4]:sw t6, 312(a1)
Current Store : [0x800009d8] : sw a4, 316(a1) -- Store: [0x80003afc]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6d1a0c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x3509c8 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x429c29 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009f0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800009f4]:csrrs a4, fcsr, zero
	-[0x800009f8]:sw t6, 320(a1)
Current Store : [0x800009fc] : sw a4, 324(a1) -- Store: [0x80003b04]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6d1a0c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x3509c8 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x429c29 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a14]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000a18]:csrrs a4, fcsr, zero
	-[0x80000a1c]:sw t6, 328(a1)
Current Store : [0x80000a20] : sw a4, 332(a1) -- Store: [0x80003b0c]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6d1a0c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x3509c8 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x429c29 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a38]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000a3c]:csrrs a4, fcsr, zero
	-[0x80000a40]:sw t6, 336(a1)
Current Store : [0x80000a44] : sw a4, 340(a1) -- Store: [0x80003b14]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf5 and fm1 == 0x2e79de and fs2 == 0 and fe2 == 0x85 and fm2 == 0x10e842 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x274f48 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a5c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000a60]:csrrs a4, fcsr, zero
	-[0x80000a64]:sw t6, 344(a1)
Current Store : [0x80000a68] : sw a4, 348(a1) -- Store: [0x80003b1c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf5 and fm1 == 0x2e79de and fs2 == 0 and fe2 == 0x85 and fm2 == 0x10e842 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x274f48 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a80]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000a84]:csrrs a4, fcsr, zero
	-[0x80000a88]:sw t6, 352(a1)
Current Store : [0x80000a8c] : sw a4, 356(a1) -- Store: [0x80003b24]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf5 and fm1 == 0x2e79de and fs2 == 0 and fe2 == 0x85 and fm2 == 0x10e842 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x274f48 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000aa4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000aa8]:csrrs a4, fcsr, zero
	-[0x80000aac]:sw t6, 360(a1)
Current Store : [0x80000ab0] : sw a4, 364(a1) -- Store: [0x80003b2c]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf5 and fm1 == 0x2e79de and fs2 == 0 and fe2 == 0x85 and fm2 == 0x10e842 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x274f48 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ac8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000acc]:csrrs a4, fcsr, zero
	-[0x80000ad0]:sw t6, 368(a1)
Current Store : [0x80000ad4] : sw a4, 372(a1) -- Store: [0x80003b34]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf5 and fm1 == 0x2e79de and fs2 == 0 and fe2 == 0x85 and fm2 == 0x10e842 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x274f48 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000aec]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000af0]:csrrs a4, fcsr, zero
	-[0x80000af4]:sw t6, 376(a1)
Current Store : [0x80000af8] : sw a4, 380(a1) -- Store: [0x80003b3c]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x20dccc and fs2 == 1 and fe2 == 0x7d and fm2 == 0x5d2c9b and fs3 == 0 and fe3 == 0xfc and fm3 == 0x257d51 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b10]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000b14]:csrrs a4, fcsr, zero
	-[0x80000b18]:sw t6, 384(a1)
Current Store : [0x80000b1c] : sw a4, 388(a1) -- Store: [0x80003b44]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x20dccc and fs2 == 1 and fe2 == 0x7d and fm2 == 0x5d2c9b and fs3 == 0 and fe3 == 0xfc and fm3 == 0x257d51 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b34]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000b38]:csrrs a4, fcsr, zero
	-[0x80000b3c]:sw t6, 392(a1)
Current Store : [0x80000b40] : sw a4, 396(a1) -- Store: [0x80003b4c]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x20dccc and fs2 == 1 and fe2 == 0x7d and fm2 == 0x5d2c9b and fs3 == 0 and fe3 == 0xfc and fm3 == 0x257d51 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b58]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000b5c]:csrrs a4, fcsr, zero
	-[0x80000b60]:sw t6, 400(a1)
Current Store : [0x80000b64] : sw a4, 404(a1) -- Store: [0x80003b54]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x20dccc and fs2 == 1 and fe2 == 0x7d and fm2 == 0x5d2c9b and fs3 == 0 and fe3 == 0xfc and fm3 == 0x257d51 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b7c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000b80]:csrrs a4, fcsr, zero
	-[0x80000b84]:sw t6, 408(a1)
Current Store : [0x80000b88] : sw a4, 412(a1) -- Store: [0x80003b5c]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x20dccc and fs2 == 1 and fe2 == 0x7d and fm2 == 0x5d2c9b and fs3 == 0 and fe3 == 0xfc and fm3 == 0x257d51 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ba0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000ba4]:csrrs a4, fcsr, zero
	-[0x80000ba8]:sw t6, 416(a1)
Current Store : [0x80000bac] : sw a4, 420(a1) -- Store: [0x80003b64]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3c9ff5 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x01f3de and fs3 == 0 and fe3 == 0xfe and fm3 == 0x278095 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bc4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000bc8]:csrrs a4, fcsr, zero
	-[0x80000bcc]:sw t6, 424(a1)
Current Store : [0x80000bd0] : sw a4, 428(a1) -- Store: [0x80003b6c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3c9ff5 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x01f3de and fs3 == 0 and fe3 == 0xfe and fm3 == 0x278095 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000be8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000bec]:csrrs a4, fcsr, zero
	-[0x80000bf0]:sw t6, 432(a1)
Current Store : [0x80000bf4] : sw a4, 436(a1) -- Store: [0x80003b74]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3c9ff5 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x01f3de and fs3 == 0 and fe3 == 0xfe and fm3 == 0x278095 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c0c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000c10]:csrrs a4, fcsr, zero
	-[0x80000c14]:sw t6, 440(a1)
Current Store : [0x80000c18] : sw a4, 444(a1) -- Store: [0x80003b7c]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3c9ff5 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x01f3de and fs3 == 0 and fe3 == 0xfe and fm3 == 0x278095 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c30]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000c34]:csrrs a4, fcsr, zero
	-[0x80000c38]:sw t6, 448(a1)
Current Store : [0x80000c3c] : sw a4, 452(a1) -- Store: [0x80003b84]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3c9ff5 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x01f3de and fs3 == 0 and fe3 == 0xfe and fm3 == 0x278095 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c54]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000c58]:csrrs a4, fcsr, zero
	-[0x80000c5c]:sw t6, 456(a1)
Current Store : [0x80000c60] : sw a4, 460(a1) -- Store: [0x80003b8c]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x5e2fb5 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x1d6497 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x389a90 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c78]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000c7c]:csrrs a4, fcsr, zero
	-[0x80000c80]:sw t6, 464(a1)
Current Store : [0x80000c84] : sw a4, 468(a1) -- Store: [0x80003b94]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x5e2fb5 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x1d6497 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x389a90 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c9c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000ca0]:csrrs a4, fcsr, zero
	-[0x80000ca4]:sw t6, 472(a1)
Current Store : [0x80000ca8] : sw a4, 476(a1) -- Store: [0x80003b9c]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x5e2fb5 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x1d6497 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x389a90 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cc0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000cc4]:csrrs a4, fcsr, zero
	-[0x80000cc8]:sw t6, 480(a1)
Current Store : [0x80000ccc] : sw a4, 484(a1) -- Store: [0x80003ba4]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x5e2fb5 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x1d6497 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x389a90 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ce4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000ce8]:csrrs a4, fcsr, zero
	-[0x80000cec]:sw t6, 488(a1)
Current Store : [0x80000cf0] : sw a4, 492(a1) -- Store: [0x80003bac]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x5e2fb5 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x1d6497 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x389a90 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d08]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000d0c]:csrrs a4, fcsr, zero
	-[0x80000d10]:sw t6, 496(a1)
Current Store : [0x80000d14] : sw a4, 500(a1) -- Store: [0x80003bb4]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4f0fd2 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x5c3f35 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0224b8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d2c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000d30]:csrrs a4, fcsr, zero
	-[0x80000d34]:sw t6, 504(a1)
Current Store : [0x80000d38] : sw a4, 508(a1) -- Store: [0x80003bbc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4f0fd2 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x5c3f35 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0224b8 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d50]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000d54]:csrrs a4, fcsr, zero
	-[0x80000d58]:sw t6, 512(a1)
Current Store : [0x80000d5c] : sw a4, 516(a1) -- Store: [0x80003bc4]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4f0fd2 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x5c3f35 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0224b8 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d74]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000d78]:csrrs a4, fcsr, zero
	-[0x80000d7c]:sw t6, 520(a1)
Current Store : [0x80000d80] : sw a4, 524(a1) -- Store: [0x80003bcc]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4f0fd2 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x5c3f35 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0224b8 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d98]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000d9c]:csrrs a4, fcsr, zero
	-[0x80000da0]:sw t6, 528(a1)
Current Store : [0x80000da4] : sw a4, 532(a1) -- Store: [0x80003bd4]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4f0fd2 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x5c3f35 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0224b8 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000dbc]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000dc0]:csrrs a4, fcsr, zero
	-[0x80000dc4]:sw t6, 536(a1)
Current Store : [0x80000dc8] : sw a4, 540(a1) -- Store: [0x80003bdc]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1d454a and fs2 == 1 and fe2 == 0x80 and fm2 == 0x09d19b and fs3 == 0 and fe3 == 0xfe and fm3 == 0x34aad9 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000de0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000de4]:csrrs a4, fcsr, zero
	-[0x80000de8]:sw t6, 544(a1)
Current Store : [0x80000dec] : sw a4, 548(a1) -- Store: [0x80003be4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1d454a and fs2 == 1 and fe2 == 0x80 and fm2 == 0x09d19b and fs3 == 0 and fe3 == 0xfe and fm3 == 0x34aad9 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e04]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000e08]:csrrs a4, fcsr, zero
	-[0x80000e0c]:sw t6, 552(a1)
Current Store : [0x80000e10] : sw a4, 556(a1) -- Store: [0x80003bec]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1d454a and fs2 == 1 and fe2 == 0x80 and fm2 == 0x09d19b and fs3 == 0 and fe3 == 0xfe and fm3 == 0x34aad9 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e28]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000e2c]:csrrs a4, fcsr, zero
	-[0x80000e30]:sw t6, 560(a1)
Current Store : [0x80000e34] : sw a4, 564(a1) -- Store: [0x80003bf4]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1d454a and fs2 == 1 and fe2 == 0x80 and fm2 == 0x09d19b and fs3 == 0 and fe3 == 0xfe and fm3 == 0x34aad9 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e4c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000e50]:csrrs a4, fcsr, zero
	-[0x80000e54]:sw t6, 568(a1)
Current Store : [0x80000e58] : sw a4, 572(a1) -- Store: [0x80003bfc]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1d454a and fs2 == 1 and fe2 == 0x80 and fm2 == 0x09d19b and fs3 == 0 and fe3 == 0xfe and fm3 == 0x34aad9 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e70]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000e74]:csrrs a4, fcsr, zero
	-[0x80000e78]:sw t6, 576(a1)
Current Store : [0x80000e7c] : sw a4, 580(a1) -- Store: [0x80003c04]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x049a6e and fs2 == 1 and fe2 == 0x80 and fm2 == 0x07e718 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x399491 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e94]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000e98]:csrrs a4, fcsr, zero
	-[0x80000e9c]:sw t6, 584(a1)
Current Store : [0x80000ea0] : sw a4, 588(a1) -- Store: [0x80003c0c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x049a6e and fs2 == 1 and fe2 == 0x80 and fm2 == 0x07e718 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x399491 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000eb8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000ebc]:csrrs a4, fcsr, zero
	-[0x80000ec0]:sw t6, 592(a1)
Current Store : [0x80000ec4] : sw a4, 596(a1) -- Store: [0x80003c14]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x049a6e and fs2 == 1 and fe2 == 0x80 and fm2 == 0x07e718 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x399491 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000edc]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000ee0]:csrrs a4, fcsr, zero
	-[0x80000ee4]:sw t6, 600(a1)
Current Store : [0x80000ee8] : sw a4, 604(a1) -- Store: [0x80003c1c]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x049a6e and fs2 == 1 and fe2 == 0x80 and fm2 == 0x07e718 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x399491 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f00]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000f04]:csrrs a4, fcsr, zero
	-[0x80000f08]:sw t6, 608(a1)
Current Store : [0x80000f0c] : sw a4, 612(a1) -- Store: [0x80003c24]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x049a6e and fs2 == 1 and fe2 == 0x80 and fm2 == 0x07e718 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x399491 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f24]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000f28]:csrrs a4, fcsr, zero
	-[0x80000f2c]:sw t6, 616(a1)
Current Store : [0x80000f30] : sw a4, 620(a1) -- Store: [0x80003c2c]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2b9930 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x48da2e and fs3 == 0 and fe3 == 0xfc and fm3 == 0x657849 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f48]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000f4c]:csrrs a4, fcsr, zero
	-[0x80000f50]:sw t6, 624(a1)
Current Store : [0x80000f54] : sw a4, 628(a1) -- Store: [0x80003c34]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2b9930 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x48da2e and fs3 == 0 and fe3 == 0xfc and fm3 == 0x657849 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f6c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000f70]:csrrs a4, fcsr, zero
	-[0x80000f74]:sw t6, 632(a1)
Current Store : [0x80000f78] : sw a4, 636(a1) -- Store: [0x80003c3c]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2b9930 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x48da2e and fs3 == 0 and fe3 == 0xfc and fm3 == 0x657849 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f90]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000f94]:csrrs a4, fcsr, zero
	-[0x80000f98]:sw t6, 640(a1)
Current Store : [0x80000f9c] : sw a4, 644(a1) -- Store: [0x80003c44]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2b9930 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x48da2e and fs3 == 0 and fe3 == 0xfc and fm3 == 0x657849 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fb4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000fb8]:csrrs a4, fcsr, zero
	-[0x80000fbc]:sw t6, 648(a1)
Current Store : [0x80000fc0] : sw a4, 652(a1) -- Store: [0x80003c4c]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2b9930 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x48da2e and fs3 == 0 and fe3 == 0xfc and fm3 == 0x657849 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fd8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80000fdc]:csrrs a4, fcsr, zero
	-[0x80000fe0]:sw t6, 656(a1)
Current Store : [0x80000fe4] : sw a4, 660(a1) -- Store: [0x80003c54]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x0ab269 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x4e9922 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x7ee893 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ffc]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001000]:csrrs a4, fcsr, zero
	-[0x80001004]:sw t6, 664(a1)
Current Store : [0x80001008] : sw a4, 668(a1) -- Store: [0x80003c5c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x0ab269 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x4e9922 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x7ee893 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001020]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001024]:csrrs a4, fcsr, zero
	-[0x80001028]:sw t6, 672(a1)
Current Store : [0x8000102c] : sw a4, 676(a1) -- Store: [0x80003c64]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x0ab269 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x4e9922 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x7ee893 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001044]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001048]:csrrs a4, fcsr, zero
	-[0x8000104c]:sw t6, 680(a1)
Current Store : [0x80001050] : sw a4, 684(a1) -- Store: [0x80003c6c]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x0ab269 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x4e9922 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x7ee893 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001068]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000106c]:csrrs a4, fcsr, zero
	-[0x80001070]:sw t6, 688(a1)
Current Store : [0x80001074] : sw a4, 692(a1) -- Store: [0x80003c74]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x0ab269 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x4e9922 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x7ee893 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000108c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001090]:csrrs a4, fcsr, zero
	-[0x80001094]:sw t6, 696(a1)
Current Store : [0x80001098] : sw a4, 700(a1) -- Store: [0x80003c7c]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x5fcc51 and fs2 == 0 and fe2 == 0x82 and fm2 == 0x547929 and fs3 == 0 and fe3 == 0xfa and fm3 == 0x481bf3 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010b0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800010b4]:csrrs a4, fcsr, zero
	-[0x800010b8]:sw t6, 704(a1)
Current Store : [0x800010bc] : sw a4, 708(a1) -- Store: [0x80003c84]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x5fcc51 and fs2 == 0 and fe2 == 0x82 and fm2 == 0x547929 and fs3 == 0 and fe3 == 0xfa and fm3 == 0x481bf3 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010d4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800010d8]:csrrs a4, fcsr, zero
	-[0x800010dc]:sw t6, 712(a1)
Current Store : [0x800010e0] : sw a4, 716(a1) -- Store: [0x80003c8c]:0x00000025




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x5fcc51 and fs2 == 0 and fe2 == 0x82 and fm2 == 0x547929 and fs3 == 0 and fe3 == 0xfa and fm3 == 0x481bf3 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010f8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800010fc]:csrrs a4, fcsr, zero
	-[0x80001100]:sw t6, 720(a1)
Current Store : [0x80001104] : sw a4, 724(a1) -- Store: [0x80003c94]:0x00000045




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x5fcc51 and fs2 == 0 and fe2 == 0x82 and fm2 == 0x547929 and fs3 == 0 and fe3 == 0xfa and fm3 == 0x481bf3 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000111c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001120]:csrrs a4, fcsr, zero
	-[0x80001124]:sw t6, 728(a1)
Current Store : [0x80001128] : sw a4, 732(a1) -- Store: [0x80003c9c]:0x00000065




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x5fcc51 and fs2 == 0 and fe2 == 0x82 and fm2 == 0x547929 and fs3 == 0 and fe3 == 0xfa and fm3 == 0x481bf3 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001140]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001144]:csrrs a4, fcsr, zero
	-[0x80001148]:sw t6, 736(a1)
Current Store : [0x8000114c] : sw a4, 740(a1) -- Store: [0x80003ca4]:0x00000085




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x67719b and fs2 == 1 and fe2 == 0x82 and fm2 == 0x0a34d7 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x67981f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001164]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001168]:csrrs a4, fcsr, zero
	-[0x8000116c]:sw t6, 744(a1)
Current Store : [0x80001170] : sw a4, 748(a1) -- Store: [0x80003cac]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x67719b and fs2 == 1 and fe2 == 0x82 and fm2 == 0x0a34d7 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x67981f and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001188]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000118c]:csrrs a4, fcsr, zero
	-[0x80001190]:sw t6, 752(a1)
Current Store : [0x80001194] : sw a4, 756(a1) -- Store: [0x80003cb4]:0x00000025




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x67719b and fs2 == 1 and fe2 == 0x82 and fm2 == 0x0a34d7 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x67981f and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011ac]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800011b0]:csrrs a4, fcsr, zero
	-[0x800011b4]:sw t6, 760(a1)
Current Store : [0x800011b8] : sw a4, 764(a1) -- Store: [0x80003cbc]:0x00000045




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x67719b and fs2 == 1 and fe2 == 0x82 and fm2 == 0x0a34d7 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x67981f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011d0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800011d4]:csrrs a4, fcsr, zero
	-[0x800011d8]:sw t6, 768(a1)
Current Store : [0x800011dc] : sw a4, 772(a1) -- Store: [0x80003cc4]:0x00000065




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x67719b and fs2 == 1 and fe2 == 0x82 and fm2 == 0x0a34d7 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x67981f and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011f4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800011f8]:csrrs a4, fcsr, zero
	-[0x800011fc]:sw t6, 776(a1)
Current Store : [0x80001200] : sw a4, 780(a1) -- Store: [0x80003ccc]:0x00000085




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf6 and fm1 == 0x26497d and fs2 == 0 and fe2 == 0x88 and fm2 == 0x757d05 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0229d2 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001218]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000121c]:csrrs a4, fcsr, zero
	-[0x80001220]:sw t6, 784(a1)
Current Store : [0x80001224] : sw a4, 788(a1) -- Store: [0x80003cd4]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf6 and fm1 == 0x26497d and fs2 == 0 and fe2 == 0x88 and fm2 == 0x757d05 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0229d2 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000123c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001240]:csrrs a4, fcsr, zero
	-[0x80001244]:sw t6, 792(a1)
Current Store : [0x80001248] : sw a4, 796(a1) -- Store: [0x80003cdc]:0x00000025




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf6 and fm1 == 0x26497d and fs2 == 0 and fe2 == 0x88 and fm2 == 0x757d05 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0229d2 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001260]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001264]:csrrs a4, fcsr, zero
	-[0x80001268]:sw t6, 800(a1)
Current Store : [0x8000126c] : sw a4, 804(a1) -- Store: [0x80003ce4]:0x00000045




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf6 and fm1 == 0x26497d and fs2 == 0 and fe2 == 0x88 and fm2 == 0x757d05 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0229d2 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001284]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001288]:csrrs a4, fcsr, zero
	-[0x8000128c]:sw t6, 808(a1)
Current Store : [0x80001290] : sw a4, 812(a1) -- Store: [0x80003cec]:0x00000065




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf6 and fm1 == 0x26497d and fs2 == 0 and fe2 == 0x88 and fm2 == 0x757d05 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0229d2 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012a8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800012ac]:csrrs a4, fcsr, zero
	-[0x800012b0]:sw t6, 816(a1)
Current Store : [0x800012b4] : sw a4, 820(a1) -- Store: [0x80003cf4]:0x00000085




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x01c09c and fs2 == 1 and fe2 == 0x82 and fm2 == 0x4c157d and fs3 == 0 and fe3 == 0xfc and fm3 == 0x6e0c34 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012cc]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800012d0]:csrrs a4, fcsr, zero
	-[0x800012d4]:sw t6, 824(a1)
Current Store : [0x800012d8] : sw a4, 828(a1) -- Store: [0x80003cfc]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x01c09c and fs2 == 1 and fe2 == 0x82 and fm2 == 0x4c157d and fs3 == 0 and fe3 == 0xfc and fm3 == 0x6e0c34 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012f0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800012f4]:csrrs a4, fcsr, zero
	-[0x800012f8]:sw t6, 832(a1)
Current Store : [0x800012fc] : sw a4, 836(a1) -- Store: [0x80003d04]:0x00000025




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x01c09c and fs2 == 1 and fe2 == 0x82 and fm2 == 0x4c157d and fs3 == 0 and fe3 == 0xfc and fm3 == 0x6e0c34 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001314]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001318]:csrrs a4, fcsr, zero
	-[0x8000131c]:sw t6, 840(a1)
Current Store : [0x80001320] : sw a4, 844(a1) -- Store: [0x80003d0c]:0x00000045




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x01c09c and fs2 == 1 and fe2 == 0x82 and fm2 == 0x4c157d and fs3 == 0 and fe3 == 0xfc and fm3 == 0x6e0c34 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001338]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000133c]:csrrs a4, fcsr, zero
	-[0x80001340]:sw t6, 848(a1)
Current Store : [0x80001344] : sw a4, 852(a1) -- Store: [0x80003d14]:0x00000065




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x01c09c and fs2 == 1 and fe2 == 0x82 and fm2 == 0x4c157d and fs3 == 0 and fe3 == 0xfc and fm3 == 0x6e0c34 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000135c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001360]:csrrs a4, fcsr, zero
	-[0x80001364]:sw t6, 856(a1)
Current Store : [0x80001368] : sw a4, 860(a1) -- Store: [0x80003d1c]:0x00000085




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d27cc and fs2 == 0 and fe2 == 0x82 and fm2 == 0x12fb98 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x389b2c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001380]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001384]:csrrs a4, fcsr, zero
	-[0x80001388]:sw t6, 864(a1)
Current Store : [0x8000138c] : sw a4, 868(a1) -- Store: [0x80003d24]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d27cc and fs2 == 0 and fe2 == 0x82 and fm2 == 0x12fb98 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x389b2c and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013a4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800013a8]:csrrs a4, fcsr, zero
	-[0x800013ac]:sw t6, 872(a1)
Current Store : [0x800013b0] : sw a4, 876(a1) -- Store: [0x80003d2c]:0x00000025




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d27cc and fs2 == 0 and fe2 == 0x82 and fm2 == 0x12fb98 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x389b2c and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013c8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800013cc]:csrrs a4, fcsr, zero
	-[0x800013d0]:sw t6, 880(a1)
Current Store : [0x800013d4] : sw a4, 884(a1) -- Store: [0x80003d34]:0x00000045




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d27cc and fs2 == 0 and fe2 == 0x82 and fm2 == 0x12fb98 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x389b2c and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013ec]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800013f0]:csrrs a4, fcsr, zero
	-[0x800013f4]:sw t6, 888(a1)
Current Store : [0x800013f8] : sw a4, 892(a1) -- Store: [0x80003d3c]:0x00000065




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d27cc and fs2 == 0 and fe2 == 0x82 and fm2 == 0x12fb98 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x389b2c and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001410]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001414]:csrrs a4, fcsr, zero
	-[0x80001418]:sw t6, 896(a1)
Current Store : [0x8000141c] : sw a4, 900(a1) -- Store: [0x80003d44]:0x00000085




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x728772 and fs2 == 1 and fe2 == 0x83 and fm2 == 0x56dae4 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x38c9b0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001434]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001438]:csrrs a4, fcsr, zero
	-[0x8000143c]:sw t6, 904(a1)
Current Store : [0x80001440] : sw a4, 908(a1) -- Store: [0x80003d4c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x728772 and fs2 == 1 and fe2 == 0x83 and fm2 == 0x56dae4 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x38c9b0 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001458]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000145c]:csrrs a4, fcsr, zero
	-[0x80001460]:sw t6, 912(a1)
Current Store : [0x80001464] : sw a4, 916(a1) -- Store: [0x80003d54]:0x00000025




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x728772 and fs2 == 1 and fe2 == 0x83 and fm2 == 0x56dae4 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x38c9b0 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000147c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001480]:csrrs a4, fcsr, zero
	-[0x80001484]:sw t6, 920(a1)
Current Store : [0x80001488] : sw a4, 924(a1) -- Store: [0x80003d5c]:0x00000045




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x728772 and fs2 == 1 and fe2 == 0x83 and fm2 == 0x56dae4 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x38c9b0 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014a0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800014a4]:csrrs a4, fcsr, zero
	-[0x800014a8]:sw t6, 928(a1)
Current Store : [0x800014ac] : sw a4, 932(a1) -- Store: [0x80003d64]:0x00000065




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x728772 and fs2 == 1 and fe2 == 0x83 and fm2 == 0x56dae4 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x38c9b0 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014c4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800014c8]:csrrs a4, fcsr, zero
	-[0x800014cc]:sw t6, 936(a1)
Current Store : [0x800014d0] : sw a4, 940(a1) -- Store: [0x80003d6c]:0x00000085




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6f31b9 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x543d9b and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0e6c50 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014e8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800014ec]:csrrs a4, fcsr, zero
	-[0x800014f0]:sw t6, 944(a1)
Current Store : [0x800014f4] : sw a4, 948(a1) -- Store: [0x80003d74]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6f31b9 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x543d9b and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0e6c50 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000150c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001510]:csrrs a4, fcsr, zero
	-[0x80001514]:sw t6, 952(a1)
Current Store : [0x80001518] : sw a4, 956(a1) -- Store: [0x80003d7c]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6f31b9 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x543d9b and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0e6c50 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001530]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001534]:csrrs a4, fcsr, zero
	-[0x80001538]:sw t6, 960(a1)
Current Store : [0x8000153c] : sw a4, 964(a1) -- Store: [0x80003d84]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6f31b9 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x543d9b and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0e6c50 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001554]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001558]:csrrs a4, fcsr, zero
	-[0x8000155c]:sw t6, 968(a1)
Current Store : [0x80001560] : sw a4, 972(a1) -- Store: [0x80003d8c]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x18ad5a and fs2 == 0 and fe2 == 0x7b and fm2 == 0x1437ba and fs3 == 0 and fe3 == 0xfe and fm3 == 0x34f350 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001578]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000157c]:csrrs a4, fcsr, zero
	-[0x80001580]:sw t6, 976(a1)
Current Store : [0x80001584] : sw a4, 980(a1) -- Store: [0x80003d94]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x18ad5a and fs2 == 0 and fe2 == 0x7b and fm2 == 0x1437ba and fs3 == 0 and fe3 == 0xfe and fm3 == 0x34f350 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000159c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800015a0]:csrrs a4, fcsr, zero
	-[0x800015a4]:sw t6, 984(a1)
Current Store : [0x800015a8] : sw a4, 988(a1) -- Store: [0x80003d9c]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x18ad5a and fs2 == 0 and fe2 == 0x7b and fm2 == 0x1437ba and fs3 == 0 and fe3 == 0xfe and fm3 == 0x34f350 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015c0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800015c4]:csrrs a4, fcsr, zero
	-[0x800015c8]:sw t6, 992(a1)
Current Store : [0x800015cc] : sw a4, 996(a1) -- Store: [0x80003da4]:0x00000041




Last Coverpoint : ['mnemonic : fmadd.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs3 : x28', 'rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x18ad5a and fs2 == 0 and fe2 == 0x7b and fm2 == 0x1437ba and fs3 == 0 and fe3 == 0xfe and fm3 == 0x34f350 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015e4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800015e8]:csrrs a4, fcsr, zero
	-[0x800015ec]:sw t6, 1000(a1)
Current Store : [0x800015f0] : sw a4, 1004(a1) -- Store: [0x80003dac]:0x00000061




Last Coverpoint : ['mnemonic : fmadd.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs3 : x28', 'rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x18ad5a and fs2 == 0 and fe2 == 0x7b and fm2 == 0x1437ba and fs3 == 0 and fe3 == 0xfe and fm3 == 0x34f350 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001608]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000160c]:csrrs a4, fcsr, zero
	-[0x80001610]:sw t6, 1008(a1)
Current Store : [0x80001614] : sw a4, 1012(a1) -- Store: [0x80003db4]:0x00000081




Last Coverpoint : ['mnemonic : fmadd.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs3 : x28', 'rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x0e0f7e and fs2 == 0 and fe2 == 0x7b and fm2 == 0x570014 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x31161d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000162c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001630]:csrrs a4, fcsr, zero
	-[0x80001634]:sw t6, 1016(a1)
Current Store : [0x80001638] : sw a4, 1020(a1) -- Store: [0x80003dbc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0e0f7e and fs2 == 0 and fe2 == 0x7b and fm2 == 0x570014 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x31161d and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001658]:fmadd.s t6, t5, t4, t3, dyn
	-[0x8000165c]:csrrs a4, fcsr, zero
	-[0x80001660]:sw t6, 0(a1)
Current Store : [0x80001664] : sw a4, 4(a1) -- Store: [0x80003dc4]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x341f5c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x4d988d and fs3 == 0 and fe3 == 0xfc and fm3 == 0x3d5e30 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000167c]:fmadd.s t6, t5, t4, t3, dyn
	-[0x80001680]:csrrs a4, fcsr, zero
	-[0x80001684]:sw t6, 8(a1)
Current Store : [0x80001688] : sw a4, 12(a1) -- Store: [0x80003dcc]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x341f5c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x4d988d and fs3 == 0 and fe3 == 0xfc and fm3 == 0x3d5e30 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016a0]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800016a4]:csrrs a4, fcsr, zero
	-[0x800016a8]:sw t6, 16(a1)
Current Store : [0x800016ac] : sw a4, 20(a1) -- Store: [0x80003dd4]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x50cddd and fs2 == 0 and fe2 == 0x7e and fm2 == 0x489620 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x5c64b6 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016c4]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800016c8]:csrrs a4, fcsr, zero
	-[0x800016cc]:sw t6, 24(a1)
Current Store : [0x800016d0] : sw a4, 28(a1) -- Store: [0x80003ddc]:0x00000041




Last Coverpoint : ['mnemonic : fmadd.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs3 : x28', 'rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd', 'fs1 == 0 and fe1 == 0xf8 and fm1 == 0x2125ab and fs2 == 0 and fe2 == 0x84 and fm2 == 0x6ee85e and fs3 == 0 and fe3 == 0xfc and fm3 == 0x2672cf and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016e8]:fmadd.s t6, t5, t4, t3, dyn
	-[0x800016ec]:csrrs a4, fcsr, zero
	-[0x800016f0]:sw t6, 32(a1)
Current Store : [0x800016f4] : sw a4, 36(a1) -- Store: [0x80003de4]:0x00000001





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                                                                                      coverpoints                                                                                                                                                                       |                                                         code                                                          |
|---:|--------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------|
|   1|[0x80003910]<br>0x7F800000|- mnemonic : fmadd.s<br> - rs1 : x30<br> - rs2 : x30<br> - rd : x31<br> - rs3 : x29<br> - rs1 == rs2 != rs3 and rs1 == rs2 != rd and rd != rs3<br>                                                                                                                                                                                                      |[0x80000128]:fmadd.s t6, t5, t5, t4, dyn<br> [0x8000012c]:csrrs tp, fcsr, zero<br> [0x80000130]:sw t6, 0(ra)<br>       |
|   2|[0x80003918]<br>0x7F3FFFFF|- rs1 : x31<br> - rs2 : x29<br> - rd : x30<br> - rs3 : x28<br> - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x6f31b9 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x543d9b and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0e6c50 and  fcsr == 0x20 and rm_val == 7   #nosat<br> |[0x8000014c]:fmadd.s t5, t6, t4, t3, dyn<br> [0x80000150]:csrrs tp, fcsr, zero<br> [0x80000154]:sw t5, 8(ra)<br>       |
|   3|[0x80003920]<br>0x7F7FFFFF|- rs1 : x28<br> - rs2 : x28<br> - rd : x28<br> - rs3 : x31<br> - rs1 == rs2 == rd != rs3<br>                                                                                                                                                                                                                                                            |[0x80000170]:fmadd.s t3, t3, t3, t6, dyn<br> [0x80000174]:csrrs tp, fcsr, zero<br> [0x80000178]:sw t3, 16(ra)<br>      |
|   4|[0x80003928]<br>0x7E464EBD|- rs1 : x29<br> - rs2 : x27<br> - rd : x27<br> - rs3 : x27<br> - rd == rs2 == rs3 != rs1<br>                                                                                                                                                                                                                                                            |[0x80000194]:fmadd.s s11, t4, s11, s11, dyn<br> [0x80000198]:csrrs tp, fcsr, zero<br> [0x8000019c]:sw s11, 24(ra)<br>  |
|   5|[0x80003930]<br>0x7E464EBD|- rs1 : x27<br> - rs2 : x26<br> - rd : x29<br> - rs3 : x26<br> - rs2 == rs3 != rs1 and rs2 == rs3 != rd and rd != rs1<br>                                                                                                                                                                                                                               |[0x800001b8]:fmadd.s t4, s11, s10, s10, dyn<br> [0x800001bc]:csrrs tp, fcsr, zero<br> [0x800001c0]:sw t4, 32(ra)<br>   |
|   6|[0x80003938]<br>0x7F800000|- rs1 : x25<br> - rs2 : x25<br> - rd : x26<br> - rs3 : x25<br> - rs1 == rs2 == rs3 != rd<br>                                                                                                                                                                                                                                                            |[0x800001dc]:fmadd.s s10, s9, s9, s9, dyn<br> [0x800001e0]:csrrs tp, fcsr, zero<br> [0x800001e4]:sw s10, 40(ra)<br>    |
|   7|[0x80003940]<br>0x7F7FFFFF|- rs1 : x24<br> - rs2 : x24<br> - rd : x24<br> - rs3 : x24<br> - rs1 == rs2 == rs3 == rd<br>                                                                                                                                                                                                                                                            |[0x80000200]:fmadd.s s8, s8, s8, s8, dyn<br> [0x80000204]:csrrs tp, fcsr, zero<br> [0x80000208]:sw s8, 48(ra)<br>      |
|   8|[0x80003948]<br>0x7F23BA08|- rs1 : x23<br> - rs2 : x31<br> - rd : x25<br> - rs3 : x23<br> - rs1 == rs3 != rs2 and rs1 == rs3 != rd and rd != rs2<br>                                                                                                                                                                                                                               |[0x80000224]:fmadd.s s9, s7, t6, s7, dyn<br> [0x80000228]:csrrs tp, fcsr, zero<br> [0x8000022c]:sw s9, 56(ra)<br>      |
|   9|[0x80003950]<br>0x7F3FFFFF|- rs1 : x22<br> - rs2 : x23<br> - rd : x22<br> - rs3 : x30<br> - rs1 == rd != rs2 and rs1 == rd != rs3 and rs3 != rs2<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x18ad5a and fs2 == 0 and fe2 == 0x7b and fm2 == 0x1437ba and fs3 == 0 and fe3 == 0xfe and fm3 == 0x34f350 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                               |[0x80000248]:fmadd.s s6, s6, s7, t5, dyn<br> [0x8000024c]:csrrs tp, fcsr, zero<br> [0x80000250]:sw s6, 64(ra)<br>      |
|  10|[0x80003958]<br>0x7F3FFFFF|- rs1 : x26<br> - rs2 : x21<br> - rd : x21<br> - rs3 : x22<br> - rs2 == rd != rs1 and rs2 == rd != rs3 and rs3 != rs1<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x18ad5a and fs2 == 0 and fe2 == 0x7b and fm2 == 0x1437ba and fs3 == 0 and fe3 == 0xfe and fm3 == 0x34f350 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                               |[0x8000026c]:fmadd.s s5, s10, s5, s6, dyn<br> [0x80000270]:csrrs tp, fcsr, zero<br> [0x80000274]:sw s5, 72(ra)<br>     |
|  11|[0x80003960]<br>0x7F3FFFFF|- rs1 : x21<br> - rs2 : x22<br> - rd : x20<br> - rs3 : x20<br> - rs3 == rd != rs1 and rs3 == rd != rs2 and rs2 != rs1<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x0e0f7e and fs2 == 0 and fe2 == 0x7b and fm2 == 0x570014 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x31161d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                |[0x80000290]:fmadd.s s4, s5, s6, s4, dyn<br> [0x80000294]:csrrs tp, fcsr, zero<br> [0x80000298]:sw s4, 80(ra)<br>      |
|  12|[0x80003968]<br>0x7F1CF95F|- rs1 : x19<br> - rs2 : x20<br> - rd : x19<br> - rs3 : x19<br> - rs1 == rd == rs3 != rs2<br>                                                                                                                                                                                                                                                            |[0x800002b4]:fmadd.s s3, s3, s4, s3, dyn<br> [0x800002b8]:csrrs tp, fcsr, zero<br> [0x800002bc]:sw s3, 88(ra)<br>      |
|  13|[0x80003970]<br>0x7F3FFFFE|- rs1 : x20<br> - rs2 : x19<br> - rd : x23<br> - rs3 : x21<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x0e0f7e and fs2 == 0 and fe2 == 0x7b and fm2 == 0x570014 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x31161d and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                          |[0x800002d8]:fmadd.s s7, s4, s3, s5, dyn<br> [0x800002dc]:csrrs tp, fcsr, zero<br> [0x800002e0]:sw s7, 96(ra)<br>      |
|  14|[0x80003978]<br>0x7F3FFFFF|- rs1 : x17<br> - rs2 : x16<br> - rd : x18<br> - rs3 : x15<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x0e0f7e and fs2 == 0 and fe2 == 0x7b and fm2 == 0x570014 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x31161d and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                          |[0x800002fc]:fmadd.s s2, a7, a6, a5, dyn<br> [0x80000300]:csrrs tp, fcsr, zero<br> [0x80000304]:sw s2, 104(ra)<br>     |
|  15|[0x80003980]<br>0x7F3FFFFF|- rs1 : x15<br> - rs2 : x18<br> - rd : x17<br> - rs3 : x16<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x0e0f7e and fs2 == 0 and fe2 == 0x7b and fm2 == 0x570014 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x31161d and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                          |[0x80000320]:fmadd.s a7, a5, s2, a6, dyn<br> [0x80000324]:csrrs tp, fcsr, zero<br> [0x80000328]:sw a7, 112(ra)<br>     |
|  16|[0x80003988]<br>0x7F3FFFFE|- rs1 : x18<br> - rs2 : x15<br> - rd : x16<br> - rs3 : x17<br> - fs1 == 0 and fe1 == 0xf9 and fm1 == 0x323547 and fs2 == 0 and fe2 == 0x83 and fm2 == 0x13634a and fs3 == 0 and fe3 == 0xfd and fm3 == 0x32cc91 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                           |[0x80000344]:fmadd.s a6, s2, a5, a7, dyn<br> [0x80000348]:csrrs tp, fcsr, zero<br> [0x8000034c]:sw a6, 120(ra)<br>     |
|  17|[0x80003990]<br>0x7F3FFFFE|- rs1 : x16<br> - rs2 : x17<br> - rd : x15<br> - rs3 : x18<br> - fs1 == 0 and fe1 == 0xf9 and fm1 == 0x323547 and fs2 == 0 and fe2 == 0x83 and fm2 == 0x13634a and fs3 == 0 and fe3 == 0xfd and fm3 == 0x32cc91 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                          |[0x80000368]:fmadd.s a5, a6, a7, s2, dyn<br> [0x8000036c]:csrrs tp, fcsr, zero<br> [0x80000370]:sw a5, 128(ra)<br>     |
|  18|[0x80003998]<br>0x7F3FFFFE|- rs1 : x13<br> - rs2 : x12<br> - rd : x14<br> - rs3 : x11<br> - fs1 == 0 and fe1 == 0xf9 and fm1 == 0x323547 and fs2 == 0 and fe2 == 0x83 and fm2 == 0x13634a and fs3 == 0 and fe3 == 0xfd and fm3 == 0x32cc91 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                          |[0x8000038c]:fmadd.s a4, a3, a2, a1, dyn<br> [0x80000390]:csrrs tp, fcsr, zero<br> [0x80000394]:sw a4, 136(ra)<br>     |
|  19|[0x800039a0]<br>0x7F3FFFFF|- rs1 : x11<br> - rs2 : x14<br> - rd : x13<br> - rs3 : x12<br> - fs1 == 0 and fe1 == 0xf9 and fm1 == 0x323547 and fs2 == 0 and fe2 == 0x83 and fm2 == 0x13634a and fs3 == 0 and fe3 == 0xfd and fm3 == 0x32cc91 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                          |[0x800003b0]:fmadd.s a3, a1, a4, a2, dyn<br> [0x800003b4]:csrrs tp, fcsr, zero<br> [0x800003b8]:sw a3, 144(ra)<br>     |
|  20|[0x800039a8]<br>0x7F3FFFFE|- rs1 : x14<br> - rs2 : x11<br> - rd : x12<br> - rs3 : x13<br> - fs1 == 0 and fe1 == 0xf9 and fm1 == 0x323547 and fs2 == 0 and fe2 == 0x83 and fm2 == 0x13634a and fs3 == 0 and fe3 == 0xfd and fm3 == 0x32cc91 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                          |[0x800003d4]:fmadd.s a2, a4, a1, a3, dyn<br> [0x800003d8]:csrrs tp, fcsr, zero<br> [0x800003dc]:sw a2, 152(ra)<br>     |
|  21|[0x800039b0]<br>0x7F3FFFFF|- rs1 : x12<br> - rs2 : x13<br> - rd : x11<br> - rs3 : x14<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x351fde and fs2 == 0 and fe2 == 0x7d and fm2 == 0x3e843a and fs3 == 0 and fe3 == 0xfd and fm3 == 0x7934ca and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                           |[0x800003f8]:fmadd.s a1, a2, a3, a4, dyn<br> [0x800003fc]:csrrs tp, fcsr, zero<br> [0x80000400]:sw a1, 160(ra)<br>     |
|  22|[0x800039b8]<br>0x7F3FFFFF|- rs1 : x9<br> - rs2 : x8<br> - rd : x10<br> - rs3 : x7<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x351fde and fs2 == 0 and fe2 == 0x7d and fm2 == 0x3e843a and fs3 == 0 and fe3 == 0xfd and fm3 == 0x7934ca and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                             |[0x8000041c]:fmadd.s a0, s1, fp, t2, dyn<br> [0x80000420]:csrrs tp, fcsr, zero<br> [0x80000424]:sw a0, 168(ra)<br>     |
|  23|[0x800039c0]<br>0x7F3FFFFF|- rs1 : x7<br> - rs2 : x10<br> - rd : x9<br> - rs3 : x8<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x351fde and fs2 == 0 and fe2 == 0x7d and fm2 == 0x3e843a and fs3 == 0 and fe3 == 0xfd and fm3 == 0x7934ca and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                             |[0x80000450]:fmadd.s s1, t2, a0, fp, dyn<br> [0x80000454]:csrrs a4, fcsr, zero<br> [0x80000458]:sw s1, 0(a1)<br>       |
|  24|[0x800039c8]<br>0x7F400000|- rs1 : x10<br> - rs2 : x7<br> - rd : x8<br> - rs3 : x9<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x351fde and fs2 == 0 and fe2 == 0x7d and fm2 == 0x3e843a and fs3 == 0 and fe3 == 0xfd and fm3 == 0x7934ca and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                             |[0x80000474]:fmadd.s fp, a0, t2, s1, dyn<br> [0x80000478]:csrrs a4, fcsr, zero<br> [0x8000047c]:sw fp, 8(a1)<br>       |
|  25|[0x800039d0]<br>0x7F3FFFFF|- rs1 : x8<br> - rs2 : x9<br> - rd : x7<br> - rs3 : x10<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x351fde and fs2 == 0 and fe2 == 0x7d and fm2 == 0x3e843a and fs3 == 0 and fe3 == 0xfd and fm3 == 0x7934ca and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                             |[0x80000498]:fmadd.s t2, fp, s1, a0, dyn<br> [0x8000049c]:csrrs a4, fcsr, zero<br> [0x800004a0]:sw t2, 16(a1)<br>      |
|  26|[0x800039d8]<br>0x7F3FFFFE|- rs1 : x5<br> - rs2 : x4<br> - rd : x6<br> - rs3 : x3<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x2cf90f and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7bfdd2 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x2de265 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800004bc]:fmadd.s t1, t0, tp, gp, dyn<br> [0x800004c0]:csrrs a4, fcsr, zero<br> [0x800004c4]:sw t1, 24(a1)<br>      |
|  27|[0x800039e0]<br>0x7F3FFFFE|- rs1 : x3<br> - rs2 : x6<br> - rd : x5<br> - rs3 : x4<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x2cf90f and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7bfdd2 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x2de265 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800004e0]:fmadd.s t0, gp, t1, tp, dyn<br> [0x800004e4]:csrrs a4, fcsr, zero<br> [0x800004e8]:sw t0, 32(a1)<br>      |
|  28|[0x800039e8]<br>0x7F3FFFFE|- rs1 : x6<br> - rs2 : x3<br> - rd : x4<br> - rs3 : x5<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x2cf90f and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7bfdd2 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x2de265 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000504]:fmadd.s tp, t1, gp, t0, dyn<br> [0x80000508]:csrrs a4, fcsr, zero<br> [0x8000050c]:sw tp, 40(a1)<br>      |
|  29|[0x800039f0]<br>0x7F3FFFFF|- rs1 : x4<br> - rs2 : x5<br> - rd : x3<br> - rs3 : x6<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x2cf90f and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7bfdd2 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x2de265 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000528]:fmadd.s gp, tp, t0, t1, dyn<br> [0x8000052c]:csrrs a4, fcsr, zero<br> [0x80000530]:sw gp, 48(a1)<br>      |
|  30|[0x800039f8]<br>0x7F3FFFFE|- rs1 : x2<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x2cf90f and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7bfdd2 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x2de265 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                                          |[0x8000054c]:fmadd.s t6, sp, t5, t4, dyn<br> [0x80000550]:csrrs a4, fcsr, zero<br> [0x80000554]:sw t6, 56(a1)<br>      |
|  31|[0x80003a00]<br>0x7F3FFFFE|- rs1 : x1<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x341f5c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x4d988d and fs3 == 0 and fe3 == 0xfc and fm3 == 0x3d5e30 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                           |[0x80000570]:fmadd.s t6, ra, t5, t4, dyn<br> [0x80000574]:csrrs a4, fcsr, zero<br> [0x80000578]:sw t6, 64(a1)<br>      |
|  32|[0x80003a08]<br>0x7E3D5E30|- rs1 : x0<br>                                                                                                                                                                                                                                                                                                                                          |[0x80000594]:fmadd.s t6, zero, t5, t4, dyn<br> [0x80000598]:csrrs a4, fcsr, zero<br> [0x8000059c]:sw t6, 72(a1)<br>    |
|  33|[0x80003a10]<br>0x7F3FFFFE|- rs2 : x2<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x341f5c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x4d988d and fs3 == 0 and fe3 == 0xfc and fm3 == 0x3d5e30 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                                          |[0x800005b8]:fmadd.s t6, t5, sp, t4, dyn<br> [0x800005bc]:csrrs a4, fcsr, zero<br> [0x800005c0]:sw t6, 80(a1)<br>      |
|  34|[0x80003a18]<br>0x7F3FFFFF|- rs2 : x1<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x341f5c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x4d988d and fs3 == 0 and fe3 == 0xfc and fm3 == 0x3d5e30 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                                          |[0x800005dc]:fmadd.s t6, t5, ra, t4, dyn<br> [0x800005e0]:csrrs a4, fcsr, zero<br> [0x800005e4]:sw t6, 88(a1)<br>      |
|  35|[0x80003a20]<br>0x7E3D5E30|- rs2 : x0<br>                                                                                                                                                                                                                                                                                                                                          |[0x80000600]:fmadd.s t6, t5, zero, t4, dyn<br> [0x80000604]:csrrs a4, fcsr, zero<br> [0x80000608]:sw t6, 96(a1)<br>    |
|  36|[0x80003a28]<br>0x7F3FFFFF|- rs3 : x2<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x50cddd and fs2 == 0 and fe2 == 0x7e and fm2 == 0x489620 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x5c64b6 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                           |[0x80000624]:fmadd.s t6, t5, t4, sp, dyn<br> [0x80000628]:csrrs a4, fcsr, zero<br> [0x8000062c]:sw t6, 104(a1)<br>     |
|  37|[0x80003a30]<br>0x7F3FFFFE|- rs3 : x1<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x50cddd and fs2 == 0 and fe2 == 0x7e and fm2 == 0x489620 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x5c64b6 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                                          |[0x80000648]:fmadd.s t6, t5, t4, ra, dyn<br> [0x8000064c]:csrrs a4, fcsr, zero<br> [0x80000650]:sw t6, 112(a1)<br>     |
|  38|[0x80003a38]<br>0x7EA39B47|- rs3 : x0<br>                                                                                                                                                                                                                                                                                                                                          |[0x8000066c]:fmadd.s t6, t5, t4, zero, dyn<br> [0x80000670]:csrrs a4, fcsr, zero<br> [0x80000674]:sw t6, 120(a1)<br>   |
|  39|[0x80003a40]<br>0x7F3FFFFF|- rd : x2<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x50cddd and fs2 == 0 and fe2 == 0x7e and fm2 == 0x489620 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x5c64b6 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                                           |[0x80000690]:fmadd.s sp, t6, t5, t4, dyn<br> [0x80000694]:csrrs a4, fcsr, zero<br> [0x80000698]:sw sp, 128(a1)<br>     |
|  40|[0x80003a48]<br>0x7F3FFFFF|- rd : x1<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x50cddd and fs2 == 0 and fe2 == 0x7e and fm2 == 0x489620 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x5c64b6 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                                           |[0x800006b4]:fmadd.s ra, t6, t5, t4, dyn<br> [0x800006b8]:csrrs a4, fcsr, zero<br> [0x800006bc]:sw ra, 136(a1)<br>     |
|  41|[0x80003a50]<br>0x00000000|- rd : x0<br> - fs1 == 0 and fe1 == 0xf8 and fm1 == 0x2125ab and fs2 == 0 and fe2 == 0x84 and fm2 == 0x6ee85e and fs3 == 0 and fe3 == 0xfc and fm3 == 0x2672cf and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                            |[0x800006d8]:fmadd.s zero, t6, t5, t4, dyn<br> [0x800006dc]:csrrs a4, fcsr, zero<br> [0x800006e0]:sw zero, 144(a1)<br> |
|  42|[0x80003a58]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x2125ab and fs2 == 0 and fe2 == 0x84 and fm2 == 0x6ee85e and fs3 == 0 and fe3 == 0xfc and fm3 == 0x2672cf and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800006fc]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000700]:csrrs a4, fcsr, zero<br> [0x80000704]:sw t6, 152(a1)<br>     |
|  43|[0x80003a60]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x2125ab and fs2 == 0 and fe2 == 0x84 and fm2 == 0x6ee85e and fs3 == 0 and fe3 == 0xfc and fm3 == 0x2672cf and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000720]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000724]:csrrs a4, fcsr, zero<br> [0x80000728]:sw t6, 160(a1)<br>     |
|  44|[0x80003a68]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x2125ab and fs2 == 0 and fe2 == 0x84 and fm2 == 0x6ee85e and fs3 == 0 and fe3 == 0xfc and fm3 == 0x2672cf and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000744]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000748]:csrrs a4, fcsr, zero<br> [0x8000074c]:sw t6, 168(a1)<br>     |
|  45|[0x80003a70]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xf8 and fm1 == 0x2125ab and fs2 == 0 and fe2 == 0x84 and fm2 == 0x6ee85e and fs3 == 0 and fe3 == 0xfc and fm3 == 0x2672cf and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000768]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000076c]:csrrs a4, fcsr, zero<br> [0x80000770]:sw t6, 176(a1)<br>     |
|  46|[0x80003a78]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x56a477 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x194070 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x2ff034 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                          |[0x8000078c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000790]:csrrs a4, fcsr, zero<br> [0x80000794]:sw t6, 184(a1)<br>     |
|  47|[0x80003a80]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x56a477 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x194070 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x2ff034 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800007b0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800007b4]:csrrs a4, fcsr, zero<br> [0x800007b8]:sw t6, 192(a1)<br>     |
|  48|[0x80003a88]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x56a477 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x194070 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x2ff034 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800007d4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800007d8]:csrrs a4, fcsr, zero<br> [0x800007dc]:sw t6, 200(a1)<br>     |
|  49|[0x80003a90]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x56a477 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x194070 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x2ff034 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800007f8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800007fc]:csrrs a4, fcsr, zero<br> [0x80000800]:sw t6, 208(a1)<br>     |
|  50|[0x80003a98]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x56a477 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x194070 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x2ff034 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000081c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000820]:csrrs a4, fcsr, zero<br> [0x80000824]:sw t6, 216(a1)<br>     |
|  51|[0x80003aa0]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1304ef and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0c7b65 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x7526ed and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                          |[0x80000840]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000844]:csrrs a4, fcsr, zero<br> [0x80000848]:sw t6, 224(a1)<br>     |
|  52|[0x80003aa8]<br>0x7F3FFFFD|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1304ef and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0c7b65 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x7526ed and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000864]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000868]:csrrs a4, fcsr, zero<br> [0x8000086c]:sw t6, 232(a1)<br>     |
|  53|[0x80003ab0]<br>0x7F3FFFFD|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1304ef and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0c7b65 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x7526ed and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000888]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000088c]:csrrs a4, fcsr, zero<br> [0x80000890]:sw t6, 240(a1)<br>     |
|  54|[0x80003ab8]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1304ef and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0c7b65 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x7526ed and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800008ac]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800008b0]:csrrs a4, fcsr, zero<br> [0x800008b4]:sw t6, 248(a1)<br>     |
|  55|[0x80003ac0]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1304ef and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0c7b65 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x7526ed and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800008d0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800008d4]:csrrs a4, fcsr, zero<br> [0x800008d8]:sw t6, 256(a1)<br>     |
|  56|[0x80003ac8]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x190d5e and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0a4a56 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x552591 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                          |[0x800008f4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800008f8]:csrrs a4, fcsr, zero<br> [0x800008fc]:sw t6, 264(a1)<br>     |
|  57|[0x80003ad0]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x190d5e and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0a4a56 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x552591 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000918]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000091c]:csrrs a4, fcsr, zero<br> [0x80000920]:sw t6, 272(a1)<br>     |
|  58|[0x80003ad8]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x190d5e and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0a4a56 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x552591 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000093c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000940]:csrrs a4, fcsr, zero<br> [0x80000944]:sw t6, 280(a1)<br>     |
|  59|[0x80003ae0]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x190d5e and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0a4a56 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x552591 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000960]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000964]:csrrs a4, fcsr, zero<br> [0x80000968]:sw t6, 288(a1)<br>     |
|  60|[0x80003ae8]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x190d5e and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0a4a56 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x552591 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000984]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000988]:csrrs a4, fcsr, zero<br> [0x8000098c]:sw t6, 296(a1)<br>     |
|  61|[0x80003af0]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6d1a0c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x3509c8 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x429c29 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                          |[0x800009a8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800009ac]:csrrs a4, fcsr, zero<br> [0x800009b0]:sw t6, 304(a1)<br>     |
|  62|[0x80003af8]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6d1a0c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x3509c8 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x429c29 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800009cc]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800009d0]:csrrs a4, fcsr, zero<br> [0x800009d4]:sw t6, 312(a1)<br>     |
|  63|[0x80003b00]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6d1a0c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x3509c8 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x429c29 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800009f0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800009f4]:csrrs a4, fcsr, zero<br> [0x800009f8]:sw t6, 320(a1)<br>     |
|  64|[0x80003b08]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6d1a0c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x3509c8 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x429c29 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000a14]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000a18]:csrrs a4, fcsr, zero<br> [0x80000a1c]:sw t6, 328(a1)<br>     |
|  65|[0x80003b10]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6d1a0c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x3509c8 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x429c29 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000a38]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000a3c]:csrrs a4, fcsr, zero<br> [0x80000a40]:sw t6, 336(a1)<br>     |
|  66|[0x80003b18]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xf5 and fm1 == 0x2e79de and fs2 == 0 and fe2 == 0x85 and fm2 == 0x10e842 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x274f48 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                          |[0x80000a5c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000a60]:csrrs a4, fcsr, zero<br> [0x80000a64]:sw t6, 344(a1)<br>     |
|  67|[0x80003b20]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xf5 and fm1 == 0x2e79de and fs2 == 0 and fe2 == 0x85 and fm2 == 0x10e842 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x274f48 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000a80]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000a84]:csrrs a4, fcsr, zero<br> [0x80000a88]:sw t6, 352(a1)<br>     |
|  68|[0x80003b28]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xf5 and fm1 == 0x2e79de and fs2 == 0 and fe2 == 0x85 and fm2 == 0x10e842 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x274f48 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000aa4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000aa8]:csrrs a4, fcsr, zero<br> [0x80000aac]:sw t6, 360(a1)<br>     |
|  69|[0x80003b30]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xf5 and fm1 == 0x2e79de and fs2 == 0 and fe2 == 0x85 and fm2 == 0x10e842 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x274f48 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000ac8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000acc]:csrrs a4, fcsr, zero<br> [0x80000ad0]:sw t6, 368(a1)<br>     |
|  70|[0x80003b38]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xf5 and fm1 == 0x2e79de and fs2 == 0 and fe2 == 0x85 and fm2 == 0x10e842 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x274f48 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000aec]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000af0]:csrrs a4, fcsr, zero<br> [0x80000af4]:sw t6, 376(a1)<br>     |
|  71|[0x80003b40]<br>0x7DBFFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x20dccc and fs2 == 1 and fe2 == 0x7d and fm2 == 0x5d2c9b and fs3 == 0 and fe3 == 0xfc and fm3 == 0x257d51 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                          |[0x80000b10]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000b14]:csrrs a4, fcsr, zero<br> [0x80000b18]:sw t6, 384(a1)<br>     |
|  72|[0x80003b48]<br>0x7DBFFFFE|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x20dccc and fs2 == 1 and fe2 == 0x7d and fm2 == 0x5d2c9b and fs3 == 0 and fe3 == 0xfc and fm3 == 0x257d51 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000b34]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000b38]:csrrs a4, fcsr, zero<br> [0x80000b3c]:sw t6, 392(a1)<br>     |
|  73|[0x80003b50]<br>0x7DBFFFFE|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x20dccc and fs2 == 1 and fe2 == 0x7d and fm2 == 0x5d2c9b and fs3 == 0 and fe3 == 0xfc and fm3 == 0x257d51 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000b58]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000b5c]:csrrs a4, fcsr, zero<br> [0x80000b60]:sw t6, 400(a1)<br>     |
|  74|[0x80003b58]<br>0x7DBFFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x20dccc and fs2 == 1 and fe2 == 0x7d and fm2 == 0x5d2c9b and fs3 == 0 and fe3 == 0xfc and fm3 == 0x257d51 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000b7c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000b80]:csrrs a4, fcsr, zero<br> [0x80000b84]:sw t6, 408(a1)<br>     |
|  75|[0x80003b60]<br>0x7DBFFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x20dccc and fs2 == 1 and fe2 == 0x7d and fm2 == 0x5d2c9b and fs3 == 0 and fe3 == 0xfc and fm3 == 0x257d51 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000ba0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000ba4]:csrrs a4, fcsr, zero<br> [0x80000ba8]:sw t6, 416(a1)<br>     |
|  76|[0x80003b68]<br>0xFDBFFFF6|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3c9ff5 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x01f3de and fs3 == 0 and fe3 == 0xfe and fm3 == 0x278095 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                          |[0x80000bc4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000bc8]:csrrs a4, fcsr, zero<br> [0x80000bcc]:sw t6, 424(a1)<br>     |
|  77|[0x80003b70]<br>0xFDBFFFF5|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3c9ff5 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x01f3de and fs3 == 0 and fe3 == 0xfe and fm3 == 0x278095 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000be8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000bec]:csrrs a4, fcsr, zero<br> [0x80000bf0]:sw t6, 432(a1)<br>     |
|  78|[0x80003b78]<br>0xFDBFFFF6|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3c9ff5 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x01f3de and fs3 == 0 and fe3 == 0xfe and fm3 == 0x278095 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000c0c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000c10]:csrrs a4, fcsr, zero<br> [0x80000c14]:sw t6, 440(a1)<br>     |
|  79|[0x80003b80]<br>0xFDBFFFF5|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3c9ff5 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x01f3de and fs3 == 0 and fe3 == 0xfe and fm3 == 0x278095 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000c30]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000c34]:csrrs a4, fcsr, zero<br> [0x80000c38]:sw t6, 448(a1)<br>     |
|  80|[0x80003b88]<br>0xFDBFFFF6|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3c9ff5 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x01f3de and fs3 == 0 and fe3 == 0xfe and fm3 == 0x278095 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000c54]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000c58]:csrrs a4, fcsr, zero<br> [0x80000c5c]:sw t6, 456(a1)<br>     |
|  81|[0x80003b90]<br>0x7E400001|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x5e2fb5 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x1d6497 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x389a90 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                          |[0x80000c78]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000c7c]:csrrs a4, fcsr, zero<br> [0x80000c80]:sw t6, 464(a1)<br>     |
|  82|[0x80003b98]<br>0x7E400001|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x5e2fb5 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x1d6497 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x389a90 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000c9c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000ca0]:csrrs a4, fcsr, zero<br> [0x80000ca4]:sw t6, 472(a1)<br>     |
|  83|[0x80003ba0]<br>0x7E400001|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x5e2fb5 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x1d6497 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x389a90 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000cc0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000cc4]:csrrs a4, fcsr, zero<br> [0x80000cc8]:sw t6, 480(a1)<br>     |
|  84|[0x80003ba8]<br>0x7E400002|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x5e2fb5 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x1d6497 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x389a90 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000ce4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000ce8]:csrrs a4, fcsr, zero<br> [0x80000cec]:sw t6, 488(a1)<br>     |
|  85|[0x80003bb0]<br>0x7E400001|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x5e2fb5 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x1d6497 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x389a90 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000d08]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000d0c]:csrrs a4, fcsr, zero<br> [0x80000d10]:sw t6, 496(a1)<br>     |
|  86|[0x80003bb8]<br>0xFE400001|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4f0fd2 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x5c3f35 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0224b8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                          |[0x80000d2c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000d30]:csrrs a4, fcsr, zero<br> [0x80000d34]:sw t6, 504(a1)<br>     |
|  87|[0x80003bc0]<br>0xFE400000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4f0fd2 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x5c3f35 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0224b8 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000d50]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000d54]:csrrs a4, fcsr, zero<br> [0x80000d58]:sw t6, 512(a1)<br>     |
|  88|[0x80003bc8]<br>0xFE400001|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4f0fd2 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x5c3f35 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0224b8 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000d74]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000d78]:csrrs a4, fcsr, zero<br> [0x80000d7c]:sw t6, 520(a1)<br>     |
|  89|[0x80003bd0]<br>0xFE400000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4f0fd2 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x5c3f35 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0224b8 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000d98]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000d9c]:csrrs a4, fcsr, zero<br> [0x80000da0]:sw t6, 528(a1)<br>     |
|  90|[0x80003bd8]<br>0xFE400001|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x4f0fd2 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x5c3f35 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0224b8 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000dbc]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000dc0]:csrrs a4, fcsr, zero<br> [0x80000dc4]:sw t6, 536(a1)<br>     |
|  91|[0x80003be0]<br>0x7EBFFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x1d454a and fs2 == 1 and fe2 == 0x80 and fm2 == 0x09d19b and fs3 == 0 and fe3 == 0xfe and fm3 == 0x34aad9 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                          |[0x80000de0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000de4]:csrrs a4, fcsr, zero<br> [0x80000de8]:sw t6, 544(a1)<br>     |
|  92|[0x80003be8]<br>0x7EBFFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x1d454a and fs2 == 1 and fe2 == 0x80 and fm2 == 0x09d19b and fs3 == 0 and fe3 == 0xfe and fm3 == 0x34aad9 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000e04]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000e08]:csrrs a4, fcsr, zero<br> [0x80000e0c]:sw t6, 552(a1)<br>     |
|  93|[0x80003bf0]<br>0x7EBFFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x1d454a and fs2 == 1 and fe2 == 0x80 and fm2 == 0x09d19b and fs3 == 0 and fe3 == 0xfe and fm3 == 0x34aad9 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000e28]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000e2c]:csrrs a4, fcsr, zero<br> [0x80000e30]:sw t6, 560(a1)<br>     |
|  94|[0x80003bf8]<br>0x7EC00000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x1d454a and fs2 == 1 and fe2 == 0x80 and fm2 == 0x09d19b and fs3 == 0 and fe3 == 0xfe and fm3 == 0x34aad9 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000e4c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000e50]:csrrs a4, fcsr, zero<br> [0x80000e54]:sw t6, 568(a1)<br>     |
|  95|[0x80003c00]<br>0x7EBFFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x1d454a and fs2 == 1 and fe2 == 0x80 and fm2 == 0x09d19b and fs3 == 0 and fe3 == 0xfe and fm3 == 0x34aad9 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000e70]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000e74]:csrrs a4, fcsr, zero<br> [0x80000e78]:sw t6, 576(a1)<br>     |
|  96|[0x80003c08]<br>0xFEBFFFFC|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x049a6e and fs2 == 1 and fe2 == 0x80 and fm2 == 0x07e718 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x399491 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                          |[0x80000e94]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000e98]:csrrs a4, fcsr, zero<br> [0x80000e9c]:sw t6, 584(a1)<br>     |
|  97|[0x80003c10]<br>0xFEBFFFFC|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x049a6e and fs2 == 1 and fe2 == 0x80 and fm2 == 0x07e718 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x399491 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000eb8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000ebc]:csrrs a4, fcsr, zero<br> [0x80000ec0]:sw t6, 592(a1)<br>     |
|  98|[0x80003c18]<br>0xFEBFFFFD|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x049a6e and fs2 == 1 and fe2 == 0x80 and fm2 == 0x07e718 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x399491 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000edc]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000ee0]:csrrs a4, fcsr, zero<br> [0x80000ee4]:sw t6, 600(a1)<br>     |
|  99|[0x80003c20]<br>0xFEBFFFFC|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x049a6e and fs2 == 1 and fe2 == 0x80 and fm2 == 0x07e718 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x399491 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000f00]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000f04]:csrrs a4, fcsr, zero<br> [0x80000f08]:sw t6, 608(a1)<br>     |
| 100|[0x80003c28]<br>0xFEBFFFFC|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x049a6e and fs2 == 1 and fe2 == 0x80 and fm2 == 0x07e718 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x399491 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000f24]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000f28]:csrrs a4, fcsr, zero<br> [0x80000f2c]:sw t6, 616(a1)<br>     |
| 101|[0x80003c30]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2b9930 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x48da2e and fs3 == 0 and fe3 == 0xfc and fm3 == 0x657849 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                          |[0x80000f48]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000f4c]:csrrs a4, fcsr, zero<br> [0x80000f50]:sw t6, 624(a1)<br>     |
| 102|[0x80003c38]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2b9930 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x48da2e and fs3 == 0 and fe3 == 0xfc and fm3 == 0x657849 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000f6c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000f70]:csrrs a4, fcsr, zero<br> [0x80000f74]:sw t6, 632(a1)<br>     |
| 103|[0x80003c40]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2b9930 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x48da2e and fs3 == 0 and fe3 == 0xfc and fm3 == 0x657849 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000f90]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000f94]:csrrs a4, fcsr, zero<br> [0x80000f98]:sw t6, 640(a1)<br>     |
| 104|[0x80003c48]<br>0x7F400000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2b9930 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x48da2e and fs3 == 0 and fe3 == 0xfc and fm3 == 0x657849 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000fb4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000fb8]:csrrs a4, fcsr, zero<br> [0x80000fbc]:sw t6, 648(a1)<br>     |
| 105|[0x80003c50]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2b9930 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x48da2e and fs3 == 0 and fe3 == 0xfc and fm3 == 0x657849 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000fd8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80000fdc]:csrrs a4, fcsr, zero<br> [0x80000fe0]:sw t6, 656(a1)<br>     |
| 106|[0x80003c58]<br>0xFF3FFFFD|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x0ab269 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x4e9922 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x7ee893 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                          |[0x80000ffc]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001000]:csrrs a4, fcsr, zero<br> [0x80001004]:sw t6, 664(a1)<br>     |
| 107|[0x80003c60]<br>0xFF3FFFFC|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x0ab269 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x4e9922 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x7ee893 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001020]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001024]:csrrs a4, fcsr, zero<br> [0x80001028]:sw t6, 672(a1)<br>     |
| 108|[0x80003c68]<br>0xFF3FFFFD|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x0ab269 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x4e9922 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x7ee893 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001044]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001048]:csrrs a4, fcsr, zero<br> [0x8000104c]:sw t6, 680(a1)<br>     |
| 109|[0x80003c70]<br>0xFF3FFFFC|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x0ab269 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x4e9922 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x7ee893 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001068]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000106c]:csrrs a4, fcsr, zero<br> [0x80001070]:sw t6, 688(a1)<br>     |
| 110|[0x80003c78]<br>0xFF3FFFFD|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x0ab269 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x4e9922 and fs3 == 0 and fe3 == 0xfb and fm3 == 0x7ee893 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000108c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001090]:csrrs a4, fcsr, zero<br> [0x80001094]:sw t6, 696(a1)<br>     |
| 111|[0x80003c80]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x5fcc51 and fs2 == 0 and fe2 == 0x82 and fm2 == 0x547929 and fs3 == 0 and fe3 == 0xfa and fm3 == 0x481bf3 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                          |[0x800010b0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800010b4]:csrrs a4, fcsr, zero<br> [0x800010b8]:sw t6, 704(a1)<br>     |
| 112|[0x80003c88]<br>0x7F7FFFFF|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x5fcc51 and fs2 == 0 and fe2 == 0x82 and fm2 == 0x547929 and fs3 == 0 and fe3 == 0xfa and fm3 == 0x481bf3 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800010d4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800010d8]:csrrs a4, fcsr, zero<br> [0x800010dc]:sw t6, 712(a1)<br>     |
| 113|[0x80003c90]<br>0x7F7FFFFF|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x5fcc51 and fs2 == 0 and fe2 == 0x82 and fm2 == 0x547929 and fs3 == 0 and fe3 == 0xfa and fm3 == 0x481bf3 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800010f8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800010fc]:csrrs a4, fcsr, zero<br> [0x80001100]:sw t6, 720(a1)<br>     |
| 114|[0x80003c98]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x5fcc51 and fs2 == 0 and fe2 == 0x82 and fm2 == 0x547929 and fs3 == 0 and fe3 == 0xfa and fm3 == 0x481bf3 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000111c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001120]:csrrs a4, fcsr, zero<br> [0x80001124]:sw t6, 728(a1)<br>     |
| 115|[0x80003ca0]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x5fcc51 and fs2 == 0 and fe2 == 0x82 and fm2 == 0x547929 and fs3 == 0 and fe3 == 0xfa and fm3 == 0x481bf3 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001140]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001144]:csrrs a4, fcsr, zero<br> [0x80001148]:sw t6, 736(a1)<br>     |
| 116|[0x80003ca8]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x67719b and fs2 == 1 and fe2 == 0x82 and fm2 == 0x0a34d7 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x67981f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                          |[0x80001164]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001168]:csrrs a4, fcsr, zero<br> [0x8000116c]:sw t6, 744(a1)<br>     |
| 117|[0x80003cb0]<br>0xFF7FFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x67719b and fs2 == 1 and fe2 == 0x82 and fm2 == 0x0a34d7 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x67981f and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001188]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000118c]:csrrs a4, fcsr, zero<br> [0x80001190]:sw t6, 752(a1)<br>     |
| 118|[0x80003cb8]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x67719b and fs2 == 1 and fe2 == 0x82 and fm2 == 0x0a34d7 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x67981f and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800011ac]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800011b0]:csrrs a4, fcsr, zero<br> [0x800011b4]:sw t6, 760(a1)<br>     |
| 119|[0x80003cc0]<br>0xFF7FFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x67719b and fs2 == 1 and fe2 == 0x82 and fm2 == 0x0a34d7 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x67981f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800011d0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800011d4]:csrrs a4, fcsr, zero<br> [0x800011d8]:sw t6, 768(a1)<br>     |
| 120|[0x80003cc8]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x67719b and fs2 == 1 and fe2 == 0x82 and fm2 == 0x0a34d7 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x67981f and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800011f4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800011f8]:csrrs a4, fcsr, zero<br> [0x800011fc]:sw t6, 776(a1)<br>     |
| 121|[0x80003cd0]<br>0x7F800000|- fs1 == 0 and fe1 == 0xf6 and fm1 == 0x26497d and fs2 == 0 and fe2 == 0x88 and fm2 == 0x757d05 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0229d2 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                          |[0x80001218]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000121c]:csrrs a4, fcsr, zero<br> [0x80001220]:sw t6, 784(a1)<br>     |
| 122|[0x80003cd8]<br>0x7F7FFFFF|- fs1 == 0 and fe1 == 0xf6 and fm1 == 0x26497d and fs2 == 0 and fe2 == 0x88 and fm2 == 0x757d05 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0229d2 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000123c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001240]:csrrs a4, fcsr, zero<br> [0x80001244]:sw t6, 792(a1)<br>     |
| 123|[0x80003ce0]<br>0x7F7FFFFF|- fs1 == 0 and fe1 == 0xf6 and fm1 == 0x26497d and fs2 == 0 and fe2 == 0x88 and fm2 == 0x757d05 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0229d2 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001260]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001264]:csrrs a4, fcsr, zero<br> [0x80001268]:sw t6, 800(a1)<br>     |
| 124|[0x80003ce8]<br>0x7F800000|- fs1 == 0 and fe1 == 0xf6 and fm1 == 0x26497d and fs2 == 0 and fe2 == 0x88 and fm2 == 0x757d05 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0229d2 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001284]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001288]:csrrs a4, fcsr, zero<br> [0x8000128c]:sw t6, 808(a1)<br>     |
| 125|[0x80003cf0]<br>0x7F800000|- fs1 == 0 and fe1 == 0xf6 and fm1 == 0x26497d and fs2 == 0 and fe2 == 0x88 and fm2 == 0x757d05 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0229d2 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800012a8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800012ac]:csrrs a4, fcsr, zero<br> [0x800012b0]:sw t6, 816(a1)<br>     |
| 126|[0x80003cf8]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x01c09c and fs2 == 1 and fe2 == 0x82 and fm2 == 0x4c157d and fs3 == 0 and fe3 == 0xfc and fm3 == 0x6e0c34 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                          |[0x800012cc]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800012d0]:csrrs a4, fcsr, zero<br> [0x800012d4]:sw t6, 824(a1)<br>     |
| 127|[0x80003d00]<br>0xFF7FFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x01c09c and fs2 == 1 and fe2 == 0x82 and fm2 == 0x4c157d and fs3 == 0 and fe3 == 0xfc and fm3 == 0x6e0c34 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800012f0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800012f4]:csrrs a4, fcsr, zero<br> [0x800012f8]:sw t6, 832(a1)<br>     |
| 128|[0x80003d08]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x01c09c and fs2 == 1 and fe2 == 0x82 and fm2 == 0x4c157d and fs3 == 0 and fe3 == 0xfc and fm3 == 0x6e0c34 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001314]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001318]:csrrs a4, fcsr, zero<br> [0x8000131c]:sw t6, 840(a1)<br>     |
| 129|[0x80003d10]<br>0xFF7FFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x01c09c and fs2 == 1 and fe2 == 0x82 and fm2 == 0x4c157d and fs3 == 0 and fe3 == 0xfc and fm3 == 0x6e0c34 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001338]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000133c]:csrrs a4, fcsr, zero<br> [0x80001340]:sw t6, 848(a1)<br>     |
| 130|[0x80003d18]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x01c09c and fs2 == 1 and fe2 == 0x82 and fm2 == 0x4c157d and fs3 == 0 and fe3 == 0xfc and fm3 == 0x6e0c34 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000135c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001360]:csrrs a4, fcsr, zero<br> [0x80001364]:sw t6, 856(a1)<br>     |
| 131|[0x80003d20]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d27cc and fs2 == 0 and fe2 == 0x82 and fm2 == 0x12fb98 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x389b2c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                          |[0x80001380]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001384]:csrrs a4, fcsr, zero<br> [0x80001388]:sw t6, 864(a1)<br>     |
| 132|[0x80003d28]<br>0x7F7FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d27cc and fs2 == 0 and fe2 == 0x82 and fm2 == 0x12fb98 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x389b2c and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800013a4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800013a8]:csrrs a4, fcsr, zero<br> [0x800013ac]:sw t6, 872(a1)<br>     |
| 133|[0x80003d30]<br>0x7F7FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d27cc and fs2 == 0 and fe2 == 0x82 and fm2 == 0x12fb98 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x389b2c and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800013c8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800013cc]:csrrs a4, fcsr, zero<br> [0x800013d0]:sw t6, 880(a1)<br>     |
| 134|[0x80003d38]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d27cc and fs2 == 0 and fe2 == 0x82 and fm2 == 0x12fb98 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x389b2c and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800013ec]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800013f0]:csrrs a4, fcsr, zero<br> [0x800013f4]:sw t6, 888(a1)<br>     |
| 135|[0x80003d40]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d27cc and fs2 == 0 and fe2 == 0x82 and fm2 == 0x12fb98 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x389b2c and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001410]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001414]:csrrs a4, fcsr, zero<br> [0x80001418]:sw t6, 896(a1)<br>     |
| 136|[0x80003d48]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x728772 and fs2 == 1 and fe2 == 0x83 and fm2 == 0x56dae4 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x38c9b0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                          |[0x80001434]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001438]:csrrs a4, fcsr, zero<br> [0x8000143c]:sw t6, 904(a1)<br>     |
| 137|[0x80003d50]<br>0xFF7FFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x728772 and fs2 == 1 and fe2 == 0x83 and fm2 == 0x56dae4 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x38c9b0 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001458]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000145c]:csrrs a4, fcsr, zero<br> [0x80001460]:sw t6, 912(a1)<br>     |
| 138|[0x80003d58]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x728772 and fs2 == 1 and fe2 == 0x83 and fm2 == 0x56dae4 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x38c9b0 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000147c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001480]:csrrs a4, fcsr, zero<br> [0x80001484]:sw t6, 920(a1)<br>     |
| 139|[0x80003d60]<br>0xFF7FFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x728772 and fs2 == 1 and fe2 == 0x83 and fm2 == 0x56dae4 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x38c9b0 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800014a0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800014a4]:csrrs a4, fcsr, zero<br> [0x800014a8]:sw t6, 928(a1)<br>     |
| 140|[0x80003d68]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x728772 and fs2 == 1 and fe2 == 0x83 and fm2 == 0x56dae4 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x38c9b0 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800014c4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800014c8]:csrrs a4, fcsr, zero<br> [0x800014cc]:sw t6, 936(a1)<br>     |
| 141|[0x80003d70]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6f31b9 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x543d9b and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0e6c50 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                          |[0x800014e8]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800014ec]:csrrs a4, fcsr, zero<br> [0x800014f0]:sw t6, 944(a1)<br>     |
| 142|[0x80003d78]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6f31b9 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x543d9b and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0e6c50 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000150c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001510]:csrrs a4, fcsr, zero<br> [0x80001514]:sw t6, 952(a1)<br>     |
| 143|[0x80003d80]<br>0x7F400000|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6f31b9 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x543d9b and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0e6c50 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001530]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001534]:csrrs a4, fcsr, zero<br> [0x80001538]:sw t6, 960(a1)<br>     |
| 144|[0x80003d88]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x6f31b9 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x543d9b and fs3 == 0 and fe3 == 0xfe and fm3 == 0x0e6c50 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001554]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001558]:csrrs a4, fcsr, zero<br> [0x8000155c]:sw t6, 968(a1)<br>     |
| 145|[0x80003d90]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x18ad5a and fs2 == 0 and fe2 == 0x7b and fm2 == 0x1437ba and fs3 == 0 and fe3 == 0xfe and fm3 == 0x34f350 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                          |[0x80001578]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000157c]:csrrs a4, fcsr, zero<br> [0x80001580]:sw t6, 976(a1)<br>     |
| 146|[0x80003d98]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x18ad5a and fs2 == 0 and fe2 == 0x7b and fm2 == 0x1437ba and fs3 == 0 and fe3 == 0xfe and fm3 == 0x34f350 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000159c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800015a0]:csrrs a4, fcsr, zero<br> [0x800015a4]:sw t6, 984(a1)<br>     |
| 147|[0x80003da0]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x18ad5a and fs2 == 0 and fe2 == 0x7b and fm2 == 0x1437ba and fs3 == 0 and fe3 == 0xfe and fm3 == 0x34f350 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800015c0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800015c4]:csrrs a4, fcsr, zero<br> [0x800015c8]:sw t6, 992(a1)<br>     |
| 148|[0x80003dc0]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0e0f7e and fs2 == 0 and fe2 == 0x7b and fm2 == 0x570014 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x31161d and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001658]:fmadd.s t6, t5, t4, t3, dyn<br> [0x8000165c]:csrrs a4, fcsr, zero<br> [0x80001660]:sw t6, 0(a1)<br>       |
| 149|[0x80003dc8]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x341f5c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x4d988d and fs3 == 0 and fe3 == 0xfc and fm3 == 0x3d5e30 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000167c]:fmadd.s t6, t5, t4, t3, dyn<br> [0x80001680]:csrrs a4, fcsr, zero<br> [0x80001684]:sw t6, 8(a1)<br>       |
| 150|[0x80003dd0]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x341f5c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x4d988d and fs3 == 0 and fe3 == 0xfc and fm3 == 0x3d5e30 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800016a0]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800016a4]:csrrs a4, fcsr, zero<br> [0x800016a8]:sw t6, 16(a1)<br>      |
| 151|[0x80003dd8]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x50cddd and fs2 == 0 and fe2 == 0x7e and fm2 == 0x489620 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x5c64b6 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800016c4]:fmadd.s t6, t5, t4, t3, dyn<br> [0x800016c8]:csrrs a4, fcsr, zero<br> [0x800016cc]:sw t6, 24(a1)<br>      |
