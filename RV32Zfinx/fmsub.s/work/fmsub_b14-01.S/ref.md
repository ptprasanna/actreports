
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000d90')]      |
| SIG_REGION                | [('0x80002610', '0x800028d0', '176 words')]      |
| COV_LABELS                | fmsub_b14      |
| TEST_NAME                 | /home/reg/work/zfinx/RV32Zfinx/fmsub.s/work/fmsub_b14-01.S/ref.S    |
| Total Number of coverpoints| 214     |
| Total Coverpoints Hit     | 214      |
| Total Signature Updates   | 176      |
| STAT1                     | 84      |
| STAT2                     | 4      |
| STAT3                     | 0     |
| STAT4                     | 88     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000bc4]:fmsub.s t6, t5, t4, t3, dyn
      [0x80000bc8]:csrrs a4, fcsr, zero
      [0x80000bcc]:sw t6, 408(s1)
 -- Signature Address: 0x80002868 Data: 0x7F800000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fmsub.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs3 : x28
      - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd
      - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x67 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000c0c]:fmsub.s t6, t5, t4, t3, dyn
      [0x80000c10]:csrrs a4, fcsr, zero
      [0x80000c14]:sw t6, 424(s1)
 -- Signature Address: 0x80002878 Data: 0x7F800000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fmsub.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs3 : x28
      - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd
      - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x69 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000cc0]:fmsub.s t6, t5, t4, t3, dyn
      [0x80000cc4]:csrrs a4, fcsr, zero
      [0x80000cc8]:sw t6, 464(s1)
 -- Signature Address: 0x800028a0 Data: 0x7F800000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fmsub.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs3 : x28
      - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd
      - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x6e and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000d74]:fmsub.s t6, t5, t4, t3, dyn
      [0x80000d78]:csrrs a4, fcsr, zero
      [0x80000d7c]:sw t6, 504(s1)
 -- Signature Address: 0x800028c8 Data: 0x7F800000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fmsub.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs3 : x28
      - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd
      - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x87 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fmsub.s', 'rs1 : x31', 'rs2 : x30', 'rd : x31', 'rs3 : x31', 'rs1 == rd == rs3 != rs2']
Last Code Sequence : 
	-[0x80000128]:fmsub.s t6, t6, t5, t6, dyn
	-[0x8000012c]:csrrs tp, fcsr, zero
	-[0x80000130]:sw t6, 0(ra)
Current Store : [0x80000134] : sw tp, 4(ra) -- Store: [0x80002614]:0x00000005




Last Coverpoint : ['rs1 : x29', 'rs2 : x29', 'rd : x30', 'rs3 : x28', 'rs1 == rs2 != rs3 and rs1 == rs2 != rd and rd != rs3']
Last Code Sequence : 
	-[0x8000014c]:fmsub.s t5, t4, t4, t3, dyn
	-[0x80000150]:csrrs tp, fcsr, zero
	-[0x80000154]:sw t5, 8(ra)
Current Store : [0x80000158] : sw tp, 12(ra) -- Store: [0x8000261c]:0x00000005




Last Coverpoint : ['rs1 : x28', 'rs2 : x31', 'rd : x28', 'rs3 : x30', 'rs1 == rd != rs2 and rs1 == rd != rs3 and rs3 != rs2', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x67 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000170]:fmsub.s t3, t3, t6, t5, dyn
	-[0x80000174]:csrrs tp, fcsr, zero
	-[0x80000178]:sw t3, 16(ra)
Current Store : [0x8000017c] : sw tp, 20(ra) -- Store: [0x80002624]:0x00000005




Last Coverpoint : ['rs1 : x30', 'rs2 : x27', 'rd : x27', 'rs3 : x27', 'rd == rs2 == rs3 != rs1']
Last Code Sequence : 
	-[0x80000194]:fmsub.s s11, t5, s11, s11, dyn
	-[0x80000198]:csrrs tp, fcsr, zero
	-[0x8000019c]:sw s11, 24(ra)
Current Store : [0x800001a0] : sw tp, 28(ra) -- Store: [0x8000262c]:0x00000005




Last Coverpoint : ['rs1 : x27', 'rs2 : x28', 'rd : x29', 'rs3 : x29', 'rs3 == rd != rs1 and rs3 == rd != rs2 and rs2 != rs1', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x69 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001b8]:fmsub.s t4, s11, t3, t4, dyn
	-[0x800001bc]:csrrs tp, fcsr, zero
	-[0x800001c0]:sw t4, 32(ra)
Current Store : [0x800001c4] : sw tp, 36(ra) -- Store: [0x80002634]:0x00000005




Last Coverpoint : ['rs1 : x26', 'rs2 : x26', 'rd : x26', 'rs3 : x25', 'rs1 == rs2 == rd != rs3']
Last Code Sequence : 
	-[0x800001dc]:fmsub.s s10, s10, s10, s9, dyn
	-[0x800001e0]:csrrs tp, fcsr, zero
	-[0x800001e4]:sw s10, 40(ra)
Current Store : [0x800001e8] : sw tp, 44(ra) -- Store: [0x8000263c]:0x00000005




Last Coverpoint : ['rs1 : x24', 'rs2 : x24', 'rd : x24', 'rs3 : x24', 'rs1 == rs2 == rs3 == rd']
Last Code Sequence : 
	-[0x80000200]:fmsub.s s8, s8, s8, s8, dyn
	-[0x80000204]:csrrs tp, fcsr, zero
	-[0x80000208]:sw s8, 48(ra)
Current Store : [0x8000020c] : sw tp, 52(ra) -- Store: [0x80002644]:0x00000005




Last Coverpoint : ['rs1 : x23', 'rs2 : x22', 'rd : x25', 'rs3 : x22', 'rs2 == rs3 != rs1 and rs2 == rs3 != rd and rd != rs1']
Last Code Sequence : 
	-[0x80000224]:fmsub.s s9, s7, s6, s6, dyn
	-[0x80000228]:csrrs tp, fcsr, zero
	-[0x8000022c]:sw s9, 56(ra)
Current Store : [0x80000230] : sw tp, 60(ra) -- Store: [0x8000264c]:0x00000005




Last Coverpoint : ['rs1 : x21', 'rs2 : x21', 'rd : x23', 'rs3 : x21', 'rs1 == rs2 == rs3 != rd']
Last Code Sequence : 
	-[0x80000248]:fmsub.s s7, s5, s5, s5, dyn
	-[0x8000024c]:csrrs tp, fcsr, zero
	-[0x80000250]:sw s7, 64(ra)
Current Store : [0x80000254] : sw tp, 68(ra) -- Store: [0x80002654]:0x00000005




Last Coverpoint : ['rs1 : x25', 'rs2 : x20', 'rd : x20', 'rs3 : x26', 'rs2 == rd != rs1 and rs2 == rd != rs3 and rs3 != rs1', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x6e and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000026c]:fmsub.s s4, s9, s4, s10, dyn
	-[0x80000270]:csrrs tp, fcsr, zero
	-[0x80000274]:sw s4, 72(ra)
Current Store : [0x80000278] : sw tp, 76(ra) -- Store: [0x8000265c]:0x00000005




Last Coverpoint : ['rs1 : x20', 'rs2 : x25', 'rd : x22', 'rs3 : x20', 'rs1 == rs3 != rs2 and rs1 == rs3 != rd and rd != rs2']
Last Code Sequence : 
	-[0x80000290]:fmsub.s s6, s4, s9, s4, dyn
	-[0x80000294]:csrrs tp, fcsr, zero
	-[0x80000298]:sw s6, 80(ra)
Current Store : [0x8000029c] : sw tp, 84(ra) -- Store: [0x80002664]:0x00000005




Last Coverpoint : ['rs1 : x22', 'rs2 : x23', 'rd : x21', 'rs3 : x19', 'rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x70 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002b4]:fmsub.s s5, s6, s7, s3, dyn
	-[0x800002b8]:csrrs tp, fcsr, zero
	-[0x800002bc]:sw s5, 88(ra)
Current Store : [0x800002c0] : sw tp, 92(ra) -- Store: [0x8000266c]:0x00000005




Last Coverpoint : ['rs1 : x18', 'rs2 : x17', 'rd : x19', 'rs3 : x23', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x71 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002d8]:fmsub.s s3, s2, a7, s7, dyn
	-[0x800002dc]:csrrs tp, fcsr, zero
	-[0x800002e0]:sw s3, 96(ra)
Current Store : [0x800002e4] : sw tp, 100(ra) -- Store: [0x80002674]:0x00000005




Last Coverpoint : ['rs1 : x17', 'rs2 : x19', 'rd : x18', 'rs3 : x16', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x72 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002fc]:fmsub.s s2, a7, s3, a6, dyn
	-[0x80000300]:csrrs tp, fcsr, zero
	-[0x80000304]:sw s2, 104(ra)
Current Store : [0x80000308] : sw tp, 108(ra) -- Store: [0x8000267c]:0x00000005




Last Coverpoint : ['rs1 : x19', 'rs2 : x16', 'rd : x17', 'rs3 : x18', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x73 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000320]:fmsub.s a7, s3, a6, s2, dyn
	-[0x80000324]:csrrs tp, fcsr, zero
	-[0x80000328]:sw a7, 112(ra)
Current Store : [0x8000032c] : sw tp, 116(ra) -- Store: [0x80002684]:0x00000005




Last Coverpoint : ['rs1 : x15', 'rs2 : x18', 'rd : x16', 'rs3 : x17', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x74 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000344]:fmsub.s a6, a5, s2, a7, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:sw a6, 120(ra)
Current Store : [0x80000350] : sw tp, 124(ra) -- Store: [0x8000268c]:0x00000005




Last Coverpoint : ['rs1 : x16', 'rs2 : x14', 'rd : x15', 'rs3 : x13', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x75 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000368]:fmsub.s a5, a6, a4, a3, dyn
	-[0x8000036c]:csrrs tp, fcsr, zero
	-[0x80000370]:sw a5, 128(ra)
Current Store : [0x80000374] : sw tp, 132(ra) -- Store: [0x80002694]:0x00000005




Last Coverpoint : ['rs1 : x13', 'rs2 : x15', 'rd : x14', 'rs3 : x12', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x76 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000038c]:fmsub.s a4, a3, a5, a2, dyn
	-[0x80000390]:csrrs tp, fcsr, zero
	-[0x80000394]:sw a4, 136(ra)
Current Store : [0x80000398] : sw tp, 140(ra) -- Store: [0x8000269c]:0x00000005




Last Coverpoint : ['rs1 : x14', 'rs2 : x12', 'rd : x13', 'rs3 : x15', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x77 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003b0]:fmsub.s a3, a4, a2, a5, dyn
	-[0x800003b4]:csrrs tp, fcsr, zero
	-[0x800003b8]:sw a3, 144(ra)
Current Store : [0x800003bc] : sw tp, 148(ra) -- Store: [0x800026a4]:0x00000005




Last Coverpoint : ['rs1 : x11', 'rs2 : x13', 'rd : x12', 'rs3 : x14', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x78 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003d4]:fmsub.s a2, a1, a3, a4, dyn
	-[0x800003d8]:csrrs tp, fcsr, zero
	-[0x800003dc]:sw a2, 152(ra)
Current Store : [0x800003e0] : sw tp, 156(ra) -- Store: [0x800026ac]:0x00000005




Last Coverpoint : ['rs1 : x12', 'rs2 : x10', 'rd : x11', 'rs3 : x9', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x79 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003f8]:fmsub.s a1, a2, a0, s1, dyn
	-[0x800003fc]:csrrs tp, fcsr, zero
	-[0x80000400]:sw a1, 160(ra)
Current Store : [0x80000404] : sw tp, 164(ra) -- Store: [0x800026b4]:0x00000005




Last Coverpoint : ['rs1 : x9', 'rs2 : x11', 'rd : x10', 'rs3 : x8', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x7a and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000424]:fmsub.s a0, s1, a1, fp, dyn
	-[0x80000428]:csrrs a4, fcsr, zero
	-[0x8000042c]:sw a0, 168(ra)
Current Store : [0x80000430] : sw a4, 172(ra) -- Store: [0x800026bc]:0x00000005




Last Coverpoint : ['rs1 : x10', 'rs2 : x8', 'rd : x9', 'rs3 : x11', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x7b and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000448]:fmsub.s s1, a0, fp, a1, dyn
	-[0x8000044c]:csrrs a4, fcsr, zero
	-[0x80000450]:sw s1, 176(ra)
Current Store : [0x80000454] : sw a4, 180(ra) -- Store: [0x800026c4]:0x00000005




Last Coverpoint : ['rs1 : x7', 'rs2 : x9', 'rd : x8', 'rs3 : x10', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x7c and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000046c]:fmsub.s fp, t2, s1, a0, dyn
	-[0x80000470]:csrrs a4, fcsr, zero
	-[0x80000474]:sw fp, 184(ra)
Current Store : [0x80000478] : sw a4, 188(ra) -- Store: [0x800026cc]:0x00000005




Last Coverpoint : ['rs1 : x8', 'rs2 : x6', 'rd : x7', 'rs3 : x5', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x7d and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000498]:fmsub.s t2, fp, t1, t0, dyn
	-[0x8000049c]:csrrs a4, fcsr, zero
	-[0x800004a0]:sw t2, 0(s1)
Current Store : [0x800004a4] : sw a4, 4(s1) -- Store: [0x800026d4]:0x00000005




Last Coverpoint : ['rs1 : x5', 'rs2 : x7', 'rd : x6', 'rs3 : x4', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x7e and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004bc]:fmsub.s t1, t0, t2, tp, dyn
	-[0x800004c0]:csrrs a4, fcsr, zero
	-[0x800004c4]:sw t1, 8(s1)
Current Store : [0x800004c8] : sw a4, 12(s1) -- Store: [0x800026dc]:0x00000005




Last Coverpoint : ['rs1 : x6', 'rs2 : x4', 'rd : x5', 'rs3 : x7', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x7f and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004e0]:fmsub.s t0, t1, tp, t2, dyn
	-[0x800004e4]:csrrs a4, fcsr, zero
	-[0x800004e8]:sw t0, 16(s1)
Current Store : [0x800004ec] : sw a4, 20(s1) -- Store: [0x800026e4]:0x00000005




Last Coverpoint : ['rs1 : x3', 'rs2 : x5', 'rd : x4', 'rs3 : x6', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x80 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000504]:fmsub.s tp, gp, t0, t1, dyn
	-[0x80000508]:csrrs a4, fcsr, zero
	-[0x8000050c]:sw tp, 24(s1)
Current Store : [0x80000510] : sw a4, 28(s1) -- Store: [0x800026ec]:0x00000005




Last Coverpoint : ['rs1 : x4', 'rs2 : x2', 'rd : x3', 'rs3 : x1', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x81 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000528]:fmsub.s gp, tp, sp, ra, dyn
	-[0x8000052c]:csrrs a4, fcsr, zero
	-[0x80000530]:sw gp, 32(s1)
Current Store : [0x80000534] : sw a4, 36(s1) -- Store: [0x800026f4]:0x00000005




Last Coverpoint : ['rs1 : x1', 'rs2 : x3', 'rd : x2', 'rs3 : x0']
Last Code Sequence : 
	-[0x8000054c]:fmsub.s sp, ra, gp, zero, dyn
	-[0x80000550]:csrrs a4, fcsr, zero
	-[0x80000554]:sw sp, 40(s1)
Current Store : [0x80000558] : sw a4, 44(s1) -- Store: [0x800026fc]:0x00000005




Last Coverpoint : ['rs1 : x2', 'rs2 : x0', 'rd : x1', 'rs3 : x3']
Last Code Sequence : 
	-[0x80000570]:fmsub.s ra, sp, zero, gp, dyn
	-[0x80000574]:csrrs a4, fcsr, zero
	-[0x80000578]:sw ra, 48(s1)
Current Store : [0x8000057c] : sw a4, 52(s1) -- Store: [0x80002704]:0x00000000




Last Coverpoint : ['rs1 : x0']
Last Code Sequence : 
	-[0x80000594]:fmsub.s t6, zero, t5, t4, dyn
	-[0x80000598]:csrrs a4, fcsr, zero
	-[0x8000059c]:sw t6, 56(s1)
Current Store : [0x800005a0] : sw a4, 60(s1) -- Store: [0x8000270c]:0x00000000




Last Coverpoint : ['rs2 : x1', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x85 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005b8]:fmsub.s t6, t5, ra, t4, dyn
	-[0x800005bc]:csrrs a4, fcsr, zero
	-[0x800005c0]:sw t6, 64(s1)
Current Store : [0x800005c4] : sw a4, 68(s1) -- Store: [0x80002714]:0x00000005




Last Coverpoint : ['rs3 : x2', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x86 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005dc]:fmsub.s t6, t5, t4, sp, dyn
	-[0x800005e0]:csrrs a4, fcsr, zero
	-[0x800005e4]:sw t6, 72(s1)
Current Store : [0x800005e8] : sw a4, 76(s1) -- Store: [0x8000271c]:0x00000005




Last Coverpoint : ['rd : x0', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x87 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000600]:fmsub.s zero, t6, t5, t4, dyn
	-[0x80000604]:csrrs a4, fcsr, zero
	-[0x80000608]:sw zero, 80(s1)
Current Store : [0x8000060c] : sw a4, 84(s1) -- Store: [0x80002724]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x88 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000624]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000628]:csrrs a4, fcsr, zero
	-[0x8000062c]:sw t6, 88(s1)
Current Store : [0x80000630] : sw a4, 92(s1) -- Store: [0x8000272c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x89 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000648]:fmsub.s t6, t5, t4, t3, dyn
	-[0x8000064c]:csrrs a4, fcsr, zero
	-[0x80000650]:sw t6, 96(s1)
Current Store : [0x80000654] : sw a4, 100(s1) -- Store: [0x80002734]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x8a and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000066c]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000670]:csrrs a4, fcsr, zero
	-[0x80000674]:sw t6, 104(s1)
Current Store : [0x80000678] : sw a4, 108(s1) -- Store: [0x8000273c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x8b and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000690]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000694]:csrrs a4, fcsr, zero
	-[0x80000698]:sw t6, 112(s1)
Current Store : [0x8000069c] : sw a4, 116(s1) -- Store: [0x80002744]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x8c and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006b4]:fmsub.s t6, t5, t4, t3, dyn
	-[0x800006b8]:csrrs a4, fcsr, zero
	-[0x800006bc]:sw t6, 120(s1)
Current Store : [0x800006c0] : sw a4, 124(s1) -- Store: [0x8000274c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x8d and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006d8]:fmsub.s t6, t5, t4, t3, dyn
	-[0x800006dc]:csrrs a4, fcsr, zero
	-[0x800006e0]:sw t6, 128(s1)
Current Store : [0x800006e4] : sw a4, 132(s1) -- Store: [0x80002754]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x8e and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006fc]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000700]:csrrs a4, fcsr, zero
	-[0x80000704]:sw t6, 136(s1)
Current Store : [0x80000708] : sw a4, 140(s1) -- Store: [0x8000275c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x8f and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000720]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000724]:csrrs a4, fcsr, zero
	-[0x80000728]:sw t6, 144(s1)
Current Store : [0x8000072c] : sw a4, 148(s1) -- Store: [0x80002764]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x90 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000744]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000748]:csrrs a4, fcsr, zero
	-[0x8000074c]:sw t6, 152(s1)
Current Store : [0x80000750] : sw a4, 156(s1) -- Store: [0x8000276c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x91 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000768]:fmsub.s t6, t5, t4, t3, dyn
	-[0x8000076c]:csrrs a4, fcsr, zero
	-[0x80000770]:sw t6, 160(s1)
Current Store : [0x80000774] : sw a4, 164(s1) -- Store: [0x80002774]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x92 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000078c]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000790]:csrrs a4, fcsr, zero
	-[0x80000794]:sw t6, 168(s1)
Current Store : [0x80000798] : sw a4, 172(s1) -- Store: [0x8000277c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x93 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007b0]:fmsub.s t6, t5, t4, t3, dyn
	-[0x800007b4]:csrrs a4, fcsr, zero
	-[0x800007b8]:sw t6, 176(s1)
Current Store : [0x800007bc] : sw a4, 180(s1) -- Store: [0x80002784]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x94 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007d4]:fmsub.s t6, t5, t4, t3, dyn
	-[0x800007d8]:csrrs a4, fcsr, zero
	-[0x800007dc]:sw t6, 184(s1)
Current Store : [0x800007e0] : sw a4, 188(s1) -- Store: [0x8000278c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x95 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007f8]:fmsub.s t6, t5, t4, t3, dyn
	-[0x800007fc]:csrrs a4, fcsr, zero
	-[0x80000800]:sw t6, 192(s1)
Current Store : [0x80000804] : sw a4, 196(s1) -- Store: [0x80002794]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x96 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000081c]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000820]:csrrs a4, fcsr, zero
	-[0x80000824]:sw t6, 200(s1)
Current Store : [0x80000828] : sw a4, 204(s1) -- Store: [0x8000279c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x97 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000840]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000844]:csrrs a4, fcsr, zero
	-[0x80000848]:sw t6, 208(s1)
Current Store : [0x8000084c] : sw a4, 212(s1) -- Store: [0x800027a4]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x98 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000864]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000868]:csrrs a4, fcsr, zero
	-[0x8000086c]:sw t6, 216(s1)
Current Store : [0x80000870] : sw a4, 220(s1) -- Store: [0x800027ac]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x99 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000888]:fmsub.s t6, t5, t4, t3, dyn
	-[0x8000088c]:csrrs a4, fcsr, zero
	-[0x80000890]:sw t6, 224(s1)
Current Store : [0x80000894] : sw a4, 228(s1) -- Store: [0x800027b4]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x9a and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008ac]:fmsub.s t6, t5, t4, t3, dyn
	-[0x800008b0]:csrrs a4, fcsr, zero
	-[0x800008b4]:sw t6, 232(s1)
Current Store : [0x800008b8] : sw a4, 236(s1) -- Store: [0x800027bc]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x9b and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008d0]:fmsub.s t6, t5, t4, t3, dyn
	-[0x800008d4]:csrrs a4, fcsr, zero
	-[0x800008d8]:sw t6, 240(s1)
Current Store : [0x800008dc] : sw a4, 244(s1) -- Store: [0x800027c4]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x9c and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008f4]:fmsub.s t6, t5, t4, t3, dyn
	-[0x800008f8]:csrrs a4, fcsr, zero
	-[0x800008fc]:sw t6, 248(s1)
Current Store : [0x80000900] : sw a4, 252(s1) -- Store: [0x800027cc]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x9d and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000918]:fmsub.s t6, t5, t4, t3, dyn
	-[0x8000091c]:csrrs a4, fcsr, zero
	-[0x80000920]:sw t6, 256(s1)
Current Store : [0x80000924] : sw a4, 260(s1) -- Store: [0x800027d4]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x9e and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000093c]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000940]:csrrs a4, fcsr, zero
	-[0x80000944]:sw t6, 264(s1)
Current Store : [0x80000948] : sw a4, 268(s1) -- Store: [0x800027dc]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x9f and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000960]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000964]:csrrs a4, fcsr, zero
	-[0x80000968]:sw t6, 272(s1)
Current Store : [0x8000096c] : sw a4, 276(s1) -- Store: [0x800027e4]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xa0 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000984]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000988]:csrrs a4, fcsr, zero
	-[0x8000098c]:sw t6, 280(s1)
Current Store : [0x80000990] : sw a4, 284(s1) -- Store: [0x800027ec]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xa1 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009a8]:fmsub.s t6, t5, t4, t3, dyn
	-[0x800009ac]:csrrs a4, fcsr, zero
	-[0x800009b0]:sw t6, 288(s1)
Current Store : [0x800009b4] : sw a4, 292(s1) -- Store: [0x800027f4]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xa2 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009cc]:fmsub.s t6, t5, t4, t3, dyn
	-[0x800009d0]:csrrs a4, fcsr, zero
	-[0x800009d4]:sw t6, 296(s1)
Current Store : [0x800009d8] : sw a4, 300(s1) -- Store: [0x800027fc]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xa3 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009f0]:fmsub.s t6, t5, t4, t3, dyn
	-[0x800009f4]:csrrs a4, fcsr, zero
	-[0x800009f8]:sw t6, 304(s1)
Current Store : [0x800009fc] : sw a4, 308(s1) -- Store: [0x80002804]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xa4 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a14]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000a18]:csrrs a4, fcsr, zero
	-[0x80000a1c]:sw t6, 312(s1)
Current Store : [0x80000a20] : sw a4, 316(s1) -- Store: [0x8000280c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xa5 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a38]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000a3c]:csrrs a4, fcsr, zero
	-[0x80000a40]:sw t6, 320(s1)
Current Store : [0x80000a44] : sw a4, 324(s1) -- Store: [0x80002814]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xa6 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a5c]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000a60]:csrrs a4, fcsr, zero
	-[0x80000a64]:sw t6, 328(s1)
Current Store : [0x80000a68] : sw a4, 332(s1) -- Store: [0x8000281c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xa7 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a80]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000a84]:csrrs a4, fcsr, zero
	-[0x80000a88]:sw t6, 336(s1)
Current Store : [0x80000a8c] : sw a4, 340(s1) -- Store: [0x80002824]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xa8 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000aa4]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000aa8]:csrrs a4, fcsr, zero
	-[0x80000aac]:sw t6, 344(s1)
Current Store : [0x80000ab0] : sw a4, 348(s1) -- Store: [0x8000282c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xa9 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ac8]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000acc]:csrrs a4, fcsr, zero
	-[0x80000ad0]:sw t6, 352(s1)
Current Store : [0x80000ad4] : sw a4, 356(s1) -- Store: [0x80002834]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xaa and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000aec]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000af0]:csrrs a4, fcsr, zero
	-[0x80000af4]:sw t6, 360(s1)
Current Store : [0x80000af8] : sw a4, 364(s1) -- Store: [0x8000283c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xab and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b10]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000b14]:csrrs a4, fcsr, zero
	-[0x80000b18]:sw t6, 368(s1)
Current Store : [0x80000b1c] : sw a4, 372(s1) -- Store: [0x80002844]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xac and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b34]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000b38]:csrrs a4, fcsr, zero
	-[0x80000b3c]:sw t6, 376(s1)
Current Store : [0x80000b40] : sw a4, 380(s1) -- Store: [0x8000284c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xbc and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b58]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000b5c]:csrrs a4, fcsr, zero
	-[0x80000b60]:sw t6, 384(s1)
Current Store : [0x80000b64] : sw a4, 388(s1) -- Store: [0x80002854]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x20 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b7c]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000b80]:csrrs a4, fcsr, zero
	-[0x80000b84]:sw t6, 392(s1)
Current Store : [0x80000b88] : sw a4, 396(s1) -- Store: [0x8000285c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x66 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ba0]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000ba4]:csrrs a4, fcsr, zero
	-[0x80000ba8]:sw t6, 400(s1)
Current Store : [0x80000bac] : sw a4, 404(s1) -- Store: [0x80002864]:0x00000005




Last Coverpoint : ['mnemonic : fmsub.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs3 : x28', 'rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x67 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bc4]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000bc8]:csrrs a4, fcsr, zero
	-[0x80000bcc]:sw t6, 408(s1)
Current Store : [0x80000bd0] : sw a4, 412(s1) -- Store: [0x8000286c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x68 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000be8]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000bec]:csrrs a4, fcsr, zero
	-[0x80000bf0]:sw t6, 416(s1)
Current Store : [0x80000bf4] : sw a4, 420(s1) -- Store: [0x80002874]:0x00000005




Last Coverpoint : ['mnemonic : fmsub.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs3 : x28', 'rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x69 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c0c]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000c10]:csrrs a4, fcsr, zero
	-[0x80000c14]:sw t6, 424(s1)
Current Store : [0x80000c18] : sw a4, 428(s1) -- Store: [0x8000287c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x6a and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c30]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000c34]:csrrs a4, fcsr, zero
	-[0x80000c38]:sw t6, 432(s1)
Current Store : [0x80000c3c] : sw a4, 436(s1) -- Store: [0x80002884]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x6b and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c54]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000c58]:csrrs a4, fcsr, zero
	-[0x80000c5c]:sw t6, 440(s1)
Current Store : [0x80000c60] : sw a4, 444(s1) -- Store: [0x8000288c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x6c and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c78]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000c7c]:csrrs a4, fcsr, zero
	-[0x80000c80]:sw t6, 448(s1)
Current Store : [0x80000c84] : sw a4, 452(s1) -- Store: [0x80002894]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x6d and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c9c]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000ca0]:csrrs a4, fcsr, zero
	-[0x80000ca4]:sw t6, 456(s1)
Current Store : [0x80000ca8] : sw a4, 460(s1) -- Store: [0x8000289c]:0x00000005




Last Coverpoint : ['mnemonic : fmsub.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs3 : x28', 'rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x6e and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cc0]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000cc4]:csrrs a4, fcsr, zero
	-[0x80000cc8]:sw t6, 464(s1)
Current Store : [0x80000ccc] : sw a4, 468(s1) -- Store: [0x800028a4]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x6f and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ce4]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000ce8]:csrrs a4, fcsr, zero
	-[0x80000cec]:sw t6, 472(s1)
Current Store : [0x80000cf0] : sw a4, 476(s1) -- Store: [0x800028ac]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x82 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d08]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000d0c]:csrrs a4, fcsr, zero
	-[0x80000d10]:sw t6, 480(s1)
Current Store : [0x80000d14] : sw a4, 484(s1) -- Store: [0x800028b4]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x83 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d2c]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000d30]:csrrs a4, fcsr, zero
	-[0x80000d34]:sw t6, 488(s1)
Current Store : [0x80000d38] : sw a4, 492(s1) -- Store: [0x800028bc]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x84 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d50]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000d54]:csrrs a4, fcsr, zero
	-[0x80000d58]:sw t6, 496(s1)
Current Store : [0x80000d5c] : sw a4, 500(s1) -- Store: [0x800028c4]:0x00000005




Last Coverpoint : ['mnemonic : fmsub.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs3 : x28', 'rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x87 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d74]:fmsub.s t6, t5, t4, t3, dyn
	-[0x80000d78]:csrrs a4, fcsr, zero
	-[0x80000d7c]:sw t6, 504(s1)
Current Store : [0x80000d80] : sw a4, 508(s1) -- Store: [0x800028cc]:0x00000005





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                                                                                      coverpoints                                                                                                                                                                      |                                                         code                                                         |
|---:|--------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002610]<br>0x7F800000|- mnemonic : fmsub.s<br> - rs1 : x31<br> - rs2 : x30<br> - rd : x31<br> - rs3 : x31<br> - rs1 == rd == rs3 != rs2<br>                                                                                                                                                                                                                                  |[0x80000128]:fmsub.s t6, t6, t5, t6, dyn<br> [0x8000012c]:csrrs tp, fcsr, zero<br> [0x80000130]:sw t6, 0(ra)<br>      |
|   2|[0x80002618]<br>0x7F800000|- rs1 : x29<br> - rs2 : x29<br> - rd : x30<br> - rs3 : x28<br> - rs1 == rs2 != rs3 and rs1 == rs2 != rd and rd != rs3<br>                                                                                                                                                                                                                              |[0x8000014c]:fmsub.s t5, t4, t4, t3, dyn<br> [0x80000150]:csrrs tp, fcsr, zero<br> [0x80000154]:sw t5, 8(ra)<br>      |
|   3|[0x80002620]<br>0x7F800000|- rs1 : x28<br> - rs2 : x31<br> - rd : x28<br> - rs3 : x30<br> - rs1 == rd != rs2 and rs1 == rd != rs3 and rs3 != rs2<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x67 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                               |[0x80000170]:fmsub.s t3, t3, t6, t5, dyn<br> [0x80000174]:csrrs tp, fcsr, zero<br> [0x80000178]:sw t3, 16(ra)<br>     |
|   4|[0x80002628]<br>0x7F800000|- rs1 : x30<br> - rs2 : x27<br> - rd : x27<br> - rs3 : x27<br> - rd == rs2 == rs3 != rs1<br>                                                                                                                                                                                                                                                           |[0x80000194]:fmsub.s s11, t5, s11, s11, dyn<br> [0x80000198]:csrrs tp, fcsr, zero<br> [0x8000019c]:sw s11, 24(ra)<br> |
|   5|[0x80002630]<br>0x7F800000|- rs1 : x27<br> - rs2 : x28<br> - rd : x29<br> - rs3 : x29<br> - rs3 == rd != rs1 and rs3 == rd != rs2 and rs2 != rs1<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x69 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                               |[0x800001b8]:fmsub.s t4, s11, t3, t4, dyn<br> [0x800001bc]:csrrs tp, fcsr, zero<br> [0x800001c0]:sw t4, 32(ra)<br>    |
|   6|[0x80002638]<br>0x7F800000|- rs1 : x26<br> - rs2 : x26<br> - rd : x26<br> - rs3 : x25<br> - rs1 == rs2 == rd != rs3<br>                                                                                                                                                                                                                                                           |[0x800001dc]:fmsub.s s10, s10, s10, s9, dyn<br> [0x800001e0]:csrrs tp, fcsr, zero<br> [0x800001e4]:sw s10, 40(ra)<br> |
|   7|[0x80002640]<br>0x7F800000|- rs1 : x24<br> - rs2 : x24<br> - rd : x24<br> - rs3 : x24<br> - rs1 == rs2 == rs3 == rd<br>                                                                                                                                                                                                                                                           |[0x80000200]:fmsub.s s8, s8, s8, s8, dyn<br> [0x80000204]:csrrs tp, fcsr, zero<br> [0x80000208]:sw s8, 48(ra)<br>     |
|   8|[0x80002648]<br>0x7F800000|- rs1 : x23<br> - rs2 : x22<br> - rd : x25<br> - rs3 : x22<br> - rs2 == rs3 != rs1 and rs2 == rs3 != rd and rd != rs1<br>                                                                                                                                                                                                                              |[0x80000224]:fmsub.s s9, s7, s6, s6, dyn<br> [0x80000228]:csrrs tp, fcsr, zero<br> [0x8000022c]:sw s9, 56(ra)<br>     |
|   9|[0x80002650]<br>0x7F800000|- rs1 : x21<br> - rs2 : x21<br> - rd : x23<br> - rs3 : x21<br> - rs1 == rs2 == rs3 != rd<br>                                                                                                                                                                                                                                                           |[0x80000248]:fmsub.s s7, s5, s5, s5, dyn<br> [0x8000024c]:csrrs tp, fcsr, zero<br> [0x80000250]:sw s7, 64(ra)<br>     |
|  10|[0x80002658]<br>0x7F800000|- rs1 : x25<br> - rs2 : x20<br> - rd : x20<br> - rs3 : x26<br> - rs2 == rd != rs1 and rs2 == rd != rs3 and rs3 != rs1<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x6e and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                               |[0x8000026c]:fmsub.s s4, s9, s4, s10, dyn<br> [0x80000270]:csrrs tp, fcsr, zero<br> [0x80000274]:sw s4, 72(ra)<br>    |
|  11|[0x80002660]<br>0x7F800000|- rs1 : x20<br> - rs2 : x25<br> - rd : x22<br> - rs3 : x20<br> - rs1 == rs3 != rs2 and rs1 == rs3 != rd and rd != rs2<br>                                                                                                                                                                                                                              |[0x80000290]:fmsub.s s6, s4, s9, s4, dyn<br> [0x80000294]:csrrs tp, fcsr, zero<br> [0x80000298]:sw s6, 80(ra)<br>     |
|  12|[0x80002668]<br>0x7F800000|- rs1 : x22<br> - rs2 : x23<br> - rd : x21<br> - rs3 : x19<br> - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x70 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x800002b4]:fmsub.s s5, s6, s7, s3, dyn<br> [0x800002b8]:csrrs tp, fcsr, zero<br> [0x800002bc]:sw s5, 88(ra)<br>     |
|  13|[0x80002670]<br>0x7F800000|- rs1 : x18<br> - rs2 : x17<br> - rd : x19<br> - rs3 : x23<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x71 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                          |[0x800002d8]:fmsub.s s3, s2, a7, s7, dyn<br> [0x800002dc]:csrrs tp, fcsr, zero<br> [0x800002e0]:sw s3, 96(ra)<br>     |
|  14|[0x80002678]<br>0x7F800000|- rs1 : x17<br> - rs2 : x19<br> - rd : x18<br> - rs3 : x16<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x72 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                          |[0x800002fc]:fmsub.s s2, a7, s3, a6, dyn<br> [0x80000300]:csrrs tp, fcsr, zero<br> [0x80000304]:sw s2, 104(ra)<br>    |
|  15|[0x80002680]<br>0x7F800000|- rs1 : x19<br> - rs2 : x16<br> - rd : x17<br> - rs3 : x18<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x73 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                          |[0x80000320]:fmsub.s a7, s3, a6, s2, dyn<br> [0x80000324]:csrrs tp, fcsr, zero<br> [0x80000328]:sw a7, 112(ra)<br>    |
|  16|[0x80002688]<br>0x7F800000|- rs1 : x15<br> - rs2 : x18<br> - rd : x16<br> - rs3 : x17<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x74 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                          |[0x80000344]:fmsub.s a6, a5, s2, a7, dyn<br> [0x80000348]:csrrs tp, fcsr, zero<br> [0x8000034c]:sw a6, 120(ra)<br>    |
|  17|[0x80002690]<br>0x7F800000|- rs1 : x16<br> - rs2 : x14<br> - rd : x15<br> - rs3 : x13<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x75 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                          |[0x80000368]:fmsub.s a5, a6, a4, a3, dyn<br> [0x8000036c]:csrrs tp, fcsr, zero<br> [0x80000370]:sw a5, 128(ra)<br>    |
|  18|[0x80002698]<br>0x7F800000|- rs1 : x13<br> - rs2 : x15<br> - rd : x14<br> - rs3 : x12<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x76 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                          |[0x8000038c]:fmsub.s a4, a3, a5, a2, dyn<br> [0x80000390]:csrrs tp, fcsr, zero<br> [0x80000394]:sw a4, 136(ra)<br>    |
|  19|[0x800026a0]<br>0x7F800000|- rs1 : x14<br> - rs2 : x12<br> - rd : x13<br> - rs3 : x15<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x77 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                          |[0x800003b0]:fmsub.s a3, a4, a2, a5, dyn<br> [0x800003b4]:csrrs tp, fcsr, zero<br> [0x800003b8]:sw a3, 144(ra)<br>    |
|  20|[0x800026a8]<br>0x7F800000|- rs1 : x11<br> - rs2 : x13<br> - rd : x12<br> - rs3 : x14<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x78 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                          |[0x800003d4]:fmsub.s a2, a1, a3, a4, dyn<br> [0x800003d8]:csrrs tp, fcsr, zero<br> [0x800003dc]:sw a2, 152(ra)<br>    |
|  21|[0x800026b0]<br>0x7F800000|- rs1 : x12<br> - rs2 : x10<br> - rd : x11<br> - rs3 : x9<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x79 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                           |[0x800003f8]:fmsub.s a1, a2, a0, s1, dyn<br> [0x800003fc]:csrrs tp, fcsr, zero<br> [0x80000400]:sw a1, 160(ra)<br>    |
|  22|[0x800026b8]<br>0x7F800000|- rs1 : x9<br> - rs2 : x11<br> - rd : x10<br> - rs3 : x8<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x7a and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                            |[0x80000424]:fmsub.s a0, s1, a1, fp, dyn<br> [0x80000428]:csrrs a4, fcsr, zero<br> [0x8000042c]:sw a0, 168(ra)<br>    |
|  23|[0x800026c0]<br>0x7F800000|- rs1 : x10<br> - rs2 : x8<br> - rd : x9<br> - rs3 : x11<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x7b and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                            |[0x80000448]:fmsub.s s1, a0, fp, a1, dyn<br> [0x8000044c]:csrrs a4, fcsr, zero<br> [0x80000450]:sw s1, 176(ra)<br>    |
|  24|[0x800026c8]<br>0x7F800000|- rs1 : x7<br> - rs2 : x9<br> - rd : x8<br> - rs3 : x10<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x7c and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                             |[0x8000046c]:fmsub.s fp, t2, s1, a0, dyn<br> [0x80000470]:csrrs a4, fcsr, zero<br> [0x80000474]:sw fp, 184(ra)<br>    |
|  25|[0x800026d0]<br>0x7F800000|- rs1 : x8<br> - rs2 : x6<br> - rd : x7<br> - rs3 : x5<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x7d and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000498]:fmsub.s t2, fp, t1, t0, dyn<br> [0x8000049c]:csrrs a4, fcsr, zero<br> [0x800004a0]:sw t2, 0(s1)<br>      |
|  26|[0x800026d8]<br>0x7F800000|- rs1 : x5<br> - rs2 : x7<br> - rd : x6<br> - rs3 : x4<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x7e and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800004bc]:fmsub.s t1, t0, t2, tp, dyn<br> [0x800004c0]:csrrs a4, fcsr, zero<br> [0x800004c4]:sw t1, 8(s1)<br>      |
|  27|[0x800026e0]<br>0x7F800000|- rs1 : x6<br> - rs2 : x4<br> - rd : x5<br> - rs3 : x7<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x7f and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800004e0]:fmsub.s t0, t1, tp, t2, dyn<br> [0x800004e4]:csrrs a4, fcsr, zero<br> [0x800004e8]:sw t0, 16(s1)<br>     |
|  28|[0x800026e8]<br>0x7F800000|- rs1 : x3<br> - rs2 : x5<br> - rd : x4<br> - rs3 : x6<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x80 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000504]:fmsub.s tp, gp, t0, t1, dyn<br> [0x80000508]:csrrs a4, fcsr, zero<br> [0x8000050c]:sw tp, 24(s1)<br>     |
|  29|[0x800026f0]<br>0x7F800000|- rs1 : x4<br> - rs2 : x2<br> - rd : x3<br> - rs3 : x1<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x81 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000528]:fmsub.s gp, tp, sp, ra, dyn<br> [0x8000052c]:csrrs a4, fcsr, zero<br> [0x80000530]:sw gp, 32(s1)<br>     |
|  30|[0x800026f8]<br>0x7F800000|- rs1 : x1<br> - rs2 : x3<br> - rd : x2<br> - rs3 : x0<br>                                                                                                                                                                                                                                                                                             |[0x8000054c]:fmsub.s sp, ra, gp, zero, dyn<br> [0x80000550]:csrrs a4, fcsr, zero<br> [0x80000554]:sw sp, 40(s1)<br>   |
|  31|[0x80002700]<br>0xC1F980C8|- rs1 : x2<br> - rs2 : x0<br> - rd : x1<br> - rs3 : x3<br>                                                                                                                                                                                                                                                                                             |[0x80000570]:fmsub.s ra, sp, zero, gp, dyn<br> [0x80000574]:csrrs a4, fcsr, zero<br> [0x80000578]:sw ra, 48(s1)<br>   |
|  32|[0x80002708]<br>0xC27980C8|- rs1 : x0<br>                                                                                                                                                                                                                                                                                                                                         |[0x80000594]:fmsub.s t6, zero, t5, t4, dyn<br> [0x80000598]:csrrs a4, fcsr, zero<br> [0x8000059c]:sw t6, 56(s1)<br>   |
|  33|[0x80002710]<br>0x7F800000|- rs2 : x1<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x85 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                          |[0x800005b8]:fmsub.s t6, t5, ra, t4, dyn<br> [0x800005bc]:csrrs a4, fcsr, zero<br> [0x800005c0]:sw t6, 64(s1)<br>     |
|  34|[0x80002718]<br>0x7F800000|- rs3 : x2<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x86 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                          |[0x800005dc]:fmsub.s t6, t5, t4, sp, dyn<br> [0x800005e0]:csrrs a4, fcsr, zero<br> [0x800005e4]:sw t6, 72(s1)<br>     |
|  35|[0x80002720]<br>0x00000000|- rd : x0<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x87 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                           |[0x80000600]:fmsub.s zero, t6, t5, t4, dyn<br> [0x80000604]:csrrs a4, fcsr, zero<br> [0x80000608]:sw zero, 80(s1)<br> |
|  36|[0x80002728]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x88 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000624]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000628]:csrrs a4, fcsr, zero<br> [0x8000062c]:sw t6, 88(s1)<br>     |
|  37|[0x80002730]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x89 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000648]:fmsub.s t6, t5, t4, t3, dyn<br> [0x8000064c]:csrrs a4, fcsr, zero<br> [0x80000650]:sw t6, 96(s1)<br>     |
|  38|[0x80002738]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x8a and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000066c]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000670]:csrrs a4, fcsr, zero<br> [0x80000674]:sw t6, 104(s1)<br>    |
|  39|[0x80002740]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x8b and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000690]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000694]:csrrs a4, fcsr, zero<br> [0x80000698]:sw t6, 112(s1)<br>    |
|  40|[0x80002748]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x8c and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800006b4]:fmsub.s t6, t5, t4, t3, dyn<br> [0x800006b8]:csrrs a4, fcsr, zero<br> [0x800006bc]:sw t6, 120(s1)<br>    |
|  41|[0x80002750]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x8d and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800006d8]:fmsub.s t6, t5, t4, t3, dyn<br> [0x800006dc]:csrrs a4, fcsr, zero<br> [0x800006e0]:sw t6, 128(s1)<br>    |
|  42|[0x80002758]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x8e and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800006fc]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000700]:csrrs a4, fcsr, zero<br> [0x80000704]:sw t6, 136(s1)<br>    |
|  43|[0x80002760]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x8f and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000720]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000724]:csrrs a4, fcsr, zero<br> [0x80000728]:sw t6, 144(s1)<br>    |
|  44|[0x80002768]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x90 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000744]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000748]:csrrs a4, fcsr, zero<br> [0x8000074c]:sw t6, 152(s1)<br>    |
|  45|[0x80002770]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x91 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000768]:fmsub.s t6, t5, t4, t3, dyn<br> [0x8000076c]:csrrs a4, fcsr, zero<br> [0x80000770]:sw t6, 160(s1)<br>    |
|  46|[0x80002778]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x92 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000078c]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000790]:csrrs a4, fcsr, zero<br> [0x80000794]:sw t6, 168(s1)<br>    |
|  47|[0x80002780]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x93 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800007b0]:fmsub.s t6, t5, t4, t3, dyn<br> [0x800007b4]:csrrs a4, fcsr, zero<br> [0x800007b8]:sw t6, 176(s1)<br>    |
|  48|[0x80002788]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x94 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800007d4]:fmsub.s t6, t5, t4, t3, dyn<br> [0x800007d8]:csrrs a4, fcsr, zero<br> [0x800007dc]:sw t6, 184(s1)<br>    |
|  49|[0x80002790]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x95 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800007f8]:fmsub.s t6, t5, t4, t3, dyn<br> [0x800007fc]:csrrs a4, fcsr, zero<br> [0x80000800]:sw t6, 192(s1)<br>    |
|  50|[0x80002798]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x96 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000081c]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000820]:csrrs a4, fcsr, zero<br> [0x80000824]:sw t6, 200(s1)<br>    |
|  51|[0x800027a0]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x97 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000840]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000844]:csrrs a4, fcsr, zero<br> [0x80000848]:sw t6, 208(s1)<br>    |
|  52|[0x800027a8]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x98 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000864]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000868]:csrrs a4, fcsr, zero<br> [0x8000086c]:sw t6, 216(s1)<br>    |
|  53|[0x800027b0]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x99 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000888]:fmsub.s t6, t5, t4, t3, dyn<br> [0x8000088c]:csrrs a4, fcsr, zero<br> [0x80000890]:sw t6, 224(s1)<br>    |
|  54|[0x800027b8]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x9a and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800008ac]:fmsub.s t6, t5, t4, t3, dyn<br> [0x800008b0]:csrrs a4, fcsr, zero<br> [0x800008b4]:sw t6, 232(s1)<br>    |
|  55|[0x800027c0]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x9b and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800008d0]:fmsub.s t6, t5, t4, t3, dyn<br> [0x800008d4]:csrrs a4, fcsr, zero<br> [0x800008d8]:sw t6, 240(s1)<br>    |
|  56|[0x800027c8]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x9c and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800008f4]:fmsub.s t6, t5, t4, t3, dyn<br> [0x800008f8]:csrrs a4, fcsr, zero<br> [0x800008fc]:sw t6, 248(s1)<br>    |
|  57|[0x800027d0]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x9d and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000918]:fmsub.s t6, t5, t4, t3, dyn<br> [0x8000091c]:csrrs a4, fcsr, zero<br> [0x80000920]:sw t6, 256(s1)<br>    |
|  58|[0x800027d8]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x9e and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000093c]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000940]:csrrs a4, fcsr, zero<br> [0x80000944]:sw t6, 264(s1)<br>    |
|  59|[0x800027e0]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x9f and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000960]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000964]:csrrs a4, fcsr, zero<br> [0x80000968]:sw t6, 272(s1)<br>    |
|  60|[0x800027e8]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xa0 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000984]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000988]:csrrs a4, fcsr, zero<br> [0x8000098c]:sw t6, 280(s1)<br>    |
|  61|[0x800027f0]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xa1 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800009a8]:fmsub.s t6, t5, t4, t3, dyn<br> [0x800009ac]:csrrs a4, fcsr, zero<br> [0x800009b0]:sw t6, 288(s1)<br>    |
|  62|[0x800027f8]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xa2 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800009cc]:fmsub.s t6, t5, t4, t3, dyn<br> [0x800009d0]:csrrs a4, fcsr, zero<br> [0x800009d4]:sw t6, 296(s1)<br>    |
|  63|[0x80002800]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xa3 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800009f0]:fmsub.s t6, t5, t4, t3, dyn<br> [0x800009f4]:csrrs a4, fcsr, zero<br> [0x800009f8]:sw t6, 304(s1)<br>    |
|  64|[0x80002808]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xa4 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000a14]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000a18]:csrrs a4, fcsr, zero<br> [0x80000a1c]:sw t6, 312(s1)<br>    |
|  65|[0x80002810]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xa5 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000a38]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000a3c]:csrrs a4, fcsr, zero<br> [0x80000a40]:sw t6, 320(s1)<br>    |
|  66|[0x80002818]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xa6 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000a5c]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000a60]:csrrs a4, fcsr, zero<br> [0x80000a64]:sw t6, 328(s1)<br>    |
|  67|[0x80002820]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xa7 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000a80]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000a84]:csrrs a4, fcsr, zero<br> [0x80000a88]:sw t6, 336(s1)<br>    |
|  68|[0x80002828]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xa8 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000aa4]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000aa8]:csrrs a4, fcsr, zero<br> [0x80000aac]:sw t6, 344(s1)<br>    |
|  69|[0x80002830]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xa9 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000ac8]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000acc]:csrrs a4, fcsr, zero<br> [0x80000ad0]:sw t6, 352(s1)<br>    |
|  70|[0x80002838]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xaa and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000aec]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000af0]:csrrs a4, fcsr, zero<br> [0x80000af4]:sw t6, 360(s1)<br>    |
|  71|[0x80002840]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xab and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000b10]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000b14]:csrrs a4, fcsr, zero<br> [0x80000b18]:sw t6, 368(s1)<br>    |
|  72|[0x80002848]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xac and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000b34]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000b38]:csrrs a4, fcsr, zero<br> [0x80000b3c]:sw t6, 376(s1)<br>    |
|  73|[0x80002850]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0xbc and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000b58]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000b5c]:csrrs a4, fcsr, zero<br> [0x80000b60]:sw t6, 384(s1)<br>    |
|  74|[0x80002858]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x20 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000b7c]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000b80]:csrrs a4, fcsr, zero<br> [0x80000b84]:sw t6, 392(s1)<br>    |
|  75|[0x80002860]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x66 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000ba0]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000ba4]:csrrs a4, fcsr, zero<br> [0x80000ba8]:sw t6, 400(s1)<br>    |
|  76|[0x80002870]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x68 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000be8]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000bec]:csrrs a4, fcsr, zero<br> [0x80000bf0]:sw t6, 416(s1)<br>    |
|  77|[0x80002880]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x6a and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000c30]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000c34]:csrrs a4, fcsr, zero<br> [0x80000c38]:sw t6, 432(s1)<br>    |
|  78|[0x80002888]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x6b and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000c54]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000c58]:csrrs a4, fcsr, zero<br> [0x80000c5c]:sw t6, 440(s1)<br>    |
|  79|[0x80002890]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x6c and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000c78]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000c7c]:csrrs a4, fcsr, zero<br> [0x80000c80]:sw t6, 448(s1)<br>    |
|  80|[0x80002898]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x6d and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000c9c]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000ca0]:csrrs a4, fcsr, zero<br> [0x80000ca4]:sw t6, 456(s1)<br>    |
|  81|[0x800028a8]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x6f and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000ce4]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000ce8]:csrrs a4, fcsr, zero<br> [0x80000cec]:sw t6, 472(s1)<br>    |
|  82|[0x800028b0]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x82 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000d08]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000d0c]:csrrs a4, fcsr, zero<br> [0x80000d10]:sw t6, 480(s1)<br>    |
|  83|[0x800028b8]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x83 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000d2c]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000d30]:csrrs a4, fcsr, zero<br> [0x80000d34]:sw t6, 488(s1)<br>    |
|  84|[0x800028c0]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x22b50f and fs3 == 0 and fe3 == 0x84 and fm3 == 0x7980c8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000d50]:fmsub.s t6, t5, t4, t3, dyn<br> [0x80000d54]:csrrs a4, fcsr, zero<br> [0x80000d58]:sw t6, 496(s1)<br>    |
