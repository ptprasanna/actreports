
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80001390')]      |
| SIG_REGION                | [('0x80003610', '0x80003ab0', '296 words')]      |
| COV_LABELS                | fsub_b4      |
| TEST_NAME                 | /home/reg/work/zfinx/fsub.s/work/fsub_b4-01.S/ref.S    |
| Total Number of coverpoints| 242     |
| Total Coverpoints Hit     | 242      |
| Total Signature Updates   | 294      |
| STAT1                     | 144      |
| STAT2                     | 3      |
| STAT3                     | 0     |
| STAT4                     | 147     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x800012b4]:fsub.s t6, t5, t4, dyn
      [0x800012b8]:csrrs a3, fcsr, zero
      [0x800012bc]:sw t6, 928(s1)
 -- Signature Address: 0x80003a70 Data: 0x7F3FFFFF
 -- Redundant Coverpoints hit by the op
      - mnemonic : fsub.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x2633ba and  fcsr == 0x0 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80001314]:fsub.s t6, t5, t4, dyn
      [0x80001318]:csrrs a3, fcsr, zero
      [0x8000131c]:sw t6, 952(s1)
 -- Signature Address: 0x80003a88 Data: 0x7F400000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fsub.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x2633ba and  fcsr == 0x60 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80001374]:fsub.s t6, t5, t4, dyn
      [0x80001378]:csrrs a3, fcsr, zero
      [0x8000137c]:sw t6, 976(s1)
 -- Signature Address: 0x80003aa0 Data: 0x7F3FFFFE
 -- Redundant Coverpoints hit by the op
      - mnemonic : fsub.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 1 and fe2 == 0xfc and fm2 == 0x3691a3 and  fcsr == 0x20 and rm_val == 7   #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fsub.s', 'rs1 : x31', 'rs2 : x30', 'rd : x31', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x2633ba and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000124]:fsub.s t6, t6, t5, dyn
	-[0x80000128]:csrrs tp, fcsr, zero
	-[0x8000012c]:sw t6, 0(ra)
Current Store : [0x80000130] : sw tp, 4(ra) -- Store: [0x80003614]:0x00000001




Last Coverpoint : ['rs1 : x29', 'rs2 : x29', 'rd : x29', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x80000144]:fsub.s t4, t4, t4, dyn
	-[0x80000148]:csrrs tp, fcsr, zero
	-[0x8000014c]:sw t4, 8(ra)
Current Store : [0x80000150] : sw tp, 12(ra) -- Store: [0x8000361c]:0x00000020




Last Coverpoint : ['rs1 : x28', 'rs2 : x28', 'rd : x30', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x80000164]:fsub.s t5, t3, t3, dyn
	-[0x80000168]:csrrs tp, fcsr, zero
	-[0x8000016c]:sw t5, 16(ra)
Current Store : [0x80000170] : sw tp, 20(ra) -- Store: [0x80003624]:0x00000040




Last Coverpoint : ['rs1 : x30', 'rs2 : x27', 'rd : x27', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x2633ba and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000184]:fsub.s s11, t5, s11, dyn
	-[0x80000188]:csrrs tp, fcsr, zero
	-[0x8000018c]:sw s11, 24(ra)
Current Store : [0x80000190] : sw tp, 28(ra) -- Store: [0x8000362c]:0x00000061




Last Coverpoint : ['rs1 : x27', 'rs2 : x31', 'rd : x28', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x2633ba and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001a4]:fsub.s t3, s11, t6, dyn
	-[0x800001a8]:csrrs tp, fcsr, zero
	-[0x800001ac]:sw t3, 32(ra)
Current Store : [0x800001b0] : sw tp, 36(ra) -- Store: [0x80003634]:0x00000081




Last Coverpoint : ['rs1 : x25', 'rs2 : x24', 'rd : x26', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x12a50c and fs2 == 1 and fe2 == 0xfc and fm2 == 0x356bca and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001c4]:fsub.s s10, s9, s8, dyn
	-[0x800001c8]:csrrs tp, fcsr, zero
	-[0x800001cc]:sw s10, 40(ra)
Current Store : [0x800001d0] : sw tp, 44(ra) -- Store: [0x8000363c]:0x00000001




Last Coverpoint : ['rs1 : x24', 'rs2 : x26', 'rd : x25', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x12a50c and fs2 == 1 and fe2 == 0xfc and fm2 == 0x356bca and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001e4]:fsub.s s9, s8, s10, dyn
	-[0x800001e8]:csrrs tp, fcsr, zero
	-[0x800001ec]:sw s9, 48(ra)
Current Store : [0x800001f0] : sw tp, 52(ra) -- Store: [0x80003644]:0x00000021




Last Coverpoint : ['rs1 : x26', 'rs2 : x25', 'rd : x24', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x12a50c and fs2 == 1 and fe2 == 0xfc and fm2 == 0x356bca and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000204]:fsub.s s8, s10, s9, dyn
	-[0x80000208]:csrrs tp, fcsr, zero
	-[0x8000020c]:sw s8, 56(ra)
Current Store : [0x80000210] : sw tp, 60(ra) -- Store: [0x8000364c]:0x00000041




Last Coverpoint : ['rs1 : x22', 'rs2 : x21', 'rd : x23', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x12a50c and fs2 == 1 and fe2 == 0xfc and fm2 == 0x356bca and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000224]:fsub.s s7, s6, s5, dyn
	-[0x80000228]:csrrs tp, fcsr, zero
	-[0x8000022c]:sw s7, 64(ra)
Current Store : [0x80000230] : sw tp, 68(ra) -- Store: [0x80003654]:0x00000061




Last Coverpoint : ['rs1 : x21', 'rs2 : x23', 'rd : x22', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x12a50c and fs2 == 1 and fe2 == 0xfc and fm2 == 0x356bca and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000244]:fsub.s s6, s5, s7, dyn
	-[0x80000248]:csrrs tp, fcsr, zero
	-[0x8000024c]:sw s6, 72(ra)
Current Store : [0x80000250] : sw tp, 76(ra) -- Store: [0x8000365c]:0x00000081




Last Coverpoint : ['rs1 : x23', 'rs2 : x22', 'rd : x21', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 1 and fe2 == 0xfd and fm2 == 0x41c0bf and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000264]:fsub.s s5, s7, s6, dyn
	-[0x80000268]:csrrs tp, fcsr, zero
	-[0x8000026c]:sw s5, 80(ra)
Current Store : [0x80000270] : sw tp, 84(ra) -- Store: [0x80003664]:0x00000000




Last Coverpoint : ['rs1 : x19', 'rs2 : x18', 'rd : x20', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 1 and fe2 == 0xfd and fm2 == 0x41c0bf and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000284]:fsub.s s4, s3, s2, dyn
	-[0x80000288]:csrrs tp, fcsr, zero
	-[0x8000028c]:sw s4, 88(ra)
Current Store : [0x80000290] : sw tp, 92(ra) -- Store: [0x8000366c]:0x00000020




Last Coverpoint : ['rs1 : x18', 'rs2 : x20', 'rd : x19', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 1 and fe2 == 0xfd and fm2 == 0x41c0bf and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002a4]:fsub.s s3, s2, s4, dyn
	-[0x800002a8]:csrrs tp, fcsr, zero
	-[0x800002ac]:sw s3, 96(ra)
Current Store : [0x800002b0] : sw tp, 100(ra) -- Store: [0x80003674]:0x00000040




Last Coverpoint : ['rs1 : x20', 'rs2 : x19', 'rd : x18', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 1 and fe2 == 0xfd and fm2 == 0x41c0bf and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002c4]:fsub.s s2, s4, s3, dyn
	-[0x800002c8]:csrrs tp, fcsr, zero
	-[0x800002cc]:sw s2, 104(ra)
Current Store : [0x800002d0] : sw tp, 108(ra) -- Store: [0x8000367c]:0x00000060




Last Coverpoint : ['rs1 : x16', 'rs2 : x15', 'rd : x17', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 1 and fe2 == 0xfd and fm2 == 0x41c0bf and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002e4]:fsub.s a7, a6, a5, dyn
	-[0x800002e8]:csrrs tp, fcsr, zero
	-[0x800002ec]:sw a7, 112(ra)
Current Store : [0x800002f0] : sw tp, 116(ra) -- Store: [0x80003684]:0x00000080




Last Coverpoint : ['rs1 : x15', 'rs2 : x17', 'rd : x16', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x7a3631 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x05c9cc and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000304]:fsub.s a6, a5, a7, dyn
	-[0x80000308]:csrrs tp, fcsr, zero
	-[0x8000030c]:sw a6, 120(ra)
Current Store : [0x80000310] : sw tp, 124(ra) -- Store: [0x8000368c]:0x00000001




Last Coverpoint : ['rs1 : x17', 'rs2 : x16', 'rd : x15', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x7a3631 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x05c9cc and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000324]:fsub.s a5, a7, a6, dyn
	-[0x80000328]:csrrs tp, fcsr, zero
	-[0x8000032c]:sw a5, 128(ra)
Current Store : [0x80000330] : sw tp, 132(ra) -- Store: [0x80003694]:0x00000021




Last Coverpoint : ['rs1 : x13', 'rs2 : x12', 'rd : x14', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x7a3631 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x05c9cc and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000344]:fsub.s a4, a3, a2, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:sw a4, 136(ra)
Current Store : [0x80000350] : sw tp, 140(ra) -- Store: [0x8000369c]:0x00000041




Last Coverpoint : ['rs1 : x12', 'rs2 : x14', 'rd : x13', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x7a3631 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x05c9cc and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000364]:fsub.s a3, a2, a4, dyn
	-[0x80000368]:csrrs tp, fcsr, zero
	-[0x8000036c]:sw a3, 144(ra)
Current Store : [0x80000370] : sw tp, 148(ra) -- Store: [0x800036a4]:0x00000061




Last Coverpoint : ['rs1 : x14', 'rs2 : x13', 'rd : x12', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x7a3631 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x05c9cc and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000384]:fsub.s a2, a4, a3, dyn
	-[0x80000388]:csrrs tp, fcsr, zero
	-[0x8000038c]:sw a2, 152(ra)
Current Store : [0x80000390] : sw tp, 156(ra) -- Store: [0x800036ac]:0x00000081




Last Coverpoint : ['rs1 : x10', 'rs2 : x9', 'rd : x11', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x2dfa9c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003a4]:fsub.s a1, a0, s1, dyn
	-[0x800003a8]:csrrs tp, fcsr, zero
	-[0x800003ac]:sw a1, 160(ra)
Current Store : [0x800003b0] : sw tp, 164(ra) -- Store: [0x800036b4]:0x00000001




Last Coverpoint : ['rs1 : x9', 'rs2 : x11', 'rd : x10', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x2dfa9c and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003cc]:fsub.s a0, s1, a1, dyn
	-[0x800003d0]:csrrs a3, fcsr, zero
	-[0x800003d4]:sw a0, 168(ra)
Current Store : [0x800003d8] : sw a3, 172(ra) -- Store: [0x800036bc]:0x00000021




Last Coverpoint : ['rs1 : x11', 'rs2 : x10', 'rd : x9', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x2dfa9c and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003ec]:fsub.s s1, a1, a0, dyn
	-[0x800003f0]:csrrs a3, fcsr, zero
	-[0x800003f4]:sw s1, 176(ra)
Current Store : [0x800003f8] : sw a3, 180(ra) -- Store: [0x800036c4]:0x00000041




Last Coverpoint : ['rs1 : x7', 'rs2 : x6', 'rd : x8', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x2dfa9c and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000040c]:fsub.s fp, t2, t1, dyn
	-[0x80000410]:csrrs a3, fcsr, zero
	-[0x80000414]:sw fp, 184(ra)
Current Store : [0x80000418] : sw a3, 188(ra) -- Store: [0x800036cc]:0x00000061




Last Coverpoint : ['rs1 : x6', 'rs2 : x8', 'rd : x7', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x2dfa9c and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000434]:fsub.s t2, t1, fp, dyn
	-[0x80000438]:csrrs a3, fcsr, zero
	-[0x8000043c]:sw t2, 0(s1)
Current Store : [0x80000440] : sw a3, 4(s1) -- Store: [0x800036d4]:0x00000081




Last Coverpoint : ['rs1 : x8', 'rs2 : x7', 'rd : x6', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x207786 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x7c43c6 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000454]:fsub.s t1, fp, t2, dyn
	-[0x80000458]:csrrs a3, fcsr, zero
	-[0x8000045c]:sw t1, 8(s1)
Current Store : [0x80000460] : sw a3, 12(s1) -- Store: [0x800036dc]:0x00000001




Last Coverpoint : ['rs1 : x4', 'rs2 : x3', 'rd : x5', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x207786 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x7c43c6 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000474]:fsub.s t0, tp, gp, dyn
	-[0x80000478]:csrrs a3, fcsr, zero
	-[0x8000047c]:sw t0, 16(s1)
Current Store : [0x80000480] : sw a3, 20(s1) -- Store: [0x800036e4]:0x00000021




Last Coverpoint : ['rs1 : x3', 'rs2 : x5', 'rd : x4', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x207786 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x7c43c6 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000494]:fsub.s tp, gp, t0, dyn
	-[0x80000498]:csrrs a3, fcsr, zero
	-[0x8000049c]:sw tp, 24(s1)
Current Store : [0x800004a0] : sw a3, 28(s1) -- Store: [0x800036ec]:0x00000041




Last Coverpoint : ['rs1 : x5', 'rs2 : x4', 'rd : x3', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x207786 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x7c43c6 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004b4]:fsub.s gp, t0, tp, dyn
	-[0x800004b8]:csrrs a3, fcsr, zero
	-[0x800004bc]:sw gp, 32(s1)
Current Store : [0x800004c0] : sw a3, 36(s1) -- Store: [0x800036f4]:0x00000061




Last Coverpoint : ['rs1 : x1', 'rs2 : x0', 'rd : x2']
Last Code Sequence : 
	-[0x800004d4]:fsub.s sp, ra, zero, dyn
	-[0x800004d8]:csrrs a3, fcsr, zero
	-[0x800004dc]:sw sp, 40(s1)
Current Store : [0x800004e0] : sw a3, 44(s1) -- Store: [0x800036fc]:0x00000080




Last Coverpoint : ['rs1 : x0', 'rs2 : x2', 'rd : x1']
Last Code Sequence : 
	-[0x800004f4]:fsub.s ra, zero, sp, dyn
	-[0x800004f8]:csrrs a3, fcsr, zero
	-[0x800004fc]:sw ra, 48(s1)
Current Store : [0x80000500] : sw a3, 52(s1) -- Store: [0x80003704]:0x00000000




Last Coverpoint : ['rs1 : x2', 'rs2 : x1', 'rd : x0', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 1 and fe2 == 0xfc and fm2 == 0x3691a3 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000514]:fsub.s zero, sp, ra, dyn
	-[0x80000518]:csrrs a3, fcsr, zero
	-[0x8000051c]:sw zero, 56(s1)
Current Store : [0x80000520] : sw a3, 60(s1) -- Store: [0x8000370c]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 1 and fe2 == 0xfc and fm2 == 0x3691a3 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000534]:fsub.s t6, t5, t4, dyn
	-[0x80000538]:csrrs a3, fcsr, zero
	-[0x8000053c]:sw t6, 64(s1)
Current Store : [0x80000540] : sw a3, 68(s1) -- Store: [0x80003714]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 1 and fe2 == 0xfc and fm2 == 0x3691a3 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000554]:fsub.s t6, t5, t4, dyn
	-[0x80000558]:csrrs a3, fcsr, zero
	-[0x8000055c]:sw t6, 72(s1)
Current Store : [0x80000560] : sw a3, 76(s1) -- Store: [0x8000371c]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 1 and fe2 == 0xfc and fm2 == 0x3691a3 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000574]:fsub.s t6, t5, t4, dyn
	-[0x80000578]:csrrs a3, fcsr, zero
	-[0x8000057c]:sw t6, 80(s1)
Current Store : [0x80000580] : sw a3, 84(s1) -- Store: [0x80003724]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2b0757 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x54f8a7 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000594]:fsub.s t6, t5, t4, dyn
	-[0x80000598]:csrrs a3, fcsr, zero
	-[0x8000059c]:sw t6, 88(s1)
Current Store : [0x800005a0] : sw a3, 92(s1) -- Store: [0x8000372c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2b0757 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x54f8a7 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005b4]:fsub.s t6, t5, t4, dyn
	-[0x800005b8]:csrrs a3, fcsr, zero
	-[0x800005bc]:sw t6, 96(s1)
Current Store : [0x800005c0] : sw a3, 100(s1) -- Store: [0x80003734]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2b0757 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x54f8a7 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005d4]:fsub.s t6, t5, t4, dyn
	-[0x800005d8]:csrrs a3, fcsr, zero
	-[0x800005dc]:sw t6, 104(s1)
Current Store : [0x800005e0] : sw a3, 108(s1) -- Store: [0x8000373c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2b0757 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x54f8a7 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005f4]:fsub.s t6, t5, t4, dyn
	-[0x800005f8]:csrrs a3, fcsr, zero
	-[0x800005fc]:sw t6, 112(s1)
Current Store : [0x80000600] : sw a3, 116(s1) -- Store: [0x80003744]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2b0757 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x54f8a7 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000614]:fsub.s t6, t5, t4, dyn
	-[0x80000618]:csrrs a3, fcsr, zero
	-[0x8000061c]:sw t6, 120(s1)
Current Store : [0x80000620] : sw a3, 124(s1) -- Store: [0x8000374c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x1413e0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000634]:fsub.s t6, t5, t4, dyn
	-[0x80000638]:csrrs a3, fcsr, zero
	-[0x8000063c]:sw t6, 128(s1)
Current Store : [0x80000640] : sw a3, 132(s1) -- Store: [0x80003754]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x1413e0 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000654]:fsub.s t6, t5, t4, dyn
	-[0x80000658]:csrrs a3, fcsr, zero
	-[0x8000065c]:sw t6, 136(s1)
Current Store : [0x80000660] : sw a3, 140(s1) -- Store: [0x8000375c]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x1413e0 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000674]:fsub.s t6, t5, t4, dyn
	-[0x80000678]:csrrs a3, fcsr, zero
	-[0x8000067c]:sw t6, 144(s1)
Current Store : [0x80000680] : sw a3, 148(s1) -- Store: [0x80003764]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x1413e0 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000694]:fsub.s t6, t5, t4, dyn
	-[0x80000698]:csrrs a3, fcsr, zero
	-[0x8000069c]:sw t6, 152(s1)
Current Store : [0x800006a0] : sw a3, 156(s1) -- Store: [0x8000376c]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x1413e0 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006b4]:fsub.s t6, t5, t4, dyn
	-[0x800006b8]:csrrs a3, fcsr, zero
	-[0x800006bc]:sw t6, 160(s1)
Current Store : [0x800006c0] : sw a3, 164(s1) -- Store: [0x80003774]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2d12f5 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x176848 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006d4]:fsub.s t6, t5, t4, dyn
	-[0x800006d8]:csrrs a3, fcsr, zero
	-[0x800006dc]:sw t6, 168(s1)
Current Store : [0x800006e0] : sw a3, 172(s1) -- Store: [0x8000377c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2d12f5 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x176848 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006f4]:fsub.s t6, t5, t4, dyn
	-[0x800006f8]:csrrs a3, fcsr, zero
	-[0x800006fc]:sw t6, 176(s1)
Current Store : [0x80000700] : sw a3, 180(s1) -- Store: [0x80003784]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2d12f5 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x176848 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000714]:fsub.s t6, t5, t4, dyn
	-[0x80000718]:csrrs a3, fcsr, zero
	-[0x8000071c]:sw t6, 184(s1)
Current Store : [0x80000720] : sw a3, 188(s1) -- Store: [0x8000378c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2d12f5 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x176848 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000734]:fsub.s t6, t5, t4, dyn
	-[0x80000738]:csrrs a3, fcsr, zero
	-[0x8000073c]:sw t6, 192(s1)
Current Store : [0x80000740] : sw a3, 196(s1) -- Store: [0x80003794]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2d12f5 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x176848 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000754]:fsub.s t6, t5, t4, dyn
	-[0x80000758]:csrrs a3, fcsr, zero
	-[0x8000075c]:sw t6, 200(s1)
Current Store : [0x80000760] : sw a3, 204(s1) -- Store: [0x8000379c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x1c56e0 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3b1d48 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000774]:fsub.s t6, t5, t4, dyn
	-[0x80000778]:csrrs a3, fcsr, zero
	-[0x8000077c]:sw t6, 208(s1)
Current Store : [0x80000780] : sw a3, 212(s1) -- Store: [0x800037a4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x1c56e0 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3b1d48 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000794]:fsub.s t6, t5, t4, dyn
	-[0x80000798]:csrrs a3, fcsr, zero
	-[0x8000079c]:sw t6, 216(s1)
Current Store : [0x800007a0] : sw a3, 220(s1) -- Store: [0x800037ac]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x1c56e0 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3b1d48 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007b4]:fsub.s t6, t5, t4, dyn
	-[0x800007b8]:csrrs a3, fcsr, zero
	-[0x800007bc]:sw t6, 224(s1)
Current Store : [0x800007c0] : sw a3, 228(s1) -- Store: [0x800037b4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x1c56e0 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3b1d48 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007d4]:fsub.s t6, t5, t4, dyn
	-[0x800007d8]:csrrs a3, fcsr, zero
	-[0x800007dc]:sw t6, 232(s1)
Current Store : [0x800007e0] : sw a3, 236(s1) -- Store: [0x800037bc]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x1c56e0 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3b1d48 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007f4]:fsub.s t6, t5, t4, dyn
	-[0x800007f8]:csrrs a3, fcsr, zero
	-[0x800007fc]:sw t6, 240(s1)
Current Store : [0x80000800] : sw a3, 244(s1) -- Store: [0x800037c4]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x34510e and fs2 == 1 and fe2 == 0xfa and fm2 == 0x3aef03 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000814]:fsub.s t6, t5, t4, dyn
	-[0x80000818]:csrrs a3, fcsr, zero
	-[0x8000081c]:sw t6, 248(s1)
Current Store : [0x80000820] : sw a3, 252(s1) -- Store: [0x800037cc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x34510e and fs2 == 1 and fe2 == 0xfa and fm2 == 0x3aef03 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000834]:fsub.s t6, t5, t4, dyn
	-[0x80000838]:csrrs a3, fcsr, zero
	-[0x8000083c]:sw t6, 256(s1)
Current Store : [0x80000840] : sw a3, 260(s1) -- Store: [0x800037d4]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x34510e and fs2 == 1 and fe2 == 0xfa and fm2 == 0x3aef03 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000854]:fsub.s t6, t5, t4, dyn
	-[0x80000858]:csrrs a3, fcsr, zero
	-[0x8000085c]:sw t6, 264(s1)
Current Store : [0x80000860] : sw a3, 268(s1) -- Store: [0x800037dc]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x34510e and fs2 == 1 and fe2 == 0xfa and fm2 == 0x3aef03 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000874]:fsub.s t6, t5, t4, dyn
	-[0x80000878]:csrrs a3, fcsr, zero
	-[0x8000087c]:sw t6, 272(s1)
Current Store : [0x80000880] : sw a3, 276(s1) -- Store: [0x800037e4]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x34510e and fs2 == 1 and fe2 == 0xfa and fm2 == 0x3aef03 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000894]:fsub.s t6, t5, t4, dyn
	-[0x80000898]:csrrs a3, fcsr, zero
	-[0x8000089c]:sw t6, 280(s1)
Current Store : [0x800008a0] : sw a3, 284(s1) -- Store: [0x800037ec]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x26592c and fs2 == 1 and fe2 == 0xfe and fm2 == 0x1669b4 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008b4]:fsub.s t6, t5, t4, dyn
	-[0x800008b8]:csrrs a3, fcsr, zero
	-[0x800008bc]:sw t6, 288(s1)
Current Store : [0x800008c0] : sw a3, 292(s1) -- Store: [0x800037f4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x26592c and fs2 == 1 and fe2 == 0xfe and fm2 == 0x1669b4 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008d4]:fsub.s t6, t5, t4, dyn
	-[0x800008d8]:csrrs a3, fcsr, zero
	-[0x800008dc]:sw t6, 296(s1)
Current Store : [0x800008e0] : sw a3, 300(s1) -- Store: [0x800037fc]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x26592c and fs2 == 1 and fe2 == 0xfe and fm2 == 0x1669b4 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008f4]:fsub.s t6, t5, t4, dyn
	-[0x800008f8]:csrrs a3, fcsr, zero
	-[0x800008fc]:sw t6, 304(s1)
Current Store : [0x80000900] : sw a3, 308(s1) -- Store: [0x80003804]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x26592c and fs2 == 1 and fe2 == 0xfe and fm2 == 0x1669b4 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000914]:fsub.s t6, t5, t4, dyn
	-[0x80000918]:csrrs a3, fcsr, zero
	-[0x8000091c]:sw t6, 312(s1)
Current Store : [0x80000920] : sw a3, 316(s1) -- Store: [0x8000380c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x26592c and fs2 == 1 and fe2 == 0xfe and fm2 == 0x1669b4 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000934]:fsub.s t6, t5, t4, dyn
	-[0x80000938]:csrrs a3, fcsr, zero
	-[0x8000093c]:sw t6, 320(s1)
Current Store : [0x80000940] : sw a3, 324(s1) -- Store: [0x80003814]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x326d35 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a6c95 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000954]:fsub.s t6, t5, t4, dyn
	-[0x80000958]:csrrs a3, fcsr, zero
	-[0x8000095c]:sw t6, 328(s1)
Current Store : [0x80000960] : sw a3, 332(s1) -- Store: [0x8000381c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x326d35 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a6c95 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000974]:fsub.s t6, t5, t4, dyn
	-[0x80000978]:csrrs a3, fcsr, zero
	-[0x8000097c]:sw t6, 336(s1)
Current Store : [0x80000980] : sw a3, 340(s1) -- Store: [0x80003824]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x326d35 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a6c95 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000994]:fsub.s t6, t5, t4, dyn
	-[0x80000998]:csrrs a3, fcsr, zero
	-[0x8000099c]:sw t6, 344(s1)
Current Store : [0x800009a0] : sw a3, 348(s1) -- Store: [0x8000382c]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x326d35 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a6c95 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009b4]:fsub.s t6, t5, t4, dyn
	-[0x800009b8]:csrrs a3, fcsr, zero
	-[0x800009bc]:sw t6, 352(s1)
Current Store : [0x800009c0] : sw a3, 356(s1) -- Store: [0x80003834]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x326d35 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a6c95 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009d4]:fsub.s t6, t5, t4, dyn
	-[0x800009d8]:csrrs a3, fcsr, zero
	-[0x800009dc]:sw t6, 360(s1)
Current Store : [0x800009e0] : sw a3, 364(s1) -- Store: [0x8000383c]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x282619 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x704c33 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009f4]:fsub.s t6, t5, t4, dyn
	-[0x800009f8]:csrrs a3, fcsr, zero
	-[0x800009fc]:sw t6, 368(s1)
Current Store : [0x80000a00] : sw a3, 372(s1) -- Store: [0x80003844]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x282619 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x704c33 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a14]:fsub.s t6, t5, t4, dyn
	-[0x80000a18]:csrrs a3, fcsr, zero
	-[0x80000a1c]:sw t6, 376(s1)
Current Store : [0x80000a20] : sw a3, 380(s1) -- Store: [0x8000384c]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x282619 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x704c33 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a34]:fsub.s t6, t5, t4, dyn
	-[0x80000a38]:csrrs a3, fcsr, zero
	-[0x80000a3c]:sw t6, 384(s1)
Current Store : [0x80000a40] : sw a3, 388(s1) -- Store: [0x80003854]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x282619 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x704c33 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a54]:fsub.s t6, t5, t4, dyn
	-[0x80000a58]:csrrs a3, fcsr, zero
	-[0x80000a5c]:sw t6, 392(s1)
Current Store : [0x80000a60] : sw a3, 396(s1) -- Store: [0x8000385c]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x282619 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x704c33 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a74]:fsub.s t6, t5, t4, dyn
	-[0x80000a78]:csrrs a3, fcsr, zero
	-[0x80000a7c]:sw t6, 400(s1)
Current Store : [0x80000a80] : sw a3, 404(s1) -- Store: [0x80003864]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x330244 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x098121 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a94]:fsub.s t6, t5, t4, dyn
	-[0x80000a98]:csrrs a3, fcsr, zero
	-[0x80000a9c]:sw t6, 408(s1)
Current Store : [0x80000aa0] : sw a3, 412(s1) -- Store: [0x8000386c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x330244 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x098121 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ab4]:fsub.s t6, t5, t4, dyn
	-[0x80000ab8]:csrrs a3, fcsr, zero
	-[0x80000abc]:sw t6, 416(s1)
Current Store : [0x80000ac0] : sw a3, 420(s1) -- Store: [0x80003874]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x330244 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x098121 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ad4]:fsub.s t6, t5, t4, dyn
	-[0x80000ad8]:csrrs a3, fcsr, zero
	-[0x80000adc]:sw t6, 424(s1)
Current Store : [0x80000ae0] : sw a3, 428(s1) -- Store: [0x8000387c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x330244 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x098121 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000af4]:fsub.s t6, t5, t4, dyn
	-[0x80000af8]:csrrs a3, fcsr, zero
	-[0x80000afc]:sw t6, 432(s1)
Current Store : [0x80000b00] : sw a3, 436(s1) -- Store: [0x80003884]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x330244 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x098121 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b14]:fsub.s t6, t5, t4, dyn
	-[0x80000b18]:csrrs a3, fcsr, zero
	-[0x80000b1c]:sw t6, 440(s1)
Current Store : [0x80000b20] : sw a3, 444(s1) -- Store: [0x8000388c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x280619 and fs2 == 1 and fe2 == 0xf9 and fm2 == 0x3fcf2e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b34]:fsub.s t6, t5, t4, dyn
	-[0x80000b38]:csrrs a3, fcsr, zero
	-[0x80000b3c]:sw t6, 448(s1)
Current Store : [0x80000b40] : sw a3, 452(s1) -- Store: [0x80003894]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x280619 and fs2 == 1 and fe2 == 0xf9 and fm2 == 0x3fcf2e and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b54]:fsub.s t6, t5, t4, dyn
	-[0x80000b58]:csrrs a3, fcsr, zero
	-[0x80000b5c]:sw t6, 456(s1)
Current Store : [0x80000b60] : sw a3, 460(s1) -- Store: [0x8000389c]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x280619 and fs2 == 1 and fe2 == 0xf9 and fm2 == 0x3fcf2e and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b74]:fsub.s t6, t5, t4, dyn
	-[0x80000b78]:csrrs a3, fcsr, zero
	-[0x80000b7c]:sw t6, 464(s1)
Current Store : [0x80000b80] : sw a3, 468(s1) -- Store: [0x800038a4]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x280619 and fs2 == 1 and fe2 == 0xf9 and fm2 == 0x3fcf2e and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b94]:fsub.s t6, t5, t4, dyn
	-[0x80000b98]:csrrs a3, fcsr, zero
	-[0x80000b9c]:sw t6, 472(s1)
Current Store : [0x80000ba0] : sw a3, 476(s1) -- Store: [0x800038ac]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x280619 and fs2 == 1 and fe2 == 0xf9 and fm2 == 0x3fcf2e and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bb4]:fsub.s t6, t5, t4, dyn
	-[0x80000bb8]:csrrs a3, fcsr, zero
	-[0x80000bbc]:sw t6, 480(s1)
Current Store : [0x80000bc0] : sw a3, 484(s1) -- Store: [0x800038b4]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x5e8d61 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x4f46b0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bd4]:fsub.s t6, t5, t4, dyn
	-[0x80000bd8]:csrrs a3, fcsr, zero
	-[0x80000bdc]:sw t6, 488(s1)
Current Store : [0x80000be0] : sw a3, 492(s1) -- Store: [0x800038bc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x5e8d61 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x4f46b0 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bf4]:fsub.s t6, t5, t4, dyn
	-[0x80000bf8]:csrrs a3, fcsr, zero
	-[0x80000bfc]:sw t6, 496(s1)
Current Store : [0x80000c00] : sw a3, 500(s1) -- Store: [0x800038c4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x5e8d61 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x4f46b0 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c14]:fsub.s t6, t5, t4, dyn
	-[0x80000c18]:csrrs a3, fcsr, zero
	-[0x80000c1c]:sw t6, 504(s1)
Current Store : [0x80000c20] : sw a3, 508(s1) -- Store: [0x800038cc]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x5e8d61 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x4f46b0 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c34]:fsub.s t6, t5, t4, dyn
	-[0x80000c38]:csrrs a3, fcsr, zero
	-[0x80000c3c]:sw t6, 512(s1)
Current Store : [0x80000c40] : sw a3, 516(s1) -- Store: [0x800038d4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x5e8d61 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x4f46b0 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c54]:fsub.s t6, t5, t4, dyn
	-[0x80000c58]:csrrs a3, fcsr, zero
	-[0x80000c5c]:sw t6, 520(s1)
Current Store : [0x80000c60] : sw a3, 524(s1) -- Store: [0x800038dc]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x20d0a1 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x01a143 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c74]:fsub.s t6, t5, t4, dyn
	-[0x80000c78]:csrrs a3, fcsr, zero
	-[0x80000c7c]:sw t6, 528(s1)
Current Store : [0x80000c80] : sw a3, 532(s1) -- Store: [0x800038e4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x20d0a1 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x01a143 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c94]:fsub.s t6, t5, t4, dyn
	-[0x80000c98]:csrrs a3, fcsr, zero
	-[0x80000c9c]:sw t6, 536(s1)
Current Store : [0x80000ca0] : sw a3, 540(s1) -- Store: [0x800038ec]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x20d0a1 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x01a143 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cb4]:fsub.s t6, t5, t4, dyn
	-[0x80000cb8]:csrrs a3, fcsr, zero
	-[0x80000cbc]:sw t6, 544(s1)
Current Store : [0x80000cc0] : sw a3, 548(s1) -- Store: [0x800038f4]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x20d0a1 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x01a143 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cd4]:fsub.s t6, t5, t4, dyn
	-[0x80000cd8]:csrrs a3, fcsr, zero
	-[0x80000cdc]:sw t6, 552(s1)
Current Store : [0x80000ce0] : sw a3, 556(s1) -- Store: [0x800038fc]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x20d0a1 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x01a143 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cf4]:fsub.s t6, t5, t4, dyn
	-[0x80000cf8]:csrrs a3, fcsr, zero
	-[0x80000cfc]:sw t6, 560(s1)
Current Store : [0x80000d00] : sw a3, 564(s1) -- Store: [0x80003904]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x76a41a and fs2 == 0 and fe2 == 0xfe and fm2 == 0x5b520c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d14]:fsub.s t6, t5, t4, dyn
	-[0x80000d18]:csrrs a3, fcsr, zero
	-[0x80000d1c]:sw t6, 568(s1)
Current Store : [0x80000d20] : sw a3, 572(s1) -- Store: [0x8000390c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x76a41a and fs2 == 0 and fe2 == 0xfe and fm2 == 0x5b520c and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d34]:fsub.s t6, t5, t4, dyn
	-[0x80000d38]:csrrs a3, fcsr, zero
	-[0x80000d3c]:sw t6, 576(s1)
Current Store : [0x80000d40] : sw a3, 580(s1) -- Store: [0x80003914]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x76a41a and fs2 == 0 and fe2 == 0xfe and fm2 == 0x5b520c and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d54]:fsub.s t6, t5, t4, dyn
	-[0x80000d58]:csrrs a3, fcsr, zero
	-[0x80000d5c]:sw t6, 584(s1)
Current Store : [0x80000d60] : sw a3, 588(s1) -- Store: [0x8000391c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x76a41a and fs2 == 0 and fe2 == 0xfe and fm2 == 0x5b520c and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d74]:fsub.s t6, t5, t4, dyn
	-[0x80000d78]:csrrs a3, fcsr, zero
	-[0x80000d7c]:sw t6, 592(s1)
Current Store : [0x80000d80] : sw a3, 596(s1) -- Store: [0x80003924]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x76a41a and fs2 == 0 and fe2 == 0xfe and fm2 == 0x5b520c and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d94]:fsub.s t6, t5, t4, dyn
	-[0x80000d98]:csrrs a3, fcsr, zero
	-[0x80000d9c]:sw t6, 600(s1)
Current Store : [0x80000da0] : sw a3, 604(s1) -- Store: [0x8000392c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3e917d and fs2 == 1 and fe2 == 0xf7 and fm2 == 0x3740ca and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000db4]:fsub.s t6, t5, t4, dyn
	-[0x80000db8]:csrrs a3, fcsr, zero
	-[0x80000dbc]:sw t6, 608(s1)
Current Store : [0x80000dc0] : sw a3, 612(s1) -- Store: [0x80003934]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3e917d and fs2 == 1 and fe2 == 0xf7 and fm2 == 0x3740ca and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000dd4]:fsub.s t6, t5, t4, dyn
	-[0x80000dd8]:csrrs a3, fcsr, zero
	-[0x80000ddc]:sw t6, 616(s1)
Current Store : [0x80000de0] : sw a3, 620(s1) -- Store: [0x8000393c]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3e917d and fs2 == 1 and fe2 == 0xf7 and fm2 == 0x3740ca and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000df4]:fsub.s t6, t5, t4, dyn
	-[0x80000df8]:csrrs a3, fcsr, zero
	-[0x80000dfc]:sw t6, 624(s1)
Current Store : [0x80000e00] : sw a3, 628(s1) -- Store: [0x80003944]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3e917d and fs2 == 1 and fe2 == 0xf7 and fm2 == 0x3740ca and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e14]:fsub.s t6, t5, t4, dyn
	-[0x80000e18]:csrrs a3, fcsr, zero
	-[0x80000e1c]:sw t6, 632(s1)
Current Store : [0x80000e20] : sw a3, 636(s1) -- Store: [0x8000394c]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3e917d and fs2 == 1 and fe2 == 0xf7 and fm2 == 0x3740ca and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e34]:fsub.s t6, t5, t4, dyn
	-[0x80000e38]:csrrs a3, fcsr, zero
	-[0x80000e3c]:sw t6, 640(s1)
Current Store : [0x80000e40] : sw a3, 644(s1) -- Store: [0x80003954]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x39afdd and fs2 == 0 and fe2 == 0xfe and fm2 == 0x5735fa and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e54]:fsub.s t6, t5, t4, dyn
	-[0x80000e58]:csrrs a3, fcsr, zero
	-[0x80000e5c]:sw t6, 648(s1)
Current Store : [0x80000e60] : sw a3, 652(s1) -- Store: [0x8000395c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x39afdd and fs2 == 0 and fe2 == 0xfe and fm2 == 0x5735fa and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e74]:fsub.s t6, t5, t4, dyn
	-[0x80000e78]:csrrs a3, fcsr, zero
	-[0x80000e7c]:sw t6, 656(s1)
Current Store : [0x80000e80] : sw a3, 660(s1) -- Store: [0x80003964]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x39afdd and fs2 == 0 and fe2 == 0xfe and fm2 == 0x5735fa and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e94]:fsub.s t6, t5, t4, dyn
	-[0x80000e98]:csrrs a3, fcsr, zero
	-[0x80000e9c]:sw t6, 664(s1)
Current Store : [0x80000ea0] : sw a3, 668(s1) -- Store: [0x8000396c]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x39afdd and fs2 == 0 and fe2 == 0xfe and fm2 == 0x5735fa and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000eb4]:fsub.s t6, t5, t4, dyn
	-[0x80000eb8]:csrrs a3, fcsr, zero
	-[0x80000ebc]:sw t6, 672(s1)
Current Store : [0x80000ec0] : sw a3, 676(s1) -- Store: [0x80003974]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x39afdd and fs2 == 0 and fe2 == 0xfe and fm2 == 0x5735fa and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ed4]:fsub.s t6, t5, t4, dyn
	-[0x80000ed8]:csrrs a3, fcsr, zero
	-[0x80000edc]:sw t6, 680(s1)
Current Store : [0x80000ee0] : sw a3, 684(s1) -- Store: [0x8000397c]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a8666 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x757998 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ef4]:fsub.s t6, t5, t4, dyn
	-[0x80000ef8]:csrrs a3, fcsr, zero
	-[0x80000efc]:sw t6, 688(s1)
Current Store : [0x80000f00] : sw a3, 692(s1) -- Store: [0x80003984]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a8666 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x757998 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f14]:fsub.s t6, t5, t4, dyn
	-[0x80000f18]:csrrs a3, fcsr, zero
	-[0x80000f1c]:sw t6, 696(s1)
Current Store : [0x80000f20] : sw a3, 700(s1) -- Store: [0x8000398c]:0x00000025




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a8666 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x757998 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f34]:fsub.s t6, t5, t4, dyn
	-[0x80000f38]:csrrs a3, fcsr, zero
	-[0x80000f3c]:sw t6, 704(s1)
Current Store : [0x80000f40] : sw a3, 708(s1) -- Store: [0x80003994]:0x00000045




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a8666 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x757998 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f54]:fsub.s t6, t5, t4, dyn
	-[0x80000f58]:csrrs a3, fcsr, zero
	-[0x80000f5c]:sw t6, 712(s1)
Current Store : [0x80000f60] : sw a3, 716(s1) -- Store: [0x8000399c]:0x00000065




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a8666 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x757998 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f74]:fsub.s t6, t5, t4, dyn
	-[0x80000f78]:csrrs a3, fcsr, zero
	-[0x80000f7c]:sw t6, 720(s1)
Current Store : [0x80000f80] : sw a3, 724(s1) -- Store: [0x800039a4]:0x00000085




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x33cbed and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f94]:fsub.s t6, t5, t4, dyn
	-[0x80000f98]:csrrs a3, fcsr, zero
	-[0x80000f9c]:sw t6, 728(s1)
Current Store : [0x80000fa0] : sw a3, 732(s1) -- Store: [0x800039ac]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x33cbed and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fb4]:fsub.s t6, t5, t4, dyn
	-[0x80000fb8]:csrrs a3, fcsr, zero
	-[0x80000fbc]:sw t6, 736(s1)
Current Store : [0x80000fc0] : sw a3, 740(s1) -- Store: [0x800039b4]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x33cbed and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fd4]:fsub.s t6, t5, t4, dyn
	-[0x80000fd8]:csrrs a3, fcsr, zero
	-[0x80000fdc]:sw t6, 744(s1)
Current Store : [0x80000fe0] : sw a3, 748(s1) -- Store: [0x800039bc]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x33cbed and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ff4]:fsub.s t6, t5, t4, dyn
	-[0x80000ff8]:csrrs a3, fcsr, zero
	-[0x80000ffc]:sw t6, 752(s1)
Current Store : [0x80001000] : sw a3, 756(s1) -- Store: [0x800039c4]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x33cbed and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001014]:fsub.s t6, t5, t4, dyn
	-[0x80001018]:csrrs a3, fcsr, zero
	-[0x8000101c]:sw t6, 760(s1)
Current Store : [0x80001020] : sw a3, 764(s1) -- Store: [0x800039cc]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1f5de9 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001034]:fsub.s t6, t5, t4, dyn
	-[0x80001038]:csrrs a3, fcsr, zero
	-[0x8000103c]:sw t6, 768(s1)
Current Store : [0x80001040] : sw a3, 772(s1) -- Store: [0x800039d4]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1f5de9 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001054]:fsub.s t6, t5, t4, dyn
	-[0x80001058]:csrrs a3, fcsr, zero
	-[0x8000105c]:sw t6, 776(s1)
Current Store : [0x80001060] : sw a3, 780(s1) -- Store: [0x800039dc]:0x00000025




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1f5de9 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001074]:fsub.s t6, t5, t4, dyn
	-[0x80001078]:csrrs a3, fcsr, zero
	-[0x8000107c]:sw t6, 784(s1)
Current Store : [0x80001080] : sw a3, 788(s1) -- Store: [0x800039e4]:0x00000045




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1f5de9 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001094]:fsub.s t6, t5, t4, dyn
	-[0x80001098]:csrrs a3, fcsr, zero
	-[0x8000109c]:sw t6, 792(s1)
Current Store : [0x800010a0] : sw a3, 796(s1) -- Store: [0x800039ec]:0x00000065




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1f5de9 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010b4]:fsub.s t6, t5, t4, dyn
	-[0x800010b8]:csrrs a3, fcsr, zero
	-[0x800010bc]:sw t6, 800(s1)
Current Store : [0x800010c0] : sw a3, 804(s1) -- Store: [0x800039f4]:0x00000085




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x68fcac and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010d4]:fsub.s t6, t5, t4, dyn
	-[0x800010d8]:csrrs a3, fcsr, zero
	-[0x800010dc]:sw t6, 808(s1)
Current Store : [0x800010e0] : sw a3, 812(s1) -- Store: [0x800039fc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x68fcac and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010f4]:fsub.s t6, t5, t4, dyn
	-[0x800010f8]:csrrs a3, fcsr, zero
	-[0x800010fc]:sw t6, 816(s1)
Current Store : [0x80001100] : sw a3, 820(s1) -- Store: [0x80003a04]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x68fcac and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001114]:fsub.s t6, t5, t4, dyn
	-[0x80001118]:csrrs a3, fcsr, zero
	-[0x8000111c]:sw t6, 824(s1)
Current Store : [0x80001120] : sw a3, 828(s1) -- Store: [0x80003a0c]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x68fcac and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001134]:fsub.s t6, t5, t4, dyn
	-[0x80001138]:csrrs a3, fcsr, zero
	-[0x8000113c]:sw t6, 832(s1)
Current Store : [0x80001140] : sw a3, 836(s1) -- Store: [0x80003a14]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x68fcac and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001154]:fsub.s t6, t5, t4, dyn
	-[0x80001158]:csrrs a3, fcsr, zero
	-[0x8000115c]:sw t6, 840(s1)
Current Store : [0x80001160] : sw a3, 844(s1) -- Store: [0x80003a1c]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x296f9b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001174]:fsub.s t6, t5, t4, dyn
	-[0x80001178]:csrrs a3, fcsr, zero
	-[0x8000117c]:sw t6, 848(s1)
Current Store : [0x80001180] : sw a3, 852(s1) -- Store: [0x80003a24]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x296f9b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001194]:fsub.s t6, t5, t4, dyn
	-[0x80001198]:csrrs a3, fcsr, zero
	-[0x8000119c]:sw t6, 856(s1)
Current Store : [0x800011a0] : sw a3, 860(s1) -- Store: [0x80003a2c]:0x00000025




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x296f9b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011b4]:fsub.s t6, t5, t4, dyn
	-[0x800011b8]:csrrs a3, fcsr, zero
	-[0x800011bc]:sw t6, 864(s1)
Current Store : [0x800011c0] : sw a3, 868(s1) -- Store: [0x80003a34]:0x00000045




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x296f9b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011d4]:fsub.s t6, t5, t4, dyn
	-[0x800011d8]:csrrs a3, fcsr, zero
	-[0x800011dc]:sw t6, 872(s1)
Current Store : [0x800011e0] : sw a3, 876(s1) -- Store: [0x80003a3c]:0x00000065




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x296f9b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011f4]:fsub.s t6, t5, t4, dyn
	-[0x800011f8]:csrrs a3, fcsr, zero
	-[0x800011fc]:sw t6, 880(s1)
Current Store : [0x80001200] : sw a3, 884(s1) -- Store: [0x80003a44]:0x00000085




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x42076b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001214]:fsub.s t6, t5, t4, dyn
	-[0x80001218]:csrrs a3, fcsr, zero
	-[0x8000121c]:sw t6, 888(s1)
Current Store : [0x80001220] : sw a3, 892(s1) -- Store: [0x80003a4c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x42076b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001234]:fsub.s t6, t5, t4, dyn
	-[0x80001238]:csrrs a3, fcsr, zero
	-[0x8000123c]:sw t6, 896(s1)
Current Store : [0x80001240] : sw a3, 900(s1) -- Store: [0x80003a54]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x42076b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001254]:fsub.s t6, t5, t4, dyn
	-[0x80001258]:csrrs a3, fcsr, zero
	-[0x8000125c]:sw t6, 904(s1)
Current Store : [0x80001260] : sw a3, 908(s1) -- Store: [0x80003a5c]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x42076b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001274]:fsub.s t6, t5, t4, dyn
	-[0x80001278]:csrrs a3, fcsr, zero
	-[0x8000127c]:sw t6, 912(s1)
Current Store : [0x80001280] : sw a3, 916(s1) -- Store: [0x80003a64]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x42076b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001294]:fsub.s t6, t5, t4, dyn
	-[0x80001298]:csrrs a3, fcsr, zero
	-[0x8000129c]:sw t6, 920(s1)
Current Store : [0x800012a0] : sw a3, 924(s1) -- Store: [0x80003a6c]:0x00000081




Last Coverpoint : ['mnemonic : fsub.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x2633ba and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012b4]:fsub.s t6, t5, t4, dyn
	-[0x800012b8]:csrrs a3, fcsr, zero
	-[0x800012bc]:sw t6, 928(s1)
Current Store : [0x800012c0] : sw a3, 932(s1) -- Store: [0x80003a74]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x2633ba and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012d4]:fsub.s t6, t5, t4, dyn
	-[0x800012d8]:csrrs a3, fcsr, zero
	-[0x800012dc]:sw t6, 936(s1)
Current Store : [0x800012e0] : sw a3, 940(s1) -- Store: [0x80003a7c]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x2633ba and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012f4]:fsub.s t6, t5, t4, dyn
	-[0x800012f8]:csrrs a3, fcsr, zero
	-[0x800012fc]:sw t6, 944(s1)
Current Store : [0x80001300] : sw a3, 948(s1) -- Store: [0x80003a84]:0x00000041




Last Coverpoint : ['mnemonic : fsub.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x2633ba and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001314]:fsub.s t6, t5, t4, dyn
	-[0x80001318]:csrrs a3, fcsr, zero
	-[0x8000131c]:sw t6, 952(s1)
Current Store : [0x80001320] : sw a3, 956(s1) -- Store: [0x80003a8c]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x207786 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x7c43c6 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001334]:fsub.s t6, t5, t4, dyn
	-[0x80001338]:csrrs a3, fcsr, zero
	-[0x8000133c]:sw t6, 960(s1)
Current Store : [0x80001340] : sw a3, 964(s1) -- Store: [0x80003a94]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 1 and fe2 == 0xfc and fm2 == 0x3691a3 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001354]:fsub.s t6, t5, t4, dyn
	-[0x80001358]:csrrs a3, fcsr, zero
	-[0x8000135c]:sw t6, 968(s1)
Current Store : [0x80001360] : sw a3, 972(s1) -- Store: [0x80003a9c]:0x00000001




Last Coverpoint : ['mnemonic : fsub.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 1 and fe2 == 0xfc and fm2 == 0x3691a3 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001374]:fsub.s t6, t5, t4, dyn
	-[0x80001378]:csrrs a3, fcsr, zero
	-[0x8000137c]:sw t6, 976(s1)
Current Store : [0x80001380] : sw a3, 980(s1) -- Store: [0x80003aa4]:0x00000021





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                                coverpoints                                                                                                                 |                                                      code                                                       |
|---:|--------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|
|   1|[0x80003610]<br>0x7F3FFFFF|- mnemonic : fsub.s<br> - rs1 : x31<br> - rs2 : x30<br> - rd : x31<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x2633ba and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x80000124]:fsub.s t6, t6, t5, dyn<br> [0x80000128]:csrrs tp, fcsr, zero<br> [0x8000012c]:sw t6, 0(ra)<br>      |
|   2|[0x80003618]<br>0x00000000|- rs1 : x29<br> - rs2 : x29<br> - rd : x29<br> - rs1 == rs2 == rd<br>                                                                                                                                                                       |[0x80000144]:fsub.s t4, t4, t4, dyn<br> [0x80000148]:csrrs tp, fcsr, zero<br> [0x8000014c]:sw t4, 8(ra)<br>      |
|   3|[0x80003620]<br>0x80000000|- rs1 : x28<br> - rs2 : x28<br> - rd : x30<br> - rs1 == rs2 != rd<br>                                                                                                                                                                       |[0x80000164]:fsub.s t5, t3, t3, dyn<br> [0x80000168]:csrrs tp, fcsr, zero<br> [0x8000016c]:sw t5, 16(ra)<br>     |
|   4|[0x80003628]<br>0x7F400000|- rs1 : x30<br> - rs2 : x27<br> - rd : x27<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x2633ba and  fcsr == 0x60 and rm_val == 7   #nosat<br>                        |[0x80000184]:fsub.s s11, t5, s11, dyn<br> [0x80000188]:csrrs tp, fcsr, zero<br> [0x8000018c]:sw s11, 24(ra)<br>  |
|   5|[0x80003630]<br>0x7F3FFFFF|- rs1 : x27<br> - rs2 : x31<br> - rd : x28<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x2633ba and  fcsr == 0x80 and rm_val == 7   #nosat<br> |[0x800001a4]:fsub.s t3, s11, t6, dyn<br> [0x800001a8]:csrrs tp, fcsr, zero<br> [0x800001ac]:sw t3, 32(ra)<br>    |
|   6|[0x80003638]<br>0x7F3FFFFE|- rs1 : x25<br> - rs2 : x24<br> - rd : x26<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x12a50c and fs2 == 1 and fe2 == 0xfc and fm2 == 0x356bca and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x800001c4]:fsub.s s10, s9, s8, dyn<br> [0x800001c8]:csrrs tp, fcsr, zero<br> [0x800001cc]:sw s10, 40(ra)<br>   |
|   7|[0x80003640]<br>0x7F3FFFFE|- rs1 : x24<br> - rs2 : x26<br> - rd : x25<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x12a50c and fs2 == 1 and fe2 == 0xfc and fm2 == 0x356bca and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                               |[0x800001e4]:fsub.s s9, s8, s10, dyn<br> [0x800001e8]:csrrs tp, fcsr, zero<br> [0x800001ec]:sw s9, 48(ra)<br>    |
|   8|[0x80003648]<br>0x7F3FFFFE|- rs1 : x26<br> - rs2 : x25<br> - rd : x24<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x12a50c and fs2 == 1 and fe2 == 0xfc and fm2 == 0x356bca and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                               |[0x80000204]:fsub.s s8, s10, s9, dyn<br> [0x80000208]:csrrs tp, fcsr, zero<br> [0x8000020c]:sw s8, 56(ra)<br>    |
|   9|[0x80003650]<br>0x7F3FFFFF|- rs1 : x22<br> - rs2 : x21<br> - rd : x23<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x12a50c and fs2 == 1 and fe2 == 0xfc and fm2 == 0x356bca and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                               |[0x80000224]:fsub.s s7, s6, s5, dyn<br> [0x80000228]:csrrs tp, fcsr, zero<br> [0x8000022c]:sw s7, 64(ra)<br>     |
|  10|[0x80003658]<br>0x7F3FFFFF|- rs1 : x21<br> - rs2 : x23<br> - rd : x22<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x12a50c and fs2 == 1 and fe2 == 0xfc and fm2 == 0x356bca and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                               |[0x80000244]:fsub.s s6, s5, s7, dyn<br> [0x80000248]:csrrs tp, fcsr, zero<br> [0x8000024c]:sw s6, 72(ra)<br>     |
|  11|[0x80003660]<br>0x7F3FFFFF|- rs1 : x23<br> - rs2 : x22<br> - rd : x21<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 1 and fe2 == 0xfd and fm2 == 0x41c0bf and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000264]:fsub.s s5, s7, s6, dyn<br> [0x80000268]:csrrs tp, fcsr, zero<br> [0x8000026c]:sw s5, 80(ra)<br>     |
|  12|[0x80003668]<br>0x7F3FFFFF|- rs1 : x19<br> - rs2 : x18<br> - rd : x20<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 1 and fe2 == 0xfd and fm2 == 0x41c0bf and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                               |[0x80000284]:fsub.s s4, s3, s2, dyn<br> [0x80000288]:csrrs tp, fcsr, zero<br> [0x8000028c]:sw s4, 88(ra)<br>     |
|  13|[0x80003670]<br>0x7F3FFFFF|- rs1 : x18<br> - rs2 : x20<br> - rd : x19<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 1 and fe2 == 0xfd and fm2 == 0x41c0bf and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                               |[0x800002a4]:fsub.s s3, s2, s4, dyn<br> [0x800002a8]:csrrs tp, fcsr, zero<br> [0x800002ac]:sw s3, 96(ra)<br>     |
|  14|[0x80003678]<br>0x7F3FFFFF|- rs1 : x20<br> - rs2 : x19<br> - rd : x18<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 1 and fe2 == 0xfd and fm2 == 0x41c0bf and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                               |[0x800002c4]:fsub.s s2, s4, s3, dyn<br> [0x800002c8]:csrrs tp, fcsr, zero<br> [0x800002cc]:sw s2, 104(ra)<br>    |
|  15|[0x80003680]<br>0x7F3FFFFF|- rs1 : x16<br> - rs2 : x15<br> - rd : x17<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 1 and fe2 == 0xfd and fm2 == 0x41c0bf and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                               |[0x800002e4]:fsub.s a7, a6, a5, dyn<br> [0x800002e8]:csrrs tp, fcsr, zero<br> [0x800002ec]:sw a7, 112(ra)<br>    |
|  16|[0x80003688]<br>0x7F3FFFFE|- rs1 : x15<br> - rs2 : x17<br> - rd : x16<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x7a3631 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x05c9cc and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000304]:fsub.s a6, a5, a7, dyn<br> [0x80000308]:csrrs tp, fcsr, zero<br> [0x8000030c]:sw a6, 120(ra)<br>    |
|  17|[0x80003690]<br>0x7F3FFFFE|- rs1 : x17<br> - rs2 : x16<br> - rd : x15<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x7a3631 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x05c9cc and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                               |[0x80000324]:fsub.s a5, a7, a6, dyn<br> [0x80000328]:csrrs tp, fcsr, zero<br> [0x8000032c]:sw a5, 128(ra)<br>    |
|  18|[0x80003698]<br>0x7F3FFFFE|- rs1 : x13<br> - rs2 : x12<br> - rd : x14<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x7a3631 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x05c9cc and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                               |[0x80000344]:fsub.s a4, a3, a2, dyn<br> [0x80000348]:csrrs tp, fcsr, zero<br> [0x8000034c]:sw a4, 136(ra)<br>    |
|  19|[0x800036a0]<br>0x7F3FFFFF|- rs1 : x12<br> - rs2 : x14<br> - rd : x13<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x7a3631 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x05c9cc and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                               |[0x80000364]:fsub.s a3, a2, a4, dyn<br> [0x80000368]:csrrs tp, fcsr, zero<br> [0x8000036c]:sw a3, 144(ra)<br>    |
|  20|[0x800036a8]<br>0x7F3FFFFF|- rs1 : x14<br> - rs2 : x13<br> - rd : x12<br> - fs1 == 0 and fe1 == 0xfd and fm1 == 0x7a3631 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x05c9cc and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                               |[0x80000384]:fsub.s a2, a4, a3, dyn<br> [0x80000388]:csrrs tp, fcsr, zero<br> [0x8000038c]:sw a2, 152(ra)<br>    |
|  21|[0x800036b0]<br>0x7F3FFFFF|- rs1 : x10<br> - rs2 : x9<br> - rd : x11<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x2dfa9c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                 |[0x800003a4]:fsub.s a1, a0, s1, dyn<br> [0x800003a8]:csrrs tp, fcsr, zero<br> [0x800003ac]:sw a1, 160(ra)<br>    |
|  22|[0x800036b8]<br>0x7F3FFFFE|- rs1 : x9<br> - rs2 : x11<br> - rd : x10<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x2dfa9c and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                |[0x800003cc]:fsub.s a0, s1, a1, dyn<br> [0x800003d0]:csrrs a3, fcsr, zero<br> [0x800003d4]:sw a0, 168(ra)<br>    |
|  23|[0x800036c0]<br>0x7F3FFFFE|- rs1 : x11<br> - rs2 : x10<br> - rd : x9<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x2dfa9c and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                |[0x800003ec]:fsub.s s1, a1, a0, dyn<br> [0x800003f0]:csrrs a3, fcsr, zero<br> [0x800003f4]:sw s1, 176(ra)<br>    |
|  24|[0x800036c8]<br>0x7F3FFFFF|- rs1 : x7<br> - rs2 : x6<br> - rd : x8<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x2dfa9c and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                  |[0x8000040c]:fsub.s fp, t2, t1, dyn<br> [0x80000410]:csrrs a3, fcsr, zero<br> [0x80000414]:sw fp, 184(ra)<br>    |
|  25|[0x800036d0]<br>0x7F3FFFFF|- rs1 : x6<br> - rs2 : x8<br> - rd : x7<br> - fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x2dfa9c and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                  |[0x80000434]:fsub.s t2, t1, fp, dyn<br> [0x80000438]:csrrs a3, fcsr, zero<br> [0x8000043c]:sw t2, 0(s1)<br>      |
|  26|[0x800036d8]<br>0x7F3FFFFF|- rs1 : x8<br> - rs2 : x7<br> - rd : x6<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x207786 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x7c43c6 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                   |[0x80000454]:fsub.s t1, fp, t2, dyn<br> [0x80000458]:csrrs a3, fcsr, zero<br> [0x8000045c]:sw t1, 8(s1)<br>      |
|  27|[0x800036e0]<br>0x7F3FFFFE|- rs1 : x4<br> - rs2 : x3<br> - rd : x5<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x207786 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x7c43c6 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                  |[0x80000474]:fsub.s t0, tp, gp, dyn<br> [0x80000478]:csrrs a3, fcsr, zero<br> [0x8000047c]:sw t0, 16(s1)<br>     |
|  28|[0x800036e8]<br>0x7F3FFFFE|- rs1 : x3<br> - rs2 : x5<br> - rd : x4<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x207786 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x7c43c6 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                  |[0x80000494]:fsub.s tp, gp, t0, dyn<br> [0x80000498]:csrrs a3, fcsr, zero<br> [0x8000049c]:sw tp, 24(s1)<br>     |
|  29|[0x800036f0]<br>0x7F3FFFFF|- rs1 : x5<br> - rs2 : x4<br> - rd : x3<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x207786 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x7c43c6 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                  |[0x800004b4]:fsub.s gp, t0, tp, dyn<br> [0x800004b8]:csrrs a3, fcsr, zero<br> [0x800004bc]:sw gp, 32(s1)<br>     |
|  30|[0x800036f8]<br>0x7F207786|- rs1 : x1<br> - rs2 : x0<br> - rd : x2<br>                                                                                                                                                                                                 |[0x800004d4]:fsub.s sp, ra, zero, dyn<br> [0x800004d8]:csrrs a3, fcsr, zero<br> [0x800004dc]:sw sp, 40(s1)<br>   |
|  31|[0x80003700]<br>0x7E3691A3|- rs1 : x0<br> - rs2 : x2<br> - rd : x1<br>                                                                                                                                                                                                 |[0x800004f4]:fsub.s ra, zero, sp, dyn<br> [0x800004f8]:csrrs a3, fcsr, zero<br> [0x800004fc]:sw ra, 48(s1)<br>   |
|  32|[0x80003708]<br>0x00000000|- rs1 : x2<br> - rs2 : x1<br> - rd : x0<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 1 and fe2 == 0xfc and fm2 == 0x3691a3 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                  |[0x80000514]:fsub.s zero, sp, ra, dyn<br> [0x80000518]:csrrs a3, fcsr, zero<br> [0x8000051c]:sw zero, 56(s1)<br> |
|  33|[0x80003710]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 1 and fe2 == 0xfc and fm2 == 0x3691a3 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000534]:fsub.s t6, t5, t4, dyn<br> [0x80000538]:csrrs a3, fcsr, zero<br> [0x8000053c]:sw t6, 64(s1)<br>     |
|  34|[0x80003718]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 1 and fe2 == 0xfc and fm2 == 0x3691a3 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000554]:fsub.s t6, t5, t4, dyn<br> [0x80000558]:csrrs a3, fcsr, zero<br> [0x8000055c]:sw t6, 72(s1)<br>     |
|  35|[0x80003720]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 1 and fe2 == 0xfc and fm2 == 0x3691a3 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000574]:fsub.s t6, t5, t4, dyn<br> [0x80000578]:csrrs a3, fcsr, zero<br> [0x8000057c]:sw t6, 80(s1)<br>     |
|  36|[0x80003728]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2b0757 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x54f8a7 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000594]:fsub.s t6, t5, t4, dyn<br> [0x80000598]:csrrs a3, fcsr, zero<br> [0x8000059c]:sw t6, 88(s1)<br>     |
|  37|[0x80003730]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2b0757 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x54f8a7 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800005b4]:fsub.s t6, t5, t4, dyn<br> [0x800005b8]:csrrs a3, fcsr, zero<br> [0x800005bc]:sw t6, 96(s1)<br>     |
|  38|[0x80003738]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2b0757 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x54f8a7 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800005d4]:fsub.s t6, t5, t4, dyn<br> [0x800005d8]:csrrs a3, fcsr, zero<br> [0x800005dc]:sw t6, 104(s1)<br>    |
|  39|[0x80003740]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2b0757 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x54f8a7 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800005f4]:fsub.s t6, t5, t4, dyn<br> [0x800005f8]:csrrs a3, fcsr, zero<br> [0x800005fc]:sw t6, 112(s1)<br>    |
|  40|[0x80003748]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x2b0757 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x54f8a7 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000614]:fsub.s t6, t5, t4, dyn<br> [0x80000618]:csrrs a3, fcsr, zero<br> [0x8000061c]:sw t6, 120(s1)<br>    |
|  41|[0x80003750]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x1413e0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000634]:fsub.s t6, t5, t4, dyn<br> [0x80000638]:csrrs a3, fcsr, zero<br> [0x8000063c]:sw t6, 128(s1)<br>    |
|  42|[0x80003758]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x1413e0 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000654]:fsub.s t6, t5, t4, dyn<br> [0x80000658]:csrrs a3, fcsr, zero<br> [0x8000065c]:sw t6, 136(s1)<br>    |
|  43|[0x80003760]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x1413e0 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000674]:fsub.s t6, t5, t4, dyn<br> [0x80000678]:csrrs a3, fcsr, zero<br> [0x8000067c]:sw t6, 144(s1)<br>    |
|  44|[0x80003768]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x1413e0 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000694]:fsub.s t6, t5, t4, dyn<br> [0x80000698]:csrrs a3, fcsr, zero<br> [0x8000069c]:sw t6, 152(s1)<br>    |
|  45|[0x80003770]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x1413e0 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800006b4]:fsub.s t6, t5, t4, dyn<br> [0x800006b8]:csrrs a3, fcsr, zero<br> [0x800006bc]:sw t6, 160(s1)<br>    |
|  46|[0x80003778]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2d12f5 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x176848 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800006d4]:fsub.s t6, t5, t4, dyn<br> [0x800006d8]:csrrs a3, fcsr, zero<br> [0x800006dc]:sw t6, 168(s1)<br>    |
|  47|[0x80003780]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2d12f5 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x176848 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800006f4]:fsub.s t6, t5, t4, dyn<br> [0x800006f8]:csrrs a3, fcsr, zero<br> [0x800006fc]:sw t6, 176(s1)<br>    |
|  48|[0x80003788]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2d12f5 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x176848 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000714]:fsub.s t6, t5, t4, dyn<br> [0x80000718]:csrrs a3, fcsr, zero<br> [0x8000071c]:sw t6, 184(s1)<br>    |
|  49|[0x80003790]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2d12f5 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x176848 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000734]:fsub.s t6, t5, t4, dyn<br> [0x80000738]:csrrs a3, fcsr, zero<br> [0x8000073c]:sw t6, 192(s1)<br>    |
|  50|[0x80003798]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x2d12f5 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x176848 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000754]:fsub.s t6, t5, t4, dyn<br> [0x80000758]:csrrs a3, fcsr, zero<br> [0x8000075c]:sw t6, 200(s1)<br>    |
|  51|[0x800037a0]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x1c56e0 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3b1d48 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000774]:fsub.s t6, t5, t4, dyn<br> [0x80000778]:csrrs a3, fcsr, zero<br> [0x8000077c]:sw t6, 208(s1)<br>    |
|  52|[0x800037a8]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x1c56e0 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3b1d48 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000794]:fsub.s t6, t5, t4, dyn<br> [0x80000798]:csrrs a3, fcsr, zero<br> [0x8000079c]:sw t6, 216(s1)<br>    |
|  53|[0x800037b0]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x1c56e0 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3b1d48 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800007b4]:fsub.s t6, t5, t4, dyn<br> [0x800007b8]:csrrs a3, fcsr, zero<br> [0x800007bc]:sw t6, 224(s1)<br>    |
|  54|[0x800037b8]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x1c56e0 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3b1d48 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800007d4]:fsub.s t6, t5, t4, dyn<br> [0x800007d8]:csrrs a3, fcsr, zero<br> [0x800007dc]:sw t6, 232(s1)<br>    |
|  55|[0x800037c0]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x1c56e0 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3b1d48 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800007f4]:fsub.s t6, t5, t4, dyn<br> [0x800007f8]:csrrs a3, fcsr, zero<br> [0x800007fc]:sw t6, 240(s1)<br>    |
|  56|[0x800037c8]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x34510e and fs2 == 1 and fe2 == 0xfa and fm2 == 0x3aef03 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000814]:fsub.s t6, t5, t4, dyn<br> [0x80000818]:csrrs a3, fcsr, zero<br> [0x8000081c]:sw t6, 248(s1)<br>    |
|  57|[0x800037d0]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x34510e and fs2 == 1 and fe2 == 0xfa and fm2 == 0x3aef03 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000834]:fsub.s t6, t5, t4, dyn<br> [0x80000838]:csrrs a3, fcsr, zero<br> [0x8000083c]:sw t6, 256(s1)<br>    |
|  58|[0x800037d8]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x34510e and fs2 == 1 and fe2 == 0xfa and fm2 == 0x3aef03 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000854]:fsub.s t6, t5, t4, dyn<br> [0x80000858]:csrrs a3, fcsr, zero<br> [0x8000085c]:sw t6, 264(s1)<br>    |
|  59|[0x800037e0]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x34510e and fs2 == 1 and fe2 == 0xfa and fm2 == 0x3aef03 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000874]:fsub.s t6, t5, t4, dyn<br> [0x80000878]:csrrs a3, fcsr, zero<br> [0x8000087c]:sw t6, 272(s1)<br>    |
|  60|[0x800037e8]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x34510e and fs2 == 1 and fe2 == 0xfa and fm2 == 0x3aef03 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000894]:fsub.s t6, t5, t4, dyn<br> [0x80000898]:csrrs a3, fcsr, zero<br> [0x8000089c]:sw t6, 280(s1)<br>    |
|  61|[0x800037f0]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x26592c and fs2 == 1 and fe2 == 0xfe and fm2 == 0x1669b4 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800008b4]:fsub.s t6, t5, t4, dyn<br> [0x800008b8]:csrrs a3, fcsr, zero<br> [0x800008bc]:sw t6, 288(s1)<br>    |
|  62|[0x800037f8]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x26592c and fs2 == 1 and fe2 == 0xfe and fm2 == 0x1669b4 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800008d4]:fsub.s t6, t5, t4, dyn<br> [0x800008d8]:csrrs a3, fcsr, zero<br> [0x800008dc]:sw t6, 296(s1)<br>    |
|  63|[0x80003800]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x26592c and fs2 == 1 and fe2 == 0xfe and fm2 == 0x1669b4 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800008f4]:fsub.s t6, t5, t4, dyn<br> [0x800008f8]:csrrs a3, fcsr, zero<br> [0x800008fc]:sw t6, 304(s1)<br>    |
|  64|[0x80003808]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x26592c and fs2 == 1 and fe2 == 0xfe and fm2 == 0x1669b4 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000914]:fsub.s t6, t5, t4, dyn<br> [0x80000918]:csrrs a3, fcsr, zero<br> [0x8000091c]:sw t6, 312(s1)<br>    |
|  65|[0x80003810]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x26592c and fs2 == 1 and fe2 == 0xfe and fm2 == 0x1669b4 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000934]:fsub.s t6, t5, t4, dyn<br> [0x80000938]:csrrs a3, fcsr, zero<br> [0x8000093c]:sw t6, 320(s1)<br>    |
|  66|[0x80003818]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x326d35 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a6c95 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000954]:fsub.s t6, t5, t4, dyn<br> [0x80000958]:csrrs a3, fcsr, zero<br> [0x8000095c]:sw t6, 328(s1)<br>    |
|  67|[0x80003820]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x326d35 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a6c95 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000974]:fsub.s t6, t5, t4, dyn<br> [0x80000978]:csrrs a3, fcsr, zero<br> [0x8000097c]:sw t6, 336(s1)<br>    |
|  68|[0x80003828]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x326d35 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a6c95 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000994]:fsub.s t6, t5, t4, dyn<br> [0x80000998]:csrrs a3, fcsr, zero<br> [0x8000099c]:sw t6, 344(s1)<br>    |
|  69|[0x80003830]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x326d35 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a6c95 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800009b4]:fsub.s t6, t5, t4, dyn<br> [0x800009b8]:csrrs a3, fcsr, zero<br> [0x800009bc]:sw t6, 352(s1)<br>    |
|  70|[0x80003838]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x326d35 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a6c95 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800009d4]:fsub.s t6, t5, t4, dyn<br> [0x800009d8]:csrrs a3, fcsr, zero<br> [0x800009dc]:sw t6, 360(s1)<br>    |
|  71|[0x80003840]<br>0x7DBFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x282619 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x704c33 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800009f4]:fsub.s t6, t5, t4, dyn<br> [0x800009f8]:csrrs a3, fcsr, zero<br> [0x800009fc]:sw t6, 368(s1)<br>    |
|  72|[0x80003848]<br>0x7DBFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x282619 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x704c33 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000a14]:fsub.s t6, t5, t4, dyn<br> [0x80000a18]:csrrs a3, fcsr, zero<br> [0x80000a1c]:sw t6, 376(s1)<br>    |
|  73|[0x80003850]<br>0x7DBFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x282619 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x704c33 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000a34]:fsub.s t6, t5, t4, dyn<br> [0x80000a38]:csrrs a3, fcsr, zero<br> [0x80000a3c]:sw t6, 384(s1)<br>    |
|  74|[0x80003858]<br>0x7DBFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x282619 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x704c33 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000a54]:fsub.s t6, t5, t4, dyn<br> [0x80000a58]:csrrs a3, fcsr, zero<br> [0x80000a5c]:sw t6, 392(s1)<br>    |
|  75|[0x80003860]<br>0x7DBFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x282619 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x704c33 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000a74]:fsub.s t6, t5, t4, dyn<br> [0x80000a78]:csrrs a3, fcsr, zero<br> [0x80000a7c]:sw t6, 400(s1)<br>    |
|  76|[0x80003868]<br>0xFDBFFFFC|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x330244 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x098121 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000a94]:fsub.s t6, t5, t4, dyn<br> [0x80000a98]:csrrs a3, fcsr, zero<br> [0x80000a9c]:sw t6, 408(s1)<br>    |
|  77|[0x80003870]<br>0xFDBFFFFC|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x330244 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x098121 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000ab4]:fsub.s t6, t5, t4, dyn<br> [0x80000ab8]:csrrs a3, fcsr, zero<br> [0x80000abc]:sw t6, 416(s1)<br>    |
|  78|[0x80003878]<br>0xFDBFFFFC|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x330244 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x098121 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000ad4]:fsub.s t6, t5, t4, dyn<br> [0x80000ad8]:csrrs a3, fcsr, zero<br> [0x80000adc]:sw t6, 424(s1)<br>    |
|  79|[0x80003880]<br>0xFDBFFFFC|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x330244 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x098121 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000af4]:fsub.s t6, t5, t4, dyn<br> [0x80000af8]:csrrs a3, fcsr, zero<br> [0x80000afc]:sw t6, 432(s1)<br>    |
|  80|[0x80003888]<br>0xFDBFFFFC|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x330244 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x098121 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000b14]:fsub.s t6, t5, t4, dyn<br> [0x80000b18]:csrrs a3, fcsr, zero<br> [0x80000b1c]:sw t6, 440(s1)<br>    |
|  81|[0x80003890]<br>0x7E3FFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x280619 and fs2 == 1 and fe2 == 0xf9 and fm2 == 0x3fcf2e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000b34]:fsub.s t6, t5, t4, dyn<br> [0x80000b38]:csrrs a3, fcsr, zero<br> [0x80000b3c]:sw t6, 448(s1)<br>    |
|  82|[0x80003898]<br>0x7E3FFFFE|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x280619 and fs2 == 1 and fe2 == 0xf9 and fm2 == 0x3fcf2e and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000b54]:fsub.s t6, t5, t4, dyn<br> [0x80000b58]:csrrs a3, fcsr, zero<br> [0x80000b5c]:sw t6, 456(s1)<br>    |
|  83|[0x800038a0]<br>0x7E3FFFFE|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x280619 and fs2 == 1 and fe2 == 0xf9 and fm2 == 0x3fcf2e and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000b74]:fsub.s t6, t5, t4, dyn<br> [0x80000b78]:csrrs a3, fcsr, zero<br> [0x80000b7c]:sw t6, 464(s1)<br>    |
|  84|[0x800038a8]<br>0x7E3FFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x280619 and fs2 == 1 and fe2 == 0xf9 and fm2 == 0x3fcf2e and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000b94]:fsub.s t6, t5, t4, dyn<br> [0x80000b98]:csrrs a3, fcsr, zero<br> [0x80000b9c]:sw t6, 472(s1)<br>    |
|  85|[0x800038b0]<br>0x7E3FFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x280619 and fs2 == 1 and fe2 == 0xf9 and fm2 == 0x3fcf2e and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000bb4]:fsub.s t6, t5, t4, dyn<br> [0x80000bb8]:csrrs a3, fcsr, zero<br> [0x80000bbc]:sw t6, 480(s1)<br>    |
|  86|[0x800038b8]<br>0xFE3FFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x5e8d61 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x4f46b0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000bd4]:fsub.s t6, t5, t4, dyn<br> [0x80000bd8]:csrrs a3, fcsr, zero<br> [0x80000bdc]:sw t6, 488(s1)<br>    |
|  87|[0x800038c0]<br>0xFE3FFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x5e8d61 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x4f46b0 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000bf4]:fsub.s t6, t5, t4, dyn<br> [0x80000bf8]:csrrs a3, fcsr, zero<br> [0x80000bfc]:sw t6, 496(s1)<br>    |
|  88|[0x800038c8]<br>0xFE3FFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x5e8d61 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x4f46b0 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000c14]:fsub.s t6, t5, t4, dyn<br> [0x80000c18]:csrrs a3, fcsr, zero<br> [0x80000c1c]:sw t6, 504(s1)<br>    |
|  89|[0x800038d0]<br>0xFE3FFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x5e8d61 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x4f46b0 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000c34]:fsub.s t6, t5, t4, dyn<br> [0x80000c38]:csrrs a3, fcsr, zero<br> [0x80000c3c]:sw t6, 512(s1)<br>    |
|  90|[0x800038d8]<br>0xFE3FFFFF|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x5e8d61 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x4f46b0 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000c54]:fsub.s t6, t5, t4, dyn<br> [0x80000c58]:csrrs a3, fcsr, zero<br> [0x80000c5c]:sw t6, 520(s1)<br>    |
|  91|[0x800038e0]<br>0x7EBFFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x20d0a1 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x01a143 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000c74]:fsub.s t6, t5, t4, dyn<br> [0x80000c78]:csrrs a3, fcsr, zero<br> [0x80000c7c]:sw t6, 528(s1)<br>    |
|  92|[0x800038e8]<br>0x7EBFFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x20d0a1 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x01a143 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000c94]:fsub.s t6, t5, t4, dyn<br> [0x80000c98]:csrrs a3, fcsr, zero<br> [0x80000c9c]:sw t6, 536(s1)<br>    |
|  93|[0x800038f0]<br>0x7EBFFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x20d0a1 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x01a143 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000cb4]:fsub.s t6, t5, t4, dyn<br> [0x80000cb8]:csrrs a3, fcsr, zero<br> [0x80000cbc]:sw t6, 544(s1)<br>    |
|  94|[0x800038f8]<br>0x7EBFFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x20d0a1 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x01a143 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000cd4]:fsub.s t6, t5, t4, dyn<br> [0x80000cd8]:csrrs a3, fcsr, zero<br> [0x80000cdc]:sw t6, 552(s1)<br>    |
|  95|[0x80003900]<br>0x7EBFFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x20d0a1 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x01a143 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000cf4]:fsub.s t6, t5, t4, dyn<br> [0x80000cf8]:csrrs a3, fcsr, zero<br> [0x80000cfc]:sw t6, 560(s1)<br>    |
|  96|[0x80003908]<br>0xFEBFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x76a41a and fs2 == 0 and fe2 == 0xfe and fm2 == 0x5b520c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000d14]:fsub.s t6, t5, t4, dyn<br> [0x80000d18]:csrrs a3, fcsr, zero<br> [0x80000d1c]:sw t6, 568(s1)<br>    |
|  97|[0x80003910]<br>0xFEBFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x76a41a and fs2 == 0 and fe2 == 0xfe and fm2 == 0x5b520c and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000d34]:fsub.s t6, t5, t4, dyn<br> [0x80000d38]:csrrs a3, fcsr, zero<br> [0x80000d3c]:sw t6, 576(s1)<br>    |
|  98|[0x80003918]<br>0xFEBFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x76a41a and fs2 == 0 and fe2 == 0xfe and fm2 == 0x5b520c and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000d54]:fsub.s t6, t5, t4, dyn<br> [0x80000d58]:csrrs a3, fcsr, zero<br> [0x80000d5c]:sw t6, 584(s1)<br>    |
|  99|[0x80003920]<br>0xFEBFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x76a41a and fs2 == 0 and fe2 == 0xfe and fm2 == 0x5b520c and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000d74]:fsub.s t6, t5, t4, dyn<br> [0x80000d78]:csrrs a3, fcsr, zero<br> [0x80000d7c]:sw t6, 592(s1)<br>    |
| 100|[0x80003928]<br>0xFEBFFFFE|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x76a41a and fs2 == 0 and fe2 == 0xfe and fm2 == 0x5b520c and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000d94]:fsub.s t6, t5, t4, dyn<br> [0x80000d98]:csrrs a3, fcsr, zero<br> [0x80000d9c]:sw t6, 600(s1)<br>    |
| 101|[0x80003930]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3e917d and fs2 == 1 and fe2 == 0xf7 and fm2 == 0x3740ca and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000db4]:fsub.s t6, t5, t4, dyn<br> [0x80000db8]:csrrs a3, fcsr, zero<br> [0x80000dbc]:sw t6, 608(s1)<br>    |
| 102|[0x80003938]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3e917d and fs2 == 1 and fe2 == 0xf7 and fm2 == 0x3740ca and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000dd4]:fsub.s t6, t5, t4, dyn<br> [0x80000dd8]:csrrs a3, fcsr, zero<br> [0x80000ddc]:sw t6, 616(s1)<br>    |
| 103|[0x80003940]<br>0x7F3FFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3e917d and fs2 == 1 and fe2 == 0xf7 and fm2 == 0x3740ca and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000df4]:fsub.s t6, t5, t4, dyn<br> [0x80000df8]:csrrs a3, fcsr, zero<br> [0x80000dfc]:sw t6, 624(s1)<br>    |
| 104|[0x80003948]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3e917d and fs2 == 1 and fe2 == 0xf7 and fm2 == 0x3740ca and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000e14]:fsub.s t6, t5, t4, dyn<br> [0x80000e18]:csrrs a3, fcsr, zero<br> [0x80000e1c]:sw t6, 632(s1)<br>    |
| 105|[0x80003950]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3e917d and fs2 == 1 and fe2 == 0xf7 and fm2 == 0x3740ca and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000e34]:fsub.s t6, t5, t4, dyn<br> [0x80000e38]:csrrs a3, fcsr, zero<br> [0x80000e3c]:sw t6, 640(s1)<br>    |
| 106|[0x80003958]<br>0xFF3FFFFE|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x39afdd and fs2 == 0 and fe2 == 0xfe and fm2 == 0x5735fa and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000e54]:fsub.s t6, t5, t4, dyn<br> [0x80000e58]:csrrs a3, fcsr, zero<br> [0x80000e5c]:sw t6, 648(s1)<br>    |
| 107|[0x80003960]<br>0xFF3FFFFE|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x39afdd and fs2 == 0 and fe2 == 0xfe and fm2 == 0x5735fa and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000e74]:fsub.s t6, t5, t4, dyn<br> [0x80000e78]:csrrs a3, fcsr, zero<br> [0x80000e7c]:sw t6, 656(s1)<br>    |
| 108|[0x80003968]<br>0xFF3FFFFF|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x39afdd and fs2 == 0 and fe2 == 0xfe and fm2 == 0x5735fa and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000e94]:fsub.s t6, t5, t4, dyn<br> [0x80000e98]:csrrs a3, fcsr, zero<br> [0x80000e9c]:sw t6, 664(s1)<br>    |
| 109|[0x80003970]<br>0xFF3FFFFE|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x39afdd and fs2 == 0 and fe2 == 0xfe and fm2 == 0x5735fa and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000eb4]:fsub.s t6, t5, t4, dyn<br> [0x80000eb8]:csrrs a3, fcsr, zero<br> [0x80000ebc]:sw t6, 672(s1)<br>    |
| 110|[0x80003978]<br>0xFF3FFFFE|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x39afdd and fs2 == 0 and fe2 == 0xfe and fm2 == 0x5735fa and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000ed4]:fsub.s t6, t5, t4, dyn<br> [0x80000ed8]:csrrs a3, fcsr, zero<br> [0x80000edc]:sw t6, 680(s1)<br>    |
| 111|[0x80003980]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a8666 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x757998 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000ef4]:fsub.s t6, t5, t4, dyn<br> [0x80000ef8]:csrrs a3, fcsr, zero<br> [0x80000efc]:sw t6, 688(s1)<br>    |
| 112|[0x80003988]<br>0x7F7FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a8666 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x757998 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000f14]:fsub.s t6, t5, t4, dyn<br> [0x80000f18]:csrrs a3, fcsr, zero<br> [0x80000f1c]:sw t6, 696(s1)<br>    |
| 113|[0x80003990]<br>0x7F7FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a8666 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x757998 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000f34]:fsub.s t6, t5, t4, dyn<br> [0x80000f38]:csrrs a3, fcsr, zero<br> [0x80000f3c]:sw t6, 704(s1)<br>    |
| 114|[0x80003998]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a8666 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x757998 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000f54]:fsub.s t6, t5, t4, dyn<br> [0x80000f58]:csrrs a3, fcsr, zero<br> [0x80000f5c]:sw t6, 712(s1)<br>    |
| 115|[0x800039a0]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a8666 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x757998 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000f74]:fsub.s t6, t5, t4, dyn<br> [0x80000f78]:csrrs a3, fcsr, zero<br> [0x80000f7c]:sw t6, 720(s1)<br>    |
| 116|[0x800039a8]<br>0xFE986824|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x33cbed and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000f94]:fsub.s t6, t5, t4, dyn<br> [0x80000f98]:csrrs a3, fcsr, zero<br> [0x80000f9c]:sw t6, 728(s1)<br>    |
| 117|[0x800039b0]<br>0xFE986824|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x33cbed and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000fb4]:fsub.s t6, t5, t4, dyn<br> [0x80000fb8]:csrrs a3, fcsr, zero<br> [0x80000fbc]:sw t6, 736(s1)<br>    |
| 118|[0x800039b8]<br>0xFE986824|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x33cbed and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000fd4]:fsub.s t6, t5, t4, dyn<br> [0x80000fd8]:csrrs a3, fcsr, zero<br> [0x80000fdc]:sw t6, 744(s1)<br>    |
| 119|[0x800039c0]<br>0xFE986824|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x33cbed and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000ff4]:fsub.s t6, t5, t4, dyn<br> [0x80000ff8]:csrrs a3, fcsr, zero<br> [0x80000ffc]:sw t6, 752(s1)<br>    |
| 120|[0x800039c8]<br>0xFE986824|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x33cbed and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80001014]:fsub.s t6, t5, t4, dyn<br> [0x80001018]:csrrs a3, fcsr, zero<br> [0x8000101c]:sw t6, 760(s1)<br>    |
| 121|[0x800039d0]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1f5de9 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001034]:fsub.s t6, t5, t4, dyn<br> [0x80001038]:csrrs a3, fcsr, zero<br> [0x8000103c]:sw t6, 768(s1)<br>    |
| 122|[0x800039d8]<br>0x7F7FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1f5de9 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80001054]:fsub.s t6, t5, t4, dyn<br> [0x80001058]:csrrs a3, fcsr, zero<br> [0x8000105c]:sw t6, 776(s1)<br>    |
| 123|[0x800039e0]<br>0x7F7FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1f5de9 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80001074]:fsub.s t6, t5, t4, dyn<br> [0x80001078]:csrrs a3, fcsr, zero<br> [0x8000107c]:sw t6, 784(s1)<br>    |
| 124|[0x800039e8]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1f5de9 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80001094]:fsub.s t6, t5, t4, dyn<br> [0x80001098]:csrrs a3, fcsr, zero<br> [0x8000109c]:sw t6, 792(s1)<br>    |
| 125|[0x800039f0]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1f5de9 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800010b4]:fsub.s t6, t5, t4, dyn<br> [0x800010b8]:csrrs a3, fcsr, zero<br> [0x800010bc]:sw t6, 800(s1)<br>    |
| 126|[0x800039f8]<br>0xFF45C0D4|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x68fcac and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800010d4]:fsub.s t6, t5, t4, dyn<br> [0x800010d8]:csrrs a3, fcsr, zero<br> [0x800010dc]:sw t6, 808(s1)<br>    |
| 127|[0x80003a00]<br>0xFF45C0D4|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x68fcac and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800010f4]:fsub.s t6, t5, t4, dyn<br> [0x800010f8]:csrrs a3, fcsr, zero<br> [0x800010fc]:sw t6, 816(s1)<br>    |
| 128|[0x80003a08]<br>0xFF45C0D4|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x68fcac and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80001114]:fsub.s t6, t5, t4, dyn<br> [0x80001118]:csrrs a3, fcsr, zero<br> [0x8000111c]:sw t6, 824(s1)<br>    |
| 129|[0x80003a10]<br>0xFF45C0D4|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x68fcac and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80001134]:fsub.s t6, t5, t4, dyn<br> [0x80001138]:csrrs a3, fcsr, zero<br> [0x8000113c]:sw t6, 832(s1)<br>    |
| 130|[0x80003a18]<br>0xFF45C0D4|- fs1 == 0 and fe1 == 0xfc and fm1 == 0x68fcac and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80001154]:fsub.s t6, t5, t4, dyn<br> [0x80001158]:csrrs a3, fcsr, zero<br> [0x8000115c]:sw t6, 840(s1)<br>    |
| 131|[0x80003a20]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x296f9b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001174]:fsub.s t6, t5, t4, dyn<br> [0x80001178]:csrrs a3, fcsr, zero<br> [0x8000117c]:sw t6, 848(s1)<br>    |
| 132|[0x80003a28]<br>0x7F7FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x296f9b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80001194]:fsub.s t6, t5, t4, dyn<br> [0x80001198]:csrrs a3, fcsr, zero<br> [0x8000119c]:sw t6, 856(s1)<br>    |
| 133|[0x80003a30]<br>0x7F7FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x296f9b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800011b4]:fsub.s t6, t5, t4, dyn<br> [0x800011b8]:csrrs a3, fcsr, zero<br> [0x800011bc]:sw t6, 864(s1)<br>    |
| 134|[0x80003a38]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x296f9b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800011d4]:fsub.s t6, t5, t4, dyn<br> [0x800011d8]:csrrs a3, fcsr, zero<br> [0x800011dc]:sw t6, 872(s1)<br>    |
| 135|[0x80003a40]<br>0x7F800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x296f9b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800011f4]:fsub.s t6, t5, t4, dyn<br> [0x800011f8]:csrrs a3, fcsr, zero<br> [0x800011fc]:sw t6, 880(s1)<br>    |
| 136|[0x80003a48]<br>0xFF1EFC4A|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x42076b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001214]:fsub.s t6, t5, t4, dyn<br> [0x80001218]:csrrs a3, fcsr, zero<br> [0x8000121c]:sw t6, 888(s1)<br>    |
| 137|[0x80003a50]<br>0xFF1EFC49|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x42076b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80001234]:fsub.s t6, t5, t4, dyn<br> [0x80001238]:csrrs a3, fcsr, zero<br> [0x8000123c]:sw t6, 896(s1)<br>    |
| 138|[0x80003a58]<br>0xFF1EFC4A|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x42076b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80001254]:fsub.s t6, t5, t4, dyn<br> [0x80001258]:csrrs a3, fcsr, zero<br> [0x8000125c]:sw t6, 904(s1)<br>    |
| 139|[0x80003a60]<br>0xFF1EFC49|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x42076b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80001274]:fsub.s t6, t5, t4, dyn<br> [0x80001278]:csrrs a3, fcsr, zero<br> [0x8000127c]:sw t6, 912(s1)<br>    |
| 140|[0x80003a68]<br>0xFF1EFC4A|- fs1 == 0 and fe1 == 0xfd and fm1 == 0x42076b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80001294]:fsub.s t6, t5, t4, dyn<br> [0x80001298]:csrrs a3, fcsr, zero<br> [0x8000129c]:sw t6, 920(s1)<br>    |
| 141|[0x80003a78]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x2633ba and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800012d4]:fsub.s t6, t5, t4, dyn<br> [0x800012d8]:csrrs a3, fcsr, zero<br> [0x800012dc]:sw t6, 936(s1)<br>    |
| 142|[0x80003a80]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x2633ba and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800012f4]:fsub.s t6, t5, t4, dyn<br> [0x800012f8]:csrrs a3, fcsr, zero<br> [0x800012fc]:sw t6, 944(s1)<br>    |
| 143|[0x80003a90]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x207786 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x7c43c6 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80001334]:fsub.s t6, t5, t4, dyn<br> [0x80001338]:csrrs a3, fcsr, zero<br> [0x8000133c]:sw t6, 960(s1)<br>    |
| 144|[0x80003a98]<br>0x7F3FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 1 and fe2 == 0xfc and fm2 == 0x3691a3 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001354]:fsub.s t6, t5, t4, dyn<br> [0x80001358]:csrrs a3, fcsr, zero<br> [0x8000135c]:sw t6, 968(s1)<br>    |
