
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000af0')]      |
| SIG_REGION                | [('0x80002410', '0x80002680', '156 words')]      |
| COV_LABELS                | fsub_b10      |
| TEST_NAME                 | /home/reg/work/zfinx/fsub.s/work/fsub_b10-01.S/ref.S    |
| Total Number of coverpoints| 173     |
| Total Coverpoints Hit     | 173      |
| Total Signature Updates   | 156      |
| STAT1                     | 75      |
| STAT2                     | 3      |
| STAT3                     | 0     |
| STAT4                     | 78     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000a14]:fsub.s t6, t5, t4, dyn
      [0x80000a18]:csrrs a3, fcsr, zero
      [0x80000a1c]:sw t6, 376(s1)
 -- Signature Address: 0x80002648 Data: 0x78D35630
 -- Redundant Coverpoints hit by the op
      - mnemonic : fsub.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x178cde and  fcsr == 0x0 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000a74]:fsub.s t6, t5, t4, dyn
      [0x80000a78]:csrrs a3, fcsr, zero
      [0x80000a7c]:sw t6, 400(s1)
 -- Signature Address: 0x80002660 Data: 0x78D35630
 -- Redundant Coverpoints hit by the op
      - mnemonic : fsub.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x6097c5 and  fcsr == 0x0 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000ad4]:fsub.s t6, t5, t4, dyn
      [0x80000ad8]:csrrs a3, fcsr, zero
      [0x80000adc]:sw t6, 424(s1)
 -- Signature Address: 0x80002678 Data: 0x78D35630
 -- Redundant Coverpoints hit by the op
      - mnemonic : fsub.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x72 and fm2 == 0x62c7e4 and  fcsr == 0x0 and rm_val == 7   #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fsub.s', 'rs1 : x31', 'rs2 : x30', 'rd : x31', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x178cde and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000124]:fsub.s t6, t6, t5, dyn
	-[0x80000128]:csrrs tp, fcsr, zero
	-[0x8000012c]:sw t6, 0(ra)
Current Store : [0x80000130] : sw tp, 4(ra) -- Store: [0x80002414]:0x00000001




Last Coverpoint : ['rs1 : x29', 'rs2 : x29', 'rd : x29', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x80000144]:fsub.s t4, t4, t4, dyn
	-[0x80000148]:csrrs tp, fcsr, zero
	-[0x8000014c]:sw t4, 8(ra)
Current Store : [0x80000150] : sw tp, 12(ra) -- Store: [0x8000241c]:0x00000000




Last Coverpoint : ['rs1 : x28', 'rs2 : x28', 'rd : x30', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x80000164]:fsub.s t5, t3, t3, dyn
	-[0x80000168]:csrrs tp, fcsr, zero
	-[0x8000016c]:sw t5, 16(ra)
Current Store : [0x80000170] : sw tp, 20(ra) -- Store: [0x80002424]:0x00000000




Last Coverpoint : ['rs1 : x30', 'rs2 : x27', 'rd : x27', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x6097c5 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000184]:fsub.s s11, t5, s11, dyn
	-[0x80000188]:csrrs tp, fcsr, zero
	-[0x8000018c]:sw s11, 24(ra)
Current Store : [0x80000190] : sw tp, 28(ra) -- Store: [0x8000242c]:0x00000001




Last Coverpoint : ['rs1 : x27', 'rs2 : x31', 'rd : x28', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x19 and fm2 == 0x0c5edb and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001a4]:fsub.s t3, s11, t6, dyn
	-[0x800001a8]:csrrs tp, fcsr, zero
	-[0x800001ac]:sw t3, 32(ra)
Current Store : [0x800001b0] : sw tp, 36(ra) -- Store: [0x80002434]:0x00000001




Last Coverpoint : ['rs1 : x25', 'rs2 : x24', 'rd : x26', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x1c and fm2 == 0x2f7692 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001c4]:fsub.s s10, s9, s8, dyn
	-[0x800001c8]:csrrs tp, fcsr, zero
	-[0x800001cc]:sw s10, 40(ra)
Current Store : [0x800001d0] : sw tp, 44(ra) -- Store: [0x8000243c]:0x00000001




Last Coverpoint : ['rs1 : x24', 'rs2 : x26', 'rd : x25', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x5b5437 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001e4]:fsub.s s9, s8, s10, dyn
	-[0x800001e8]:csrrs tp, fcsr, zero
	-[0x800001ec]:sw s9, 48(ra)
Current Store : [0x800001f0] : sw tp, 52(ra) -- Store: [0x80002444]:0x00000001




Last Coverpoint : ['rs1 : x26', 'rs2 : x25', 'rd : x24', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x23 and fm2 == 0x0914a2 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000204]:fsub.s s8, s10, s9, dyn
	-[0x80000208]:csrrs tp, fcsr, zero
	-[0x8000020c]:sw s8, 56(ra)
Current Store : [0x80000210] : sw tp, 60(ra) -- Store: [0x8000244c]:0x00000001




Last Coverpoint : ['rs1 : x22', 'rs2 : x21', 'rd : x23', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x26 and fm2 == 0x2b59cb and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000224]:fsub.s s7, s6, s5, dyn
	-[0x80000228]:csrrs tp, fcsr, zero
	-[0x8000022c]:sw s7, 64(ra)
Current Store : [0x80000230] : sw tp, 68(ra) -- Store: [0x80002454]:0x00000001




Last Coverpoint : ['rs1 : x21', 'rs2 : x23', 'rd : x22', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x29 and fm2 == 0x56303d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000244]:fsub.s s6, s5, s7, dyn
	-[0x80000248]:csrrs tp, fcsr, zero
	-[0x8000024c]:sw s6, 72(ra)
Current Store : [0x80000250] : sw tp, 76(ra) -- Store: [0x8000245c]:0x00000001




Last Coverpoint : ['rs1 : x23', 'rs2 : x22', 'rd : x21', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x2d and fm2 == 0x05de26 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000264]:fsub.s s5, s7, s6, dyn
	-[0x80000268]:csrrs tp, fcsr, zero
	-[0x8000026c]:sw s5, 80(ra)
Current Store : [0x80000270] : sw tp, 84(ra) -- Store: [0x80002464]:0x00000001




Last Coverpoint : ['rs1 : x19', 'rs2 : x18', 'rd : x20', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x30 and fm2 == 0x2755b0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000284]:fsub.s s4, s3, s2, dyn
	-[0x80000288]:csrrs tp, fcsr, zero
	-[0x8000028c]:sw s4, 88(ra)
Current Store : [0x80000290] : sw tp, 92(ra) -- Store: [0x8000246c]:0x00000001




Last Coverpoint : ['rs1 : x18', 'rs2 : x20', 'rd : x19', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x33 and fm2 == 0x512b1c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002a4]:fsub.s s3, s2, s4, dyn
	-[0x800002a8]:csrrs tp, fcsr, zero
	-[0x800002ac]:sw s3, 96(ra)
Current Store : [0x800002b0] : sw tp, 100(ra) -- Store: [0x80002474]:0x00000001




Last Coverpoint : ['rs1 : x20', 'rs2 : x19', 'rd : x18', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x37 and fm2 == 0x02baf1 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002c4]:fsub.s s2, s4, s3, dyn
	-[0x800002c8]:csrrs tp, fcsr, zero
	-[0x800002cc]:sw s2, 104(ra)
Current Store : [0x800002d0] : sw tp, 108(ra) -- Store: [0x8000247c]:0x00000001




Last Coverpoint : ['rs1 : x16', 'rs2 : x15', 'rd : x17', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x3a and fm2 == 0x2369ae and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002e4]:fsub.s a7, a6, a5, dyn
	-[0x800002e8]:csrrs tp, fcsr, zero
	-[0x800002ec]:sw a7, 112(ra)
Current Store : [0x800002f0] : sw tp, 116(ra) -- Store: [0x80002484]:0x00000001




Last Coverpoint : ['rs1 : x15', 'rs2 : x17', 'rd : x16', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x3d and fm2 == 0x4c4419 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000304]:fsub.s a6, a5, a7, dyn
	-[0x80000308]:csrrs tp, fcsr, zero
	-[0x8000030c]:sw a6, 120(ra)
Current Store : [0x80000310] : sw tp, 124(ra) -- Store: [0x8000248c]:0x00000001




Last Coverpoint : ['rs1 : x17', 'rs2 : x16', 'rd : x15', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x40 and fm2 == 0x7f5520 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000324]:fsub.s a5, a7, a6, dyn
	-[0x80000328]:csrrs tp, fcsr, zero
	-[0x8000032c]:sw a5, 128(ra)
Current Store : [0x80000330] : sw tp, 132(ra) -- Store: [0x80002494]:0x00000001




Last Coverpoint : ['rs1 : x13', 'rs2 : x12', 'rd : x14', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x44 and fm2 == 0x1f9534 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000344]:fsub.s a4, a3, a2, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:sw a4, 136(ra)
Current Store : [0x80000350] : sw tp, 140(ra) -- Store: [0x8000249c]:0x00000001




Last Coverpoint : ['rs1 : x12', 'rs2 : x14', 'rd : x13', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x47 and fm2 == 0x477a81 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000364]:fsub.s a3, a2, a4, dyn
	-[0x80000368]:csrrs tp, fcsr, zero
	-[0x8000036c]:sw a3, 144(ra)
Current Store : [0x80000370] : sw tp, 148(ra) -- Store: [0x800024a4]:0x00000001




Last Coverpoint : ['rs1 : x14', 'rs2 : x13', 'rd : x12', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x4a and fm2 == 0x795921 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000384]:fsub.s a2, a4, a3, dyn
	-[0x80000388]:csrrs tp, fcsr, zero
	-[0x8000038c]:sw a2, 152(ra)
Current Store : [0x80000390] : sw tp, 156(ra) -- Store: [0x800024ac]:0x00000001




Last Coverpoint : ['rs1 : x10', 'rs2 : x9', 'rd : x11', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x4e and fm2 == 0x1bd7b4 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003a4]:fsub.s a1, a0, s1, dyn
	-[0x800003a8]:csrrs tp, fcsr, zero
	-[0x800003ac]:sw a1, 160(ra)
Current Store : [0x800003b0] : sw tp, 164(ra) -- Store: [0x800024b4]:0x00000001




Last Coverpoint : ['rs1 : x9', 'rs2 : x11', 'rd : x10', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x51 and fm2 == 0x42cda2 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003cc]:fsub.s a0, s1, a1, dyn
	-[0x800003d0]:csrrs a3, fcsr, zero
	-[0x800003d4]:sw a0, 168(ra)
Current Store : [0x800003d8] : sw a3, 172(ra) -- Store: [0x800024bc]:0x00000001




Last Coverpoint : ['rs1 : x11', 'rs2 : x10', 'rd : x9', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x54 and fm2 == 0x73810a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003ec]:fsub.s s1, a1, a0, dyn
	-[0x800003f0]:csrrs a3, fcsr, zero
	-[0x800003f4]:sw s1, 176(ra)
Current Store : [0x800003f8] : sw a3, 180(ra) -- Store: [0x800024c4]:0x00000001




Last Coverpoint : ['rs1 : x7', 'rs2 : x6', 'rd : x8', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x58 and fm2 == 0x1830a6 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000040c]:fsub.s fp, t2, t1, dyn
	-[0x80000410]:csrrs a3, fcsr, zero
	-[0x80000414]:sw fp, 184(ra)
Current Store : [0x80000418] : sw a3, 188(ra) -- Store: [0x800024cc]:0x00000001




Last Coverpoint : ['rs1 : x6', 'rs2 : x8', 'rd : x7', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x5b and fm2 == 0x3e3cd0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000434]:fsub.s t2, t1, fp, dyn
	-[0x80000438]:csrrs a3, fcsr, zero
	-[0x8000043c]:sw t2, 0(s1)
Current Store : [0x80000440] : sw a3, 4(s1) -- Store: [0x800024d4]:0x00000001




Last Coverpoint : ['rs1 : x8', 'rs2 : x7', 'rd : x6', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x5e and fm2 == 0x6dcc04 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000454]:fsub.s t1, fp, t2, dyn
	-[0x80000458]:csrrs a3, fcsr, zero
	-[0x8000045c]:sw t1, 8(s1)
Current Store : [0x80000460] : sw a3, 12(s1) -- Store: [0x800024dc]:0x00000001




Last Coverpoint : ['rs1 : x4', 'rs2 : x3', 'rd : x5', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x62 and fm2 == 0x149f82 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000474]:fsub.s t0, tp, gp, dyn
	-[0x80000478]:csrrs a3, fcsr, zero
	-[0x8000047c]:sw t0, 16(s1)
Current Store : [0x80000480] : sw a3, 20(s1) -- Store: [0x800024e4]:0x00000001




Last Coverpoint : ['rs1 : x3', 'rs2 : x5', 'rd : x4', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x65 and fm2 == 0x39c763 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000494]:fsub.s tp, gp, t0, dyn
	-[0x80000498]:csrrs a3, fcsr, zero
	-[0x8000049c]:sw tp, 24(s1)
Current Store : [0x800004a0] : sw a3, 28(s1) -- Store: [0x800024ec]:0x00000001




Last Coverpoint : ['rs1 : x5', 'rs2 : x4', 'rd : x3', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x68 and fm2 == 0x68393c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004b4]:fsub.s gp, t0, tp, dyn
	-[0x800004b8]:csrrs a3, fcsr, zero
	-[0x800004bc]:sw gp, 32(s1)
Current Store : [0x800004c0] : sw a3, 36(s1) -- Store: [0x800024f4]:0x00000001




Last Coverpoint : ['rs1 : x1', 'rs2 : x0', 'rd : x2']
Last Code Sequence : 
	-[0x800004d4]:fsub.s sp, ra, zero, dyn
	-[0x800004d8]:csrrs a3, fcsr, zero
	-[0x800004dc]:sw sp, 40(s1)
Current Store : [0x800004e0] : sw a3, 44(s1) -- Store: [0x800024fc]:0x00000000




Last Coverpoint : ['rs1 : x0', 'rs2 : x2', 'rd : x1']
Last Code Sequence : 
	-[0x800004f4]:fsub.s ra, zero, sp, dyn
	-[0x800004f8]:csrrs a3, fcsr, zero
	-[0x800004fc]:sw ra, 48(s1)
Current Store : [0x80000500] : sw a3, 52(s1) -- Store: [0x80002504]:0x00000000




Last Coverpoint : ['rs1 : x2', 'rs2 : x1', 'rd : x0', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x72 and fm2 == 0x62c7e4 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000514]:fsub.s zero, sp, ra, dyn
	-[0x80000518]:csrrs a3, fcsr, zero
	-[0x8000051c]:sw zero, 56(s1)
Current Store : [0x80000520] : sw a3, 60(s1) -- Store: [0x8000250c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x76 and fm2 == 0x0dbcef and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000534]:fsub.s t6, t5, t4, dyn
	-[0x80000538]:csrrs a3, fcsr, zero
	-[0x8000053c]:sw t6, 64(s1)
Current Store : [0x80000540] : sw a3, 68(s1) -- Store: [0x80002514]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x79 and fm2 == 0x312c2a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000554]:fsub.s t6, t5, t4, dyn
	-[0x80000558]:csrrs a3, fcsr, zero
	-[0x8000055c]:sw t6, 72(s1)
Current Store : [0x80000560] : sw a3, 76(s1) -- Store: [0x8000251c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x7c and fm2 == 0x5d7735 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000574]:fsub.s t6, t5, t4, dyn
	-[0x80000578]:csrrs a3, fcsr, zero
	-[0x8000057c]:sw t6, 80(s1)
Current Store : [0x80000580] : sw a3, 84(s1) -- Store: [0x80002524]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x0a6a81 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000594]:fsub.s t6, t5, t4, dyn
	-[0x80000598]:csrrs a3, fcsr, zero
	-[0x8000059c]:sw t6, 88(s1)
Current Store : [0x800005a0] : sw a3, 92(s1) -- Store: [0x8000252c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x83 and fm2 == 0x2d0521 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005b4]:fsub.s t6, t5, t4, dyn
	-[0x800005b8]:csrrs a3, fcsr, zero
	-[0x800005bc]:sw t6, 96(s1)
Current Store : [0x800005c0] : sw a3, 100(s1) -- Store: [0x80002534]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x86 and fm2 == 0x58466a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005d4]:fsub.s t6, t5, t4, dyn
	-[0x800005d8]:csrrs a3, fcsr, zero
	-[0x800005dc]:sw t6, 104(s1)
Current Store : [0x800005e0] : sw a3, 108(s1) -- Store: [0x8000253c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x8a and fm2 == 0x072c02 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005f4]:fsub.s t6, t5, t4, dyn
	-[0x800005f8]:csrrs a3, fcsr, zero
	-[0x800005fc]:sw t6, 112(s1)
Current Store : [0x80000600] : sw a3, 116(s1) -- Store: [0x80002544]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x8d and fm2 == 0x28f703 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000614]:fsub.s t6, t5, t4, dyn
	-[0x80000618]:csrrs a3, fcsr, zero
	-[0x8000061c]:sw t6, 120(s1)
Current Store : [0x80000620] : sw a3, 124(s1) -- Store: [0x8000254c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x90 and fm2 == 0x5334c3 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000634]:fsub.s t6, t5, t4, dyn
	-[0x80000638]:csrrs a3, fcsr, zero
	-[0x8000063c]:sw t6, 128(s1)
Current Store : [0x80000640] : sw a3, 132(s1) -- Store: [0x80002554]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x94 and fm2 == 0x0400fa and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000654]:fsub.s t6, t5, t4, dyn
	-[0x80000658]:csrrs a3, fcsr, zero
	-[0x8000065c]:sw t6, 136(s1)
Current Store : [0x80000660] : sw a3, 140(s1) -- Store: [0x8000255c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x97 and fm2 == 0x250138 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000674]:fsub.s t6, t5, t4, dyn
	-[0x80000678]:csrrs a3, fcsr, zero
	-[0x8000067c]:sw t6, 144(s1)
Current Store : [0x80000680] : sw a3, 148(s1) -- Store: [0x80002564]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x9a and fm2 == 0x4e4187 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000694]:fsub.s t6, t5, t4, dyn
	-[0x80000698]:csrrs a3, fcsr, zero
	-[0x8000069c]:sw t6, 152(s1)
Current Store : [0x800006a0] : sw a3, 156(s1) -- Store: [0x8000256c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x9e and fm2 == 0x00e8f4 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006b4]:fsub.s t6, t5, t4, dyn
	-[0x800006b8]:csrrs a3, fcsr, zero
	-[0x800006bc]:sw t6, 160(s1)
Current Store : [0x800006c0] : sw a3, 164(s1) -- Store: [0x80002574]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xa1 and fm2 == 0x212331 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006d4]:fsub.s t6, t5, t4, dyn
	-[0x800006d8]:csrrs a3, fcsr, zero
	-[0x800006dc]:sw t6, 168(s1)
Current Store : [0x800006e0] : sw a3, 172(s1) -- Store: [0x8000257c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xa4 and fm2 == 0x496bfe and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006f4]:fsub.s t6, t5, t4, dyn
	-[0x800006f8]:csrrs a3, fcsr, zero
	-[0x800006fc]:sw t6, 176(s1)
Current Store : [0x80000700] : sw a3, 180(s1) -- Store: [0x80002584]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xa7 and fm2 == 0x7bc6fd and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000714]:fsub.s t6, t5, t4, dyn
	-[0x80000718]:csrrs a3, fcsr, zero
	-[0x8000071c]:sw t6, 184(s1)
Current Store : [0x80000720] : sw a3, 188(s1) -- Store: [0x8000258c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xab and fm2 == 0x1d5c5e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000734]:fsub.s t6, t5, t4, dyn
	-[0x80000738]:csrrs a3, fcsr, zero
	-[0x8000073c]:sw t6, 192(s1)
Current Store : [0x80000740] : sw a3, 196(s1) -- Store: [0x80002594]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xae and fm2 == 0x44b376 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000754]:fsub.s t6, t5, t4, dyn
	-[0x80000758]:csrrs a3, fcsr, zero
	-[0x8000075c]:sw t6, 200(s1)
Current Store : [0x80000760] : sw a3, 204(s1) -- Store: [0x8000259c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x75e053 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000774]:fsub.s t6, t5, t4, dyn
	-[0x80000778]:csrrs a3, fcsr, zero
	-[0x8000077c]:sw t6, 208(s1)
Current Store : [0x80000780] : sw a3, 212(s1) -- Store: [0x800025a4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xb5 and fm2 == 0x19ac34 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000794]:fsub.s t6, t5, t4, dyn
	-[0x80000798]:csrrs a3, fcsr, zero
	-[0x8000079c]:sw t6, 216(s1)
Current Store : [0x800007a0] : sw a3, 220(s1) -- Store: [0x800025ac]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xb8 and fm2 == 0x401741 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007b4]:fsub.s t6, t5, t4, dyn
	-[0x800007b8]:csrrs a3, fcsr, zero
	-[0x800007bc]:sw t6, 224(s1)
Current Store : [0x800007c0] : sw a3, 228(s1) -- Store: [0x800025b4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xbb and fm2 == 0x701d11 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007d4]:fsub.s t6, t5, t4, dyn
	-[0x800007d8]:csrrs a3, fcsr, zero
	-[0x800007dc]:sw t6, 232(s1)
Current Store : [0x800007e0] : sw a3, 236(s1) -- Store: [0x800025bc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xbf and fm2 == 0x16122b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007f4]:fsub.s t6, t5, t4, dyn
	-[0x800007f8]:csrrs a3, fcsr, zero
	-[0x800007fc]:sw t6, 240(s1)
Current Store : [0x80000800] : sw a3, 244(s1) -- Store: [0x800025c4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xc2 and fm2 == 0x3b96b5 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000814]:fsub.s t6, t5, t4, dyn
	-[0x80000818]:csrrs a3, fcsr, zero
	-[0x8000081c]:sw t6, 248(s1)
Current Store : [0x80000820] : sw a3, 252(s1) -- Store: [0x800025cc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xc5 and fm2 == 0x6a7c63 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000834]:fsub.s t6, t5, t4, dyn
	-[0x80000838]:csrrs a3, fcsr, zero
	-[0x8000083c]:sw t6, 256(s1)
Current Store : [0x80000840] : sw a3, 260(s1) -- Store: [0x800025d4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xc9 and fm2 == 0x128dbe and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000854]:fsub.s t6, t5, t4, dyn
	-[0x80000858]:csrrs a3, fcsr, zero
	-[0x8000085c]:sw t6, 264(s1)
Current Store : [0x80000860] : sw a3, 268(s1) -- Store: [0x800025dc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xcc and fm2 == 0x37312d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000874]:fsub.s t6, t5, t4, dyn
	-[0x80000878]:csrrs a3, fcsr, zero
	-[0x8000087c]:sw t6, 272(s1)
Current Store : [0x80000880] : sw a3, 276(s1) -- Store: [0x800025e4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xcf and fm2 == 0x64fd78 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000894]:fsub.s t6, t5, t4, dyn
	-[0x80000898]:csrrs a3, fcsr, zero
	-[0x8000089c]:sw t6, 280(s1)
Current Store : [0x800008a0] : sw a3, 284(s1) -- Store: [0x800025ec]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xd3 and fm2 == 0x0f1e6b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008b4]:fsub.s t6, t5, t4, dyn
	-[0x800008b8]:csrrs a3, fcsr, zero
	-[0x800008bc]:sw t6, 288(s1)
Current Store : [0x800008c0] : sw a3, 292(s1) -- Store: [0x800025f4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xd6 and fm2 == 0x32e606 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008d4]:fsub.s t6, t5, t4, dyn
	-[0x800008d8]:csrrs a3, fcsr, zero
	-[0x800008dc]:sw t6, 296(s1)
Current Store : [0x800008e0] : sw a3, 300(s1) -- Store: [0x800025fc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xd9 and fm2 == 0x5f9f88 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008f4]:fsub.s t6, t5, t4, dyn
	-[0x800008f8]:csrrs a3, fcsr, zero
	-[0x800008fc]:sw t6, 304(s1)
Current Store : [0x80000900] : sw a3, 308(s1) -- Store: [0x80002604]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xdd and fm2 == 0x0bc3b5 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000914]:fsub.s t6, t5, t4, dyn
	-[0x80000918]:csrrs a3, fcsr, zero
	-[0x8000091c]:sw t6, 312(s1)
Current Store : [0x80000920] : sw a3, 316(s1) -- Store: [0x8000260c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xe0 and fm2 == 0x2eb4a2 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000934]:fsub.s t6, t5, t4, dyn
	-[0x80000938]:csrrs a3, fcsr, zero
	-[0x8000093c]:sw t6, 320(s1)
Current Store : [0x80000940] : sw a3, 324(s1) -- Store: [0x80002614]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xe3 and fm2 == 0x5a61ca and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000954]:fsub.s t6, t5, t4, dyn
	-[0x80000958]:csrrs a3, fcsr, zero
	-[0x8000095c]:sw t6, 328(s1)
Current Store : [0x80000960] : sw a3, 332(s1) -- Store: [0x8000261c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xe7 and fm2 == 0x087d1e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000974]:fsub.s t6, t5, t4, dyn
	-[0x80000978]:csrrs a3, fcsr, zero
	-[0x8000097c]:sw t6, 336(s1)
Current Store : [0x80000980] : sw a3, 340(s1) -- Store: [0x80002624]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xea and fm2 == 0x2a9c66 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000994]:fsub.s t6, t5, t4, dyn
	-[0x80000998]:csrrs a3, fcsr, zero
	-[0x8000099c]:sw t6, 344(s1)
Current Store : [0x800009a0] : sw a3, 348(s1) -- Store: [0x8000262c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xed and fm2 == 0x554380 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009b4]:fsub.s t6, t5, t4, dyn
	-[0x800009b8]:csrrs a3, fcsr, zero
	-[0x800009bc]:sw t6, 352(s1)
Current Store : [0x800009c0] : sw a3, 356(s1) -- Store: [0x80002634]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xf1 and fm2 == 0x054a30 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009d4]:fsub.s t6, t5, t4, dyn
	-[0x800009d8]:csrrs a3, fcsr, zero
	-[0x800009dc]:sw t6, 360(s1)
Current Store : [0x800009e0] : sw a3, 364(s1) -- Store: [0x8000263c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009f4]:fsub.s t6, t5, t4, dyn
	-[0x800009f8]:csrrs a3, fcsr, zero
	-[0x800009fc]:sw t6, 368(s1)
Current Store : [0x80000a00] : sw a3, 372(s1) -- Store: [0x80002644]:0x00000001




Last Coverpoint : ['mnemonic : fsub.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x178cde and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a14]:fsub.s t6, t5, t4, dyn
	-[0x80000a18]:csrrs a3, fcsr, zero
	-[0x80000a1c]:sw t6, 376(s1)
Current Store : [0x80000a20] : sw a3, 380(s1) -- Store: [0x8000264c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x0fbd4b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a34]:fsub.s t6, t5, t4, dyn
	-[0x80000a38]:csrrs a3, fcsr, zero
	-[0x80000a3c]:sw t6, 384(s1)
Current Store : [0x80000a40] : sw a3, 388(s1) -- Store: [0x80002654]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x12 and fm2 == 0x33ac9e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a54]:fsub.s t6, t5, t4, dyn
	-[0x80000a58]:csrrs a3, fcsr, zero
	-[0x80000a5c]:sw t6, 392(s1)
Current Store : [0x80000a60] : sw a3, 396(s1) -- Store: [0x8000265c]:0x00000001




Last Coverpoint : ['mnemonic : fsub.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x6097c5 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a74]:fsub.s t6, t5, t4, dyn
	-[0x80000a78]:csrrs a3, fcsr, zero
	-[0x80000a7c]:sw t6, 400(s1)
Current Store : [0x80000a80] : sw a3, 404(s1) -- Store: [0x80002664]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x6c and fm2 == 0x1123c5 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a94]:fsub.s t6, t5, t4, dyn
	-[0x80000a98]:csrrs a3, fcsr, zero
	-[0x80000a9c]:sw t6, 408(s1)
Current Store : [0x80000aa0] : sw a3, 412(s1) -- Store: [0x8000266c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x6f and fm2 == 0x356cb7 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ab4]:fsub.s t6, t5, t4, dyn
	-[0x80000ab8]:csrrs a3, fcsr, zero
	-[0x80000abc]:sw t6, 416(s1)
Current Store : [0x80000ac0] : sw a3, 420(s1) -- Store: [0x80002674]:0x00000001




Last Coverpoint : ['mnemonic : fsub.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x72 and fm2 == 0x62c7e4 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ad4]:fsub.s t6, t5, t4, dyn
	-[0x80000ad8]:csrrs a3, fcsr, zero
	-[0x80000adc]:sw t6, 424(s1)
Current Store : [0x80000ae0] : sw a3, 428(s1) -- Store: [0x8000267c]:0x00000001





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                                coverpoints                                                                                                                 |                                                      code                                                       |
|---:|--------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|
|   1|[0x80002410]<br>0x78D35630|- mnemonic : fsub.s<br> - rs1 : x31<br> - rs2 : x30<br> - rd : x31<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x178cde and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x80000124]:fsub.s t6, t6, t5, dyn<br> [0x80000128]:csrrs tp, fcsr, zero<br> [0x8000012c]:sw t6, 0(ra)<br>      |
|   2|[0x80002418]<br>0x00000000|- rs1 : x29<br> - rs2 : x29<br> - rd : x29<br> - rs1 == rs2 == rd<br>                                                                                                                                                                       |[0x80000144]:fsub.s t4, t4, t4, dyn<br> [0x80000148]:csrrs tp, fcsr, zero<br> [0x8000014c]:sw t4, 8(ra)<br>      |
|   3|[0x80002420]<br>0x00000000|- rs1 : x28<br> - rs2 : x28<br> - rd : x30<br> - rs1 == rs2 != rd<br>                                                                                                                                                                       |[0x80000164]:fsub.s t5, t3, t3, dyn<br> [0x80000168]:csrrs tp, fcsr, zero<br> [0x8000016c]:sw t5, 16(ra)<br>     |
|   4|[0x80002428]<br>0x78D35630|- rs1 : x30<br> - rs2 : x27<br> - rd : x27<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x6097c5 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                         |[0x80000184]:fsub.s s11, t5, s11, dyn<br> [0x80000188]:csrrs tp, fcsr, zero<br> [0x8000018c]:sw s11, 24(ra)<br>  |
|   5|[0x80002430]<br>0x78D35630|- rs1 : x27<br> - rs2 : x31<br> - rd : x28<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x19 and fm2 == 0x0c5edb and  fcsr == 0x0 and rm_val == 7   #nosat<br>  |[0x800001a4]:fsub.s t3, s11, t6, dyn<br> [0x800001a8]:csrrs tp, fcsr, zero<br> [0x800001ac]:sw t3, 32(ra)<br>    |
|   6|[0x80002438]<br>0x78D35630|- rs1 : x25<br> - rs2 : x24<br> - rd : x26<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x1c and fm2 == 0x2f7692 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x800001c4]:fsub.s s10, s9, s8, dyn<br> [0x800001c8]:csrrs tp, fcsr, zero<br> [0x800001cc]:sw s10, 40(ra)<br>   |
|   7|[0x80002440]<br>0x78D35630|- rs1 : x24<br> - rs2 : x26<br> - rd : x25<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x5b5437 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x800001e4]:fsub.s s9, s8, s10, dyn<br> [0x800001e8]:csrrs tp, fcsr, zero<br> [0x800001ec]:sw s9, 48(ra)<br>    |
|   8|[0x80002448]<br>0x78D35630|- rs1 : x26<br> - rs2 : x25<br> - rd : x24<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x23 and fm2 == 0x0914a2 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000204]:fsub.s s8, s10, s9, dyn<br> [0x80000208]:csrrs tp, fcsr, zero<br> [0x8000020c]:sw s8, 56(ra)<br>    |
|   9|[0x80002450]<br>0x78D35630|- rs1 : x22<br> - rs2 : x21<br> - rd : x23<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x26 and fm2 == 0x2b59cb and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000224]:fsub.s s7, s6, s5, dyn<br> [0x80000228]:csrrs tp, fcsr, zero<br> [0x8000022c]:sw s7, 64(ra)<br>     |
|  10|[0x80002458]<br>0x78D35630|- rs1 : x21<br> - rs2 : x23<br> - rd : x22<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x29 and fm2 == 0x56303d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000244]:fsub.s s6, s5, s7, dyn<br> [0x80000248]:csrrs tp, fcsr, zero<br> [0x8000024c]:sw s6, 72(ra)<br>     |
|  11|[0x80002460]<br>0x78D35630|- rs1 : x23<br> - rs2 : x22<br> - rd : x21<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x2d and fm2 == 0x05de26 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000264]:fsub.s s5, s7, s6, dyn<br> [0x80000268]:csrrs tp, fcsr, zero<br> [0x8000026c]:sw s5, 80(ra)<br>     |
|  12|[0x80002468]<br>0x78D35630|- rs1 : x19<br> - rs2 : x18<br> - rd : x20<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x30 and fm2 == 0x2755b0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000284]:fsub.s s4, s3, s2, dyn<br> [0x80000288]:csrrs tp, fcsr, zero<br> [0x8000028c]:sw s4, 88(ra)<br>     |
|  13|[0x80002470]<br>0x78D35630|- rs1 : x18<br> - rs2 : x20<br> - rd : x19<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x33 and fm2 == 0x512b1c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x800002a4]:fsub.s s3, s2, s4, dyn<br> [0x800002a8]:csrrs tp, fcsr, zero<br> [0x800002ac]:sw s3, 96(ra)<br>     |
|  14|[0x80002478]<br>0x78D35630|- rs1 : x20<br> - rs2 : x19<br> - rd : x18<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x37 and fm2 == 0x02baf1 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x800002c4]:fsub.s s2, s4, s3, dyn<br> [0x800002c8]:csrrs tp, fcsr, zero<br> [0x800002cc]:sw s2, 104(ra)<br>    |
|  15|[0x80002480]<br>0x78D35630|- rs1 : x16<br> - rs2 : x15<br> - rd : x17<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x3a and fm2 == 0x2369ae and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x800002e4]:fsub.s a7, a6, a5, dyn<br> [0x800002e8]:csrrs tp, fcsr, zero<br> [0x800002ec]:sw a7, 112(ra)<br>    |
|  16|[0x80002488]<br>0x78D35630|- rs1 : x15<br> - rs2 : x17<br> - rd : x16<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x3d and fm2 == 0x4c4419 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000304]:fsub.s a6, a5, a7, dyn<br> [0x80000308]:csrrs tp, fcsr, zero<br> [0x8000030c]:sw a6, 120(ra)<br>    |
|  17|[0x80002490]<br>0x78D35630|- rs1 : x17<br> - rs2 : x16<br> - rd : x15<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x40 and fm2 == 0x7f5520 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000324]:fsub.s a5, a7, a6, dyn<br> [0x80000328]:csrrs tp, fcsr, zero<br> [0x8000032c]:sw a5, 128(ra)<br>    |
|  18|[0x80002498]<br>0x78D35630|- rs1 : x13<br> - rs2 : x12<br> - rd : x14<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x44 and fm2 == 0x1f9534 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000344]:fsub.s a4, a3, a2, dyn<br> [0x80000348]:csrrs tp, fcsr, zero<br> [0x8000034c]:sw a4, 136(ra)<br>    |
|  19|[0x800024a0]<br>0x78D35630|- rs1 : x12<br> - rs2 : x14<br> - rd : x13<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x47 and fm2 == 0x477a81 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000364]:fsub.s a3, a2, a4, dyn<br> [0x80000368]:csrrs tp, fcsr, zero<br> [0x8000036c]:sw a3, 144(ra)<br>    |
|  20|[0x800024a8]<br>0x78D35630|- rs1 : x14<br> - rs2 : x13<br> - rd : x12<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x4a and fm2 == 0x795921 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000384]:fsub.s a2, a4, a3, dyn<br> [0x80000388]:csrrs tp, fcsr, zero<br> [0x8000038c]:sw a2, 152(ra)<br>    |
|  21|[0x800024b0]<br>0x78D35630|- rs1 : x10<br> - rs2 : x9<br> - rd : x11<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x4e and fm2 == 0x1bd7b4 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                 |[0x800003a4]:fsub.s a1, a0, s1, dyn<br> [0x800003a8]:csrrs tp, fcsr, zero<br> [0x800003ac]:sw a1, 160(ra)<br>    |
|  22|[0x800024b8]<br>0x78D35630|- rs1 : x9<br> - rs2 : x11<br> - rd : x10<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x51 and fm2 == 0x42cda2 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                 |[0x800003cc]:fsub.s a0, s1, a1, dyn<br> [0x800003d0]:csrrs a3, fcsr, zero<br> [0x800003d4]:sw a0, 168(ra)<br>    |
|  23|[0x800024c0]<br>0x78D35630|- rs1 : x11<br> - rs2 : x10<br> - rd : x9<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x54 and fm2 == 0x73810a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                 |[0x800003ec]:fsub.s s1, a1, a0, dyn<br> [0x800003f0]:csrrs a3, fcsr, zero<br> [0x800003f4]:sw s1, 176(ra)<br>    |
|  24|[0x800024c8]<br>0x78D35630|- rs1 : x7<br> - rs2 : x6<br> - rd : x8<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x58 and fm2 == 0x1830a6 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                   |[0x8000040c]:fsub.s fp, t2, t1, dyn<br> [0x80000410]:csrrs a3, fcsr, zero<br> [0x80000414]:sw fp, 184(ra)<br>    |
|  25|[0x800024d0]<br>0x78D35630|- rs1 : x6<br> - rs2 : x8<br> - rd : x7<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x5b and fm2 == 0x3e3cd0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                   |[0x80000434]:fsub.s t2, t1, fp, dyn<br> [0x80000438]:csrrs a3, fcsr, zero<br> [0x8000043c]:sw t2, 0(s1)<br>      |
|  26|[0x800024d8]<br>0x78D35630|- rs1 : x8<br> - rs2 : x7<br> - rd : x6<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x5e and fm2 == 0x6dcc04 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                   |[0x80000454]:fsub.s t1, fp, t2, dyn<br> [0x80000458]:csrrs a3, fcsr, zero<br> [0x8000045c]:sw t1, 8(s1)<br>      |
|  27|[0x800024e0]<br>0x78D35630|- rs1 : x4<br> - rs2 : x3<br> - rd : x5<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x62 and fm2 == 0x149f82 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                   |[0x80000474]:fsub.s t0, tp, gp, dyn<br> [0x80000478]:csrrs a3, fcsr, zero<br> [0x8000047c]:sw t0, 16(s1)<br>     |
|  28|[0x800024e8]<br>0x78D35630|- rs1 : x3<br> - rs2 : x5<br> - rd : x4<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x65 and fm2 == 0x39c763 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                   |[0x80000494]:fsub.s tp, gp, t0, dyn<br> [0x80000498]:csrrs a3, fcsr, zero<br> [0x8000049c]:sw tp, 24(s1)<br>     |
|  29|[0x800024f0]<br>0x78D35630|- rs1 : x5<br> - rs2 : x4<br> - rd : x3<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x68 and fm2 == 0x68393c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                   |[0x800004b4]:fsub.s gp, t0, tp, dyn<br> [0x800004b8]:csrrs a3, fcsr, zero<br> [0x800004bc]:sw gp, 32(s1)<br>     |
|  30|[0x800024f8]<br>0x78D35630|- rs1 : x1<br> - rs2 : x0<br> - rd : x2<br>                                                                                                                                                                                                 |[0x800004d4]:fsub.s sp, ra, zero, dyn<br> [0x800004d8]:csrrs a3, fcsr, zero<br> [0x800004dc]:sw sp, 40(s1)<br>   |
|  31|[0x80002500]<br>0xB7B56CB7|- rs1 : x0<br> - rs2 : x2<br> - rd : x1<br>                                                                                                                                                                                                 |[0x800004f4]:fsub.s ra, zero, sp, dyn<br> [0x800004f8]:csrrs a3, fcsr, zero<br> [0x800004fc]:sw ra, 48(s1)<br>   |
|  32|[0x80002508]<br>0x00000000|- rs1 : x2<br> - rs2 : x1<br> - rd : x0<br> - fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x72 and fm2 == 0x62c7e4 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                   |[0x80000514]:fsub.s zero, sp, ra, dyn<br> [0x80000518]:csrrs a3, fcsr, zero<br> [0x8000051c]:sw zero, 56(s1)<br> |
|  33|[0x80002510]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x76 and fm2 == 0x0dbcef and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000534]:fsub.s t6, t5, t4, dyn<br> [0x80000538]:csrrs a3, fcsr, zero<br> [0x8000053c]:sw t6, 64(s1)<br>     |
|  34|[0x80002518]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x79 and fm2 == 0x312c2a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000554]:fsub.s t6, t5, t4, dyn<br> [0x80000558]:csrrs a3, fcsr, zero<br> [0x8000055c]:sw t6, 72(s1)<br>     |
|  35|[0x80002520]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x7c and fm2 == 0x5d7735 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000574]:fsub.s t6, t5, t4, dyn<br> [0x80000578]:csrrs a3, fcsr, zero<br> [0x8000057c]:sw t6, 80(s1)<br>     |
|  36|[0x80002528]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x0a6a81 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000594]:fsub.s t6, t5, t4, dyn<br> [0x80000598]:csrrs a3, fcsr, zero<br> [0x8000059c]:sw t6, 88(s1)<br>     |
|  37|[0x80002530]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x83 and fm2 == 0x2d0521 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800005b4]:fsub.s t6, t5, t4, dyn<br> [0x800005b8]:csrrs a3, fcsr, zero<br> [0x800005bc]:sw t6, 96(s1)<br>     |
|  38|[0x80002538]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x86 and fm2 == 0x58466a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800005d4]:fsub.s t6, t5, t4, dyn<br> [0x800005d8]:csrrs a3, fcsr, zero<br> [0x800005dc]:sw t6, 104(s1)<br>    |
|  39|[0x80002540]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x8a and fm2 == 0x072c02 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800005f4]:fsub.s t6, t5, t4, dyn<br> [0x800005f8]:csrrs a3, fcsr, zero<br> [0x800005fc]:sw t6, 112(s1)<br>    |
|  40|[0x80002548]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x8d and fm2 == 0x28f703 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000614]:fsub.s t6, t5, t4, dyn<br> [0x80000618]:csrrs a3, fcsr, zero<br> [0x8000061c]:sw t6, 120(s1)<br>    |
|  41|[0x80002550]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x90 and fm2 == 0x5334c3 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000634]:fsub.s t6, t5, t4, dyn<br> [0x80000638]:csrrs a3, fcsr, zero<br> [0x8000063c]:sw t6, 128(s1)<br>    |
|  42|[0x80002558]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x94 and fm2 == 0x0400fa and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000654]:fsub.s t6, t5, t4, dyn<br> [0x80000658]:csrrs a3, fcsr, zero<br> [0x8000065c]:sw t6, 136(s1)<br>    |
|  43|[0x80002560]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x97 and fm2 == 0x250138 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000674]:fsub.s t6, t5, t4, dyn<br> [0x80000678]:csrrs a3, fcsr, zero<br> [0x8000067c]:sw t6, 144(s1)<br>    |
|  44|[0x80002568]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x9a and fm2 == 0x4e4187 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000694]:fsub.s t6, t5, t4, dyn<br> [0x80000698]:csrrs a3, fcsr, zero<br> [0x8000069c]:sw t6, 152(s1)<br>    |
|  45|[0x80002570]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x9e and fm2 == 0x00e8f4 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800006b4]:fsub.s t6, t5, t4, dyn<br> [0x800006b8]:csrrs a3, fcsr, zero<br> [0x800006bc]:sw t6, 160(s1)<br>    |
|  46|[0x80002578]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xa1 and fm2 == 0x212331 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800006d4]:fsub.s t6, t5, t4, dyn<br> [0x800006d8]:csrrs a3, fcsr, zero<br> [0x800006dc]:sw t6, 168(s1)<br>    |
|  47|[0x80002580]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xa4 and fm2 == 0x496bfe and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800006f4]:fsub.s t6, t5, t4, dyn<br> [0x800006f8]:csrrs a3, fcsr, zero<br> [0x800006fc]:sw t6, 176(s1)<br>    |
|  48|[0x80002588]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xa7 and fm2 == 0x7bc6fd and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000714]:fsub.s t6, t5, t4, dyn<br> [0x80000718]:csrrs a3, fcsr, zero<br> [0x8000071c]:sw t6, 184(s1)<br>    |
|  49|[0x80002590]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xab and fm2 == 0x1d5c5e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000734]:fsub.s t6, t5, t4, dyn<br> [0x80000738]:csrrs a3, fcsr, zero<br> [0x8000073c]:sw t6, 192(s1)<br>    |
|  50|[0x80002598]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xae and fm2 == 0x44b376 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000754]:fsub.s t6, t5, t4, dyn<br> [0x80000758]:csrrs a3, fcsr, zero<br> [0x8000075c]:sw t6, 200(s1)<br>    |
|  51|[0x800025a0]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x75e053 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000774]:fsub.s t6, t5, t4, dyn<br> [0x80000778]:csrrs a3, fcsr, zero<br> [0x8000077c]:sw t6, 208(s1)<br>    |
|  52|[0x800025a8]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xb5 and fm2 == 0x19ac34 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000794]:fsub.s t6, t5, t4, dyn<br> [0x80000798]:csrrs a3, fcsr, zero<br> [0x8000079c]:sw t6, 216(s1)<br>    |
|  53|[0x800025b0]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xb8 and fm2 == 0x401741 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800007b4]:fsub.s t6, t5, t4, dyn<br> [0x800007b8]:csrrs a3, fcsr, zero<br> [0x800007bc]:sw t6, 224(s1)<br>    |
|  54|[0x800025b8]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xbb and fm2 == 0x701d11 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800007d4]:fsub.s t6, t5, t4, dyn<br> [0x800007d8]:csrrs a3, fcsr, zero<br> [0x800007dc]:sw t6, 232(s1)<br>    |
|  55|[0x800025c0]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xbf and fm2 == 0x16122b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800007f4]:fsub.s t6, t5, t4, dyn<br> [0x800007f8]:csrrs a3, fcsr, zero<br> [0x800007fc]:sw t6, 240(s1)<br>    |
|  56|[0x800025c8]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xc2 and fm2 == 0x3b96b5 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000814]:fsub.s t6, t5, t4, dyn<br> [0x80000818]:csrrs a3, fcsr, zero<br> [0x8000081c]:sw t6, 248(s1)<br>    |
|  57|[0x800025d0]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xc5 and fm2 == 0x6a7c63 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000834]:fsub.s t6, t5, t4, dyn<br> [0x80000838]:csrrs a3, fcsr, zero<br> [0x8000083c]:sw t6, 256(s1)<br>    |
|  58|[0x800025d8]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xc9 and fm2 == 0x128dbe and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000854]:fsub.s t6, t5, t4, dyn<br> [0x80000858]:csrrs a3, fcsr, zero<br> [0x8000085c]:sw t6, 264(s1)<br>    |
|  59|[0x800025e0]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xcc and fm2 == 0x37312d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000874]:fsub.s t6, t5, t4, dyn<br> [0x80000878]:csrrs a3, fcsr, zero<br> [0x8000087c]:sw t6, 272(s1)<br>    |
|  60|[0x800025e8]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xcf and fm2 == 0x64fd78 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000894]:fsub.s t6, t5, t4, dyn<br> [0x80000898]:csrrs a3, fcsr, zero<br> [0x8000089c]:sw t6, 280(s1)<br>    |
|  61|[0x800025f0]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xd3 and fm2 == 0x0f1e6b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800008b4]:fsub.s t6, t5, t4, dyn<br> [0x800008b8]:csrrs a3, fcsr, zero<br> [0x800008bc]:sw t6, 288(s1)<br>    |
|  62|[0x800025f8]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xd6 and fm2 == 0x32e606 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800008d4]:fsub.s t6, t5, t4, dyn<br> [0x800008d8]:csrrs a3, fcsr, zero<br> [0x800008dc]:sw t6, 296(s1)<br>    |
|  63|[0x80002600]<br>0x78D3562F|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xd9 and fm2 == 0x5f9f88 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800008f4]:fsub.s t6, t5, t4, dyn<br> [0x800008f8]:csrrs a3, fcsr, zero<br> [0x800008fc]:sw t6, 304(s1)<br>    |
|  64|[0x80002608]<br>0x78D35627|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xdd and fm2 == 0x0bc3b5 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000914]:fsub.s t6, t5, t4, dyn<br> [0x80000918]:csrrs a3, fcsr, zero<br> [0x8000091c]:sw t6, 312(s1)<br>    |
|  65|[0x80002610]<br>0x78D355D9|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xe0 and fm2 == 0x2eb4a2 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000934]:fsub.s t6, t5, t4, dyn<br> [0x80000938]:csrrs a3, fcsr, zero<br> [0x8000093c]:sw t6, 320(s1)<br>    |
|  66|[0x80002618]<br>0x78D352C6|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xe3 and fm2 == 0x5a61ca and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000954]:fsub.s t6, t5, t4, dyn<br> [0x80000958]:csrrs a3, fcsr, zero<br> [0x8000095c]:sw t6, 328(s1)<br>    |
|  67|[0x80002620]<br>0x78D33411|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xe7 and fm2 == 0x087d1e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000974]:fsub.s t6, t5, t4, dyn<br> [0x80000978]:csrrs a3, fcsr, zero<br> [0x8000097c]:sw t6, 336(s1)<br>    |
|  68|[0x80002628]<br>0x78D200F7|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xea and fm2 == 0x2a9c66 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000994]:fsub.s t6, t5, t4, dyn<br> [0x80000998]:csrrs a3, fcsr, zero<br> [0x8000099c]:sw t6, 344(s1)<br>    |
|  69|[0x80002630]<br>0x78C601F8|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xed and fm2 == 0x554380 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800009b4]:fsub.s t6, t5, t4, dyn<br> [0x800009b8]:csrrs a3, fcsr, zero<br> [0x800009bc]:sw t6, 352(s1)<br>    |
|  70|[0x80002638]<br>0x781C1800|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xf1 and fm2 == 0x054a30 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800009d4]:fsub.s t6, t5, t4, dyn<br> [0x800009d8]:csrrs a3, fcsr, zero<br> [0x800009dc]:sw t6, 360(s1)<br>    |
|  71|[0x80002640]<br>0xFF7FF964|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800009f4]:fsub.s t6, t5, t4, dyn<br> [0x800009f8]:csrrs a3, fcsr, zero<br> [0x800009fc]:sw t6, 368(s1)<br>    |
|  72|[0x80002650]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x0fbd4b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000a34]:fsub.s t6, t5, t4, dyn<br> [0x80000a38]:csrrs a3, fcsr, zero<br> [0x80000a3c]:sw t6, 384(s1)<br>    |
|  73|[0x80002658]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x12 and fm2 == 0x33ac9e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000a54]:fsub.s t6, t5, t4, dyn<br> [0x80000a58]:csrrs a3, fcsr, zero<br> [0x80000a5c]:sw t6, 392(s1)<br>    |
|  74|[0x80002668]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x6c and fm2 == 0x1123c5 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000a94]:fsub.s t6, t5, t4, dyn<br> [0x80000a98]:csrrs a3, fcsr, zero<br> [0x80000a9c]:sw t6, 408(s1)<br>    |
|  75|[0x80002670]<br>0x78D35630|- fs1 == 0 and fe1 == 0xf1 and fm1 == 0x535630 and fs2 == 0 and fe2 == 0x6f and fm2 == 0x356cb7 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000ab4]:fsub.s t6, t5, t4, dyn<br> [0x80000ab8]:csrrs a3, fcsr, zero<br> [0x80000abc]:sw t6, 416(s1)<br>    |
