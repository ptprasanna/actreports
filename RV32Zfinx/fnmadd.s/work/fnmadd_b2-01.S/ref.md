
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80003cc0')]      |
| SIG_REGION                | [('0x80005f10', '0x80006830', '584 words')]      |
| COV_LABELS                | fnmadd_b2      |
| TEST_NAME                 | /home/reg/work/zfinx/RV32Zfinx/fnmadd.s/work/fnmadd_b2-01.S/ref.S    |
| Total Number of coverpoints| 417     |
| Total Coverpoints Hit     | 417      |
| Total Signature Updates   | 582      |
| STAT1                     | 286      |
| STAT2                     | 5      |
| STAT3                     | 0     |
| STAT4                     | 291     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80003810]:fnmadd.s t6, t5, t4, t3, dyn
      [0x80003814]:csrrs a4, fcsr, zero
      [0x80003818]:sw t6, 1000(a1)
 -- Signature Address: 0x800067b0 Data: 0x8000003F
 -- Redundant Coverpoints hit by the op
      - mnemonic : fnmadd.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs3 : x28
      - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd
      - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x2dc2d2 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80003914]:fnmadd.s t6, t5, t4, t3, dyn
      [0x80003918]:csrrs a4, fcsr, zero
      [0x8000391c]:sw t6, 0(a1)
 -- Signature Address: 0x800067c8 Data: 0x80000029
 -- Redundant Coverpoints hit by the op
      - mnemonic : fnmadd.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs3 : x28
      - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd
      - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x84 and fm2 == 0x0c213e and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000029 and  fcsr == 0x0 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80003b0c]:fnmadd.s t6, t5, t4, t3, dyn
      [0x80003b10]:csrrs a4, fcsr, zero
      [0x80003b14]:sw t6, 48(a1)
 -- Signature Address: 0x800067f8 Data: 0x80000037
 -- Redundant Coverpoints hit by the op
      - mnemonic : fnmadd.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs3 : x28
      - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd
      - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x89 and fm2 == 0x29bb76 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000037 and  fcsr == 0x0 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80003c08]:fnmadd.s t6, t5, t4, t3, dyn
      [0x80003c0c]:csrrs a4, fcsr, zero
      [0x80003c10]:sw t6, 72(a1)
 -- Signature Address: 0x80006810 Data: 0x8000005A
 -- Redundant Coverpoints hit by the op
      - mnemonic : fnmadd.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs3 : x28
      - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd
      - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x87 and fm2 == 0x726cb5 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005a and  fcsr == 0x0 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80003cb0]:fnmadd.s t6, t5, t4, t3, dyn
      [0x80003cb4]:csrrs a4, fcsr, zero
      [0x80003cb8]:sw t6, 88(a1)
 -- Signature Address: 0x80006820 Data: 0x80000026
 -- Redundant Coverpoints hit by the op
      - mnemonic : fnmadd.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs3 : x28
      - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd
      - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8d and fm2 == 0x33c83f and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000026 and  fcsr == 0x0 and rm_val == 7   #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fnmadd.s', 'rs1 : x31', 'rs2 : x30', 'rd : x31', 'rs3 : x29', 'rs1 == rd != rs2 and rs1 == rd != rs3 and rs3 != rs2', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x2dc2d2 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000128]:fnmadd.s t6, t6, t5, t4, dyn
	-[0x8000012c]:csrrs tp, fcsr, zero
	-[0x80000130]:sw t6, 0(ra)
Current Store : [0x80000134] : sw tp, 4(ra) -- Store: [0x80005f14]:0x00000000




Last Coverpoint : ['rs1 : x29', 'rs2 : x29', 'rd : x30', 'rs3 : x31', 'rs1 == rs2 != rs3 and rs1 == rs2 != rd and rd != rs3']
Last Code Sequence : 
	-[0x8000014c]:fnmadd.s t5, t4, t4, t6, dyn
	-[0x80000150]:csrrs tp, fcsr, zero
	-[0x80000154]:sw t5, 8(ra)
Current Store : [0x80000158] : sw tp, 12(ra) -- Store: [0x80005f1c]:0x00000000




Last Coverpoint : ['rs1 : x30', 'rs2 : x31', 'rd : x29', 'rs3 : x28', 'rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x83 and fm2 == 0x2827e4 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000013 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000170]:fnmadd.s t4, t5, t6, t3, dyn
	-[0x80000174]:csrrs tp, fcsr, zero
	-[0x80000178]:sw t4, 16(ra)
Current Store : [0x8000017c] : sw tp, 20(ra) -- Store: [0x80005f24]:0x00000000




Last Coverpoint : ['rs1 : x27', 'rs2 : x28', 'rd : x27', 'rs3 : x27', 'rs1 == rd == rs3 != rs2']
Last Code Sequence : 
	-[0x80000194]:fnmadd.s s11, s11, t3, s11, dyn
	-[0x80000198]:csrrs tp, fcsr, zero
	-[0x8000019c]:sw s11, 24(ra)
Current Store : [0x800001a0] : sw tp, 28(ra) -- Store: [0x80005f2c]:0x00000000




Last Coverpoint : ['rs1 : x28', 'rs2 : x26', 'rd : x26', 'rs3 : x30', 'rs2 == rd != rs1 and rs2 == rd != rs3 and rs3 != rs1', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x84 and fm2 == 0x0c213e and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000029 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001b8]:fnmadd.s s10, t3, s10, t5, dyn
	-[0x800001bc]:csrrs tp, fcsr, zero
	-[0x800001c0]:sw s10, 32(ra)
Current Store : [0x800001c4] : sw tp, 36(ra) -- Store: [0x80005f34]:0x00000000




Last Coverpoint : ['rs1 : x25', 'rs2 : x25', 'rd : x25', 'rs3 : x25', 'rs1 == rs2 == rs3 == rd']
Last Code Sequence : 
	-[0x800001dc]:fnmadd.s s9, s9, s9, s9, dyn
	-[0x800001e0]:csrrs tp, fcsr, zero
	-[0x800001e4]:sw s9, 40(ra)
Current Store : [0x800001e8] : sw tp, 44(ra) -- Store: [0x80005f3c]:0x00000000




Last Coverpoint : ['rs1 : x26', 'rs2 : x27', 'rd : x28', 'rs3 : x26', 'rs1 == rs3 != rs2 and rs1 == rs3 != rd and rd != rs2']
Last Code Sequence : 
	-[0x80000200]:fnmadd.s t3, s10, s11, s10, dyn
	-[0x80000204]:csrrs tp, fcsr, zero
	-[0x80000208]:sw t3, 48(ra)
Current Store : [0x8000020c] : sw tp, 52(ra) -- Store: [0x80005f44]:0x00000000




Last Coverpoint : ['rs1 : x23', 'rs2 : x22', 'rd : x24', 'rs3 : x22', 'rs2 == rs3 != rs1 and rs2 == rs3 != rd and rd != rs1']
Last Code Sequence : 
	-[0x80000224]:fnmadd.s s8, s7, s6, s6, dyn
	-[0x80000228]:csrrs tp, fcsr, zero
	-[0x8000022c]:sw s8, 56(ra)
Current Store : [0x80000230] : sw tp, 60(ra) -- Store: [0x80005f4c]:0x00000000




Last Coverpoint : ['rs1 : x24', 'rs2 : x23', 'rd : x23', 'rs3 : x23', 'rd == rs2 == rs3 != rs1']
Last Code Sequence : 
	-[0x80000248]:fnmadd.s s7, s8, s7, s7, dyn
	-[0x8000024c]:csrrs tp, fcsr, zero
	-[0x80000250]:sw s7, 64(ra)
Current Store : [0x80000254] : sw tp, 68(ra) -- Store: [0x80005f54]:0x00000000




Last Coverpoint : ['rs1 : x21', 'rs2 : x21', 'rd : x21', 'rs3 : x24', 'rs1 == rs2 == rd != rs3']
Last Code Sequence : 
	-[0x8000026c]:fnmadd.s s5, s5, s5, s8, dyn
	-[0x80000270]:csrrs tp, fcsr, zero
	-[0x80000274]:sw s5, 72(ra)
Current Store : [0x80000278] : sw tp, 76(ra) -- Store: [0x80005f5c]:0x00000000




Last Coverpoint : ['rs1 : x22', 'rs2 : x24', 'rd : x20', 'rs3 : x20', 'rs3 == rd != rs1 and rs3 == rd != rs2 and rs2 != rs1', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x89 and fm2 == 0x29bb76 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000037 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000290]:fnmadd.s s4, s6, s8, s4, dyn
	-[0x80000294]:csrrs tp, fcsr, zero
	-[0x80000298]:sw s4, 80(ra)
Current Store : [0x8000029c] : sw tp, 84(ra) -- Store: [0x80005f64]:0x00000000




Last Coverpoint : ['rs1 : x19', 'rs2 : x19', 'rd : x22', 'rs3 : x19', 'rs1 == rs2 == rs3 != rd']
Last Code Sequence : 
	-[0x800002b4]:fnmadd.s s6, s3, s3, s3, dyn
	-[0x800002b8]:csrrs tp, fcsr, zero
	-[0x800002bc]:sw s6, 88(ra)
Current Store : [0x800002c0] : sw tp, 92(ra) -- Store: [0x80005f6c]:0x00000000




Last Coverpoint : ['rs1 : x20', 'rs2 : x18', 'rd : x19', 'rs3 : x21', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8b and fm2 == 0x310b99 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000035 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002d8]:fnmadd.s s3, s4, s2, s5, dyn
	-[0x800002dc]:csrrs tp, fcsr, zero
	-[0x800002e0]:sw s3, 96(ra)
Current Store : [0x800002e4] : sw tp, 100(ra) -- Store: [0x80005f74]:0x00000000




Last Coverpoint : ['rs1 : x17', 'rs2 : x20', 'rd : x18', 'rs3 : x16', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8c and fm2 == 0x321e41 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000039 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002fc]:fnmadd.s s2, a7, s4, a6, dyn
	-[0x80000300]:csrrs tp, fcsr, zero
	-[0x80000304]:sw s2, 104(ra)
Current Store : [0x80000308] : sw tp, 108(ra) -- Store: [0x80005f7c]:0x00000000




Last Coverpoint : ['rs1 : x18', 'rs2 : x16', 'rd : x17', 'rs3 : x15', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8d and fm2 == 0x324de5 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000061 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000320]:fnmadd.s a7, s2, a6, a5, dyn
	-[0x80000324]:csrrs tp, fcsr, zero
	-[0x80000328]:sw a7, 112(ra)
Current Store : [0x8000032c] : sw tp, 116(ra) -- Store: [0x80005f84]:0x00000000




Last Coverpoint : ['rs1 : x15', 'rs2 : x17', 'rd : x16', 'rs3 : x18', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8e and fm2 == 0x32dfa2 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000344]:fnmadd.s a6, a5, a7, s2, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:sw a6, 120(ra)
Current Store : [0x80000350] : sw tp, 124(ra) -- Store: [0x80005f8c]:0x00000000




Last Coverpoint : ['rs1 : x16', 'rs2 : x14', 'rd : x15', 'rs3 : x17', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8f and fm2 == 0x3354a4 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00000d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000368]:fnmadd.s a5, a6, a4, a7, dyn
	-[0x8000036c]:csrrs tp, fcsr, zero
	-[0x80000370]:sw a5, 128(ra)
Current Store : [0x80000374] : sw tp, 132(ra) -- Store: [0x80005f94]:0x00000000




Last Coverpoint : ['rs1 : x13', 'rs2 : x15', 'rd : x14', 'rs3 : x12', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x90 and fm2 == 0x334320 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000038c]:fnmadd.s a4, a3, a5, a2, dyn
	-[0x80000390]:csrrs tp, fcsr, zero
	-[0x80000394]:sw a4, 136(ra)
Current Store : [0x80000398] : sw tp, 140(ra) -- Store: [0x80005f9c]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rs2 : x12', 'rd : x13', 'rs3 : x11', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x91 and fm2 == 0x334f09 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000054 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003b0]:fnmadd.s a3, a4, a2, a1, dyn
	-[0x800003b4]:csrrs tp, fcsr, zero
	-[0x800003b8]:sw a3, 144(ra)
Current Store : [0x800003bc] : sw tp, 148(ra) -- Store: [0x80005fa4]:0x00000000




Last Coverpoint : ['rs1 : x11', 'rs2 : x13', 'rd : x12', 'rs3 : x14', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x92 and fm2 == 0x33583b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003d4]:fnmadd.s a2, a1, a3, a4, dyn
	-[0x800003d8]:csrrs tp, fcsr, zero
	-[0x800003dc]:sw a2, 152(ra)
Current Store : [0x800003e0] : sw tp, 156(ra) -- Store: [0x80005fac]:0x00000000




Last Coverpoint : ['rs1 : x12', 'rs2 : x10', 'rd : x11', 'rs3 : x13', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x93 and fm2 == 0x335adc and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000042 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003f8]:fnmadd.s a1, a2, a0, a3, dyn
	-[0x800003fc]:csrrs tp, fcsr, zero
	-[0x80000400]:sw a1, 160(ra)
Current Store : [0x80000404] : sw tp, 164(ra) -- Store: [0x80005fb4]:0x00000000




Last Coverpoint : ['rs1 : x9', 'rs2 : x11', 'rd : x10', 'rs3 : x8', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x94 and fm2 == 0x335bf4 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000052 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000424]:fnmadd.s a0, s1, a1, fp, dyn
	-[0x80000428]:csrrs a4, fcsr, zero
	-[0x8000042c]:sw a0, 168(ra)
Current Store : [0x80000430] : sw a4, 172(ra) -- Store: [0x80005fbc]:0x00000000




Last Coverpoint : ['rs1 : x10', 'rs2 : x8', 'rd : x9', 'rs3 : x7', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x95 and fm2 == 0x335d66 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000020 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000448]:fnmadd.s s1, a0, fp, t2, dyn
	-[0x8000044c]:csrrs a4, fcsr, zero
	-[0x80000450]:sw s1, 176(ra)
Current Store : [0x80000454] : sw a4, 180(ra) -- Store: [0x80005fc4]:0x00000000




Last Coverpoint : ['rs1 : x7', 'rs2 : x9', 'rd : x8', 'rs3 : x10', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x84 and fm2 == 0x60352f and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000027 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000474]:fnmadd.s fp, t2, s1, a0, dyn
	-[0x80000478]:csrrs a4, fcsr, zero
	-[0x8000047c]:sw fp, 0(a1)
Current Store : [0x80000480] : sw a4, 4(a1) -- Store: [0x80005fcc]:0x00000000




Last Coverpoint : ['rs1 : x8', 'rs2 : x6', 'rd : x7', 'rs3 : x9', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x5a9a41 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000498]:fnmadd.s t2, fp, t1, s1, dyn
	-[0x8000049c]:csrrs a4, fcsr, zero
	-[0x800004a0]:sw t2, 8(a1)
Current Store : [0x800004a4] : sw a4, 12(a1) -- Store: [0x80005fd4]:0x00000000




Last Coverpoint : ['rs1 : x5', 'rs2 : x7', 'rd : x6', 'rs3 : x4', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x83 and fm2 == 0x335dc0 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00000c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004bc]:fnmadd.s t1, t0, t2, tp, dyn
	-[0x800004c0]:csrrs a4, fcsr, zero
	-[0x800004c4]:sw t1, 16(a1)
Current Store : [0x800004c8] : sw a4, 20(a1) -- Store: [0x80005fdc]:0x00000000




Last Coverpoint : ['rs1 : x6', 'rs2 : x4', 'rd : x5', 'rs3 : x3', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x84 and fm2 == 0x00eb62 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00000f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004e0]:fnmadd.s t0, t1, tp, gp, dyn
	-[0x800004e4]:csrrs a4, fcsr, zero
	-[0x800004e8]:sw t0, 24(a1)
Current Store : [0x800004ec] : sw a4, 28(a1) -- Store: [0x80005fe4]:0x00000000




Last Coverpoint : ['rs1 : x3', 'rs2 : x5', 'rd : x4', 'rs3 : x6', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x1cf208 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000028 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000504]:fnmadd.s tp, gp, t0, t1, dyn
	-[0x80000508]:csrrs a4, fcsr, zero
	-[0x8000050c]:sw tp, 32(a1)
Current Store : [0x80000510] : sw a4, 36(a1) -- Store: [0x80005fec]:0x00000000




Last Coverpoint : ['rs1 : x4', 'rs2 : x2', 'rd : x3', 'rs3 : x5', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x6302a6 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000031 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000528]:fnmadd.s gp, tp, sp, t0, dyn
	-[0x8000052c]:csrrs a4, fcsr, zero
	-[0x80000530]:sw gp, 40(a1)
Current Store : [0x80000534] : sw a4, 44(a1) -- Store: [0x80005ff4]:0x00000000




Last Coverpoint : ['rs1 : x1', 'rs2 : x3', 'rd : x2', 'rs3 : x0']
Last Code Sequence : 
	-[0x8000054c]:fnmadd.s sp, ra, gp, zero, dyn
	-[0x80000550]:csrrs a4, fcsr, zero
	-[0x80000554]:sw sp, 48(a1)
Current Store : [0x80000558] : sw a4, 52(a1) -- Store: [0x80005ffc]:0x00000000




Last Coverpoint : ['rs1 : x2', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x87 and fm2 == 0x0fa212 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000570]:fnmadd.s t6, sp, t5, t4, dyn
	-[0x80000574]:csrrs a4, fcsr, zero
	-[0x80000578]:sw t6, 56(a1)
Current Store : [0x8000057c] : sw a4, 60(a1) -- Store: [0x80006004]:0x00000000




Last Coverpoint : ['rs1 : x0', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x87 and fm2 == 0x726cb5 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000594]:fnmadd.s t6, zero, t5, t4, dyn
	-[0x80000598]:csrrs a4, fcsr, zero
	-[0x8000059c]:sw t6, 64(a1)
Current Store : [0x800005a0] : sw a4, 68(a1) -- Store: [0x8000600c]:0x00000000




Last Coverpoint : ['rs2 : x1', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x88 and fm2 == 0x3c1fd3 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000019 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005b8]:fnmadd.s t6, t5, ra, t4, dyn
	-[0x800005bc]:csrrs a4, fcsr, zero
	-[0x800005c0]:sw t6, 72(a1)
Current Store : [0x800005c4] : sw a4, 76(a1) -- Store: [0x80006014]:0x00000000




Last Coverpoint : ['rs2 : x0']
Last Code Sequence : 
	-[0x800005dc]:fnmadd.s t6, t5, zero, t4, dyn
	-[0x800005e0]:csrrs a4, fcsr, zero
	-[0x800005e4]:sw t6, 80(a1)
Current Store : [0x800005e8] : sw a4, 84(a1) -- Store: [0x8000601c]:0x00000000




Last Coverpoint : ['rs3 : x2', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8a and fm2 == 0x39ac0b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000048 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000600]:fnmadd.s t6, t5, t4, sp, dyn
	-[0x80000604]:csrrs a4, fcsr, zero
	-[0x80000608]:sw t6, 88(a1)
Current Store : [0x8000060c] : sw a4, 92(a1) -- Store: [0x80006024]:0x00000000




Last Coverpoint : ['rs3 : x1', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8b and fm2 == 0x3577d9 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000030 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000624]:fnmadd.s t6, t5, t4, ra, dyn
	-[0x80000628]:csrrs a4, fcsr, zero
	-[0x8000062c]:sw t6, 96(a1)
Current Store : [0x80000630] : sw a4, 100(a1) -- Store: [0x8000602c]:0x00000000




Last Coverpoint : ['rd : x1', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8c and fm2 == 0x34dae7 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000044 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000648]:fnmadd.s ra, t6, t5, t4, dyn
	-[0x8000064c]:csrrs a4, fcsr, zero
	-[0x80000650]:sw ra, 104(a1)
Current Store : [0x80000654] : sw a4, 108(a1) -- Store: [0x80006034]:0x00000000




Last Coverpoint : ['rd : x0', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8d and fm2 == 0x33c83f and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000026 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000066c]:fnmadd.s zero, t6, t5, t4, dyn
	-[0x80000670]:csrrs a4, fcsr, zero
	-[0x80000674]:sw zero, 112(a1)
Current Store : [0x80000678] : sw a4, 116(a1) -- Store: [0x8000603c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8e and fm2 == 0x33d7a9 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000057 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000690]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000694]:csrrs a4, fcsr, zero
	-[0x80000698]:sw t6, 120(a1)
Current Store : [0x8000069c] : sw a4, 124(a1) -- Store: [0x80006044]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8f and fm2 == 0x3362a7 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000007 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006b4]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800006b8]:csrrs a4, fcsr, zero
	-[0x800006bc]:sw t6, 128(a1)
Current Store : [0x800006c0] : sw a4, 132(a1) -- Store: [0x8000604c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x90 and fm2 == 0x337104 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000037 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006d8]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800006dc]:csrrs a4, fcsr, zero
	-[0x800006e0]:sw t6, 136(a1)
Current Store : [0x800006e4] : sw a4, 140(a1) -- Store: [0x80006054]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x91 and fm2 == 0x336006 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00000d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006fc]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000700]:csrrs a4, fcsr, zero
	-[0x80000704]:sw t6, 144(a1)
Current Store : [0x80000708] : sw a4, 148(a1) -- Store: [0x8000605c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x92 and fm2 == 0x33608d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000020 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000720]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000724]:csrrs a4, fcsr, zero
	-[0x80000728]:sw t6, 152(a1)
Current Store : [0x8000072c] : sw a4, 156(a1) -- Store: [0x80006064]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x93 and fm2 == 0x33612a and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000744]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000748]:csrrs a4, fcsr, zero
	-[0x8000074c]:sw t6, 160(a1)
Current Store : [0x80000750] : sw a4, 164(a1) -- Store: [0x8000606c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x94 and fm2 == 0x335f05 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000768]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x8000076c]:csrrs a4, fcsr, zero
	-[0x80000770]:sw t6, 168(a1)
Current Store : [0x80000774] : sw a4, 172(a1) -- Store: [0x80006074]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x95 and fm2 == 0x335ddc and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00000a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000078c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000790]:csrrs a4, fcsr, zero
	-[0x80000794]:sw t6, 176(a1)
Current Store : [0x80000798] : sw a4, 180(a1) -- Store: [0x8000607c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6d and fm2 == 0x700000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00003d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007b0]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800007b4]:csrrs a4, fcsr, zero
	-[0x800007b8]:sw t6, 184(a1)
Current Store : [0x800007bc] : sw a4, 188(a1) -- Store: [0x80006084]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6e and fm2 == 0x320000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00005b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007d4]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800007d8]:csrrs a4, fcsr, zero
	-[0x800007dc]:sw t6, 192(a1)
Current Store : [0x800007e0] : sw a4, 196(a1) -- Store: [0x8000608c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6a and fm2 == 0x600000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00000b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007f8]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800007fc]:csrrs a4, fcsr, zero
	-[0x80000800]:sw t6, 200(a1)
Current Store : [0x80000804] : sw a4, 204(a1) -- Store: [0x80006094]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6e and fm2 == 0x300000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000060 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000081c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000820]:csrrs a4, fcsr, zero
	-[0x80000824]:sw t6, 208(a1)
Current Store : [0x80000828] : sw a4, 212(a1) -- Store: [0x8000609c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6d and fm2 == 0x280000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00003a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000840]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000844]:csrrs a4, fcsr, zero
	-[0x80000848]:sw t6, 216(a1)
Current Store : [0x8000084c] : sw a4, 220(a1) -- Store: [0x800060a4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6d and fm2 == 0x740000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00005d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000864]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000868]:csrrs a4, fcsr, zero
	-[0x8000086c]:sw t6, 224(a1)
Current Store : [0x80000870] : sw a4, 228(a1) -- Store: [0x800060ac]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6d and fm2 == 0x740000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000003 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000888]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x8000088c]:csrrs a4, fcsr, zero
	-[0x80000890]:sw t6, 232(a1)
Current Store : [0x80000894] : sw a4, 236(a1) -- Store: [0x800060b4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6d and fm2 == 0x6c0000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000045 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008ac]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800008b0]:csrrs a4, fcsr, zero
	-[0x800008b4]:sw t6, 240(a1)
Current Store : [0x800008b8] : sw a4, 244(a1) -- Store: [0x800060bc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6f and fm2 == 0x5f0000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000021 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008d0]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800008d4]:csrrs a4, fcsr, zero
	-[0x800008d8]:sw t6, 248(a1)
Current Store : [0x800008dc] : sw a4, 252(a1) -- Store: [0x800060c4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x70 and fm2 == 0x738000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000019 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008f4]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800008f8]:csrrs a4, fcsr, zero
	-[0x800008fc]:sw t6, 256(a1)
Current Store : [0x80000900] : sw a4, 260(a1) -- Store: [0x800060cc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x71 and fm2 == 0x6fc000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000041 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000918]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x8000091c]:csrrs a4, fcsr, zero
	-[0x80000920]:sw t6, 264(a1)
Current Store : [0x80000924] : sw a4, 268(a1) -- Store: [0x800060d4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x72 and fm2 == 0x7aa000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00002b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000093c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000940]:csrrs a4, fcsr, zero
	-[0x80000944]:sw t6, 272(a1)
Current Store : [0x80000948] : sw a4, 276(a1) -- Store: [0x800060dc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x73 and fm2 == 0x7cb000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000035 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000960]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000964]:csrrs a4, fcsr, zero
	-[0x80000968]:sw t6, 280(a1)
Current Store : [0x8000096c] : sw a4, 284(a1) -- Store: [0x800060e4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x74 and fm2 == 0x7d8800 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00004f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000984]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000988]:csrrs a4, fcsr, zero
	-[0x8000098c]:sw t6, 288(a1)
Current Store : [0x80000990] : sw a4, 292(a1) -- Store: [0x800060ec]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x75 and fm2 == 0x7f7400 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000023 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009a8]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800009ac]:csrrs a4, fcsr, zero
	-[0x800009b0]:sw t6, 296(a1)
Current Store : [0x800009b4] : sw a4, 300(a1) -- Store: [0x800060f4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x76 and fm2 == 0x7f5600 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000055 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009cc]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800009d0]:csrrs a4, fcsr, zero
	-[0x800009d4]:sw t6, 304(a1)
Current Store : [0x800009d8] : sw a4, 308(a1) -- Store: [0x800060fc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x77 and fm2 == 0x7fc100 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009f0]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800009f4]:csrrs a4, fcsr, zero
	-[0x800009f8]:sw t6, 312(a1)
Current Store : [0x800009fc] : sw a4, 316(a1) -- Store: [0x80006104]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x78 and fm2 == 0x7fd180 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00005d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a14]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000a18]:csrrs a4, fcsr, zero
	-[0x80000a1c]:sw t6, 320(a1)
Current Store : [0x80000a20] : sw a4, 324(a1) -- Store: [0x8000610c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x79 and fm2 == 0x7ff000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000040 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a38]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000a3c]:csrrs a4, fcsr, zero
	-[0x80000a40]:sw t6, 328(a1)
Current Store : [0x80000a44] : sw a4, 332(a1) -- Store: [0x80006114]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7a and fm2 == 0x7ff5c0 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000052 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a5c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000a60]:csrrs a4, fcsr, zero
	-[0x80000a64]:sw t6, 336(a1)
Current Store : [0x80000a68] : sw a4, 340(a1) -- Store: [0x8000611c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7b and fm2 == 0x7ffd60 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00002a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a80]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000a84]:csrrs a4, fcsr, zero
	-[0x80000a88]:sw t6, 344(a1)
Current Store : [0x80000a8c] : sw a4, 348(a1) -- Store: [0x80006124]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7c and fm2 == 0x7ffd58 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000055 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000aa4]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000aa8]:csrrs a4, fcsr, zero
	-[0x80000aac]:sw t6, 352(a1)
Current Store : [0x80000ab0] : sw a4, 356(a1) -- Store: [0x8000612c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7d and fm2 == 0x7ffec0 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000050 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ac8]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000acc]:csrrs a4, fcsr, zero
	-[0x80000ad0]:sw t6, 360(a1)
Current Store : [0x80000ad4] : sw a4, 364(a1) -- Store: [0x80006134]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x00002d and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000059 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000aec]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000af0]:csrrs a4, fcsr, zero
	-[0x80000af4]:sw t6, 368(a1)
Current Store : [0x80000af8] : sw a4, 372(a1) -- Store: [0x8000613c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000016 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000029 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b10]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000b14]:csrrs a4, fcsr, zero
	-[0x80000b18]:sw t6, 376(a1)
Current Store : [0x80000b1c] : sw a4, 380(a1) -- Store: [0x80006144]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000022 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b34]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000b38]:csrrs a4, fcsr, zero
	-[0x80000b3c]:sw t6, 384(a1)
Current Store : [0x80000b40] : sw a4, 388(a1) -- Store: [0x8000614c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000017 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000026 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b58]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000b5c]:csrrs a4, fcsr, zero
	-[0x80000b60]:sw t6, 392(a1)
Current Store : [0x80000b64] : sw a4, 396(a1) -- Store: [0x80006154]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000030 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000050 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b7c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000b80]:csrrs a4, fcsr, zero
	-[0x80000b84]:sw t6, 400(a1)
Current Store : [0x80000b88] : sw a4, 404(a1) -- Store: [0x8000615c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000014 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000008 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ba0]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000ba4]:csrrs a4, fcsr, zero
	-[0x80000ba8]:sw t6, 408(a1)
Current Store : [0x80000bac] : sw a4, 412(a1) -- Store: [0x80006164]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000022 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000004 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bc4]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000bc8]:csrrs a4, fcsr, zero
	-[0x80000bcc]:sw t6, 416(a1)
Current Store : [0x80000bd0] : sw a4, 420(a1) -- Store: [0x8000616c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x00006c and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000058 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000be8]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000bec]:csrrs a4, fcsr, zero
	-[0x80000bf0]:sw t6, 424(a1)
Current Store : [0x80000bf4] : sw a4, 428(a1) -- Store: [0x80006174]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000098 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00002f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c0c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000c10]:csrrs a4, fcsr, zero
	-[0x80000c14]:sw t6, 432(a1)
Current Store : [0x80000c18] : sw a4, 436(a1) -- Store: [0x8000617c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000126 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00004c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c30]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000c34]:csrrs a4, fcsr, zero
	-[0x80000c38]:sw t6, 440(a1)
Current Store : [0x80000c3c] : sw a4, 444(a1) -- Store: [0x80006184]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x00020c and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000019 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c54]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000c58]:csrrs a4, fcsr, zero
	-[0x80000c5c]:sw t6, 448(a1)
Current Store : [0x80000c60] : sw a4, 452(a1) -- Store: [0x8000618c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000412 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000023 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c78]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000c7c]:csrrs a4, fcsr, zero
	-[0x80000c80]:sw t6, 456(a1)
Current Store : [0x80000c84] : sw a4, 460(a1) -- Store: [0x80006194]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x00081c and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000037 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c9c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000ca0]:csrrs a4, fcsr, zero
	-[0x80000ca4]:sw t6, 464(a1)
Current Store : [0x80000ca8] : sw a4, 468(a1) -- Store: [0x8000619c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x001005 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00000a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cc0]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000cc4]:csrrs a4, fcsr, zero
	-[0x80000cc8]:sw t6, 472(a1)
Current Store : [0x80000ccc] : sw a4, 476(a1) -- Store: [0x800061a4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x00200f and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00001e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ce4]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000ce8]:csrrs a4, fcsr, zero
	-[0x80000cec]:sw t6, 480(a1)
Current Store : [0x80000cf0] : sw a4, 484(a1) -- Store: [0x800061ac]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x004018 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000030 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d08]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000d0c]:csrrs a4, fcsr, zero
	-[0x80000d10]:sw t6, 488(a1)
Current Store : [0x80000d14] : sw a4, 492(a1) -- Store: [0x800061b4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x00800a and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000013 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d2c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000d30]:csrrs a4, fcsr, zero
	-[0x80000d34]:sw t6, 496(a1)
Current Store : [0x80000d38] : sw a4, 500(a1) -- Store: [0x800061bc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x010027 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00004e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d50]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000d54]:csrrs a4, fcsr, zero
	-[0x80000d58]:sw t6, 504(a1)
Current Store : [0x80000d5c] : sw a4, 508(a1) -- Store: [0x800061c4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x02000a and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000013 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d74]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000d78]:csrrs a4, fcsr, zero
	-[0x80000d7c]:sw t6, 512(a1)
Current Store : [0x80000d80] : sw a4, 516(a1) -- Store: [0x800061cc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x040028 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000051 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d98]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000d9c]:csrrs a4, fcsr, zero
	-[0x80000da0]:sw t6, 520(a1)
Current Store : [0x80000da4] : sw a4, 524(a1) -- Store: [0x800061d4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x08002e and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00005c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000dbc]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000dc0]:csrrs a4, fcsr, zero
	-[0x80000dc4]:sw t6, 528(a1)
Current Store : [0x80000dc8] : sw a4, 532(a1) -- Store: [0x800061dc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x100003 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000006 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000de0]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000de4]:csrrs a4, fcsr, zero
	-[0x80000de8]:sw t6, 536(a1)
Current Store : [0x80000dec] : sw a4, 540(a1) -- Store: [0x800061e4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x20001e and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00003c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e04]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000e08]:csrrs a4, fcsr, zero
	-[0x80000e0c]:sw t6, 544(a1)
Current Store : [0x80000e10] : sw a4, 548(a1) -- Store: [0x800061ec]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x6102a6 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000051 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e28]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000e2c]:csrrs a4, fcsr, zero
	-[0x80000e30]:sw t6, 552(a1)
Current Store : [0x80000e34] : sw a4, 556(a1) -- Store: [0x800061f4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x73d370 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e4c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000e50]:csrrs a4, fcsr, zero
	-[0x80000e54]:sw t6, 560(a1)
Current Store : [0x80000e58] : sw a4, 564(a1) -- Store: [0x800061fc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x4f6465 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e70]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000e74]:csrrs a4, fcsr, zero
	-[0x80000e78]:sw t6, 568(a1)
Current Store : [0x80000e7c] : sw a4, 572(a1) -- Store: [0x80006204]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x83 and fm2 == 0x068650 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000015 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e94]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000e98]:csrrs a4, fcsr, zero
	-[0x80000e9c]:sw t6, 576(a1)
Current Store : [0x80000ea0] : sw a4, 580(a1) -- Store: [0x8000620c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x3bc624 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000054 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000eb8]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000ebc]:csrrs a4, fcsr, zero
	-[0x80000ec0]:sw t6, 584(a1)
Current Store : [0x80000ec4] : sw a4, 588(a1) -- Store: [0x80006214]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x84 and fm2 == 0x2dc2d2 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000040 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000edc]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000ee0]:csrrs a4, fcsr, zero
	-[0x80000ee4]:sw t6, 592(a1)
Current Store : [0x80000ee8] : sw a4, 596(a1) -- Store: [0x8000621c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x83 and fm2 == 0x3e939b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000052 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f00]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000f04]:csrrs a4, fcsr, zero
	-[0x80000f08]:sw t6, 600(a1)
Current Store : [0x80000f0c] : sw a4, 604(a1) -- Store: [0x80006224]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x86 and fm2 == 0x11bc2c and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000019 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f24]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000f28]:csrrs a4, fcsr, zero
	-[0x80000f2c]:sw t6, 608(a1)
Current Store : [0x80000f30] : sw a4, 612(a1) -- Store: [0x8000622c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x87 and fm2 == 0x08a069 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f48]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000f4c]:csrrs a4, fcsr, zero
	-[0x80000f50]:sw t6, 616(a1)
Current Store : [0x80000f54] : sw a4, 620(a1) -- Store: [0x80006234]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x88 and fm2 == 0x1f65d0 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f6c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000f70]:csrrs a4, fcsr, zero
	-[0x80000f74]:sw t6, 624(a1)
Current Store : [0x80000f78] : sw a4, 628(a1) -- Store: [0x8000623c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x89 and fm2 == 0x28ae6a and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f90]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000f94]:csrrs a4, fcsr, zero
	-[0x80000f98]:sw t6, 632(a1)
Current Store : [0x80000f9c] : sw a4, 636(a1) -- Store: [0x80006244]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8a and fm2 == 0x30202e and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000026 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fb4]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000fb8]:csrrs a4, fcsr, zero
	-[0x80000fbc]:sw t6, 640(a1)
Current Store : [0x80000fc0] : sw a4, 644(a1) -- Store: [0x8000624c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8b and fm2 == 0x31ca2d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000025 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fd8]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80000fdc]:csrrs a4, fcsr, zero
	-[0x80000fe0]:sw t6, 648(a1)
Current Store : [0x80000fe4] : sw a4, 652(a1) -- Store: [0x80006254]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8c and fm2 == 0x334754 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000005 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ffc]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001000]:csrrs a4, fcsr, zero
	-[0x80001004]:sw t6, 656(a1)
Current Store : [0x80001008] : sw a4, 660(a1) -- Store: [0x8000625c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8d and fm2 == 0x32aa62 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000041 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001020]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001024]:csrrs a4, fcsr, zero
	-[0x80001028]:sw t6, 664(a1)
Current Store : [0x8000102c] : sw a4, 668(a1) -- Store: [0x80006264]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8e and fm2 == 0x332cb4 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000024 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001044]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001048]:csrrs a4, fcsr, zero
	-[0x8000104c]:sw t6, 672(a1)
Current Store : [0x80001050] : sw a4, 676(a1) -- Store: [0x8000626c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8f and fm2 == 0x3333b6 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001068]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x8000106c]:csrrs a4, fcsr, zero
	-[0x80001070]:sw t6, 680(a1)
Current Store : [0x80001074] : sw a4, 684(a1) -- Store: [0x80006274]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x90 and fm2 == 0x334a21 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000039 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000108c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001090]:csrrs a4, fcsr, zero
	-[0x80001094]:sw t6, 688(a1)
Current Store : [0x80001098] : sw a4, 692(a1) -- Store: [0x8000627c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x91 and fm2 == 0x335d0c and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000005 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010b0]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800010b4]:csrrs a4, fcsr, zero
	-[0x800010b8]:sw t6, 696(a1)
Current Store : [0x800010bc] : sw a4, 700(a1) -- Store: [0x80006284]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x92 and fm2 == 0x335b08 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000020 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010d4]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800010d8]:csrrs a4, fcsr, zero
	-[0x800010dc]:sw t6, 704(a1)
Current Store : [0x800010e0] : sw a4, 708(a1) -- Store: [0x8000628c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x93 and fm2 == 0x335ac5 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000045 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010f8]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800010fc]:csrrs a4, fcsr, zero
	-[0x80001100]:sw t6, 712(a1)
Current Store : [0x80001104] : sw a4, 716(a1) -- Store: [0x80006294]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x94 and fm2 == 0x335bb1 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000111c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001120]:csrrs a4, fcsr, zero
	-[0x80001124]:sw t6, 720(a1)
Current Store : [0x80001128] : sw a4, 724(a1) -- Store: [0x8000629c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x95 and fm2 == 0x335d47 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001140]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001144]:csrrs a4, fcsr, zero
	-[0x80001148]:sw t6, 728(a1)
Current Store : [0x8000114c] : sw a4, 732(a1) -- Store: [0x800062a4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x82 and fm2 == 0x001a98 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000005 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001164]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001168]:csrrs a4, fcsr, zero
	-[0x8000116c]:sw t6, 736(a1)
Current Store : [0x80001170] : sw a4, 740(a1) -- Store: [0x800062ac]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x86 and fm2 == 0x0953c7 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001188]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x8000118c]:csrrs a4, fcsr, zero
	-[0x80001190]:sw t6, 744(a1)
Current Store : [0x80001194] : sw a4, 748(a1) -- Store: [0x800062b4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x86 and fm2 == 0x051f94 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011ac]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800011b0]:csrrs a4, fcsr, zero
	-[0x800011b4]:sw t6, 752(a1)
Current Store : [0x800011b8] : sw a4, 756(a1) -- Store: [0x800062bc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x1489a3 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011d0]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800011d4]:csrrs a4, fcsr, zero
	-[0x800011d8]:sw t6, 760(a1)
Current Store : [0x800011dc] : sw a4, 764(a1) -- Store: [0x800062c4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x84 and fm2 == 0x54ff53 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000015 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011f4]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800011f8]:csrrs a4, fcsr, zero
	-[0x800011fc]:sw t6, 768(a1)
Current Store : [0x80001200] : sw a4, 772(a1) -- Store: [0x800062cc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x6e3882 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000034 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001218]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x8000121c]:csrrs a4, fcsr, zero
	-[0x80001220]:sw t6, 776(a1)
Current Store : [0x80001224] : sw a4, 780(a1) -- Store: [0x800062d4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x86 and fm2 == 0x2f298d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000123c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001240]:csrrs a4, fcsr, zero
	-[0x80001244]:sw t6, 784(a1)
Current Store : [0x80001248] : sw a4, 788(a1) -- Store: [0x800062dc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x87 and fm2 == 0x13d645 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000052 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001260]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001264]:csrrs a4, fcsr, zero
	-[0x80001268]:sw t6, 792(a1)
Current Store : [0x8000126c] : sw a4, 796(a1) -- Store: [0x800062e4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x87 and fm2 == 0x3ffa57 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000011 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001284]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001288]:csrrs a4, fcsr, zero
	-[0x8000128c]:sw t6, 800(a1)
Current Store : [0x80001290] : sw a4, 804(a1) -- Store: [0x800062ec]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x88 and fm2 == 0x437b2c and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012a8]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800012ac]:csrrs a4, fcsr, zero
	-[0x800012b0]:sw t6, 808(a1)
Current Store : [0x800012b4] : sw a4, 812(a1) -- Store: [0x800062f4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x89 and fm2 == 0x37bec9 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000018 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012cc]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800012d0]:csrrs a4, fcsr, zero
	-[0x800012d4]:sw t6, 816(a1)
Current Store : [0x800012d8] : sw a4, 820(a1) -- Store: [0x800062fc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8a and fm2 == 0x358e44 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000018 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012f0]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800012f4]:csrrs a4, fcsr, zero
	-[0x800012f8]:sw t6, 824(a1)
Current Store : [0x800012fc] : sw a4, 828(a1) -- Store: [0x80006304]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8b and fm2 == 0x3791f2 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001314]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001318]:csrrs a4, fcsr, zero
	-[0x8000131c]:sw t6, 832(a1)
Current Store : [0x80001320] : sw a4, 836(a1) -- Store: [0x8000630c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8c and fm2 == 0x34cfb1 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000041 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001338]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x8000133c]:csrrs a4, fcsr, zero
	-[0x80001340]:sw t6, 840(a1)
Current Store : [0x80001344] : sw a4, 844(a1) -- Store: [0x80006314]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8d and fm2 == 0x33b1d3 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000135c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001360]:csrrs a4, fcsr, zero
	-[0x80001364]:sw t6, 848(a1)
Current Store : [0x80001368] : sw a4, 852(a1) -- Store: [0x8000631c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8e and fm2 == 0x33dd44 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001380]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001384]:csrrs a4, fcsr, zero
	-[0x80001388]:sw t6, 856(a1)
Current Store : [0x8000138c] : sw a4, 860(a1) -- Store: [0x80006324]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8f and fm2 == 0x338449 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000036 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013a4]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800013a8]:csrrs a4, fcsr, zero
	-[0x800013ac]:sw t6, 864(a1)
Current Store : [0x800013b0] : sw a4, 868(a1) -- Store: [0x8000632c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x90 and fm2 == 0x338014 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000061 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013c8]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800013cc]:csrrs a4, fcsr, zero
	-[0x800013d0]:sw t6, 872(a1)
Current Store : [0x800013d4] : sw a4, 876(a1) -- Store: [0x80006334]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x91 and fm2 == 0x33635a and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013ec]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800013f0]:csrrs a4, fcsr, zero
	-[0x800013f4]:sw t6, 880(a1)
Current Store : [0x800013f8] : sw a4, 884(a1) -- Store: [0x8000633c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x92 and fm2 == 0x33643b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000049 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001410]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001414]:csrrs a4, fcsr, zero
	-[0x80001418]:sw t6, 888(a1)
Current Store : [0x8000141c] : sw a4, 892(a1) -- Store: [0x80006344]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x93 and fm2 == 0x335ee3 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000019 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001434]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001438]:csrrs a4, fcsr, zero
	-[0x8000143c]:sw t6, 896(a1)
Current Store : [0x80001440] : sw a4, 900(a1) -- Store: [0x8000634c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x94 and fm2 == 0x335ed8 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000031 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001458]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x8000145c]:csrrs a4, fcsr, zero
	-[0x80001460]:sw t6, 904(a1)
Current Store : [0x80001464] : sw a4, 908(a1) -- Store: [0x80006354]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x95 and fm2 == 0x335dc8 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000147c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001480]:csrrs a4, fcsr, zero
	-[0x80001484]:sw t6, 912(a1)
Current Store : [0x80001488] : sw a4, 916(a1) -- Store: [0x8000635c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335d5d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000044 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014a0]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800014a4]:csrrs a4, fcsr, zero
	-[0x800014a8]:sw t6, 920(a1)
Current Store : [0x800014ac] : sw a4, 924(a1) -- Store: [0x80006364]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335d3a and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014c4]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800014c8]:csrrs a4, fcsr, zero
	-[0x800014cc]:sw t6, 928(a1)
Current Store : [0x800014d0] : sw a4, 932(a1) -- Store: [0x8000636c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335d44 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000053 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014e8]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800014ec]:csrrs a4, fcsr, zero
	-[0x800014f0]:sw t6, 936(a1)
Current Store : [0x800014f4] : sw a4, 940(a1) -- Store: [0x80006374]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335d82 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000023 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000150c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001510]:csrrs a4, fcsr, zero
	-[0x80001514]:sw t6, 944(a1)
Current Store : [0x80001518] : sw a4, 948(a1) -- Store: [0x8000637c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335d8c and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000014 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001530]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001534]:csrrs a4, fcsr, zero
	-[0x80001538]:sw t6, 952(a1)
Current Store : [0x8000153c] : sw a4, 956(a1) -- Store: [0x80006384]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335d15 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000059 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001554]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001558]:csrrs a4, fcsr, zero
	-[0x8000155c]:sw t6, 960(a1)
Current Store : [0x80001560] : sw a4, 964(a1) -- Store: [0x8000638c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335d1e and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000032 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001578]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x8000157c]:csrrs a4, fcsr, zero
	-[0x80001580]:sw t6, 968(a1)
Current Store : [0x80001584] : sw a4, 972(a1) -- Store: [0x80006394]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335d00 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000008 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000159c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800015a0]:csrrs a4, fcsr, zero
	-[0x800015a4]:sw t6, 976(a1)
Current Store : [0x800015a8] : sw a4, 980(a1) -- Store: [0x8000639c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335c4f and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000006 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015c0]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800015c4]:csrrs a4, fcsr, zero
	-[0x800015c8]:sw t6, 984(a1)
Current Store : [0x800015cc] : sw a4, 988(a1) -- Store: [0x800063a4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335a67 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000062 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015e4]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800015e8]:csrrs a4, fcsr, zero
	-[0x800015ec]:sw t6, 992(a1)
Current Store : [0x800015f0] : sw a4, 996(a1) -- Store: [0x800063ac]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x33580e and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00000f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001608]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x8000160c]:csrrs a4, fcsr, zero
	-[0x80001610]:sw t6, 1000(a1)
Current Store : [0x80001614] : sw a4, 1004(a1) -- Store: [0x800063b4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x33520e and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000057 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000162c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001630]:csrrs a4, fcsr, zero
	-[0x80001634]:sw t6, 1008(a1)
Current Store : [0x80001638] : sw a4, 1012(a1) -- Store: [0x800063bc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x3346e6 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001650]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001654]:csrrs a4, fcsr, zero
	-[0x80001658]:sw t6, 1016(a1)
Current Store : [0x8000165c] : sw a4, 1020(a1) -- Store: [0x800063c4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x3330db and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000008 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000167c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001680]:csrrs a4, fcsr, zero
	-[0x80001684]:sw t6, 0(a1)
Current Store : [0x80001688] : sw a4, 4(a1) -- Store: [0x800063cc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x3303c5 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000035 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016a0]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800016a4]:csrrs a4, fcsr, zero
	-[0x800016a8]:sw t6, 8(a1)
Current Store : [0x800016ac] : sw a4, 12(a1) -- Store: [0x800063d4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x32a9d7 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000062 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016c4]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800016c8]:csrrs a4, fcsr, zero
	-[0x800016cc]:sw t6, 16(a1)
Current Store : [0x800016d0] : sw a4, 20(a1) -- Store: [0x800063dc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x31f6f2 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00000c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016e8]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800016ec]:csrrs a4, fcsr, zero
	-[0x800016f0]:sw t6, 24(a1)
Current Store : [0x800016f4] : sw a4, 28(a1) -- Store: [0x800063e4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x309039 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00000a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000170c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001710]:csrrs a4, fcsr, zero
	-[0x80001714]:sw t6, 32(a1)
Current Store : [0x80001718] : sw a4, 36(a1) -- Store: [0x800063ec]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x2dc2b1 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000016 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001730]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001734]:csrrs a4, fcsr, zero
	-[0x80001738]:sw t6, 40(a1)
Current Store : [0x8000173c] : sw a4, 44(a1) -- Store: [0x800063f4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x2827cc and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000010 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001754]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001758]:csrrs a4, fcsr, zero
	-[0x8000175c]:sw t6, 48(a1)
Current Store : [0x80001760] : sw a4, 52(a1) -- Store: [0x800063fc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x1cf188 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001778]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x8000177c]:csrrs a4, fcsr, zero
	-[0x80001780]:sw t6, 56(a1)
Current Store : [0x80001784] : sw a4, 60(a1) -- Store: [0x80006404]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x068644 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000007 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000179c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800017a0]:csrrs a4, fcsr, zero
	-[0x800017a4]:sw t6, 64(a1)
Current Store : [0x800017a8] : sw a4, 68(a1) -- Store: [0x8000640c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x95 and fm2 == 0x335cad and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000061 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017c0]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800017c4]:csrrs a4, fcsr, zero
	-[0x800017c8]:sw t6, 72(a1)
Current Store : [0x800017cc] : sw a4, 76(a1) -- Store: [0x80006414]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335e32 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000054 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017e4]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800017e8]:csrrs a4, fcsr, zero
	-[0x800017ec]:sw t6, 80(a1)
Current Store : [0x800017f0] : sw a4, 84(a1) -- Store: [0x8000641c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335dea and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000021 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001808]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x8000180c]:csrrs a4, fcsr, zero
	-[0x80001810]:sw t6, 88(a1)
Current Store : [0x80001814] : sw a4, 92(a1) -- Store: [0x80006424]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335de3 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000182c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001830]:csrrs a4, fcsr, zero
	-[0x80001834]:sw t6, 96(a1)
Current Store : [0x80001838] : sw a4, 100(a1) -- Store: [0x8000642c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335e29 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000054 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001850]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001854]:csrrs a4, fcsr, zero
	-[0x80001858]:sw t6, 104(a1)
Current Store : [0x8000185c] : sw a4, 108(a1) -- Store: [0x80006434]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335dda and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000024 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001874]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001878]:csrrs a4, fcsr, zero
	-[0x8000187c]:sw t6, 112(a1)
Current Store : [0x80001880] : sw a4, 116(a1) -- Store: [0x8000643c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335dc0 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000021 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001898]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x8000189c]:csrrs a4, fcsr, zero
	-[0x800018a0]:sw t6, 120(a1)
Current Store : [0x800018a4] : sw a4, 124(a1) -- Store: [0x80006444]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335dc2 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000043 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018bc]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800018c0]:csrrs a4, fcsr, zero
	-[0x800018c4]:sw t6, 128(a1)
Current Store : [0x800018c8] : sw a4, 132(a1) -- Store: [0x8000644c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335d74 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018e0]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800018e4]:csrrs a4, fcsr, zero
	-[0x800018e8]:sw t6, 136(a1)
Current Store : [0x800018ec] : sw a4, 140(a1) -- Store: [0x80006454]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335c99 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001904]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001908]:csrrs a4, fcsr, zero
	-[0x8000190c]:sw t6, 144(a1)
Current Store : [0x80001910] : sw a4, 148(a1) -- Store: [0x8000645c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335b6d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000059 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001928]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x8000192c]:csrrs a4, fcsr, zero
	-[0x80001930]:sw t6, 152(a1)
Current Store : [0x80001934] : sw a4, 156(a1) -- Store: [0x80006464]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x33584c and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000194c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001950]:csrrs a4, fcsr, zero
	-[0x80001954]:sw t6, 160(a1)
Current Store : [0x80001958] : sw a4, 164(a1) -- Store: [0x8000646c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x3352ca and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001970]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001974]:csrrs a4, fcsr, zero
	-[0x80001978]:sw t6, 168(a1)
Current Store : [0x8000197c] : sw a4, 172(a1) -- Store: [0x80006474]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x3347a1 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000038 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001994]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001998]:csrrs a4, fcsr, zero
	-[0x8000199c]:sw t6, 176(a1)
Current Store : [0x800019a0] : sw a4, 180(a1) -- Store: [0x8000647c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x333126 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800019b8]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800019bc]:csrrs a4, fcsr, zero
	-[0x800019c0]:sw t6, 184(a1)
Current Store : [0x800019c4] : sw a4, 188(a1) -- Store: [0x80006484]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x330415 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000004 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800019dc]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800019e0]:csrrs a4, fcsr, zero
	-[0x800019e4]:sw t6, 192(a1)
Current Store : [0x800019e8] : sw a4, 196(a1) -- Store: [0x8000648c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x32aa9e and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a00]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001a04]:csrrs a4, fcsr, zero
	-[0x80001a08]:sw t6, 200(a1)
Current Store : [0x80001a0c] : sw a4, 204(a1) -- Store: [0x80006494]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x31f75b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a24]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001a28]:csrrs a4, fcsr, zero
	-[0x80001a2c]:sw t6, 208(a1)
Current Store : [0x80001a30] : sw a4, 212(a1) -- Store: [0x8000649c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x30909b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a48]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001a4c]:csrrs a4, fcsr, zero
	-[0x80001a50]:sw t6, 216(a1)
Current Store : [0x80001a54] : sw a4, 220(a1) -- Store: [0x800064a4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x2dc2e1 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00000c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a6c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001a70]:csrrs a4, fcsr, zero
	-[0x80001a74]:sw t6, 224(a1)
Current Store : [0x80001a78] : sw a4, 228(a1) -- Store: [0x800064ac]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x28280c and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a90]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001a94]:csrrs a4, fcsr, zero
	-[0x80001a98]:sw t6, 232(a1)
Current Store : [0x80001a9c] : sw a4, 236(a1) -- Store: [0x800064b4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x1cf21e and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000011 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ab4]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001ab8]:csrrs a4, fcsr, zero
	-[0x80001abc]:sw t6, 240(a1)
Current Store : [0x80001ac0] : sw a4, 244(a1) -- Store: [0x800064bc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x0686b0 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000046 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ad8]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001adc]:csrrs a4, fcsr, zero
	-[0x80001ae0]:sw t6, 248(a1)
Current Store : [0x80001ae4] : sw a4, 252(a1) -- Store: [0x800064c4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x95 and fm2 == 0x335e16 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000020 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001afc]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001b00]:csrrs a4, fcsr, zero
	-[0x80001b04]:sw t6, 256(a1)
Current Store : [0x80001b08] : sw a4, 260(a1) -- Store: [0x800064cc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6e and fm2 == 0x180000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00004d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b20]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001b24]:csrrs a4, fcsr, zero
	-[0x80001b28]:sw t6, 264(a1)
Current Store : [0x80001b2c] : sw a4, 268(a1) -- Store: [0x800064d4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6e and fm2 == 0x0e0000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000049 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b44]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001b48]:csrrs a4, fcsr, zero
	-[0x80001b4c]:sw t6, 272(a1)
Current Store : [0x80001b50] : sw a4, 276(a1) -- Store: [0x800064dc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6c and fm2 == 0x780000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000023 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b68]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001b6c]:csrrs a4, fcsr, zero
	-[0x80001b70]:sw t6, 280(a1)
Current Store : [0x80001b74] : sw a4, 284(a1) -- Store: [0x800064e4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6b and fm2 == 0x200000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000012 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b8c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001b90]:csrrs a4, fcsr, zero
	-[0x80001b94]:sw t6, 288(a1)
Current Store : [0x80001b98] : sw a4, 292(a1) -- Store: [0x800064ec]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6b and fm2 == 0x300000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000005 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001bb0]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001bb4]:csrrs a4, fcsr, zero
	-[0x80001bb8]:sw t6, 296(a1)
Current Store : [0x80001bbc] : sw a4, 300(a1) -- Store: [0x800064f4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6e and fm2 == 0x040000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000062 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001bd4]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001bd8]:csrrs a4, fcsr, zero
	-[0x80001bdc]:sw t6, 304(a1)
Current Store : [0x80001be0] : sw a4, 308(a1) -- Store: [0x800064fc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6d and fm2 == 0x480000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00000e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001bf8]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001bfc]:csrrs a4, fcsr, zero
	-[0x80001c00]:sw t6, 312(a1)
Current Store : [0x80001c04] : sw a4, 316(a1) -- Store: [0x80006504]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6d and fm2 == 0x580000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00004a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c2c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001c30]:csrrs a4, fcsr, zero
	-[0x80001c34]:sw t6, 320(a1)
Current Store : [0x80001c38] : sw a4, 324(a1) -- Store: [0x8000650c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6f and fm2 == 0x7f0000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c80]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001c84]:csrrs a4, fcsr, zero
	-[0x80001c88]:sw t6, 328(a1)
Current Store : [0x80001c8c] : sw a4, 332(a1) -- Store: [0x80006514]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x70 and fm2 == 0x780000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000010 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001cd4]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001cd8]:csrrs a4, fcsr, zero
	-[0x80001cdc]:sw t6, 336(a1)
Current Store : [0x80001ce0] : sw a4, 340(a1) -- Store: [0x8000651c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x71 and fm2 == 0x6d0000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00004c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d28]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001d2c]:csrrs a4, fcsr, zero
	-[0x80001d30]:sw t6, 344(a1)
Current Store : [0x80001d34] : sw a4, 348(a1) -- Store: [0x80006524]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x72 and fm2 == 0x786000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00003d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d7c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001d80]:csrrs a4, fcsr, zero
	-[0x80001d84]:sw t6, 352(a1)
Current Store : [0x80001d88] : sw a4, 356(a1) -- Store: [0x8000652c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x73 and fm2 == 0x7bc000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000044 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001dd0]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001dd4]:csrrs a4, fcsr, zero
	-[0x80001dd8]:sw t6, 360(a1)
Current Store : [0x80001ddc] : sw a4, 364(a1) -- Store: [0x80006534]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x74 and fm2 == 0x7f2000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00001c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e24]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001e28]:csrrs a4, fcsr, zero
	-[0x80001e2c]:sw t6, 368(a1)
Current Store : [0x80001e30] : sw a4, 372(a1) -- Store: [0x8000653c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x75 and fm2 == 0x7fb000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000014 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e78]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001e7c]:csrrs a4, fcsr, zero
	-[0x80001e80]:sw t6, 376(a1)
Current Store : [0x80001e84] : sw a4, 380(a1) -- Store: [0x80006544]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x76 and fm2 == 0x7ffc00 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ecc]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001ed0]:csrrs a4, fcsr, zero
	-[0x80001ed4]:sw t6, 384(a1)
Current Store : [0x80001ed8] : sw a4, 388(a1) -- Store: [0x8000654c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x77 and fm2 == 0x7fca00 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000036 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f20]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001f24]:csrrs a4, fcsr, zero
	-[0x80001f28]:sw t6, 392(a1)
Current Store : [0x80001f2c] : sw a4, 396(a1) -- Store: [0x80006554]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x78 and fm2 == 0x7feb80 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000029 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f74]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001f78]:csrrs a4, fcsr, zero
	-[0x80001f7c]:sw t6, 400(a1)
Current Store : [0x80001f80] : sw a4, 404(a1) -- Store: [0x8000655c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x79 and fm2 == 0x7ff6c0 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000025 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001fc8]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80001fcc]:csrrs a4, fcsr, zero
	-[0x80001fd0]:sw t6, 408(a1)
Current Store : [0x80001fd4] : sw a4, 412(a1) -- Store: [0x80006564]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7a and fm2 == 0x7ff680 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00004c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000201c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002020]:csrrs a4, fcsr, zero
	-[0x80002024]:sw t6, 416(a1)
Current Store : [0x80002028] : sw a4, 420(a1) -- Store: [0x8000656c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7b and fm2 == 0x7ffe30 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00001d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002070]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002074]:csrrs a4, fcsr, zero
	-[0x80002078]:sw t6, 424(a1)
Current Store : [0x8000207c] : sw a4, 428(a1) -- Store: [0x80006574]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7c and fm2 == 0x7ffec0 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000028 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800020c4]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800020c8]:csrrs a4, fcsr, zero
	-[0x800020cc]:sw t6, 432(a1)
Current Store : [0x800020d0] : sw a4, 436(a1) -- Store: [0x8000657c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7d and fm2 == 0x7fffd4 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00000b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002118]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x8000211c]:csrrs a4, fcsr, zero
	-[0x80002120]:sw t6, 440(a1)
Current Store : [0x80002124] : sw a4, 444(a1) -- Store: [0x80006584]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000028 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000050 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000216c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002170]:csrrs a4, fcsr, zero
	-[0x80002174]:sw t6, 448(a1)
Current Store : [0x80002178] : sw a4, 452(a1) -- Store: [0x8000658c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x00002a and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000053 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800021c0]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800021c4]:csrrs a4, fcsr, zero
	-[0x800021c8]:sw t6, 456(a1)
Current Store : [0x800021cc] : sw a4, 460(a1) -- Store: [0x80006594]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x00000e and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000019 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002214]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002218]:csrrs a4, fcsr, zero
	-[0x8000221c]:sw t6, 464(a1)
Current Store : [0x80002220] : sw a4, 468(a1) -- Store: [0x8000659c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x00001e and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000035 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002268]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x8000226c]:csrrs a4, fcsr, zero
	-[0x80002270]:sw t6, 472(a1)
Current Store : [0x80002274] : sw a4, 476(a1) -- Store: [0x800065a4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000038 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000061 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800022bc]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800022c0]:csrrs a4, fcsr, zero
	-[0x800022c4]:sw t6, 480(a1)
Current Store : [0x800022c8] : sw a4, 484(a1) -- Store: [0x800065ac]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000032 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000043 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002310]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002314]:csrrs a4, fcsr, zero
	-[0x80002318]:sw t6, 488(a1)
Current Store : [0x8000231c] : sw a4, 492(a1) -- Store: [0x800065b4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000042 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000043 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002364]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002368]:csrrs a4, fcsr, zero
	-[0x8000236c]:sw t6, 496(a1)
Current Store : [0x80002370] : sw a4, 500(a1) -- Store: [0x800065bc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000058 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00002f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800023b8]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800023bc]:csrrs a4, fcsr, zero
	-[0x800023c0]:sw t6, 504(a1)
Current Store : [0x800023c4] : sw a4, 508(a1) -- Store: [0x800065c4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x0000ae and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00005c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000240c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002410]:csrrs a4, fcsr, zero
	-[0x80002414]:sw t6, 512(a1)
Current Store : [0x80002418] : sw a4, 516(a1) -- Store: [0x800065cc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000112 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000023 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002460]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002464]:csrrs a4, fcsr, zero
	-[0x80002468]:sw t6, 520(a1)
Current Store : [0x8000246c] : sw a4, 524(a1) -- Store: [0x800065d4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000211 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000022 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800024b4]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800024b8]:csrrs a4, fcsr, zero
	-[0x800024bc]:sw t6, 528(a1)
Current Store : [0x800024c0] : sw a4, 532(a1) -- Store: [0x800065dc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000403 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000006 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002508]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x8000250c]:csrrs a4, fcsr, zero
	-[0x80002510]:sw t6, 536(a1)
Current Store : [0x80002514] : sw a4, 540(a1) -- Store: [0x800065e4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000826 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00004c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000255c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002560]:csrrs a4, fcsr, zero
	-[0x80002564]:sw t6, 544(a1)
Current Store : [0x80002568] : sw a4, 548(a1) -- Store: [0x800065ec]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x001020 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000041 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800025b0]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800025b4]:csrrs a4, fcsr, zero
	-[0x800025b8]:sw t6, 552(a1)
Current Store : [0x800025bc] : sw a4, 556(a1) -- Store: [0x800065f4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x00202c and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000059 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002604]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002608]:csrrs a4, fcsr, zero
	-[0x8000260c]:sw t6, 560(a1)
Current Store : [0x80002610] : sw a4, 564(a1) -- Store: [0x800065fc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x004006 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00000c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002658]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x8000265c]:csrrs a4, fcsr, zero
	-[0x80002660]:sw t6, 568(a1)
Current Store : [0x80002664] : sw a4, 572(a1) -- Store: [0x80006604]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x00800e and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00001c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800026ac]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800026b0]:csrrs a4, fcsr, zero
	-[0x800026b4]:sw t6, 576(a1)
Current Store : [0x800026b8] : sw a4, 580(a1) -- Store: [0x8000660c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x010016 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00002c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002700]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002704]:csrrs a4, fcsr, zero
	-[0x80002708]:sw t6, 584(a1)
Current Store : [0x8000270c] : sw a4, 588(a1) -- Store: [0x80006614]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x02002f and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00005e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002754]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002758]:csrrs a4, fcsr, zero
	-[0x8000275c]:sw t6, 592(a1)
Current Store : [0x80002760] : sw a4, 596(a1) -- Store: [0x8000661c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x04000c and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000017 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800027a8]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800027ac]:csrrs a4, fcsr, zero
	-[0x800027b0]:sw t6, 600(a1)
Current Store : [0x800027b4] : sw a4, 604(a1) -- Store: [0x80006624]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x08000e and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00001d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800027fc]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002800]:csrrs a4, fcsr, zero
	-[0x80002804]:sw t6, 608(a1)
Current Store : [0x80002808] : sw a4, 612(a1) -- Store: [0x8000662c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x10001f and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00003e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002850]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002854]:csrrs a4, fcsr, zero
	-[0x80002858]:sw t6, 616(a1)
Current Store : [0x8000285c] : sw a4, 620(a1) -- Store: [0x80006634]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x200028 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000050 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800028a4]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800028a8]:csrrs a4, fcsr, zero
	-[0x800028ac]:sw t6, 624(a1)
Current Store : [0x800028b0] : sw a4, 628(a1) -- Store: [0x8000663c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7fff42 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00005d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800028f8]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800028fc]:csrrs a4, fcsr, zero
	-[0x80002900]:sw t6, 632(a1)
Current Store : [0x80002904] : sw a4, 636(a1) -- Store: [0x80006644]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7fff84 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00003b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000294c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002950]:csrrs a4, fcsr, zero
	-[0x80002954]:sw t6, 640(a1)
Current Store : [0x80002958] : sw a4, 644(a1) -- Store: [0x8000664c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7fff62 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00004a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800029a0]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800029a4]:csrrs a4, fcsr, zero
	-[0x800029a8]:sw t6, 648(a1)
Current Store : [0x800029ac] : sw a4, 652(a1) -- Store: [0x80006654]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7fff32 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00005e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800029f4]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800029f8]:csrrs a4, fcsr, zero
	-[0x800029fc]:sw t6, 656(a1)
Current Store : [0x80002a00] : sw a4, 660(a1) -- Store: [0x8000665c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7fff88 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00002b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002a48]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002a4c]:csrrs a4, fcsr, zero
	-[0x80002a50]:sw t6, 664(a1)
Current Store : [0x80002a54] : sw a4, 668(a1) -- Store: [0x80006664]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7fffac and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000009 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002a9c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002aa0]:csrrs a4, fcsr, zero
	-[0x80002aa4]:sw t6, 672(a1)
Current Store : [0x80002aa8] : sw a4, 676(a1) -- Store: [0x8000666c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7ffebc and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000061 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002af0]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002af4]:csrrs a4, fcsr, zero
	-[0x80002af8]:sw t6, 680(a1)
Current Store : [0x80002afc] : sw a4, 684(a1) -- Store: [0x80006674]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7ffeee and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000008 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002b44]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002b48]:csrrs a4, fcsr, zero
	-[0x80002b4c]:sw t6, 688(a1)
Current Store : [0x80002b50] : sw a4, 692(a1) -- Store: [0x8000667c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7ffd82 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00003e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002b98]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002b9c]:csrrs a4, fcsr, zero
	-[0x80002ba0]:sw t6, 696(a1)
Current Store : [0x80002ba4] : sw a4, 700(a1) -- Store: [0x80006684]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7ffbac and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000029 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002bec]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002bf0]:csrrs a4, fcsr, zero
	-[0x80002bf4]:sw t6, 704(a1)
Current Store : [0x80002bf8] : sw a4, 708(a1) -- Store: [0x8000668c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7ff7fa and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002c40]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002c44]:csrrs a4, fcsr, zero
	-[0x80002c48]:sw t6, 712(a1)
Current Store : [0x80002c4c] : sw a4, 716(a1) -- Store: [0x80006694]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7fef44 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00005d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002c94]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002c98]:csrrs a4, fcsr, zero
	-[0x80002c9c]:sw t6, 720(a1)
Current Store : [0x80002ca0] : sw a4, 724(a1) -- Store: [0x8000669c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7fdf9a and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000032 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002ce8]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002cec]:csrrs a4, fcsr, zero
	-[0x80002cf0]:sw t6, 728(a1)
Current Store : [0x80002cf4] : sw a4, 732(a1) -- Store: [0x800066a4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7fbfe2 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00000e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002d3c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002d40]:csrrs a4, fcsr, zero
	-[0x80002d44]:sw t6, 736(a1)
Current Store : [0x80002d48] : sw a4, 740(a1) -- Store: [0x800066ac]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7f7fd8 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000013 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002d90]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002d94]:csrrs a4, fcsr, zero
	-[0x80002d98]:sw t6, 744(a1)
Current Store : [0x80002d9c] : sw a4, 748(a1) -- Store: [0x800066b4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7eff9e and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000030 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002de4]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002de8]:csrrs a4, fcsr, zero
	-[0x80002dec]:sw t6, 752(a1)
Current Store : [0x80002df0] : sw a4, 756(a1) -- Store: [0x800066bc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7dff3e and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000060 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002e38]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002e3c]:csrrs a4, fcsr, zero
	-[0x80002e40]:sw t6, 760(a1)
Current Store : [0x80002e44] : sw a4, 764(a1) -- Store: [0x800066c4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7bfff8 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000003 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002e8c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002e90]:csrrs a4, fcsr, zero
	-[0x80002e94]:sw t6, 768(a1)
Current Store : [0x80002e98] : sw a4, 772(a1) -- Store: [0x800066cc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x77ffe2 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00000e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002ee0]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002ee4]:csrrs a4, fcsr, zero
	-[0x80002ee8]:sw t6, 776(a1)
Current Store : [0x80002eec] : sw a4, 780(a1) -- Store: [0x800066d4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x6fff9e and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000030 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002f34]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002f38]:csrrs a4, fcsr, zero
	-[0x80002f3c]:sw t6, 784(a1)
Current Store : [0x80002f40] : sw a4, 788(a1) -- Store: [0x800066dc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x5fffdc and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000011 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002f88]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002f8c]:csrrs a4, fcsr, zero
	-[0x80002f90]:sw t6, 792(a1)
Current Store : [0x80002f94] : sw a4, 796(a1) -- Store: [0x800066e4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x3fffba and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000022 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002fdc]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80002fe0]:csrrs a4, fcsr, zero
	-[0x80002fe4]:sw t6, 800(a1)
Current Store : [0x80002fe8] : sw a4, 804(a1) -- Store: [0x800066ec]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7d and fm2 == 0x7fff04 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00003e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003030]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80003034]:csrrs a4, fcsr, zero
	-[0x80003038]:sw t6, 808(a1)
Current Store : [0x8000303c] : sw a4, 812(a1) -- Store: [0x800066f4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x400019 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000034 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003084]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80003088]:csrrs a4, fcsr, zero
	-[0x8000308c]:sw t6, 816(a1)
Current Store : [0x80003090] : sw a4, 820(a1) -- Store: [0x800066fc]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x400022 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000046 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800030d8]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800030dc]:csrrs a4, fcsr, zero
	-[0x800030e0]:sw t6, 824(a1)
Current Store : [0x800030e4] : sw a4, 828(a1) -- Store: [0x80006704]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x40001a and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000039 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000312c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80003130]:csrrs a4, fcsr, zero
	-[0x80003134]:sw t6, 832(a1)
Current Store : [0x80003138] : sw a4, 836(a1) -- Store: [0x8000670c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x40000c and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000021 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003180]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80003184]:csrrs a4, fcsr, zero
	-[0x80003188]:sw t6, 840(a1)
Current Store : [0x8000318c] : sw a4, 844(a1) -- Store: [0x80006714]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x40000a and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000026 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800031d4]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800031d8]:csrrs a4, fcsr, zero
	-[0x800031dc]:sw t6, 848(a1)
Current Store : [0x800031e0] : sw a4, 852(a1) -- Store: [0x8000671c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x40000c and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00003a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003228]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x8000322c]:csrrs a4, fcsr, zero
	-[0x80003230]:sw t6, 856(a1)
Current Store : [0x80003234] : sw a4, 860(a1) -- Store: [0x80006724]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x400000 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000041 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000327c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80003280]:csrrs a4, fcsr, zero
	-[0x80003284]:sw t6, 864(a1)
Current Store : [0x80003288] : sw a4, 868(a1) -- Store: [0x8000672c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3fffea and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000055 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800032d0]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800032d4]:csrrs a4, fcsr, zero
	-[0x800032d8]:sw t6, 872(a1)
Current Store : [0x800032dc] : sw a4, 876(a1) -- Store: [0x80006734]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3fffa2 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000044 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003324]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80003328]:csrrs a4, fcsr, zero
	-[0x8000332c]:sw t6, 880(a1)
Current Store : [0x80003330] : sw a4, 884(a1) -- Store: [0x8000673c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3fff24 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000048 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003378]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x8000337c]:csrrs a4, fcsr, zero
	-[0x80003380]:sw t6, 888(a1)
Current Store : [0x80003384] : sw a4, 892(a1) -- Store: [0x80006744]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3ffe0a and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000016 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800033cc]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800033d0]:csrrs a4, fcsr, zero
	-[0x800033d4]:sw t6, 896(a1)
Current Store : [0x800033d8] : sw a4, 900(a1) -- Store: [0x8000674c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3ffc14 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00002a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003420]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80003424]:csrrs a4, fcsr, zero
	-[0x80003428]:sw t6, 904(a1)
Current Store : [0x8000342c] : sw a4, 908(a1) -- Store: [0x80006754]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3ff80d and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00001b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003474]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80003478]:csrrs a4, fcsr, zero
	-[0x8000347c]:sw t6, 912(a1)
Current Store : [0x80003480] : sw a4, 916(a1) -- Store: [0x8000675c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3ff00c and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000019 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800034c8]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800034cc]:csrrs a4, fcsr, zero
	-[0x800034d0]:sw t6, 920(a1)
Current Store : [0x800034d4] : sw a4, 924(a1) -- Store: [0x80006764]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3fe012 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000024 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000351c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80003520]:csrrs a4, fcsr, zero
	-[0x80003524]:sw t6, 928(a1)
Current Store : [0x80003528] : sw a4, 932(a1) -- Store: [0x8000676c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3fc011 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000023 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003570]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80003574]:csrrs a4, fcsr, zero
	-[0x80003578]:sw t6, 936(a1)
Current Store : [0x8000357c] : sw a4, 940(a1) -- Store: [0x80006774]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3f802c and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00005a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800035c4]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800035c8]:csrrs a4, fcsr, zero
	-[0x800035cc]:sw t6, 944(a1)
Current Store : [0x800035d0] : sw a4, 948(a1) -- Store: [0x8000677c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3f000e and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00001c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003618]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x8000361c]:csrrs a4, fcsr, zero
	-[0x80003620]:sw t6, 952(a1)
Current Store : [0x80003624] : sw a4, 956(a1) -- Store: [0x80006784]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3e0026 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00004d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000366c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80003670]:csrrs a4, fcsr, zero
	-[0x80003674]:sw t6, 960(a1)
Current Store : [0x80003678] : sw a4, 964(a1) -- Store: [0x8000678c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3c0006 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00000c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800036c0]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800036c4]:csrrs a4, fcsr, zero
	-[0x800036c8]:sw t6, 968(a1)
Current Store : [0x800036cc] : sw a4, 972(a1) -- Store: [0x80006794]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x380024 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00004a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003714]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80003718]:csrrs a4, fcsr, zero
	-[0x8000371c]:sw t6, 976(a1)
Current Store : [0x80003720] : sw a4, 980(a1) -- Store: [0x8000679c]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x300016 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00002d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003768]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x8000376c]:csrrs a4, fcsr, zero
	-[0x80003770]:sw t6, 984(a1)
Current Store : [0x80003774] : sw a4, 988(a1) -- Store: [0x800067a4]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x20001e and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00003e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800037bc]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800037c0]:csrrs a4, fcsr, zero
	-[0x800037c4]:sw t6, 992(a1)
Current Store : [0x800037c8] : sw a4, 996(a1) -- Store: [0x800067ac]:0x00000005




Last Coverpoint : ['mnemonic : fnmadd.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs3 : x28', 'rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x2dc2d2 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003810]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80003814]:csrrs a4, fcsr, zero
	-[0x80003818]:sw t6, 1000(a1)
Current Store : [0x8000381c] : sw a4, 1004(a1) -- Store: [0x800067b4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x068650 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000005 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003864]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80003868]:csrrs a4, fcsr, zero
	-[0x8000386c]:sw t6, 1008(a1)
Current Store : [0x80003870] : sw a4, 1012(a1) -- Store: [0x800067bc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x7c3bd5 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000062 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800038b8]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800038bc]:csrrs a4, fcsr, zero
	-[0x800038c0]:sw t6, 1016(a1)
Current Store : [0x800038c4] : sw a4, 1020(a1) -- Store: [0x800067c4]:0x00000000




Last Coverpoint : ['mnemonic : fnmadd.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs3 : x28', 'rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x84 and fm2 == 0x0c213e and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000029 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003914]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80003918]:csrrs a4, fcsr, zero
	-[0x8000391c]:sw t6, 0(a1)
Current Store : [0x80003920] : sw a4, 4(a1) -- Store: [0x800067cc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x068650 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000023 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003968]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x8000396c]:csrrs a4, fcsr, zero
	-[0x80003970]:sw t6, 8(a1)
Current Store : [0x80003974] : sw a4, 12(a1) -- Store: [0x800067d4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x84 and fm2 == 0x54ff53 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800039bc]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x800039c0]:csrrs a4, fcsr, zero
	-[0x800039c4]:sw t6, 16(a1)
Current Store : [0x800039c8] : sw a4, 20(a1) -- Store: [0x800067dc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x85 and fm2 == 0x255a6d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000045 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003a10]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80003a14]:csrrs a4, fcsr, zero
	-[0x80003a18]:sw t6, 24(a1)
Current Store : [0x80003a1c] : sw a4, 28(a1) -- Store: [0x800067e4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x87 and fm2 == 0x21d998 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000019 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003a64]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80003a68]:csrrs a4, fcsr, zero
	-[0x80003a6c]:sw t6, 32(a1)
Current Store : [0x80003a70] : sw a4, 36(a1) -- Store: [0x800067ec]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x88 and fm2 == 0x1596af and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000055 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003ab8]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80003abc]:csrrs a4, fcsr, zero
	-[0x80003ac0]:sw t6, 40(a1)
Current Store : [0x80003ac4] : sw a4, 44(a1) -- Store: [0x800067f4]:0x00000000




Last Coverpoint : ['mnemonic : fnmadd.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs3 : x28', 'rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x89 and fm2 == 0x29bb76 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000037 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003b0c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80003b10]:csrrs a4, fcsr, zero
	-[0x80003b14]:sw t6, 48(a1)
Current Store : [0x80003b18] : sw a4, 52(a1) -- Store: [0x800067fc]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8a and fm2 == 0x3250b3 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00000c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003b60]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80003b64]:csrrs a4, fcsr, zero
	-[0x80003b68]:sw t6, 56(a1)
Current Store : [0x80003b6c] : sw a4, 60(a1) -- Store: [0x80006804]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x86 and fm2 == 0x02521d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003bb4]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80003bb8]:csrrs a4, fcsr, zero
	-[0x80003bbc]:sw t6, 64(a1)
Current Store : [0x80003bc0] : sw a4, 68(a1) -- Store: [0x8000680c]:0x00000000




Last Coverpoint : ['mnemonic : fnmadd.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs3 : x28', 'rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x87 and fm2 == 0x726cb5 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003c08]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80003c0c]:csrrs a4, fcsr, zero
	-[0x80003c10]:sw t6, 72(a1)
Current Store : [0x80003c14] : sw a4, 76(a1) -- Store: [0x80006814]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x89 and fm2 == 0x33e446 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000003 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003c5c]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80003c60]:csrrs a4, fcsr, zero
	-[0x80003c64]:sw t6, 80(a1)
Current Store : [0x80003c68] : sw a4, 84(a1) -- Store: [0x8000681c]:0x00000000




Last Coverpoint : ['mnemonic : fnmadd.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs3 : x28', 'rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8d and fm2 == 0x33c83f and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000026 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003cb0]:fnmadd.s t6, t5, t4, t3, dyn
	-[0x80003cb4]:csrrs a4, fcsr, zero
	-[0x80003cb8]:sw t6, 88(a1)
Current Store : [0x80003cbc] : sw a4, 92(a1) -- Store: [0x80006824]:0x00000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                                                                                      coverpoints                                                                                                                                                                      |                                                          code                                                          |
|---:|--------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80005f10]<br>0x8000003F|- mnemonic : fnmadd.s<br> - rs1 : x31<br> - rs2 : x30<br> - rd : x31<br> - rs3 : x29<br> - rs1 == rd != rs2 and rs1 == rd != rs3 and rs3 != rs2<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x2dc2d2 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat<br>     |[0x80000128]:fnmadd.s t6, t6, t5, t4, dyn<br> [0x8000012c]:csrrs tp, fcsr, zero<br> [0x80000130]:sw t6, 0(ra)<br>       |
|   2|[0x80005f18]<br>0x80000005|- rs1 : x29<br> - rs2 : x29<br> - rd : x30<br> - rs3 : x31<br> - rs1 == rs2 != rs3 and rs1 == rs2 != rd and rd != rs3<br>                                                                                                                                                                                                                              |[0x8000014c]:fnmadd.s t5, t4, t4, t6, dyn<br> [0x80000150]:csrrs tp, fcsr, zero<br> [0x80000154]:sw t5, 8(ra)<br>       |
|   3|[0x80005f20]<br>0x80000013|- rs1 : x30<br> - rs2 : x31<br> - rd : x29<br> - rs3 : x28<br> - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x83 and fm2 == 0x2827e4 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000013 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x80000170]:fnmadd.s t4, t5, t6, t3, dyn<br> [0x80000174]:csrrs tp, fcsr, zero<br> [0x80000178]:sw t4, 16(ra)<br>      |
|   4|[0x80005f28]<br>0x80000000|- rs1 : x27<br> - rs2 : x28<br> - rd : x27<br> - rs3 : x27<br> - rs1 == rd == rs3 != rs2<br>                                                                                                                                                                                                                                                           |[0x80000194]:fnmadd.s s11, s11, t3, s11, dyn<br> [0x80000198]:csrrs tp, fcsr, zero<br> [0x8000019c]:sw s11, 24(ra)<br>  |
|   5|[0x80005f30]<br>0x80000029|- rs1 : x28<br> - rs2 : x26<br> - rd : x26<br> - rs3 : x30<br> - rs2 == rd != rs1 and rs2 == rd != rs3 and rs3 != rs1<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x84 and fm2 == 0x0c213e and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000029 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                               |[0x800001b8]:fnmadd.s s10, t3, s10, t5, dyn<br> [0x800001bc]:csrrs tp, fcsr, zero<br> [0x800001c0]:sw s10, 32(ra)<br>   |
|   6|[0x80005f38]<br>0x80000000|- rs1 : x25<br> - rs2 : x25<br> - rd : x25<br> - rs3 : x25<br> - rs1 == rs2 == rs3 == rd<br>                                                                                                                                                                                                                                                           |[0x800001dc]:fnmadd.s s9, s9, s9, s9, dyn<br> [0x800001e0]:csrrs tp, fcsr, zero<br> [0x800001e4]:sw s9, 40(ra)<br>      |
|   7|[0x80005f40]<br>0x00000000|- rs1 : x26<br> - rs2 : x27<br> - rd : x28<br> - rs3 : x26<br> - rs1 == rs3 != rs2 and rs1 == rs3 != rd and rd != rs2<br>                                                                                                                                                                                                                              |[0x80000200]:fnmadd.s t3, s10, s11, s10, dyn<br> [0x80000204]:csrrs tp, fcsr, zero<br> [0x80000208]:sw t3, 48(ra)<br>   |
|   8|[0x80005f48]<br>0x42A55A6D|- rs1 : x23<br> - rs2 : x22<br> - rd : x24<br> - rs3 : x22<br> - rs2 == rs3 != rs1 and rs2 == rs3 != rd and rd != rs1<br>                                                                                                                                                                                                                              |[0x80000224]:fnmadd.s s8, s7, s6, s6, dyn<br> [0x80000228]:csrrs tp, fcsr, zero<br> [0x8000022c]:sw s8, 56(ra)<br>      |
|   9|[0x80005f50]<br>0x43A1D998|- rs1 : x24<br> - rs2 : x23<br> - rd : x23<br> - rs3 : x23<br> - rd == rs2 == rs3 != rs1<br>                                                                                                                                                                                                                                                           |[0x80000248]:fnmadd.s s7, s8, s7, s7, dyn<br> [0x8000024c]:csrrs tp, fcsr, zero<br> [0x80000250]:sw s7, 64(ra)<br>      |
|  10|[0x80005f58]<br>0x80000055|- rs1 : x21<br> - rs2 : x21<br> - rd : x21<br> - rs3 : x24<br> - rs1 == rs2 == rd != rs3<br>                                                                                                                                                                                                                                                           |[0x8000026c]:fnmadd.s s5, s5, s5, s8, dyn<br> [0x80000270]:csrrs tp, fcsr, zero<br> [0x80000274]:sw s5, 72(ra)<br>      |
|  11|[0x80005f60]<br>0x80000037|- rs1 : x22<br> - rs2 : x24<br> - rd : x20<br> - rs3 : x20<br> - rs3 == rd != rs1 and rs3 == rd != rs2 and rs2 != rs1<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x89 and fm2 == 0x29bb76 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000037 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                               |[0x80000290]:fnmadd.s s4, s6, s8, s4, dyn<br> [0x80000294]:csrrs tp, fcsr, zero<br> [0x80000298]:sw s4, 80(ra)<br>      |
|  12|[0x80005f68]<br>0x80000000|- rs1 : x19<br> - rs2 : x19<br> - rd : x22<br> - rs3 : x19<br> - rs1 == rs2 == rs3 != rd<br>                                                                                                                                                                                                                                                           |[0x800002b4]:fnmadd.s s6, s3, s3, s3, dyn<br> [0x800002b8]:csrrs tp, fcsr, zero<br> [0x800002bc]:sw s6, 88(ra)<br>      |
|  13|[0x80005f70]<br>0x80000035|- rs1 : x20<br> - rs2 : x18<br> - rd : x19<br> - rs3 : x21<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8b and fm2 == 0x310b99 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000035 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                          |[0x800002d8]:fnmadd.s s3, s4, s2, s5, dyn<br> [0x800002dc]:csrrs tp, fcsr, zero<br> [0x800002e0]:sw s3, 96(ra)<br>      |
|  14|[0x80005f78]<br>0x80000039|- rs1 : x17<br> - rs2 : x20<br> - rd : x18<br> - rs3 : x16<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8c and fm2 == 0x321e41 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000039 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                          |[0x800002fc]:fnmadd.s s2, a7, s4, a6, dyn<br> [0x80000300]:csrrs tp, fcsr, zero<br> [0x80000304]:sw s2, 104(ra)<br>     |
|  15|[0x80005f80]<br>0x80000061|- rs1 : x18<br> - rs2 : x16<br> - rd : x17<br> - rs3 : x15<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8d and fm2 == 0x324de5 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000061 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                          |[0x80000320]:fnmadd.s a7, s2, a6, a5, dyn<br> [0x80000324]:csrrs tp, fcsr, zero<br> [0x80000328]:sw a7, 112(ra)<br>     |
|  16|[0x80005f88]<br>0x8000005A|- rs1 : x15<br> - rs2 : x17<br> - rd : x16<br> - rs3 : x18<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8e and fm2 == 0x32dfa2 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                          |[0x80000344]:fnmadd.s a6, a5, a7, s2, dyn<br> [0x80000348]:csrrs tp, fcsr, zero<br> [0x8000034c]:sw a6, 120(ra)<br>     |
|  17|[0x80005f90]<br>0x8000000D|- rs1 : x16<br> - rs2 : x14<br> - rd : x15<br> - rs3 : x17<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8f and fm2 == 0x3354a4 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00000d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                          |[0x80000368]:fnmadd.s a5, a6, a4, a7, dyn<br> [0x8000036c]:csrrs tp, fcsr, zero<br> [0x80000370]:sw a5, 128(ra)<br>     |
|  18|[0x80005f98]<br>0x8000004C|- rs1 : x13<br> - rs2 : x15<br> - rd : x14<br> - rs3 : x12<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x90 and fm2 == 0x334320 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                          |[0x8000038c]:fnmadd.s a4, a3, a5, a2, dyn<br> [0x80000390]:csrrs tp, fcsr, zero<br> [0x80000394]:sw a4, 136(ra)<br>     |
|  19|[0x80005fa0]<br>0x80000054|- rs1 : x14<br> - rs2 : x12<br> - rd : x13<br> - rs3 : x11<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x91 and fm2 == 0x334f09 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000054 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                          |[0x800003b0]:fnmadd.s a3, a4, a2, a1, dyn<br> [0x800003b4]:csrrs tp, fcsr, zero<br> [0x800003b8]:sw a3, 144(ra)<br>     |
|  20|[0x80005fa8]<br>0x8000003F|- rs1 : x11<br> - rs2 : x13<br> - rd : x12<br> - rs3 : x14<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x92 and fm2 == 0x33583b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                          |[0x800003d4]:fnmadd.s a2, a1, a3, a4, dyn<br> [0x800003d8]:csrrs tp, fcsr, zero<br> [0x800003dc]:sw a2, 152(ra)<br>     |
|  21|[0x80005fb0]<br>0x80000042|- rs1 : x12<br> - rs2 : x10<br> - rd : x11<br> - rs3 : x13<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x93 and fm2 == 0x335adc and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000042 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                          |[0x800003f8]:fnmadd.s a1, a2, a0, a3, dyn<br> [0x800003fc]:csrrs tp, fcsr, zero<br> [0x80000400]:sw a1, 160(ra)<br>     |
|  22|[0x80005fb8]<br>0x80000052|- rs1 : x9<br> - rs2 : x11<br> - rd : x10<br> - rs3 : x8<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x94 and fm2 == 0x335bf4 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000052 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                            |[0x80000424]:fnmadd.s a0, s1, a1, fp, dyn<br> [0x80000428]:csrrs a4, fcsr, zero<br> [0x8000042c]:sw a0, 168(ra)<br>     |
|  23|[0x80005fc0]<br>0x80000020|- rs1 : x10<br> - rs2 : x8<br> - rd : x9<br> - rs3 : x7<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x95 and fm2 == 0x335d66 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000020 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                             |[0x80000448]:fnmadd.s s1, a0, fp, t2, dyn<br> [0x8000044c]:csrrs a4, fcsr, zero<br> [0x80000450]:sw s1, 176(ra)<br>     |
|  24|[0x80005fc8]<br>0x80000027|- rs1 : x7<br> - rs2 : x9<br> - rd : x8<br> - rs3 : x10<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x84 and fm2 == 0x60352f and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000027 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                             |[0x80000474]:fnmadd.s fp, t2, s1, a0, dyn<br> [0x80000478]:csrrs a4, fcsr, zero<br> [0x8000047c]:sw fp, 0(a1)<br>       |
|  25|[0x80005fd0]<br>0x8000004C|- rs1 : x8<br> - rs2 : x6<br> - rd : x7<br> - rs3 : x9<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x5a9a41 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000498]:fnmadd.s t2, fp, t1, s1, dyn<br> [0x8000049c]:csrrs a4, fcsr, zero<br> [0x800004a0]:sw t2, 8(a1)<br>       |
|  26|[0x80005fd8]<br>0x8000000C|- rs1 : x5<br> - rs2 : x7<br> - rd : x6<br> - rs3 : x4<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x83 and fm2 == 0x335dc0 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00000c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800004bc]:fnmadd.s t1, t0, t2, tp, dyn<br> [0x800004c0]:csrrs a4, fcsr, zero<br> [0x800004c4]:sw t1, 16(a1)<br>      |
|  27|[0x80005fe0]<br>0x8000000F|- rs1 : x6<br> - rs2 : x4<br> - rd : x5<br> - rs3 : x3<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x84 and fm2 == 0x00eb62 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00000f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800004e0]:fnmadd.s t0, t1, tp, gp, dyn<br> [0x800004e4]:csrrs a4, fcsr, zero<br> [0x800004e8]:sw t0, 24(a1)<br>      |
|  28|[0x80005fe8]<br>0x80000028|- rs1 : x3<br> - rs2 : x5<br> - rd : x4<br> - rs3 : x6<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x1cf208 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000028 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000504]:fnmadd.s tp, gp, t0, t1, dyn<br> [0x80000508]:csrrs a4, fcsr, zero<br> [0x8000050c]:sw tp, 32(a1)<br>      |
|  29|[0x80005ff0]<br>0x80000031|- rs1 : x4<br> - rs2 : x2<br> - rd : x3<br> - rs3 : x5<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x6302a6 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000031 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000528]:fnmadd.s gp, tp, sp, t0, dyn<br> [0x8000052c]:csrrs a4, fcsr, zero<br> [0x80000530]:sw gp, 40(a1)<br>      |
|  30|[0x80005ff8]<br>0x80000000|- rs1 : x1<br> - rs2 : x3<br> - rd : x2<br> - rs3 : x0<br>                                                                                                                                                                                                                                                                                             |[0x8000054c]:fnmadd.s sp, ra, gp, zero, dyn<br> [0x80000550]:csrrs a4, fcsr, zero<br> [0x80000554]:sw sp, 48(a1)<br>    |
|  31|[0x80006000]<br>0x8000004D|- rs1 : x2<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x87 and fm2 == 0x0fa212 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                          |[0x80000570]:fnmadd.s t6, sp, t5, t4, dyn<br> [0x80000574]:csrrs a4, fcsr, zero<br> [0x80000578]:sw t6, 56(a1)<br>      |
|  32|[0x80006008]<br>0x8000005A|- rs1 : x0<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x87 and fm2 == 0x726cb5 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                          |[0x80000594]:fnmadd.s t6, zero, t5, t4, dyn<br> [0x80000598]:csrrs a4, fcsr, zero<br> [0x8000059c]:sw t6, 64(a1)<br>    |
|  33|[0x80006010]<br>0x80000019|- rs2 : x1<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x88 and fm2 == 0x3c1fd3 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000019 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                          |[0x800005b8]:fnmadd.s t6, t5, ra, t4, dyn<br> [0x800005bc]:csrrs a4, fcsr, zero<br> [0x800005c0]:sw t6, 72(a1)<br>      |
|  34|[0x80006018]<br>0x80000003|- rs2 : x0<br>                                                                                                                                                                                                                                                                                                                                         |[0x800005dc]:fnmadd.s t6, t5, zero, t4, dyn<br> [0x800005e0]:csrrs a4, fcsr, zero<br> [0x800005e4]:sw t6, 80(a1)<br>    |
|  35|[0x80006020]<br>0x80000048|- rs3 : x2<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8a and fm2 == 0x39ac0b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000048 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                          |[0x80000600]:fnmadd.s t6, t5, t4, sp, dyn<br> [0x80000604]:csrrs a4, fcsr, zero<br> [0x80000608]:sw t6, 88(a1)<br>      |
|  36|[0x80006028]<br>0x80000030|- rs3 : x1<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8b and fm2 == 0x3577d9 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000030 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                          |[0x80000624]:fnmadd.s t6, t5, t4, ra, dyn<br> [0x80000628]:csrrs a4, fcsr, zero<br> [0x8000062c]:sw t6, 96(a1)<br>      |
|  37|[0x80006030]<br>0x80000044|- rd : x1<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8c and fm2 == 0x34dae7 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000044 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                           |[0x80000648]:fnmadd.s ra, t6, t5, t4, dyn<br> [0x8000064c]:csrrs a4, fcsr, zero<br> [0x80000650]:sw ra, 104(a1)<br>     |
|  38|[0x80006038]<br>0x00000000|- rd : x0<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8d and fm2 == 0x33c83f and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000026 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                           |[0x8000066c]:fnmadd.s zero, t6, t5, t4, dyn<br> [0x80000670]:csrrs a4, fcsr, zero<br> [0x80000674]:sw zero, 112(a1)<br> |
|  39|[0x80006040]<br>0x80000057|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8e and fm2 == 0x33d7a9 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000057 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000690]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000694]:csrrs a4, fcsr, zero<br> [0x80000698]:sw t6, 120(a1)<br>     |
|  40|[0x80006048]<br>0x80000007|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8f and fm2 == 0x3362a7 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000007 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800006b4]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800006b8]:csrrs a4, fcsr, zero<br> [0x800006bc]:sw t6, 128(a1)<br>     |
|  41|[0x80006050]<br>0x80000037|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x90 and fm2 == 0x337104 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000037 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800006d8]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800006dc]:csrrs a4, fcsr, zero<br> [0x800006e0]:sw t6, 136(a1)<br>     |
|  42|[0x80006058]<br>0x8000000D|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x91 and fm2 == 0x336006 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00000d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800006fc]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000700]:csrrs a4, fcsr, zero<br> [0x80000704]:sw t6, 144(a1)<br>     |
|  43|[0x80006060]<br>0x80000020|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x92 and fm2 == 0x33608d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000020 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000720]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000724]:csrrs a4, fcsr, zero<br> [0x80000728]:sw t6, 152(a1)<br>     |
|  44|[0x80006068]<br>0x8000004E|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x93 and fm2 == 0x33612a and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000744]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000748]:csrrs a4, fcsr, zero<br> [0x8000074c]:sw t6, 160(a1)<br>     |
|  45|[0x80006070]<br>0x8000003A|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x94 and fm2 == 0x335f05 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000768]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x8000076c]:csrrs a4, fcsr, zero<br> [0x80000770]:sw t6, 168(a1)<br>     |
|  46|[0x80006078]<br>0x8000000A|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x95 and fm2 == 0x335ddc and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00000a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000078c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000790]:csrrs a4, fcsr, zero<br> [0x80000794]:sw t6, 176(a1)<br>     |
|  47|[0x80006080]<br>0xBF800079|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6d and fm2 == 0x700000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00003d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800007b0]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800007b4]:csrrs a4, fcsr, zero<br> [0x800007b8]:sw t6, 184(a1)<br>     |
|  48|[0x80006088]<br>0xBF8000B4|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6e and fm2 == 0x320000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00005b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800007d4]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800007d8]:csrrs a4, fcsr, zero<br> [0x800007dc]:sw t6, 192(a1)<br>     |
|  49|[0x80006090]<br>0xBF800012|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6a and fm2 == 0x600000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00000b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800007f8]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800007fc]:csrrs a4, fcsr, zero<br> [0x80000800]:sw t6, 200(a1)<br>     |
|  50|[0x80006098]<br>0xBF8000B8|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6e and fm2 == 0x300000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000060 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000081c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000820]:csrrs a4, fcsr, zero<br> [0x80000824]:sw t6, 208(a1)<br>     |
|  51|[0x800060a0]<br>0xBF800064|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6d and fm2 == 0x280000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00003a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000840]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000844]:csrrs a4, fcsr, zero<br> [0x80000848]:sw t6, 216(a1)<br>     |
|  52|[0x800060a8]<br>0xBF80009A|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6d and fm2 == 0x740000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00005d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000864]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000868]:csrrs a4, fcsr, zero<br> [0x8000086c]:sw t6, 224(a1)<br>     |
|  53|[0x800060b0]<br>0xBF7FFF8C|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6d and fm2 == 0x740000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000003 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000888]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x8000088c]:csrrs a4, fcsr, zero<br> [0x80000890]:sw t6, 232(a1)<br>     |
|  54|[0x800060b8]<br>0xBF80000A|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6d and fm2 == 0x6c0000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000045 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800008ac]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800008b0]:csrrs a4, fcsr, zero<br> [0x800008b4]:sw t6, 240(a1)<br>     |
|  55|[0x800060c0]<br>0xBF7FFE84|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6f and fm2 == 0x5f0000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000021 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800008d0]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800008d4]:csrrs a4, fcsr, zero<br> [0x800008d8]:sw t6, 248(a1)<br>     |
|  56|[0x800060c8]<br>0xBF7FFC64|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x70 and fm2 == 0x738000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000019 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800008f4]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800008f8]:csrrs a4, fcsr, zero<br> [0x800008fc]:sw t6, 256(a1)<br>     |
|  57|[0x800060d0]<br>0xBF7FF904|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x71 and fm2 == 0x6fc000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000041 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000918]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x8000091c]:csrrs a4, fcsr, zero<br> [0x80000920]:sw t6, 264(a1)<br>     |
|  58|[0x800060d8]<br>0xBF7FF0AC|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x72 and fm2 == 0x7aa000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00002b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000093c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000940]:csrrs a4, fcsr, zero<br> [0x80000944]:sw t6, 272(a1)<br>     |
|  59|[0x800060e0]<br>0xBF7FE0D4|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x73 and fm2 == 0x7cb000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000035 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000960]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000964]:csrrs a4, fcsr, zero<br> [0x80000968]:sw t6, 280(a1)<br>     |
|  60|[0x800060e8]<br>0xBF7FC13C|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x74 and fm2 == 0x7d8800 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00004f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000984]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000988]:csrrs a4, fcsr, zero<br> [0x8000098c]:sw t6, 288(a1)<br>     |
|  61|[0x800060f0]<br>0xBF7F808C|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x75 and fm2 == 0x7f7400 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000023 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800009a8]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800009ac]:csrrs a4, fcsr, zero<br> [0x800009b0]:sw t6, 296(a1)<br>     |
|  62|[0x800060f8]<br>0xBF7F0154|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x76 and fm2 == 0x7f5600 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000055 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800009cc]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800009d0]:csrrs a4, fcsr, zero<br> [0x800009d4]:sw t6, 304(a1)<br>     |
|  63|[0x80006100]<br>0xBF7E00FC|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x77 and fm2 == 0x7fc100 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800009f0]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800009f4]:csrrs a4, fcsr, zero<br> [0x800009f8]:sw t6, 312(a1)<br>     |
|  64|[0x80006108]<br>0xBF7C0174|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x78 and fm2 == 0x7fd180 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00005d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000a14]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000a18]:csrrs a4, fcsr, zero<br> [0x80000a1c]:sw t6, 320(a1)<br>     |
|  65|[0x80006110]<br>0xBF780100|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x79 and fm2 == 0x7ff000 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000040 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000a38]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000a3c]:csrrs a4, fcsr, zero<br> [0x80000a40]:sw t6, 328(a1)<br>     |
|  66|[0x80006118]<br>0xBF700148|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7a and fm2 == 0x7ff5c0 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000052 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000a5c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000a60]:csrrs a4, fcsr, zero<br> [0x80000a64]:sw t6, 336(a1)<br>     |
|  67|[0x80006120]<br>0xBF6000A8|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7b and fm2 == 0x7ffd60 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00002a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000a80]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000a84]:csrrs a4, fcsr, zero<br> [0x80000a88]:sw t6, 344(a1)<br>     |
|  68|[0x80006128]<br>0xBF400154|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7c and fm2 == 0x7ffd58 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000055 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000aa4]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000aa8]:csrrs a4, fcsr, zero<br> [0x80000aac]:sw t6, 352(a1)<br>     |
|  69|[0x80006130]<br>0xBF000140|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7d and fm2 == 0x7ffec0 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000050 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000ac8]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000acc]:csrrs a4, fcsr, zero<br> [0x80000ad0]:sw t6, 360(a1)<br>     |
|  70|[0x80006138]<br>0xC040005A|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x00002d and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000059 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000aec]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000af0]:csrrs a4, fcsr, zero<br> [0x80000af4]:sw t6, 368(a1)<br>     |
|  71|[0x80006140]<br>0xC040002A|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000016 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000029 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000b10]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000b14]:csrrs a4, fcsr, zero<br> [0x80000b18]:sw t6, 376(a1)<br>     |
|  72|[0x80006148]<br>0xC0400042|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000022 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000b34]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000b38]:csrrs a4, fcsr, zero<br> [0x80000b3c]:sw t6, 384(a1)<br>     |
|  73|[0x80006150]<br>0xC040002A|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000017 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000026 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000b58]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000b5c]:csrrs a4, fcsr, zero<br> [0x80000b60]:sw t6, 392(a1)<br>     |
|  74|[0x80006158]<br>0xC0400058|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000030 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000050 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000b7c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000b80]:csrrs a4, fcsr, zero<br> [0x80000b84]:sw t6, 400(a1)<br>     |
|  75|[0x80006160]<br>0xC0400018|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000014 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000008 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000ba0]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000ba4]:csrrs a4, fcsr, zero<br> [0x80000ba8]:sw t6, 408(a1)<br>     |
|  76|[0x80006168]<br>0xC0400024|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000022 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000004 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000bc4]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000bc8]:csrrs a4, fcsr, zero<br> [0x80000bcc]:sw t6, 416(a1)<br>     |
|  77|[0x80006170]<br>0xC0400098|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x00006c and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000058 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000be8]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000bec]:csrrs a4, fcsr, zero<br> [0x80000bf0]:sw t6, 424(a1)<br>     |
|  78|[0x80006178]<br>0xC04000B0|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000098 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00002f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000c0c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000c10]:csrrs a4, fcsr, zero<br> [0x80000c14]:sw t6, 432(a1)<br>     |
|  79|[0x80006180]<br>0xC040014C|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000126 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00004c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000c30]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000c34]:csrrs a4, fcsr, zero<br> [0x80000c38]:sw t6, 440(a1)<br>     |
|  80|[0x80006188]<br>0xC0400218|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x00020c and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000019 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000c54]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000c58]:csrrs a4, fcsr, zero<br> [0x80000c5c]:sw t6, 448(a1)<br>     |
|  81|[0x80006190]<br>0xC0400424|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000412 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000023 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000c78]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000c7c]:csrrs a4, fcsr, zero<br> [0x80000c80]:sw t6, 456(a1)<br>     |
|  82|[0x80006198]<br>0xC0400838|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x00081c and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000037 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000c9c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000ca0]:csrrs a4, fcsr, zero<br> [0x80000ca4]:sw t6, 464(a1)<br>     |
|  83|[0x800061a0]<br>0xC040100A|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x001005 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00000a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000cc0]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000cc4]:csrrs a4, fcsr, zero<br> [0x80000cc8]:sw t6, 472(a1)<br>     |
|  84|[0x800061a8]<br>0xC040201E|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x00200f and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00001e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000ce4]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000ce8]:csrrs a4, fcsr, zero<br> [0x80000cec]:sw t6, 480(a1)<br>     |
|  85|[0x800061b0]<br>0xC0404030|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x004018 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000030 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000d08]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000d0c]:csrrs a4, fcsr, zero<br> [0x80000d10]:sw t6, 488(a1)<br>     |
|  86|[0x800061b8]<br>0xC0408014|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x00800a and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000013 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000d2c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000d30]:csrrs a4, fcsr, zero<br> [0x80000d34]:sw t6, 496(a1)<br>     |
|  87|[0x800061c0]<br>0xC041004E|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x010027 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00004e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000d50]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000d54]:csrrs a4, fcsr, zero<br> [0x80000d58]:sw t6, 504(a1)<br>     |
|  88|[0x800061c8]<br>0xC0420014|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x02000a and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000013 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000d74]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000d78]:csrrs a4, fcsr, zero<br> [0x80000d7c]:sw t6, 512(a1)<br>     |
|  89|[0x800061d0]<br>0xC0440050|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x040028 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000051 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000d98]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000d9c]:csrrs a4, fcsr, zero<br> [0x80000da0]:sw t6, 520(a1)<br>     |
|  90|[0x800061d8]<br>0xC048005C|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x08002e and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00005c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000dbc]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000dc0]:csrrs a4, fcsr, zero<br> [0x80000dc4]:sw t6, 528(a1)<br>     |
|  91|[0x800061e0]<br>0xC0500006|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x100003 and fs3 == 0 and fe3 == 0x7f and fm3 == 0x000006 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000de0]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000de4]:csrrs a4, fcsr, zero<br> [0x80000de8]:sw t6, 536(a1)<br>     |
|  92|[0x800061e8]<br>0xC060003C|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x20001e and fs3 == 0 and fe3 == 0x7f and fm3 == 0x00003c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000e04]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000e08]:csrrs a4, fcsr, zero<br> [0x80000e0c]:sw t6, 544(a1)<br>     |
|  93|[0x800061f0]<br>0x80000051|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x6102a6 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000051 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000e28]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000e2c]:csrrs a4, fcsr, zero<br> [0x80000e30]:sw t6, 552(a1)<br>     |
|  94|[0x800061f8]<br>0x8000005A|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x73d370 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000e4c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000e50]:csrrs a4, fcsr, zero<br> [0x80000e54]:sw t6, 560(a1)<br>     |
|  95|[0x80006200]<br>0x8000004F|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x4f6465 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000e70]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000e74]:csrrs a4, fcsr, zero<br> [0x80000e78]:sw t6, 568(a1)<br>     |
|  96|[0x80006208]<br>0x80000015|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x83 and fm2 == 0x068650 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000015 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000e94]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000e98]:csrrs a4, fcsr, zero<br> [0x80000e9c]:sw t6, 576(a1)<br>     |
|  97|[0x80006210]<br>0x80000054|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x3bc624 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000054 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000eb8]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000ebc]:csrrs a4, fcsr, zero<br> [0x80000ec0]:sw t6, 584(a1)<br>     |
|  98|[0x80006218]<br>0x80000040|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x84 and fm2 == 0x2dc2d2 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000040 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000edc]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000ee0]:csrrs a4, fcsr, zero<br> [0x80000ee4]:sw t6, 592(a1)<br>     |
|  99|[0x80006220]<br>0x80000052|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x83 and fm2 == 0x3e939b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000052 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000f00]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000f04]:csrrs a4, fcsr, zero<br> [0x80000f08]:sw t6, 600(a1)<br>     |
| 100|[0x80006228]<br>0x80000019|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x86 and fm2 == 0x11bc2c and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000019 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000f24]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000f28]:csrrs a4, fcsr, zero<br> [0x80000f2c]:sw t6, 608(a1)<br>     |
| 101|[0x80006230]<br>0x8000003E|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x87 and fm2 == 0x08a069 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000f48]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000f4c]:csrrs a4, fcsr, zero<br> [0x80000f50]:sw t6, 616(a1)<br>     |
| 102|[0x80006238]<br>0x8000003A|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x88 and fm2 == 0x1f65d0 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000f6c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000f70]:csrrs a4, fcsr, zero<br> [0x80000f74]:sw t6, 624(a1)<br>     |
| 103|[0x80006240]<br>0x8000003E|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x89 and fm2 == 0x28ae6a and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000f90]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000f94]:csrrs a4, fcsr, zero<br> [0x80000f98]:sw t6, 632(a1)<br>     |
| 104|[0x80006248]<br>0x80000026|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8a and fm2 == 0x30202e and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000026 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000fb4]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000fb8]:csrrs a4, fcsr, zero<br> [0x80000fbc]:sw t6, 640(a1)<br>     |
| 105|[0x80006250]<br>0x80000025|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8b and fm2 == 0x31ca2d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000025 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000fd8]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80000fdc]:csrrs a4, fcsr, zero<br> [0x80000fe0]:sw t6, 648(a1)<br>     |
| 106|[0x80006258]<br>0x80000005|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8c and fm2 == 0x334754 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000005 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80000ffc]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001000]:csrrs a4, fcsr, zero<br> [0x80001004]:sw t6, 656(a1)<br>     |
| 107|[0x80006260]<br>0x80000041|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8d and fm2 == 0x32aa62 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000041 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001020]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001024]:csrrs a4, fcsr, zero<br> [0x80001028]:sw t6, 664(a1)<br>     |
| 108|[0x80006268]<br>0x80000024|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8e and fm2 == 0x332cb4 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000024 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001044]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001048]:csrrs a4, fcsr, zero<br> [0x8000104c]:sw t6, 672(a1)<br>     |
| 109|[0x80006270]<br>0x8000003D|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8f and fm2 == 0x3333b6 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001068]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x8000106c]:csrrs a4, fcsr, zero<br> [0x80001070]:sw t6, 680(a1)<br>     |
| 110|[0x80006278]<br>0x80000039|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x90 and fm2 == 0x334a21 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000039 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000108c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001090]:csrrs a4, fcsr, zero<br> [0x80001094]:sw t6, 688(a1)<br>     |
| 111|[0x80006280]<br>0x80000005|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x91 and fm2 == 0x335d0c and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000005 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800010b0]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800010b4]:csrrs a4, fcsr, zero<br> [0x800010b8]:sw t6, 696(a1)<br>     |
| 112|[0x80006288]<br>0x80000020|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x92 and fm2 == 0x335b08 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000020 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800010d4]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800010d8]:csrrs a4, fcsr, zero<br> [0x800010dc]:sw t6, 704(a1)<br>     |
| 113|[0x80006290]<br>0x80000045|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x93 and fm2 == 0x335ac5 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000045 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800010f8]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800010fc]:csrrs a4, fcsr, zero<br> [0x80001100]:sw t6, 712(a1)<br>     |
| 114|[0x80006298]<br>0x8000005F|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x94 and fm2 == 0x335bb1 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000111c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001120]:csrrs a4, fcsr, zero<br> [0x80001124]:sw t6, 720(a1)<br>     |
| 115|[0x800062a0]<br>0x8000002C|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x95 and fm2 == 0x335d47 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001140]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001144]:csrrs a4, fcsr, zero<br> [0x80001148]:sw t6, 728(a1)<br>     |
| 116|[0x800062a8]<br>0x80000005|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x82 and fm2 == 0x001a98 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000005 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001164]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001168]:csrrs a4, fcsr, zero<br> [0x8000116c]:sw t6, 736(a1)<br>     |
| 117|[0x800062b0]<br>0x8000005F|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x86 and fm2 == 0x0953c7 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001188]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x8000118c]:csrrs a4, fcsr, zero<br> [0x80001190]:sw t6, 744(a1)<br>     |
| 118|[0x800062b8]<br>0x8000005A|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x86 and fm2 == 0x051f94 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800011ac]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800011b0]:csrrs a4, fcsr, zero<br> [0x800011b4]:sw t6, 752(a1)<br>     |
| 119|[0x800062c0]<br>0x8000002C|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x1489a3 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800011d0]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800011d4]:csrrs a4, fcsr, zero<br> [0x800011d8]:sw t6, 760(a1)<br>     |
| 120|[0x800062c8]<br>0x80000015|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x84 and fm2 == 0x54ff53 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000015 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800011f4]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800011f8]:csrrs a4, fcsr, zero<br> [0x800011fc]:sw t6, 768(a1)<br>     |
| 121|[0x800062d0]<br>0x80000034|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x6e3882 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000034 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001218]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x8000121c]:csrrs a4, fcsr, zero<br> [0x80001220]:sw t6, 776(a1)<br>     |
| 122|[0x800062d8]<br>0x8000003C|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x86 and fm2 == 0x2f298d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000123c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001240]:csrrs a4, fcsr, zero<br> [0x80001244]:sw t6, 784(a1)<br>     |
| 123|[0x800062e0]<br>0x80000052|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x87 and fm2 == 0x13d645 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000052 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001260]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001264]:csrrs a4, fcsr, zero<br> [0x80001268]:sw t6, 792(a1)<br>     |
| 124|[0x800062e8]<br>0x80000011|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x87 and fm2 == 0x3ffa57 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000011 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001284]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001288]:csrrs a4, fcsr, zero<br> [0x8000128c]:sw t6, 800(a1)<br>     |
| 125|[0x800062f0]<br>0x8000002D|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x88 and fm2 == 0x437b2c and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800012a8]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800012ac]:csrrs a4, fcsr, zero<br> [0x800012b0]:sw t6, 808(a1)<br>     |
| 126|[0x800062f8]<br>0x80000018|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x89 and fm2 == 0x37bec9 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000018 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800012cc]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800012d0]:csrrs a4, fcsr, zero<br> [0x800012d4]:sw t6, 816(a1)<br>     |
| 127|[0x80006300]<br>0x80000018|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8a and fm2 == 0x358e44 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000018 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800012f0]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800012f4]:csrrs a4, fcsr, zero<br> [0x800012f8]:sw t6, 824(a1)<br>     |
| 128|[0x80006308]<br>0x8000005F|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8b and fm2 == 0x3791f2 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001314]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001318]:csrrs a4, fcsr, zero<br> [0x8000131c]:sw t6, 832(a1)<br>     |
| 129|[0x80006310]<br>0x80000041|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8c and fm2 == 0x34cfb1 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000041 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001338]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x8000133c]:csrrs a4, fcsr, zero<br> [0x80001340]:sw t6, 840(a1)<br>     |
| 130|[0x80006318]<br>0x8000001D|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8d and fm2 == 0x33b1d3 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000135c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001360]:csrrs a4, fcsr, zero<br> [0x80001364]:sw t6, 848(a1)<br>     |
| 131|[0x80006320]<br>0x8000005A|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8e and fm2 == 0x33dd44 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001380]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001384]:csrrs a4, fcsr, zero<br> [0x80001388]:sw t6, 856(a1)<br>     |
| 132|[0x80006328]<br>0x80000036|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x8f and fm2 == 0x338449 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000036 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800013a4]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800013a8]:csrrs a4, fcsr, zero<br> [0x800013ac]:sw t6, 864(a1)<br>     |
| 133|[0x80006330]<br>0x80000061|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x90 and fm2 == 0x338014 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000061 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800013c8]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800013cc]:csrrs a4, fcsr, zero<br> [0x800013d0]:sw t6, 872(a1)<br>     |
| 134|[0x80006338]<br>0x8000001F|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x91 and fm2 == 0x33635a and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800013ec]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800013f0]:csrrs a4, fcsr, zero<br> [0x800013f4]:sw t6, 880(a1)<br>     |
| 135|[0x80006340]<br>0x80000049|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x92 and fm2 == 0x33643b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000049 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001410]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001414]:csrrs a4, fcsr, zero<br> [0x80001418]:sw t6, 888(a1)<br>     |
| 136|[0x80006348]<br>0x80000019|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x93 and fm2 == 0x335ee3 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000019 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001434]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001438]:csrrs a4, fcsr, zero<br> [0x8000143c]:sw t6, 896(a1)<br>     |
| 137|[0x80006350]<br>0x80000031|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x94 and fm2 == 0x335ed8 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000031 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001458]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x8000145c]:csrrs a4, fcsr, zero<br> [0x80001460]:sw t6, 904(a1)<br>     |
| 138|[0x80006358]<br>0x80000002|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x95 and fm2 == 0x335dc8 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000147c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001480]:csrrs a4, fcsr, zero<br> [0x80001484]:sw t6, 912(a1)<br>     |
| 139|[0x80006360]<br>0x80000044|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335d5d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000044 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800014a0]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800014a4]:csrrs a4, fcsr, zero<br> [0x800014a8]:sw t6, 920(a1)<br>     |
| 140|[0x80006368]<br>0x8000005C|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335d3a and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800014c4]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800014c8]:csrrs a4, fcsr, zero<br> [0x800014cc]:sw t6, 928(a1)<br>     |
| 141|[0x80006370]<br>0x80000053|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335d44 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000053 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800014e8]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800014ec]:csrrs a4, fcsr, zero<br> [0x800014f0]:sw t6, 936(a1)<br>     |
| 142|[0x80006378]<br>0x80000023|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335d82 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000023 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000150c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001510]:csrrs a4, fcsr, zero<br> [0x80001514]:sw t6, 944(a1)<br>     |
| 143|[0x80006380]<br>0x80000014|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335d8c and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000014 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001530]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001534]:csrrs a4, fcsr, zero<br> [0x80001538]:sw t6, 952(a1)<br>     |
| 144|[0x80006388]<br>0x80000059|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335d15 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000059 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001554]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001558]:csrrs a4, fcsr, zero<br> [0x8000155c]:sw t6, 960(a1)<br>     |
| 145|[0x80006390]<br>0x80000032|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335d1e and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000032 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001578]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x8000157c]:csrrs a4, fcsr, zero<br> [0x80001580]:sw t6, 968(a1)<br>     |
| 146|[0x80006398]<br>0x80000008|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335d00 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000008 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000159c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800015a0]:csrrs a4, fcsr, zero<br> [0x800015a4]:sw t6, 976(a1)<br>     |
| 147|[0x800063a0]<br>0x80000006|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335c4f and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000006 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800015c0]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800015c4]:csrrs a4, fcsr, zero<br> [0x800015c8]:sw t6, 984(a1)<br>     |
| 148|[0x800063a8]<br>0x80000062|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x335a67 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000062 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800015e4]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800015e8]:csrrs a4, fcsr, zero<br> [0x800015ec]:sw t6, 992(a1)<br>     |
| 149|[0x800063b0]<br>0x8000000F|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x33580e and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00000f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001608]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x8000160c]:csrrs a4, fcsr, zero<br> [0x80001610]:sw t6, 1000(a1)<br>    |
| 150|[0x800063b8]<br>0x80000057|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x33520e and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000057 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000162c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001630]:csrrs a4, fcsr, zero<br> [0x80001634]:sw t6, 1008(a1)<br>    |
| 151|[0x800063c0]<br>0x8000004D|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x3346e6 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001650]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001654]:csrrs a4, fcsr, zero<br> [0x80001658]:sw t6, 1016(a1)<br>    |
| 152|[0x800063c8]<br>0x80000008|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x3330db and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000008 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000167c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001680]:csrrs a4, fcsr, zero<br> [0x80001684]:sw t6, 0(a1)<br>       |
| 153|[0x800063d0]<br>0x80000035|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x3303c5 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000035 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800016a0]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800016a4]:csrrs a4, fcsr, zero<br> [0x800016a8]:sw t6, 8(a1)<br>       |
| 154|[0x800063d8]<br>0x80000062|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x32a9d7 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000062 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800016c4]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800016c8]:csrrs a4, fcsr, zero<br> [0x800016cc]:sw t6, 16(a1)<br>      |
| 155|[0x800063e0]<br>0x8000000C|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x31f6f2 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00000c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800016e8]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800016ec]:csrrs a4, fcsr, zero<br> [0x800016f0]:sw t6, 24(a1)<br>      |
| 156|[0x800063e8]<br>0x8000000A|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x309039 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00000a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000170c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001710]:csrrs a4, fcsr, zero<br> [0x80001714]:sw t6, 32(a1)<br>      |
| 157|[0x800063f0]<br>0x80000016|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x2dc2b1 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000016 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001730]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001734]:csrrs a4, fcsr, zero<br> [0x80001738]:sw t6, 40(a1)<br>      |
| 158|[0x800063f8]<br>0x80000010|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x2827cc and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000010 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001754]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001758]:csrrs a4, fcsr, zero<br> [0x8000175c]:sw t6, 48(a1)<br>      |
| 159|[0x80006400]<br>0x8000005A|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x1cf188 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00005a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001778]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x8000177c]:csrrs a4, fcsr, zero<br> [0x80001780]:sw t6, 56(a1)<br>      |
| 160|[0x80006408]<br>0x80000007|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x96 and fm2 == 0x068644 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000007 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000179c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800017a0]:csrrs a4, fcsr, zero<br> [0x800017a4]:sw t6, 64(a1)<br>      |
| 161|[0x80006410]<br>0x80000061|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x95 and fm2 == 0x335cad and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000061 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800017c0]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800017c4]:csrrs a4, fcsr, zero<br> [0x800017c8]:sw t6, 72(a1)<br>      |
| 162|[0x80006418]<br>0x80000054|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335e32 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000054 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800017e4]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800017e8]:csrrs a4, fcsr, zero<br> [0x800017ec]:sw t6, 80(a1)<br>      |
| 163|[0x80006420]<br>0x80000021|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335dea and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000021 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001808]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x8000180c]:csrrs a4, fcsr, zero<br> [0x80001810]:sw t6, 88(a1)<br>      |
| 164|[0x80006428]<br>0x8000001E|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335de3 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000182c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001830]:csrrs a4, fcsr, zero<br> [0x80001834]:sw t6, 96(a1)<br>      |
| 165|[0x80006430]<br>0x80000054|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335e29 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000054 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001850]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001854]:csrrs a4, fcsr, zero<br> [0x80001858]:sw t6, 104(a1)<br>     |
| 166|[0x80006438]<br>0x80000024|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335dda and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000024 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001874]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001878]:csrrs a4, fcsr, zero<br> [0x8000187c]:sw t6, 112(a1)<br>     |
| 167|[0x80006440]<br>0x80000021|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335dc0 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000021 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001898]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x8000189c]:csrrs a4, fcsr, zero<br> [0x800018a0]:sw t6, 120(a1)<br>     |
| 168|[0x80006448]<br>0x80000043|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335dc2 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000043 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800018bc]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800018c0]:csrrs a4, fcsr, zero<br> [0x800018c4]:sw t6, 128(a1)<br>     |
| 169|[0x80006450]<br>0x8000004B|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335d74 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00004b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800018e0]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800018e4]:csrrs a4, fcsr, zero<br> [0x800018e8]:sw t6, 136(a1)<br>     |
| 170|[0x80006458]<br>0x8000002F|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335c99 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001904]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001908]:csrrs a4, fcsr, zero<br> [0x8000190c]:sw t6, 144(a1)<br>     |
| 171|[0x80006460]<br>0x80000059|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x335b6d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000059 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001928]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x8000192c]:csrrs a4, fcsr, zero<br> [0x80001930]:sw t6, 152(a1)<br>     |
| 172|[0x80006468]<br>0x8000001D|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x33584c and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000194c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001950]:csrrs a4, fcsr, zero<br> [0x80001954]:sw t6, 160(a1)<br>     |
| 173|[0x80006470]<br>0x8000002F|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x3352ca and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001970]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001974]:csrrs a4, fcsr, zero<br> [0x80001978]:sw t6, 168(a1)<br>     |
| 174|[0x80006478]<br>0x80000038|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x3347a1 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000038 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001994]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001998]:csrrs a4, fcsr, zero<br> [0x8000199c]:sw t6, 176(a1)<br>     |
| 175|[0x80006480]<br>0x8000002D|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x333126 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800019b8]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800019bc]:csrrs a4, fcsr, zero<br> [0x800019c0]:sw t6, 184(a1)<br>     |
| 176|[0x80006488]<br>0x80000004|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x330415 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000004 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800019dc]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800019e0]:csrrs a4, fcsr, zero<br> [0x800019e4]:sw t6, 192(a1)<br>     |
| 177|[0x80006490]<br>0x8000002C|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x32aa9e and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00002c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001a00]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001a04]:csrrs a4, fcsr, zero<br> [0x80001a08]:sw t6, 200(a1)<br>     |
| 178|[0x80006498]<br>0x8000003F|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x31f75b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001a24]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001a28]:csrrs a4, fcsr, zero<br> [0x80001a2c]:sw t6, 208(a1)<br>     |
| 179|[0x800064a0]<br>0x8000003C|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x30909b and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00003c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001a48]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001a4c]:csrrs a4, fcsr, zero<br> [0x80001a50]:sw t6, 216(a1)<br>     |
| 180|[0x800064a8]<br>0x8000000C|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x2dc2e1 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00000c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001a6c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001a70]:csrrs a4, fcsr, zero<br> [0x80001a74]:sw t6, 224(a1)<br>     |
| 181|[0x800064b0]<br>0x8000001E|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x28280c and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001a90]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001a94]:csrrs a4, fcsr, zero<br> [0x80001a98]:sw t6, 232(a1)<br>     |
| 182|[0x800064b8]<br>0x80000011|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x1cf21e and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000011 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001ab4]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001ab8]:csrrs a4, fcsr, zero<br> [0x80001abc]:sw t6, 240(a1)<br>     |
| 183|[0x800064c0]<br>0x80000046|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x96 and fm2 == 0x0686b0 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000046 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001ad8]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001adc]:csrrs a4, fcsr, zero<br> [0x80001ae0]:sw t6, 248(a1)<br>     |
| 184|[0x800064c8]<br>0x80000020|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x95 and fm2 == 0x335e16 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000020 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001afc]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001b00]:csrrs a4, fcsr, zero<br> [0x80001b04]:sw t6, 256(a1)<br>     |
| 185|[0x800064d0]<br>0x80800099|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6e and fm2 == 0x180000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00004d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001b20]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001b24]:csrrs a4, fcsr, zero<br> [0x80001b28]:sw t6, 264(a1)<br>     |
| 186|[0x800064d8]<br>0x80800090|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6e and fm2 == 0x0e0000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000049 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001b44]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001b48]:csrrs a4, fcsr, zero<br> [0x80001b4c]:sw t6, 272(a1)<br>     |
| 187|[0x800064e0]<br>0x80800042|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6c and fm2 == 0x780000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000023 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001b68]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001b6c]:csrrs a4, fcsr, zero<br> [0x80001b70]:sw t6, 280(a1)<br>     |
| 188|[0x800064e8]<br>0x8080001C|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6b and fm2 == 0x200000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000012 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001b8c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001b90]:csrrs a4, fcsr, zero<br> [0x80001b94]:sw t6, 288(a1)<br>     |
| 189|[0x800064f0]<br>0x807FFFFA|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6b and fm2 == 0x300000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000005 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001bb0]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001bb4]:csrrs a4, fcsr, zero<br> [0x80001bb8]:sw t6, 296(a1)<br>     |
| 190|[0x800064f8]<br>0x808000A4|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x6e and fm2 == 0x040000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000062 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001bd4]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001bd8]:csrrs a4, fcsr, zero<br> [0x80001bdc]:sw t6, 304(a1)<br>     |
| 191|[0x80006500]<br>0x807FFFDC|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6d and fm2 == 0x480000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00000e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001bf8]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001bfc]:csrrs a4, fcsr, zero<br> [0x80001c00]:sw t6, 312(a1)<br>     |
| 192|[0x80006508]<br>0x80800014|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6d and fm2 == 0x580000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00004a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001c2c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001c30]:csrrs a4, fcsr, zero<br> [0x80001c34]:sw t6, 320(a1)<br>     |
| 193|[0x80006510]<br>0x807FFF02|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x6f and fm2 == 0x7f0000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001c80]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001c84]:csrrs a4, fcsr, zero<br> [0x80001c88]:sw t6, 328(a1)<br>     |
| 194|[0x80006518]<br>0x807FFE20|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x70 and fm2 == 0x780000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000010 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001cd4]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001cd8]:csrrs a4, fcsr, zero<br> [0x80001cdc]:sw t6, 336(a1)<br>     |
| 195|[0x80006520]<br>0x807FFC98|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x71 and fm2 == 0x6d0000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00004c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001d28]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001d2c]:csrrs a4, fcsr, zero<br> [0x80001d30]:sw t6, 344(a1)<br>     |
| 196|[0x80006528]<br>0x807FF87A|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x72 and fm2 == 0x786000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00003d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001d7c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001d80]:csrrs a4, fcsr, zero<br> [0x80001d84]:sw t6, 352(a1)<br>     |
| 197|[0x80006530]<br>0x807FF088|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x73 and fm2 == 0x7bc000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000044 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001dd0]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001dd4]:csrrs a4, fcsr, zero<br> [0x80001dd8]:sw t6, 360(a1)<br>     |
| 198|[0x80006538]<br>0x807FE038|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x74 and fm2 == 0x7f2000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00001c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001e24]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001e28]:csrrs a4, fcsr, zero<br> [0x80001e2c]:sw t6, 368(a1)<br>     |
| 199|[0x80006540]<br>0x807FC028|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x75 and fm2 == 0x7fb000 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000014 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001e78]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001e7c]:csrrs a4, fcsr, zero<br> [0x80001e80]:sw t6, 376(a1)<br>     |
| 200|[0x80006548]<br>0x807F8004|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x76 and fm2 == 0x7ffc00 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001ecc]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001ed0]:csrrs a4, fcsr, zero<br> [0x80001ed4]:sw t6, 384(a1)<br>     |
| 201|[0x80006550]<br>0x807F006C|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x77 and fm2 == 0x7fca00 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000036 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001f20]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001f24]:csrrs a4, fcsr, zero<br> [0x80001f28]:sw t6, 392(a1)<br>     |
| 202|[0x80006558]<br>0x807E0052|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x78 and fm2 == 0x7feb80 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000029 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001f74]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001f78]:csrrs a4, fcsr, zero<br> [0x80001f7c]:sw t6, 400(a1)<br>     |
| 203|[0x80006560]<br>0x807C004A|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x79 and fm2 == 0x7ff6c0 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000025 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80001fc8]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80001fcc]:csrrs a4, fcsr, zero<br> [0x80001fd0]:sw t6, 408(a1)<br>     |
| 204|[0x80006568]<br>0x80780098|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7a and fm2 == 0x7ff680 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00004c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000201c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002020]:csrrs a4, fcsr, zero<br> [0x80002024]:sw t6, 416(a1)<br>     |
| 205|[0x80006570]<br>0x8070003A|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7b and fm2 == 0x7ffe30 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00001d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002070]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002074]:csrrs a4, fcsr, zero<br> [0x80002078]:sw t6, 424(a1)<br>     |
| 206|[0x80006578]<br>0x80600050|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7c and fm2 == 0x7ffec0 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000028 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800020c4]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800020c8]:csrrs a4, fcsr, zero<br> [0x800020cc]:sw t6, 432(a1)<br>     |
| 207|[0x80006580]<br>0x80400016|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7d and fm2 == 0x7fffd4 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00000b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002118]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x8000211c]:csrrs a4, fcsr, zero<br> [0x80002120]:sw t6, 440(a1)<br>     |
| 208|[0x80006588]<br>0x81400050|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000028 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000050 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000216c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002170]:csrrs a4, fcsr, zero<br> [0x80002174]:sw t6, 448(a1)<br>     |
| 209|[0x80006590]<br>0x81400054|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x00002a and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000053 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800021c0]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800021c4]:csrrs a4, fcsr, zero<br> [0x800021c8]:sw t6, 456(a1)<br>     |
| 210|[0x80006598]<br>0x8140001A|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x00000e and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000019 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002214]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002218]:csrrs a4, fcsr, zero<br> [0x8000221c]:sw t6, 464(a1)<br>     |
| 211|[0x800065a0]<br>0x81400038|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x00001e and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000035 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002268]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x8000226c]:csrrs a4, fcsr, zero<br> [0x80002270]:sw t6, 472(a1)<br>     |
| 212|[0x800065a8]<br>0x81400068|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000038 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000061 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800022bc]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800022c0]:csrrs a4, fcsr, zero<br> [0x800022c4]:sw t6, 480(a1)<br>     |
| 213|[0x800065b0]<br>0x81400054|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000032 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000043 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002310]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002314]:csrrs a4, fcsr, zero<br> [0x80002318]:sw t6, 488(a1)<br>     |
| 214|[0x800065b8]<br>0x81400064|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000042 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000043 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002364]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002368]:csrrs a4, fcsr, zero<br> [0x8000236c]:sw t6, 496(a1)<br>     |
| 215|[0x800065c0]<br>0x81400070|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000058 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00002f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800023b8]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800023bc]:csrrs a4, fcsr, zero<br> [0x800023c0]:sw t6, 504(a1)<br>     |
| 216|[0x800065c8]<br>0x814000DC|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x0000ae and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00005c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000240c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002410]:csrrs a4, fcsr, zero<br> [0x80002414]:sw t6, 512(a1)<br>     |
| 217|[0x800065d0]<br>0x81400124|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000112 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000023 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002460]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002464]:csrrs a4, fcsr, zero<br> [0x80002468]:sw t6, 520(a1)<br>     |
| 218|[0x800065d8]<br>0x81400222|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000211 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000022 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800024b4]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800024b8]:csrrs a4, fcsr, zero<br> [0x800024bc]:sw t6, 528(a1)<br>     |
| 219|[0x800065e0]<br>0x81400406|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000403 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000006 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002508]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x8000250c]:csrrs a4, fcsr, zero<br> [0x80002510]:sw t6, 536(a1)<br>     |
| 220|[0x800065e8]<br>0x8140084C|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x000826 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00004c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000255c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002560]:csrrs a4, fcsr, zero<br> [0x80002564]:sw t6, 544(a1)<br>     |
| 221|[0x800065f0]<br>0x81401040|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x001020 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000041 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800025b0]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800025b4]:csrrs a4, fcsr, zero<br> [0x800025b8]:sw t6, 552(a1)<br>     |
| 222|[0x800065f8]<br>0x81402058|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x00202c and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000059 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002604]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002608]:csrrs a4, fcsr, zero<br> [0x8000260c]:sw t6, 560(a1)<br>     |
| 223|[0x80006600]<br>0x8140400C|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x004006 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00000c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002658]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x8000265c]:csrrs a4, fcsr, zero<br> [0x80002660]:sw t6, 568(a1)<br>     |
| 224|[0x80006608]<br>0x8140801C|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x00800e and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00001c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800026ac]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800026b0]:csrrs a4, fcsr, zero<br> [0x800026b4]:sw t6, 576(a1)<br>     |
| 225|[0x80006610]<br>0x8141002C|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x010016 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00002c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002700]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002704]:csrrs a4, fcsr, zero<br> [0x80002708]:sw t6, 584(a1)<br>     |
| 226|[0x80006618]<br>0x8142005E|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x02002f and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00005e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002754]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002758]:csrrs a4, fcsr, zero<br> [0x8000275c]:sw t6, 592(a1)<br>     |
| 227|[0x80006620]<br>0x81440018|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x04000c and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000017 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800027a8]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800027ac]:csrrs a4, fcsr, zero<br> [0x800027b0]:sw t6, 600(a1)<br>     |
| 228|[0x80006628]<br>0x8148001C|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x08000e and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00001d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800027fc]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002800]:csrrs a4, fcsr, zero<br> [0x80002804]:sw t6, 608(a1)<br>     |
| 229|[0x80006630]<br>0x8150003E|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x10001f and fs3 == 0 and fe3 == 0x01 and fm3 == 0x00003e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002850]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002854]:csrrs a4, fcsr, zero<br> [0x80002858]:sw t6, 616(a1)<br>     |
| 230|[0x80006638]<br>0x81600050|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x200028 and fs3 == 0 and fe3 == 0x01 and fm3 == 0x000050 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800028a4]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800028a8]:csrrs a4, fcsr, zero<br> [0x800028ac]:sw t6, 624(a1)<br>     |
| 231|[0x80006640]<br>0xF73C0000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7fff42 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00005d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800028f8]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800028fc]:csrrs a4, fcsr, zero<br> [0x80002900]:sw t6, 632(a1)<br>     |
| 232|[0x80006648]<br>0xF6F20000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7fff84 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00003b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000294c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002950]:csrrs a4, fcsr, zero<br> [0x80002954]:sw t6, 640(a1)<br>     |
| 233|[0x80006650]<br>0xF7190000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7fff62 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00004a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800029a0]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800029a4]:csrrs a4, fcsr, zero<br> [0x800029a8]:sw t6, 648(a1)<br>     |
| 234|[0x80006658]<br>0xF7450000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7fff32 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00005e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800029f4]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800029f8]:csrrs a4, fcsr, zero<br> [0x800029fc]:sw t6, 656(a1)<br>     |
| 235|[0x80006660]<br>0xF6CE0000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7fff88 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00002b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002a48]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002a4c]:csrrs a4, fcsr, zero<br> [0x80002a50]:sw t6, 664(a1)<br>     |
| 236|[0x80006668]<br>0xF64C0000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7fffac and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000009 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002a9c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002aa0]:csrrs a4, fcsr, zero<br> [0x80002aa4]:sw t6, 672(a1)<br>     |
| 237|[0x80006670]<br>0xF7818000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7ffebc and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000061 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002af0]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002af4]:csrrs a4, fcsr, zero<br> [0x80002af8]:sw t6, 680(a1)<br>     |
| 238|[0x80006678]<br>0xF7110000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7ffeee and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000008 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002b44]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002b48]:csrrs a4, fcsr, zero<br> [0x80002b4c]:sw t6, 688(a1)<br>     |
| 239|[0x80006680]<br>0xF7BE8000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7ffd82 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00003e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002b98]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002b9c]:csrrs a4, fcsr, zero<br> [0x80002ba0]:sw t6, 696(a1)<br>     |
| 240|[0x80006688]<br>0xF814C000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7ffbac and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000029 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002bec]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002bf0]:csrrs a4, fcsr, zero<br> [0x80002bf4]:sw t6, 704(a1)<br>     |
| 241|[0x80006690]<br>0xF880A000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7ff7fa and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002c40]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002c44]:csrrs a4, fcsr, zero<br> [0x80002c48]:sw t6, 712(a1)<br>     |
| 242|[0x80006698]<br>0xF90BB000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7fef44 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00005d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002c94]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002c98]:csrrs a4, fcsr, zero<br> [0x80002c9c]:sw t6, 720(a1)<br>     |
| 243|[0x800066a0]<br>0xF9832800|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7fdf9a and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000032 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002ce8]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002cec]:csrrs a4, fcsr, zero<br> [0x80002cf0]:sw t6, 728(a1)<br>     |
| 244|[0x800066a8]<br>0xFA007400|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7fbfe2 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00000e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002d3c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002d40]:csrrs a4, fcsr, zero<br> [0x80002d44]:sw t6, 736(a1)<br>     |
| 245|[0x800066b0]<br>0xFA804E00|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7f7fd8 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000013 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002d90]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002d94]:csrrs a4, fcsr, zero<br> [0x80002d98]:sw t6, 744(a1)<br>     |
| 246|[0x800066b8]<br>0xFB006100|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7eff9e and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000030 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002de4]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002de8]:csrrs a4, fcsr, zero<br> [0x80002dec]:sw t6, 752(a1)<br>     |
| 247|[0x800066c0]<br>0xFB806080|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7dff3e and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000060 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002e38]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002e3c]:csrrs a4, fcsr, zero<br> [0x80002e40]:sw t6, 760(a1)<br>     |
| 248|[0x800066c8]<br>0xFC0001C0|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x7bfff8 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000003 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002e8c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002e90]:csrrs a4, fcsr, zero<br> [0x80002e94]:sw t6, 768(a1)<br>     |
| 249|[0x800066d0]<br>0xFC8003A0|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x77ffe2 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00000e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002ee0]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002ee4]:csrrs a4, fcsr, zero<br> [0x80002ee8]:sw t6, 776(a1)<br>     |
| 250|[0x800066d8]<br>0xFD000610|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x6fff9e and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000030 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002f34]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002f38]:csrrs a4, fcsr, zero<br> [0x80002f3c]:sw t6, 784(a1)<br>     |
| 251|[0x800066e0]<br>0xFD800118|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x5fffdc and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000011 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002f88]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002f8c]:csrrs a4, fcsr, zero<br> [0x80002f90]:sw t6, 792(a1)<br>     |
| 252|[0x800066e8]<br>0xFE000114|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x3fffba and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000022 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80002fdc]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80002fe0]:csrrs a4, fcsr, zero<br> [0x80002fe4]:sw t6, 800(a1)<br>     |
| 253|[0x800066f0]<br>0xFE8000FA|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7d and fm2 == 0x7fff04 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00003e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003030]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80003034]:csrrs a4, fcsr, zero<br> [0x80003038]:sw t6, 808(a1)<br>     |
| 254|[0x800066f8]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x400019 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000034 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003084]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80003088]:csrrs a4, fcsr, zero<br> [0x8000308c]:sw t6, 816(a1)<br>     |
| 255|[0x80006700]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x400022 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000046 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800030d8]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800030dc]:csrrs a4, fcsr, zero<br> [0x800030e0]:sw t6, 824(a1)<br>     |
| 256|[0x80006708]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x40001a and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000039 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000312c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80003130]:csrrs a4, fcsr, zero<br> [0x80003134]:sw t6, 832(a1)<br>     |
| 257|[0x80006710]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x40000c and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000021 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003180]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80003184]:csrrs a4, fcsr, zero<br> [0x80003188]:sw t6, 840(a1)<br>     |
| 258|[0x80006718]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x40000a and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000026 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800031d4]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800031d8]:csrrs a4, fcsr, zero<br> [0x800031dc]:sw t6, 848(a1)<br>     |
| 259|[0x80006720]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x40000c and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00003a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003228]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x8000322c]:csrrs a4, fcsr, zero<br> [0x80003230]:sw t6, 856(a1)<br>     |
| 260|[0x80006728]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x400000 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000041 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000327c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80003280]:csrrs a4, fcsr, zero<br> [0x80003284]:sw t6, 864(a1)<br>     |
| 261|[0x80006730]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3fffea and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000055 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800032d0]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800032d4]:csrrs a4, fcsr, zero<br> [0x800032d8]:sw t6, 872(a1)<br>     |
| 262|[0x80006738]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3fffa2 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000044 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003324]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80003328]:csrrs a4, fcsr, zero<br> [0x8000332c]:sw t6, 880(a1)<br>     |
| 263|[0x80006740]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3fff24 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000048 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003378]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x8000337c]:csrrs a4, fcsr, zero<br> [0x80003380]:sw t6, 888(a1)<br>     |
| 264|[0x80006748]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3ffe0a and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000016 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800033cc]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800033d0]:csrrs a4, fcsr, zero<br> [0x800033d4]:sw t6, 896(a1)<br>     |
| 265|[0x80006750]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3ffc14 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00002a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003420]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80003424]:csrrs a4, fcsr, zero<br> [0x80003428]:sw t6, 904(a1)<br>     |
| 266|[0x80006758]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3ff80d and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00001b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003474]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80003478]:csrrs a4, fcsr, zero<br> [0x8000347c]:sw t6, 912(a1)<br>     |
| 267|[0x80006760]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3ff00c and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000019 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800034c8]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800034cc]:csrrs a4, fcsr, zero<br> [0x800034d0]:sw t6, 920(a1)<br>     |
| 268|[0x80006768]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3fe012 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000024 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000351c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80003520]:csrrs a4, fcsr, zero<br> [0x80003524]:sw t6, 928(a1)<br>     |
| 269|[0x80006770]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3fc011 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x000023 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003570]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80003574]:csrrs a4, fcsr, zero<br> [0x80003578]:sw t6, 936(a1)<br>     |
| 270|[0x80006778]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3f802c and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00005a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800035c4]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800035c8]:csrrs a4, fcsr, zero<br> [0x800035cc]:sw t6, 944(a1)<br>     |
| 271|[0x80006780]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3f000e and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00001c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003618]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x8000361c]:csrrs a4, fcsr, zero<br> [0x80003620]:sw t6, 952(a1)<br>     |
| 272|[0x80006788]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3e0026 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00004d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x8000366c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80003670]:csrrs a4, fcsr, zero<br> [0x80003674]:sw t6, 960(a1)<br>     |
| 273|[0x80006790]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3c0006 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00000c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800036c0]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800036c4]:csrrs a4, fcsr, zero<br> [0x800036c8]:sw t6, 968(a1)<br>     |
| 274|[0x80006798]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x380024 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00004a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003714]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80003718]:csrrs a4, fcsr, zero<br> [0x8000371c]:sw t6, 976(a1)<br>     |
| 275|[0x800067a0]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x300016 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00002d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003768]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x8000376c]:csrrs a4, fcsr, zero<br> [0x80003770]:sw t6, 984(a1)<br>     |
| 276|[0x800067a8]<br>0xFF800000|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x20001e and fs3 == 0 and fe3 == 0xfe and fm3 == 0x00003e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800037bc]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800037c0]:csrrs a4, fcsr, zero<br> [0x800037c4]:sw t6, 992(a1)<br>     |
| 277|[0x800067b8]<br>0x80000005|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x068650 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000005 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003864]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80003868]:csrrs a4, fcsr, zero<br> [0x8000386c]:sw t6, 1008(a1)<br>    |
| 278|[0x800067c0]<br>0x80000062|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x85 and fm2 == 0x7c3bd5 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000062 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800038b8]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800038bc]:csrrs a4, fcsr, zero<br> [0x800038c0]:sw t6, 1016(a1)<br>    |
| 279|[0x800067d0]<br>0x80000023|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x068650 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000023 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003968]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x8000396c]:csrrs a4, fcsr, zero<br> [0x80003970]:sw t6, 8(a1)<br>       |
| 280|[0x800067d8]<br>0x8000001A|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x84 and fm2 == 0x54ff53 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x800039bc]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x800039c0]:csrrs a4, fcsr, zero<br> [0x800039c4]:sw t6, 16(a1)<br>      |
| 281|[0x800067e0]<br>0x80000045|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x85 and fm2 == 0x255a6d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000045 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003a10]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80003a14]:csrrs a4, fcsr, zero<br> [0x80003a18]:sw t6, 24(a1)<br>      |
| 282|[0x800067e8]<br>0x80000019|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x87 and fm2 == 0x21d998 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000019 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003a64]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80003a68]:csrrs a4, fcsr, zero<br> [0x80003a6c]:sw t6, 32(a1)<br>      |
| 283|[0x800067f0]<br>0x80000055|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x88 and fm2 == 0x1596af and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000055 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003ab8]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80003abc]:csrrs a4, fcsr, zero<br> [0x80003ac0]:sw t6, 40(a1)<br>      |
| 284|[0x80006800]<br>0x8000000C|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x8a and fm2 == 0x3250b3 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00000c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003b60]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80003b64]:csrrs a4, fcsr, zero<br> [0x80003b68]:sw t6, 56(a1)<br>      |
| 285|[0x80006808]<br>0x8000001D|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x86 and fm2 == 0x02521d and fs3 == 0 and fe3 == 0x00 and fm3 == 0x00001d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003bb4]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80003bb8]:csrrs a4, fcsr, zero<br> [0x80003bbc]:sw t6, 64(a1)<br>      |
| 286|[0x80006818]<br>0x80000003|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x89 and fm2 == 0x33e446 and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000003 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                                                         |[0x80003c5c]:fnmadd.s t6, t5, t4, t3, dyn<br> [0x80003c60]:csrrs a4, fcsr, zero<br> [0x80003c64]:sw t6, 80(a1)<br>      |
