
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800016c0')]      |
| SIG_REGION                | [('0x80003510', '0x80003b40', '396 words')]      |
| COV_LABELS                | fsqrt_b9      |
| TEST_NAME                 | /home/reg/work/zfinx/fsqrt.s/work/fsqrt_b9-01.S/ref.S    |
| Total Number of coverpoints| 261     |
| Total Coverpoints Hit     | 261      |
| Total Signature Updates   | 394      |
| STAT1                     | 195      |
| STAT2                     | 2      |
| STAT3                     | 0     |
| STAT4                     | 197     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80001670]:fsqrt.s t6, t5, dyn
      [0x80001674]:csrrs a0, fcsr, zero
      [0x80001678]:sw t6, 320(t2)
 -- Signature Address: 0x80003b20 Data: 0x1FFFFFFF
 -- Redundant Coverpoints hit by the op
      - mnemonic : fsqrt.s
      - rs1 : x30
      - rd : x31
      - rs1 != rd
      - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x800016a8]:fsqrt.s t6, t5, dyn
      [0x800016ac]:csrrs a0, fcsr, zero
      [0x800016b0]:sw t6, 336(t2)
 -- Signature Address: 0x80003b30 Data: 0x1FFFFF80
 -- Redundant Coverpoints hit by the op
      - mnemonic : fsqrt.s
      - rs1 : x30
      - rd : x31
      - rs1 != rd
      - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fff80 and  fcsr == 0x0 and rm_val == 7   #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fsqrt.s', 'rs1 : x30', 'rd : x31', 'rs1 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000120]:fsqrt.s t6, t5, dyn
	-[0x80000124]:csrrs tp, fcsr, zero
	-[0x80000128]:sw t6, 0(ra)
Current Store : [0x8000012c] : sw tp, 4(ra) -- Store: [0x80003514]:0x00000000




Last Coverpoint : ['rs1 : x29', 'rd : x29', 'rs1 == rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000013c]:fsqrt.s t4, t4, dyn
	-[0x80000140]:csrrs tp, fcsr, zero
	-[0x80000144]:sw t4, 8(ra)
Current Store : [0x80000148] : sw tp, 12(ra) -- Store: [0x8000351c]:0x00000001




Last Coverpoint : ['rs1 : x31', 'rd : x30', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x400000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000158]:fsqrt.s t5, t6, dyn
	-[0x8000015c]:csrrs tp, fcsr, zero
	-[0x80000160]:sw t5, 16(ra)
Current Store : [0x80000164] : sw tp, 20(ra) -- Store: [0x80003524]:0x00000001




Last Coverpoint : ['rs1 : x27', 'rd : x28', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000174]:fsqrt.s t3, s11, dyn
	-[0x80000178]:csrrs tp, fcsr, zero
	-[0x8000017c]:sw t3, 24(ra)
Current Store : [0x80000180] : sw tp, 28(ra) -- Store: [0x8000352c]:0x00000001




Last Coverpoint : ['rs1 : x28', 'rd : x27', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x600000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000190]:fsqrt.s s11, t3, dyn
	-[0x80000194]:csrrs tp, fcsr, zero
	-[0x80000198]:sw s11, 32(ra)
Current Store : [0x8000019c] : sw tp, 36(ra) -- Store: [0x80003534]:0x00000001




Last Coverpoint : ['rs1 : x25', 'rd : x26', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x1fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001ac]:fsqrt.s s10, s9, dyn
	-[0x800001b0]:csrrs tp, fcsr, zero
	-[0x800001b4]:sw s10, 40(ra)
Current Store : [0x800001b8] : sw tp, 44(ra) -- Store: [0x8000353c]:0x00000001




Last Coverpoint : ['rs1 : x26', 'rd : x25', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x700000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001c8]:fsqrt.s s9, s10, dyn
	-[0x800001cc]:csrrs tp, fcsr, zero
	-[0x800001d0]:sw s9, 48(ra)
Current Store : [0x800001d4] : sw tp, 52(ra) -- Store: [0x80003544]:0x00000001




Last Coverpoint : ['rs1 : x23', 'rd : x24', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x0fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001e4]:fsqrt.s s8, s7, dyn
	-[0x800001e8]:csrrs tp, fcsr, zero
	-[0x800001ec]:sw s8, 56(ra)
Current Store : [0x800001f0] : sw tp, 60(ra) -- Store: [0x8000354c]:0x00000001




Last Coverpoint : ['rs1 : x24', 'rd : x23', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x780000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000200]:fsqrt.s s7, s8, dyn
	-[0x80000204]:csrrs tp, fcsr, zero
	-[0x80000208]:sw s7, 64(ra)
Current Store : [0x8000020c] : sw tp, 68(ra) -- Store: [0x80003554]:0x00000001




Last Coverpoint : ['rs1 : x21', 'rd : x22', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x07ffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000021c]:fsqrt.s s6, s5, dyn
	-[0x80000220]:csrrs tp, fcsr, zero
	-[0x80000224]:sw s6, 72(ra)
Current Store : [0x80000228] : sw tp, 76(ra) -- Store: [0x8000355c]:0x00000001




Last Coverpoint : ['rs1 : x22', 'rd : x21', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7c0000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000238]:fsqrt.s s5, s6, dyn
	-[0x8000023c]:csrrs tp, fcsr, zero
	-[0x80000240]:sw s5, 80(ra)
Current Store : [0x80000244] : sw tp, 84(ra) -- Store: [0x80003564]:0x00000001




Last Coverpoint : ['rs1 : x19', 'rd : x20', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x03ffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000254]:fsqrt.s s4, s3, dyn
	-[0x80000258]:csrrs tp, fcsr, zero
	-[0x8000025c]:sw s4, 88(ra)
Current Store : [0x80000260] : sw tp, 92(ra) -- Store: [0x8000356c]:0x00000001




Last Coverpoint : ['rs1 : x20', 'rd : x19', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7e0000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000270]:fsqrt.s s3, s4, dyn
	-[0x80000274]:csrrs tp, fcsr, zero
	-[0x80000278]:sw s3, 96(ra)
Current Store : [0x8000027c] : sw tp, 100(ra) -- Store: [0x80003574]:0x00000001




Last Coverpoint : ['rs1 : x17', 'rd : x18', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x01ffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000028c]:fsqrt.s s2, a7, dyn
	-[0x80000290]:csrrs tp, fcsr, zero
	-[0x80000294]:sw s2, 104(ra)
Current Store : [0x80000298] : sw tp, 108(ra) -- Store: [0x8000357c]:0x00000001




Last Coverpoint : ['rs1 : x18', 'rd : x17', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7f0000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002a8]:fsqrt.s a7, s2, dyn
	-[0x800002ac]:csrrs tp, fcsr, zero
	-[0x800002b0]:sw a7, 112(ra)
Current Store : [0x800002b4] : sw tp, 116(ra) -- Store: [0x80003584]:0x00000001




Last Coverpoint : ['rs1 : x15', 'rd : x16', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x00ffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002c4]:fsqrt.s a6, a5, dyn
	-[0x800002c8]:csrrs tp, fcsr, zero
	-[0x800002cc]:sw a6, 120(ra)
Current Store : [0x800002d0] : sw tp, 124(ra) -- Store: [0x8000358c]:0x00000001




Last Coverpoint : ['rs1 : x16', 'rd : x15', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7f8000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002e0]:fsqrt.s a5, a6, dyn
	-[0x800002e4]:csrrs tp, fcsr, zero
	-[0x800002e8]:sw a5, 128(ra)
Current Store : [0x800002ec] : sw tp, 132(ra) -- Store: [0x80003594]:0x00000001




Last Coverpoint : ['rs1 : x13', 'rd : x14', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x007fff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002fc]:fsqrt.s a4, a3, dyn
	-[0x80000300]:csrrs tp, fcsr, zero
	-[0x80000304]:sw a4, 136(ra)
Current Store : [0x80000308] : sw tp, 140(ra) -- Store: [0x8000359c]:0x00000001




Last Coverpoint : ['rs1 : x14', 'rd : x13', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fc000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000318]:fsqrt.s a3, a4, dyn
	-[0x8000031c]:csrrs tp, fcsr, zero
	-[0x80000320]:sw a3, 144(ra)
Current Store : [0x80000324] : sw tp, 148(ra) -- Store: [0x800035a4]:0x00000001




Last Coverpoint : ['rs1 : x11', 'rd : x12', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x003fff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000334]:fsqrt.s a2, a1, dyn
	-[0x80000338]:csrrs tp, fcsr, zero
	-[0x8000033c]:sw a2, 152(ra)
Current Store : [0x80000340] : sw tp, 156(ra) -- Store: [0x800035ac]:0x00000001




Last Coverpoint : ['rs1 : x12', 'rd : x11', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fe000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000350]:fsqrt.s a1, a2, dyn
	-[0x80000354]:csrrs tp, fcsr, zero
	-[0x80000358]:sw a1, 160(ra)
Current Store : [0x8000035c] : sw tp, 164(ra) -- Store: [0x800035b4]:0x00000001




Last Coverpoint : ['rs1 : x9', 'rd : x10', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x001fff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000036c]:fsqrt.s a0, s1, dyn
	-[0x80000370]:csrrs tp, fcsr, zero
	-[0x80000374]:sw a0, 168(ra)
Current Store : [0x80000378] : sw tp, 172(ra) -- Store: [0x800035bc]:0x00000001




Last Coverpoint : ['rs1 : x10', 'rd : x9', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ff000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000388]:fsqrt.s s1, a0, dyn
	-[0x8000038c]:csrrs tp, fcsr, zero
	-[0x80000390]:sw s1, 176(ra)
Current Store : [0x80000394] : sw tp, 180(ra) -- Store: [0x800035c4]:0x00000001




Last Coverpoint : ['rs1 : x7', 'rd : x8', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000fff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003a4]:fsqrt.s fp, t2, dyn
	-[0x800003a8]:csrrs tp, fcsr, zero
	-[0x800003ac]:sw fp, 184(ra)
Current Store : [0x800003b0] : sw tp, 188(ra) -- Store: [0x800035cc]:0x00000001




Last Coverpoint : ['rs1 : x8', 'rd : x7', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ff800 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003c8]:fsqrt.s t2, fp, dyn
	-[0x800003cc]:csrrs a0, fcsr, zero
	-[0x800003d0]:sw t2, 192(ra)
Current Store : [0x800003d4] : sw a0, 196(ra) -- Store: [0x800035d4]:0x00000001




Last Coverpoint : ['rs1 : x5', 'rd : x6', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007ff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003e4]:fsqrt.s t1, t0, dyn
	-[0x800003e8]:csrrs a0, fcsr, zero
	-[0x800003ec]:sw t1, 200(ra)
Current Store : [0x800003f0] : sw a0, 204(ra) -- Store: [0x800035dc]:0x00000001




Last Coverpoint : ['rs1 : x6', 'rd : x5', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ffc00 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000408]:fsqrt.s t0, t1, dyn
	-[0x8000040c]:csrrs a0, fcsr, zero
	-[0x80000410]:sw t0, 0(t2)
Current Store : [0x80000414] : sw a0, 4(t2) -- Store: [0x800035e4]:0x00000001




Last Coverpoint : ['rs1 : x3', 'rd : x4', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x0003ff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000424]:fsqrt.s tp, gp, dyn
	-[0x80000428]:csrrs a0, fcsr, zero
	-[0x8000042c]:sw tp, 8(t2)
Current Store : [0x80000430] : sw a0, 12(t2) -- Store: [0x800035ec]:0x00000001




Last Coverpoint : ['rs1 : x4', 'rd : x3', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ffe00 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000440]:fsqrt.s gp, tp, dyn
	-[0x80000444]:csrrs a0, fcsr, zero
	-[0x80000448]:sw gp, 16(t2)
Current Store : [0x8000044c] : sw a0, 20(t2) -- Store: [0x800035f4]:0x00000001




Last Coverpoint : ['rs1 : x1', 'rd : x2', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x0001ff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000045c]:fsqrt.s sp, ra, dyn
	-[0x80000460]:csrrs a0, fcsr, zero
	-[0x80000464]:sw sp, 24(t2)
Current Store : [0x80000468] : sw a0, 28(t2) -- Store: [0x800035fc]:0x00000001




Last Coverpoint : ['rs1 : x2', 'rd : x1', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fff00 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000478]:fsqrt.s ra, sp, dyn
	-[0x8000047c]:csrrs a0, fcsr, zero
	-[0x80000480]:sw ra, 32(t2)
Current Store : [0x80000484] : sw a0, 36(t2) -- Store: [0x80003604]:0x00000001




Last Coverpoint : ['rs1 : x0']
Last Code Sequence : 
	-[0x80000494]:fsqrt.s t6, zero, dyn
	-[0x80000498]:csrrs a0, fcsr, zero
	-[0x8000049c]:sw t6, 40(t2)
Current Store : [0x800004a0] : sw a0, 44(t2) -- Store: [0x8000360c]:0x00000000




Last Coverpoint : ['rd : x0', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fff80 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004b0]:fsqrt.s zero, t6, dyn
	-[0x800004b4]:csrrs a0, fcsr, zero
	-[0x800004b8]:sw zero, 48(t2)
Current Store : [0x800004bc] : sw a0, 52(t2) -- Store: [0x80003614]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x00007f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004cc]:fsqrt.s t6, t5, dyn
	-[0x800004d0]:csrrs a0, fcsr, zero
	-[0x800004d4]:sw t6, 56(t2)
Current Store : [0x800004d8] : sw a0, 60(t2) -- Store: [0x8000361c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffc0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004e8]:fsqrt.s t6, t5, dyn
	-[0x800004ec]:csrrs a0, fcsr, zero
	-[0x800004f0]:sw t6, 64(t2)
Current Store : [0x800004f4] : sw a0, 68(t2) -- Store: [0x80003624]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000504]:fsqrt.s t6, t5, dyn
	-[0x80000508]:csrrs a0, fcsr, zero
	-[0x8000050c]:sw t6, 72(t2)
Current Store : [0x80000510] : sw a0, 76(t2) -- Store: [0x8000362c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffe0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000520]:fsqrt.s t6, t5, dyn
	-[0x80000524]:csrrs a0, fcsr, zero
	-[0x80000528]:sw t6, 80(t2)
Current Store : [0x8000052c] : sw a0, 84(t2) -- Store: [0x80003634]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x00001f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000053c]:fsqrt.s t6, t5, dyn
	-[0x80000540]:csrrs a0, fcsr, zero
	-[0x80000544]:sw t6, 88(t2)
Current Store : [0x80000548] : sw a0, 92(t2) -- Store: [0x8000363c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ffff0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000558]:fsqrt.s t6, t5, dyn
	-[0x8000055c]:csrrs a0, fcsr, zero
	-[0x80000560]:sw t6, 96(t2)
Current Store : [0x80000564] : sw a0, 100(t2) -- Store: [0x80003644]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x00000f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000574]:fsqrt.s t6, t5, dyn
	-[0x80000578]:csrrs a0, fcsr, zero
	-[0x8000057c]:sw t6, 104(t2)
Current Store : [0x80000580] : sw a0, 108(t2) -- Store: [0x8000364c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ffff8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000590]:fsqrt.s t6, t5, dyn
	-[0x80000594]:csrrs a0, fcsr, zero
	-[0x80000598]:sw t6, 112(t2)
Current Store : [0x8000059c] : sw a0, 116(t2) -- Store: [0x80003654]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000007 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005ac]:fsqrt.s t6, t5, dyn
	-[0x800005b0]:csrrs a0, fcsr, zero
	-[0x800005b4]:sw t6, 120(t2)
Current Store : [0x800005b8] : sw a0, 124(t2) -- Store: [0x8000365c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ffffc and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005c8]:fsqrt.s t6, t5, dyn
	-[0x800005cc]:csrrs a0, fcsr, zero
	-[0x800005d0]:sw t6, 128(t2)
Current Store : [0x800005d4] : sw a0, 132(t2) -- Store: [0x80003664]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000003 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005e4]:fsqrt.s t6, t5, dyn
	-[0x800005e8]:csrrs a0, fcsr, zero
	-[0x800005ec]:sw t6, 136(t2)
Current Store : [0x800005f0] : sw a0, 140(t2) -- Store: [0x8000366c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ffffe and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000600]:fsqrt.s t6, t5, dyn
	-[0x80000604]:csrrs a0, fcsr, zero
	-[0x80000608]:sw t6, 144(t2)
Current Store : [0x8000060c] : sw a0, 148(t2) -- Store: [0x80003674]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000061c]:fsqrt.s t6, t5, dyn
	-[0x80000620]:csrrs a0, fcsr, zero
	-[0x80000624]:sw t6, 152(t2)
Current Store : [0x80000628] : sw a0, 156(t2) -- Store: [0x8000367c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000638]:fsqrt.s t6, t5, dyn
	-[0x8000063c]:csrrs a0, fcsr, zero
	-[0x80000640]:sw t6, 160(t2)
Current Store : [0x80000644] : sw a0, 164(t2) -- Store: [0x80003684]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000654]:fsqrt.s t6, t5, dyn
	-[0x80000658]:csrrs a0, fcsr, zero
	-[0x8000065c]:sw t6, 168(t2)
Current Store : [0x80000660] : sw a0, 172(t2) -- Store: [0x8000368c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x400000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000670]:fsqrt.s t6, t5, dyn
	-[0x80000674]:csrrs a0, fcsr, zero
	-[0x80000678]:sw t6, 176(t2)
Current Store : [0x8000067c] : sw a0, 180(t2) -- Store: [0x80003694]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x3fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000068c]:fsqrt.s t6, t5, dyn
	-[0x80000690]:csrrs a0, fcsr, zero
	-[0x80000694]:sw t6, 184(t2)
Current Store : [0x80000698] : sw a0, 188(t2) -- Store: [0x8000369c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x600000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006a8]:fsqrt.s t6, t5, dyn
	-[0x800006ac]:csrrs a0, fcsr, zero
	-[0x800006b0]:sw t6, 192(t2)
Current Store : [0x800006b4] : sw a0, 196(t2) -- Store: [0x800036a4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x1fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006c4]:fsqrt.s t6, t5, dyn
	-[0x800006c8]:csrrs a0, fcsr, zero
	-[0x800006cc]:sw t6, 200(t2)
Current Store : [0x800006d0] : sw a0, 204(t2) -- Store: [0x800036ac]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x700000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006e0]:fsqrt.s t6, t5, dyn
	-[0x800006e4]:csrrs a0, fcsr, zero
	-[0x800006e8]:sw t6, 208(t2)
Current Store : [0x800006ec] : sw a0, 212(t2) -- Store: [0x800036b4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006fc]:fsqrt.s t6, t5, dyn
	-[0x80000700]:csrrs a0, fcsr, zero
	-[0x80000704]:sw t6, 216(t2)
Current Store : [0x80000708] : sw a0, 220(t2) -- Store: [0x800036bc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x780000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000718]:fsqrt.s t6, t5, dyn
	-[0x8000071c]:csrrs a0, fcsr, zero
	-[0x80000720]:sw t6, 224(t2)
Current Store : [0x80000724] : sw a0, 228(t2) -- Store: [0x800036c4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x07ffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000734]:fsqrt.s t6, t5, dyn
	-[0x80000738]:csrrs a0, fcsr, zero
	-[0x8000073c]:sw t6, 232(t2)
Current Store : [0x80000740] : sw a0, 236(t2) -- Store: [0x800036cc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7c0000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000750]:fsqrt.s t6, t5, dyn
	-[0x80000754]:csrrs a0, fcsr, zero
	-[0x80000758]:sw t6, 240(t2)
Current Store : [0x8000075c] : sw a0, 244(t2) -- Store: [0x800036d4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x03ffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000076c]:fsqrt.s t6, t5, dyn
	-[0x80000770]:csrrs a0, fcsr, zero
	-[0x80000774]:sw t6, 248(t2)
Current Store : [0x80000778] : sw a0, 252(t2) -- Store: [0x800036dc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7e0000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000788]:fsqrt.s t6, t5, dyn
	-[0x8000078c]:csrrs a0, fcsr, zero
	-[0x80000790]:sw t6, 256(t2)
Current Store : [0x80000794] : sw a0, 260(t2) -- Store: [0x800036e4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x01ffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007a4]:fsqrt.s t6, t5, dyn
	-[0x800007a8]:csrrs a0, fcsr, zero
	-[0x800007ac]:sw t6, 264(t2)
Current Store : [0x800007b0] : sw a0, 268(t2) -- Store: [0x800036ec]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7f0000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007c0]:fsqrt.s t6, t5, dyn
	-[0x800007c4]:csrrs a0, fcsr, zero
	-[0x800007c8]:sw t6, 272(t2)
Current Store : [0x800007cc] : sw a0, 276(t2) -- Store: [0x800036f4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x00ffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007dc]:fsqrt.s t6, t5, dyn
	-[0x800007e0]:csrrs a0, fcsr, zero
	-[0x800007e4]:sw t6, 280(t2)
Current Store : [0x800007e8] : sw a0, 284(t2) -- Store: [0x800036fc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7f8000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007f8]:fsqrt.s t6, t5, dyn
	-[0x800007fc]:csrrs a0, fcsr, zero
	-[0x80000800]:sw t6, 288(t2)
Current Store : [0x80000804] : sw a0, 292(t2) -- Store: [0x80003704]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x007fff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000814]:fsqrt.s t6, t5, dyn
	-[0x80000818]:csrrs a0, fcsr, zero
	-[0x8000081c]:sw t6, 296(t2)
Current Store : [0x80000820] : sw a0, 300(t2) -- Store: [0x8000370c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fc000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000830]:fsqrt.s t6, t5, dyn
	-[0x80000834]:csrrs a0, fcsr, zero
	-[0x80000838]:sw t6, 304(t2)
Current Store : [0x8000083c] : sw a0, 308(t2) -- Store: [0x80003714]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x003fff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000084c]:fsqrt.s t6, t5, dyn
	-[0x80000850]:csrrs a0, fcsr, zero
	-[0x80000854]:sw t6, 312(t2)
Current Store : [0x80000858] : sw a0, 316(t2) -- Store: [0x8000371c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fe000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000868]:fsqrt.s t6, t5, dyn
	-[0x8000086c]:csrrs a0, fcsr, zero
	-[0x80000870]:sw t6, 320(t2)
Current Store : [0x80000874] : sw a0, 324(t2) -- Store: [0x80003724]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x001fff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000884]:fsqrt.s t6, t5, dyn
	-[0x80000888]:csrrs a0, fcsr, zero
	-[0x8000088c]:sw t6, 328(t2)
Current Store : [0x80000890] : sw a0, 332(t2) -- Store: [0x8000372c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ff000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008a0]:fsqrt.s t6, t5, dyn
	-[0x800008a4]:csrrs a0, fcsr, zero
	-[0x800008a8]:sw t6, 336(t2)
Current Store : [0x800008ac] : sw a0, 340(t2) -- Store: [0x80003734]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000fff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008bc]:fsqrt.s t6, t5, dyn
	-[0x800008c0]:csrrs a0, fcsr, zero
	-[0x800008c4]:sw t6, 344(t2)
Current Store : [0x800008c8] : sw a0, 348(t2) -- Store: [0x8000373c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ff800 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008d8]:fsqrt.s t6, t5, dyn
	-[0x800008dc]:csrrs a0, fcsr, zero
	-[0x800008e0]:sw t6, 352(t2)
Current Store : [0x800008e4] : sw a0, 356(t2) -- Store: [0x80003744]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0007ff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008f4]:fsqrt.s t6, t5, dyn
	-[0x800008f8]:csrrs a0, fcsr, zero
	-[0x800008fc]:sw t6, 360(t2)
Current Store : [0x80000900] : sw a0, 364(t2) -- Store: [0x8000374c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ffc00 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000910]:fsqrt.s t6, t5, dyn
	-[0x80000914]:csrrs a0, fcsr, zero
	-[0x80000918]:sw t6, 368(t2)
Current Store : [0x8000091c] : sw a0, 372(t2) -- Store: [0x80003754]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0003ff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000092c]:fsqrt.s t6, t5, dyn
	-[0x80000930]:csrrs a0, fcsr, zero
	-[0x80000934]:sw t6, 376(t2)
Current Store : [0x80000938] : sw a0, 380(t2) -- Store: [0x8000375c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ffe00 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000948]:fsqrt.s t6, t5, dyn
	-[0x8000094c]:csrrs a0, fcsr, zero
	-[0x80000950]:sw t6, 384(t2)
Current Store : [0x80000954] : sw a0, 388(t2) -- Store: [0x80003764]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0001ff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000964]:fsqrt.s t6, t5, dyn
	-[0x80000968]:csrrs a0, fcsr, zero
	-[0x8000096c]:sw t6, 392(t2)
Current Store : [0x80000970] : sw a0, 396(t2) -- Store: [0x8000376c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fff00 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000980]:fsqrt.s t6, t5, dyn
	-[0x80000984]:csrrs a0, fcsr, zero
	-[0x80000988]:sw t6, 400(t2)
Current Store : [0x8000098c] : sw a0, 404(t2) -- Store: [0x80003774]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0000ff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000099c]:fsqrt.s t6, t5, dyn
	-[0x800009a0]:csrrs a0, fcsr, zero
	-[0x800009a4]:sw t6, 408(t2)
Current Store : [0x800009a8] : sw a0, 412(t2) -- Store: [0x8000377c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fff80 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009b8]:fsqrt.s t6, t5, dyn
	-[0x800009bc]:csrrs a0, fcsr, zero
	-[0x800009c0]:sw t6, 416(t2)
Current Store : [0x800009c4] : sw a0, 420(t2) -- Store: [0x80003784]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x00007f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009d4]:fsqrt.s t6, t5, dyn
	-[0x800009d8]:csrrs a0, fcsr, zero
	-[0x800009dc]:sw t6, 424(t2)
Current Store : [0x800009e0] : sw a0, 428(t2) -- Store: [0x8000378c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fffc0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009f0]:fsqrt.s t6, t5, dyn
	-[0x800009f4]:csrrs a0, fcsr, zero
	-[0x800009f8]:sw t6, 432(t2)
Current Store : [0x800009fc] : sw a0, 436(t2) -- Store: [0x80003794]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a0c]:fsqrt.s t6, t5, dyn
	-[0x80000a10]:csrrs a0, fcsr, zero
	-[0x80000a14]:sw t6, 440(t2)
Current Store : [0x80000a18] : sw a0, 444(t2) -- Store: [0x8000379c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fffe0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a28]:fsqrt.s t6, t5, dyn
	-[0x80000a2c]:csrrs a0, fcsr, zero
	-[0x80000a30]:sw t6, 448(t2)
Current Store : [0x80000a34] : sw a0, 452(t2) -- Store: [0x800037a4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x00001f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a44]:fsqrt.s t6, t5, dyn
	-[0x80000a48]:csrrs a0, fcsr, zero
	-[0x80000a4c]:sw t6, 456(t2)
Current Store : [0x80000a50] : sw a0, 460(t2) -- Store: [0x800037ac]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ffff0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a60]:fsqrt.s t6, t5, dyn
	-[0x80000a64]:csrrs a0, fcsr, zero
	-[0x80000a68]:sw t6, 464(t2)
Current Store : [0x80000a6c] : sw a0, 468(t2) -- Store: [0x800037b4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x00000f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a7c]:fsqrt.s t6, t5, dyn
	-[0x80000a80]:csrrs a0, fcsr, zero
	-[0x80000a84]:sw t6, 472(t2)
Current Store : [0x80000a88] : sw a0, 476(t2) -- Store: [0x800037bc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ffff8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a98]:fsqrt.s t6, t5, dyn
	-[0x80000a9c]:csrrs a0, fcsr, zero
	-[0x80000aa0]:sw t6, 480(t2)
Current Store : [0x80000aa4] : sw a0, 484(t2) -- Store: [0x800037c4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000007 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ab4]:fsqrt.s t6, t5, dyn
	-[0x80000ab8]:csrrs a0, fcsr, zero
	-[0x80000abc]:sw t6, 488(t2)
Current Store : [0x80000ac0] : sw a0, 492(t2) -- Store: [0x800037cc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ffffc and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ad0]:fsqrt.s t6, t5, dyn
	-[0x80000ad4]:csrrs a0, fcsr, zero
	-[0x80000ad8]:sw t6, 496(t2)
Current Store : [0x80000adc] : sw a0, 500(t2) -- Store: [0x800037d4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000003 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000aec]:fsqrt.s t6, t5, dyn
	-[0x80000af0]:csrrs a0, fcsr, zero
	-[0x80000af4]:sw t6, 504(t2)
Current Store : [0x80000af8] : sw a0, 508(t2) -- Store: [0x800037dc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ffffe and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b08]:fsqrt.s t6, t5, dyn
	-[0x80000b0c]:csrrs a0, fcsr, zero
	-[0x80000b10]:sw t6, 512(t2)
Current Store : [0x80000b14] : sw a0, 516(t2) -- Store: [0x800037e4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b24]:fsqrt.s t6, t5, dyn
	-[0x80000b28]:csrrs a0, fcsr, zero
	-[0x80000b2c]:sw t6, 520(t2)
Current Store : [0x80000b30] : sw a0, 524(t2) -- Store: [0x800037ec]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b40]:fsqrt.s t6, t5, dyn
	-[0x80000b44]:csrrs a0, fcsr, zero
	-[0x80000b48]:sw t6, 528(t2)
Current Store : [0x80000b4c] : sw a0, 532(t2) -- Store: [0x800037f4]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b5c]:fsqrt.s t6, t5, dyn
	-[0x80000b60]:csrrs a0, fcsr, zero
	-[0x80000b64]:sw t6, 536(t2)
Current Store : [0x80000b68] : sw a0, 540(t2) -- Store: [0x800037fc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x400000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b78]:fsqrt.s t6, t5, dyn
	-[0x80000b7c]:csrrs a0, fcsr, zero
	-[0x80000b80]:sw t6, 544(t2)
Current Store : [0x80000b84] : sw a0, 548(t2) -- Store: [0x80003804]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x3fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b94]:fsqrt.s t6, t5, dyn
	-[0x80000b98]:csrrs a0, fcsr, zero
	-[0x80000b9c]:sw t6, 552(t2)
Current Store : [0x80000ba0] : sw a0, 556(t2) -- Store: [0x8000380c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x600000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bb0]:fsqrt.s t6, t5, dyn
	-[0x80000bb4]:csrrs a0, fcsr, zero
	-[0x80000bb8]:sw t6, 560(t2)
Current Store : [0x80000bbc] : sw a0, 564(t2) -- Store: [0x80003814]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x1fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bcc]:fsqrt.s t6, t5, dyn
	-[0x80000bd0]:csrrs a0, fcsr, zero
	-[0x80000bd4]:sw t6, 568(t2)
Current Store : [0x80000bd8] : sw a0, 572(t2) -- Store: [0x8000381c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x700000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000be8]:fsqrt.s t6, t5, dyn
	-[0x80000bec]:csrrs a0, fcsr, zero
	-[0x80000bf0]:sw t6, 576(t2)
Current Store : [0x80000bf4] : sw a0, 580(t2) -- Store: [0x80003824]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c04]:fsqrt.s t6, t5, dyn
	-[0x80000c08]:csrrs a0, fcsr, zero
	-[0x80000c0c]:sw t6, 584(t2)
Current Store : [0x80000c10] : sw a0, 588(t2) -- Store: [0x8000382c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x780000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c20]:fsqrt.s t6, t5, dyn
	-[0x80000c24]:csrrs a0, fcsr, zero
	-[0x80000c28]:sw t6, 592(t2)
Current Store : [0x80000c2c] : sw a0, 596(t2) -- Store: [0x80003834]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x07ffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c3c]:fsqrt.s t6, t5, dyn
	-[0x80000c40]:csrrs a0, fcsr, zero
	-[0x80000c44]:sw t6, 600(t2)
Current Store : [0x80000c48] : sw a0, 604(t2) -- Store: [0x8000383c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x7c0000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c58]:fsqrt.s t6, t5, dyn
	-[0x80000c5c]:csrrs a0, fcsr, zero
	-[0x80000c60]:sw t6, 608(t2)
Current Store : [0x80000c64] : sw a0, 612(t2) -- Store: [0x80003844]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x03ffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c74]:fsqrt.s t6, t5, dyn
	-[0x80000c78]:csrrs a0, fcsr, zero
	-[0x80000c7c]:sw t6, 616(t2)
Current Store : [0x80000c80] : sw a0, 620(t2) -- Store: [0x8000384c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x7e0000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c90]:fsqrt.s t6, t5, dyn
	-[0x80000c94]:csrrs a0, fcsr, zero
	-[0x80000c98]:sw t6, 624(t2)
Current Store : [0x80000c9c] : sw a0, 628(t2) -- Store: [0x80003854]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x01ffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cac]:fsqrt.s t6, t5, dyn
	-[0x80000cb0]:csrrs a0, fcsr, zero
	-[0x80000cb4]:sw t6, 632(t2)
Current Store : [0x80000cb8] : sw a0, 636(t2) -- Store: [0x8000385c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x7f0000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cc8]:fsqrt.s t6, t5, dyn
	-[0x80000ccc]:csrrs a0, fcsr, zero
	-[0x80000cd0]:sw t6, 640(t2)
Current Store : [0x80000cd4] : sw a0, 644(t2) -- Store: [0x80003864]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x00ffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ce4]:fsqrt.s t6, t5, dyn
	-[0x80000ce8]:csrrs a0, fcsr, zero
	-[0x80000cec]:sw t6, 648(t2)
Current Store : [0x80000cf0] : sw a0, 652(t2) -- Store: [0x8000386c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x7f8000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d00]:fsqrt.s t6, t5, dyn
	-[0x80000d04]:csrrs a0, fcsr, zero
	-[0x80000d08]:sw t6, 656(t2)
Current Store : [0x80000d0c] : sw a0, 660(t2) -- Store: [0x80003874]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x007fff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d1c]:fsqrt.s t6, t5, dyn
	-[0x80000d20]:csrrs a0, fcsr, zero
	-[0x80000d24]:sw t6, 664(t2)
Current Store : [0x80000d28] : sw a0, 668(t2) -- Store: [0x8000387c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x7fc000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d38]:fsqrt.s t6, t5, dyn
	-[0x80000d3c]:csrrs a0, fcsr, zero
	-[0x80000d40]:sw t6, 672(t2)
Current Store : [0x80000d44] : sw a0, 676(t2) -- Store: [0x80003884]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x003fff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d54]:fsqrt.s t6, t5, dyn
	-[0x80000d58]:csrrs a0, fcsr, zero
	-[0x80000d5c]:sw t6, 680(t2)
Current Store : [0x80000d60] : sw a0, 684(t2) -- Store: [0x8000388c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x7fe000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d70]:fsqrt.s t6, t5, dyn
	-[0x80000d74]:csrrs a0, fcsr, zero
	-[0x80000d78]:sw t6, 688(t2)
Current Store : [0x80000d7c] : sw a0, 692(t2) -- Store: [0x80003894]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001fff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d8c]:fsqrt.s t6, t5, dyn
	-[0x80000d90]:csrrs a0, fcsr, zero
	-[0x80000d94]:sw t6, 696(t2)
Current Store : [0x80000d98] : sw a0, 700(t2) -- Store: [0x8000389c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x7ff000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000da8]:fsqrt.s t6, t5, dyn
	-[0x80000dac]:csrrs a0, fcsr, zero
	-[0x80000db0]:sw t6, 704(t2)
Current Store : [0x80000db4] : sw a0, 708(t2) -- Store: [0x800038a4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000fff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000dc4]:fsqrt.s t6, t5, dyn
	-[0x80000dc8]:csrrs a0, fcsr, zero
	-[0x80000dcc]:sw t6, 712(t2)
Current Store : [0x80000dd0] : sw a0, 716(t2) -- Store: [0x800038ac]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x7ff800 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000de0]:fsqrt.s t6, t5, dyn
	-[0x80000de4]:csrrs a0, fcsr, zero
	-[0x80000de8]:sw t6, 720(t2)
Current Store : [0x80000dec] : sw a0, 724(t2) -- Store: [0x800038b4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0007ff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000dfc]:fsqrt.s t6, t5, dyn
	-[0x80000e00]:csrrs a0, fcsr, zero
	-[0x80000e04]:sw t6, 728(t2)
Current Store : [0x80000e08] : sw a0, 732(t2) -- Store: [0x800038bc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x7ffc00 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e18]:fsqrt.s t6, t5, dyn
	-[0x80000e1c]:csrrs a0, fcsr, zero
	-[0x80000e20]:sw t6, 736(t2)
Current Store : [0x80000e24] : sw a0, 740(t2) -- Store: [0x800038c4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0003ff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e34]:fsqrt.s t6, t5, dyn
	-[0x80000e38]:csrrs a0, fcsr, zero
	-[0x80000e3c]:sw t6, 744(t2)
Current Store : [0x80000e40] : sw a0, 748(t2) -- Store: [0x800038cc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x7ffe00 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e50]:fsqrt.s t6, t5, dyn
	-[0x80000e54]:csrrs a0, fcsr, zero
	-[0x80000e58]:sw t6, 752(t2)
Current Store : [0x80000e5c] : sw a0, 756(t2) -- Store: [0x800038d4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0001ff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e6c]:fsqrt.s t6, t5, dyn
	-[0x80000e70]:csrrs a0, fcsr, zero
	-[0x80000e74]:sw t6, 760(t2)
Current Store : [0x80000e78] : sw a0, 764(t2) -- Store: [0x800038dc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x7fff00 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e88]:fsqrt.s t6, t5, dyn
	-[0x80000e8c]:csrrs a0, fcsr, zero
	-[0x80000e90]:sw t6, 768(t2)
Current Store : [0x80000e94] : sw a0, 772(t2) -- Store: [0x800038e4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0000ff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ea4]:fsqrt.s t6, t5, dyn
	-[0x80000ea8]:csrrs a0, fcsr, zero
	-[0x80000eac]:sw t6, 776(t2)
Current Store : [0x80000eb0] : sw a0, 780(t2) -- Store: [0x800038ec]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x7fff80 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ec0]:fsqrt.s t6, t5, dyn
	-[0x80000ec4]:csrrs a0, fcsr, zero
	-[0x80000ec8]:sw t6, 784(t2)
Current Store : [0x80000ecc] : sw a0, 788(t2) -- Store: [0x800038f4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x00007f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000edc]:fsqrt.s t6, t5, dyn
	-[0x80000ee0]:csrrs a0, fcsr, zero
	-[0x80000ee4]:sw t6, 792(t2)
Current Store : [0x80000ee8] : sw a0, 796(t2) -- Store: [0x800038fc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x7fffc0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ef8]:fsqrt.s t6, t5, dyn
	-[0x80000efc]:csrrs a0, fcsr, zero
	-[0x80000f00]:sw t6, 800(t2)
Current Store : [0x80000f04] : sw a0, 804(t2) -- Store: [0x80003904]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f14]:fsqrt.s t6, t5, dyn
	-[0x80000f18]:csrrs a0, fcsr, zero
	-[0x80000f1c]:sw t6, 808(t2)
Current Store : [0x80000f20] : sw a0, 812(t2) -- Store: [0x8000390c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x7fffe0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f30]:fsqrt.s t6, t5, dyn
	-[0x80000f34]:csrrs a0, fcsr, zero
	-[0x80000f38]:sw t6, 816(t2)
Current Store : [0x80000f3c] : sw a0, 820(t2) -- Store: [0x80003914]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x00001f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f4c]:fsqrt.s t6, t5, dyn
	-[0x80000f50]:csrrs a0, fcsr, zero
	-[0x80000f54]:sw t6, 824(t2)
Current Store : [0x80000f58] : sw a0, 828(t2) -- Store: [0x8000391c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x7ffff0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f68]:fsqrt.s t6, t5, dyn
	-[0x80000f6c]:csrrs a0, fcsr, zero
	-[0x80000f70]:sw t6, 832(t2)
Current Store : [0x80000f74] : sw a0, 836(t2) -- Store: [0x80003924]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x00000f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f84]:fsqrt.s t6, t5, dyn
	-[0x80000f88]:csrrs a0, fcsr, zero
	-[0x80000f8c]:sw t6, 840(t2)
Current Store : [0x80000f90] : sw a0, 844(t2) -- Store: [0x8000392c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x7ffff8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fa0]:fsqrt.s t6, t5, dyn
	-[0x80000fa4]:csrrs a0, fcsr, zero
	-[0x80000fa8]:sw t6, 848(t2)
Current Store : [0x80000fac] : sw a0, 852(t2) -- Store: [0x80003934]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000007 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fbc]:fsqrt.s t6, t5, dyn
	-[0x80000fc0]:csrrs a0, fcsr, zero
	-[0x80000fc4]:sw t6, 856(t2)
Current Store : [0x80000fc8] : sw a0, 860(t2) -- Store: [0x8000393c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x7ffffc and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fd8]:fsqrt.s t6, t5, dyn
	-[0x80000fdc]:csrrs a0, fcsr, zero
	-[0x80000fe0]:sw t6, 864(t2)
Current Store : [0x80000fe4] : sw a0, 868(t2) -- Store: [0x80003944]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000003 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ff4]:fsqrt.s t6, t5, dyn
	-[0x80000ff8]:csrrs a0, fcsr, zero
	-[0x80000ffc]:sw t6, 872(t2)
Current Store : [0x80001000] : sw a0, 876(t2) -- Store: [0x8000394c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x7ffffe and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001010]:fsqrt.s t6, t5, dyn
	-[0x80001014]:csrrs a0, fcsr, zero
	-[0x80001018]:sw t6, 880(t2)
Current Store : [0x8000101c] : sw a0, 884(t2) -- Store: [0x80003954]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000102c]:fsqrt.s t6, t5, dyn
	-[0x80001030]:csrrs a0, fcsr, zero
	-[0x80001034]:sw t6, 888(t2)
Current Store : [0x80001038] : sw a0, 892(t2) -- Store: [0x8000395c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001048]:fsqrt.s t6, t5, dyn
	-[0x8000104c]:csrrs a0, fcsr, zero
	-[0x80001050]:sw t6, 896(t2)
Current Store : [0x80001054] : sw a0, 900(t2) -- Store: [0x80003964]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001064]:fsqrt.s t6, t5, dyn
	-[0x80001068]:csrrs a0, fcsr, zero
	-[0x8000106c]:sw t6, 904(t2)
Current Store : [0x80001070] : sw a0, 908(t2) -- Store: [0x8000396c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001080]:fsqrt.s t6, t5, dyn
	-[0x80001084]:csrrs a0, fcsr, zero
	-[0x80001088]:sw t6, 912(t2)
Current Store : [0x8000108c] : sw a0, 916(t2) -- Store: [0x80003974]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x400000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000109c]:fsqrt.s t6, t5, dyn
	-[0x800010a0]:csrrs a0, fcsr, zero
	-[0x800010a4]:sw t6, 920(t2)
Current Store : [0x800010a8] : sw a0, 924(t2) -- Store: [0x8000397c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010b8]:fsqrt.s t6, t5, dyn
	-[0x800010bc]:csrrs a0, fcsr, zero
	-[0x800010c0]:sw t6, 928(t2)
Current Store : [0x800010c4] : sw a0, 932(t2) -- Store: [0x80003984]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x600000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010d4]:fsqrt.s t6, t5, dyn
	-[0x800010d8]:csrrs a0, fcsr, zero
	-[0x800010dc]:sw t6, 936(t2)
Current Store : [0x800010e0] : sw a0, 940(t2) -- Store: [0x8000398c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010f0]:fsqrt.s t6, t5, dyn
	-[0x800010f4]:csrrs a0, fcsr, zero
	-[0x800010f8]:sw t6, 944(t2)
Current Store : [0x800010fc] : sw a0, 948(t2) -- Store: [0x80003994]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x700000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000110c]:fsqrt.s t6, t5, dyn
	-[0x80001110]:csrrs a0, fcsr, zero
	-[0x80001114]:sw t6, 952(t2)
Current Store : [0x80001118] : sw a0, 956(t2) -- Store: [0x8000399c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x07ffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001128]:fsqrt.s t6, t5, dyn
	-[0x8000112c]:csrrs a0, fcsr, zero
	-[0x80001130]:sw t6, 960(t2)
Current Store : [0x80001134] : sw a0, 964(t2) -- Store: [0x800039a4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x780000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001144]:fsqrt.s t6, t5, dyn
	-[0x80001148]:csrrs a0, fcsr, zero
	-[0x8000114c]:sw t6, 968(t2)
Current Store : [0x80001150] : sw a0, 972(t2) -- Store: [0x800039ac]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x03ffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001160]:fsqrt.s t6, t5, dyn
	-[0x80001164]:csrrs a0, fcsr, zero
	-[0x80001168]:sw t6, 976(t2)
Current Store : [0x8000116c] : sw a0, 980(t2) -- Store: [0x800039b4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7c0000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000117c]:fsqrt.s t6, t5, dyn
	-[0x80001180]:csrrs a0, fcsr, zero
	-[0x80001184]:sw t6, 984(t2)
Current Store : [0x80001188] : sw a0, 988(t2) -- Store: [0x800039bc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x01ffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001198]:fsqrt.s t6, t5, dyn
	-[0x8000119c]:csrrs a0, fcsr, zero
	-[0x800011a0]:sw t6, 992(t2)
Current Store : [0x800011a4] : sw a0, 996(t2) -- Store: [0x800039c4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7e0000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011b4]:fsqrt.s t6, t5, dyn
	-[0x800011b8]:csrrs a0, fcsr, zero
	-[0x800011bc]:sw t6, 1000(t2)
Current Store : [0x800011c0] : sw a0, 1004(t2) -- Store: [0x800039cc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00ffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011d0]:fsqrt.s t6, t5, dyn
	-[0x800011d4]:csrrs a0, fcsr, zero
	-[0x800011d8]:sw t6, 1008(t2)
Current Store : [0x800011dc] : sw a0, 1012(t2) -- Store: [0x800039d4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7f0000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011ec]:fsqrt.s t6, t5, dyn
	-[0x800011f0]:csrrs a0, fcsr, zero
	-[0x800011f4]:sw t6, 1016(t2)
Current Store : [0x800011f8] : sw a0, 1020(t2) -- Store: [0x800039dc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x007fff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001210]:fsqrt.s t6, t5, dyn
	-[0x80001214]:csrrs a0, fcsr, zero
	-[0x80001218]:sw t6, 0(t2)
Current Store : [0x8000121c] : sw a0, 4(t2) -- Store: [0x800039e4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7f8000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000122c]:fsqrt.s t6, t5, dyn
	-[0x80001230]:csrrs a0, fcsr, zero
	-[0x80001234]:sw t6, 8(t2)
Current Store : [0x80001238] : sw a0, 12(t2) -- Store: [0x800039ec]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x003fff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001248]:fsqrt.s t6, t5, dyn
	-[0x8000124c]:csrrs a0, fcsr, zero
	-[0x80001250]:sw t6, 16(t2)
Current Store : [0x80001254] : sw a0, 20(t2) -- Store: [0x800039f4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fc000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001264]:fsqrt.s t6, t5, dyn
	-[0x80001268]:csrrs a0, fcsr, zero
	-[0x8000126c]:sw t6, 24(t2)
Current Store : [0x80001270] : sw a0, 28(t2) -- Store: [0x800039fc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x001fff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001280]:fsqrt.s t6, t5, dyn
	-[0x80001284]:csrrs a0, fcsr, zero
	-[0x80001288]:sw t6, 32(t2)
Current Store : [0x8000128c] : sw a0, 36(t2) -- Store: [0x80003a04]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fe000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000129c]:fsqrt.s t6, t5, dyn
	-[0x800012a0]:csrrs a0, fcsr, zero
	-[0x800012a4]:sw t6, 40(t2)
Current Store : [0x800012a8] : sw a0, 44(t2) -- Store: [0x80003a0c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000fff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012b8]:fsqrt.s t6, t5, dyn
	-[0x800012bc]:csrrs a0, fcsr, zero
	-[0x800012c0]:sw t6, 48(t2)
Current Store : [0x800012c4] : sw a0, 52(t2) -- Store: [0x80003a14]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7ff000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012d4]:fsqrt.s t6, t5, dyn
	-[0x800012d8]:csrrs a0, fcsr, zero
	-[0x800012dc]:sw t6, 56(t2)
Current Store : [0x800012e0] : sw a0, 60(t2) -- Store: [0x80003a1c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0007ff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012f0]:fsqrt.s t6, t5, dyn
	-[0x800012f4]:csrrs a0, fcsr, zero
	-[0x800012f8]:sw t6, 64(t2)
Current Store : [0x800012fc] : sw a0, 68(t2) -- Store: [0x80003a24]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7ff800 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000130c]:fsqrt.s t6, t5, dyn
	-[0x80001310]:csrrs a0, fcsr, zero
	-[0x80001314]:sw t6, 72(t2)
Current Store : [0x80001318] : sw a0, 76(t2) -- Store: [0x80003a2c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0003ff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001328]:fsqrt.s t6, t5, dyn
	-[0x8000132c]:csrrs a0, fcsr, zero
	-[0x80001330]:sw t6, 80(t2)
Current Store : [0x80001334] : sw a0, 84(t2) -- Store: [0x80003a34]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7ffc00 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001344]:fsqrt.s t6, t5, dyn
	-[0x80001348]:csrrs a0, fcsr, zero
	-[0x8000134c]:sw t6, 88(t2)
Current Store : [0x80001350] : sw a0, 92(t2) -- Store: [0x80003a3c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0001ff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001360]:fsqrt.s t6, t5, dyn
	-[0x80001364]:csrrs a0, fcsr, zero
	-[0x80001368]:sw t6, 96(t2)
Current Store : [0x8000136c] : sw a0, 100(t2) -- Store: [0x80003a44]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7ffe00 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000137c]:fsqrt.s t6, t5, dyn
	-[0x80001380]:csrrs a0, fcsr, zero
	-[0x80001384]:sw t6, 104(t2)
Current Store : [0x80001388] : sw a0, 108(t2) -- Store: [0x80003a4c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0000ff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001398]:fsqrt.s t6, t5, dyn
	-[0x8000139c]:csrrs a0, fcsr, zero
	-[0x800013a0]:sw t6, 112(t2)
Current Store : [0x800013a4] : sw a0, 116(t2) -- Store: [0x80003a54]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fff00 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013b4]:fsqrt.s t6, t5, dyn
	-[0x800013b8]:csrrs a0, fcsr, zero
	-[0x800013bc]:sw t6, 120(t2)
Current Store : [0x800013c0] : sw a0, 124(t2) -- Store: [0x80003a5c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00007f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013d0]:fsqrt.s t6, t5, dyn
	-[0x800013d4]:csrrs a0, fcsr, zero
	-[0x800013d8]:sw t6, 128(t2)
Current Store : [0x800013dc] : sw a0, 132(t2) -- Store: [0x80003a64]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fff80 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013ec]:fsqrt.s t6, t5, dyn
	-[0x800013f0]:csrrs a0, fcsr, zero
	-[0x800013f4]:sw t6, 136(t2)
Current Store : [0x800013f8] : sw a0, 140(t2) -- Store: [0x80003a6c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001408]:fsqrt.s t6, t5, dyn
	-[0x8000140c]:csrrs a0, fcsr, zero
	-[0x80001410]:sw t6, 144(t2)
Current Store : [0x80001414] : sw a0, 148(t2) -- Store: [0x80003a74]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffc0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001424]:fsqrt.s t6, t5, dyn
	-[0x80001428]:csrrs a0, fcsr, zero
	-[0x8000142c]:sw t6, 152(t2)
Current Store : [0x80001430] : sw a0, 156(t2) -- Store: [0x80003a7c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00001f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001440]:fsqrt.s t6, t5, dyn
	-[0x80001444]:csrrs a0, fcsr, zero
	-[0x80001448]:sw t6, 160(t2)
Current Store : [0x8000144c] : sw a0, 164(t2) -- Store: [0x80003a84]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffe0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000145c]:fsqrt.s t6, t5, dyn
	-[0x80001460]:csrrs a0, fcsr, zero
	-[0x80001464]:sw t6, 168(t2)
Current Store : [0x80001468] : sw a0, 172(t2) -- Store: [0x80003a8c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00000f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001478]:fsqrt.s t6, t5, dyn
	-[0x8000147c]:csrrs a0, fcsr, zero
	-[0x80001480]:sw t6, 176(t2)
Current Store : [0x80001484] : sw a0, 180(t2) -- Store: [0x80003a94]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7ffff0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001494]:fsqrt.s t6, t5, dyn
	-[0x80001498]:csrrs a0, fcsr, zero
	-[0x8000149c]:sw t6, 184(t2)
Current Store : [0x800014a0] : sw a0, 188(t2) -- Store: [0x80003a9c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000007 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014b0]:fsqrt.s t6, t5, dyn
	-[0x800014b4]:csrrs a0, fcsr, zero
	-[0x800014b8]:sw t6, 192(t2)
Current Store : [0x800014bc] : sw a0, 196(t2) -- Store: [0x80003aa4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7ffff8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014cc]:fsqrt.s t6, t5, dyn
	-[0x800014d0]:csrrs a0, fcsr, zero
	-[0x800014d4]:sw t6, 200(t2)
Current Store : [0x800014d8] : sw a0, 204(t2) -- Store: [0x80003aac]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000003 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014e8]:fsqrt.s t6, t5, dyn
	-[0x800014ec]:csrrs a0, fcsr, zero
	-[0x800014f0]:sw t6, 208(t2)
Current Store : [0x800014f4] : sw a0, 212(t2) -- Store: [0x80003ab4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7ffffc and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001504]:fsqrt.s t6, t5, dyn
	-[0x80001508]:csrrs a0, fcsr, zero
	-[0x8000150c]:sw t6, 216(t2)
Current Store : [0x80001510] : sw a0, 220(t2) -- Store: [0x80003abc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001520]:fsqrt.s t6, t5, dyn
	-[0x80001524]:csrrs a0, fcsr, zero
	-[0x80001528]:sw t6, 224(t2)
Current Store : [0x8000152c] : sw a0, 228(t2) -- Store: [0x80003ac4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7ffffe and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000153c]:fsqrt.s t6, t5, dyn
	-[0x80001540]:csrrs a0, fcsr, zero
	-[0x80001544]:sw t6, 232(t2)
Current Store : [0x80001548] : sw a0, 236(t2) -- Store: [0x80003acc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x36db6d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001558]:fsqrt.s t6, t5, dyn
	-[0x8000155c]:csrrs a0, fcsr, zero
	-[0x80001560]:sw t6, 240(t2)
Current Store : [0x80001564] : sw a0, 244(t2) -- Store: [0x80003ad4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x6db6db and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001574]:fsqrt.s t6, t5, dyn
	-[0x80001578]:csrrs a0, fcsr, zero
	-[0x8000157c]:sw t6, 248(t2)
Current Store : [0x80001580] : sw a0, 252(t2) -- Store: [0x80003adc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x199999 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001590]:fsqrt.s t6, t5, dyn
	-[0x80001594]:csrrs a0, fcsr, zero
	-[0x80001598]:sw t6, 256(t2)
Current Store : [0x8000159c] : sw a0, 260(t2) -- Store: [0x80003ae4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x666666 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015ac]:fsqrt.s t6, t5, dyn
	-[0x800015b0]:csrrs a0, fcsr, zero
	-[0x800015b4]:sw t6, 264(t2)
Current Store : [0x800015b8] : sw a0, 268(t2) -- Store: [0x80003aec]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3bbbbb and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015c8]:fsqrt.s t6, t5, dyn
	-[0x800015cc]:csrrs a0, fcsr, zero
	-[0x800015d0]:sw t6, 272(t2)
Current Store : [0x800015d4] : sw a0, 276(t2) -- Store: [0x80003af4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x444444 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015e4]:fsqrt.s t6, t5, dyn
	-[0x800015e8]:csrrs a0, fcsr, zero
	-[0x800015ec]:sw t6, 280(t2)
Current Store : [0x800015f0] : sw a0, 284(t2) -- Store: [0x80003afc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x249249 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001600]:fsqrt.s t6, t5, dyn
	-[0x80001604]:csrrs a0, fcsr, zero
	-[0x80001608]:sw t6, 288(t2)
Current Store : [0x8000160c] : sw a0, 292(t2) -- Store: [0x80003b04]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x5b6db6 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000161c]:fsqrt.s t6, t5, dyn
	-[0x80001620]:csrrs a0, fcsr, zero
	-[0x80001624]:sw t6, 296(t2)
Current Store : [0x80001628] : sw a0, 300(t2) -- Store: [0x80003b0c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x333333 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001638]:fsqrt.s t6, t5, dyn
	-[0x8000163c]:csrrs a0, fcsr, zero
	-[0x80001640]:sw t6, 304(t2)
Current Store : [0x80001644] : sw a0, 308(t2) -- Store: [0x80003b14]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x4ccccc and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001654]:fsqrt.s t6, t5, dyn
	-[0x80001658]:csrrs a0, fcsr, zero
	-[0x8000165c]:sw t6, 312(t2)
Current Store : [0x80001660] : sw a0, 316(t2) -- Store: [0x80003b1c]:0x00000001




Last Coverpoint : ['mnemonic : fsqrt.s', 'rs1 : x30', 'rd : x31', 'rs1 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001670]:fsqrt.s t6, t5, dyn
	-[0x80001674]:csrrs a0, fcsr, zero
	-[0x80001678]:sw t6, 320(t2)
Current Store : [0x8000167c] : sw a0, 324(t2) -- Store: [0x80003b24]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0000ff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000168c]:fsqrt.s t6, t5, dyn
	-[0x80001690]:csrrs a0, fcsr, zero
	-[0x80001694]:sw t6, 328(t2)
Current Store : [0x80001698] : sw a0, 332(t2) -- Store: [0x80003b2c]:0x00000001




Last Coverpoint : ['mnemonic : fsqrt.s', 'rs1 : x30', 'rd : x31', 'rs1 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fff80 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016a8]:fsqrt.s t6, t5, dyn
	-[0x800016ac]:csrrs a0, fcsr, zero
	-[0x800016b0]:sw t6, 336(t2)
Current Store : [0x800016b4] : sw a0, 340(t2) -- Store: [0x80003b34]:0x00000001





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                             coverpoints                                                                             |                                                     code                                                     |
|---:|--------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------|
|   1|[0x80003510]<br>0x00000000|- mnemonic : fsqrt.s<br> - rs1 : x30<br> - rd : x31<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x80000120]:fsqrt.s t6, t5, dyn<br> [0x80000124]:csrrs tp, fcsr, zero<br> [0x80000128]:sw t6, 0(ra)<br>      |
|   2|[0x80003518]<br>0x1FFFFFFF|- rs1 : x29<br> - rd : x29<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                          |[0x8000013c]:fsqrt.s t4, t4, dyn<br> [0x80000140]:csrrs tp, fcsr, zero<br> [0x80000144]:sw t4, 8(ra)<br>      |
|   3|[0x80003520]<br>0x1FB504F3|- rs1 : x31<br> - rd : x30<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x400000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000158]:fsqrt.s t5, t6, dyn<br> [0x8000015c]:csrrs tp, fcsr, zero<br> [0x80000160]:sw t5, 16(ra)<br>     |
|   4|[0x80003528]<br>0x1FB504F2|- rs1 : x27<br> - rd : x28<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000174]:fsqrt.s t3, s11, dyn<br> [0x80000178]:csrrs tp, fcsr, zero<br> [0x8000017c]:sw t3, 24(ra)<br>    |
|   5|[0x80003530]<br>0x1FDDB3D7|- rs1 : x28<br> - rd : x27<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x600000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000190]:fsqrt.s s11, t3, dyn<br> [0x80000194]:csrrs tp, fcsr, zero<br> [0x80000198]:sw s11, 32(ra)<br>   |
|   6|[0x80003538]<br>0x1F7FFFFC|- rs1 : x25<br> - rd : x26<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x1fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800001ac]:fsqrt.s s10, s9, dyn<br> [0x800001b0]:csrrs tp, fcsr, zero<br> [0x800001b4]:sw s10, 40(ra)<br>   |
|   7|[0x80003540]<br>0x1FEF7751|- rs1 : x26<br> - rd : x25<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x700000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800001c8]:fsqrt.s s9, s10, dyn<br> [0x800001cc]:csrrs tp, fcsr, zero<br> [0x800001d0]:sw s9, 48(ra)<br>    |
|   8|[0x80003548]<br>0x1F3504EE|- rs1 : x23<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x0fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800001e4]:fsqrt.s s8, s7, dyn<br> [0x800001e8]:csrrs tp, fcsr, zero<br> [0x800001ec]:sw s8, 56(ra)<br>     |
|   9|[0x80003550]<br>0x1FF7DEF6|- rs1 : x24<br> - rd : x23<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x780000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000200]:fsqrt.s s7, s8, dyn<br> [0x80000204]:csrrs tp, fcsr, zero<br> [0x80000208]:sw s7, 64(ra)<br>     |
|  10|[0x80003558]<br>0x1EFFFFF0|- rs1 : x21<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x07ffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x8000021c]:fsqrt.s s6, s5, dyn<br> [0x80000220]:csrrs tp, fcsr, zero<br> [0x80000224]:sw s6, 72(ra)<br>     |
|  11|[0x80003560]<br>0x1FFBF7DF|- rs1 : x22<br> - rd : x21<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7c0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000238]:fsqrt.s s5, s6, dyn<br> [0x8000023c]:csrrs tp, fcsr, zero<br> [0x80000240]:sw s5, 80(ra)<br>     |
|  12|[0x80003568]<br>0x1EB504DD|- rs1 : x19<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x03ffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000254]:fsqrt.s s4, s3, dyn<br> [0x80000258]:csrrs tp, fcsr, zero<br> [0x8000025c]:sw s4, 88(ra)<br>     |
|  13|[0x80003570]<br>0x1FFDFDFC|- rs1 : x20<br> - rd : x19<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7e0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000270]:fsqrt.s s3, s4, dyn<br> [0x80000274]:csrrs tp, fcsr, zero<br> [0x80000278]:sw s3, 96(ra)<br>     |
|  14|[0x80003578]<br>0x1E7FFFC0|- rs1 : x17<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x01ffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x8000028c]:fsqrt.s s2, a7, dyn<br> [0x80000290]:csrrs tp, fcsr, zero<br> [0x80000294]:sw s2, 104(ra)<br>    |
|  15|[0x80003580]<br>0x1FFEFF7F|- rs1 : x18<br> - rd : x17<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7f0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800002a8]:fsqrt.s a7, s2, dyn<br> [0x800002ac]:csrrs tp, fcsr, zero<br> [0x800002b0]:sw a7, 112(ra)<br>    |
|  16|[0x80003588]<br>0x1E350499|- rs1 : x15<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x00ffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800002c4]:fsqrt.s a6, a5, dyn<br> [0x800002c8]:csrrs tp, fcsr, zero<br> [0x800002cc]:sw a6, 120(ra)<br>    |
|  17|[0x80003590]<br>0x1FFF7FE0|- rs1 : x16<br> - rd : x15<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7f8000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800002e0]:fsqrt.s a5, a6, dyn<br> [0x800002e4]:csrrs tp, fcsr, zero<br> [0x800002e8]:sw a5, 128(ra)<br>    |
|  18|[0x80003598]<br>0x1DFFFF00|- rs1 : x13<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x007fff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800002fc]:fsqrt.s a4, a3, dyn<br> [0x80000300]:csrrs tp, fcsr, zero<br> [0x80000304]:sw a4, 136(ra)<br>    |
|  19|[0x800035a0]<br>0x1FFFBFF8|- rs1 : x14<br> - rd : x13<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fc000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000318]:fsqrt.s a3, a4, dyn<br> [0x8000031c]:csrrs tp, fcsr, zero<br> [0x80000320]:sw a3, 144(ra)<br>    |
|  20|[0x800035a8]<br>0x1DB50389|- rs1 : x11<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x003fff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000334]:fsqrt.s a2, a1, dyn<br> [0x80000338]:csrrs tp, fcsr, zero<br> [0x8000033c]:sw a2, 152(ra)<br>    |
|  21|[0x800035b0]<br>0x1FFFDFFE|- rs1 : x12<br> - rd : x11<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fe000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000350]:fsqrt.s a1, a2, dyn<br> [0x80000354]:csrrs tp, fcsr, zero<br> [0x80000358]:sw a1, 160(ra)<br>    |
|  22|[0x800035b8]<br>0x1D7FFC00|- rs1 : x9<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x001fff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x8000036c]:fsqrt.s a0, s1, dyn<br> [0x80000370]:csrrs tp, fcsr, zero<br> [0x80000374]:sw a0, 168(ra)<br>    |
|  23|[0x800035c0]<br>0x1FFFEFFF|- rs1 : x10<br> - rd : x9<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ff000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x80000388]:fsqrt.s s1, a0, dyn<br> [0x8000038c]:csrrs tp, fcsr, zero<br> [0x80000390]:sw s1, 176(ra)<br>    |
|  24|[0x800035c8]<br>0x1D34FF4B|- rs1 : x7<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000fff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x800003a4]:fsqrt.s fp, t2, dyn<br> [0x800003a8]:csrrs tp, fcsr, zero<br> [0x800003ac]:sw fp, 184(ra)<br>    |
|  25|[0x800035d0]<br>0x1FFFF800|- rs1 : x8<br> - rd : x7<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ff800 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x800003c8]:fsqrt.s t2, fp, dyn<br> [0x800003cc]:csrrs a0, fcsr, zero<br> [0x800003d0]:sw t2, 192(ra)<br>    |
|  26|[0x800035d8]<br>0x1CFFEFFF|- rs1 : x5<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007ff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x800003e4]:fsqrt.s t1, t0, dyn<br> [0x800003e8]:csrrs a0, fcsr, zero<br> [0x800003ec]:sw t1, 200(ra)<br>    |
|  27|[0x800035e0]<br>0x1FFFFC00|- rs1 : x6<br> - rd : x5<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ffc00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x80000408]:fsqrt.s t0, t1, dyn<br> [0x8000040c]:csrrs a0, fcsr, zero<br> [0x80000410]:sw t0, 0(t2)<br>      |
|  28|[0x800035e8]<br>0x1CB4EE51|- rs1 : x3<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x0003ff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x80000424]:fsqrt.s tp, gp, dyn<br> [0x80000428]:csrrs a0, fcsr, zero<br> [0x8000042c]:sw tp, 8(t2)<br>      |
|  29|[0x800035f0]<br>0x1FFFFE00|- rs1 : x4<br> - rd : x3<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ffe00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x80000440]:fsqrt.s gp, tp, dyn<br> [0x80000444]:csrrs a0, fcsr, zero<br> [0x80000448]:sw gp, 16(t2)<br>     |
|  30|[0x800035f8]<br>0x1C7FBFF8|- rs1 : x1<br> - rd : x2<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x0001ff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x8000045c]:fsqrt.s sp, ra, dyn<br> [0x80000460]:csrrs a0, fcsr, zero<br> [0x80000464]:sw sp, 24(t2)<br>     |
|  31|[0x80003600]<br>0x1FFFFF00|- rs1 : x2<br> - rd : x1<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fff00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x80000478]:fsqrt.s ra, sp, dyn<br> [0x8000047c]:csrrs a0, fcsr, zero<br> [0x80000480]:sw ra, 32(t2)<br>     |
|  32|[0x80003608]<br>0x00000000|- rs1 : x0<br>                                                                                                                                                       |[0x80000494]:fsqrt.s t6, zero, dyn<br> [0x80000498]:csrrs a0, fcsr, zero<br> [0x8000049c]:sw t6, 40(t2)<br>   |
|  33|[0x80003610]<br>0x00000000|- rd : x0<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fff80 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                           |[0x800004b0]:fsqrt.s zero, t6, dyn<br> [0x800004b4]:csrrs a0, fcsr, zero<br> [0x800004b8]:sw zero, 48(t2)<br> |
|  34|[0x80003618]<br>0x1BFEFF7F|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x00007f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800004cc]:fsqrt.s t6, t5, dyn<br> [0x800004d0]:csrrs a0, fcsr, zero<br> [0x800004d4]:sw t6, 56(t2)<br>     |
|  35|[0x80003620]<br>0x1FFFFFC0|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffc0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800004e8]:fsqrt.s t6, t5, dyn<br> [0x800004ec]:csrrs a0, fcsr, zero<br> [0x800004f0]:sw t6, 64(t2)<br>     |
|  36|[0x80003628]<br>0x1BB3997C|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000504]:fsqrt.s t6, t5, dyn<br> [0x80000508]:csrrs a0, fcsr, zero<br> [0x8000050c]:sw t6, 72(t2)<br>     |
|  37|[0x80003630]<br>0x1FFFFFE0|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffe0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000520]:fsqrt.s t6, t5, dyn<br> [0x80000524]:csrrs a0, fcsr, zero<br> [0x80000528]:sw t6, 80(t2)<br>     |
|  38|[0x80003638]<br>0x1B7BF7DF|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x00001f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000053c]:fsqrt.s t6, t5, dyn<br> [0x80000540]:csrrs a0, fcsr, zero<br> [0x80000544]:sw t6, 88(t2)<br>     |
|  39|[0x80003640]<br>0x1FFFFFF0|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ffff0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000558]:fsqrt.s t6, t5, dyn<br> [0x8000055c]:csrrs a0, fcsr, zero<br> [0x80000560]:sw t6, 96(t2)<br>     |
|  40|[0x80003648]<br>0x1B2F456F|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x00000f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000574]:fsqrt.s t6, t5, dyn<br> [0x80000578]:csrrs a0, fcsr, zero<br> [0x8000057c]:sw t6, 104(t2)<br>    |
|  41|[0x80003650]<br>0x1FFFFFF8|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ffff8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000590]:fsqrt.s t6, t5, dyn<br> [0x80000594]:csrrs a0, fcsr, zero<br> [0x80000598]:sw t6, 112(t2)<br>    |
|  42|[0x80003658]<br>0x1AEF7751|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000007 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800005ac]:fsqrt.s t6, t5, dyn<br> [0x800005b0]:csrrs a0, fcsr, zero<br> [0x800005b4]:sw t6, 120(t2)<br>    |
|  43|[0x80003660]<br>0x1FFFFFFC|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ffffc and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800005c8]:fsqrt.s t6, t5, dyn<br> [0x800005cc]:csrrs a0, fcsr, zero<br> [0x800005d0]:sw t6, 128(t2)<br>    |
|  44|[0x80003668]<br>0x1A9CC471|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000003 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800005e4]:fsqrt.s t6, t5, dyn<br> [0x800005e8]:csrrs a0, fcsr, zero<br> [0x800005ec]:sw t6, 136(t2)<br>    |
|  45|[0x80003670]<br>0x1FFFFFFE|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ffffe and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000600]:fsqrt.s t6, t5, dyn<br> [0x80000604]:csrrs a0, fcsr, zero<br> [0x80000608]:sw t6, 144(t2)<br>    |
|  46|[0x80003678]<br>0x1A3504F3|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000061c]:fsqrt.s t6, t5, dyn<br> [0x80000620]:csrrs a0, fcsr, zero<br> [0x80000624]:sw t6, 152(t2)<br>    |
|  47|[0x80003680]<br>0x3F800000|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000638]:fsqrt.s t6, t5, dyn<br> [0x8000063c]:csrrs a0, fcsr, zero<br> [0x80000640]:sw t6, 160(t2)<br>    |
|  48|[0x80003688]<br>0x3FB504F3|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000654]:fsqrt.s t6, t5, dyn<br> [0x80000658]:csrrs a0, fcsr, zero<br> [0x8000065c]:sw t6, 168(t2)<br>    |
|  49|[0x80003690]<br>0x3F9CC471|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x400000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000670]:fsqrt.s t6, t5, dyn<br> [0x80000674]:csrrs a0, fcsr, zero<br> [0x80000678]:sw t6, 176(t2)<br>    |
|  50|[0x80003698]<br>0x3F9CC470|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x3fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000068c]:fsqrt.s t6, t5, dyn<br> [0x80000690]:csrrs a0, fcsr, zero<br> [0x80000694]:sw t6, 184(t2)<br>    |
|  51|[0x800036a0]<br>0x3FA953FD|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x600000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800006a8]:fsqrt.s t6, t5, dyn<br> [0x800006ac]:csrrs a0, fcsr, zero<br> [0x800006b0]:sw t6, 192(t2)<br>    |
|  52|[0x800036a8]<br>0x3F8F1BBC|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x1fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800006c4]:fsqrt.s t6, t5, dyn<br> [0x800006c8]:csrrs a0, fcsr, zero<br> [0x800006cc]:sw t6, 200(t2)<br>    |
|  53|[0x800036b0]<br>0x3FAF456F|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x700000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800006e0]:fsqrt.s t6, t5, dyn<br> [0x800006e4]:csrrs a0, fcsr, zero<br> [0x800006e8]:sw t6, 208(t2)<br>    |
|  54|[0x800036b8]<br>0x3F87C3B6|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800006fc]:fsqrt.s t6, t5, dyn<br> [0x80000700]:csrrs a0, fcsr, zero<br> [0x80000704]:sw t6, 216(t2)<br>    |
|  55|[0x800036c0]<br>0x3FB22B20|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x780000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000718]:fsqrt.s t6, t5, dyn<br> [0x8000071c]:csrrs a0, fcsr, zero<br> [0x80000720]:sw t6, 224(t2)<br>    |
|  56|[0x800036c8]<br>0x3F83F07B|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x07ffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000734]:fsqrt.s t6, t5, dyn<br> [0x80000738]:csrrs a0, fcsr, zero<br> [0x8000073c]:sw t6, 232(t2)<br>    |
|  57|[0x800036d0]<br>0x3FB3997C|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7c0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000750]:fsqrt.s t6, t5, dyn<br> [0x80000754]:csrrs a0, fcsr, zero<br> [0x80000758]:sw t6, 240(t2)<br>    |
|  58|[0x800036d8]<br>0x3F81FC0F|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x03ffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000076c]:fsqrt.s t6, t5, dyn<br> [0x80000770]:csrrs a0, fcsr, zero<br> [0x80000774]:sw t6, 248(t2)<br>    |
|  59|[0x800036e0]<br>0x3FB44F93|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7e0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000788]:fsqrt.s t6, t5, dyn<br> [0x8000078c]:csrrs a0, fcsr, zero<br> [0x80000790]:sw t6, 256(t2)<br>    |
|  60|[0x800036e8]<br>0x3F80FF01|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x01ffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800007a4]:fsqrt.s t6, t5, dyn<br> [0x800007a8]:csrrs a0, fcsr, zero<br> [0x800007ac]:sw t6, 264(t2)<br>    |
|  61|[0x800036f0]<br>0x3FB4AA5A|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7f0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800007c0]:fsqrt.s t6, t5, dyn<br> [0x800007c4]:csrrs a0, fcsr, zero<br> [0x800007c8]:sw t6, 272(t2)<br>    |
|  62|[0x800036f8]<br>0x3F807FC0|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x00ffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800007dc]:fsqrt.s t6, t5, dyn<br> [0x800007e0]:csrrs a0, fcsr, zero<br> [0x800007e4]:sw t6, 280(t2)<br>    |
|  63|[0x80003700]<br>0x3FB4D7AC|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7f8000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800007f8]:fsqrt.s t6, t5, dyn<br> [0x800007fc]:csrrs a0, fcsr, zero<br> [0x80000800]:sw t6, 288(t2)<br>    |
|  64|[0x80003708]<br>0x3F803FF0|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x007fff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000814]:fsqrt.s t6, t5, dyn<br> [0x80000818]:csrrs a0, fcsr, zero<br> [0x8000081c]:sw t6, 296(t2)<br>    |
|  65|[0x80003710]<br>0x3FB4EE51|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fc000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000830]:fsqrt.s t6, t5, dyn<br> [0x80000834]:csrrs a0, fcsr, zero<br> [0x80000838]:sw t6, 304(t2)<br>    |
|  66|[0x80003718]<br>0x3F801FFC|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x003fff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000084c]:fsqrt.s t6, t5, dyn<br> [0x80000850]:csrrs a0, fcsr, zero<br> [0x80000854]:sw t6, 312(t2)<br>    |
|  67|[0x80003720]<br>0x3FB4F9A3|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fe000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000868]:fsqrt.s t6, t5, dyn<br> [0x8000086c]:csrrs a0, fcsr, zero<br> [0x80000870]:sw t6, 320(t2)<br>    |
|  68|[0x80003728]<br>0x3F800FFF|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x001fff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000884]:fsqrt.s t6, t5, dyn<br> [0x80000888]:csrrs a0, fcsr, zero<br> [0x8000088c]:sw t6, 328(t2)<br>    |
|  69|[0x80003730]<br>0x3FB4FF4B|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ff000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800008a0]:fsqrt.s t6, t5, dyn<br> [0x800008a4]:csrrs a0, fcsr, zero<br> [0x800008a8]:sw t6, 336(t2)<br>    |
|  70|[0x80003738]<br>0x3F8007FF|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000fff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800008bc]:fsqrt.s t6, t5, dyn<br> [0x800008c0]:csrrs a0, fcsr, zero<br> [0x800008c4]:sw t6, 344(t2)<br>    |
|  71|[0x80003740]<br>0x3FB5021F|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ff800 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800008d8]:fsqrt.s t6, t5, dyn<br> [0x800008dc]:csrrs a0, fcsr, zero<br> [0x800008e0]:sw t6, 352(t2)<br>    |
|  72|[0x80003748]<br>0x3F8003FF|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0007ff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800008f4]:fsqrt.s t6, t5, dyn<br> [0x800008f8]:csrrs a0, fcsr, zero<br> [0x800008fc]:sw t6, 360(t2)<br>    |
|  73|[0x80003750]<br>0x3FB50389|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ffc00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000910]:fsqrt.s t6, t5, dyn<br> [0x80000914]:csrrs a0, fcsr, zero<br> [0x80000918]:sw t6, 368(t2)<br>    |
|  74|[0x80003758]<br>0x3F8001FF|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0003ff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000092c]:fsqrt.s t6, t5, dyn<br> [0x80000930]:csrrs a0, fcsr, zero<br> [0x80000934]:sw t6, 376(t2)<br>    |
|  75|[0x80003760]<br>0x3FB5043E|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ffe00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000948]:fsqrt.s t6, t5, dyn<br> [0x8000094c]:csrrs a0, fcsr, zero<br> [0x80000950]:sw t6, 384(t2)<br>    |
|  76|[0x80003768]<br>0x3F8000FF|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0001ff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000964]:fsqrt.s t6, t5, dyn<br> [0x80000968]:csrrs a0, fcsr, zero<br> [0x8000096c]:sw t6, 392(t2)<br>    |
|  77|[0x80003770]<br>0x3FB50499|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fff00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000980]:fsqrt.s t6, t5, dyn<br> [0x80000984]:csrrs a0, fcsr, zero<br> [0x80000988]:sw t6, 400(t2)<br>    |
|  78|[0x80003778]<br>0x3F80007F|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0000ff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000099c]:fsqrt.s t6, t5, dyn<br> [0x800009a0]:csrrs a0, fcsr, zero<br> [0x800009a4]:sw t6, 408(t2)<br>    |
|  79|[0x80003780]<br>0x3FB504C6|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fff80 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800009b8]:fsqrt.s t6, t5, dyn<br> [0x800009bc]:csrrs a0, fcsr, zero<br> [0x800009c0]:sw t6, 416(t2)<br>    |
|  80|[0x80003788]<br>0x3F80003F|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x00007f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800009d4]:fsqrt.s t6, t5, dyn<br> [0x800009d8]:csrrs a0, fcsr, zero<br> [0x800009dc]:sw t6, 424(t2)<br>    |
|  81|[0x80003790]<br>0x3FB504DD|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fffc0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800009f0]:fsqrt.s t6, t5, dyn<br> [0x800009f4]:csrrs a0, fcsr, zero<br> [0x800009f8]:sw t6, 432(t2)<br>    |
|  82|[0x80003798]<br>0x3F80001F|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000a0c]:fsqrt.s t6, t5, dyn<br> [0x80000a10]:csrrs a0, fcsr, zero<br> [0x80000a14]:sw t6, 440(t2)<br>    |
|  83|[0x800037a0]<br>0x3FB504E8|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fffe0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000a28]:fsqrt.s t6, t5, dyn<br> [0x80000a2c]:csrrs a0, fcsr, zero<br> [0x80000a30]:sw t6, 448(t2)<br>    |
|  84|[0x800037a8]<br>0x3F80000F|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x00001f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000a44]:fsqrt.s t6, t5, dyn<br> [0x80000a48]:csrrs a0, fcsr, zero<br> [0x80000a4c]:sw t6, 456(t2)<br>    |
|  85|[0x800037b0]<br>0x3FB504EE|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ffff0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000a60]:fsqrt.s t6, t5, dyn<br> [0x80000a64]:csrrs a0, fcsr, zero<br> [0x80000a68]:sw t6, 464(t2)<br>    |
|  86|[0x800037b8]<br>0x3F800007|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x00000f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000a7c]:fsqrt.s t6, t5, dyn<br> [0x80000a80]:csrrs a0, fcsr, zero<br> [0x80000a84]:sw t6, 472(t2)<br>    |
|  87|[0x800037c0]<br>0x3FB504F0|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ffff8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000a98]:fsqrt.s t6, t5, dyn<br> [0x80000a9c]:csrrs a0, fcsr, zero<br> [0x80000aa0]:sw t6, 480(t2)<br>    |
|  88|[0x800037c8]<br>0x3F800003|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000007 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000ab4]:fsqrt.s t6, t5, dyn<br> [0x80000ab8]:csrrs a0, fcsr, zero<br> [0x80000abc]:sw t6, 488(t2)<br>    |
|  89|[0x800037d0]<br>0x3FB504F2|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ffffc and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000ad0]:fsqrt.s t6, t5, dyn<br> [0x80000ad4]:csrrs a0, fcsr, zero<br> [0x80000ad8]:sw t6, 496(t2)<br>    |
|  90|[0x800037d8]<br>0x3F800001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000003 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000aec]:fsqrt.s t6, t5, dyn<br> [0x80000af0]:csrrs a0, fcsr, zero<br> [0x80000af4]:sw t6, 504(t2)<br>    |
|  91|[0x800037e0]<br>0x3FB504F2|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ffffe and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000b08]:fsqrt.s t6, t5, dyn<br> [0x80000b0c]:csrrs a0, fcsr, zero<br> [0x80000b10]:sw t6, 512(t2)<br>    |
|  92|[0x800037e8]<br>0x3F800000|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000b24]:fsqrt.s t6, t5, dyn<br> [0x80000b28]:csrrs a0, fcsr, zero<br> [0x80000b2c]:sw t6, 520(t2)<br>    |
|  93|[0x800037f0]<br>0x20000000|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000b40]:fsqrt.s t6, t5, dyn<br> [0x80000b44]:csrrs a0, fcsr, zero<br> [0x80000b48]:sw t6, 528(t2)<br>    |
|  94|[0x800037f8]<br>0x203504F3|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000b5c]:fsqrt.s t6, t5, dyn<br> [0x80000b60]:csrrs a0, fcsr, zero<br> [0x80000b64]:sw t6, 536(t2)<br>    |
|  95|[0x80003800]<br>0x201CC471|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x400000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000b78]:fsqrt.s t6, t5, dyn<br> [0x80000b7c]:csrrs a0, fcsr, zero<br> [0x80000b80]:sw t6, 544(t2)<br>    |
|  96|[0x80003808]<br>0x201CC470|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x3fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000b94]:fsqrt.s t6, t5, dyn<br> [0x80000b98]:csrrs a0, fcsr, zero<br> [0x80000b9c]:sw t6, 552(t2)<br>    |
|  97|[0x80003810]<br>0x202953FD|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x600000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000bb0]:fsqrt.s t6, t5, dyn<br> [0x80000bb4]:csrrs a0, fcsr, zero<br> [0x80000bb8]:sw t6, 560(t2)<br>    |
|  98|[0x80003818]<br>0x200F1BBC|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x1fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000bcc]:fsqrt.s t6, t5, dyn<br> [0x80000bd0]:csrrs a0, fcsr, zero<br> [0x80000bd4]:sw t6, 568(t2)<br>    |
|  99|[0x80003820]<br>0x202F456F|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x700000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000be8]:fsqrt.s t6, t5, dyn<br> [0x80000bec]:csrrs a0, fcsr, zero<br> [0x80000bf0]:sw t6, 576(t2)<br>    |
| 100|[0x80003828]<br>0x2007C3B6|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000c04]:fsqrt.s t6, t5, dyn<br> [0x80000c08]:csrrs a0, fcsr, zero<br> [0x80000c0c]:sw t6, 584(t2)<br>    |
| 101|[0x80003830]<br>0x20322B20|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x780000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000c20]:fsqrt.s t6, t5, dyn<br> [0x80000c24]:csrrs a0, fcsr, zero<br> [0x80000c28]:sw t6, 592(t2)<br>    |
| 102|[0x80003838]<br>0x2003F07B|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x07ffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000c3c]:fsqrt.s t6, t5, dyn<br> [0x80000c40]:csrrs a0, fcsr, zero<br> [0x80000c44]:sw t6, 600(t2)<br>    |
| 103|[0x80003840]<br>0x2033997C|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x7c0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000c58]:fsqrt.s t6, t5, dyn<br> [0x80000c5c]:csrrs a0, fcsr, zero<br> [0x80000c60]:sw t6, 608(t2)<br>    |
| 104|[0x80003848]<br>0x2001FC0F|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x03ffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000c74]:fsqrt.s t6, t5, dyn<br> [0x80000c78]:csrrs a0, fcsr, zero<br> [0x80000c7c]:sw t6, 616(t2)<br>    |
| 105|[0x80003850]<br>0x20344F93|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x7e0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000c90]:fsqrt.s t6, t5, dyn<br> [0x80000c94]:csrrs a0, fcsr, zero<br> [0x80000c98]:sw t6, 624(t2)<br>    |
| 106|[0x80003858]<br>0x2000FF01|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x01ffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000cac]:fsqrt.s t6, t5, dyn<br> [0x80000cb0]:csrrs a0, fcsr, zero<br> [0x80000cb4]:sw t6, 632(t2)<br>    |
| 107|[0x80003860]<br>0x2034AA5A|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x7f0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000cc8]:fsqrt.s t6, t5, dyn<br> [0x80000ccc]:csrrs a0, fcsr, zero<br> [0x80000cd0]:sw t6, 640(t2)<br>    |
| 108|[0x80003868]<br>0x20007FC0|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x00ffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000ce4]:fsqrt.s t6, t5, dyn<br> [0x80000ce8]:csrrs a0, fcsr, zero<br> [0x80000cec]:sw t6, 648(t2)<br>    |
| 109|[0x80003870]<br>0x2034D7AC|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x7f8000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000d00]:fsqrt.s t6, t5, dyn<br> [0x80000d04]:csrrs a0, fcsr, zero<br> [0x80000d08]:sw t6, 656(t2)<br>    |
| 110|[0x80003878]<br>0x20003FF0|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x007fff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000d1c]:fsqrt.s t6, t5, dyn<br> [0x80000d20]:csrrs a0, fcsr, zero<br> [0x80000d24]:sw t6, 664(t2)<br>    |
| 111|[0x80003880]<br>0x2034EE51|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x7fc000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000d38]:fsqrt.s t6, t5, dyn<br> [0x80000d3c]:csrrs a0, fcsr, zero<br> [0x80000d40]:sw t6, 672(t2)<br>    |
| 112|[0x80003888]<br>0x20001FFC|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x003fff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000d54]:fsqrt.s t6, t5, dyn<br> [0x80000d58]:csrrs a0, fcsr, zero<br> [0x80000d5c]:sw t6, 680(t2)<br>    |
| 113|[0x80003890]<br>0x2034F9A3|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x7fe000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000d70]:fsqrt.s t6, t5, dyn<br> [0x80000d74]:csrrs a0, fcsr, zero<br> [0x80000d78]:sw t6, 688(t2)<br>    |
| 114|[0x80003898]<br>0x20000FFF|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001fff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000d8c]:fsqrt.s t6, t5, dyn<br> [0x80000d90]:csrrs a0, fcsr, zero<br> [0x80000d94]:sw t6, 696(t2)<br>    |
| 115|[0x800038a0]<br>0x2034FF4B|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x7ff000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000da8]:fsqrt.s t6, t5, dyn<br> [0x80000dac]:csrrs a0, fcsr, zero<br> [0x80000db0]:sw t6, 704(t2)<br>    |
| 116|[0x800038a8]<br>0x200007FF|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000fff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000dc4]:fsqrt.s t6, t5, dyn<br> [0x80000dc8]:csrrs a0, fcsr, zero<br> [0x80000dcc]:sw t6, 712(t2)<br>    |
| 117|[0x800038b0]<br>0x2035021F|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x7ff800 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000de0]:fsqrt.s t6, t5, dyn<br> [0x80000de4]:csrrs a0, fcsr, zero<br> [0x80000de8]:sw t6, 720(t2)<br>    |
| 118|[0x800038b8]<br>0x200003FF|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0007ff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000dfc]:fsqrt.s t6, t5, dyn<br> [0x80000e00]:csrrs a0, fcsr, zero<br> [0x80000e04]:sw t6, 728(t2)<br>    |
| 119|[0x800038c0]<br>0x20350389|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x7ffc00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000e18]:fsqrt.s t6, t5, dyn<br> [0x80000e1c]:csrrs a0, fcsr, zero<br> [0x80000e20]:sw t6, 736(t2)<br>    |
| 120|[0x800038c8]<br>0x200001FF|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0003ff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000e34]:fsqrt.s t6, t5, dyn<br> [0x80000e38]:csrrs a0, fcsr, zero<br> [0x80000e3c]:sw t6, 744(t2)<br>    |
| 121|[0x800038d0]<br>0x2035043E|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x7ffe00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000e50]:fsqrt.s t6, t5, dyn<br> [0x80000e54]:csrrs a0, fcsr, zero<br> [0x80000e58]:sw t6, 752(t2)<br>    |
| 122|[0x800038d8]<br>0x200000FF|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0001ff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000e6c]:fsqrt.s t6, t5, dyn<br> [0x80000e70]:csrrs a0, fcsr, zero<br> [0x80000e74]:sw t6, 760(t2)<br>    |
| 123|[0x800038e0]<br>0x20350499|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x7fff00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000e88]:fsqrt.s t6, t5, dyn<br> [0x80000e8c]:csrrs a0, fcsr, zero<br> [0x80000e90]:sw t6, 768(t2)<br>    |
| 124|[0x800038e8]<br>0x2000007F|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0000ff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000ea4]:fsqrt.s t6, t5, dyn<br> [0x80000ea8]:csrrs a0, fcsr, zero<br> [0x80000eac]:sw t6, 776(t2)<br>    |
| 125|[0x800038f0]<br>0x203504C6|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x7fff80 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000ec0]:fsqrt.s t6, t5, dyn<br> [0x80000ec4]:csrrs a0, fcsr, zero<br> [0x80000ec8]:sw t6, 784(t2)<br>    |
| 126|[0x800038f8]<br>0x2000003F|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x00007f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000edc]:fsqrt.s t6, t5, dyn<br> [0x80000ee0]:csrrs a0, fcsr, zero<br> [0x80000ee4]:sw t6, 792(t2)<br>    |
| 127|[0x80003900]<br>0x203504DD|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x7fffc0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000ef8]:fsqrt.s t6, t5, dyn<br> [0x80000efc]:csrrs a0, fcsr, zero<br> [0x80000f00]:sw t6, 800(t2)<br>    |
| 128|[0x80003908]<br>0x2000001F|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000f14]:fsqrt.s t6, t5, dyn<br> [0x80000f18]:csrrs a0, fcsr, zero<br> [0x80000f1c]:sw t6, 808(t2)<br>    |
| 129|[0x80003910]<br>0x203504E8|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x7fffe0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000f30]:fsqrt.s t6, t5, dyn<br> [0x80000f34]:csrrs a0, fcsr, zero<br> [0x80000f38]:sw t6, 816(t2)<br>    |
| 130|[0x80003918]<br>0x2000000F|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x00001f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000f4c]:fsqrt.s t6, t5, dyn<br> [0x80000f50]:csrrs a0, fcsr, zero<br> [0x80000f54]:sw t6, 824(t2)<br>    |
| 131|[0x80003920]<br>0x203504EE|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x7ffff0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000f68]:fsqrt.s t6, t5, dyn<br> [0x80000f6c]:csrrs a0, fcsr, zero<br> [0x80000f70]:sw t6, 832(t2)<br>    |
| 132|[0x80003928]<br>0x20000007|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x00000f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000f84]:fsqrt.s t6, t5, dyn<br> [0x80000f88]:csrrs a0, fcsr, zero<br> [0x80000f8c]:sw t6, 840(t2)<br>    |
| 133|[0x80003930]<br>0x203504F0|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x7ffff8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000fa0]:fsqrt.s t6, t5, dyn<br> [0x80000fa4]:csrrs a0, fcsr, zero<br> [0x80000fa8]:sw t6, 848(t2)<br>    |
| 134|[0x80003938]<br>0x20000003|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000007 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000fbc]:fsqrt.s t6, t5, dyn<br> [0x80000fc0]:csrrs a0, fcsr, zero<br> [0x80000fc4]:sw t6, 856(t2)<br>    |
| 135|[0x80003940]<br>0x203504F2|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x7ffffc and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000fd8]:fsqrt.s t6, t5, dyn<br> [0x80000fdc]:csrrs a0, fcsr, zero<br> [0x80000fe0]:sw t6, 864(t2)<br>    |
| 136|[0x80003948]<br>0x20000001|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000003 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000ff4]:fsqrt.s t6, t5, dyn<br> [0x80000ff8]:csrrs a0, fcsr, zero<br> [0x80000ffc]:sw t6, 872(t2)<br>    |
| 137|[0x80003950]<br>0x203504F2|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x7ffffe and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001010]:fsqrt.s t6, t5, dyn<br> [0x80001014]:csrrs a0, fcsr, zero<br> [0x80001018]:sw t6, 880(t2)<br>    |
| 138|[0x80003958]<br>0x20000000|- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000102c]:fsqrt.s t6, t5, dyn<br> [0x80001030]:csrrs a0, fcsr, zero<br> [0x80001034]:sw t6, 888(t2)<br>    |
| 139|[0x80003960]<br>0x5F7FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001048]:fsqrt.s t6, t5, dyn<br> [0x8000104c]:csrrs a0, fcsr, zero<br> [0x80001050]:sw t6, 896(t2)<br>    |
| 140|[0x80003968]<br>0x5F3504F3|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001064]:fsqrt.s t6, t5, dyn<br> [0x80001068]:csrrs a0, fcsr, zero<br> [0x8000106c]:sw t6, 904(t2)<br>    |
| 141|[0x80003970]<br>0x5F5DB3D7|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001080]:fsqrt.s t6, t5, dyn<br> [0x80001084]:csrrs a0, fcsr, zero<br> [0x80001088]:sw t6, 912(t2)<br>    |
| 142|[0x80003978]<br>0x5F5DB3D7|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x400000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000109c]:fsqrt.s t6, t5, dyn<br> [0x800010a0]:csrrs a0, fcsr, zero<br> [0x800010a4]:sw t6, 920(t2)<br>    |
| 143|[0x80003980]<br>0x5F4A62C1|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x1fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800010b8]:fsqrt.s t6, t5, dyn<br> [0x800010bc]:csrrs a0, fcsr, zero<br> [0x800010c0]:sw t6, 928(t2)<br>    |
| 144|[0x80003988]<br>0x5F6F7751|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x600000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800010d4]:fsqrt.s t6, t5, dyn<br> [0x800010d8]:csrrs a0, fcsr, zero<br> [0x800010dc]:sw t6, 936(t2)<br>    |
| 145|[0x80003990]<br>0x5F3FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800010f0]:fsqrt.s t6, t5, dyn<br> [0x800010f4]:csrrs a0, fcsr, zero<br> [0x800010f8]:sw t6, 944(t2)<br>    |
| 146|[0x80003998]<br>0x5F77DEF6|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x700000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000110c]:fsqrt.s t6, t5, dyn<br> [0x80001110]:csrrs a0, fcsr, zero<br> [0x80001114]:sw t6, 952(t2)<br>    |
| 147|[0x800039a0]<br>0x5F3A9728|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x07ffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001128]:fsqrt.s t6, t5, dyn<br> [0x8000112c]:csrrs a0, fcsr, zero<br> [0x80001130]:sw t6, 960(t2)<br>    |
| 148|[0x800039a8]<br>0x5F7BF7DF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x780000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001144]:fsqrt.s t6, t5, dyn<br> [0x80001148]:csrrs a0, fcsr, zero<br> [0x8000114c]:sw t6, 968(t2)<br>    |
| 149|[0x800039b0]<br>0x5F37D374|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x03ffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001160]:fsqrt.s t6, t5, dyn<br> [0x80001164]:csrrs a0, fcsr, zero<br> [0x80001168]:sw t6, 976(t2)<br>    |
| 150|[0x800039b8]<br>0x5F7DFDFC|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7c0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000117c]:fsqrt.s t6, t5, dyn<br> [0x80001180]:csrrs a0, fcsr, zero<br> [0x80001184]:sw t6, 984(t2)<br>    |
| 151|[0x800039c0]<br>0x5F366D95|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x01ffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001198]:fsqrt.s t6, t5, dyn<br> [0x8000119c]:csrrs a0, fcsr, zero<br> [0x800011a0]:sw t6, 992(t2)<br>    |
| 152|[0x800039c8]<br>0x5F7EFF7F|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7e0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800011b4]:fsqrt.s t6, t5, dyn<br> [0x800011b8]:csrrs a0, fcsr, zero<br> [0x800011bc]:sw t6, 1000(t2)<br>   |
| 153|[0x800039d0]<br>0x5F35B99D|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x00ffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800011d0]:fsqrt.s t6, t5, dyn<br> [0x800011d4]:csrrs a0, fcsr, zero<br> [0x800011d8]:sw t6, 1008(t2)<br>   |
| 154|[0x800039d8]<br>0x5F7F7FE0|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7f0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800011ec]:fsqrt.s t6, t5, dyn<br> [0x800011f0]:csrrs a0, fcsr, zero<br> [0x800011f4]:sw t6, 1016(t2)<br>   |
| 155|[0x800039e0]<br>0x5F355F5E|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x007fff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001210]:fsqrt.s t6, t5, dyn<br> [0x80001214]:csrrs a0, fcsr, zero<br> [0x80001218]:sw t6, 0(t2)<br>      |
| 156|[0x800039e8]<br>0x5F7FBFF8|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7f8000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000122c]:fsqrt.s t6, t5, dyn<br> [0x80001230]:csrrs a0, fcsr, zero<br> [0x80001234]:sw t6, 8(t2)<br>      |
| 157|[0x800039f0]<br>0x5F35322E|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x003fff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001248]:fsqrt.s t6, t5, dyn<br> [0x8000124c]:csrrs a0, fcsr, zero<br> [0x80001250]:sw t6, 16(t2)<br>     |
| 158|[0x800039f8]<br>0x5F7FDFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fc000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001264]:fsqrt.s t6, t5, dyn<br> [0x80001268]:csrrs a0, fcsr, zero<br> [0x8000126c]:sw t6, 24(t2)<br>     |
| 159|[0x80003a00]<br>0x5F351B92|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x001fff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001280]:fsqrt.s t6, t5, dyn<br> [0x80001284]:csrrs a0, fcsr, zero<br> [0x80001288]:sw t6, 32(t2)<br>     |
| 160|[0x80003a08]<br>0x5F7FEFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fe000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000129c]:fsqrt.s t6, t5, dyn<br> [0x800012a0]:csrrs a0, fcsr, zero<br> [0x800012a4]:sw t6, 40(t2)<br>     |
| 161|[0x80003a10]<br>0x5F351042|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000fff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800012b8]:fsqrt.s t6, t5, dyn<br> [0x800012bc]:csrrs a0, fcsr, zero<br> [0x800012c0]:sw t6, 48(t2)<br>     |
| 162|[0x80003a18]<br>0x5F7FF800|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7ff000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800012d4]:fsqrt.s t6, t5, dyn<br> [0x800012d8]:csrrs a0, fcsr, zero<br> [0x800012dc]:sw t6, 56(t2)<br>     |
| 163|[0x80003a20]<br>0x5F350A9B|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0007ff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800012f0]:fsqrt.s t6, t5, dyn<br> [0x800012f4]:csrrs a0, fcsr, zero<br> [0x800012f8]:sw t6, 64(t2)<br>     |
| 164|[0x80003a28]<br>0x5F7FFC00|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7ff800 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000130c]:fsqrt.s t6, t5, dyn<br> [0x80001310]:csrrs a0, fcsr, zero<br> [0x80001314]:sw t6, 72(t2)<br>     |
| 165|[0x80003a30]<br>0x5F3507C7|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0003ff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001328]:fsqrt.s t6, t5, dyn<br> [0x8000132c]:csrrs a0, fcsr, zero<br> [0x80001330]:sw t6, 80(t2)<br>     |
| 166|[0x80003a38]<br>0x5F7FFE00|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7ffc00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001344]:fsqrt.s t6, t5, dyn<br> [0x80001348]:csrrs a0, fcsr, zero<br> [0x8000134c]:sw t6, 88(t2)<br>     |
| 167|[0x80003a40]<br>0x5F35065D|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0001ff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001360]:fsqrt.s t6, t5, dyn<br> [0x80001364]:csrrs a0, fcsr, zero<br> [0x80001368]:sw t6, 96(t2)<br>     |
| 168|[0x80003a48]<br>0x5F7FFF00|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7ffe00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000137c]:fsqrt.s t6, t5, dyn<br> [0x80001380]:csrrs a0, fcsr, zero<br> [0x80001384]:sw t6, 104(t2)<br>    |
| 169|[0x80003a50]<br>0x5F3505A8|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x0000ff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001398]:fsqrt.s t6, t5, dyn<br> [0x8000139c]:csrrs a0, fcsr, zero<br> [0x800013a0]:sw t6, 112(t2)<br>    |
| 170|[0x80003a58]<br>0x5F7FFF80|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fff00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800013b4]:fsqrt.s t6, t5, dyn<br> [0x800013b8]:csrrs a0, fcsr, zero<br> [0x800013bc]:sw t6, 120(t2)<br>    |
| 171|[0x80003a60]<br>0x5F35054D|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x00007f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800013d0]:fsqrt.s t6, t5, dyn<br> [0x800013d4]:csrrs a0, fcsr, zero<br> [0x800013d8]:sw t6, 128(t2)<br>    |
| 172|[0x80003a68]<br>0x5F7FFFC0|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fff80 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800013ec]:fsqrt.s t6, t5, dyn<br> [0x800013f0]:csrrs a0, fcsr, zero<br> [0x800013f4]:sw t6, 136(t2)<br>    |
| 173|[0x80003a70]<br>0x5F350520|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x00003f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001408]:fsqrt.s t6, t5, dyn<br> [0x8000140c]:csrrs a0, fcsr, zero<br> [0x80001410]:sw t6, 144(t2)<br>    |
| 174|[0x80003a78]<br>0x5F7FFFE0|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffc0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001424]:fsqrt.s t6, t5, dyn<br> [0x80001428]:csrrs a0, fcsr, zero<br> [0x8000142c]:sw t6, 152(t2)<br>    |
| 175|[0x80003a80]<br>0x5F350509|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x00001f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001440]:fsqrt.s t6, t5, dyn<br> [0x80001444]:csrrs a0, fcsr, zero<br> [0x80001448]:sw t6, 160(t2)<br>    |
| 176|[0x80003a88]<br>0x5F7FFFF0|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffe0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000145c]:fsqrt.s t6, t5, dyn<br> [0x80001460]:csrrs a0, fcsr, zero<br> [0x80001464]:sw t6, 168(t2)<br>    |
| 177|[0x80003a90]<br>0x5F3504FE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x00000f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001478]:fsqrt.s t6, t5, dyn<br> [0x8000147c]:csrrs a0, fcsr, zero<br> [0x80001480]:sw t6, 176(t2)<br>    |
| 178|[0x80003a98]<br>0x5F7FFFF8|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7ffff0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001494]:fsqrt.s t6, t5, dyn<br> [0x80001498]:csrrs a0, fcsr, zero<br> [0x8000149c]:sw t6, 184(t2)<br>    |
| 179|[0x80003aa0]<br>0x5F3504F8|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000007 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800014b0]:fsqrt.s t6, t5, dyn<br> [0x800014b4]:csrrs a0, fcsr, zero<br> [0x800014b8]:sw t6, 192(t2)<br>    |
| 180|[0x80003aa8]<br>0x5F7FFFFC|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7ffff8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800014cc]:fsqrt.s t6, t5, dyn<br> [0x800014d0]:csrrs a0, fcsr, zero<br> [0x800014d4]:sw t6, 200(t2)<br>    |
| 181|[0x80003ab0]<br>0x5F3504F5|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000003 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800014e8]:fsqrt.s t6, t5, dyn<br> [0x800014ec]:csrrs a0, fcsr, zero<br> [0x800014f0]:sw t6, 208(t2)<br>    |
| 182|[0x80003ab8]<br>0x5F7FFFFE|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7ffffc and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001504]:fsqrt.s t6, t5, dyn<br> [0x80001508]:csrrs a0, fcsr, zero<br> [0x8000150c]:sw t6, 216(t2)<br>    |
| 183|[0x80003ac0]<br>0x5F3504F4|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001520]:fsqrt.s t6, t5, dyn<br> [0x80001524]:csrrs a0, fcsr, zero<br> [0x80001528]:sw t6, 224(t2)<br>    |
| 184|[0x80003ac8]<br>0x5F7FFFFF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7ffffe and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000153c]:fsqrt.s t6, t5, dyn<br> [0x80001540]:csrrs a0, fcsr, zero<br> [0x80001544]:sw t6, 232(t2)<br>    |
| 185|[0x80003ad0]<br>0x5F585C07|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x36db6d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001558]:fsqrt.s t6, t5, dyn<br> [0x8000155c]:csrrs a0, fcsr, zero<br> [0x80001560]:sw t6, 240(t2)<br>    |
| 186|[0x80003ad8]<br>0x5F76B012|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x6db6db and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001574]:fsqrt.s t6, t5, dyn<br> [0x80001578]:csrrs a0, fcsr, zero<br> [0x8000157c]:sw t6, 248(t2)<br>    |
| 187|[0x80003ae0]<br>0x5F464BF7|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x199999 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001590]:fsqrt.s t6, t5, dyn<br> [0x80001594]:csrrs a0, fcsr, zero<br> [0x80001598]:sw t6, 256(t2)<br>    |
| 188|[0x80003ae8]<br>0x5F72DCE8|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x666666 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800015ac]:fsqrt.s t6, t5, dyn<br> [0x800015b0]:csrrs a0, fcsr, zero<br> [0x800015b4]:sw t6, 264(t2)<br>    |
| 189|[0x80003af0]<br>0x5F5B39AD|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x3bbbbb and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800015c8]:fsqrt.s t6, t5, dyn<br> [0x800015cc]:csrrs a0, fcsr, zero<br> [0x800015d0]:sw t6, 272(t2)<br>    |
| 190|[0x80003af8]<br>0x5F6026FF|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x444444 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800015e4]:fsqrt.s t6, t5, dyn<br> [0x800015e8]:csrrs a0, fcsr, zero<br> [0x800015ec]:sw t6, 280(t2)<br>    |
| 191|[0x80003b00]<br>0x5F4D41B3|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x249249 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001600]:fsqrt.s t6, t5, dyn<br> [0x80001604]:csrrs a0, fcsr, zero<br> [0x80001608]:sw t6, 288(t2)<br>    |
| 192|[0x80003b08]<br>0x5F6D028B|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x5b6db6 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000161c]:fsqrt.s t6, t5, dyn<br> [0x80001620]:csrrs a0, fcsr, zero<br> [0x80001624]:sw t6, 296(t2)<br>    |
| 193|[0x80003b10]<br>0x5F562F5A|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x333333 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001638]:fsqrt.s t6, t5, dyn<br> [0x8000163c]:csrrs a0, fcsr, zero<br> [0x80001640]:sw t6, 304(t2)<br>    |
| 194|[0x80003b18]<br>0x5F64F92E|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x4ccccc and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001654]:fsqrt.s t6, t5, dyn<br> [0x80001658]:csrrs a0, fcsr, zero<br> [0x8000165c]:sw t6, 312(t2)<br>    |
| 195|[0x80003b28]<br>0x1C34AA5A|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x0000ff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000168c]:fsqrt.s t6, t5, dyn<br> [0x80001690]:csrrs a0, fcsr, zero<br> [0x80001694]:sw t6, 328(t2)<br>    |
