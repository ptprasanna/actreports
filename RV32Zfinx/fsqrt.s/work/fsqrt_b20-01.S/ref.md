
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800008a0')]      |
| SIG_REGION                | [('0x80002310', '0x80002530', '136 words')]      |
| COV_LABELS                | fsqrt_b20      |
| TEST_NAME                 | /home/reg/work/zfinx/fsqrt.s/work/fsqrt_b20-01.S/ref.S    |
| Total Number of coverpoints| 132     |
| Total Coverpoints Hit     | 132      |
| Total Signature Updates   | 136      |
| STAT1                     | 66      |
| STAT2                     | 2      |
| STAT3                     | 0     |
| STAT4                     | 68     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x8000084c]:fsqrt.s t6, t5, dyn
      [0x80000850]:csrrs a0, fcsr, zero
      [0x80000854]:sw t6, 312(t2)
 -- Signature Address: 0x80002518 Data: 0x00000000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fsqrt.s
      - rs1 : x30
      - rd : x31
      - rs1 != rd
      - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000884]:fsqrt.s t6, t5, dyn
      [0x80000888]:csrrs a0, fcsr, zero
      [0x8000088c]:sw t6, 328(t2)
 -- Signature Address: 0x80002528 Data: 0x344B4800
 -- Redundant Coverpoints hit by the op
      - mnemonic : fsqrt.s
      - rs1 : x30
      - rd : x31
      - rs1 != rd
      - fs1 == 0 and fe1 == 0x52 and fm1 == 0x216b44 and  fcsr == 0x0 and rm_val == 7   #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fsqrt.s', 'rs1 : x30', 'rd : x31', 'rs1 != rd', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000120]:fsqrt.s t6, t5, dyn
	-[0x80000124]:csrrs tp, fcsr, zero
	-[0x80000128]:sw t6, 0(ra)
Current Store : [0x8000012c] : sw tp, 4(ra) -- Store: [0x80002314]:0x00000001




Last Coverpoint : ['rs1 : x29', 'rd : x29', 'rs1 == rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000013c]:fsqrt.s t4, t4, dyn
	-[0x80000140]:csrrs tp, fcsr, zero
	-[0x80000144]:sw t4, 8(ra)
Current Store : [0x80000148] : sw tp, 12(ra) -- Store: [0x8000231c]:0x00000000




Last Coverpoint : ['rs1 : x31', 'rd : x30', 'fs1 == 0 and fe1 == 0x39 and fm1 == 0x480000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000158]:fsqrt.s t5, t6, dyn
	-[0x8000015c]:csrrs tp, fcsr, zero
	-[0x80000160]:sw t5, 16(ra)
Current Store : [0x80000164] : sw tp, 20(ra) -- Store: [0x80002324]:0x00000000




Last Coverpoint : ['rs1 : x27', 'rd : x28', 'fs1 == 0 and fe1 == 0xf9 and fm1 == 0x480000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000174]:fsqrt.s t3, s11, dyn
	-[0x80000178]:csrrs tp, fcsr, zero
	-[0x8000017c]:sw t3, 24(ra)
Current Store : [0x80000180] : sw tp, 28(ra) -- Store: [0x8000232c]:0x00000000




Last Coverpoint : ['rs1 : x28', 'rd : x27', 'fs1 == 0 and fe1 == 0xb7 and fm1 == 0x720000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000190]:fsqrt.s s11, t3, dyn
	-[0x80000194]:csrrs tp, fcsr, zero
	-[0x80000198]:sw s11, 32(ra)
Current Store : [0x8000019c] : sw tp, 36(ra) -- Store: [0x80002334]:0x00000000




Last Coverpoint : ['rs1 : x25', 'rd : x26', 'fs1 == 0 and fe1 == 0xc7 and fm1 == 0x720000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001ac]:fsqrt.s s10, s9, dyn
	-[0x800001b0]:csrrs tp, fcsr, zero
	-[0x800001b4]:sw s10, 40(ra)
Current Store : [0x800001b8] : sw tp, 44(ra) -- Store: [0x8000233c]:0x00000000




Last Coverpoint : ['rs1 : x26', 'rd : x25', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x610000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001c8]:fsqrt.s s9, s10, dyn
	-[0x800001cc]:csrrs tp, fcsr, zero
	-[0x800001d0]:sw s9, 48(ra)
Current Store : [0x800001d4] : sw tp, 52(ra) -- Store: [0x80002344]:0x00000000




Last Coverpoint : ['rs1 : x23', 'rd : x24', 'fs1 == 0 and fe1 == 0x86 and fm1 == 0x704000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001e4]:fsqrt.s s8, s7, dyn
	-[0x800001e8]:csrrs tp, fcsr, zero
	-[0x800001ec]:sw s8, 56(ra)
Current Store : [0x800001f0] : sw tp, 60(ra) -- Store: [0x8000234c]:0x00000000




Last Coverpoint : ['rs1 : x24', 'rd : x23', 'fs1 == 0 and fe1 == 0x97 and fm1 == 0x5c8000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000200]:fsqrt.s s7, s8, dyn
	-[0x80000204]:csrrs tp, fcsr, zero
	-[0x80000208]:sw s7, 64(ra)
Current Store : [0x8000020c] : sw tp, 68(ra) -- Store: [0x80002354]:0x00000000




Last Coverpoint : ['rs1 : x21', 'rd : x22', 'fs1 == 0 and fe1 == 0x82 and fm1 == 0x044000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000021c]:fsqrt.s s6, s5, dyn
	-[0x80000220]:csrrs tp, fcsr, zero
	-[0x80000224]:sw s6, 72(ra)
Current Store : [0x80000228] : sw tp, 76(ra) -- Store: [0x8000235c]:0x00000000




Last Coverpoint : ['rs1 : x22', 'rd : x21', 'fs1 == 0 and fe1 == 0x3b and fm1 == 0x108000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000238]:fsqrt.s s5, s6, dyn
	-[0x8000023c]:csrrs tp, fcsr, zero
	-[0x80000240]:sw s5, 80(ra)
Current Store : [0x80000244] : sw tp, 84(ra) -- Store: [0x80002364]:0x00000000




Last Coverpoint : ['rs1 : x19', 'rd : x20', 'fs1 == 0 and fe1 == 0x24 and fm1 == 0x689000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000254]:fsqrt.s s4, s3, dyn
	-[0x80000258]:csrrs tp, fcsr, zero
	-[0x8000025c]:sw s4, 88(ra)
Current Store : [0x80000260] : sw tp, 92(ra) -- Store: [0x8000236c]:0x00000000




Last Coverpoint : ['rs1 : x20', 'rd : x19', 'fs1 == 0 and fe1 == 0x59 and fm1 == 0x7d2000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000270]:fsqrt.s s3, s4, dyn
	-[0x80000274]:csrrs tp, fcsr, zero
	-[0x80000278]:sw s3, 96(ra)
Current Store : [0x8000027c] : sw tp, 100(ra) -- Store: [0x80002374]:0x00000000




Last Coverpoint : ['rs1 : x17', 'rd : x18', 'fs1 == 0 and fe1 == 0x8e and fm1 == 0x689000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000028c]:fsqrt.s s2, a7, dyn
	-[0x80000290]:csrrs tp, fcsr, zero
	-[0x80000294]:sw s2, 104(ra)
Current Store : [0x80000298] : sw tp, 108(ra) -- Store: [0x8000237c]:0x00000000




Last Coverpoint : ['rs1 : x18', 'rd : x17', 'fs1 == 0 and fe1 == 0x86 and fm1 == 0x130400 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002a8]:fsqrt.s a7, s2, dyn
	-[0x800002ac]:csrrs tp, fcsr, zero
	-[0x800002b0]:sw a7, 112(ra)
Current Store : [0x800002b4] : sw tp, 116(ra) -- Store: [0x80002384]:0x00000000




Last Coverpoint : ['rs1 : x15', 'rd : x16', 'fs1 == 0 and fe1 == 0x88 and fm1 == 0x7c0400 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002c4]:fsqrt.s a6, a5, dyn
	-[0x800002c8]:csrrs tp, fcsr, zero
	-[0x800002cc]:sw a6, 120(ra)
Current Store : [0x800002d0] : sw tp, 124(ra) -- Store: [0x8000238c]:0x00000000




Last Coverpoint : ['rs1 : x16', 'rd : x15', 'fs1 == 0 and fe1 == 0x66 and fm1 == 0x64c400 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002e0]:fsqrt.s a5, a6, dyn
	-[0x800002e4]:csrrs tp, fcsr, zero
	-[0x800002e8]:sw a5, 128(ra)
Current Store : [0x800002ec] : sw tp, 132(ra) -- Store: [0x80002394]:0x00000000




Last Coverpoint : ['rs1 : x13', 'rd : x14', 'fs1 == 0 and fe1 == 0xf9 and fm1 == 0x7ff200 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002fc]:fsqrt.s a4, a3, dyn
	-[0x80000300]:csrrs tp, fcsr, zero
	-[0x80000304]:sw a4, 136(ra)
Current Store : [0x80000308] : sw tp, 140(ra) -- Store: [0x8000239c]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rd : x13', 'fs1 == 0 and fe1 == 0x39 and fm1 == 0x69d200 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000318]:fsqrt.s a3, a4, dyn
	-[0x8000031c]:csrrs tp, fcsr, zero
	-[0x80000320]:sw a3, 144(ra)
Current Store : [0x80000324] : sw tp, 148(ra) -- Store: [0x800023a4]:0x00000000




Last Coverpoint : ['rs1 : x11', 'rd : x12', 'fs1 == 0 and fe1 == 0xef and fm1 == 0x7bb880 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000334]:fsqrt.s a2, a1, dyn
	-[0x80000338]:csrrs tp, fcsr, zero
	-[0x8000033c]:sw a2, 152(ra)
Current Store : [0x80000340] : sw tp, 156(ra) -- Store: [0x800023ac]:0x00000000




Last Coverpoint : ['rs1 : x12', 'rd : x11', 'fs1 == 0 and fe1 == 0xd0 and fm1 == 0x095440 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000350]:fsqrt.s a1, a2, dyn
	-[0x80000354]:csrrs tp, fcsr, zero
	-[0x80000358]:sw a1, 160(ra)
Current Store : [0x8000035c] : sw tp, 164(ra) -- Store: [0x800023b4]:0x00000000




Last Coverpoint : ['rs1 : x9', 'rd : x10', 'fs1 == 0 and fe1 == 0x7b and fm1 == 0x46c080 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000036c]:fsqrt.s a0, s1, dyn
	-[0x80000370]:csrrs tp, fcsr, zero
	-[0x80000374]:sw a0, 168(ra)
Current Store : [0x80000378] : sw tp, 172(ra) -- Store: [0x800023bc]:0x00000000




Last Coverpoint : ['rs1 : x10', 'rd : x9', 'fs1 == 0 and fe1 == 0x80 and fm1 == 0x6d5a40 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000388]:fsqrt.s s1, a0, dyn
	-[0x8000038c]:csrrs tp, fcsr, zero
	-[0x80000390]:sw s1, 176(ra)
Current Store : [0x80000394] : sw tp, 180(ra) -- Store: [0x800023c4]:0x00000000




Last Coverpoint : ['rs1 : x7', 'rd : x8', 'fs1 == 0 and fe1 == 0xd3 and fm1 == 0x6a7f20 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003a4]:fsqrt.s fp, t2, dyn
	-[0x800003a8]:csrrs tp, fcsr, zero
	-[0x800003ac]:sw fp, 184(ra)
Current Store : [0x800003b0] : sw tp, 188(ra) -- Store: [0x800023cc]:0x00000000




Last Coverpoint : ['rs1 : x8', 'rd : x7', 'fs1 == 0 and fe1 == 0x34 and fm1 == 0x08f690 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003c8]:fsqrt.s t2, fp, dyn
	-[0x800003cc]:csrrs a0, fcsr, zero
	-[0x800003d0]:sw t2, 192(ra)
Current Store : [0x800003d4] : sw a0, 196(ra) -- Store: [0x800023d4]:0x00000000




Last Coverpoint : ['rs1 : x5', 'rd : x6', 'fs1 == 0 and fe1 == 0x6a and fm1 == 0x3e2364 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003e4]:fsqrt.s t1, t0, dyn
	-[0x800003e8]:csrrs a0, fcsr, zero
	-[0x800003ec]:sw t1, 200(ra)
Current Store : [0x800003f0] : sw a0, 204(ra) -- Store: [0x800023dc]:0x00000000




Last Coverpoint : ['rs1 : x6', 'rd : x5', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000160 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000408]:fsqrt.s t0, t1, dyn
	-[0x8000040c]:csrrs a0, fcsr, zero
	-[0x80000410]:sw t0, 0(t2)
Current Store : [0x80000414] : sw a0, 4(t2) -- Store: [0x800023e4]:0x00000001




Last Coverpoint : ['rs1 : x3', 'rd : x4', 'fs1 == 0 and fe1 == 0xbc and fm1 == 0x68cd04 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000424]:fsqrt.s tp, gp, dyn
	-[0x80000428]:csrrs a0, fcsr, zero
	-[0x8000042c]:sw tp, 8(t2)
Current Store : [0x80000430] : sw a0, 12(t2) -- Store: [0x800023ec]:0x00000000




Last Coverpoint : ['rs1 : x4', 'rd : x3', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x5b5a62 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000440]:fsqrt.s gp, tp, dyn
	-[0x80000444]:csrrs a0, fcsr, zero
	-[0x80000448]:sw gp, 16(t2)
Current Store : [0x8000044c] : sw a0, 20(t2) -- Store: [0x800023f4]:0x00000000




Last Coverpoint : ['rs1 : x1', 'rd : x2', 'fs1 == 0 and fe1 == 0xea and fm1 == 0x4f33d9 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000045c]:fsqrt.s sp, ra, dyn
	-[0x80000460]:csrrs a0, fcsr, zero
	-[0x80000464]:sw sp, 24(t2)
Current Store : [0x80000468] : sw a0, 28(t2) -- Store: [0x800023fc]:0x00000000




Last Coverpoint : ['rs1 : x2', 'rd : x1', 'fs1 == 0 and fe1 == 0xa0 and fm1 == 0x10d851 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000478]:fsqrt.s ra, sp, dyn
	-[0x8000047c]:csrrs a0, fcsr, zero
	-[0x80000480]:sw ra, 32(t2)
Current Store : [0x80000484] : sw a0, 36(t2) -- Store: [0x80002404]:0x00000000




Last Coverpoint : ['rs1 : x0']
Last Code Sequence : 
	-[0x80000494]:fsqrt.s t6, zero, dyn
	-[0x80000498]:csrrs a0, fcsr, zero
	-[0x8000049c]:sw t6, 40(t2)
Current Store : [0x800004a0] : sw a0, 44(t2) -- Store: [0x8000240c]:0x00000000




Last Coverpoint : ['rd : x0', 'fs1 == 0 and fe1 == 0x52 and fm1 == 0x216b44 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004b0]:fsqrt.s zero, t6, dyn
	-[0x800004b4]:csrrs a0, fcsr, zero
	-[0x800004b8]:sw zero, 48(t2)
Current Store : [0x800004bc] : sw a0, 52(t2) -- Store: [0x80002414]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000005 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004cc]:fsqrt.s t6, t5, dyn
	-[0x800004d0]:csrrs a0, fcsr, zero
	-[0x800004d4]:sw t6, 56(t2)
Current Store : [0x800004d8] : sw a0, 60(t2) -- Store: [0x8000241c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3c and fm1 == 0x124e58 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004e8]:fsqrt.s t6, t5, dyn
	-[0x800004ec]:csrrs a0, fcsr, zero
	-[0x800004f0]:sw t6, 64(t2)
Current Store : [0x800004f4] : sw a0, 68(t2) -- Store: [0x80002424]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xc0 and fm1 == 0x3590aa and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000504]:fsqrt.s t6, t5, dyn
	-[0x80000508]:csrrs a0, fcsr, zero
	-[0x8000050c]:sw t6, 72(t2)
Current Store : [0x80000510] : sw a0, 76(t2) -- Store: [0x8000242c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf3 and fm1 == 0x6653ed and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000520]:fsqrt.s t6, t5, dyn
	-[0x80000524]:csrrs a0, fcsr, zero
	-[0x80000528]:sw t6, 80(t2)
Current Store : [0x8000052c] : sw a0, 84(t2) -- Store: [0x80002434]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7f3827 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000053c]:fsqrt.s t6, t5, dyn
	-[0x80000540]:csrrs a0, fcsr, zero
	-[0x80000544]:sw t6, 88(t2)
Current Store : [0x80000548] : sw a0, 92(t2) -- Store: [0x8000243c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xa7 and fm1 == 0x0f78f8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000558]:fsqrt.s t6, t5, dyn
	-[0x8000055c]:csrrs a0, fcsr, zero
	-[0x80000560]:sw t6, 96(t2)
Current Store : [0x80000564] : sw a0, 100(t2) -- Store: [0x80002444]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0b and fm1 == 0x0cd684 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000574]:fsqrt.s t6, t5, dyn
	-[0x80000578]:csrrs a0, fcsr, zero
	-[0x8000057c]:sw t6, 104(t2)
Current Store : [0x80000580] : sw a0, 108(t2) -- Store: [0x8000244c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xdd and fm1 == 0x4096e8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000590]:fsqrt.s t6, t5, dyn
	-[0x80000594]:csrrs a0, fcsr, zero
	-[0x80000598]:sw t6, 112(t2)
Current Store : [0x8000059c] : sw a0, 116(t2) -- Store: [0x80002454]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0cd173 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005ac]:fsqrt.s t6, t5, dyn
	-[0x800005b0]:csrrs a0, fcsr, zero
	-[0x800005b4]:sw t6, 120(t2)
Current Store : [0x800005b8] : sw a0, 124(t2) -- Store: [0x8000245c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x59 and fm1 == 0x0fed85 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005c8]:fsqrt.s t6, t5, dyn
	-[0x800005cc]:csrrs a0, fcsr, zero
	-[0x800005d0]:sw t6, 128(t2)
Current Store : [0x800005d4] : sw a0, 132(t2) -- Store: [0x80002464]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39 and fm1 == 0x0ef3b1 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005e4]:fsqrt.s t6, t5, dyn
	-[0x800005e8]:csrrs a0, fcsr, zero
	-[0x800005ec]:sw t6, 136(t2)
Current Store : [0x800005f0] : sw a0, 140(t2) -- Store: [0x8000246c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2bf296 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000600]:fsqrt.s t6, t5, dyn
	-[0x80000604]:csrrs a0, fcsr, zero
	-[0x80000608]:sw t6, 144(t2)
Current Store : [0x8000060c] : sw a0, 148(t2) -- Store: [0x80002474]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf5 and fm1 == 0x0aadc1 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000061c]:fsqrt.s t6, t5, dyn
	-[0x80000620]:csrrs a0, fcsr, zero
	-[0x80000624]:sw t6, 152(t2)
Current Store : [0x80000628] : sw a0, 156(t2) -- Store: [0x8000247c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f and fm1 == 0x0577a2 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000638]:fsqrt.s t6, t5, dyn
	-[0x8000063c]:csrrs a0, fcsr, zero
	-[0x80000640]:sw t6, 160(t2)
Current Store : [0x80000644] : sw a0, 164(t2) -- Store: [0x80002484]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x2b61ee and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000654]:fsqrt.s t6, t5, dyn
	-[0x80000658]:csrrs a0, fcsr, zero
	-[0x8000065c]:sw t6, 168(t2)
Current Store : [0x80000660] : sw a0, 172(t2) -- Store: [0x8000248c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xe4 and fm1 == 0x1477dc and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000670]:fsqrt.s t6, t5, dyn
	-[0x80000674]:csrrs a0, fcsr, zero
	-[0x80000678]:sw t6, 176(t2)
Current Store : [0x8000067c] : sw a0, 180(t2) -- Store: [0x80002494]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xad and fm1 == 0x75bbd8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000068c]:fsqrt.s t6, t5, dyn
	-[0x80000690]:csrrs a0, fcsr, zero
	-[0x80000694]:sw t6, 184(t2)
Current Store : [0x80000698] : sw a0, 188(t2) -- Store: [0x8000249c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x31 and fm1 == 0x011313 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006a8]:fsqrt.s t6, t5, dyn
	-[0x800006ac]:csrrs a0, fcsr, zero
	-[0x800006b0]:sw t6, 192(t2)
Current Store : [0x800006b4] : sw a0, 196(t2) -- Store: [0x800024a4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xc2 and fm1 == 0x26f9c3 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006c4]:fsqrt.s t6, t5, dyn
	-[0x800006c8]:csrrs a0, fcsr, zero
	-[0x800006cc]:sw t6, 200(t2)
Current Store : [0x800006d0] : sw a0, 204(t2) -- Store: [0x800024ac]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7b and fm1 == 0x64e1f0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006e0]:fsqrt.s t6, t5, dyn
	-[0x800006e4]:csrrs a0, fcsr, zero
	-[0x800006e8]:sw t6, 208(t2)
Current Store : [0x800006ec] : sw a0, 212(t2) -- Store: [0x800024b4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x65 and fm1 == 0x5b1e82 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006fc]:fsqrt.s t6, t5, dyn
	-[0x80000700]:csrrs a0, fcsr, zero
	-[0x80000704]:sw t6, 216(t2)
Current Store : [0x80000708] : sw a0, 220(t2) -- Store: [0x800024bc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xb6 and fm1 == 0x479816 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000718]:fsqrt.s t6, t5, dyn
	-[0x8000071c]:csrrs a0, fcsr, zero
	-[0x80000720]:sw t6, 224(t2)
Current Store : [0x80000724] : sw a0, 228(t2) -- Store: [0x800024c4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x4e and fm1 == 0x454542 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000734]:fsqrt.s t6, t5, dyn
	-[0x80000738]:csrrs a0, fcsr, zero
	-[0x8000073c]:sw t6, 232(t2)
Current Store : [0x80000740] : sw a0, 236(t2) -- Store: [0x800024cc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xea and fm1 == 0x284ae6 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000750]:fsqrt.s t6, t5, dyn
	-[0x80000754]:csrrs a0, fcsr, zero
	-[0x80000758]:sw t6, 240(t2)
Current Store : [0x8000075c] : sw a0, 244(t2) -- Store: [0x800024d4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xd3 and fm1 == 0x190acf and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000076c]:fsqrt.s t6, t5, dyn
	-[0x80000770]:csrrs a0, fcsr, zero
	-[0x80000774]:sw t6, 248(t2)
Current Store : [0x80000778] : sw a0, 252(t2) -- Store: [0x800024dc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x65 and fm1 == 0x064562 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000788]:fsqrt.s t6, t5, dyn
	-[0x8000078c]:csrrs a0, fcsr, zero
	-[0x80000790]:sw t6, 256(t2)
Current Store : [0x80000794] : sw a0, 260(t2) -- Store: [0x800024e4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xac and fm1 == 0x13884e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007a4]:fsqrt.s t6, t5, dyn
	-[0x800007a8]:csrrs a0, fcsr, zero
	-[0x800007ac]:sw t6, 264(t2)
Current Store : [0x800007b0] : sw a0, 268(t2) -- Store: [0x800024ec]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x79 and fm1 == 0x785c55 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007c0]:fsqrt.s t6, t5, dyn
	-[0x800007c4]:csrrs a0, fcsr, zero
	-[0x800007c8]:sw t6, 272(t2)
Current Store : [0x800007cc] : sw a0, 276(t2) -- Store: [0x800024f4]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x30 and fm1 == 0x75cb89 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007dc]:fsqrt.s t6, t5, dyn
	-[0x800007e0]:csrrs a0, fcsr, zero
	-[0x800007e4]:sw t6, 280(t2)
Current Store : [0x800007e8] : sw a0, 284(t2) -- Store: [0x800024fc]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xb7 and fm1 == 0x4bce51 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007f8]:fsqrt.s t6, t5, dyn
	-[0x800007fc]:csrrs a0, fcsr, zero
	-[0x80000800]:sw t6, 288(t2)
Current Store : [0x80000804] : sw a0, 292(t2) -- Store: [0x80002504]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xd0 and fm1 == 0x010151 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000814]:fsqrt.s t6, t5, dyn
	-[0x80000818]:csrrs a0, fcsr, zero
	-[0x8000081c]:sw t6, 296(t2)
Current Store : [0x80000820] : sw a0, 300(t2) -- Store: [0x8000250c]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x9e and fm1 == 0x38d874 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000830]:fsqrt.s t6, t5, dyn
	-[0x80000834]:csrrs a0, fcsr, zero
	-[0x80000838]:sw t6, 304(t2)
Current Store : [0x8000083c] : sw a0, 308(t2) -- Store: [0x80002514]:0x00000001




Last Coverpoint : ['mnemonic : fsqrt.s', 'rs1 : x30', 'rd : x31', 'rs1 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000084c]:fsqrt.s t6, t5, dyn
	-[0x80000850]:csrrs a0, fcsr, zero
	-[0x80000854]:sw t6, 312(t2)
Current Store : [0x80000858] : sw a0, 316(t2) -- Store: [0x8000251c]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xce and fm1 == 0x1168e1 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000868]:fsqrt.s t6, t5, dyn
	-[0x8000086c]:csrrs a0, fcsr, zero
	-[0x80000870]:sw t6, 320(t2)
Current Store : [0x80000874] : sw a0, 324(t2) -- Store: [0x80002524]:0x00000000




Last Coverpoint : ['mnemonic : fsqrt.s', 'rs1 : x30', 'rd : x31', 'rs1 != rd', 'fs1 == 0 and fe1 == 0x52 and fm1 == 0x216b44 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000884]:fsqrt.s t6, t5, dyn
	-[0x80000888]:csrrs a0, fcsr, zero
	-[0x8000088c]:sw t6, 328(t2)
Current Store : [0x80000890] : sw a0, 332(t2) -- Store: [0x8000252c]:0x00000001





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                             coverpoints                                                                             |                                                     code                                                     |
|---:|--------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------|
|   1|[0x80002310]<br>0x5F7FFFFF|- mnemonic : fsqrt.s<br> - rs1 : x30<br> - rd : x31<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x80000120]:fsqrt.s t6, t5, dyn<br> [0x80000124]:csrrs tp, fcsr, zero<br> [0x80000128]:sw t6, 0(ra)<br>      |
|   2|[0x80002318]<br>0x00000000|- rs1 : x29<br> - rd : x29<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                          |[0x8000013c]:fsqrt.s t4, t4, dyn<br> [0x80000140]:csrrs tp, fcsr, zero<br> [0x80000144]:sw t4, 8(ra)<br>      |
|   3|[0x80002320]<br>0x2E200000|- rs1 : x31<br> - rd : x30<br> - fs1 == 0 and fe1 == 0x39 and fm1 == 0x480000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000158]:fsqrt.s t5, t6, dyn<br> [0x8000015c]:csrrs tp, fcsr, zero<br> [0x80000160]:sw t5, 16(ra)<br>     |
|   4|[0x80002328]<br>0x5E200000|- rs1 : x27<br> - rd : x28<br> - fs1 == 0 and fe1 == 0xf9 and fm1 == 0x480000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000174]:fsqrt.s t3, s11, dyn<br> [0x80000178]:csrrs tp, fcsr, zero<br> [0x8000017c]:sw t3, 24(ra)<br>    |
|   5|[0x80002330]<br>0x4DB00000|- rs1 : x28<br> - rd : x27<br> - fs1 == 0 and fe1 == 0xb7 and fm1 == 0x720000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000190]:fsqrt.s s11, t3, dyn<br> [0x80000194]:csrrs tp, fcsr, zero<br> [0x80000198]:sw s11, 32(ra)<br>   |
|   6|[0x80002338]<br>0x51B00000|- rs1 : x25<br> - rd : x26<br> - fs1 == 0 and fe1 == 0xc7 and fm1 == 0x720000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800001ac]:fsqrt.s s10, s9, dyn<br> [0x800001b0]:csrrs tp, fcsr, zero<br> [0x800001b4]:sw s10, 40(ra)<br>   |
|   7|[0x80002340]<br>0x5CF00000|- rs1 : x26<br> - rd : x25<br> - fs1 == 0 and fe1 == 0xf4 and fm1 == 0x610000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800001c8]:fsqrt.s s9, s10, dyn<br> [0x800001cc]:csrrs tp, fcsr, zero<br> [0x800001d0]:sw s9, 48(ra)<br>    |
|   8|[0x80002348]<br>0x41780000|- rs1 : x23<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x86 and fm1 == 0x704000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800001e4]:fsqrt.s s8, s7, dyn<br> [0x800001e8]:csrrs tp, fcsr, zero<br> [0x800001ec]:sw s8, 56(ra)<br>     |
|   9|[0x80002350]<br>0x45A80000|- rs1 : x24<br> - rd : x23<br> - fs1 == 0 and fe1 == 0x97 and fm1 == 0x5c8000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000200]:fsqrt.s s7, s8, dyn<br> [0x80000204]:csrrs tp, fcsr, zero<br> [0x80000208]:sw s7, 64(ra)<br>     |
|  10|[0x80002358]<br>0x40380000|- rs1 : x21<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x82 and fm1 == 0x044000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x8000021c]:fsqrt.s s6, s5, dyn<br> [0x80000220]:csrrs tp, fcsr, zero<br> [0x80000224]:sw s6, 72(ra)<br>     |
|  11|[0x80002360]<br>0x2E880000|- rs1 : x22<br> - rd : x21<br> - fs1 == 0 and fe1 == 0x3b and fm1 == 0x108000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000238]:fsqrt.s s5, s6, dyn<br> [0x8000023c]:csrrs tp, fcsr, zero<br> [0x80000240]:sw s5, 80(ra)<br>     |
|  12|[0x80002368]<br>0x28F40000|- rs1 : x19<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x24 and fm1 == 0x689000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000254]:fsqrt.s s4, s3, dyn<br> [0x80000258]:csrrs tp, fcsr, zero<br> [0x8000025c]:sw s4, 88(ra)<br>     |
|  13|[0x80002370]<br>0x36340000|- rs1 : x20<br> - rd : x19<br> - fs1 == 0 and fe1 == 0x59 and fm1 == 0x7d2000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000270]:fsqrt.s s3, s4, dyn<br> [0x80000274]:csrrs tp, fcsr, zero<br> [0x80000278]:sw s3, 96(ra)<br>     |
|  14|[0x80002378]<br>0x43740000|- rs1 : x17<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x8e and fm1 == 0x689000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x8000028c]:fsqrt.s s2, a7, dyn<br> [0x80000290]:csrrs tp, fcsr, zero<br> [0x80000294]:sw s2, 104(ra)<br>    |
|  15|[0x80002380]<br>0x41420000|- rs1 : x18<br> - rd : x17<br> - fs1 == 0 and fe1 == 0x86 and fm1 == 0x130400 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800002a8]:fsqrt.s a7, s2, dyn<br> [0x800002ac]:csrrs tp, fcsr, zero<br> [0x800002b0]:sw a7, 112(ra)<br>    |
|  16|[0x80002388]<br>0x41FE0000|- rs1 : x15<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x88 and fm1 == 0x7c0400 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800002c4]:fsqrt.s a6, a5, dyn<br> [0x800002c8]:csrrs tp, fcsr, zero<br> [0x800002cc]:sw a6, 120(ra)<br>    |
|  17|[0x80002390]<br>0x39720000|- rs1 : x16<br> - rd : x15<br> - fs1 == 0 and fe1 == 0x66 and fm1 == 0x64c400 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800002e0]:fsqrt.s a5, a6, dyn<br> [0x800002e4]:csrrs tp, fcsr, zero<br> [0x800002e8]:sw a5, 128(ra)<br>    |
|  18|[0x80002398]<br>0x5E350000|- rs1 : x13<br> - rd : x14<br> - fs1 == 0 and fe1 == 0xf9 and fm1 == 0x7ff200 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800002fc]:fsqrt.s a4, a3, dyn<br> [0x80000300]:csrrs tp, fcsr, zero<br> [0x80000304]:sw a4, 136(ra)<br>    |
|  19|[0x800023a0]<br>0x2E2D0000|- rs1 : x14<br> - rd : x13<br> - fs1 == 0 and fe1 == 0x39 and fm1 == 0x69d200 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000318]:fsqrt.s a3, a4, dyn<br> [0x8000031c]:csrrs tp, fcsr, zero<br> [0x80000320]:sw a3, 144(ra)<br>    |
|  20|[0x800023a8]<br>0x5BB38000|- rs1 : x11<br> - rd : x12<br> - fs1 == 0 and fe1 == 0xef and fm1 == 0x7bb880 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000334]:fsqrt.s a2, a1, dyn<br> [0x80000338]:csrrs tp, fcsr, zero<br> [0x8000033c]:sw a2, 152(ra)<br>    |
|  21|[0x800023b0]<br>0x53BB8000|- rs1 : x12<br> - rd : x11<br> - fs1 == 0 and fe1 == 0xd0 and fm1 == 0x095440 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000350]:fsqrt.s a1, a2, dyn<br> [0x80000354]:csrrs tp, fcsr, zero<br> [0x80000358]:sw a1, 160(ra)<br>    |
|  22|[0x800023b8]<br>0x3E9F8000|- rs1 : x9<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x7b and fm1 == 0x46c080 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x8000036c]:fsqrt.s a0, s1, dyn<br> [0x80000370]:csrrs tp, fcsr, zero<br> [0x80000374]:sw a0, 168(ra)<br>    |
|  23|[0x800023c0]<br>0x3FF68000|- rs1 : x10<br> - rd : x9<br> - fs1 == 0 and fe1 == 0x80 and fm1 == 0x6d5a40 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x80000388]:fsqrt.s s1, a0, dyn<br> [0x8000038c]:csrrs tp, fcsr, zero<br> [0x80000390]:sw s1, 176(ra)<br>    |
|  24|[0x800023c8]<br>0x54AD4000|- rs1 : x7<br> - rd : x8<br> - fs1 == 0 and fe1 == 0xd3 and fm1 == 0x6a7f20 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x800003a4]:fsqrt.s fp, t2, dyn<br> [0x800003a8]:csrrs tp, fcsr, zero<br> [0x800003ac]:sw fp, 184(ra)<br>    |
|  25|[0x800023d0]<br>0x2CBB4000|- rs1 : x8<br> - rd : x7<br> - fs1 == 0 and fe1 == 0x34 and fm1 == 0x08f690 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x800003c8]:fsqrt.s t2, fp, dyn<br> [0x800003cc]:csrrs a0, fcsr, zero<br> [0x800003d0]:sw t2, 192(ra)<br>    |
|  26|[0x800023d8]<br>0x3A5CA000|- rs1 : x5<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x6a and fm1 == 0x3e2364 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x800003e4]:fsqrt.s t1, t0, dyn<br> [0x800003e8]:csrrs a0, fcsr, zero<br> [0x800003ec]:sw t1, 200(ra)<br>    |
|  27|[0x800023e0]<br>0x1C544395|- rs1 : x6<br> - rd : x5<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000160 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x80000408]:fsqrt.s t0, t1, dyn<br> [0x8000040c]:csrrs a0, fcsr, zero<br> [0x80000410]:sw t0, 0(t2)<br>      |
|  28|[0x800023e8]<br>0x4EF42000|- rs1 : x3<br> - rd : x4<br> - fs1 == 0 and fe1 == 0xbc and fm1 == 0x68cd04 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x80000424]:fsqrt.s tp, gp, dyn<br> [0x80000428]:csrrs a0, fcsr, zero<br> [0x8000042c]:sw tp, 8(t2)<br>      |
|  29|[0x800023f0]<br>0x26A79000|- rs1 : x4<br> - rd : x3<br> - fs1 == 0 and fe1 == 0x1b and fm1 == 0x5b5a62 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x80000440]:fsqrt.s gp, tp, dyn<br> [0x80000444]:csrrs a0, fcsr, zero<br> [0x80000448]:sw gp, 16(t2)<br>     |
|  30|[0x800023f8]<br>0x5A665000|- rs1 : x1<br> - rd : x2<br> - fs1 == 0 and fe1 == 0xea and fm1 == 0x4f33d9 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x8000045c]:fsqrt.s sp, ra, dyn<br> [0x80000460]:csrrs a0, fcsr, zero<br> [0x80000464]:sw sp, 24(t2)<br>     |
|  31|[0x80002400]<br>0x47C09000|- rs1 : x2<br> - rd : x1<br> - fs1 == 0 and fe1 == 0xa0 and fm1 == 0x10d851 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x80000478]:fsqrt.s ra, sp, dyn<br> [0x8000047c]:csrrs a0, fcsr, zero<br> [0x80000480]:sw ra, 32(t2)<br>     |
|  32|[0x80002408]<br>0x00000000|- rs1 : x0<br>                                                                                                                                                       |[0x80000494]:fsqrt.s t6, zero, dyn<br> [0x80000498]:csrrs a0, fcsr, zero<br> [0x8000049c]:sw t6, 40(t2)<br>   |
|  33|[0x80002410]<br>0x00000000|- rd : x0<br> - fs1 == 0 and fe1 == 0x52 and fm1 == 0x216b44 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                           |[0x800004b0]:fsqrt.s zero, t6, dyn<br> [0x800004b4]:csrrs a0, fcsr, zero<br> [0x800004b8]:sw zero, 48(t2)<br> |
|  34|[0x80002418]<br>0x1ACA62C2|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000005 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800004cc]:fsqrt.s t6, t5, dyn<br> [0x800004d0]:csrrs a0, fcsr, zero<br> [0x800004d4]:sw t6, 56(t2)<br>     |
|  35|[0x80002420]<br>0x2EC18800|- fs1 == 0 and fe1 == 0x3c and fm1 == 0x124e58 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800004e8]:fsqrt.s t6, t5, dyn<br> [0x800004ec]:csrrs a0, fcsr, zero<br> [0x800004f0]:sw t6, 64(t2)<br>     |
|  36|[0x80002428]<br>0x4FD79800|- fs1 == 0 and fe1 == 0xc0 and fm1 == 0x3590aa and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000504]:fsqrt.s t6, t5, dyn<br> [0x80000508]:csrrs a0, fcsr, zero<br> [0x8000050c]:sw t6, 72(t2)<br>     |
|  37|[0x80002430]<br>0x5CABB400|- fs1 == 0 and fe1 == 0xf3 and fm1 == 0x6653ed and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000520]:fsqrt.s t6, t5, dyn<br> [0x80000524]:csrrs a0, fcsr, zero<br> [0x80000528]:sw t6, 80(t2)<br>     |
|  38|[0x80002438]<br>0x5F7F9C00|- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7f3827 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000053c]:fsqrt.s t6, t5, dyn<br> [0x80000540]:csrrs a0, fcsr, zero<br> [0x80000544]:sw t6, 88(t2)<br>     |
|  39|[0x80002440]<br>0x49878400|- fs1 == 0 and fe1 == 0xa7 and fm1 == 0x0f78f8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000558]:fsqrt.s t6, t5, dyn<br> [0x8000055c]:csrrs a0, fcsr, zero<br> [0x80000560]:sw t6, 96(t2)<br>     |
|  40|[0x80002448]<br>0x22864400|- fs1 == 0 and fe1 == 0x0b and fm1 == 0x0cd684 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000574]:fsqrt.s t6, t5, dyn<br> [0x80000578]:csrrs a0, fcsr, zero<br> [0x8000057c]:sw t6, 104(t2)<br>    |
|  41|[0x80002450]<br>0x571D0200|- fs1 == 0 and fe1 == 0xdd and fm1 == 0x4096e8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000590]:fsqrt.s t6, t5, dyn<br> [0x80000594]:csrrs a0, fcsr, zero<br> [0x80000598]:sw t6, 112(t2)<br>    |
|  42|[0x80002458]<br>0x1F220600|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x0cd173 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800005ac]:fsqrt.s t6, t5, dyn<br> [0x800005b0]:csrrs a0, fcsr, zero<br> [0x800005b4]:sw t6, 120(t2)<br>    |
|  43|[0x80002460]<br>0x3607BB00|- fs1 == 0 and fe1 == 0x59 and fm1 == 0x0fed85 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800005c8]:fsqrt.s t6, t5, dyn<br> [0x800005cc]:csrrs a0, fcsr, zero<br> [0x800005d0]:sw t6, 128(t2)<br>    |
|  44|[0x80002468]<br>0x2E074500|- fs1 == 0 and fe1 == 0x39 and fm1 == 0x0ef3b1 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800005e4]:fsqrt.s t6, t5, dyn<br> [0x800005e8]:csrrs a0, fcsr, zero<br> [0x800005ec]:sw t6, 136(t2)<br>    |
|  45|[0x80002470]<br>0x1F960100|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2bf296 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000600]:fsqrt.s t6, t5, dyn<br> [0x80000604]:csrrs a0, fcsr, zero<br> [0x80000608]:sw t6, 144(t2)<br>    |
|  46|[0x80002478]<br>0x5D053B80|- fs1 == 0 and fe1 == 0xf5 and fm1 == 0x0aadc1 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000061c]:fsqrt.s t6, t5, dyn<br> [0x80000620]:csrrs a0, fcsr, zero<br> [0x80000624]:sw t6, 152(t2)<br>    |
|  47|[0x80002480]<br>0x2F82B480|- fs1 == 0 and fe1 == 0x3f and fm1 == 0x0577a2 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000638]:fsqrt.s t6, t5, dyn<br> [0x8000063c]:csrrs a0, fcsr, zero<br> [0x80000640]:sw t6, 160(t2)<br>    |
|  48|[0x80002488]<br>0x5E141C80|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x2b61ee and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000654]:fsqrt.s t6, t5, dyn<br> [0x80000658]:csrrs a0, fcsr, zero<br> [0x8000065c]:sw t6, 168(t2)<br>    |
|  49|[0x80002490]<br>0x58C2F4BF|- fs1 == 0 and fe1 == 0xe4 and fm1 == 0x1477dc and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000670]:fsqrt.s t6, t5, dyn<br> [0x80000674]:csrrs a0, fcsr, zero<br> [0x80000678]:sw t6, 176(t2)<br>    |
|  50|[0x80002498]<br>0x4B315A40|- fs1 == 0 and fe1 == 0xad and fm1 == 0x75bbd8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000068c]:fsqrt.s t6, t5, dyn<br> [0x80000690]:csrrs a0, fcsr, zero<br> [0x80000694]:sw t6, 184(t2)<br>    |
|  51|[0x800024a0]<br>0x2C008940|- fs1 == 0 and fe1 == 0x31 and fm1 == 0x011313 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800006a8]:fsqrt.s t6, t5, dyn<br> [0x800006ac]:csrrs a0, fcsr, zero<br> [0x800006b0]:sw t6, 192(t2)<br>    |
|  52|[0x800024a8]<br>0x504EC020|- fs1 == 0 and fe1 == 0xc2 and fm1 == 0x26f9c3 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800006c4]:fsqrt.s t6, t5, dyn<br> [0x800006c8]:csrrs a0, fcsr, zero<br> [0x800006cc]:sw t6, 200(t2)<br>    |
|  53|[0x800024b0]<br>0x3EAB29E0|- fs1 == 0 and fe1 == 0x7b and fm1 == 0x64e1f0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800006e0]:fsqrt.s t6, t5, dyn<br> [0x800006e4]:csrrs a0, fcsr, zero<br> [0x800006e8]:sw t6, 208(t2)<br>    |
|  54|[0x800024b8]<br>0x39277920|- fs1 == 0 and fe1 == 0x65 and fm1 == 0x5b1e82 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800006fc]:fsqrt.s t6, t5, dyn<br> [0x80000700]:csrrs a0, fcsr, zero<br> [0x80000704]:sw t6, 216(t2)<br>    |
|  55|[0x800024c0]<br>0x4D620B60|- fs1 == 0 and fe1 == 0xb6 and fm1 == 0x479816 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000718]:fsqrt.s t6, t5, dyn<br> [0x8000071c]:csrrs a0, fcsr, zero<br> [0x80000720]:sw t6, 224(t2)<br>    |
|  56|[0x800024c8]<br>0x3360B990|- fs1 == 0 and fe1 == 0x4e and fm1 == 0x454542 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000734]:fsqrt.s t6, t5, dyn<br> [0x80000738]:csrrs a0, fcsr, zero<br> [0x8000073c]:sw t6, 232(t2)<br>    |
|  57|[0x800024d0]<br>0x5A4F9070|- fs1 == 0 and fe1 == 0xea and fm1 == 0x284ae6 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000750]:fsqrt.s t6, t5, dyn<br> [0x80000754]:csrrs a0, fcsr, zero<br> [0x80000758]:sw t6, 240(t2)<br>    |
|  58|[0x800024d8]<br>0x548BF650|- fs1 == 0 and fe1 == 0xd3 and fm1 == 0x190acf and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000076c]:fsqrt.s t6, t5, dyn<br> [0x80000770]:csrrs a0, fcsr, zero<br> [0x80000774]:sw t6, 248(t2)<br>    |
|  59|[0x800024e0]<br>0x39031918|- fs1 == 0 and fe1 == 0x65 and fm1 == 0x064562 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000788]:fsqrt.s t6, t5, dyn<br> [0x8000078c]:csrrs a0, fcsr, zero<br> [0x80000790]:sw t6, 256(t2)<br>    |
|  60|[0x800024e8]<br>0x4AC25738|- fs1 == 0 and fe1 == 0xac and fm1 == 0x13884e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800007a4]:fsqrt.s t6, t5, dyn<br> [0x800007a8]:csrrs a0, fcsr, zero<br> [0x800007ac]:sw t6, 264(t2)<br>    |
|  61|[0x800024f0]<br>0x3E324C48|- fs1 == 0 and fe1 == 0x79 and fm1 == 0x785c55 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800007c0]:fsqrt.s t6, t5, dyn<br> [0x800007c4]:csrrs a0, fcsr, zero<br> [0x800007c8]:sw t6, 272(t2)<br>    |
|  62|[0x800024f8]<br>0x2BFAD87C|- fs1 == 0 and fe1 == 0x30 and fm1 == 0x75cb89 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800007dc]:fsqrt.s t6, t5, dyn<br> [0x800007e0]:csrrs a0, fcsr, zero<br> [0x800007e4]:sw t6, 280(t2)<br>    |
|  63|[0x80002500]<br>0x4DA183E4|- fs1 == 0 and fe1 == 0xb7 and fm1 == 0x4bce51 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800007f8]:fsqrt.s t6, t5, dyn<br> [0x800007fc]:csrrs a0, fcsr, zero<br> [0x80000800]:sw t6, 288(t2)<br>    |
|  64|[0x80002508]<br>0x53B5BA8B|- fs1 == 0 and fe1 == 0xd0 and fm1 == 0x010151 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000814]:fsqrt.s t6, t5, dyn<br> [0x80000818]:csrrs a0, fcsr, zero<br> [0x8000081c]:sw t6, 296(t2)<br>    |
|  65|[0x80002510]<br>0x4759885C|- fs1 == 0 and fe1 == 0x9e and fm1 == 0x38d874 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000830]:fsqrt.s t6, t5, dyn<br> [0x80000834]:csrrs a0, fcsr, zero<br> [0x80000838]:sw t6, 304(t2)<br>    |
|  66|[0x80002520]<br>0x5340F000|- fs1 == 0 and fe1 == 0xce and fm1 == 0x1168e1 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000868]:fsqrt.s t6, t5, dyn<br> [0x8000086c]:csrrs a0, fcsr, zero<br> [0x80000870]:sw t6, 320(t2)<br>    |
