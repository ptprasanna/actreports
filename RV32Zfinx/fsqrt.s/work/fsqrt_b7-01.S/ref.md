
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000520')]      |
| SIG_REGION                | [('0x80002210', '0x80002330', '72 words')]      |
| COV_LABELS                | fsqrt_b7      |
| TEST_NAME                 | /home/reg/work/zfinx/fsqrt.s/work/fsqrt_b7-01.S/ref.S    |
| Total Number of coverpoints| 70     |
| Total Coverpoints Hit     | 70      |
| Total Signature Updates   | 72      |
| STAT1                     | 33      |
| STAT2                     | 3      |
| STAT3                     | 0     |
| STAT4                     | 36     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x800004cc]:fsqrt.s t6, t5, dyn
      [0x800004d0]:csrrs a0, fcsr, zero
      [0x800004d4]:sw t6, 56(t2)
 -- Signature Address: 0x80002318 Data: 0x55000000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fsqrt.s
      - rs1 : x30
      - rd : x31
      - rs1 != rd
      - fs1 == 0 and fe1 == 0xd5 and fm1 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x800004e8]:fsqrt.s t6, t5, dyn
      [0x800004ec]:csrrs a0, fcsr, zero
      [0x800004f0]:sw t6, 64(t2)
 -- Signature Address: 0x80002320 Data: 0x00000000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fsqrt.s
      - rs1 : x30
      - rd : x31
      - rs1 != rd




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000504]:fsqrt.s t6, t5, dyn
      [0x80000508]:csrrs a0, fcsr, zero
      [0x8000050c]:sw t6, 72(t2)
 -- Signature Address: 0x80002328 Data: 0x00000000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fsqrt.s
      - rs1 : x30
      - rd : x31
      - rs1 != rd






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fsqrt.s', 'rs1 : x30', 'rd : x31', 'rs1 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000120]:fsqrt.s t6, t5, dyn
	-[0x80000124]:csrrs tp, fcsr, zero
	-[0x80000128]:sw t6, 0(ra)
Current Store : [0x8000012c] : sw tp, 4(ra) -- Store: [0x80002214]:0x00000060




Last Coverpoint : ['rs1 : x29', 'rd : x29', 'rs1 == rd', 'fs1 == 0 and fe1 == 0xd5 and fm1 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000013c]:fsqrt.s t4, t4, dyn
	-[0x80000140]:csrrs tp, fcsr, zero
	-[0x80000144]:sw t4, 8(ra)
Current Store : [0x80000148] : sw tp, 12(ra) -- Store: [0x8000221c]:0x00000060




Last Coverpoint : ['rs1 : x31', 'rd : x30', 'fs1 == 0 and fe1 == 0x29 and fm1 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000158]:fsqrt.s t5, t6, dyn
	-[0x8000015c]:csrrs tp, fcsr, zero
	-[0x80000160]:sw t5, 16(ra)
Current Store : [0x80000164] : sw tp, 20(ra) -- Store: [0x80002224]:0x00000060




Last Coverpoint : ['rs1 : x27', 'rd : x28']
Last Code Sequence : 
	-[0x80000174]:fsqrt.s t3, s11, dyn
	-[0x80000178]:csrrs tp, fcsr, zero
	-[0x8000017c]:sw t3, 24(ra)
Current Store : [0x80000180] : sw tp, 28(ra) -- Store: [0x8000222c]:0x00000000




Last Coverpoint : ['rs1 : x28', 'rd : x27']
Last Code Sequence : 
	-[0x80000190]:fsqrt.s s11, t3, dyn
	-[0x80000194]:csrrs tp, fcsr, zero
	-[0x80000198]:sw s11, 32(ra)
Current Store : [0x8000019c] : sw tp, 36(ra) -- Store: [0x80002234]:0x00000000




Last Coverpoint : ['rs1 : x25', 'rd : x26']
Last Code Sequence : 
	-[0x800001ac]:fsqrt.s s10, s9, dyn
	-[0x800001b0]:csrrs tp, fcsr, zero
	-[0x800001b4]:sw s10, 40(ra)
Current Store : [0x800001b8] : sw tp, 44(ra) -- Store: [0x8000223c]:0x00000000




Last Coverpoint : ['rs1 : x26', 'rd : x25']
Last Code Sequence : 
	-[0x800001c8]:fsqrt.s s9, s10, dyn
	-[0x800001cc]:csrrs tp, fcsr, zero
	-[0x800001d0]:sw s9, 48(ra)
Current Store : [0x800001d4] : sw tp, 52(ra) -- Store: [0x80002244]:0x00000000




Last Coverpoint : ['rs1 : x23', 'rd : x24']
Last Code Sequence : 
	-[0x800001e4]:fsqrt.s s8, s7, dyn
	-[0x800001e8]:csrrs tp, fcsr, zero
	-[0x800001ec]:sw s8, 56(ra)
Current Store : [0x800001f0] : sw tp, 60(ra) -- Store: [0x8000224c]:0x00000000




Last Coverpoint : ['rs1 : x24', 'rd : x23']
Last Code Sequence : 
	-[0x80000200]:fsqrt.s s7, s8, dyn
	-[0x80000204]:csrrs tp, fcsr, zero
	-[0x80000208]:sw s7, 64(ra)
Current Store : [0x8000020c] : sw tp, 68(ra) -- Store: [0x80002254]:0x00000000




Last Coverpoint : ['rs1 : x21', 'rd : x22']
Last Code Sequence : 
	-[0x8000021c]:fsqrt.s s6, s5, dyn
	-[0x80000220]:csrrs tp, fcsr, zero
	-[0x80000224]:sw s6, 72(ra)
Current Store : [0x80000228] : sw tp, 76(ra) -- Store: [0x8000225c]:0x00000000




Last Coverpoint : ['rs1 : x22', 'rd : x21']
Last Code Sequence : 
	-[0x80000238]:fsqrt.s s5, s6, dyn
	-[0x8000023c]:csrrs tp, fcsr, zero
	-[0x80000240]:sw s5, 80(ra)
Current Store : [0x80000244] : sw tp, 84(ra) -- Store: [0x80002264]:0x00000000




Last Coverpoint : ['rs1 : x19', 'rd : x20']
Last Code Sequence : 
	-[0x80000254]:fsqrt.s s4, s3, dyn
	-[0x80000258]:csrrs tp, fcsr, zero
	-[0x8000025c]:sw s4, 88(ra)
Current Store : [0x80000260] : sw tp, 92(ra) -- Store: [0x8000226c]:0x00000000




Last Coverpoint : ['rs1 : x20', 'rd : x19']
Last Code Sequence : 
	-[0x80000270]:fsqrt.s s3, s4, dyn
	-[0x80000274]:csrrs tp, fcsr, zero
	-[0x80000278]:sw s3, 96(ra)
Current Store : [0x8000027c] : sw tp, 100(ra) -- Store: [0x80002274]:0x00000000




Last Coverpoint : ['rs1 : x17', 'rd : x18']
Last Code Sequence : 
	-[0x8000028c]:fsqrt.s s2, a7, dyn
	-[0x80000290]:csrrs tp, fcsr, zero
	-[0x80000294]:sw s2, 104(ra)
Current Store : [0x80000298] : sw tp, 108(ra) -- Store: [0x8000227c]:0x00000000




Last Coverpoint : ['rs1 : x18', 'rd : x17']
Last Code Sequence : 
	-[0x800002a8]:fsqrt.s a7, s2, dyn
	-[0x800002ac]:csrrs tp, fcsr, zero
	-[0x800002b0]:sw a7, 112(ra)
Current Store : [0x800002b4] : sw tp, 116(ra) -- Store: [0x80002284]:0x00000000




Last Coverpoint : ['rs1 : x15', 'rd : x16']
Last Code Sequence : 
	-[0x800002c4]:fsqrt.s a6, a5, dyn
	-[0x800002c8]:csrrs tp, fcsr, zero
	-[0x800002cc]:sw a6, 120(ra)
Current Store : [0x800002d0] : sw tp, 124(ra) -- Store: [0x8000228c]:0x00000000




Last Coverpoint : ['rs1 : x16', 'rd : x15']
Last Code Sequence : 
	-[0x800002e0]:fsqrt.s a5, a6, dyn
	-[0x800002e4]:csrrs tp, fcsr, zero
	-[0x800002e8]:sw a5, 128(ra)
Current Store : [0x800002ec] : sw tp, 132(ra) -- Store: [0x80002294]:0x00000000




Last Coverpoint : ['rs1 : x13', 'rd : x14']
Last Code Sequence : 
	-[0x800002fc]:fsqrt.s a4, a3, dyn
	-[0x80000300]:csrrs tp, fcsr, zero
	-[0x80000304]:sw a4, 136(ra)
Current Store : [0x80000308] : sw tp, 140(ra) -- Store: [0x8000229c]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rd : x13']
Last Code Sequence : 
	-[0x80000318]:fsqrt.s a3, a4, dyn
	-[0x8000031c]:csrrs tp, fcsr, zero
	-[0x80000320]:sw a3, 144(ra)
Current Store : [0x80000324] : sw tp, 148(ra) -- Store: [0x800022a4]:0x00000000




Last Coverpoint : ['rs1 : x11', 'rd : x12']
Last Code Sequence : 
	-[0x80000334]:fsqrt.s a2, a1, dyn
	-[0x80000338]:csrrs tp, fcsr, zero
	-[0x8000033c]:sw a2, 152(ra)
Current Store : [0x80000340] : sw tp, 156(ra) -- Store: [0x800022ac]:0x00000000




Last Coverpoint : ['rs1 : x12', 'rd : x11']
Last Code Sequence : 
	-[0x80000350]:fsqrt.s a1, a2, dyn
	-[0x80000354]:csrrs tp, fcsr, zero
	-[0x80000358]:sw a1, 160(ra)
Current Store : [0x8000035c] : sw tp, 164(ra) -- Store: [0x800022b4]:0x00000000




Last Coverpoint : ['rs1 : x9', 'rd : x10']
Last Code Sequence : 
	-[0x8000036c]:fsqrt.s a0, s1, dyn
	-[0x80000370]:csrrs tp, fcsr, zero
	-[0x80000374]:sw a0, 168(ra)
Current Store : [0x80000378] : sw tp, 172(ra) -- Store: [0x800022bc]:0x00000000




Last Coverpoint : ['rs1 : x10', 'rd : x9']
Last Code Sequence : 
	-[0x80000388]:fsqrt.s s1, a0, dyn
	-[0x8000038c]:csrrs tp, fcsr, zero
	-[0x80000390]:sw s1, 176(ra)
Current Store : [0x80000394] : sw tp, 180(ra) -- Store: [0x800022c4]:0x00000000




Last Coverpoint : ['rs1 : x7', 'rd : x8']
Last Code Sequence : 
	-[0x800003a4]:fsqrt.s fp, t2, dyn
	-[0x800003a8]:csrrs tp, fcsr, zero
	-[0x800003ac]:sw fp, 184(ra)
Current Store : [0x800003b0] : sw tp, 188(ra) -- Store: [0x800022cc]:0x00000000




Last Coverpoint : ['rs1 : x8', 'rd : x7']
Last Code Sequence : 
	-[0x800003c8]:fsqrt.s t2, fp, dyn
	-[0x800003cc]:csrrs a0, fcsr, zero
	-[0x800003d0]:sw t2, 192(ra)
Current Store : [0x800003d4] : sw a0, 196(ra) -- Store: [0x800022d4]:0x00000000




Last Coverpoint : ['rs1 : x5', 'rd : x6']
Last Code Sequence : 
	-[0x800003e4]:fsqrt.s t1, t0, dyn
	-[0x800003e8]:csrrs a0, fcsr, zero
	-[0x800003ec]:sw t1, 200(ra)
Current Store : [0x800003f0] : sw a0, 204(ra) -- Store: [0x800022dc]:0x00000000




Last Coverpoint : ['rs1 : x6', 'rd : x5']
Last Code Sequence : 
	-[0x80000408]:fsqrt.s t0, t1, dyn
	-[0x8000040c]:csrrs a0, fcsr, zero
	-[0x80000410]:sw t0, 0(t2)
Current Store : [0x80000414] : sw a0, 4(t2) -- Store: [0x800022e4]:0x00000000




Last Coverpoint : ['rs1 : x3', 'rd : x4']
Last Code Sequence : 
	-[0x80000424]:fsqrt.s tp, gp, dyn
	-[0x80000428]:csrrs a0, fcsr, zero
	-[0x8000042c]:sw tp, 8(t2)
Current Store : [0x80000430] : sw a0, 12(t2) -- Store: [0x800022ec]:0x00000000




Last Coverpoint : ['rs1 : x4', 'rd : x3']
Last Code Sequence : 
	-[0x80000440]:fsqrt.s gp, tp, dyn
	-[0x80000444]:csrrs a0, fcsr, zero
	-[0x80000448]:sw gp, 16(t2)
Current Store : [0x8000044c] : sw a0, 20(t2) -- Store: [0x800022f4]:0x00000000




Last Coverpoint : ['rs1 : x1', 'rd : x2']
Last Code Sequence : 
	-[0x8000045c]:fsqrt.s sp, ra, dyn
	-[0x80000460]:csrrs a0, fcsr, zero
	-[0x80000464]:sw sp, 24(t2)
Current Store : [0x80000468] : sw a0, 28(t2) -- Store: [0x800022fc]:0x00000000




Last Coverpoint : ['rs1 : x2', 'rd : x1']
Last Code Sequence : 
	-[0x80000478]:fsqrt.s ra, sp, dyn
	-[0x8000047c]:csrrs a0, fcsr, zero
	-[0x80000480]:sw ra, 32(t2)
Current Store : [0x80000484] : sw a0, 36(t2) -- Store: [0x80002304]:0x00000000




Last Coverpoint : ['rs1 : x0']
Last Code Sequence : 
	-[0x80000494]:fsqrt.s t6, zero, dyn
	-[0x80000498]:csrrs a0, fcsr, zero
	-[0x8000049c]:sw t6, 40(t2)
Current Store : [0x800004a0] : sw a0, 44(t2) -- Store: [0x8000230c]:0x00000000




Last Coverpoint : ['rd : x0']
Last Code Sequence : 
	-[0x800004b0]:fsqrt.s zero, t6, dyn
	-[0x800004b4]:csrrs a0, fcsr, zero
	-[0x800004b8]:sw zero, 48(t2)
Current Store : [0x800004bc] : sw a0, 52(t2) -- Store: [0x80002314]:0x00000000




Last Coverpoint : ['mnemonic : fsqrt.s', 'rs1 : x30', 'rd : x31', 'rs1 != rd', 'fs1 == 0 and fe1 == 0xd5 and fm1 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004cc]:fsqrt.s t6, t5, dyn
	-[0x800004d0]:csrrs a0, fcsr, zero
	-[0x800004d4]:sw t6, 56(t2)
Current Store : [0x800004d8] : sw a0, 60(t2) -- Store: [0x8000231c]:0x00000060




Last Coverpoint : ['mnemonic : fsqrt.s', 'rs1 : x30', 'rd : x31', 'rs1 != rd']
Last Code Sequence : 
	-[0x800004e8]:fsqrt.s t6, t5, dyn
	-[0x800004ec]:csrrs a0, fcsr, zero
	-[0x800004f0]:sw t6, 64(t2)
Current Store : [0x800004f4] : sw a0, 68(t2) -- Store: [0x80002324]:0x00000000




Last Coverpoint : ['mnemonic : fsqrt.s', 'rs1 : x30', 'rd : x31', 'rs1 != rd']
Last Code Sequence : 
	-[0x80000504]:fsqrt.s t6, t5, dyn
	-[0x80000508]:csrrs a0, fcsr, zero
	-[0x8000050c]:sw t6, 72(t2)
Current Store : [0x80000510] : sw a0, 76(t2) -- Store: [0x8000232c]:0x00000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                             coverpoints                                                                              |                                                     code                                                     |
|---:|--------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------|
|   1|[0x80002210]<br>0x00000000|- mnemonic : fsqrt.s<br> - rs1 : x30<br> - rd : x31<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br> |[0x80000120]:fsqrt.s t6, t5, dyn<br> [0x80000124]:csrrs tp, fcsr, zero<br> [0x80000128]:sw t6, 0(ra)<br>      |
|   2|[0x80002218]<br>0x55000000|- rs1 : x29<br> - rd : x29<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0xd5 and fm1 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                          |[0x8000013c]:fsqrt.s t4, t4, dyn<br> [0x80000140]:csrrs tp, fcsr, zero<br> [0x80000144]:sw t4, 8(ra)<br>      |
|   3|[0x80002220]<br>0x2A000000|- rs1 : x31<br> - rd : x30<br> - fs1 == 0 and fe1 == 0x29 and fm1 == 0x000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                          |[0x80000158]:fsqrt.s t5, t6, dyn<br> [0x8000015c]:csrrs tp, fcsr, zero<br> [0x80000160]:sw t5, 16(ra)<br>     |
|   4|[0x80002228]<br>0x00000000|- rs1 : x27<br> - rd : x28<br>                                                                                                                                        |[0x80000174]:fsqrt.s t3, s11, dyn<br> [0x80000178]:csrrs tp, fcsr, zero<br> [0x8000017c]:sw t3, 24(ra)<br>    |
|   5|[0x80002230]<br>0x00000000|- rs1 : x28<br> - rd : x27<br>                                                                                                                                        |[0x80000190]:fsqrt.s s11, t3, dyn<br> [0x80000194]:csrrs tp, fcsr, zero<br> [0x80000198]:sw s11, 32(ra)<br>   |
|   6|[0x80002238]<br>0x00000000|- rs1 : x25<br> - rd : x26<br>                                                                                                                                        |[0x800001ac]:fsqrt.s s10, s9, dyn<br> [0x800001b0]:csrrs tp, fcsr, zero<br> [0x800001b4]:sw s10, 40(ra)<br>   |
|   7|[0x80002240]<br>0x00000000|- rs1 : x26<br> - rd : x25<br>                                                                                                                                        |[0x800001c8]:fsqrt.s s9, s10, dyn<br> [0x800001cc]:csrrs tp, fcsr, zero<br> [0x800001d0]:sw s9, 48(ra)<br>    |
|   8|[0x80002248]<br>0x00000000|- rs1 : x23<br> - rd : x24<br>                                                                                                                                        |[0x800001e4]:fsqrt.s s8, s7, dyn<br> [0x800001e8]:csrrs tp, fcsr, zero<br> [0x800001ec]:sw s8, 56(ra)<br>     |
|   9|[0x80002250]<br>0x00000000|- rs1 : x24<br> - rd : x23<br>                                                                                                                                        |[0x80000200]:fsqrt.s s7, s8, dyn<br> [0x80000204]:csrrs tp, fcsr, zero<br> [0x80000208]:sw s7, 64(ra)<br>     |
|  10|[0x80002258]<br>0x00000000|- rs1 : x21<br> - rd : x22<br>                                                                                                                                        |[0x8000021c]:fsqrt.s s6, s5, dyn<br> [0x80000220]:csrrs tp, fcsr, zero<br> [0x80000224]:sw s6, 72(ra)<br>     |
|  11|[0x80002260]<br>0x00000000|- rs1 : x22<br> - rd : x21<br>                                                                                                                                        |[0x80000238]:fsqrt.s s5, s6, dyn<br> [0x8000023c]:csrrs tp, fcsr, zero<br> [0x80000240]:sw s5, 80(ra)<br>     |
|  12|[0x80002268]<br>0x00000000|- rs1 : x19<br> - rd : x20<br>                                                                                                                                        |[0x80000254]:fsqrt.s s4, s3, dyn<br> [0x80000258]:csrrs tp, fcsr, zero<br> [0x8000025c]:sw s4, 88(ra)<br>     |
|  13|[0x80002270]<br>0x00000000|- rs1 : x20<br> - rd : x19<br>                                                                                                                                        |[0x80000270]:fsqrt.s s3, s4, dyn<br> [0x80000274]:csrrs tp, fcsr, zero<br> [0x80000278]:sw s3, 96(ra)<br>     |
|  14|[0x80002278]<br>0x00000000|- rs1 : x17<br> - rd : x18<br>                                                                                                                                        |[0x8000028c]:fsqrt.s s2, a7, dyn<br> [0x80000290]:csrrs tp, fcsr, zero<br> [0x80000294]:sw s2, 104(ra)<br>    |
|  15|[0x80002280]<br>0x00000000|- rs1 : x18<br> - rd : x17<br>                                                                                                                                        |[0x800002a8]:fsqrt.s a7, s2, dyn<br> [0x800002ac]:csrrs tp, fcsr, zero<br> [0x800002b0]:sw a7, 112(ra)<br>    |
|  16|[0x80002288]<br>0x00000000|- rs1 : x15<br> - rd : x16<br>                                                                                                                                        |[0x800002c4]:fsqrt.s a6, a5, dyn<br> [0x800002c8]:csrrs tp, fcsr, zero<br> [0x800002cc]:sw a6, 120(ra)<br>    |
|  17|[0x80002290]<br>0x00000000|- rs1 : x16<br> - rd : x15<br>                                                                                                                                        |[0x800002e0]:fsqrt.s a5, a6, dyn<br> [0x800002e4]:csrrs tp, fcsr, zero<br> [0x800002e8]:sw a5, 128(ra)<br>    |
|  18|[0x80002298]<br>0x00000000|- rs1 : x13<br> - rd : x14<br>                                                                                                                                        |[0x800002fc]:fsqrt.s a4, a3, dyn<br> [0x80000300]:csrrs tp, fcsr, zero<br> [0x80000304]:sw a4, 136(ra)<br>    |
|  19|[0x800022a0]<br>0x00000000|- rs1 : x14<br> - rd : x13<br>                                                                                                                                        |[0x80000318]:fsqrt.s a3, a4, dyn<br> [0x8000031c]:csrrs tp, fcsr, zero<br> [0x80000320]:sw a3, 144(ra)<br>    |
|  20|[0x800022a8]<br>0x00000000|- rs1 : x11<br> - rd : x12<br>                                                                                                                                        |[0x80000334]:fsqrt.s a2, a1, dyn<br> [0x80000338]:csrrs tp, fcsr, zero<br> [0x8000033c]:sw a2, 152(ra)<br>    |
|  21|[0x800022b0]<br>0x00000000|- rs1 : x12<br> - rd : x11<br>                                                                                                                                        |[0x80000350]:fsqrt.s a1, a2, dyn<br> [0x80000354]:csrrs tp, fcsr, zero<br> [0x80000358]:sw a1, 160(ra)<br>    |
|  22|[0x800022b8]<br>0x00000000|- rs1 : x9<br> - rd : x10<br>                                                                                                                                         |[0x8000036c]:fsqrt.s a0, s1, dyn<br> [0x80000370]:csrrs tp, fcsr, zero<br> [0x80000374]:sw a0, 168(ra)<br>    |
|  23|[0x800022c0]<br>0x00000000|- rs1 : x10<br> - rd : x9<br>                                                                                                                                         |[0x80000388]:fsqrt.s s1, a0, dyn<br> [0x8000038c]:csrrs tp, fcsr, zero<br> [0x80000390]:sw s1, 176(ra)<br>    |
|  24|[0x800022c8]<br>0x00000000|- rs1 : x7<br> - rd : x8<br>                                                                                                                                          |[0x800003a4]:fsqrt.s fp, t2, dyn<br> [0x800003a8]:csrrs tp, fcsr, zero<br> [0x800003ac]:sw fp, 184(ra)<br>    |
|  25|[0x800022d0]<br>0x00000000|- rs1 : x8<br> - rd : x7<br>                                                                                                                                          |[0x800003c8]:fsqrt.s t2, fp, dyn<br> [0x800003cc]:csrrs a0, fcsr, zero<br> [0x800003d0]:sw t2, 192(ra)<br>    |
|  26|[0x800022d8]<br>0x00000000|- rs1 : x5<br> - rd : x6<br>                                                                                                                                          |[0x800003e4]:fsqrt.s t1, t0, dyn<br> [0x800003e8]:csrrs a0, fcsr, zero<br> [0x800003ec]:sw t1, 200(ra)<br>    |
|  27|[0x800022e0]<br>0x00000000|- rs1 : x6<br> - rd : x5<br>                                                                                                                                          |[0x80000408]:fsqrt.s t0, t1, dyn<br> [0x8000040c]:csrrs a0, fcsr, zero<br> [0x80000410]:sw t0, 0(t2)<br>      |
|  28|[0x800022e8]<br>0x00000000|- rs1 : x3<br> - rd : x4<br>                                                                                                                                          |[0x80000424]:fsqrt.s tp, gp, dyn<br> [0x80000428]:csrrs a0, fcsr, zero<br> [0x8000042c]:sw tp, 8(t2)<br>      |
|  29|[0x800022f0]<br>0x00000000|- rs1 : x4<br> - rd : x3<br>                                                                                                                                          |[0x80000440]:fsqrt.s gp, tp, dyn<br> [0x80000444]:csrrs a0, fcsr, zero<br> [0x80000448]:sw gp, 16(t2)<br>     |
|  30|[0x800022f8]<br>0x00000000|- rs1 : x1<br> - rd : x2<br>                                                                                                                                          |[0x8000045c]:fsqrt.s sp, ra, dyn<br> [0x80000460]:csrrs a0, fcsr, zero<br> [0x80000464]:sw sp, 24(t2)<br>     |
|  31|[0x80002300]<br>0x00000000|- rs1 : x2<br> - rd : x1<br>                                                                                                                                          |[0x80000478]:fsqrt.s ra, sp, dyn<br> [0x8000047c]:csrrs a0, fcsr, zero<br> [0x80000480]:sw ra, 32(t2)<br>     |
|  32|[0x80002308]<br>0x00000000|- rs1 : x0<br>                                                                                                                                                        |[0x80000494]:fsqrt.s t6, zero, dyn<br> [0x80000498]:csrrs a0, fcsr, zero<br> [0x8000049c]:sw t6, 40(t2)<br>   |
|  33|[0x80002310]<br>0x00000000|- rd : x0<br>                                                                                                                                                         |[0x800004b0]:fsqrt.s zero, t6, dyn<br> [0x800004b4]:csrrs a0, fcsr, zero<br> [0x800004b8]:sw zero, 48(t2)<br> |
