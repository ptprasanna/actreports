
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x800008a0')]      |
| SIG_REGION                | [('0x80002310', '0x800025f0', '92 dwords')]      |
| COV_LABELS                | fcvt.lu.d_b23      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV64Zdinx/work-fcvtlud/fcvt.lu.d_b23-01.S/ref.S    |
| Total Number of coverpoints| 76     |
| Total Coverpoints Hit     | 76      |
| Total Signature Updates   | 90      |
| STAT1                     | 45      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                                  signature                                   |                                                                          coverpoints                                                                          |                                                                    code                                                                     |
|---:|------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002318]<br>0x7FFFFFFFFFFFF000<br> [0x80002320]<br>0x0000000000000000<br> |- mnemonic : fcvt.lu.d<br> - rs1 : x28<br> - rd : x30<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x800003b8]:fcvt.lu.d t5, t3, dyn<br> [0x800003bc]:csrrs t2, fcsr, zero<br> [0x800003c0]:sd t5, 0(ra)<br> [0x800003c4]:sd t2, 8(ra)<br>     |
|   2|[0x80002328]<br>0x7FFFFFFFFFFFF000<br> [0x80002330]<br>0x0000000000000020<br> |- rs1 : x30<br> - rd : x28<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and  fcsr == 0x20 and rm_val == 7   #nosat<br>                           |[0x800003d4]:fcvt.lu.d t3, t5, dyn<br> [0x800003d8]:csrrs t2, fcsr, zero<br> [0x800003dc]:sd t3, 16(ra)<br> [0x800003e0]:sd t2, 24(ra)<br>   |
|   3|[0x80002338]<br>0x7FFFFFFFFFFFF000<br> [0x80002340]<br>0x0000000000000040<br> |- rs1 : x24<br> - rd : x26<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and  fcsr == 0x40 and rm_val == 7   #nosat<br>                           |[0x800003f0]:fcvt.lu.d s10, s8, dyn<br> [0x800003f4]:csrrs t2, fcsr, zero<br> [0x800003f8]:sd s10, 32(ra)<br> [0x800003fc]:sd t2, 40(ra)<br> |
|   4|[0x80002348]<br>0x7FFFFFFFFFFFF000<br> [0x80002350]<br>0x0000000000000060<br> |- rs1 : x26<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and  fcsr == 0x60 and rm_val == 7   #nosat<br>                           |[0x8000040c]:fcvt.lu.d s8, s10, dyn<br> [0x80000410]:csrrs t2, fcsr, zero<br> [0x80000414]:sd s8, 48(ra)<br> [0x80000418]:sd t2, 56(ra)<br>  |
|   5|[0x80002358]<br>0x7FFFFFFFFFFFF000<br> [0x80002360]<br>0x0000000000000080<br> |- rs1 : x20<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and  fcsr == 0x80 and rm_val == 7   #nosat<br>                           |[0x80000428]:fcvt.lu.d s6, s4, dyn<br> [0x8000042c]:csrrs t2, fcsr, zero<br> [0x80000430]:sd s6, 64(ra)<br> [0x80000434]:sd t2, 72(ra)<br>   |
|   6|[0x80002368]<br>0x7FFFFFFFFFFFF400<br> [0x80002370]<br>0x0000000000000000<br> |- rs1 : x22<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffd and  fcsr == 0x0 and rm_val == 7   #nosat<br>                            |[0x80000444]:fcvt.lu.d s4, s6, dyn<br> [0x80000448]:csrrs t2, fcsr, zero<br> [0x8000044c]:sd s4, 80(ra)<br> [0x80000450]:sd t2, 88(ra)<br>   |
|   7|[0x80002378]<br>0x7FFFFFFFFFFFF400<br> [0x80002380]<br>0x0000000000000020<br> |- rs1 : x16<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffd and  fcsr == 0x20 and rm_val == 7   #nosat<br>                           |[0x80000460]:fcvt.lu.d s2, a6, dyn<br> [0x80000464]:csrrs t2, fcsr, zero<br> [0x80000468]:sd s2, 96(ra)<br> [0x8000046c]:sd t2, 104(ra)<br>  |
|   8|[0x80002388]<br>0x7FFFFFFFFFFFF400<br> [0x80002390]<br>0x0000000000000040<br> |- rs1 : x18<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffd and  fcsr == 0x40 and rm_val == 7   #nosat<br>                           |[0x8000047c]:fcvt.lu.d a6, s2, dyn<br> [0x80000480]:csrrs t2, fcsr, zero<br> [0x80000484]:sd a6, 112(ra)<br> [0x80000488]:sd t2, 120(ra)<br> |
|   9|[0x80002398]<br>0x7FFFFFFFFFFFF400<br> [0x800023a0]<br>0x0000000000000060<br> |- rs1 : x12<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffd and  fcsr == 0x60 and rm_val == 7   #nosat<br>                           |[0x80000498]:fcvt.lu.d a4, a2, dyn<br> [0x8000049c]:csrrs t2, fcsr, zero<br> [0x800004a0]:sd a4, 128(ra)<br> [0x800004a4]:sd t2, 136(ra)<br> |
|  10|[0x800023a8]<br>0x7FFFFFFFFFFFF400<br> [0x800023b0]<br>0x0000000000000080<br> |- rs1 : x14<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffd and  fcsr == 0x80 and rm_val == 7   #nosat<br>                           |[0x800004b4]:fcvt.lu.d a2, a4, dyn<br> [0x800004b8]:csrrs t2, fcsr, zero<br> [0x800004bc]:sd a2, 144(ra)<br> [0x800004c0]:sd t2, 152(ra)<br> |
|  11|[0x800023b8]<br>0x7FFFFFFFFFFFF800<br> [0x800023c0]<br>0x0000000000000000<br> |- rs1 : x8<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffe and  fcsr == 0x0 and rm_val == 7   #nosat<br>                             |[0x800004d0]:fcvt.lu.d a0, fp, dyn<br> [0x800004d4]:csrrs t2, fcsr, zero<br> [0x800004d8]:sd a0, 160(ra)<br> [0x800004dc]:sd t2, 168(ra)<br> |
|  12|[0x800023c8]<br>0x7FFFFFFFFFFFF800<br> [0x800023d0]<br>0x0000000000000020<br> |- rs1 : x10<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffe and  fcsr == 0x20 and rm_val == 7   #nosat<br>                            |[0x800004ec]:fcvt.lu.d fp, a0, dyn<br> [0x800004f0]:csrrs t2, fcsr, zero<br> [0x800004f4]:sd fp, 176(ra)<br> [0x800004f8]:sd t2, 184(ra)<br> |
|  13|[0x800023d8]<br>0x7FFFFFFFFFFFF800<br> [0x800023e0]<br>0x0000000000000040<br> |- rs1 : x4<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffe and  fcsr == 0x40 and rm_val == 7   #nosat<br>                             |[0x80000508]:fcvt.lu.d t1, tp, dyn<br> [0x8000050c]:csrrs t2, fcsr, zero<br> [0x80000510]:sd t1, 192(ra)<br> [0x80000514]:sd t2, 200(ra)<br> |
|  14|[0x800023e8]<br>0x7FFFFFFFFFFFF800<br> [0x800023f0]<br>0x0000000000000060<br> |- rs1 : x6<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffe and  fcsr == 0x60 and rm_val == 7   #nosat<br>                             |[0x80000524]:fcvt.lu.d tp, t1, dyn<br> [0x80000528]:csrrs t2, fcsr, zero<br> [0x8000052c]:sd tp, 208(ra)<br> [0x80000530]:sd t2, 216(ra)<br> |
|  15|[0x800023f8]<br>0x7FFFFFFFFFFFF800<br> [0x80002400]<br>0x0000000000000080<br> |- rs1 : x2<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffe and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                           |[0x80000540]:fcvt.lu.d t6, sp, dyn<br> [0x80000544]:csrrs t2, fcsr, zero<br> [0x80000548]:sd t6, 224(ra)<br> [0x8000054c]:sd t2, 232(ra)<br> |
|  16|[0x80002408]<br>0x7FFFFFFFFFFFFC00<br> [0x80002410]<br>0x0000000000000000<br> |- rd : x2<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                             |[0x8000055c]:fcvt.lu.d sp, t6, dyn<br> [0x80000560]:csrrs t2, fcsr, zero<br> [0x80000564]:sd sp, 240(ra)<br> [0x80000568]:sd t2, 248(ra)<br> |
|  17|[0x80002418]<br>0x7FFFFFFFFFFFFC00<br> [0x80002420]<br>0x0000000000000020<br> |- fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                          |[0x80000578]:fcvt.lu.d t6, t5, dyn<br> [0x8000057c]:csrrs t2, fcsr, zero<br> [0x80000580]:sd t6, 256(ra)<br> [0x80000584]:sd t2, 264(ra)<br> |
|  18|[0x80002428]<br>0x7FFFFFFFFFFFFC00<br> [0x80002430]<br>0x0000000000000040<br> |- fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                          |[0x80000594]:fcvt.lu.d t6, t5, dyn<br> [0x80000598]:csrrs t2, fcsr, zero<br> [0x8000059c]:sd t6, 272(ra)<br> [0x800005a0]:sd t2, 280(ra)<br> |
|  19|[0x80002438]<br>0x7FFFFFFFFFFFFC00<br> [0x80002440]<br>0x0000000000000060<br> |- fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                          |[0x800005b0]:fcvt.lu.d t6, t5, dyn<br> [0x800005b4]:csrrs t2, fcsr, zero<br> [0x800005b8]:sd t6, 288(ra)<br> [0x800005bc]:sd t2, 296(ra)<br> |
|  20|[0x80002448]<br>0x7FFFFFFFFFFFFC00<br> [0x80002450]<br>0x0000000000000080<br> |- fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                          |[0x800005cc]:fcvt.lu.d t6, t5, dyn<br> [0x800005d0]:csrrs t2, fcsr, zero<br> [0x800005d4]:sd t6, 304(ra)<br> [0x800005d8]:sd t2, 312(ra)<br> |
|  21|[0x80002458]<br>0x8000000000000000<br> [0x80002460]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                           |[0x800005e8]:fcvt.lu.d t6, t5, dyn<br> [0x800005ec]:csrrs t2, fcsr, zero<br> [0x800005f0]:sd t6, 320(ra)<br> [0x800005f4]:sd t2, 328(ra)<br> |
|  22|[0x80002468]<br>0x8000000000000000<br> [0x80002470]<br>0x0000000000000020<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                          |[0x80000604]:fcvt.lu.d t6, t5, dyn<br> [0x80000608]:csrrs t2, fcsr, zero<br> [0x8000060c]:sd t6, 336(ra)<br> [0x80000610]:sd t2, 344(ra)<br> |
|  23|[0x80002478]<br>0x8000000000000000<br> [0x80002480]<br>0x0000000000000040<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                          |[0x80000620]:fcvt.lu.d t6, t5, dyn<br> [0x80000624]:csrrs t2, fcsr, zero<br> [0x80000628]:sd t6, 352(ra)<br> [0x8000062c]:sd t2, 360(ra)<br> |
|  24|[0x80002488]<br>0x8000000000000000<br> [0x80002490]<br>0x0000000000000060<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                          |[0x8000063c]:fcvt.lu.d t6, t5, dyn<br> [0x80000640]:csrrs t2, fcsr, zero<br> [0x80000644]:sd t6, 368(ra)<br> [0x80000648]:sd t2, 376(ra)<br> |
|  25|[0x80002498]<br>0x8000000000000000<br> [0x800024a0]<br>0x0000000000000080<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                          |[0x80000658]:fcvt.lu.d t6, t5, dyn<br> [0x8000065c]:csrrs t2, fcsr, zero<br> [0x80000660]:sd t6, 384(ra)<br> [0x80000664]:sd t2, 392(ra)<br> |
|  26|[0x800024a8]<br>0x8000000000000800<br> [0x800024b0]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                           |[0x80000674]:fcvt.lu.d t6, t5, dyn<br> [0x80000678]:csrrs t2, fcsr, zero<br> [0x8000067c]:sd t6, 400(ra)<br> [0x80000680]:sd t2, 408(ra)<br> |
|  27|[0x800024b8]<br>0x8000000000000800<br> [0x800024c0]<br>0x0000000000000020<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                          |[0x80000690]:fcvt.lu.d t6, t5, dyn<br> [0x80000694]:csrrs t2, fcsr, zero<br> [0x80000698]:sd t6, 416(ra)<br> [0x8000069c]:sd t2, 424(ra)<br> |
|  28|[0x800024c8]<br>0x8000000000000800<br> [0x800024d0]<br>0x0000000000000040<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                          |[0x800006ac]:fcvt.lu.d t6, t5, dyn<br> [0x800006b0]:csrrs t2, fcsr, zero<br> [0x800006b4]:sd t6, 432(ra)<br> [0x800006b8]:sd t2, 440(ra)<br> |
|  29|[0x800024d8]<br>0x8000000000000800<br> [0x800024e0]<br>0x0000000000000060<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                          |[0x800006c8]:fcvt.lu.d t6, t5, dyn<br> [0x800006cc]:csrrs t2, fcsr, zero<br> [0x800006d0]:sd t6, 448(ra)<br> [0x800006d4]:sd t2, 456(ra)<br> |
|  30|[0x800024e8]<br>0x8000000000000800<br> [0x800024f0]<br>0x0000000000000080<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                          |[0x800006e4]:fcvt.lu.d t6, t5, dyn<br> [0x800006e8]:csrrs t2, fcsr, zero<br> [0x800006ec]:sd t6, 464(ra)<br> [0x800006f0]:sd t2, 472(ra)<br> |
|  31|[0x800024f8]<br>0x8000000000001000<br> [0x80002500]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                           |[0x80000700]:fcvt.lu.d t6, t5, dyn<br> [0x80000704]:csrrs t2, fcsr, zero<br> [0x80000708]:sd t6, 480(ra)<br> [0x8000070c]:sd t2, 488(ra)<br> |
|  32|[0x80002508]<br>0x8000000000001000<br> [0x80002510]<br>0x0000000000000020<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                          |[0x8000071c]:fcvt.lu.d t6, t5, dyn<br> [0x80000720]:csrrs t2, fcsr, zero<br> [0x80000724]:sd t6, 496(ra)<br> [0x80000728]:sd t2, 504(ra)<br> |
|  33|[0x80002518]<br>0x8000000000001000<br> [0x80002520]<br>0x0000000000000040<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                          |[0x80000738]:fcvt.lu.d t6, t5, dyn<br> [0x8000073c]:csrrs t2, fcsr, zero<br> [0x80000740]:sd t6, 512(ra)<br> [0x80000744]:sd t2, 520(ra)<br> |
|  34|[0x80002528]<br>0x8000000000001000<br> [0x80002530]<br>0x0000000000000060<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                          |[0x80000754]:fcvt.lu.d t6, t5, dyn<br> [0x80000758]:csrrs t2, fcsr, zero<br> [0x8000075c]:sd t6, 528(ra)<br> [0x80000760]:sd t2, 536(ra)<br> |
|  35|[0x80002538]<br>0x8000000000001000<br> [0x80002540]<br>0x0000000000000080<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                          |[0x80000770]:fcvt.lu.d t6, t5, dyn<br> [0x80000774]:csrrs t2, fcsr, zero<br> [0x80000778]:sd t6, 544(ra)<br> [0x8000077c]:sd t2, 552(ra)<br> |
|  36|[0x80002548]<br>0x8000000000001800<br> [0x80002550]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                           |[0x8000078c]:fcvt.lu.d t6, t5, dyn<br> [0x80000790]:csrrs t2, fcsr, zero<br> [0x80000794]:sd t6, 560(ra)<br> [0x80000798]:sd t2, 568(ra)<br> |
|  37|[0x80002558]<br>0x8000000000001800<br> [0x80002560]<br>0x0000000000000020<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                          |[0x800007a8]:fcvt.lu.d t6, t5, dyn<br> [0x800007ac]:csrrs t2, fcsr, zero<br> [0x800007b0]:sd t6, 576(ra)<br> [0x800007b4]:sd t2, 584(ra)<br> |
|  38|[0x80002568]<br>0x8000000000001800<br> [0x80002570]<br>0x0000000000000040<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                          |[0x800007c4]:fcvt.lu.d t6, t5, dyn<br> [0x800007c8]:csrrs t2, fcsr, zero<br> [0x800007cc]:sd t6, 592(ra)<br> [0x800007d0]:sd t2, 600(ra)<br> |
|  39|[0x80002578]<br>0x8000000000001800<br> [0x80002580]<br>0x0000000000000060<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                          |[0x800007e0]:fcvt.lu.d t6, t5, dyn<br> [0x800007e4]:csrrs t2, fcsr, zero<br> [0x800007e8]:sd t6, 608(ra)<br> [0x800007ec]:sd t2, 616(ra)<br> |
|  40|[0x80002588]<br>0x8000000000001800<br> [0x80002590]<br>0x0000000000000080<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                          |[0x800007fc]:fcvt.lu.d t6, t5, dyn<br> [0x80000800]:csrrs t2, fcsr, zero<br> [0x80000804]:sd t6, 624(ra)<br> [0x80000808]:sd t2, 632(ra)<br> |
|  41|[0x80002598]<br>0x8000000000002000<br> [0x800025a0]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                           |[0x80000818]:fcvt.lu.d t6, t5, dyn<br> [0x8000081c]:csrrs t2, fcsr, zero<br> [0x80000820]:sd t6, 640(ra)<br> [0x80000824]:sd t2, 648(ra)<br> |
|  42|[0x800025a8]<br>0x8000000000002000<br> [0x800025b0]<br>0x0000000000000020<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                          |[0x80000834]:fcvt.lu.d t6, t5, dyn<br> [0x80000838]:csrrs t2, fcsr, zero<br> [0x8000083c]:sd t6, 656(ra)<br> [0x80000840]:sd t2, 664(ra)<br> |
|  43|[0x800025b8]<br>0x8000000000002000<br> [0x800025c0]<br>0x0000000000000040<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                          |[0x80000850]:fcvt.lu.d t6, t5, dyn<br> [0x80000854]:csrrs t2, fcsr, zero<br> [0x80000858]:sd t6, 672(ra)<br> [0x8000085c]:sd t2, 680(ra)<br> |
|  44|[0x800025c8]<br>0x8000000000002000<br> [0x800025d0]<br>0x0000000000000060<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                          |[0x8000086c]:fcvt.lu.d t6, t5, dyn<br> [0x80000870]:csrrs t2, fcsr, zero<br> [0x80000874]:sd t6, 688(ra)<br> [0x80000878]:sd t2, 696(ra)<br> |
|  45|[0x800025d8]<br>0x8000000000002000<br> [0x800025e0]<br>0x0000000000000080<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                          |[0x80000888]:fcvt.lu.d t6, t5, dyn<br> [0x8000088c]:csrrs t2, fcsr, zero<br> [0x80000890]:sd t6, 704(ra)<br> [0x80000894]:sd t2, 712(ra)<br> |
