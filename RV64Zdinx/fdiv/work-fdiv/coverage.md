
|Covergroup|Coverage|
|:--------:|:------:|
|fdiv.d_b1|627/627 (100.00%)|
|fdiv.d_b2|327/327 (100.00%)|
|fdiv.d_b20|298/298 (100.00%)|
|fdiv.d_b21|727/727 (100.00%)|
|fdiv.d_b3|1171/1171 (100.00%)|
|fdiv.d_b4|191/191 (100.00%)|
|fdiv.d_b5|261/261 (100.00%)|
|fdiv.d_b6|191/191 (100.00%)|
|fdiv.d_b7|387/387 (100.00%)|
|fdiv.d_b8|1521/1521 (100.00%)|
|fdiv.d_b9|2367/2367 (100.00%)|
