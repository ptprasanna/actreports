
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x800008f0')]      |
| SIG_REGION                | [('0x80002310', '0x800024a0', '50 dwords')]      |
| COV_LABELS                | fsqrt.d_b2      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV64Zdinx/work-fsqrt1/fsqrt.d_b2-01.S/ref.S    |
| Total Number of coverpoints| 81     |
| Total Coverpoints Hit     | 81      |
| Total Signature Updates   | 50      |
| STAT1                     | 25      |
| STAT2                     | 0      |
| STAT3                     | 23     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000674]:fsqrt.d t6, t5, dyn
[0x80000678]:csrrs t2, fcsr, zero
[0x8000067c]:sd t6, 400(ra)
[0x80000680]:sd t2, 408(ra)
[0x80000684]:ld t5, 208(t0)
[0x80000688]:addi gp, zero, 0
[0x8000068c]:csrrw zero, fcsr, gp
[0x80000690]:fsqrt.d t6, t5, dyn
[0x80000694]:csrrs t2, fcsr, zero

[0x80000690]:fsqrt.d t6, t5, dyn
[0x80000694]:csrrs t2, fcsr, zero
[0x80000698]:sd t6, 416(ra)
[0x8000069c]:sd t2, 424(ra)
[0x800006a0]:ld t5, 216(t0)
[0x800006a4]:addi gp, zero, 0
[0x800006a8]:csrrw zero, fcsr, gp
[0x800006ac]:fsqrt.d t6, t5, dyn
[0x800006b0]:csrrs t2, fcsr, zero

[0x800006ac]:fsqrt.d t6, t5, dyn
[0x800006b0]:csrrs t2, fcsr, zero
[0x800006b4]:sd t6, 432(ra)
[0x800006b8]:sd t2, 440(ra)
[0x800006bc]:ld t5, 224(t0)
[0x800006c0]:addi gp, zero, 0
[0x800006c4]:csrrw zero, fcsr, gp
[0x800006c8]:fsqrt.d t6, t5, dyn
[0x800006cc]:csrrs t2, fcsr, zero

[0x800006c8]:fsqrt.d t6, t5, dyn
[0x800006cc]:csrrs t2, fcsr, zero
[0x800006d0]:sd t6, 448(ra)
[0x800006d4]:sd t2, 456(ra)
[0x800006d8]:ld t5, 232(t0)
[0x800006dc]:addi gp, zero, 0
[0x800006e0]:csrrw zero, fcsr, gp
[0x800006e4]:fsqrt.d t6, t5, dyn
[0x800006e8]:csrrs t2, fcsr, zero

[0x800006e4]:fsqrt.d t6, t5, dyn
[0x800006e8]:csrrs t2, fcsr, zero
[0x800006ec]:sd t6, 464(ra)
[0x800006f0]:sd t2, 472(ra)
[0x800006f4]:ld t5, 240(t0)
[0x800006f8]:addi gp, zero, 0
[0x800006fc]:csrrw zero, fcsr, gp
[0x80000700]:fsqrt.d t6, t5, dyn
[0x80000704]:csrrs t2, fcsr, zero

[0x80000700]:fsqrt.d t6, t5, dyn
[0x80000704]:csrrs t2, fcsr, zero
[0x80000708]:sd t6, 480(ra)
[0x8000070c]:sd t2, 488(ra)
[0x80000710]:ld t5, 248(t0)
[0x80000714]:addi gp, zero, 0
[0x80000718]:csrrw zero, fcsr, gp
[0x8000071c]:fsqrt.d t6, t5, dyn
[0x80000720]:csrrs t2, fcsr, zero

[0x8000071c]:fsqrt.d t6, t5, dyn
[0x80000720]:csrrs t2, fcsr, zero
[0x80000724]:sd t6, 496(ra)
[0x80000728]:sd t2, 504(ra)
[0x8000072c]:ld t5, 256(t0)
[0x80000730]:addi gp, zero, 0
[0x80000734]:csrrw zero, fcsr, gp
[0x80000738]:fsqrt.d t6, t5, dyn
[0x8000073c]:csrrs t2, fcsr, zero

[0x80000738]:fsqrt.d t6, t5, dyn
[0x8000073c]:csrrs t2, fcsr, zero
[0x80000740]:sd t6, 512(ra)
[0x80000744]:sd t2, 520(ra)
[0x80000748]:ld t5, 264(t0)
[0x8000074c]:addi gp, zero, 0
[0x80000750]:csrrw zero, fcsr, gp
[0x80000754]:fsqrt.d t6, t5, dyn
[0x80000758]:csrrs t2, fcsr, zero

[0x80000754]:fsqrt.d t6, t5, dyn
[0x80000758]:csrrs t2, fcsr, zero
[0x8000075c]:sd t6, 528(ra)
[0x80000760]:sd t2, 536(ra)
[0x80000764]:ld t5, 272(t0)
[0x80000768]:addi gp, zero, 0
[0x8000076c]:csrrw zero, fcsr, gp
[0x80000770]:fsqrt.d t6, t5, dyn
[0x80000774]:csrrs t2, fcsr, zero

[0x80000770]:fsqrt.d t6, t5, dyn
[0x80000774]:csrrs t2, fcsr, zero
[0x80000778]:sd t6, 544(ra)
[0x8000077c]:sd t2, 552(ra)
[0x80000780]:ld t5, 280(t0)
[0x80000784]:addi gp, zero, 0
[0x80000788]:csrrw zero, fcsr, gp
[0x8000078c]:fsqrt.d t6, t5, dyn
[0x80000790]:csrrs t2, fcsr, zero

[0x8000078c]:fsqrt.d t6, t5, dyn
[0x80000790]:csrrs t2, fcsr, zero
[0x80000794]:sd t6, 560(ra)
[0x80000798]:sd t2, 568(ra)
[0x8000079c]:ld t5, 288(t0)
[0x800007a0]:addi gp, zero, 0
[0x800007a4]:csrrw zero, fcsr, gp
[0x800007a8]:fsqrt.d t6, t5, dyn
[0x800007ac]:csrrs t2, fcsr, zero

[0x800007a8]:fsqrt.d t6, t5, dyn
[0x800007ac]:csrrs t2, fcsr, zero
[0x800007b0]:sd t6, 576(ra)
[0x800007b4]:sd t2, 584(ra)
[0x800007b8]:ld t5, 296(t0)
[0x800007bc]:addi gp, zero, 0
[0x800007c0]:csrrw zero, fcsr, gp
[0x800007c4]:fsqrt.d t6, t5, dyn
[0x800007c8]:csrrs t2, fcsr, zero

[0x800007c4]:fsqrt.d t6, t5, dyn
[0x800007c8]:csrrs t2, fcsr, zero
[0x800007cc]:sd t6, 592(ra)
[0x800007d0]:sd t2, 600(ra)
[0x800007d4]:ld t5, 304(t0)
[0x800007d8]:addi gp, zero, 0
[0x800007dc]:csrrw zero, fcsr, gp
[0x800007e0]:fsqrt.d t6, t5, dyn
[0x800007e4]:csrrs t2, fcsr, zero

[0x800007e0]:fsqrt.d t6, t5, dyn
[0x800007e4]:csrrs t2, fcsr, zero
[0x800007e8]:sd t6, 608(ra)
[0x800007ec]:sd t2, 616(ra)
[0x800007f0]:ld t5, 312(t0)
[0x800007f4]:addi gp, zero, 0
[0x800007f8]:csrrw zero, fcsr, gp
[0x800007fc]:fsqrt.d t6, t5, dyn
[0x80000800]:csrrs t2, fcsr, zero

[0x800007fc]:fsqrt.d t6, t5, dyn
[0x80000800]:csrrs t2, fcsr, zero
[0x80000804]:sd t6, 624(ra)
[0x80000808]:sd t2, 632(ra)
[0x8000080c]:ld t5, 320(t0)
[0x80000810]:addi gp, zero, 0
[0x80000814]:csrrw zero, fcsr, gp
[0x80000818]:fsqrt.d t6, t5, dyn
[0x8000081c]:csrrs t2, fcsr, zero

[0x80000818]:fsqrt.d t6, t5, dyn
[0x8000081c]:csrrs t2, fcsr, zero
[0x80000820]:sd t6, 640(ra)
[0x80000824]:sd t2, 648(ra)
[0x80000828]:ld t5, 328(t0)
[0x8000082c]:addi gp, zero, 0
[0x80000830]:csrrw zero, fcsr, gp
[0x80000834]:fsqrt.d t6, t5, dyn
[0x80000838]:csrrs t2, fcsr, zero

[0x80000834]:fsqrt.d t6, t5, dyn
[0x80000838]:csrrs t2, fcsr, zero
[0x8000083c]:sd t6, 656(ra)
[0x80000840]:sd t2, 664(ra)
[0x80000844]:ld t5, 336(t0)
[0x80000848]:addi gp, zero, 0
[0x8000084c]:csrrw zero, fcsr, gp
[0x80000850]:fsqrt.d t6, t5, dyn
[0x80000854]:csrrs t2, fcsr, zero

[0x80000850]:fsqrt.d t6, t5, dyn
[0x80000854]:csrrs t2, fcsr, zero
[0x80000858]:sd t6, 672(ra)
[0x8000085c]:sd t2, 680(ra)
[0x80000860]:ld t5, 344(t0)
[0x80000864]:addi gp, zero, 0
[0x80000868]:csrrw zero, fcsr, gp
[0x8000086c]:fsqrt.d t6, t5, dyn
[0x80000870]:csrrs t2, fcsr, zero

[0x8000086c]:fsqrt.d t6, t5, dyn
[0x80000870]:csrrs t2, fcsr, zero
[0x80000874]:sd t6, 688(ra)
[0x80000878]:sd t2, 696(ra)
[0x8000087c]:ld t5, 352(t0)
[0x80000880]:addi gp, zero, 0
[0x80000884]:csrrw zero, fcsr, gp
[0x80000888]:fsqrt.d t6, t5, dyn
[0x8000088c]:csrrs t2, fcsr, zero

[0x80000888]:fsqrt.d t6, t5, dyn
[0x8000088c]:csrrs t2, fcsr, zero
[0x80000890]:sd t6, 704(ra)
[0x80000894]:sd t2, 712(ra)
[0x80000898]:ld t5, 360(t0)
[0x8000089c]:addi gp, zero, 0
[0x800008a0]:csrrw zero, fcsr, gp
[0x800008a4]:fsqrt.d t6, t5, dyn
[0x800008a8]:csrrs t2, fcsr, zero

[0x800008a4]:fsqrt.d t6, t5, dyn
[0x800008a8]:csrrs t2, fcsr, zero
[0x800008ac]:sd t6, 720(ra)
[0x800008b0]:sd t2, 728(ra)
[0x800008b4]:ld t5, 368(t0)
[0x800008b8]:addi gp, zero, 0
[0x800008bc]:csrrw zero, fcsr, gp
[0x800008c0]:fsqrt.d t6, t5, dyn
[0x800008c4]:csrrs t2, fcsr, zero

[0x800008c0]:fsqrt.d t6, t5, dyn
[0x800008c4]:csrrs t2, fcsr, zero
[0x800008c8]:sd t6, 736(ra)
[0x800008cc]:sd t2, 744(ra)
[0x800008d0]:ld t5, 376(t0)
[0x800008d4]:addi gp, zero, 0
[0x800008d8]:csrrw zero, fcsr, gp
[0x800008dc]:fsqrt.d t6, t5, dyn
[0x800008e0]:csrrs t2, fcsr, zero

[0x800008dc]:fsqrt.d t6, t5, dyn
[0x800008e0]:csrrs t2, fcsr, zero
[0x800008e4]:sd t6, 752(ra)
[0x800008e8]:sd t2, 760(ra)
[0x800008ec]:addi zero, zero, 0



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                                  signature                                   |                                                                                 coverpoints                                                                                 |                                                                    code                                                                    |
|---:|------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002318]<br>0x0000000000000000<br> [0x80002320]<br>0x0000000000000000<br> |- mnemonic : fsqrt.d<br> - rs1 : x28<br> - rd : x30<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x800003b8]:fsqrt.d t5, t3, dyn<br> [0x800003bc]:csrrs t2, fcsr, zero<br> [0x800003c0]:sd t5, 0(ra)<br> [0x800003c4]:sd t2, 8(ra)<br>      |
|   2|[0x80002328]<br>0x3FF0000000000001<br> [0x80002330]<br>0x0000000000000001<br> |- rs1 : x26<br> - rd : x26<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                          |[0x800003d4]:fsqrt.d s10, s10, dyn<br> [0x800003d8]:csrrs t2, fcsr, zero<br> [0x800003dc]:sd s10, 16(ra)<br> [0x800003e0]:sd t2, 24(ra)<br> |
|   3|[0x80002338]<br>0x3FF0000000000002<br> [0x80002340]<br>0x0000000000000001<br> |- rs1 : x30<br> - rd : x28<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000004 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800003f0]:fsqrt.d t3, t5, dyn<br> [0x800003f4]:csrrs t2, fcsr, zero<br> [0x800003f8]:sd t3, 32(ra)<br> [0x800003fc]:sd t2, 40(ra)<br>    |
|   4|[0x80002348]<br>0x3FF0000000000004<br> [0x80002350]<br>0x0000000000000001<br> |- rs1 : x22<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000008 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x8000040c]:fsqrt.d s8, s6, dyn<br> [0x80000410]:csrrs t2, fcsr, zero<br> [0x80000414]:sd s8, 48(ra)<br> [0x80000418]:sd t2, 56(ra)<br>    |
|   5|[0x80002358]<br>0x3FF0000000000008<br> [0x80002360]<br>0x0000000000000001<br> |- rs1 : x24<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000010 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000428]:fsqrt.d s6, s8, dyn<br> [0x8000042c]:csrrs t2, fcsr, zero<br> [0x80000430]:sd s6, 64(ra)<br> [0x80000434]:sd t2, 72(ra)<br>    |
|   6|[0x80002368]<br>0x3FF0000000000010<br> [0x80002370]<br>0x0000000000000001<br> |- rs1 : x18<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000020 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000444]:fsqrt.d s4, s2, dyn<br> [0x80000448]:csrrs t2, fcsr, zero<br> [0x8000044c]:sd s4, 80(ra)<br> [0x80000450]:sd t2, 88(ra)<br>    |
|   7|[0x80002378]<br>0x3FF0000000000020<br> [0x80002380]<br>0x0000000000000001<br> |- rs1 : x20<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000040 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000460]:fsqrt.d s2, s4, dyn<br> [0x80000464]:csrrs t2, fcsr, zero<br> [0x80000468]:sd s2, 96(ra)<br> [0x8000046c]:sd t2, 104(ra)<br>   |
|   8|[0x80002388]<br>0x3FF0000000000040<br> [0x80002390]<br>0x0000000000000001<br> |- rs1 : x14<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000080 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x8000047c]:fsqrt.d a6, a4, dyn<br> [0x80000480]:csrrs t2, fcsr, zero<br> [0x80000484]:sd a6, 112(ra)<br> [0x80000488]:sd t2, 120(ra)<br>  |
|   9|[0x80002398]<br>0x3FF0000000000080<br> [0x800023a0]<br>0x0000000000000001<br> |- rs1 : x16<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000100 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000498]:fsqrt.d a4, a6, dyn<br> [0x8000049c]:csrrs t2, fcsr, zero<br> [0x800004a0]:sd a4, 128(ra)<br> [0x800004a4]:sd t2, 136(ra)<br>  |
|  10|[0x800023a8]<br>0x3FF0000000000100<br> [0x800023b0]<br>0x0000000000000001<br> |- rs1 : x10<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000200 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800004b4]:fsqrt.d a2, a0, dyn<br> [0x800004b8]:csrrs t2, fcsr, zero<br> [0x800004bc]:sd a2, 144(ra)<br> [0x800004c0]:sd t2, 152(ra)<br>  |
|  11|[0x800023b8]<br>0x3FF0000000000200<br> [0x800023c0]<br>0x0000000000000001<br> |- rs1 : x12<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000400 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800004d0]:fsqrt.d a0, a2, dyn<br> [0x800004d4]:csrrs t2, fcsr, zero<br> [0x800004d8]:sd a0, 160(ra)<br> [0x800004dc]:sd t2, 168(ra)<br>  |
|  12|[0x800023c8]<br>0x3FF0000000000400<br> [0x800023d0]<br>0x0000000000000001<br> |- rs1 : x6<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000800 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x800004ec]:fsqrt.d fp, t1, dyn<br> [0x800004f0]:csrrs t2, fcsr, zero<br> [0x800004f4]:sd fp, 176(ra)<br> [0x800004f8]:sd t2, 184(ra)<br>  |
|  13|[0x800023d8]<br>0x3FF0000000000800<br> [0x800023e0]<br>0x0000000000000001<br> |- rs1 : x8<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000001000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x80000508]:fsqrt.d t1, fp, dyn<br> [0x8000050c]:csrrs t2, fcsr, zero<br> [0x80000510]:sd t1, 192(ra)<br> [0x80000514]:sd t2, 200(ra)<br>  |
|  14|[0x800023e8]<br>0x3FF0000000001000<br> [0x800023f0]<br>0x0000000000000001<br> |- rs1 : x2<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000002000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x80000524]:fsqrt.d tp, sp, dyn<br> [0x80000528]:csrrs t2, fcsr, zero<br> [0x8000052c]:sd tp, 208(ra)<br> [0x80000530]:sd t2, 216(ra)<br>  |
|  15|[0x800023f8]<br>0x3FF0000000002000<br> [0x80002400]<br>0x0000000000000001<br> |- rs1 : x4<br> - rd : x2<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000004000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x80000540]:fsqrt.d sp, tp, dyn<br> [0x80000544]:csrrs t2, fcsr, zero<br> [0x80000548]:sd sp, 224(ra)<br> [0x8000054c]:sd t2, 232(ra)<br>  |
|  16|[0x80002408]<br>0x3FF0000000004000<br> [0x80002410]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000008000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000055c]:fsqrt.d t6, t5, dyn<br> [0x80000560]:csrrs t2, fcsr, zero<br> [0x80000564]:sd t6, 240(ra)<br> [0x80000568]:sd t2, 248(ra)<br>  |
|  17|[0x80002418]<br>0x3FF0000000008000<br> [0x80002420]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000010000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000578]:fsqrt.d t6, t5, dyn<br> [0x8000057c]:csrrs t2, fcsr, zero<br> [0x80000580]:sd t6, 256(ra)<br> [0x80000584]:sd t2, 264(ra)<br>  |
|  18|[0x80002428]<br>0x3FF0000000010000<br> [0x80002430]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000020000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000594]:fsqrt.d t6, t5, dyn<br> [0x80000598]:csrrs t2, fcsr, zero<br> [0x8000059c]:sd t6, 272(ra)<br> [0x800005a0]:sd t2, 280(ra)<br>  |
|  19|[0x80002438]<br>0x3FF0000000020000<br> [0x80002440]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000040000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800005b0]:fsqrt.d t6, t5, dyn<br> [0x800005b4]:csrrs t2, fcsr, zero<br> [0x800005b8]:sd t6, 288(ra)<br> [0x800005bc]:sd t2, 296(ra)<br>  |
|  20|[0x80002448]<br>0x3FF0000000040000<br> [0x80002450]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000080000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800005cc]:fsqrt.d t6, t5, dyn<br> [0x800005d0]:csrrs t2, fcsr, zero<br> [0x800005d4]:sd t6, 304(ra)<br> [0x800005d8]:sd t2, 312(ra)<br>  |
|  21|[0x80002458]<br>0x3FF0000000080000<br> [0x80002460]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000100000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800005e8]:fsqrt.d t6, t5, dyn<br> [0x800005ec]:csrrs t2, fcsr, zero<br> [0x800005f0]:sd t6, 320(ra)<br> [0x800005f4]:sd t2, 328(ra)<br>  |
|  22|[0x80002468]<br>0x3FF0000000100000<br> [0x80002470]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000200000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000604]:fsqrt.d t6, t5, dyn<br> [0x80000608]:csrrs t2, fcsr, zero<br> [0x8000060c]:sd t6, 336(ra)<br> [0x80000610]:sd t2, 344(ra)<br>  |
|  23|[0x80002478]<br>0x3FF0000000200000<br> [0x80002480]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000400000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000620]:fsqrt.d t6, t5, dyn<br> [0x80000624]:csrrs t2, fcsr, zero<br> [0x80000628]:sd t6, 352(ra)<br> [0x8000062c]:sd t2, 360(ra)<br>  |
|  24|[0x80002488]<br>0x3FF0000000400000<br> [0x80002490]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000800000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000063c]:fsqrt.d t6, t5, dyn<br> [0x80000640]:csrrs t2, fcsr, zero<br> [0x80000644]:sd t6, 368(ra)<br> [0x80000648]:sd t2, 376(ra)<br>  |
|  25|[0x80002498]<br>0x3F80000000000001<br> [0x800024a0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000658]:fsqrt.d t6, t5, dyn<br> [0x8000065c]:csrrs t2, fcsr, zero<br> [0x80000660]:sd t6, 384(ra)<br> [0x80000664]:sd t2, 392(ra)<br>  |
