
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80003590')]      |
| SIG_REGION                | [('0x80005e10', '0x80006a20', '386 dwords')]      |
| COV_LABELS                | fsqrt.d_b9      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV64Zdinx/work-fsqrt1/fsqrt.d_b9-01.S/ref.S    |
| Total Number of coverpoints| 416     |
| Total Coverpoints Hit     | 416      |
| Total Signature Updates   | 516      |
| STAT1                     | 258      |
| STAT2                     | 0      |
| STAT3                     | 125     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x800018d8]:fsqrt.d t6, t5, dyn
[0x800018dc]:csrrs t2, fcsr, zero
[0x800018e0]:sd t6, 1048(ra)
[0x800018e4]:sd t2, 1056(ra)
[0x800018e8]:ld t5, 1552(t0)
[0x800018ec]:addi gp, zero, 0
[0x800018f0]:csrrw zero, fcsr, gp
[0x800018f4]:fsqrt.d t6, t5, dyn
[0x800018f8]:csrrs t2, fcsr, zero

[0x800018f4]:fsqrt.d t6, t5, dyn
[0x800018f8]:csrrs t2, fcsr, zero
[0x800018fc]:sd t6, 1064(ra)
[0x80001900]:sd t2, 1072(ra)
[0x80001904]:ld t5, 1560(t0)
[0x80001908]:addi gp, zero, 0
[0x8000190c]:csrrw zero, fcsr, gp
[0x80001910]:fsqrt.d t6, t5, dyn
[0x80001914]:csrrs t2, fcsr, zero

[0x80001910]:fsqrt.d t6, t5, dyn
[0x80001914]:csrrs t2, fcsr, zero
[0x80001918]:sd t6, 1080(ra)
[0x8000191c]:sd t2, 1088(ra)
[0x80001920]:ld t5, 1568(t0)
[0x80001924]:addi gp, zero, 0
[0x80001928]:csrrw zero, fcsr, gp
[0x8000192c]:fsqrt.d t6, t5, dyn
[0x80001930]:csrrs t2, fcsr, zero

[0x8000192c]:fsqrt.d t6, t5, dyn
[0x80001930]:csrrs t2, fcsr, zero
[0x80001934]:sd t6, 1096(ra)
[0x80001938]:sd t2, 1104(ra)
[0x8000193c]:ld t5, 1576(t0)
[0x80001940]:addi gp, zero, 0
[0x80001944]:csrrw zero, fcsr, gp
[0x80001948]:fsqrt.d t6, t5, dyn
[0x8000194c]:csrrs t2, fcsr, zero

[0x80001948]:fsqrt.d t6, t5, dyn
[0x8000194c]:csrrs t2, fcsr, zero
[0x80001950]:sd t6, 1112(ra)
[0x80001954]:sd t2, 1120(ra)
[0x80001958]:ld t5, 1584(t0)
[0x8000195c]:addi gp, zero, 0
[0x80001960]:csrrw zero, fcsr, gp
[0x80001964]:fsqrt.d t6, t5, dyn
[0x80001968]:csrrs t2, fcsr, zero

[0x80001964]:fsqrt.d t6, t5, dyn
[0x80001968]:csrrs t2, fcsr, zero
[0x8000196c]:sd t6, 1128(ra)
[0x80001970]:sd t2, 1136(ra)
[0x80001974]:ld t5, 1592(t0)
[0x80001978]:addi gp, zero, 0
[0x8000197c]:csrrw zero, fcsr, gp
[0x80001980]:fsqrt.d t6, t5, dyn
[0x80001984]:csrrs t2, fcsr, zero

[0x80001980]:fsqrt.d t6, t5, dyn
[0x80001984]:csrrs t2, fcsr, zero
[0x80001988]:sd t6, 1144(ra)
[0x8000198c]:sd t2, 1152(ra)
[0x80001990]:ld t5, 1600(t0)
[0x80001994]:addi gp, zero, 0
[0x80001998]:csrrw zero, fcsr, gp
[0x8000199c]:fsqrt.d t6, t5, dyn
[0x800019a0]:csrrs t2, fcsr, zero

[0x8000199c]:fsqrt.d t6, t5, dyn
[0x800019a0]:csrrs t2, fcsr, zero
[0x800019a4]:sd t6, 1160(ra)
[0x800019a8]:sd t2, 1168(ra)
[0x800019ac]:ld t5, 1608(t0)
[0x800019b0]:addi gp, zero, 0
[0x800019b4]:csrrw zero, fcsr, gp
[0x800019b8]:fsqrt.d t6, t5, dyn
[0x800019bc]:csrrs t2, fcsr, zero

[0x800019b8]:fsqrt.d t6, t5, dyn
[0x800019bc]:csrrs t2, fcsr, zero
[0x800019c0]:sd t6, 1176(ra)
[0x800019c4]:sd t2, 1184(ra)
[0x800019c8]:ld t5, 1616(t0)
[0x800019cc]:addi gp, zero, 0
[0x800019d0]:csrrw zero, fcsr, gp
[0x800019d4]:fsqrt.d t6, t5, dyn
[0x800019d8]:csrrs t2, fcsr, zero

[0x800019d4]:fsqrt.d t6, t5, dyn
[0x800019d8]:csrrs t2, fcsr, zero
[0x800019dc]:sd t6, 1192(ra)
[0x800019e0]:sd t2, 1200(ra)
[0x800019e4]:ld t5, 1624(t0)
[0x800019e8]:addi gp, zero, 0
[0x800019ec]:csrrw zero, fcsr, gp
[0x800019f0]:fsqrt.d t6, t5, dyn
[0x800019f4]:csrrs t2, fcsr, zero

[0x800019f0]:fsqrt.d t6, t5, dyn
[0x800019f4]:csrrs t2, fcsr, zero
[0x800019f8]:sd t6, 1208(ra)
[0x800019fc]:sd t2, 1216(ra)
[0x80001a00]:ld t5, 1632(t0)
[0x80001a04]:addi gp, zero, 0
[0x80001a08]:csrrw zero, fcsr, gp
[0x80001a0c]:fsqrt.d t6, t5, dyn
[0x80001a10]:csrrs t2, fcsr, zero

[0x80001a0c]:fsqrt.d t6, t5, dyn
[0x80001a10]:csrrs t2, fcsr, zero
[0x80001a14]:sd t6, 1224(ra)
[0x80001a18]:sd t2, 1232(ra)
[0x80001a1c]:ld t5, 1640(t0)
[0x80001a20]:addi gp, zero, 0
[0x80001a24]:csrrw zero, fcsr, gp
[0x80001a28]:fsqrt.d t6, t5, dyn
[0x80001a2c]:csrrs t2, fcsr, zero

[0x80001a28]:fsqrt.d t6, t5, dyn
[0x80001a2c]:csrrs t2, fcsr, zero
[0x80001a30]:sd t6, 1240(ra)
[0x80001a34]:sd t2, 1248(ra)
[0x80001a38]:ld t5, 1648(t0)
[0x80001a3c]:addi gp, zero, 0
[0x80001a40]:csrrw zero, fcsr, gp
[0x80001a44]:fsqrt.d t6, t5, dyn
[0x80001a48]:csrrs t2, fcsr, zero

[0x80001a44]:fsqrt.d t6, t5, dyn
[0x80001a48]:csrrs t2, fcsr, zero
[0x80001a4c]:sd t6, 1256(ra)
[0x80001a50]:sd t2, 1264(ra)
[0x80001a54]:ld t5, 1656(t0)
[0x80001a58]:addi gp, zero, 0
[0x80001a5c]:csrrw zero, fcsr, gp
[0x80001a60]:fsqrt.d t6, t5, dyn
[0x80001a64]:csrrs t2, fcsr, zero

[0x80001a60]:fsqrt.d t6, t5, dyn
[0x80001a64]:csrrs t2, fcsr, zero
[0x80001a68]:sd t6, 1272(ra)
[0x80001a6c]:sd t2, 1280(ra)
[0x80001a70]:ld t5, 1664(t0)
[0x80001a74]:addi gp, zero, 0
[0x80001a78]:csrrw zero, fcsr, gp
[0x80001a7c]:fsqrt.d t6, t5, dyn
[0x80001a80]:csrrs t2, fcsr, zero

[0x80001a7c]:fsqrt.d t6, t5, dyn
[0x80001a80]:csrrs t2, fcsr, zero
[0x80001a84]:sd t6, 1288(ra)
[0x80001a88]:sd t2, 1296(ra)
[0x80001a8c]:ld t5, 1672(t0)
[0x80001a90]:addi gp, zero, 0
[0x80001a94]:csrrw zero, fcsr, gp
[0x80001a98]:fsqrt.d t6, t5, dyn
[0x80001a9c]:csrrs t2, fcsr, zero

[0x80001a98]:fsqrt.d t6, t5, dyn
[0x80001a9c]:csrrs t2, fcsr, zero
[0x80001aa0]:sd t6, 1304(ra)
[0x80001aa4]:sd t2, 1312(ra)
[0x80001aa8]:ld t5, 1680(t0)
[0x80001aac]:addi gp, zero, 0
[0x80001ab0]:csrrw zero, fcsr, gp
[0x80001ab4]:fsqrt.d t6, t5, dyn
[0x80001ab8]:csrrs t2, fcsr, zero

[0x80001ab4]:fsqrt.d t6, t5, dyn
[0x80001ab8]:csrrs t2, fcsr, zero
[0x80001abc]:sd t6, 1320(ra)
[0x80001ac0]:sd t2, 1328(ra)
[0x80001ac4]:ld t5, 1688(t0)
[0x80001ac8]:addi gp, zero, 0
[0x80001acc]:csrrw zero, fcsr, gp
[0x80001ad0]:fsqrt.d t6, t5, dyn
[0x80001ad4]:csrrs t2, fcsr, zero

[0x80001ad0]:fsqrt.d t6, t5, dyn
[0x80001ad4]:csrrs t2, fcsr, zero
[0x80001ad8]:sd t6, 1336(ra)
[0x80001adc]:sd t2, 1344(ra)
[0x80001ae0]:ld t5, 1696(t0)
[0x80001ae4]:addi gp, zero, 0
[0x80001ae8]:csrrw zero, fcsr, gp
[0x80001aec]:fsqrt.d t6, t5, dyn
[0x80001af0]:csrrs t2, fcsr, zero

[0x80001aec]:fsqrt.d t6, t5, dyn
[0x80001af0]:csrrs t2, fcsr, zero
[0x80001af4]:sd t6, 1352(ra)
[0x80001af8]:sd t2, 1360(ra)
[0x80001afc]:ld t5, 1704(t0)
[0x80001b00]:addi gp, zero, 0
[0x80001b04]:csrrw zero, fcsr, gp
[0x80001b08]:fsqrt.d t6, t5, dyn
[0x80001b0c]:csrrs t2, fcsr, zero

[0x80001b08]:fsqrt.d t6, t5, dyn
[0x80001b0c]:csrrs t2, fcsr, zero
[0x80001b10]:sd t6, 1368(ra)
[0x80001b14]:sd t2, 1376(ra)
[0x80001b18]:ld t5, 1712(t0)
[0x80001b1c]:addi gp, zero, 0
[0x80001b20]:csrrw zero, fcsr, gp
[0x80001b24]:fsqrt.d t6, t5, dyn
[0x80001b28]:csrrs t2, fcsr, zero

[0x80001b24]:fsqrt.d t6, t5, dyn
[0x80001b28]:csrrs t2, fcsr, zero
[0x80001b2c]:sd t6, 1384(ra)
[0x80001b30]:sd t2, 1392(ra)
[0x80001b34]:ld t5, 1720(t0)
[0x80001b38]:addi gp, zero, 0
[0x80001b3c]:csrrw zero, fcsr, gp
[0x80001b40]:fsqrt.d t6, t5, dyn
[0x80001b44]:csrrs t2, fcsr, zero

[0x80001b40]:fsqrt.d t6, t5, dyn
[0x80001b44]:csrrs t2, fcsr, zero
[0x80001b48]:sd t6, 1400(ra)
[0x80001b4c]:sd t2, 1408(ra)
[0x80001b50]:ld t5, 1728(t0)
[0x80001b54]:addi gp, zero, 0
[0x80001b58]:csrrw zero, fcsr, gp
[0x80001b5c]:fsqrt.d t6, t5, dyn
[0x80001b60]:csrrs t2, fcsr, zero

[0x80001b5c]:fsqrt.d t6, t5, dyn
[0x80001b60]:csrrs t2, fcsr, zero
[0x80001b64]:sd t6, 1416(ra)
[0x80001b68]:sd t2, 1424(ra)
[0x80001b6c]:ld t5, 1736(t0)
[0x80001b70]:addi gp, zero, 0
[0x80001b74]:csrrw zero, fcsr, gp
[0x80001b78]:fsqrt.d t6, t5, dyn
[0x80001b7c]:csrrs t2, fcsr, zero

[0x80001b78]:fsqrt.d t6, t5, dyn
[0x80001b7c]:csrrs t2, fcsr, zero
[0x80001b80]:sd t6, 1432(ra)
[0x80001b84]:sd t2, 1440(ra)
[0x80001b88]:ld t5, 1744(t0)
[0x80001b8c]:addi gp, zero, 0
[0x80001b90]:csrrw zero, fcsr, gp
[0x80001b94]:fsqrt.d t6, t5, dyn
[0x80001b98]:csrrs t2, fcsr, zero

[0x80001b94]:fsqrt.d t6, t5, dyn
[0x80001b98]:csrrs t2, fcsr, zero
[0x80001b9c]:sd t6, 1448(ra)
[0x80001ba0]:sd t2, 1456(ra)
[0x80001ba4]:ld t5, 1752(t0)
[0x80001ba8]:addi gp, zero, 0
[0x80001bac]:csrrw zero, fcsr, gp
[0x80001bb0]:fsqrt.d t6, t5, dyn
[0x80001bb4]:csrrs t2, fcsr, zero

[0x80001bb0]:fsqrt.d t6, t5, dyn
[0x80001bb4]:csrrs t2, fcsr, zero
[0x80001bb8]:sd t6, 1464(ra)
[0x80001bbc]:sd t2, 1472(ra)
[0x80001bc0]:ld t5, 1760(t0)
[0x80001bc4]:addi gp, zero, 0
[0x80001bc8]:csrrw zero, fcsr, gp
[0x80001bcc]:fsqrt.d t6, t5, dyn
[0x80001bd0]:csrrs t2, fcsr, zero

[0x80001bcc]:fsqrt.d t6, t5, dyn
[0x80001bd0]:csrrs t2, fcsr, zero
[0x80001bd4]:sd t6, 1480(ra)
[0x80001bd8]:sd t2, 1488(ra)
[0x80001bdc]:ld t5, 1768(t0)
[0x80001be0]:addi gp, zero, 0
[0x80001be4]:csrrw zero, fcsr, gp
[0x80001be8]:fsqrt.d t6, t5, dyn
[0x80001bec]:csrrs t2, fcsr, zero

[0x80001be8]:fsqrt.d t6, t5, dyn
[0x80001bec]:csrrs t2, fcsr, zero
[0x80001bf0]:sd t6, 1496(ra)
[0x80001bf4]:sd t2, 1504(ra)
[0x80001bf8]:ld t5, 1776(t0)
[0x80001bfc]:addi gp, zero, 0
[0x80001c00]:csrrw zero, fcsr, gp
[0x80001c04]:fsqrt.d t6, t5, dyn
[0x80001c08]:csrrs t2, fcsr, zero

[0x80001c04]:fsqrt.d t6, t5, dyn
[0x80001c08]:csrrs t2, fcsr, zero
[0x80001c0c]:sd t6, 1512(ra)
[0x80001c10]:sd t2, 1520(ra)
[0x80001c14]:ld t5, 1784(t0)
[0x80001c18]:addi gp, zero, 0
[0x80001c1c]:csrrw zero, fcsr, gp
[0x80001c20]:fsqrt.d t6, t5, dyn
[0x80001c24]:csrrs t2, fcsr, zero

[0x80001c20]:fsqrt.d t6, t5, dyn
[0x80001c24]:csrrs t2, fcsr, zero
[0x80001c28]:sd t6, 1528(ra)
[0x80001c2c]:sd t2, 1536(ra)
[0x80001c30]:ld t5, 1792(t0)
[0x80001c34]:addi gp, zero, 0
[0x80001c38]:csrrw zero, fcsr, gp
[0x80001c3c]:fsqrt.d t6, t5, dyn
[0x80001c40]:csrrs t2, fcsr, zero

[0x80001c3c]:fsqrt.d t6, t5, dyn
[0x80001c40]:csrrs t2, fcsr, zero
[0x80001c44]:sd t6, 1544(ra)
[0x80001c48]:sd t2, 1552(ra)
[0x80001c4c]:ld t5, 1800(t0)
[0x80001c50]:addi gp, zero, 0
[0x80001c54]:csrrw zero, fcsr, gp
[0x80001c58]:fsqrt.d t6, t5, dyn
[0x80001c5c]:csrrs t2, fcsr, zero

[0x80001c58]:fsqrt.d t6, t5, dyn
[0x80001c5c]:csrrs t2, fcsr, zero
[0x80001c60]:sd t6, 1560(ra)
[0x80001c64]:sd t2, 1568(ra)
[0x80001c68]:ld t5, 1808(t0)
[0x80001c6c]:addi gp, zero, 0
[0x80001c70]:csrrw zero, fcsr, gp
[0x80001c74]:fsqrt.d t6, t5, dyn
[0x80001c78]:csrrs t2, fcsr, zero

[0x80001c74]:fsqrt.d t6, t5, dyn
[0x80001c78]:csrrs t2, fcsr, zero
[0x80001c7c]:sd t6, 1576(ra)
[0x80001c80]:sd t2, 1584(ra)
[0x80001c84]:ld t5, 1816(t0)
[0x80001c88]:addi gp, zero, 0
[0x80001c8c]:csrrw zero, fcsr, gp
[0x80001c90]:fsqrt.d t6, t5, dyn
[0x80001c94]:csrrs t2, fcsr, zero

[0x80001c90]:fsqrt.d t6, t5, dyn
[0x80001c94]:csrrs t2, fcsr, zero
[0x80001c98]:sd t6, 1592(ra)
[0x80001c9c]:sd t2, 1600(ra)
[0x80001ca0]:ld t5, 1824(t0)
[0x80001ca4]:addi gp, zero, 0
[0x80001ca8]:csrrw zero, fcsr, gp
[0x80001cac]:fsqrt.d t6, t5, dyn
[0x80001cb0]:csrrs t2, fcsr, zero

[0x80001cac]:fsqrt.d t6, t5, dyn
[0x80001cb0]:csrrs t2, fcsr, zero
[0x80001cb4]:sd t6, 1608(ra)
[0x80001cb8]:sd t2, 1616(ra)
[0x80001cbc]:ld t5, 1832(t0)
[0x80001cc0]:addi gp, zero, 0
[0x80001cc4]:csrrw zero, fcsr, gp
[0x80001cc8]:fsqrt.d t6, t5, dyn
[0x80001ccc]:csrrs t2, fcsr, zero

[0x80001cc8]:fsqrt.d t6, t5, dyn
[0x80001ccc]:csrrs t2, fcsr, zero
[0x80001cd0]:sd t6, 1624(ra)
[0x80001cd4]:sd t2, 1632(ra)
[0x80001cd8]:ld t5, 1840(t0)
[0x80001cdc]:addi gp, zero, 0
[0x80001ce0]:csrrw zero, fcsr, gp
[0x80001ce4]:fsqrt.d t6, t5, dyn
[0x80001ce8]:csrrs t2, fcsr, zero

[0x80001ce4]:fsqrt.d t6, t5, dyn
[0x80001ce8]:csrrs t2, fcsr, zero
[0x80001cec]:sd t6, 1640(ra)
[0x80001cf0]:sd t2, 1648(ra)
[0x80001cf4]:ld t5, 1848(t0)
[0x80001cf8]:addi gp, zero, 0
[0x80001cfc]:csrrw zero, fcsr, gp
[0x80001d00]:fsqrt.d t6, t5, dyn
[0x80001d04]:csrrs t2, fcsr, zero

[0x80001d00]:fsqrt.d t6, t5, dyn
[0x80001d04]:csrrs t2, fcsr, zero
[0x80001d08]:sd t6, 1656(ra)
[0x80001d0c]:sd t2, 1664(ra)
[0x80001d10]:ld t5, 1856(t0)
[0x80001d14]:addi gp, zero, 0
[0x80001d18]:csrrw zero, fcsr, gp
[0x80001d1c]:fsqrt.d t6, t5, dyn
[0x80001d20]:csrrs t2, fcsr, zero

[0x80001d1c]:fsqrt.d t6, t5, dyn
[0x80001d20]:csrrs t2, fcsr, zero
[0x80001d24]:sd t6, 1672(ra)
[0x80001d28]:sd t2, 1680(ra)
[0x80001d2c]:ld t5, 1864(t0)
[0x80001d30]:addi gp, zero, 0
[0x80001d34]:csrrw zero, fcsr, gp
[0x80001d38]:fsqrt.d t6, t5, dyn
[0x80001d3c]:csrrs t2, fcsr, zero

[0x80001d38]:fsqrt.d t6, t5, dyn
[0x80001d3c]:csrrs t2, fcsr, zero
[0x80001d40]:sd t6, 1688(ra)
[0x80001d44]:sd t2, 1696(ra)
[0x80001d48]:ld t5, 1872(t0)
[0x80001d4c]:addi gp, zero, 0
[0x80001d50]:csrrw zero, fcsr, gp
[0x80001d54]:fsqrt.d t6, t5, dyn
[0x80001d58]:csrrs t2, fcsr, zero

[0x80001d54]:fsqrt.d t6, t5, dyn
[0x80001d58]:csrrs t2, fcsr, zero
[0x80001d5c]:sd t6, 1704(ra)
[0x80001d60]:sd t2, 1712(ra)
[0x80001d64]:ld t5, 1880(t0)
[0x80001d68]:addi gp, zero, 0
[0x80001d6c]:csrrw zero, fcsr, gp
[0x80001d70]:fsqrt.d t6, t5, dyn
[0x80001d74]:csrrs t2, fcsr, zero

[0x80001d70]:fsqrt.d t6, t5, dyn
[0x80001d74]:csrrs t2, fcsr, zero
[0x80001d78]:sd t6, 1720(ra)
[0x80001d7c]:sd t2, 1728(ra)
[0x80001d80]:ld t5, 1888(t0)
[0x80001d84]:addi gp, zero, 0
[0x80001d88]:csrrw zero, fcsr, gp
[0x80001d8c]:fsqrt.d t6, t5, dyn
[0x80001d90]:csrrs t2, fcsr, zero

[0x80001d8c]:fsqrt.d t6, t5, dyn
[0x80001d90]:csrrs t2, fcsr, zero
[0x80001d94]:sd t6, 1736(ra)
[0x80001d98]:sd t2, 1744(ra)
[0x80001d9c]:ld t5, 1896(t0)
[0x80001da0]:addi gp, zero, 0
[0x80001da4]:csrrw zero, fcsr, gp
[0x80001da8]:fsqrt.d t6, t5, dyn
[0x80001dac]:csrrs t2, fcsr, zero

[0x80001da8]:fsqrt.d t6, t5, dyn
[0x80001dac]:csrrs t2, fcsr, zero
[0x80001db0]:sd t6, 1752(ra)
[0x80001db4]:sd t2, 1760(ra)
[0x80001db8]:ld t5, 1904(t0)
[0x80001dbc]:addi gp, zero, 0
[0x80001dc0]:csrrw zero, fcsr, gp
[0x80001dc4]:fsqrt.d t6, t5, dyn
[0x80001dc8]:csrrs t2, fcsr, zero

[0x80001dc4]:fsqrt.d t6, t5, dyn
[0x80001dc8]:csrrs t2, fcsr, zero
[0x80001dcc]:sd t6, 1768(ra)
[0x80001dd0]:sd t2, 1776(ra)
[0x80001dd4]:ld t5, 1912(t0)
[0x80001dd8]:addi gp, zero, 0
[0x80001ddc]:csrrw zero, fcsr, gp
[0x80001de0]:fsqrt.d t6, t5, dyn
[0x80001de4]:csrrs t2, fcsr, zero

[0x80001de0]:fsqrt.d t6, t5, dyn
[0x80001de4]:csrrs t2, fcsr, zero
[0x80001de8]:sd t6, 1784(ra)
[0x80001dec]:sd t2, 1792(ra)
[0x80001df0]:ld t5, 1920(t0)
[0x80001df4]:addi gp, zero, 0
[0x80001df8]:csrrw zero, fcsr, gp
[0x80001dfc]:fsqrt.d t6, t5, dyn
[0x80001e00]:csrrs t2, fcsr, zero

[0x80001dfc]:fsqrt.d t6, t5, dyn
[0x80001e00]:csrrs t2, fcsr, zero
[0x80001e04]:sd t6, 1800(ra)
[0x80001e08]:sd t2, 1808(ra)
[0x80001e0c]:ld t5, 1928(t0)
[0x80001e10]:addi gp, zero, 0
[0x80001e14]:csrrw zero, fcsr, gp
[0x80001e18]:fsqrt.d t6, t5, dyn
[0x80001e1c]:csrrs t2, fcsr, zero

[0x80001e18]:fsqrt.d t6, t5, dyn
[0x80001e1c]:csrrs t2, fcsr, zero
[0x80001e20]:sd t6, 1816(ra)
[0x80001e24]:sd t2, 1824(ra)
[0x80001e28]:ld t5, 1936(t0)
[0x80001e2c]:addi gp, zero, 0
[0x80001e30]:csrrw zero, fcsr, gp
[0x80001e34]:fsqrt.d t6, t5, dyn
[0x80001e38]:csrrs t2, fcsr, zero

[0x80001e34]:fsqrt.d t6, t5, dyn
[0x80001e38]:csrrs t2, fcsr, zero
[0x80001e3c]:sd t6, 1832(ra)
[0x80001e40]:sd t2, 1840(ra)
[0x80001e44]:ld t5, 1944(t0)
[0x80001e48]:addi gp, zero, 0
[0x80001e4c]:csrrw zero, fcsr, gp
[0x80001e50]:fsqrt.d t6, t5, dyn
[0x80001e54]:csrrs t2, fcsr, zero

[0x80001e50]:fsqrt.d t6, t5, dyn
[0x80001e54]:csrrs t2, fcsr, zero
[0x80001e58]:sd t6, 1848(ra)
[0x80001e5c]:sd t2, 1856(ra)
[0x80001e60]:ld t5, 1952(t0)
[0x80001e64]:addi gp, zero, 0
[0x80001e68]:csrrw zero, fcsr, gp
[0x80001e6c]:fsqrt.d t6, t5, dyn
[0x80001e70]:csrrs t2, fcsr, zero

[0x80001e6c]:fsqrt.d t6, t5, dyn
[0x80001e70]:csrrs t2, fcsr, zero
[0x80001e74]:sd t6, 1864(ra)
[0x80001e78]:sd t2, 1872(ra)
[0x80001e7c]:ld t5, 1960(t0)
[0x80001e80]:addi gp, zero, 0
[0x80001e84]:csrrw zero, fcsr, gp
[0x80001e88]:fsqrt.d t6, t5, dyn
[0x80001e8c]:csrrs t2, fcsr, zero

[0x80001e88]:fsqrt.d t6, t5, dyn
[0x80001e8c]:csrrs t2, fcsr, zero
[0x80001e90]:sd t6, 1880(ra)
[0x80001e94]:sd t2, 1888(ra)
[0x80001e98]:ld t5, 1968(t0)
[0x80001e9c]:addi gp, zero, 0
[0x80001ea0]:csrrw zero, fcsr, gp
[0x80001ea4]:fsqrt.d t6, t5, dyn
[0x80001ea8]:csrrs t2, fcsr, zero

[0x80001ea4]:fsqrt.d t6, t5, dyn
[0x80001ea8]:csrrs t2, fcsr, zero
[0x80001eac]:sd t6, 1896(ra)
[0x80001eb0]:sd t2, 1904(ra)
[0x80001eb4]:ld t5, 1976(t0)
[0x80001eb8]:addi gp, zero, 0
[0x80001ebc]:csrrw zero, fcsr, gp
[0x80001ec0]:fsqrt.d t6, t5, dyn
[0x80001ec4]:csrrs t2, fcsr, zero

[0x80001ec0]:fsqrt.d t6, t5, dyn
[0x80001ec4]:csrrs t2, fcsr, zero
[0x80001ec8]:sd t6, 1912(ra)
[0x80001ecc]:sd t2, 1920(ra)
[0x80001ed0]:ld t5, 1984(t0)
[0x80001ed4]:addi gp, zero, 0
[0x80001ed8]:csrrw zero, fcsr, gp
[0x80001edc]:fsqrt.d t6, t5, dyn
[0x80001ee0]:csrrs t2, fcsr, zero

[0x80001edc]:fsqrt.d t6, t5, dyn
[0x80001ee0]:csrrs t2, fcsr, zero
[0x80001ee4]:sd t6, 1928(ra)
[0x80001ee8]:sd t2, 1936(ra)
[0x80001eec]:ld t5, 1992(t0)
[0x80001ef0]:addi gp, zero, 0
[0x80001ef4]:csrrw zero, fcsr, gp
[0x80001ef8]:fsqrt.d t6, t5, dyn
[0x80001efc]:csrrs t2, fcsr, zero

[0x80001ef8]:fsqrt.d t6, t5, dyn
[0x80001efc]:csrrs t2, fcsr, zero
[0x80001f00]:sd t6, 1944(ra)
[0x80001f04]:sd t2, 1952(ra)
[0x80001f08]:ld t5, 2000(t0)
[0x80001f0c]:addi gp, zero, 0
[0x80001f10]:csrrw zero, fcsr, gp
[0x80001f14]:fsqrt.d t6, t5, dyn
[0x80001f18]:csrrs t2, fcsr, zero

[0x80001f14]:fsqrt.d t6, t5, dyn
[0x80001f18]:csrrs t2, fcsr, zero
[0x80001f1c]:sd t6, 1960(ra)
[0x80001f20]:sd t2, 1968(ra)
[0x80001f24]:ld t5, 2008(t0)
[0x80001f28]:addi gp, zero, 0
[0x80001f2c]:csrrw zero, fcsr, gp
[0x80001f30]:fsqrt.d t6, t5, dyn
[0x80001f34]:csrrs t2, fcsr, zero

[0x80001f30]:fsqrt.d t6, t5, dyn
[0x80001f34]:csrrs t2, fcsr, zero
[0x80001f38]:sd t6, 1976(ra)
[0x80001f3c]:sd t2, 1984(ra)
[0x80001f40]:ld t5, 2016(t0)
[0x80001f44]:addi gp, zero, 0
[0x80001f48]:csrrw zero, fcsr, gp
[0x80001f4c]:fsqrt.d t6, t5, dyn
[0x80001f50]:csrrs t2, fcsr, zero

[0x80001f4c]:fsqrt.d t6, t5, dyn
[0x80001f50]:csrrs t2, fcsr, zero
[0x80001f54]:sd t6, 1992(ra)
[0x80001f58]:sd t2, 2000(ra)
[0x80001f5c]:ld t5, 2024(t0)
[0x80001f60]:addi gp, zero, 0
[0x80001f64]:csrrw zero, fcsr, gp
[0x80001f68]:fsqrt.d t6, t5, dyn
[0x80001f6c]:csrrs t2, fcsr, zero

[0x80001f68]:fsqrt.d t6, t5, dyn
[0x80001f6c]:csrrs t2, fcsr, zero
[0x80001f70]:sd t6, 2008(ra)
[0x80001f74]:sd t2, 2016(ra)
[0x80001f78]:ld t5, 2032(t0)
[0x80001f7c]:addi gp, zero, 0
[0x80001f80]:csrrw zero, fcsr, gp
[0x80001f84]:fsqrt.d t6, t5, dyn
[0x80001f88]:csrrs t2, fcsr, zero

[0x80001f84]:fsqrt.d t6, t5, dyn
[0x80001f88]:csrrs t2, fcsr, zero
[0x80001f8c]:sd t6, 2024(ra)
[0x80001f90]:sd t2, 2032(ra)
[0x80001f94]:ld t5, 2040(t0)
[0x80001f98]:addi gp, zero, 0
[0x80001f9c]:csrrw zero, fcsr, gp
[0x80001fa0]:fsqrt.d t6, t5, dyn
[0x80001fa4]:csrrs t2, fcsr, zero

[0x80001fa0]:fsqrt.d t6, t5, dyn
[0x80001fa4]:csrrs t2, fcsr, zero
[0x80001fa8]:sd t6, 2040(ra)
[0x80001fac]:addi ra, ra, 2040
[0x80001fb0]:sd t2, 8(ra)
[0x80001fb4]:auipc ra, 4
[0x80001fb8]:addi ra, ra, 1636
[0x80001fbc]:lui gp, 1
[0x80001fc0]:addiw gp, gp, 2048
[0x80001fc4]:add t0, t0, gp
[0x80001fc8]:ld t5, 0(t0)
[0x80001fcc]:sub t0, t0, gp
[0x80001fd0]:addi gp, zero, 0
[0x80001fd4]:csrrw zero, fcsr, gp
[0x80001fd8]:fsqrt.d t6, t5, dyn
[0x80001fdc]:csrrs t2, fcsr, zero

[0x80002b04]:fsqrt.d t6, t5, dyn
[0x80002b08]:csrrs t2, fcsr, zero
[0x80002b0c]:sd t6, 1040(ra)
[0x80002b10]:sd t2, 1048(ra)
[0x80002b14]:lui gp, 1
[0x80002b18]:addiw gp, gp, 2048
[0x80002b1c]:add t0, t0, gp
[0x80002b20]:ld t5, 528(t0)
[0x80002b24]:sub t0, t0, gp
[0x80002b28]:addi gp, zero, 0
[0x80002b2c]:csrrw zero, fcsr, gp
[0x80002b30]:fsqrt.d t6, t5, dyn
[0x80002b34]:csrrs t2, fcsr, zero

[0x80002b30]:fsqrt.d t6, t5, dyn
[0x80002b34]:csrrs t2, fcsr, zero
[0x80002b38]:sd t6, 1056(ra)
[0x80002b3c]:sd t2, 1064(ra)
[0x80002b40]:lui gp, 1
[0x80002b44]:addiw gp, gp, 2048
[0x80002b48]:add t0, t0, gp
[0x80002b4c]:ld t5, 536(t0)
[0x80002b50]:sub t0, t0, gp
[0x80002b54]:addi gp, zero, 0
[0x80002b58]:csrrw zero, fcsr, gp
[0x80002b5c]:fsqrt.d t6, t5, dyn
[0x80002b60]:csrrs t2, fcsr, zero

[0x80002b5c]:fsqrt.d t6, t5, dyn
[0x80002b60]:csrrs t2, fcsr, zero
[0x80002b64]:sd t6, 1072(ra)
[0x80002b68]:sd t2, 1080(ra)
[0x80002b6c]:lui gp, 1
[0x80002b70]:addiw gp, gp, 2048
[0x80002b74]:add t0, t0, gp
[0x80002b78]:ld t5, 544(t0)
[0x80002b7c]:sub t0, t0, gp
[0x80002b80]:addi gp, zero, 0
[0x80002b84]:csrrw zero, fcsr, gp
[0x80002b88]:fsqrt.d t6, t5, dyn
[0x80002b8c]:csrrs t2, fcsr, zero

[0x80002b88]:fsqrt.d t6, t5, dyn
[0x80002b8c]:csrrs t2, fcsr, zero
[0x80002b90]:sd t6, 1088(ra)
[0x80002b94]:sd t2, 1096(ra)
[0x80002b98]:lui gp, 1
[0x80002b9c]:addiw gp, gp, 2048
[0x80002ba0]:add t0, t0, gp
[0x80002ba4]:ld t5, 552(t0)
[0x80002ba8]:sub t0, t0, gp
[0x80002bac]:addi gp, zero, 0
[0x80002bb0]:csrrw zero, fcsr, gp
[0x80002bb4]:fsqrt.d t6, t5, dyn
[0x80002bb8]:csrrs t2, fcsr, zero

[0x80002bb4]:fsqrt.d t6, t5, dyn
[0x80002bb8]:csrrs t2, fcsr, zero
[0x80002bbc]:sd t6, 1104(ra)
[0x80002bc0]:sd t2, 1112(ra)
[0x80002bc4]:lui gp, 1
[0x80002bc8]:addiw gp, gp, 2048
[0x80002bcc]:add t0, t0, gp
[0x80002bd0]:ld t5, 560(t0)
[0x80002bd4]:sub t0, t0, gp
[0x80002bd8]:addi gp, zero, 0
[0x80002bdc]:csrrw zero, fcsr, gp
[0x80002be0]:fsqrt.d t6, t5, dyn
[0x80002be4]:csrrs t2, fcsr, zero

[0x80002be0]:fsqrt.d t6, t5, dyn
[0x80002be4]:csrrs t2, fcsr, zero
[0x80002be8]:sd t6, 1120(ra)
[0x80002bec]:sd t2, 1128(ra)
[0x80002bf0]:lui gp, 1
[0x80002bf4]:addiw gp, gp, 2048
[0x80002bf8]:add t0, t0, gp
[0x80002bfc]:ld t5, 568(t0)
[0x80002c00]:sub t0, t0, gp
[0x80002c04]:addi gp, zero, 0
[0x80002c08]:csrrw zero, fcsr, gp
[0x80002c0c]:fsqrt.d t6, t5, dyn
[0x80002c10]:csrrs t2, fcsr, zero

[0x80002c0c]:fsqrt.d t6, t5, dyn
[0x80002c10]:csrrs t2, fcsr, zero
[0x80002c14]:sd t6, 1136(ra)
[0x80002c18]:sd t2, 1144(ra)
[0x80002c1c]:lui gp, 1
[0x80002c20]:addiw gp, gp, 2048
[0x80002c24]:add t0, t0, gp
[0x80002c28]:ld t5, 576(t0)
[0x80002c2c]:sub t0, t0, gp
[0x80002c30]:addi gp, zero, 0
[0x80002c34]:csrrw zero, fcsr, gp
[0x80002c38]:fsqrt.d t6, t5, dyn
[0x80002c3c]:csrrs t2, fcsr, zero

[0x80002c38]:fsqrt.d t6, t5, dyn
[0x80002c3c]:csrrs t2, fcsr, zero
[0x80002c40]:sd t6, 1152(ra)
[0x80002c44]:sd t2, 1160(ra)
[0x80002c48]:lui gp, 1
[0x80002c4c]:addiw gp, gp, 2048
[0x80002c50]:add t0, t0, gp
[0x80002c54]:ld t5, 584(t0)
[0x80002c58]:sub t0, t0, gp
[0x80002c5c]:addi gp, zero, 0
[0x80002c60]:csrrw zero, fcsr, gp
[0x80002c64]:fsqrt.d t6, t5, dyn
[0x80002c68]:csrrs t2, fcsr, zero

[0x80002c64]:fsqrt.d t6, t5, dyn
[0x80002c68]:csrrs t2, fcsr, zero
[0x80002c6c]:sd t6, 1168(ra)
[0x80002c70]:sd t2, 1176(ra)
[0x80002c74]:lui gp, 1
[0x80002c78]:addiw gp, gp, 2048
[0x80002c7c]:add t0, t0, gp
[0x80002c80]:ld t5, 592(t0)
[0x80002c84]:sub t0, t0, gp
[0x80002c88]:addi gp, zero, 0
[0x80002c8c]:csrrw zero, fcsr, gp
[0x80002c90]:fsqrt.d t6, t5, dyn
[0x80002c94]:csrrs t2, fcsr, zero

[0x80002c90]:fsqrt.d t6, t5, dyn
[0x80002c94]:csrrs t2, fcsr, zero
[0x80002c98]:sd t6, 1184(ra)
[0x80002c9c]:sd t2, 1192(ra)
[0x80002ca0]:lui gp, 1
[0x80002ca4]:addiw gp, gp, 2048
[0x80002ca8]:add t0, t0, gp
[0x80002cac]:ld t5, 600(t0)
[0x80002cb0]:sub t0, t0, gp
[0x80002cb4]:addi gp, zero, 0
[0x80002cb8]:csrrw zero, fcsr, gp
[0x80002cbc]:fsqrt.d t6, t5, dyn
[0x80002cc0]:csrrs t2, fcsr, zero

[0x80002cbc]:fsqrt.d t6, t5, dyn
[0x80002cc0]:csrrs t2, fcsr, zero
[0x80002cc4]:sd t6, 1200(ra)
[0x80002cc8]:sd t2, 1208(ra)
[0x80002ccc]:lui gp, 1
[0x80002cd0]:addiw gp, gp, 2048
[0x80002cd4]:add t0, t0, gp
[0x80002cd8]:ld t5, 608(t0)
[0x80002cdc]:sub t0, t0, gp
[0x80002ce0]:addi gp, zero, 0
[0x80002ce4]:csrrw zero, fcsr, gp
[0x80002ce8]:fsqrt.d t6, t5, dyn
[0x80002cec]:csrrs t2, fcsr, zero

[0x80002ce8]:fsqrt.d t6, t5, dyn
[0x80002cec]:csrrs t2, fcsr, zero
[0x80002cf0]:sd t6, 1216(ra)
[0x80002cf4]:sd t2, 1224(ra)
[0x80002cf8]:lui gp, 1
[0x80002cfc]:addiw gp, gp, 2048
[0x80002d00]:add t0, t0, gp
[0x80002d04]:ld t5, 616(t0)
[0x80002d08]:sub t0, t0, gp
[0x80002d0c]:addi gp, zero, 0
[0x80002d10]:csrrw zero, fcsr, gp
[0x80002d14]:fsqrt.d t6, t5, dyn
[0x80002d18]:csrrs t2, fcsr, zero

[0x80002d14]:fsqrt.d t6, t5, dyn
[0x80002d18]:csrrs t2, fcsr, zero
[0x80002d1c]:sd t6, 1232(ra)
[0x80002d20]:sd t2, 1240(ra)
[0x80002d24]:lui gp, 1
[0x80002d28]:addiw gp, gp, 2048
[0x80002d2c]:add t0, t0, gp
[0x80002d30]:ld t5, 624(t0)
[0x80002d34]:sub t0, t0, gp
[0x80002d38]:addi gp, zero, 0
[0x80002d3c]:csrrw zero, fcsr, gp
[0x80002d40]:fsqrt.d t6, t5, dyn
[0x80002d44]:csrrs t2, fcsr, zero

[0x80002d40]:fsqrt.d t6, t5, dyn
[0x80002d44]:csrrs t2, fcsr, zero
[0x80002d48]:sd t6, 1248(ra)
[0x80002d4c]:sd t2, 1256(ra)
[0x80002d50]:lui gp, 1
[0x80002d54]:addiw gp, gp, 2048
[0x80002d58]:add t0, t0, gp
[0x80002d5c]:ld t5, 632(t0)
[0x80002d60]:sub t0, t0, gp
[0x80002d64]:addi gp, zero, 0
[0x80002d68]:csrrw zero, fcsr, gp
[0x80002d6c]:fsqrt.d t6, t5, dyn
[0x80002d70]:csrrs t2, fcsr, zero

[0x80002d6c]:fsqrt.d t6, t5, dyn
[0x80002d70]:csrrs t2, fcsr, zero
[0x80002d74]:sd t6, 1264(ra)
[0x80002d78]:sd t2, 1272(ra)
[0x80002d7c]:lui gp, 1
[0x80002d80]:addiw gp, gp, 2048
[0x80002d84]:add t0, t0, gp
[0x80002d88]:ld t5, 640(t0)
[0x80002d8c]:sub t0, t0, gp
[0x80002d90]:addi gp, zero, 0
[0x80002d94]:csrrw zero, fcsr, gp
[0x80002d98]:fsqrt.d t6, t5, dyn
[0x80002d9c]:csrrs t2, fcsr, zero

[0x80002d98]:fsqrt.d t6, t5, dyn
[0x80002d9c]:csrrs t2, fcsr, zero
[0x80002da0]:sd t6, 1280(ra)
[0x80002da4]:sd t2, 1288(ra)
[0x80002da8]:lui gp, 1
[0x80002dac]:addiw gp, gp, 2048
[0x80002db0]:add t0, t0, gp
[0x80002db4]:ld t5, 648(t0)
[0x80002db8]:sub t0, t0, gp
[0x80002dbc]:addi gp, zero, 0
[0x80002dc0]:csrrw zero, fcsr, gp
[0x80002dc4]:fsqrt.d t6, t5, dyn
[0x80002dc8]:csrrs t2, fcsr, zero

[0x80002dc4]:fsqrt.d t6, t5, dyn
[0x80002dc8]:csrrs t2, fcsr, zero
[0x80002dcc]:sd t6, 1296(ra)
[0x80002dd0]:sd t2, 1304(ra)
[0x80002dd4]:lui gp, 1
[0x80002dd8]:addiw gp, gp, 2048
[0x80002ddc]:add t0, t0, gp
[0x80002de0]:ld t5, 656(t0)
[0x80002de4]:sub t0, t0, gp
[0x80002de8]:addi gp, zero, 0
[0x80002dec]:csrrw zero, fcsr, gp
[0x80002df0]:fsqrt.d t6, t5, dyn
[0x80002df4]:csrrs t2, fcsr, zero

[0x80002df0]:fsqrt.d t6, t5, dyn
[0x80002df4]:csrrs t2, fcsr, zero
[0x80002df8]:sd t6, 1312(ra)
[0x80002dfc]:sd t2, 1320(ra)
[0x80002e00]:lui gp, 1
[0x80002e04]:addiw gp, gp, 2048
[0x80002e08]:add t0, t0, gp
[0x80002e0c]:ld t5, 664(t0)
[0x80002e10]:sub t0, t0, gp
[0x80002e14]:addi gp, zero, 0
[0x80002e18]:csrrw zero, fcsr, gp
[0x80002e1c]:fsqrt.d t6, t5, dyn
[0x80002e20]:csrrs t2, fcsr, zero

[0x80002e1c]:fsqrt.d t6, t5, dyn
[0x80002e20]:csrrs t2, fcsr, zero
[0x80002e24]:sd t6, 1328(ra)
[0x80002e28]:sd t2, 1336(ra)
[0x80002e2c]:lui gp, 1
[0x80002e30]:addiw gp, gp, 2048
[0x80002e34]:add t0, t0, gp
[0x80002e38]:ld t5, 672(t0)
[0x80002e3c]:sub t0, t0, gp
[0x80002e40]:addi gp, zero, 0
[0x80002e44]:csrrw zero, fcsr, gp
[0x80002e48]:fsqrt.d t6, t5, dyn
[0x80002e4c]:csrrs t2, fcsr, zero

[0x80002e48]:fsqrt.d t6, t5, dyn
[0x80002e4c]:csrrs t2, fcsr, zero
[0x80002e50]:sd t6, 1344(ra)
[0x80002e54]:sd t2, 1352(ra)
[0x80002e58]:lui gp, 1
[0x80002e5c]:addiw gp, gp, 2048
[0x80002e60]:add t0, t0, gp
[0x80002e64]:ld t5, 680(t0)
[0x80002e68]:sub t0, t0, gp
[0x80002e6c]:addi gp, zero, 0
[0x80002e70]:csrrw zero, fcsr, gp
[0x80002e74]:fsqrt.d t6, t5, dyn
[0x80002e78]:csrrs t2, fcsr, zero

[0x80002e74]:fsqrt.d t6, t5, dyn
[0x80002e78]:csrrs t2, fcsr, zero
[0x80002e7c]:sd t6, 1360(ra)
[0x80002e80]:sd t2, 1368(ra)
[0x80002e84]:lui gp, 1
[0x80002e88]:addiw gp, gp, 2048
[0x80002e8c]:add t0, t0, gp
[0x80002e90]:ld t5, 688(t0)
[0x80002e94]:sub t0, t0, gp
[0x80002e98]:addi gp, zero, 0
[0x80002e9c]:csrrw zero, fcsr, gp
[0x80002ea0]:fsqrt.d t6, t5, dyn
[0x80002ea4]:csrrs t2, fcsr, zero

[0x80002ea0]:fsqrt.d t6, t5, dyn
[0x80002ea4]:csrrs t2, fcsr, zero
[0x80002ea8]:sd t6, 1376(ra)
[0x80002eac]:sd t2, 1384(ra)
[0x80002eb0]:lui gp, 1
[0x80002eb4]:addiw gp, gp, 2048
[0x80002eb8]:add t0, t0, gp
[0x80002ebc]:ld t5, 696(t0)
[0x80002ec0]:sub t0, t0, gp
[0x80002ec4]:addi gp, zero, 0
[0x80002ec8]:csrrw zero, fcsr, gp
[0x80002ecc]:fsqrt.d t6, t5, dyn
[0x80002ed0]:csrrs t2, fcsr, zero

[0x80002ecc]:fsqrt.d t6, t5, dyn
[0x80002ed0]:csrrs t2, fcsr, zero
[0x80002ed4]:sd t6, 1392(ra)
[0x80002ed8]:sd t2, 1400(ra)
[0x80002edc]:lui gp, 1
[0x80002ee0]:addiw gp, gp, 2048
[0x80002ee4]:add t0, t0, gp
[0x80002ee8]:ld t5, 704(t0)
[0x80002eec]:sub t0, t0, gp
[0x80002ef0]:addi gp, zero, 0
[0x80002ef4]:csrrw zero, fcsr, gp
[0x80002ef8]:fsqrt.d t6, t5, dyn
[0x80002efc]:csrrs t2, fcsr, zero

[0x80002ef8]:fsqrt.d t6, t5, dyn
[0x80002efc]:csrrs t2, fcsr, zero
[0x80002f00]:sd t6, 1408(ra)
[0x80002f04]:sd t2, 1416(ra)
[0x80002f08]:lui gp, 1
[0x80002f0c]:addiw gp, gp, 2048
[0x80002f10]:add t0, t0, gp
[0x80002f14]:ld t5, 712(t0)
[0x80002f18]:sub t0, t0, gp
[0x80002f1c]:addi gp, zero, 0
[0x80002f20]:csrrw zero, fcsr, gp
[0x80002f24]:fsqrt.d t6, t5, dyn
[0x80002f28]:csrrs t2, fcsr, zero

[0x80002f24]:fsqrt.d t6, t5, dyn
[0x80002f28]:csrrs t2, fcsr, zero
[0x80002f2c]:sd t6, 1424(ra)
[0x80002f30]:sd t2, 1432(ra)
[0x80002f34]:lui gp, 1
[0x80002f38]:addiw gp, gp, 2048
[0x80002f3c]:add t0, t0, gp
[0x80002f40]:ld t5, 720(t0)
[0x80002f44]:sub t0, t0, gp
[0x80002f48]:addi gp, zero, 0
[0x80002f4c]:csrrw zero, fcsr, gp
[0x80002f50]:fsqrt.d t6, t5, dyn
[0x80002f54]:csrrs t2, fcsr, zero

[0x80002f50]:fsqrt.d t6, t5, dyn
[0x80002f54]:csrrs t2, fcsr, zero
[0x80002f58]:sd t6, 1440(ra)
[0x80002f5c]:sd t2, 1448(ra)
[0x80002f60]:lui gp, 1
[0x80002f64]:addiw gp, gp, 2048
[0x80002f68]:add t0, t0, gp
[0x80002f6c]:ld t5, 728(t0)
[0x80002f70]:sub t0, t0, gp
[0x80002f74]:addi gp, zero, 0
[0x80002f78]:csrrw zero, fcsr, gp
[0x80002f7c]:fsqrt.d t6, t5, dyn
[0x80002f80]:csrrs t2, fcsr, zero

[0x80002f7c]:fsqrt.d t6, t5, dyn
[0x80002f80]:csrrs t2, fcsr, zero
[0x80002f84]:sd t6, 1456(ra)
[0x80002f88]:sd t2, 1464(ra)
[0x80002f8c]:lui gp, 1
[0x80002f90]:addiw gp, gp, 2048
[0x80002f94]:add t0, t0, gp
[0x80002f98]:ld t5, 736(t0)
[0x80002f9c]:sub t0, t0, gp
[0x80002fa0]:addi gp, zero, 0
[0x80002fa4]:csrrw zero, fcsr, gp
[0x80002fa8]:fsqrt.d t6, t5, dyn
[0x80002fac]:csrrs t2, fcsr, zero

[0x80002fa8]:fsqrt.d t6, t5, dyn
[0x80002fac]:csrrs t2, fcsr, zero
[0x80002fb0]:sd t6, 1472(ra)
[0x80002fb4]:sd t2, 1480(ra)
[0x80002fb8]:lui gp, 1
[0x80002fbc]:addiw gp, gp, 2048
[0x80002fc0]:add t0, t0, gp
[0x80002fc4]:ld t5, 744(t0)
[0x80002fc8]:sub t0, t0, gp
[0x80002fcc]:addi gp, zero, 0
[0x80002fd0]:csrrw zero, fcsr, gp
[0x80002fd4]:fsqrt.d t6, t5, dyn
[0x80002fd8]:csrrs t2, fcsr, zero

[0x80002fd4]:fsqrt.d t6, t5, dyn
[0x80002fd8]:csrrs t2, fcsr, zero
[0x80002fdc]:sd t6, 1488(ra)
[0x80002fe0]:sd t2, 1496(ra)
[0x80002fe4]:lui gp, 1
[0x80002fe8]:addiw gp, gp, 2048
[0x80002fec]:add t0, t0, gp
[0x80002ff0]:ld t5, 752(t0)
[0x80002ff4]:sub t0, t0, gp
[0x80002ff8]:addi gp, zero, 0
[0x80002ffc]:csrrw zero, fcsr, gp
[0x80003000]:fsqrt.d t6, t5, dyn
[0x80003004]:csrrs t2, fcsr, zero

[0x80003000]:fsqrt.d t6, t5, dyn
[0x80003004]:csrrs t2, fcsr, zero
[0x80003008]:sd t6, 1504(ra)
[0x8000300c]:sd t2, 1512(ra)
[0x80003010]:lui gp, 1
[0x80003014]:addiw gp, gp, 2048
[0x80003018]:add t0, t0, gp
[0x8000301c]:ld t5, 760(t0)
[0x80003020]:sub t0, t0, gp
[0x80003024]:addi gp, zero, 0
[0x80003028]:csrrw zero, fcsr, gp
[0x8000302c]:fsqrt.d t6, t5, dyn
[0x80003030]:csrrs t2, fcsr, zero

[0x8000302c]:fsqrt.d t6, t5, dyn
[0x80003030]:csrrs t2, fcsr, zero
[0x80003034]:sd t6, 1520(ra)
[0x80003038]:sd t2, 1528(ra)
[0x8000303c]:lui gp, 1
[0x80003040]:addiw gp, gp, 2048
[0x80003044]:add t0, t0, gp
[0x80003048]:ld t5, 768(t0)
[0x8000304c]:sub t0, t0, gp
[0x80003050]:addi gp, zero, 0
[0x80003054]:csrrw zero, fcsr, gp
[0x80003058]:fsqrt.d t6, t5, dyn
[0x8000305c]:csrrs t2, fcsr, zero

[0x80003058]:fsqrt.d t6, t5, dyn
[0x8000305c]:csrrs t2, fcsr, zero
[0x80003060]:sd t6, 1536(ra)
[0x80003064]:sd t2, 1544(ra)
[0x80003068]:lui gp, 1
[0x8000306c]:addiw gp, gp, 2048
[0x80003070]:add t0, t0, gp
[0x80003074]:ld t5, 776(t0)
[0x80003078]:sub t0, t0, gp
[0x8000307c]:addi gp, zero, 0
[0x80003080]:csrrw zero, fcsr, gp
[0x80003084]:fsqrt.d t6, t5, dyn
[0x80003088]:csrrs t2, fcsr, zero

[0x80003084]:fsqrt.d t6, t5, dyn
[0x80003088]:csrrs t2, fcsr, zero
[0x8000308c]:sd t6, 1552(ra)
[0x80003090]:sd t2, 1560(ra)
[0x80003094]:lui gp, 1
[0x80003098]:addiw gp, gp, 2048
[0x8000309c]:add t0, t0, gp
[0x800030a0]:ld t5, 784(t0)
[0x800030a4]:sub t0, t0, gp
[0x800030a8]:addi gp, zero, 0
[0x800030ac]:csrrw zero, fcsr, gp
[0x800030b0]:fsqrt.d t6, t5, dyn
[0x800030b4]:csrrs t2, fcsr, zero

[0x800030b0]:fsqrt.d t6, t5, dyn
[0x800030b4]:csrrs t2, fcsr, zero
[0x800030b8]:sd t6, 1568(ra)
[0x800030bc]:sd t2, 1576(ra)
[0x800030c0]:lui gp, 1
[0x800030c4]:addiw gp, gp, 2048
[0x800030c8]:add t0, t0, gp
[0x800030cc]:ld t5, 792(t0)
[0x800030d0]:sub t0, t0, gp
[0x800030d4]:addi gp, zero, 0
[0x800030d8]:csrrw zero, fcsr, gp
[0x800030dc]:fsqrt.d t6, t5, dyn
[0x800030e0]:csrrs t2, fcsr, zero

[0x800030dc]:fsqrt.d t6, t5, dyn
[0x800030e0]:csrrs t2, fcsr, zero
[0x800030e4]:sd t6, 1584(ra)
[0x800030e8]:sd t2, 1592(ra)
[0x800030ec]:lui gp, 1
[0x800030f0]:addiw gp, gp, 2048
[0x800030f4]:add t0, t0, gp
[0x800030f8]:ld t5, 800(t0)
[0x800030fc]:sub t0, t0, gp
[0x80003100]:addi gp, zero, 0
[0x80003104]:csrrw zero, fcsr, gp
[0x80003108]:fsqrt.d t6, t5, dyn
[0x8000310c]:csrrs t2, fcsr, zero

[0x80003108]:fsqrt.d t6, t5, dyn
[0x8000310c]:csrrs t2, fcsr, zero
[0x80003110]:sd t6, 1600(ra)
[0x80003114]:sd t2, 1608(ra)
[0x80003118]:lui gp, 1
[0x8000311c]:addiw gp, gp, 2048
[0x80003120]:add t0, t0, gp
[0x80003124]:ld t5, 808(t0)
[0x80003128]:sub t0, t0, gp
[0x8000312c]:addi gp, zero, 0
[0x80003130]:csrrw zero, fcsr, gp
[0x80003134]:fsqrt.d t6, t5, dyn
[0x80003138]:csrrs t2, fcsr, zero

[0x80003134]:fsqrt.d t6, t5, dyn
[0x80003138]:csrrs t2, fcsr, zero
[0x8000313c]:sd t6, 1616(ra)
[0x80003140]:sd t2, 1624(ra)
[0x80003144]:lui gp, 1
[0x80003148]:addiw gp, gp, 2048
[0x8000314c]:add t0, t0, gp
[0x80003150]:ld t5, 816(t0)
[0x80003154]:sub t0, t0, gp
[0x80003158]:addi gp, zero, 0
[0x8000315c]:csrrw zero, fcsr, gp
[0x80003160]:fsqrt.d t6, t5, dyn
[0x80003164]:csrrs t2, fcsr, zero

[0x80003160]:fsqrt.d t6, t5, dyn
[0x80003164]:csrrs t2, fcsr, zero
[0x80003168]:sd t6, 1632(ra)
[0x8000316c]:sd t2, 1640(ra)
[0x80003170]:lui gp, 1
[0x80003174]:addiw gp, gp, 2048
[0x80003178]:add t0, t0, gp
[0x8000317c]:ld t5, 824(t0)
[0x80003180]:sub t0, t0, gp
[0x80003184]:addi gp, zero, 0
[0x80003188]:csrrw zero, fcsr, gp
[0x8000318c]:fsqrt.d t6, t5, dyn
[0x80003190]:csrrs t2, fcsr, zero

[0x8000318c]:fsqrt.d t6, t5, dyn
[0x80003190]:csrrs t2, fcsr, zero
[0x80003194]:sd t6, 1648(ra)
[0x80003198]:sd t2, 1656(ra)
[0x8000319c]:lui gp, 1
[0x800031a0]:addiw gp, gp, 2048
[0x800031a4]:add t0, t0, gp
[0x800031a8]:ld t5, 832(t0)
[0x800031ac]:sub t0, t0, gp
[0x800031b0]:addi gp, zero, 0
[0x800031b4]:csrrw zero, fcsr, gp
[0x800031b8]:fsqrt.d t6, t5, dyn
[0x800031bc]:csrrs t2, fcsr, zero

[0x800031b8]:fsqrt.d t6, t5, dyn
[0x800031bc]:csrrs t2, fcsr, zero
[0x800031c0]:sd t6, 1664(ra)
[0x800031c4]:sd t2, 1672(ra)
[0x800031c8]:lui gp, 1
[0x800031cc]:addiw gp, gp, 2048
[0x800031d0]:add t0, t0, gp
[0x800031d4]:ld t5, 840(t0)
[0x800031d8]:sub t0, t0, gp
[0x800031dc]:addi gp, zero, 0
[0x800031e0]:csrrw zero, fcsr, gp
[0x800031e4]:fsqrt.d t6, t5, dyn
[0x800031e8]:csrrs t2, fcsr, zero

[0x800031e4]:fsqrt.d t6, t5, dyn
[0x800031e8]:csrrs t2, fcsr, zero
[0x800031ec]:sd t6, 1680(ra)
[0x800031f0]:sd t2, 1688(ra)
[0x800031f4]:lui gp, 1
[0x800031f8]:addiw gp, gp, 2048
[0x800031fc]:add t0, t0, gp
[0x80003200]:ld t5, 848(t0)
[0x80003204]:sub t0, t0, gp
[0x80003208]:addi gp, zero, 0
[0x8000320c]:csrrw zero, fcsr, gp
[0x80003210]:fsqrt.d t6, t5, dyn
[0x80003214]:csrrs t2, fcsr, zero

[0x80003210]:fsqrt.d t6, t5, dyn
[0x80003214]:csrrs t2, fcsr, zero
[0x80003218]:sd t6, 1696(ra)
[0x8000321c]:sd t2, 1704(ra)
[0x80003220]:lui gp, 1
[0x80003224]:addiw gp, gp, 2048
[0x80003228]:add t0, t0, gp
[0x8000322c]:ld t5, 856(t0)
[0x80003230]:sub t0, t0, gp
[0x80003234]:addi gp, zero, 0
[0x80003238]:csrrw zero, fcsr, gp
[0x8000323c]:fsqrt.d t6, t5, dyn
[0x80003240]:csrrs t2, fcsr, zero

[0x8000323c]:fsqrt.d t6, t5, dyn
[0x80003240]:csrrs t2, fcsr, zero
[0x80003244]:sd t6, 1712(ra)
[0x80003248]:sd t2, 1720(ra)
[0x8000324c]:lui gp, 1
[0x80003250]:addiw gp, gp, 2048
[0x80003254]:add t0, t0, gp
[0x80003258]:ld t5, 864(t0)
[0x8000325c]:sub t0, t0, gp
[0x80003260]:addi gp, zero, 0
[0x80003264]:csrrw zero, fcsr, gp
[0x80003268]:fsqrt.d t6, t5, dyn
[0x8000326c]:csrrs t2, fcsr, zero

[0x80003268]:fsqrt.d t6, t5, dyn
[0x8000326c]:csrrs t2, fcsr, zero
[0x80003270]:sd t6, 1728(ra)
[0x80003274]:sd t2, 1736(ra)
[0x80003278]:lui gp, 1
[0x8000327c]:addiw gp, gp, 2048
[0x80003280]:add t0, t0, gp
[0x80003284]:ld t5, 872(t0)
[0x80003288]:sub t0, t0, gp
[0x8000328c]:addi gp, zero, 0
[0x80003290]:csrrw zero, fcsr, gp
[0x80003294]:fsqrt.d t6, t5, dyn
[0x80003298]:csrrs t2, fcsr, zero

[0x80003294]:fsqrt.d t6, t5, dyn
[0x80003298]:csrrs t2, fcsr, zero
[0x8000329c]:sd t6, 1744(ra)
[0x800032a0]:sd t2, 1752(ra)
[0x800032a4]:lui gp, 1
[0x800032a8]:addiw gp, gp, 2048
[0x800032ac]:add t0, t0, gp
[0x800032b0]:ld t5, 880(t0)
[0x800032b4]:sub t0, t0, gp
[0x800032b8]:addi gp, zero, 0
[0x800032bc]:csrrw zero, fcsr, gp
[0x800032c0]:fsqrt.d t6, t5, dyn
[0x800032c4]:csrrs t2, fcsr, zero

[0x800032c0]:fsqrt.d t6, t5, dyn
[0x800032c4]:csrrs t2, fcsr, zero
[0x800032c8]:sd t6, 1760(ra)
[0x800032cc]:sd t2, 1768(ra)
[0x800032d0]:lui gp, 1
[0x800032d4]:addiw gp, gp, 2048
[0x800032d8]:add t0, t0, gp
[0x800032dc]:ld t5, 888(t0)
[0x800032e0]:sub t0, t0, gp
[0x800032e4]:addi gp, zero, 0
[0x800032e8]:csrrw zero, fcsr, gp
[0x800032ec]:fsqrt.d t6, t5, dyn
[0x800032f0]:csrrs t2, fcsr, zero

[0x800032ec]:fsqrt.d t6, t5, dyn
[0x800032f0]:csrrs t2, fcsr, zero
[0x800032f4]:sd t6, 1776(ra)
[0x800032f8]:sd t2, 1784(ra)
[0x800032fc]:lui gp, 1
[0x80003300]:addiw gp, gp, 2048
[0x80003304]:add t0, t0, gp
[0x80003308]:ld t5, 896(t0)
[0x8000330c]:sub t0, t0, gp
[0x80003310]:addi gp, zero, 0
[0x80003314]:csrrw zero, fcsr, gp
[0x80003318]:fsqrt.d t6, t5, dyn
[0x8000331c]:csrrs t2, fcsr, zero

[0x80003318]:fsqrt.d t6, t5, dyn
[0x8000331c]:csrrs t2, fcsr, zero
[0x80003320]:sd t6, 1792(ra)
[0x80003324]:sd t2, 1800(ra)
[0x80003328]:lui gp, 1
[0x8000332c]:addiw gp, gp, 2048
[0x80003330]:add t0, t0, gp
[0x80003334]:ld t5, 904(t0)
[0x80003338]:sub t0, t0, gp
[0x8000333c]:addi gp, zero, 0
[0x80003340]:csrrw zero, fcsr, gp
[0x80003344]:fsqrt.d t6, t5, dyn
[0x80003348]:csrrs t2, fcsr, zero

[0x80003344]:fsqrt.d t6, t5, dyn
[0x80003348]:csrrs t2, fcsr, zero
[0x8000334c]:sd t6, 1808(ra)
[0x80003350]:sd t2, 1816(ra)
[0x80003354]:lui gp, 1
[0x80003358]:addiw gp, gp, 2048
[0x8000335c]:add t0, t0, gp
[0x80003360]:ld t5, 912(t0)
[0x80003364]:sub t0, t0, gp
[0x80003368]:addi gp, zero, 0
[0x8000336c]:csrrw zero, fcsr, gp
[0x80003370]:fsqrt.d t6, t5, dyn
[0x80003374]:csrrs t2, fcsr, zero

[0x80003370]:fsqrt.d t6, t5, dyn
[0x80003374]:csrrs t2, fcsr, zero
[0x80003378]:sd t6, 1824(ra)
[0x8000337c]:sd t2, 1832(ra)
[0x80003380]:lui gp, 1
[0x80003384]:addiw gp, gp, 2048
[0x80003388]:add t0, t0, gp
[0x8000338c]:ld t5, 920(t0)
[0x80003390]:sub t0, t0, gp
[0x80003394]:addi gp, zero, 0
[0x80003398]:csrrw zero, fcsr, gp
[0x8000339c]:fsqrt.d t6, t5, dyn
[0x800033a0]:csrrs t2, fcsr, zero

[0x8000339c]:fsqrt.d t6, t5, dyn
[0x800033a0]:csrrs t2, fcsr, zero
[0x800033a4]:sd t6, 1840(ra)
[0x800033a8]:sd t2, 1848(ra)
[0x800033ac]:lui gp, 1
[0x800033b0]:addiw gp, gp, 2048
[0x800033b4]:add t0, t0, gp
[0x800033b8]:ld t5, 928(t0)
[0x800033bc]:sub t0, t0, gp
[0x800033c0]:addi gp, zero, 0
[0x800033c4]:csrrw zero, fcsr, gp
[0x800033c8]:fsqrt.d t6, t5, dyn
[0x800033cc]:csrrs t2, fcsr, zero

[0x800033c8]:fsqrt.d t6, t5, dyn
[0x800033cc]:csrrs t2, fcsr, zero
[0x800033d0]:sd t6, 1856(ra)
[0x800033d4]:sd t2, 1864(ra)
[0x800033d8]:lui gp, 1
[0x800033dc]:addiw gp, gp, 2048
[0x800033e0]:add t0, t0, gp
[0x800033e4]:ld t5, 936(t0)
[0x800033e8]:sub t0, t0, gp
[0x800033ec]:addi gp, zero, 0
[0x800033f0]:csrrw zero, fcsr, gp
[0x800033f4]:fsqrt.d t6, t5, dyn
[0x800033f8]:csrrs t2, fcsr, zero

[0x800033f4]:fsqrt.d t6, t5, dyn
[0x800033f8]:csrrs t2, fcsr, zero
[0x800033fc]:sd t6, 1872(ra)
[0x80003400]:sd t2, 1880(ra)
[0x80003404]:lui gp, 1
[0x80003408]:addiw gp, gp, 2048
[0x8000340c]:add t0, t0, gp
[0x80003410]:ld t5, 944(t0)
[0x80003414]:sub t0, t0, gp
[0x80003418]:addi gp, zero, 0
[0x8000341c]:csrrw zero, fcsr, gp
[0x80003420]:fsqrt.d t6, t5, dyn
[0x80003424]:csrrs t2, fcsr, zero

[0x80003420]:fsqrt.d t6, t5, dyn
[0x80003424]:csrrs t2, fcsr, zero
[0x80003428]:sd t6, 1888(ra)
[0x8000342c]:sd t2, 1896(ra)
[0x80003430]:lui gp, 1
[0x80003434]:addiw gp, gp, 2048
[0x80003438]:add t0, t0, gp
[0x8000343c]:ld t5, 952(t0)
[0x80003440]:sub t0, t0, gp
[0x80003444]:addi gp, zero, 0
[0x80003448]:csrrw zero, fcsr, gp
[0x8000344c]:fsqrt.d t6, t5, dyn
[0x80003450]:csrrs t2, fcsr, zero

[0x8000344c]:fsqrt.d t6, t5, dyn
[0x80003450]:csrrs t2, fcsr, zero
[0x80003454]:sd t6, 1904(ra)
[0x80003458]:sd t2, 1912(ra)
[0x8000345c]:lui gp, 1
[0x80003460]:addiw gp, gp, 2048
[0x80003464]:add t0, t0, gp
[0x80003468]:ld t5, 960(t0)
[0x8000346c]:sub t0, t0, gp
[0x80003470]:addi gp, zero, 0
[0x80003474]:csrrw zero, fcsr, gp
[0x80003478]:fsqrt.d t6, t5, dyn
[0x8000347c]:csrrs t2, fcsr, zero

[0x80003478]:fsqrt.d t6, t5, dyn
[0x8000347c]:csrrs t2, fcsr, zero
[0x80003480]:sd t6, 1920(ra)
[0x80003484]:sd t2, 1928(ra)
[0x80003488]:lui gp, 1
[0x8000348c]:addiw gp, gp, 2048
[0x80003490]:add t0, t0, gp
[0x80003494]:ld t5, 968(t0)
[0x80003498]:sub t0, t0, gp
[0x8000349c]:addi gp, zero, 0
[0x800034a0]:csrrw zero, fcsr, gp
[0x800034a4]:fsqrt.d t6, t5, dyn
[0x800034a8]:csrrs t2, fcsr, zero

[0x800034a4]:fsqrt.d t6, t5, dyn
[0x800034a8]:csrrs t2, fcsr, zero
[0x800034ac]:sd t6, 1936(ra)
[0x800034b0]:sd t2, 1944(ra)
[0x800034b4]:lui gp, 1
[0x800034b8]:addiw gp, gp, 2048
[0x800034bc]:add t0, t0, gp
[0x800034c0]:ld t5, 976(t0)
[0x800034c4]:sub t0, t0, gp
[0x800034c8]:addi gp, zero, 0
[0x800034cc]:csrrw zero, fcsr, gp
[0x800034d0]:fsqrt.d t6, t5, dyn
[0x800034d4]:csrrs t2, fcsr, zero

[0x800034d0]:fsqrt.d t6, t5, dyn
[0x800034d4]:csrrs t2, fcsr, zero
[0x800034d8]:sd t6, 1952(ra)
[0x800034dc]:sd t2, 1960(ra)
[0x800034e0]:lui gp, 1
[0x800034e4]:addiw gp, gp, 2048
[0x800034e8]:add t0, t0, gp
[0x800034ec]:ld t5, 984(t0)
[0x800034f0]:sub t0, t0, gp
[0x800034f4]:addi gp, zero, 0
[0x800034f8]:csrrw zero, fcsr, gp
[0x800034fc]:fsqrt.d t6, t5, dyn
[0x80003500]:csrrs t2, fcsr, zero

[0x800034fc]:fsqrt.d t6, t5, dyn
[0x80003500]:csrrs t2, fcsr, zero
[0x80003504]:sd t6, 1968(ra)
[0x80003508]:sd t2, 1976(ra)
[0x8000350c]:lui gp, 1
[0x80003510]:addiw gp, gp, 2048
[0x80003514]:add t0, t0, gp
[0x80003518]:ld t5, 992(t0)
[0x8000351c]:sub t0, t0, gp
[0x80003520]:addi gp, zero, 0
[0x80003524]:csrrw zero, fcsr, gp
[0x80003528]:fsqrt.d t6, t5, dyn
[0x8000352c]:csrrs t2, fcsr, zero

[0x80003528]:fsqrt.d t6, t5, dyn
[0x8000352c]:csrrs t2, fcsr, zero
[0x80003530]:sd t6, 1984(ra)
[0x80003534]:sd t2, 1992(ra)
[0x80003538]:lui gp, 1
[0x8000353c]:addiw gp, gp, 2048
[0x80003540]:add t0, t0, gp
[0x80003544]:ld t5, 1000(t0)
[0x80003548]:sub t0, t0, gp
[0x8000354c]:addi gp, zero, 0
[0x80003550]:csrrw zero, fcsr, gp
[0x80003554]:fsqrt.d t6, t5, dyn
[0x80003558]:csrrs t2, fcsr, zero

[0x80003554]:fsqrt.d t6, t5, dyn
[0x80003558]:csrrs t2, fcsr, zero
[0x8000355c]:sd t6, 2000(ra)
[0x80003560]:sd t2, 2008(ra)
[0x80003564]:lui gp, 1
[0x80003568]:addiw gp, gp, 2048
[0x8000356c]:add t0, t0, gp
[0x80003570]:ld t5, 1008(t0)
[0x80003574]:sub t0, t0, gp
[0x80003578]:addi gp, zero, 0
[0x8000357c]:csrrw zero, fcsr, gp
[0x80003580]:fsqrt.d t6, t5, dyn
[0x80003584]:csrrs t2, fcsr, zero

[0x80003580]:fsqrt.d t6, t5, dyn
[0x80003584]:csrrs t2, fcsr, zero
[0x80003588]:sd t6, 2016(ra)
[0x8000358c]:sd t2, 2024(ra)



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                                  signature                                   |                                                                                 coverpoints                                                                                 |                                                                                   code                                                                                    |
|---:|------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80005e18]<br>0x3B431D633DEB8924<br> [0x80005e20]<br>0x0000000000000001<br> |- mnemonic : fsqrt.d<br> - rs1 : x28<br> - rd : x30<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0x369 and fm1 == 0x6d601ad376ab9 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x800003b8]:fsqrt.d t5, t3, dyn<br> [0x800003bc]:csrrs t2, fcsr, zero<br> [0x800003c0]:sd t5, 0(ra)<br> [0x800003c4]:sd t2, 8(ra)<br>                                     |
|   2|[0x80005e28]<br>0x3BE6A09E667F3BCC<br> [0x80005e30]<br>0x0000000000000001<br> |- rs1 : x26<br> - rd : x26<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffffffffe and  fcsr == 0x0 and rm_val == 7   #nosat<br>                          |[0x800003d4]:fsqrt.d s10, s10, dyn<br> [0x800003d8]:csrrs t2, fcsr, zero<br> [0x800003dc]:sd s10, 16(ra)<br> [0x800003e0]:sd t2, 24(ra)<br>                                |
|   3|[0x80005e38]<br>0x3BE0000000000000<br> [0x80005e40]<br>0x0000000000000000<br> |- rs1 : x30<br> - rd : x28<br> - fs1 == 0 and fe1 == 0x37d and fm1 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800003f0]:fsqrt.d t3, t5, dyn<br> [0x800003f4]:csrrs t2, fcsr, zero<br> [0x800003f8]:sd t3, 32(ra)<br> [0x800003fc]:sd t2, 40(ra)<br>                                   |
|   4|[0x80005e48]<br>0x3BDFFFFFFFFFFFFE<br> [0x80005e50]<br>0x0000000000000001<br> |- rs1 : x22<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x37c and fm1 == 0xffffffffffffc and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x8000040c]:fsqrt.d s8, s6, dyn<br> [0x80000410]:csrrs t2, fcsr, zero<br> [0x80000414]:sd s8, 48(ra)<br> [0x80000418]:sd t2, 56(ra)<br>                                   |
|   5|[0x80005e58]<br>0x3BE3988E1409212E<br> [0x80005e60]<br>0x0000000000000001<br> |- rs1 : x24<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x37d and fm1 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000428]:fsqrt.d s6, s8, dyn<br> [0x8000042c]:csrrs t2, fcsr, zero<br> [0x80000430]:sd s6, 64(ra)<br> [0x80000434]:sd t2, 72(ra)<br>                                   |
|   6|[0x80005e68]<br>0x3BD6A09E667F3BCA<br> [0x80005e70]<br>0x0000000000000001<br> |- rs1 : x18<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x37b and fm1 == 0xffffffffffff8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000444]:fsqrt.d s4, s2, dyn<br> [0x80000448]:csrrs t2, fcsr, zero<br> [0x8000044c]:sd s4, 80(ra)<br> [0x80000450]:sd t2, 88(ra)<br>                                   |
|   7|[0x80005e78]<br>0x3BE52A7FA9D2F8EA<br> [0x80005e80]<br>0x0000000000000001<br> |- rs1 : x20<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x37d and fm1 == 0xc000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000460]:fsqrt.d s2, s4, dyn<br> [0x80000464]:csrrs t2, fcsr, zero<br> [0x80000468]:sd s2, 96(ra)<br> [0x8000046c]:sd t2, 104(ra)<br>                                  |
|   8|[0x80005e88]<br>0x3BCFFFFFFFFFFFF8<br> [0x80005e90]<br>0x0000000000000001<br> |- rs1 : x14<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x37a and fm1 == 0xffffffffffff0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x8000047c]:fsqrt.d a6, a4, dyn<br> [0x80000480]:csrrs t2, fcsr, zero<br> [0x80000484]:sd a6, 112(ra)<br> [0x80000488]:sd t2, 120(ra)<br>                                 |
|   9|[0x80005e98]<br>0x3BE5E8ADD236A58F<br> [0x80005ea0]<br>0x0000000000000001<br> |- rs1 : x16<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x37d and fm1 == 0xe000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000498]:fsqrt.d a4, a6, dyn<br> [0x8000049c]:csrrs t2, fcsr, zero<br> [0x800004a0]:sd a4, 128(ra)<br> [0x800004a4]:sd t2, 136(ra)<br>                                 |
|  10|[0x80005ea8]<br>0x3BC6A09E667F3BC1<br> [0x80005eb0]<br>0x0000000000000001<br> |- rs1 : x10<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x379 and fm1 == 0xfffffffffffe0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800004b4]:fsqrt.d a2, a0, dyn<br> [0x800004b8]:csrrs t2, fcsr, zero<br> [0x800004bc]:sd a2, 144(ra)<br> [0x800004c0]:sd t2, 152(ra)<br>                                 |
|  11|[0x80005eb8]<br>0x3BE645640568C1C3<br> [0x80005ec0]<br>0x0000000000000001<br> |- rs1 : x12<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x37d and fm1 == 0xf000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800004d0]:fsqrt.d a0, a2, dyn<br> [0x800004d4]:csrrs t2, fcsr, zero<br> [0x800004d8]:sd a0, 160(ra)<br> [0x800004dc]:sd t2, 168(ra)<br>                                 |
|  12|[0x80005ec8]<br>0x3BBFFFFFFFFFFFE0<br> [0x80005ed0]<br>0x0000000000000001<br> |- rs1 : x6<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x378 and fm1 == 0xfffffffffffc0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x800004ec]:fsqrt.d fp, t1, dyn<br> [0x800004f0]:csrrs t2, fcsr, zero<br> [0x800004f4]:sd fp, 176(ra)<br> [0x800004f8]:sd t2, 184(ra)<br>                                 |
|  13|[0x80005ed8]<br>0x3BE6732F8D0E2F77<br> [0x80005ee0]<br>0x0000000000000001<br> |- rs1 : x8<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x37d and fm1 == 0xf800000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x80000508]:fsqrt.d t1, fp, dyn<br> [0x8000050c]:csrrs t2, fcsr, zero<br> [0x80000510]:sd t1, 192(ra)<br> [0x80000514]:sd t2, 200(ra)<br>                                 |
|  14|[0x80005ee8]<br>0x3BB6A09E667F3B9F<br> [0x80005ef0]<br>0x0000000000000001<br> |- rs1 : x2<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x377 and fm1 == 0xfffffffffff80 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x80000524]:fsqrt.d tp, sp, dyn<br> [0x80000528]:csrrs t2, fcsr, zero<br> [0x8000052c]:sd tp, 208(ra)<br> [0x80000530]:sd t2, 216(ra)<br>                                 |
|  15|[0x80005ef8]<br>0x3BE689F26C6B01D0<br> [0x80005f00]<br>0x0000000000000001<br> |- rs1 : x4<br> - rd : x2<br> - fs1 == 0 and fe1 == 0x37d and fm1 == 0xfc00000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x80000540]:fsqrt.d sp, tp, dyn<br> [0x80000544]:csrrs t2, fcsr, zero<br> [0x80000548]:sd sp, 224(ra)<br> [0x8000054c]:sd t2, 232(ra)<br>                                 |
|  16|[0x80005f08]<br>0x3BAFFFFFFFFFFF80<br> [0x80005f10]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x376 and fm1 == 0xfffffffffff00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000055c]:fsqrt.d t6, t5, dyn<br> [0x80000560]:csrrs t2, fcsr, zero<br> [0x80000564]:sd t6, 240(ra)<br> [0x80000568]:sd t2, 248(ra)<br>                                 |
|  17|[0x80005f18]<br>0x3BE6954B41CD4293<br> [0x80005f20]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfe00000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000578]:fsqrt.d t6, t5, dyn<br> [0x8000057c]:csrrs t2, fcsr, zero<br> [0x80000580]:sd t6, 256(ra)<br> [0x80000584]:sd t2, 264(ra)<br>                                 |
|  18|[0x80005f28]<br>0x3BA6A09E667F3B18<br> [0x80005f30]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x375 and fm1 == 0xffffffffffe00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000594]:fsqrt.d t6, t5, dyn<br> [0x80000598]:csrrs t2, fcsr, zero<br> [0x8000059c]:sd t6, 272(ra)<br> [0x800005a0]:sd t2, 280(ra)<br>                                 |
|  19|[0x80005f38]<br>0x3BE69AF589B35963<br> [0x80005f40]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xff00000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800005b0]:fsqrt.d t6, t5, dyn<br> [0x800005b4]:csrrs t2, fcsr, zero<br> [0x800005b8]:sd t6, 288(ra)<br> [0x800005bc]:sd t2, 296(ra)<br>                                 |
|  20|[0x80005f48]<br>0x3B9FFFFFFFFFFE00<br> [0x80005f50]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x374 and fm1 == 0xffffffffffc00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800005cc]:fsqrt.d t6, t5, dyn<br> [0x800005d0]:csrrs t2, fcsr, zero<br> [0x800005d4]:sd t6, 304(ra)<br> [0x800005d8]:sd t2, 312(ra)<br>                                 |
|  21|[0x80005f58]<br>0x3BE69DCA256B860E<br> [0x80005f60]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xff80000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800005e8]:fsqrt.d t6, t5, dyn<br> [0x800005ec]:csrrs t2, fcsr, zero<br> [0x800005f0]:sd t6, 320(ra)<br> [0x800005f4]:sd t2, 328(ra)<br>                                 |
|  22|[0x80005f68]<br>0x3B96A09E667F38F8<br> [0x80005f70]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x373 and fm1 == 0xffffffffff800 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000604]:fsqrt.d t6, t5, dyn<br> [0x80000608]:csrrs t2, fcsr, zero<br> [0x8000060c]:sd t6, 336(ra)<br> [0x80000610]:sd t2, 344(ra)<br>                                 |
|  23|[0x80005f78]<br>0x3BE69F345147CF92<br> [0x80005f80]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffc0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000620]:fsqrt.d t6, t5, dyn<br> [0x80000624]:csrrs t2, fcsr, zero<br> [0x80000628]:sd t6, 352(ra)<br> [0x8000062c]:sd t2, 360(ra)<br>                                 |
|  24|[0x80005f88]<br>0x3B8FFFFFFFFFF800<br> [0x80005f90]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x372 and fm1 == 0xffffffffff000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000063c]:fsqrt.d t6, t5, dyn<br> [0x80000640]:csrrs t2, fcsr, zero<br> [0x80000644]:sd t6, 368(ra)<br> [0x80000648]:sd t2, 376(ra)<br>                                 |
|  25|[0x80005f98]<br>0x3BE69FE95EB7DD64<br> [0x80005fa0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffe0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000658]:fsqrt.d t6, t5, dyn<br> [0x8000065c]:csrrs t2, fcsr, zero<br> [0x80000660]:sd t6, 384(ra)<br> [0x80000664]:sd t2, 392(ra)<br>                                 |
|  26|[0x80005fa8]<br>0x3B86A09E667F307C<br> [0x80005fb0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x371 and fm1 == 0xfffffffffe000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000674]:fsqrt.d t6, t5, dyn<br> [0x80000678]:csrrs t2, fcsr, zero<br> [0x8000067c]:sd t6, 400(ra)<br> [0x80000680]:sd t2, 408(ra)<br>                                 |
|  27|[0x80005fb8]<br>0x3BE6A043E3509A08<br> [0x80005fc0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfff0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000690]:fsqrt.d t6, t5, dyn<br> [0x80000694]:csrrs t2, fcsr, zero<br> [0x80000698]:sd t6, 416(ra)<br> [0x8000069c]:sd t2, 424(ra)<br>                                 |
|  28|[0x80005fc8]<br>0x3B7FFFFFFFFFE000<br> [0x80005fd0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x370 and fm1 == 0xfffffffffc000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800006ac]:fsqrt.d t6, t5, dyn<br> [0x800006b0]:csrrs t2, fcsr, zero<br> [0x800006b4]:sd t6, 432(ra)<br> [0x800006b8]:sd t2, 440(ra)<br>                                 |
|  29|[0x80005fd8]<br>0x3BE6A07125152D37<br> [0x80005fe0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfff8000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800006c8]:fsqrt.d t6, t5, dyn<br> [0x800006cc]:csrrs t2, fcsr, zero<br> [0x800006d0]:sd t6, 448(ra)<br> [0x800006d4]:sd t2, 456(ra)<br>                                 |
|  30|[0x80005fe8]<br>0x3B76A09E667F0E8B<br> [0x80005ff0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x36f and fm1 == 0xfffffffff8000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800006e4]:fsqrt.d t6, t5, dyn<br> [0x800006e8]:csrrs t2, fcsr, zero<br> [0x800006ec]:sd t6, 464(ra)<br> [0x800006f0]:sd t2, 472(ra)<br>                                 |
|  31|[0x80005ff8]<br>0x3BE6A087C5D584F3<br> [0x80006000]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffc000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000700]:fsqrt.d t6, t5, dyn<br> [0x80000704]:csrrs t2, fcsr, zero<br> [0x80000708]:sd t6, 480(ra)<br> [0x8000070c]:sd t2, 488(ra)<br>                                 |
|  32|[0x80006008]<br>0x3B6FFFFFFFFF8000<br> [0x80006010]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x36e and fm1 == 0xfffffffff0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000071c]:fsqrt.d t6, t5, dyn<br> [0x80000720]:csrrs t2, fcsr, zero<br> [0x80000724]:sd t6, 496(ra)<br> [0x80000728]:sd t2, 504(ra)<br>                                 |
|  33|[0x80006018]<br>0x3BE6A093162D3478<br> [0x80006020]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffe000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000738]:fsqrt.d t6, t5, dyn<br> [0x8000073c]:csrrs t2, fcsr, zero<br> [0x80000740]:sd t6, 512(ra)<br> [0x80000744]:sd t2, 520(ra)<br>                                 |
|  34|[0x80006028]<br>0x3B66A09E667E86C8<br> [0x80006030]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x36d and fm1 == 0xffffffffe0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000754]:fsqrt.d t6, t5, dyn<br> [0x80000758]:csrrs t2, fcsr, zero<br> [0x8000075c]:sd t6, 528(ra)<br> [0x80000760]:sd t2, 536(ra)<br>                                 |
|  35|[0x80006038]<br>0x3BE6A098BE56ED28<br> [0x80006040]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffff000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000770]:fsqrt.d t6, t5, dyn<br> [0x80000774]:csrrs t2, fcsr, zero<br> [0x80000778]:sd t6, 544(ra)<br> [0x8000077c]:sd t2, 552(ra)<br>                                 |
|  36|[0x80006048]<br>0x3B5FFFFFFFFE0000<br> [0x80006050]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x36c and fm1 == 0xffffffffc0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000078c]:fsqrt.d t6, t5, dyn<br> [0x80000790]:csrrs t2, fcsr, zero<br> [0x80000794]:sd t6, 560(ra)<br> [0x80000798]:sd t2, 568(ra)<br>                                 |
|  37|[0x80006058]<br>0x3BE6A09B926B41BB<br> [0x80006060]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffff800000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800007a8]:fsqrt.d t6, t5, dyn<br> [0x800007ac]:csrrs t2, fcsr, zero<br> [0x800007b0]:sd t6, 576(ra)<br> [0x800007b4]:sd t2, 584(ra)<br>                                 |
|  38|[0x80006068]<br>0x3B56A09E667C67B9<br> [0x80006070]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x36b and fm1 == 0xffffffff80000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800007c4]:fsqrt.d t6, t5, dyn<br> [0x800007c8]:csrrs t2, fcsr, zero<br> [0x800007cc]:sd t6, 592(ra)<br> [0x800007d0]:sd t2, 600(ra)<br>                                 |
|  39|[0x80006078]<br>0x3BE6A09CFC754A14<br> [0x80006080]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffc00000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800007e0]:fsqrt.d t6, t5, dyn<br> [0x800007e4]:csrrs t2, fcsr, zero<br> [0x800007e8]:sd t6, 608(ra)<br> [0x800007ec]:sd t2, 616(ra)<br>                                 |
|  40|[0x80006088]<br>0x3B4FFFFFFFF80000<br> [0x80006090]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x36a and fm1 == 0xffffffff00000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800007fc]:fsqrt.d t6, t5, dyn<br> [0x80000800]:csrrs t2, fcsr, zero<br> [0x80000804]:sd t6, 624(ra)<br> [0x80000808]:sd t2, 632(ra)<br>                                 |
|  41|[0x80006098]<br>0x3BE6A09DB17A45C5<br> [0x800060a0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffe00000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000818]:fsqrt.d t6, t5, dyn<br> [0x8000081c]:csrrs t2, fcsr, zero<br> [0x80000820]:sd t6, 640(ra)<br> [0x80000824]:sd t2, 648(ra)<br>                                 |
|  42|[0x800060a8]<br>0x3B46A09E6673EB7D<br> [0x800060b0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x369 and fm1 == 0xfffffffe00000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000834]:fsqrt.d t6, t5, dyn<br> [0x80000838]:csrrs t2, fcsr, zero<br> [0x8000083c]:sd t6, 656(ra)<br> [0x80000840]:sd t2, 664(ra)<br>                                 |
|  43|[0x800060b8]<br>0x3BE6A09E0BFCC17E<br> [0x800060c0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffff00000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000850]:fsqrt.d t6, t5, dyn<br> [0x80000854]:csrrs t2, fcsr, zero<br> [0x80000858]:sd t6, 672(ra)<br> [0x8000085c]:sd t2, 680(ra)<br>                                 |
|  44|[0x800060c8]<br>0x3BE6A09E393DFED2<br> [0x800060d0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffff80000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000086c]:fsqrt.d t6, t5, dyn<br> [0x80000870]:csrrs t2, fcsr, zero<br> [0x80000874]:sd t6, 688(ra)<br> [0x80000878]:sd t2, 696(ra)<br>                                 |
|  45|[0x800060d8]<br>0x3BE6A09E4FDE9D5B<br> [0x800060e0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffc0000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000888]:fsqrt.d t6, t5, dyn<br> [0x8000088c]:csrrs t2, fcsr, zero<br> [0x80000890]:sd t6, 704(ra)<br> [0x80000894]:sd t2, 712(ra)<br>                                 |
|  46|[0x800060e8]<br>0x3BE6A09E5B2EEC96<br> [0x800060f0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffe0000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800008a4]:fsqrt.d t6, t5, dyn<br> [0x800008a8]:csrrs t2, fcsr, zero<br> [0x800008ac]:sd t6, 720(ra)<br> [0x800008b0]:sd t2, 728(ra)<br>                                 |
|  47|[0x800060f8]<br>0x3BE6A09E60D71432<br> [0x80006100]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffff0000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800008c0]:fsqrt.d t6, t5, dyn<br> [0x800008c4]:csrrs t2, fcsr, zero<br> [0x800008c8]:sd t6, 736(ra)<br> [0x800008cc]:sd t2, 744(ra)<br>                                 |
|  48|[0x80006108]<br>0x3BE6A09E63AB2800<br> [0x80006110]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffff8000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800008dc]:fsqrt.d t6, t5, dyn<br> [0x800008e0]:csrrs t2, fcsr, zero<br> [0x800008e4]:sd t6, 752(ra)<br> [0x800008e8]:sd t2, 760(ra)<br>                                 |
|  49|[0x80006118]<br>0x3BE6A09E651531E6<br> [0x80006120]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffc000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800008f8]:fsqrt.d t6, t5, dyn<br> [0x800008fc]:csrrs t2, fcsr, zero<br> [0x80000900]:sd t6, 768(ra)<br> [0x80000904]:sd t2, 776(ra)<br>                                 |
|  50|[0x80006128]<br>0x3BE6A09E65CA36D9<br> [0x80006130]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffe000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000914]:fsqrt.d t6, t5, dyn<br> [0x80000918]:csrrs t2, fcsr, zero<br> [0x8000091c]:sd t6, 784(ra)<br> [0x80000920]:sd t2, 792(ra)<br>                                 |
|  51|[0x80006138]<br>0x3BE6A09E6624B953<br> [0x80006140]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffff000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000930]:fsqrt.d t6, t5, dyn<br> [0x80000934]:csrrs t2, fcsr, zero<br> [0x80000938]:sd t6, 800(ra)<br> [0x8000093c]:sd t2, 808(ra)<br>                                 |
|  52|[0x80006148]<br>0x3BE6A09E6651FA90<br> [0x80006150]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffff800000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000094c]:fsqrt.d t6, t5, dyn<br> [0x80000950]:csrrs t2, fcsr, zero<br> [0x80000954]:sd t6, 816(ra)<br> [0x80000958]:sd t2, 824(ra)<br>                                 |
|  53|[0x80006158]<br>0x3BE6A09E66689B2E<br> [0x80006160]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffffc00000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000968]:fsqrt.d t6, t5, dyn<br> [0x8000096c]:csrrs t2, fcsr, zero<br> [0x80000970]:sd t6, 832(ra)<br> [0x80000974]:sd t2, 840(ra)<br>                                 |
|  54|[0x80006168]<br>0x3BE6A09E6673EB7D<br> [0x80006170]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffffe00000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000984]:fsqrt.d t6, t5, dyn<br> [0x80000988]:csrrs t2, fcsr, zero<br> [0x8000098c]:sd t6, 848(ra)<br> [0x80000990]:sd t2, 856(ra)<br>                                 |
|  55|[0x80006178]<br>0x3BE6A09E667993A5<br> [0x80006180]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffff00000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800009a0]:fsqrt.d t6, t5, dyn<br> [0x800009a4]:csrrs t2, fcsr, zero<br> [0x800009a8]:sd t6, 864(ra)<br> [0x800009ac]:sd t2, 872(ra)<br>                                 |
|  56|[0x80006188]<br>0x3BE6A09E667C67B9<br> [0x80006190]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffff80000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800009bc]:fsqrt.d t6, t5, dyn<br> [0x800009c0]:csrrs t2, fcsr, zero<br> [0x800009c4]:sd t6, 880(ra)<br> [0x800009c8]:sd t2, 888(ra)<br>                                 |
|  57|[0x80006198]<br>0x3BE6A09E667DD1C3<br> [0x800061a0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffffc0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800009d8]:fsqrt.d t6, t5, dyn<br> [0x800009dc]:csrrs t2, fcsr, zero<br> [0x800009e0]:sd t6, 896(ra)<br> [0x800009e4]:sd t2, 904(ra)<br>                                 |
|  58|[0x800061a8]<br>0x3BE6A09E667E86C8<br> [0x800061b0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffffe0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800009f4]:fsqrt.d t6, t5, dyn<br> [0x800009f8]:csrrs t2, fcsr, zero<br> [0x800009fc]:sd t6, 912(ra)<br> [0x80000a00]:sd t2, 920(ra)<br>                                 |
|  59|[0x800061b8]<br>0x3BE6A09E667EE14A<br> [0x800061c0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffffff0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000a10]:fsqrt.d t6, t5, dyn<br> [0x80000a14]:csrrs t2, fcsr, zero<br> [0x80000a18]:sd t6, 928(ra)<br> [0x80000a1c]:sd t2, 936(ra)<br>                                 |
|  60|[0x800061c8]<br>0x3BE6A09E667F0E8B<br> [0x800061d0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffffff8000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000a2c]:fsqrt.d t6, t5, dyn<br> [0x80000a30]:csrrs t2, fcsr, zero<br> [0x80000a34]:sd t6, 944(ra)<br> [0x80000a38]:sd t2, 952(ra)<br>                                 |
|  61|[0x800061d8]<br>0x3BE6A09E667F252C<br> [0x800061e0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffffffc000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000a48]:fsqrt.d t6, t5, dyn<br> [0x80000a4c]:csrrs t2, fcsr, zero<br> [0x80000a50]:sd t6, 960(ra)<br> [0x80000a54]:sd t2, 968(ra)<br>                                 |
|  62|[0x800061e8]<br>0x3BE6A09E667F307C<br> [0x800061f0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffffffe000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000a64]:fsqrt.d t6, t5, dyn<br> [0x80000a68]:csrrs t2, fcsr, zero<br> [0x80000a6c]:sd t6, 976(ra)<br> [0x80000a70]:sd t2, 984(ra)<br>                                 |
|  63|[0x800061f8]<br>0x3BE6A09E667F3624<br> [0x80006200]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffffff000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000a80]:fsqrt.d t6, t5, dyn<br> [0x80000a84]:csrrs t2, fcsr, zero<br> [0x80000a88]:sd t6, 992(ra)<br> [0x80000a8c]:sd t2, 1000(ra)<br>                                |
|  64|[0x80006208]<br>0x3BE6A09E667F38F8<br> [0x80006210]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffffff800 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000a9c]:fsqrt.d t6, t5, dyn<br> [0x80000aa0]:csrrs t2, fcsr, zero<br> [0x80000aa4]:sd t6, 1008(ra)<br> [0x80000aa8]:sd t2, 1016(ra)<br>                               |
|  65|[0x80006218]<br>0x3BE6A09E667F3A63<br> [0x80006220]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffffffc00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000ab8]:fsqrt.d t6, t5, dyn<br> [0x80000abc]:csrrs t2, fcsr, zero<br> [0x80000ac0]:sd t6, 1024(ra)<br> [0x80000ac4]:sd t2, 1032(ra)<br>                               |
|  66|[0x80006228]<br>0x3BE6A09E667F3B18<br> [0x80006230]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffffffe00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000ad4]:fsqrt.d t6, t5, dyn<br> [0x80000ad8]:csrrs t2, fcsr, zero<br> [0x80000adc]:sd t6, 1040(ra)<br> [0x80000ae0]:sd t2, 1048(ra)<br>                               |
|  67|[0x80006238]<br>0x3BE6A09E667F3B72<br> [0x80006240]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffffffff00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000af0]:fsqrt.d t6, t5, dyn<br> [0x80000af4]:csrrs t2, fcsr, zero<br> [0x80000af8]:sd t6, 1056(ra)<br> [0x80000afc]:sd t2, 1064(ra)<br>                               |
|  68|[0x80006248]<br>0x3BE6A09E667F3B9F<br> [0x80006250]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffffffff80 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000b0c]:fsqrt.d t6, t5, dyn<br> [0x80000b10]:csrrs t2, fcsr, zero<br> [0x80000b14]:sd t6, 1072(ra)<br> [0x80000b18]:sd t2, 1080(ra)<br>                               |
|  69|[0x80006258]<br>0x3BE6A09E667F3BB6<br> [0x80006260]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffffffffc0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000b28]:fsqrt.d t6, t5, dyn<br> [0x80000b2c]:csrrs t2, fcsr, zero<br> [0x80000b30]:sd t6, 1088(ra)<br> [0x80000b34]:sd t2, 1096(ra)<br>                               |
|  70|[0x80006268]<br>0x3BE6A09E667F3BC1<br> [0x80006270]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffffffffe0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000b44]:fsqrt.d t6, t5, dyn<br> [0x80000b48]:csrrs t2, fcsr, zero<br> [0x80000b4c]:sd t6, 1104(ra)<br> [0x80000b50]:sd t2, 1112(ra)<br>                               |
|  71|[0x80006278]<br>0x3BE6A09E667F3BC7<br> [0x80006280]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffffffff0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000b60]:fsqrt.d t6, t5, dyn<br> [0x80000b64]:csrrs t2, fcsr, zero<br> [0x80000b68]:sd t6, 1120(ra)<br> [0x80000b6c]:sd t2, 1128(ra)<br>                               |
|  72|[0x80006288]<br>0x3BE6A09E667F3BCA<br> [0x80006290]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffffffff8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000b7c]:fsqrt.d t6, t5, dyn<br> [0x80000b80]:csrrs t2, fcsr, zero<br> [0x80000b84]:sd t6, 1136(ra)<br> [0x80000b88]:sd t2, 1144(ra)<br>                               |
|  73|[0x80006298]<br>0x3BE6A09E667F3BCB<br> [0x800062a0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffffffffc and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000b98]:fsqrt.d t6, t5, dyn<br> [0x80000b9c]:csrrs t2, fcsr, zero<br> [0x80000ba0]:sd t6, 1152(ra)<br> [0x80000ba4]:sd t2, 1160(ra)<br>                               |
|  74|[0x800062a8]<br>0x3FF5E8ADD236A58F<br> [0x800062b0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000bb4]:fsqrt.d t6, t5, dyn<br> [0x80000bb8]:csrrs t2, fcsr, zero<br> [0x80000bbc]:sd t6, 1168(ra)<br> [0x80000bc0]:sd t2, 1176(ra)<br>                               |
|  75|[0x800062b8]<br>0x3FF6A09E667F3BCD<br> [0x800062c0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x400 and fm1 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000bd0]:fsqrt.d t6, t5, dyn<br> [0x80000bd4]:csrrs t2, fcsr, zero<br> [0x80000bd8]:sd t6, 1184(ra)<br> [0x80000bdc]:sd t2, 1192(ra)<br>                               |
|  76|[0x800062c8]<br>0x3FF645640568C1C3<br> [0x800062d0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xf000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000bec]:fsqrt.d t6, t5, dyn<br> [0x80000bf0]:csrrs t2, fcsr, zero<br> [0x80000bf4]:sd t6, 1200(ra)<br> [0x80000bf8]:sd t2, 1208(ra)<br>                               |
|  77|[0x800062d8]<br>0x3FF6732F8D0E2F77<br> [0x800062e0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xf800000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000c08]:fsqrt.d t6, t5, dyn<br> [0x80000c0c]:csrrs t2, fcsr, zero<br> [0x80000c10]:sd t6, 1216(ra)<br> [0x80000c14]:sd t2, 1224(ra)<br>                               |
|  78|[0x800062e8]<br>0x3FF617398F2AAA48<br> [0x800062f0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe800000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000c24]:fsqrt.d t6, t5, dyn<br> [0x80000c28]:csrrs t2, fcsr, zero<br> [0x80000c2c]:sd t6, 1232(ra)<br> [0x80000c30]:sd t2, 1240(ra)<br>                               |
|  79|[0x800062f8]<br>0x3FF689F26C6B01D0<br> [0x80006300]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfc00000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000c40]:fsqrt.d t6, t5, dyn<br> [0x80000c44]:csrrs t2, fcsr, zero<br> [0x80000c48]:sd t6, 1248(ra)<br> [0x80000c4c]:sd t2, 1256(ra)<br>                               |
|  80|[0x80006308]<br>0x3FF6000000000000<br> [0x80006310]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe400000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000c5c]:fsqrt.d t6, t5, dyn<br> [0x80000c60]:csrrs t2, fcsr, zero<br> [0x80000c64]:sd t6, 1264(ra)<br> [0x80000c68]:sd t2, 1272(ra)<br>                               |
|  81|[0x80006318]<br>0x3FF6954B41CD4293<br> [0x80006320]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfe00000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000c78]:fsqrt.d t6, t5, dyn<br> [0x80000c7c]:csrrs t2, fcsr, zero<br> [0x80000c80]:sd t6, 1280(ra)<br> [0x80000c84]:sd t2, 1288(ra)<br>                               |
|  82|[0x80006328]<br>0x3FF5F45A01D483B4<br> [0x80006330]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe200000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000c94]:fsqrt.d t6, t5, dyn<br> [0x80000c98]:csrrs t2, fcsr, zero<br> [0x80000c9c]:sd t6, 1296(ra)<br> [0x80000ca0]:sd t2, 1304(ra)<br>                               |
|  83|[0x80006338]<br>0x3FF69AF589B35963<br> [0x80006340]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xff00000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000cb0]:fsqrt.d t6, t5, dyn<br> [0x80000cb4]:csrrs t2, fcsr, zero<br> [0x80000cb8]:sd t6, 1312(ra)<br> [0x80000cbc]:sd t2, 1320(ra)<br>                               |
|  84|[0x80006348]<br>0x3FF5EE84B0D1F876<br> [0x80006350]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe100000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000ccc]:fsqrt.d t6, t5, dyn<br> [0x80000cd0]:csrrs t2, fcsr, zero<br> [0x80000cd4]:sd t6, 1328(ra)<br> [0x80000cd8]:sd t2, 1336(ra)<br>                               |
|  85|[0x80006358]<br>0x3FF69DCA256B860E<br> [0x80006360]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xff80000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000ce8]:fsqrt.d t6, t5, dyn<br> [0x80000cec]:csrrs t2, fcsr, zero<br> [0x80000cf0]:sd t6, 1344(ra)<br> [0x80000cf4]:sd t2, 1352(ra)<br>                               |
|  86|[0x80006368]<br>0x3FF5EB99734B41EE<br> [0x80006370]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe080000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000d04]:fsqrt.d t6, t5, dyn<br> [0x80000d08]:csrrs t2, fcsr, zero<br> [0x80000d0c]:sd t6, 1360(ra)<br> [0x80000d10]:sd t2, 1368(ra)<br>                               |
|  87|[0x80006378]<br>0x3FF69F345147CF92<br> [0x80006380]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffc0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000d20]:fsqrt.d t6, t5, dyn<br> [0x80000d24]:csrrs t2, fcsr, zero<br> [0x80000d28]:sd t6, 1376(ra)<br> [0x80000d2c]:sd t2, 1384(ra)<br>                               |
|  88|[0x80006388]<br>0x3FF5EA23AF352D2B<br> [0x80006390]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe040000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000d3c]:fsqrt.d t6, t5, dyn<br> [0x80000d40]:csrrs t2, fcsr, zero<br> [0x80000d44]:sd t6, 1392(ra)<br> [0x80000d48]:sd t2, 1400(ra)<br>                               |
|  89|[0x80006398]<br>0x3FF69FE95EB7DD64<br> [0x800063a0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffe0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000d58]:fsqrt.d t6, t5, dyn<br> [0x80000d5c]:csrrs t2, fcsr, zero<br> [0x80000d60]:sd t6, 1408(ra)<br> [0x80000d64]:sd t2, 1416(ra)<br>                               |
|  90|[0x800063a8]<br>0x3FF5E968C3D34765<br> [0x800063b0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe020000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000d74]:fsqrt.d t6, t5, dyn<br> [0x80000d78]:csrrs t2, fcsr, zero<br> [0x80000d7c]:sd t6, 1424(ra)<br> [0x80000d80]:sd t2, 1432(ra)<br>                               |
|  91|[0x800063b8]<br>0x3FF6A043E3509A08<br> [0x800063c0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfff0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000d90]:fsqrt.d t6, t5, dyn<br> [0x80000d94]:csrrs t2, fcsr, zero<br> [0x80000d98]:sd t6, 1440(ra)<br> [0x80000d9c]:sd t2, 1448(ra)<br>                               |
|  92|[0x800063c8]<br>0x3FF5E90B4BCC57F3<br> [0x800063d0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe010000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000dac]:fsqrt.d t6, t5, dyn<br> [0x80000db0]:csrrs t2, fcsr, zero<br> [0x80000db4]:sd t6, 1456(ra)<br> [0x80000db8]:sd t2, 1464(ra)<br>                               |
|  93|[0x800063d8]<br>0x3FF6A07125152D37<br> [0x800063e0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfff8000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000dc8]:fsqrt.d t6, t5, dyn<br> [0x80000dcc]:csrrs t2, fcsr, zero<br> [0x80000dd0]:sd t6, 1472(ra)<br> [0x80000dd4]:sd t2, 1480(ra)<br>                               |
|  94|[0x800063e8]<br>0x3FF5E8DC8F33585E<br> [0x800063f0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe008000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000de4]:fsqrt.d t6, t5, dyn<br> [0x80000de8]:csrrs t2, fcsr, zero<br> [0x80000dec]:sd t6, 1488(ra)<br> [0x80000df0]:sd t2, 1496(ra)<br>                               |
|  95|[0x800063f8]<br>0x3FF6A087C5D584F3<br> [0x80006400]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffc000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000e00]:fsqrt.d t6, t5, dyn<br> [0x80000e04]:csrrs t2, fcsr, zero<br> [0x80000e08]:sd t6, 1504(ra)<br> [0x80000e0c]:sd t2, 1512(ra)<br>                               |
|  96|[0x80006408]<br>0x3FF5E8C530C17586<br> [0x80006410]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe004000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000e1c]:fsqrt.d t6, t5, dyn<br> [0x80000e20]:csrrs t2, fcsr, zero<br> [0x80000e24]:sd t6, 1520(ra)<br> [0x80000e28]:sd t2, 1528(ra)<br>                               |
|  97|[0x80006418]<br>0x3FF6A093162D3478<br> [0x80006420]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffe000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000e38]:fsqrt.d t6, t5, dyn<br> [0x80000e3c]:csrrs t2, fcsr, zero<br> [0x80000e40]:sd t6, 1536(ra)<br> [0x80000e44]:sd t2, 1544(ra)<br>                               |
|  98|[0x80006428]<br>0x3FF5E8B9817F2B33<br> [0x80006430]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe002000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000e54]:fsqrt.d t6, t5, dyn<br> [0x80000e58]:csrrs t2, fcsr, zero<br> [0x80000e5c]:sd t6, 1552(ra)<br> [0x80000e60]:sd t2, 1560(ra)<br>                               |
|  99|[0x80006438]<br>0x3FF6A098BE56ED28<br> [0x80006440]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffff000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000e70]:fsqrt.d t6, t5, dyn<br> [0x80000e74]:csrrs t2, fcsr, zero<br> [0x80000e78]:sd t6, 1568(ra)<br> [0x80000e7c]:sd t2, 1576(ra)<br>                               |
| 100|[0x80006448]<br>0x3FF5E8B3A9DBAFCC<br> [0x80006450]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe001000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000e8c]:fsqrt.d t6, t5, dyn<br> [0x80000e90]:csrrs t2, fcsr, zero<br> [0x80000e94]:sd t6, 1584(ra)<br> [0x80000e98]:sd t2, 1592(ra)<br>                               |
| 101|[0x80006458]<br>0x3FF6A09B926B41BB<br> [0x80006460]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffff800000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000ea8]:fsqrt.d t6, t5, dyn<br> [0x80000eac]:csrrs t2, fcsr, zero<br> [0x80000eb0]:sd t6, 1600(ra)<br> [0x80000eb4]:sd t2, 1608(ra)<br>                               |
| 102|[0x80006468]<br>0x3FF5E8B0BE095C88<br> [0x80006470]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000800000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000ec4]:fsqrt.d t6, t5, dyn<br> [0x80000ec8]:csrrs t2, fcsr, zero<br> [0x80000ecc]:sd t6, 1616(ra)<br> [0x80000ed0]:sd t2, 1624(ra)<br>                               |
| 103|[0x80006478]<br>0x3FF6A09CFC754A14<br> [0x80006480]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffc00000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000ee0]:fsqrt.d t6, t5, dyn<br> [0x80000ee4]:csrrs t2, fcsr, zero<br> [0x80000ee8]:sd t6, 1632(ra)<br> [0x80000eec]:sd t2, 1640(ra)<br>                               |
| 104|[0x80006488]<br>0x3FF5E8AF48200D82<br> [0x80006490]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000400000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000efc]:fsqrt.d t6, t5, dyn<br> [0x80000f00]:csrrs t2, fcsr, zero<br> [0x80000f04]:sd t6, 1648(ra)<br> [0x80000f08]:sd t2, 1656(ra)<br>                               |
| 105|[0x80006498]<br>0x3FF6A09DB17A45C5<br> [0x800064a0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffe00000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000f18]:fsqrt.d t6, t5, dyn<br> [0x80000f1c]:csrrs t2, fcsr, zero<br> [0x80000f20]:sd t6, 1664(ra)<br> [0x80000f24]:sd t2, 1672(ra)<br>                               |
| 106|[0x800064a8]<br>0x3FF5E8AE8D2B5CA6<br> [0x800064b0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000200000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000f34]:fsqrt.d t6, t5, dyn<br> [0x80000f38]:csrrs t2, fcsr, zero<br> [0x80000f3c]:sd t6, 1680(ra)<br> [0x80000f40]:sd t2, 1688(ra)<br>                               |
| 107|[0x800064b8]<br>0x3FF6A09E0BFCC17E<br> [0x800064c0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffff00000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000f50]:fsqrt.d t6, t5, dyn<br> [0x80000f54]:csrrs t2, fcsr, zero<br> [0x80000f58]:sd t6, 1696(ra)<br> [0x80000f5c]:sd t2, 1704(ra)<br>                               |
| 108|[0x800064c8]<br>0x3FF5E8AE2FB101E2<br> [0x800064d0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000100000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000f6c]:fsqrt.d t6, t5, dyn<br> [0x80000f70]:csrrs t2, fcsr, zero<br> [0x80000f74]:sd t6, 1712(ra)<br> [0x80000f78]:sd t2, 1720(ra)<br>                               |
| 109|[0x800064d8]<br>0x3FF6A09E393DFED2<br> [0x800064e0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffff80000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000f88]:fsqrt.d t6, t5, dyn<br> [0x80000f8c]:csrrs t2, fcsr, zero<br> [0x80000f90]:sd t6, 1728(ra)<br> [0x80000f94]:sd t2, 1736(ra)<br>                               |
| 110|[0x800064e8]<br>0x3FF5E8AE00F3D3EA<br> [0x800064f0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000080000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000fa4]:fsqrt.d t6, t5, dyn<br> [0x80000fa8]:csrrs t2, fcsr, zero<br> [0x80000fac]:sd t6, 1744(ra)<br> [0x80000fb0]:sd t2, 1752(ra)<br>                               |
| 111|[0x800064f8]<br>0x3FF6A09E4FDE9D5B<br> [0x80006500]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffc0000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000fc0]:fsqrt.d t6, t5, dyn<br> [0x80000fc4]:csrrs t2, fcsr, zero<br> [0x80000fc8]:sd t6, 1760(ra)<br> [0x80000fcc]:sd t2, 1768(ra)<br>                               |
| 112|[0x80006508]<br>0x3FF5E8ADE9953CC9<br> [0x80006510]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000040000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000fdc]:fsqrt.d t6, t5, dyn<br> [0x80000fe0]:csrrs t2, fcsr, zero<br> [0x80000fe4]:sd t6, 1776(ra)<br> [0x80000fe8]:sd t2, 1784(ra)<br>                               |
| 113|[0x80006518]<br>0x3FF6A09E5B2EEC96<br> [0x80006520]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffe0000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000ff8]:fsqrt.d t6, t5, dyn<br> [0x80000ffc]:csrrs t2, fcsr, zero<br> [0x80001000]:sd t6, 1792(ra)<br> [0x80001004]:sd t2, 1800(ra)<br>                               |
| 114|[0x80006528]<br>0x3FF5E8ADDDE5F12F<br> [0x80006530]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000020000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001014]:fsqrt.d t6, t5, dyn<br> [0x80001018]:csrrs t2, fcsr, zero<br> [0x8000101c]:sd t6, 1808(ra)<br> [0x80001020]:sd t2, 1816(ra)<br>                               |
| 115|[0x80006538]<br>0x3FF6A09E60D71432<br> [0x80006540]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffff0000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001030]:fsqrt.d t6, t5, dyn<br> [0x80001034]:csrrs t2, fcsr, zero<br> [0x80001038]:sd t6, 1824(ra)<br> [0x8000103c]:sd t2, 1832(ra)<br>                               |
| 116|[0x80006548]<br>0x3FF5E8ADD80E4B60<br> [0x80006550]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000010000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000104c]:fsqrt.d t6, t5, dyn<br> [0x80001050]:csrrs t2, fcsr, zero<br> [0x80001054]:sd t6, 1840(ra)<br> [0x80001058]:sd t2, 1848(ra)<br>                               |
| 117|[0x80006558]<br>0x3FF6A09E63AB2800<br> [0x80006560]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffff8000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001068]:fsqrt.d t6, t5, dyn<br> [0x8000106c]:csrrs t2, fcsr, zero<br> [0x80001070]:sd t6, 1856(ra)<br> [0x80001074]:sd t2, 1864(ra)<br>                               |
| 118|[0x80006568]<br>0x3FF5E8ADD5227878<br> [0x80006570]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000008000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001084]:fsqrt.d t6, t5, dyn<br> [0x80001088]:csrrs t2, fcsr, zero<br> [0x8000108c]:sd t6, 1872(ra)<br> [0x80001090]:sd t2, 1880(ra)<br>                               |
| 119|[0x80006578]<br>0x3FF6A09E651531E6<br> [0x80006580]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffc000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800010a0]:fsqrt.d t6, t5, dyn<br> [0x800010a4]:csrrs t2, fcsr, zero<br> [0x800010a8]:sd t6, 1888(ra)<br> [0x800010ac]:sd t2, 1896(ra)<br>                               |
| 120|[0x80006588]<br>0x3FF5E8ADD3AC8F03<br> [0x80006590]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000004000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800010bc]:fsqrt.d t6, t5, dyn<br> [0x800010c0]:csrrs t2, fcsr, zero<br> [0x800010c4]:sd t6, 1904(ra)<br> [0x800010c8]:sd t2, 1912(ra)<br>                               |
| 121|[0x80006598]<br>0x3FF6A09E65CA36D9<br> [0x800065a0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffe000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800010d8]:fsqrt.d t6, t5, dyn<br> [0x800010dc]:csrrs t2, fcsr, zero<br> [0x800010e0]:sd t6, 1920(ra)<br> [0x800010e4]:sd t2, 1928(ra)<br>                               |
| 122|[0x800065a8]<br>0x3FF5E8ADD2F19A49<br> [0x800065b0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000002000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800010f4]:fsqrt.d t6, t5, dyn<br> [0x800010f8]:csrrs t2, fcsr, zero<br> [0x800010fc]:sd t6, 1936(ra)<br> [0x80001100]:sd t2, 1944(ra)<br>                               |
| 123|[0x800065b8]<br>0x3FF6A09E6624B953<br> [0x800065c0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffff000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001110]:fsqrt.d t6, t5, dyn<br> [0x80001114]:csrrs t2, fcsr, zero<br> [0x80001118]:sd t6, 1952(ra)<br> [0x8000111c]:sd t2, 1960(ra)<br>                               |
| 124|[0x800065c8]<br>0x3FF5E8ADD2941FEC<br> [0x800065d0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000001000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000112c]:fsqrt.d t6, t5, dyn<br> [0x80001130]:csrrs t2, fcsr, zero<br> [0x80001134]:sd t6, 1968(ra)<br> [0x80001138]:sd t2, 1976(ra)<br>                               |
| 125|[0x800065d8]<br>0x3FF6A09E6651FA90<br> [0x800065e0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffff800000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001148]:fsqrt.d t6, t5, dyn<br> [0x8000114c]:csrrs t2, fcsr, zero<br> [0x80001150]:sd t6, 1984(ra)<br> [0x80001154]:sd t2, 1992(ra)<br>                               |
| 126|[0x800065e8]<br>0x3FF5E8ADD26562BD<br> [0x800065f0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000800000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001164]:fsqrt.d t6, t5, dyn<br> [0x80001168]:csrrs t2, fcsr, zero<br> [0x8000116c]:sd t6, 2000(ra)<br> [0x80001170]:sd t2, 2008(ra)<br>                               |
| 127|[0x800065f8]<br>0x3FF6A09E66689B2E<br> [0x80006600]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffffc00000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001180]:fsqrt.d t6, t5, dyn<br> [0x80001184]:csrrs t2, fcsr, zero<br> [0x80001188]:sd t6, 2016(ra)<br> [0x8000118c]:sd t2, 2024(ra)<br>                               |
| 128|[0x80006608]<br>0x3FF5E8ADD24E0426<br> [0x80006610]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000400000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000119c]:fsqrt.d t6, t5, dyn<br> [0x800011a0]:csrrs t2, fcsr, zero<br> [0x800011a4]:sd t6, 2032(ra)<br> [0x800011a8]:sd t2, 2040(ra)<br>                               |
| 129|[0x80006618]<br>0x3FF6A09E6673EB7D<br> [0x80006620]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffffe00000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800011b8]:fsqrt.d t6, t5, dyn<br> [0x800011bc]:csrrs t2, fcsr, zero<br> [0x800011c0]:addi ra, ra, 2040<br> [0x800011c4]:sd t6, 8(ra)<br> [0x800011c8]:sd t2, 16(ra)<br> |
| 130|[0x80006628]<br>0x3FF5E8ADD24254DB<br> [0x80006630]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000200000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800011d8]:fsqrt.d t6, t5, dyn<br> [0x800011dc]:csrrs t2, fcsr, zero<br> [0x800011e0]:sd t6, 24(ra)<br> [0x800011e4]:sd t2, 32(ra)<br>                                   |
| 131|[0x80006638]<br>0x3FF6A09E667993A5<br> [0x80006640]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffff00000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800011f4]:fsqrt.d t6, t5, dyn<br> [0x800011f8]:csrrs t2, fcsr, zero<br> [0x800011fc]:sd t6, 40(ra)<br> [0x80001200]:sd t2, 48(ra)<br>                                   |
| 132|[0x80006648]<br>0x3FF5E8ADD23C7D35<br> [0x80006650]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000100000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001210]:fsqrt.d t6, t5, dyn<br> [0x80001214]:csrrs t2, fcsr, zero<br> [0x80001218]:sd t6, 56(ra)<br> [0x8000121c]:sd t2, 64(ra)<br>                                   |
| 133|[0x80006658]<br>0x3FF6A09E667C67B9<br> [0x80006660]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffff80000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000122c]:fsqrt.d t6, t5, dyn<br> [0x80001230]:csrrs t2, fcsr, zero<br> [0x80001234]:sd t6, 72(ra)<br> [0x80001238]:sd t2, 80(ra)<br>                                   |
| 134|[0x80006668]<br>0x3FF5E8ADD2399162<br> [0x80006670]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000080000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001248]:fsqrt.d t6, t5, dyn<br> [0x8000124c]:csrrs t2, fcsr, zero<br> [0x80001250]:sd t6, 88(ra)<br> [0x80001254]:sd t2, 96(ra)<br>                                   |
| 135|[0x80006678]<br>0x3FF6A09E667DD1C3<br> [0x80006680]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffffc0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001264]:fsqrt.d t6, t5, dyn<br> [0x80001268]:csrrs t2, fcsr, zero<br> [0x8000126c]:sd t6, 104(ra)<br> [0x80001270]:sd t2, 112(ra)<br>                                 |
| 136|[0x80006688]<br>0x3FF5E8ADD2381B78<br> [0x80006690]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000040000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001280]:fsqrt.d t6, t5, dyn<br> [0x80001284]:csrrs t2, fcsr, zero<br> [0x80001288]:sd t6, 120(ra)<br> [0x8000128c]:sd t2, 128(ra)<br>                                 |
| 137|[0x80006698]<br>0x3FF6A09E667E86C8<br> [0x800066a0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffffe0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000129c]:fsqrt.d t6, t5, dyn<br> [0x800012a0]:csrrs t2, fcsr, zero<br> [0x800012a4]:sd t6, 136(ra)<br> [0x800012a8]:sd t2, 144(ra)<br>                                 |
| 138|[0x800066a8]<br>0x3FF5E8ADD2376084<br> [0x800066b0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000020000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800012b8]:fsqrt.d t6, t5, dyn<br> [0x800012bc]:csrrs t2, fcsr, zero<br> [0x800012c0]:sd t6, 152(ra)<br> [0x800012c4]:sd t2, 160(ra)<br>                                 |
| 139|[0x800066b8]<br>0x3FF6A09E667EE14A<br> [0x800066c0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffffff0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800012d4]:fsqrt.d t6, t5, dyn<br> [0x800012d8]:csrrs t2, fcsr, zero<br> [0x800012dc]:sd t6, 168(ra)<br> [0x800012e0]:sd t2, 176(ra)<br>                                 |
| 140|[0x800066c8]<br>0x3FF5E8ADD2370309<br> [0x800066d0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000010000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800012f0]:fsqrt.d t6, t5, dyn<br> [0x800012f4]:csrrs t2, fcsr, zero<br> [0x800012f8]:sd t6, 184(ra)<br> [0x800012fc]:sd t2, 192(ra)<br>                                 |
| 141|[0x800066d8]<br>0x3FF6A09E667F0E8B<br> [0x800066e0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffffff8000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000130c]:fsqrt.d t6, t5, dyn<br> [0x80001310]:csrrs t2, fcsr, zero<br> [0x80001314]:sd t6, 200(ra)<br> [0x80001318]:sd t2, 208(ra)<br>                                 |
| 142|[0x800066e8]<br>0x3FF5E8ADD236D44C<br> [0x800066f0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000008000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001328]:fsqrt.d t6, t5, dyn<br> [0x8000132c]:csrrs t2, fcsr, zero<br> [0x80001330]:sd t6, 216(ra)<br> [0x80001334]:sd t2, 224(ra)<br>                                 |
| 143|[0x800066f8]<br>0x3FF6A09E667F252C<br> [0x80006700]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffffffc000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001344]:fsqrt.d t6, t5, dyn<br> [0x80001348]:csrrs t2, fcsr, zero<br> [0x8000134c]:sd t6, 232(ra)<br> [0x80001350]:sd t2, 240(ra)<br>                                 |
| 144|[0x80006708]<br>0x3FF5E8ADD236BCEE<br> [0x80006710]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000004000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001360]:fsqrt.d t6, t5, dyn<br> [0x80001364]:csrrs t2, fcsr, zero<br> [0x80001368]:sd t6, 248(ra)<br> [0x8000136c]:sd t2, 256(ra)<br>                                 |
| 145|[0x80006718]<br>0x3FF6A09E667F307C<br> [0x80006720]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffffffe000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000137c]:fsqrt.d t6, t5, dyn<br> [0x80001380]:csrrs t2, fcsr, zero<br> [0x80001384]:sd t6, 264(ra)<br> [0x80001388]:sd t2, 272(ra)<br>                                 |
| 146|[0x80006728]<br>0x3FF5E8ADD236B13E<br> [0x80006730]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000002000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001398]:fsqrt.d t6, t5, dyn<br> [0x8000139c]:csrrs t2, fcsr, zero<br> [0x800013a0]:sd t6, 280(ra)<br> [0x800013a4]:sd t2, 288(ra)<br>                                 |
| 147|[0x80006738]<br>0x3FF6A09E667F3624<br> [0x80006740]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffffff000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800013b4]:fsqrt.d t6, t5, dyn<br> [0x800013b8]:csrrs t2, fcsr, zero<br> [0x800013bc]:sd t6, 296(ra)<br> [0x800013c0]:sd t2, 304(ra)<br>                                 |
| 148|[0x80006748]<br>0x3FF5E8ADD236AB67<br> [0x80006750]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000001000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800013d0]:fsqrt.d t6, t5, dyn<br> [0x800013d4]:csrrs t2, fcsr, zero<br> [0x800013d8]:sd t6, 312(ra)<br> [0x800013dc]:sd t2, 320(ra)<br>                                 |
| 149|[0x80006758]<br>0x3FF6A09E667F38F8<br> [0x80006760]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffffff800 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800013ec]:fsqrt.d t6, t5, dyn<br> [0x800013f0]:csrrs t2, fcsr, zero<br> [0x800013f4]:sd t6, 328(ra)<br> [0x800013f8]:sd t2, 336(ra)<br>                                 |
| 150|[0x80006768]<br>0x3FF5E8ADD236A87B<br> [0x80006770]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000000800 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001408]:fsqrt.d t6, t5, dyn<br> [0x8000140c]:csrrs t2, fcsr, zero<br> [0x80001410]:sd t6, 344(ra)<br> [0x80001414]:sd t2, 352(ra)<br>                                 |
| 151|[0x80006778]<br>0x3FF6A09E667F3A63<br> [0x80006780]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffffffc00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001424]:fsqrt.d t6, t5, dyn<br> [0x80001428]:csrrs t2, fcsr, zero<br> [0x8000142c]:sd t6, 360(ra)<br> [0x80001430]:sd t2, 368(ra)<br>                                 |
| 152|[0x80006788]<br>0x3FF5E8ADD236A705<br> [0x80006790]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000000400 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001440]:fsqrt.d t6, t5, dyn<br> [0x80001444]:csrrs t2, fcsr, zero<br> [0x80001448]:sd t6, 376(ra)<br> [0x8000144c]:sd t2, 384(ra)<br>                                 |
| 153|[0x80006798]<br>0x3FF6A09E667F3B18<br> [0x800067a0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffffffe00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000145c]:fsqrt.d t6, t5, dyn<br> [0x80001460]:csrrs t2, fcsr, zero<br> [0x80001464]:sd t6, 392(ra)<br> [0x80001468]:sd t2, 400(ra)<br>                                 |
| 154|[0x800067a8]<br>0x3FF5E8ADD236A64A<br> [0x800067b0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000000200 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001478]:fsqrt.d t6, t5, dyn<br> [0x8000147c]:csrrs t2, fcsr, zero<br> [0x80001480]:sd t6, 408(ra)<br> [0x80001484]:sd t2, 416(ra)<br>                                 |
| 155|[0x800067b8]<br>0x3FF6A09E667F3B72<br> [0x800067c0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffffffff00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001494]:fsqrt.d t6, t5, dyn<br> [0x80001498]:csrrs t2, fcsr, zero<br> [0x8000149c]:sd t6, 424(ra)<br> [0x800014a0]:sd t2, 432(ra)<br>                                 |
| 156|[0x800067c8]<br>0x3FF5E8ADD236A5EC<br> [0x800067d0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000000100 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800014b0]:fsqrt.d t6, t5, dyn<br> [0x800014b4]:csrrs t2, fcsr, zero<br> [0x800014b8]:sd t6, 440(ra)<br> [0x800014bc]:sd t2, 448(ra)<br>                                 |
| 157|[0x800067d8]<br>0x3FF6A09E667F3B9F<br> [0x800067e0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffffffff80 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800014cc]:fsqrt.d t6, t5, dyn<br> [0x800014d0]:csrrs t2, fcsr, zero<br> [0x800014d4]:sd t6, 456(ra)<br> [0x800014d8]:sd t2, 464(ra)<br>                                 |
| 158|[0x800067e8]<br>0x3FF5E8ADD236A5BE<br> [0x800067f0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000000080 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800014e8]:fsqrt.d t6, t5, dyn<br> [0x800014ec]:csrrs t2, fcsr, zero<br> [0x800014f0]:sd t6, 472(ra)<br> [0x800014f4]:sd t2, 480(ra)<br>                                 |
| 159|[0x800067f8]<br>0x3FF6A09E667F3BB6<br> [0x80006800]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffffffffc0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001504]:fsqrt.d t6, t5, dyn<br> [0x80001508]:csrrs t2, fcsr, zero<br> [0x8000150c]:sd t6, 488(ra)<br> [0x80001510]:sd t2, 496(ra)<br>                                 |
| 160|[0x80006808]<br>0x3FF5E8ADD236A5A6<br> [0x80006810]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000000040 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001520]:fsqrt.d t6, t5, dyn<br> [0x80001524]:csrrs t2, fcsr, zero<br> [0x80001528]:sd t6, 504(ra)<br> [0x8000152c]:sd t2, 512(ra)<br>                                 |
| 161|[0x80006818]<br>0x3FF6A09E667F3BC1<br> [0x80006820]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffffffffe0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000153c]:fsqrt.d t6, t5, dyn<br> [0x80001540]:csrrs t2, fcsr, zero<br> [0x80001544]:sd t6, 520(ra)<br> [0x80001548]:sd t2, 528(ra)<br>                                 |
| 162|[0x80006828]<br>0x3FF5E8ADD236A59B<br> [0x80006830]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000000020 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001558]:fsqrt.d t6, t5, dyn<br> [0x8000155c]:csrrs t2, fcsr, zero<br> [0x80001560]:sd t6, 536(ra)<br> [0x80001564]:sd t2, 544(ra)<br>                                 |
| 163|[0x80006838]<br>0x3FF6A09E667F3BC7<br> [0x80006840]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffffffff0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001574]:fsqrt.d t6, t5, dyn<br> [0x80001578]:csrrs t2, fcsr, zero<br> [0x8000157c]:sd t6, 552(ra)<br> [0x80001580]:sd t2, 560(ra)<br>                                 |
| 164|[0x80006848]<br>0x3FF5E8ADD236A595<br> [0x80006850]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000000010 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001590]:fsqrt.d t6, t5, dyn<br> [0x80001594]:csrrs t2, fcsr, zero<br> [0x80001598]:sd t6, 568(ra)<br> [0x8000159c]:sd t2, 576(ra)<br>                                 |
| 165|[0x80006858]<br>0x3FF6A09E667F3BCA<br> [0x80006860]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffffffff8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800015ac]:fsqrt.d t6, t5, dyn<br> [0x800015b0]:csrrs t2, fcsr, zero<br> [0x800015b4]:sd t6, 584(ra)<br> [0x800015b8]:sd t2, 592(ra)<br>                                 |
| 166|[0x80006868]<br>0x3FF5E8ADD236A592<br> [0x80006870]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000000008 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800015c8]:fsqrt.d t6, t5, dyn<br> [0x800015cc]:csrrs t2, fcsr, zero<br> [0x800015d0]:sd t6, 600(ra)<br> [0x800015d4]:sd t2, 608(ra)<br>                                 |
| 167|[0x80006878]<br>0x3FF6A09E667F3BCB<br> [0x80006880]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffffffffc and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800015e4]:fsqrt.d t6, t5, dyn<br> [0x800015e8]:csrrs t2, fcsr, zero<br> [0x800015ec]:sd t6, 616(ra)<br> [0x800015f0]:sd t2, 624(ra)<br>                                 |
| 168|[0x80006888]<br>0x3FF5E8ADD236A590<br> [0x80006890]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000000004 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001600]:fsqrt.d t6, t5, dyn<br> [0x80001604]:csrrs t2, fcsr, zero<br> [0x80001608]:sd t6, 632(ra)<br> [0x8000160c]:sd t2, 640(ra)<br>                                 |
| 169|[0x80006898]<br>0x3FF6A09E667F3BCC<br> [0x800068a0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffffffffe and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000161c]:fsqrt.d t6, t5, dyn<br> [0x80001620]:csrrs t2, fcsr, zero<br> [0x80001624]:sd t6, 648(ra)<br> [0x80001628]:sd t2, 656(ra)<br>                                 |
| 170|[0x800068a8]<br>0x3FF5E8ADD236A590<br> [0x800068b0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001638]:fsqrt.d t6, t5, dyn<br> [0x8000163c]:csrrs t2, fcsr, zero<br> [0x80001640]:sd t6, 664(ra)<br> [0x80001644]:sd t2, 672(ra)<br>                                 |
| 171|[0x800068b8]<br>0x3FF6A09E667F3BCC<br> [0x800068c0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001654]:fsqrt.d t6, t5, dyn<br> [0x80001658]:csrrs t2, fcsr, zero<br> [0x8000165c]:sd t6, 680(ra)<br> [0x80001660]:sd t2, 688(ra)<br>                                 |
| 172|[0x800068c8]<br>0x3BE6A09E667F3BCD<br> [0x800068d0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001670]:fsqrt.d t6, t5, dyn<br> [0x80001674]:csrrs t2, fcsr, zero<br> [0x80001678]:sd t6, 696(ra)<br> [0x8000167c]:sd t2, 704(ra)<br>                                 |
| 173|[0x800068d8]<br>0x3BEFFFFFFFFFFFFF<br> [0x800068e0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000168c]:fsqrt.d t6, t5, dyn<br> [0x80001690]:csrrs t2, fcsr, zero<br> [0x80001694]:sd t6, 712(ra)<br> [0x80001698]:sd t2, 720(ra)<br>                                 |
| 174|[0x800068e8]<br>0x3BEBB67AE8584CAA<br> [0x800068f0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800016a8]:fsqrt.d t6, t5, dyn<br> [0x800016ac]:csrrs t2, fcsr, zero<br> [0x800016b0]:sd t6, 728(ra)<br> [0x800016b4]:sd t2, 736(ra)<br>                                 |
| 175|[0x800068f8]<br>0x3BEBB67AE8584CAA<br> [0x80006900]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x7ffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800016c4]:fsqrt.d t6, t5, dyn<br> [0x800016c8]:csrrs t2, fcsr, zero<br> [0x800016cc]:sd t6, 744(ra)<br> [0x800016d0]:sd t2, 752(ra)<br>                                 |
| 176|[0x80006908]<br>0x3BEDEEEA11683F49<br> [0x80006910]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xc000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800016e0]:fsqrt.d t6, t5, dyn<br> [0x800016e4]:csrrs t2, fcsr, zero<br> [0x800016e8]:sd t6, 760(ra)<br> [0x800016ec]:sd t2, 768(ra)<br>                                 |
| 177|[0x80006918]<br>0x3BE94C583ADA5B52<br> [0x80006920]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x3ffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800016fc]:fsqrt.d t6, t5, dyn<br> [0x80001700]:csrrs t2, fcsr, zero<br> [0x80001704]:sd t6, 776(ra)<br> [0x80001708]:sd t2, 784(ra)<br>                                 |
| 178|[0x80006928]<br>0x3BEEFBDEB14F4EDA<br> [0x80006930]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xe000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001718]:fsqrt.d t6, t5, dyn<br> [0x8000171c]:csrrs t2, fcsr, zero<br> [0x80001720]:sd t6, 792(ra)<br> [0x80001724]:sd t2, 800(ra)<br>                                 |
| 179|[0x80006938]<br>0x3BE7FFFFFFFFFFFF<br> [0x80006940]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x1ffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001734]:fsqrt.d t6, t5, dyn<br> [0x80001738]:csrrs t2, fcsr, zero<br> [0x8000173c]:sd t6, 808(ra)<br> [0x80001740]:sd t2, 816(ra)<br>                                 |
| 180|[0x80006948]<br>0x3BEF7EFBEB8D4F12<br> [0x80006950]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xf000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001750]:fsqrt.d t6, t5, dyn<br> [0x80001754]:csrrs t2, fcsr, zero<br> [0x80001758]:sd t6, 824(ra)<br> [0x8000175c]:sd t2, 832(ra)<br>                                 |
| 181|[0x80006958]<br>0x3BE752E50DB3A3A1<br> [0x80006960]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x0ffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000176c]:fsqrt.d t6, t5, dyn<br> [0x80001770]:csrrs t2, fcsr, zero<br> [0x80001774]:sd t6, 840(ra)<br> [0x80001778]:sd t2, 848(ra)<br>                                 |
| 182|[0x80006968]<br>0x3BEFBFBF7EBC755F<br> [0x80006970]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xf800000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001788]:fsqrt.d t6, t5, dyn<br> [0x8000178c]:csrrs t2, fcsr, zero<br> [0x80001790]:sd t6, 856(ra)<br> [0x80001794]:sd t2, 864(ra)<br>                                 |
| 183|[0x80006978]<br>0x3BE6FA6EA162D0EF<br> [0x80006980]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x07fffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800017a4]:fsqrt.d t6, t5, dyn<br> [0x800017a8]:csrrs t2, fcsr, zero<br> [0x800017ac]:sd t6, 872(ra)<br> [0x800017b0]:sd t2, 880(ra)<br>                                 |
| 184|[0x80006988]<br>0x3BEFDFEFEFEBE3D6<br> [0x80006990]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xfc00000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800017c0]:fsqrt.d t6, t5, dyn<br> [0x800017c4]:csrrs t2, fcsr, zero<br> [0x800017c8]:sd t6, 888(ra)<br> [0x800017cc]:sd t2, 896(ra)<br>                                 |
| 185|[0x80006998]<br>0x3BE6CDB2BBB212EB<br> [0x800069a0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x03fffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800017dc]:fsqrt.d t6, t5, dyn<br> [0x800017e0]:csrrs t2, fcsr, zero<br> [0x800017e4]:sd t6, 904(ra)<br> [0x800017e8]:sd t2, 912(ra)<br>                                 |
| 186|[0x800069a8]<br>0x3BEFEFFBFDFEBF1F<br> [0x800069b0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xfe00000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800017f8]:fsqrt.d t6, t5, dyn<br> [0x800017fc]:csrrs t2, fcsr, zero<br> [0x80001800]:sd t6, 920(ra)<br> [0x80001804]:sd t2, 928(ra)<br>                                 |
| 187|[0x800069b8]<br>0x3BE6B733BFD8C647<br> [0x800069c0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x01fffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001814]:fsqrt.d t6, t5, dyn<br> [0x80001818]:csrrs t2, fcsr, zero<br> [0x8000181c]:sd t6, 936(ra)<br> [0x80001820]:sd t2, 944(ra)<br>                                 |
| 188|[0x800069c8]<br>0x3BEFF7FEFFBFEBF9<br> [0x800069d0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xff00000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001830]:fsqrt.d t6, t5, dyn<br> [0x80001834]:csrrs t2, fcsr, zero<br> [0x80001838]:sd t6, 952(ra)<br> [0x8000183c]:sd t2, 960(ra)<br>                                 |
| 189|[0x800069d8]<br>0x3BE6ABEBE307D6D8<br> [0x800069e0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x00fffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000184c]:fsqrt.d t6, t5, dyn<br> [0x80001850]:csrrs t2, fcsr, zero<br> [0x80001854]:sd t6, 968(ra)<br> [0x80001858]:sd t2, 976(ra)<br>                                 |
| 190|[0x800069e8]<br>0x3BEFFBFFBFF7FEC0<br> [0x800069f0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xff80000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001868]:fsqrt.d t6, t5, dyn<br> [0x8000186c]:csrrs t2, fcsr, zero<br> [0x80001870]:sd t6, 984(ra)<br> [0x80001874]:sd t2, 992(ra)<br>                                 |
| 191|[0x800069f8]<br>0x3BE6A645D9411B84<br> [0x80006a00]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x007ffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001884]:fsqrt.d t6, t5, dyn<br> [0x80001888]:csrrs t2, fcsr, zero<br> [0x8000188c]:sd t6, 1000(ra)<br> [0x80001890]:sd t2, 1008(ra)<br>                               |
| 192|[0x80006a08]<br>0x3BEFFDFFEFFEFFEC<br> [0x80006a10]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xffc0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800018a0]:fsqrt.d t6, t5, dyn<br> [0x800018a4]:csrrs t2, fcsr, zero<br> [0x800018a8]:sd t6, 1016(ra)<br> [0x800018ac]:sd t2, 1024(ra)<br>                               |
| 193|[0x80006a18]<br>0x3BE6A3724D10762C<br> [0x80006a20]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x003ffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800018bc]:fsqrt.d t6, t5, dyn<br> [0x800018c0]:csrrs t2, fcsr, zero<br> [0x800018c4]:sd t6, 1032(ra)<br> [0x800018c8]:sd t2, 1040(ra)<br>                               |
| 194|[0x80006618]<br>0x3BE6A09E667F3EA0<br> [0x80006620]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x00000000003ff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001fd8]:fsqrt.d t6, t5, dyn<br> [0x80001fdc]:csrrs t2, fcsr, zero<br> [0x80001fe0]:sd t6, 0(ra)<br> [0x80001fe4]:sd t2, 8(ra)<br>                                     |
| 195|[0x80006628]<br>0x3BEFFFFFFFFFFF00<br> [0x80006630]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xffffffffffe00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002004]:fsqrt.d t6, t5, dyn<br> [0x80002008]:csrrs t2, fcsr, zero<br> [0x8000200c]:sd t6, 16(ra)<br> [0x80002010]:sd t2, 24(ra)<br>                                   |
| 196|[0x80006638]<br>0x3BE6A09E667F3D36<br> [0x80006640]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x00000000001ff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002030]:fsqrt.d t6, t5, dyn<br> [0x80002034]:csrrs t2, fcsr, zero<br> [0x80002038]:sd t6, 32(ra)<br> [0x8000203c]:sd t2, 40(ra)<br>                                   |
| 197|[0x80006648]<br>0x3BEFFFFFFFFFFF80<br> [0x80006650]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xfffffffffff00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000205c]:fsqrt.d t6, t5, dyn<br> [0x80002060]:csrrs t2, fcsr, zero<br> [0x80002064]:sd t6, 48(ra)<br> [0x80002068]:sd t2, 56(ra)<br>                                   |
| 198|[0x80006658]<br>0x3BE6A09E667F3C81<br> [0x80006660]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x00000000000ff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002088]:fsqrt.d t6, t5, dyn<br> [0x8000208c]:csrrs t2, fcsr, zero<br> [0x80002090]:sd t6, 64(ra)<br> [0x80002094]:sd t2, 72(ra)<br>                                   |
| 199|[0x80006668]<br>0x3BEFFFFFFFFFFFC0<br> [0x80006670]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xfffffffffff80 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800020b4]:fsqrt.d t6, t5, dyn<br> [0x800020b8]:csrrs t2, fcsr, zero<br> [0x800020bc]:sd t6, 80(ra)<br> [0x800020c0]:sd t2, 88(ra)<br>                                   |
| 200|[0x80006678]<br>0x3BE6A09E667F3C26<br> [0x80006680]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x000000000007f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800020e0]:fsqrt.d t6, t5, dyn<br> [0x800020e4]:csrrs t2, fcsr, zero<br> [0x800020e8]:sd t6, 96(ra)<br> [0x800020ec]:sd t2, 104(ra)<br>                                  |
| 201|[0x80006688]<br>0x3BEFFFFFFFFFFFE0<br> [0x80006690]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xfffffffffffc0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000210c]:fsqrt.d t6, t5, dyn<br> [0x80002110]:csrrs t2, fcsr, zero<br> [0x80002114]:sd t6, 112(ra)<br> [0x80002118]:sd t2, 120(ra)<br>                                 |
| 202|[0x80006698]<br>0x3BE6A09E667F3BF9<br> [0x800066a0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x000000000003f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002138]:fsqrt.d t6, t5, dyn<br> [0x8000213c]:csrrs t2, fcsr, zero<br> [0x80002140]:sd t6, 128(ra)<br> [0x80002144]:sd t2, 136(ra)<br>                                 |
| 203|[0x800066a8]<br>0x3BEFFFFFFFFFFFF0<br> [0x800066b0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xfffffffffffe0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002164]:fsqrt.d t6, t5, dyn<br> [0x80002168]:csrrs t2, fcsr, zero<br> [0x8000216c]:sd t6, 144(ra)<br> [0x80002170]:sd t2, 152(ra)<br>                                 |
| 204|[0x800066b8]<br>0x3BE6A09E667F3BE2<br> [0x800066c0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x000000000001f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002190]:fsqrt.d t6, t5, dyn<br> [0x80002194]:csrrs t2, fcsr, zero<br> [0x80002198]:sd t6, 160(ra)<br> [0x8000219c]:sd t2, 168(ra)<br>                                 |
| 205|[0x800066c8]<br>0x3BEFFFFFFFFFFFF8<br> [0x800066d0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xffffffffffff0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800021bc]:fsqrt.d t6, t5, dyn<br> [0x800021c0]:csrrs t2, fcsr, zero<br> [0x800021c4]:sd t6, 176(ra)<br> [0x800021c8]:sd t2, 184(ra)<br>                                 |
| 206|[0x800066d8]<br>0x3BE6A09E667F3BD7<br> [0x800066e0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x000000000000f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800021e8]:fsqrt.d t6, t5, dyn<br> [0x800021ec]:csrrs t2, fcsr, zero<br> [0x800021f0]:sd t6, 192(ra)<br> [0x800021f4]:sd t2, 200(ra)<br>                                 |
| 207|[0x800066e8]<br>0x3BEFFFFFFFFFFFFC<br> [0x800066f0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xffffffffffff8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002214]:fsqrt.d t6, t5, dyn<br> [0x80002218]:csrrs t2, fcsr, zero<br> [0x8000221c]:sd t6, 208(ra)<br> [0x80002220]:sd t2, 216(ra)<br>                                 |
| 208|[0x800066f8]<br>0x3BE6A09E667F3BD2<br> [0x80006700]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x0000000000007 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002240]:fsqrt.d t6, t5, dyn<br> [0x80002244]:csrrs t2, fcsr, zero<br> [0x80002248]:sd t6, 224(ra)<br> [0x8000224c]:sd t2, 232(ra)<br>                                 |
| 209|[0x80006708]<br>0x3BEFFFFFFFFFFFFE<br> [0x80006710]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xffffffffffffc and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000226c]:fsqrt.d t6, t5, dyn<br> [0x80002270]:csrrs t2, fcsr, zero<br> [0x80002274]:sd t6, 240(ra)<br> [0x80002278]:sd t2, 248(ra)<br>                                 |
| 210|[0x80006718]<br>0x3BE6A09E667F3BCF<br> [0x80006720]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x0000000000003 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002298]:fsqrt.d t6, t5, dyn<br> [0x8000229c]:csrrs t2, fcsr, zero<br> [0x800022a0]:sd t6, 256(ra)<br> [0x800022a4]:sd t2, 264(ra)<br>                                 |
| 211|[0x80006728]<br>0x3BEFFFFFFFFFFFFF<br> [0x80006730]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xffffffffffffe and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800022c4]:fsqrt.d t6, t5, dyn<br> [0x800022c8]:csrrs t2, fcsr, zero<br> [0x800022cc]:sd t6, 272(ra)<br> [0x800022d0]:sd t2, 280(ra)<br>                                 |
| 212|[0x80006738]<br>0x3BE6A09E667F3BCD<br> [0x80006740]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800022f0]:fsqrt.d t6, t5, dyn<br> [0x800022f4]:csrrs t2, fcsr, zero<br> [0x800022f8]:sd t6, 288(ra)<br> [0x800022fc]:sd t2, 296(ra)<br>                                 |
| 213|[0x80006748]<br>0x43F5E8ADD236A58F<br> [0x80006750]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xe000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000231c]:fsqrt.d t6, t5, dyn<br> [0x80002320]:csrrs t2, fcsr, zero<br> [0x80002324]:sd t6, 304(ra)<br> [0x80002328]:sd t2, 312(ra)<br>                                 |
| 214|[0x80006758]<br>0x43F52A7FA9D2F8EA<br> [0x80006760]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002348]:fsqrt.d t6, t5, dyn<br> [0x8000234c]:csrrs t2, fcsr, zero<br> [0x80002350]:sd t6, 320(ra)<br> [0x80002354]:sd t2, 328(ra)<br>                                 |
| 215|[0x80006768]<br>0x43F58A68A4A8D9F3<br> [0x80006770]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xd000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002374]:fsqrt.d t6, t5, dyn<br> [0x80002378]:csrrs t2, fcsr, zero<br> [0x8000237c]:sd t6, 336(ra)<br> [0x80002380]:sd t2, 344(ra)<br>                                 |
| 216|[0x80006778]<br>0x43F55AAA002A9D5A<br> [0x80006780]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc800000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800023a0]:fsqrt.d t6, t5, dyn<br> [0x800023a4]:csrrs t2, fcsr, zero<br> [0x800023a8]:sd t6, 352(ra)<br> [0x800023ac]:sd t2, 360(ra)<br>                                 |
| 217|[0x80006788]<br>0x43F5B9BE5D52A9DA<br> [0x80006790]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xd800000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800023cc]:fsqrt.d t6, t5, dyn<br> [0x800023d0]:csrrs t2, fcsr, zero<br> [0x800023d4]:sd t6, 368(ra)<br> [0x800023d8]:sd t2, 376(ra)<br>                                 |
| 218|[0x80006798]<br>0x43F542A278D2D036<br> [0x800067a0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc400000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800023f8]:fsqrt.d t6, t5, dyn<br> [0x800023fc]:csrrs t2, fcsr, zero<br> [0x80002400]:sd t6, 384(ra)<br> [0x80002404]:sd t2, 392(ra)<br>                                 |
| 219|[0x800067a8]<br>0x43F5D142B6DBADC5<br> [0x800067b0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdc00000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002424]:fsqrt.d t6, t5, dyn<br> [0x80002428]:csrrs t2, fcsr, zero<br> [0x8000242c]:sd t6, 400(ra)<br> [0x80002430]:sd t2, 408(ra)<br>                                 |
| 220|[0x800067b8]<br>0x43F5369480174810<br> [0x800067c0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc200000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002450]:fsqrt.d t6, t5, dyn<br> [0x80002454]:csrrs t2, fcsr, zero<br> [0x80002458]:sd t6, 416(ra)<br> [0x8000245c]:sd t2, 424(ra)<br>                                 |
| 221|[0x800067c8]<br>0x43F5DCFB673B05DF<br> [0x800067d0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xde00000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000247c]:fsqrt.d t6, t5, dyn<br> [0x80002480]:csrrs t2, fcsr, zero<br> [0x80002484]:sd t6, 432(ra)<br> [0x80002488]:sd t2, 440(ra)<br>                                 |
| 222|[0x800067d8]<br>0x43F5308AF161F4A5<br> [0x800067e0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc100000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800024a8]:fsqrt.d t6, t5, dyn<br> [0x800024ac]:csrrs t2, fcsr, zero<br> [0x800024b0]:sd t6, 448(ra)<br> [0x800024b4]:sd t2, 456(ra)<br>                                 |
| 223|[0x800067e8]<br>0x43F5E2D564C44CA1<br> [0x800067f0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdf00000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800024d4]:fsqrt.d t6, t5, dyn<br> [0x800024d8]:csrrs t2, fcsr, zero<br> [0x800024dc]:sd t6, 464(ra)<br> [0x800024e0]:sd t2, 472(ra)<br>                                 |
| 224|[0x800067f8]<br>0x43F52D8584CD4081<br> [0x80006800]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc080000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002500]:fsqrt.d t6, t5, dyn<br> [0x80002504]:csrrs t2, fcsr, zero<br> [0x80002508]:sd t6, 480(ra)<br> [0x8000250c]:sd t2, 488(ra)<br>                                 |
| 225|[0x80006808]<br>0x43F5E5C1CD6C4E54<br> [0x80006810]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdf80000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000252c]:fsqrt.d t6, t5, dyn<br> [0x80002530]:csrrs t2, fcsr, zero<br> [0x80002534]:sd t6, 496(ra)<br> [0x80002538]:sd t2, 504(ra)<br>                                 |
| 226|[0x80006818]<br>0x43F52C02A51FC391<br> [0x80006820]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc040000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002558]:fsqrt.d t6, t5, dyn<br> [0x8000255c]:csrrs t2, fcsr, zero<br> [0x80002560]:sd t6, 512(ra)<br> [0x80002564]:sd t2, 520(ra)<br>                                 |
| 227|[0x80006828]<br>0x43F5E737DC4AAFA7<br> [0x80006830]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdfc0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002584]:fsqrt.d t6, t5, dyn<br> [0x80002588]:csrrs t2, fcsr, zero<br> [0x8000258c]:sd t6, 528(ra)<br> [0x80002590]:sd t2, 536(ra)<br>                                 |
| 228|[0x80006838]<br>0x43F52B412AEDA69F<br> [0x80006840]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc020000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800025b0]:fsqrt.d t6, t5, dyn<br> [0x800025b4]:csrrs t2, fcsr, zero<br> [0x800025b8]:sd t6, 544(ra)<br> [0x800025bc]:sd t2, 552(ra)<br>                                 |
| 229|[0x80006848]<br>0x43F5E7F2DA5EA82C<br> [0x80006850]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdfe0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800025dc]:fsqrt.d t6, t5, dyn<br> [0x800025e0]:csrrs t2, fcsr, zero<br> [0x800025e4]:sd t6, 560(ra)<br> [0x800025e8]:sd t2, 568(ra)<br>                                 |
| 230|[0x80006858]<br>0x43F52AE06B3D6DB4<br> [0x80006860]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc010000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002608]:fsqrt.d t6, t5, dyn<br> [0x8000260c]:csrrs t2, fcsr, zero<br> [0x80002610]:sd t6, 576(ra)<br> [0x80002614]:sd t2, 584(ra)<br>                                 |
| 231|[0x80006868]<br>0x43F5E85057121C48<br> [0x80006870]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdff0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002634]:fsqrt.d t6, t5, dyn<br> [0x80002638]:csrrs t2, fcsr, zero<br> [0x8000263c]:sd t6, 592(ra)<br> [0x80002640]:sd t2, 600(ra)<br>                                 |
| 232|[0x80006878]<br>0x43F52AB00ABF7C46<br> [0x80006880]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc008000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002660]:fsqrt.d t6, t5, dyn<br> [0x80002664]:csrrs t2, fcsr, zero<br> [0x80002668]:sd t6, 608(ra)<br> [0x8000266c]:sd t2, 616(ra)<br>                                 |
| 233|[0x80006888]<br>0x43F5E87F14D63D07<br> [0x80006890]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdff8000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000268c]:fsqrt.d t6, t5, dyn<br> [0x80002690]:csrrs t2, fcsr, zero<br> [0x80002694]:sd t6, 624(ra)<br> [0x80002698]:sd t2, 632(ra)<br>                                 |
| 234|[0x80006898]<br>0x43F52A97DA570D05<br> [0x800068a0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc004000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800026b8]:fsqrt.d t6, t5, dyn<br> [0x800026bc]:csrrs t2, fcsr, zero<br> [0x800026c0]:sd t6, 640(ra)<br> [0x800026c4]:sd t2, 648(ra)<br>                                 |
| 235|[0x800068a8]<br>0x43F5E8967392E82A<br> [0x800068b0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdffc000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800026e4]:fsqrt.d t6, t5, dyn<br> [0x800026e8]:csrrs t2, fcsr, zero<br> [0x800026ec]:sd t6, 656(ra)<br> [0x800026f0]:sd t2, 664(ra)<br>                                 |
| 236|[0x800068b8]<br>0x43F52A8BC2187799<br> [0x800068c0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc002000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002710]:fsqrt.d t6, t5, dyn<br> [0x80002714]:csrrs t2, fcsr, zero<br> [0x80002718]:sd t6, 672(ra)<br> [0x8000271c]:sd t2, 680(ra)<br>                                 |
| 237|[0x800068c8]<br>0x43F5E8A222E7E48F<br> [0x800068d0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdffe000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000273c]:fsqrt.d t6, t5, dyn<br> [0x80002740]:csrrs t2, fcsr, zero<br> [0x80002744]:sd t6, 688(ra)<br> [0x80002748]:sd t2, 696(ra)<br>                                 |
| 238|[0x800068d8]<br>0x43F52A85B5F6956A<br> [0x800068e0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc001000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002768]:fsqrt.d t6, t5, dyn<br> [0x8000276c]:csrrs t2, fcsr, zero<br> [0x80002770]:sd t6, 704(ra)<br> [0x80002774]:sd t2, 712(ra)<br>                                 |
| 239|[0x800068e8]<br>0x43F5E8A7FA900C7B<br> [0x800068f0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdfff000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002794]:fsqrt.d t6, t5, dyn<br> [0x80002798]:csrrs t2, fcsr, zero<br> [0x8000279c]:sd t6, 720(ra)<br> [0x800027a0]:sd t2, 728(ra)<br>                                 |
| 240|[0x800068f8]<br>0x43F52A82AFE4FE74<br> [0x80006900]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc000800000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800027c0]:fsqrt.d t6, t5, dyn<br> [0x800027c4]:csrrs t2, fcsr, zero<br> [0x800027c8]:sd t6, 736(ra)<br> [0x800027cc]:sd t2, 744(ra)<br>                                 |
| 241|[0x80006908]<br>0x43F5E8AAE6638AE0<br> [0x80006910]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdfff800000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800027ec]:fsqrt.d t6, t5, dyn<br> [0x800027f0]:csrrs t2, fcsr, zero<br> [0x800027f4]:sd t6, 752(ra)<br> [0x800027f8]:sd t2, 760(ra)<br>                                 |
| 242|[0x80006918]<br>0x43F52A812CDC0982<br> [0x80006920]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc000400000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002818]:fsqrt.d t6, t5, dyn<br> [0x8000281c]:csrrs t2, fcsr, zero<br> [0x80002820]:sd t6, 768(ra)<br> [0x80002824]:sd t2, 776(ra)<br>                                 |
| 243|[0x80006928]<br>0x43F5E8AC5C4D24AE<br> [0x80006930]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdfffc00000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002844]:fsqrt.d t6, t5, dyn<br> [0x80002848]:csrrs t2, fcsr, zero<br> [0x8000284c]:sd t6, 784(ra)<br> [0x80002850]:sd t2, 792(ra)<br>                                 |
| 244|[0x80006938]<br>0x43F52A806B5784AA<br> [0x80006940]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc000200000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002870]:fsqrt.d t6, t5, dyn<br> [0x80002874]:csrrs t2, fcsr, zero<br> [0x80002878]:sd t6, 800(ra)<br> [0x8000287c]:sd t2, 808(ra)<br>                                 |
| 245|[0x80006948]<br>0x43F5E8AD1741E83C<br> [0x80006950]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdfffe00000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000289c]:fsqrt.d t6, t5, dyn<br> [0x800028a0]:csrrs t2, fcsr, zero<br> [0x800028a4]:sd t6, 816(ra)<br> [0x800028a8]:sd t2, 824(ra)<br>                                 |
| 246|[0x80006958]<br>0x43F52A800A953FA7<br> [0x80006960]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc000100000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800028c8]:fsqrt.d t6, t5, dyn<br> [0x800028cc]:csrrs t2, fcsr, zero<br> [0x800028d0]:sd t6, 832(ra)<br> [0x800028d4]:sd t2, 840(ra)<br>                                 |
| 247|[0x80006968]<br>0x43F5E8AD74BC47AD<br> [0x80006970]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdffff00000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800028f4]:fsqrt.d t6, t5, dyn<br> [0x800028f8]:csrrs t2, fcsr, zero<br> [0x800028fc]:sd t6, 848(ra)<br> [0x80002900]:sd t2, 856(ra)<br>                                 |
| 248|[0x80006978]<br>0x43F52A7FDA341C80<br> [0x80006980]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc000080000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002920]:fsqrt.d t6, t5, dyn<br> [0x80002924]:csrrs t2, fcsr, zero<br> [0x80002928]:sd t6, 864(ra)<br> [0x8000292c]:sd t2, 872(ra)<br>                                 |
| 249|[0x80006988]<br>0x43F5E8ADA37976D0<br> [0x80006990]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdffff80000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000294c]:fsqrt.d t6, t5, dyn<br> [0x80002950]:csrrs t2, fcsr, zero<br> [0x80002954]:sd t6, 880(ra)<br> [0x80002958]:sd t2, 888(ra)<br>                                 |
| 250|[0x80006998]<br>0x43F52A7FC2038AC3<br> [0x800069a0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc000040000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002978]:fsqrt.d t6, t5, dyn<br> [0x8000297c]:csrrs t2, fcsr, zero<br> [0x80002980]:sd t6, 896(ra)<br> [0x80002984]:sd t2, 904(ra)<br>                                 |
| 251|[0x800069a8]<br>0x43F5E8ADBAD80E3C<br> [0x800069b0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdffffc0000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800029a4]:fsqrt.d t6, t5, dyn<br> [0x800029a8]:csrrs t2, fcsr, zero<br> [0x800029ac]:sd t6, 912(ra)<br> [0x800029b0]:sd t2, 920(ra)<br>                                 |
| 252|[0x800069b8]<br>0x43F52A7FB5EB41DA<br> [0x800069c0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc000020000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800029d0]:fsqrt.d t6, t5, dyn<br> [0x800029d4]:csrrs t2, fcsr, zero<br> [0x800029d8]:sd t6, 928(ra)<br> [0x800029dc]:sd t2, 936(ra)<br>                                 |
| 253|[0x800069c8]<br>0x43F5E8ADC68759E9<br> [0x800069d0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdffffe0000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800029fc]:fsqrt.d t6, t5, dyn<br> [0x80002a00]:csrrs t2, fcsr, zero<br> [0x80002a04]:sd t6, 944(ra)<br> [0x80002a08]:sd t2, 952(ra)<br>                                 |
| 254|[0x800069d8]<br>0x43F52A7FAFDF1D63<br> [0x800069e0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc000010000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002a28]:fsqrt.d t6, t5, dyn<br> [0x80002a2c]:csrrs t2, fcsr, zero<br> [0x80002a30]:sd t6, 960(ra)<br> [0x80002a34]:sd t2, 968(ra)<br>                                 |
| 255|[0x800069e8]<br>0x43F5E8ADCC5EFFBD<br> [0x800069f0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdfffff0000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002a54]:fsqrt.d t6, t5, dyn<br> [0x80002a58]:csrrs t2, fcsr, zero<br> [0x80002a5c]:sd t6, 976(ra)<br> [0x80002a60]:sd t2, 984(ra)<br>                                 |
| 256|[0x800069f8]<br>0x43F52A7FACD90B26<br> [0x80006a00]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc000008000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002a80]:fsqrt.d t6, t5, dyn<br> [0x80002a84]:csrrs t2, fcsr, zero<br> [0x80002a88]:sd t6, 992(ra)<br> [0x80002a8c]:sd t2, 1000(ra)<br>                                |
| 257|[0x80006a08]<br>0x43F5E8ADCF4AD2A6<br> [0x80006a10]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdfffff8000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002aac]:fsqrt.d t6, t5, dyn<br> [0x80002ab0]:csrrs t2, fcsr, zero<br> [0x80002ab4]:sd t6, 1008(ra)<br> [0x80002ab8]:sd t2, 1016(ra)<br>                               |
| 258|[0x80006a18]<br>0x43F52A7FAB560208<br> [0x80006a20]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc000004000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002ad8]:fsqrt.d t6, t5, dyn<br> [0x80002adc]:csrrs t2, fcsr, zero<br> [0x80002ae0]:sd t6, 1024(ra)<br> [0x80002ae4]:sd t2, 1032(ra)<br>                               |
