
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000550')]      |
| SIG_REGION                | [('0x80002210', '0x800022a0', '18 dwords')]      |
| COV_LABELS                | fsqrt.d_b5      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV64Zdinx/work-fsqrt1/fsqrt.d_b5-01.S/ref.S    |
| Total Number of coverpoints| 38     |
| Total Coverpoints Hit     | 38      |
| Total Signature Updates   | 18      |
| STAT1                     | 9      |
| STAT2                     | 0      |
| STAT3                     | 6     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x800004b4]:fsqrt.d a2, a0, dyn
[0x800004b8]:csrrs t2, fcsr, zero
[0x800004bc]:sd a2, 144(ra)
[0x800004c0]:sd t2, 152(ra)
[0x800004c4]:ld a2, 80(t0)
[0x800004c8]:addi gp, zero, 0
[0x800004cc]:csrrw zero, fcsr, gp
[0x800004d0]:fsqrt.d a0, a2, dyn
[0x800004d4]:csrrs t2, fcsr, zero
[0x800004d8]:sd a0, 160(ra)
[0x800004dc]:sd t2, 168(ra)
[0x800004e0]:ld t1, 88(t0)
[0x800004e4]:addi gp, zero, 0
[0x800004e8]:csrrw zero, fcsr, gp
[0x800004ec]:fsqrt.d fp, t1, dyn
[0x800004f0]:csrrs t2, fcsr, zero
[0x800004f4]:sd fp, 176(ra)
[0x800004f8]:sd t2, 184(ra)
[0x800004fc]:ld fp, 96(t0)
[0x80000500]:addi gp, zero, 0
[0x80000504]:csrrw zero, fcsr, gp
[0x80000508]:fsqrt.d t1, fp, dyn
[0x8000050c]:csrrs t2, fcsr, zero
[0x80000510]:sd t1, 192(ra)
[0x80000514]:sd t2, 200(ra)
[0x80000518]:ld sp, 104(t0)
[0x8000051c]:addi gp, zero, 0
[0x80000520]:csrrw zero, fcsr, gp
[0x80000524]:fsqrt.d tp, sp, dyn
[0x80000528]:csrrs t2, fcsr, zero
[0x8000052c]:sd tp, 208(ra)
[0x80000530]:sd t2, 216(ra)
[0x80000534]:ld tp, 112(t0)
[0x80000538]:addi gp, zero, 0
[0x8000053c]:csrrw zero, fcsr, gp
[0x80000540]:fsqrt.d sp, tp, dyn
[0x80000544]:csrrs t2, fcsr, zero
[0x80000548]:sd sp, 224(ra)
[0x8000054c]:sd t2, 232(ra)

[0x800004d0]:fsqrt.d a0, a2, dyn
[0x800004d4]:csrrs t2, fcsr, zero
[0x800004d8]:sd a0, 160(ra)
[0x800004dc]:sd t2, 168(ra)
[0x800004e0]:ld t1, 88(t0)
[0x800004e4]:addi gp, zero, 0
[0x800004e8]:csrrw zero, fcsr, gp
[0x800004ec]:fsqrt.d fp, t1, dyn
[0x800004f0]:csrrs t2, fcsr, zero
[0x800004f4]:sd fp, 176(ra)
[0x800004f8]:sd t2, 184(ra)
[0x800004fc]:ld fp, 96(t0)
[0x80000500]:addi gp, zero, 0
[0x80000504]:csrrw zero, fcsr, gp
[0x80000508]:fsqrt.d t1, fp, dyn
[0x8000050c]:csrrs t2, fcsr, zero
[0x80000510]:sd t1, 192(ra)
[0x80000514]:sd t2, 200(ra)
[0x80000518]:ld sp, 104(t0)
[0x8000051c]:addi gp, zero, 0
[0x80000520]:csrrw zero, fcsr, gp
[0x80000524]:fsqrt.d tp, sp, dyn
[0x80000528]:csrrs t2, fcsr, zero
[0x8000052c]:sd tp, 208(ra)
[0x80000530]:sd t2, 216(ra)
[0x80000534]:ld tp, 112(t0)
[0x80000538]:addi gp, zero, 0
[0x8000053c]:csrrw zero, fcsr, gp
[0x80000540]:fsqrt.d sp, tp, dyn
[0x80000544]:csrrs t2, fcsr, zero
[0x80000548]:sd sp, 224(ra)
[0x8000054c]:sd t2, 232(ra)

[0x800004ec]:fsqrt.d fp, t1, dyn
[0x800004f0]:csrrs t2, fcsr, zero
[0x800004f4]:sd fp, 176(ra)
[0x800004f8]:sd t2, 184(ra)
[0x800004fc]:ld fp, 96(t0)
[0x80000500]:addi gp, zero, 0
[0x80000504]:csrrw zero, fcsr, gp
[0x80000508]:fsqrt.d t1, fp, dyn
[0x8000050c]:csrrs t2, fcsr, zero
[0x80000510]:sd t1, 192(ra)
[0x80000514]:sd t2, 200(ra)
[0x80000518]:ld sp, 104(t0)
[0x8000051c]:addi gp, zero, 0
[0x80000520]:csrrw zero, fcsr, gp
[0x80000524]:fsqrt.d tp, sp, dyn
[0x80000528]:csrrs t2, fcsr, zero
[0x8000052c]:sd tp, 208(ra)
[0x80000530]:sd t2, 216(ra)
[0x80000534]:ld tp, 112(t0)
[0x80000538]:addi gp, zero, 0
[0x8000053c]:csrrw zero, fcsr, gp
[0x80000540]:fsqrt.d sp, tp, dyn
[0x80000544]:csrrs t2, fcsr, zero
[0x80000548]:sd sp, 224(ra)
[0x8000054c]:sd t2, 232(ra)

[0x80000508]:fsqrt.d t1, fp, dyn
[0x8000050c]:csrrs t2, fcsr, zero
[0x80000510]:sd t1, 192(ra)
[0x80000514]:sd t2, 200(ra)
[0x80000518]:ld sp, 104(t0)
[0x8000051c]:addi gp, zero, 0
[0x80000520]:csrrw zero, fcsr, gp
[0x80000524]:fsqrt.d tp, sp, dyn
[0x80000528]:csrrs t2, fcsr, zero
[0x8000052c]:sd tp, 208(ra)
[0x80000530]:sd t2, 216(ra)
[0x80000534]:ld tp, 112(t0)
[0x80000538]:addi gp, zero, 0
[0x8000053c]:csrrw zero, fcsr, gp
[0x80000540]:fsqrt.d sp, tp, dyn
[0x80000544]:csrrs t2, fcsr, zero
[0x80000548]:sd sp, 224(ra)
[0x8000054c]:sd t2, 232(ra)

[0x80000524]:fsqrt.d tp, sp, dyn
[0x80000528]:csrrs t2, fcsr, zero
[0x8000052c]:sd tp, 208(ra)
[0x80000530]:sd t2, 216(ra)
[0x80000534]:ld tp, 112(t0)
[0x80000538]:addi gp, zero, 0
[0x8000053c]:csrrw zero, fcsr, gp
[0x80000540]:fsqrt.d sp, tp, dyn
[0x80000544]:csrrs t2, fcsr, zero
[0x80000548]:sd sp, 224(ra)
[0x8000054c]:sd t2, 232(ra)

[0x80000540]:fsqrt.d sp, tp, dyn
[0x80000544]:csrrs t2, fcsr, zero
[0x80000548]:sd sp, 224(ra)
[0x8000054c]:sd t2, 232(ra)



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                                  signature                                   |                                                                                 coverpoints                                                                                 |                                                                    code                                                                    |
|---:|------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002218]<br>0x0000000000000000<br> [0x80002220]<br>0x0000000000000000<br> |- mnemonic : fsqrt.d<br> - rs1 : x28<br> - rd : x30<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x800003b8]:fsqrt.d t5, t3, dyn<br> [0x800003bc]:csrrs t2, fcsr, zero<br> [0x800003c0]:sd t5, 0(ra)<br> [0x800003c4]:sd t2, 8(ra)<br>      |
|   2|[0x80002228]<br>0x0000000000000000<br> [0x80002230]<br>0x0000000000000020<br> |- rs1 : x26<br> - rd : x26<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                         |[0x800003d4]:fsqrt.d s10, s10, dyn<br> [0x800003d8]:csrrs t2, fcsr, zero<br> [0x800003dc]:sd s10, 16(ra)<br> [0x800003e0]:sd t2, 24(ra)<br> |
|   3|[0x80002238]<br>0x0000000000000000<br> [0x80002240]<br>0x0000000000000040<br> |- rs1 : x30<br> - rd : x28<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                         |[0x800003f0]:fsqrt.d t3, t5, dyn<br> [0x800003f4]:csrrs t2, fcsr, zero<br> [0x800003f8]:sd t3, 32(ra)<br> [0x800003fc]:sd t2, 40(ra)<br>    |
|   4|[0x80002248]<br>0x0000000000000000<br> [0x80002250]<br>0x0000000000000060<br> |- rs1 : x22<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                         |[0x8000040c]:fsqrt.d s8, s6, dyn<br> [0x80000410]:csrrs t2, fcsr, zero<br> [0x80000414]:sd s8, 48(ra)<br> [0x80000418]:sd t2, 56(ra)<br>    |
|   5|[0x80002258]<br>0x0000000000000000<br> [0x80002260]<br>0x0000000000000080<br> |- rs1 : x24<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                         |[0x80000428]:fsqrt.d s6, s8, dyn<br> [0x8000042c]:csrrs t2, fcsr, zero<br> [0x80000430]:sd s6, 64(ra)<br> [0x80000434]:sd t2, 72(ra)<br>    |
|   6|[0x80002268]<br>0x0000000000000000<br> [0x80002270]<br>0x0000000000000000<br> |- rs1 : x18<br> - rd : x20<br>                                                                                                                                               |[0x80000444]:fsqrt.d s4, s2, dyn<br> [0x80000448]:csrrs t2, fcsr, zero<br> [0x8000044c]:sd s4, 80(ra)<br> [0x80000450]:sd t2, 88(ra)<br>    |
|   7|[0x80002278]<br>0x0000000000000000<br> [0x80002280]<br>0x0000000000000000<br> |- rs1 : x20<br> - rd : x18<br>                                                                                                                                               |[0x80000460]:fsqrt.d s2, s4, dyn<br> [0x80000464]:csrrs t2, fcsr, zero<br> [0x80000468]:sd s2, 96(ra)<br> [0x8000046c]:sd t2, 104(ra)<br>   |
|   8|[0x80002288]<br>0x0000000000000000<br> [0x80002290]<br>0x0000000000000000<br> |- rs1 : x14<br> - rd : x16<br>                                                                                                                                               |[0x8000047c]:fsqrt.d a6, a4, dyn<br> [0x80000480]:csrrs t2, fcsr, zero<br> [0x80000484]:sd a6, 112(ra)<br> [0x80000488]:sd t2, 120(ra)<br>  |
|   9|[0x80002298]<br>0x0000000000000000<br> [0x800022a0]<br>0x0000000000000000<br> |- rs1 : x16<br> - rd : x14<br>                                                                                                                                               |[0x80000498]:fsqrt.d a4, a6, dyn<br> [0x8000049c]:csrrs t2, fcsr, zero<br> [0x800004a0]:sd a4, 128(ra)<br> [0x800004a4]:sd t2, 136(ra)<br>  |
