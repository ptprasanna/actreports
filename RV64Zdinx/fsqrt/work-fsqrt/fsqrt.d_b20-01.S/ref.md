
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x800012b0')]      |
| SIG_REGION                | [('0x80003610', '0x80003a70', '140 dwords')]      |
| COV_LABELS                | fsqrt.d_b20      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV64Zdinx/work-fsqrt1/fsqrt.d_b20-01.S/ref.S    |
| Total Number of coverpoints| 170     |
| Total Coverpoints Hit     | 170      |
| Total Signature Updates   | 140      |
| STAT1                     | 70      |
| STAT2                     | 0      |
| STAT3                     | 67     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000b60]:fsqrt.d t6, t5, dyn
[0x80000b64]:csrrs t2, fcsr, zero
[0x80000b68]:sd t6, 1120(ra)
[0x80000b6c]:sd t2, 1128(ra)
[0x80000b70]:ld t5, 568(t0)
[0x80000b74]:addi gp, zero, 0
[0x80000b78]:csrrw zero, fcsr, gp
[0x80000b7c]:fsqrt.d t6, t5, dyn
[0x80000b80]:csrrs t2, fcsr, zero

[0x80000b7c]:fsqrt.d t6, t5, dyn
[0x80000b80]:csrrs t2, fcsr, zero
[0x80000b84]:sd t6, 1136(ra)
[0x80000b88]:sd t2, 1144(ra)
[0x80000b8c]:ld t5, 576(t0)
[0x80000b90]:addi gp, zero, 0
[0x80000b94]:csrrw zero, fcsr, gp
[0x80000b98]:fsqrt.d t6, t5, dyn
[0x80000b9c]:csrrs t2, fcsr, zero

[0x80000b98]:fsqrt.d t6, t5, dyn
[0x80000b9c]:csrrs t2, fcsr, zero
[0x80000ba0]:sd t6, 1152(ra)
[0x80000ba4]:sd t2, 1160(ra)
[0x80000ba8]:ld t5, 584(t0)
[0x80000bac]:addi gp, zero, 0
[0x80000bb0]:csrrw zero, fcsr, gp
[0x80000bb4]:fsqrt.d t6, t5, dyn
[0x80000bb8]:csrrs t2, fcsr, zero

[0x80000bb4]:fsqrt.d t6, t5, dyn
[0x80000bb8]:csrrs t2, fcsr, zero
[0x80000bbc]:sd t6, 1168(ra)
[0x80000bc0]:sd t2, 1176(ra)
[0x80000bc4]:ld t5, 592(t0)
[0x80000bc8]:addi gp, zero, 0
[0x80000bcc]:csrrw zero, fcsr, gp
[0x80000bd0]:fsqrt.d t6, t5, dyn
[0x80000bd4]:csrrs t2, fcsr, zero

[0x80000bd0]:fsqrt.d t6, t5, dyn
[0x80000bd4]:csrrs t2, fcsr, zero
[0x80000bd8]:sd t6, 1184(ra)
[0x80000bdc]:sd t2, 1192(ra)
[0x80000be0]:ld t5, 600(t0)
[0x80000be4]:addi gp, zero, 0
[0x80000be8]:csrrw zero, fcsr, gp
[0x80000bec]:fsqrt.d t6, t5, dyn
[0x80000bf0]:csrrs t2, fcsr, zero

[0x80000bec]:fsqrt.d t6, t5, dyn
[0x80000bf0]:csrrs t2, fcsr, zero
[0x80000bf4]:sd t6, 1200(ra)
[0x80000bf8]:sd t2, 1208(ra)
[0x80000bfc]:ld t5, 608(t0)
[0x80000c00]:addi gp, zero, 0
[0x80000c04]:csrrw zero, fcsr, gp
[0x80000c08]:fsqrt.d t6, t5, dyn
[0x80000c0c]:csrrs t2, fcsr, zero

[0x80000c08]:fsqrt.d t6, t5, dyn
[0x80000c0c]:csrrs t2, fcsr, zero
[0x80000c10]:sd t6, 1216(ra)
[0x80000c14]:sd t2, 1224(ra)
[0x80000c18]:ld t5, 616(t0)
[0x80000c1c]:addi gp, zero, 0
[0x80000c20]:csrrw zero, fcsr, gp
[0x80000c24]:fsqrt.d t6, t5, dyn
[0x80000c28]:csrrs t2, fcsr, zero

[0x80000c24]:fsqrt.d t6, t5, dyn
[0x80000c28]:csrrs t2, fcsr, zero
[0x80000c2c]:sd t6, 1232(ra)
[0x80000c30]:sd t2, 1240(ra)
[0x80000c34]:ld t5, 624(t0)
[0x80000c38]:addi gp, zero, 0
[0x80000c3c]:csrrw zero, fcsr, gp
[0x80000c40]:fsqrt.d t6, t5, dyn
[0x80000c44]:csrrs t2, fcsr, zero

[0x80000c40]:fsqrt.d t6, t5, dyn
[0x80000c44]:csrrs t2, fcsr, zero
[0x80000c48]:sd t6, 1248(ra)
[0x80000c4c]:sd t2, 1256(ra)
[0x80000c50]:ld t5, 632(t0)
[0x80000c54]:addi gp, zero, 0
[0x80000c58]:csrrw zero, fcsr, gp
[0x80000c5c]:fsqrt.d t6, t5, dyn
[0x80000c60]:csrrs t2, fcsr, zero

[0x80000c5c]:fsqrt.d t6, t5, dyn
[0x80000c60]:csrrs t2, fcsr, zero
[0x80000c64]:sd t6, 1264(ra)
[0x80000c68]:sd t2, 1272(ra)
[0x80000c6c]:ld t5, 640(t0)
[0x80000c70]:addi gp, zero, 0
[0x80000c74]:csrrw zero, fcsr, gp
[0x80000c78]:fsqrt.d t6, t5, dyn
[0x80000c7c]:csrrs t2, fcsr, zero

[0x80000c78]:fsqrt.d t6, t5, dyn
[0x80000c7c]:csrrs t2, fcsr, zero
[0x80000c80]:sd t6, 1280(ra)
[0x80000c84]:sd t2, 1288(ra)
[0x80000c88]:ld t5, 648(t0)
[0x80000c8c]:addi gp, zero, 0
[0x80000c90]:csrrw zero, fcsr, gp
[0x80000c94]:fsqrt.d t6, t5, dyn
[0x80000c98]:csrrs t2, fcsr, zero

[0x80000c94]:fsqrt.d t6, t5, dyn
[0x80000c98]:csrrs t2, fcsr, zero
[0x80000c9c]:sd t6, 1296(ra)
[0x80000ca0]:sd t2, 1304(ra)
[0x80000ca4]:ld t5, 656(t0)
[0x80000ca8]:addi gp, zero, 0
[0x80000cac]:csrrw zero, fcsr, gp
[0x80000cb0]:fsqrt.d t6, t5, dyn
[0x80000cb4]:csrrs t2, fcsr, zero

[0x80000cb0]:fsqrt.d t6, t5, dyn
[0x80000cb4]:csrrs t2, fcsr, zero
[0x80000cb8]:sd t6, 1312(ra)
[0x80000cbc]:sd t2, 1320(ra)
[0x80000cc0]:ld t5, 664(t0)
[0x80000cc4]:addi gp, zero, 0
[0x80000cc8]:csrrw zero, fcsr, gp
[0x80000ccc]:fsqrt.d t6, t5, dyn
[0x80000cd0]:csrrs t2, fcsr, zero

[0x80000ccc]:fsqrt.d t6, t5, dyn
[0x80000cd0]:csrrs t2, fcsr, zero
[0x80000cd4]:sd t6, 1328(ra)
[0x80000cd8]:sd t2, 1336(ra)
[0x80000cdc]:ld t5, 672(t0)
[0x80000ce0]:addi gp, zero, 0
[0x80000ce4]:csrrw zero, fcsr, gp
[0x80000ce8]:fsqrt.d t6, t5, dyn
[0x80000cec]:csrrs t2, fcsr, zero

[0x80000ce8]:fsqrt.d t6, t5, dyn
[0x80000cec]:csrrs t2, fcsr, zero
[0x80000cf0]:sd t6, 1344(ra)
[0x80000cf4]:sd t2, 1352(ra)
[0x80000cf8]:ld t5, 680(t0)
[0x80000cfc]:addi gp, zero, 0
[0x80000d00]:csrrw zero, fcsr, gp
[0x80000d04]:fsqrt.d t6, t5, dyn
[0x80000d08]:csrrs t2, fcsr, zero

[0x80000d04]:fsqrt.d t6, t5, dyn
[0x80000d08]:csrrs t2, fcsr, zero
[0x80000d0c]:sd t6, 1360(ra)
[0x80000d10]:sd t2, 1368(ra)
[0x80000d14]:ld t5, 688(t0)
[0x80000d18]:addi gp, zero, 0
[0x80000d1c]:csrrw zero, fcsr, gp
[0x80000d20]:fsqrt.d t6, t5, dyn
[0x80000d24]:csrrs t2, fcsr, zero

[0x80000d20]:fsqrt.d t6, t5, dyn
[0x80000d24]:csrrs t2, fcsr, zero
[0x80000d28]:sd t6, 1376(ra)
[0x80000d2c]:sd t2, 1384(ra)
[0x80000d30]:ld t5, 696(t0)
[0x80000d34]:addi gp, zero, 0
[0x80000d38]:csrrw zero, fcsr, gp
[0x80000d3c]:fsqrt.d t6, t5, dyn
[0x80000d40]:csrrs t2, fcsr, zero

[0x80000d3c]:fsqrt.d t6, t5, dyn
[0x80000d40]:csrrs t2, fcsr, zero
[0x80000d44]:sd t6, 1392(ra)
[0x80000d48]:sd t2, 1400(ra)
[0x80000d4c]:ld t5, 704(t0)
[0x80000d50]:addi gp, zero, 0
[0x80000d54]:csrrw zero, fcsr, gp
[0x80000d58]:fsqrt.d t6, t5, dyn
[0x80000d5c]:csrrs t2, fcsr, zero

[0x80000d58]:fsqrt.d t6, t5, dyn
[0x80000d5c]:csrrs t2, fcsr, zero
[0x80000d60]:sd t6, 1408(ra)
[0x80000d64]:sd t2, 1416(ra)
[0x80000d68]:ld t5, 712(t0)
[0x80000d6c]:addi gp, zero, 0
[0x80000d70]:csrrw zero, fcsr, gp
[0x80000d74]:fsqrt.d t6, t5, dyn
[0x80000d78]:csrrs t2, fcsr, zero

[0x80000d74]:fsqrt.d t6, t5, dyn
[0x80000d78]:csrrs t2, fcsr, zero
[0x80000d7c]:sd t6, 1424(ra)
[0x80000d80]:sd t2, 1432(ra)
[0x80000d84]:ld t5, 720(t0)
[0x80000d88]:addi gp, zero, 0
[0x80000d8c]:csrrw zero, fcsr, gp
[0x80000d90]:fsqrt.d t6, t5, dyn
[0x80000d94]:csrrs t2, fcsr, zero

[0x80000d90]:fsqrt.d t6, t5, dyn
[0x80000d94]:csrrs t2, fcsr, zero
[0x80000d98]:sd t6, 1440(ra)
[0x80000d9c]:sd t2, 1448(ra)
[0x80000da0]:ld t5, 728(t0)
[0x80000da4]:addi gp, zero, 0
[0x80000da8]:csrrw zero, fcsr, gp
[0x80000dac]:fsqrt.d t6, t5, dyn
[0x80000db0]:csrrs t2, fcsr, zero

[0x80000dac]:fsqrt.d t6, t5, dyn
[0x80000db0]:csrrs t2, fcsr, zero
[0x80000db4]:sd t6, 1456(ra)
[0x80000db8]:sd t2, 1464(ra)
[0x80000dbc]:ld t5, 736(t0)
[0x80000dc0]:addi gp, zero, 0
[0x80000dc4]:csrrw zero, fcsr, gp
[0x80000dc8]:fsqrt.d t6, t5, dyn
[0x80000dcc]:csrrs t2, fcsr, zero

[0x80000dc8]:fsqrt.d t6, t5, dyn
[0x80000dcc]:csrrs t2, fcsr, zero
[0x80000dd0]:sd t6, 1472(ra)
[0x80000dd4]:sd t2, 1480(ra)
[0x80000dd8]:ld t5, 744(t0)
[0x80000ddc]:addi gp, zero, 0
[0x80000de0]:csrrw zero, fcsr, gp
[0x80000de4]:fsqrt.d t6, t5, dyn
[0x80000de8]:csrrs t2, fcsr, zero

[0x80000de4]:fsqrt.d t6, t5, dyn
[0x80000de8]:csrrs t2, fcsr, zero
[0x80000dec]:sd t6, 1488(ra)
[0x80000df0]:sd t2, 1496(ra)
[0x80000df4]:ld t5, 752(t0)
[0x80000df8]:addi gp, zero, 0
[0x80000dfc]:csrrw zero, fcsr, gp
[0x80000e00]:fsqrt.d t6, t5, dyn
[0x80000e04]:csrrs t2, fcsr, zero

[0x80000e00]:fsqrt.d t6, t5, dyn
[0x80000e04]:csrrs t2, fcsr, zero
[0x80000e08]:sd t6, 1504(ra)
[0x80000e0c]:sd t2, 1512(ra)
[0x80000e10]:ld t5, 760(t0)
[0x80000e14]:addi gp, zero, 0
[0x80000e18]:csrrw zero, fcsr, gp
[0x80000e1c]:fsqrt.d t6, t5, dyn
[0x80000e20]:csrrs t2, fcsr, zero

[0x80000e1c]:fsqrt.d t6, t5, dyn
[0x80000e20]:csrrs t2, fcsr, zero
[0x80000e24]:sd t6, 1520(ra)
[0x80000e28]:sd t2, 1528(ra)
[0x80000e2c]:ld t5, 768(t0)
[0x80000e30]:addi gp, zero, 0
[0x80000e34]:csrrw zero, fcsr, gp
[0x80000e38]:fsqrt.d t6, t5, dyn
[0x80000e3c]:csrrs t2, fcsr, zero

[0x80000e38]:fsqrt.d t6, t5, dyn
[0x80000e3c]:csrrs t2, fcsr, zero
[0x80000e40]:sd t6, 1536(ra)
[0x80000e44]:sd t2, 1544(ra)
[0x80000e48]:ld t5, 776(t0)
[0x80000e4c]:addi gp, zero, 0
[0x80000e50]:csrrw zero, fcsr, gp
[0x80000e54]:fsqrt.d t6, t5, dyn
[0x80000e58]:csrrs t2, fcsr, zero

[0x80000e54]:fsqrt.d t6, t5, dyn
[0x80000e58]:csrrs t2, fcsr, zero
[0x80000e5c]:sd t6, 1552(ra)
[0x80000e60]:sd t2, 1560(ra)
[0x80000e64]:ld t5, 784(t0)
[0x80000e68]:addi gp, zero, 0
[0x80000e6c]:csrrw zero, fcsr, gp
[0x80000e70]:fsqrt.d t6, t5, dyn
[0x80000e74]:csrrs t2, fcsr, zero

[0x80000e70]:fsqrt.d t6, t5, dyn
[0x80000e74]:csrrs t2, fcsr, zero
[0x80000e78]:sd t6, 1568(ra)
[0x80000e7c]:sd t2, 1576(ra)
[0x80000e80]:ld t5, 792(t0)
[0x80000e84]:addi gp, zero, 0
[0x80000e88]:csrrw zero, fcsr, gp
[0x80000e8c]:fsqrt.d t6, t5, dyn
[0x80000e90]:csrrs t2, fcsr, zero

[0x80000e8c]:fsqrt.d t6, t5, dyn
[0x80000e90]:csrrs t2, fcsr, zero
[0x80000e94]:sd t6, 1584(ra)
[0x80000e98]:sd t2, 1592(ra)
[0x80000e9c]:ld t5, 800(t0)
[0x80000ea0]:addi gp, zero, 0
[0x80000ea4]:csrrw zero, fcsr, gp
[0x80000ea8]:fsqrt.d t6, t5, dyn
[0x80000eac]:csrrs t2, fcsr, zero

[0x80000ea8]:fsqrt.d t6, t5, dyn
[0x80000eac]:csrrs t2, fcsr, zero
[0x80000eb0]:sd t6, 1600(ra)
[0x80000eb4]:sd t2, 1608(ra)
[0x80000eb8]:ld t5, 808(t0)
[0x80000ebc]:addi gp, zero, 0
[0x80000ec0]:csrrw zero, fcsr, gp
[0x80000ec4]:fsqrt.d t6, t5, dyn
[0x80000ec8]:csrrs t2, fcsr, zero

[0x80000ec4]:fsqrt.d t6, t5, dyn
[0x80000ec8]:csrrs t2, fcsr, zero
[0x80000ecc]:sd t6, 1616(ra)
[0x80000ed0]:sd t2, 1624(ra)
[0x80000ed4]:ld t5, 816(t0)
[0x80000ed8]:addi gp, zero, 0
[0x80000edc]:csrrw zero, fcsr, gp
[0x80000ee0]:fsqrt.d t6, t5, dyn
[0x80000ee4]:csrrs t2, fcsr, zero

[0x80000ee0]:fsqrt.d t6, t5, dyn
[0x80000ee4]:csrrs t2, fcsr, zero
[0x80000ee8]:sd t6, 1632(ra)
[0x80000eec]:sd t2, 1640(ra)
[0x80000ef0]:ld t5, 824(t0)
[0x80000ef4]:addi gp, zero, 0
[0x80000ef8]:csrrw zero, fcsr, gp
[0x80000efc]:fsqrt.d t6, t5, dyn
[0x80000f00]:csrrs t2, fcsr, zero

[0x80000efc]:fsqrt.d t6, t5, dyn
[0x80000f00]:csrrs t2, fcsr, zero
[0x80000f04]:sd t6, 1648(ra)
[0x80000f08]:sd t2, 1656(ra)
[0x80000f0c]:ld t5, 832(t0)
[0x80000f10]:addi gp, zero, 0
[0x80000f14]:csrrw zero, fcsr, gp
[0x80000f18]:fsqrt.d t6, t5, dyn
[0x80000f1c]:csrrs t2, fcsr, zero

[0x80000f18]:fsqrt.d t6, t5, dyn
[0x80000f1c]:csrrs t2, fcsr, zero
[0x80000f20]:sd t6, 1664(ra)
[0x80000f24]:sd t2, 1672(ra)
[0x80000f28]:ld t5, 840(t0)
[0x80000f2c]:addi gp, zero, 0
[0x80000f30]:csrrw zero, fcsr, gp
[0x80000f34]:fsqrt.d t6, t5, dyn
[0x80000f38]:csrrs t2, fcsr, zero

[0x80000f34]:fsqrt.d t6, t5, dyn
[0x80000f38]:csrrs t2, fcsr, zero
[0x80000f3c]:sd t6, 1680(ra)
[0x80000f40]:sd t2, 1688(ra)
[0x80000f44]:ld t5, 848(t0)
[0x80000f48]:addi gp, zero, 0
[0x80000f4c]:csrrw zero, fcsr, gp
[0x80000f50]:fsqrt.d t6, t5, dyn
[0x80000f54]:csrrs t2, fcsr, zero

[0x80000f50]:fsqrt.d t6, t5, dyn
[0x80000f54]:csrrs t2, fcsr, zero
[0x80000f58]:sd t6, 1696(ra)
[0x80000f5c]:sd t2, 1704(ra)
[0x80000f60]:ld t5, 856(t0)
[0x80000f64]:addi gp, zero, 0
[0x80000f68]:csrrw zero, fcsr, gp
[0x80000f6c]:fsqrt.d t6, t5, dyn
[0x80000f70]:csrrs t2, fcsr, zero

[0x80000f6c]:fsqrt.d t6, t5, dyn
[0x80000f70]:csrrs t2, fcsr, zero
[0x80000f74]:sd t6, 1712(ra)
[0x80000f78]:sd t2, 1720(ra)
[0x80000f7c]:ld t5, 864(t0)
[0x80000f80]:addi gp, zero, 0
[0x80000f84]:csrrw zero, fcsr, gp
[0x80000f88]:fsqrt.d t6, t5, dyn
[0x80000f8c]:csrrs t2, fcsr, zero

[0x80000f88]:fsqrt.d t6, t5, dyn
[0x80000f8c]:csrrs t2, fcsr, zero
[0x80000f90]:sd t6, 1728(ra)
[0x80000f94]:sd t2, 1736(ra)
[0x80000f98]:ld t5, 872(t0)
[0x80000f9c]:addi gp, zero, 0
[0x80000fa0]:csrrw zero, fcsr, gp
[0x80000fa4]:fsqrt.d t6, t5, dyn
[0x80000fa8]:csrrs t2, fcsr, zero

[0x80000fa4]:fsqrt.d t6, t5, dyn
[0x80000fa8]:csrrs t2, fcsr, zero
[0x80000fac]:sd t6, 1744(ra)
[0x80000fb0]:sd t2, 1752(ra)
[0x80000fb4]:ld t5, 880(t0)
[0x80000fb8]:addi gp, zero, 0
[0x80000fbc]:csrrw zero, fcsr, gp
[0x80000fc0]:fsqrt.d t6, t5, dyn
[0x80000fc4]:csrrs t2, fcsr, zero

[0x80000fc0]:fsqrt.d t6, t5, dyn
[0x80000fc4]:csrrs t2, fcsr, zero
[0x80000fc8]:sd t6, 1760(ra)
[0x80000fcc]:sd t2, 1768(ra)
[0x80000fd0]:ld t5, 888(t0)
[0x80000fd4]:addi gp, zero, 0
[0x80000fd8]:csrrw zero, fcsr, gp
[0x80000fdc]:fsqrt.d t6, t5, dyn
[0x80000fe0]:csrrs t2, fcsr, zero

[0x80000fdc]:fsqrt.d t6, t5, dyn
[0x80000fe0]:csrrs t2, fcsr, zero
[0x80000fe4]:sd t6, 1776(ra)
[0x80000fe8]:sd t2, 1784(ra)
[0x80000fec]:ld t5, 896(t0)
[0x80000ff0]:addi gp, zero, 0
[0x80000ff4]:csrrw zero, fcsr, gp
[0x80000ff8]:fsqrt.d t6, t5, dyn
[0x80000ffc]:csrrs t2, fcsr, zero

[0x80000ff8]:fsqrt.d t6, t5, dyn
[0x80000ffc]:csrrs t2, fcsr, zero
[0x80001000]:sd t6, 1792(ra)
[0x80001004]:sd t2, 1800(ra)
[0x80001008]:ld t5, 904(t0)
[0x8000100c]:addi gp, zero, 0
[0x80001010]:csrrw zero, fcsr, gp
[0x80001014]:fsqrt.d t6, t5, dyn
[0x80001018]:csrrs t2, fcsr, zero

[0x80001014]:fsqrt.d t6, t5, dyn
[0x80001018]:csrrs t2, fcsr, zero
[0x8000101c]:sd t6, 1808(ra)
[0x80001020]:sd t2, 1816(ra)
[0x80001024]:ld t5, 912(t0)
[0x80001028]:addi gp, zero, 0
[0x8000102c]:csrrw zero, fcsr, gp
[0x80001030]:fsqrt.d t6, t5, dyn
[0x80001034]:csrrs t2, fcsr, zero

[0x80001030]:fsqrt.d t6, t5, dyn
[0x80001034]:csrrs t2, fcsr, zero
[0x80001038]:sd t6, 1824(ra)
[0x8000103c]:sd t2, 1832(ra)
[0x80001040]:ld t5, 920(t0)
[0x80001044]:addi gp, zero, 0
[0x80001048]:csrrw zero, fcsr, gp
[0x8000104c]:fsqrt.d t6, t5, dyn
[0x80001050]:csrrs t2, fcsr, zero

[0x8000104c]:fsqrt.d t6, t5, dyn
[0x80001050]:csrrs t2, fcsr, zero
[0x80001054]:sd t6, 1840(ra)
[0x80001058]:sd t2, 1848(ra)
[0x8000105c]:ld t5, 928(t0)
[0x80001060]:addi gp, zero, 0
[0x80001064]:csrrw zero, fcsr, gp
[0x80001068]:fsqrt.d t6, t5, dyn
[0x8000106c]:csrrs t2, fcsr, zero

[0x80001068]:fsqrt.d t6, t5, dyn
[0x8000106c]:csrrs t2, fcsr, zero
[0x80001070]:sd t6, 1856(ra)
[0x80001074]:sd t2, 1864(ra)
[0x80001078]:ld t5, 936(t0)
[0x8000107c]:addi gp, zero, 0
[0x80001080]:csrrw zero, fcsr, gp
[0x80001084]:fsqrt.d t6, t5, dyn
[0x80001088]:csrrs t2, fcsr, zero

[0x80001084]:fsqrt.d t6, t5, dyn
[0x80001088]:csrrs t2, fcsr, zero
[0x8000108c]:sd t6, 1872(ra)
[0x80001090]:sd t2, 1880(ra)
[0x80001094]:ld t5, 944(t0)
[0x80001098]:addi gp, zero, 0
[0x8000109c]:csrrw zero, fcsr, gp
[0x800010a0]:fsqrt.d t6, t5, dyn
[0x800010a4]:csrrs t2, fcsr, zero

[0x800010a0]:fsqrt.d t6, t5, dyn
[0x800010a4]:csrrs t2, fcsr, zero
[0x800010a8]:sd t6, 1888(ra)
[0x800010ac]:sd t2, 1896(ra)
[0x800010b0]:ld t5, 952(t0)
[0x800010b4]:addi gp, zero, 0
[0x800010b8]:csrrw zero, fcsr, gp
[0x800010bc]:fsqrt.d t6, t5, dyn
[0x800010c0]:csrrs t2, fcsr, zero

[0x800010bc]:fsqrt.d t6, t5, dyn
[0x800010c0]:csrrs t2, fcsr, zero
[0x800010c4]:sd t6, 1904(ra)
[0x800010c8]:sd t2, 1912(ra)
[0x800010cc]:ld t5, 960(t0)
[0x800010d0]:addi gp, zero, 0
[0x800010d4]:csrrw zero, fcsr, gp
[0x800010d8]:fsqrt.d t6, t5, dyn
[0x800010dc]:csrrs t2, fcsr, zero

[0x800010d8]:fsqrt.d t6, t5, dyn
[0x800010dc]:csrrs t2, fcsr, zero
[0x800010e0]:sd t6, 1920(ra)
[0x800010e4]:sd t2, 1928(ra)
[0x800010e8]:ld t5, 968(t0)
[0x800010ec]:addi gp, zero, 0
[0x800010f0]:csrrw zero, fcsr, gp
[0x800010f4]:fsqrt.d t6, t5, dyn
[0x800010f8]:csrrs t2, fcsr, zero

[0x800010f4]:fsqrt.d t6, t5, dyn
[0x800010f8]:csrrs t2, fcsr, zero
[0x800010fc]:sd t6, 1936(ra)
[0x80001100]:sd t2, 1944(ra)
[0x80001104]:ld t5, 976(t0)
[0x80001108]:addi gp, zero, 0
[0x8000110c]:csrrw zero, fcsr, gp
[0x80001110]:fsqrt.d t6, t5, dyn
[0x80001114]:csrrs t2, fcsr, zero

[0x80001110]:fsqrt.d t6, t5, dyn
[0x80001114]:csrrs t2, fcsr, zero
[0x80001118]:sd t6, 1952(ra)
[0x8000111c]:sd t2, 1960(ra)
[0x80001120]:ld t5, 984(t0)
[0x80001124]:addi gp, zero, 0
[0x80001128]:csrrw zero, fcsr, gp
[0x8000112c]:fsqrt.d t6, t5, dyn
[0x80001130]:csrrs t2, fcsr, zero

[0x8000112c]:fsqrt.d t6, t5, dyn
[0x80001130]:csrrs t2, fcsr, zero
[0x80001134]:sd t6, 1968(ra)
[0x80001138]:sd t2, 1976(ra)
[0x8000113c]:ld t5, 992(t0)
[0x80001140]:addi gp, zero, 0
[0x80001144]:csrrw zero, fcsr, gp
[0x80001148]:fsqrt.d t6, t5, dyn
[0x8000114c]:csrrs t2, fcsr, zero

[0x80001148]:fsqrt.d t6, t5, dyn
[0x8000114c]:csrrs t2, fcsr, zero
[0x80001150]:sd t6, 1984(ra)
[0x80001154]:sd t2, 1992(ra)
[0x80001158]:ld t5, 1000(t0)
[0x8000115c]:addi gp, zero, 0
[0x80001160]:csrrw zero, fcsr, gp
[0x80001164]:fsqrt.d t6, t5, dyn
[0x80001168]:csrrs t2, fcsr, zero

[0x80001164]:fsqrt.d t6, t5, dyn
[0x80001168]:csrrs t2, fcsr, zero
[0x8000116c]:sd t6, 2000(ra)
[0x80001170]:sd t2, 2008(ra)
[0x80001174]:ld t5, 1008(t0)
[0x80001178]:addi gp, zero, 0
[0x8000117c]:csrrw zero, fcsr, gp
[0x80001180]:fsqrt.d t6, t5, dyn
[0x80001184]:csrrs t2, fcsr, zero

[0x80001180]:fsqrt.d t6, t5, dyn
[0x80001184]:csrrs t2, fcsr, zero
[0x80001188]:sd t6, 2016(ra)
[0x8000118c]:sd t2, 2024(ra)
[0x80001190]:ld t5, 1016(t0)
[0x80001194]:addi gp, zero, 0
[0x80001198]:csrrw zero, fcsr, gp
[0x8000119c]:fsqrt.d t6, t5, dyn
[0x800011a0]:csrrs t2, fcsr, zero

[0x8000119c]:fsqrt.d t6, t5, dyn
[0x800011a0]:csrrs t2, fcsr, zero
[0x800011a4]:sd t6, 2032(ra)
[0x800011a8]:sd t2, 2040(ra)
[0x800011ac]:ld t5, 1024(t0)
[0x800011b0]:addi gp, zero, 0
[0x800011b4]:csrrw zero, fcsr, gp
[0x800011b8]:fsqrt.d t6, t5, dyn
[0x800011bc]:csrrs t2, fcsr, zero

[0x800011b8]:fsqrt.d t6, t5, dyn
[0x800011bc]:csrrs t2, fcsr, zero
[0x800011c0]:addi ra, ra, 2040
[0x800011c4]:sd t6, 8(ra)
[0x800011c8]:sd t2, 16(ra)
[0x800011cc]:ld t5, 1032(t0)
[0x800011d0]:addi gp, zero, 0
[0x800011d4]:csrrw zero, fcsr, gp
[0x800011d8]:fsqrt.d t6, t5, dyn
[0x800011dc]:csrrs t2, fcsr, zero

[0x800011d8]:fsqrt.d t6, t5, dyn
[0x800011dc]:csrrs t2, fcsr, zero
[0x800011e0]:sd t6, 24(ra)
[0x800011e4]:sd t2, 32(ra)
[0x800011e8]:ld t5, 1040(t0)
[0x800011ec]:addi gp, zero, 0
[0x800011f0]:csrrw zero, fcsr, gp
[0x800011f4]:fsqrt.d t6, t5, dyn
[0x800011f8]:csrrs t2, fcsr, zero

[0x800011f4]:fsqrt.d t6, t5, dyn
[0x800011f8]:csrrs t2, fcsr, zero
[0x800011fc]:sd t6, 40(ra)
[0x80001200]:sd t2, 48(ra)
[0x80001204]:ld t5, 1048(t0)
[0x80001208]:addi gp, zero, 0
[0x8000120c]:csrrw zero, fcsr, gp
[0x80001210]:fsqrt.d t6, t5, dyn
[0x80001214]:csrrs t2, fcsr, zero

[0x80001210]:fsqrt.d t6, t5, dyn
[0x80001214]:csrrs t2, fcsr, zero
[0x80001218]:sd t6, 56(ra)
[0x8000121c]:sd t2, 64(ra)
[0x80001220]:ld t5, 1056(t0)
[0x80001224]:addi gp, zero, 0
[0x80001228]:csrrw zero, fcsr, gp
[0x8000122c]:fsqrt.d t6, t5, dyn
[0x80001230]:csrrs t2, fcsr, zero

[0x8000122c]:fsqrt.d t6, t5, dyn
[0x80001230]:csrrs t2, fcsr, zero
[0x80001234]:sd t6, 72(ra)
[0x80001238]:sd t2, 80(ra)
[0x8000123c]:ld t5, 1064(t0)
[0x80001240]:addi gp, zero, 0
[0x80001244]:csrrw zero, fcsr, gp
[0x80001248]:fsqrt.d t6, t5, dyn
[0x8000124c]:csrrs t2, fcsr, zero

[0x80001248]:fsqrt.d t6, t5, dyn
[0x8000124c]:csrrs t2, fcsr, zero
[0x80001250]:sd t6, 88(ra)
[0x80001254]:sd t2, 96(ra)
[0x80001258]:ld t5, 1072(t0)
[0x8000125c]:addi gp, zero, 0
[0x80001260]:csrrw zero, fcsr, gp
[0x80001264]:fsqrt.d t6, t5, dyn
[0x80001268]:csrrs t2, fcsr, zero

[0x80001264]:fsqrt.d t6, t5, dyn
[0x80001268]:csrrs t2, fcsr, zero
[0x8000126c]:sd t6, 104(ra)
[0x80001270]:sd t2, 112(ra)
[0x80001274]:ld t5, 1080(t0)
[0x80001278]:addi gp, zero, 0
[0x8000127c]:csrrw zero, fcsr, gp
[0x80001280]:fsqrt.d t6, t5, dyn
[0x80001284]:csrrs t2, fcsr, zero

[0x80001280]:fsqrt.d t6, t5, dyn
[0x80001284]:csrrs t2, fcsr, zero
[0x80001288]:sd t6, 120(ra)
[0x8000128c]:sd t2, 128(ra)
[0x80001290]:ld t5, 1088(t0)
[0x80001294]:addi gp, zero, 0
[0x80001298]:csrrw zero, fcsr, gp
[0x8000129c]:fsqrt.d t6, t5, dyn
[0x800012a0]:csrrs t2, fcsr, zero

[0x8000129c]:fsqrt.d t6, t5, dyn
[0x800012a0]:csrrs t2, fcsr, zero
[0x800012a4]:sd t6, 136(ra)
[0x800012a8]:sd t2, 144(ra)
[0x800012ac]:addi zero, zero, 0



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                                  signature                                   |                                                                                 coverpoints                                                                                 |                                                                    code                                                                     |
|---:|------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80003618]<br>0x7FF0000000000000<br> [0x80003620]<br>0x0000000000000000<br> |- mnemonic : fsqrt.d<br> - rs1 : x28<br> - rd : x30<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x800003b8]:fsqrt.d t5, t3, dyn<br> [0x800003bc]:csrrs t2, fcsr, zero<br> [0x800003c0]:sd t5, 0(ra)<br> [0x800003c4]:sd t2, 8(ra)<br>       |
|   2|[0x80003628]<br>0x0000000000000000<br> [0x80003630]<br>0x0000000000000000<br> |- rs1 : x26<br> - rd : x26<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                          |[0x800003d4]:fsqrt.d s10, s10, dyn<br> [0x800003d8]:csrrs t2, fcsr, zero<br> [0x800003dc]:sd s10, 16(ra)<br> [0x800003e0]:sd t2, 24(ra)<br>  |
|   3|[0x80003638]<br>0x2E34000000000000<br> [0x80003640]<br>0x0000000000000000<br> |- rs1 : x30<br> - rd : x28<br> - fs1 == 0 and fe1 == 0x1c7 and fm1 == 0x9000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800003f0]:fsqrt.d t3, t5, dyn<br> [0x800003f4]:csrrs t2, fcsr, zero<br> [0x800003f8]:sd t3, 32(ra)<br> [0x800003fc]:sd t2, 40(ra)<br>     |
|   4|[0x80003648]<br>0x5E34000000000000<br> [0x80003650]<br>0x0000000000000000<br> |- rs1 : x22<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x7c7 and fm1 == 0x9000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x8000040c]:fsqrt.d s8, s6, dyn<br> [0x80000410]:csrrs t2, fcsr, zero<br> [0x80000414]:sd s8, 48(ra)<br> [0x80000418]:sd t2, 56(ra)<br>     |
|   5|[0x80003658]<br>0x4D86000000000000<br> [0x80003660]<br>0x0000000000000000<br> |- rs1 : x24<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x5b1 and fm1 == 0xe400000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000428]:fsqrt.d s6, s8, dyn<br> [0x8000042c]:csrrs t2, fcsr, zero<br> [0x80000430]:sd s6, 64(ra)<br> [0x80000434]:sd t2, 72(ra)<br>     |
|   6|[0x80003668]<br>0x51B6000000000000<br> [0x80003670]<br>0x0000000000000000<br> |- rs1 : x18<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x637 and fm1 == 0xe400000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000444]:fsqrt.d s4, s2, dyn<br> [0x80000448]:csrrs t2, fcsr, zero<br> [0x8000044c]:sd s4, 80(ra)<br> [0x80000450]:sd t2, 88(ra)<br>     |
|   7|[0x80003678]<br>0x5C8E000000000000<br> [0x80003680]<br>0x0000000000000000<br> |- rs1 : x20<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x792 and fm1 == 0xc200000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000460]:fsqrt.d s2, s4, dyn<br> [0x80000464]:csrrs t2, fcsr, zero<br> [0x80000468]:sd s2, 96(ra)<br> [0x8000046c]:sd t2, 104(ra)<br>    |
|   8|[0x80003688]<br>0x412F000000000000<br> [0x80003690]<br>0x0000000000000000<br> |- rs1 : x14<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x426 and fm1 == 0xe080000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x8000047c]:fsqrt.d a6, a4, dyn<br> [0x80000480]:csrrs t2, fcsr, zero<br> [0x80000484]:sd a6, 112(ra)<br> [0x80000488]:sd t2, 120(ra)<br>   |
|   9|[0x80003698]<br>0x45A5000000000000<br> [0x800036a0]<br>0x0000000000000000<br> |- rs1 : x16<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x4b5 and fm1 == 0xb900000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000498]:fsqrt.d a4, a6, dyn<br> [0x8000049c]:csrrs t2, fcsr, zero<br> [0x800004a0]:sd a4, 128(ra)<br> [0x800004a4]:sd t2, 136(ra)<br>   |
|  10|[0x800036a8]<br>0x4047000000000000<br> [0x800036b0]<br>0x0000000000000000<br> |- rs1 : x10<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x40a and fm1 == 0x0880000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800004b4]:fsqrt.d a2, a0, dyn<br> [0x800004b8]:csrrs t2, fcsr, zero<br> [0x800004bc]:sd a2, 144(ra)<br> [0x800004c0]:sd t2, 152(ra)<br>   |
|  11|[0x800036b8]<br>0x2E91000000000000<br> [0x800036c0]<br>0x0000000000000000<br> |- rs1 : x12<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x1d3 and fm1 == 0x2100000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800004d0]:fsqrt.d a0, a2, dyn<br> [0x800004d4]:csrrs t2, fcsr, zero<br> [0x800004d8]:sd a0, 160(ra)<br> [0x800004dc]:sd t2, 168(ra)<br>   |
|  12|[0x800036c8]<br>0x28CE800000000000<br> [0x800036d0]<br>0x0000000000000000<br> |- rs1 : x6<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x11a and fm1 == 0xd120000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x800004ec]:fsqrt.d fp, t1, dyn<br> [0x800004f0]:csrrs t2, fcsr, zero<br> [0x800004f4]:sd fp, 176(ra)<br> [0x800004f8]:sd t2, 184(ra)<br>   |
|  13|[0x800036d8]<br>0x3636800000000000<br> [0x800036e0]<br>0x0000000000000000<br> |- rs1 : x8<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x2c7 and fm1 == 0xfa40000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x80000508]:fsqrt.d t1, fp, dyn<br> [0x8000050c]:csrrs t2, fcsr, zero<br> [0x80000510]:sd t1, 192(ra)<br> [0x80000514]:sd t2, 200(ra)<br>   |
|  14|[0x800036e8]<br>0x434E800000000000<br> [0x800036f0]<br>0x0000000000000000<br> |- rs1 : x2<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x46a and fm1 == 0xd120000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x80000524]:fsqrt.d tp, sp, dyn<br> [0x80000528]:csrrs t2, fcsr, zero<br> [0x8000052c]:sd tp, 208(ra)<br> [0x80000530]:sd t2, 216(ra)<br>   |
|  15|[0x800036f8]<br>0x4148400000000000<br> [0x80003700]<br>0x0000000000000000<br> |- rs1 : x4<br> - rd : x2<br> - fs1 == 0 and fe1 == 0x42a and fm1 == 0x2608000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x80000540]:fsqrt.d sp, tp, dyn<br> [0x80000544]:csrrs t2, fcsr, zero<br> [0x80000548]:sd sp, 224(ra)<br> [0x8000054c]:sd t2, 232(ra)<br>   |
|  16|[0x80003708]<br>0x41CFC00000000000<br> [0x80003710]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x43a and fm1 == 0xf808000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000055c]:fsqrt.d t6, t5, dyn<br> [0x80000560]:csrrs t2, fcsr, zero<br> [0x80000564]:sd t6, 240(ra)<br> [0x80000568]:sd t2, 248(ra)<br>   |
|  17|[0x80003718]<br>0x390E400000000000<br> [0x80003720]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x322 and fm1 == 0xc988000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000578]:fsqrt.d t6, t5, dyn<br> [0x8000057c]:csrrs t2, fcsr, zero<br> [0x80000580]:sd t6, 256(ra)<br> [0x80000584]:sd t2, 264(ra)<br>   |
|  18|[0x80003728]<br>0x5E06A00000000000<br> [0x80003730]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x7c1 and fm1 == 0xffe4000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000594]:fsqrt.d t6, t5, dyn<br> [0x80000598]:csrrs t2, fcsr, zero<br> [0x8000059c]:sd t6, 272(ra)<br> [0x800005a0]:sd t2, 280(ra)<br>   |
|  19|[0x80003738]<br>0x2E55A00000000000<br> [0x80003740]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x1cb and fm1 == 0xd3a4000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800005b0]:fsqrt.d t6, t5, dyn<br> [0x800005b4]:csrrs t2, fcsr, zero<br> [0x800005b8]:sd t6, 288(ra)<br> [0x800005bc]:sd t2, 296(ra)<br>   |
|  20|[0x80003748]<br>0x5B86700000000000<br> [0x80003750]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x771 and fm1 == 0xf771000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800005cc]:fsqrt.d t6, t5, dyn<br> [0x800005d0]:csrrs t2, fcsr, zero<br> [0x800005d4]:sd t6, 304(ra)<br> [0x800005d8]:sd t2, 312(ra)<br>   |
|  21|[0x80003758]<br>0x53D7700000000000<br> [0x80003760]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x67c and fm1 == 0x12a8800000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800005e8]:fsqrt.d t6, t5, dyn<br> [0x800005ec]:csrrs t2, fcsr, zero<br> [0x800005f0]:sd t6, 320(ra)<br> [0x800005f4]:sd t2, 328(ra)<br>   |
|  22|[0x80003768]<br>0x3EB3F00000000000<br> [0x80003770]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x3d7 and fm1 == 0x8d81000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000604]:fsqrt.d t6, t5, dyn<br> [0x80000608]:csrrs t2, fcsr, zero<br> [0x8000060c]:sd t6, 336(ra)<br> [0x80000610]:sd t2, 344(ra)<br>   |
|  23|[0x80003778]<br>0x3FCED00000000000<br> [0x80003780]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x3fa and fm1 == 0xdab4800000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000620]:fsqrt.d t6, t5, dyn<br> [0x80000624]:csrrs t2, fcsr, zero<br> [0x80000628]:sd t6, 352(ra)<br> [0x8000062c]:sd t2, 360(ra)<br>   |
|  24|[0x80003788]<br>0x54B5A80000000000<br> [0x80003790]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x697 and fm1 == 0xd4fe400000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000063c]:fsqrt.d t6, t5, dyn<br> [0x80000640]:csrrs t2, fcsr, zero<br> [0x80000644]:sd t6, 368(ra)<br> [0x80000648]:sd t2, 376(ra)<br>   |
|  25|[0x80003798]<br>0x2CE7680000000000<br> [0x800037a0]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x19e and fm1 == 0x11ed200000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000658]:fsqrt.d t6, t5, dyn<br> [0x8000065c]:csrrs t2, fcsr, zero<br> [0x80000660]:sd t6, 384(ra)<br> [0x80000664]:sd t2, 392(ra)<br>   |
|  26|[0x800037a8]<br>0x3A7B940000000000<br> [0x800037b0]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x350 and fm1 == 0x7c46c80000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000674]:fsqrt.d t6, t5, dyn<br> [0x80000678]:csrrs t2, fcsr, zero<br> [0x8000067c]:sd t6, 400(ra)<br> [0x80000680]:sd t2, 408(ra)<br>   |
|  27|[0x800037b8]<br>0x4EEE840000000000<br> [0x800037c0]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x5de and fm1 == 0xd19a080000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000690]:fsqrt.d t6, t5, dyn<br> [0x80000694]:csrrs t2, fcsr, zero<br> [0x80000698]:sd t6, 416(ra)<br> [0x8000069c]:sd t2, 424(ra)<br>   |
|  28|[0x800037c8]<br>0x26D4F20000000000<br> [0x800037d0]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x0db and fm1 == 0xb6b4c40000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800006ac]:fsqrt.d t6, t5, dyn<br> [0x800006b0]:csrrs t2, fcsr, zero<br> [0x800006b4]:sd t6, 432(ra)<br> [0x800006b8]:sd t2, 440(ra)<br>   |
|  29|[0x800037d8]<br>0x5A6CCA0000000000<br> [0x800037e0]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x74e and fm1 == 0x9e67b20000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800006c8]:fsqrt.d t6, t5, dyn<br> [0x800006cc]:csrrs t2, fcsr, zero<br> [0x800006d0]:sd t6, 448(ra)<br> [0x800006d4]:sd t2, 456(ra)<br>   |
|  30|[0x800037e8]<br>0x47F8120000000000<br> [0x800037f0]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x500 and fm1 == 0x21b0a20000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800006e4]:fsqrt.d t6, t5, dyn<br> [0x800006e8]:csrrs t2, fcsr, zero<br> [0x800006ec]:sd t6, 464(ra)<br> [0x800006f0]:sd t2, 472(ra)<br>   |
|  31|[0x800037f8]<br>0x53681E0000000000<br> [0x80003800]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x66e and fm1 == 0x22d1c20000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000700]:fsqrt.d t6, t5, dyn<br> [0x80000704]:csrrs t2, fcsr, zero<br> [0x80000708]:sd t6, 480(ra)<br> [0x8000070c]:sd t2, 488(ra)<br>   |
|  32|[0x80003808]<br>0x3409690000000000<br> [0x80003810]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x282 and fm1 == 0x42d6888000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000071c]:fsqrt.d t6, t5, dyn<br> [0x80000720]:csrrs t2, fcsr, zero<br> [0x80000724]:sd t6, 496(ra)<br> [0x80000728]:sd t2, 504(ra)<br>   |
|  33|[0x80003818]<br>0x2EE8310000000000<br> [0x80003820]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x1de and fm1 == 0x249cb08000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000738]:fsqrt.d t6, t5, dyn<br> [0x8000073c]:csrrs t2, fcsr, zero<br> [0x80000740]:sd t6, 512(ra)<br> [0x80000744]:sd t2, 520(ra)<br>   |
|  34|[0x80003828]<br>0x4FCAF30000000000<br> [0x80003830]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x6b21548000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000754]:fsqrt.d t6, t5, dyn<br> [0x80000758]:csrrs t2, fcsr, zero<br> [0x8000075c]:sd t6, 528(ra)<br> [0x80000760]:sd t2, 536(ra)<br>   |
|  35|[0x80003838]<br>0x5CE5768000000000<br> [0x80003840]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x79d and fm1 == 0xcca7da4000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000770]:fsqrt.d t6, t5, dyn<br> [0x80000774]:csrrs t2, fcsr, zero<br> [0x80000778]:sd t6, 544(ra)<br> [0x8000077c]:sd t2, 552(ra)<br>   |
|  36|[0x80003848]<br>0x5F5FF38000000000<br> [0x80003850]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x7ec and fm1 == 0xfe704e2000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000078c]:fsqrt.d t6, t5, dyn<br> [0x80000790]:csrrs t2, fcsr, zero<br> [0x80000794]:sd t6, 560(ra)<br> [0x80000798]:sd t2, 568(ra)<br>   |
|  37|[0x80003858]<br>0x4980F08000000000<br> [0x80003860]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x531 and fm1 == 0x1ef1f04000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800007a8]:fsqrt.d t6, t5, dyn<br> [0x800007ac]:csrrs t2, fcsr, zero<br> [0x800007b0]:sd t6, 576(ra)<br> [0x800007b4]:sd t2, 584(ra)<br>   |
|  38|[0x80003868]<br>0x22F0C88000000000<br> [0x80003870]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x05f and fm1 == 0x19ad084000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800007c4]:fsqrt.d t6, t5, dyn<br> [0x800007c8]:csrrs t2, fcsr, zero<br> [0x800007cc]:sd t6, 592(ra)<br> [0x800007d0]:sd t2, 600(ra)<br>   |
|  39|[0x80003878]<br>0x5753A04000000000<br> [0x80003880]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x6eb and fm1 == 0x812dd01000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800007e0]:fsqrt.d t6, t5, dyn<br> [0x800007e4]:csrrs t2, fcsr, zero<br> [0x800007e8]:sd t6, 608(ra)<br> [0x800007ec]:sd t2, 616(ra)<br>   |
|  40|[0x80003888]<br>0x1F5440C000000000<br> [0x80003890]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x00000668b9824 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800007fc]:fsqrt.d t6, t5, dyn<br> [0x80000800]:csrrs t2, fcsr, zero<br> [0x80000804]:sd t6, 624(ra)<br> [0x80000808]:sd t2, 632(ra)<br>   |
|  41|[0x80003898]<br>0x3620F76000000000<br> [0x800038a0]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x2c5 and fm1 == 0x1fdb0a6400000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000818]:fsqrt.d t6, t5, dyn<br> [0x8000081c]:csrrs t2, fcsr, zero<br> [0x80000820]:sd t6, 640(ra)<br> [0x80000824]:sd t2, 648(ra)<br>   |
|  42|[0x800038a8]<br>0x2E60E8A000000000<br> [0x800038b0]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x1cd and fm1 == 0x1de7626400000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000834]:fsqrt.d t6, t5, dyn<br> [0x80000838]:csrrs t2, fcsr, zero<br> [0x8000083c]:sd t6, 656(ra)<br> [0x80000840]:sd t2, 664(ra)<br>   |
|  43|[0x800038b8]<br>0x1FE2C02000000000<br> [0x800038c0]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x15f94b0040000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000850]:fsqrt.d t6, t5, dyn<br> [0x80000854]:csrrs t2, fcsr, zero<br> [0x80000858]:sd t6, 672(ra)<br> [0x8000085c]:sd t2, 680(ra)<br>   |
|  44|[0x800038c8]<br>0x5D50A77000000000<br> [0x800038d0]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x7ab and fm1 == 0x155b835100000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000086c]:fsqrt.d t6, t5, dyn<br> [0x80000870]:csrrs t2, fcsr, zero<br> [0x80000874]:sd t6, 688(ra)<br> [0x80000878]:sd t2, 696(ra)<br>   |
|  45|[0x800038d8]<br>0x2FC0569000000000<br> [0x800038e0]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x1f9 and fm1 == 0x0aef451100000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000888]:fsqrt.d t6, t5, dyn<br> [0x8000088c]:csrrs t2, fcsr, zero<br> [0x80000890]:sd t6, 704(ra)<br> [0x80000894]:sd t2, 712(ra)<br>   |
|  46|[0x800038e8]<br>0x5E02839000000000<br> [0x800038f0]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x7c1 and fm1 == 0x56c3dcb100000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800008a4]:fsqrt.d t6, t5, dyn<br> [0x800008a8]:csrrs t2, fcsr, zero<br> [0x800008ac]:sd t6, 720(ra)<br> [0x800008b0]:sd t2, 728(ra)<br>   |
|  47|[0x800038f8]<br>0x58885E9800000000<br> [0x80003900]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x712 and fm1 == 0x28efb9fd20000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800008c0]:fsqrt.d t6, t5, dyn<br> [0x800008c4]:csrrs t2, fcsr, zero<br> [0x800008c8]:sd t6, 736(ra)<br> [0x800008cc]:sd t2, 744(ra)<br>   |
|  48|[0x80003908]<br>0x4B762B4800000000<br> [0x80003910]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x56f and fm1 == 0xeb77b14440000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800008dc]:fsqrt.d t6, t5, dyn<br> [0x800008e0]:csrrs t2, fcsr, zero<br> [0x800008e4]:sd t6, 752(ra)<br> [0x800008e8]:sd t2, 760(ra)<br>   |
|  49|[0x80003918]<br>0x2C00112800000000<br> [0x80003920]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x181 and fm1 == 0x0226265640000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800008f8]:fsqrt.d t6, t5, dyn<br> [0x800008fc]:csrrs t2, fcsr, zero<br> [0x80000900]:sd t6, 768(ra)<br> [0x80000904]:sd t2, 776(ra)<br>   |
|  50|[0x80003928]<br>0x5059D80400000000<br> [0x80003930]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x60c and fm1 == 0x4df3876008000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000914]:fsqrt.d t6, t5, dyn<br> [0x80000918]:csrrs t2, fcsr, zero<br> [0x8000091c]:sd t6, 784(ra)<br> [0x80000920]:sd t2, 792(ra)<br>   |
|  51|[0x80003938]<br>0x3E85653C00000000<br> [0x80003940]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x3d1 and fm1 == 0xc9c3e06610000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000930]:fsqrt.d t6, t5, dyn<br> [0x80000934]:csrrs t2, fcsr, zero<br> [0x80000938]:sd t6, 800(ra)<br> [0x8000093c]:sd t2, 808(ra)<br>   |
|  52|[0x80003948]<br>0x3964EF2400000000<br> [0x80003950]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x32d and fm1 == 0xb63d043d10000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000094c]:fsqrt.d t6, t5, dyn<br> [0x80000950]:csrrs t2, fcsr, zero<br> [0x80000954]:sd t6, 816(ra)<br> [0x80000958]:sd t2, 824(ra)<br>   |
|  53|[0x80003958]<br>0x4D7C416C00000000<br> [0x80003960]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x5b0 and fm1 == 0x8f302c02c8000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000968]:fsqrt.d t6, t5, dyn<br> [0x8000096c]:csrrs t2, fcsr, zero<br> [0x80000970]:sd t6, 832(ra)<br> [0x80000974]:sd t2, 840(ra)<br>   |
|  54|[0x80003968]<br>0x336C173200000000<br> [0x80003970]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x26e and fm1 == 0x8a8a8502e2000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000984]:fsqrt.d t6, t5, dyn<br> [0x80000988]:csrrs t2, fcsr, zero<br> [0x8000098c]:sd t6, 848(ra)<br> [0x80000990]:sd t2, 856(ra)<br>   |
|  55|[0x80003978]<br>0x5A49F20E00000000<br> [0x80003980]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x74a and fm1 == 0x5095cd3c62000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800009a0]:fsqrt.d t6, t5, dyn<br> [0x800009a4]:csrrs t2, fcsr, zero<br> [0x800009a8]:sd t6, 864(ra)<br> [0x800009ac]:sd t2, 872(ra)<br>   |
|  56|[0x80003988]<br>0x54917ECA00000000<br> [0x80003990]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x693 and fm1 == 0x32159f7764000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800009bc]:fsqrt.d t6, t5, dyn<br> [0x800009c0]:csrrs t2, fcsr, zero<br> [0x800009c4]:sd t6, 880(ra)<br> [0x800009c8]:sd t2, 888(ra)<br>   |
|  57|[0x80003998]<br>0x3950632300000000<br> [0x800039a0]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x32b and fm1 == 0x0c8ac416c9000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800009d8]:fsqrt.d t6, t5, dyn<br> [0x800009dc]:csrrs t2, fcsr, zero<br> [0x800009e0]:sd t6, 896(ra)<br> [0x800009e4]:sd t2, 904(ra)<br>   |
|  58|[0x800039a8]<br>0x4AD84AE700000000<br> [0x800039b0]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x55c and fm1 == 0x27109d2e38800 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800009f4]:fsqrt.d t6, t5, dyn<br> [0x800009f8]:csrrs t2, fcsr, zero<br> [0x800009fc]:sd t6, 912(ra)<br> [0x80000a00]:sd t2, 920(ra)<br>   |
|  59|[0x800039b8]<br>0x3E56498900000000<br> [0x800039c0]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x3cb and fm1 == 0xf0b8ab6b51000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000a10]:fsqrt.d t6, t5, dyn<br> [0x80000a14]:csrrs t2, fcsr, zero<br> [0x80000a18]:sd t6, 928(ra)<br> [0x80000a1c]:sd t2, 936(ra)<br>   |
|  60|[0x800039c8]<br>0x2BAF5B0F80000000<br> [0x800039d0]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x176 and fm1 == 0xeb971282f8200 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000a2c]:fsqrt.d t6, t5, dyn<br> [0x80000a30]:csrrs t2, fcsr, zero<br> [0x80000a34]:sd t6, 944(ra)<br> [0x80000a38]:sd t2, 952(ra)<br>   |
|  61|[0x800039d8]<br>0x4D94307C80000000<br> [0x800039e0]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x5b3 and fm1 == 0x979ca2ec8c400 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000a48]:fsqrt.d t6, t5, dyn<br> [0x80000a4c]:csrrs t2, fcsr, zero<br> [0x80000a50]:sd t6, 960(ra)<br> [0x80000a54]:sd t2, 968(ra)<br>   |
|  62|[0x800039e8]<br>0x5396B75180000000<br> [0x800039f0]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x674 and fm1 == 0x0202a3cf79200 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000a64]:fsqrt.d t6, t5, dyn<br> [0x80000a68]:csrrs t2, fcsr, zero<br> [0x80000a6c]:sd t6, 976(ra)<br> [0x80000a70]:sd t2, 984(ra)<br>   |
|  63|[0x800039f8]<br>0x476B310B80000000<br> [0x80003a00]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x4ee and fm1 == 0x71b0e933c2200 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000a80]:fsqrt.d t6, t5, dyn<br> [0x80000a84]:csrrs t2, fcsr, zero<br> [0x80000a88]:sd t6, 992(ra)<br> [0x80000a8c]:sd t2, 1000(ra)<br>  |
|  64|[0x80003a08]<br>0x55D23FC5C0000000<br> [0x80003a10]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x6bb and fm1 == 0x4d07b1ed41100 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000a9c]:fsqrt.d t6, t5, dyn<br> [0x80000aa0]:csrrs t2, fcsr, zero<br> [0x80000aa4]:sd t6, 1008(ra)<br> [0x80000aa8]:sd t2, 1016(ra)<br> |
|  65|[0x80003a18]<br>0x51474115C0000000<br> [0x80003a20]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x62a and fm1 == 0x0e613a46ac880 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000ab8]:fsqrt.d t6, t5, dyn<br> [0x80000abc]:csrrs t2, fcsr, zero<br> [0x80000ac0]:sd t6, 1024(ra)<br> [0x80000ac4]:sd t2, 1032(ra)<br> |
|  66|[0x80003a28]<br>0x1F9FE99360000272<br> [0x80003a30]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000fe99b3b666 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000ad4]:fsqrt.d t6, t5, dyn<br> [0x80000ad8]:csrrs t2, fcsr, zero<br> [0x80000adc]:sd t6, 1040(ra)<br> [0x80000ae0]:sd t2, 1048(ra)<br> |
|  67|[0x80003a38]<br>0x38EB6C0060000000<br> [0x80003a40]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x31e and fm1 == 0x77fad24880120 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000af0]:fsqrt.d t6, t5, dyn<br> [0x80000af4]:csrrs t2, fcsr, zero<br> [0x80000af8]:sd t6, 1056(ra)<br> [0x80000afc]:sd t2, 1064(ra)<br> |
|  68|[0x80003a48]<br>0x4FE3A6B860000000<br> [0x80003a50]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x5fd and fm1 == 0x822bf1e14a240 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000b0c]:fsqrt.d t6, t5, dyn<br> [0x80000b10]:csrrs t2, fcsr, zero<br> [0x80000b14]:sd t6, 1072(ra)<br> [0x80000b18]:sd t2, 1080(ra)<br> |
|  69|[0x80003a58]<br>0x4FDCCDA650000000<br> [0x80003a60]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x5fc and fm1 == 0x9ed0caa415ec8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000b28]:fsqrt.d t6, t5, dyn<br> [0x80000b2c]:csrrs t2, fcsr, zero<br> [0x80000b30]:sd t6, 1088(ra)<br> [0x80000b34]:sd t2, 1096(ra)<br> |
|  70|[0x80003a68]<br>0x20D5C1DC10000000<br> [0x80003a70]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x01b and fm1 == 0xd960e82d4b810 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000b44]:fsqrt.d t6, t5, dyn<br> [0x80000b48]:csrrs t2, fcsr, zero<br> [0x80000b4c]:sd t6, 1104(ra)<br> [0x80000b50]:sd t2, 1112(ra)<br> |
