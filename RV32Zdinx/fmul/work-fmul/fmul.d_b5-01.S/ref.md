
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80004f60')]      |
| SIG_REGION                | [('0x80006f10', '0x80007c60', '852 words')]      |
| COV_LABELS                | fmul.d_b5      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-fmul/fmul.d_b5-01.S/ref.S    |
| Total Number of coverpoints| 261     |
| Total Coverpoints Hit     | 261      |
| Total Signature Updates   | 446      |
| STAT1                     | 111      |
| STAT2                     | 0      |
| STAT3                     | 101     |
| STAT4                     | 223     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x800020a0]:fmul.d t5, t3, s10, dyn
[0x800020a4]:csrrs a6, fcsr, zero
[0x800020a8]:sw t5, 1192(ra)
[0x800020ac]:sw t6, 1200(ra)
[0x800020b0]:sw t5, 1208(ra)
[0x800020b4]:sw a6, 1216(ra)
[0x800020b8]:lw t3, 1632(a5)
[0x800020bc]:lw t4, 1636(a5)
[0x800020c0]:lw s10, 1640(a5)
[0x800020c4]:lw s11, 1644(a5)
[0x800020c8]:lui t3, 498823
[0x800020cc]:addi t3, t3, 1959
[0x800020d0]:lui t4, 524012
[0x800020d4]:addi t4, t4, 2020
[0x800020d8]:lui s10, 8
[0x800020dc]:addi s11, zero, 0
[0x800020e0]:addi a4, zero, 64
[0x800020e4]:csrrw zero, fcsr, a4
[0x800020e8]:fmul.d t5, t3, s10, dyn
[0x800020ec]:csrrs a6, fcsr, zero

[0x800020e8]:fmul.d t5, t3, s10, dyn
[0x800020ec]:csrrs a6, fcsr, zero
[0x800020f0]:sw t5, 1224(ra)
[0x800020f4]:sw t6, 1232(ra)
[0x800020f8]:sw t5, 1240(ra)
[0x800020fc]:sw a6, 1248(ra)
[0x80002100]:lw t3, 1648(a5)
[0x80002104]:lw t4, 1652(a5)
[0x80002108]:lw s10, 1656(a5)
[0x8000210c]:lw s11, 1660(a5)
[0x80002110]:lui t3, 498823
[0x80002114]:addi t3, t3, 1959
[0x80002118]:lui t4, 524012
[0x8000211c]:addi t4, t4, 2020
[0x80002120]:lui s10, 8
[0x80002124]:addi s11, zero, 0
[0x80002128]:addi a4, zero, 96
[0x8000212c]:csrrw zero, fcsr, a4
[0x80002130]:fmul.d t5, t3, s10, dyn
[0x80002134]:csrrs a6, fcsr, zero

[0x80002130]:fmul.d t5, t3, s10, dyn
[0x80002134]:csrrs a6, fcsr, zero
[0x80002138]:sw t5, 1256(ra)
[0x8000213c]:sw t6, 1264(ra)
[0x80002140]:sw t5, 1272(ra)
[0x80002144]:sw a6, 1280(ra)
[0x80002148]:lw t3, 1664(a5)
[0x8000214c]:lw t4, 1668(a5)
[0x80002150]:lw s10, 1672(a5)
[0x80002154]:lw s11, 1676(a5)
[0x80002158]:lui t3, 498823
[0x8000215c]:addi t3, t3, 1959
[0x80002160]:lui t4, 524012
[0x80002164]:addi t4, t4, 2020
[0x80002168]:lui s10, 8
[0x8000216c]:addi s11, zero, 0
[0x80002170]:addi a4, zero, 128
[0x80002174]:csrrw zero, fcsr, a4
[0x80002178]:fmul.d t5, t3, s10, dyn
[0x8000217c]:csrrs a6, fcsr, zero

[0x80002178]:fmul.d t5, t3, s10, dyn
[0x8000217c]:csrrs a6, fcsr, zero
[0x80002180]:sw t5, 1288(ra)
[0x80002184]:sw t6, 1296(ra)
[0x80002188]:sw t5, 1304(ra)
[0x8000218c]:sw a6, 1312(ra)
[0x80002190]:lw t3, 1680(a5)
[0x80002194]:lw t4, 1684(a5)
[0x80002198]:lw s10, 1688(a5)
[0x8000219c]:lw s11, 1692(a5)
[0x800021a0]:lui t3, 154007
[0x800021a4]:addi t3, t3, 1487
[0x800021a8]:lui t4, 523906
[0x800021ac]:addi t4, t4, 205
[0x800021b0]:lui s10, 8
[0x800021b4]:addi s11, zero, 0
[0x800021b8]:addi a4, zero, 0
[0x800021bc]:csrrw zero, fcsr, a4
[0x800021c0]:fmul.d t5, t3, s10, dyn
[0x800021c4]:csrrs a6, fcsr, zero

[0x800021c0]:fmul.d t5, t3, s10, dyn
[0x800021c4]:csrrs a6, fcsr, zero
[0x800021c8]:sw t5, 1320(ra)
[0x800021cc]:sw t6, 1328(ra)
[0x800021d0]:sw t5, 1336(ra)
[0x800021d4]:sw a6, 1344(ra)
[0x800021d8]:lw t3, 1696(a5)
[0x800021dc]:lw t4, 1700(a5)
[0x800021e0]:lw s10, 1704(a5)
[0x800021e4]:lw s11, 1708(a5)
[0x800021e8]:lui t3, 154007
[0x800021ec]:addi t3, t3, 1487
[0x800021f0]:lui t4, 523906
[0x800021f4]:addi t4, t4, 205
[0x800021f8]:lui s10, 8
[0x800021fc]:addi s11, zero, 0
[0x80002200]:addi a4, zero, 32
[0x80002204]:csrrw zero, fcsr, a4
[0x80002208]:fmul.d t5, t3, s10, dyn
[0x8000220c]:csrrs a6, fcsr, zero

[0x80002208]:fmul.d t5, t3, s10, dyn
[0x8000220c]:csrrs a6, fcsr, zero
[0x80002210]:sw t5, 1352(ra)
[0x80002214]:sw t6, 1360(ra)
[0x80002218]:sw t5, 1368(ra)
[0x8000221c]:sw a6, 1376(ra)
[0x80002220]:lw t3, 1712(a5)
[0x80002224]:lw t4, 1716(a5)
[0x80002228]:lw s10, 1720(a5)
[0x8000222c]:lw s11, 1724(a5)
[0x80002230]:lui t3, 154007
[0x80002234]:addi t3, t3, 1487
[0x80002238]:lui t4, 523906
[0x8000223c]:addi t4, t4, 205
[0x80002240]:lui s10, 8
[0x80002244]:addi s11, zero, 0
[0x80002248]:addi a4, zero, 64
[0x8000224c]:csrrw zero, fcsr, a4
[0x80002250]:fmul.d t5, t3, s10, dyn
[0x80002254]:csrrs a6, fcsr, zero

[0x80002250]:fmul.d t5, t3, s10, dyn
[0x80002254]:csrrs a6, fcsr, zero
[0x80002258]:sw t5, 1384(ra)
[0x8000225c]:sw t6, 1392(ra)
[0x80002260]:sw t5, 1400(ra)
[0x80002264]:sw a6, 1408(ra)
[0x80002268]:lw t3, 1728(a5)
[0x8000226c]:lw t4, 1732(a5)
[0x80002270]:lw s10, 1736(a5)
[0x80002274]:lw s11, 1740(a5)
[0x80002278]:lui t3, 154007
[0x8000227c]:addi t3, t3, 1487
[0x80002280]:lui t4, 523906
[0x80002284]:addi t4, t4, 205
[0x80002288]:lui s10, 8
[0x8000228c]:addi s11, zero, 0
[0x80002290]:addi a4, zero, 96
[0x80002294]:csrrw zero, fcsr, a4
[0x80002298]:fmul.d t5, t3, s10, dyn
[0x8000229c]:csrrs a6, fcsr, zero

[0x80002298]:fmul.d t5, t3, s10, dyn
[0x8000229c]:csrrs a6, fcsr, zero
[0x800022a0]:sw t5, 1416(ra)
[0x800022a4]:sw t6, 1424(ra)
[0x800022a8]:sw t5, 1432(ra)
[0x800022ac]:sw a6, 1440(ra)
[0x800022b0]:lw t3, 1744(a5)
[0x800022b4]:lw t4, 1748(a5)
[0x800022b8]:lw s10, 1752(a5)
[0x800022bc]:lw s11, 1756(a5)
[0x800022c0]:lui t3, 154007
[0x800022c4]:addi t3, t3, 1487
[0x800022c8]:lui t4, 523906
[0x800022cc]:addi t4, t4, 205
[0x800022d0]:lui s10, 8
[0x800022d4]:addi s11, zero, 0
[0x800022d8]:addi a4, zero, 128
[0x800022dc]:csrrw zero, fcsr, a4
[0x800022e0]:fmul.d t5, t3, s10, dyn
[0x800022e4]:csrrs a6, fcsr, zero

[0x800022e0]:fmul.d t5, t3, s10, dyn
[0x800022e4]:csrrs a6, fcsr, zero
[0x800022e8]:sw t5, 1448(ra)
[0x800022ec]:sw t6, 1456(ra)
[0x800022f0]:sw t5, 1464(ra)
[0x800022f4]:sw a6, 1472(ra)
[0x800022f8]:lw t3, 1760(a5)
[0x800022fc]:lw t4, 1764(a5)
[0x80002300]:lw s10, 1768(a5)
[0x80002304]:lw s11, 1772(a5)
[0x80002308]:lui t3, 1004500
[0x8000230c]:addi t3, t3, 501
[0x80002310]:lui t4, 523736
[0x80002314]:addi t4, t4, 1967
[0x80002318]:lui s10, 8
[0x8000231c]:addi s11, zero, 0
[0x80002320]:addi a4, zero, 0
[0x80002324]:csrrw zero, fcsr, a4
[0x80002328]:fmul.d t5, t3, s10, dyn
[0x8000232c]:csrrs a6, fcsr, zero

[0x80002328]:fmul.d t5, t3, s10, dyn
[0x8000232c]:csrrs a6, fcsr, zero
[0x80002330]:sw t5, 1480(ra)
[0x80002334]:sw t6, 1488(ra)
[0x80002338]:sw t5, 1496(ra)
[0x8000233c]:sw a6, 1504(ra)
[0x80002340]:lw t3, 1776(a5)
[0x80002344]:lw t4, 1780(a5)
[0x80002348]:lw s10, 1784(a5)
[0x8000234c]:lw s11, 1788(a5)
[0x80002350]:lui t3, 1004500
[0x80002354]:addi t3, t3, 501
[0x80002358]:lui t4, 523736
[0x8000235c]:addi t4, t4, 1967
[0x80002360]:lui s10, 8
[0x80002364]:addi s11, zero, 0
[0x80002368]:addi a4, zero, 32
[0x8000236c]:csrrw zero, fcsr, a4
[0x80002370]:fmul.d t5, t3, s10, dyn
[0x80002374]:csrrs a6, fcsr, zero

[0x80002370]:fmul.d t5, t3, s10, dyn
[0x80002374]:csrrs a6, fcsr, zero
[0x80002378]:sw t5, 1512(ra)
[0x8000237c]:sw t6, 1520(ra)
[0x80002380]:sw t5, 1528(ra)
[0x80002384]:sw a6, 1536(ra)
[0x80002388]:lw t3, 1792(a5)
[0x8000238c]:lw t4, 1796(a5)
[0x80002390]:lw s10, 1800(a5)
[0x80002394]:lw s11, 1804(a5)
[0x80002398]:lui t3, 1004500
[0x8000239c]:addi t3, t3, 501
[0x800023a0]:lui t4, 523736
[0x800023a4]:addi t4, t4, 1967
[0x800023a8]:lui s10, 8
[0x800023ac]:addi s11, zero, 0
[0x800023b0]:addi a4, zero, 64
[0x800023b4]:csrrw zero, fcsr, a4
[0x800023b8]:fmul.d t5, t3, s10, dyn
[0x800023bc]:csrrs a6, fcsr, zero

[0x800023b8]:fmul.d t5, t3, s10, dyn
[0x800023bc]:csrrs a6, fcsr, zero
[0x800023c0]:sw t5, 1544(ra)
[0x800023c4]:sw t6, 1552(ra)
[0x800023c8]:sw t5, 1560(ra)
[0x800023cc]:sw a6, 1568(ra)
[0x800023d0]:lw t3, 1808(a5)
[0x800023d4]:lw t4, 1812(a5)
[0x800023d8]:lw s10, 1816(a5)
[0x800023dc]:lw s11, 1820(a5)
[0x800023e0]:lui t3, 1004500
[0x800023e4]:addi t3, t3, 501
[0x800023e8]:lui t4, 523736
[0x800023ec]:addi t4, t4, 1967
[0x800023f0]:lui s10, 8
[0x800023f4]:addi s11, zero, 0
[0x800023f8]:addi a4, zero, 96
[0x800023fc]:csrrw zero, fcsr, a4
[0x80002400]:fmul.d t5, t3, s10, dyn
[0x80002404]:csrrs a6, fcsr, zero

[0x80002400]:fmul.d t5, t3, s10, dyn
[0x80002404]:csrrs a6, fcsr, zero
[0x80002408]:sw t5, 1576(ra)
[0x8000240c]:sw t6, 1584(ra)
[0x80002410]:sw t5, 1592(ra)
[0x80002414]:sw a6, 1600(ra)
[0x80002418]:lw t3, 1824(a5)
[0x8000241c]:lw t4, 1828(a5)
[0x80002420]:lw s10, 1832(a5)
[0x80002424]:lw s11, 1836(a5)
[0x80002428]:lui t3, 1004500
[0x8000242c]:addi t3, t3, 501
[0x80002430]:lui t4, 523736
[0x80002434]:addi t4, t4, 1967
[0x80002438]:lui s10, 8
[0x8000243c]:addi s11, zero, 0
[0x80002440]:addi a4, zero, 128
[0x80002444]:csrrw zero, fcsr, a4
[0x80002448]:fmul.d t5, t3, s10, dyn
[0x8000244c]:csrrs a6, fcsr, zero

[0x80002448]:fmul.d t5, t3, s10, dyn
[0x8000244c]:csrrs a6, fcsr, zero
[0x80002450]:sw t5, 1608(ra)
[0x80002454]:sw t6, 1616(ra)
[0x80002458]:sw t5, 1624(ra)
[0x8000245c]:sw a6, 1632(ra)
[0x80002460]:lw t3, 1840(a5)
[0x80002464]:lw t4, 1844(a5)
[0x80002468]:lw s10, 1848(a5)
[0x8000246c]:lw s11, 1852(a5)
[0x80002470]:lui t3, 369594
[0x80002474]:addi t3, t3, 2547
[0x80002478]:lui t4, 523766
[0x8000247c]:addi t4, t4, 3171
[0x80002480]:lui s10, 8
[0x80002484]:addi s11, zero, 0
[0x80002488]:addi a4, zero, 0
[0x8000248c]:csrrw zero, fcsr, a4
[0x80002490]:fmul.d t5, t3, s10, dyn
[0x80002494]:csrrs a6, fcsr, zero

[0x80002490]:fmul.d t5, t3, s10, dyn
[0x80002494]:csrrs a6, fcsr, zero
[0x80002498]:sw t5, 1640(ra)
[0x8000249c]:sw t6, 1648(ra)
[0x800024a0]:sw t5, 1656(ra)
[0x800024a4]:sw a6, 1664(ra)
[0x800024a8]:lw t3, 1856(a5)
[0x800024ac]:lw t4, 1860(a5)
[0x800024b0]:lw s10, 1864(a5)
[0x800024b4]:lw s11, 1868(a5)
[0x800024b8]:lui t3, 369594
[0x800024bc]:addi t3, t3, 2547
[0x800024c0]:lui t4, 523766
[0x800024c4]:addi t4, t4, 3171
[0x800024c8]:lui s10, 8
[0x800024cc]:addi s11, zero, 0
[0x800024d0]:addi a4, zero, 32
[0x800024d4]:csrrw zero, fcsr, a4
[0x800024d8]:fmul.d t5, t3, s10, dyn
[0x800024dc]:csrrs a6, fcsr, zero

[0x800024d8]:fmul.d t5, t3, s10, dyn
[0x800024dc]:csrrs a6, fcsr, zero
[0x800024e0]:sw t5, 1672(ra)
[0x800024e4]:sw t6, 1680(ra)
[0x800024e8]:sw t5, 1688(ra)
[0x800024ec]:sw a6, 1696(ra)
[0x800024f0]:lw t3, 1872(a5)
[0x800024f4]:lw t4, 1876(a5)
[0x800024f8]:lw s10, 1880(a5)
[0x800024fc]:lw s11, 1884(a5)
[0x80002500]:lui t3, 369594
[0x80002504]:addi t3, t3, 2547
[0x80002508]:lui t4, 523766
[0x8000250c]:addi t4, t4, 3171
[0x80002510]:lui s10, 8
[0x80002514]:addi s11, zero, 0
[0x80002518]:addi a4, zero, 64
[0x8000251c]:csrrw zero, fcsr, a4
[0x80002520]:fmul.d t5, t3, s10, dyn
[0x80002524]:csrrs a6, fcsr, zero

[0x80002520]:fmul.d t5, t3, s10, dyn
[0x80002524]:csrrs a6, fcsr, zero
[0x80002528]:sw t5, 1704(ra)
[0x8000252c]:sw t6, 1712(ra)
[0x80002530]:sw t5, 1720(ra)
[0x80002534]:sw a6, 1728(ra)
[0x80002538]:lw t3, 1888(a5)
[0x8000253c]:lw t4, 1892(a5)
[0x80002540]:lw s10, 1896(a5)
[0x80002544]:lw s11, 1900(a5)
[0x80002548]:lui t3, 369594
[0x8000254c]:addi t3, t3, 2547
[0x80002550]:lui t4, 523766
[0x80002554]:addi t4, t4, 3171
[0x80002558]:lui s10, 8
[0x8000255c]:addi s11, zero, 0
[0x80002560]:addi a4, zero, 96
[0x80002564]:csrrw zero, fcsr, a4
[0x80002568]:fmul.d t5, t3, s10, dyn
[0x8000256c]:csrrs a6, fcsr, zero

[0x80002568]:fmul.d t5, t3, s10, dyn
[0x8000256c]:csrrs a6, fcsr, zero
[0x80002570]:sw t5, 1736(ra)
[0x80002574]:sw t6, 1744(ra)
[0x80002578]:sw t5, 1752(ra)
[0x8000257c]:sw a6, 1760(ra)
[0x80002580]:lw t3, 1904(a5)
[0x80002584]:lw t4, 1908(a5)
[0x80002588]:lw s10, 1912(a5)
[0x8000258c]:lw s11, 1916(a5)
[0x80002590]:lui t3, 369594
[0x80002594]:addi t3, t3, 2547
[0x80002598]:lui t4, 523766
[0x8000259c]:addi t4, t4, 3171
[0x800025a0]:lui s10, 8
[0x800025a4]:addi s11, zero, 0
[0x800025a8]:addi a4, zero, 128
[0x800025ac]:csrrw zero, fcsr, a4
[0x800025b0]:fmul.d t5, t3, s10, dyn
[0x800025b4]:csrrs a6, fcsr, zero

[0x800025b0]:fmul.d t5, t3, s10, dyn
[0x800025b4]:csrrs a6, fcsr, zero
[0x800025b8]:sw t5, 1768(ra)
[0x800025bc]:sw t6, 1776(ra)
[0x800025c0]:sw t5, 1784(ra)
[0x800025c4]:sw a6, 1792(ra)
[0x800025c8]:lw t3, 1920(a5)
[0x800025cc]:lw t4, 1924(a5)
[0x800025d0]:lw s10, 1928(a5)
[0x800025d4]:lw s11, 1932(a5)
[0x800025d8]:lui t3, 876796
[0x800025dc]:addi t3, t3, 760
[0x800025e0]:lui t4, 523776
[0x800025e4]:addi t4, t4, 1763
[0x800025e8]:lui s10, 8
[0x800025ec]:addi s11, zero, 0
[0x800025f0]:addi a4, zero, 0
[0x800025f4]:csrrw zero, fcsr, a4
[0x800025f8]:fmul.d t5, t3, s10, dyn
[0x800025fc]:csrrs a6, fcsr, zero

[0x800025f8]:fmul.d t5, t3, s10, dyn
[0x800025fc]:csrrs a6, fcsr, zero
[0x80002600]:sw t5, 1800(ra)
[0x80002604]:sw t6, 1808(ra)
[0x80002608]:sw t5, 1816(ra)
[0x8000260c]:sw a6, 1824(ra)
[0x80002610]:lw t3, 1936(a5)
[0x80002614]:lw t4, 1940(a5)
[0x80002618]:lw s10, 1944(a5)
[0x8000261c]:lw s11, 1948(a5)
[0x80002620]:lui t3, 876796
[0x80002624]:addi t3, t3, 760
[0x80002628]:lui t4, 523776
[0x8000262c]:addi t4, t4, 1763
[0x80002630]:lui s10, 8
[0x80002634]:addi s11, zero, 0
[0x80002638]:addi a4, zero, 32
[0x8000263c]:csrrw zero, fcsr, a4
[0x80002640]:fmul.d t5, t3, s10, dyn
[0x80002644]:csrrs a6, fcsr, zero

[0x80002640]:fmul.d t5, t3, s10, dyn
[0x80002644]:csrrs a6, fcsr, zero
[0x80002648]:sw t5, 1832(ra)
[0x8000264c]:sw t6, 1840(ra)
[0x80002650]:sw t5, 1848(ra)
[0x80002654]:sw a6, 1856(ra)
[0x80002658]:lw t3, 1952(a5)
[0x8000265c]:lw t4, 1956(a5)
[0x80002660]:lw s10, 1960(a5)
[0x80002664]:lw s11, 1964(a5)
[0x80002668]:lui t3, 876796
[0x8000266c]:addi t3, t3, 760
[0x80002670]:lui t4, 523776
[0x80002674]:addi t4, t4, 1763
[0x80002678]:lui s10, 8
[0x8000267c]:addi s11, zero, 0
[0x80002680]:addi a4, zero, 64
[0x80002684]:csrrw zero, fcsr, a4
[0x80002688]:fmul.d t5, t3, s10, dyn
[0x8000268c]:csrrs a6, fcsr, zero

[0x80002688]:fmul.d t5, t3, s10, dyn
[0x8000268c]:csrrs a6, fcsr, zero
[0x80002690]:sw t5, 1864(ra)
[0x80002694]:sw t6, 1872(ra)
[0x80002698]:sw t5, 1880(ra)
[0x8000269c]:sw a6, 1888(ra)
[0x800026a0]:lw t3, 1968(a5)
[0x800026a4]:lw t4, 1972(a5)
[0x800026a8]:lw s10, 1976(a5)
[0x800026ac]:lw s11, 1980(a5)
[0x800026b0]:lui t3, 876796
[0x800026b4]:addi t3, t3, 760
[0x800026b8]:lui t4, 523776
[0x800026bc]:addi t4, t4, 1763
[0x800026c0]:lui s10, 8
[0x800026c4]:addi s11, zero, 0
[0x800026c8]:addi a4, zero, 96
[0x800026cc]:csrrw zero, fcsr, a4
[0x800026d0]:fmul.d t5, t3, s10, dyn
[0x800026d4]:csrrs a6, fcsr, zero

[0x800026d0]:fmul.d t5, t3, s10, dyn
[0x800026d4]:csrrs a6, fcsr, zero
[0x800026d8]:sw t5, 1896(ra)
[0x800026dc]:sw t6, 1904(ra)
[0x800026e0]:sw t5, 1912(ra)
[0x800026e4]:sw a6, 1920(ra)
[0x800026e8]:lw t3, 1984(a5)
[0x800026ec]:lw t4, 1988(a5)
[0x800026f0]:lw s10, 1992(a5)
[0x800026f4]:lw s11, 1996(a5)
[0x800026f8]:lui t3, 876796
[0x800026fc]:addi t3, t3, 760
[0x80002700]:lui t4, 523776
[0x80002704]:addi t4, t4, 1763
[0x80002708]:lui s10, 8
[0x8000270c]:addi s11, zero, 0
[0x80002710]:addi a4, zero, 128
[0x80002714]:csrrw zero, fcsr, a4
[0x80002718]:fmul.d t5, t3, s10, dyn
[0x8000271c]:csrrs a6, fcsr, zero

[0x80002718]:fmul.d t5, t3, s10, dyn
[0x8000271c]:csrrs a6, fcsr, zero
[0x80002720]:sw t5, 1928(ra)
[0x80002724]:sw t6, 1936(ra)
[0x80002728]:sw t5, 1944(ra)
[0x8000272c]:sw a6, 1952(ra)
[0x80002730]:lw t3, 2000(a5)
[0x80002734]:lw t4, 2004(a5)
[0x80002738]:lw s10, 2008(a5)
[0x8000273c]:lw s11, 2012(a5)
[0x80002740]:lui t3, 742578
[0x80002744]:addi t3, t3, 463
[0x80002748]:lui t4, 523626
[0x8000274c]:addi t4, t4, 1713
[0x80002750]:lui s10, 8
[0x80002754]:addi s11, zero, 0
[0x80002758]:addi a4, zero, 0
[0x8000275c]:csrrw zero, fcsr, a4
[0x80002760]:fmul.d t5, t3, s10, dyn
[0x80002764]:csrrs a6, fcsr, zero

[0x80002760]:fmul.d t5, t3, s10, dyn
[0x80002764]:csrrs a6, fcsr, zero
[0x80002768]:sw t5, 1960(ra)
[0x8000276c]:sw t6, 1968(ra)
[0x80002770]:sw t5, 1976(ra)
[0x80002774]:sw a6, 1984(ra)
[0x80002778]:lw t3, 2016(a5)
[0x8000277c]:lw t4, 2020(a5)
[0x80002780]:lw s10, 2024(a5)
[0x80002784]:lw s11, 2028(a5)
[0x80002788]:lui t3, 742578
[0x8000278c]:addi t3, t3, 463
[0x80002790]:lui t4, 523626
[0x80002794]:addi t4, t4, 1713
[0x80002798]:lui s10, 8
[0x8000279c]:addi s11, zero, 0
[0x800027a0]:addi a4, zero, 32
[0x800027a4]:csrrw zero, fcsr, a4
[0x800027a8]:fmul.d t5, t3, s10, dyn
[0x800027ac]:csrrs a6, fcsr, zero

[0x800027a8]:fmul.d t5, t3, s10, dyn
[0x800027ac]:csrrs a6, fcsr, zero
[0x800027b0]:sw t5, 1992(ra)
[0x800027b4]:sw t6, 2000(ra)
[0x800027b8]:sw t5, 2008(ra)
[0x800027bc]:sw a6, 2016(ra)
[0x800027c0]:lw t3, 2032(a5)
[0x800027c4]:lw t4, 2036(a5)
[0x800027c8]:lw s10, 2040(a5)
[0x800027cc]:lw s11, 2044(a5)
[0x800027d0]:lui t3, 742578
[0x800027d4]:addi t3, t3, 463
[0x800027d8]:lui t4, 523626
[0x800027dc]:addi t4, t4, 1713
[0x800027e0]:lui s10, 8
[0x800027e4]:addi s11, zero, 0
[0x800027e8]:addi a4, zero, 64
[0x800027ec]:csrrw zero, fcsr, a4
[0x800027f0]:fmul.d t5, t3, s10, dyn
[0x800027f4]:csrrs a6, fcsr, zero

[0x800027f0]:fmul.d t5, t3, s10, dyn
[0x800027f4]:csrrs a6, fcsr, zero
[0x800027f8]:sw t5, 2024(ra)
[0x800027fc]:sw t6, 2032(ra)
[0x80002800]:sw t5, 2040(ra)
[0x80002804]:addi ra, ra, 2040
[0x80002808]:sw a6, 8(ra)
[0x8000280c]:lui a4, 1
[0x80002810]:addi a4, a4, 2048
[0x80002814]:add a5, a5, a4
[0x80002818]:lw t3, 0(a5)
[0x8000281c]:sub a5, a5, a4
[0x80002820]:lui a4, 1
[0x80002824]:addi a4, a4, 2048
[0x80002828]:add a5, a5, a4
[0x8000282c]:lw t4, 4(a5)
[0x80002830]:sub a5, a5, a4
[0x80002834]:lui a4, 1
[0x80002838]:addi a4, a4, 2048
[0x8000283c]:add a5, a5, a4
[0x80002840]:lw s10, 8(a5)
[0x80002844]:sub a5, a5, a4
[0x80002848]:lui a4, 1
[0x8000284c]:addi a4, a4, 2048
[0x80002850]:add a5, a5, a4
[0x80002854]:lw s11, 12(a5)
[0x80002858]:sub a5, a5, a4
[0x8000285c]:lui t3, 742578
[0x80002860]:addi t3, t3, 463
[0x80002864]:lui t4, 523626
[0x80002868]:addi t4, t4, 1713
[0x8000286c]:lui s10, 8
[0x80002870]:addi s11, zero, 0
[0x80002874]:addi a4, zero, 96
[0x80002878]:csrrw zero, fcsr, a4
[0x8000287c]:fmul.d t5, t3, s10, dyn
[0x80002880]:csrrs a6, fcsr, zero

[0x8000287c]:fmul.d t5, t3, s10, dyn
[0x80002880]:csrrs a6, fcsr, zero
[0x80002884]:sw t5, 16(ra)
[0x80002888]:sw t6, 24(ra)
[0x8000288c]:sw t5, 32(ra)
[0x80002890]:sw a6, 40(ra)
[0x80002894]:lui a4, 1
[0x80002898]:addi a4, a4, 2048
[0x8000289c]:add a5, a5, a4
[0x800028a0]:lw t3, 16(a5)
[0x800028a4]:sub a5, a5, a4
[0x800028a8]:lui a4, 1
[0x800028ac]:addi a4, a4, 2048
[0x800028b0]:add a5, a5, a4
[0x800028b4]:lw t4, 20(a5)
[0x800028b8]:sub a5, a5, a4
[0x800028bc]:lui a4, 1
[0x800028c0]:addi a4, a4, 2048
[0x800028c4]:add a5, a5, a4
[0x800028c8]:lw s10, 24(a5)
[0x800028cc]:sub a5, a5, a4
[0x800028d0]:lui a4, 1
[0x800028d4]:addi a4, a4, 2048
[0x800028d8]:add a5, a5, a4
[0x800028dc]:lw s11, 28(a5)
[0x800028e0]:sub a5, a5, a4
[0x800028e4]:lui t3, 742578
[0x800028e8]:addi t3, t3, 463
[0x800028ec]:lui t4, 523626
[0x800028f0]:addi t4, t4, 1713
[0x800028f4]:lui s10, 8
[0x800028f8]:addi s11, zero, 0
[0x800028fc]:addi a4, zero, 128
[0x80002900]:csrrw zero, fcsr, a4
[0x80002904]:fmul.d t5, t3, s10, dyn
[0x80002908]:csrrs a6, fcsr, zero

[0x80002904]:fmul.d t5, t3, s10, dyn
[0x80002908]:csrrs a6, fcsr, zero
[0x8000290c]:sw t5, 48(ra)
[0x80002910]:sw t6, 56(ra)
[0x80002914]:sw t5, 64(ra)
[0x80002918]:sw a6, 72(ra)
[0x8000291c]:lui a4, 1
[0x80002920]:addi a4, a4, 2048
[0x80002924]:add a5, a5, a4
[0x80002928]:lw t3, 32(a5)
[0x8000292c]:sub a5, a5, a4
[0x80002930]:lui a4, 1
[0x80002934]:addi a4, a4, 2048
[0x80002938]:add a5, a5, a4
[0x8000293c]:lw t4, 36(a5)
[0x80002940]:sub a5, a5, a4
[0x80002944]:lui a4, 1
[0x80002948]:addi a4, a4, 2048
[0x8000294c]:add a5, a5, a4
[0x80002950]:lw s10, 40(a5)
[0x80002954]:sub a5, a5, a4
[0x80002958]:lui a4, 1
[0x8000295c]:addi a4, a4, 2048
[0x80002960]:add a5, a5, a4
[0x80002964]:lw s11, 44(a5)
[0x80002968]:sub a5, a5, a4
[0x8000296c]:lui t3, 932302
[0x80002970]:addi t3, t3, 2646
[0x80002974]:lui t4, 523981
[0x80002978]:addi t4, t4, 2648
[0x8000297c]:lui s10, 8
[0x80002980]:addi s11, zero, 0
[0x80002984]:addi a4, zero, 0
[0x80002988]:csrrw zero, fcsr, a4
[0x8000298c]:fmul.d t5, t3, s10, dyn
[0x80002990]:csrrs a6, fcsr, zero

[0x8000298c]:fmul.d t5, t3, s10, dyn
[0x80002990]:csrrs a6, fcsr, zero
[0x80002994]:sw t5, 80(ra)
[0x80002998]:sw t6, 88(ra)
[0x8000299c]:sw t5, 96(ra)
[0x800029a0]:sw a6, 104(ra)
[0x800029a4]:lui a4, 1
[0x800029a8]:addi a4, a4, 2048
[0x800029ac]:add a5, a5, a4
[0x800029b0]:lw t3, 48(a5)
[0x800029b4]:sub a5, a5, a4
[0x800029b8]:lui a4, 1
[0x800029bc]:addi a4, a4, 2048
[0x800029c0]:add a5, a5, a4
[0x800029c4]:lw t4, 52(a5)
[0x800029c8]:sub a5, a5, a4
[0x800029cc]:lui a4, 1
[0x800029d0]:addi a4, a4, 2048
[0x800029d4]:add a5, a5, a4
[0x800029d8]:lw s10, 56(a5)
[0x800029dc]:sub a5, a5, a4
[0x800029e0]:lui a4, 1
[0x800029e4]:addi a4, a4, 2048
[0x800029e8]:add a5, a5, a4
[0x800029ec]:lw s11, 60(a5)
[0x800029f0]:sub a5, a5, a4
[0x800029f4]:lui t3, 932302
[0x800029f8]:addi t3, t3, 2646
[0x800029fc]:lui t4, 523981
[0x80002a00]:addi t4, t4, 2648
[0x80002a04]:lui s10, 8
[0x80002a08]:addi s11, zero, 0
[0x80002a0c]:addi a4, zero, 32
[0x80002a10]:csrrw zero, fcsr, a4
[0x80002a14]:fmul.d t5, t3, s10, dyn
[0x80002a18]:csrrs a6, fcsr, zero

[0x80002a14]:fmul.d t5, t3, s10, dyn
[0x80002a18]:csrrs a6, fcsr, zero
[0x80002a1c]:sw t5, 112(ra)
[0x80002a20]:sw t6, 120(ra)
[0x80002a24]:sw t5, 128(ra)
[0x80002a28]:sw a6, 136(ra)
[0x80002a2c]:lui a4, 1
[0x80002a30]:addi a4, a4, 2048
[0x80002a34]:add a5, a5, a4
[0x80002a38]:lw t3, 64(a5)
[0x80002a3c]:sub a5, a5, a4
[0x80002a40]:lui a4, 1
[0x80002a44]:addi a4, a4, 2048
[0x80002a48]:add a5, a5, a4
[0x80002a4c]:lw t4, 68(a5)
[0x80002a50]:sub a5, a5, a4
[0x80002a54]:lui a4, 1
[0x80002a58]:addi a4, a4, 2048
[0x80002a5c]:add a5, a5, a4
[0x80002a60]:lw s10, 72(a5)
[0x80002a64]:sub a5, a5, a4
[0x80002a68]:lui a4, 1
[0x80002a6c]:addi a4, a4, 2048
[0x80002a70]:add a5, a5, a4
[0x80002a74]:lw s11, 76(a5)
[0x80002a78]:sub a5, a5, a4
[0x80002a7c]:lui t3, 932302
[0x80002a80]:addi t3, t3, 2646
[0x80002a84]:lui t4, 523981
[0x80002a88]:addi t4, t4, 2648
[0x80002a8c]:lui s10, 8
[0x80002a90]:addi s11, zero, 0
[0x80002a94]:addi a4, zero, 64
[0x80002a98]:csrrw zero, fcsr, a4
[0x80002a9c]:fmul.d t5, t3, s10, dyn
[0x80002aa0]:csrrs a6, fcsr, zero

[0x80002a9c]:fmul.d t5, t3, s10, dyn
[0x80002aa0]:csrrs a6, fcsr, zero
[0x80002aa4]:sw t5, 144(ra)
[0x80002aa8]:sw t6, 152(ra)
[0x80002aac]:sw t5, 160(ra)
[0x80002ab0]:sw a6, 168(ra)
[0x80002ab4]:lui a4, 1
[0x80002ab8]:addi a4, a4, 2048
[0x80002abc]:add a5, a5, a4
[0x80002ac0]:lw t3, 80(a5)
[0x80002ac4]:sub a5, a5, a4
[0x80002ac8]:lui a4, 1
[0x80002acc]:addi a4, a4, 2048
[0x80002ad0]:add a5, a5, a4
[0x80002ad4]:lw t4, 84(a5)
[0x80002ad8]:sub a5, a5, a4
[0x80002adc]:lui a4, 1
[0x80002ae0]:addi a4, a4, 2048
[0x80002ae4]:add a5, a5, a4
[0x80002ae8]:lw s10, 88(a5)
[0x80002aec]:sub a5, a5, a4
[0x80002af0]:lui a4, 1
[0x80002af4]:addi a4, a4, 2048
[0x80002af8]:add a5, a5, a4
[0x80002afc]:lw s11, 92(a5)
[0x80002b00]:sub a5, a5, a4
[0x80002b04]:lui t3, 932302
[0x80002b08]:addi t3, t3, 2646
[0x80002b0c]:lui t4, 523981
[0x80002b10]:addi t4, t4, 2648
[0x80002b14]:lui s10, 8
[0x80002b18]:addi s11, zero, 0
[0x80002b1c]:addi a4, zero, 96
[0x80002b20]:csrrw zero, fcsr, a4
[0x80002b24]:fmul.d t5, t3, s10, dyn
[0x80002b28]:csrrs a6, fcsr, zero

[0x80002b24]:fmul.d t5, t3, s10, dyn
[0x80002b28]:csrrs a6, fcsr, zero
[0x80002b2c]:sw t5, 176(ra)
[0x80002b30]:sw t6, 184(ra)
[0x80002b34]:sw t5, 192(ra)
[0x80002b38]:sw a6, 200(ra)
[0x80002b3c]:lui a4, 1
[0x80002b40]:addi a4, a4, 2048
[0x80002b44]:add a5, a5, a4
[0x80002b48]:lw t3, 96(a5)
[0x80002b4c]:sub a5, a5, a4
[0x80002b50]:lui a4, 1
[0x80002b54]:addi a4, a4, 2048
[0x80002b58]:add a5, a5, a4
[0x80002b5c]:lw t4, 100(a5)
[0x80002b60]:sub a5, a5, a4
[0x80002b64]:lui a4, 1
[0x80002b68]:addi a4, a4, 2048
[0x80002b6c]:add a5, a5, a4
[0x80002b70]:lw s10, 104(a5)
[0x80002b74]:sub a5, a5, a4
[0x80002b78]:lui a4, 1
[0x80002b7c]:addi a4, a4, 2048
[0x80002b80]:add a5, a5, a4
[0x80002b84]:lw s11, 108(a5)
[0x80002b88]:sub a5, a5, a4
[0x80002b8c]:lui t3, 932302
[0x80002b90]:addi t3, t3, 2646
[0x80002b94]:lui t4, 523981
[0x80002b98]:addi t4, t4, 2648
[0x80002b9c]:lui s10, 8
[0x80002ba0]:addi s11, zero, 0
[0x80002ba4]:addi a4, zero, 128
[0x80002ba8]:csrrw zero, fcsr, a4
[0x80002bac]:fmul.d t5, t3, s10, dyn
[0x80002bb0]:csrrs a6, fcsr, zero

[0x80002bac]:fmul.d t5, t3, s10, dyn
[0x80002bb0]:csrrs a6, fcsr, zero
[0x80002bb4]:sw t5, 208(ra)
[0x80002bb8]:sw t6, 216(ra)
[0x80002bbc]:sw t5, 224(ra)
[0x80002bc0]:sw a6, 232(ra)
[0x80002bc4]:lui a4, 1
[0x80002bc8]:addi a4, a4, 2048
[0x80002bcc]:add a5, a5, a4
[0x80002bd0]:lw t3, 112(a5)
[0x80002bd4]:sub a5, a5, a4
[0x80002bd8]:lui a4, 1
[0x80002bdc]:addi a4, a4, 2048
[0x80002be0]:add a5, a5, a4
[0x80002be4]:lw t4, 116(a5)
[0x80002be8]:sub a5, a5, a4
[0x80002bec]:lui a4, 1
[0x80002bf0]:addi a4, a4, 2048
[0x80002bf4]:add a5, a5, a4
[0x80002bf8]:lw s10, 120(a5)
[0x80002bfc]:sub a5, a5, a4
[0x80002c00]:lui a4, 1
[0x80002c04]:addi a4, a4, 2048
[0x80002c08]:add a5, a5, a4
[0x80002c0c]:lw s11, 124(a5)
[0x80002c10]:sub a5, a5, a4
[0x80002c14]:lui t3, 863861
[0x80002c18]:addi t3, t3, 3698
[0x80002c1c]:lui t4, 523811
[0x80002c20]:addi t4, t4, 2723
[0x80002c24]:lui s10, 8
[0x80002c28]:addi s11, zero, 0
[0x80002c2c]:addi a4, zero, 0
[0x80002c30]:csrrw zero, fcsr, a4
[0x80002c34]:fmul.d t5, t3, s10, dyn
[0x80002c38]:csrrs a6, fcsr, zero

[0x80002c34]:fmul.d t5, t3, s10, dyn
[0x80002c38]:csrrs a6, fcsr, zero
[0x80002c3c]:sw t5, 240(ra)
[0x80002c40]:sw t6, 248(ra)
[0x80002c44]:sw t5, 256(ra)
[0x80002c48]:sw a6, 264(ra)
[0x80002c4c]:lui a4, 1
[0x80002c50]:addi a4, a4, 2048
[0x80002c54]:add a5, a5, a4
[0x80002c58]:lw t3, 128(a5)
[0x80002c5c]:sub a5, a5, a4
[0x80002c60]:lui a4, 1
[0x80002c64]:addi a4, a4, 2048
[0x80002c68]:add a5, a5, a4
[0x80002c6c]:lw t4, 132(a5)
[0x80002c70]:sub a5, a5, a4
[0x80002c74]:lui a4, 1
[0x80002c78]:addi a4, a4, 2048
[0x80002c7c]:add a5, a5, a4
[0x80002c80]:lw s10, 136(a5)
[0x80002c84]:sub a5, a5, a4
[0x80002c88]:lui a4, 1
[0x80002c8c]:addi a4, a4, 2048
[0x80002c90]:add a5, a5, a4
[0x80002c94]:lw s11, 140(a5)
[0x80002c98]:sub a5, a5, a4
[0x80002c9c]:lui t3, 863861
[0x80002ca0]:addi t3, t3, 3698
[0x80002ca4]:lui t4, 523811
[0x80002ca8]:addi t4, t4, 2723
[0x80002cac]:lui s10, 8
[0x80002cb0]:addi s11, zero, 0
[0x80002cb4]:addi a4, zero, 32
[0x80002cb8]:csrrw zero, fcsr, a4
[0x80002cbc]:fmul.d t5, t3, s10, dyn
[0x80002cc0]:csrrs a6, fcsr, zero

[0x80002cbc]:fmul.d t5, t3, s10, dyn
[0x80002cc0]:csrrs a6, fcsr, zero
[0x80002cc4]:sw t5, 272(ra)
[0x80002cc8]:sw t6, 280(ra)
[0x80002ccc]:sw t5, 288(ra)
[0x80002cd0]:sw a6, 296(ra)
[0x80002cd4]:lui a4, 1
[0x80002cd8]:addi a4, a4, 2048
[0x80002cdc]:add a5, a5, a4
[0x80002ce0]:lw t3, 144(a5)
[0x80002ce4]:sub a5, a5, a4
[0x80002ce8]:lui a4, 1
[0x80002cec]:addi a4, a4, 2048
[0x80002cf0]:add a5, a5, a4
[0x80002cf4]:lw t4, 148(a5)
[0x80002cf8]:sub a5, a5, a4
[0x80002cfc]:lui a4, 1
[0x80002d00]:addi a4, a4, 2048
[0x80002d04]:add a5, a5, a4
[0x80002d08]:lw s10, 152(a5)
[0x80002d0c]:sub a5, a5, a4
[0x80002d10]:lui a4, 1
[0x80002d14]:addi a4, a4, 2048
[0x80002d18]:add a5, a5, a4
[0x80002d1c]:lw s11, 156(a5)
[0x80002d20]:sub a5, a5, a4
[0x80002d24]:lui t3, 863861
[0x80002d28]:addi t3, t3, 3698
[0x80002d2c]:lui t4, 523811
[0x80002d30]:addi t4, t4, 2723
[0x80002d34]:lui s10, 8
[0x80002d38]:addi s11, zero, 0
[0x80002d3c]:addi a4, zero, 64
[0x80002d40]:csrrw zero, fcsr, a4
[0x80002d44]:fmul.d t5, t3, s10, dyn
[0x80002d48]:csrrs a6, fcsr, zero

[0x80002d44]:fmul.d t5, t3, s10, dyn
[0x80002d48]:csrrs a6, fcsr, zero
[0x80002d4c]:sw t5, 304(ra)
[0x80002d50]:sw t6, 312(ra)
[0x80002d54]:sw t5, 320(ra)
[0x80002d58]:sw a6, 328(ra)
[0x80002d5c]:lui a4, 1
[0x80002d60]:addi a4, a4, 2048
[0x80002d64]:add a5, a5, a4
[0x80002d68]:lw t3, 160(a5)
[0x80002d6c]:sub a5, a5, a4
[0x80002d70]:lui a4, 1
[0x80002d74]:addi a4, a4, 2048
[0x80002d78]:add a5, a5, a4
[0x80002d7c]:lw t4, 164(a5)
[0x80002d80]:sub a5, a5, a4
[0x80002d84]:lui a4, 1
[0x80002d88]:addi a4, a4, 2048
[0x80002d8c]:add a5, a5, a4
[0x80002d90]:lw s10, 168(a5)
[0x80002d94]:sub a5, a5, a4
[0x80002d98]:lui a4, 1
[0x80002d9c]:addi a4, a4, 2048
[0x80002da0]:add a5, a5, a4
[0x80002da4]:lw s11, 172(a5)
[0x80002da8]:sub a5, a5, a4
[0x80002dac]:lui t3, 863861
[0x80002db0]:addi t3, t3, 3698
[0x80002db4]:lui t4, 523811
[0x80002db8]:addi t4, t4, 2723
[0x80002dbc]:lui s10, 8
[0x80002dc0]:addi s11, zero, 0
[0x80002dc4]:addi a4, zero, 96
[0x80002dc8]:csrrw zero, fcsr, a4
[0x80002dcc]:fmul.d t5, t3, s10, dyn
[0x80002dd0]:csrrs a6, fcsr, zero

[0x80002dcc]:fmul.d t5, t3, s10, dyn
[0x80002dd0]:csrrs a6, fcsr, zero
[0x80002dd4]:sw t5, 336(ra)
[0x80002dd8]:sw t6, 344(ra)
[0x80002ddc]:sw t5, 352(ra)
[0x80002de0]:sw a6, 360(ra)
[0x80002de4]:lui a4, 1
[0x80002de8]:addi a4, a4, 2048
[0x80002dec]:add a5, a5, a4
[0x80002df0]:lw t3, 176(a5)
[0x80002df4]:sub a5, a5, a4
[0x80002df8]:lui a4, 1
[0x80002dfc]:addi a4, a4, 2048
[0x80002e00]:add a5, a5, a4
[0x80002e04]:lw t4, 180(a5)
[0x80002e08]:sub a5, a5, a4
[0x80002e0c]:lui a4, 1
[0x80002e10]:addi a4, a4, 2048
[0x80002e14]:add a5, a5, a4
[0x80002e18]:lw s10, 184(a5)
[0x80002e1c]:sub a5, a5, a4
[0x80002e20]:lui a4, 1
[0x80002e24]:addi a4, a4, 2048
[0x80002e28]:add a5, a5, a4
[0x80002e2c]:lw s11, 188(a5)
[0x80002e30]:sub a5, a5, a4
[0x80002e34]:lui t3, 863861
[0x80002e38]:addi t3, t3, 3698
[0x80002e3c]:lui t4, 523811
[0x80002e40]:addi t4, t4, 2723
[0x80002e44]:lui s10, 8
[0x80002e48]:addi s11, zero, 0
[0x80002e4c]:addi a4, zero, 128
[0x80002e50]:csrrw zero, fcsr, a4
[0x80002e54]:fmul.d t5, t3, s10, dyn
[0x80002e58]:csrrs a6, fcsr, zero

[0x80002e54]:fmul.d t5, t3, s10, dyn
[0x80002e58]:csrrs a6, fcsr, zero
[0x80002e5c]:sw t5, 368(ra)
[0x80002e60]:sw t6, 376(ra)
[0x80002e64]:sw t5, 384(ra)
[0x80002e68]:sw a6, 392(ra)
[0x80002e6c]:lui a4, 1
[0x80002e70]:addi a4, a4, 2048
[0x80002e74]:add a5, a5, a4
[0x80002e78]:lw t3, 192(a5)
[0x80002e7c]:sub a5, a5, a4
[0x80002e80]:lui a4, 1
[0x80002e84]:addi a4, a4, 2048
[0x80002e88]:add a5, a5, a4
[0x80002e8c]:lw t4, 196(a5)
[0x80002e90]:sub a5, a5, a4
[0x80002e94]:lui a4, 1
[0x80002e98]:addi a4, a4, 2048
[0x80002e9c]:add a5, a5, a4
[0x80002ea0]:lw s10, 200(a5)
[0x80002ea4]:sub a5, a5, a4
[0x80002ea8]:lui a4, 1
[0x80002eac]:addi a4, a4, 2048
[0x80002eb0]:add a5, a5, a4
[0x80002eb4]:lw s11, 204(a5)
[0x80002eb8]:sub a5, a5, a4
[0x80002ebc]:lui t3, 941368
[0x80002ec0]:addi t3, t3, 545
[0x80002ec4]:lui t4, 523891
[0x80002ec8]:addi t4, t4, 2341
[0x80002ecc]:lui s10, 8
[0x80002ed0]:addi s11, zero, 0
[0x80002ed4]:addi a4, zero, 0
[0x80002ed8]:csrrw zero, fcsr, a4
[0x80002edc]:fmul.d t5, t3, s10, dyn
[0x80002ee0]:csrrs a6, fcsr, zero

[0x80002edc]:fmul.d t5, t3, s10, dyn
[0x80002ee0]:csrrs a6, fcsr, zero
[0x80002ee4]:sw t5, 400(ra)
[0x80002ee8]:sw t6, 408(ra)
[0x80002eec]:sw t5, 416(ra)
[0x80002ef0]:sw a6, 424(ra)
[0x80002ef4]:lui a4, 1
[0x80002ef8]:addi a4, a4, 2048
[0x80002efc]:add a5, a5, a4
[0x80002f00]:lw t3, 208(a5)
[0x80002f04]:sub a5, a5, a4
[0x80002f08]:lui a4, 1
[0x80002f0c]:addi a4, a4, 2048
[0x80002f10]:add a5, a5, a4
[0x80002f14]:lw t4, 212(a5)
[0x80002f18]:sub a5, a5, a4
[0x80002f1c]:lui a4, 1
[0x80002f20]:addi a4, a4, 2048
[0x80002f24]:add a5, a5, a4
[0x80002f28]:lw s10, 216(a5)
[0x80002f2c]:sub a5, a5, a4
[0x80002f30]:lui a4, 1
[0x80002f34]:addi a4, a4, 2048
[0x80002f38]:add a5, a5, a4
[0x80002f3c]:lw s11, 220(a5)
[0x80002f40]:sub a5, a5, a4
[0x80002f44]:lui t3, 941368
[0x80002f48]:addi t3, t3, 545
[0x80002f4c]:lui t4, 523891
[0x80002f50]:addi t4, t4, 2341
[0x80002f54]:lui s10, 8
[0x80002f58]:addi s11, zero, 0
[0x80002f5c]:addi a4, zero, 32
[0x80002f60]:csrrw zero, fcsr, a4
[0x80002f64]:fmul.d t5, t3, s10, dyn
[0x80002f68]:csrrs a6, fcsr, zero

[0x80002f64]:fmul.d t5, t3, s10, dyn
[0x80002f68]:csrrs a6, fcsr, zero
[0x80002f6c]:sw t5, 432(ra)
[0x80002f70]:sw t6, 440(ra)
[0x80002f74]:sw t5, 448(ra)
[0x80002f78]:sw a6, 456(ra)
[0x80002f7c]:lui a4, 1
[0x80002f80]:addi a4, a4, 2048
[0x80002f84]:add a5, a5, a4
[0x80002f88]:lw t3, 224(a5)
[0x80002f8c]:sub a5, a5, a4
[0x80002f90]:lui a4, 1
[0x80002f94]:addi a4, a4, 2048
[0x80002f98]:add a5, a5, a4
[0x80002f9c]:lw t4, 228(a5)
[0x80002fa0]:sub a5, a5, a4
[0x80002fa4]:lui a4, 1
[0x80002fa8]:addi a4, a4, 2048
[0x80002fac]:add a5, a5, a4
[0x80002fb0]:lw s10, 232(a5)
[0x80002fb4]:sub a5, a5, a4
[0x80002fb8]:lui a4, 1
[0x80002fbc]:addi a4, a4, 2048
[0x80002fc0]:add a5, a5, a4
[0x80002fc4]:lw s11, 236(a5)
[0x80002fc8]:sub a5, a5, a4
[0x80002fcc]:lui t3, 941368
[0x80002fd0]:addi t3, t3, 545
[0x80002fd4]:lui t4, 523891
[0x80002fd8]:addi t4, t4, 2341
[0x80002fdc]:lui s10, 8
[0x80002fe0]:addi s11, zero, 0
[0x80002fe4]:addi a4, zero, 64
[0x80002fe8]:csrrw zero, fcsr, a4
[0x80002fec]:fmul.d t5, t3, s10, dyn
[0x80002ff0]:csrrs a6, fcsr, zero

[0x80002fec]:fmul.d t5, t3, s10, dyn
[0x80002ff0]:csrrs a6, fcsr, zero
[0x80002ff4]:sw t5, 464(ra)
[0x80002ff8]:sw t6, 472(ra)
[0x80002ffc]:sw t5, 480(ra)
[0x80003000]:sw a6, 488(ra)
[0x80003004]:lui a4, 1
[0x80003008]:addi a4, a4, 2048
[0x8000300c]:add a5, a5, a4
[0x80003010]:lw t3, 240(a5)
[0x80003014]:sub a5, a5, a4
[0x80003018]:lui a4, 1
[0x8000301c]:addi a4, a4, 2048
[0x80003020]:add a5, a5, a4
[0x80003024]:lw t4, 244(a5)
[0x80003028]:sub a5, a5, a4
[0x8000302c]:lui a4, 1
[0x80003030]:addi a4, a4, 2048
[0x80003034]:add a5, a5, a4
[0x80003038]:lw s10, 248(a5)
[0x8000303c]:sub a5, a5, a4
[0x80003040]:lui a4, 1
[0x80003044]:addi a4, a4, 2048
[0x80003048]:add a5, a5, a4
[0x8000304c]:lw s11, 252(a5)
[0x80003050]:sub a5, a5, a4
[0x80003054]:lui t3, 941368
[0x80003058]:addi t3, t3, 545
[0x8000305c]:lui t4, 523891
[0x80003060]:addi t4, t4, 2341
[0x80003064]:lui s10, 8
[0x80003068]:addi s11, zero, 0
[0x8000306c]:addi a4, zero, 96
[0x80003070]:csrrw zero, fcsr, a4
[0x80003074]:fmul.d t5, t3, s10, dyn
[0x80003078]:csrrs a6, fcsr, zero

[0x80003074]:fmul.d t5, t3, s10, dyn
[0x80003078]:csrrs a6, fcsr, zero
[0x8000307c]:sw t5, 496(ra)
[0x80003080]:sw t6, 504(ra)
[0x80003084]:sw t5, 512(ra)
[0x80003088]:sw a6, 520(ra)
[0x8000308c]:lui a4, 1
[0x80003090]:addi a4, a4, 2048
[0x80003094]:add a5, a5, a4
[0x80003098]:lw t3, 256(a5)
[0x8000309c]:sub a5, a5, a4
[0x800030a0]:lui a4, 1
[0x800030a4]:addi a4, a4, 2048
[0x800030a8]:add a5, a5, a4
[0x800030ac]:lw t4, 260(a5)
[0x800030b0]:sub a5, a5, a4
[0x800030b4]:lui a4, 1
[0x800030b8]:addi a4, a4, 2048
[0x800030bc]:add a5, a5, a4
[0x800030c0]:lw s10, 264(a5)
[0x800030c4]:sub a5, a5, a4
[0x800030c8]:lui a4, 1
[0x800030cc]:addi a4, a4, 2048
[0x800030d0]:add a5, a5, a4
[0x800030d4]:lw s11, 268(a5)
[0x800030d8]:sub a5, a5, a4
[0x800030dc]:lui t3, 941368
[0x800030e0]:addi t3, t3, 545
[0x800030e4]:lui t4, 523891
[0x800030e8]:addi t4, t4, 2341
[0x800030ec]:lui s10, 8
[0x800030f0]:addi s11, zero, 0
[0x800030f4]:addi a4, zero, 128
[0x800030f8]:csrrw zero, fcsr, a4
[0x800030fc]:fmul.d t5, t3, s10, dyn
[0x80003100]:csrrs a6, fcsr, zero

[0x800030fc]:fmul.d t5, t3, s10, dyn
[0x80003100]:csrrs a6, fcsr, zero
[0x80003104]:sw t5, 528(ra)
[0x80003108]:sw t6, 536(ra)
[0x8000310c]:sw t5, 544(ra)
[0x80003110]:sw a6, 552(ra)
[0x80003114]:lui a4, 1
[0x80003118]:addi a4, a4, 2048
[0x8000311c]:add a5, a5, a4
[0x80003120]:lw t3, 272(a5)
[0x80003124]:sub a5, a5, a4
[0x80003128]:lui a4, 1
[0x8000312c]:addi a4, a4, 2048
[0x80003130]:add a5, a5, a4
[0x80003134]:lw t4, 276(a5)
[0x80003138]:sub a5, a5, a4
[0x8000313c]:lui a4, 1
[0x80003140]:addi a4, a4, 2048
[0x80003144]:add a5, a5, a4
[0x80003148]:lw s10, 280(a5)
[0x8000314c]:sub a5, a5, a4
[0x80003150]:lui a4, 1
[0x80003154]:addi a4, a4, 2048
[0x80003158]:add a5, a5, a4
[0x8000315c]:lw s11, 284(a5)
[0x80003160]:sub a5, a5, a4
[0x80003164]:lui t3, 651467
[0x80003168]:addi t3, t3, 3819
[0x8000316c]:lui t4, 523462
[0x80003170]:addi t4, t4, 1101
[0x80003174]:lui s10, 8
[0x80003178]:addi s11, zero, 0
[0x8000317c]:addi a4, zero, 0
[0x80003180]:csrrw zero, fcsr, a4
[0x80003184]:fmul.d t5, t3, s10, dyn
[0x80003188]:csrrs a6, fcsr, zero

[0x80003184]:fmul.d t5, t3, s10, dyn
[0x80003188]:csrrs a6, fcsr, zero
[0x8000318c]:sw t5, 560(ra)
[0x80003190]:sw t6, 568(ra)
[0x80003194]:sw t5, 576(ra)
[0x80003198]:sw a6, 584(ra)
[0x8000319c]:lui a4, 1
[0x800031a0]:addi a4, a4, 2048
[0x800031a4]:add a5, a5, a4
[0x800031a8]:lw t3, 288(a5)
[0x800031ac]:sub a5, a5, a4
[0x800031b0]:lui a4, 1
[0x800031b4]:addi a4, a4, 2048
[0x800031b8]:add a5, a5, a4
[0x800031bc]:lw t4, 292(a5)
[0x800031c0]:sub a5, a5, a4
[0x800031c4]:lui a4, 1
[0x800031c8]:addi a4, a4, 2048
[0x800031cc]:add a5, a5, a4
[0x800031d0]:lw s10, 296(a5)
[0x800031d4]:sub a5, a5, a4
[0x800031d8]:lui a4, 1
[0x800031dc]:addi a4, a4, 2048
[0x800031e0]:add a5, a5, a4
[0x800031e4]:lw s11, 300(a5)
[0x800031e8]:sub a5, a5, a4
[0x800031ec]:lui t3, 651467
[0x800031f0]:addi t3, t3, 3819
[0x800031f4]:lui t4, 523462
[0x800031f8]:addi t4, t4, 1101
[0x800031fc]:lui s10, 8
[0x80003200]:addi s11, zero, 0
[0x80003204]:addi a4, zero, 32
[0x80003208]:csrrw zero, fcsr, a4
[0x8000320c]:fmul.d t5, t3, s10, dyn
[0x80003210]:csrrs a6, fcsr, zero

[0x8000320c]:fmul.d t5, t3, s10, dyn
[0x80003210]:csrrs a6, fcsr, zero
[0x80003214]:sw t5, 592(ra)
[0x80003218]:sw t6, 600(ra)
[0x8000321c]:sw t5, 608(ra)
[0x80003220]:sw a6, 616(ra)
[0x80003224]:lui a4, 1
[0x80003228]:addi a4, a4, 2048
[0x8000322c]:add a5, a5, a4
[0x80003230]:lw t3, 304(a5)
[0x80003234]:sub a5, a5, a4
[0x80003238]:lui a4, 1
[0x8000323c]:addi a4, a4, 2048
[0x80003240]:add a5, a5, a4
[0x80003244]:lw t4, 308(a5)
[0x80003248]:sub a5, a5, a4
[0x8000324c]:lui a4, 1
[0x80003250]:addi a4, a4, 2048
[0x80003254]:add a5, a5, a4
[0x80003258]:lw s10, 312(a5)
[0x8000325c]:sub a5, a5, a4
[0x80003260]:lui a4, 1
[0x80003264]:addi a4, a4, 2048
[0x80003268]:add a5, a5, a4
[0x8000326c]:lw s11, 316(a5)
[0x80003270]:sub a5, a5, a4
[0x80003274]:lui t3, 651467
[0x80003278]:addi t3, t3, 3819
[0x8000327c]:lui t4, 523462
[0x80003280]:addi t4, t4, 1101
[0x80003284]:lui s10, 8
[0x80003288]:addi s11, zero, 0
[0x8000328c]:addi a4, zero, 64
[0x80003290]:csrrw zero, fcsr, a4
[0x80003294]:fmul.d t5, t3, s10, dyn
[0x80003298]:csrrs a6, fcsr, zero

[0x80003294]:fmul.d t5, t3, s10, dyn
[0x80003298]:csrrs a6, fcsr, zero
[0x8000329c]:sw t5, 624(ra)
[0x800032a0]:sw t6, 632(ra)
[0x800032a4]:sw t5, 640(ra)
[0x800032a8]:sw a6, 648(ra)
[0x800032ac]:lui a4, 1
[0x800032b0]:addi a4, a4, 2048
[0x800032b4]:add a5, a5, a4
[0x800032b8]:lw t3, 320(a5)
[0x800032bc]:sub a5, a5, a4
[0x800032c0]:lui a4, 1
[0x800032c4]:addi a4, a4, 2048
[0x800032c8]:add a5, a5, a4
[0x800032cc]:lw t4, 324(a5)
[0x800032d0]:sub a5, a5, a4
[0x800032d4]:lui a4, 1
[0x800032d8]:addi a4, a4, 2048
[0x800032dc]:add a5, a5, a4
[0x800032e0]:lw s10, 328(a5)
[0x800032e4]:sub a5, a5, a4
[0x800032e8]:lui a4, 1
[0x800032ec]:addi a4, a4, 2048
[0x800032f0]:add a5, a5, a4
[0x800032f4]:lw s11, 332(a5)
[0x800032f8]:sub a5, a5, a4
[0x800032fc]:lui t3, 651467
[0x80003300]:addi t3, t3, 3819
[0x80003304]:lui t4, 523462
[0x80003308]:addi t4, t4, 1101
[0x8000330c]:lui s10, 8
[0x80003310]:addi s11, zero, 0
[0x80003314]:addi a4, zero, 96
[0x80003318]:csrrw zero, fcsr, a4
[0x8000331c]:fmul.d t5, t3, s10, dyn
[0x80003320]:csrrs a6, fcsr, zero

[0x8000331c]:fmul.d t5, t3, s10, dyn
[0x80003320]:csrrs a6, fcsr, zero
[0x80003324]:sw t5, 656(ra)
[0x80003328]:sw t6, 664(ra)
[0x8000332c]:sw t5, 672(ra)
[0x80003330]:sw a6, 680(ra)
[0x80003334]:lui a4, 1
[0x80003338]:addi a4, a4, 2048
[0x8000333c]:add a5, a5, a4
[0x80003340]:lw t3, 336(a5)
[0x80003344]:sub a5, a5, a4
[0x80003348]:lui a4, 1
[0x8000334c]:addi a4, a4, 2048
[0x80003350]:add a5, a5, a4
[0x80003354]:lw t4, 340(a5)
[0x80003358]:sub a5, a5, a4
[0x8000335c]:lui a4, 1
[0x80003360]:addi a4, a4, 2048
[0x80003364]:add a5, a5, a4
[0x80003368]:lw s10, 344(a5)
[0x8000336c]:sub a5, a5, a4
[0x80003370]:lui a4, 1
[0x80003374]:addi a4, a4, 2048
[0x80003378]:add a5, a5, a4
[0x8000337c]:lw s11, 348(a5)
[0x80003380]:sub a5, a5, a4
[0x80003384]:lui t3, 651467
[0x80003388]:addi t3, t3, 3819
[0x8000338c]:lui t4, 523462
[0x80003390]:addi t4, t4, 1101
[0x80003394]:lui s10, 8
[0x80003398]:addi s11, zero, 0
[0x8000339c]:addi a4, zero, 128
[0x800033a0]:csrrw zero, fcsr, a4
[0x800033a4]:fmul.d t5, t3, s10, dyn
[0x800033a8]:csrrs a6, fcsr, zero

[0x800033a4]:fmul.d t5, t3, s10, dyn
[0x800033a8]:csrrs a6, fcsr, zero
[0x800033ac]:sw t5, 688(ra)
[0x800033b0]:sw t6, 696(ra)
[0x800033b4]:sw t5, 704(ra)
[0x800033b8]:sw a6, 712(ra)
[0x800033bc]:lui a4, 1
[0x800033c0]:addi a4, a4, 2048
[0x800033c4]:add a5, a5, a4
[0x800033c8]:lw t3, 352(a5)
[0x800033cc]:sub a5, a5, a4
[0x800033d0]:lui a4, 1
[0x800033d4]:addi a4, a4, 2048
[0x800033d8]:add a5, a5, a4
[0x800033dc]:lw t4, 356(a5)
[0x800033e0]:sub a5, a5, a4
[0x800033e4]:lui a4, 1
[0x800033e8]:addi a4, a4, 2048
[0x800033ec]:add a5, a5, a4
[0x800033f0]:lw s10, 360(a5)
[0x800033f4]:sub a5, a5, a4
[0x800033f8]:lui a4, 1
[0x800033fc]:addi a4, a4, 2048
[0x80003400]:add a5, a5, a4
[0x80003404]:lw s11, 364(a5)
[0x80003408]:sub a5, a5, a4
[0x8000340c]:lui t3, 62833
[0x80003410]:addi t3, t3, 296
[0x80003414]:lui t4, 523878
[0x80003418]:addi t4, t4, 740
[0x8000341c]:lui s10, 8
[0x80003420]:addi s11, zero, 0
[0x80003424]:addi a4, zero, 0
[0x80003428]:csrrw zero, fcsr, a4
[0x8000342c]:fmul.d t5, t3, s10, dyn
[0x80003430]:csrrs a6, fcsr, zero

[0x8000342c]:fmul.d t5, t3, s10, dyn
[0x80003430]:csrrs a6, fcsr, zero
[0x80003434]:sw t5, 720(ra)
[0x80003438]:sw t6, 728(ra)
[0x8000343c]:sw t5, 736(ra)
[0x80003440]:sw a6, 744(ra)
[0x80003444]:lui a4, 1
[0x80003448]:addi a4, a4, 2048
[0x8000344c]:add a5, a5, a4
[0x80003450]:lw t3, 368(a5)
[0x80003454]:sub a5, a5, a4
[0x80003458]:lui a4, 1
[0x8000345c]:addi a4, a4, 2048
[0x80003460]:add a5, a5, a4
[0x80003464]:lw t4, 372(a5)
[0x80003468]:sub a5, a5, a4
[0x8000346c]:lui a4, 1
[0x80003470]:addi a4, a4, 2048
[0x80003474]:add a5, a5, a4
[0x80003478]:lw s10, 376(a5)
[0x8000347c]:sub a5, a5, a4
[0x80003480]:lui a4, 1
[0x80003484]:addi a4, a4, 2048
[0x80003488]:add a5, a5, a4
[0x8000348c]:lw s11, 380(a5)
[0x80003490]:sub a5, a5, a4
[0x80003494]:lui t3, 62833
[0x80003498]:addi t3, t3, 296
[0x8000349c]:lui t4, 523878
[0x800034a0]:addi t4, t4, 740
[0x800034a4]:lui s10, 8
[0x800034a8]:addi s11, zero, 0
[0x800034ac]:addi a4, zero, 32
[0x800034b0]:csrrw zero, fcsr, a4
[0x800034b4]:fmul.d t5, t3, s10, dyn
[0x800034b8]:csrrs a6, fcsr, zero

[0x800034b4]:fmul.d t5, t3, s10, dyn
[0x800034b8]:csrrs a6, fcsr, zero
[0x800034bc]:sw t5, 752(ra)
[0x800034c0]:sw t6, 760(ra)
[0x800034c4]:sw t5, 768(ra)
[0x800034c8]:sw a6, 776(ra)
[0x800034cc]:lui a4, 1
[0x800034d0]:addi a4, a4, 2048
[0x800034d4]:add a5, a5, a4
[0x800034d8]:lw t3, 384(a5)
[0x800034dc]:sub a5, a5, a4
[0x800034e0]:lui a4, 1
[0x800034e4]:addi a4, a4, 2048
[0x800034e8]:add a5, a5, a4
[0x800034ec]:lw t4, 388(a5)
[0x800034f0]:sub a5, a5, a4
[0x800034f4]:lui a4, 1
[0x800034f8]:addi a4, a4, 2048
[0x800034fc]:add a5, a5, a4
[0x80003500]:lw s10, 392(a5)
[0x80003504]:sub a5, a5, a4
[0x80003508]:lui a4, 1
[0x8000350c]:addi a4, a4, 2048
[0x80003510]:add a5, a5, a4
[0x80003514]:lw s11, 396(a5)
[0x80003518]:sub a5, a5, a4
[0x8000351c]:lui t3, 62833
[0x80003520]:addi t3, t3, 296
[0x80003524]:lui t4, 523878
[0x80003528]:addi t4, t4, 740
[0x8000352c]:lui s10, 8
[0x80003530]:addi s11, zero, 0
[0x80003534]:addi a4, zero, 64
[0x80003538]:csrrw zero, fcsr, a4
[0x8000353c]:fmul.d t5, t3, s10, dyn
[0x80003540]:csrrs a6, fcsr, zero

[0x8000353c]:fmul.d t5, t3, s10, dyn
[0x80003540]:csrrs a6, fcsr, zero
[0x80003544]:sw t5, 784(ra)
[0x80003548]:sw t6, 792(ra)
[0x8000354c]:sw t5, 800(ra)
[0x80003550]:sw a6, 808(ra)
[0x80003554]:lui a4, 1
[0x80003558]:addi a4, a4, 2048
[0x8000355c]:add a5, a5, a4
[0x80003560]:lw t3, 400(a5)
[0x80003564]:sub a5, a5, a4
[0x80003568]:lui a4, 1
[0x8000356c]:addi a4, a4, 2048
[0x80003570]:add a5, a5, a4
[0x80003574]:lw t4, 404(a5)
[0x80003578]:sub a5, a5, a4
[0x8000357c]:lui a4, 1
[0x80003580]:addi a4, a4, 2048
[0x80003584]:add a5, a5, a4
[0x80003588]:lw s10, 408(a5)
[0x8000358c]:sub a5, a5, a4
[0x80003590]:lui a4, 1
[0x80003594]:addi a4, a4, 2048
[0x80003598]:add a5, a5, a4
[0x8000359c]:lw s11, 412(a5)
[0x800035a0]:sub a5, a5, a4
[0x800035a4]:lui t3, 62833
[0x800035a8]:addi t3, t3, 296
[0x800035ac]:lui t4, 523878
[0x800035b0]:addi t4, t4, 740
[0x800035b4]:lui s10, 8
[0x800035b8]:addi s11, zero, 0
[0x800035bc]:addi a4, zero, 96
[0x800035c0]:csrrw zero, fcsr, a4
[0x800035c4]:fmul.d t5, t3, s10, dyn
[0x800035c8]:csrrs a6, fcsr, zero

[0x800035c4]:fmul.d t5, t3, s10, dyn
[0x800035c8]:csrrs a6, fcsr, zero
[0x800035cc]:sw t5, 816(ra)
[0x800035d0]:sw t6, 824(ra)
[0x800035d4]:sw t5, 832(ra)
[0x800035d8]:sw a6, 840(ra)
[0x800035dc]:lui a4, 1
[0x800035e0]:addi a4, a4, 2048
[0x800035e4]:add a5, a5, a4
[0x800035e8]:lw t3, 416(a5)
[0x800035ec]:sub a5, a5, a4
[0x800035f0]:lui a4, 1
[0x800035f4]:addi a4, a4, 2048
[0x800035f8]:add a5, a5, a4
[0x800035fc]:lw t4, 420(a5)
[0x80003600]:sub a5, a5, a4
[0x80003604]:lui a4, 1
[0x80003608]:addi a4, a4, 2048
[0x8000360c]:add a5, a5, a4
[0x80003610]:lw s10, 424(a5)
[0x80003614]:sub a5, a5, a4
[0x80003618]:lui a4, 1
[0x8000361c]:addi a4, a4, 2048
[0x80003620]:add a5, a5, a4
[0x80003624]:lw s11, 428(a5)
[0x80003628]:sub a5, a5, a4
[0x8000362c]:lui t3, 62833
[0x80003630]:addi t3, t3, 296
[0x80003634]:lui t4, 523878
[0x80003638]:addi t4, t4, 740
[0x8000363c]:lui s10, 8
[0x80003640]:addi s11, zero, 0
[0x80003644]:addi a4, zero, 128
[0x80003648]:csrrw zero, fcsr, a4
[0x8000364c]:fmul.d t5, t3, s10, dyn
[0x80003650]:csrrs a6, fcsr, zero

[0x8000364c]:fmul.d t5, t3, s10, dyn
[0x80003650]:csrrs a6, fcsr, zero
[0x80003654]:sw t5, 848(ra)
[0x80003658]:sw t6, 856(ra)
[0x8000365c]:sw t5, 864(ra)
[0x80003660]:sw a6, 872(ra)
[0x80003664]:lui a4, 1
[0x80003668]:addi a4, a4, 2048
[0x8000366c]:add a5, a5, a4
[0x80003670]:lw t3, 432(a5)
[0x80003674]:sub a5, a5, a4
[0x80003678]:lui a4, 1
[0x8000367c]:addi a4, a4, 2048
[0x80003680]:add a5, a5, a4
[0x80003684]:lw t4, 436(a5)
[0x80003688]:sub a5, a5, a4
[0x8000368c]:lui a4, 1
[0x80003690]:addi a4, a4, 2048
[0x80003694]:add a5, a5, a4
[0x80003698]:lw s10, 440(a5)
[0x8000369c]:sub a5, a5, a4
[0x800036a0]:lui a4, 1
[0x800036a4]:addi a4, a4, 2048
[0x800036a8]:add a5, a5, a4
[0x800036ac]:lw s11, 444(a5)
[0x800036b0]:sub a5, a5, a4
[0x800036b4]:lui t3, 239474
[0x800036b8]:addi t3, t3, 876
[0x800036bc]:lui t4, 523985
[0x800036c0]:addi t4, t4, 3545
[0x800036c4]:lui s10, 8
[0x800036c8]:addi s11, zero, 0
[0x800036cc]:addi a4, zero, 0
[0x800036d0]:csrrw zero, fcsr, a4
[0x800036d4]:fmul.d t5, t3, s10, dyn
[0x800036d8]:csrrs a6, fcsr, zero

[0x800036d4]:fmul.d t5, t3, s10, dyn
[0x800036d8]:csrrs a6, fcsr, zero
[0x800036dc]:sw t5, 880(ra)
[0x800036e0]:sw t6, 888(ra)
[0x800036e4]:sw t5, 896(ra)
[0x800036e8]:sw a6, 904(ra)
[0x800036ec]:lui a4, 1
[0x800036f0]:addi a4, a4, 2048
[0x800036f4]:add a5, a5, a4
[0x800036f8]:lw t3, 448(a5)
[0x800036fc]:sub a5, a5, a4
[0x80003700]:lui a4, 1
[0x80003704]:addi a4, a4, 2048
[0x80003708]:add a5, a5, a4
[0x8000370c]:lw t4, 452(a5)
[0x80003710]:sub a5, a5, a4
[0x80003714]:lui a4, 1
[0x80003718]:addi a4, a4, 2048
[0x8000371c]:add a5, a5, a4
[0x80003720]:lw s10, 456(a5)
[0x80003724]:sub a5, a5, a4
[0x80003728]:lui a4, 1
[0x8000372c]:addi a4, a4, 2048
[0x80003730]:add a5, a5, a4
[0x80003734]:lw s11, 460(a5)
[0x80003738]:sub a5, a5, a4
[0x8000373c]:lui t3, 239474
[0x80003740]:addi t3, t3, 876
[0x80003744]:lui t4, 523985
[0x80003748]:addi t4, t4, 3545
[0x8000374c]:lui s10, 8
[0x80003750]:addi s11, zero, 0
[0x80003754]:addi a4, zero, 32
[0x80003758]:csrrw zero, fcsr, a4
[0x8000375c]:fmul.d t5, t3, s10, dyn
[0x80003760]:csrrs a6, fcsr, zero

[0x8000375c]:fmul.d t5, t3, s10, dyn
[0x80003760]:csrrs a6, fcsr, zero
[0x80003764]:sw t5, 912(ra)
[0x80003768]:sw t6, 920(ra)
[0x8000376c]:sw t5, 928(ra)
[0x80003770]:sw a6, 936(ra)
[0x80003774]:lui a4, 1
[0x80003778]:addi a4, a4, 2048
[0x8000377c]:add a5, a5, a4
[0x80003780]:lw t3, 464(a5)
[0x80003784]:sub a5, a5, a4
[0x80003788]:lui a4, 1
[0x8000378c]:addi a4, a4, 2048
[0x80003790]:add a5, a5, a4
[0x80003794]:lw t4, 468(a5)
[0x80003798]:sub a5, a5, a4
[0x8000379c]:lui a4, 1
[0x800037a0]:addi a4, a4, 2048
[0x800037a4]:add a5, a5, a4
[0x800037a8]:lw s10, 472(a5)
[0x800037ac]:sub a5, a5, a4
[0x800037b0]:lui a4, 1
[0x800037b4]:addi a4, a4, 2048
[0x800037b8]:add a5, a5, a4
[0x800037bc]:lw s11, 476(a5)
[0x800037c0]:sub a5, a5, a4
[0x800037c4]:lui t3, 239474
[0x800037c8]:addi t3, t3, 876
[0x800037cc]:lui t4, 523985
[0x800037d0]:addi t4, t4, 3545
[0x800037d4]:lui s10, 8
[0x800037d8]:addi s11, zero, 0
[0x800037dc]:addi a4, zero, 64
[0x800037e0]:csrrw zero, fcsr, a4
[0x800037e4]:fmul.d t5, t3, s10, dyn
[0x800037e8]:csrrs a6, fcsr, zero

[0x800037e4]:fmul.d t5, t3, s10, dyn
[0x800037e8]:csrrs a6, fcsr, zero
[0x800037ec]:sw t5, 944(ra)
[0x800037f0]:sw t6, 952(ra)
[0x800037f4]:sw t5, 960(ra)
[0x800037f8]:sw a6, 968(ra)
[0x800037fc]:lui a4, 1
[0x80003800]:addi a4, a4, 2048
[0x80003804]:add a5, a5, a4
[0x80003808]:lw t3, 480(a5)
[0x8000380c]:sub a5, a5, a4
[0x80003810]:lui a4, 1
[0x80003814]:addi a4, a4, 2048
[0x80003818]:add a5, a5, a4
[0x8000381c]:lw t4, 484(a5)
[0x80003820]:sub a5, a5, a4
[0x80003824]:lui a4, 1
[0x80003828]:addi a4, a4, 2048
[0x8000382c]:add a5, a5, a4
[0x80003830]:lw s10, 488(a5)
[0x80003834]:sub a5, a5, a4
[0x80003838]:lui a4, 1
[0x8000383c]:addi a4, a4, 2048
[0x80003840]:add a5, a5, a4
[0x80003844]:lw s11, 492(a5)
[0x80003848]:sub a5, a5, a4
[0x8000384c]:lui t3, 239474
[0x80003850]:addi t3, t3, 876
[0x80003854]:lui t4, 523985
[0x80003858]:addi t4, t4, 3545
[0x8000385c]:lui s10, 8
[0x80003860]:addi s11, zero, 0
[0x80003864]:addi a4, zero, 96
[0x80003868]:csrrw zero, fcsr, a4
[0x8000386c]:fmul.d t5, t3, s10, dyn
[0x80003870]:csrrs a6, fcsr, zero

[0x8000386c]:fmul.d t5, t3, s10, dyn
[0x80003870]:csrrs a6, fcsr, zero
[0x80003874]:sw t5, 976(ra)
[0x80003878]:sw t6, 984(ra)
[0x8000387c]:sw t5, 992(ra)
[0x80003880]:sw a6, 1000(ra)
[0x80003884]:lui a4, 1
[0x80003888]:addi a4, a4, 2048
[0x8000388c]:add a5, a5, a4
[0x80003890]:lw t3, 496(a5)
[0x80003894]:sub a5, a5, a4
[0x80003898]:lui a4, 1
[0x8000389c]:addi a4, a4, 2048
[0x800038a0]:add a5, a5, a4
[0x800038a4]:lw t4, 500(a5)
[0x800038a8]:sub a5, a5, a4
[0x800038ac]:lui a4, 1
[0x800038b0]:addi a4, a4, 2048
[0x800038b4]:add a5, a5, a4
[0x800038b8]:lw s10, 504(a5)
[0x800038bc]:sub a5, a5, a4
[0x800038c0]:lui a4, 1
[0x800038c4]:addi a4, a4, 2048
[0x800038c8]:add a5, a5, a4
[0x800038cc]:lw s11, 508(a5)
[0x800038d0]:sub a5, a5, a4
[0x800038d4]:lui t3, 239474
[0x800038d8]:addi t3, t3, 876
[0x800038dc]:lui t4, 523985
[0x800038e0]:addi t4, t4, 3545
[0x800038e4]:lui s10, 8
[0x800038e8]:addi s11, zero, 0
[0x800038ec]:addi a4, zero, 128
[0x800038f0]:csrrw zero, fcsr, a4
[0x800038f4]:fmul.d t5, t3, s10, dyn
[0x800038f8]:csrrs a6, fcsr, zero

[0x800038f4]:fmul.d t5, t3, s10, dyn
[0x800038f8]:csrrs a6, fcsr, zero
[0x800038fc]:sw t5, 1008(ra)
[0x80003900]:sw t6, 1016(ra)
[0x80003904]:sw t5, 1024(ra)
[0x80003908]:sw a6, 1032(ra)
[0x8000390c]:lui a4, 1
[0x80003910]:addi a4, a4, 2048
[0x80003914]:add a5, a5, a4
[0x80003918]:lw t3, 512(a5)
[0x8000391c]:sub a5, a5, a4
[0x80003920]:lui a4, 1
[0x80003924]:addi a4, a4, 2048
[0x80003928]:add a5, a5, a4
[0x8000392c]:lw t4, 516(a5)
[0x80003930]:sub a5, a5, a4
[0x80003934]:lui a4, 1
[0x80003938]:addi a4, a4, 2048
[0x8000393c]:add a5, a5, a4
[0x80003940]:lw s10, 520(a5)
[0x80003944]:sub a5, a5, a4
[0x80003948]:lui a4, 1
[0x8000394c]:addi a4, a4, 2048
[0x80003950]:add a5, a5, a4
[0x80003954]:lw s11, 524(a5)
[0x80003958]:sub a5, a5, a4
[0x8000395c]:lui t3, 892596
[0x80003960]:addi t3, t3, 2000
[0x80003964]:lui t4, 523987
[0x80003968]:addi t4, t4, 2561
[0x8000396c]:lui s10, 8
[0x80003970]:addi s11, zero, 0
[0x80003974]:addi a4, zero, 0
[0x80003978]:csrrw zero, fcsr, a4
[0x8000397c]:fmul.d t5, t3, s10, dyn
[0x80003980]:csrrs a6, fcsr, zero

[0x8000397c]:fmul.d t5, t3, s10, dyn
[0x80003980]:csrrs a6, fcsr, zero
[0x80003984]:sw t5, 1040(ra)
[0x80003988]:sw t6, 1048(ra)
[0x8000398c]:sw t5, 1056(ra)
[0x80003990]:sw a6, 1064(ra)
[0x80003994]:lui a4, 1
[0x80003998]:addi a4, a4, 2048
[0x8000399c]:add a5, a5, a4
[0x800039a0]:lw t3, 528(a5)
[0x800039a4]:sub a5, a5, a4
[0x800039a8]:lui a4, 1
[0x800039ac]:addi a4, a4, 2048
[0x800039b0]:add a5, a5, a4
[0x800039b4]:lw t4, 532(a5)
[0x800039b8]:sub a5, a5, a4
[0x800039bc]:lui a4, 1
[0x800039c0]:addi a4, a4, 2048
[0x800039c4]:add a5, a5, a4
[0x800039c8]:lw s10, 536(a5)
[0x800039cc]:sub a5, a5, a4
[0x800039d0]:lui a4, 1
[0x800039d4]:addi a4, a4, 2048
[0x800039d8]:add a5, a5, a4
[0x800039dc]:lw s11, 540(a5)
[0x800039e0]:sub a5, a5, a4
[0x800039e4]:lui t3, 892596
[0x800039e8]:addi t3, t3, 2000
[0x800039ec]:lui t4, 523987
[0x800039f0]:addi t4, t4, 2561
[0x800039f4]:lui s10, 8
[0x800039f8]:addi s11, zero, 0
[0x800039fc]:addi a4, zero, 32
[0x80003a00]:csrrw zero, fcsr, a4
[0x80003a04]:fmul.d t5, t3, s10, dyn
[0x80003a08]:csrrs a6, fcsr, zero

[0x80003a04]:fmul.d t5, t3, s10, dyn
[0x80003a08]:csrrs a6, fcsr, zero
[0x80003a0c]:sw t5, 1072(ra)
[0x80003a10]:sw t6, 1080(ra)
[0x80003a14]:sw t5, 1088(ra)
[0x80003a18]:sw a6, 1096(ra)
[0x80003a1c]:lui a4, 1
[0x80003a20]:addi a4, a4, 2048
[0x80003a24]:add a5, a5, a4
[0x80003a28]:lw t3, 544(a5)
[0x80003a2c]:sub a5, a5, a4
[0x80003a30]:lui a4, 1
[0x80003a34]:addi a4, a4, 2048
[0x80003a38]:add a5, a5, a4
[0x80003a3c]:lw t4, 548(a5)
[0x80003a40]:sub a5, a5, a4
[0x80003a44]:lui a4, 1
[0x80003a48]:addi a4, a4, 2048
[0x80003a4c]:add a5, a5, a4
[0x80003a50]:lw s10, 552(a5)
[0x80003a54]:sub a5, a5, a4
[0x80003a58]:lui a4, 1
[0x80003a5c]:addi a4, a4, 2048
[0x80003a60]:add a5, a5, a4
[0x80003a64]:lw s11, 556(a5)
[0x80003a68]:sub a5, a5, a4
[0x80003a6c]:lui t3, 892596
[0x80003a70]:addi t3, t3, 2000
[0x80003a74]:lui t4, 523987
[0x80003a78]:addi t4, t4, 2561
[0x80003a7c]:lui s10, 8
[0x80003a80]:addi s11, zero, 0
[0x80003a84]:addi a4, zero, 64
[0x80003a88]:csrrw zero, fcsr, a4
[0x80003a8c]:fmul.d t5, t3, s10, dyn
[0x80003a90]:csrrs a6, fcsr, zero

[0x80003a8c]:fmul.d t5, t3, s10, dyn
[0x80003a90]:csrrs a6, fcsr, zero
[0x80003a94]:sw t5, 1104(ra)
[0x80003a98]:sw t6, 1112(ra)
[0x80003a9c]:sw t5, 1120(ra)
[0x80003aa0]:sw a6, 1128(ra)
[0x80003aa4]:lui a4, 1
[0x80003aa8]:addi a4, a4, 2048
[0x80003aac]:add a5, a5, a4
[0x80003ab0]:lw t3, 560(a5)
[0x80003ab4]:sub a5, a5, a4
[0x80003ab8]:lui a4, 1
[0x80003abc]:addi a4, a4, 2048
[0x80003ac0]:add a5, a5, a4
[0x80003ac4]:lw t4, 564(a5)
[0x80003ac8]:sub a5, a5, a4
[0x80003acc]:lui a4, 1
[0x80003ad0]:addi a4, a4, 2048
[0x80003ad4]:add a5, a5, a4
[0x80003ad8]:lw s10, 568(a5)
[0x80003adc]:sub a5, a5, a4
[0x80003ae0]:lui a4, 1
[0x80003ae4]:addi a4, a4, 2048
[0x80003ae8]:add a5, a5, a4
[0x80003aec]:lw s11, 572(a5)
[0x80003af0]:sub a5, a5, a4
[0x80003af4]:lui t3, 892596
[0x80003af8]:addi t3, t3, 2000
[0x80003afc]:lui t4, 523987
[0x80003b00]:addi t4, t4, 2561
[0x80003b04]:lui s10, 8
[0x80003b08]:addi s11, zero, 0
[0x80003b0c]:addi a4, zero, 96
[0x80003b10]:csrrw zero, fcsr, a4
[0x80003b14]:fmul.d t5, t3, s10, dyn
[0x80003b18]:csrrs a6, fcsr, zero

[0x80003b14]:fmul.d t5, t3, s10, dyn
[0x80003b18]:csrrs a6, fcsr, zero
[0x80003b1c]:sw t5, 1136(ra)
[0x80003b20]:sw t6, 1144(ra)
[0x80003b24]:sw t5, 1152(ra)
[0x80003b28]:sw a6, 1160(ra)
[0x80003b2c]:lui a4, 1
[0x80003b30]:addi a4, a4, 2048
[0x80003b34]:add a5, a5, a4
[0x80003b38]:lw t3, 576(a5)
[0x80003b3c]:sub a5, a5, a4
[0x80003b40]:lui a4, 1
[0x80003b44]:addi a4, a4, 2048
[0x80003b48]:add a5, a5, a4
[0x80003b4c]:lw t4, 580(a5)
[0x80003b50]:sub a5, a5, a4
[0x80003b54]:lui a4, 1
[0x80003b58]:addi a4, a4, 2048
[0x80003b5c]:add a5, a5, a4
[0x80003b60]:lw s10, 584(a5)
[0x80003b64]:sub a5, a5, a4
[0x80003b68]:lui a4, 1
[0x80003b6c]:addi a4, a4, 2048
[0x80003b70]:add a5, a5, a4
[0x80003b74]:lw s11, 588(a5)
[0x80003b78]:sub a5, a5, a4
[0x80003b7c]:lui t3, 892596
[0x80003b80]:addi t3, t3, 2000
[0x80003b84]:lui t4, 523987
[0x80003b88]:addi t4, t4, 2561
[0x80003b8c]:lui s10, 8
[0x80003b90]:addi s11, zero, 0
[0x80003b94]:addi a4, zero, 128
[0x80003b98]:csrrw zero, fcsr, a4
[0x80003b9c]:fmul.d t5, t3, s10, dyn
[0x80003ba0]:csrrs a6, fcsr, zero

[0x80003b9c]:fmul.d t5, t3, s10, dyn
[0x80003ba0]:csrrs a6, fcsr, zero
[0x80003ba4]:sw t5, 1168(ra)
[0x80003ba8]:sw t6, 1176(ra)
[0x80003bac]:sw t5, 1184(ra)
[0x80003bb0]:sw a6, 1192(ra)
[0x80003bb4]:lui a4, 1
[0x80003bb8]:addi a4, a4, 2048
[0x80003bbc]:add a5, a5, a4
[0x80003bc0]:lw t3, 592(a5)
[0x80003bc4]:sub a5, a5, a4
[0x80003bc8]:lui a4, 1
[0x80003bcc]:addi a4, a4, 2048
[0x80003bd0]:add a5, a5, a4
[0x80003bd4]:lw t4, 596(a5)
[0x80003bd8]:sub a5, a5, a4
[0x80003bdc]:lui a4, 1
[0x80003be0]:addi a4, a4, 2048
[0x80003be4]:add a5, a5, a4
[0x80003be8]:lw s10, 600(a5)
[0x80003bec]:sub a5, a5, a4
[0x80003bf0]:lui a4, 1
[0x80003bf4]:addi a4, a4, 2048
[0x80003bf8]:add a5, a5, a4
[0x80003bfc]:lw s11, 604(a5)
[0x80003c00]:sub a5, a5, a4
[0x80003c04]:lui t3, 29479
[0x80003c08]:addi t3, t3, 234
[0x80003c0c]:lui t4, 524010
[0x80003c10]:addi t4, t4, 693
[0x80003c14]:lui s10, 8
[0x80003c18]:addi s11, zero, 0
[0x80003c1c]:addi a4, zero, 0
[0x80003c20]:csrrw zero, fcsr, a4
[0x80003c24]:fmul.d t5, t3, s10, dyn
[0x80003c28]:csrrs a6, fcsr, zero

[0x80003c24]:fmul.d t5, t3, s10, dyn
[0x80003c28]:csrrs a6, fcsr, zero
[0x80003c2c]:sw t5, 1200(ra)
[0x80003c30]:sw t6, 1208(ra)
[0x80003c34]:sw t5, 1216(ra)
[0x80003c38]:sw a6, 1224(ra)
[0x80003c3c]:lui a4, 1
[0x80003c40]:addi a4, a4, 2048
[0x80003c44]:add a5, a5, a4
[0x80003c48]:lw t3, 608(a5)
[0x80003c4c]:sub a5, a5, a4
[0x80003c50]:lui a4, 1
[0x80003c54]:addi a4, a4, 2048
[0x80003c58]:add a5, a5, a4
[0x80003c5c]:lw t4, 612(a5)
[0x80003c60]:sub a5, a5, a4
[0x80003c64]:lui a4, 1
[0x80003c68]:addi a4, a4, 2048
[0x80003c6c]:add a5, a5, a4
[0x80003c70]:lw s10, 616(a5)
[0x80003c74]:sub a5, a5, a4
[0x80003c78]:lui a4, 1
[0x80003c7c]:addi a4, a4, 2048
[0x80003c80]:add a5, a5, a4
[0x80003c84]:lw s11, 620(a5)
[0x80003c88]:sub a5, a5, a4
[0x80003c8c]:lui t3, 29479
[0x80003c90]:addi t3, t3, 234
[0x80003c94]:lui t4, 524010
[0x80003c98]:addi t4, t4, 693
[0x80003c9c]:lui s10, 8
[0x80003ca0]:addi s11, zero, 0
[0x80003ca4]:addi a4, zero, 32
[0x80003ca8]:csrrw zero, fcsr, a4
[0x80003cac]:fmul.d t5, t3, s10, dyn
[0x80003cb0]:csrrs a6, fcsr, zero

[0x80003cac]:fmul.d t5, t3, s10, dyn
[0x80003cb0]:csrrs a6, fcsr, zero
[0x80003cb4]:sw t5, 1232(ra)
[0x80003cb8]:sw t6, 1240(ra)
[0x80003cbc]:sw t5, 1248(ra)
[0x80003cc0]:sw a6, 1256(ra)
[0x80003cc4]:lui a4, 1
[0x80003cc8]:addi a4, a4, 2048
[0x80003ccc]:add a5, a5, a4
[0x80003cd0]:lw t3, 624(a5)
[0x80003cd4]:sub a5, a5, a4
[0x80003cd8]:lui a4, 1
[0x80003cdc]:addi a4, a4, 2048
[0x80003ce0]:add a5, a5, a4
[0x80003ce4]:lw t4, 628(a5)
[0x80003ce8]:sub a5, a5, a4
[0x80003cec]:lui a4, 1
[0x80003cf0]:addi a4, a4, 2048
[0x80003cf4]:add a5, a5, a4
[0x80003cf8]:lw s10, 632(a5)
[0x80003cfc]:sub a5, a5, a4
[0x80003d00]:lui a4, 1
[0x80003d04]:addi a4, a4, 2048
[0x80003d08]:add a5, a5, a4
[0x80003d0c]:lw s11, 636(a5)
[0x80003d10]:sub a5, a5, a4
[0x80003d14]:lui t3, 29479
[0x80003d18]:addi t3, t3, 234
[0x80003d1c]:lui t4, 524010
[0x80003d20]:addi t4, t4, 693
[0x80003d24]:lui s10, 8
[0x80003d28]:addi s11, zero, 0
[0x80003d2c]:addi a4, zero, 64
[0x80003d30]:csrrw zero, fcsr, a4
[0x80003d34]:fmul.d t5, t3, s10, dyn
[0x80003d38]:csrrs a6, fcsr, zero

[0x80003d34]:fmul.d t5, t3, s10, dyn
[0x80003d38]:csrrs a6, fcsr, zero
[0x80003d3c]:sw t5, 1264(ra)
[0x80003d40]:sw t6, 1272(ra)
[0x80003d44]:sw t5, 1280(ra)
[0x80003d48]:sw a6, 1288(ra)
[0x80003d4c]:lui a4, 1
[0x80003d50]:addi a4, a4, 2048
[0x80003d54]:add a5, a5, a4
[0x80003d58]:lw t3, 640(a5)
[0x80003d5c]:sub a5, a5, a4
[0x80003d60]:lui a4, 1
[0x80003d64]:addi a4, a4, 2048
[0x80003d68]:add a5, a5, a4
[0x80003d6c]:lw t4, 644(a5)
[0x80003d70]:sub a5, a5, a4
[0x80003d74]:lui a4, 1
[0x80003d78]:addi a4, a4, 2048
[0x80003d7c]:add a5, a5, a4
[0x80003d80]:lw s10, 648(a5)
[0x80003d84]:sub a5, a5, a4
[0x80003d88]:lui a4, 1
[0x80003d8c]:addi a4, a4, 2048
[0x80003d90]:add a5, a5, a4
[0x80003d94]:lw s11, 652(a5)
[0x80003d98]:sub a5, a5, a4
[0x80003d9c]:lui t3, 29479
[0x80003da0]:addi t3, t3, 234
[0x80003da4]:lui t4, 524010
[0x80003da8]:addi t4, t4, 693
[0x80003dac]:lui s10, 8
[0x80003db0]:addi s11, zero, 0
[0x80003db4]:addi a4, zero, 96
[0x80003db8]:csrrw zero, fcsr, a4
[0x80003dbc]:fmul.d t5, t3, s10, dyn
[0x80003dc0]:csrrs a6, fcsr, zero

[0x80003dbc]:fmul.d t5, t3, s10, dyn
[0x80003dc0]:csrrs a6, fcsr, zero
[0x80003dc4]:sw t5, 1296(ra)
[0x80003dc8]:sw t6, 1304(ra)
[0x80003dcc]:sw t5, 1312(ra)
[0x80003dd0]:sw a6, 1320(ra)
[0x80003dd4]:lui a4, 1
[0x80003dd8]:addi a4, a4, 2048
[0x80003ddc]:add a5, a5, a4
[0x80003de0]:lw t3, 656(a5)
[0x80003de4]:sub a5, a5, a4
[0x80003de8]:lui a4, 1
[0x80003dec]:addi a4, a4, 2048
[0x80003df0]:add a5, a5, a4
[0x80003df4]:lw t4, 660(a5)
[0x80003df8]:sub a5, a5, a4
[0x80003dfc]:lui a4, 1
[0x80003e00]:addi a4, a4, 2048
[0x80003e04]:add a5, a5, a4
[0x80003e08]:lw s10, 664(a5)
[0x80003e0c]:sub a5, a5, a4
[0x80003e10]:lui a4, 1
[0x80003e14]:addi a4, a4, 2048
[0x80003e18]:add a5, a5, a4
[0x80003e1c]:lw s11, 668(a5)
[0x80003e20]:sub a5, a5, a4
[0x80003e24]:lui t3, 29479
[0x80003e28]:addi t3, t3, 234
[0x80003e2c]:lui t4, 524010
[0x80003e30]:addi t4, t4, 693
[0x80003e34]:lui s10, 8
[0x80003e38]:addi s11, zero, 0
[0x80003e3c]:addi a4, zero, 128
[0x80003e40]:csrrw zero, fcsr, a4
[0x80003e44]:fmul.d t5, t3, s10, dyn
[0x80003e48]:csrrs a6, fcsr, zero

[0x80003e44]:fmul.d t5, t3, s10, dyn
[0x80003e48]:csrrs a6, fcsr, zero
[0x80003e4c]:sw t5, 1328(ra)
[0x80003e50]:sw t6, 1336(ra)
[0x80003e54]:sw t5, 1344(ra)
[0x80003e58]:sw a6, 1352(ra)
[0x80003e5c]:lui a4, 1
[0x80003e60]:addi a4, a4, 2048
[0x80003e64]:add a5, a5, a4
[0x80003e68]:lw t3, 672(a5)
[0x80003e6c]:sub a5, a5, a4
[0x80003e70]:lui a4, 1
[0x80003e74]:addi a4, a4, 2048
[0x80003e78]:add a5, a5, a4
[0x80003e7c]:lw t4, 676(a5)
[0x80003e80]:sub a5, a5, a4
[0x80003e84]:lui a4, 1
[0x80003e88]:addi a4, a4, 2048
[0x80003e8c]:add a5, a5, a4
[0x80003e90]:lw s10, 680(a5)
[0x80003e94]:sub a5, a5, a4
[0x80003e98]:lui a4, 1
[0x80003e9c]:addi a4, a4, 2048
[0x80003ea0]:add a5, a5, a4
[0x80003ea4]:lw s11, 684(a5)
[0x80003ea8]:sub a5, a5, a4
[0x80003eac]:lui t3, 435195
[0x80003eb0]:addi t3, t3, 3684
[0x80003eb4]:lui t4, 523778
[0x80003eb8]:addi t4, t4, 718
[0x80003ebc]:lui s10, 8
[0x80003ec0]:addi s11, zero, 0
[0x80003ec4]:addi a4, zero, 0
[0x80003ec8]:csrrw zero, fcsr, a4
[0x80003ecc]:fmul.d t5, t3, s10, dyn
[0x80003ed0]:csrrs a6, fcsr, zero

[0x80003ecc]:fmul.d t5, t3, s10, dyn
[0x80003ed0]:csrrs a6, fcsr, zero
[0x80003ed4]:sw t5, 1360(ra)
[0x80003ed8]:sw t6, 1368(ra)
[0x80003edc]:sw t5, 1376(ra)
[0x80003ee0]:sw a6, 1384(ra)
[0x80003ee4]:lui a4, 1
[0x80003ee8]:addi a4, a4, 2048
[0x80003eec]:add a5, a5, a4
[0x80003ef0]:lw t3, 688(a5)
[0x80003ef4]:sub a5, a5, a4
[0x80003ef8]:lui a4, 1
[0x80003efc]:addi a4, a4, 2048
[0x80003f00]:add a5, a5, a4
[0x80003f04]:lw t4, 692(a5)
[0x80003f08]:sub a5, a5, a4
[0x80003f0c]:lui a4, 1
[0x80003f10]:addi a4, a4, 2048
[0x80003f14]:add a5, a5, a4
[0x80003f18]:lw s10, 696(a5)
[0x80003f1c]:sub a5, a5, a4
[0x80003f20]:lui a4, 1
[0x80003f24]:addi a4, a4, 2048
[0x80003f28]:add a5, a5, a4
[0x80003f2c]:lw s11, 700(a5)
[0x80003f30]:sub a5, a5, a4
[0x80003f34]:lui t3, 435195
[0x80003f38]:addi t3, t3, 3684
[0x80003f3c]:lui t4, 523778
[0x80003f40]:addi t4, t4, 718
[0x80003f44]:lui s10, 8
[0x80003f48]:addi s11, zero, 0
[0x80003f4c]:addi a4, zero, 32
[0x80003f50]:csrrw zero, fcsr, a4
[0x80003f54]:fmul.d t5, t3, s10, dyn
[0x80003f58]:csrrs a6, fcsr, zero

[0x80003f54]:fmul.d t5, t3, s10, dyn
[0x80003f58]:csrrs a6, fcsr, zero
[0x80003f5c]:sw t5, 1392(ra)
[0x80003f60]:sw t6, 1400(ra)
[0x80003f64]:sw t5, 1408(ra)
[0x80003f68]:sw a6, 1416(ra)
[0x80003f6c]:lui a4, 1
[0x80003f70]:addi a4, a4, 2048
[0x80003f74]:add a5, a5, a4
[0x80003f78]:lw t3, 704(a5)
[0x80003f7c]:sub a5, a5, a4
[0x80003f80]:lui a4, 1
[0x80003f84]:addi a4, a4, 2048
[0x80003f88]:add a5, a5, a4
[0x80003f8c]:lw t4, 708(a5)
[0x80003f90]:sub a5, a5, a4
[0x80003f94]:lui a4, 1
[0x80003f98]:addi a4, a4, 2048
[0x80003f9c]:add a5, a5, a4
[0x80003fa0]:lw s10, 712(a5)
[0x80003fa4]:sub a5, a5, a4
[0x80003fa8]:lui a4, 1
[0x80003fac]:addi a4, a4, 2048
[0x80003fb0]:add a5, a5, a4
[0x80003fb4]:lw s11, 716(a5)
[0x80003fb8]:sub a5, a5, a4
[0x80003fbc]:lui t3, 435195
[0x80003fc0]:addi t3, t3, 3684
[0x80003fc4]:lui t4, 523778
[0x80003fc8]:addi t4, t4, 718
[0x80003fcc]:lui s10, 8
[0x80003fd0]:addi s11, zero, 0
[0x80003fd4]:addi a4, zero, 64
[0x80003fd8]:csrrw zero, fcsr, a4
[0x80003fdc]:fmul.d t5, t3, s10, dyn
[0x80003fe0]:csrrs a6, fcsr, zero

[0x80003fdc]:fmul.d t5, t3, s10, dyn
[0x80003fe0]:csrrs a6, fcsr, zero
[0x80003fe4]:sw t5, 1424(ra)
[0x80003fe8]:sw t6, 1432(ra)
[0x80003fec]:sw t5, 1440(ra)
[0x80003ff0]:sw a6, 1448(ra)
[0x80003ff4]:lui a4, 1
[0x80003ff8]:addi a4, a4, 2048
[0x80003ffc]:add a5, a5, a4
[0x80004000]:lw t3, 720(a5)
[0x80004004]:sub a5, a5, a4
[0x80004008]:lui a4, 1
[0x8000400c]:addi a4, a4, 2048
[0x80004010]:add a5, a5, a4
[0x80004014]:lw t4, 724(a5)
[0x80004018]:sub a5, a5, a4
[0x8000401c]:lui a4, 1
[0x80004020]:addi a4, a4, 2048
[0x80004024]:add a5, a5, a4
[0x80004028]:lw s10, 728(a5)
[0x8000402c]:sub a5, a5, a4
[0x80004030]:lui a4, 1
[0x80004034]:addi a4, a4, 2048
[0x80004038]:add a5, a5, a4
[0x8000403c]:lw s11, 732(a5)
[0x80004040]:sub a5, a5, a4
[0x80004044]:lui t3, 435195
[0x80004048]:addi t3, t3, 3684
[0x8000404c]:lui t4, 523778
[0x80004050]:addi t4, t4, 718
[0x80004054]:lui s10, 8
[0x80004058]:addi s11, zero, 0
[0x8000405c]:addi a4, zero, 96
[0x80004060]:csrrw zero, fcsr, a4
[0x80004064]:fmul.d t5, t3, s10, dyn
[0x80004068]:csrrs a6, fcsr, zero

[0x80004064]:fmul.d t5, t3, s10, dyn
[0x80004068]:csrrs a6, fcsr, zero
[0x8000406c]:sw t5, 1456(ra)
[0x80004070]:sw t6, 1464(ra)
[0x80004074]:sw t5, 1472(ra)
[0x80004078]:sw a6, 1480(ra)
[0x8000407c]:lui a4, 1
[0x80004080]:addi a4, a4, 2048
[0x80004084]:add a5, a5, a4
[0x80004088]:lw t3, 736(a5)
[0x8000408c]:sub a5, a5, a4
[0x80004090]:lui a4, 1
[0x80004094]:addi a4, a4, 2048
[0x80004098]:add a5, a5, a4
[0x8000409c]:lw t4, 740(a5)
[0x800040a0]:sub a5, a5, a4
[0x800040a4]:lui a4, 1
[0x800040a8]:addi a4, a4, 2048
[0x800040ac]:add a5, a5, a4
[0x800040b0]:lw s10, 744(a5)
[0x800040b4]:sub a5, a5, a4
[0x800040b8]:lui a4, 1
[0x800040bc]:addi a4, a4, 2048
[0x800040c0]:add a5, a5, a4
[0x800040c4]:lw s11, 748(a5)
[0x800040c8]:sub a5, a5, a4
[0x800040cc]:lui t3, 435195
[0x800040d0]:addi t3, t3, 3684
[0x800040d4]:lui t4, 523778
[0x800040d8]:addi t4, t4, 718
[0x800040dc]:lui s10, 8
[0x800040e0]:addi s11, zero, 0
[0x800040e4]:addi a4, zero, 128
[0x800040e8]:csrrw zero, fcsr, a4
[0x800040ec]:fmul.d t5, t3, s10, dyn
[0x800040f0]:csrrs a6, fcsr, zero

[0x800040ec]:fmul.d t5, t3, s10, dyn
[0x800040f0]:csrrs a6, fcsr, zero
[0x800040f4]:sw t5, 1488(ra)
[0x800040f8]:sw t6, 1496(ra)
[0x800040fc]:sw t5, 1504(ra)
[0x80004100]:sw a6, 1512(ra)
[0x80004104]:lui a4, 1
[0x80004108]:addi a4, a4, 2048
[0x8000410c]:add a5, a5, a4
[0x80004110]:lw t3, 752(a5)
[0x80004114]:sub a5, a5, a4
[0x80004118]:lui a4, 1
[0x8000411c]:addi a4, a4, 2048
[0x80004120]:add a5, a5, a4
[0x80004124]:lw t4, 756(a5)
[0x80004128]:sub a5, a5, a4
[0x8000412c]:lui a4, 1
[0x80004130]:addi a4, a4, 2048
[0x80004134]:add a5, a5, a4
[0x80004138]:lw s10, 760(a5)
[0x8000413c]:sub a5, a5, a4
[0x80004140]:lui a4, 1
[0x80004144]:addi a4, a4, 2048
[0x80004148]:add a5, a5, a4
[0x8000414c]:lw s11, 764(a5)
[0x80004150]:sub a5, a5, a4
[0x80004154]:lui t3, 10089
[0x80004158]:addi t3, t3, 1644
[0x8000415c]:lui t4, 523854
[0x80004160]:addi t4, t4, 2147
[0x80004164]:lui s10, 8
[0x80004168]:addi s11, zero, 0
[0x8000416c]:addi a4, zero, 0
[0x80004170]:csrrw zero, fcsr, a4
[0x80004174]:fmul.d t5, t3, s10, dyn
[0x80004178]:csrrs a6, fcsr, zero

[0x80004174]:fmul.d t5, t3, s10, dyn
[0x80004178]:csrrs a6, fcsr, zero
[0x8000417c]:sw t5, 1520(ra)
[0x80004180]:sw t6, 1528(ra)
[0x80004184]:sw t5, 1536(ra)
[0x80004188]:sw a6, 1544(ra)
[0x8000418c]:lui a4, 1
[0x80004190]:addi a4, a4, 2048
[0x80004194]:add a5, a5, a4
[0x80004198]:lw t3, 768(a5)
[0x8000419c]:sub a5, a5, a4
[0x800041a0]:lui a4, 1
[0x800041a4]:addi a4, a4, 2048
[0x800041a8]:add a5, a5, a4
[0x800041ac]:lw t4, 772(a5)
[0x800041b0]:sub a5, a5, a4
[0x800041b4]:lui a4, 1
[0x800041b8]:addi a4, a4, 2048
[0x800041bc]:add a5, a5, a4
[0x800041c0]:lw s10, 776(a5)
[0x800041c4]:sub a5, a5, a4
[0x800041c8]:lui a4, 1
[0x800041cc]:addi a4, a4, 2048
[0x800041d0]:add a5, a5, a4
[0x800041d4]:lw s11, 780(a5)
[0x800041d8]:sub a5, a5, a4
[0x800041dc]:lui t3, 10089
[0x800041e0]:addi t3, t3, 1644
[0x800041e4]:lui t4, 523854
[0x800041e8]:addi t4, t4, 2147
[0x800041ec]:lui s10, 8
[0x800041f0]:addi s11, zero, 0
[0x800041f4]:addi a4, zero, 32
[0x800041f8]:csrrw zero, fcsr, a4
[0x800041fc]:fmul.d t5, t3, s10, dyn
[0x80004200]:csrrs a6, fcsr, zero

[0x800041fc]:fmul.d t5, t3, s10, dyn
[0x80004200]:csrrs a6, fcsr, zero
[0x80004204]:sw t5, 1552(ra)
[0x80004208]:sw t6, 1560(ra)
[0x8000420c]:sw t5, 1568(ra)
[0x80004210]:sw a6, 1576(ra)
[0x80004214]:lui a4, 1
[0x80004218]:addi a4, a4, 2048
[0x8000421c]:add a5, a5, a4
[0x80004220]:lw t3, 784(a5)
[0x80004224]:sub a5, a5, a4
[0x80004228]:lui a4, 1
[0x8000422c]:addi a4, a4, 2048
[0x80004230]:add a5, a5, a4
[0x80004234]:lw t4, 788(a5)
[0x80004238]:sub a5, a5, a4
[0x8000423c]:lui a4, 1
[0x80004240]:addi a4, a4, 2048
[0x80004244]:add a5, a5, a4
[0x80004248]:lw s10, 792(a5)
[0x8000424c]:sub a5, a5, a4
[0x80004250]:lui a4, 1
[0x80004254]:addi a4, a4, 2048
[0x80004258]:add a5, a5, a4
[0x8000425c]:lw s11, 796(a5)
[0x80004260]:sub a5, a5, a4
[0x80004264]:lui t3, 10089
[0x80004268]:addi t3, t3, 1644
[0x8000426c]:lui t4, 523854
[0x80004270]:addi t4, t4, 2147
[0x80004274]:lui s10, 8
[0x80004278]:addi s11, zero, 0
[0x8000427c]:addi a4, zero, 64
[0x80004280]:csrrw zero, fcsr, a4
[0x80004284]:fmul.d t5, t3, s10, dyn
[0x80004288]:csrrs a6, fcsr, zero

[0x80004284]:fmul.d t5, t3, s10, dyn
[0x80004288]:csrrs a6, fcsr, zero
[0x8000428c]:sw t5, 1584(ra)
[0x80004290]:sw t6, 1592(ra)
[0x80004294]:sw t5, 1600(ra)
[0x80004298]:sw a6, 1608(ra)
[0x8000429c]:lui a4, 1
[0x800042a0]:addi a4, a4, 2048
[0x800042a4]:add a5, a5, a4
[0x800042a8]:lw t3, 800(a5)
[0x800042ac]:sub a5, a5, a4
[0x800042b0]:lui a4, 1
[0x800042b4]:addi a4, a4, 2048
[0x800042b8]:add a5, a5, a4
[0x800042bc]:lw t4, 804(a5)
[0x800042c0]:sub a5, a5, a4
[0x800042c4]:lui a4, 1
[0x800042c8]:addi a4, a4, 2048
[0x800042cc]:add a5, a5, a4
[0x800042d0]:lw s10, 808(a5)
[0x800042d4]:sub a5, a5, a4
[0x800042d8]:lui a4, 1
[0x800042dc]:addi a4, a4, 2048
[0x800042e0]:add a5, a5, a4
[0x800042e4]:lw s11, 812(a5)
[0x800042e8]:sub a5, a5, a4
[0x800042ec]:lui t3, 10089
[0x800042f0]:addi t3, t3, 1644
[0x800042f4]:lui t4, 523854
[0x800042f8]:addi t4, t4, 2147
[0x800042fc]:lui s10, 8
[0x80004300]:addi s11, zero, 0
[0x80004304]:addi a4, zero, 96
[0x80004308]:csrrw zero, fcsr, a4
[0x8000430c]:fmul.d t5, t3, s10, dyn
[0x80004310]:csrrs a6, fcsr, zero

[0x8000430c]:fmul.d t5, t3, s10, dyn
[0x80004310]:csrrs a6, fcsr, zero
[0x80004314]:sw t5, 1616(ra)
[0x80004318]:sw t6, 1624(ra)
[0x8000431c]:sw t5, 1632(ra)
[0x80004320]:sw a6, 1640(ra)
[0x80004324]:lui a4, 1
[0x80004328]:addi a4, a4, 2048
[0x8000432c]:add a5, a5, a4
[0x80004330]:lw t3, 816(a5)
[0x80004334]:sub a5, a5, a4
[0x80004338]:lui a4, 1
[0x8000433c]:addi a4, a4, 2048
[0x80004340]:add a5, a5, a4
[0x80004344]:lw t4, 820(a5)
[0x80004348]:sub a5, a5, a4
[0x8000434c]:lui a4, 1
[0x80004350]:addi a4, a4, 2048
[0x80004354]:add a5, a5, a4
[0x80004358]:lw s10, 824(a5)
[0x8000435c]:sub a5, a5, a4
[0x80004360]:lui a4, 1
[0x80004364]:addi a4, a4, 2048
[0x80004368]:add a5, a5, a4
[0x8000436c]:lw s11, 828(a5)
[0x80004370]:sub a5, a5, a4
[0x80004374]:lui t3, 10089
[0x80004378]:addi t3, t3, 1644
[0x8000437c]:lui t4, 523854
[0x80004380]:addi t4, t4, 2147
[0x80004384]:lui s10, 8
[0x80004388]:addi s11, zero, 0
[0x8000438c]:addi a4, zero, 128
[0x80004390]:csrrw zero, fcsr, a4
[0x80004394]:fmul.d t5, t3, s10, dyn
[0x80004398]:csrrs a6, fcsr, zero

[0x80004394]:fmul.d t5, t3, s10, dyn
[0x80004398]:csrrs a6, fcsr, zero
[0x8000439c]:sw t5, 1648(ra)
[0x800043a0]:sw t6, 1656(ra)
[0x800043a4]:sw t5, 1664(ra)
[0x800043a8]:sw a6, 1672(ra)
[0x800043ac]:lui a4, 1
[0x800043b0]:addi a4, a4, 2048
[0x800043b4]:add a5, a5, a4
[0x800043b8]:lw t3, 832(a5)
[0x800043bc]:sub a5, a5, a4
[0x800043c0]:lui a4, 1
[0x800043c4]:addi a4, a4, 2048
[0x800043c8]:add a5, a5, a4
[0x800043cc]:lw t4, 836(a5)
[0x800043d0]:sub a5, a5, a4
[0x800043d4]:lui a4, 1
[0x800043d8]:addi a4, a4, 2048
[0x800043dc]:add a5, a5, a4
[0x800043e0]:lw s10, 840(a5)
[0x800043e4]:sub a5, a5, a4
[0x800043e8]:lui a4, 1
[0x800043ec]:addi a4, a4, 2048
[0x800043f0]:add a5, a5, a4
[0x800043f4]:lw s11, 844(a5)
[0x800043f8]:sub a5, a5, a4
[0x800043fc]:lui t3, 941455
[0x80004400]:addi t3, t3, 449
[0x80004404]:lui t4, 523583
[0x80004408]:addi t4, t4, 1345
[0x8000440c]:lui s10, 8
[0x80004410]:addi s11, zero, 0
[0x80004414]:addi a4, zero, 0
[0x80004418]:csrrw zero, fcsr, a4
[0x8000441c]:fmul.d t5, t3, s10, dyn
[0x80004420]:csrrs a6, fcsr, zero

[0x8000441c]:fmul.d t5, t3, s10, dyn
[0x80004420]:csrrs a6, fcsr, zero
[0x80004424]:sw t5, 1680(ra)
[0x80004428]:sw t6, 1688(ra)
[0x8000442c]:sw t5, 1696(ra)
[0x80004430]:sw a6, 1704(ra)
[0x80004434]:lui a4, 1
[0x80004438]:addi a4, a4, 2048
[0x8000443c]:add a5, a5, a4
[0x80004440]:lw t3, 848(a5)
[0x80004444]:sub a5, a5, a4
[0x80004448]:lui a4, 1
[0x8000444c]:addi a4, a4, 2048
[0x80004450]:add a5, a5, a4
[0x80004454]:lw t4, 852(a5)
[0x80004458]:sub a5, a5, a4
[0x8000445c]:lui a4, 1
[0x80004460]:addi a4, a4, 2048
[0x80004464]:add a5, a5, a4
[0x80004468]:lw s10, 856(a5)
[0x8000446c]:sub a5, a5, a4
[0x80004470]:lui a4, 1
[0x80004474]:addi a4, a4, 2048
[0x80004478]:add a5, a5, a4
[0x8000447c]:lw s11, 860(a5)
[0x80004480]:sub a5, a5, a4
[0x80004484]:lui t3, 941455
[0x80004488]:addi t3, t3, 449
[0x8000448c]:lui t4, 523583
[0x80004490]:addi t4, t4, 1345
[0x80004494]:lui s10, 8
[0x80004498]:addi s11, zero, 0
[0x8000449c]:addi a4, zero, 32
[0x800044a0]:csrrw zero, fcsr, a4
[0x800044a4]:fmul.d t5, t3, s10, dyn
[0x800044a8]:csrrs a6, fcsr, zero

[0x800044a4]:fmul.d t5, t3, s10, dyn
[0x800044a8]:csrrs a6, fcsr, zero
[0x800044ac]:sw t5, 1712(ra)
[0x800044b0]:sw t6, 1720(ra)
[0x800044b4]:sw t5, 1728(ra)
[0x800044b8]:sw a6, 1736(ra)
[0x800044bc]:lui a4, 1
[0x800044c0]:addi a4, a4, 2048
[0x800044c4]:add a5, a5, a4
[0x800044c8]:lw t3, 864(a5)
[0x800044cc]:sub a5, a5, a4
[0x800044d0]:lui a4, 1
[0x800044d4]:addi a4, a4, 2048
[0x800044d8]:add a5, a5, a4
[0x800044dc]:lw t4, 868(a5)
[0x800044e0]:sub a5, a5, a4
[0x800044e4]:lui a4, 1
[0x800044e8]:addi a4, a4, 2048
[0x800044ec]:add a5, a5, a4
[0x800044f0]:lw s10, 872(a5)
[0x800044f4]:sub a5, a5, a4
[0x800044f8]:lui a4, 1
[0x800044fc]:addi a4, a4, 2048
[0x80004500]:add a5, a5, a4
[0x80004504]:lw s11, 876(a5)
[0x80004508]:sub a5, a5, a4
[0x8000450c]:lui t3, 941455
[0x80004510]:addi t3, t3, 449
[0x80004514]:lui t4, 523583
[0x80004518]:addi t4, t4, 1345
[0x8000451c]:lui s10, 8
[0x80004520]:addi s11, zero, 0
[0x80004524]:addi a4, zero, 64
[0x80004528]:csrrw zero, fcsr, a4
[0x8000452c]:fmul.d t5, t3, s10, dyn
[0x80004530]:csrrs a6, fcsr, zero

[0x8000452c]:fmul.d t5, t3, s10, dyn
[0x80004530]:csrrs a6, fcsr, zero
[0x80004534]:sw t5, 1744(ra)
[0x80004538]:sw t6, 1752(ra)
[0x8000453c]:sw t5, 1760(ra)
[0x80004540]:sw a6, 1768(ra)
[0x80004544]:lui a4, 1
[0x80004548]:addi a4, a4, 2048
[0x8000454c]:add a5, a5, a4
[0x80004550]:lw t3, 880(a5)
[0x80004554]:sub a5, a5, a4
[0x80004558]:lui a4, 1
[0x8000455c]:addi a4, a4, 2048
[0x80004560]:add a5, a5, a4
[0x80004564]:lw t4, 884(a5)
[0x80004568]:sub a5, a5, a4
[0x8000456c]:lui a4, 1
[0x80004570]:addi a4, a4, 2048
[0x80004574]:add a5, a5, a4
[0x80004578]:lw s10, 888(a5)
[0x8000457c]:sub a5, a5, a4
[0x80004580]:lui a4, 1
[0x80004584]:addi a4, a4, 2048
[0x80004588]:add a5, a5, a4
[0x8000458c]:lw s11, 892(a5)
[0x80004590]:sub a5, a5, a4
[0x80004594]:lui t3, 941455
[0x80004598]:addi t3, t3, 449
[0x8000459c]:lui t4, 523583
[0x800045a0]:addi t4, t4, 1345
[0x800045a4]:lui s10, 8
[0x800045a8]:addi s11, zero, 0
[0x800045ac]:addi a4, zero, 96
[0x800045b0]:csrrw zero, fcsr, a4
[0x800045b4]:fmul.d t5, t3, s10, dyn
[0x800045b8]:csrrs a6, fcsr, zero

[0x800045b4]:fmul.d t5, t3, s10, dyn
[0x800045b8]:csrrs a6, fcsr, zero
[0x800045bc]:sw t5, 1776(ra)
[0x800045c0]:sw t6, 1784(ra)
[0x800045c4]:sw t5, 1792(ra)
[0x800045c8]:sw a6, 1800(ra)
[0x800045cc]:lui a4, 1
[0x800045d0]:addi a4, a4, 2048
[0x800045d4]:add a5, a5, a4
[0x800045d8]:lw t3, 896(a5)
[0x800045dc]:sub a5, a5, a4
[0x800045e0]:lui a4, 1
[0x800045e4]:addi a4, a4, 2048
[0x800045e8]:add a5, a5, a4
[0x800045ec]:lw t4, 900(a5)
[0x800045f0]:sub a5, a5, a4
[0x800045f4]:lui a4, 1
[0x800045f8]:addi a4, a4, 2048
[0x800045fc]:add a5, a5, a4
[0x80004600]:lw s10, 904(a5)
[0x80004604]:sub a5, a5, a4
[0x80004608]:lui a4, 1
[0x8000460c]:addi a4, a4, 2048
[0x80004610]:add a5, a5, a4
[0x80004614]:lw s11, 908(a5)
[0x80004618]:sub a5, a5, a4
[0x8000461c]:lui t3, 941455
[0x80004620]:addi t3, t3, 449
[0x80004624]:lui t4, 523583
[0x80004628]:addi t4, t4, 1345
[0x8000462c]:lui s10, 8
[0x80004630]:addi s11, zero, 0
[0x80004634]:addi a4, zero, 128
[0x80004638]:csrrw zero, fcsr, a4
[0x8000463c]:fmul.d t5, t3, s10, dyn
[0x80004640]:csrrs a6, fcsr, zero

[0x8000463c]:fmul.d t5, t3, s10, dyn
[0x80004640]:csrrs a6, fcsr, zero
[0x80004644]:sw t5, 1808(ra)
[0x80004648]:sw t6, 1816(ra)
[0x8000464c]:sw t5, 1824(ra)
[0x80004650]:sw a6, 1832(ra)
[0x80004654]:lui a4, 1
[0x80004658]:addi a4, a4, 2048
[0x8000465c]:add a5, a5, a4
[0x80004660]:lw t3, 912(a5)
[0x80004664]:sub a5, a5, a4
[0x80004668]:lui a4, 1
[0x8000466c]:addi a4, a4, 2048
[0x80004670]:add a5, a5, a4
[0x80004674]:lw t4, 916(a5)
[0x80004678]:sub a5, a5, a4
[0x8000467c]:lui a4, 1
[0x80004680]:addi a4, a4, 2048
[0x80004684]:add a5, a5, a4
[0x80004688]:lw s10, 920(a5)
[0x8000468c]:sub a5, a5, a4
[0x80004690]:lui a4, 1
[0x80004694]:addi a4, a4, 2048
[0x80004698]:add a5, a5, a4
[0x8000469c]:lw s11, 924(a5)
[0x800046a0]:sub a5, a5, a4
[0x800046a4]:lui t3, 644982
[0x800046a8]:addi t3, t3, 1414
[0x800046ac]:lui t4, 523782
[0x800046b0]:addi t4, t4, 362
[0x800046b4]:lui s10, 8
[0x800046b8]:addi s11, zero, 0
[0x800046bc]:addi a4, zero, 0
[0x800046c0]:csrrw zero, fcsr, a4
[0x800046c4]:fmul.d t5, t3, s10, dyn
[0x800046c8]:csrrs a6, fcsr, zero

[0x800046c4]:fmul.d t5, t3, s10, dyn
[0x800046c8]:csrrs a6, fcsr, zero
[0x800046cc]:sw t5, 1840(ra)
[0x800046d0]:sw t6, 1848(ra)
[0x800046d4]:sw t5, 1856(ra)
[0x800046d8]:sw a6, 1864(ra)
[0x800046dc]:lui a4, 1
[0x800046e0]:addi a4, a4, 2048
[0x800046e4]:add a5, a5, a4
[0x800046e8]:lw t3, 928(a5)
[0x800046ec]:sub a5, a5, a4
[0x800046f0]:lui a4, 1
[0x800046f4]:addi a4, a4, 2048
[0x800046f8]:add a5, a5, a4
[0x800046fc]:lw t4, 932(a5)
[0x80004700]:sub a5, a5, a4
[0x80004704]:lui a4, 1
[0x80004708]:addi a4, a4, 2048
[0x8000470c]:add a5, a5, a4
[0x80004710]:lw s10, 936(a5)
[0x80004714]:sub a5, a5, a4
[0x80004718]:lui a4, 1
[0x8000471c]:addi a4, a4, 2048
[0x80004720]:add a5, a5, a4
[0x80004724]:lw s11, 940(a5)
[0x80004728]:sub a5, a5, a4
[0x8000472c]:lui t3, 644982
[0x80004730]:addi t3, t3, 1414
[0x80004734]:lui t4, 523782
[0x80004738]:addi t4, t4, 362
[0x8000473c]:lui s10, 8
[0x80004740]:addi s11, zero, 0
[0x80004744]:addi a4, zero, 32
[0x80004748]:csrrw zero, fcsr, a4
[0x8000474c]:fmul.d t5, t3, s10, dyn
[0x80004750]:csrrs a6, fcsr, zero

[0x8000474c]:fmul.d t5, t3, s10, dyn
[0x80004750]:csrrs a6, fcsr, zero
[0x80004754]:sw t5, 1872(ra)
[0x80004758]:sw t6, 1880(ra)
[0x8000475c]:sw t5, 1888(ra)
[0x80004760]:sw a6, 1896(ra)
[0x80004764]:lui a4, 1
[0x80004768]:addi a4, a4, 2048
[0x8000476c]:add a5, a5, a4
[0x80004770]:lw t3, 944(a5)
[0x80004774]:sub a5, a5, a4
[0x80004778]:lui a4, 1
[0x8000477c]:addi a4, a4, 2048
[0x80004780]:add a5, a5, a4
[0x80004784]:lw t4, 948(a5)
[0x80004788]:sub a5, a5, a4
[0x8000478c]:lui a4, 1
[0x80004790]:addi a4, a4, 2048
[0x80004794]:add a5, a5, a4
[0x80004798]:lw s10, 952(a5)
[0x8000479c]:sub a5, a5, a4
[0x800047a0]:lui a4, 1
[0x800047a4]:addi a4, a4, 2048
[0x800047a8]:add a5, a5, a4
[0x800047ac]:lw s11, 956(a5)
[0x800047b0]:sub a5, a5, a4
[0x800047b4]:lui t3, 644982
[0x800047b8]:addi t3, t3, 1414
[0x800047bc]:lui t4, 523782
[0x800047c0]:addi t4, t4, 362
[0x800047c4]:lui s10, 8
[0x800047c8]:addi s11, zero, 0
[0x800047cc]:addi a4, zero, 64
[0x800047d0]:csrrw zero, fcsr, a4
[0x800047d4]:fmul.d t5, t3, s10, dyn
[0x800047d8]:csrrs a6, fcsr, zero

[0x800047d4]:fmul.d t5, t3, s10, dyn
[0x800047d8]:csrrs a6, fcsr, zero
[0x800047dc]:sw t5, 1904(ra)
[0x800047e0]:sw t6, 1912(ra)
[0x800047e4]:sw t5, 1920(ra)
[0x800047e8]:sw a6, 1928(ra)
[0x800047ec]:lui a4, 1
[0x800047f0]:addi a4, a4, 2048
[0x800047f4]:add a5, a5, a4
[0x800047f8]:lw t3, 960(a5)
[0x800047fc]:sub a5, a5, a4
[0x80004800]:lui a4, 1
[0x80004804]:addi a4, a4, 2048
[0x80004808]:add a5, a5, a4
[0x8000480c]:lw t4, 964(a5)
[0x80004810]:sub a5, a5, a4
[0x80004814]:lui a4, 1
[0x80004818]:addi a4, a4, 2048
[0x8000481c]:add a5, a5, a4
[0x80004820]:lw s10, 968(a5)
[0x80004824]:sub a5, a5, a4
[0x80004828]:lui a4, 1
[0x8000482c]:addi a4, a4, 2048
[0x80004830]:add a5, a5, a4
[0x80004834]:lw s11, 972(a5)
[0x80004838]:sub a5, a5, a4
[0x8000483c]:lui t3, 644982
[0x80004840]:addi t3, t3, 1414
[0x80004844]:lui t4, 523782
[0x80004848]:addi t4, t4, 362
[0x8000484c]:lui s10, 8
[0x80004850]:addi s11, zero, 0
[0x80004854]:addi a4, zero, 96
[0x80004858]:csrrw zero, fcsr, a4
[0x8000485c]:fmul.d t5, t3, s10, dyn
[0x80004860]:csrrs a6, fcsr, zero

[0x8000485c]:fmul.d t5, t3, s10, dyn
[0x80004860]:csrrs a6, fcsr, zero
[0x80004864]:sw t5, 1936(ra)
[0x80004868]:sw t6, 1944(ra)
[0x8000486c]:sw t5, 1952(ra)
[0x80004870]:sw a6, 1960(ra)
[0x80004874]:lui a4, 1
[0x80004878]:addi a4, a4, 2048
[0x8000487c]:add a5, a5, a4
[0x80004880]:lw t3, 976(a5)
[0x80004884]:sub a5, a5, a4
[0x80004888]:lui a4, 1
[0x8000488c]:addi a4, a4, 2048
[0x80004890]:add a5, a5, a4
[0x80004894]:lw t4, 980(a5)
[0x80004898]:sub a5, a5, a4
[0x8000489c]:lui a4, 1
[0x800048a0]:addi a4, a4, 2048
[0x800048a4]:add a5, a5, a4
[0x800048a8]:lw s10, 984(a5)
[0x800048ac]:sub a5, a5, a4
[0x800048b0]:lui a4, 1
[0x800048b4]:addi a4, a4, 2048
[0x800048b8]:add a5, a5, a4
[0x800048bc]:lw s11, 988(a5)
[0x800048c0]:sub a5, a5, a4
[0x800048c4]:lui t3, 644982
[0x800048c8]:addi t3, t3, 1414
[0x800048cc]:lui t4, 523782
[0x800048d0]:addi t4, t4, 362
[0x800048d4]:lui s10, 8
[0x800048d8]:addi s11, zero, 0
[0x800048dc]:addi a4, zero, 128
[0x800048e0]:csrrw zero, fcsr, a4
[0x800048e4]:fmul.d t5, t3, s10, dyn
[0x800048e8]:csrrs a6, fcsr, zero

[0x800048e4]:fmul.d t5, t3, s10, dyn
[0x800048e8]:csrrs a6, fcsr, zero
[0x800048ec]:sw t5, 1968(ra)
[0x800048f0]:sw t6, 1976(ra)
[0x800048f4]:sw t5, 1984(ra)
[0x800048f8]:sw a6, 1992(ra)
[0x800048fc]:lui a4, 1
[0x80004900]:addi a4, a4, 2048
[0x80004904]:add a5, a5, a4
[0x80004908]:lw t3, 992(a5)
[0x8000490c]:sub a5, a5, a4
[0x80004910]:lui a4, 1
[0x80004914]:addi a4, a4, 2048
[0x80004918]:add a5, a5, a4
[0x8000491c]:lw t4, 996(a5)
[0x80004920]:sub a5, a5, a4
[0x80004924]:lui a4, 1
[0x80004928]:addi a4, a4, 2048
[0x8000492c]:add a5, a5, a4
[0x80004930]:lw s10, 1000(a5)
[0x80004934]:sub a5, a5, a4
[0x80004938]:lui a4, 1
[0x8000493c]:addi a4, a4, 2048
[0x80004940]:add a5, a5, a4
[0x80004944]:lw s11, 1004(a5)
[0x80004948]:sub a5, a5, a4
[0x8000494c]:lui t3, 653428
[0x80004950]:addi t3, t3, 3388
[0x80004954]:lui t4, 523839
[0x80004958]:addi t4, t4, 457
[0x8000495c]:lui s10, 8
[0x80004960]:addi s11, zero, 0
[0x80004964]:addi a4, zero, 0
[0x80004968]:csrrw zero, fcsr, a4
[0x8000496c]:fmul.d t5, t3, s10, dyn
[0x80004970]:csrrs a6, fcsr, zero

[0x8000496c]:fmul.d t5, t3, s10, dyn
[0x80004970]:csrrs a6, fcsr, zero
[0x80004974]:sw t5, 2000(ra)
[0x80004978]:sw t6, 2008(ra)
[0x8000497c]:sw t5, 2016(ra)
[0x80004980]:sw a6, 2024(ra)
[0x80004984]:lui a4, 1
[0x80004988]:addi a4, a4, 2048
[0x8000498c]:add a5, a5, a4
[0x80004990]:lw t3, 1008(a5)
[0x80004994]:sub a5, a5, a4
[0x80004998]:lui a4, 1
[0x8000499c]:addi a4, a4, 2048
[0x800049a0]:add a5, a5, a4
[0x800049a4]:lw t4, 1012(a5)
[0x800049a8]:sub a5, a5, a4
[0x800049ac]:lui a4, 1
[0x800049b0]:addi a4, a4, 2048
[0x800049b4]:add a5, a5, a4
[0x800049b8]:lw s10, 1016(a5)
[0x800049bc]:sub a5, a5, a4
[0x800049c0]:lui a4, 1
[0x800049c4]:addi a4, a4, 2048
[0x800049c8]:add a5, a5, a4
[0x800049cc]:lw s11, 1020(a5)
[0x800049d0]:sub a5, a5, a4
[0x800049d4]:lui t3, 653428
[0x800049d8]:addi t3, t3, 3388
[0x800049dc]:lui t4, 523839
[0x800049e0]:addi t4, t4, 457
[0x800049e4]:lui s10, 8
[0x800049e8]:addi s11, zero, 0
[0x800049ec]:addi a4, zero, 32
[0x800049f0]:csrrw zero, fcsr, a4
[0x800049f4]:fmul.d t5, t3, s10, dyn
[0x800049f8]:csrrs a6, fcsr, zero

[0x800049f4]:fmul.d t5, t3, s10, dyn
[0x800049f8]:csrrs a6, fcsr, zero
[0x800049fc]:sw t5, 2032(ra)
[0x80004a00]:sw t6, 2040(ra)
[0x80004a04]:addi ra, ra, 2040
[0x80004a08]:sw t5, 8(ra)
[0x80004a0c]:sw a6, 16(ra)
[0x80004a10]:lui a4, 1
[0x80004a14]:addi a4, a4, 2048
[0x80004a18]:add a5, a5, a4
[0x80004a1c]:lw t3, 1024(a5)
[0x80004a20]:sub a5, a5, a4
[0x80004a24]:lui a4, 1
[0x80004a28]:addi a4, a4, 2048
[0x80004a2c]:add a5, a5, a4
[0x80004a30]:lw t4, 1028(a5)
[0x80004a34]:sub a5, a5, a4
[0x80004a38]:lui a4, 1
[0x80004a3c]:addi a4, a4, 2048
[0x80004a40]:add a5, a5, a4
[0x80004a44]:lw s10, 1032(a5)
[0x80004a48]:sub a5, a5, a4
[0x80004a4c]:lui a4, 1
[0x80004a50]:addi a4, a4, 2048
[0x80004a54]:add a5, a5, a4
[0x80004a58]:lw s11, 1036(a5)
[0x80004a5c]:sub a5, a5, a4
[0x80004a60]:lui t3, 653428
[0x80004a64]:addi t3, t3, 3388
[0x80004a68]:lui t4, 523839
[0x80004a6c]:addi t4, t4, 457
[0x80004a70]:lui s10, 8
[0x80004a74]:addi s11, zero, 0
[0x80004a78]:addi a4, zero, 64
[0x80004a7c]:csrrw zero, fcsr, a4
[0x80004a80]:fmul.d t5, t3, s10, dyn
[0x80004a84]:csrrs a6, fcsr, zero

[0x80004a80]:fmul.d t5, t3, s10, dyn
[0x80004a84]:csrrs a6, fcsr, zero
[0x80004a88]:sw t5, 24(ra)
[0x80004a8c]:sw t6, 32(ra)
[0x80004a90]:sw t5, 40(ra)
[0x80004a94]:sw a6, 48(ra)
[0x80004a98]:lui a4, 1
[0x80004a9c]:addi a4, a4, 2048
[0x80004aa0]:add a5, a5, a4
[0x80004aa4]:lw t3, 1040(a5)
[0x80004aa8]:sub a5, a5, a4
[0x80004aac]:lui a4, 1
[0x80004ab0]:addi a4, a4, 2048
[0x80004ab4]:add a5, a5, a4
[0x80004ab8]:lw t4, 1044(a5)
[0x80004abc]:sub a5, a5, a4
[0x80004ac0]:lui a4, 1
[0x80004ac4]:addi a4, a4, 2048
[0x80004ac8]:add a5, a5, a4
[0x80004acc]:lw s10, 1048(a5)
[0x80004ad0]:sub a5, a5, a4
[0x80004ad4]:lui a4, 1
[0x80004ad8]:addi a4, a4, 2048
[0x80004adc]:add a5, a5, a4
[0x80004ae0]:lw s11, 1052(a5)
[0x80004ae4]:sub a5, a5, a4
[0x80004ae8]:lui t3, 653428
[0x80004aec]:addi t3, t3, 3388
[0x80004af0]:lui t4, 523839
[0x80004af4]:addi t4, t4, 457
[0x80004af8]:lui s10, 8
[0x80004afc]:addi s11, zero, 0
[0x80004b00]:addi a4, zero, 96
[0x80004b04]:csrrw zero, fcsr, a4
[0x80004b08]:fmul.d t5, t3, s10, dyn
[0x80004b0c]:csrrs a6, fcsr, zero

[0x80004b08]:fmul.d t5, t3, s10, dyn
[0x80004b0c]:csrrs a6, fcsr, zero
[0x80004b10]:sw t5, 56(ra)
[0x80004b14]:sw t6, 64(ra)
[0x80004b18]:sw t5, 72(ra)
[0x80004b1c]:sw a6, 80(ra)
[0x80004b20]:lui a4, 1
[0x80004b24]:addi a4, a4, 2048
[0x80004b28]:add a5, a5, a4
[0x80004b2c]:lw t3, 1056(a5)
[0x80004b30]:sub a5, a5, a4
[0x80004b34]:lui a4, 1
[0x80004b38]:addi a4, a4, 2048
[0x80004b3c]:add a5, a5, a4
[0x80004b40]:lw t4, 1060(a5)
[0x80004b44]:sub a5, a5, a4
[0x80004b48]:lui a4, 1
[0x80004b4c]:addi a4, a4, 2048
[0x80004b50]:add a5, a5, a4
[0x80004b54]:lw s10, 1064(a5)
[0x80004b58]:sub a5, a5, a4
[0x80004b5c]:lui a4, 1
[0x80004b60]:addi a4, a4, 2048
[0x80004b64]:add a5, a5, a4
[0x80004b68]:lw s11, 1068(a5)
[0x80004b6c]:sub a5, a5, a4
[0x80004b70]:lui t3, 653428
[0x80004b74]:addi t3, t3, 3388
[0x80004b78]:lui t4, 523839
[0x80004b7c]:addi t4, t4, 457
[0x80004b80]:lui s10, 8
[0x80004b84]:addi s11, zero, 0
[0x80004b88]:addi a4, zero, 128
[0x80004b8c]:csrrw zero, fcsr, a4
[0x80004b90]:fmul.d t5, t3, s10, dyn
[0x80004b94]:csrrs a6, fcsr, zero

[0x80004b90]:fmul.d t5, t3, s10, dyn
[0x80004b94]:csrrs a6, fcsr, zero
[0x80004b98]:sw t5, 88(ra)
[0x80004b9c]:sw t6, 96(ra)
[0x80004ba0]:sw t5, 104(ra)
[0x80004ba4]:sw a6, 112(ra)
[0x80004ba8]:lui a4, 1
[0x80004bac]:addi a4, a4, 2048
[0x80004bb0]:add a5, a5, a4
[0x80004bb4]:lw t3, 1072(a5)
[0x80004bb8]:sub a5, a5, a4
[0x80004bbc]:lui a4, 1
[0x80004bc0]:addi a4, a4, 2048
[0x80004bc4]:add a5, a5, a4
[0x80004bc8]:lw t4, 1076(a5)
[0x80004bcc]:sub a5, a5, a4
[0x80004bd0]:lui a4, 1
[0x80004bd4]:addi a4, a4, 2048
[0x80004bd8]:add a5, a5, a4
[0x80004bdc]:lw s10, 1080(a5)
[0x80004be0]:sub a5, a5, a4
[0x80004be4]:lui a4, 1
[0x80004be8]:addi a4, a4, 2048
[0x80004bec]:add a5, a5, a4
[0x80004bf0]:lw s11, 1084(a5)
[0x80004bf4]:sub a5, a5, a4
[0x80004bf8]:lui t3, 513498
[0x80004bfc]:addi t3, t3, 1895
[0x80004c00]:lui t4, 523940
[0x80004c04]:addi t4, t4, 183
[0x80004c08]:lui s10, 8
[0x80004c0c]:addi s11, zero, 0
[0x80004c10]:addi a4, zero, 0
[0x80004c14]:csrrw zero, fcsr, a4
[0x80004c18]:fmul.d t5, t3, s10, dyn
[0x80004c1c]:csrrs a6, fcsr, zero

[0x80004c18]:fmul.d t5, t3, s10, dyn
[0x80004c1c]:csrrs a6, fcsr, zero
[0x80004c20]:sw t5, 120(ra)
[0x80004c24]:sw t6, 128(ra)
[0x80004c28]:sw t5, 136(ra)
[0x80004c2c]:sw a6, 144(ra)
[0x80004c30]:lui a4, 1
[0x80004c34]:addi a4, a4, 2048
[0x80004c38]:add a5, a5, a4
[0x80004c3c]:lw t3, 1088(a5)
[0x80004c40]:sub a5, a5, a4
[0x80004c44]:lui a4, 1
[0x80004c48]:addi a4, a4, 2048
[0x80004c4c]:add a5, a5, a4
[0x80004c50]:lw t4, 1092(a5)
[0x80004c54]:sub a5, a5, a4
[0x80004c58]:lui a4, 1
[0x80004c5c]:addi a4, a4, 2048
[0x80004c60]:add a5, a5, a4
[0x80004c64]:lw s10, 1096(a5)
[0x80004c68]:sub a5, a5, a4
[0x80004c6c]:lui a4, 1
[0x80004c70]:addi a4, a4, 2048
[0x80004c74]:add a5, a5, a4
[0x80004c78]:lw s11, 1100(a5)
[0x80004c7c]:sub a5, a5, a4
[0x80004c80]:lui t3, 513498
[0x80004c84]:addi t3, t3, 1895
[0x80004c88]:lui t4, 523940
[0x80004c8c]:addi t4, t4, 183
[0x80004c90]:lui s10, 8
[0x80004c94]:addi s11, zero, 0
[0x80004c98]:addi a4, zero, 32
[0x80004c9c]:csrrw zero, fcsr, a4
[0x80004ca0]:fmul.d t5, t3, s10, dyn
[0x80004ca4]:csrrs a6, fcsr, zero

[0x80004ca0]:fmul.d t5, t3, s10, dyn
[0x80004ca4]:csrrs a6, fcsr, zero
[0x80004ca8]:sw t5, 152(ra)
[0x80004cac]:sw t6, 160(ra)
[0x80004cb0]:sw t5, 168(ra)
[0x80004cb4]:sw a6, 176(ra)
[0x80004cb8]:lui a4, 1
[0x80004cbc]:addi a4, a4, 2048
[0x80004cc0]:add a5, a5, a4
[0x80004cc4]:lw t3, 1104(a5)
[0x80004cc8]:sub a5, a5, a4
[0x80004ccc]:lui a4, 1
[0x80004cd0]:addi a4, a4, 2048
[0x80004cd4]:add a5, a5, a4
[0x80004cd8]:lw t4, 1108(a5)
[0x80004cdc]:sub a5, a5, a4
[0x80004ce0]:lui a4, 1
[0x80004ce4]:addi a4, a4, 2048
[0x80004ce8]:add a5, a5, a4
[0x80004cec]:lw s10, 1112(a5)
[0x80004cf0]:sub a5, a5, a4
[0x80004cf4]:lui a4, 1
[0x80004cf8]:addi a4, a4, 2048
[0x80004cfc]:add a5, a5, a4
[0x80004d00]:lw s11, 1116(a5)
[0x80004d04]:sub a5, a5, a4
[0x80004d08]:lui t3, 513498
[0x80004d0c]:addi t3, t3, 1895
[0x80004d10]:lui t4, 523940
[0x80004d14]:addi t4, t4, 183
[0x80004d18]:lui s10, 8
[0x80004d1c]:addi s11, zero, 0
[0x80004d20]:addi a4, zero, 64
[0x80004d24]:csrrw zero, fcsr, a4
[0x80004d28]:fmul.d t5, t3, s10, dyn
[0x80004d2c]:csrrs a6, fcsr, zero

[0x80004d28]:fmul.d t5, t3, s10, dyn
[0x80004d2c]:csrrs a6, fcsr, zero
[0x80004d30]:sw t5, 184(ra)
[0x80004d34]:sw t6, 192(ra)
[0x80004d38]:sw t5, 200(ra)
[0x80004d3c]:sw a6, 208(ra)
[0x80004d40]:lui a4, 1
[0x80004d44]:addi a4, a4, 2048
[0x80004d48]:add a5, a5, a4
[0x80004d4c]:lw t3, 1120(a5)
[0x80004d50]:sub a5, a5, a4
[0x80004d54]:lui a4, 1
[0x80004d58]:addi a4, a4, 2048
[0x80004d5c]:add a5, a5, a4
[0x80004d60]:lw t4, 1124(a5)
[0x80004d64]:sub a5, a5, a4
[0x80004d68]:lui a4, 1
[0x80004d6c]:addi a4, a4, 2048
[0x80004d70]:add a5, a5, a4
[0x80004d74]:lw s10, 1128(a5)
[0x80004d78]:sub a5, a5, a4
[0x80004d7c]:lui a4, 1
[0x80004d80]:addi a4, a4, 2048
[0x80004d84]:add a5, a5, a4
[0x80004d88]:lw s11, 1132(a5)
[0x80004d8c]:sub a5, a5, a4
[0x80004d90]:lui t3, 513498
[0x80004d94]:addi t3, t3, 1895
[0x80004d98]:lui t4, 523940
[0x80004d9c]:addi t4, t4, 183
[0x80004da0]:lui s10, 8
[0x80004da4]:addi s11, zero, 0
[0x80004da8]:addi a4, zero, 96
[0x80004dac]:csrrw zero, fcsr, a4
[0x80004db0]:fmul.d t5, t3, s10, dyn
[0x80004db4]:csrrs a6, fcsr, zero

[0x80004db0]:fmul.d t5, t3, s10, dyn
[0x80004db4]:csrrs a6, fcsr, zero
[0x80004db8]:sw t5, 216(ra)
[0x80004dbc]:sw t6, 224(ra)
[0x80004dc0]:sw t5, 232(ra)
[0x80004dc4]:sw a6, 240(ra)
[0x80004dc8]:lui a4, 1
[0x80004dcc]:addi a4, a4, 2048
[0x80004dd0]:add a5, a5, a4
[0x80004dd4]:lw t3, 1136(a5)
[0x80004dd8]:sub a5, a5, a4
[0x80004ddc]:lui a4, 1
[0x80004de0]:addi a4, a4, 2048
[0x80004de4]:add a5, a5, a4
[0x80004de8]:lw t4, 1140(a5)
[0x80004dec]:sub a5, a5, a4
[0x80004df0]:lui a4, 1
[0x80004df4]:addi a4, a4, 2048
[0x80004df8]:add a5, a5, a4
[0x80004dfc]:lw s10, 1144(a5)
[0x80004e00]:sub a5, a5, a4
[0x80004e04]:lui a4, 1
[0x80004e08]:addi a4, a4, 2048
[0x80004e0c]:add a5, a5, a4
[0x80004e10]:lw s11, 1148(a5)
[0x80004e14]:sub a5, a5, a4
[0x80004e18]:lui t3, 513498
[0x80004e1c]:addi t3, t3, 1895
[0x80004e20]:lui t4, 523940
[0x80004e24]:addi t4, t4, 183
[0x80004e28]:lui s10, 8
[0x80004e2c]:addi s11, zero, 0
[0x80004e30]:addi a4, zero, 128
[0x80004e34]:csrrw zero, fcsr, a4
[0x80004e38]:fmul.d t5, t3, s10, dyn
[0x80004e3c]:csrrs a6, fcsr, zero

[0x80004e38]:fmul.d t5, t3, s10, dyn
[0x80004e3c]:csrrs a6, fcsr, zero
[0x80004e40]:sw t5, 248(ra)
[0x80004e44]:sw t6, 256(ra)
[0x80004e48]:sw t5, 264(ra)
[0x80004e4c]:sw a6, 272(ra)
[0x80004e50]:lui a4, 1
[0x80004e54]:addi a4, a4, 2048
[0x80004e58]:add a5, a5, a4
[0x80004e5c]:lw t3, 1152(a5)
[0x80004e60]:sub a5, a5, a4
[0x80004e64]:lui a4, 1
[0x80004e68]:addi a4, a4, 2048
[0x80004e6c]:add a5, a5, a4
[0x80004e70]:lw t4, 1156(a5)
[0x80004e74]:sub a5, a5, a4
[0x80004e78]:lui a4, 1
[0x80004e7c]:addi a4, a4, 2048
[0x80004e80]:add a5, a5, a4
[0x80004e84]:lw s10, 1160(a5)
[0x80004e88]:sub a5, a5, a4
[0x80004e8c]:lui a4, 1
[0x80004e90]:addi a4, a4, 2048
[0x80004e94]:add a5, a5, a4
[0x80004e98]:lw s11, 1164(a5)
[0x80004e9c]:sub a5, a5, a4
[0x80004ea0]:lui t3, 194365
[0x80004ea4]:addi t3, t3, 749
[0x80004ea8]:lui t4, 524009
[0x80004eac]:addi t4, t4, 2005
[0x80004eb0]:addi s10, zero, 0
[0x80004eb4]:addi s11, zero, 0
[0x80004eb8]:addi a4, zero, 64
[0x80004ebc]:csrrw zero, fcsr, a4
[0x80004ec0]:fmul.d t5, t3, s10, dyn
[0x80004ec4]:csrrs a6, fcsr, zero

[0x80004ec0]:fmul.d t5, t3, s10, dyn
[0x80004ec4]:csrrs a6, fcsr, zero
[0x80004ec8]:sw t5, 280(ra)
[0x80004ecc]:sw t6, 288(ra)
[0x80004ed0]:sw t5, 296(ra)
[0x80004ed4]:sw a6, 304(ra)
[0x80004ed8]:lui a4, 1
[0x80004edc]:addi a4, a4, 2048
[0x80004ee0]:add a5, a5, a4
[0x80004ee4]:lw t3, 1168(a5)
[0x80004ee8]:sub a5, a5, a4
[0x80004eec]:lui a4, 1
[0x80004ef0]:addi a4, a4, 2048
[0x80004ef4]:add a5, a5, a4
[0x80004ef8]:lw t4, 1172(a5)
[0x80004efc]:sub a5, a5, a4
[0x80004f00]:lui a4, 1
[0x80004f04]:addi a4, a4, 2048
[0x80004f08]:add a5, a5, a4
[0x80004f0c]:lw s10, 1176(a5)
[0x80004f10]:sub a5, a5, a4
[0x80004f14]:lui a4, 1
[0x80004f18]:addi a4, a4, 2048
[0x80004f1c]:add a5, a5, a4
[0x80004f20]:lw s11, 1180(a5)
[0x80004f24]:sub a5, a5, a4
[0x80004f28]:lui t3, 194365
[0x80004f2c]:addi t3, t3, 749
[0x80004f30]:lui t4, 524009
[0x80004f34]:addi t4, t4, 2005
[0x80004f38]:addi s10, zero, 0
[0x80004f3c]:addi s11, zero, 0
[0x80004f40]:addi a4, zero, 96
[0x80004f44]:csrrw zero, fcsr, a4
[0x80004f48]:fmul.d t5, t3, s10, dyn
[0x80004f4c]:csrrs a6, fcsr, zero

[0x80004f48]:fmul.d t5, t3, s10, dyn
[0x80004f4c]:csrrs a6, fcsr, zero
[0x80004f50]:sw t5, 312(ra)
[0x80004f54]:sw t6, 320(ra)
[0x80004f58]:sw t5, 328(ra)
[0x80004f5c]:sw a6, 336(ra)



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fmul.d', 'rs1 : x28', 'rs2 : x26', 'rd : x30', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000144]:fmul.d t5, t3, s10, dyn
	-[0x80000148]:csrrs tp, fcsr, zero
	-[0x8000014c]:sw t5, 0(ra)
	-[0x80000150]:sw t6, 8(ra)
Current Store : [0x80000150] : sw t6, 8(ra) -- Store: [0x80006f20]:0xFBB6FAB7




Last Coverpoint : ['mnemonic : fmul.d', 'rs1 : x28', 'rs2 : x26', 'rd : x30', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000144]:fmul.d t5, t3, s10, dyn
	-[0x80000148]:csrrs tp, fcsr, zero
	-[0x8000014c]:sw t5, 0(ra)
	-[0x80000150]:sw t6, 8(ra)
	-[0x80000154]:sw t5, 16(ra)
Current Store : [0x80000154] : sw t5, 16(ra) -- Store: [0x80006f28]:0x00000000




Last Coverpoint : ['rs1 : x26', 'rs2 : x30', 'rd : x26', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000018c]:fmul.d s10, s10, t5, dyn
	-[0x80000190]:csrrs tp, fcsr, zero
	-[0x80000194]:sw s10, 32(ra)
	-[0x80000198]:sw s11, 40(ra)
Current Store : [0x80000198] : sw s11, 40(ra) -- Store: [0x80006f40]:0x7FEE97D5




Last Coverpoint : ['rs1 : x26', 'rs2 : x30', 'rd : x26', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000018c]:fmul.d s10, s10, t5, dyn
	-[0x80000190]:csrrs tp, fcsr, zero
	-[0x80000194]:sw s10, 32(ra)
	-[0x80000198]:sw s11, 40(ra)
	-[0x8000019c]:sw s10, 48(ra)
Current Store : [0x8000019c] : sw s10, 48(ra) -- Store: [0x80006f48]:0x00000000




Last Coverpoint : ['rs1 : x24', 'rs2 : x24', 'rd : x24', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x800001dc]:fmul.d s8, s8, s8, dyn
	-[0x800001e0]:csrrs tp, fcsr, zero
	-[0x800001e4]:sw s8, 64(ra)
	-[0x800001e8]:sw s9, 72(ra)
Current Store : [0x800001e8] : sw s9, 72(ra) -- Store: [0x80006f60]:0x7FEE97D5




Last Coverpoint : ['rs1 : x24', 'rs2 : x24', 'rd : x24', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x800001dc]:fmul.d s8, s8, s8, dyn
	-[0x800001e0]:csrrs tp, fcsr, zero
	-[0x800001e4]:sw s8, 64(ra)
	-[0x800001e8]:sw s9, 72(ra)
	-[0x800001ec]:sw s8, 80(ra)
Current Store : [0x800001ec] : sw s8, 80(ra) -- Store: [0x80006f68]:0xFFFFFFFF




Last Coverpoint : ['rs1 : x22', 'rs2 : x22', 'rd : x28', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x8000022c]:fmul.d t3, s6, s6, dyn
	-[0x80000230]:csrrs tp, fcsr, zero
	-[0x80000234]:sw t3, 96(ra)
	-[0x80000238]:sw t4, 104(ra)
Current Store : [0x80000238] : sw t4, 104(ra) -- Store: [0x80006f80]:0x7FEE97D5




Last Coverpoint : ['rs1 : x22', 'rs2 : x22', 'rd : x28', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x8000022c]:fmul.d t3, s6, s6, dyn
	-[0x80000230]:csrrs tp, fcsr, zero
	-[0x80000234]:sw t3, 96(ra)
	-[0x80000238]:sw t4, 104(ra)
	-[0x8000023c]:sw t3, 112(ra)
Current Store : [0x8000023c] : sw t3, 112(ra) -- Store: [0x80006f88]:0x00000000




Last Coverpoint : ['rs1 : x30', 'rs2 : x20', 'rd : x20', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000274]:fmul.d s4, t5, s4, dyn
	-[0x80000278]:csrrs tp, fcsr, zero
	-[0x8000027c]:sw s4, 128(ra)
	-[0x80000280]:sw s5, 136(ra)
Current Store : [0x80000280] : sw s5, 136(ra) -- Store: [0x80006fa0]:0x00000000




Last Coverpoint : ['rs1 : x30', 'rs2 : x20', 'rd : x20', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000274]:fmul.d s4, t5, s4, dyn
	-[0x80000278]:csrrs tp, fcsr, zero
	-[0x8000027c]:sw s4, 128(ra)
	-[0x80000280]:sw s5, 136(ra)
	-[0x80000284]:sw s4, 144(ra)
Current Store : [0x80000284] : sw s4, 144(ra) -- Store: [0x80006fa8]:0x00000000




Last Coverpoint : ['rs1 : x20', 'rs2 : x28', 'rd : x22', 'fs1 == 0 and fe1 == 0x7fa and fm1 == 0xcf44d05dc866f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002bc]:fmul.d s6, s4, t3, dyn
	-[0x800002c0]:csrrs tp, fcsr, zero
	-[0x800002c4]:sw s6, 160(ra)
	-[0x800002c8]:sw s7, 168(ra)
Current Store : [0x800002c8] : sw s7, 168(ra) -- Store: [0x80006fc0]:0x7FEE97D5




Last Coverpoint : ['rs1 : x20', 'rs2 : x28', 'rd : x22', 'fs1 == 0 and fe1 == 0x7fa and fm1 == 0xcf44d05dc866f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002bc]:fmul.d s6, s4, t3, dyn
	-[0x800002c0]:csrrs tp, fcsr, zero
	-[0x800002c4]:sw s6, 160(ra)
	-[0x800002c8]:sw s7, 168(ra)
	-[0x800002cc]:sw s6, 176(ra)
Current Store : [0x800002cc] : sw s6, 176(ra) -- Store: [0x80006fc8]:0x00000000




Last Coverpoint : ['rs1 : x16', 'rs2 : x14', 'rd : x18', 'fs1 == 0 and fe1 == 0x7fa and fm1 == 0xcf44d05dc866f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000304]:fmul.d s2, a6, a4, dyn
	-[0x80000308]:csrrs tp, fcsr, zero
	-[0x8000030c]:sw s2, 192(ra)
	-[0x80000310]:sw s3, 200(ra)
Current Store : [0x80000310] : sw s3, 200(ra) -- Store: [0x80006fe0]:0x6FAB7FBB




Last Coverpoint : ['rs1 : x16', 'rs2 : x14', 'rd : x18', 'fs1 == 0 and fe1 == 0x7fa and fm1 == 0xcf44d05dc866f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000304]:fmul.d s2, a6, a4, dyn
	-[0x80000308]:csrrs tp, fcsr, zero
	-[0x8000030c]:sw s2, 192(ra)
	-[0x80000310]:sw s3, 200(ra)
	-[0x80000314]:sw s2, 208(ra)
Current Store : [0x80000314] : sw s2, 208(ra) -- Store: [0x80006fe8]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rs2 : x18', 'rd : x16', 'fs1 == 0 and fe1 == 0x7fa and fm1 == 0xcf44d05dc866f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000034c]:fmul.d a6, a4, s2, dyn
	-[0x80000350]:csrrs tp, fcsr, zero
	-[0x80000354]:sw a6, 224(ra)
	-[0x80000358]:sw a7, 232(ra)
Current Store : [0x80000358] : sw a7, 232(ra) -- Store: [0x80007000]:0x7FACF44D




Last Coverpoint : ['rs1 : x14', 'rs2 : x18', 'rd : x16', 'fs1 == 0 and fe1 == 0x7fa and fm1 == 0xcf44d05dc866f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000034c]:fmul.d a6, a4, s2, dyn
	-[0x80000350]:csrrs tp, fcsr, zero
	-[0x80000354]:sw a6, 224(ra)
	-[0x80000358]:sw a7, 232(ra)
	-[0x8000035c]:sw a6, 240(ra)
Current Store : [0x8000035c] : sw a6, 240(ra) -- Store: [0x80007008]:0x00000000




Last Coverpoint : ['rs1 : x18', 'rs2 : x16', 'rd : x14', 'fs1 == 0 and fe1 == 0x7fa and fm1 == 0xcf44d05dc866f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000394]:fmul.d a4, s2, a6, dyn
	-[0x80000398]:csrrs tp, fcsr, zero
	-[0x8000039c]:sw a4, 256(ra)
	-[0x800003a0]:sw a5, 264(ra)
Current Store : [0x800003a0] : sw a5, 264(ra) -- Store: [0x80007020]:0x7FACF44D




Last Coverpoint : ['rs1 : x18', 'rs2 : x16', 'rd : x14', 'fs1 == 0 and fe1 == 0x7fa and fm1 == 0xcf44d05dc866f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000394]:fmul.d a4, s2, a6, dyn
	-[0x80000398]:csrrs tp, fcsr, zero
	-[0x8000039c]:sw a4, 256(ra)
	-[0x800003a0]:sw a5, 264(ra)
	-[0x800003a4]:sw a4, 272(ra)
Current Store : [0x800003a4] : sw a4, 272(ra) -- Store: [0x80007028]:0x00000000




Last Coverpoint : ['rs1 : x10', 'rs2 : x8', 'rd : x12', 'fs1 == 0 and fe1 == 0x7fa and fm1 == 0xcf44d05dc866f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003dc]:fmul.d a2, a0, fp, dyn
	-[0x800003e0]:csrrs tp, fcsr, zero
	-[0x800003e4]:sw a2, 288(ra)
	-[0x800003e8]:sw a3, 296(ra)
Current Store : [0x800003e8] : sw a3, 296(ra) -- Store: [0x80007040]:0xEADFEEDB




Last Coverpoint : ['rs1 : x10', 'rs2 : x8', 'rd : x12', 'fs1 == 0 and fe1 == 0x7fa and fm1 == 0xcf44d05dc866f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003dc]:fmul.d a2, a0, fp, dyn
	-[0x800003e0]:csrrs tp, fcsr, zero
	-[0x800003e4]:sw a2, 288(ra)
	-[0x800003e8]:sw a3, 296(ra)
	-[0x800003ec]:sw a2, 304(ra)
Current Store : [0x800003ec] : sw a2, 304(ra) -- Store: [0x80007048]:0x00000000




Last Coverpoint : ['rs1 : x8', 'rs2 : x12', 'rd : x10', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xabc6824ad2440 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000434]:fmul.d a0, fp, a2, dyn
	-[0x80000438]:csrrs a6, fcsr, zero
	-[0x8000043c]:sw a0, 0(ra)
	-[0x80000440]:sw a1, 8(ra)
Current Store : [0x80000440] : sw a1, 8(ra) -- Store: [0x80006fc0]:0x7FACF44D




Last Coverpoint : ['rs1 : x8', 'rs2 : x12', 'rd : x10', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xabc6824ad2440 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000434]:fmul.d a0, fp, a2, dyn
	-[0x80000438]:csrrs a6, fcsr, zero
	-[0x8000043c]:sw a0, 0(ra)
	-[0x80000440]:sw a1, 8(ra)
	-[0x80000444]:sw a0, 16(ra)
Current Store : [0x80000444] : sw a0, 16(ra) -- Store: [0x80006fc8]:0x00000000




Last Coverpoint : ['rs1 : x12', 'rs2 : x10', 'rd : x8', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xabc6824ad2440 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000047c]:fmul.d fp, a2, a0, dyn
	-[0x80000480]:csrrs a6, fcsr, zero
	-[0x80000484]:sw fp, 32(ra)
	-[0x80000488]:sw s1, 40(ra)
Current Store : [0x80000488] : sw s1, 40(ra) -- Store: [0x80006fe0]:0x7FEABC68




Last Coverpoint : ['rs1 : x12', 'rs2 : x10', 'rd : x8', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xabc6824ad2440 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000047c]:fmul.d fp, a2, a0, dyn
	-[0x80000480]:csrrs a6, fcsr, zero
	-[0x80000484]:sw fp, 32(ra)
	-[0x80000488]:sw s1, 40(ra)
	-[0x8000048c]:sw fp, 48(ra)
Current Store : [0x8000048c] : sw fp, 48(ra) -- Store: [0x80006fe8]:0x00000000




Last Coverpoint : ['rs1 : x4', 'rs2 : x2', 'rd : x6', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xabc6824ad2440 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004c4]:fmul.d t1, tp, sp, dyn
	-[0x800004c8]:csrrs a6, fcsr, zero
	-[0x800004cc]:sw t1, 64(ra)
	-[0x800004d0]:sw t2, 72(ra)
Current Store : [0x800004d0] : sw t2, 72(ra) -- Store: [0x80007000]:0xB7FBB6FA




Last Coverpoint : ['rs1 : x4', 'rs2 : x2', 'rd : x6', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xabc6824ad2440 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004c4]:fmul.d t1, tp, sp, dyn
	-[0x800004c8]:csrrs a6, fcsr, zero
	-[0x800004cc]:sw t1, 64(ra)
	-[0x800004d0]:sw t2, 72(ra)
	-[0x800004d4]:sw t1, 80(ra)
Current Store : [0x800004d4] : sw t1, 80(ra) -- Store: [0x80007008]:0x00000000




Last Coverpoint : ['rs1 : x2', 'rs2 : x6', 'rd : x4', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xabc6824ad2440 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000050c]:fmul.d tp, sp, t1, dyn
	-[0x80000510]:csrrs a6, fcsr, zero
	-[0x80000514]:sw tp, 96(ra)
	-[0x80000518]:sw t0, 104(ra)
Current Store : [0x80000518] : sw t0, 104(ra) -- Store: [0x80007020]:0x7FEABC68




Last Coverpoint : ['rs1 : x2', 'rs2 : x6', 'rd : x4', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xabc6824ad2440 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000050c]:fmul.d tp, sp, t1, dyn
	-[0x80000510]:csrrs a6, fcsr, zero
	-[0x80000514]:sw tp, 96(ra)
	-[0x80000518]:sw t0, 104(ra)
	-[0x8000051c]:sw tp, 112(ra)
Current Store : [0x8000051c] : sw tp, 112(ra) -- Store: [0x80007028]:0x00000000




Last Coverpoint : ['rs1 : x6', 'rs2 : x4', 'rd : x2', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xabc6824ad2440 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000554]:fmul.d sp, t1, tp, dyn
	-[0x80000558]:csrrs a6, fcsr, zero
	-[0x8000055c]:sw sp, 128(ra)
	-[0x80000560]:sw gp, 136(ra)
Current Store : [0x80000560] : sw gp, 136(ra) -- Store: [0x80007040]:0x7FEABC68




Last Coverpoint : ['rs1 : x6', 'rs2 : x4', 'rd : x2', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xabc6824ad2440 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000554]:fmul.d sp, t1, tp, dyn
	-[0x80000558]:csrrs a6, fcsr, zero
	-[0x8000055c]:sw sp, 128(ra)
	-[0x80000560]:sw gp, 136(ra)
	-[0x80000564]:sw sp, 144(ra)
Current Store : [0x80000564] : sw sp, 144(ra) -- Store: [0x80007048]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x56e6e736a538e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000059c]:fmul.d t5, t3, s10, dyn
	-[0x800005a0]:csrrs a6, fcsr, zero
	-[0x800005a4]:sw t5, 160(ra)
	-[0x800005a8]:sw t6, 168(ra)
Current Store : [0x800005a8] : sw t6, 168(ra) -- Store: [0x80007060]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x56e6e736a538e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000059c]:fmul.d t5, t3, s10, dyn
	-[0x800005a0]:csrrs a6, fcsr, zero
	-[0x800005a4]:sw t5, 160(ra)
	-[0x800005a8]:sw t6, 168(ra)
	-[0x800005ac]:sw t5, 176(ra)
Current Store : [0x800005ac] : sw t5, 176(ra) -- Store: [0x80007068]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x56e6e736a538e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005e4]:fmul.d t5, t3, s10, dyn
	-[0x800005e8]:csrrs a6, fcsr, zero
	-[0x800005ec]:sw t5, 192(ra)
	-[0x800005f0]:sw t6, 200(ra)
Current Store : [0x800005f0] : sw t6, 200(ra) -- Store: [0x80007080]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x56e6e736a538e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005e4]:fmul.d t5, t3, s10, dyn
	-[0x800005e8]:csrrs a6, fcsr, zero
	-[0x800005ec]:sw t5, 192(ra)
	-[0x800005f0]:sw t6, 200(ra)
	-[0x800005f4]:sw t5, 208(ra)
Current Store : [0x800005f4] : sw t5, 208(ra) -- Store: [0x80007088]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x56e6e736a538e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000062c]:fmul.d t5, t3, s10, dyn
	-[0x80000630]:csrrs a6, fcsr, zero
	-[0x80000634]:sw t5, 224(ra)
	-[0x80000638]:sw t6, 232(ra)
Current Store : [0x80000638] : sw t6, 232(ra) -- Store: [0x800070a0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x56e6e736a538e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000062c]:fmul.d t5, t3, s10, dyn
	-[0x80000630]:csrrs a6, fcsr, zero
	-[0x80000634]:sw t5, 224(ra)
	-[0x80000638]:sw t6, 232(ra)
	-[0x8000063c]:sw t5, 240(ra)
Current Store : [0x8000063c] : sw t5, 240(ra) -- Store: [0x800070a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x56e6e736a538e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000674]:fmul.d t5, t3, s10, dyn
	-[0x80000678]:csrrs a6, fcsr, zero
	-[0x8000067c]:sw t5, 256(ra)
	-[0x80000680]:sw t6, 264(ra)
Current Store : [0x80000680] : sw t6, 264(ra) -- Store: [0x800070c0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x56e6e736a538e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000674]:fmul.d t5, t3, s10, dyn
	-[0x80000678]:csrrs a6, fcsr, zero
	-[0x8000067c]:sw t5, 256(ra)
	-[0x80000680]:sw t6, 264(ra)
	-[0x80000684]:sw t5, 272(ra)
Current Store : [0x80000684] : sw t5, 272(ra) -- Store: [0x800070c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x56e6e736a538e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006bc]:fmul.d t5, t3, s10, dyn
	-[0x800006c0]:csrrs a6, fcsr, zero
	-[0x800006c4]:sw t5, 288(ra)
	-[0x800006c8]:sw t6, 296(ra)
Current Store : [0x800006c8] : sw t6, 296(ra) -- Store: [0x800070e0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x56e6e736a538e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006bc]:fmul.d t5, t3, s10, dyn
	-[0x800006c0]:csrrs a6, fcsr, zero
	-[0x800006c4]:sw t5, 288(ra)
	-[0x800006c8]:sw t6, 296(ra)
	-[0x800006cc]:sw t5, 304(ra)
Current Store : [0x800006cc] : sw t5, 304(ra) -- Store: [0x800070e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x363e504d94fe2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000704]:fmul.d t5, t3, s10, dyn
	-[0x80000708]:csrrs a6, fcsr, zero
	-[0x8000070c]:sw t5, 320(ra)
	-[0x80000710]:sw t6, 328(ra)
Current Store : [0x80000710] : sw t6, 328(ra) -- Store: [0x80007100]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x363e504d94fe2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000704]:fmul.d t5, t3, s10, dyn
	-[0x80000708]:csrrs a6, fcsr, zero
	-[0x8000070c]:sw t5, 320(ra)
	-[0x80000710]:sw t6, 328(ra)
	-[0x80000714]:sw t5, 336(ra)
Current Store : [0x80000714] : sw t5, 336(ra) -- Store: [0x80007108]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x363e504d94fe2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000074c]:fmul.d t5, t3, s10, dyn
	-[0x80000750]:csrrs a6, fcsr, zero
	-[0x80000754]:sw t5, 352(ra)
	-[0x80000758]:sw t6, 360(ra)
Current Store : [0x80000758] : sw t6, 360(ra) -- Store: [0x80007120]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x363e504d94fe2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000074c]:fmul.d t5, t3, s10, dyn
	-[0x80000750]:csrrs a6, fcsr, zero
	-[0x80000754]:sw t5, 352(ra)
	-[0x80000758]:sw t6, 360(ra)
	-[0x8000075c]:sw t5, 368(ra)
Current Store : [0x8000075c] : sw t5, 368(ra) -- Store: [0x80007128]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x363e504d94fe2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000794]:fmul.d t5, t3, s10, dyn
	-[0x80000798]:csrrs a6, fcsr, zero
	-[0x8000079c]:sw t5, 384(ra)
	-[0x800007a0]:sw t6, 392(ra)
Current Store : [0x800007a0] : sw t6, 392(ra) -- Store: [0x80007140]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x363e504d94fe2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000794]:fmul.d t5, t3, s10, dyn
	-[0x80000798]:csrrs a6, fcsr, zero
	-[0x8000079c]:sw t5, 384(ra)
	-[0x800007a0]:sw t6, 392(ra)
	-[0x800007a4]:sw t5, 400(ra)
Current Store : [0x800007a4] : sw t5, 400(ra) -- Store: [0x80007148]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x363e504d94fe2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007dc]:fmul.d t5, t3, s10, dyn
	-[0x800007e0]:csrrs a6, fcsr, zero
	-[0x800007e4]:sw t5, 416(ra)
	-[0x800007e8]:sw t6, 424(ra)
Current Store : [0x800007e8] : sw t6, 424(ra) -- Store: [0x80007160]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x363e504d94fe2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007dc]:fmul.d t5, t3, s10, dyn
	-[0x800007e0]:csrrs a6, fcsr, zero
	-[0x800007e4]:sw t5, 416(ra)
	-[0x800007e8]:sw t6, 424(ra)
	-[0x800007ec]:sw t5, 432(ra)
Current Store : [0x800007ec] : sw t5, 432(ra) -- Store: [0x80007168]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x363e504d94fe2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000824]:fmul.d t5, t3, s10, dyn
	-[0x80000828]:csrrs a6, fcsr, zero
	-[0x8000082c]:sw t5, 448(ra)
	-[0x80000830]:sw t6, 456(ra)
Current Store : [0x80000830] : sw t6, 456(ra) -- Store: [0x80007180]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x363e504d94fe2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000824]:fmul.d t5, t3, s10, dyn
	-[0x80000828]:csrrs a6, fcsr, zero
	-[0x8000082c]:sw t5, 448(ra)
	-[0x80000830]:sw t6, 456(ra)
	-[0x80000834]:sw t5, 464(ra)
Current Store : [0x80000834] : sw t5, 464(ra) -- Store: [0x80007188]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x299392ab99898 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000086c]:fmul.d t5, t3, s10, dyn
	-[0x80000870]:csrrs a6, fcsr, zero
	-[0x80000874]:sw t5, 480(ra)
	-[0x80000878]:sw t6, 488(ra)
Current Store : [0x80000878] : sw t6, 488(ra) -- Store: [0x800071a0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x299392ab99898 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000086c]:fmul.d t5, t3, s10, dyn
	-[0x80000870]:csrrs a6, fcsr, zero
	-[0x80000874]:sw t5, 480(ra)
	-[0x80000878]:sw t6, 488(ra)
	-[0x8000087c]:sw t5, 496(ra)
Current Store : [0x8000087c] : sw t5, 496(ra) -- Store: [0x800071a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x299392ab99898 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008b4]:fmul.d t5, t3, s10, dyn
	-[0x800008b8]:csrrs a6, fcsr, zero
	-[0x800008bc]:sw t5, 512(ra)
	-[0x800008c0]:sw t6, 520(ra)
Current Store : [0x800008c0] : sw t6, 520(ra) -- Store: [0x800071c0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x299392ab99898 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008b4]:fmul.d t5, t3, s10, dyn
	-[0x800008b8]:csrrs a6, fcsr, zero
	-[0x800008bc]:sw t5, 512(ra)
	-[0x800008c0]:sw t6, 520(ra)
	-[0x800008c4]:sw t5, 528(ra)
Current Store : [0x800008c4] : sw t5, 528(ra) -- Store: [0x800071c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x299392ab99898 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008fc]:fmul.d t5, t3, s10, dyn
	-[0x80000900]:csrrs a6, fcsr, zero
	-[0x80000904]:sw t5, 544(ra)
	-[0x80000908]:sw t6, 552(ra)
Current Store : [0x80000908] : sw t6, 552(ra) -- Store: [0x800071e0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x299392ab99898 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008fc]:fmul.d t5, t3, s10, dyn
	-[0x80000900]:csrrs a6, fcsr, zero
	-[0x80000904]:sw t5, 544(ra)
	-[0x80000908]:sw t6, 552(ra)
	-[0x8000090c]:sw t5, 560(ra)
Current Store : [0x8000090c] : sw t5, 560(ra) -- Store: [0x800071e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x299392ab99898 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000944]:fmul.d t5, t3, s10, dyn
	-[0x80000948]:csrrs a6, fcsr, zero
	-[0x8000094c]:sw t5, 576(ra)
	-[0x80000950]:sw t6, 584(ra)
Current Store : [0x80000950] : sw t6, 584(ra) -- Store: [0x80007200]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x299392ab99898 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000944]:fmul.d t5, t3, s10, dyn
	-[0x80000948]:csrrs a6, fcsr, zero
	-[0x8000094c]:sw t5, 576(ra)
	-[0x80000950]:sw t6, 584(ra)
	-[0x80000954]:sw t5, 592(ra)
Current Store : [0x80000954] : sw t5, 592(ra) -- Store: [0x80007208]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x299392ab99898 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000098c]:fmul.d t5, t3, s10, dyn
	-[0x80000990]:csrrs a6, fcsr, zero
	-[0x80000994]:sw t5, 608(ra)
	-[0x80000998]:sw t6, 616(ra)
Current Store : [0x80000998] : sw t6, 616(ra) -- Store: [0x80007220]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x299392ab99898 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000098c]:fmul.d t5, t3, s10, dyn
	-[0x80000990]:csrrs a6, fcsr, zero
	-[0x80000994]:sw t5, 608(ra)
	-[0x80000998]:sw t6, 616(ra)
	-[0x8000099c]:sw t5, 624(ra)
Current Store : [0x8000099c] : sw t5, 624(ra) -- Store: [0x80007228]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xb9017651b96db and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009d4]:fmul.d t5, t3, s10, dyn
	-[0x800009d8]:csrrs a6, fcsr, zero
	-[0x800009dc]:sw t5, 640(ra)
	-[0x800009e0]:sw t6, 648(ra)
Current Store : [0x800009e0] : sw t6, 648(ra) -- Store: [0x80007240]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xb9017651b96db and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009d4]:fmul.d t5, t3, s10, dyn
	-[0x800009d8]:csrrs a6, fcsr, zero
	-[0x800009dc]:sw t5, 640(ra)
	-[0x800009e0]:sw t6, 648(ra)
	-[0x800009e4]:sw t5, 656(ra)
Current Store : [0x800009e4] : sw t5, 656(ra) -- Store: [0x80007248]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xb9017651b96db and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a1c]:fmul.d t5, t3, s10, dyn
	-[0x80000a20]:csrrs a6, fcsr, zero
	-[0x80000a24]:sw t5, 672(ra)
	-[0x80000a28]:sw t6, 680(ra)
Current Store : [0x80000a28] : sw t6, 680(ra) -- Store: [0x80007260]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xb9017651b96db and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a1c]:fmul.d t5, t3, s10, dyn
	-[0x80000a20]:csrrs a6, fcsr, zero
	-[0x80000a24]:sw t5, 672(ra)
	-[0x80000a28]:sw t6, 680(ra)
	-[0x80000a2c]:sw t5, 688(ra)
Current Store : [0x80000a2c] : sw t5, 688(ra) -- Store: [0x80007268]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xb9017651b96db and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a64]:fmul.d t5, t3, s10, dyn
	-[0x80000a68]:csrrs a6, fcsr, zero
	-[0x80000a6c]:sw t5, 704(ra)
	-[0x80000a70]:sw t6, 712(ra)
Current Store : [0x80000a70] : sw t6, 712(ra) -- Store: [0x80007280]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xb9017651b96db and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a64]:fmul.d t5, t3, s10, dyn
	-[0x80000a68]:csrrs a6, fcsr, zero
	-[0x80000a6c]:sw t5, 704(ra)
	-[0x80000a70]:sw t6, 712(ra)
	-[0x80000a74]:sw t5, 720(ra)
Current Store : [0x80000a74] : sw t5, 720(ra) -- Store: [0x80007288]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xb9017651b96db and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000aac]:fmul.d t5, t3, s10, dyn
	-[0x80000ab0]:csrrs a6, fcsr, zero
	-[0x80000ab4]:sw t5, 736(ra)
	-[0x80000ab8]:sw t6, 744(ra)
Current Store : [0x80000ab8] : sw t6, 744(ra) -- Store: [0x800072a0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xb9017651b96db and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000aac]:fmul.d t5, t3, s10, dyn
	-[0x80000ab0]:csrrs a6, fcsr, zero
	-[0x80000ab4]:sw t5, 736(ra)
	-[0x80000ab8]:sw t6, 744(ra)
	-[0x80000abc]:sw t5, 752(ra)
Current Store : [0x80000abc] : sw t5, 752(ra) -- Store: [0x800072a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xb9017651b96db and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000af4]:fmul.d t5, t3, s10, dyn
	-[0x80000af8]:csrrs a6, fcsr, zero
	-[0x80000afc]:sw t5, 768(ra)
	-[0x80000b00]:sw t6, 776(ra)
Current Store : [0x80000b00] : sw t6, 776(ra) -- Store: [0x800072c0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xb9017651b96db and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000af4]:fmul.d t5, t3, s10, dyn
	-[0x80000af8]:csrrs a6, fcsr, zero
	-[0x80000afc]:sw t5, 768(ra)
	-[0x80000b00]:sw t6, 776(ra)
	-[0x80000b04]:sw t5, 784(ra)
Current Store : [0x80000b04] : sw t5, 784(ra) -- Store: [0x800072c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x722ea3b70e3d3 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b3c]:fmul.d t5, t3, s10, dyn
	-[0x80000b40]:csrrs a6, fcsr, zero
	-[0x80000b44]:sw t5, 800(ra)
	-[0x80000b48]:sw t6, 808(ra)
Current Store : [0x80000b48] : sw t6, 808(ra) -- Store: [0x800072e0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x722ea3b70e3d3 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b3c]:fmul.d t5, t3, s10, dyn
	-[0x80000b40]:csrrs a6, fcsr, zero
	-[0x80000b44]:sw t5, 800(ra)
	-[0x80000b48]:sw t6, 808(ra)
	-[0x80000b4c]:sw t5, 816(ra)
Current Store : [0x80000b4c] : sw t5, 816(ra) -- Store: [0x800072e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x722ea3b70e3d3 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b84]:fmul.d t5, t3, s10, dyn
	-[0x80000b88]:csrrs a6, fcsr, zero
	-[0x80000b8c]:sw t5, 832(ra)
	-[0x80000b90]:sw t6, 840(ra)
Current Store : [0x80000b90] : sw t6, 840(ra) -- Store: [0x80007300]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x722ea3b70e3d3 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b84]:fmul.d t5, t3, s10, dyn
	-[0x80000b88]:csrrs a6, fcsr, zero
	-[0x80000b8c]:sw t5, 832(ra)
	-[0x80000b90]:sw t6, 840(ra)
	-[0x80000b94]:sw t5, 848(ra)
Current Store : [0x80000b94] : sw t5, 848(ra) -- Store: [0x80007308]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x722ea3b70e3d3 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bcc]:fmul.d t5, t3, s10, dyn
	-[0x80000bd0]:csrrs a6, fcsr, zero
	-[0x80000bd4]:sw t5, 864(ra)
	-[0x80000bd8]:sw t6, 872(ra)
Current Store : [0x80000bd8] : sw t6, 872(ra) -- Store: [0x80007320]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x722ea3b70e3d3 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bcc]:fmul.d t5, t3, s10, dyn
	-[0x80000bd0]:csrrs a6, fcsr, zero
	-[0x80000bd4]:sw t5, 864(ra)
	-[0x80000bd8]:sw t6, 872(ra)
	-[0x80000bdc]:sw t5, 880(ra)
Current Store : [0x80000bdc] : sw t5, 880(ra) -- Store: [0x80007328]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x722ea3b70e3d3 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c14]:fmul.d t5, t3, s10, dyn
	-[0x80000c18]:csrrs a6, fcsr, zero
	-[0x80000c1c]:sw t5, 896(ra)
	-[0x80000c20]:sw t6, 904(ra)
Current Store : [0x80000c20] : sw t6, 904(ra) -- Store: [0x80007340]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x722ea3b70e3d3 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c14]:fmul.d t5, t3, s10, dyn
	-[0x80000c18]:csrrs a6, fcsr, zero
	-[0x80000c1c]:sw t5, 896(ra)
	-[0x80000c20]:sw t6, 904(ra)
	-[0x80000c24]:sw t5, 912(ra)
Current Store : [0x80000c24] : sw t5, 912(ra) -- Store: [0x80007348]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x722ea3b70e3d3 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c5c]:fmul.d t5, t3, s10, dyn
	-[0x80000c60]:csrrs a6, fcsr, zero
	-[0x80000c64]:sw t5, 928(ra)
	-[0x80000c68]:sw t6, 936(ra)
Current Store : [0x80000c68] : sw t6, 936(ra) -- Store: [0x80007360]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x722ea3b70e3d3 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c5c]:fmul.d t5, t3, s10, dyn
	-[0x80000c60]:csrrs a6, fcsr, zero
	-[0x80000c64]:sw t5, 928(ra)
	-[0x80000c68]:sw t6, 936(ra)
	-[0x80000c6c]:sw t5, 944(ra)
Current Store : [0x80000c6c] : sw t5, 944(ra) -- Store: [0x80007368]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe61729d7cfd5e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ca4]:fmul.d t5, t3, s10, dyn
	-[0x80000ca8]:csrrs a6, fcsr, zero
	-[0x80000cac]:sw t5, 960(ra)
	-[0x80000cb0]:sw t6, 968(ra)
Current Store : [0x80000cb0] : sw t6, 968(ra) -- Store: [0x80007380]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe61729d7cfd5e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ca4]:fmul.d t5, t3, s10, dyn
	-[0x80000ca8]:csrrs a6, fcsr, zero
	-[0x80000cac]:sw t5, 960(ra)
	-[0x80000cb0]:sw t6, 968(ra)
	-[0x80000cb4]:sw t5, 976(ra)
Current Store : [0x80000cb4] : sw t5, 976(ra) -- Store: [0x80007388]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe61729d7cfd5e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cec]:fmul.d t5, t3, s10, dyn
	-[0x80000cf0]:csrrs a6, fcsr, zero
	-[0x80000cf4]:sw t5, 992(ra)
	-[0x80000cf8]:sw t6, 1000(ra)
Current Store : [0x80000cf8] : sw t6, 1000(ra) -- Store: [0x800073a0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe61729d7cfd5e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cec]:fmul.d t5, t3, s10, dyn
	-[0x80000cf0]:csrrs a6, fcsr, zero
	-[0x80000cf4]:sw t5, 992(ra)
	-[0x80000cf8]:sw t6, 1000(ra)
	-[0x80000cfc]:sw t5, 1008(ra)
Current Store : [0x80000cfc] : sw t5, 1008(ra) -- Store: [0x800073a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe61729d7cfd5e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d34]:fmul.d t5, t3, s10, dyn
	-[0x80000d38]:csrrs a6, fcsr, zero
	-[0x80000d3c]:sw t5, 1024(ra)
	-[0x80000d40]:sw t6, 1032(ra)
Current Store : [0x80000d40] : sw t6, 1032(ra) -- Store: [0x800073c0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe61729d7cfd5e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d34]:fmul.d t5, t3, s10, dyn
	-[0x80000d38]:csrrs a6, fcsr, zero
	-[0x80000d3c]:sw t5, 1024(ra)
	-[0x80000d40]:sw t6, 1032(ra)
	-[0x80000d44]:sw t5, 1040(ra)
Current Store : [0x80000d44] : sw t5, 1040(ra) -- Store: [0x800073c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe61729d7cfd5e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d7c]:fmul.d t5, t3, s10, dyn
	-[0x80000d80]:csrrs a6, fcsr, zero
	-[0x80000d84]:sw t5, 1056(ra)
	-[0x80000d88]:sw t6, 1064(ra)
Current Store : [0x80000d88] : sw t6, 1064(ra) -- Store: [0x800073e0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe61729d7cfd5e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d7c]:fmul.d t5, t3, s10, dyn
	-[0x80000d80]:csrrs a6, fcsr, zero
	-[0x80000d84]:sw t5, 1056(ra)
	-[0x80000d88]:sw t6, 1064(ra)
	-[0x80000d8c]:sw t5, 1072(ra)
Current Store : [0x80000d8c] : sw t5, 1072(ra) -- Store: [0x800073e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe61729d7cfd5e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000dc4]:fmul.d t5, t3, s10, dyn
	-[0x80000dc8]:csrrs a6, fcsr, zero
	-[0x80000dcc]:sw t5, 1088(ra)
	-[0x80000dd0]:sw t6, 1096(ra)
Current Store : [0x80000dd0] : sw t6, 1096(ra) -- Store: [0x80007400]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe61729d7cfd5e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000dc4]:fmul.d t5, t3, s10, dyn
	-[0x80000dc8]:csrrs a6, fcsr, zero
	-[0x80000dcc]:sw t5, 1088(ra)
	-[0x80000dd0]:sw t6, 1096(ra)
	-[0x80000dd4]:sw t5, 1104(ra)
Current Store : [0x80000dd4] : sw t5, 1104(ra) -- Store: [0x80007408]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc787db4043bd9 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e0c]:fmul.d t5, t3, s10, dyn
	-[0x80000e10]:csrrs a6, fcsr, zero
	-[0x80000e14]:sw t5, 1120(ra)
	-[0x80000e18]:sw t6, 1128(ra)
Current Store : [0x80000e18] : sw t6, 1128(ra) -- Store: [0x80007420]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc787db4043bd9 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e0c]:fmul.d t5, t3, s10, dyn
	-[0x80000e10]:csrrs a6, fcsr, zero
	-[0x80000e14]:sw t5, 1120(ra)
	-[0x80000e18]:sw t6, 1128(ra)
	-[0x80000e1c]:sw t5, 1136(ra)
Current Store : [0x80000e1c] : sw t5, 1136(ra) -- Store: [0x80007428]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc787db4043bd9 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e54]:fmul.d t5, t3, s10, dyn
	-[0x80000e58]:csrrs a6, fcsr, zero
	-[0x80000e5c]:sw t5, 1152(ra)
	-[0x80000e60]:sw t6, 1160(ra)
Current Store : [0x80000e60] : sw t6, 1160(ra) -- Store: [0x80007440]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc787db4043bd9 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e54]:fmul.d t5, t3, s10, dyn
	-[0x80000e58]:csrrs a6, fcsr, zero
	-[0x80000e5c]:sw t5, 1152(ra)
	-[0x80000e60]:sw t6, 1160(ra)
	-[0x80000e64]:sw t5, 1168(ra)
Current Store : [0x80000e64] : sw t5, 1168(ra) -- Store: [0x80007448]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc787db4043bd9 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e9c]:fmul.d t5, t3, s10, dyn
	-[0x80000ea0]:csrrs a6, fcsr, zero
	-[0x80000ea4]:sw t5, 1184(ra)
	-[0x80000ea8]:sw t6, 1192(ra)
Current Store : [0x80000ea8] : sw t6, 1192(ra) -- Store: [0x80007460]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc787db4043bd9 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e9c]:fmul.d t5, t3, s10, dyn
	-[0x80000ea0]:csrrs a6, fcsr, zero
	-[0x80000ea4]:sw t5, 1184(ra)
	-[0x80000ea8]:sw t6, 1192(ra)
	-[0x80000eac]:sw t5, 1200(ra)
Current Store : [0x80000eac] : sw t5, 1200(ra) -- Store: [0x80007468]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc787db4043bd9 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ee4]:fmul.d t5, t3, s10, dyn
	-[0x80000ee8]:csrrs a6, fcsr, zero
	-[0x80000eec]:sw t5, 1216(ra)
	-[0x80000ef0]:sw t6, 1224(ra)
Current Store : [0x80000ef0] : sw t6, 1224(ra) -- Store: [0x80007480]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc787db4043bd9 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ee4]:fmul.d t5, t3, s10, dyn
	-[0x80000ee8]:csrrs a6, fcsr, zero
	-[0x80000eec]:sw t5, 1216(ra)
	-[0x80000ef0]:sw t6, 1224(ra)
	-[0x80000ef4]:sw t5, 1232(ra)
Current Store : [0x80000ef4] : sw t5, 1232(ra) -- Store: [0x80007488]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc787db4043bd9 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f2c]:fmul.d t5, t3, s10, dyn
	-[0x80000f30]:csrrs a6, fcsr, zero
	-[0x80000f34]:sw t5, 1248(ra)
	-[0x80000f38]:sw t6, 1256(ra)
Current Store : [0x80000f38] : sw t6, 1256(ra) -- Store: [0x800074a0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc787db4043bd9 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f2c]:fmul.d t5, t3, s10, dyn
	-[0x80000f30]:csrrs a6, fcsr, zero
	-[0x80000f34]:sw t5, 1248(ra)
	-[0x80000f38]:sw t6, 1256(ra)
	-[0x80000f3c]:sw t5, 1264(ra)
Current Store : [0x80000f3c] : sw t5, 1264(ra) -- Store: [0x800074a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fa and fm1 == 0x264ac77bf010f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f74]:fmul.d t5, t3, s10, dyn
	-[0x80000f78]:csrrs a6, fcsr, zero
	-[0x80000f7c]:sw t5, 1280(ra)
	-[0x80000f80]:sw t6, 1288(ra)
Current Store : [0x80000f80] : sw t6, 1288(ra) -- Store: [0x800074c0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fa and fm1 == 0x264ac77bf010f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f74]:fmul.d t5, t3, s10, dyn
	-[0x80000f78]:csrrs a6, fcsr, zero
	-[0x80000f7c]:sw t5, 1280(ra)
	-[0x80000f80]:sw t6, 1288(ra)
	-[0x80000f84]:sw t5, 1296(ra)
Current Store : [0x80000f84] : sw t5, 1296(ra) -- Store: [0x800074c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fa and fm1 == 0x264ac77bf010f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fbc]:fmul.d t5, t3, s10, dyn
	-[0x80000fc0]:csrrs a6, fcsr, zero
	-[0x80000fc4]:sw t5, 1312(ra)
	-[0x80000fc8]:sw t6, 1320(ra)
Current Store : [0x80000fc8] : sw t6, 1320(ra) -- Store: [0x800074e0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fa and fm1 == 0x264ac77bf010f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fbc]:fmul.d t5, t3, s10, dyn
	-[0x80000fc0]:csrrs a6, fcsr, zero
	-[0x80000fc4]:sw t5, 1312(ra)
	-[0x80000fc8]:sw t6, 1320(ra)
	-[0x80000fcc]:sw t5, 1328(ra)
Current Store : [0x80000fcc] : sw t5, 1328(ra) -- Store: [0x800074e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fa and fm1 == 0x264ac77bf010f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001004]:fmul.d t5, t3, s10, dyn
	-[0x80001008]:csrrs a6, fcsr, zero
	-[0x8000100c]:sw t5, 1344(ra)
	-[0x80001010]:sw t6, 1352(ra)
Current Store : [0x80001010] : sw t6, 1352(ra) -- Store: [0x80007500]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fa and fm1 == 0x264ac77bf010f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001004]:fmul.d t5, t3, s10, dyn
	-[0x80001008]:csrrs a6, fcsr, zero
	-[0x8000100c]:sw t5, 1344(ra)
	-[0x80001010]:sw t6, 1352(ra)
	-[0x80001014]:sw t5, 1360(ra)
Current Store : [0x80001014] : sw t5, 1360(ra) -- Store: [0x80007508]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fa and fm1 == 0x264ac77bf010f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000104c]:fmul.d t5, t3, s10, dyn
	-[0x80001050]:csrrs a6, fcsr, zero
	-[0x80001054]:sw t5, 1376(ra)
	-[0x80001058]:sw t6, 1384(ra)
Current Store : [0x80001058] : sw t6, 1384(ra) -- Store: [0x80007520]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fa and fm1 == 0x264ac77bf010f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000104c]:fmul.d t5, t3, s10, dyn
	-[0x80001050]:csrrs a6, fcsr, zero
	-[0x80001054]:sw t5, 1376(ra)
	-[0x80001058]:sw t6, 1384(ra)
	-[0x8000105c]:sw t5, 1392(ra)
Current Store : [0x8000105c] : sw t5, 1392(ra) -- Store: [0x80007528]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fa and fm1 == 0x264ac77bf010f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001094]:fmul.d t5, t3, s10, dyn
	-[0x80001098]:csrrs a6, fcsr, zero
	-[0x8000109c]:sw t5, 1408(ra)
	-[0x800010a0]:sw t6, 1416(ra)
Current Store : [0x800010a0] : sw t6, 1416(ra) -- Store: [0x80007540]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fa and fm1 == 0x264ac77bf010f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001094]:fmul.d t5, t3, s10, dyn
	-[0x80001098]:csrrs a6, fcsr, zero
	-[0x8000109c]:sw t5, 1408(ra)
	-[0x800010a0]:sw t6, 1416(ra)
	-[0x800010a4]:sw t5, 1424(ra)
Current Store : [0x800010a4] : sw t5, 1424(ra) -- Store: [0x80007548]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xdc0d22f746bf5 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010dc]:fmul.d t5, t3, s10, dyn
	-[0x800010e0]:csrrs a6, fcsr, zero
	-[0x800010e4]:sw t5, 1440(ra)
	-[0x800010e8]:sw t6, 1448(ra)
Current Store : [0x800010e8] : sw t6, 1448(ra) -- Store: [0x80007560]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xdc0d22f746bf5 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010dc]:fmul.d t5, t3, s10, dyn
	-[0x800010e0]:csrrs a6, fcsr, zero
	-[0x800010e4]:sw t5, 1440(ra)
	-[0x800010e8]:sw t6, 1448(ra)
	-[0x800010ec]:sw t5, 1456(ra)
Current Store : [0x800010ec] : sw t5, 1456(ra) -- Store: [0x80007568]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xdc0d22f746bf5 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001124]:fmul.d t5, t3, s10, dyn
	-[0x80001128]:csrrs a6, fcsr, zero
	-[0x8000112c]:sw t5, 1472(ra)
	-[0x80001130]:sw t6, 1480(ra)
Current Store : [0x80001130] : sw t6, 1480(ra) -- Store: [0x80007580]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xdc0d22f746bf5 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001124]:fmul.d t5, t3, s10, dyn
	-[0x80001128]:csrrs a6, fcsr, zero
	-[0x8000112c]:sw t5, 1472(ra)
	-[0x80001130]:sw t6, 1480(ra)
	-[0x80001134]:sw t5, 1488(ra)
Current Store : [0x80001134] : sw t5, 1488(ra) -- Store: [0x80007588]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xdc0d22f746bf5 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000116c]:fmul.d t5, t3, s10, dyn
	-[0x80001170]:csrrs a6, fcsr, zero
	-[0x80001174]:sw t5, 1504(ra)
	-[0x80001178]:sw t6, 1512(ra)
Current Store : [0x80001178] : sw t6, 1512(ra) -- Store: [0x800075a0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xdc0d22f746bf5 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000116c]:fmul.d t5, t3, s10, dyn
	-[0x80001170]:csrrs a6, fcsr, zero
	-[0x80001174]:sw t5, 1504(ra)
	-[0x80001178]:sw t6, 1512(ra)
	-[0x8000117c]:sw t5, 1520(ra)
Current Store : [0x8000117c] : sw t5, 1520(ra) -- Store: [0x800075a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xdc0d22f746bf5 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011b4]:fmul.d t5, t3, s10, dyn
	-[0x800011b8]:csrrs a6, fcsr, zero
	-[0x800011bc]:sw t5, 1536(ra)
	-[0x800011c0]:sw t6, 1544(ra)
Current Store : [0x800011c0] : sw t6, 1544(ra) -- Store: [0x800075c0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xdc0d22f746bf5 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011b4]:fmul.d t5, t3, s10, dyn
	-[0x800011b8]:csrrs a6, fcsr, zero
	-[0x800011bc]:sw t5, 1536(ra)
	-[0x800011c0]:sw t6, 1544(ra)
	-[0x800011c4]:sw t5, 1552(ra)
Current Store : [0x800011c4] : sw t5, 1552(ra) -- Store: [0x800075c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xdc0d22f746bf5 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011fc]:fmul.d t5, t3, s10, dyn
	-[0x80001200]:csrrs a6, fcsr, zero
	-[0x80001204]:sw t5, 1568(ra)
	-[0x80001208]:sw t6, 1576(ra)
Current Store : [0x80001208] : sw t6, 1576(ra) -- Store: [0x800075e0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xdc0d22f746bf5 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011fc]:fmul.d t5, t3, s10, dyn
	-[0x80001200]:csrrs a6, fcsr, zero
	-[0x80001204]:sw t5, 1568(ra)
	-[0x80001208]:sw t6, 1576(ra)
	-[0x8000120c]:sw t5, 1584(ra)
Current Store : [0x8000120c] : sw t5, 1584(ra) -- Store: [0x800075e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x8522a1b638e23 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001244]:fmul.d t5, t3, s10, dyn
	-[0x80001248]:csrrs a6, fcsr, zero
	-[0x8000124c]:sw t5, 1600(ra)
	-[0x80001250]:sw t6, 1608(ra)
Current Store : [0x80001250] : sw t6, 1608(ra) -- Store: [0x80007600]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x8522a1b638e23 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001244]:fmul.d t5, t3, s10, dyn
	-[0x80001248]:csrrs a6, fcsr, zero
	-[0x8000124c]:sw t5, 1600(ra)
	-[0x80001250]:sw t6, 1608(ra)
	-[0x80001254]:sw t5, 1616(ra)
Current Store : [0x80001254] : sw t5, 1616(ra) -- Store: [0x80007608]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x8522a1b638e23 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000128c]:fmul.d t5, t3, s10, dyn
	-[0x80001290]:csrrs a6, fcsr, zero
	-[0x80001294]:sw t5, 1632(ra)
	-[0x80001298]:sw t6, 1640(ra)
Current Store : [0x80001298] : sw t6, 1640(ra) -- Store: [0x80007620]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x8522a1b638e23 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000128c]:fmul.d t5, t3, s10, dyn
	-[0x80001290]:csrrs a6, fcsr, zero
	-[0x80001294]:sw t5, 1632(ra)
	-[0x80001298]:sw t6, 1640(ra)
	-[0x8000129c]:sw t5, 1648(ra)
Current Store : [0x8000129c] : sw t5, 1648(ra) -- Store: [0x80007628]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x8522a1b638e23 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012d4]:fmul.d t5, t3, s10, dyn
	-[0x800012d8]:csrrs a6, fcsr, zero
	-[0x800012dc]:sw t5, 1664(ra)
	-[0x800012e0]:sw t6, 1672(ra)
Current Store : [0x800012e0] : sw t6, 1672(ra) -- Store: [0x80007640]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x8522a1b638e23 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012d4]:fmul.d t5, t3, s10, dyn
	-[0x800012d8]:csrrs a6, fcsr, zero
	-[0x800012dc]:sw t5, 1664(ra)
	-[0x800012e0]:sw t6, 1672(ra)
	-[0x800012e4]:sw t5, 1680(ra)
Current Store : [0x800012e4] : sw t5, 1680(ra) -- Store: [0x80007648]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x8522a1b638e23 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000131c]:fmul.d t5, t3, s10, dyn
	-[0x80001320]:csrrs a6, fcsr, zero
	-[0x80001324]:sw t5, 1696(ra)
	-[0x80001328]:sw t6, 1704(ra)
Current Store : [0x80001328] : sw t6, 1704(ra) -- Store: [0x80007660]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x8522a1b638e23 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000131c]:fmul.d t5, t3, s10, dyn
	-[0x80001320]:csrrs a6, fcsr, zero
	-[0x80001324]:sw t5, 1696(ra)
	-[0x80001328]:sw t6, 1704(ra)
	-[0x8000132c]:sw t5, 1712(ra)
Current Store : [0x8000132c] : sw t5, 1712(ra) -- Store: [0x80007668]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x8522a1b638e23 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001364]:fmul.d t5, t3, s10, dyn
	-[0x80001368]:csrrs a6, fcsr, zero
	-[0x8000136c]:sw t5, 1728(ra)
	-[0x80001370]:sw t6, 1736(ra)
Current Store : [0x80001370] : sw t6, 1736(ra) -- Store: [0x80007680]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x8522a1b638e23 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001364]:fmul.d t5, t3, s10, dyn
	-[0x80001368]:csrrs a6, fcsr, zero
	-[0x8000136c]:sw t5, 1728(ra)
	-[0x80001370]:sw t6, 1736(ra)
	-[0x80001374]:sw t5, 1744(ra)
Current Store : [0x80001374] : sw t5, 1744(ra) -- Store: [0x80007688]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0d2f778a86fa6 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013ac]:fmul.d t5, t3, s10, dyn
	-[0x800013b0]:csrrs a6, fcsr, zero
	-[0x800013b4]:sw t5, 1760(ra)
	-[0x800013b8]:sw t6, 1768(ra)
Current Store : [0x800013b8] : sw t6, 1768(ra) -- Store: [0x800076a0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0d2f778a86fa6 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013ac]:fmul.d t5, t3, s10, dyn
	-[0x800013b0]:csrrs a6, fcsr, zero
	-[0x800013b4]:sw t5, 1760(ra)
	-[0x800013b8]:sw t6, 1768(ra)
	-[0x800013bc]:sw t5, 1776(ra)
Current Store : [0x800013bc] : sw t5, 1776(ra) -- Store: [0x800076a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0d2f778a86fa6 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013f4]:fmul.d t5, t3, s10, dyn
	-[0x800013f8]:csrrs a6, fcsr, zero
	-[0x800013fc]:sw t5, 1792(ra)
	-[0x80001400]:sw t6, 1800(ra)
Current Store : [0x80001400] : sw t6, 1800(ra) -- Store: [0x800076c0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0d2f778a86fa6 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013f4]:fmul.d t5, t3, s10, dyn
	-[0x800013f8]:csrrs a6, fcsr, zero
	-[0x800013fc]:sw t5, 1792(ra)
	-[0x80001400]:sw t6, 1800(ra)
	-[0x80001404]:sw t5, 1808(ra)
Current Store : [0x80001404] : sw t5, 1808(ra) -- Store: [0x800076c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0d2f778a86fa6 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000143c]:fmul.d t5, t3, s10, dyn
	-[0x80001440]:csrrs a6, fcsr, zero
	-[0x80001444]:sw t5, 1824(ra)
	-[0x80001448]:sw t6, 1832(ra)
Current Store : [0x80001448] : sw t6, 1832(ra) -- Store: [0x800076e0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0d2f778a86fa6 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000143c]:fmul.d t5, t3, s10, dyn
	-[0x80001440]:csrrs a6, fcsr, zero
	-[0x80001444]:sw t5, 1824(ra)
	-[0x80001448]:sw t6, 1832(ra)
	-[0x8000144c]:sw t5, 1840(ra)
Current Store : [0x8000144c] : sw t5, 1840(ra) -- Store: [0x800076e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0d2f778a86fa6 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001484]:fmul.d t5, t3, s10, dyn
	-[0x80001488]:csrrs a6, fcsr, zero
	-[0x8000148c]:sw t5, 1856(ra)
	-[0x80001490]:sw t6, 1864(ra)
Current Store : [0x80001490] : sw t6, 1864(ra) -- Store: [0x80007700]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0d2f778a86fa6 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001484]:fmul.d t5, t3, s10, dyn
	-[0x80001488]:csrrs a6, fcsr, zero
	-[0x8000148c]:sw t5, 1856(ra)
	-[0x80001490]:sw t6, 1864(ra)
	-[0x80001494]:sw t5, 1872(ra)
Current Store : [0x80001494] : sw t5, 1872(ra) -- Store: [0x80007708]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0d2f778a86fa6 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014cc]:fmul.d t5, t3, s10, dyn
	-[0x800014d0]:csrrs a6, fcsr, zero
	-[0x800014d4]:sw t5, 1888(ra)
	-[0x800014d8]:sw t6, 1896(ra)
Current Store : [0x800014d8] : sw t6, 1896(ra) -- Store: [0x80007720]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0d2f778a86fa6 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014cc]:fmul.d t5, t3, s10, dyn
	-[0x800014d0]:csrrs a6, fcsr, zero
	-[0x800014d4]:sw t5, 1888(ra)
	-[0x800014d8]:sw t6, 1896(ra)
	-[0x800014dc]:sw t5, 1904(ra)
Current Store : [0x800014dc] : sw t5, 1904(ra) -- Store: [0x80007728]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xe394ab3b08c6b and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001514]:fmul.d t5, t3, s10, dyn
	-[0x80001518]:csrrs a6, fcsr, zero
	-[0x8000151c]:sw t5, 1920(ra)
	-[0x80001520]:sw t6, 1928(ra)
Current Store : [0x80001520] : sw t6, 1928(ra) -- Store: [0x80007740]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xe394ab3b08c6b and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001514]:fmul.d t5, t3, s10, dyn
	-[0x80001518]:csrrs a6, fcsr, zero
	-[0x8000151c]:sw t5, 1920(ra)
	-[0x80001520]:sw t6, 1928(ra)
	-[0x80001524]:sw t5, 1936(ra)
Current Store : [0x80001524] : sw t5, 1936(ra) -- Store: [0x80007748]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xe394ab3b08c6b and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000155c]:fmul.d t5, t3, s10, dyn
	-[0x80001560]:csrrs a6, fcsr, zero
	-[0x80001564]:sw t5, 1952(ra)
	-[0x80001568]:sw t6, 1960(ra)
Current Store : [0x80001568] : sw t6, 1960(ra) -- Store: [0x80007760]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xe394ab3b08c6b and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000155c]:fmul.d t5, t3, s10, dyn
	-[0x80001560]:csrrs a6, fcsr, zero
	-[0x80001564]:sw t5, 1952(ra)
	-[0x80001568]:sw t6, 1960(ra)
	-[0x8000156c]:sw t5, 1968(ra)
Current Store : [0x8000156c] : sw t5, 1968(ra) -- Store: [0x80007768]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xe394ab3b08c6b and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015a4]:fmul.d t5, t3, s10, dyn
	-[0x800015a8]:csrrs a6, fcsr, zero
	-[0x800015ac]:sw t5, 1984(ra)
	-[0x800015b0]:sw t6, 1992(ra)
Current Store : [0x800015b0] : sw t6, 1992(ra) -- Store: [0x80007780]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xe394ab3b08c6b and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015a4]:fmul.d t5, t3, s10, dyn
	-[0x800015a8]:csrrs a6, fcsr, zero
	-[0x800015ac]:sw t5, 1984(ra)
	-[0x800015b0]:sw t6, 1992(ra)
	-[0x800015b4]:sw t5, 2000(ra)
Current Store : [0x800015b4] : sw t5, 2000(ra) -- Store: [0x80007788]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xe394ab3b08c6b and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015ec]:fmul.d t5, t3, s10, dyn
	-[0x800015f0]:csrrs a6, fcsr, zero
	-[0x800015f4]:sw t5, 2016(ra)
	-[0x800015f8]:sw t6, 2024(ra)
Current Store : [0x800015f8] : sw t6, 2024(ra) -- Store: [0x800077a0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xe394ab3b08c6b and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015ec]:fmul.d t5, t3, s10, dyn
	-[0x800015f0]:csrrs a6, fcsr, zero
	-[0x800015f4]:sw t5, 2016(ra)
	-[0x800015f8]:sw t6, 2024(ra)
	-[0x800015fc]:sw t5, 2032(ra)
Current Store : [0x800015fc] : sw t5, 2032(ra) -- Store: [0x800077a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xe394ab3b08c6b and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001634]:fmul.d t5, t3, s10, dyn
	-[0x80001638]:csrrs a6, fcsr, zero
	-[0x8000163c]:addi ra, ra, 2040
	-[0x80001640]:sw t5, 8(ra)
	-[0x80001644]:sw t6, 16(ra)
Current Store : [0x80001644] : sw t6, 16(ra) -- Store: [0x800077c0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xe394ab3b08c6b and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001634]:fmul.d t5, t3, s10, dyn
	-[0x80001638]:csrrs a6, fcsr, zero
	-[0x8000163c]:addi ra, ra, 2040
	-[0x80001640]:sw t5, 8(ra)
	-[0x80001644]:sw t6, 16(ra)
	-[0x80001648]:sw t5, 24(ra)
Current Store : [0x80001648] : sw t5, 24(ra) -- Store: [0x800077c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x4cf244963827f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001680]:fmul.d t5, t3, s10, dyn
	-[0x80001684]:csrrs a6, fcsr, zero
	-[0x80001688]:sw t5, 40(ra)
	-[0x8000168c]:sw t6, 48(ra)
Current Store : [0x8000168c] : sw t6, 48(ra) -- Store: [0x800077e0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x4cf244963827f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001680]:fmul.d t5, t3, s10, dyn
	-[0x80001684]:csrrs a6, fcsr, zero
	-[0x80001688]:sw t5, 40(ra)
	-[0x8000168c]:sw t6, 48(ra)
	-[0x80001690]:sw t5, 56(ra)
Current Store : [0x80001690] : sw t5, 56(ra) -- Store: [0x800077e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x4cf244963827f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016c8]:fmul.d t5, t3, s10, dyn
	-[0x800016cc]:csrrs a6, fcsr, zero
	-[0x800016d0]:sw t5, 72(ra)
	-[0x800016d4]:sw t6, 80(ra)
Current Store : [0x800016d4] : sw t6, 80(ra) -- Store: [0x80007800]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x4cf244963827f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016c8]:fmul.d t5, t3, s10, dyn
	-[0x800016cc]:csrrs a6, fcsr, zero
	-[0x800016d0]:sw t5, 72(ra)
	-[0x800016d4]:sw t6, 80(ra)
	-[0x800016d8]:sw t5, 88(ra)
Current Store : [0x800016d8] : sw t5, 88(ra) -- Store: [0x80007808]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x4cf244963827f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001710]:fmul.d t5, t3, s10, dyn
	-[0x80001714]:csrrs a6, fcsr, zero
	-[0x80001718]:sw t5, 104(ra)
	-[0x8000171c]:sw t6, 112(ra)
Current Store : [0x8000171c] : sw t6, 112(ra) -- Store: [0x80007820]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x4cf244963827f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001710]:fmul.d t5, t3, s10, dyn
	-[0x80001714]:csrrs a6, fcsr, zero
	-[0x80001718]:sw t5, 104(ra)
	-[0x8000171c]:sw t6, 112(ra)
	-[0x80001720]:sw t5, 120(ra)
Current Store : [0x80001720] : sw t5, 120(ra) -- Store: [0x80007828]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x4cf244963827f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001758]:fmul.d t5, t3, s10, dyn
	-[0x8000175c]:csrrs a6, fcsr, zero
	-[0x80001760]:sw t5, 136(ra)
	-[0x80001764]:sw t6, 144(ra)
Current Store : [0x80001764] : sw t6, 144(ra) -- Store: [0x80007840]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x4cf244963827f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001758]:fmul.d t5, t3, s10, dyn
	-[0x8000175c]:csrrs a6, fcsr, zero
	-[0x80001760]:sw t5, 136(ra)
	-[0x80001764]:sw t6, 144(ra)
	-[0x80001768]:sw t5, 152(ra)
Current Store : [0x80001768] : sw t5, 152(ra) -- Store: [0x80007848]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x4cf244963827f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017a0]:fmul.d t5, t3, s10, dyn
	-[0x800017a4]:csrrs a6, fcsr, zero
	-[0x800017a8]:sw t5, 168(ra)
	-[0x800017ac]:sw t6, 176(ra)
Current Store : [0x800017ac] : sw t6, 176(ra) -- Store: [0x80007860]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x4cf244963827f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017a0]:fmul.d t5, t3, s10, dyn
	-[0x800017a4]:csrrs a6, fcsr, zero
	-[0x800017a8]:sw t5, 168(ra)
	-[0x800017ac]:sw t6, 176(ra)
	-[0x800017b0]:sw t5, 184(ra)
Current Store : [0x800017b0] : sw t5, 184(ra) -- Store: [0x80007868]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x053c0f57052f0 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017e8]:fmul.d t5, t3, s10, dyn
	-[0x800017ec]:csrrs a6, fcsr, zero
	-[0x800017f0]:sw t5, 200(ra)
	-[0x800017f4]:sw t6, 208(ra)
Current Store : [0x800017f4] : sw t6, 208(ra) -- Store: [0x80007880]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x053c0f57052f0 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017e8]:fmul.d t5, t3, s10, dyn
	-[0x800017ec]:csrrs a6, fcsr, zero
	-[0x800017f0]:sw t5, 200(ra)
	-[0x800017f4]:sw t6, 208(ra)
	-[0x800017f8]:sw t5, 216(ra)
Current Store : [0x800017f8] : sw t5, 216(ra) -- Store: [0x80007888]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x053c0f57052f0 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001830]:fmul.d t5, t3, s10, dyn
	-[0x80001834]:csrrs a6, fcsr, zero
	-[0x80001838]:sw t5, 232(ra)
	-[0x8000183c]:sw t6, 240(ra)
Current Store : [0x8000183c] : sw t6, 240(ra) -- Store: [0x800078a0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x053c0f57052f0 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001830]:fmul.d t5, t3, s10, dyn
	-[0x80001834]:csrrs a6, fcsr, zero
	-[0x80001838]:sw t5, 232(ra)
	-[0x8000183c]:sw t6, 240(ra)
	-[0x80001840]:sw t5, 248(ra)
Current Store : [0x80001840] : sw t5, 248(ra) -- Store: [0x800078a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x053c0f57052f0 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001878]:fmul.d t5, t3, s10, dyn
	-[0x8000187c]:csrrs a6, fcsr, zero
	-[0x80001880]:sw t5, 264(ra)
	-[0x80001884]:sw t6, 272(ra)
Current Store : [0x80001884] : sw t6, 272(ra) -- Store: [0x800078c0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x053c0f57052f0 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001878]:fmul.d t5, t3, s10, dyn
	-[0x8000187c]:csrrs a6, fcsr, zero
	-[0x80001880]:sw t5, 264(ra)
	-[0x80001884]:sw t6, 272(ra)
	-[0x80001888]:sw t5, 280(ra)
Current Store : [0x80001888] : sw t5, 280(ra) -- Store: [0x800078c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x053c0f57052f0 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018c0]:fmul.d t5, t3, s10, dyn
	-[0x800018c4]:csrrs a6, fcsr, zero
	-[0x800018c8]:sw t5, 296(ra)
	-[0x800018cc]:sw t6, 304(ra)
Current Store : [0x800018cc] : sw t6, 304(ra) -- Store: [0x800078e0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x053c0f57052f0 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018c0]:fmul.d t5, t3, s10, dyn
	-[0x800018c4]:csrrs a6, fcsr, zero
	-[0x800018c8]:sw t5, 296(ra)
	-[0x800018cc]:sw t6, 304(ra)
	-[0x800018d0]:sw t5, 312(ra)
Current Store : [0x800018d0] : sw t5, 312(ra) -- Store: [0x800078e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x053c0f57052f0 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001908]:fmul.d t5, t3, s10, dyn
	-[0x8000190c]:csrrs a6, fcsr, zero
	-[0x80001910]:sw t5, 328(ra)
	-[0x80001914]:sw t6, 336(ra)
Current Store : [0x80001914] : sw t6, 336(ra) -- Store: [0x80007900]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x053c0f57052f0 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001908]:fmul.d t5, t3, s10, dyn
	-[0x8000190c]:csrrs a6, fcsr, zero
	-[0x80001910]:sw t5, 328(ra)
	-[0x80001914]:sw t6, 336(ra)
	-[0x80001918]:sw t5, 344(ra)
Current Store : [0x80001918] : sw t5, 344(ra) -- Store: [0x80007908]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x59556723d53e2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001950]:fmul.d t5, t3, s10, dyn
	-[0x80001954]:csrrs a6, fcsr, zero
	-[0x80001958]:sw t5, 360(ra)
	-[0x8000195c]:sw t6, 368(ra)
Current Store : [0x8000195c] : sw t6, 368(ra) -- Store: [0x80007920]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x59556723d53e2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001950]:fmul.d t5, t3, s10, dyn
	-[0x80001954]:csrrs a6, fcsr, zero
	-[0x80001958]:sw t5, 360(ra)
	-[0x8000195c]:sw t6, 368(ra)
	-[0x80001960]:sw t5, 376(ra)
Current Store : [0x80001960] : sw t5, 376(ra) -- Store: [0x80007928]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x59556723d53e2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001998]:fmul.d t5, t3, s10, dyn
	-[0x8000199c]:csrrs a6, fcsr, zero
	-[0x800019a0]:sw t5, 392(ra)
	-[0x800019a4]:sw t6, 400(ra)
Current Store : [0x800019a4] : sw t6, 400(ra) -- Store: [0x80007940]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x59556723d53e2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001998]:fmul.d t5, t3, s10, dyn
	-[0x8000199c]:csrrs a6, fcsr, zero
	-[0x800019a0]:sw t5, 392(ra)
	-[0x800019a4]:sw t6, 400(ra)
	-[0x800019a8]:sw t5, 408(ra)
Current Store : [0x800019a8] : sw t5, 408(ra) -- Store: [0x80007948]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x59556723d53e2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800019e0]:fmul.d t5, t3, s10, dyn
	-[0x800019e4]:csrrs a6, fcsr, zero
	-[0x800019e8]:sw t5, 424(ra)
	-[0x800019ec]:sw t6, 432(ra)
Current Store : [0x800019ec] : sw t6, 432(ra) -- Store: [0x80007960]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x59556723d53e2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800019e0]:fmul.d t5, t3, s10, dyn
	-[0x800019e4]:csrrs a6, fcsr, zero
	-[0x800019e8]:sw t5, 424(ra)
	-[0x800019ec]:sw t6, 432(ra)
	-[0x800019f0]:sw t5, 440(ra)
Current Store : [0x800019f0] : sw t5, 440(ra) -- Store: [0x80007968]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x59556723d53e2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a28]:fmul.d t5, t3, s10, dyn
	-[0x80001a2c]:csrrs a6, fcsr, zero
	-[0x80001a30]:sw t5, 456(ra)
	-[0x80001a34]:sw t6, 464(ra)
Current Store : [0x80001a34] : sw t6, 464(ra) -- Store: [0x80007980]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x59556723d53e2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a28]:fmul.d t5, t3, s10, dyn
	-[0x80001a2c]:csrrs a6, fcsr, zero
	-[0x80001a30]:sw t5, 456(ra)
	-[0x80001a34]:sw t6, 464(ra)
	-[0x80001a38]:sw t5, 472(ra)
Current Store : [0x80001a38] : sw t5, 472(ra) -- Store: [0x80007988]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x59556723d53e2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a70]:fmul.d t5, t3, s10, dyn
	-[0x80001a74]:csrrs a6, fcsr, zero
	-[0x80001a78]:sw t5, 488(ra)
	-[0x80001a7c]:sw t6, 496(ra)
Current Store : [0x80001a7c] : sw t6, 496(ra) -- Store: [0x800079a0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x59556723d53e2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a70]:fmul.d t5, t3, s10, dyn
	-[0x80001a74]:csrrs a6, fcsr, zero
	-[0x80001a78]:sw t5, 488(ra)
	-[0x80001a7c]:sw t6, 496(ra)
	-[0x80001a80]:sw t5, 504(ra)
Current Store : [0x80001a80] : sw t5, 504(ra) -- Store: [0x800079a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc982355c85538 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ab8]:fmul.d t5, t3, s10, dyn
	-[0x80001abc]:csrrs a6, fcsr, zero
	-[0x80001ac0]:sw t5, 520(ra)
	-[0x80001ac4]:sw t6, 528(ra)
Current Store : [0x80001ac4] : sw t6, 528(ra) -- Store: [0x800079c0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc982355c85538 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ab8]:fmul.d t5, t3, s10, dyn
	-[0x80001abc]:csrrs a6, fcsr, zero
	-[0x80001ac0]:sw t5, 520(ra)
	-[0x80001ac4]:sw t6, 528(ra)
	-[0x80001ac8]:sw t5, 536(ra)
Current Store : [0x80001ac8] : sw t5, 536(ra) -- Store: [0x800079c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc982355c85538 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b00]:fmul.d t5, t3, s10, dyn
	-[0x80001b04]:csrrs a6, fcsr, zero
	-[0x80001b08]:sw t5, 552(ra)
	-[0x80001b0c]:sw t6, 560(ra)
Current Store : [0x80001b0c] : sw t6, 560(ra) -- Store: [0x800079e0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc982355c85538 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b00]:fmul.d t5, t3, s10, dyn
	-[0x80001b04]:csrrs a6, fcsr, zero
	-[0x80001b08]:sw t5, 552(ra)
	-[0x80001b0c]:sw t6, 560(ra)
	-[0x80001b10]:sw t5, 568(ra)
Current Store : [0x80001b10] : sw t5, 568(ra) -- Store: [0x800079e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc982355c85538 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b48]:fmul.d t5, t3, s10, dyn
	-[0x80001b4c]:csrrs a6, fcsr, zero
	-[0x80001b50]:sw t5, 584(ra)
	-[0x80001b54]:sw t6, 592(ra)
Current Store : [0x80001b54] : sw t6, 592(ra) -- Store: [0x80007a00]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc982355c85538 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b48]:fmul.d t5, t3, s10, dyn
	-[0x80001b4c]:csrrs a6, fcsr, zero
	-[0x80001b50]:sw t5, 584(ra)
	-[0x80001b54]:sw t6, 592(ra)
	-[0x80001b58]:sw t5, 600(ra)
Current Store : [0x80001b58] : sw t5, 600(ra) -- Store: [0x80007a08]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc982355c85538 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b90]:fmul.d t5, t3, s10, dyn
	-[0x80001b94]:csrrs a6, fcsr, zero
	-[0x80001b98]:sw t5, 616(ra)
	-[0x80001b9c]:sw t6, 624(ra)
Current Store : [0x80001b9c] : sw t6, 624(ra) -- Store: [0x80007a20]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc982355c85538 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b90]:fmul.d t5, t3, s10, dyn
	-[0x80001b94]:csrrs a6, fcsr, zero
	-[0x80001b98]:sw t5, 616(ra)
	-[0x80001b9c]:sw t6, 624(ra)
	-[0x80001ba0]:sw t5, 632(ra)
Current Store : [0x80001ba0] : sw t5, 632(ra) -- Store: [0x80007a28]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc982355c85538 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001bd8]:fmul.d t5, t3, s10, dyn
	-[0x80001bdc]:csrrs a6, fcsr, zero
	-[0x80001be0]:sw t5, 648(ra)
	-[0x80001be4]:sw t6, 656(ra)
Current Store : [0x80001be4] : sw t6, 656(ra) -- Store: [0x80007a40]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc982355c85538 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001bd8]:fmul.d t5, t3, s10, dyn
	-[0x80001bdc]:csrrs a6, fcsr, zero
	-[0x80001be0]:sw t5, 648(ra)
	-[0x80001be4]:sw t6, 656(ra)
	-[0x80001be8]:sw t5, 664(ra)
Current Store : [0x80001be8] : sw t5, 664(ra) -- Store: [0x80007a48]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x78037fa19f977 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c20]:fmul.d t5, t3, s10, dyn
	-[0x80001c24]:csrrs a6, fcsr, zero
	-[0x80001c28]:sw t5, 680(ra)
	-[0x80001c2c]:sw t6, 688(ra)
Current Store : [0x80001c2c] : sw t6, 688(ra) -- Store: [0x80007a60]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x78037fa19f977 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c20]:fmul.d t5, t3, s10, dyn
	-[0x80001c24]:csrrs a6, fcsr, zero
	-[0x80001c28]:sw t5, 680(ra)
	-[0x80001c2c]:sw t6, 688(ra)
	-[0x80001c30]:sw t5, 696(ra)
Current Store : [0x80001c30] : sw t5, 696(ra) -- Store: [0x80007a68]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x78037fa19f977 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c68]:fmul.d t5, t3, s10, dyn
	-[0x80001c6c]:csrrs a6, fcsr, zero
	-[0x80001c70]:sw t5, 712(ra)
	-[0x80001c74]:sw t6, 720(ra)
Current Store : [0x80001c74] : sw t6, 720(ra) -- Store: [0x80007a80]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x78037fa19f977 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c68]:fmul.d t5, t3, s10, dyn
	-[0x80001c6c]:csrrs a6, fcsr, zero
	-[0x80001c70]:sw t5, 712(ra)
	-[0x80001c74]:sw t6, 720(ra)
	-[0x80001c78]:sw t5, 728(ra)
Current Store : [0x80001c78] : sw t5, 728(ra) -- Store: [0x80007a88]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x78037fa19f977 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001cb0]:fmul.d t5, t3, s10, dyn
	-[0x80001cb4]:csrrs a6, fcsr, zero
	-[0x80001cb8]:sw t5, 744(ra)
	-[0x80001cbc]:sw t6, 752(ra)
Current Store : [0x80001cbc] : sw t6, 752(ra) -- Store: [0x80007aa0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x78037fa19f977 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001cb0]:fmul.d t5, t3, s10, dyn
	-[0x80001cb4]:csrrs a6, fcsr, zero
	-[0x80001cb8]:sw t5, 744(ra)
	-[0x80001cbc]:sw t6, 752(ra)
	-[0x80001cc0]:sw t5, 760(ra)
Current Store : [0x80001cc0] : sw t5, 760(ra) -- Store: [0x80007aa8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x78037fa19f977 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001cf8]:fmul.d t5, t3, s10, dyn
	-[0x80001cfc]:csrrs a6, fcsr, zero
	-[0x80001d00]:sw t5, 776(ra)
	-[0x80001d04]:sw t6, 784(ra)
Current Store : [0x80001d04] : sw t6, 784(ra) -- Store: [0x80007ac0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x78037fa19f977 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001cf8]:fmul.d t5, t3, s10, dyn
	-[0x80001cfc]:csrrs a6, fcsr, zero
	-[0x80001d00]:sw t5, 776(ra)
	-[0x80001d04]:sw t6, 784(ra)
	-[0x80001d08]:sw t5, 792(ra)
Current Store : [0x80001d08] : sw t5, 792(ra) -- Store: [0x80007ac8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x78037fa19f977 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d40]:fmul.d t5, t3, s10, dyn
	-[0x80001d44]:csrrs a6, fcsr, zero
	-[0x80001d48]:sw t5, 808(ra)
	-[0x80001d4c]:sw t6, 816(ra)
Current Store : [0x80001d4c] : sw t6, 816(ra) -- Store: [0x80007ae0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x78037fa19f977 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d40]:fmul.d t5, t3, s10, dyn
	-[0x80001d44]:csrrs a6, fcsr, zero
	-[0x80001d48]:sw t5, 808(ra)
	-[0x80001d4c]:sw t6, 816(ra)
	-[0x80001d50]:sw t5, 824(ra)
Current Store : [0x80001d50] : sw t5, 824(ra) -- Store: [0x80007ae8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8698ed174ff65 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d88]:fmul.d t5, t3, s10, dyn
	-[0x80001d8c]:csrrs a6, fcsr, zero
	-[0x80001d90]:sw t5, 840(ra)
	-[0x80001d94]:sw t6, 848(ra)
Current Store : [0x80001d94] : sw t6, 848(ra) -- Store: [0x80007b00]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8698ed174ff65 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d88]:fmul.d t5, t3, s10, dyn
	-[0x80001d8c]:csrrs a6, fcsr, zero
	-[0x80001d90]:sw t5, 840(ra)
	-[0x80001d94]:sw t6, 848(ra)
	-[0x80001d98]:sw t5, 856(ra)
Current Store : [0x80001d98] : sw t5, 856(ra) -- Store: [0x80007b08]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8698ed174ff65 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001dd0]:fmul.d t5, t3, s10, dyn
	-[0x80001dd4]:csrrs a6, fcsr, zero
	-[0x80001dd8]:sw t5, 872(ra)
	-[0x80001ddc]:sw t6, 880(ra)
Current Store : [0x80001ddc] : sw t6, 880(ra) -- Store: [0x80007b20]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8698ed174ff65 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001dd0]:fmul.d t5, t3, s10, dyn
	-[0x80001dd4]:csrrs a6, fcsr, zero
	-[0x80001dd8]:sw t5, 872(ra)
	-[0x80001ddc]:sw t6, 880(ra)
	-[0x80001de0]:sw t5, 888(ra)
Current Store : [0x80001de0] : sw t5, 888(ra) -- Store: [0x80007b28]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8698ed174ff65 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e18]:fmul.d t5, t3, s10, dyn
	-[0x80001e1c]:csrrs a6, fcsr, zero
	-[0x80001e20]:sw t5, 904(ra)
	-[0x80001e24]:sw t6, 912(ra)
Current Store : [0x80001e24] : sw t6, 912(ra) -- Store: [0x80007b40]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8698ed174ff65 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e18]:fmul.d t5, t3, s10, dyn
	-[0x80001e1c]:csrrs a6, fcsr, zero
	-[0x80001e20]:sw t5, 904(ra)
	-[0x80001e24]:sw t6, 912(ra)
	-[0x80001e28]:sw t5, 920(ra)
Current Store : [0x80001e28] : sw t5, 920(ra) -- Store: [0x80007b48]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8698ed174ff65 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e60]:fmul.d t5, t3, s10, dyn
	-[0x80001e64]:csrrs a6, fcsr, zero
	-[0x80001e68]:sw t5, 936(ra)
	-[0x80001e6c]:sw t6, 944(ra)
Current Store : [0x80001e6c] : sw t6, 944(ra) -- Store: [0x80007b60]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8698ed174ff65 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e60]:fmul.d t5, t3, s10, dyn
	-[0x80001e64]:csrrs a6, fcsr, zero
	-[0x80001e68]:sw t5, 936(ra)
	-[0x80001e6c]:sw t6, 944(ra)
	-[0x80001e70]:sw t5, 952(ra)
Current Store : [0x80001e70] : sw t5, 952(ra) -- Store: [0x80007b68]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8698ed174ff65 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ea8]:fmul.d t5, t3, s10, dyn
	-[0x80001eac]:csrrs a6, fcsr, zero
	-[0x80001eb0]:sw t5, 968(ra)
	-[0x80001eb4]:sw t6, 976(ra)
Current Store : [0x80001eb4] : sw t6, 976(ra) -- Store: [0x80007b80]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8698ed174ff65 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ea8]:fmul.d t5, t3, s10, dyn
	-[0x80001eac]:csrrs a6, fcsr, zero
	-[0x80001eb0]:sw t5, 968(ra)
	-[0x80001eb4]:sw t6, 976(ra)
	-[0x80001eb8]:sw t5, 984(ra)
Current Store : [0x80001eb8] : sw t5, 984(ra) -- Store: [0x80007b88]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x6a47222e524ad and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000008000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ef0]:fmul.d t5, t3, s10, dyn
	-[0x80001ef4]:csrrs a6, fcsr, zero
	-[0x80001ef8]:sw t5, 1000(ra)
	-[0x80001efc]:sw t6, 1008(ra)
Current Store : [0x80001efc] : sw t6, 1008(ra) -- Store: [0x80007ba0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x6a47222e524ad and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000008000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ef0]:fmul.d t5, t3, s10, dyn
	-[0x80001ef4]:csrrs a6, fcsr, zero
	-[0x80001ef8]:sw t5, 1000(ra)
	-[0x80001efc]:sw t6, 1008(ra)
	-[0x80001f00]:sw t5, 1016(ra)
Current Store : [0x80001f00] : sw t5, 1016(ra) -- Store: [0x80007ba8]:0x22E524AD




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x6a47222e524ad and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000008000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f38]:fmul.d t5, t3, s10, dyn
	-[0x80001f3c]:csrrs a6, fcsr, zero
	-[0x80001f40]:sw t5, 1032(ra)
	-[0x80001f44]:sw t6, 1040(ra)
Current Store : [0x80001f44] : sw t6, 1040(ra) -- Store: [0x80007bc0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x6a47222e524ad and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000008000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f38]:fmul.d t5, t3, s10, dyn
	-[0x80001f3c]:csrrs a6, fcsr, zero
	-[0x80001f40]:sw t5, 1032(ra)
	-[0x80001f44]:sw t6, 1040(ra)
	-[0x80001f48]:sw t5, 1048(ra)
Current Store : [0x80001f48] : sw t5, 1048(ra) -- Store: [0x80007bc8]:0x22E524AD




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x6a47222e524ad and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000008000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f80]:fmul.d t5, t3, s10, dyn
	-[0x80001f84]:csrrs a6, fcsr, zero
	-[0x80001f88]:sw t5, 1064(ra)
	-[0x80001f8c]:sw t6, 1072(ra)
Current Store : [0x80001f8c] : sw t6, 1072(ra) -- Store: [0x80007be0]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x6a47222e524ad and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000008000 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f80]:fmul.d t5, t3, s10, dyn
	-[0x80001f84]:csrrs a6, fcsr, zero
	-[0x80001f88]:sw t5, 1064(ra)
	-[0x80001f8c]:sw t6, 1072(ra)
	-[0x80001f90]:sw t5, 1080(ra)
Current Store : [0x80001f90] : sw t5, 1080(ra) -- Store: [0x80007be8]:0x22E524AD




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x6a47222e524ad and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000008000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001fc8]:fmul.d t5, t3, s10, dyn
	-[0x80001fcc]:csrrs a6, fcsr, zero
	-[0x80001fd0]:sw t5, 1096(ra)
	-[0x80001fd4]:sw t6, 1104(ra)
Current Store : [0x80001fd4] : sw t6, 1104(ra) -- Store: [0x80007c00]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x6a47222e524ad and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000008000 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001fc8]:fmul.d t5, t3, s10, dyn
	-[0x80001fcc]:csrrs a6, fcsr, zero
	-[0x80001fd0]:sw t5, 1096(ra)
	-[0x80001fd4]:sw t6, 1104(ra)
	-[0x80001fd8]:sw t5, 1112(ra)
Current Store : [0x80001fd8] : sw t5, 1112(ra) -- Store: [0x80007c08]:0x22E524AD




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x6a47222e524ad and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000008000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002010]:fmul.d t5, t3, s10, dyn
	-[0x80002014]:csrrs a6, fcsr, zero
	-[0x80002018]:sw t5, 1128(ra)
	-[0x8000201c]:sw t6, 1136(ra)
Current Store : [0x8000201c] : sw t6, 1136(ra) -- Store: [0x80007c20]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x6a47222e524ad and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000008000 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002010]:fmul.d t5, t3, s10, dyn
	-[0x80002014]:csrrs a6, fcsr, zero
	-[0x80002018]:sw t5, 1128(ra)
	-[0x8000201c]:sw t6, 1136(ra)
	-[0x80002020]:sw t5, 1144(ra)
Current Store : [0x80002020] : sw t5, 1144(ra) -- Store: [0x80007c28]:0x22E524AD




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xec7e479c877a7 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000008000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002058]:fmul.d t5, t3, s10, dyn
	-[0x8000205c]:csrrs a6, fcsr, zero
	-[0x80002060]:sw t5, 1160(ra)
	-[0x80002064]:sw t6, 1168(ra)
Current Store : [0x80002064] : sw t6, 1168(ra) -- Store: [0x80007c40]:0x7FEE97D5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xec7e479c877a7 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000008000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002058]:fmul.d t5, t3, s10, dyn
	-[0x8000205c]:csrrs a6, fcsr, zero
	-[0x80002060]:sw t5, 1160(ra)
	-[0x80002064]:sw t6, 1168(ra)
	-[0x80002068]:sw t5, 1176(ra)
Current Store : [0x80002068] : sw t5, 1176(ra) -- Store: [0x80007c48]:0x79C877A7




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xec7e479c877a7 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000008000 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800020a0]:fmul.d t5, t3, s10, dyn
	-[0x800020a4]:csrrs a6, fcsr, zero
	-[0x800020a8]:sw t5, 1192(ra)
	-[0x800020ac]:sw t6, 1200(ra)
Current Store : [0x800020ac] : sw t6, 1200(ra) -- Store: [0x80007c60]:0x7FEE97D5





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                          signature                           |                                                                                                                                    coverpoints                                                                                                                                    |                                                                                                                    code                                                                                                                     |
|---:|--------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80006f18]<br>0x00000000<br> [0x80006f30]<br>0x00000000<br> |- mnemonic : fmul.d<br> - rs1 : x28<br> - rs2 : x26<br> - rd : x30<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x80000144]:fmul.d t5, t3, s10, dyn<br> [0x80000148]:csrrs tp, fcsr, zero<br> [0x8000014c]:sw t5, 0(ra)<br> [0x80000150]:sw t6, 8(ra)<br> [0x80000154]:sw t5, 16(ra)<br> [0x80000158]:sw tp, 24(ra)<br>                                     |
|   2|[0x80006f38]<br>0x00000000<br> [0x80006f50]<br>0x00000020<br> |- rs1 : x26<br> - rs2 : x30<br> - rd : x26<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                               |[0x8000018c]:fmul.d s10, s10, t5, dyn<br> [0x80000190]:csrrs tp, fcsr, zero<br> [0x80000194]:sw s10, 32(ra)<br> [0x80000198]:sw s11, 40(ra)<br> [0x8000019c]:sw s10, 48(ra)<br> [0x800001a0]:sw tp, 56(ra)<br>                               |
|   3|[0x80006f58]<br>0xFFFFFFFF<br> [0x80006f70]<br>0x00000045<br> |- rs1 : x24<br> - rs2 : x24<br> - rd : x24<br> - rs1 == rs2 == rd<br>                                                                                                                                                                                                              |[0x800001dc]:fmul.d s8, s8, s8, dyn<br> [0x800001e0]:csrrs tp, fcsr, zero<br> [0x800001e4]:sw s8, 64(ra)<br> [0x800001e8]:sw s9, 72(ra)<br> [0x800001ec]:sw s8, 80(ra)<br> [0x800001f0]:sw tp, 88(ra)<br>                                    |
|   4|[0x80006f78]<br>0x00000000<br> [0x80006f90]<br>0x00000065<br> |- rs1 : x22<br> - rs2 : x22<br> - rd : x28<br> - rs1 == rs2 != rd<br>                                                                                                                                                                                                              |[0x8000022c]:fmul.d t3, s6, s6, dyn<br> [0x80000230]:csrrs tp, fcsr, zero<br> [0x80000234]:sw t3, 96(ra)<br> [0x80000238]:sw t4, 104(ra)<br> [0x8000023c]:sw t3, 112(ra)<br> [0x80000240]:sw tp, 120(ra)<br>                                 |
|   5|[0x80006f98]<br>0x00000000<br> [0x80006fb0]<br>0x00000080<br> |- rs1 : x30<br> - rs2 : x20<br> - rd : x20<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                               |[0x80000274]:fmul.d s4, t5, s4, dyn<br> [0x80000278]:csrrs tp, fcsr, zero<br> [0x8000027c]:sw s4, 128(ra)<br> [0x80000280]:sw s5, 136(ra)<br> [0x80000284]:sw s4, 144(ra)<br> [0x80000288]:sw tp, 152(ra)<br>                                |
|   6|[0x80006fb8]<br>0x00000000<br> [0x80006fd0]<br>0x00000000<br> |- rs1 : x20<br> - rs2 : x28<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x7fa and fm1 == 0xcf44d05dc866f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                       |[0x800002bc]:fmul.d s6, s4, t3, dyn<br> [0x800002c0]:csrrs tp, fcsr, zero<br> [0x800002c4]:sw s6, 160(ra)<br> [0x800002c8]:sw s7, 168(ra)<br> [0x800002cc]:sw s6, 176(ra)<br> [0x800002d0]:sw tp, 184(ra)<br>                                |
|   7|[0x80006fd8]<br>0x00000000<br> [0x80006ff0]<br>0x00000020<br> |- rs1 : x16<br> - rs2 : x14<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x7fa and fm1 == 0xcf44d05dc866f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                      |[0x80000304]:fmul.d s2, a6, a4, dyn<br> [0x80000308]:csrrs tp, fcsr, zero<br> [0x8000030c]:sw s2, 192(ra)<br> [0x80000310]:sw s3, 200(ra)<br> [0x80000314]:sw s2, 208(ra)<br> [0x80000318]:sw tp, 216(ra)<br>                                |
|   8|[0x80006ff8]<br>0x00000000<br> [0x80007010]<br>0x00000040<br> |- rs1 : x14<br> - rs2 : x18<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x7fa and fm1 == 0xcf44d05dc866f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                      |[0x8000034c]:fmul.d a6, a4, s2, dyn<br> [0x80000350]:csrrs tp, fcsr, zero<br> [0x80000354]:sw a6, 224(ra)<br> [0x80000358]:sw a7, 232(ra)<br> [0x8000035c]:sw a6, 240(ra)<br> [0x80000360]:sw tp, 248(ra)<br>                                |
|   9|[0x80007018]<br>0x00000000<br> [0x80007030]<br>0x00000060<br> |- rs1 : x18<br> - rs2 : x16<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x7fa and fm1 == 0xcf44d05dc866f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                      |[0x80000394]:fmul.d a4, s2, a6, dyn<br> [0x80000398]:csrrs tp, fcsr, zero<br> [0x8000039c]:sw a4, 256(ra)<br> [0x800003a0]:sw a5, 264(ra)<br> [0x800003a4]:sw a4, 272(ra)<br> [0x800003a8]:sw tp, 280(ra)<br>                                |
|  10|[0x80007038]<br>0x00000000<br> [0x80007050]<br>0x00000080<br> |- rs1 : x10<br> - rs2 : x8<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x7fa and fm1 == 0xcf44d05dc866f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                       |[0x800003dc]:fmul.d a2, a0, fp, dyn<br> [0x800003e0]:csrrs tp, fcsr, zero<br> [0x800003e4]:sw a2, 288(ra)<br> [0x800003e8]:sw a3, 296(ra)<br> [0x800003ec]:sw a2, 304(ra)<br> [0x800003f0]:sw tp, 312(ra)<br>                                |
|  11|[0x80006fb8]<br>0x00000000<br> [0x80006fd0]<br>0x00000000<br> |- rs1 : x8<br> - rs2 : x12<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xabc6824ad2440 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                        |[0x80000434]:fmul.d a0, fp, a2, dyn<br> [0x80000438]:csrrs a6, fcsr, zero<br> [0x8000043c]:sw a0, 0(ra)<br> [0x80000440]:sw a1, 8(ra)<br> [0x80000444]:sw a0, 16(ra)<br> [0x80000448]:sw a6, 24(ra)<br>                                      |
|  12|[0x80006fd8]<br>0x00000000<br> [0x80006ff0]<br>0x00000020<br> |- rs1 : x12<br> - rs2 : x10<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xabc6824ad2440 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                       |[0x8000047c]:fmul.d fp, a2, a0, dyn<br> [0x80000480]:csrrs a6, fcsr, zero<br> [0x80000484]:sw fp, 32(ra)<br> [0x80000488]:sw s1, 40(ra)<br> [0x8000048c]:sw fp, 48(ra)<br> [0x80000490]:sw a6, 56(ra)<br>                                    |
|  13|[0x80006ff8]<br>0x00000000<br> [0x80007010]<br>0x00000040<br> |- rs1 : x4<br> - rs2 : x2<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xabc6824ad2440 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                         |[0x800004c4]:fmul.d t1, tp, sp, dyn<br> [0x800004c8]:csrrs a6, fcsr, zero<br> [0x800004cc]:sw t1, 64(ra)<br> [0x800004d0]:sw t2, 72(ra)<br> [0x800004d4]:sw t1, 80(ra)<br> [0x800004d8]:sw a6, 88(ra)<br>                                    |
|  14|[0x80007018]<br>0x00000000<br> [0x80007030]<br>0x00000060<br> |- rs1 : x2<br> - rs2 : x6<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xabc6824ad2440 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                         |[0x8000050c]:fmul.d tp, sp, t1, dyn<br> [0x80000510]:csrrs a6, fcsr, zero<br> [0x80000514]:sw tp, 96(ra)<br> [0x80000518]:sw t0, 104(ra)<br> [0x8000051c]:sw tp, 112(ra)<br> [0x80000520]:sw a6, 120(ra)<br>                                 |
|  15|[0x80007038]<br>0x00000000<br> [0x80007050]<br>0x00000080<br> |- rs1 : x6<br> - rs2 : x4<br> - rd : x2<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xabc6824ad2440 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                         |[0x80000554]:fmul.d sp, t1, tp, dyn<br> [0x80000558]:csrrs a6, fcsr, zero<br> [0x8000055c]:sw sp, 128(ra)<br> [0x80000560]:sw gp, 136(ra)<br> [0x80000564]:sw sp, 144(ra)<br> [0x80000568]:sw a6, 152(ra)<br>                                |
|  16|[0x80007058]<br>0x00000000<br> [0x80007070]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x56e6e736a538e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000059c]:fmul.d t5, t3, s10, dyn<br> [0x800005a0]:csrrs a6, fcsr, zero<br> [0x800005a4]:sw t5, 160(ra)<br> [0x800005a8]:sw t6, 168(ra)<br> [0x800005ac]:sw t5, 176(ra)<br> [0x800005b0]:sw a6, 184(ra)<br>                               |
|  17|[0x80007078]<br>0x00000000<br> [0x80007090]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x56e6e736a538e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800005e4]:fmul.d t5, t3, s10, dyn<br> [0x800005e8]:csrrs a6, fcsr, zero<br> [0x800005ec]:sw t5, 192(ra)<br> [0x800005f0]:sw t6, 200(ra)<br> [0x800005f4]:sw t5, 208(ra)<br> [0x800005f8]:sw a6, 216(ra)<br>                               |
|  18|[0x80007098]<br>0x00000000<br> [0x800070b0]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x56e6e736a538e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000062c]:fmul.d t5, t3, s10, dyn<br> [0x80000630]:csrrs a6, fcsr, zero<br> [0x80000634]:sw t5, 224(ra)<br> [0x80000638]:sw t6, 232(ra)<br> [0x8000063c]:sw t5, 240(ra)<br> [0x80000640]:sw a6, 248(ra)<br>                               |
|  19|[0x800070b8]<br>0x00000000<br> [0x800070d0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x56e6e736a538e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000674]:fmul.d t5, t3, s10, dyn<br> [0x80000678]:csrrs a6, fcsr, zero<br> [0x8000067c]:sw t5, 256(ra)<br> [0x80000680]:sw t6, 264(ra)<br> [0x80000684]:sw t5, 272(ra)<br> [0x80000688]:sw a6, 280(ra)<br>                               |
|  20|[0x800070d8]<br>0x00000000<br> [0x800070f0]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x56e6e736a538e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800006bc]:fmul.d t5, t3, s10, dyn<br> [0x800006c0]:csrrs a6, fcsr, zero<br> [0x800006c4]:sw t5, 288(ra)<br> [0x800006c8]:sw t6, 296(ra)<br> [0x800006cc]:sw t5, 304(ra)<br> [0x800006d0]:sw a6, 312(ra)<br>                               |
|  21|[0x800070f8]<br>0x00000000<br> [0x80007110]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x363e504d94fe2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000704]:fmul.d t5, t3, s10, dyn<br> [0x80000708]:csrrs a6, fcsr, zero<br> [0x8000070c]:sw t5, 320(ra)<br> [0x80000710]:sw t6, 328(ra)<br> [0x80000714]:sw t5, 336(ra)<br> [0x80000718]:sw a6, 344(ra)<br>                               |
|  22|[0x80007118]<br>0x00000000<br> [0x80007130]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x363e504d94fe2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000074c]:fmul.d t5, t3, s10, dyn<br> [0x80000750]:csrrs a6, fcsr, zero<br> [0x80000754]:sw t5, 352(ra)<br> [0x80000758]:sw t6, 360(ra)<br> [0x8000075c]:sw t5, 368(ra)<br> [0x80000760]:sw a6, 376(ra)<br>                               |
|  23|[0x80007138]<br>0x00000000<br> [0x80007150]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x363e504d94fe2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000794]:fmul.d t5, t3, s10, dyn<br> [0x80000798]:csrrs a6, fcsr, zero<br> [0x8000079c]:sw t5, 384(ra)<br> [0x800007a0]:sw t6, 392(ra)<br> [0x800007a4]:sw t5, 400(ra)<br> [0x800007a8]:sw a6, 408(ra)<br>                               |
|  24|[0x80007158]<br>0x00000000<br> [0x80007170]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x363e504d94fe2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800007dc]:fmul.d t5, t3, s10, dyn<br> [0x800007e0]:csrrs a6, fcsr, zero<br> [0x800007e4]:sw t5, 416(ra)<br> [0x800007e8]:sw t6, 424(ra)<br> [0x800007ec]:sw t5, 432(ra)<br> [0x800007f0]:sw a6, 440(ra)<br>                               |
|  25|[0x80007178]<br>0x00000000<br> [0x80007190]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x363e504d94fe2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000824]:fmul.d t5, t3, s10, dyn<br> [0x80000828]:csrrs a6, fcsr, zero<br> [0x8000082c]:sw t5, 448(ra)<br> [0x80000830]:sw t6, 456(ra)<br> [0x80000834]:sw t5, 464(ra)<br> [0x80000838]:sw a6, 472(ra)<br>                               |
|  26|[0x80007198]<br>0x00000000<br> [0x800071b0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x299392ab99898 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000086c]:fmul.d t5, t3, s10, dyn<br> [0x80000870]:csrrs a6, fcsr, zero<br> [0x80000874]:sw t5, 480(ra)<br> [0x80000878]:sw t6, 488(ra)<br> [0x8000087c]:sw t5, 496(ra)<br> [0x80000880]:sw a6, 504(ra)<br>                               |
|  27|[0x800071b8]<br>0x00000000<br> [0x800071d0]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x299392ab99898 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800008b4]:fmul.d t5, t3, s10, dyn<br> [0x800008b8]:csrrs a6, fcsr, zero<br> [0x800008bc]:sw t5, 512(ra)<br> [0x800008c0]:sw t6, 520(ra)<br> [0x800008c4]:sw t5, 528(ra)<br> [0x800008c8]:sw a6, 536(ra)<br>                               |
|  28|[0x800071d8]<br>0x00000000<br> [0x800071f0]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x299392ab99898 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800008fc]:fmul.d t5, t3, s10, dyn<br> [0x80000900]:csrrs a6, fcsr, zero<br> [0x80000904]:sw t5, 544(ra)<br> [0x80000908]:sw t6, 552(ra)<br> [0x8000090c]:sw t5, 560(ra)<br> [0x80000910]:sw a6, 568(ra)<br>                               |
|  29|[0x800071f8]<br>0x00000000<br> [0x80007210]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x299392ab99898 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000944]:fmul.d t5, t3, s10, dyn<br> [0x80000948]:csrrs a6, fcsr, zero<br> [0x8000094c]:sw t5, 576(ra)<br> [0x80000950]:sw t6, 584(ra)<br> [0x80000954]:sw t5, 592(ra)<br> [0x80000958]:sw a6, 600(ra)<br>                               |
|  30|[0x80007218]<br>0x00000000<br> [0x80007230]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x299392ab99898 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000098c]:fmul.d t5, t3, s10, dyn<br> [0x80000990]:csrrs a6, fcsr, zero<br> [0x80000994]:sw t5, 608(ra)<br> [0x80000998]:sw t6, 616(ra)<br> [0x8000099c]:sw t5, 624(ra)<br> [0x800009a0]:sw a6, 632(ra)<br>                               |
|  31|[0x80007238]<br>0x00000000<br> [0x80007250]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xb9017651b96db and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800009d4]:fmul.d t5, t3, s10, dyn<br> [0x800009d8]:csrrs a6, fcsr, zero<br> [0x800009dc]:sw t5, 640(ra)<br> [0x800009e0]:sw t6, 648(ra)<br> [0x800009e4]:sw t5, 656(ra)<br> [0x800009e8]:sw a6, 664(ra)<br>                               |
|  32|[0x80007258]<br>0x00000000<br> [0x80007270]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xb9017651b96db and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000a1c]:fmul.d t5, t3, s10, dyn<br> [0x80000a20]:csrrs a6, fcsr, zero<br> [0x80000a24]:sw t5, 672(ra)<br> [0x80000a28]:sw t6, 680(ra)<br> [0x80000a2c]:sw t5, 688(ra)<br> [0x80000a30]:sw a6, 696(ra)<br>                               |
|  33|[0x80007278]<br>0x00000000<br> [0x80007290]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xb9017651b96db and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000a64]:fmul.d t5, t3, s10, dyn<br> [0x80000a68]:csrrs a6, fcsr, zero<br> [0x80000a6c]:sw t5, 704(ra)<br> [0x80000a70]:sw t6, 712(ra)<br> [0x80000a74]:sw t5, 720(ra)<br> [0x80000a78]:sw a6, 728(ra)<br>                               |
|  34|[0x80007298]<br>0x00000000<br> [0x800072b0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xb9017651b96db and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000aac]:fmul.d t5, t3, s10, dyn<br> [0x80000ab0]:csrrs a6, fcsr, zero<br> [0x80000ab4]:sw t5, 736(ra)<br> [0x80000ab8]:sw t6, 744(ra)<br> [0x80000abc]:sw t5, 752(ra)<br> [0x80000ac0]:sw a6, 760(ra)<br>                               |
|  35|[0x800072b8]<br>0x00000000<br> [0x800072d0]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xb9017651b96db and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000af4]:fmul.d t5, t3, s10, dyn<br> [0x80000af8]:csrrs a6, fcsr, zero<br> [0x80000afc]:sw t5, 768(ra)<br> [0x80000b00]:sw t6, 776(ra)<br> [0x80000b04]:sw t5, 784(ra)<br> [0x80000b08]:sw a6, 792(ra)<br>                               |
|  36|[0x800072d8]<br>0x00000000<br> [0x800072f0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x722ea3b70e3d3 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000b3c]:fmul.d t5, t3, s10, dyn<br> [0x80000b40]:csrrs a6, fcsr, zero<br> [0x80000b44]:sw t5, 800(ra)<br> [0x80000b48]:sw t6, 808(ra)<br> [0x80000b4c]:sw t5, 816(ra)<br> [0x80000b50]:sw a6, 824(ra)<br>                               |
|  37|[0x800072f8]<br>0x00000000<br> [0x80007310]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x722ea3b70e3d3 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000b84]:fmul.d t5, t3, s10, dyn<br> [0x80000b88]:csrrs a6, fcsr, zero<br> [0x80000b8c]:sw t5, 832(ra)<br> [0x80000b90]:sw t6, 840(ra)<br> [0x80000b94]:sw t5, 848(ra)<br> [0x80000b98]:sw a6, 856(ra)<br>                               |
|  38|[0x80007318]<br>0x00000000<br> [0x80007330]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x722ea3b70e3d3 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000bcc]:fmul.d t5, t3, s10, dyn<br> [0x80000bd0]:csrrs a6, fcsr, zero<br> [0x80000bd4]:sw t5, 864(ra)<br> [0x80000bd8]:sw t6, 872(ra)<br> [0x80000bdc]:sw t5, 880(ra)<br> [0x80000be0]:sw a6, 888(ra)<br>                               |
|  39|[0x80007338]<br>0x00000000<br> [0x80007350]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x722ea3b70e3d3 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000c14]:fmul.d t5, t3, s10, dyn<br> [0x80000c18]:csrrs a6, fcsr, zero<br> [0x80000c1c]:sw t5, 896(ra)<br> [0x80000c20]:sw t6, 904(ra)<br> [0x80000c24]:sw t5, 912(ra)<br> [0x80000c28]:sw a6, 920(ra)<br>                               |
|  40|[0x80007358]<br>0x00000000<br> [0x80007370]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x722ea3b70e3d3 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000c5c]:fmul.d t5, t3, s10, dyn<br> [0x80000c60]:csrrs a6, fcsr, zero<br> [0x80000c64]:sw t5, 928(ra)<br> [0x80000c68]:sw t6, 936(ra)<br> [0x80000c6c]:sw t5, 944(ra)<br> [0x80000c70]:sw a6, 952(ra)<br>                               |
|  41|[0x80007378]<br>0x00000000<br> [0x80007390]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe61729d7cfd5e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000ca4]:fmul.d t5, t3, s10, dyn<br> [0x80000ca8]:csrrs a6, fcsr, zero<br> [0x80000cac]:sw t5, 960(ra)<br> [0x80000cb0]:sw t6, 968(ra)<br> [0x80000cb4]:sw t5, 976(ra)<br> [0x80000cb8]:sw a6, 984(ra)<br>                               |
|  42|[0x80007398]<br>0x00000000<br> [0x800073b0]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe61729d7cfd5e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000cec]:fmul.d t5, t3, s10, dyn<br> [0x80000cf0]:csrrs a6, fcsr, zero<br> [0x80000cf4]:sw t5, 992(ra)<br> [0x80000cf8]:sw t6, 1000(ra)<br> [0x80000cfc]:sw t5, 1008(ra)<br> [0x80000d00]:sw a6, 1016(ra)<br>                            |
|  43|[0x800073b8]<br>0x00000000<br> [0x800073d0]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe61729d7cfd5e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000d34]:fmul.d t5, t3, s10, dyn<br> [0x80000d38]:csrrs a6, fcsr, zero<br> [0x80000d3c]:sw t5, 1024(ra)<br> [0x80000d40]:sw t6, 1032(ra)<br> [0x80000d44]:sw t5, 1040(ra)<br> [0x80000d48]:sw a6, 1048(ra)<br>                           |
|  44|[0x800073d8]<br>0x00000000<br> [0x800073f0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe61729d7cfd5e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000d7c]:fmul.d t5, t3, s10, dyn<br> [0x80000d80]:csrrs a6, fcsr, zero<br> [0x80000d84]:sw t5, 1056(ra)<br> [0x80000d88]:sw t6, 1064(ra)<br> [0x80000d8c]:sw t5, 1072(ra)<br> [0x80000d90]:sw a6, 1080(ra)<br>                           |
|  45|[0x800073f8]<br>0x00000000<br> [0x80007410]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe61729d7cfd5e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000dc4]:fmul.d t5, t3, s10, dyn<br> [0x80000dc8]:csrrs a6, fcsr, zero<br> [0x80000dcc]:sw t5, 1088(ra)<br> [0x80000dd0]:sw t6, 1096(ra)<br> [0x80000dd4]:sw t5, 1104(ra)<br> [0x80000dd8]:sw a6, 1112(ra)<br>                           |
|  46|[0x80007418]<br>0x00000000<br> [0x80007430]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc787db4043bd9 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000e0c]:fmul.d t5, t3, s10, dyn<br> [0x80000e10]:csrrs a6, fcsr, zero<br> [0x80000e14]:sw t5, 1120(ra)<br> [0x80000e18]:sw t6, 1128(ra)<br> [0x80000e1c]:sw t5, 1136(ra)<br> [0x80000e20]:sw a6, 1144(ra)<br>                           |
|  47|[0x80007438]<br>0x00000000<br> [0x80007450]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc787db4043bd9 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000e54]:fmul.d t5, t3, s10, dyn<br> [0x80000e58]:csrrs a6, fcsr, zero<br> [0x80000e5c]:sw t5, 1152(ra)<br> [0x80000e60]:sw t6, 1160(ra)<br> [0x80000e64]:sw t5, 1168(ra)<br> [0x80000e68]:sw a6, 1176(ra)<br>                           |
|  48|[0x80007458]<br>0x00000000<br> [0x80007470]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc787db4043bd9 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000e9c]:fmul.d t5, t3, s10, dyn<br> [0x80000ea0]:csrrs a6, fcsr, zero<br> [0x80000ea4]:sw t5, 1184(ra)<br> [0x80000ea8]:sw t6, 1192(ra)<br> [0x80000eac]:sw t5, 1200(ra)<br> [0x80000eb0]:sw a6, 1208(ra)<br>                           |
|  49|[0x80007478]<br>0x00000000<br> [0x80007490]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc787db4043bd9 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000ee4]:fmul.d t5, t3, s10, dyn<br> [0x80000ee8]:csrrs a6, fcsr, zero<br> [0x80000eec]:sw t5, 1216(ra)<br> [0x80000ef0]:sw t6, 1224(ra)<br> [0x80000ef4]:sw t5, 1232(ra)<br> [0x80000ef8]:sw a6, 1240(ra)<br>                           |
|  50|[0x80007498]<br>0x00000000<br> [0x800074b0]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc787db4043bd9 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000f2c]:fmul.d t5, t3, s10, dyn<br> [0x80000f30]:csrrs a6, fcsr, zero<br> [0x80000f34]:sw t5, 1248(ra)<br> [0x80000f38]:sw t6, 1256(ra)<br> [0x80000f3c]:sw t5, 1264(ra)<br> [0x80000f40]:sw a6, 1272(ra)<br>                           |
|  51|[0x800074b8]<br>0x00000000<br> [0x800074d0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x264ac77bf010f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000f74]:fmul.d t5, t3, s10, dyn<br> [0x80000f78]:csrrs a6, fcsr, zero<br> [0x80000f7c]:sw t5, 1280(ra)<br> [0x80000f80]:sw t6, 1288(ra)<br> [0x80000f84]:sw t5, 1296(ra)<br> [0x80000f88]:sw a6, 1304(ra)<br>                           |
|  52|[0x800074d8]<br>0x00000000<br> [0x800074f0]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x264ac77bf010f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000fbc]:fmul.d t5, t3, s10, dyn<br> [0x80000fc0]:csrrs a6, fcsr, zero<br> [0x80000fc4]:sw t5, 1312(ra)<br> [0x80000fc8]:sw t6, 1320(ra)<br> [0x80000fcc]:sw t5, 1328(ra)<br> [0x80000fd0]:sw a6, 1336(ra)<br>                           |
|  53|[0x800074f8]<br>0x00000000<br> [0x80007510]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x264ac77bf010f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001004]:fmul.d t5, t3, s10, dyn<br> [0x80001008]:csrrs a6, fcsr, zero<br> [0x8000100c]:sw t5, 1344(ra)<br> [0x80001010]:sw t6, 1352(ra)<br> [0x80001014]:sw t5, 1360(ra)<br> [0x80001018]:sw a6, 1368(ra)<br>                           |
|  54|[0x80007518]<br>0x00000000<br> [0x80007530]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x264ac77bf010f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000104c]:fmul.d t5, t3, s10, dyn<br> [0x80001050]:csrrs a6, fcsr, zero<br> [0x80001054]:sw t5, 1376(ra)<br> [0x80001058]:sw t6, 1384(ra)<br> [0x8000105c]:sw t5, 1392(ra)<br> [0x80001060]:sw a6, 1400(ra)<br>                           |
|  55|[0x80007538]<br>0x00000000<br> [0x80007550]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x264ac77bf010f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001094]:fmul.d t5, t3, s10, dyn<br> [0x80001098]:csrrs a6, fcsr, zero<br> [0x8000109c]:sw t5, 1408(ra)<br> [0x800010a0]:sw t6, 1416(ra)<br> [0x800010a4]:sw t5, 1424(ra)<br> [0x800010a8]:sw a6, 1432(ra)<br>                           |
|  56|[0x80007558]<br>0x00000000<br> [0x80007570]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xdc0d22f746bf5 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800010dc]:fmul.d t5, t3, s10, dyn<br> [0x800010e0]:csrrs a6, fcsr, zero<br> [0x800010e4]:sw t5, 1440(ra)<br> [0x800010e8]:sw t6, 1448(ra)<br> [0x800010ec]:sw t5, 1456(ra)<br> [0x800010f0]:sw a6, 1464(ra)<br>                           |
|  57|[0x80007578]<br>0x00000000<br> [0x80007590]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xdc0d22f746bf5 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001124]:fmul.d t5, t3, s10, dyn<br> [0x80001128]:csrrs a6, fcsr, zero<br> [0x8000112c]:sw t5, 1472(ra)<br> [0x80001130]:sw t6, 1480(ra)<br> [0x80001134]:sw t5, 1488(ra)<br> [0x80001138]:sw a6, 1496(ra)<br>                           |
|  58|[0x80007598]<br>0x00000000<br> [0x800075b0]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xdc0d22f746bf5 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000116c]:fmul.d t5, t3, s10, dyn<br> [0x80001170]:csrrs a6, fcsr, zero<br> [0x80001174]:sw t5, 1504(ra)<br> [0x80001178]:sw t6, 1512(ra)<br> [0x8000117c]:sw t5, 1520(ra)<br> [0x80001180]:sw a6, 1528(ra)<br>                           |
|  59|[0x800075b8]<br>0x00000000<br> [0x800075d0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xdc0d22f746bf5 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800011b4]:fmul.d t5, t3, s10, dyn<br> [0x800011b8]:csrrs a6, fcsr, zero<br> [0x800011bc]:sw t5, 1536(ra)<br> [0x800011c0]:sw t6, 1544(ra)<br> [0x800011c4]:sw t5, 1552(ra)<br> [0x800011c8]:sw a6, 1560(ra)<br>                           |
|  60|[0x800075d8]<br>0x00000000<br> [0x800075f0]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xdc0d22f746bf5 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800011fc]:fmul.d t5, t3, s10, dyn<br> [0x80001200]:csrrs a6, fcsr, zero<br> [0x80001204]:sw t5, 1568(ra)<br> [0x80001208]:sw t6, 1576(ra)<br> [0x8000120c]:sw t5, 1584(ra)<br> [0x80001210]:sw a6, 1592(ra)<br>                           |
|  61|[0x800075f8]<br>0x00000000<br> [0x80007610]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x8522a1b638e23 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001244]:fmul.d t5, t3, s10, dyn<br> [0x80001248]:csrrs a6, fcsr, zero<br> [0x8000124c]:sw t5, 1600(ra)<br> [0x80001250]:sw t6, 1608(ra)<br> [0x80001254]:sw t5, 1616(ra)<br> [0x80001258]:sw a6, 1624(ra)<br>                           |
|  62|[0x80007618]<br>0x00000000<br> [0x80007630]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x8522a1b638e23 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000128c]:fmul.d t5, t3, s10, dyn<br> [0x80001290]:csrrs a6, fcsr, zero<br> [0x80001294]:sw t5, 1632(ra)<br> [0x80001298]:sw t6, 1640(ra)<br> [0x8000129c]:sw t5, 1648(ra)<br> [0x800012a0]:sw a6, 1656(ra)<br>                           |
|  63|[0x80007638]<br>0x00000000<br> [0x80007650]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x8522a1b638e23 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800012d4]:fmul.d t5, t3, s10, dyn<br> [0x800012d8]:csrrs a6, fcsr, zero<br> [0x800012dc]:sw t5, 1664(ra)<br> [0x800012e0]:sw t6, 1672(ra)<br> [0x800012e4]:sw t5, 1680(ra)<br> [0x800012e8]:sw a6, 1688(ra)<br>                           |
|  64|[0x80007658]<br>0x00000000<br> [0x80007670]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x8522a1b638e23 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000131c]:fmul.d t5, t3, s10, dyn<br> [0x80001320]:csrrs a6, fcsr, zero<br> [0x80001324]:sw t5, 1696(ra)<br> [0x80001328]:sw t6, 1704(ra)<br> [0x8000132c]:sw t5, 1712(ra)<br> [0x80001330]:sw a6, 1720(ra)<br>                           |
|  65|[0x80007678]<br>0x00000000<br> [0x80007690]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x8522a1b638e23 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001364]:fmul.d t5, t3, s10, dyn<br> [0x80001368]:csrrs a6, fcsr, zero<br> [0x8000136c]:sw t5, 1728(ra)<br> [0x80001370]:sw t6, 1736(ra)<br> [0x80001374]:sw t5, 1744(ra)<br> [0x80001378]:sw a6, 1752(ra)<br>                           |
|  66|[0x80007698]<br>0x00000000<br> [0x800076b0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0d2f778a86fa6 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800013ac]:fmul.d t5, t3, s10, dyn<br> [0x800013b0]:csrrs a6, fcsr, zero<br> [0x800013b4]:sw t5, 1760(ra)<br> [0x800013b8]:sw t6, 1768(ra)<br> [0x800013bc]:sw t5, 1776(ra)<br> [0x800013c0]:sw a6, 1784(ra)<br>                           |
|  67|[0x800076b8]<br>0x00000000<br> [0x800076d0]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0d2f778a86fa6 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800013f4]:fmul.d t5, t3, s10, dyn<br> [0x800013f8]:csrrs a6, fcsr, zero<br> [0x800013fc]:sw t5, 1792(ra)<br> [0x80001400]:sw t6, 1800(ra)<br> [0x80001404]:sw t5, 1808(ra)<br> [0x80001408]:sw a6, 1816(ra)<br>                           |
|  68|[0x800076d8]<br>0x00000000<br> [0x800076f0]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0d2f778a86fa6 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000143c]:fmul.d t5, t3, s10, dyn<br> [0x80001440]:csrrs a6, fcsr, zero<br> [0x80001444]:sw t5, 1824(ra)<br> [0x80001448]:sw t6, 1832(ra)<br> [0x8000144c]:sw t5, 1840(ra)<br> [0x80001450]:sw a6, 1848(ra)<br>                           |
|  69|[0x800076f8]<br>0x00000000<br> [0x80007710]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0d2f778a86fa6 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001484]:fmul.d t5, t3, s10, dyn<br> [0x80001488]:csrrs a6, fcsr, zero<br> [0x8000148c]:sw t5, 1856(ra)<br> [0x80001490]:sw t6, 1864(ra)<br> [0x80001494]:sw t5, 1872(ra)<br> [0x80001498]:sw a6, 1880(ra)<br>                           |
|  70|[0x80007718]<br>0x00000000<br> [0x80007730]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0d2f778a86fa6 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800014cc]:fmul.d t5, t3, s10, dyn<br> [0x800014d0]:csrrs a6, fcsr, zero<br> [0x800014d4]:sw t5, 1888(ra)<br> [0x800014d8]:sw t6, 1896(ra)<br> [0x800014dc]:sw t5, 1904(ra)<br> [0x800014e0]:sw a6, 1912(ra)<br>                           |
|  71|[0x80007738]<br>0x00000000<br> [0x80007750]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xe394ab3b08c6b and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001514]:fmul.d t5, t3, s10, dyn<br> [0x80001518]:csrrs a6, fcsr, zero<br> [0x8000151c]:sw t5, 1920(ra)<br> [0x80001520]:sw t6, 1928(ra)<br> [0x80001524]:sw t5, 1936(ra)<br> [0x80001528]:sw a6, 1944(ra)<br>                           |
|  72|[0x80007758]<br>0x00000000<br> [0x80007770]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xe394ab3b08c6b and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000155c]:fmul.d t5, t3, s10, dyn<br> [0x80001560]:csrrs a6, fcsr, zero<br> [0x80001564]:sw t5, 1952(ra)<br> [0x80001568]:sw t6, 1960(ra)<br> [0x8000156c]:sw t5, 1968(ra)<br> [0x80001570]:sw a6, 1976(ra)<br>                           |
|  73|[0x80007778]<br>0x00000000<br> [0x80007790]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xe394ab3b08c6b and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800015a4]:fmul.d t5, t3, s10, dyn<br> [0x800015a8]:csrrs a6, fcsr, zero<br> [0x800015ac]:sw t5, 1984(ra)<br> [0x800015b0]:sw t6, 1992(ra)<br> [0x800015b4]:sw t5, 2000(ra)<br> [0x800015b8]:sw a6, 2008(ra)<br>                           |
|  74|[0x80007798]<br>0x00000000<br> [0x800077b0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xe394ab3b08c6b and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800015ec]:fmul.d t5, t3, s10, dyn<br> [0x800015f0]:csrrs a6, fcsr, zero<br> [0x800015f4]:sw t5, 2016(ra)<br> [0x800015f8]:sw t6, 2024(ra)<br> [0x800015fc]:sw t5, 2032(ra)<br> [0x80001600]:sw a6, 2040(ra)<br>                           |
|  75|[0x800077b8]<br>0x00000000<br> [0x800077d0]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xe394ab3b08c6b and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001634]:fmul.d t5, t3, s10, dyn<br> [0x80001638]:csrrs a6, fcsr, zero<br> [0x8000163c]:addi ra, ra, 2040<br> [0x80001640]:sw t5, 8(ra)<br> [0x80001644]:sw t6, 16(ra)<br> [0x80001648]:sw t5, 24(ra)<br> [0x8000164c]:sw a6, 32(ra)<br> |
|  76|[0x800077d8]<br>0x00000000<br> [0x800077f0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x4cf244963827f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001680]:fmul.d t5, t3, s10, dyn<br> [0x80001684]:csrrs a6, fcsr, zero<br> [0x80001688]:sw t5, 40(ra)<br> [0x8000168c]:sw t6, 48(ra)<br> [0x80001690]:sw t5, 56(ra)<br> [0x80001694]:sw a6, 64(ra)<br>                                   |
|  77|[0x800077f8]<br>0x00000000<br> [0x80007810]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x4cf244963827f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800016c8]:fmul.d t5, t3, s10, dyn<br> [0x800016cc]:csrrs a6, fcsr, zero<br> [0x800016d0]:sw t5, 72(ra)<br> [0x800016d4]:sw t6, 80(ra)<br> [0x800016d8]:sw t5, 88(ra)<br> [0x800016dc]:sw a6, 96(ra)<br>                                   |
|  78|[0x80007818]<br>0x00000000<br> [0x80007830]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x4cf244963827f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001710]:fmul.d t5, t3, s10, dyn<br> [0x80001714]:csrrs a6, fcsr, zero<br> [0x80001718]:sw t5, 104(ra)<br> [0x8000171c]:sw t6, 112(ra)<br> [0x80001720]:sw t5, 120(ra)<br> [0x80001724]:sw a6, 128(ra)<br>                               |
|  79|[0x80007838]<br>0x00000000<br> [0x80007850]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x4cf244963827f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001758]:fmul.d t5, t3, s10, dyn<br> [0x8000175c]:csrrs a6, fcsr, zero<br> [0x80001760]:sw t5, 136(ra)<br> [0x80001764]:sw t6, 144(ra)<br> [0x80001768]:sw t5, 152(ra)<br> [0x8000176c]:sw a6, 160(ra)<br>                               |
|  80|[0x80007858]<br>0x00000000<br> [0x80007870]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x4cf244963827f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800017a0]:fmul.d t5, t3, s10, dyn<br> [0x800017a4]:csrrs a6, fcsr, zero<br> [0x800017a8]:sw t5, 168(ra)<br> [0x800017ac]:sw t6, 176(ra)<br> [0x800017b0]:sw t5, 184(ra)<br> [0x800017b4]:sw a6, 192(ra)<br>                               |
|  81|[0x80007878]<br>0x00000000<br> [0x80007890]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x053c0f57052f0 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800017e8]:fmul.d t5, t3, s10, dyn<br> [0x800017ec]:csrrs a6, fcsr, zero<br> [0x800017f0]:sw t5, 200(ra)<br> [0x800017f4]:sw t6, 208(ra)<br> [0x800017f8]:sw t5, 216(ra)<br> [0x800017fc]:sw a6, 224(ra)<br>                               |
|  82|[0x80007898]<br>0x00000000<br> [0x800078b0]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x053c0f57052f0 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001830]:fmul.d t5, t3, s10, dyn<br> [0x80001834]:csrrs a6, fcsr, zero<br> [0x80001838]:sw t5, 232(ra)<br> [0x8000183c]:sw t6, 240(ra)<br> [0x80001840]:sw t5, 248(ra)<br> [0x80001844]:sw a6, 256(ra)<br>                               |
|  83|[0x800078b8]<br>0x00000000<br> [0x800078d0]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x053c0f57052f0 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001878]:fmul.d t5, t3, s10, dyn<br> [0x8000187c]:csrrs a6, fcsr, zero<br> [0x80001880]:sw t5, 264(ra)<br> [0x80001884]:sw t6, 272(ra)<br> [0x80001888]:sw t5, 280(ra)<br> [0x8000188c]:sw a6, 288(ra)<br>                               |
|  84|[0x800078d8]<br>0x00000000<br> [0x800078f0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x053c0f57052f0 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800018c0]:fmul.d t5, t3, s10, dyn<br> [0x800018c4]:csrrs a6, fcsr, zero<br> [0x800018c8]:sw t5, 296(ra)<br> [0x800018cc]:sw t6, 304(ra)<br> [0x800018d0]:sw t5, 312(ra)<br> [0x800018d4]:sw a6, 320(ra)<br>                               |
|  85|[0x800078f8]<br>0x00000000<br> [0x80007910]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x053c0f57052f0 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001908]:fmul.d t5, t3, s10, dyn<br> [0x8000190c]:csrrs a6, fcsr, zero<br> [0x80001910]:sw t5, 328(ra)<br> [0x80001914]:sw t6, 336(ra)<br> [0x80001918]:sw t5, 344(ra)<br> [0x8000191c]:sw a6, 352(ra)<br>                               |
|  86|[0x80007918]<br>0x00000000<br> [0x80007930]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x59556723d53e2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001950]:fmul.d t5, t3, s10, dyn<br> [0x80001954]:csrrs a6, fcsr, zero<br> [0x80001958]:sw t5, 360(ra)<br> [0x8000195c]:sw t6, 368(ra)<br> [0x80001960]:sw t5, 376(ra)<br> [0x80001964]:sw a6, 384(ra)<br>                               |
|  87|[0x80007938]<br>0x00000000<br> [0x80007950]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x59556723d53e2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001998]:fmul.d t5, t3, s10, dyn<br> [0x8000199c]:csrrs a6, fcsr, zero<br> [0x800019a0]:sw t5, 392(ra)<br> [0x800019a4]:sw t6, 400(ra)<br> [0x800019a8]:sw t5, 408(ra)<br> [0x800019ac]:sw a6, 416(ra)<br>                               |
|  88|[0x80007958]<br>0x00000000<br> [0x80007970]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x59556723d53e2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800019e0]:fmul.d t5, t3, s10, dyn<br> [0x800019e4]:csrrs a6, fcsr, zero<br> [0x800019e8]:sw t5, 424(ra)<br> [0x800019ec]:sw t6, 432(ra)<br> [0x800019f0]:sw t5, 440(ra)<br> [0x800019f4]:sw a6, 448(ra)<br>                               |
|  89|[0x80007978]<br>0x00000000<br> [0x80007990]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x59556723d53e2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001a28]:fmul.d t5, t3, s10, dyn<br> [0x80001a2c]:csrrs a6, fcsr, zero<br> [0x80001a30]:sw t5, 456(ra)<br> [0x80001a34]:sw t6, 464(ra)<br> [0x80001a38]:sw t5, 472(ra)<br> [0x80001a3c]:sw a6, 480(ra)<br>                               |
|  90|[0x80007998]<br>0x00000000<br> [0x800079b0]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x59556723d53e2 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001a70]:fmul.d t5, t3, s10, dyn<br> [0x80001a74]:csrrs a6, fcsr, zero<br> [0x80001a78]:sw t5, 488(ra)<br> [0x80001a7c]:sw t6, 496(ra)<br> [0x80001a80]:sw t5, 504(ra)<br> [0x80001a84]:sw a6, 512(ra)<br>                               |
|  91|[0x800079b8]<br>0x00000000<br> [0x800079d0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc982355c85538 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001ab8]:fmul.d t5, t3, s10, dyn<br> [0x80001abc]:csrrs a6, fcsr, zero<br> [0x80001ac0]:sw t5, 520(ra)<br> [0x80001ac4]:sw t6, 528(ra)<br> [0x80001ac8]:sw t5, 536(ra)<br> [0x80001acc]:sw a6, 544(ra)<br>                               |
|  92|[0x800079d8]<br>0x00000000<br> [0x800079f0]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc982355c85538 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001b00]:fmul.d t5, t3, s10, dyn<br> [0x80001b04]:csrrs a6, fcsr, zero<br> [0x80001b08]:sw t5, 552(ra)<br> [0x80001b0c]:sw t6, 560(ra)<br> [0x80001b10]:sw t5, 568(ra)<br> [0x80001b14]:sw a6, 576(ra)<br>                               |
|  93|[0x800079f8]<br>0x00000000<br> [0x80007a10]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc982355c85538 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001b48]:fmul.d t5, t3, s10, dyn<br> [0x80001b4c]:csrrs a6, fcsr, zero<br> [0x80001b50]:sw t5, 584(ra)<br> [0x80001b54]:sw t6, 592(ra)<br> [0x80001b58]:sw t5, 600(ra)<br> [0x80001b5c]:sw a6, 608(ra)<br>                               |
|  94|[0x80007a18]<br>0x00000000<br> [0x80007a30]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc982355c85538 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001b90]:fmul.d t5, t3, s10, dyn<br> [0x80001b94]:csrrs a6, fcsr, zero<br> [0x80001b98]:sw t5, 616(ra)<br> [0x80001b9c]:sw t6, 624(ra)<br> [0x80001ba0]:sw t5, 632(ra)<br> [0x80001ba4]:sw a6, 640(ra)<br>                               |
|  95|[0x80007a38]<br>0x00000000<br> [0x80007a50]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc982355c85538 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001bd8]:fmul.d t5, t3, s10, dyn<br> [0x80001bdc]:csrrs a6, fcsr, zero<br> [0x80001be0]:sw t5, 648(ra)<br> [0x80001be4]:sw t6, 656(ra)<br> [0x80001be8]:sw t5, 664(ra)<br> [0x80001bec]:sw a6, 672(ra)<br>                               |
|  96|[0x80007a58]<br>0x00000000<br> [0x80007a70]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x78037fa19f977 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001c20]:fmul.d t5, t3, s10, dyn<br> [0x80001c24]:csrrs a6, fcsr, zero<br> [0x80001c28]:sw t5, 680(ra)<br> [0x80001c2c]:sw t6, 688(ra)<br> [0x80001c30]:sw t5, 696(ra)<br> [0x80001c34]:sw a6, 704(ra)<br>                               |
|  97|[0x80007a78]<br>0x00000000<br> [0x80007a90]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x78037fa19f977 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001c68]:fmul.d t5, t3, s10, dyn<br> [0x80001c6c]:csrrs a6, fcsr, zero<br> [0x80001c70]:sw t5, 712(ra)<br> [0x80001c74]:sw t6, 720(ra)<br> [0x80001c78]:sw t5, 728(ra)<br> [0x80001c7c]:sw a6, 736(ra)<br>                               |
|  98|[0x80007a98]<br>0x00000000<br> [0x80007ab0]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x78037fa19f977 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001cb0]:fmul.d t5, t3, s10, dyn<br> [0x80001cb4]:csrrs a6, fcsr, zero<br> [0x80001cb8]:sw t5, 744(ra)<br> [0x80001cbc]:sw t6, 752(ra)<br> [0x80001cc0]:sw t5, 760(ra)<br> [0x80001cc4]:sw a6, 768(ra)<br>                               |
|  99|[0x80007ab8]<br>0x00000000<br> [0x80007ad0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x78037fa19f977 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001cf8]:fmul.d t5, t3, s10, dyn<br> [0x80001cfc]:csrrs a6, fcsr, zero<br> [0x80001d00]:sw t5, 776(ra)<br> [0x80001d04]:sw t6, 784(ra)<br> [0x80001d08]:sw t5, 792(ra)<br> [0x80001d0c]:sw a6, 800(ra)<br>                               |
| 100|[0x80007ad8]<br>0x00000000<br> [0x80007af0]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x78037fa19f977 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001d40]:fmul.d t5, t3, s10, dyn<br> [0x80001d44]:csrrs a6, fcsr, zero<br> [0x80001d48]:sw t5, 808(ra)<br> [0x80001d4c]:sw t6, 816(ra)<br> [0x80001d50]:sw t5, 824(ra)<br> [0x80001d54]:sw a6, 832(ra)<br>                               |
| 101|[0x80007af8]<br>0x00000000<br> [0x80007b10]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8698ed174ff65 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001d88]:fmul.d t5, t3, s10, dyn<br> [0x80001d8c]:csrrs a6, fcsr, zero<br> [0x80001d90]:sw t5, 840(ra)<br> [0x80001d94]:sw t6, 848(ra)<br> [0x80001d98]:sw t5, 856(ra)<br> [0x80001d9c]:sw a6, 864(ra)<br>                               |
| 102|[0x80007b18]<br>0x00000000<br> [0x80007b30]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8698ed174ff65 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001dd0]:fmul.d t5, t3, s10, dyn<br> [0x80001dd4]:csrrs a6, fcsr, zero<br> [0x80001dd8]:sw t5, 872(ra)<br> [0x80001ddc]:sw t6, 880(ra)<br> [0x80001de0]:sw t5, 888(ra)<br> [0x80001de4]:sw a6, 896(ra)<br>                               |
| 103|[0x80007b38]<br>0x00000000<br> [0x80007b50]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8698ed174ff65 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001e18]:fmul.d t5, t3, s10, dyn<br> [0x80001e1c]:csrrs a6, fcsr, zero<br> [0x80001e20]:sw t5, 904(ra)<br> [0x80001e24]:sw t6, 912(ra)<br> [0x80001e28]:sw t5, 920(ra)<br> [0x80001e2c]:sw a6, 928(ra)<br>                               |
| 104|[0x80007b58]<br>0x00000000<br> [0x80007b70]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8698ed174ff65 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001e60]:fmul.d t5, t3, s10, dyn<br> [0x80001e64]:csrrs a6, fcsr, zero<br> [0x80001e68]:sw t5, 936(ra)<br> [0x80001e6c]:sw t6, 944(ra)<br> [0x80001e70]:sw t5, 952(ra)<br> [0x80001e74]:sw a6, 960(ra)<br>                               |
| 105|[0x80007b78]<br>0x00000000<br> [0x80007b90]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8698ed174ff65 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001ea8]:fmul.d t5, t3, s10, dyn<br> [0x80001eac]:csrrs a6, fcsr, zero<br> [0x80001eb0]:sw t5, 968(ra)<br> [0x80001eb4]:sw t6, 976(ra)<br> [0x80001eb8]:sw t5, 984(ra)<br> [0x80001ebc]:sw a6, 992(ra)<br>                               |
| 106|[0x80007b98]<br>0x22E524AD<br> [0x80007bb0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x6a47222e524ad and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000008000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001ef0]:fmul.d t5, t3, s10, dyn<br> [0x80001ef4]:csrrs a6, fcsr, zero<br> [0x80001ef8]:sw t5, 1000(ra)<br> [0x80001efc]:sw t6, 1008(ra)<br> [0x80001f00]:sw t5, 1016(ra)<br> [0x80001f04]:sw a6, 1024(ra)<br>                           |
| 107|[0x80007bb8]<br>0x22E524AD<br> [0x80007bd0]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x6a47222e524ad and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000008000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001f38]:fmul.d t5, t3, s10, dyn<br> [0x80001f3c]:csrrs a6, fcsr, zero<br> [0x80001f40]:sw t5, 1032(ra)<br> [0x80001f44]:sw t6, 1040(ra)<br> [0x80001f48]:sw t5, 1048(ra)<br> [0x80001f4c]:sw a6, 1056(ra)<br>                           |
| 108|[0x80007bd8]<br>0x22E524AD<br> [0x80007bf0]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x6a47222e524ad and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000008000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001f80]:fmul.d t5, t3, s10, dyn<br> [0x80001f84]:csrrs a6, fcsr, zero<br> [0x80001f88]:sw t5, 1064(ra)<br> [0x80001f8c]:sw t6, 1072(ra)<br> [0x80001f90]:sw t5, 1080(ra)<br> [0x80001f94]:sw a6, 1088(ra)<br>                           |
| 109|[0x80007bf8]<br>0x22E524AD<br> [0x80007c10]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x6a47222e524ad and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000008000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80001fc8]:fmul.d t5, t3, s10, dyn<br> [0x80001fcc]:csrrs a6, fcsr, zero<br> [0x80001fd0]:sw t5, 1096(ra)<br> [0x80001fd4]:sw t6, 1104(ra)<br> [0x80001fd8]:sw t5, 1112(ra)<br> [0x80001fdc]:sw a6, 1120(ra)<br>                           |
| 110|[0x80007c18]<br>0x22E524AD<br> [0x80007c30]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x6a47222e524ad and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000008000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80002010]:fmul.d t5, t3, s10, dyn<br> [0x80002014]:csrrs a6, fcsr, zero<br> [0x80002018]:sw t5, 1128(ra)<br> [0x8000201c]:sw t6, 1136(ra)<br> [0x80002020]:sw t5, 1144(ra)<br> [0x80002024]:sw a6, 1152(ra)<br>                           |
| 111|[0x80007c38]<br>0x79C877A7<br> [0x80007c50]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xec7e479c877a7 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000008000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002058]:fmul.d t5, t3, s10, dyn<br> [0x8000205c]:csrrs a6, fcsr, zero<br> [0x80002060]:sw t5, 1160(ra)<br> [0x80002064]:sw t6, 1168(ra)<br> [0x80002068]:sw t5, 1176(ra)<br> [0x8000206c]:sw a6, 1184(ra)<br>                           |
