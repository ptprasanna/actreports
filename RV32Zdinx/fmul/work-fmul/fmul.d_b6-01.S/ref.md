
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80002e90')]      |
| SIG_REGION                | [('0x80004a10', '0x80005300', '572 words')]      |
| COV_LABELS                | fmul.d_b6      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-fmul/fmul.d_b6-01.S/ref.S    |
| Total Number of coverpoints| 191     |
| Total Coverpoints Hit     | 191      |
| Total Signature Updates   | 306      |
| STAT1                     | 76      |
| STAT2                     | 0      |
| STAT3                     | 66     |
| STAT4                     | 153     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80001920]:fmul.d t5, t3, s10, dyn
[0x80001924]:csrrs a6, fcsr, zero
[0x80001928]:sw t5, 72(ra)
[0x8000192c]:sw t6, 80(ra)
[0x80001930]:sw t5, 88(ra)
[0x80001934]:sw a6, 96(ra)
[0x80001938]:lw t3, 1072(a5)
[0x8000193c]:lw t4, 1076(a5)
[0x80001940]:lw s10, 1080(a5)
[0x80001944]:lw s11, 1084(a5)
[0x80001948]:lui t3, 998089
[0x8000194c]:addi t3, t3, 315
[0x80001950]:lui t4, 236132
[0x80001954]:addi t4, t4, 516
[0x80001958]:lui s10, 955016
[0x8000195c]:addi s10, s10, 3149
[0x80001960]:lui s11, 266096
[0x80001964]:addi s11, s11, 201
[0x80001968]:addi a4, zero, 64
[0x8000196c]:csrrw zero, fcsr, a4
[0x80001970]:fmul.d t5, t3, s10, dyn
[0x80001974]:csrrs a6, fcsr, zero

[0x80001970]:fmul.d t5, t3, s10, dyn
[0x80001974]:csrrs a6, fcsr, zero
[0x80001978]:sw t5, 104(ra)
[0x8000197c]:sw t6, 112(ra)
[0x80001980]:sw t5, 120(ra)
[0x80001984]:sw a6, 128(ra)
[0x80001988]:lw t3, 1088(a5)
[0x8000198c]:lw t4, 1092(a5)
[0x80001990]:lw s10, 1096(a5)
[0x80001994]:lw s11, 1100(a5)
[0x80001998]:lui t3, 998089
[0x8000199c]:addi t3, t3, 315
[0x800019a0]:lui t4, 236132
[0x800019a4]:addi t4, t4, 516
[0x800019a8]:lui s10, 955016
[0x800019ac]:addi s10, s10, 3149
[0x800019b0]:lui s11, 266096
[0x800019b4]:addi s11, s11, 201
[0x800019b8]:addi a4, zero, 96
[0x800019bc]:csrrw zero, fcsr, a4
[0x800019c0]:fmul.d t5, t3, s10, dyn
[0x800019c4]:csrrs a6, fcsr, zero

[0x800019c0]:fmul.d t5, t3, s10, dyn
[0x800019c4]:csrrs a6, fcsr, zero
[0x800019c8]:sw t5, 136(ra)
[0x800019cc]:sw t6, 144(ra)
[0x800019d0]:sw t5, 152(ra)
[0x800019d4]:sw a6, 160(ra)
[0x800019d8]:lw t3, 1104(a5)
[0x800019dc]:lw t4, 1108(a5)
[0x800019e0]:lw s10, 1112(a5)
[0x800019e4]:lw s11, 1116(a5)
[0x800019e8]:lui t3, 998089
[0x800019ec]:addi t3, t3, 315
[0x800019f0]:lui t4, 236132
[0x800019f4]:addi t4, t4, 516
[0x800019f8]:lui s10, 955016
[0x800019fc]:addi s10, s10, 3149
[0x80001a00]:lui s11, 266096
[0x80001a04]:addi s11, s11, 201
[0x80001a08]:addi a4, zero, 128
[0x80001a0c]:csrrw zero, fcsr, a4
[0x80001a10]:fmul.d t5, t3, s10, dyn
[0x80001a14]:csrrs a6, fcsr, zero

[0x80001a10]:fmul.d t5, t3, s10, dyn
[0x80001a14]:csrrs a6, fcsr, zero
[0x80001a18]:sw t5, 168(ra)
[0x80001a1c]:sw t6, 176(ra)
[0x80001a20]:sw t5, 184(ra)
[0x80001a24]:sw a6, 192(ra)
[0x80001a28]:lw t3, 1120(a5)
[0x80001a2c]:lw t4, 1124(a5)
[0x80001a30]:lw s10, 1128(a5)
[0x80001a34]:lw s11, 1132(a5)
[0x80001a38]:lui t3, 247182
[0x80001a3c]:addi t3, t3, 1520
[0x80001a40]:lui t4, 236243
[0x80001a44]:addi t4, t4, 471
[0x80001a48]:lui s10, 845975
[0x80001a4c]:addi s10, s10, 3501
[0x80001a50]:lui s11, 263916
[0x80001a54]:addi s11, s11, 525
[0x80001a58]:addi a4, zero, 0
[0x80001a5c]:csrrw zero, fcsr, a4
[0x80001a60]:fmul.d t5, t3, s10, dyn
[0x80001a64]:csrrs a6, fcsr, zero

[0x80001a60]:fmul.d t5, t3, s10, dyn
[0x80001a64]:csrrs a6, fcsr, zero
[0x80001a68]:sw t5, 200(ra)
[0x80001a6c]:sw t6, 208(ra)
[0x80001a70]:sw t5, 216(ra)
[0x80001a74]:sw a6, 224(ra)
[0x80001a78]:lw t3, 1136(a5)
[0x80001a7c]:lw t4, 1140(a5)
[0x80001a80]:lw s10, 1144(a5)
[0x80001a84]:lw s11, 1148(a5)
[0x80001a88]:lui t3, 247182
[0x80001a8c]:addi t3, t3, 1520
[0x80001a90]:lui t4, 236243
[0x80001a94]:addi t4, t4, 471
[0x80001a98]:lui s10, 845975
[0x80001a9c]:addi s10, s10, 3501
[0x80001aa0]:lui s11, 263916
[0x80001aa4]:addi s11, s11, 525
[0x80001aa8]:addi a4, zero, 32
[0x80001aac]:csrrw zero, fcsr, a4
[0x80001ab0]:fmul.d t5, t3, s10, dyn
[0x80001ab4]:csrrs a6, fcsr, zero

[0x80001ab0]:fmul.d t5, t3, s10, dyn
[0x80001ab4]:csrrs a6, fcsr, zero
[0x80001ab8]:sw t5, 232(ra)
[0x80001abc]:sw t6, 240(ra)
[0x80001ac0]:sw t5, 248(ra)
[0x80001ac4]:sw a6, 256(ra)
[0x80001ac8]:lw t3, 1152(a5)
[0x80001acc]:lw t4, 1156(a5)
[0x80001ad0]:lw s10, 1160(a5)
[0x80001ad4]:lw s11, 1164(a5)
[0x80001ad8]:lui t3, 247182
[0x80001adc]:addi t3, t3, 1520
[0x80001ae0]:lui t4, 236243
[0x80001ae4]:addi t4, t4, 471
[0x80001ae8]:lui s10, 845975
[0x80001aec]:addi s10, s10, 3501
[0x80001af0]:lui s11, 263916
[0x80001af4]:addi s11, s11, 525
[0x80001af8]:addi a4, zero, 64
[0x80001afc]:csrrw zero, fcsr, a4
[0x80001b00]:fmul.d t5, t3, s10, dyn
[0x80001b04]:csrrs a6, fcsr, zero

[0x80001b00]:fmul.d t5, t3, s10, dyn
[0x80001b04]:csrrs a6, fcsr, zero
[0x80001b08]:sw t5, 264(ra)
[0x80001b0c]:sw t6, 272(ra)
[0x80001b10]:sw t5, 280(ra)
[0x80001b14]:sw a6, 288(ra)
[0x80001b18]:lw t3, 1168(a5)
[0x80001b1c]:lw t4, 1172(a5)
[0x80001b20]:lw s10, 1176(a5)
[0x80001b24]:lw s11, 1180(a5)
[0x80001b28]:lui t3, 247182
[0x80001b2c]:addi t3, t3, 1520
[0x80001b30]:lui t4, 236243
[0x80001b34]:addi t4, t4, 471
[0x80001b38]:lui s10, 845975
[0x80001b3c]:addi s10, s10, 3501
[0x80001b40]:lui s11, 263916
[0x80001b44]:addi s11, s11, 525
[0x80001b48]:addi a4, zero, 96
[0x80001b4c]:csrrw zero, fcsr, a4
[0x80001b50]:fmul.d t5, t3, s10, dyn
[0x80001b54]:csrrs a6, fcsr, zero

[0x80001b50]:fmul.d t5, t3, s10, dyn
[0x80001b54]:csrrs a6, fcsr, zero
[0x80001b58]:sw t5, 296(ra)
[0x80001b5c]:sw t6, 304(ra)
[0x80001b60]:sw t5, 312(ra)
[0x80001b64]:sw a6, 320(ra)
[0x80001b68]:lw t3, 1184(a5)
[0x80001b6c]:lw t4, 1188(a5)
[0x80001b70]:lw s10, 1192(a5)
[0x80001b74]:lw s11, 1196(a5)
[0x80001b78]:lui t3, 247182
[0x80001b7c]:addi t3, t3, 1520
[0x80001b80]:lui t4, 236243
[0x80001b84]:addi t4, t4, 471
[0x80001b88]:lui s10, 845975
[0x80001b8c]:addi s10, s10, 3501
[0x80001b90]:lui s11, 263916
[0x80001b94]:addi s11, s11, 525
[0x80001b98]:addi a4, zero, 128
[0x80001b9c]:csrrw zero, fcsr, a4
[0x80001ba0]:fmul.d t5, t3, s10, dyn
[0x80001ba4]:csrrs a6, fcsr, zero

[0x80001ba0]:fmul.d t5, t3, s10, dyn
[0x80001ba4]:csrrs a6, fcsr, zero
[0x80001ba8]:sw t5, 328(ra)
[0x80001bac]:sw t6, 336(ra)
[0x80001bb0]:sw t5, 344(ra)
[0x80001bb4]:sw a6, 352(ra)
[0x80001bb8]:lw t3, 1200(a5)
[0x80001bbc]:lw t4, 1204(a5)
[0x80001bc0]:lw s10, 1208(a5)
[0x80001bc4]:lw s11, 1212(a5)
[0x80001bc8]:lui t3, 37547
[0x80001bcc]:addi t3, t3, 1679
[0x80001bd0]:lui t4, 236300
[0x80001bd4]:addi t4, t4, 2775
[0x80001bd8]:lui s10, 220819
[0x80001bdc]:addi s10, s10, 1993
[0x80001be0]:lui s11, 262378
[0x80001be4]:addi s11, s11, 2719
[0x80001be8]:addi a4, zero, 0
[0x80001bec]:csrrw zero, fcsr, a4
[0x80001bf0]:fmul.d t5, t3, s10, dyn
[0x80001bf4]:csrrs a6, fcsr, zero

[0x80001bf0]:fmul.d t5, t3, s10, dyn
[0x80001bf4]:csrrs a6, fcsr, zero
[0x80001bf8]:sw t5, 360(ra)
[0x80001bfc]:sw t6, 368(ra)
[0x80001c00]:sw t5, 376(ra)
[0x80001c04]:sw a6, 384(ra)
[0x80001c08]:lw t3, 1216(a5)
[0x80001c0c]:lw t4, 1220(a5)
[0x80001c10]:lw s10, 1224(a5)
[0x80001c14]:lw s11, 1228(a5)
[0x80001c18]:lui t3, 37547
[0x80001c1c]:addi t3, t3, 1679
[0x80001c20]:lui t4, 236300
[0x80001c24]:addi t4, t4, 2775
[0x80001c28]:lui s10, 220819
[0x80001c2c]:addi s10, s10, 1993
[0x80001c30]:lui s11, 262378
[0x80001c34]:addi s11, s11, 2719
[0x80001c38]:addi a4, zero, 32
[0x80001c3c]:csrrw zero, fcsr, a4
[0x80001c40]:fmul.d t5, t3, s10, dyn
[0x80001c44]:csrrs a6, fcsr, zero

[0x80001c40]:fmul.d t5, t3, s10, dyn
[0x80001c44]:csrrs a6, fcsr, zero
[0x80001c48]:sw t5, 392(ra)
[0x80001c4c]:sw t6, 400(ra)
[0x80001c50]:sw t5, 408(ra)
[0x80001c54]:sw a6, 416(ra)
[0x80001c58]:lw t3, 1232(a5)
[0x80001c5c]:lw t4, 1236(a5)
[0x80001c60]:lw s10, 1240(a5)
[0x80001c64]:lw s11, 1244(a5)
[0x80001c68]:lui t3, 37547
[0x80001c6c]:addi t3, t3, 1679
[0x80001c70]:lui t4, 236300
[0x80001c74]:addi t4, t4, 2775
[0x80001c78]:lui s10, 220819
[0x80001c7c]:addi s10, s10, 1993
[0x80001c80]:lui s11, 262378
[0x80001c84]:addi s11, s11, 2719
[0x80001c88]:addi a4, zero, 64
[0x80001c8c]:csrrw zero, fcsr, a4
[0x80001c90]:fmul.d t5, t3, s10, dyn
[0x80001c94]:csrrs a6, fcsr, zero

[0x80001c90]:fmul.d t5, t3, s10, dyn
[0x80001c94]:csrrs a6, fcsr, zero
[0x80001c98]:sw t5, 424(ra)
[0x80001c9c]:sw t6, 432(ra)
[0x80001ca0]:sw t5, 440(ra)
[0x80001ca4]:sw a6, 448(ra)
[0x80001ca8]:lw t3, 1248(a5)
[0x80001cac]:lw t4, 1252(a5)
[0x80001cb0]:lw s10, 1256(a5)
[0x80001cb4]:lw s11, 1260(a5)
[0x80001cb8]:lui t3, 37547
[0x80001cbc]:addi t3, t3, 1679
[0x80001cc0]:lui t4, 236300
[0x80001cc4]:addi t4, t4, 2775
[0x80001cc8]:lui s10, 220819
[0x80001ccc]:addi s10, s10, 1993
[0x80001cd0]:lui s11, 262378
[0x80001cd4]:addi s11, s11, 2719
[0x80001cd8]:addi a4, zero, 96
[0x80001cdc]:csrrw zero, fcsr, a4
[0x80001ce0]:fmul.d t5, t3, s10, dyn
[0x80001ce4]:csrrs a6, fcsr, zero

[0x80001ce0]:fmul.d t5, t3, s10, dyn
[0x80001ce4]:csrrs a6, fcsr, zero
[0x80001ce8]:sw t5, 456(ra)
[0x80001cec]:sw t6, 464(ra)
[0x80001cf0]:sw t5, 472(ra)
[0x80001cf4]:sw a6, 480(ra)
[0x80001cf8]:lw t3, 1264(a5)
[0x80001cfc]:lw t4, 1268(a5)
[0x80001d00]:lw s10, 1272(a5)
[0x80001d04]:lw s11, 1276(a5)
[0x80001d08]:lui t3, 37547
[0x80001d0c]:addi t3, t3, 1679
[0x80001d10]:lui t4, 236300
[0x80001d14]:addi t4, t4, 2775
[0x80001d18]:lui s10, 220819
[0x80001d1c]:addi s10, s10, 1993
[0x80001d20]:lui s11, 262378
[0x80001d24]:addi s11, s11, 2719
[0x80001d28]:addi a4, zero, 128
[0x80001d2c]:csrrw zero, fcsr, a4
[0x80001d30]:fmul.d t5, t3, s10, dyn
[0x80001d34]:csrrs a6, fcsr, zero

[0x80001d30]:fmul.d t5, t3, s10, dyn
[0x80001d34]:csrrs a6, fcsr, zero
[0x80001d38]:sw t5, 488(ra)
[0x80001d3c]:sw t6, 496(ra)
[0x80001d40]:sw t5, 504(ra)
[0x80001d44]:sw a6, 512(ra)
[0x80001d48]:lw t3, 1280(a5)
[0x80001d4c]:lw t4, 1284(a5)
[0x80001d50]:lw s10, 1288(a5)
[0x80001d54]:lw s11, 1292(a5)
[0x80001d58]:lui t3, 47298
[0x80001d5c]:addi t3, t3, 2324
[0x80001d60]:lui t4, 233852
[0x80001d64]:addi t4, t4, 3141
[0x80001d68]:lui s10, 540185
[0x80001d6c]:addi s10, s10, 359
[0x80001d70]:lui s11, 263654
[0x80001d74]:addi s11, s11, 3025
[0x80001d78]:addi a4, zero, 0
[0x80001d7c]:csrrw zero, fcsr, a4
[0x80001d80]:fmul.d t5, t3, s10, dyn
[0x80001d84]:csrrs a6, fcsr, zero

[0x80001d80]:fmul.d t5, t3, s10, dyn
[0x80001d84]:csrrs a6, fcsr, zero
[0x80001d88]:sw t5, 520(ra)
[0x80001d8c]:sw t6, 528(ra)
[0x80001d90]:sw t5, 536(ra)
[0x80001d94]:sw a6, 544(ra)
[0x80001d98]:lw t3, 1296(a5)
[0x80001d9c]:lw t4, 1300(a5)
[0x80001da0]:lw s10, 1304(a5)
[0x80001da4]:lw s11, 1308(a5)
[0x80001da8]:lui t3, 47298
[0x80001dac]:addi t3, t3, 2324
[0x80001db0]:lui t4, 233852
[0x80001db4]:addi t4, t4, 3141
[0x80001db8]:lui s10, 540185
[0x80001dbc]:addi s10, s10, 359
[0x80001dc0]:lui s11, 263654
[0x80001dc4]:addi s11, s11, 3025
[0x80001dc8]:addi a4, zero, 32
[0x80001dcc]:csrrw zero, fcsr, a4
[0x80001dd0]:fmul.d t5, t3, s10, dyn
[0x80001dd4]:csrrs a6, fcsr, zero

[0x80001dd0]:fmul.d t5, t3, s10, dyn
[0x80001dd4]:csrrs a6, fcsr, zero
[0x80001dd8]:sw t5, 552(ra)
[0x80001ddc]:sw t6, 560(ra)
[0x80001de0]:sw t5, 568(ra)
[0x80001de4]:sw a6, 576(ra)
[0x80001de8]:lw t3, 1312(a5)
[0x80001dec]:lw t4, 1316(a5)
[0x80001df0]:lw s10, 1320(a5)
[0x80001df4]:lw s11, 1324(a5)
[0x80001df8]:lui t3, 47298
[0x80001dfc]:addi t3, t3, 2324
[0x80001e00]:lui t4, 233852
[0x80001e04]:addi t4, t4, 3141
[0x80001e08]:lui s10, 540185
[0x80001e0c]:addi s10, s10, 359
[0x80001e10]:lui s11, 263654
[0x80001e14]:addi s11, s11, 3025
[0x80001e18]:addi a4, zero, 64
[0x80001e1c]:csrrw zero, fcsr, a4
[0x80001e20]:fmul.d t5, t3, s10, dyn
[0x80001e24]:csrrs a6, fcsr, zero

[0x80001e20]:fmul.d t5, t3, s10, dyn
[0x80001e24]:csrrs a6, fcsr, zero
[0x80001e28]:sw t5, 584(ra)
[0x80001e2c]:sw t6, 592(ra)
[0x80001e30]:sw t5, 600(ra)
[0x80001e34]:sw a6, 608(ra)
[0x80001e38]:lw t3, 1328(a5)
[0x80001e3c]:lw t4, 1332(a5)
[0x80001e40]:lw s10, 1336(a5)
[0x80001e44]:lw s11, 1340(a5)
[0x80001e48]:lui t3, 47298
[0x80001e4c]:addi t3, t3, 2324
[0x80001e50]:lui t4, 233852
[0x80001e54]:addi t4, t4, 3141
[0x80001e58]:lui s10, 540185
[0x80001e5c]:addi s10, s10, 359
[0x80001e60]:lui s11, 263654
[0x80001e64]:addi s11, s11, 3025
[0x80001e68]:addi a4, zero, 96
[0x80001e6c]:csrrw zero, fcsr, a4
[0x80001e70]:fmul.d t5, t3, s10, dyn
[0x80001e74]:csrrs a6, fcsr, zero

[0x80001e70]:fmul.d t5, t3, s10, dyn
[0x80001e74]:csrrs a6, fcsr, zero
[0x80001e78]:sw t5, 616(ra)
[0x80001e7c]:sw t6, 624(ra)
[0x80001e80]:sw t5, 632(ra)
[0x80001e84]:sw a6, 640(ra)
[0x80001e88]:lw t3, 1344(a5)
[0x80001e8c]:lw t4, 1348(a5)
[0x80001e90]:lw s10, 1352(a5)
[0x80001e94]:lw s11, 1356(a5)
[0x80001e98]:lui t3, 47298
[0x80001e9c]:addi t3, t3, 2324
[0x80001ea0]:lui t4, 233852
[0x80001ea4]:addi t4, t4, 3141
[0x80001ea8]:lui s10, 540185
[0x80001eac]:addi s10, s10, 359
[0x80001eb0]:lui s11, 263654
[0x80001eb4]:addi s11, s11, 3025
[0x80001eb8]:addi a4, zero, 128
[0x80001ebc]:csrrw zero, fcsr, a4
[0x80001ec0]:fmul.d t5, t3, s10, dyn
[0x80001ec4]:csrrs a6, fcsr, zero

[0x80001ec0]:fmul.d t5, t3, s10, dyn
[0x80001ec4]:csrrs a6, fcsr, zero
[0x80001ec8]:sw t5, 648(ra)
[0x80001ecc]:sw t6, 656(ra)
[0x80001ed0]:sw t5, 664(ra)
[0x80001ed4]:sw a6, 672(ra)
[0x80001ed8]:lw t3, 1360(a5)
[0x80001edc]:lw t4, 1364(a5)
[0x80001ee0]:lw s10, 1368(a5)
[0x80001ee4]:lw s11, 1372(a5)
[0x80001ee8]:lui t3, 942047
[0x80001eec]:addi t3, t3, 2682
[0x80001ef0]:lui t4, 236314
[0x80001ef4]:addi t4, t4, 2265
[0x80001ef8]:lui s10, 611059
[0x80001efc]:addi s10, s10, 669
[0x80001f00]:lui s11, 260248
[0x80001f04]:addi s11, s11, 968
[0x80001f08]:addi a4, zero, 0
[0x80001f0c]:csrrw zero, fcsr, a4
[0x80001f10]:fmul.d t5, t3, s10, dyn
[0x80001f14]:csrrs a6, fcsr, zero

[0x80001f10]:fmul.d t5, t3, s10, dyn
[0x80001f14]:csrrs a6, fcsr, zero
[0x80001f18]:sw t5, 680(ra)
[0x80001f1c]:sw t6, 688(ra)
[0x80001f20]:sw t5, 696(ra)
[0x80001f24]:sw a6, 704(ra)
[0x80001f28]:lw t3, 1376(a5)
[0x80001f2c]:lw t4, 1380(a5)
[0x80001f30]:lw s10, 1384(a5)
[0x80001f34]:lw s11, 1388(a5)
[0x80001f38]:lui t3, 942047
[0x80001f3c]:addi t3, t3, 2682
[0x80001f40]:lui t4, 236314
[0x80001f44]:addi t4, t4, 2265
[0x80001f48]:lui s10, 611059
[0x80001f4c]:addi s10, s10, 669
[0x80001f50]:lui s11, 260248
[0x80001f54]:addi s11, s11, 968
[0x80001f58]:addi a4, zero, 32
[0x80001f5c]:csrrw zero, fcsr, a4
[0x80001f60]:fmul.d t5, t3, s10, dyn
[0x80001f64]:csrrs a6, fcsr, zero

[0x80001f60]:fmul.d t5, t3, s10, dyn
[0x80001f64]:csrrs a6, fcsr, zero
[0x80001f68]:sw t5, 712(ra)
[0x80001f6c]:sw t6, 720(ra)
[0x80001f70]:sw t5, 728(ra)
[0x80001f74]:sw a6, 736(ra)
[0x80001f78]:lw t3, 1392(a5)
[0x80001f7c]:lw t4, 1396(a5)
[0x80001f80]:lw s10, 1400(a5)
[0x80001f84]:lw s11, 1404(a5)
[0x80001f88]:lui t3, 942047
[0x80001f8c]:addi t3, t3, 2682
[0x80001f90]:lui t4, 236314
[0x80001f94]:addi t4, t4, 2265
[0x80001f98]:lui s10, 611059
[0x80001f9c]:addi s10, s10, 669
[0x80001fa0]:lui s11, 260248
[0x80001fa4]:addi s11, s11, 968
[0x80001fa8]:addi a4, zero, 64
[0x80001fac]:csrrw zero, fcsr, a4
[0x80001fb0]:fmul.d t5, t3, s10, dyn
[0x80001fb4]:csrrs a6, fcsr, zero

[0x80001fb0]:fmul.d t5, t3, s10, dyn
[0x80001fb4]:csrrs a6, fcsr, zero
[0x80001fb8]:sw t5, 744(ra)
[0x80001fbc]:sw t6, 752(ra)
[0x80001fc0]:sw t5, 760(ra)
[0x80001fc4]:sw a6, 768(ra)
[0x80001fc8]:lw t3, 1408(a5)
[0x80001fcc]:lw t4, 1412(a5)
[0x80001fd0]:lw s10, 1416(a5)
[0x80001fd4]:lw s11, 1420(a5)
[0x80001fd8]:lui t3, 942047
[0x80001fdc]:addi t3, t3, 2682
[0x80001fe0]:lui t4, 236314
[0x80001fe4]:addi t4, t4, 2265
[0x80001fe8]:lui s10, 611059
[0x80001fec]:addi s10, s10, 669
[0x80001ff0]:lui s11, 260248
[0x80001ff4]:addi s11, s11, 968
[0x80001ff8]:addi a4, zero, 96
[0x80001ffc]:csrrw zero, fcsr, a4
[0x80002000]:fmul.d t5, t3, s10, dyn
[0x80002004]:csrrs a6, fcsr, zero

[0x80002000]:fmul.d t5, t3, s10, dyn
[0x80002004]:csrrs a6, fcsr, zero
[0x80002008]:sw t5, 776(ra)
[0x8000200c]:sw t6, 784(ra)
[0x80002010]:sw t5, 792(ra)
[0x80002014]:sw a6, 800(ra)
[0x80002018]:lw t3, 1424(a5)
[0x8000201c]:lw t4, 1428(a5)
[0x80002020]:lw s10, 1432(a5)
[0x80002024]:lw s11, 1436(a5)
[0x80002028]:lui t3, 942047
[0x8000202c]:addi t3, t3, 2682
[0x80002030]:lui t4, 236314
[0x80002034]:addi t4, t4, 2265
[0x80002038]:lui s10, 611059
[0x8000203c]:addi s10, s10, 669
[0x80002040]:lui s11, 260248
[0x80002044]:addi s11, s11, 968
[0x80002048]:addi a4, zero, 128
[0x8000204c]:csrrw zero, fcsr, a4
[0x80002050]:fmul.d t5, t3, s10, dyn
[0x80002054]:csrrs a6, fcsr, zero

[0x80002050]:fmul.d t5, t3, s10, dyn
[0x80002054]:csrrs a6, fcsr, zero
[0x80002058]:sw t5, 808(ra)
[0x8000205c]:sw t6, 816(ra)
[0x80002060]:sw t5, 824(ra)
[0x80002064]:sw a6, 832(ra)
[0x80002068]:lw t3, 1440(a5)
[0x8000206c]:lw t4, 1444(a5)
[0x80002070]:lw s10, 1448(a5)
[0x80002074]:lw s11, 1452(a5)
[0x80002078]:lui t3, 1020538
[0x8000207c]:addi t3, t3, 72
[0x80002080]:lui t4, 235942
[0x80002084]:addi t4, t4, 570
[0x80002088]:lui s10, 974339
[0x8000208c]:addi s10, s10, 1117
[0x80002090]:lui s11, 259831
[0x80002094]:addi s11, s11, 1486
[0x80002098]:addi a4, zero, 0
[0x8000209c]:csrrw zero, fcsr, a4
[0x800020a0]:fmul.d t5, t3, s10, dyn
[0x800020a4]:csrrs a6, fcsr, zero

[0x800020a0]:fmul.d t5, t3, s10, dyn
[0x800020a4]:csrrs a6, fcsr, zero
[0x800020a8]:sw t5, 840(ra)
[0x800020ac]:sw t6, 848(ra)
[0x800020b0]:sw t5, 856(ra)
[0x800020b4]:sw a6, 864(ra)
[0x800020b8]:lw t3, 1456(a5)
[0x800020bc]:lw t4, 1460(a5)
[0x800020c0]:lw s10, 1464(a5)
[0x800020c4]:lw s11, 1468(a5)
[0x800020c8]:lui t3, 1020538
[0x800020cc]:addi t3, t3, 72
[0x800020d0]:lui t4, 235942
[0x800020d4]:addi t4, t4, 570
[0x800020d8]:lui s10, 974339
[0x800020dc]:addi s10, s10, 1117
[0x800020e0]:lui s11, 259831
[0x800020e4]:addi s11, s11, 1486
[0x800020e8]:addi a4, zero, 32
[0x800020ec]:csrrw zero, fcsr, a4
[0x800020f0]:fmul.d t5, t3, s10, dyn
[0x800020f4]:csrrs a6, fcsr, zero

[0x800020f0]:fmul.d t5, t3, s10, dyn
[0x800020f4]:csrrs a6, fcsr, zero
[0x800020f8]:sw t5, 872(ra)
[0x800020fc]:sw t6, 880(ra)
[0x80002100]:sw t5, 888(ra)
[0x80002104]:sw a6, 896(ra)
[0x80002108]:lw t3, 1472(a5)
[0x8000210c]:lw t4, 1476(a5)
[0x80002110]:lw s10, 1480(a5)
[0x80002114]:lw s11, 1484(a5)
[0x80002118]:lui t3, 1020538
[0x8000211c]:addi t3, t3, 72
[0x80002120]:lui t4, 235942
[0x80002124]:addi t4, t4, 570
[0x80002128]:lui s10, 974339
[0x8000212c]:addi s10, s10, 1117
[0x80002130]:lui s11, 259831
[0x80002134]:addi s11, s11, 1486
[0x80002138]:addi a4, zero, 64
[0x8000213c]:csrrw zero, fcsr, a4
[0x80002140]:fmul.d t5, t3, s10, dyn
[0x80002144]:csrrs a6, fcsr, zero

[0x80002140]:fmul.d t5, t3, s10, dyn
[0x80002144]:csrrs a6, fcsr, zero
[0x80002148]:sw t5, 904(ra)
[0x8000214c]:sw t6, 912(ra)
[0x80002150]:sw t5, 920(ra)
[0x80002154]:sw a6, 928(ra)
[0x80002158]:lw t3, 1488(a5)
[0x8000215c]:lw t4, 1492(a5)
[0x80002160]:lw s10, 1496(a5)
[0x80002164]:lw s11, 1500(a5)
[0x80002168]:lui t3, 1020538
[0x8000216c]:addi t3, t3, 72
[0x80002170]:lui t4, 235942
[0x80002174]:addi t4, t4, 570
[0x80002178]:lui s10, 974339
[0x8000217c]:addi s10, s10, 1117
[0x80002180]:lui s11, 259831
[0x80002184]:addi s11, s11, 1486
[0x80002188]:addi a4, zero, 96
[0x8000218c]:csrrw zero, fcsr, a4
[0x80002190]:fmul.d t5, t3, s10, dyn
[0x80002194]:csrrs a6, fcsr, zero

[0x80002190]:fmul.d t5, t3, s10, dyn
[0x80002194]:csrrs a6, fcsr, zero
[0x80002198]:sw t5, 936(ra)
[0x8000219c]:sw t6, 944(ra)
[0x800021a0]:sw t5, 952(ra)
[0x800021a4]:sw a6, 960(ra)
[0x800021a8]:lw t3, 1504(a5)
[0x800021ac]:lw t4, 1508(a5)
[0x800021b0]:lw s10, 1512(a5)
[0x800021b4]:lw s11, 1516(a5)
[0x800021b8]:lui t3, 1020538
[0x800021bc]:addi t3, t3, 72
[0x800021c0]:lui t4, 235942
[0x800021c4]:addi t4, t4, 570
[0x800021c8]:lui s10, 974339
[0x800021cc]:addi s10, s10, 1117
[0x800021d0]:lui s11, 259831
[0x800021d4]:addi s11, s11, 1486
[0x800021d8]:addi a4, zero, 128
[0x800021dc]:csrrw zero, fcsr, a4
[0x800021e0]:fmul.d t5, t3, s10, dyn
[0x800021e4]:csrrs a6, fcsr, zero

[0x800021e0]:fmul.d t5, t3, s10, dyn
[0x800021e4]:csrrs a6, fcsr, zero
[0x800021e8]:sw t5, 968(ra)
[0x800021ec]:sw t6, 976(ra)
[0x800021f0]:sw t5, 984(ra)
[0x800021f4]:sw a6, 992(ra)
[0x800021f8]:lw t3, 1520(a5)
[0x800021fc]:lw t4, 1524(a5)
[0x80002200]:lw s10, 1528(a5)
[0x80002204]:lw s11, 1532(a5)
[0x80002208]:lui t3, 856462
[0x8000220c]:addi t3, t3, 2350
[0x80002210]:lui t4, 235760
[0x80002214]:addi t4, t4, 157
[0x80002218]:lui s10, 1003929
[0x8000221c]:addi s10, s10, 1167
[0x80002220]:lui s11, 270088
[0x80002224]:addi s11, s11, 972
[0x80002228]:addi a4, zero, 0
[0x8000222c]:csrrw zero, fcsr, a4
[0x80002230]:fmul.d t5, t3, s10, dyn
[0x80002234]:csrrs a6, fcsr, zero

[0x80002230]:fmul.d t5, t3, s10, dyn
[0x80002234]:csrrs a6, fcsr, zero
[0x80002238]:sw t5, 1000(ra)
[0x8000223c]:sw t6, 1008(ra)
[0x80002240]:sw t5, 1016(ra)
[0x80002244]:sw a6, 1024(ra)
[0x80002248]:lw t3, 1536(a5)
[0x8000224c]:lw t4, 1540(a5)
[0x80002250]:lw s10, 1544(a5)
[0x80002254]:lw s11, 1548(a5)
[0x80002258]:lui t3, 856462
[0x8000225c]:addi t3, t3, 2350
[0x80002260]:lui t4, 235760
[0x80002264]:addi t4, t4, 157
[0x80002268]:lui s10, 1003929
[0x8000226c]:addi s10, s10, 1167
[0x80002270]:lui s11, 270088
[0x80002274]:addi s11, s11, 972
[0x80002278]:addi a4, zero, 32
[0x8000227c]:csrrw zero, fcsr, a4
[0x80002280]:fmul.d t5, t3, s10, dyn
[0x80002284]:csrrs a6, fcsr, zero

[0x80002280]:fmul.d t5, t3, s10, dyn
[0x80002284]:csrrs a6, fcsr, zero
[0x80002288]:sw t5, 1032(ra)
[0x8000228c]:sw t6, 1040(ra)
[0x80002290]:sw t5, 1048(ra)
[0x80002294]:sw a6, 1056(ra)
[0x80002298]:lw t3, 1552(a5)
[0x8000229c]:lw t4, 1556(a5)
[0x800022a0]:lw s10, 1560(a5)
[0x800022a4]:lw s11, 1564(a5)
[0x800022a8]:lui t3, 856462
[0x800022ac]:addi t3, t3, 2350
[0x800022b0]:lui t4, 235760
[0x800022b4]:addi t4, t4, 157
[0x800022b8]:lui s10, 1003929
[0x800022bc]:addi s10, s10, 1167
[0x800022c0]:lui s11, 270088
[0x800022c4]:addi s11, s11, 972
[0x800022c8]:addi a4, zero, 64
[0x800022cc]:csrrw zero, fcsr, a4
[0x800022d0]:fmul.d t5, t3, s10, dyn
[0x800022d4]:csrrs a6, fcsr, zero

[0x800022d0]:fmul.d t5, t3, s10, dyn
[0x800022d4]:csrrs a6, fcsr, zero
[0x800022d8]:sw t5, 1064(ra)
[0x800022dc]:sw t6, 1072(ra)
[0x800022e0]:sw t5, 1080(ra)
[0x800022e4]:sw a6, 1088(ra)
[0x800022e8]:lw t3, 1568(a5)
[0x800022ec]:lw t4, 1572(a5)
[0x800022f0]:lw s10, 1576(a5)
[0x800022f4]:lw s11, 1580(a5)
[0x800022f8]:lui t3, 856462
[0x800022fc]:addi t3, t3, 2350
[0x80002300]:lui t4, 235760
[0x80002304]:addi t4, t4, 157
[0x80002308]:lui s10, 1003929
[0x8000230c]:addi s10, s10, 1167
[0x80002310]:lui s11, 270088
[0x80002314]:addi s11, s11, 972
[0x80002318]:addi a4, zero, 96
[0x8000231c]:csrrw zero, fcsr, a4
[0x80002320]:fmul.d t5, t3, s10, dyn
[0x80002324]:csrrs a6, fcsr, zero

[0x80002320]:fmul.d t5, t3, s10, dyn
[0x80002324]:csrrs a6, fcsr, zero
[0x80002328]:sw t5, 1096(ra)
[0x8000232c]:sw t6, 1104(ra)
[0x80002330]:sw t5, 1112(ra)
[0x80002334]:sw a6, 1120(ra)
[0x80002338]:lw t3, 1584(a5)
[0x8000233c]:lw t4, 1588(a5)
[0x80002340]:lw s10, 1592(a5)
[0x80002344]:lw s11, 1596(a5)
[0x80002348]:lui t3, 856462
[0x8000234c]:addi t3, t3, 2350
[0x80002350]:lui t4, 235760
[0x80002354]:addi t4, t4, 157
[0x80002358]:lui s10, 1003929
[0x8000235c]:addi s10, s10, 1167
[0x80002360]:lui s11, 270088
[0x80002364]:addi s11, s11, 972
[0x80002368]:addi a4, zero, 128
[0x8000236c]:csrrw zero, fcsr, a4
[0x80002370]:fmul.d t5, t3, s10, dyn
[0x80002374]:csrrs a6, fcsr, zero

[0x80002370]:fmul.d t5, t3, s10, dyn
[0x80002374]:csrrs a6, fcsr, zero
[0x80002378]:sw t5, 1128(ra)
[0x8000237c]:sw t6, 1136(ra)
[0x80002380]:sw t5, 1144(ra)
[0x80002384]:sw a6, 1152(ra)
[0x80002388]:lw t3, 1600(a5)
[0x8000238c]:lw t4, 1604(a5)
[0x80002390]:lw s10, 1608(a5)
[0x80002394]:lw s11, 1612(a5)
[0x80002398]:lui t3, 933466
[0x8000239c]:addi t3, t3, 2865
[0x800023a0]:lui t4, 235830
[0x800023a4]:addi t4, t4, 3035
[0x800023a8]:lui s10, 793625
[0x800023ac]:addi s10, s10, 1745
[0x800023b0]:lui s11, 266407
[0x800023b4]:addi s11, s11, 680
[0x800023b8]:addi a4, zero, 0
[0x800023bc]:csrrw zero, fcsr, a4
[0x800023c0]:fmul.d t5, t3, s10, dyn
[0x800023c4]:csrrs a6, fcsr, zero

[0x800023c0]:fmul.d t5, t3, s10, dyn
[0x800023c4]:csrrs a6, fcsr, zero
[0x800023c8]:sw t5, 1160(ra)
[0x800023cc]:sw t6, 1168(ra)
[0x800023d0]:sw t5, 1176(ra)
[0x800023d4]:sw a6, 1184(ra)
[0x800023d8]:lw t3, 1616(a5)
[0x800023dc]:lw t4, 1620(a5)
[0x800023e0]:lw s10, 1624(a5)
[0x800023e4]:lw s11, 1628(a5)
[0x800023e8]:lui t3, 933466
[0x800023ec]:addi t3, t3, 2865
[0x800023f0]:lui t4, 235830
[0x800023f4]:addi t4, t4, 3035
[0x800023f8]:lui s10, 793625
[0x800023fc]:addi s10, s10, 1745
[0x80002400]:lui s11, 266407
[0x80002404]:addi s11, s11, 680
[0x80002408]:addi a4, zero, 32
[0x8000240c]:csrrw zero, fcsr, a4
[0x80002410]:fmul.d t5, t3, s10, dyn
[0x80002414]:csrrs a6, fcsr, zero

[0x80002410]:fmul.d t5, t3, s10, dyn
[0x80002414]:csrrs a6, fcsr, zero
[0x80002418]:sw t5, 1192(ra)
[0x8000241c]:sw t6, 1200(ra)
[0x80002420]:sw t5, 1208(ra)
[0x80002424]:sw a6, 1216(ra)
[0x80002428]:lw t3, 1632(a5)
[0x8000242c]:lw t4, 1636(a5)
[0x80002430]:lw s10, 1640(a5)
[0x80002434]:lw s11, 1644(a5)
[0x80002438]:lui t3, 933466
[0x8000243c]:addi t3, t3, 2865
[0x80002440]:lui t4, 235830
[0x80002444]:addi t4, t4, 3035
[0x80002448]:lui s10, 793625
[0x8000244c]:addi s10, s10, 1745
[0x80002450]:lui s11, 266407
[0x80002454]:addi s11, s11, 680
[0x80002458]:addi a4, zero, 64
[0x8000245c]:csrrw zero, fcsr, a4
[0x80002460]:fmul.d t5, t3, s10, dyn
[0x80002464]:csrrs a6, fcsr, zero

[0x80002460]:fmul.d t5, t3, s10, dyn
[0x80002464]:csrrs a6, fcsr, zero
[0x80002468]:sw t5, 1224(ra)
[0x8000246c]:sw t6, 1232(ra)
[0x80002470]:sw t5, 1240(ra)
[0x80002474]:sw a6, 1248(ra)
[0x80002478]:lw t3, 1648(a5)
[0x8000247c]:lw t4, 1652(a5)
[0x80002480]:lw s10, 1656(a5)
[0x80002484]:lw s11, 1660(a5)
[0x80002488]:lui t3, 933466
[0x8000248c]:addi t3, t3, 2865
[0x80002490]:lui t4, 235830
[0x80002494]:addi t4, t4, 3035
[0x80002498]:lui s10, 793625
[0x8000249c]:addi s10, s10, 1745
[0x800024a0]:lui s11, 266407
[0x800024a4]:addi s11, s11, 680
[0x800024a8]:addi a4, zero, 96
[0x800024ac]:csrrw zero, fcsr, a4
[0x800024b0]:fmul.d t5, t3, s10, dyn
[0x800024b4]:csrrs a6, fcsr, zero

[0x800024b0]:fmul.d t5, t3, s10, dyn
[0x800024b4]:csrrs a6, fcsr, zero
[0x800024b8]:sw t5, 1256(ra)
[0x800024bc]:sw t6, 1264(ra)
[0x800024c0]:sw t5, 1272(ra)
[0x800024c4]:sw a6, 1280(ra)
[0x800024c8]:lw t3, 1664(a5)
[0x800024cc]:lw t4, 1668(a5)
[0x800024d0]:lw s10, 1672(a5)
[0x800024d4]:lw s11, 1676(a5)
[0x800024d8]:lui t3, 933466
[0x800024dc]:addi t3, t3, 2865
[0x800024e0]:lui t4, 235830
[0x800024e4]:addi t4, t4, 3035
[0x800024e8]:lui s10, 793625
[0x800024ec]:addi s10, s10, 1745
[0x800024f0]:lui s11, 266407
[0x800024f4]:addi s11, s11, 680
[0x800024f8]:addi a4, zero, 128
[0x800024fc]:csrrw zero, fcsr, a4
[0x80002500]:fmul.d t5, t3, s10, dyn
[0x80002504]:csrrs a6, fcsr, zero

[0x80002500]:fmul.d t5, t3, s10, dyn
[0x80002504]:csrrs a6, fcsr, zero
[0x80002508]:sw t5, 1288(ra)
[0x8000250c]:sw t6, 1296(ra)
[0x80002510]:sw t5, 1304(ra)
[0x80002514]:sw a6, 1312(ra)
[0x80002518]:lw t3, 1680(a5)
[0x8000251c]:lw t4, 1684(a5)
[0x80002520]:lw s10, 1688(a5)
[0x80002524]:lw s11, 1692(a5)
[0x80002528]:lui t3, 667111
[0x8000252c]:addi t3, t3, 3440
[0x80002530]:lui t4, 236293
[0x80002534]:addi t4, t4, 2648
[0x80002538]:lui s10, 254835
[0x8000253c]:addi s10, s10, 2947
[0x80002540]:lui s11, 263865
[0x80002544]:addi s11, s11, 4019
[0x80002548]:addi a4, zero, 0
[0x8000254c]:csrrw zero, fcsr, a4
[0x80002550]:fmul.d t5, t3, s10, dyn
[0x80002554]:csrrs a6, fcsr, zero

[0x80002550]:fmul.d t5, t3, s10, dyn
[0x80002554]:csrrs a6, fcsr, zero
[0x80002558]:sw t5, 1320(ra)
[0x8000255c]:sw t6, 1328(ra)
[0x80002560]:sw t5, 1336(ra)
[0x80002564]:sw a6, 1344(ra)
[0x80002568]:lw t3, 1696(a5)
[0x8000256c]:lw t4, 1700(a5)
[0x80002570]:lw s10, 1704(a5)
[0x80002574]:lw s11, 1708(a5)
[0x80002578]:lui t3, 667111
[0x8000257c]:addi t3, t3, 3440
[0x80002580]:lui t4, 236293
[0x80002584]:addi t4, t4, 2648
[0x80002588]:lui s10, 254835
[0x8000258c]:addi s10, s10, 2947
[0x80002590]:lui s11, 263865
[0x80002594]:addi s11, s11, 4019
[0x80002598]:addi a4, zero, 32
[0x8000259c]:csrrw zero, fcsr, a4
[0x800025a0]:fmul.d t5, t3, s10, dyn
[0x800025a4]:csrrs a6, fcsr, zero

[0x800025a0]:fmul.d t5, t3, s10, dyn
[0x800025a4]:csrrs a6, fcsr, zero
[0x800025a8]:sw t5, 1352(ra)
[0x800025ac]:sw t6, 1360(ra)
[0x800025b0]:sw t5, 1368(ra)
[0x800025b4]:sw a6, 1376(ra)
[0x800025b8]:lw t3, 1712(a5)
[0x800025bc]:lw t4, 1716(a5)
[0x800025c0]:lw s10, 1720(a5)
[0x800025c4]:lw s11, 1724(a5)
[0x800025c8]:lui t3, 667111
[0x800025cc]:addi t3, t3, 3440
[0x800025d0]:lui t4, 236293
[0x800025d4]:addi t4, t4, 2648
[0x800025d8]:lui s10, 254835
[0x800025dc]:addi s10, s10, 2947
[0x800025e0]:lui s11, 263865
[0x800025e4]:addi s11, s11, 4019
[0x800025e8]:addi a4, zero, 64
[0x800025ec]:csrrw zero, fcsr, a4
[0x800025f0]:fmul.d t5, t3, s10, dyn
[0x800025f4]:csrrs a6, fcsr, zero

[0x800025f0]:fmul.d t5, t3, s10, dyn
[0x800025f4]:csrrs a6, fcsr, zero
[0x800025f8]:sw t5, 1384(ra)
[0x800025fc]:sw t6, 1392(ra)
[0x80002600]:sw t5, 1400(ra)
[0x80002604]:sw a6, 1408(ra)
[0x80002608]:lw t3, 1728(a5)
[0x8000260c]:lw t4, 1732(a5)
[0x80002610]:lw s10, 1736(a5)
[0x80002614]:lw s11, 1740(a5)
[0x80002618]:lui t3, 667111
[0x8000261c]:addi t3, t3, 3440
[0x80002620]:lui t4, 236293
[0x80002624]:addi t4, t4, 2648
[0x80002628]:lui s10, 254835
[0x8000262c]:addi s10, s10, 2947
[0x80002630]:lui s11, 263865
[0x80002634]:addi s11, s11, 4019
[0x80002638]:addi a4, zero, 96
[0x8000263c]:csrrw zero, fcsr, a4
[0x80002640]:fmul.d t5, t3, s10, dyn
[0x80002644]:csrrs a6, fcsr, zero

[0x80002640]:fmul.d t5, t3, s10, dyn
[0x80002644]:csrrs a6, fcsr, zero
[0x80002648]:sw t5, 1416(ra)
[0x8000264c]:sw t6, 1424(ra)
[0x80002650]:sw t5, 1432(ra)
[0x80002654]:sw a6, 1440(ra)
[0x80002658]:lw t3, 1744(a5)
[0x8000265c]:lw t4, 1748(a5)
[0x80002660]:lw s10, 1752(a5)
[0x80002664]:lw s11, 1756(a5)
[0x80002668]:lui t3, 667111
[0x8000266c]:addi t3, t3, 3440
[0x80002670]:lui t4, 236293
[0x80002674]:addi t4, t4, 2648
[0x80002678]:lui s10, 254835
[0x8000267c]:addi s10, s10, 2947
[0x80002680]:lui s11, 263865
[0x80002684]:addi s11, s11, 4019
[0x80002688]:addi a4, zero, 128
[0x8000268c]:csrrw zero, fcsr, a4
[0x80002690]:fmul.d t5, t3, s10, dyn
[0x80002694]:csrrs a6, fcsr, zero

[0x80002690]:fmul.d t5, t3, s10, dyn
[0x80002694]:csrrs a6, fcsr, zero
[0x80002698]:sw t5, 1448(ra)
[0x8000269c]:sw t6, 1456(ra)
[0x800026a0]:sw t5, 1464(ra)
[0x800026a4]:sw a6, 1472(ra)
[0x800026a8]:lw t3, 1760(a5)
[0x800026ac]:lw t4, 1764(a5)
[0x800026b0]:lw s10, 1768(a5)
[0x800026b4]:lw s11, 1772(a5)
[0x800026b8]:lui t3, 363459
[0x800026bc]:addi t3, t3, 1310
[0x800026c0]:lui t4, 235426
[0x800026c4]:addi t4, t4, 2861
[0x800026c8]:lui s10, 37316
[0x800026cc]:addi s10, s10, 2733
[0x800026d0]:lui s11, 263226
[0x800026d4]:addi s11, s11, 3259
[0x800026d8]:addi a4, zero, 0
[0x800026dc]:csrrw zero, fcsr, a4
[0x800026e0]:fmul.d t5, t3, s10, dyn
[0x800026e4]:csrrs a6, fcsr, zero

[0x800026e0]:fmul.d t5, t3, s10, dyn
[0x800026e4]:csrrs a6, fcsr, zero
[0x800026e8]:sw t5, 1480(ra)
[0x800026ec]:sw t6, 1488(ra)
[0x800026f0]:sw t5, 1496(ra)
[0x800026f4]:sw a6, 1504(ra)
[0x800026f8]:lw t3, 1776(a5)
[0x800026fc]:lw t4, 1780(a5)
[0x80002700]:lw s10, 1784(a5)
[0x80002704]:lw s11, 1788(a5)
[0x80002708]:lui t3, 363459
[0x8000270c]:addi t3, t3, 1310
[0x80002710]:lui t4, 235426
[0x80002714]:addi t4, t4, 2861
[0x80002718]:lui s10, 37316
[0x8000271c]:addi s10, s10, 2733
[0x80002720]:lui s11, 263226
[0x80002724]:addi s11, s11, 3259
[0x80002728]:addi a4, zero, 32
[0x8000272c]:csrrw zero, fcsr, a4
[0x80002730]:fmul.d t5, t3, s10, dyn
[0x80002734]:csrrs a6, fcsr, zero

[0x80002730]:fmul.d t5, t3, s10, dyn
[0x80002734]:csrrs a6, fcsr, zero
[0x80002738]:sw t5, 1512(ra)
[0x8000273c]:sw t6, 1520(ra)
[0x80002740]:sw t5, 1528(ra)
[0x80002744]:sw a6, 1536(ra)
[0x80002748]:lw t3, 1792(a5)
[0x8000274c]:lw t4, 1796(a5)
[0x80002750]:lw s10, 1800(a5)
[0x80002754]:lw s11, 1804(a5)
[0x80002758]:lui t3, 363459
[0x8000275c]:addi t3, t3, 1310
[0x80002760]:lui t4, 235426
[0x80002764]:addi t4, t4, 2861
[0x80002768]:lui s10, 37316
[0x8000276c]:addi s10, s10, 2733
[0x80002770]:lui s11, 263226
[0x80002774]:addi s11, s11, 3259
[0x80002778]:addi a4, zero, 64
[0x8000277c]:csrrw zero, fcsr, a4
[0x80002780]:fmul.d t5, t3, s10, dyn
[0x80002784]:csrrs a6, fcsr, zero

[0x80002780]:fmul.d t5, t3, s10, dyn
[0x80002784]:csrrs a6, fcsr, zero
[0x80002788]:sw t5, 1544(ra)
[0x8000278c]:sw t6, 1552(ra)
[0x80002790]:sw t5, 1560(ra)
[0x80002794]:sw a6, 1568(ra)
[0x80002798]:lw t3, 1808(a5)
[0x8000279c]:lw t4, 1812(a5)
[0x800027a0]:lw s10, 1816(a5)
[0x800027a4]:lw s11, 1820(a5)
[0x800027a8]:lui t3, 363459
[0x800027ac]:addi t3, t3, 1310
[0x800027b0]:lui t4, 235426
[0x800027b4]:addi t4, t4, 2861
[0x800027b8]:lui s10, 37316
[0x800027bc]:addi s10, s10, 2733
[0x800027c0]:lui s11, 263226
[0x800027c4]:addi s11, s11, 3259
[0x800027c8]:addi a4, zero, 96
[0x800027cc]:csrrw zero, fcsr, a4
[0x800027d0]:fmul.d t5, t3, s10, dyn
[0x800027d4]:csrrs a6, fcsr, zero

[0x800027d0]:fmul.d t5, t3, s10, dyn
[0x800027d4]:csrrs a6, fcsr, zero
[0x800027d8]:sw t5, 1576(ra)
[0x800027dc]:sw t6, 1584(ra)
[0x800027e0]:sw t5, 1592(ra)
[0x800027e4]:sw a6, 1600(ra)
[0x800027e8]:lw t3, 1824(a5)
[0x800027ec]:lw t4, 1828(a5)
[0x800027f0]:lw s10, 1832(a5)
[0x800027f4]:lw s11, 1836(a5)
[0x800027f8]:lui t3, 363459
[0x800027fc]:addi t3, t3, 1310
[0x80002800]:lui t4, 235426
[0x80002804]:addi t4, t4, 2861
[0x80002808]:lui s10, 37316
[0x8000280c]:addi s10, s10, 2733
[0x80002810]:lui s11, 263226
[0x80002814]:addi s11, s11, 3259
[0x80002818]:addi a4, zero, 128
[0x8000281c]:csrrw zero, fcsr, a4
[0x80002820]:fmul.d t5, t3, s10, dyn
[0x80002824]:csrrs a6, fcsr, zero

[0x80002820]:fmul.d t5, t3, s10, dyn
[0x80002824]:csrrs a6, fcsr, zero
[0x80002828]:sw t5, 1608(ra)
[0x8000282c]:sw t6, 1616(ra)
[0x80002830]:sw t5, 1624(ra)
[0x80002834]:sw a6, 1632(ra)
[0x80002838]:lw t3, 1840(a5)
[0x8000283c]:lw t4, 1844(a5)
[0x80002840]:lw s10, 1848(a5)
[0x80002844]:lw s11, 1852(a5)
[0x80002848]:lui t3, 885492
[0x8000284c]:addi t3, t3, 2688
[0x80002850]:lui t4, 236106
[0x80002854]:addi t4, t4, 2753
[0x80002858]:lui s10, 1044603
[0x8000285c]:addi s10, s10, 1801
[0x80002860]:lui s11, 261400
[0x80002864]:addi s11, s11, 3169
[0x80002868]:addi a4, zero, 0
[0x8000286c]:csrrw zero, fcsr, a4
[0x80002870]:fmul.d t5, t3, s10, dyn
[0x80002874]:csrrs a6, fcsr, zero

[0x80002870]:fmul.d t5, t3, s10, dyn
[0x80002874]:csrrs a6, fcsr, zero
[0x80002878]:sw t5, 1640(ra)
[0x8000287c]:sw t6, 1648(ra)
[0x80002880]:sw t5, 1656(ra)
[0x80002884]:sw a6, 1664(ra)
[0x80002888]:lw t3, 1856(a5)
[0x8000288c]:lw t4, 1860(a5)
[0x80002890]:lw s10, 1864(a5)
[0x80002894]:lw s11, 1868(a5)
[0x80002898]:lui t3, 885492
[0x8000289c]:addi t3, t3, 2688
[0x800028a0]:lui t4, 236106
[0x800028a4]:addi t4, t4, 2753
[0x800028a8]:lui s10, 1044603
[0x800028ac]:addi s10, s10, 1801
[0x800028b0]:lui s11, 261400
[0x800028b4]:addi s11, s11, 3169
[0x800028b8]:addi a4, zero, 32
[0x800028bc]:csrrw zero, fcsr, a4
[0x800028c0]:fmul.d t5, t3, s10, dyn
[0x800028c4]:csrrs a6, fcsr, zero

[0x800028c0]:fmul.d t5, t3, s10, dyn
[0x800028c4]:csrrs a6, fcsr, zero
[0x800028c8]:sw t5, 1672(ra)
[0x800028cc]:sw t6, 1680(ra)
[0x800028d0]:sw t5, 1688(ra)
[0x800028d4]:sw a6, 1696(ra)
[0x800028d8]:lw t3, 1872(a5)
[0x800028dc]:lw t4, 1876(a5)
[0x800028e0]:lw s10, 1880(a5)
[0x800028e4]:lw s11, 1884(a5)
[0x800028e8]:lui t3, 885492
[0x800028ec]:addi t3, t3, 2688
[0x800028f0]:lui t4, 236106
[0x800028f4]:addi t4, t4, 2753
[0x800028f8]:lui s10, 1044603
[0x800028fc]:addi s10, s10, 1801
[0x80002900]:lui s11, 261400
[0x80002904]:addi s11, s11, 3169
[0x80002908]:addi a4, zero, 64
[0x8000290c]:csrrw zero, fcsr, a4
[0x80002910]:fmul.d t5, t3, s10, dyn
[0x80002914]:csrrs a6, fcsr, zero

[0x80002910]:fmul.d t5, t3, s10, dyn
[0x80002914]:csrrs a6, fcsr, zero
[0x80002918]:sw t5, 1704(ra)
[0x8000291c]:sw t6, 1712(ra)
[0x80002920]:sw t5, 1720(ra)
[0x80002924]:sw a6, 1728(ra)
[0x80002928]:lw t3, 1888(a5)
[0x8000292c]:lw t4, 1892(a5)
[0x80002930]:lw s10, 1896(a5)
[0x80002934]:lw s11, 1900(a5)
[0x80002938]:lui t3, 885492
[0x8000293c]:addi t3, t3, 2688
[0x80002940]:lui t4, 236106
[0x80002944]:addi t4, t4, 2753
[0x80002948]:lui s10, 1044603
[0x8000294c]:addi s10, s10, 1801
[0x80002950]:lui s11, 261400
[0x80002954]:addi s11, s11, 3169
[0x80002958]:addi a4, zero, 96
[0x8000295c]:csrrw zero, fcsr, a4
[0x80002960]:fmul.d t5, t3, s10, dyn
[0x80002964]:csrrs a6, fcsr, zero

[0x80002960]:fmul.d t5, t3, s10, dyn
[0x80002964]:csrrs a6, fcsr, zero
[0x80002968]:sw t5, 1736(ra)
[0x8000296c]:sw t6, 1744(ra)
[0x80002970]:sw t5, 1752(ra)
[0x80002974]:sw a6, 1760(ra)
[0x80002978]:lw t3, 1904(a5)
[0x8000297c]:lw t4, 1908(a5)
[0x80002980]:lw s10, 1912(a5)
[0x80002984]:lw s11, 1916(a5)
[0x80002988]:lui t3, 885492
[0x8000298c]:addi t3, t3, 2688
[0x80002990]:lui t4, 236106
[0x80002994]:addi t4, t4, 2753
[0x80002998]:lui s10, 1044603
[0x8000299c]:addi s10, s10, 1801
[0x800029a0]:lui s11, 261400
[0x800029a4]:addi s11, s11, 3169
[0x800029a8]:addi a4, zero, 128
[0x800029ac]:csrrw zero, fcsr, a4
[0x800029b0]:fmul.d t5, t3, s10, dyn
[0x800029b4]:csrrs a6, fcsr, zero

[0x800029b0]:fmul.d t5, t3, s10, dyn
[0x800029b4]:csrrs a6, fcsr, zero
[0x800029b8]:sw t5, 1768(ra)
[0x800029bc]:sw t6, 1776(ra)
[0x800029c0]:sw t5, 1784(ra)
[0x800029c4]:sw a6, 1792(ra)
[0x800029c8]:lw t3, 1920(a5)
[0x800029cc]:lw t4, 1924(a5)
[0x800029d0]:lw s10, 1928(a5)
[0x800029d4]:lw s11, 1932(a5)
[0x800029d8]:lui t3, 981229
[0x800029dc]:addi t3, t3, 313
[0x800029e0]:lui t4, 235547
[0x800029e4]:addi t4, t4, 533
[0x800029e8]:lui s10, 67669
[0x800029ec]:addi s10, s10, 2529
[0x800029f0]:lui s11, 261014
[0x800029f4]:addi s11, s11, 3942
[0x800029f8]:addi a4, zero, 0
[0x800029fc]:csrrw zero, fcsr, a4
[0x80002a00]:fmul.d t5, t3, s10, dyn
[0x80002a04]:csrrs a6, fcsr, zero

[0x80002a00]:fmul.d t5, t3, s10, dyn
[0x80002a04]:csrrs a6, fcsr, zero
[0x80002a08]:sw t5, 1800(ra)
[0x80002a0c]:sw t6, 1808(ra)
[0x80002a10]:sw t5, 1816(ra)
[0x80002a14]:sw a6, 1824(ra)
[0x80002a18]:lw t3, 1936(a5)
[0x80002a1c]:lw t4, 1940(a5)
[0x80002a20]:lw s10, 1944(a5)
[0x80002a24]:lw s11, 1948(a5)
[0x80002a28]:lui t3, 981229
[0x80002a2c]:addi t3, t3, 313
[0x80002a30]:lui t4, 235547
[0x80002a34]:addi t4, t4, 533
[0x80002a38]:lui s10, 67669
[0x80002a3c]:addi s10, s10, 2529
[0x80002a40]:lui s11, 261014
[0x80002a44]:addi s11, s11, 3942
[0x80002a48]:addi a4, zero, 32
[0x80002a4c]:csrrw zero, fcsr, a4
[0x80002a50]:fmul.d t5, t3, s10, dyn
[0x80002a54]:csrrs a6, fcsr, zero

[0x80002a50]:fmul.d t5, t3, s10, dyn
[0x80002a54]:csrrs a6, fcsr, zero
[0x80002a58]:sw t5, 1832(ra)
[0x80002a5c]:sw t6, 1840(ra)
[0x80002a60]:sw t5, 1848(ra)
[0x80002a64]:sw a6, 1856(ra)
[0x80002a68]:lw t3, 1952(a5)
[0x80002a6c]:lw t4, 1956(a5)
[0x80002a70]:lw s10, 1960(a5)
[0x80002a74]:lw s11, 1964(a5)
[0x80002a78]:lui t3, 981229
[0x80002a7c]:addi t3, t3, 313
[0x80002a80]:lui t4, 235547
[0x80002a84]:addi t4, t4, 533
[0x80002a88]:lui s10, 67669
[0x80002a8c]:addi s10, s10, 2529
[0x80002a90]:lui s11, 261014
[0x80002a94]:addi s11, s11, 3942
[0x80002a98]:addi a4, zero, 64
[0x80002a9c]:csrrw zero, fcsr, a4
[0x80002aa0]:fmul.d t5, t3, s10, dyn
[0x80002aa4]:csrrs a6, fcsr, zero

[0x80002aa0]:fmul.d t5, t3, s10, dyn
[0x80002aa4]:csrrs a6, fcsr, zero
[0x80002aa8]:sw t5, 1864(ra)
[0x80002aac]:sw t6, 1872(ra)
[0x80002ab0]:sw t5, 1880(ra)
[0x80002ab4]:sw a6, 1888(ra)
[0x80002ab8]:lw t3, 1968(a5)
[0x80002abc]:lw t4, 1972(a5)
[0x80002ac0]:lw s10, 1976(a5)
[0x80002ac4]:lw s11, 1980(a5)
[0x80002ac8]:lui t3, 981229
[0x80002acc]:addi t3, t3, 313
[0x80002ad0]:lui t4, 235547
[0x80002ad4]:addi t4, t4, 533
[0x80002ad8]:lui s10, 67669
[0x80002adc]:addi s10, s10, 2529
[0x80002ae0]:lui s11, 261014
[0x80002ae4]:addi s11, s11, 3942
[0x80002ae8]:addi a4, zero, 96
[0x80002aec]:csrrw zero, fcsr, a4
[0x80002af0]:fmul.d t5, t3, s10, dyn
[0x80002af4]:csrrs a6, fcsr, zero

[0x80002af0]:fmul.d t5, t3, s10, dyn
[0x80002af4]:csrrs a6, fcsr, zero
[0x80002af8]:sw t5, 1896(ra)
[0x80002afc]:sw t6, 1904(ra)
[0x80002b00]:sw t5, 1912(ra)
[0x80002b04]:sw a6, 1920(ra)
[0x80002b08]:lw t3, 1984(a5)
[0x80002b0c]:lw t4, 1988(a5)
[0x80002b10]:lw s10, 1992(a5)
[0x80002b14]:lw s11, 1996(a5)
[0x80002b18]:lui t3, 981229
[0x80002b1c]:addi t3, t3, 313
[0x80002b20]:lui t4, 235547
[0x80002b24]:addi t4, t4, 533
[0x80002b28]:lui s10, 67669
[0x80002b2c]:addi s10, s10, 2529
[0x80002b30]:lui s11, 261014
[0x80002b34]:addi s11, s11, 3942
[0x80002b38]:addi a4, zero, 128
[0x80002b3c]:csrrw zero, fcsr, a4
[0x80002b40]:fmul.d t5, t3, s10, dyn
[0x80002b44]:csrrs a6, fcsr, zero

[0x80002b40]:fmul.d t5, t3, s10, dyn
[0x80002b44]:csrrs a6, fcsr, zero
[0x80002b48]:sw t5, 1928(ra)
[0x80002b4c]:sw t6, 1936(ra)
[0x80002b50]:sw t5, 1944(ra)
[0x80002b54]:sw a6, 1952(ra)
[0x80002b58]:lw t3, 2000(a5)
[0x80002b5c]:lw t4, 2004(a5)
[0x80002b60]:lw s10, 2008(a5)
[0x80002b64]:lw s11, 2012(a5)
[0x80002b68]:lui t3, 433558
[0x80002b6c]:addi t3, t3, 2083
[0x80002b70]:lui t4, 236235
[0x80002b74]:addi t4, t4, 2394
[0x80002b78]:lui s10, 117992
[0x80002b7c]:addi s10, s10, 3732
[0x80002b80]:lui s11, 259535
[0x80002b84]:addi s11, s11, 1471
[0x80002b88]:addi a4, zero, 0
[0x80002b8c]:csrrw zero, fcsr, a4
[0x80002b90]:fmul.d t5, t3, s10, dyn
[0x80002b94]:csrrs a6, fcsr, zero

[0x80002b90]:fmul.d t5, t3, s10, dyn
[0x80002b94]:csrrs a6, fcsr, zero
[0x80002b98]:sw t5, 1960(ra)
[0x80002b9c]:sw t6, 1968(ra)
[0x80002ba0]:sw t5, 1976(ra)
[0x80002ba4]:sw a6, 1984(ra)
[0x80002ba8]:lw t3, 2016(a5)
[0x80002bac]:lw t4, 2020(a5)
[0x80002bb0]:lw s10, 2024(a5)
[0x80002bb4]:lw s11, 2028(a5)
[0x80002bb8]:lui t3, 433558
[0x80002bbc]:addi t3, t3, 2083
[0x80002bc0]:lui t4, 236235
[0x80002bc4]:addi t4, t4, 2394
[0x80002bc8]:lui s10, 117992
[0x80002bcc]:addi s10, s10, 3732
[0x80002bd0]:lui s11, 259535
[0x80002bd4]:addi s11, s11, 1471
[0x80002bd8]:addi a4, zero, 32
[0x80002bdc]:csrrw zero, fcsr, a4
[0x80002be0]:fmul.d t5, t3, s10, dyn
[0x80002be4]:csrrs a6, fcsr, zero

[0x80002be0]:fmul.d t5, t3, s10, dyn
[0x80002be4]:csrrs a6, fcsr, zero
[0x80002be8]:sw t5, 1992(ra)
[0x80002bec]:sw t6, 2000(ra)
[0x80002bf0]:sw t5, 2008(ra)
[0x80002bf4]:sw a6, 2016(ra)
[0x80002bf8]:lw t3, 2032(a5)
[0x80002bfc]:lw t4, 2036(a5)
[0x80002c00]:lw s10, 2040(a5)
[0x80002c04]:lw s11, 2044(a5)
[0x80002c08]:lui t3, 433558
[0x80002c0c]:addi t3, t3, 2083
[0x80002c10]:lui t4, 236235
[0x80002c14]:addi t4, t4, 2394
[0x80002c18]:lui s10, 117992
[0x80002c1c]:addi s10, s10, 3732
[0x80002c20]:lui s11, 259535
[0x80002c24]:addi s11, s11, 1471
[0x80002c28]:addi a4, zero, 64
[0x80002c2c]:csrrw zero, fcsr, a4
[0x80002c30]:fmul.d t5, t3, s10, dyn
[0x80002c34]:csrrs a6, fcsr, zero

[0x80002c30]:fmul.d t5, t3, s10, dyn
[0x80002c34]:csrrs a6, fcsr, zero
[0x80002c38]:sw t5, 2024(ra)
[0x80002c3c]:sw t6, 2032(ra)
[0x80002c40]:sw t5, 2040(ra)
[0x80002c44]:addi ra, ra, 2040
[0x80002c48]:sw a6, 8(ra)
[0x80002c4c]:lui a4, 1
[0x80002c50]:addi a4, a4, 2048
[0x80002c54]:add a5, a5, a4
[0x80002c58]:lw t3, 0(a5)
[0x80002c5c]:sub a5, a5, a4
[0x80002c60]:lui a4, 1
[0x80002c64]:addi a4, a4, 2048
[0x80002c68]:add a5, a5, a4
[0x80002c6c]:lw t4, 4(a5)
[0x80002c70]:sub a5, a5, a4
[0x80002c74]:lui a4, 1
[0x80002c78]:addi a4, a4, 2048
[0x80002c7c]:add a5, a5, a4
[0x80002c80]:lw s10, 8(a5)
[0x80002c84]:sub a5, a5, a4
[0x80002c88]:lui a4, 1
[0x80002c8c]:addi a4, a4, 2048
[0x80002c90]:add a5, a5, a4
[0x80002c94]:lw s11, 12(a5)
[0x80002c98]:sub a5, a5, a4
[0x80002c9c]:lui t3, 433558
[0x80002ca0]:addi t3, t3, 2083
[0x80002ca4]:lui t4, 236235
[0x80002ca8]:addi t4, t4, 2394
[0x80002cac]:lui s10, 117992
[0x80002cb0]:addi s10, s10, 3732
[0x80002cb4]:lui s11, 259535
[0x80002cb8]:addi s11, s11, 1471
[0x80002cbc]:addi a4, zero, 96
[0x80002cc0]:csrrw zero, fcsr, a4
[0x80002cc4]:fmul.d t5, t3, s10, dyn
[0x80002cc8]:csrrs a6, fcsr, zero

[0x80002cc4]:fmul.d t5, t3, s10, dyn
[0x80002cc8]:csrrs a6, fcsr, zero
[0x80002ccc]:sw t5, 16(ra)
[0x80002cd0]:sw t6, 24(ra)
[0x80002cd4]:sw t5, 32(ra)
[0x80002cd8]:sw a6, 40(ra)
[0x80002cdc]:lui a4, 1
[0x80002ce0]:addi a4, a4, 2048
[0x80002ce4]:add a5, a5, a4
[0x80002ce8]:lw t3, 16(a5)
[0x80002cec]:sub a5, a5, a4
[0x80002cf0]:lui a4, 1
[0x80002cf4]:addi a4, a4, 2048
[0x80002cf8]:add a5, a5, a4
[0x80002cfc]:lw t4, 20(a5)
[0x80002d00]:sub a5, a5, a4
[0x80002d04]:lui a4, 1
[0x80002d08]:addi a4, a4, 2048
[0x80002d0c]:add a5, a5, a4
[0x80002d10]:lw s10, 24(a5)
[0x80002d14]:sub a5, a5, a4
[0x80002d18]:lui a4, 1
[0x80002d1c]:addi a4, a4, 2048
[0x80002d20]:add a5, a5, a4
[0x80002d24]:lw s11, 28(a5)
[0x80002d28]:sub a5, a5, a4
[0x80002d2c]:lui t3, 433558
[0x80002d30]:addi t3, t3, 2083
[0x80002d34]:lui t4, 236235
[0x80002d38]:addi t4, t4, 2394
[0x80002d3c]:lui s10, 117992
[0x80002d40]:addi s10, s10, 3732
[0x80002d44]:lui s11, 259535
[0x80002d48]:addi s11, s11, 1471
[0x80002d4c]:addi a4, zero, 128
[0x80002d50]:csrrw zero, fcsr, a4
[0x80002d54]:fmul.d t5, t3, s10, dyn
[0x80002d58]:csrrs a6, fcsr, zero

[0x80002d54]:fmul.d t5, t3, s10, dyn
[0x80002d58]:csrrs a6, fcsr, zero
[0x80002d5c]:sw t5, 48(ra)
[0x80002d60]:sw t6, 56(ra)
[0x80002d64]:sw t5, 64(ra)
[0x80002d68]:sw a6, 72(ra)
[0x80002d6c]:lui a4, 1
[0x80002d70]:addi a4, a4, 2048
[0x80002d74]:add a5, a5, a4
[0x80002d78]:lw t3, 32(a5)
[0x80002d7c]:sub a5, a5, a4
[0x80002d80]:lui a4, 1
[0x80002d84]:addi a4, a4, 2048
[0x80002d88]:add a5, a5, a4
[0x80002d8c]:lw t4, 36(a5)
[0x80002d90]:sub a5, a5, a4
[0x80002d94]:lui a4, 1
[0x80002d98]:addi a4, a4, 2048
[0x80002d9c]:add a5, a5, a4
[0x80002da0]:lw s10, 40(a5)
[0x80002da4]:sub a5, a5, a4
[0x80002da8]:lui a4, 1
[0x80002dac]:addi a4, a4, 2048
[0x80002db0]:add a5, a5, a4
[0x80002db4]:lw s11, 44(a5)
[0x80002db8]:sub a5, a5, a4
[0x80002dbc]:lui t3, 363185
[0x80002dc0]:addi t3, t3, 1577
[0x80002dc4]:lui t4, 236108
[0x80002dc8]:addi t4, t4, 3425
[0x80002dcc]:lui s10, 1002677
[0x80002dd0]:addi s10, s10, 2233
[0x80002dd4]:lui s11, 269707
[0x80002dd8]:addi s11, s11, 4054
[0x80002ddc]:addi a4, zero, 64
[0x80002de0]:csrrw zero, fcsr, a4
[0x80002de4]:fmul.d t5, t3, s10, dyn
[0x80002de8]:csrrs a6, fcsr, zero

[0x80002de4]:fmul.d t5, t3, s10, dyn
[0x80002de8]:csrrs a6, fcsr, zero
[0x80002dec]:sw t5, 80(ra)
[0x80002df0]:sw t6, 88(ra)
[0x80002df4]:sw t5, 96(ra)
[0x80002df8]:sw a6, 104(ra)
[0x80002dfc]:lui a4, 1
[0x80002e00]:addi a4, a4, 2048
[0x80002e04]:add a5, a5, a4
[0x80002e08]:lw t3, 48(a5)
[0x80002e0c]:sub a5, a5, a4
[0x80002e10]:lui a4, 1
[0x80002e14]:addi a4, a4, 2048
[0x80002e18]:add a5, a5, a4
[0x80002e1c]:lw t4, 52(a5)
[0x80002e20]:sub a5, a5, a4
[0x80002e24]:lui a4, 1
[0x80002e28]:addi a4, a4, 2048
[0x80002e2c]:add a5, a5, a4
[0x80002e30]:lw s10, 56(a5)
[0x80002e34]:sub a5, a5, a4
[0x80002e38]:lui a4, 1
[0x80002e3c]:addi a4, a4, 2048
[0x80002e40]:add a5, a5, a4
[0x80002e44]:lw s11, 60(a5)
[0x80002e48]:sub a5, a5, a4
[0x80002e4c]:lui t3, 363185
[0x80002e50]:addi t3, t3, 1577
[0x80002e54]:lui t4, 236108
[0x80002e58]:addi t4, t4, 3425
[0x80002e5c]:lui s10, 1002677
[0x80002e60]:addi s10, s10, 2233
[0x80002e64]:lui s11, 269707
[0x80002e68]:addi s11, s11, 4054
[0x80002e6c]:addi a4, zero, 96
[0x80002e70]:csrrw zero, fcsr, a4
[0x80002e74]:fmul.d t5, t3, s10, dyn
[0x80002e78]:csrrs a6, fcsr, zero

[0x80002e74]:fmul.d t5, t3, s10, dyn
[0x80002e78]:csrrs a6, fcsr, zero
[0x80002e7c]:sw t5, 112(ra)
[0x80002e80]:sw t6, 120(ra)
[0x80002e84]:sw t5, 128(ra)
[0x80002e88]:sw a6, 136(ra)
[0x80002e8c]:addi zero, zero, 0



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fmul.d', 'rs1 : x28', 'rs2 : x26', 'rd : x30', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x4bd6158ab1629 and fs2 == 0 and fe2 == 0x41d and fm2 == 0x8afd6f4cb48b9 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000014c]:fmul.d t5, t3, s10, dyn
	-[0x80000150]:csrrs tp, fcsr, zero
	-[0x80000154]:sw t5, 0(ra)
	-[0x80000158]:sw t6, 8(ra)
Current Store : [0x80000158] : sw t6, 8(ra) -- Store: [0x80004a20]:0xFBB6FAB7




Last Coverpoint : ['mnemonic : fmul.d', 'rs1 : x28', 'rs2 : x26', 'rd : x30', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x4bd6158ab1629 and fs2 == 0 and fe2 == 0x41d and fm2 == 0x8afd6f4cb48b9 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000014c]:fmul.d t5, t3, s10, dyn
	-[0x80000150]:csrrs tp, fcsr, zero
	-[0x80000154]:sw t5, 0(ra)
	-[0x80000158]:sw t6, 8(ra)
	-[0x8000015c]:sw t5, 16(ra)
Current Store : [0x8000015c] : sw t5, 16(ra) -- Store: [0x80004a28]:0xFFFFFFFF




Last Coverpoint : ['rs1 : x26', 'rs2 : x30', 'rd : x26', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x4bd6158ab1629 and fs2 == 0 and fe2 == 0x41d and fm2 == 0x8afd6f4cb48b9 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000019c]:fmul.d s10, s10, t5, dyn
	-[0x800001a0]:csrrs tp, fcsr, zero
	-[0x800001a4]:sw s10, 32(ra)
	-[0x800001a8]:sw s11, 40(ra)
Current Store : [0x800001a8] : sw s11, 40(ra) -- Store: [0x80004a40]:0x39A4BD61




Last Coverpoint : ['rs1 : x26', 'rs2 : x30', 'rd : x26', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x4bd6158ab1629 and fs2 == 0 and fe2 == 0x41d and fm2 == 0x8afd6f4cb48b9 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000019c]:fmul.d s10, s10, t5, dyn
	-[0x800001a0]:csrrs tp, fcsr, zero
	-[0x800001a4]:sw s10, 32(ra)
	-[0x800001a8]:sw s11, 40(ra)
	-[0x800001ac]:sw s10, 48(ra)
Current Store : [0x800001ac] : sw s10, 48(ra) -- Store: [0x80004a48]:0xFFFFFFFF




Last Coverpoint : ['rs1 : x24', 'rs2 : x24', 'rd : x24', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x800001ec]:fmul.d s8, s8, s8, dyn
	-[0x800001f0]:csrrs tp, fcsr, zero
	-[0x800001f4]:sw s8, 64(ra)
	-[0x800001f8]:sw s9, 72(ra)
Current Store : [0x800001f8] : sw s9, 72(ra) -- Store: [0x80004a60]:0x39A4BD61




Last Coverpoint : ['rs1 : x24', 'rs2 : x24', 'rd : x24', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x800001ec]:fmul.d s8, s8, s8, dyn
	-[0x800001f0]:csrrs tp, fcsr, zero
	-[0x800001f4]:sw s8, 64(ra)
	-[0x800001f8]:sw s9, 72(ra)
	-[0x800001fc]:sw s8, 80(ra)
Current Store : [0x800001fc] : sw s8, 80(ra) -- Store: [0x80004a68]:0xEBCAC657




Last Coverpoint : ['rs1 : x22', 'rs2 : x22', 'rd : x28', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x8000023c]:fmul.d t3, s6, s6, dyn
	-[0x80000240]:csrrs tp, fcsr, zero
	-[0x80000244]:sw t3, 96(ra)
	-[0x80000248]:sw t4, 104(ra)
Current Store : [0x80000248] : sw t4, 104(ra) -- Store: [0x80004a80]:0x39A4BD61




Last Coverpoint : ['rs1 : x22', 'rs2 : x22', 'rd : x28', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x8000023c]:fmul.d t3, s6, s6, dyn
	-[0x80000240]:csrrs tp, fcsr, zero
	-[0x80000244]:sw t3, 96(ra)
	-[0x80000248]:sw t4, 104(ra)
	-[0x8000024c]:sw t3, 112(ra)
Current Store : [0x8000024c] : sw t3, 112(ra) -- Store: [0x80004a88]:0xEBCAC658




Last Coverpoint : ['rs1 : x30', 'rs2 : x20', 'rd : x20', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x4bd6158ab1629 and fs2 == 0 and fe2 == 0x41d and fm2 == 0x8afd6f4cb48b9 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000028c]:fmul.d s4, t5, s4, dyn
	-[0x80000290]:csrrs tp, fcsr, zero
	-[0x80000294]:sw s4, 128(ra)
	-[0x80000298]:sw s5, 136(ra)
Current Store : [0x80000298] : sw s5, 136(ra) -- Store: [0x80004aa0]:0x41D8AFD6




Last Coverpoint : ['rs1 : x30', 'rs2 : x20', 'rd : x20', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x4bd6158ab1629 and fs2 == 0 and fe2 == 0x41d and fm2 == 0x8afd6f4cb48b9 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000028c]:fmul.d s4, t5, s4, dyn
	-[0x80000290]:csrrs tp, fcsr, zero
	-[0x80000294]:sw s4, 128(ra)
	-[0x80000298]:sw s5, 136(ra)
	-[0x8000029c]:sw s4, 144(ra)
Current Store : [0x8000029c] : sw s4, 144(ra) -- Store: [0x80004aa8]:0xFFFFFFFF




Last Coverpoint : ['rs1 : x20', 'rs2 : x28', 'rd : x22', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002dc]:fmul.d s6, s4, t3, dyn
	-[0x800002e0]:csrrs tp, fcsr, zero
	-[0x800002e4]:sw s6, 160(ra)
	-[0x800002e8]:sw s7, 168(ra)
Current Store : [0x800002e8] : sw s7, 168(ra) -- Store: [0x80004ac0]:0x39A4BD61




Last Coverpoint : ['rs1 : x20', 'rs2 : x28', 'rd : x22', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002dc]:fmul.d s6, s4, t3, dyn
	-[0x800002e0]:csrrs tp, fcsr, zero
	-[0x800002e4]:sw s6, 160(ra)
	-[0x800002e8]:sw s7, 168(ra)
	-[0x800002ec]:sw s6, 176(ra)
Current Store : [0x800002ec] : sw s6, 176(ra) -- Store: [0x80004ac8]:0x00000000




Last Coverpoint : ['rs1 : x16', 'rs2 : x14', 'rd : x18', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000032c]:fmul.d s2, a6, a4, dyn
	-[0x80000330]:csrrs tp, fcsr, zero
	-[0x80000334]:sw s2, 192(ra)
	-[0x80000338]:sw s3, 200(ra)
Current Store : [0x80000338] : sw s3, 200(ra) -- Store: [0x80004ae0]:0x6FAB7FBB




Last Coverpoint : ['rs1 : x16', 'rs2 : x14', 'rd : x18', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000032c]:fmul.d s2, a6, a4, dyn
	-[0x80000330]:csrrs tp, fcsr, zero
	-[0x80000334]:sw s2, 192(ra)
	-[0x80000338]:sw s3, 200(ra)
	-[0x8000033c]:sw s2, 208(ra)
Current Store : [0x8000033c] : sw s2, 208(ra) -- Store: [0x80004ae8]:0xFFFFFFFF




Last Coverpoint : ['rs1 : x14', 'rs2 : x18', 'rd : x16', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000037c]:fmul.d a6, a4, s2, dyn
	-[0x80000380]:csrrs tp, fcsr, zero
	-[0x80000384]:sw a6, 224(ra)
	-[0x80000388]:sw a7, 232(ra)
Current Store : [0x80000388] : sw a7, 232(ra) -- Store: [0x80004b00]:0x39AFCB6E




Last Coverpoint : ['rs1 : x14', 'rs2 : x18', 'rd : x16', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000037c]:fmul.d a6, a4, s2, dyn
	-[0x80000380]:csrrs tp, fcsr, zero
	-[0x80000384]:sw a6, 224(ra)
	-[0x80000388]:sw a7, 232(ra)
	-[0x8000038c]:sw a6, 240(ra)
Current Store : [0x8000038c] : sw a6, 240(ra) -- Store: [0x80004b08]:0xFFFFFFFF




Last Coverpoint : ['rs1 : x18', 'rs2 : x16', 'rd : x14', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003cc]:fmul.d a4, s2, a6, dyn
	-[0x800003d0]:csrrs tp, fcsr, zero
	-[0x800003d4]:sw a4, 256(ra)
	-[0x800003d8]:sw a5, 264(ra)
Current Store : [0x800003d8] : sw a5, 264(ra) -- Store: [0x80004b20]:0x39AFCB6E




Last Coverpoint : ['rs1 : x18', 'rs2 : x16', 'rd : x14', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003cc]:fmul.d a4, s2, a6, dyn
	-[0x800003d0]:csrrs tp, fcsr, zero
	-[0x800003d4]:sw a4, 256(ra)
	-[0x800003d8]:sw a5, 264(ra)
	-[0x800003dc]:sw a4, 272(ra)
Current Store : [0x800003dc] : sw a4, 272(ra) -- Store: [0x80004b28]:0x00000000




Last Coverpoint : ['rs1 : x10', 'rs2 : x8', 'rd : x12', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000041c]:fmul.d a2, a0, fp, dyn
	-[0x80000420]:csrrs tp, fcsr, zero
	-[0x80000424]:sw a2, 288(ra)
	-[0x80000428]:sw a3, 296(ra)
Current Store : [0x80000428] : sw a3, 296(ra) -- Store: [0x80004b40]:0xEADFEEDB




Last Coverpoint : ['rs1 : x10', 'rs2 : x8', 'rd : x12', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000041c]:fmul.d a2, a0, fp, dyn
	-[0x80000420]:csrrs tp, fcsr, zero
	-[0x80000424]:sw a2, 288(ra)
	-[0x80000428]:sw a3, 296(ra)
	-[0x8000042c]:sw a2, 304(ra)
Current Store : [0x8000042c] : sw a2, 304(ra) -- Store: [0x80004b48]:0x00000000




Last Coverpoint : ['rs1 : x8', 'rs2 : x12', 'rd : x10', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000047c]:fmul.d a0, fp, a2, dyn
	-[0x80000480]:csrrs a6, fcsr, zero
	-[0x80000484]:sw a0, 0(ra)
	-[0x80000488]:sw a1, 8(ra)
Current Store : [0x80000488] : sw a1, 8(ra) -- Store: [0x80004ac0]:0x39AFCB6E




Last Coverpoint : ['rs1 : x8', 'rs2 : x12', 'rd : x10', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000047c]:fmul.d a0, fp, a2, dyn
	-[0x80000480]:csrrs a6, fcsr, zero
	-[0x80000484]:sw a0, 0(ra)
	-[0x80000488]:sw a1, 8(ra)
	-[0x8000048c]:sw a0, 16(ra)
Current Store : [0x8000048c] : sw a0, 16(ra) -- Store: [0x80004ac8]:0x8A1EDE0E




Last Coverpoint : ['rs1 : x12', 'rs2 : x10', 'rd : x8', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004cc]:fmul.d fp, a2, a0, dyn
	-[0x800004d0]:csrrs a6, fcsr, zero
	-[0x800004d4]:sw fp, 32(ra)
	-[0x800004d8]:sw s1, 40(ra)
Current Store : [0x800004d8] : sw s1, 40(ra) -- Store: [0x80004ae0]:0x39A35544




Last Coverpoint : ['rs1 : x12', 'rs2 : x10', 'rd : x8', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004cc]:fmul.d fp, a2, a0, dyn
	-[0x800004d0]:csrrs a6, fcsr, zero
	-[0x800004d4]:sw fp, 32(ra)
	-[0x800004d8]:sw s1, 40(ra)
	-[0x800004dc]:sw fp, 48(ra)
Current Store : [0x800004dc] : sw fp, 48(ra) -- Store: [0x80004ae8]:0x8A1EDE0D




Last Coverpoint : ['rs1 : x4', 'rs2 : x2', 'rd : x6', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000051c]:fmul.d t1, tp, sp, dyn
	-[0x80000520]:csrrs a6, fcsr, zero
	-[0x80000524]:sw t1, 64(ra)
	-[0x80000528]:sw t2, 72(ra)
Current Store : [0x80000528] : sw t2, 72(ra) -- Store: [0x80004b00]:0xB7FBB6FA




Last Coverpoint : ['rs1 : x4', 'rs2 : x2', 'rd : x6', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000051c]:fmul.d t1, tp, sp, dyn
	-[0x80000520]:csrrs a6, fcsr, zero
	-[0x80000524]:sw t1, 64(ra)
	-[0x80000528]:sw t2, 72(ra)
	-[0x8000052c]:sw t1, 80(ra)
Current Store : [0x8000052c] : sw t1, 80(ra) -- Store: [0x80004b08]:0x8A1EDE0D




Last Coverpoint : ['rs1 : x2', 'rs2 : x6', 'rd : x4', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000056c]:fmul.d tp, sp, t1, dyn
	-[0x80000570]:csrrs a6, fcsr, zero
	-[0x80000574]:sw tp, 96(ra)
	-[0x80000578]:sw t0, 104(ra)
Current Store : [0x80000578] : sw t0, 104(ra) -- Store: [0x80004b20]:0x39A35544




Last Coverpoint : ['rs1 : x2', 'rs2 : x6', 'rd : x4', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000056c]:fmul.d tp, sp, t1, dyn
	-[0x80000570]:csrrs a6, fcsr, zero
	-[0x80000574]:sw tp, 96(ra)
	-[0x80000578]:sw t0, 104(ra)
	-[0x8000057c]:sw tp, 112(ra)
Current Store : [0x8000057c] : sw tp, 112(ra) -- Store: [0x80004b28]:0x8A1EDE0E




Last Coverpoint : ['rs1 : x6', 'rs2 : x4', 'rd : x2', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005bc]:fmul.d sp, t1, tp, dyn
	-[0x800005c0]:csrrs a6, fcsr, zero
	-[0x800005c4]:sw sp, 128(ra)
	-[0x800005c8]:sw gp, 136(ra)
Current Store : [0x800005c8] : sw gp, 136(ra) -- Store: [0x80004b40]:0x39A35544




Last Coverpoint : ['rs1 : x6', 'rs2 : x4', 'rd : x2', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005bc]:fmul.d sp, t1, tp, dyn
	-[0x800005c0]:csrrs a6, fcsr, zero
	-[0x800005c4]:sw sp, 128(ra)
	-[0x800005c8]:sw gp, 136(ra)
	-[0x800005cc]:sw sp, 144(ra)
Current Store : [0x800005cc] : sw sp, 144(ra) -- Store: [0x80004b48]:0x8A1EDE0E




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000060c]:fmul.d t5, t3, s10, dyn
	-[0x80000610]:csrrs a6, fcsr, zero
	-[0x80000614]:sw t5, 160(ra)
	-[0x80000618]:sw t6, 168(ra)
Current Store : [0x80000618] : sw t6, 168(ra) -- Store: [0x80004b60]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000060c]:fmul.d t5, t3, s10, dyn
	-[0x80000610]:csrrs a6, fcsr, zero
	-[0x80000614]:sw t5, 160(ra)
	-[0x80000618]:sw t6, 168(ra)
	-[0x8000061c]:sw t5, 176(ra)
Current Store : [0x8000061c] : sw t5, 176(ra) -- Store: [0x80004b68]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000065c]:fmul.d t5, t3, s10, dyn
	-[0x80000660]:csrrs a6, fcsr, zero
	-[0x80000664]:sw t5, 192(ra)
	-[0x80000668]:sw t6, 200(ra)
Current Store : [0x80000668] : sw t6, 200(ra) -- Store: [0x80004b80]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000065c]:fmul.d t5, t3, s10, dyn
	-[0x80000660]:csrrs a6, fcsr, zero
	-[0x80000664]:sw t5, 192(ra)
	-[0x80000668]:sw t6, 200(ra)
	-[0x8000066c]:sw t5, 208(ra)
Current Store : [0x8000066c] : sw t5, 208(ra) -- Store: [0x80004b88]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006ac]:fmul.d t5, t3, s10, dyn
	-[0x800006b0]:csrrs a6, fcsr, zero
	-[0x800006b4]:sw t5, 224(ra)
	-[0x800006b8]:sw t6, 232(ra)
Current Store : [0x800006b8] : sw t6, 232(ra) -- Store: [0x80004ba0]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006ac]:fmul.d t5, t3, s10, dyn
	-[0x800006b0]:csrrs a6, fcsr, zero
	-[0x800006b4]:sw t5, 224(ra)
	-[0x800006b8]:sw t6, 232(ra)
	-[0x800006bc]:sw t5, 240(ra)
Current Store : [0x800006bc] : sw t5, 240(ra) -- Store: [0x80004ba8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006fc]:fmul.d t5, t3, s10, dyn
	-[0x80000700]:csrrs a6, fcsr, zero
	-[0x80000704]:sw t5, 256(ra)
	-[0x80000708]:sw t6, 264(ra)
Current Store : [0x80000708] : sw t6, 264(ra) -- Store: [0x80004bc0]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006fc]:fmul.d t5, t3, s10, dyn
	-[0x80000700]:csrrs a6, fcsr, zero
	-[0x80000704]:sw t5, 256(ra)
	-[0x80000708]:sw t6, 264(ra)
	-[0x8000070c]:sw t5, 272(ra)
Current Store : [0x8000070c] : sw t5, 272(ra) -- Store: [0x80004bc8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000074c]:fmul.d t5, t3, s10, dyn
	-[0x80000750]:csrrs a6, fcsr, zero
	-[0x80000754]:sw t5, 288(ra)
	-[0x80000758]:sw t6, 296(ra)
Current Store : [0x80000758] : sw t6, 296(ra) -- Store: [0x80004be0]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000074c]:fmul.d t5, t3, s10, dyn
	-[0x80000750]:csrrs a6, fcsr, zero
	-[0x80000754]:sw t5, 288(ra)
	-[0x80000758]:sw t6, 296(ra)
	-[0x8000075c]:sw t5, 304(ra)
Current Store : [0x8000075c] : sw t5, 304(ra) -- Store: [0x80004be8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000079c]:fmul.d t5, t3, s10, dyn
	-[0x800007a0]:csrrs a6, fcsr, zero
	-[0x800007a4]:sw t5, 320(ra)
	-[0x800007a8]:sw t6, 328(ra)
Current Store : [0x800007a8] : sw t6, 328(ra) -- Store: [0x80004c00]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000079c]:fmul.d t5, t3, s10, dyn
	-[0x800007a0]:csrrs a6, fcsr, zero
	-[0x800007a4]:sw t5, 320(ra)
	-[0x800007a8]:sw t6, 328(ra)
	-[0x800007ac]:sw t5, 336(ra)
Current Store : [0x800007ac] : sw t5, 336(ra) -- Store: [0x80004c08]:0x86A12B9B




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007ec]:fmul.d t5, t3, s10, dyn
	-[0x800007f0]:csrrs a6, fcsr, zero
	-[0x800007f4]:sw t5, 352(ra)
	-[0x800007f8]:sw t6, 360(ra)
Current Store : [0x800007f8] : sw t6, 360(ra) -- Store: [0x80004c20]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007ec]:fmul.d t5, t3, s10, dyn
	-[0x800007f0]:csrrs a6, fcsr, zero
	-[0x800007f4]:sw t5, 352(ra)
	-[0x800007f8]:sw t6, 360(ra)
	-[0x800007fc]:sw t5, 368(ra)
Current Store : [0x800007fc] : sw t5, 368(ra) -- Store: [0x80004c28]:0x86A12B9A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000083c]:fmul.d t5, t3, s10, dyn
	-[0x80000840]:csrrs a6, fcsr, zero
	-[0x80000844]:sw t5, 384(ra)
	-[0x80000848]:sw t6, 392(ra)
Current Store : [0x80000848] : sw t6, 392(ra) -- Store: [0x80004c40]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000083c]:fmul.d t5, t3, s10, dyn
	-[0x80000840]:csrrs a6, fcsr, zero
	-[0x80000844]:sw t5, 384(ra)
	-[0x80000848]:sw t6, 392(ra)
	-[0x8000084c]:sw t5, 400(ra)
Current Store : [0x8000084c] : sw t5, 400(ra) -- Store: [0x80004c48]:0x86A12B9A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000088c]:fmul.d t5, t3, s10, dyn
	-[0x80000890]:csrrs a6, fcsr, zero
	-[0x80000894]:sw t5, 416(ra)
	-[0x80000898]:sw t6, 424(ra)
Current Store : [0x80000898] : sw t6, 424(ra) -- Store: [0x80004c60]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000088c]:fmul.d t5, t3, s10, dyn
	-[0x80000890]:csrrs a6, fcsr, zero
	-[0x80000894]:sw t5, 416(ra)
	-[0x80000898]:sw t6, 424(ra)
	-[0x8000089c]:sw t5, 432(ra)
Current Store : [0x8000089c] : sw t5, 432(ra) -- Store: [0x80004c68]:0x86A12B9B




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008dc]:fmul.d t5, t3, s10, dyn
	-[0x800008e0]:csrrs a6, fcsr, zero
	-[0x800008e4]:sw t5, 448(ra)
	-[0x800008e8]:sw t6, 456(ra)
Current Store : [0x800008e8] : sw t6, 456(ra) -- Store: [0x80004c80]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008dc]:fmul.d t5, t3, s10, dyn
	-[0x800008e0]:csrrs a6, fcsr, zero
	-[0x800008e4]:sw t5, 448(ra)
	-[0x800008e8]:sw t6, 456(ra)
	-[0x800008ec]:sw t5, 464(ra)
Current Store : [0x800008ec] : sw t5, 464(ra) -- Store: [0x80004c88]:0x86A12B9B




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000092c]:fmul.d t5, t3, s10, dyn
	-[0x80000930]:csrrs a6, fcsr, zero
	-[0x80000934]:sw t5, 480(ra)
	-[0x80000938]:sw t6, 488(ra)
Current Store : [0x80000938] : sw t6, 488(ra) -- Store: [0x80004ca0]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000092c]:fmul.d t5, t3, s10, dyn
	-[0x80000930]:csrrs a6, fcsr, zero
	-[0x80000934]:sw t5, 480(ra)
	-[0x80000938]:sw t6, 488(ra)
	-[0x8000093c]:sw t5, 496(ra)
Current Store : [0x8000093c] : sw t5, 496(ra) -- Store: [0x80004ca8]:0x8A1EDE0D




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000097c]:fmul.d t5, t3, s10, dyn
	-[0x80000980]:csrrs a6, fcsr, zero
	-[0x80000984]:sw t5, 512(ra)
	-[0x80000988]:sw t6, 520(ra)
Current Store : [0x80000988] : sw t6, 520(ra) -- Store: [0x80004cc0]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000097c]:fmul.d t5, t3, s10, dyn
	-[0x80000980]:csrrs a6, fcsr, zero
	-[0x80000984]:sw t5, 512(ra)
	-[0x80000988]:sw t6, 520(ra)
	-[0x8000098c]:sw t5, 528(ra)
Current Store : [0x8000098c] : sw t5, 528(ra) -- Store: [0x80004cc8]:0x8A1EDE0D




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009cc]:fmul.d t5, t3, s10, dyn
	-[0x800009d0]:csrrs a6, fcsr, zero
	-[0x800009d4]:sw t5, 544(ra)
	-[0x800009d8]:sw t6, 552(ra)
Current Store : [0x800009d8] : sw t6, 552(ra) -- Store: [0x80004ce0]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009cc]:fmul.d t5, t3, s10, dyn
	-[0x800009d0]:csrrs a6, fcsr, zero
	-[0x800009d4]:sw t5, 544(ra)
	-[0x800009d8]:sw t6, 552(ra)
	-[0x800009dc]:sw t5, 560(ra)
Current Store : [0x800009dc] : sw t5, 560(ra) -- Store: [0x80004ce8]:0x8A1EDE0D




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a1c]:fmul.d t5, t3, s10, dyn
	-[0x80000a20]:csrrs a6, fcsr, zero
	-[0x80000a24]:sw t5, 576(ra)
	-[0x80000a28]:sw t6, 584(ra)
Current Store : [0x80000a28] : sw t6, 584(ra) -- Store: [0x80004d00]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a1c]:fmul.d t5, t3, s10, dyn
	-[0x80000a20]:csrrs a6, fcsr, zero
	-[0x80000a24]:sw t5, 576(ra)
	-[0x80000a28]:sw t6, 584(ra)
	-[0x80000a2c]:sw t5, 592(ra)
Current Store : [0x80000a2c] : sw t5, 592(ra) -- Store: [0x80004d08]:0x8A1EDE0E




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a6c]:fmul.d t5, t3, s10, dyn
	-[0x80000a70]:csrrs a6, fcsr, zero
	-[0x80000a74]:sw t5, 608(ra)
	-[0x80000a78]:sw t6, 616(ra)
Current Store : [0x80000a78] : sw t6, 616(ra) -- Store: [0x80004d20]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a6c]:fmul.d t5, t3, s10, dyn
	-[0x80000a70]:csrrs a6, fcsr, zero
	-[0x80000a74]:sw t5, 608(ra)
	-[0x80000a78]:sw t6, 616(ra)
	-[0x80000a7c]:sw t5, 624(ra)
Current Store : [0x80000a7c] : sw t5, 624(ra) -- Store: [0x80004d28]:0x8A1EDE0D




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000abc]:fmul.d t5, t3, s10, dyn
	-[0x80000ac0]:csrrs a6, fcsr, zero
	-[0x80000ac4]:sw t5, 640(ra)
	-[0x80000ac8]:sw t6, 648(ra)
Current Store : [0x80000ac8] : sw t6, 648(ra) -- Store: [0x80004d40]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000abc]:fmul.d t5, t3, s10, dyn
	-[0x80000ac0]:csrrs a6, fcsr, zero
	-[0x80000ac4]:sw t5, 640(ra)
	-[0x80000ac8]:sw t6, 648(ra)
	-[0x80000acc]:sw t5, 656(ra)
Current Store : [0x80000acc] : sw t5, 656(ra) -- Store: [0x80004d48]:0xE5795FA5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b0c]:fmul.d t5, t3, s10, dyn
	-[0x80000b10]:csrrs a6, fcsr, zero
	-[0x80000b14]:sw t5, 672(ra)
	-[0x80000b18]:sw t6, 680(ra)
Current Store : [0x80000b18] : sw t6, 680(ra) -- Store: [0x80004d60]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b0c]:fmul.d t5, t3, s10, dyn
	-[0x80000b10]:csrrs a6, fcsr, zero
	-[0x80000b14]:sw t5, 672(ra)
	-[0x80000b18]:sw t6, 680(ra)
	-[0x80000b1c]:sw t5, 688(ra)
Current Store : [0x80000b1c] : sw t5, 688(ra) -- Store: [0x80004d68]:0xE5795FA5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b5c]:fmul.d t5, t3, s10, dyn
	-[0x80000b60]:csrrs a6, fcsr, zero
	-[0x80000b64]:sw t5, 704(ra)
	-[0x80000b68]:sw t6, 712(ra)
Current Store : [0x80000b68] : sw t6, 712(ra) -- Store: [0x80004d80]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b5c]:fmul.d t5, t3, s10, dyn
	-[0x80000b60]:csrrs a6, fcsr, zero
	-[0x80000b64]:sw t5, 704(ra)
	-[0x80000b68]:sw t6, 712(ra)
	-[0x80000b6c]:sw t5, 720(ra)
Current Store : [0x80000b6c] : sw t5, 720(ra) -- Store: [0x80004d88]:0xE5795FA5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bac]:fmul.d t5, t3, s10, dyn
	-[0x80000bb0]:csrrs a6, fcsr, zero
	-[0x80000bb4]:sw t5, 736(ra)
	-[0x80000bb8]:sw t6, 744(ra)
Current Store : [0x80000bb8] : sw t6, 744(ra) -- Store: [0x80004da0]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bac]:fmul.d t5, t3, s10, dyn
	-[0x80000bb0]:csrrs a6, fcsr, zero
	-[0x80000bb4]:sw t5, 736(ra)
	-[0x80000bb8]:sw t6, 744(ra)
	-[0x80000bbc]:sw t5, 752(ra)
Current Store : [0x80000bbc] : sw t5, 752(ra) -- Store: [0x80004da8]:0xE5795FA6




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bfc]:fmul.d t5, t3, s10, dyn
	-[0x80000c00]:csrrs a6, fcsr, zero
	-[0x80000c04]:sw t5, 768(ra)
	-[0x80000c08]:sw t6, 776(ra)
Current Store : [0x80000c08] : sw t6, 776(ra) -- Store: [0x80004dc0]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bfc]:fmul.d t5, t3, s10, dyn
	-[0x80000c00]:csrrs a6, fcsr, zero
	-[0x80000c04]:sw t5, 768(ra)
	-[0x80000c08]:sw t6, 776(ra)
	-[0x80000c0c]:sw t5, 784(ra)
Current Store : [0x80000c0c] : sw t5, 784(ra) -- Store: [0x80004dc8]:0xE5795FA5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c4c]:fmul.d t5, t3, s10, dyn
	-[0x80000c50]:csrrs a6, fcsr, zero
	-[0x80000c54]:sw t5, 800(ra)
	-[0x80000c58]:sw t6, 808(ra)
Current Store : [0x80000c58] : sw t6, 808(ra) -- Store: [0x80004de0]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c4c]:fmul.d t5, t3, s10, dyn
	-[0x80000c50]:csrrs a6, fcsr, zero
	-[0x80000c54]:sw t5, 800(ra)
	-[0x80000c58]:sw t6, 808(ra)
	-[0x80000c5c]:sw t5, 816(ra)
Current Store : [0x80000c5c] : sw t5, 816(ra) -- Store: [0x80004de8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c9c]:fmul.d t5, t3, s10, dyn
	-[0x80000ca0]:csrrs a6, fcsr, zero
	-[0x80000ca4]:sw t5, 832(ra)
	-[0x80000ca8]:sw t6, 840(ra)
Current Store : [0x80000ca8] : sw t6, 840(ra) -- Store: [0x80004e00]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c9c]:fmul.d t5, t3, s10, dyn
	-[0x80000ca0]:csrrs a6, fcsr, zero
	-[0x80000ca4]:sw t5, 832(ra)
	-[0x80000ca8]:sw t6, 840(ra)
	-[0x80000cac]:sw t5, 848(ra)
Current Store : [0x80000cac] : sw t5, 848(ra) -- Store: [0x80004e08]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cec]:fmul.d t5, t3, s10, dyn
	-[0x80000cf0]:csrrs a6, fcsr, zero
	-[0x80000cf4]:sw t5, 864(ra)
	-[0x80000cf8]:sw t6, 872(ra)
Current Store : [0x80000cf8] : sw t6, 872(ra) -- Store: [0x80004e20]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cec]:fmul.d t5, t3, s10, dyn
	-[0x80000cf0]:csrrs a6, fcsr, zero
	-[0x80000cf4]:sw t5, 864(ra)
	-[0x80000cf8]:sw t6, 872(ra)
	-[0x80000cfc]:sw t5, 880(ra)
Current Store : [0x80000cfc] : sw t5, 880(ra) -- Store: [0x80004e28]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d3c]:fmul.d t5, t3, s10, dyn
	-[0x80000d40]:csrrs a6, fcsr, zero
	-[0x80000d44]:sw t5, 896(ra)
	-[0x80000d48]:sw t6, 904(ra)
Current Store : [0x80000d48] : sw t6, 904(ra) -- Store: [0x80004e40]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d3c]:fmul.d t5, t3, s10, dyn
	-[0x80000d40]:csrrs a6, fcsr, zero
	-[0x80000d44]:sw t5, 896(ra)
	-[0x80000d48]:sw t6, 904(ra)
	-[0x80000d4c]:sw t5, 912(ra)
Current Store : [0x80000d4c] : sw t5, 912(ra) -- Store: [0x80004e48]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d8c]:fmul.d t5, t3, s10, dyn
	-[0x80000d90]:csrrs a6, fcsr, zero
	-[0x80000d94]:sw t5, 928(ra)
	-[0x80000d98]:sw t6, 936(ra)
Current Store : [0x80000d98] : sw t6, 936(ra) -- Store: [0x80004e60]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d8c]:fmul.d t5, t3, s10, dyn
	-[0x80000d90]:csrrs a6, fcsr, zero
	-[0x80000d94]:sw t5, 928(ra)
	-[0x80000d98]:sw t6, 936(ra)
	-[0x80000d9c]:sw t5, 944(ra)
Current Store : [0x80000d9c] : sw t5, 944(ra) -- Store: [0x80004e68]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ddc]:fmul.d t5, t3, s10, dyn
	-[0x80000de0]:csrrs a6, fcsr, zero
	-[0x80000de4]:sw t5, 960(ra)
	-[0x80000de8]:sw t6, 968(ra)
Current Store : [0x80000de8] : sw t6, 968(ra) -- Store: [0x80004e80]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ddc]:fmul.d t5, t3, s10, dyn
	-[0x80000de0]:csrrs a6, fcsr, zero
	-[0x80000de4]:sw t5, 960(ra)
	-[0x80000de8]:sw t6, 968(ra)
	-[0x80000dec]:sw t5, 976(ra)
Current Store : [0x80000dec] : sw t5, 976(ra) -- Store: [0x80004e88]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e2c]:fmul.d t5, t3, s10, dyn
	-[0x80000e30]:csrrs a6, fcsr, zero
	-[0x80000e34]:sw t5, 992(ra)
	-[0x80000e38]:sw t6, 1000(ra)
Current Store : [0x80000e38] : sw t6, 1000(ra) -- Store: [0x80004ea0]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e2c]:fmul.d t5, t3, s10, dyn
	-[0x80000e30]:csrrs a6, fcsr, zero
	-[0x80000e34]:sw t5, 992(ra)
	-[0x80000e38]:sw t6, 1000(ra)
	-[0x80000e3c]:sw t5, 1008(ra)
Current Store : [0x80000e3c] : sw t5, 1008(ra) -- Store: [0x80004ea8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e7c]:fmul.d t5, t3, s10, dyn
	-[0x80000e80]:csrrs a6, fcsr, zero
	-[0x80000e84]:sw t5, 1024(ra)
	-[0x80000e88]:sw t6, 1032(ra)
Current Store : [0x80000e88] : sw t6, 1032(ra) -- Store: [0x80004ec0]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e7c]:fmul.d t5, t3, s10, dyn
	-[0x80000e80]:csrrs a6, fcsr, zero
	-[0x80000e84]:sw t5, 1024(ra)
	-[0x80000e88]:sw t6, 1032(ra)
	-[0x80000e8c]:sw t5, 1040(ra)
Current Store : [0x80000e8c] : sw t5, 1040(ra) -- Store: [0x80004ec8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ecc]:fmul.d t5, t3, s10, dyn
	-[0x80000ed0]:csrrs a6, fcsr, zero
	-[0x80000ed4]:sw t5, 1056(ra)
	-[0x80000ed8]:sw t6, 1064(ra)
Current Store : [0x80000ed8] : sw t6, 1064(ra) -- Store: [0x80004ee0]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ecc]:fmul.d t5, t3, s10, dyn
	-[0x80000ed0]:csrrs a6, fcsr, zero
	-[0x80000ed4]:sw t5, 1056(ra)
	-[0x80000ed8]:sw t6, 1064(ra)
	-[0x80000edc]:sw t5, 1072(ra)
Current Store : [0x80000edc] : sw t5, 1072(ra) -- Store: [0x80004ee8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f1c]:fmul.d t5, t3, s10, dyn
	-[0x80000f20]:csrrs a6, fcsr, zero
	-[0x80000f24]:sw t5, 1088(ra)
	-[0x80000f28]:sw t6, 1096(ra)
Current Store : [0x80000f28] : sw t6, 1096(ra) -- Store: [0x80004f00]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f1c]:fmul.d t5, t3, s10, dyn
	-[0x80000f20]:csrrs a6, fcsr, zero
	-[0x80000f24]:sw t5, 1088(ra)
	-[0x80000f28]:sw t6, 1096(ra)
	-[0x80000f2c]:sw t5, 1104(ra)
Current Store : [0x80000f2c] : sw t5, 1104(ra) -- Store: [0x80004f08]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f6c]:fmul.d t5, t3, s10, dyn
	-[0x80000f70]:csrrs a6, fcsr, zero
	-[0x80000f74]:sw t5, 1120(ra)
	-[0x80000f78]:sw t6, 1128(ra)
Current Store : [0x80000f78] : sw t6, 1128(ra) -- Store: [0x80004f20]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f6c]:fmul.d t5, t3, s10, dyn
	-[0x80000f70]:csrrs a6, fcsr, zero
	-[0x80000f74]:sw t5, 1120(ra)
	-[0x80000f78]:sw t6, 1128(ra)
	-[0x80000f7c]:sw t5, 1136(ra)
Current Store : [0x80000f7c] : sw t5, 1136(ra) -- Store: [0x80004f28]:0x8A1EDE0D




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fbc]:fmul.d t5, t3, s10, dyn
	-[0x80000fc0]:csrrs a6, fcsr, zero
	-[0x80000fc4]:sw t5, 1152(ra)
	-[0x80000fc8]:sw t6, 1160(ra)
Current Store : [0x80000fc8] : sw t6, 1160(ra) -- Store: [0x80004f40]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fbc]:fmul.d t5, t3, s10, dyn
	-[0x80000fc0]:csrrs a6, fcsr, zero
	-[0x80000fc4]:sw t5, 1152(ra)
	-[0x80000fc8]:sw t6, 1160(ra)
	-[0x80000fcc]:sw t5, 1168(ra)
Current Store : [0x80000fcc] : sw t5, 1168(ra) -- Store: [0x80004f48]:0x8A1EDE0D




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000100c]:fmul.d t5, t3, s10, dyn
	-[0x80001010]:csrrs a6, fcsr, zero
	-[0x80001014]:sw t5, 1184(ra)
	-[0x80001018]:sw t6, 1192(ra)
Current Store : [0x80001018] : sw t6, 1192(ra) -- Store: [0x80004f60]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000100c]:fmul.d t5, t3, s10, dyn
	-[0x80001010]:csrrs a6, fcsr, zero
	-[0x80001014]:sw t5, 1184(ra)
	-[0x80001018]:sw t6, 1192(ra)
	-[0x8000101c]:sw t5, 1200(ra)
Current Store : [0x8000101c] : sw t5, 1200(ra) -- Store: [0x80004f68]:0x8A1EDE0D




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000105c]:fmul.d t5, t3, s10, dyn
	-[0x80001060]:csrrs a6, fcsr, zero
	-[0x80001064]:sw t5, 1216(ra)
	-[0x80001068]:sw t6, 1224(ra)
Current Store : [0x80001068] : sw t6, 1224(ra) -- Store: [0x80004f80]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000105c]:fmul.d t5, t3, s10, dyn
	-[0x80001060]:csrrs a6, fcsr, zero
	-[0x80001064]:sw t5, 1216(ra)
	-[0x80001068]:sw t6, 1224(ra)
	-[0x8000106c]:sw t5, 1232(ra)
Current Store : [0x8000106c] : sw t5, 1232(ra) -- Store: [0x80004f88]:0x8A1EDE0E




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010ac]:fmul.d t5, t3, s10, dyn
	-[0x800010b0]:csrrs a6, fcsr, zero
	-[0x800010b4]:sw t5, 1248(ra)
	-[0x800010b8]:sw t6, 1256(ra)
Current Store : [0x800010b8] : sw t6, 1256(ra) -- Store: [0x80004fa0]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010ac]:fmul.d t5, t3, s10, dyn
	-[0x800010b0]:csrrs a6, fcsr, zero
	-[0x800010b4]:sw t5, 1248(ra)
	-[0x800010b8]:sw t6, 1256(ra)
	-[0x800010bc]:sw t5, 1264(ra)
Current Store : [0x800010bc] : sw t5, 1264(ra) -- Store: [0x80004fa8]:0x8A1EDE0D




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010fc]:fmul.d t5, t3, s10, dyn
	-[0x80001100]:csrrs a6, fcsr, zero
	-[0x80001104]:sw t5, 1280(ra)
	-[0x80001108]:sw t6, 1288(ra)
Current Store : [0x80001108] : sw t6, 1288(ra) -- Store: [0x80004fc0]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010fc]:fmul.d t5, t3, s10, dyn
	-[0x80001100]:csrrs a6, fcsr, zero
	-[0x80001104]:sw t5, 1280(ra)
	-[0x80001108]:sw t6, 1288(ra)
	-[0x8000110c]:sw t5, 1296(ra)
Current Store : [0x8000110c] : sw t5, 1296(ra) -- Store: [0x80004fc8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000114c]:fmul.d t5, t3, s10, dyn
	-[0x80001150]:csrrs a6, fcsr, zero
	-[0x80001154]:sw t5, 1312(ra)
	-[0x80001158]:sw t6, 1320(ra)
Current Store : [0x80001158] : sw t6, 1320(ra) -- Store: [0x80004fe0]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000114c]:fmul.d t5, t3, s10, dyn
	-[0x80001150]:csrrs a6, fcsr, zero
	-[0x80001154]:sw t5, 1312(ra)
	-[0x80001158]:sw t6, 1320(ra)
	-[0x8000115c]:sw t5, 1328(ra)
Current Store : [0x8000115c] : sw t5, 1328(ra) -- Store: [0x80004fe8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000119c]:fmul.d t5, t3, s10, dyn
	-[0x800011a0]:csrrs a6, fcsr, zero
	-[0x800011a4]:sw t5, 1344(ra)
	-[0x800011a8]:sw t6, 1352(ra)
Current Store : [0x800011a8] : sw t6, 1352(ra) -- Store: [0x80005000]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000119c]:fmul.d t5, t3, s10, dyn
	-[0x800011a0]:csrrs a6, fcsr, zero
	-[0x800011a4]:sw t5, 1344(ra)
	-[0x800011a8]:sw t6, 1352(ra)
	-[0x800011ac]:sw t5, 1360(ra)
Current Store : [0x800011ac] : sw t5, 1360(ra) -- Store: [0x80005008]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011ec]:fmul.d t5, t3, s10, dyn
	-[0x800011f0]:csrrs a6, fcsr, zero
	-[0x800011f4]:sw t5, 1376(ra)
	-[0x800011f8]:sw t6, 1384(ra)
Current Store : [0x800011f8] : sw t6, 1384(ra) -- Store: [0x80005020]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011ec]:fmul.d t5, t3, s10, dyn
	-[0x800011f0]:csrrs a6, fcsr, zero
	-[0x800011f4]:sw t5, 1376(ra)
	-[0x800011f8]:sw t6, 1384(ra)
	-[0x800011fc]:sw t5, 1392(ra)
Current Store : [0x800011fc] : sw t5, 1392(ra) -- Store: [0x80005028]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000123c]:fmul.d t5, t3, s10, dyn
	-[0x80001240]:csrrs a6, fcsr, zero
	-[0x80001244]:sw t5, 1408(ra)
	-[0x80001248]:sw t6, 1416(ra)
Current Store : [0x80001248] : sw t6, 1416(ra) -- Store: [0x80005040]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000123c]:fmul.d t5, t3, s10, dyn
	-[0x80001240]:csrrs a6, fcsr, zero
	-[0x80001244]:sw t5, 1408(ra)
	-[0x80001248]:sw t6, 1416(ra)
	-[0x8000124c]:sw t5, 1424(ra)
Current Store : [0x8000124c] : sw t5, 1424(ra) -- Store: [0x80005048]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000128c]:fmul.d t5, t3, s10, dyn
	-[0x80001290]:csrrs a6, fcsr, zero
	-[0x80001294]:sw t5, 1440(ra)
	-[0x80001298]:sw t6, 1448(ra)
Current Store : [0x80001298] : sw t6, 1448(ra) -- Store: [0x80005060]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000128c]:fmul.d t5, t3, s10, dyn
	-[0x80001290]:csrrs a6, fcsr, zero
	-[0x80001294]:sw t5, 1440(ra)
	-[0x80001298]:sw t6, 1448(ra)
	-[0x8000129c]:sw t5, 1456(ra)
Current Store : [0x8000129c] : sw t5, 1456(ra) -- Store: [0x80005068]:0x86A12B9A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012dc]:fmul.d t5, t3, s10, dyn
	-[0x800012e0]:csrrs a6, fcsr, zero
	-[0x800012e4]:sw t5, 1472(ra)
	-[0x800012e8]:sw t6, 1480(ra)
Current Store : [0x800012e8] : sw t6, 1480(ra) -- Store: [0x80005080]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012dc]:fmul.d t5, t3, s10, dyn
	-[0x800012e0]:csrrs a6, fcsr, zero
	-[0x800012e4]:sw t5, 1472(ra)
	-[0x800012e8]:sw t6, 1480(ra)
	-[0x800012ec]:sw t5, 1488(ra)
Current Store : [0x800012ec] : sw t5, 1488(ra) -- Store: [0x80005088]:0x86A12B9A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000132c]:fmul.d t5, t3, s10, dyn
	-[0x80001330]:csrrs a6, fcsr, zero
	-[0x80001334]:sw t5, 1504(ra)
	-[0x80001338]:sw t6, 1512(ra)
Current Store : [0x80001338] : sw t6, 1512(ra) -- Store: [0x800050a0]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000132c]:fmul.d t5, t3, s10, dyn
	-[0x80001330]:csrrs a6, fcsr, zero
	-[0x80001334]:sw t5, 1504(ra)
	-[0x80001338]:sw t6, 1512(ra)
	-[0x8000133c]:sw t5, 1520(ra)
Current Store : [0x8000133c] : sw t5, 1520(ra) -- Store: [0x800050a8]:0x86A12B9A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000137c]:fmul.d t5, t3, s10, dyn
	-[0x80001380]:csrrs a6, fcsr, zero
	-[0x80001384]:sw t5, 1536(ra)
	-[0x80001388]:sw t6, 1544(ra)
Current Store : [0x80001388] : sw t6, 1544(ra) -- Store: [0x800050c0]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000137c]:fmul.d t5, t3, s10, dyn
	-[0x80001380]:csrrs a6, fcsr, zero
	-[0x80001384]:sw t5, 1536(ra)
	-[0x80001388]:sw t6, 1544(ra)
	-[0x8000138c]:sw t5, 1552(ra)
Current Store : [0x8000138c] : sw t5, 1552(ra) -- Store: [0x800050c8]:0x86A12B9B




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013cc]:fmul.d t5, t3, s10, dyn
	-[0x800013d0]:csrrs a6, fcsr, zero
	-[0x800013d4]:sw t5, 1568(ra)
	-[0x800013d8]:sw t6, 1576(ra)
Current Store : [0x800013d8] : sw t6, 1576(ra) -- Store: [0x800050e0]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013cc]:fmul.d t5, t3, s10, dyn
	-[0x800013d0]:csrrs a6, fcsr, zero
	-[0x800013d4]:sw t5, 1568(ra)
	-[0x800013d8]:sw t6, 1576(ra)
	-[0x800013dc]:sw t5, 1584(ra)
Current Store : [0x800013dc] : sw t5, 1584(ra) -- Store: [0x800050e8]:0x86A12B9A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000141c]:fmul.d t5, t3, s10, dyn
	-[0x80001420]:csrrs a6, fcsr, zero
	-[0x80001424]:sw t5, 1600(ra)
	-[0x80001428]:sw t6, 1608(ra)
Current Store : [0x80001428] : sw t6, 1608(ra) -- Store: [0x80005100]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000141c]:fmul.d t5, t3, s10, dyn
	-[0x80001420]:csrrs a6, fcsr, zero
	-[0x80001424]:sw t5, 1600(ra)
	-[0x80001428]:sw t6, 1608(ra)
	-[0x8000142c]:sw t5, 1616(ra)
Current Store : [0x8000142c] : sw t5, 1616(ra) -- Store: [0x80005108]:0x8A1EDE0D




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000146c]:fmul.d t5, t3, s10, dyn
	-[0x80001470]:csrrs a6, fcsr, zero
	-[0x80001474]:sw t5, 1632(ra)
	-[0x80001478]:sw t6, 1640(ra)
Current Store : [0x80001478] : sw t6, 1640(ra) -- Store: [0x80005120]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000146c]:fmul.d t5, t3, s10, dyn
	-[0x80001470]:csrrs a6, fcsr, zero
	-[0x80001474]:sw t5, 1632(ra)
	-[0x80001478]:sw t6, 1640(ra)
	-[0x8000147c]:sw t5, 1648(ra)
Current Store : [0x8000147c] : sw t5, 1648(ra) -- Store: [0x80005128]:0x8A1EDE0D




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014bc]:fmul.d t5, t3, s10, dyn
	-[0x800014c0]:csrrs a6, fcsr, zero
	-[0x800014c4]:sw t5, 1664(ra)
	-[0x800014c8]:sw t6, 1672(ra)
Current Store : [0x800014c8] : sw t6, 1672(ra) -- Store: [0x80005140]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014bc]:fmul.d t5, t3, s10, dyn
	-[0x800014c0]:csrrs a6, fcsr, zero
	-[0x800014c4]:sw t5, 1664(ra)
	-[0x800014c8]:sw t6, 1672(ra)
	-[0x800014cc]:sw t5, 1680(ra)
Current Store : [0x800014cc] : sw t5, 1680(ra) -- Store: [0x80005148]:0x8A1EDE0D




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000150c]:fmul.d t5, t3, s10, dyn
	-[0x80001510]:csrrs a6, fcsr, zero
	-[0x80001514]:sw t5, 1696(ra)
	-[0x80001518]:sw t6, 1704(ra)
Current Store : [0x80001518] : sw t6, 1704(ra) -- Store: [0x80005160]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000150c]:fmul.d t5, t3, s10, dyn
	-[0x80001510]:csrrs a6, fcsr, zero
	-[0x80001514]:sw t5, 1696(ra)
	-[0x80001518]:sw t6, 1704(ra)
	-[0x8000151c]:sw t5, 1712(ra)
Current Store : [0x8000151c] : sw t5, 1712(ra) -- Store: [0x80005168]:0x8A1EDE0E




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000155c]:fmul.d t5, t3, s10, dyn
	-[0x80001560]:csrrs a6, fcsr, zero
	-[0x80001564]:sw t5, 1728(ra)
	-[0x80001568]:sw t6, 1736(ra)
Current Store : [0x80001568] : sw t6, 1736(ra) -- Store: [0x80005180]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000155c]:fmul.d t5, t3, s10, dyn
	-[0x80001560]:csrrs a6, fcsr, zero
	-[0x80001564]:sw t5, 1728(ra)
	-[0x80001568]:sw t6, 1736(ra)
	-[0x8000156c]:sw t5, 1744(ra)
Current Store : [0x8000156c] : sw t5, 1744(ra) -- Store: [0x80005188]:0x8A1EDE0D




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015ac]:fmul.d t5, t3, s10, dyn
	-[0x800015b0]:csrrs a6, fcsr, zero
	-[0x800015b4]:sw t5, 1760(ra)
	-[0x800015b8]:sw t6, 1768(ra)
Current Store : [0x800015b8] : sw t6, 1768(ra) -- Store: [0x800051a0]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015ac]:fmul.d t5, t3, s10, dyn
	-[0x800015b0]:csrrs a6, fcsr, zero
	-[0x800015b4]:sw t5, 1760(ra)
	-[0x800015b8]:sw t6, 1768(ra)
	-[0x800015bc]:sw t5, 1776(ra)
Current Store : [0x800015bc] : sw t5, 1776(ra) -- Store: [0x800051a8]:0xE5795FA6




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015fc]:fmul.d t5, t3, s10, dyn
	-[0x80001600]:csrrs a6, fcsr, zero
	-[0x80001604]:sw t5, 1792(ra)
	-[0x80001608]:sw t6, 1800(ra)
Current Store : [0x80001608] : sw t6, 1800(ra) -- Store: [0x800051c0]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015fc]:fmul.d t5, t3, s10, dyn
	-[0x80001600]:csrrs a6, fcsr, zero
	-[0x80001604]:sw t5, 1792(ra)
	-[0x80001608]:sw t6, 1800(ra)
	-[0x8000160c]:sw t5, 1808(ra)
Current Store : [0x8000160c] : sw t5, 1808(ra) -- Store: [0x800051c8]:0xE5795FA5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000164c]:fmul.d t5, t3, s10, dyn
	-[0x80001650]:csrrs a6, fcsr, zero
	-[0x80001654]:sw t5, 1824(ra)
	-[0x80001658]:sw t6, 1832(ra)
Current Store : [0x80001658] : sw t6, 1832(ra) -- Store: [0x800051e0]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000164c]:fmul.d t5, t3, s10, dyn
	-[0x80001650]:csrrs a6, fcsr, zero
	-[0x80001654]:sw t5, 1824(ra)
	-[0x80001658]:sw t6, 1832(ra)
	-[0x8000165c]:sw t5, 1840(ra)
Current Store : [0x8000165c] : sw t5, 1840(ra) -- Store: [0x800051e8]:0xE5795FA5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000169c]:fmul.d t5, t3, s10, dyn
	-[0x800016a0]:csrrs a6, fcsr, zero
	-[0x800016a4]:sw t5, 1856(ra)
	-[0x800016a8]:sw t6, 1864(ra)
Current Store : [0x800016a8] : sw t6, 1864(ra) -- Store: [0x80005200]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000169c]:fmul.d t5, t3, s10, dyn
	-[0x800016a0]:csrrs a6, fcsr, zero
	-[0x800016a4]:sw t5, 1856(ra)
	-[0x800016a8]:sw t6, 1864(ra)
	-[0x800016ac]:sw t5, 1872(ra)
Current Store : [0x800016ac] : sw t5, 1872(ra) -- Store: [0x80005208]:0xE5795FA6




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016ec]:fmul.d t5, t3, s10, dyn
	-[0x800016f0]:csrrs a6, fcsr, zero
	-[0x800016f4]:sw t5, 1888(ra)
	-[0x800016f8]:sw t6, 1896(ra)
Current Store : [0x800016f8] : sw t6, 1896(ra) -- Store: [0x80005220]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016ec]:fmul.d t5, t3, s10, dyn
	-[0x800016f0]:csrrs a6, fcsr, zero
	-[0x800016f4]:sw t5, 1888(ra)
	-[0x800016f8]:sw t6, 1896(ra)
	-[0x800016fc]:sw t5, 1904(ra)
Current Store : [0x800016fc] : sw t5, 1904(ra) -- Store: [0x80005228]:0xE5795FA6




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000173c]:fmul.d t5, t3, s10, dyn
	-[0x80001740]:csrrs a6, fcsr, zero
	-[0x80001744]:sw t5, 1920(ra)
	-[0x80001748]:sw t6, 1928(ra)
Current Store : [0x80001748] : sw t6, 1928(ra) -- Store: [0x80005240]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000173c]:fmul.d t5, t3, s10, dyn
	-[0x80001740]:csrrs a6, fcsr, zero
	-[0x80001744]:sw t5, 1920(ra)
	-[0x80001748]:sw t6, 1928(ra)
	-[0x8000174c]:sw t5, 1936(ra)
Current Store : [0x8000174c] : sw t5, 1936(ra) -- Store: [0x80005248]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000178c]:fmul.d t5, t3, s10, dyn
	-[0x80001790]:csrrs a6, fcsr, zero
	-[0x80001794]:sw t5, 1952(ra)
	-[0x80001798]:sw t6, 1960(ra)
Current Store : [0x80001798] : sw t6, 1960(ra) -- Store: [0x80005260]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000178c]:fmul.d t5, t3, s10, dyn
	-[0x80001790]:csrrs a6, fcsr, zero
	-[0x80001794]:sw t5, 1952(ra)
	-[0x80001798]:sw t6, 1960(ra)
	-[0x8000179c]:sw t5, 1968(ra)
Current Store : [0x8000179c] : sw t5, 1968(ra) -- Store: [0x80005268]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017dc]:fmul.d t5, t3, s10, dyn
	-[0x800017e0]:csrrs a6, fcsr, zero
	-[0x800017e4]:sw t5, 1984(ra)
	-[0x800017e8]:sw t6, 1992(ra)
Current Store : [0x800017e8] : sw t6, 1992(ra) -- Store: [0x80005280]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017dc]:fmul.d t5, t3, s10, dyn
	-[0x800017e0]:csrrs a6, fcsr, zero
	-[0x800017e4]:sw t5, 1984(ra)
	-[0x800017e8]:sw t6, 1992(ra)
	-[0x800017ec]:sw t5, 2000(ra)
Current Store : [0x800017ec] : sw t5, 2000(ra) -- Store: [0x80005288]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000182c]:fmul.d t5, t3, s10, dyn
	-[0x80001830]:csrrs a6, fcsr, zero
	-[0x80001834]:sw t5, 2016(ra)
	-[0x80001838]:sw t6, 2024(ra)
Current Store : [0x80001838] : sw t6, 2024(ra) -- Store: [0x800052a0]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000182c]:fmul.d t5, t3, s10, dyn
	-[0x80001830]:csrrs a6, fcsr, zero
	-[0x80001834]:sw t5, 2016(ra)
	-[0x80001838]:sw t6, 2024(ra)
	-[0x8000183c]:sw t5, 2032(ra)
Current Store : [0x8000183c] : sw t5, 2032(ra) -- Store: [0x800052a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000187c]:fmul.d t5, t3, s10, dyn
	-[0x80001880]:csrrs a6, fcsr, zero
	-[0x80001884]:addi ra, ra, 2040
	-[0x80001888]:sw t5, 8(ra)
	-[0x8000188c]:sw t6, 16(ra)
Current Store : [0x8000188c] : sw t6, 16(ra) -- Store: [0x800052c0]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000187c]:fmul.d t5, t3, s10, dyn
	-[0x80001880]:csrrs a6, fcsr, zero
	-[0x80001884]:addi ra, ra, 2040
	-[0x80001888]:sw t5, 8(ra)
	-[0x8000188c]:sw t6, 16(ra)
	-[0x80001890]:sw t5, 24(ra)
Current Store : [0x80001890] : sw t5, 24(ra) -- Store: [0x800052c8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x64204f3ac913b and fs2 == 0 and fe2 == 0x40f and fm2 == 0x700c9e9287c4d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018d0]:fmul.d t5, t3, s10, dyn
	-[0x800018d4]:csrrs a6, fcsr, zero
	-[0x800018d8]:sw t5, 40(ra)
	-[0x800018dc]:sw t6, 48(ra)
Current Store : [0x800018dc] : sw t6, 48(ra) -- Store: [0x800052e0]:0x39A4BD61




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x64204f3ac913b and fs2 == 0 and fe2 == 0x40f and fm2 == 0x700c9e9287c4d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018d0]:fmul.d t5, t3, s10, dyn
	-[0x800018d4]:csrrs a6, fcsr, zero
	-[0x800018d8]:sw t5, 40(ra)
	-[0x800018dc]:sw t6, 48(ra)
	-[0x800018e0]:sw t5, 56(ra)
Current Store : [0x800018e0] : sw t5, 56(ra) -- Store: [0x800052e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x64204f3ac913b and fs2 == 0 and fe2 == 0x40f and fm2 == 0x700c9e9287c4d and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001920]:fmul.d t5, t3, s10, dyn
	-[0x80001924]:csrrs a6, fcsr, zero
	-[0x80001928]:sw t5, 72(ra)
	-[0x8000192c]:sw t6, 80(ra)
Current Store : [0x8000192c] : sw t6, 80(ra) -- Store: [0x80005300]:0x39A4BD61





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                          signature                           |                                                                                                                                    coverpoints                                                                                                                                    |                                                                                                                    code                                                                                                                     |
|---:|--------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80004a18]<br>0xFFFFFFFF<br> [0x80004a30]<br>0x00000001<br> |- mnemonic : fmul.d<br> - rs1 : x28<br> - rs2 : x26<br> - rd : x30<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x39a and fm1 == 0x4bd6158ab1629 and fs2 == 0 and fe2 == 0x41d and fm2 == 0x8afd6f4cb48b9 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x8000014c]:fmul.d t5, t3, s10, dyn<br> [0x80000150]:csrrs tp, fcsr, zero<br> [0x80000154]:sw t5, 0(ra)<br> [0x80000158]:sw t6, 8(ra)<br> [0x8000015c]:sw t5, 16(ra)<br> [0x80000160]:sw tp, 24(ra)<br>                                     |
|   2|[0x80004a38]<br>0xFFFFFFFF<br> [0x80004a50]<br>0x00000021<br> |- rs1 : x26<br> - rs2 : x30<br> - rd : x26<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x39a and fm1 == 0x4bd6158ab1629 and fs2 == 0 and fe2 == 0x41d and fm2 == 0x8afd6f4cb48b9 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                               |[0x8000019c]:fmul.d s10, s10, t5, dyn<br> [0x800001a0]:csrrs tp, fcsr, zero<br> [0x800001a4]:sw s10, 32(ra)<br> [0x800001a8]:sw s11, 40(ra)<br> [0x800001ac]:sw s10, 48(ra)<br> [0x800001b0]:sw tp, 56(ra)<br>                               |
|   3|[0x80004a58]<br>0xEBCAC657<br> [0x80004a70]<br>0x00000041<br> |- rs1 : x24<br> - rs2 : x24<br> - rd : x24<br> - rs1 == rs2 == rd<br>                                                                                                                                                                                                              |[0x800001ec]:fmul.d s8, s8, s8, dyn<br> [0x800001f0]:csrrs tp, fcsr, zero<br> [0x800001f4]:sw s8, 64(ra)<br> [0x800001f8]:sw s9, 72(ra)<br> [0x800001fc]:sw s8, 80(ra)<br> [0x80000200]:sw tp, 88(ra)<br>                                    |
|   4|[0x80004a78]<br>0xEBCAC658<br> [0x80004a90]<br>0x00000061<br> |- rs1 : x22<br> - rs2 : x22<br> - rd : x28<br> - rs1 == rs2 != rd<br>                                                                                                                                                                                                              |[0x8000023c]:fmul.d t3, s6, s6, dyn<br> [0x80000240]:csrrs tp, fcsr, zero<br> [0x80000244]:sw t3, 96(ra)<br> [0x80000248]:sw t4, 104(ra)<br> [0x8000024c]:sw t3, 112(ra)<br> [0x80000250]:sw tp, 120(ra)<br>                                 |
|   5|[0x80004a98]<br>0xFFFFFFFF<br> [0x80004ab0]<br>0x00000081<br> |- rs1 : x30<br> - rs2 : x20<br> - rd : x20<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x39a and fm1 == 0x4bd6158ab1629 and fs2 == 0 and fe2 == 0x41d and fm2 == 0x8afd6f4cb48b9 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                               |[0x8000028c]:fmul.d s4, t5, s4, dyn<br> [0x80000290]:csrrs tp, fcsr, zero<br> [0x80000294]:sw s4, 128(ra)<br> [0x80000298]:sw s5, 136(ra)<br> [0x8000029c]:sw s4, 144(ra)<br> [0x800002a0]:sw tp, 152(ra)<br>                                |
|   6|[0x80004ab8]<br>0x00000000<br> [0x80004ad0]<br>0x00000001<br> |- rs1 : x20<br> - rs2 : x28<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                       |[0x800002dc]:fmul.d s6, s4, t3, dyn<br> [0x800002e0]:csrrs tp, fcsr, zero<br> [0x800002e4]:sw s6, 160(ra)<br> [0x800002e8]:sw s7, 168(ra)<br> [0x800002ec]:sw s6, 176(ra)<br> [0x800002f0]:sw tp, 184(ra)<br>                                |
|   7|[0x80004ad8]<br>0xFFFFFFFF<br> [0x80004af0]<br>0x00000021<br> |- rs1 : x16<br> - rs2 : x14<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                      |[0x8000032c]:fmul.d s2, a6, a4, dyn<br> [0x80000330]:csrrs tp, fcsr, zero<br> [0x80000334]:sw s2, 192(ra)<br> [0x80000338]:sw s3, 200(ra)<br> [0x8000033c]:sw s2, 208(ra)<br> [0x80000340]:sw tp, 216(ra)<br>                                |
|   8|[0x80004af8]<br>0xFFFFFFFF<br> [0x80004b10]<br>0x00000041<br> |- rs1 : x14<br> - rs2 : x18<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                      |[0x8000037c]:fmul.d a6, a4, s2, dyn<br> [0x80000380]:csrrs tp, fcsr, zero<br> [0x80000384]:sw a6, 224(ra)<br> [0x80000388]:sw a7, 232(ra)<br> [0x8000038c]:sw a6, 240(ra)<br> [0x80000390]:sw tp, 248(ra)<br>                                |
|   9|[0x80004b18]<br>0x00000000<br> [0x80004b30]<br>0x00000061<br> |- rs1 : x18<br> - rs2 : x16<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                      |[0x800003cc]:fmul.d a4, s2, a6, dyn<br> [0x800003d0]:csrrs tp, fcsr, zero<br> [0x800003d4]:sw a4, 256(ra)<br> [0x800003d8]:sw a5, 264(ra)<br> [0x800003dc]:sw a4, 272(ra)<br> [0x800003e0]:sw tp, 280(ra)<br>                                |
|  10|[0x80004b38]<br>0x00000000<br> [0x80004b50]<br>0x00000081<br> |- rs1 : x10<br> - rs2 : x8<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                       |[0x8000041c]:fmul.d a2, a0, fp, dyn<br> [0x80000420]:csrrs tp, fcsr, zero<br> [0x80000424]:sw a2, 288(ra)<br> [0x80000428]:sw a3, 296(ra)<br> [0x8000042c]:sw a2, 304(ra)<br> [0x80000430]:sw tp, 312(ra)<br>                                |
|  11|[0x80004ab8]<br>0x8A1EDE0E<br> [0x80004ad0]<br>0x00000001<br> |- rs1 : x8<br> - rs2 : x12<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                        |[0x8000047c]:fmul.d a0, fp, a2, dyn<br> [0x80000480]:csrrs a6, fcsr, zero<br> [0x80000484]:sw a0, 0(ra)<br> [0x80000488]:sw a1, 8(ra)<br> [0x8000048c]:sw a0, 16(ra)<br> [0x80000490]:sw a6, 24(ra)<br>                                      |
|  12|[0x80004ad8]<br>0x8A1EDE0D<br> [0x80004af0]<br>0x00000021<br> |- rs1 : x12<br> - rs2 : x10<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                       |[0x800004cc]:fmul.d fp, a2, a0, dyn<br> [0x800004d0]:csrrs a6, fcsr, zero<br> [0x800004d4]:sw fp, 32(ra)<br> [0x800004d8]:sw s1, 40(ra)<br> [0x800004dc]:sw fp, 48(ra)<br> [0x800004e0]:sw a6, 56(ra)<br>                                    |
|  13|[0x80004af8]<br>0x8A1EDE0D<br> [0x80004b10]<br>0x00000041<br> |- rs1 : x4<br> - rs2 : x2<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                         |[0x8000051c]:fmul.d t1, tp, sp, dyn<br> [0x80000520]:csrrs a6, fcsr, zero<br> [0x80000524]:sw t1, 64(ra)<br> [0x80000528]:sw t2, 72(ra)<br> [0x8000052c]:sw t1, 80(ra)<br> [0x80000530]:sw a6, 88(ra)<br>                                    |
|  14|[0x80004b18]<br>0x8A1EDE0E<br> [0x80004b30]<br>0x00000061<br> |- rs1 : x2<br> - rs2 : x6<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                         |[0x8000056c]:fmul.d tp, sp, t1, dyn<br> [0x80000570]:csrrs a6, fcsr, zero<br> [0x80000574]:sw tp, 96(ra)<br> [0x80000578]:sw t0, 104(ra)<br> [0x8000057c]:sw tp, 112(ra)<br> [0x80000580]:sw a6, 120(ra)<br>                                 |
|  15|[0x80004b38]<br>0x8A1EDE0E<br> [0x80004b50]<br>0x00000081<br> |- rs1 : x6<br> - rs2 : x4<br> - rd : x2<br> - fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                         |[0x800005bc]:fmul.d sp, t1, tp, dyn<br> [0x800005c0]:csrrs a6, fcsr, zero<br> [0x800005c4]:sw sp, 128(ra)<br> [0x800005c8]:sw gp, 136(ra)<br> [0x800005cc]:sw sp, 144(ra)<br> [0x800005d0]:sw a6, 152(ra)<br>                                |
|  16|[0x80004b58]<br>0x00000000<br> [0x80004b70]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000060c]:fmul.d t5, t3, s10, dyn<br> [0x80000610]:csrrs a6, fcsr, zero<br> [0x80000614]:sw t5, 160(ra)<br> [0x80000618]:sw t6, 168(ra)<br> [0x8000061c]:sw t5, 176(ra)<br> [0x80000620]:sw a6, 184(ra)<br>                               |
|  17|[0x80004b78]<br>0xFFFFFFFF<br> [0x80004b90]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000065c]:fmul.d t5, t3, s10, dyn<br> [0x80000660]:csrrs a6, fcsr, zero<br> [0x80000664]:sw t5, 192(ra)<br> [0x80000668]:sw t6, 200(ra)<br> [0x8000066c]:sw t5, 208(ra)<br> [0x80000670]:sw a6, 216(ra)<br>                               |
|  18|[0x80004b98]<br>0xFFFFFFFF<br> [0x80004bb0]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800006ac]:fmul.d t5, t3, s10, dyn<br> [0x800006b0]:csrrs a6, fcsr, zero<br> [0x800006b4]:sw t5, 224(ra)<br> [0x800006b8]:sw t6, 232(ra)<br> [0x800006bc]:sw t5, 240(ra)<br> [0x800006c0]:sw a6, 248(ra)<br>                               |
|  19|[0x80004bb8]<br>0x00000000<br> [0x80004bd0]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800006fc]:fmul.d t5, t3, s10, dyn<br> [0x80000700]:csrrs a6, fcsr, zero<br> [0x80000704]:sw t5, 256(ra)<br> [0x80000708]:sw t6, 264(ra)<br> [0x8000070c]:sw t5, 272(ra)<br> [0x80000710]:sw a6, 280(ra)<br>                               |
|  20|[0x80004bd8]<br>0x00000000<br> [0x80004bf0]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000074c]:fmul.d t5, t3, s10, dyn<br> [0x80000750]:csrrs a6, fcsr, zero<br> [0x80000754]:sw t5, 288(ra)<br> [0x80000758]:sw t6, 296(ra)<br> [0x8000075c]:sw t5, 304(ra)<br> [0x80000760]:sw a6, 312(ra)<br>                               |
|  21|[0x80004bf8]<br>0x86A12B9B<br> [0x80004c10]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000079c]:fmul.d t5, t3, s10, dyn<br> [0x800007a0]:csrrs a6, fcsr, zero<br> [0x800007a4]:sw t5, 320(ra)<br> [0x800007a8]:sw t6, 328(ra)<br> [0x800007ac]:sw t5, 336(ra)<br> [0x800007b0]:sw a6, 344(ra)<br>                               |
|  22|[0x80004c18]<br>0x86A12B9A<br> [0x80004c30]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800007ec]:fmul.d t5, t3, s10, dyn<br> [0x800007f0]:csrrs a6, fcsr, zero<br> [0x800007f4]:sw t5, 352(ra)<br> [0x800007f8]:sw t6, 360(ra)<br> [0x800007fc]:sw t5, 368(ra)<br> [0x80000800]:sw a6, 376(ra)<br>                               |
|  23|[0x80004c38]<br>0x86A12B9A<br> [0x80004c50]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000083c]:fmul.d t5, t3, s10, dyn<br> [0x80000840]:csrrs a6, fcsr, zero<br> [0x80000844]:sw t5, 384(ra)<br> [0x80000848]:sw t6, 392(ra)<br> [0x8000084c]:sw t5, 400(ra)<br> [0x80000850]:sw a6, 408(ra)<br>                               |
|  24|[0x80004c58]<br>0x86A12B9B<br> [0x80004c70]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000088c]:fmul.d t5, t3, s10, dyn<br> [0x80000890]:csrrs a6, fcsr, zero<br> [0x80000894]:sw t5, 416(ra)<br> [0x80000898]:sw t6, 424(ra)<br> [0x8000089c]:sw t5, 432(ra)<br> [0x800008a0]:sw a6, 440(ra)<br>                               |
|  25|[0x80004c78]<br>0x86A12B9B<br> [0x80004c90]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800008dc]:fmul.d t5, t3, s10, dyn<br> [0x800008e0]:csrrs a6, fcsr, zero<br> [0x800008e4]:sw t5, 448(ra)<br> [0x800008e8]:sw t6, 456(ra)<br> [0x800008ec]:sw t5, 464(ra)<br> [0x800008f0]:sw a6, 472(ra)<br>                               |
|  26|[0x80004c98]<br>0x8A1EDE0D<br> [0x80004cb0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000092c]:fmul.d t5, t3, s10, dyn<br> [0x80000930]:csrrs a6, fcsr, zero<br> [0x80000934]:sw t5, 480(ra)<br> [0x80000938]:sw t6, 488(ra)<br> [0x8000093c]:sw t5, 496(ra)<br> [0x80000940]:sw a6, 504(ra)<br>                               |
|  27|[0x80004cb8]<br>0x8A1EDE0D<br> [0x80004cd0]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000097c]:fmul.d t5, t3, s10, dyn<br> [0x80000980]:csrrs a6, fcsr, zero<br> [0x80000984]:sw t5, 512(ra)<br> [0x80000988]:sw t6, 520(ra)<br> [0x8000098c]:sw t5, 528(ra)<br> [0x80000990]:sw a6, 536(ra)<br>                               |
|  28|[0x80004cd8]<br>0x8A1EDE0D<br> [0x80004cf0]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800009cc]:fmul.d t5, t3, s10, dyn<br> [0x800009d0]:csrrs a6, fcsr, zero<br> [0x800009d4]:sw t5, 544(ra)<br> [0x800009d8]:sw t6, 552(ra)<br> [0x800009dc]:sw t5, 560(ra)<br> [0x800009e0]:sw a6, 568(ra)<br>                               |
|  29|[0x80004cf8]<br>0x8A1EDE0E<br> [0x80004d10]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000a1c]:fmul.d t5, t3, s10, dyn<br> [0x80000a20]:csrrs a6, fcsr, zero<br> [0x80000a24]:sw t5, 576(ra)<br> [0x80000a28]:sw t6, 584(ra)<br> [0x80000a2c]:sw t5, 592(ra)<br> [0x80000a30]:sw a6, 600(ra)<br>                               |
|  30|[0x80004d18]<br>0x8A1EDE0D<br> [0x80004d30]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000a6c]:fmul.d t5, t3, s10, dyn<br> [0x80000a70]:csrrs a6, fcsr, zero<br> [0x80000a74]:sw t5, 608(ra)<br> [0x80000a78]:sw t6, 616(ra)<br> [0x80000a7c]:sw t5, 624(ra)<br> [0x80000a80]:sw a6, 632(ra)<br>                               |
|  31|[0x80004d38]<br>0xE5795FA5<br> [0x80004d50]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000abc]:fmul.d t5, t3, s10, dyn<br> [0x80000ac0]:csrrs a6, fcsr, zero<br> [0x80000ac4]:sw t5, 640(ra)<br> [0x80000ac8]:sw t6, 648(ra)<br> [0x80000acc]:sw t5, 656(ra)<br> [0x80000ad0]:sw a6, 664(ra)<br>                               |
|  32|[0x80004d58]<br>0xE5795FA5<br> [0x80004d70]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000b0c]:fmul.d t5, t3, s10, dyn<br> [0x80000b10]:csrrs a6, fcsr, zero<br> [0x80000b14]:sw t5, 672(ra)<br> [0x80000b18]:sw t6, 680(ra)<br> [0x80000b1c]:sw t5, 688(ra)<br> [0x80000b20]:sw a6, 696(ra)<br>                               |
|  33|[0x80004d78]<br>0xE5795FA5<br> [0x80004d90]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000b5c]:fmul.d t5, t3, s10, dyn<br> [0x80000b60]:csrrs a6, fcsr, zero<br> [0x80000b64]:sw t5, 704(ra)<br> [0x80000b68]:sw t6, 712(ra)<br> [0x80000b6c]:sw t5, 720(ra)<br> [0x80000b70]:sw a6, 728(ra)<br>                               |
|  34|[0x80004d98]<br>0xE5795FA6<br> [0x80004db0]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000bac]:fmul.d t5, t3, s10, dyn<br> [0x80000bb0]:csrrs a6, fcsr, zero<br> [0x80000bb4]:sw t5, 736(ra)<br> [0x80000bb8]:sw t6, 744(ra)<br> [0x80000bbc]:sw t5, 752(ra)<br> [0x80000bc0]:sw a6, 760(ra)<br>                               |
|  35|[0x80004db8]<br>0xE5795FA5<br> [0x80004dd0]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000bfc]:fmul.d t5, t3, s10, dyn<br> [0x80000c00]:csrrs a6, fcsr, zero<br> [0x80000c04]:sw t5, 768(ra)<br> [0x80000c08]:sw t6, 776(ra)<br> [0x80000c0c]:sw t5, 784(ra)<br> [0x80000c10]:sw a6, 792(ra)<br>                               |
|  36|[0x80004dd8]<br>0x00000000<br> [0x80004df0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000c4c]:fmul.d t5, t3, s10, dyn<br> [0x80000c50]:csrrs a6, fcsr, zero<br> [0x80000c54]:sw t5, 800(ra)<br> [0x80000c58]:sw t6, 808(ra)<br> [0x80000c5c]:sw t5, 816(ra)<br> [0x80000c60]:sw a6, 824(ra)<br>                               |
|  37|[0x80004df8]<br>0x00000000<br> [0x80004e10]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000c9c]:fmul.d t5, t3, s10, dyn<br> [0x80000ca0]:csrrs a6, fcsr, zero<br> [0x80000ca4]:sw t5, 832(ra)<br> [0x80000ca8]:sw t6, 840(ra)<br> [0x80000cac]:sw t5, 848(ra)<br> [0x80000cb0]:sw a6, 856(ra)<br>                               |
|  38|[0x80004e18]<br>0x00000000<br> [0x80004e30]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000cec]:fmul.d t5, t3, s10, dyn<br> [0x80000cf0]:csrrs a6, fcsr, zero<br> [0x80000cf4]:sw t5, 864(ra)<br> [0x80000cf8]:sw t6, 872(ra)<br> [0x80000cfc]:sw t5, 880(ra)<br> [0x80000d00]:sw a6, 888(ra)<br>                               |
|  39|[0x80004e38]<br>0x00000001<br> [0x80004e50]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000d3c]:fmul.d t5, t3, s10, dyn<br> [0x80000d40]:csrrs a6, fcsr, zero<br> [0x80000d44]:sw t5, 896(ra)<br> [0x80000d48]:sw t6, 904(ra)<br> [0x80000d4c]:sw t5, 912(ra)<br> [0x80000d50]:sw a6, 920(ra)<br>                               |
|  40|[0x80004e58]<br>0x00000000<br> [0x80004e70]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000d8c]:fmul.d t5, t3, s10, dyn<br> [0x80000d90]:csrrs a6, fcsr, zero<br> [0x80000d94]:sw t5, 928(ra)<br> [0x80000d98]:sw t6, 936(ra)<br> [0x80000d9c]:sw t5, 944(ra)<br> [0x80000da0]:sw a6, 952(ra)<br>                               |
|  41|[0x80004e78]<br>0x00000000<br> [0x80004e90]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000ddc]:fmul.d t5, t3, s10, dyn<br> [0x80000de0]:csrrs a6, fcsr, zero<br> [0x80000de4]:sw t5, 960(ra)<br> [0x80000de8]:sw t6, 968(ra)<br> [0x80000dec]:sw t5, 976(ra)<br> [0x80000df0]:sw a6, 984(ra)<br>                               |
|  42|[0x80004e98]<br>0xFFFFFFFF<br> [0x80004eb0]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000e2c]:fmul.d t5, t3, s10, dyn<br> [0x80000e30]:csrrs a6, fcsr, zero<br> [0x80000e34]:sw t5, 992(ra)<br> [0x80000e38]:sw t6, 1000(ra)<br> [0x80000e3c]:sw t5, 1008(ra)<br> [0x80000e40]:sw a6, 1016(ra)<br>                            |
|  43|[0x80004eb8]<br>0xFFFFFFFF<br> [0x80004ed0]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000e7c]:fmul.d t5, t3, s10, dyn<br> [0x80000e80]:csrrs a6, fcsr, zero<br> [0x80000e84]:sw t5, 1024(ra)<br> [0x80000e88]:sw t6, 1032(ra)<br> [0x80000e8c]:sw t5, 1040(ra)<br> [0x80000e90]:sw a6, 1048(ra)<br>                           |
|  44|[0x80004ed8]<br>0x00000000<br> [0x80004ef0]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000ecc]:fmul.d t5, t3, s10, dyn<br> [0x80000ed0]:csrrs a6, fcsr, zero<br> [0x80000ed4]:sw t5, 1056(ra)<br> [0x80000ed8]:sw t6, 1064(ra)<br> [0x80000edc]:sw t5, 1072(ra)<br> [0x80000ee0]:sw a6, 1080(ra)<br>                           |
|  45|[0x80004ef8]<br>0x00000000<br> [0x80004f10]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000f1c]:fmul.d t5, t3, s10, dyn<br> [0x80000f20]:csrrs a6, fcsr, zero<br> [0x80000f24]:sw t5, 1088(ra)<br> [0x80000f28]:sw t6, 1096(ra)<br> [0x80000f2c]:sw t5, 1104(ra)<br> [0x80000f30]:sw a6, 1112(ra)<br>                           |
|  46|[0x80004f18]<br>0x8A1EDE0D<br> [0x80004f30]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000f6c]:fmul.d t5, t3, s10, dyn<br> [0x80000f70]:csrrs a6, fcsr, zero<br> [0x80000f74]:sw t5, 1120(ra)<br> [0x80000f78]:sw t6, 1128(ra)<br> [0x80000f7c]:sw t5, 1136(ra)<br> [0x80000f80]:sw a6, 1144(ra)<br>                           |
|  47|[0x80004f38]<br>0x8A1EDE0D<br> [0x80004f50]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x80000fbc]:fmul.d t5, t3, s10, dyn<br> [0x80000fc0]:csrrs a6, fcsr, zero<br> [0x80000fc4]:sw t5, 1152(ra)<br> [0x80000fc8]:sw t6, 1160(ra)<br> [0x80000fcc]:sw t5, 1168(ra)<br> [0x80000fd0]:sw a6, 1176(ra)<br>                           |
|  48|[0x80004f58]<br>0x8A1EDE0D<br> [0x80004f70]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000100c]:fmul.d t5, t3, s10, dyn<br> [0x80001010]:csrrs a6, fcsr, zero<br> [0x80001014]:sw t5, 1184(ra)<br> [0x80001018]:sw t6, 1192(ra)<br> [0x8000101c]:sw t5, 1200(ra)<br> [0x80001020]:sw a6, 1208(ra)<br>                           |
|  49|[0x80004f78]<br>0x8A1EDE0E<br> [0x80004f90]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000105c]:fmul.d t5, t3, s10, dyn<br> [0x80001060]:csrrs a6, fcsr, zero<br> [0x80001064]:sw t5, 1216(ra)<br> [0x80001068]:sw t6, 1224(ra)<br> [0x8000106c]:sw t5, 1232(ra)<br> [0x80001070]:sw a6, 1240(ra)<br>                           |
|  50|[0x80004f98]<br>0x8A1EDE0D<br> [0x80004fb0]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800010ac]:fmul.d t5, t3, s10, dyn<br> [0x800010b0]:csrrs a6, fcsr, zero<br> [0x800010b4]:sw t5, 1248(ra)<br> [0x800010b8]:sw t6, 1256(ra)<br> [0x800010bc]:sw t5, 1264(ra)<br> [0x800010c0]:sw a6, 1272(ra)<br>                           |
|  51|[0x80004fb8]<br>0x00000000<br> [0x80004fd0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800010fc]:fmul.d t5, t3, s10, dyn<br> [0x80001100]:csrrs a6, fcsr, zero<br> [0x80001104]:sw t5, 1280(ra)<br> [0x80001108]:sw t6, 1288(ra)<br> [0x8000110c]:sw t5, 1296(ra)<br> [0x80001110]:sw a6, 1304(ra)<br>                           |
|  52|[0x80004fd8]<br>0x00000000<br> [0x80004ff0]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000114c]:fmul.d t5, t3, s10, dyn<br> [0x80001150]:csrrs a6, fcsr, zero<br> [0x80001154]:sw t5, 1312(ra)<br> [0x80001158]:sw t6, 1320(ra)<br> [0x8000115c]:sw t5, 1328(ra)<br> [0x80001160]:sw a6, 1336(ra)<br>                           |
|  53|[0x80004ff8]<br>0x00000000<br> [0x80005010]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000119c]:fmul.d t5, t3, s10, dyn<br> [0x800011a0]:csrrs a6, fcsr, zero<br> [0x800011a4]:sw t5, 1344(ra)<br> [0x800011a8]:sw t6, 1352(ra)<br> [0x800011ac]:sw t5, 1360(ra)<br> [0x800011b0]:sw a6, 1368(ra)<br>                           |
|  54|[0x80005018]<br>0x00000001<br> [0x80005030]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800011ec]:fmul.d t5, t3, s10, dyn<br> [0x800011f0]:csrrs a6, fcsr, zero<br> [0x800011f4]:sw t5, 1376(ra)<br> [0x800011f8]:sw t6, 1384(ra)<br> [0x800011fc]:sw t5, 1392(ra)<br> [0x80001200]:sw a6, 1400(ra)<br>                           |
|  55|[0x80005038]<br>0x00000000<br> [0x80005050]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000123c]:fmul.d t5, t3, s10, dyn<br> [0x80001240]:csrrs a6, fcsr, zero<br> [0x80001244]:sw t5, 1408(ra)<br> [0x80001248]:sw t6, 1416(ra)<br> [0x8000124c]:sw t5, 1424(ra)<br> [0x80001250]:sw a6, 1432(ra)<br>                           |
|  56|[0x80005058]<br>0x86A12B9A<br> [0x80005070]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000128c]:fmul.d t5, t3, s10, dyn<br> [0x80001290]:csrrs a6, fcsr, zero<br> [0x80001294]:sw t5, 1440(ra)<br> [0x80001298]:sw t6, 1448(ra)<br> [0x8000129c]:sw t5, 1456(ra)<br> [0x800012a0]:sw a6, 1464(ra)<br>                           |
|  57|[0x80005078]<br>0x86A12B9A<br> [0x80005090]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800012dc]:fmul.d t5, t3, s10, dyn<br> [0x800012e0]:csrrs a6, fcsr, zero<br> [0x800012e4]:sw t5, 1472(ra)<br> [0x800012e8]:sw t6, 1480(ra)<br> [0x800012ec]:sw t5, 1488(ra)<br> [0x800012f0]:sw a6, 1496(ra)<br>                           |
|  58|[0x80005098]<br>0x86A12B9A<br> [0x800050b0]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000132c]:fmul.d t5, t3, s10, dyn<br> [0x80001330]:csrrs a6, fcsr, zero<br> [0x80001334]:sw t5, 1504(ra)<br> [0x80001338]:sw t6, 1512(ra)<br> [0x8000133c]:sw t5, 1520(ra)<br> [0x80001340]:sw a6, 1528(ra)<br>                           |
|  59|[0x800050b8]<br>0x86A12B9B<br> [0x800050d0]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000137c]:fmul.d t5, t3, s10, dyn<br> [0x80001380]:csrrs a6, fcsr, zero<br> [0x80001384]:sw t5, 1536(ra)<br> [0x80001388]:sw t6, 1544(ra)<br> [0x8000138c]:sw t5, 1552(ra)<br> [0x80001390]:sw a6, 1560(ra)<br>                           |
|  60|[0x800050d8]<br>0x86A12B9A<br> [0x800050f0]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800013cc]:fmul.d t5, t3, s10, dyn<br> [0x800013d0]:csrrs a6, fcsr, zero<br> [0x800013d4]:sw t5, 1568(ra)<br> [0x800013d8]:sw t6, 1576(ra)<br> [0x800013dc]:sw t5, 1584(ra)<br> [0x800013e0]:sw a6, 1592(ra)<br>                           |
|  61|[0x800050f8]<br>0x8A1EDE0D<br> [0x80005110]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000141c]:fmul.d t5, t3, s10, dyn<br> [0x80001420]:csrrs a6, fcsr, zero<br> [0x80001424]:sw t5, 1600(ra)<br> [0x80001428]:sw t6, 1608(ra)<br> [0x8000142c]:sw t5, 1616(ra)<br> [0x80001430]:sw a6, 1624(ra)<br>                           |
|  62|[0x80005118]<br>0x8A1EDE0D<br> [0x80005130]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000146c]:fmul.d t5, t3, s10, dyn<br> [0x80001470]:csrrs a6, fcsr, zero<br> [0x80001474]:sw t5, 1632(ra)<br> [0x80001478]:sw t6, 1640(ra)<br> [0x8000147c]:sw t5, 1648(ra)<br> [0x80001480]:sw a6, 1656(ra)<br>                           |
|  63|[0x80005138]<br>0x8A1EDE0D<br> [0x80005150]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800014bc]:fmul.d t5, t3, s10, dyn<br> [0x800014c0]:csrrs a6, fcsr, zero<br> [0x800014c4]:sw t5, 1664(ra)<br> [0x800014c8]:sw t6, 1672(ra)<br> [0x800014cc]:sw t5, 1680(ra)<br> [0x800014d0]:sw a6, 1688(ra)<br>                           |
|  64|[0x80005158]<br>0x8A1EDE0E<br> [0x80005170]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000150c]:fmul.d t5, t3, s10, dyn<br> [0x80001510]:csrrs a6, fcsr, zero<br> [0x80001514]:sw t5, 1696(ra)<br> [0x80001518]:sw t6, 1704(ra)<br> [0x8000151c]:sw t5, 1712(ra)<br> [0x80001520]:sw a6, 1720(ra)<br>                           |
|  65|[0x80005178]<br>0x8A1EDE0D<br> [0x80005190]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000155c]:fmul.d t5, t3, s10, dyn<br> [0x80001560]:csrrs a6, fcsr, zero<br> [0x80001564]:sw t5, 1728(ra)<br> [0x80001568]:sw t6, 1736(ra)<br> [0x8000156c]:sw t5, 1744(ra)<br> [0x80001570]:sw a6, 1752(ra)<br>                           |
|  66|[0x80005198]<br>0xE5795FA6<br> [0x800051b0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800015ac]:fmul.d t5, t3, s10, dyn<br> [0x800015b0]:csrrs a6, fcsr, zero<br> [0x800015b4]:sw t5, 1760(ra)<br> [0x800015b8]:sw t6, 1768(ra)<br> [0x800015bc]:sw t5, 1776(ra)<br> [0x800015c0]:sw a6, 1784(ra)<br>                           |
|  67|[0x800051b8]<br>0xE5795FA5<br> [0x800051d0]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800015fc]:fmul.d t5, t3, s10, dyn<br> [0x80001600]:csrrs a6, fcsr, zero<br> [0x80001604]:sw t5, 1792(ra)<br> [0x80001608]:sw t6, 1800(ra)<br> [0x8000160c]:sw t5, 1808(ra)<br> [0x80001610]:sw a6, 1816(ra)<br>                           |
|  68|[0x800051d8]<br>0xE5795FA5<br> [0x800051f0]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000164c]:fmul.d t5, t3, s10, dyn<br> [0x80001650]:csrrs a6, fcsr, zero<br> [0x80001654]:sw t5, 1824(ra)<br> [0x80001658]:sw t6, 1832(ra)<br> [0x8000165c]:sw t5, 1840(ra)<br> [0x80001660]:sw a6, 1848(ra)<br>                           |
|  69|[0x800051f8]<br>0xE5795FA6<br> [0x80005210]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000169c]:fmul.d t5, t3, s10, dyn<br> [0x800016a0]:csrrs a6, fcsr, zero<br> [0x800016a4]:sw t5, 1856(ra)<br> [0x800016a8]:sw t6, 1864(ra)<br> [0x800016ac]:sw t5, 1872(ra)<br> [0x800016b0]:sw a6, 1880(ra)<br>                           |
|  70|[0x80005218]<br>0xE5795FA6<br> [0x80005230]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800016ec]:fmul.d t5, t3, s10, dyn<br> [0x800016f0]:csrrs a6, fcsr, zero<br> [0x800016f4]:sw t5, 1888(ra)<br> [0x800016f8]:sw t6, 1896(ra)<br> [0x800016fc]:sw t5, 1904(ra)<br> [0x80001700]:sw a6, 1912(ra)<br>                           |
|  71|[0x80005238]<br>0xFFFFFFFF<br> [0x80005250]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000173c]:fmul.d t5, t3, s10, dyn<br> [0x80001740]:csrrs a6, fcsr, zero<br> [0x80001744]:sw t5, 1920(ra)<br> [0x80001748]:sw t6, 1928(ra)<br> [0x8000174c]:sw t5, 1936(ra)<br> [0x80001750]:sw a6, 1944(ra)<br>                           |
|  72|[0x80005258]<br>0xFFFFFFFF<br> [0x80005270]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000178c]:fmul.d t5, t3, s10, dyn<br> [0x80001790]:csrrs a6, fcsr, zero<br> [0x80001794]:sw t5, 1952(ra)<br> [0x80001798]:sw t6, 1960(ra)<br> [0x8000179c]:sw t5, 1968(ra)<br> [0x800017a0]:sw a6, 1976(ra)<br>                           |
|  73|[0x80005278]<br>0xFFFFFFFF<br> [0x80005290]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                                                     |[0x800017dc]:fmul.d t5, t3, s10, dyn<br> [0x800017e0]:csrrs a6, fcsr, zero<br> [0x800017e4]:sw t5, 1984(ra)<br> [0x800017e8]:sw t6, 1992(ra)<br> [0x800017ec]:sw t5, 2000(ra)<br> [0x800017f0]:sw a6, 2008(ra)<br>                           |
|  74|[0x80005298]<br>0x00000000<br> [0x800052b0]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000182c]:fmul.d t5, t3, s10, dyn<br> [0x80001830]:csrrs a6, fcsr, zero<br> [0x80001834]:sw t5, 2016(ra)<br> [0x80001838]:sw t6, 2024(ra)<br> [0x8000183c]:sw t5, 2032(ra)<br> [0x80001840]:sw a6, 2040(ra)<br>                           |
|  75|[0x800052b8]<br>0xFFFFFFFF<br> [0x800052d0]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                                                     |[0x8000187c]:fmul.d t5, t3, s10, dyn<br> [0x80001880]:csrrs a6, fcsr, zero<br> [0x80001884]:addi ra, ra, 2040<br> [0x80001888]:sw t5, 8(ra)<br> [0x8000188c]:sw t6, 16(ra)<br> [0x80001890]:sw t5, 24(ra)<br> [0x80001894]:sw a6, 32(ra)<br> |
|  76|[0x800052d8]<br>0x00000000<br> [0x800052f0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x39a and fm1 == 0x64204f3ac913b and fs2 == 0 and fe2 == 0x40f and fm2 == 0x700c9e9287c4d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800018d0]:fmul.d t5, t3, s10, dyn<br> [0x800018d4]:csrrs a6, fcsr, zero<br> [0x800018d8]:sw t5, 40(ra)<br> [0x800018dc]:sw t6, 48(ra)<br> [0x800018e0]:sw t5, 56(ra)<br> [0x800018e4]:sw a6, 64(ra)<br>                                   |
