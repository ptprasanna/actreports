
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x8000d4e0')]      |
| SIG_REGION                | [('0x80011610', '0x80013a40', '2316 words')]      |
| COV_LABELS                | fmul.d_b1      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-fmul/fmul.d_b1-01.S/ref.S    |
| Total Number of coverpoints| 627     |
| Total Coverpoints Hit     | 627      |
| Total Signature Updates   | 1804      |
| STAT1                     | 450      |
| STAT2                     | 0      |
| STAT3                     | 128     |
| STAT4                     | 902     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x800094cc]:fmul.d t5, t3, s10, dyn
[0x800094d0]:csrrs a6, fcsr, zero
[0x800094d4]:sw t5, 912(ra)
[0x800094d8]:sw t6, 920(ra)
[0x800094dc]:sw t5, 928(ra)
[0x800094e0]:sw a6, 936(ra)
[0x800094e4]:lui a4, 1
[0x800094e8]:addi a4, a4, 2048
[0x800094ec]:add a5, a5, a4
[0x800094f0]:lw t3, 464(a5)
[0x800094f4]:sub a5, a5, a4
[0x800094f8]:lui a4, 1
[0x800094fc]:addi a4, a4, 2048
[0x80009500]:add a5, a5, a4
[0x80009504]:lw t4, 468(a5)
[0x80009508]:sub a5, a5, a4
[0x8000950c]:lui a4, 1
[0x80009510]:addi a4, a4, 2048
[0x80009514]:add a5, a5, a4
[0x80009518]:lw s10, 472(a5)
[0x8000951c]:sub a5, a5, a4
[0x80009520]:lui a4, 1
[0x80009524]:addi a4, a4, 2048
[0x80009528]:add a5, a5, a4
[0x8000952c]:lw s11, 476(a5)
[0x80009530]:sub a5, a5, a4
[0x80009534]:addi t3, zero, 0
[0x80009538]:lui t4, 1048448
[0x8000953c]:addi s10, zero, 0
[0x80009540]:lui s11, 1048320
[0x80009544]:addi a4, zero, 0
[0x80009548]:csrrw zero, fcsr, a4
[0x8000954c]:fmul.d t5, t3, s10, dyn
[0x80009550]:csrrs a6, fcsr, zero

[0x8000954c]:fmul.d t5, t3, s10, dyn
[0x80009550]:csrrs a6, fcsr, zero
[0x80009554]:sw t5, 944(ra)
[0x80009558]:sw t6, 952(ra)
[0x8000955c]:sw t5, 960(ra)
[0x80009560]:sw a6, 968(ra)
[0x80009564]:lui a4, 1
[0x80009568]:addi a4, a4, 2048
[0x8000956c]:add a5, a5, a4
[0x80009570]:lw t3, 480(a5)
[0x80009574]:sub a5, a5, a4
[0x80009578]:lui a4, 1
[0x8000957c]:addi a4, a4, 2048
[0x80009580]:add a5, a5, a4
[0x80009584]:lw t4, 484(a5)
[0x80009588]:sub a5, a5, a4
[0x8000958c]:lui a4, 1
[0x80009590]:addi a4, a4, 2048
[0x80009594]:add a5, a5, a4
[0x80009598]:lw s10, 488(a5)
[0x8000959c]:sub a5, a5, a4
[0x800095a0]:lui a4, 1
[0x800095a4]:addi a4, a4, 2048
[0x800095a8]:add a5, a5, a4
[0x800095ac]:lw s11, 492(a5)
[0x800095b0]:sub a5, a5, a4
[0x800095b4]:addi t3, zero, 0
[0x800095b8]:lui t4, 1048448
[0x800095bc]:addi s10, zero, 0
[0x800095c0]:lui s11, 524160
[0x800095c4]:addi a4, zero, 0
[0x800095c8]:csrrw zero, fcsr, a4
[0x800095cc]:fmul.d t5, t3, s10, dyn
[0x800095d0]:csrrs a6, fcsr, zero

[0x800095cc]:fmul.d t5, t3, s10, dyn
[0x800095d0]:csrrs a6, fcsr, zero
[0x800095d4]:sw t5, 976(ra)
[0x800095d8]:sw t6, 984(ra)
[0x800095dc]:sw t5, 992(ra)
[0x800095e0]:sw a6, 1000(ra)
[0x800095e4]:lui a4, 1
[0x800095e8]:addi a4, a4, 2048
[0x800095ec]:add a5, a5, a4
[0x800095f0]:lw t3, 496(a5)
[0x800095f4]:sub a5, a5, a4
[0x800095f8]:lui a4, 1
[0x800095fc]:addi a4, a4, 2048
[0x80009600]:add a5, a5, a4
[0x80009604]:lw t4, 500(a5)
[0x80009608]:sub a5, a5, a4
[0x8000960c]:lui a4, 1
[0x80009610]:addi a4, a4, 2048
[0x80009614]:add a5, a5, a4
[0x80009618]:lw s10, 504(a5)
[0x8000961c]:sub a5, a5, a4
[0x80009620]:lui a4, 1
[0x80009624]:addi a4, a4, 2048
[0x80009628]:add a5, a5, a4
[0x8000962c]:lw s11, 508(a5)
[0x80009630]:sub a5, a5, a4
[0x80009634]:addi t3, zero, 0
[0x80009638]:lui t4, 1048448
[0x8000963c]:addi s10, zero, 0
[0x80009640]:lui s11, 1048448
[0x80009644]:addi a4, zero, 0
[0x80009648]:csrrw zero, fcsr, a4
[0x8000964c]:fmul.d t5, t3, s10, dyn
[0x80009650]:csrrs a6, fcsr, zero

[0x8000964c]:fmul.d t5, t3, s10, dyn
[0x80009650]:csrrs a6, fcsr, zero
[0x80009654]:sw t5, 1008(ra)
[0x80009658]:sw t6, 1016(ra)
[0x8000965c]:sw t5, 1024(ra)
[0x80009660]:sw a6, 1032(ra)
[0x80009664]:lui a4, 1
[0x80009668]:addi a4, a4, 2048
[0x8000966c]:add a5, a5, a4
[0x80009670]:lw t3, 512(a5)
[0x80009674]:sub a5, a5, a4
[0x80009678]:lui a4, 1
[0x8000967c]:addi a4, a4, 2048
[0x80009680]:add a5, a5, a4
[0x80009684]:lw t4, 516(a5)
[0x80009688]:sub a5, a5, a4
[0x8000968c]:lui a4, 1
[0x80009690]:addi a4, a4, 2048
[0x80009694]:add a5, a5, a4
[0x80009698]:lw s10, 520(a5)
[0x8000969c]:sub a5, a5, a4
[0x800096a0]:lui a4, 1
[0x800096a4]:addi a4, a4, 2048
[0x800096a8]:add a5, a5, a4
[0x800096ac]:lw s11, 524(a5)
[0x800096b0]:sub a5, a5, a4
[0x800096b4]:addi t3, zero, 0
[0x800096b8]:lui t4, 1048448
[0x800096bc]:addi s10, zero, 1
[0x800096c0]:lui s11, 524160
[0x800096c4]:addi a4, zero, 0
[0x800096c8]:csrrw zero, fcsr, a4
[0x800096cc]:fmul.d t5, t3, s10, dyn
[0x800096d0]:csrrs a6, fcsr, zero

[0x800096cc]:fmul.d t5, t3, s10, dyn
[0x800096d0]:csrrs a6, fcsr, zero
[0x800096d4]:sw t5, 1040(ra)
[0x800096d8]:sw t6, 1048(ra)
[0x800096dc]:sw t5, 1056(ra)
[0x800096e0]:sw a6, 1064(ra)
[0x800096e4]:lui a4, 1
[0x800096e8]:addi a4, a4, 2048
[0x800096ec]:add a5, a5, a4
[0x800096f0]:lw t3, 528(a5)
[0x800096f4]:sub a5, a5, a4
[0x800096f8]:lui a4, 1
[0x800096fc]:addi a4, a4, 2048
[0x80009700]:add a5, a5, a4
[0x80009704]:lw t4, 532(a5)
[0x80009708]:sub a5, a5, a4
[0x8000970c]:lui a4, 1
[0x80009710]:addi a4, a4, 2048
[0x80009714]:add a5, a5, a4
[0x80009718]:lw s10, 536(a5)
[0x8000971c]:sub a5, a5, a4
[0x80009720]:lui a4, 1
[0x80009724]:addi a4, a4, 2048
[0x80009728]:add a5, a5, a4
[0x8000972c]:lw s11, 540(a5)
[0x80009730]:sub a5, a5, a4
[0x80009734]:addi t3, zero, 0
[0x80009738]:lui t4, 1048448
[0x8000973c]:addi s10, zero, 1
[0x80009740]:lui s11, 1048448
[0x80009744]:addi a4, zero, 0
[0x80009748]:csrrw zero, fcsr, a4
[0x8000974c]:fmul.d t5, t3, s10, dyn
[0x80009750]:csrrs a6, fcsr, zero

[0x8000974c]:fmul.d t5, t3, s10, dyn
[0x80009750]:csrrs a6, fcsr, zero
[0x80009754]:sw t5, 1072(ra)
[0x80009758]:sw t6, 1080(ra)
[0x8000975c]:sw t5, 1088(ra)
[0x80009760]:sw a6, 1096(ra)
[0x80009764]:lui a4, 1
[0x80009768]:addi a4, a4, 2048
[0x8000976c]:add a5, a5, a4
[0x80009770]:lw t3, 544(a5)
[0x80009774]:sub a5, a5, a4
[0x80009778]:lui a4, 1
[0x8000977c]:addi a4, a4, 2048
[0x80009780]:add a5, a5, a4
[0x80009784]:lw t4, 548(a5)
[0x80009788]:sub a5, a5, a4
[0x8000978c]:lui a4, 1
[0x80009790]:addi a4, a4, 2048
[0x80009794]:add a5, a5, a4
[0x80009798]:lw s10, 552(a5)
[0x8000979c]:sub a5, a5, a4
[0x800097a0]:lui a4, 1
[0x800097a4]:addi a4, a4, 2048
[0x800097a8]:add a5, a5, a4
[0x800097ac]:lw s11, 556(a5)
[0x800097b0]:sub a5, a5, a4
[0x800097b4]:addi t3, zero, 0
[0x800097b8]:lui t4, 1048448
[0x800097bc]:addi s10, zero, 1
[0x800097c0]:lui s11, 524032
[0x800097c4]:addi a4, zero, 0
[0x800097c8]:csrrw zero, fcsr, a4
[0x800097cc]:fmul.d t5, t3, s10, dyn
[0x800097d0]:csrrs a6, fcsr, zero

[0x800097cc]:fmul.d t5, t3, s10, dyn
[0x800097d0]:csrrs a6, fcsr, zero
[0x800097d4]:sw t5, 1104(ra)
[0x800097d8]:sw t6, 1112(ra)
[0x800097dc]:sw t5, 1120(ra)
[0x800097e0]:sw a6, 1128(ra)
[0x800097e4]:lui a4, 1
[0x800097e8]:addi a4, a4, 2048
[0x800097ec]:add a5, a5, a4
[0x800097f0]:lw t3, 560(a5)
[0x800097f4]:sub a5, a5, a4
[0x800097f8]:lui a4, 1
[0x800097fc]:addi a4, a4, 2048
[0x80009800]:add a5, a5, a4
[0x80009804]:lw t4, 564(a5)
[0x80009808]:sub a5, a5, a4
[0x8000980c]:lui a4, 1
[0x80009810]:addi a4, a4, 2048
[0x80009814]:add a5, a5, a4
[0x80009818]:lw s10, 568(a5)
[0x8000981c]:sub a5, a5, a4
[0x80009820]:lui a4, 1
[0x80009824]:addi a4, a4, 2048
[0x80009828]:add a5, a5, a4
[0x8000982c]:lw s11, 572(a5)
[0x80009830]:sub a5, a5, a4
[0x80009834]:addi t3, zero, 0
[0x80009838]:lui t4, 1048448
[0x8000983c]:addi s10, zero, 1
[0x80009840]:lui s11, 1048320
[0x80009844]:addi a4, zero, 0
[0x80009848]:csrrw zero, fcsr, a4
[0x8000984c]:fmul.d t5, t3, s10, dyn
[0x80009850]:csrrs a6, fcsr, zero

[0x8000984c]:fmul.d t5, t3, s10, dyn
[0x80009850]:csrrs a6, fcsr, zero
[0x80009854]:sw t5, 1136(ra)
[0x80009858]:sw t6, 1144(ra)
[0x8000985c]:sw t5, 1152(ra)
[0x80009860]:sw a6, 1160(ra)
[0x80009864]:lui a4, 1
[0x80009868]:addi a4, a4, 2048
[0x8000986c]:add a5, a5, a4
[0x80009870]:lw t3, 576(a5)
[0x80009874]:sub a5, a5, a4
[0x80009878]:lui a4, 1
[0x8000987c]:addi a4, a4, 2048
[0x80009880]:add a5, a5, a4
[0x80009884]:lw t4, 580(a5)
[0x80009888]:sub a5, a5, a4
[0x8000988c]:lui a4, 1
[0x80009890]:addi a4, a4, 2048
[0x80009894]:add a5, a5, a4
[0x80009898]:lw s10, 584(a5)
[0x8000989c]:sub a5, a5, a4
[0x800098a0]:lui a4, 1
[0x800098a4]:addi a4, a4, 2048
[0x800098a8]:add a5, a5, a4
[0x800098ac]:lw s11, 588(a5)
[0x800098b0]:sub a5, a5, a4
[0x800098b4]:addi t3, zero, 0
[0x800098b8]:lui t4, 1048448
[0x800098bc]:addi s10, zero, 0
[0x800098c0]:lui s11, 261888
[0x800098c4]:addi a4, zero, 0
[0x800098c8]:csrrw zero, fcsr, a4
[0x800098cc]:fmul.d t5, t3, s10, dyn
[0x800098d0]:csrrs a6, fcsr, zero

[0x800098cc]:fmul.d t5, t3, s10, dyn
[0x800098d0]:csrrs a6, fcsr, zero
[0x800098d4]:sw t5, 1168(ra)
[0x800098d8]:sw t6, 1176(ra)
[0x800098dc]:sw t5, 1184(ra)
[0x800098e0]:sw a6, 1192(ra)
[0x800098e4]:lui a4, 1
[0x800098e8]:addi a4, a4, 2048
[0x800098ec]:add a5, a5, a4
[0x800098f0]:lw t3, 592(a5)
[0x800098f4]:sub a5, a5, a4
[0x800098f8]:lui a4, 1
[0x800098fc]:addi a4, a4, 2048
[0x80009900]:add a5, a5, a4
[0x80009904]:lw t4, 596(a5)
[0x80009908]:sub a5, a5, a4
[0x8000990c]:lui a4, 1
[0x80009910]:addi a4, a4, 2048
[0x80009914]:add a5, a5, a4
[0x80009918]:lw s10, 600(a5)
[0x8000991c]:sub a5, a5, a4
[0x80009920]:lui a4, 1
[0x80009924]:addi a4, a4, 2048
[0x80009928]:add a5, a5, a4
[0x8000992c]:lw s11, 604(a5)
[0x80009930]:sub a5, a5, a4
[0x80009934]:addi t3, zero, 0
[0x80009938]:lui t4, 1048448
[0x8000993c]:addi s10, zero, 0
[0x80009940]:lui s11, 784384
[0x80009944]:addi a4, zero, 0
[0x80009948]:csrrw zero, fcsr, a4
[0x8000994c]:fmul.d t5, t3, s10, dyn
[0x80009950]:csrrs a6, fcsr, zero

[0x8000994c]:fmul.d t5, t3, s10, dyn
[0x80009950]:csrrs a6, fcsr, zero
[0x80009954]:sw t5, 1200(ra)
[0x80009958]:sw t6, 1208(ra)
[0x8000995c]:sw t5, 1216(ra)
[0x80009960]:sw a6, 1224(ra)
[0x80009964]:lui a4, 1
[0x80009968]:addi a4, a4, 2048
[0x8000996c]:add a5, a5, a4
[0x80009970]:lw t3, 608(a5)
[0x80009974]:sub a5, a5, a4
[0x80009978]:lui a4, 1
[0x8000997c]:addi a4, a4, 2048
[0x80009980]:add a5, a5, a4
[0x80009984]:lw t4, 612(a5)
[0x80009988]:sub a5, a5, a4
[0x8000998c]:lui a4, 1
[0x80009990]:addi a4, a4, 2048
[0x80009994]:add a5, a5, a4
[0x80009998]:lw s10, 616(a5)
[0x8000999c]:sub a5, a5, a4
[0x800099a0]:lui a4, 1
[0x800099a4]:addi a4, a4, 2048
[0x800099a8]:add a5, a5, a4
[0x800099ac]:lw s11, 620(a5)
[0x800099b0]:sub a5, a5, a4
[0x800099b4]:addi t3, zero, 1
[0x800099b8]:lui t4, 524160
[0x800099bc]:addi s10, zero, 0
[0x800099c0]:addi s11, zero, 0
[0x800099c4]:addi a4, zero, 0
[0x800099c8]:csrrw zero, fcsr, a4
[0x800099cc]:fmul.d t5, t3, s10, dyn
[0x800099d0]:csrrs a6, fcsr, zero

[0x800099cc]:fmul.d t5, t3, s10, dyn
[0x800099d0]:csrrs a6, fcsr, zero
[0x800099d4]:sw t5, 1232(ra)
[0x800099d8]:sw t6, 1240(ra)
[0x800099dc]:sw t5, 1248(ra)
[0x800099e0]:sw a6, 1256(ra)
[0x800099e4]:lui a4, 1
[0x800099e8]:addi a4, a4, 2048
[0x800099ec]:add a5, a5, a4
[0x800099f0]:lw t3, 624(a5)
[0x800099f4]:sub a5, a5, a4
[0x800099f8]:lui a4, 1
[0x800099fc]:addi a4, a4, 2048
[0x80009a00]:add a5, a5, a4
[0x80009a04]:lw t4, 628(a5)
[0x80009a08]:sub a5, a5, a4
[0x80009a0c]:lui a4, 1
[0x80009a10]:addi a4, a4, 2048
[0x80009a14]:add a5, a5, a4
[0x80009a18]:lw s10, 632(a5)
[0x80009a1c]:sub a5, a5, a4
[0x80009a20]:lui a4, 1
[0x80009a24]:addi a4, a4, 2048
[0x80009a28]:add a5, a5, a4
[0x80009a2c]:lw s11, 636(a5)
[0x80009a30]:sub a5, a5, a4
[0x80009a34]:addi t3, zero, 1
[0x80009a38]:lui t4, 524160
[0x80009a3c]:addi s10, zero, 0
[0x80009a40]:lui s11, 524288
[0x80009a44]:addi a4, zero, 0
[0x80009a48]:csrrw zero, fcsr, a4
[0x80009a4c]:fmul.d t5, t3, s10, dyn
[0x80009a50]:csrrs a6, fcsr, zero

[0x80009a4c]:fmul.d t5, t3, s10, dyn
[0x80009a50]:csrrs a6, fcsr, zero
[0x80009a54]:sw t5, 1264(ra)
[0x80009a58]:sw t6, 1272(ra)
[0x80009a5c]:sw t5, 1280(ra)
[0x80009a60]:sw a6, 1288(ra)
[0x80009a64]:lui a4, 1
[0x80009a68]:addi a4, a4, 2048
[0x80009a6c]:add a5, a5, a4
[0x80009a70]:lw t3, 640(a5)
[0x80009a74]:sub a5, a5, a4
[0x80009a78]:lui a4, 1
[0x80009a7c]:addi a4, a4, 2048
[0x80009a80]:add a5, a5, a4
[0x80009a84]:lw t4, 644(a5)
[0x80009a88]:sub a5, a5, a4
[0x80009a8c]:lui a4, 1
[0x80009a90]:addi a4, a4, 2048
[0x80009a94]:add a5, a5, a4
[0x80009a98]:lw s10, 648(a5)
[0x80009a9c]:sub a5, a5, a4
[0x80009aa0]:lui a4, 1
[0x80009aa4]:addi a4, a4, 2048
[0x80009aa8]:add a5, a5, a4
[0x80009aac]:lw s11, 652(a5)
[0x80009ab0]:sub a5, a5, a4
[0x80009ab4]:addi t3, zero, 1
[0x80009ab8]:lui t4, 524160
[0x80009abc]:addi s10, zero, 1
[0x80009ac0]:addi s11, zero, 0
[0x80009ac4]:addi a4, zero, 0
[0x80009ac8]:csrrw zero, fcsr, a4
[0x80009acc]:fmul.d t5, t3, s10, dyn
[0x80009ad0]:csrrs a6, fcsr, zero

[0x80009acc]:fmul.d t5, t3, s10, dyn
[0x80009ad0]:csrrs a6, fcsr, zero
[0x80009ad4]:sw t5, 1296(ra)
[0x80009ad8]:sw t6, 1304(ra)
[0x80009adc]:sw t5, 1312(ra)
[0x80009ae0]:sw a6, 1320(ra)
[0x80009ae4]:lui a4, 1
[0x80009ae8]:addi a4, a4, 2048
[0x80009aec]:add a5, a5, a4
[0x80009af0]:lw t3, 656(a5)
[0x80009af4]:sub a5, a5, a4
[0x80009af8]:lui a4, 1
[0x80009afc]:addi a4, a4, 2048
[0x80009b00]:add a5, a5, a4
[0x80009b04]:lw t4, 660(a5)
[0x80009b08]:sub a5, a5, a4
[0x80009b0c]:lui a4, 1
[0x80009b10]:addi a4, a4, 2048
[0x80009b14]:add a5, a5, a4
[0x80009b18]:lw s10, 664(a5)
[0x80009b1c]:sub a5, a5, a4
[0x80009b20]:lui a4, 1
[0x80009b24]:addi a4, a4, 2048
[0x80009b28]:add a5, a5, a4
[0x80009b2c]:lw s11, 668(a5)
[0x80009b30]:sub a5, a5, a4
[0x80009b34]:addi t3, zero, 1
[0x80009b38]:lui t4, 524160
[0x80009b3c]:addi s10, zero, 1
[0x80009b40]:lui s11, 524288
[0x80009b44]:addi a4, zero, 0
[0x80009b48]:csrrw zero, fcsr, a4
[0x80009b4c]:fmul.d t5, t3, s10, dyn
[0x80009b50]:csrrs a6, fcsr, zero

[0x80009b4c]:fmul.d t5, t3, s10, dyn
[0x80009b50]:csrrs a6, fcsr, zero
[0x80009b54]:sw t5, 1328(ra)
[0x80009b58]:sw t6, 1336(ra)
[0x80009b5c]:sw t5, 1344(ra)
[0x80009b60]:sw a6, 1352(ra)
[0x80009b64]:lui a4, 1
[0x80009b68]:addi a4, a4, 2048
[0x80009b6c]:add a5, a5, a4
[0x80009b70]:lw t3, 672(a5)
[0x80009b74]:sub a5, a5, a4
[0x80009b78]:lui a4, 1
[0x80009b7c]:addi a4, a4, 2048
[0x80009b80]:add a5, a5, a4
[0x80009b84]:lw t4, 676(a5)
[0x80009b88]:sub a5, a5, a4
[0x80009b8c]:lui a4, 1
[0x80009b90]:addi a4, a4, 2048
[0x80009b94]:add a5, a5, a4
[0x80009b98]:lw s10, 680(a5)
[0x80009b9c]:sub a5, a5, a4
[0x80009ba0]:lui a4, 1
[0x80009ba4]:addi a4, a4, 2048
[0x80009ba8]:add a5, a5, a4
[0x80009bac]:lw s11, 684(a5)
[0x80009bb0]:sub a5, a5, a4
[0x80009bb4]:addi t3, zero, 1
[0x80009bb8]:lui t4, 524160
[0x80009bbc]:addi s10, zero, 2
[0x80009bc0]:addi s11, zero, 0
[0x80009bc4]:addi a4, zero, 0
[0x80009bc8]:csrrw zero, fcsr, a4
[0x80009bcc]:fmul.d t5, t3, s10, dyn
[0x80009bd0]:csrrs a6, fcsr, zero

[0x80009bcc]:fmul.d t5, t3, s10, dyn
[0x80009bd0]:csrrs a6, fcsr, zero
[0x80009bd4]:sw t5, 1360(ra)
[0x80009bd8]:sw t6, 1368(ra)
[0x80009bdc]:sw t5, 1376(ra)
[0x80009be0]:sw a6, 1384(ra)
[0x80009be4]:lui a4, 1
[0x80009be8]:addi a4, a4, 2048
[0x80009bec]:add a5, a5, a4
[0x80009bf0]:lw t3, 688(a5)
[0x80009bf4]:sub a5, a5, a4
[0x80009bf8]:lui a4, 1
[0x80009bfc]:addi a4, a4, 2048
[0x80009c00]:add a5, a5, a4
[0x80009c04]:lw t4, 692(a5)
[0x80009c08]:sub a5, a5, a4
[0x80009c0c]:lui a4, 1
[0x80009c10]:addi a4, a4, 2048
[0x80009c14]:add a5, a5, a4
[0x80009c18]:lw s10, 696(a5)
[0x80009c1c]:sub a5, a5, a4
[0x80009c20]:lui a4, 1
[0x80009c24]:addi a4, a4, 2048
[0x80009c28]:add a5, a5, a4
[0x80009c2c]:lw s11, 700(a5)
[0x80009c30]:sub a5, a5, a4
[0x80009c34]:addi t3, zero, 1
[0x80009c38]:lui t4, 524160
[0x80009c3c]:addi s10, zero, 2
[0x80009c40]:lui s11, 524288
[0x80009c44]:addi a4, zero, 0
[0x80009c48]:csrrw zero, fcsr, a4
[0x80009c4c]:fmul.d t5, t3, s10, dyn
[0x80009c50]:csrrs a6, fcsr, zero

[0x80009c4c]:fmul.d t5, t3, s10, dyn
[0x80009c50]:csrrs a6, fcsr, zero
[0x80009c54]:sw t5, 1392(ra)
[0x80009c58]:sw t6, 1400(ra)
[0x80009c5c]:sw t5, 1408(ra)
[0x80009c60]:sw a6, 1416(ra)
[0x80009c64]:lui a4, 1
[0x80009c68]:addi a4, a4, 2048
[0x80009c6c]:add a5, a5, a4
[0x80009c70]:lw t3, 704(a5)
[0x80009c74]:sub a5, a5, a4
[0x80009c78]:lui a4, 1
[0x80009c7c]:addi a4, a4, 2048
[0x80009c80]:add a5, a5, a4
[0x80009c84]:lw t4, 708(a5)
[0x80009c88]:sub a5, a5, a4
[0x80009c8c]:lui a4, 1
[0x80009c90]:addi a4, a4, 2048
[0x80009c94]:add a5, a5, a4
[0x80009c98]:lw s10, 712(a5)
[0x80009c9c]:sub a5, a5, a4
[0x80009ca0]:lui a4, 1
[0x80009ca4]:addi a4, a4, 2048
[0x80009ca8]:add a5, a5, a4
[0x80009cac]:lw s11, 716(a5)
[0x80009cb0]:sub a5, a5, a4
[0x80009cb4]:addi t3, zero, 1
[0x80009cb8]:lui t4, 524160
[0x80009cbc]:addi s10, zero, 4095
[0x80009cc0]:lui s11, 256
[0x80009cc4]:addi s11, s11, 4095
[0x80009cc8]:addi a4, zero, 0
[0x80009ccc]:csrrw zero, fcsr, a4
[0x80009cd0]:fmul.d t5, t3, s10, dyn
[0x80009cd4]:csrrs a6, fcsr, zero

[0x80009cd0]:fmul.d t5, t3, s10, dyn
[0x80009cd4]:csrrs a6, fcsr, zero
[0x80009cd8]:sw t5, 1424(ra)
[0x80009cdc]:sw t6, 1432(ra)
[0x80009ce0]:sw t5, 1440(ra)
[0x80009ce4]:sw a6, 1448(ra)
[0x80009ce8]:lui a4, 1
[0x80009cec]:addi a4, a4, 2048
[0x80009cf0]:add a5, a5, a4
[0x80009cf4]:lw t3, 720(a5)
[0x80009cf8]:sub a5, a5, a4
[0x80009cfc]:lui a4, 1
[0x80009d00]:addi a4, a4, 2048
[0x80009d04]:add a5, a5, a4
[0x80009d08]:lw t4, 724(a5)
[0x80009d0c]:sub a5, a5, a4
[0x80009d10]:lui a4, 1
[0x80009d14]:addi a4, a4, 2048
[0x80009d18]:add a5, a5, a4
[0x80009d1c]:lw s10, 728(a5)
[0x80009d20]:sub a5, a5, a4
[0x80009d24]:lui a4, 1
[0x80009d28]:addi a4, a4, 2048
[0x80009d2c]:add a5, a5, a4
[0x80009d30]:lw s11, 732(a5)
[0x80009d34]:sub a5, a5, a4
[0x80009d38]:addi t3, zero, 1
[0x80009d3c]:lui t4, 524160
[0x80009d40]:addi s10, zero, 4095
[0x80009d44]:lui s11, 524544
[0x80009d48]:addi s11, s11, 4095
[0x80009d4c]:addi a4, zero, 0
[0x80009d50]:csrrw zero, fcsr, a4
[0x80009d54]:fmul.d t5, t3, s10, dyn
[0x80009d58]:csrrs a6, fcsr, zero

[0x80009d54]:fmul.d t5, t3, s10, dyn
[0x80009d58]:csrrs a6, fcsr, zero
[0x80009d5c]:sw t5, 1456(ra)
[0x80009d60]:sw t6, 1464(ra)
[0x80009d64]:sw t5, 1472(ra)
[0x80009d68]:sw a6, 1480(ra)
[0x80009d6c]:lui a4, 1
[0x80009d70]:addi a4, a4, 2048
[0x80009d74]:add a5, a5, a4
[0x80009d78]:lw t3, 736(a5)
[0x80009d7c]:sub a5, a5, a4
[0x80009d80]:lui a4, 1
[0x80009d84]:addi a4, a4, 2048
[0x80009d88]:add a5, a5, a4
[0x80009d8c]:lw t4, 740(a5)
[0x80009d90]:sub a5, a5, a4
[0x80009d94]:lui a4, 1
[0x80009d98]:addi a4, a4, 2048
[0x80009d9c]:add a5, a5, a4
[0x80009da0]:lw s10, 744(a5)
[0x80009da4]:sub a5, a5, a4
[0x80009da8]:lui a4, 1
[0x80009dac]:addi a4, a4, 2048
[0x80009db0]:add a5, a5, a4
[0x80009db4]:lw s11, 748(a5)
[0x80009db8]:sub a5, a5, a4
[0x80009dbc]:addi t3, zero, 1
[0x80009dc0]:lui t4, 524160
[0x80009dc4]:addi s10, zero, 0
[0x80009dc8]:lui s11, 256
[0x80009dcc]:addi a4, zero, 0
[0x80009dd0]:csrrw zero, fcsr, a4
[0x80009dd4]:fmul.d t5, t3, s10, dyn
[0x80009dd8]:csrrs a6, fcsr, zero

[0x80009dd4]:fmul.d t5, t3, s10, dyn
[0x80009dd8]:csrrs a6, fcsr, zero
[0x80009ddc]:sw t5, 1488(ra)
[0x80009de0]:sw t6, 1496(ra)
[0x80009de4]:sw t5, 1504(ra)
[0x80009de8]:sw a6, 1512(ra)
[0x80009dec]:lui a4, 1
[0x80009df0]:addi a4, a4, 2048
[0x80009df4]:add a5, a5, a4
[0x80009df8]:lw t3, 752(a5)
[0x80009dfc]:sub a5, a5, a4
[0x80009e00]:lui a4, 1
[0x80009e04]:addi a4, a4, 2048
[0x80009e08]:add a5, a5, a4
[0x80009e0c]:lw t4, 756(a5)
[0x80009e10]:sub a5, a5, a4
[0x80009e14]:lui a4, 1
[0x80009e18]:addi a4, a4, 2048
[0x80009e1c]:add a5, a5, a4
[0x80009e20]:lw s10, 760(a5)
[0x80009e24]:sub a5, a5, a4
[0x80009e28]:lui a4, 1
[0x80009e2c]:addi a4, a4, 2048
[0x80009e30]:add a5, a5, a4
[0x80009e34]:lw s11, 764(a5)
[0x80009e38]:sub a5, a5, a4
[0x80009e3c]:addi t3, zero, 1
[0x80009e40]:lui t4, 524160
[0x80009e44]:addi s10, zero, 0
[0x80009e48]:lui s11, 524544
[0x80009e4c]:addi a4, zero, 0
[0x80009e50]:csrrw zero, fcsr, a4
[0x80009e54]:fmul.d t5, t3, s10, dyn
[0x80009e58]:csrrs a6, fcsr, zero

[0x80009e54]:fmul.d t5, t3, s10, dyn
[0x80009e58]:csrrs a6, fcsr, zero
[0x80009e5c]:sw t5, 1520(ra)
[0x80009e60]:sw t6, 1528(ra)
[0x80009e64]:sw t5, 1536(ra)
[0x80009e68]:sw a6, 1544(ra)
[0x80009e6c]:lui a4, 1
[0x80009e70]:addi a4, a4, 2048
[0x80009e74]:add a5, a5, a4
[0x80009e78]:lw t3, 768(a5)
[0x80009e7c]:sub a5, a5, a4
[0x80009e80]:lui a4, 1
[0x80009e84]:addi a4, a4, 2048
[0x80009e88]:add a5, a5, a4
[0x80009e8c]:lw t4, 772(a5)
[0x80009e90]:sub a5, a5, a4
[0x80009e94]:lui a4, 1
[0x80009e98]:addi a4, a4, 2048
[0x80009e9c]:add a5, a5, a4
[0x80009ea0]:lw s10, 776(a5)
[0x80009ea4]:sub a5, a5, a4
[0x80009ea8]:lui a4, 1
[0x80009eac]:addi a4, a4, 2048
[0x80009eb0]:add a5, a5, a4
[0x80009eb4]:lw s11, 780(a5)
[0x80009eb8]:sub a5, a5, a4
[0x80009ebc]:addi t3, zero, 1
[0x80009ec0]:lui t4, 524160
[0x80009ec4]:addi s10, zero, 2
[0x80009ec8]:lui s11, 256
[0x80009ecc]:addi a4, zero, 0
[0x80009ed0]:csrrw zero, fcsr, a4
[0x80009ed4]:fmul.d t5, t3, s10, dyn
[0x80009ed8]:csrrs a6, fcsr, zero

[0x80009ed4]:fmul.d t5, t3, s10, dyn
[0x80009ed8]:csrrs a6, fcsr, zero
[0x80009edc]:sw t5, 1552(ra)
[0x80009ee0]:sw t6, 1560(ra)
[0x80009ee4]:sw t5, 1568(ra)
[0x80009ee8]:sw a6, 1576(ra)
[0x80009eec]:lui a4, 1
[0x80009ef0]:addi a4, a4, 2048
[0x80009ef4]:add a5, a5, a4
[0x80009ef8]:lw t3, 784(a5)
[0x80009efc]:sub a5, a5, a4
[0x80009f00]:lui a4, 1
[0x80009f04]:addi a4, a4, 2048
[0x80009f08]:add a5, a5, a4
[0x80009f0c]:lw t4, 788(a5)
[0x80009f10]:sub a5, a5, a4
[0x80009f14]:lui a4, 1
[0x80009f18]:addi a4, a4, 2048
[0x80009f1c]:add a5, a5, a4
[0x80009f20]:lw s10, 792(a5)
[0x80009f24]:sub a5, a5, a4
[0x80009f28]:lui a4, 1
[0x80009f2c]:addi a4, a4, 2048
[0x80009f30]:add a5, a5, a4
[0x80009f34]:lw s11, 796(a5)
[0x80009f38]:sub a5, a5, a4
[0x80009f3c]:addi t3, zero, 1
[0x80009f40]:lui t4, 524160
[0x80009f44]:addi s10, zero, 2
[0x80009f48]:lui s11, 524544
[0x80009f4c]:addi a4, zero, 0
[0x80009f50]:csrrw zero, fcsr, a4
[0x80009f54]:fmul.d t5, t3, s10, dyn
[0x80009f58]:csrrs a6, fcsr, zero

[0x80009f54]:fmul.d t5, t3, s10, dyn
[0x80009f58]:csrrs a6, fcsr, zero
[0x80009f5c]:sw t5, 1584(ra)
[0x80009f60]:sw t6, 1592(ra)
[0x80009f64]:sw t5, 1600(ra)
[0x80009f68]:sw a6, 1608(ra)
[0x80009f6c]:lui a4, 1
[0x80009f70]:addi a4, a4, 2048
[0x80009f74]:add a5, a5, a4
[0x80009f78]:lw t3, 800(a5)
[0x80009f7c]:sub a5, a5, a4
[0x80009f80]:lui a4, 1
[0x80009f84]:addi a4, a4, 2048
[0x80009f88]:add a5, a5, a4
[0x80009f8c]:lw t4, 804(a5)
[0x80009f90]:sub a5, a5, a4
[0x80009f94]:lui a4, 1
[0x80009f98]:addi a4, a4, 2048
[0x80009f9c]:add a5, a5, a4
[0x80009fa0]:lw s10, 808(a5)
[0x80009fa4]:sub a5, a5, a4
[0x80009fa8]:lui a4, 1
[0x80009fac]:addi a4, a4, 2048
[0x80009fb0]:add a5, a5, a4
[0x80009fb4]:lw s11, 812(a5)
[0x80009fb8]:sub a5, a5, a4
[0x80009fbc]:addi t3, zero, 1
[0x80009fc0]:lui t4, 524160
[0x80009fc4]:addi s10, zero, 4095
[0x80009fc8]:lui s11, 524032
[0x80009fcc]:addi s11, s11, 4095
[0x80009fd0]:addi a4, zero, 0
[0x80009fd4]:csrrw zero, fcsr, a4
[0x80009fd8]:fmul.d t5, t3, s10, dyn
[0x80009fdc]:csrrs a6, fcsr, zero

[0x80009fd8]:fmul.d t5, t3, s10, dyn
[0x80009fdc]:csrrs a6, fcsr, zero
[0x80009fe0]:sw t5, 1616(ra)
[0x80009fe4]:sw t6, 1624(ra)
[0x80009fe8]:sw t5, 1632(ra)
[0x80009fec]:sw a6, 1640(ra)
[0x80009ff0]:lui a4, 1
[0x80009ff4]:addi a4, a4, 2048
[0x80009ff8]:add a5, a5, a4
[0x80009ffc]:lw t3, 816(a5)
[0x8000a000]:sub a5, a5, a4
[0x8000a004]:lui a4, 1
[0x8000a008]:addi a4, a4, 2048
[0x8000a00c]:add a5, a5, a4
[0x8000a010]:lw t4, 820(a5)
[0x8000a014]:sub a5, a5, a4
[0x8000a018]:lui a4, 1
[0x8000a01c]:addi a4, a4, 2048
[0x8000a020]:add a5, a5, a4
[0x8000a024]:lw s10, 824(a5)
[0x8000a028]:sub a5, a5, a4
[0x8000a02c]:lui a4, 1
[0x8000a030]:addi a4, a4, 2048
[0x8000a034]:add a5, a5, a4
[0x8000a038]:lw s11, 828(a5)
[0x8000a03c]:sub a5, a5, a4
[0x8000a040]:addi t3, zero, 1
[0x8000a044]:lui t4, 524160
[0x8000a048]:addi s10, zero, 4095
[0x8000a04c]:lui s11, 1048320
[0x8000a050]:addi s11, s11, 4095
[0x8000a054]:addi a4, zero, 0
[0x8000a058]:csrrw zero, fcsr, a4
[0x8000a05c]:fmul.d t5, t3, s10, dyn
[0x8000a060]:csrrs a6, fcsr, zero

[0x8000a05c]:fmul.d t5, t3, s10, dyn
[0x8000a060]:csrrs a6, fcsr, zero
[0x8000a064]:sw t5, 1648(ra)
[0x8000a068]:sw t6, 1656(ra)
[0x8000a06c]:sw t5, 1664(ra)
[0x8000a070]:sw a6, 1672(ra)
[0x8000a074]:lui a4, 1
[0x8000a078]:addi a4, a4, 2048
[0x8000a07c]:add a5, a5, a4
[0x8000a080]:lw t3, 832(a5)
[0x8000a084]:sub a5, a5, a4
[0x8000a088]:lui a4, 1
[0x8000a08c]:addi a4, a4, 2048
[0x8000a090]:add a5, a5, a4
[0x8000a094]:lw t4, 836(a5)
[0x8000a098]:sub a5, a5, a4
[0x8000a09c]:lui a4, 1
[0x8000a0a0]:addi a4, a4, 2048
[0x8000a0a4]:add a5, a5, a4
[0x8000a0a8]:lw s10, 840(a5)
[0x8000a0ac]:sub a5, a5, a4
[0x8000a0b0]:lui a4, 1
[0x8000a0b4]:addi a4, a4, 2048
[0x8000a0b8]:add a5, a5, a4
[0x8000a0bc]:lw s11, 844(a5)
[0x8000a0c0]:sub a5, a5, a4
[0x8000a0c4]:addi t3, zero, 1
[0x8000a0c8]:lui t4, 524160
[0x8000a0cc]:addi s10, zero, 0
[0x8000a0d0]:lui s11, 524032
[0x8000a0d4]:addi a4, zero, 0
[0x8000a0d8]:csrrw zero, fcsr, a4
[0x8000a0dc]:fmul.d t5, t3, s10, dyn
[0x8000a0e0]:csrrs a6, fcsr, zero

[0x8000a0dc]:fmul.d t5, t3, s10, dyn
[0x8000a0e0]:csrrs a6, fcsr, zero
[0x8000a0e4]:sw t5, 1680(ra)
[0x8000a0e8]:sw t6, 1688(ra)
[0x8000a0ec]:sw t5, 1696(ra)
[0x8000a0f0]:sw a6, 1704(ra)
[0x8000a0f4]:lui a4, 1
[0x8000a0f8]:addi a4, a4, 2048
[0x8000a0fc]:add a5, a5, a4
[0x8000a100]:lw t3, 848(a5)
[0x8000a104]:sub a5, a5, a4
[0x8000a108]:lui a4, 1
[0x8000a10c]:addi a4, a4, 2048
[0x8000a110]:add a5, a5, a4
[0x8000a114]:lw t4, 852(a5)
[0x8000a118]:sub a5, a5, a4
[0x8000a11c]:lui a4, 1
[0x8000a120]:addi a4, a4, 2048
[0x8000a124]:add a5, a5, a4
[0x8000a128]:lw s10, 856(a5)
[0x8000a12c]:sub a5, a5, a4
[0x8000a130]:lui a4, 1
[0x8000a134]:addi a4, a4, 2048
[0x8000a138]:add a5, a5, a4
[0x8000a13c]:lw s11, 860(a5)
[0x8000a140]:sub a5, a5, a4
[0x8000a144]:addi t3, zero, 1
[0x8000a148]:lui t4, 524160
[0x8000a14c]:addi s10, zero, 0
[0x8000a150]:lui s11, 1048320
[0x8000a154]:addi a4, zero, 0
[0x8000a158]:csrrw zero, fcsr, a4
[0x8000a15c]:fmul.d t5, t3, s10, dyn
[0x8000a160]:csrrs a6, fcsr, zero

[0x8000a15c]:fmul.d t5, t3, s10, dyn
[0x8000a160]:csrrs a6, fcsr, zero
[0x8000a164]:sw t5, 1712(ra)
[0x8000a168]:sw t6, 1720(ra)
[0x8000a16c]:sw t5, 1728(ra)
[0x8000a170]:sw a6, 1736(ra)
[0x8000a174]:lui a4, 1
[0x8000a178]:addi a4, a4, 2048
[0x8000a17c]:add a5, a5, a4
[0x8000a180]:lw t3, 864(a5)
[0x8000a184]:sub a5, a5, a4
[0x8000a188]:lui a4, 1
[0x8000a18c]:addi a4, a4, 2048
[0x8000a190]:add a5, a5, a4
[0x8000a194]:lw t4, 868(a5)
[0x8000a198]:sub a5, a5, a4
[0x8000a19c]:lui a4, 1
[0x8000a1a0]:addi a4, a4, 2048
[0x8000a1a4]:add a5, a5, a4
[0x8000a1a8]:lw s10, 872(a5)
[0x8000a1ac]:sub a5, a5, a4
[0x8000a1b0]:lui a4, 1
[0x8000a1b4]:addi a4, a4, 2048
[0x8000a1b8]:add a5, a5, a4
[0x8000a1bc]:lw s11, 876(a5)
[0x8000a1c0]:sub a5, a5, a4
[0x8000a1c4]:addi t3, zero, 1
[0x8000a1c8]:lui t4, 524160
[0x8000a1cc]:addi s10, zero, 0
[0x8000a1d0]:lui s11, 524160
[0x8000a1d4]:addi a4, zero, 0
[0x8000a1d8]:csrrw zero, fcsr, a4
[0x8000a1dc]:fmul.d t5, t3, s10, dyn
[0x8000a1e0]:csrrs a6, fcsr, zero

[0x8000a1dc]:fmul.d t5, t3, s10, dyn
[0x8000a1e0]:csrrs a6, fcsr, zero
[0x8000a1e4]:sw t5, 1744(ra)
[0x8000a1e8]:sw t6, 1752(ra)
[0x8000a1ec]:sw t5, 1760(ra)
[0x8000a1f0]:sw a6, 1768(ra)
[0x8000a1f4]:lui a4, 1
[0x8000a1f8]:addi a4, a4, 2048
[0x8000a1fc]:add a5, a5, a4
[0x8000a200]:lw t3, 880(a5)
[0x8000a204]:sub a5, a5, a4
[0x8000a208]:lui a4, 1
[0x8000a20c]:addi a4, a4, 2048
[0x8000a210]:add a5, a5, a4
[0x8000a214]:lw t4, 884(a5)
[0x8000a218]:sub a5, a5, a4
[0x8000a21c]:lui a4, 1
[0x8000a220]:addi a4, a4, 2048
[0x8000a224]:add a5, a5, a4
[0x8000a228]:lw s10, 888(a5)
[0x8000a22c]:sub a5, a5, a4
[0x8000a230]:lui a4, 1
[0x8000a234]:addi a4, a4, 2048
[0x8000a238]:add a5, a5, a4
[0x8000a23c]:lw s11, 892(a5)
[0x8000a240]:sub a5, a5, a4
[0x8000a244]:addi t3, zero, 1
[0x8000a248]:lui t4, 524160
[0x8000a24c]:addi s10, zero, 0
[0x8000a250]:lui s11, 1048448
[0x8000a254]:addi a4, zero, 0
[0x8000a258]:csrrw zero, fcsr, a4
[0x8000a25c]:fmul.d t5, t3, s10, dyn
[0x8000a260]:csrrs a6, fcsr, zero

[0x8000a25c]:fmul.d t5, t3, s10, dyn
[0x8000a260]:csrrs a6, fcsr, zero
[0x8000a264]:sw t5, 1776(ra)
[0x8000a268]:sw t6, 1784(ra)
[0x8000a26c]:sw t5, 1792(ra)
[0x8000a270]:sw a6, 1800(ra)
[0x8000a274]:lui a4, 1
[0x8000a278]:addi a4, a4, 2048
[0x8000a27c]:add a5, a5, a4
[0x8000a280]:lw t3, 896(a5)
[0x8000a284]:sub a5, a5, a4
[0x8000a288]:lui a4, 1
[0x8000a28c]:addi a4, a4, 2048
[0x8000a290]:add a5, a5, a4
[0x8000a294]:lw t4, 900(a5)
[0x8000a298]:sub a5, a5, a4
[0x8000a29c]:lui a4, 1
[0x8000a2a0]:addi a4, a4, 2048
[0x8000a2a4]:add a5, a5, a4
[0x8000a2a8]:lw s10, 904(a5)
[0x8000a2ac]:sub a5, a5, a4
[0x8000a2b0]:lui a4, 1
[0x8000a2b4]:addi a4, a4, 2048
[0x8000a2b8]:add a5, a5, a4
[0x8000a2bc]:lw s11, 908(a5)
[0x8000a2c0]:sub a5, a5, a4
[0x8000a2c4]:addi t3, zero, 1
[0x8000a2c8]:lui t4, 524160
[0x8000a2cc]:addi s10, zero, 1
[0x8000a2d0]:lui s11, 524160
[0x8000a2d4]:addi a4, zero, 0
[0x8000a2d8]:csrrw zero, fcsr, a4
[0x8000a2dc]:fmul.d t5, t3, s10, dyn
[0x8000a2e0]:csrrs a6, fcsr, zero

[0x8000a2dc]:fmul.d t5, t3, s10, dyn
[0x8000a2e0]:csrrs a6, fcsr, zero
[0x8000a2e4]:sw t5, 1808(ra)
[0x8000a2e8]:sw t6, 1816(ra)
[0x8000a2ec]:sw t5, 1824(ra)
[0x8000a2f0]:sw a6, 1832(ra)
[0x8000a2f4]:lui a4, 1
[0x8000a2f8]:addi a4, a4, 2048
[0x8000a2fc]:add a5, a5, a4
[0x8000a300]:lw t3, 912(a5)
[0x8000a304]:sub a5, a5, a4
[0x8000a308]:lui a4, 1
[0x8000a30c]:addi a4, a4, 2048
[0x8000a310]:add a5, a5, a4
[0x8000a314]:lw t4, 916(a5)
[0x8000a318]:sub a5, a5, a4
[0x8000a31c]:lui a4, 1
[0x8000a320]:addi a4, a4, 2048
[0x8000a324]:add a5, a5, a4
[0x8000a328]:lw s10, 920(a5)
[0x8000a32c]:sub a5, a5, a4
[0x8000a330]:lui a4, 1
[0x8000a334]:addi a4, a4, 2048
[0x8000a338]:add a5, a5, a4
[0x8000a33c]:lw s11, 924(a5)
[0x8000a340]:sub a5, a5, a4
[0x8000a344]:addi t3, zero, 1
[0x8000a348]:lui t4, 524160
[0x8000a34c]:addi s10, zero, 1
[0x8000a350]:lui s11, 1048448
[0x8000a354]:addi a4, zero, 0
[0x8000a358]:csrrw zero, fcsr, a4
[0x8000a35c]:fmul.d t5, t3, s10, dyn
[0x8000a360]:csrrs a6, fcsr, zero

[0x8000a35c]:fmul.d t5, t3, s10, dyn
[0x8000a360]:csrrs a6, fcsr, zero
[0x8000a364]:sw t5, 1840(ra)
[0x8000a368]:sw t6, 1848(ra)
[0x8000a36c]:sw t5, 1856(ra)
[0x8000a370]:sw a6, 1864(ra)
[0x8000a374]:lui a4, 1
[0x8000a378]:addi a4, a4, 2048
[0x8000a37c]:add a5, a5, a4
[0x8000a380]:lw t3, 928(a5)
[0x8000a384]:sub a5, a5, a4
[0x8000a388]:lui a4, 1
[0x8000a38c]:addi a4, a4, 2048
[0x8000a390]:add a5, a5, a4
[0x8000a394]:lw t4, 932(a5)
[0x8000a398]:sub a5, a5, a4
[0x8000a39c]:lui a4, 1
[0x8000a3a0]:addi a4, a4, 2048
[0x8000a3a4]:add a5, a5, a4
[0x8000a3a8]:lw s10, 936(a5)
[0x8000a3ac]:sub a5, a5, a4
[0x8000a3b0]:lui a4, 1
[0x8000a3b4]:addi a4, a4, 2048
[0x8000a3b8]:add a5, a5, a4
[0x8000a3bc]:lw s11, 940(a5)
[0x8000a3c0]:sub a5, a5, a4
[0x8000a3c4]:addi t3, zero, 1
[0x8000a3c8]:lui t4, 524160
[0x8000a3cc]:addi s10, zero, 1
[0x8000a3d0]:lui s11, 524032
[0x8000a3d4]:addi a4, zero, 0
[0x8000a3d8]:csrrw zero, fcsr, a4
[0x8000a3dc]:fmul.d t5, t3, s10, dyn
[0x8000a3e0]:csrrs a6, fcsr, zero

[0x8000a3dc]:fmul.d t5, t3, s10, dyn
[0x8000a3e0]:csrrs a6, fcsr, zero
[0x8000a3e4]:sw t5, 1872(ra)
[0x8000a3e8]:sw t6, 1880(ra)
[0x8000a3ec]:sw t5, 1888(ra)
[0x8000a3f0]:sw a6, 1896(ra)
[0x8000a3f4]:lui a4, 1
[0x8000a3f8]:addi a4, a4, 2048
[0x8000a3fc]:add a5, a5, a4
[0x8000a400]:lw t3, 944(a5)
[0x8000a404]:sub a5, a5, a4
[0x8000a408]:lui a4, 1
[0x8000a40c]:addi a4, a4, 2048
[0x8000a410]:add a5, a5, a4
[0x8000a414]:lw t4, 948(a5)
[0x8000a418]:sub a5, a5, a4
[0x8000a41c]:lui a4, 1
[0x8000a420]:addi a4, a4, 2048
[0x8000a424]:add a5, a5, a4
[0x8000a428]:lw s10, 952(a5)
[0x8000a42c]:sub a5, a5, a4
[0x8000a430]:lui a4, 1
[0x8000a434]:addi a4, a4, 2048
[0x8000a438]:add a5, a5, a4
[0x8000a43c]:lw s11, 956(a5)
[0x8000a440]:sub a5, a5, a4
[0x8000a444]:addi t3, zero, 1
[0x8000a448]:lui t4, 524160
[0x8000a44c]:addi s10, zero, 1
[0x8000a450]:lui s11, 1048320
[0x8000a454]:addi a4, zero, 0
[0x8000a458]:csrrw zero, fcsr, a4
[0x8000a45c]:fmul.d t5, t3, s10, dyn
[0x8000a460]:csrrs a6, fcsr, zero

[0x8000a45c]:fmul.d t5, t3, s10, dyn
[0x8000a460]:csrrs a6, fcsr, zero
[0x8000a464]:sw t5, 1904(ra)
[0x8000a468]:sw t6, 1912(ra)
[0x8000a46c]:sw t5, 1920(ra)
[0x8000a470]:sw a6, 1928(ra)
[0x8000a474]:lui a4, 1
[0x8000a478]:addi a4, a4, 2048
[0x8000a47c]:add a5, a5, a4
[0x8000a480]:lw t3, 960(a5)
[0x8000a484]:sub a5, a5, a4
[0x8000a488]:lui a4, 1
[0x8000a48c]:addi a4, a4, 2048
[0x8000a490]:add a5, a5, a4
[0x8000a494]:lw t4, 964(a5)
[0x8000a498]:sub a5, a5, a4
[0x8000a49c]:lui a4, 1
[0x8000a4a0]:addi a4, a4, 2048
[0x8000a4a4]:add a5, a5, a4
[0x8000a4a8]:lw s10, 968(a5)
[0x8000a4ac]:sub a5, a5, a4
[0x8000a4b0]:lui a4, 1
[0x8000a4b4]:addi a4, a4, 2048
[0x8000a4b8]:add a5, a5, a4
[0x8000a4bc]:lw s11, 972(a5)
[0x8000a4c0]:sub a5, a5, a4
[0x8000a4c4]:addi t3, zero, 1
[0x8000a4c8]:lui t4, 524160
[0x8000a4cc]:addi s10, zero, 0
[0x8000a4d0]:lui s11, 261888
[0x8000a4d4]:addi a4, zero, 0
[0x8000a4d8]:csrrw zero, fcsr, a4
[0x8000a4dc]:fmul.d t5, t3, s10, dyn
[0x8000a4e0]:csrrs a6, fcsr, zero

[0x8000a4dc]:fmul.d t5, t3, s10, dyn
[0x8000a4e0]:csrrs a6, fcsr, zero
[0x8000a4e4]:sw t5, 1936(ra)
[0x8000a4e8]:sw t6, 1944(ra)
[0x8000a4ec]:sw t5, 1952(ra)
[0x8000a4f0]:sw a6, 1960(ra)
[0x8000a4f4]:lui a4, 1
[0x8000a4f8]:addi a4, a4, 2048
[0x8000a4fc]:add a5, a5, a4
[0x8000a500]:lw t3, 976(a5)
[0x8000a504]:sub a5, a5, a4
[0x8000a508]:lui a4, 1
[0x8000a50c]:addi a4, a4, 2048
[0x8000a510]:add a5, a5, a4
[0x8000a514]:lw t4, 980(a5)
[0x8000a518]:sub a5, a5, a4
[0x8000a51c]:lui a4, 1
[0x8000a520]:addi a4, a4, 2048
[0x8000a524]:add a5, a5, a4
[0x8000a528]:lw s10, 984(a5)
[0x8000a52c]:sub a5, a5, a4
[0x8000a530]:lui a4, 1
[0x8000a534]:addi a4, a4, 2048
[0x8000a538]:add a5, a5, a4
[0x8000a53c]:lw s11, 988(a5)
[0x8000a540]:sub a5, a5, a4
[0x8000a544]:addi t3, zero, 1
[0x8000a548]:lui t4, 524160
[0x8000a54c]:addi s10, zero, 0
[0x8000a550]:lui s11, 784384
[0x8000a554]:addi a4, zero, 0
[0x8000a558]:csrrw zero, fcsr, a4
[0x8000a55c]:fmul.d t5, t3, s10, dyn
[0x8000a560]:csrrs a6, fcsr, zero

[0x8000a55c]:fmul.d t5, t3, s10, dyn
[0x8000a560]:csrrs a6, fcsr, zero
[0x8000a564]:sw t5, 1968(ra)
[0x8000a568]:sw t6, 1976(ra)
[0x8000a56c]:sw t5, 1984(ra)
[0x8000a570]:sw a6, 1992(ra)
[0x8000a574]:lui a4, 1
[0x8000a578]:addi a4, a4, 2048
[0x8000a57c]:add a5, a5, a4
[0x8000a580]:lw t3, 992(a5)
[0x8000a584]:sub a5, a5, a4
[0x8000a588]:lui a4, 1
[0x8000a58c]:addi a4, a4, 2048
[0x8000a590]:add a5, a5, a4
[0x8000a594]:lw t4, 996(a5)
[0x8000a598]:sub a5, a5, a4
[0x8000a59c]:lui a4, 1
[0x8000a5a0]:addi a4, a4, 2048
[0x8000a5a4]:add a5, a5, a4
[0x8000a5a8]:lw s10, 1000(a5)
[0x8000a5ac]:sub a5, a5, a4
[0x8000a5b0]:lui a4, 1
[0x8000a5b4]:addi a4, a4, 2048
[0x8000a5b8]:add a5, a5, a4
[0x8000a5bc]:lw s11, 1004(a5)
[0x8000a5c0]:sub a5, a5, a4
[0x8000a5c4]:addi t3, zero, 1
[0x8000a5c8]:lui t4, 1048448
[0x8000a5cc]:addi s10, zero, 0
[0x8000a5d0]:addi s11, zero, 0
[0x8000a5d4]:addi a4, zero, 0
[0x8000a5d8]:csrrw zero, fcsr, a4
[0x8000a5dc]:fmul.d t5, t3, s10, dyn
[0x8000a5e0]:csrrs a6, fcsr, zero

[0x8000a5dc]:fmul.d t5, t3, s10, dyn
[0x8000a5e0]:csrrs a6, fcsr, zero
[0x8000a5e4]:sw t5, 2000(ra)
[0x8000a5e8]:sw t6, 2008(ra)
[0x8000a5ec]:sw t5, 2016(ra)
[0x8000a5f0]:sw a6, 2024(ra)
[0x8000a5f4]:lui a4, 1
[0x8000a5f8]:addi a4, a4, 2048
[0x8000a5fc]:add a5, a5, a4
[0x8000a600]:lw t3, 1008(a5)
[0x8000a604]:sub a5, a5, a4
[0x8000a608]:lui a4, 1
[0x8000a60c]:addi a4, a4, 2048
[0x8000a610]:add a5, a5, a4
[0x8000a614]:lw t4, 1012(a5)
[0x8000a618]:sub a5, a5, a4
[0x8000a61c]:lui a4, 1
[0x8000a620]:addi a4, a4, 2048
[0x8000a624]:add a5, a5, a4
[0x8000a628]:lw s10, 1016(a5)
[0x8000a62c]:sub a5, a5, a4
[0x8000a630]:lui a4, 1
[0x8000a634]:addi a4, a4, 2048
[0x8000a638]:add a5, a5, a4
[0x8000a63c]:lw s11, 1020(a5)
[0x8000a640]:sub a5, a5, a4
[0x8000a644]:addi t3, zero, 1
[0x8000a648]:lui t4, 1048448
[0x8000a64c]:addi s10, zero, 0
[0x8000a650]:lui s11, 524288
[0x8000a654]:addi a4, zero, 0
[0x8000a658]:csrrw zero, fcsr, a4
[0x8000a65c]:fmul.d t5, t3, s10, dyn
[0x8000a660]:csrrs a6, fcsr, zero

[0x8000a65c]:fmul.d t5, t3, s10, dyn
[0x8000a660]:csrrs a6, fcsr, zero
[0x8000a664]:sw t5, 2032(ra)
[0x8000a668]:sw t6, 2040(ra)
[0x8000a66c]:addi ra, ra, 2040
[0x8000a670]:sw t5, 8(ra)
[0x8000a674]:sw a6, 16(ra)
[0x8000a678]:lui a4, 1
[0x8000a67c]:addi a4, a4, 2048
[0x8000a680]:add a5, a5, a4
[0x8000a684]:lw t3, 1024(a5)
[0x8000a688]:sub a5, a5, a4
[0x8000a68c]:lui a4, 1
[0x8000a690]:addi a4, a4, 2048
[0x8000a694]:add a5, a5, a4
[0x8000a698]:lw t4, 1028(a5)
[0x8000a69c]:sub a5, a5, a4
[0x8000a6a0]:lui a4, 1
[0x8000a6a4]:addi a4, a4, 2048
[0x8000a6a8]:add a5, a5, a4
[0x8000a6ac]:lw s10, 1032(a5)
[0x8000a6b0]:sub a5, a5, a4
[0x8000a6b4]:lui a4, 1
[0x8000a6b8]:addi a4, a4, 2048
[0x8000a6bc]:add a5, a5, a4
[0x8000a6c0]:lw s11, 1036(a5)
[0x8000a6c4]:sub a5, a5, a4
[0x8000a6c8]:addi t3, zero, 1
[0x8000a6cc]:lui t4, 1048448
[0x8000a6d0]:addi s10, zero, 1
[0x8000a6d4]:addi s11, zero, 0
[0x8000a6d8]:addi a4, zero, 0
[0x8000a6dc]:csrrw zero, fcsr, a4
[0x8000a6e0]:fmul.d t5, t3, s10, dyn
[0x8000a6e4]:csrrs a6, fcsr, zero

[0x8000a6e0]:fmul.d t5, t3, s10, dyn
[0x8000a6e4]:csrrs a6, fcsr, zero
[0x8000a6e8]:sw t5, 24(ra)
[0x8000a6ec]:sw t6, 32(ra)
[0x8000a6f0]:sw t5, 40(ra)
[0x8000a6f4]:sw a6, 48(ra)
[0x8000a6f8]:lui a4, 1
[0x8000a6fc]:addi a4, a4, 2048
[0x8000a700]:add a5, a5, a4
[0x8000a704]:lw t3, 1040(a5)
[0x8000a708]:sub a5, a5, a4
[0x8000a70c]:lui a4, 1
[0x8000a710]:addi a4, a4, 2048
[0x8000a714]:add a5, a5, a4
[0x8000a718]:lw t4, 1044(a5)
[0x8000a71c]:sub a5, a5, a4
[0x8000a720]:lui a4, 1
[0x8000a724]:addi a4, a4, 2048
[0x8000a728]:add a5, a5, a4
[0x8000a72c]:lw s10, 1048(a5)
[0x8000a730]:sub a5, a5, a4
[0x8000a734]:lui a4, 1
[0x8000a738]:addi a4, a4, 2048
[0x8000a73c]:add a5, a5, a4
[0x8000a740]:lw s11, 1052(a5)
[0x8000a744]:sub a5, a5, a4
[0x8000a748]:addi t3, zero, 1
[0x8000a74c]:lui t4, 1048448
[0x8000a750]:addi s10, zero, 1
[0x8000a754]:lui s11, 524288
[0x8000a758]:addi a4, zero, 0
[0x8000a75c]:csrrw zero, fcsr, a4
[0x8000a760]:fmul.d t5, t3, s10, dyn
[0x8000a764]:csrrs a6, fcsr, zero

[0x8000a760]:fmul.d t5, t3, s10, dyn
[0x8000a764]:csrrs a6, fcsr, zero
[0x8000a768]:sw t5, 56(ra)
[0x8000a76c]:sw t6, 64(ra)
[0x8000a770]:sw t5, 72(ra)
[0x8000a774]:sw a6, 80(ra)
[0x8000a778]:lui a4, 1
[0x8000a77c]:addi a4, a4, 2048
[0x8000a780]:add a5, a5, a4
[0x8000a784]:lw t3, 1056(a5)
[0x8000a788]:sub a5, a5, a4
[0x8000a78c]:lui a4, 1
[0x8000a790]:addi a4, a4, 2048
[0x8000a794]:add a5, a5, a4
[0x8000a798]:lw t4, 1060(a5)
[0x8000a79c]:sub a5, a5, a4
[0x8000a7a0]:lui a4, 1
[0x8000a7a4]:addi a4, a4, 2048
[0x8000a7a8]:add a5, a5, a4
[0x8000a7ac]:lw s10, 1064(a5)
[0x8000a7b0]:sub a5, a5, a4
[0x8000a7b4]:lui a4, 1
[0x8000a7b8]:addi a4, a4, 2048
[0x8000a7bc]:add a5, a5, a4
[0x8000a7c0]:lw s11, 1068(a5)
[0x8000a7c4]:sub a5, a5, a4
[0x8000a7c8]:addi t3, zero, 1
[0x8000a7cc]:lui t4, 1048448
[0x8000a7d0]:addi s10, zero, 2
[0x8000a7d4]:addi s11, zero, 0
[0x8000a7d8]:addi a4, zero, 0
[0x8000a7dc]:csrrw zero, fcsr, a4
[0x8000a7e0]:fmul.d t5, t3, s10, dyn
[0x8000a7e4]:csrrs a6, fcsr, zero

[0x8000a7e0]:fmul.d t5, t3, s10, dyn
[0x8000a7e4]:csrrs a6, fcsr, zero
[0x8000a7e8]:sw t5, 88(ra)
[0x8000a7ec]:sw t6, 96(ra)
[0x8000a7f0]:sw t5, 104(ra)
[0x8000a7f4]:sw a6, 112(ra)
[0x8000a7f8]:lui a4, 1
[0x8000a7fc]:addi a4, a4, 2048
[0x8000a800]:add a5, a5, a4
[0x8000a804]:lw t3, 1072(a5)
[0x8000a808]:sub a5, a5, a4
[0x8000a80c]:lui a4, 1
[0x8000a810]:addi a4, a4, 2048
[0x8000a814]:add a5, a5, a4
[0x8000a818]:lw t4, 1076(a5)
[0x8000a81c]:sub a5, a5, a4
[0x8000a820]:lui a4, 1
[0x8000a824]:addi a4, a4, 2048
[0x8000a828]:add a5, a5, a4
[0x8000a82c]:lw s10, 1080(a5)
[0x8000a830]:sub a5, a5, a4
[0x8000a834]:lui a4, 1
[0x8000a838]:addi a4, a4, 2048
[0x8000a83c]:add a5, a5, a4
[0x8000a840]:lw s11, 1084(a5)
[0x8000a844]:sub a5, a5, a4
[0x8000a848]:addi t3, zero, 1
[0x8000a84c]:lui t4, 1048448
[0x8000a850]:addi s10, zero, 2
[0x8000a854]:lui s11, 524288
[0x8000a858]:addi a4, zero, 0
[0x8000a85c]:csrrw zero, fcsr, a4
[0x8000a860]:fmul.d t5, t3, s10, dyn
[0x8000a864]:csrrs a6, fcsr, zero

[0x8000a860]:fmul.d t5, t3, s10, dyn
[0x8000a864]:csrrs a6, fcsr, zero
[0x8000a868]:sw t5, 120(ra)
[0x8000a86c]:sw t6, 128(ra)
[0x8000a870]:sw t5, 136(ra)
[0x8000a874]:sw a6, 144(ra)
[0x8000a878]:lui a4, 1
[0x8000a87c]:addi a4, a4, 2048
[0x8000a880]:add a5, a5, a4
[0x8000a884]:lw t3, 1088(a5)
[0x8000a888]:sub a5, a5, a4
[0x8000a88c]:lui a4, 1
[0x8000a890]:addi a4, a4, 2048
[0x8000a894]:add a5, a5, a4
[0x8000a898]:lw t4, 1092(a5)
[0x8000a89c]:sub a5, a5, a4
[0x8000a8a0]:lui a4, 1
[0x8000a8a4]:addi a4, a4, 2048
[0x8000a8a8]:add a5, a5, a4
[0x8000a8ac]:lw s10, 1096(a5)
[0x8000a8b0]:sub a5, a5, a4
[0x8000a8b4]:lui a4, 1
[0x8000a8b8]:addi a4, a4, 2048
[0x8000a8bc]:add a5, a5, a4
[0x8000a8c0]:lw s11, 1100(a5)
[0x8000a8c4]:sub a5, a5, a4
[0x8000a8c8]:addi t3, zero, 1
[0x8000a8cc]:lui t4, 1048448
[0x8000a8d0]:addi s10, zero, 4095
[0x8000a8d4]:lui s11, 256
[0x8000a8d8]:addi s11, s11, 4095
[0x8000a8dc]:addi a4, zero, 0
[0x8000a8e0]:csrrw zero, fcsr, a4
[0x8000a8e4]:fmul.d t5, t3, s10, dyn
[0x8000a8e8]:csrrs a6, fcsr, zero

[0x8000a8e4]:fmul.d t5, t3, s10, dyn
[0x8000a8e8]:csrrs a6, fcsr, zero
[0x8000a8ec]:sw t5, 152(ra)
[0x8000a8f0]:sw t6, 160(ra)
[0x8000a8f4]:sw t5, 168(ra)
[0x8000a8f8]:sw a6, 176(ra)
[0x8000a8fc]:lui a4, 1
[0x8000a900]:addi a4, a4, 2048
[0x8000a904]:add a5, a5, a4
[0x8000a908]:lw t3, 1104(a5)
[0x8000a90c]:sub a5, a5, a4
[0x8000a910]:lui a4, 1
[0x8000a914]:addi a4, a4, 2048
[0x8000a918]:add a5, a5, a4
[0x8000a91c]:lw t4, 1108(a5)
[0x8000a920]:sub a5, a5, a4
[0x8000a924]:lui a4, 1
[0x8000a928]:addi a4, a4, 2048
[0x8000a92c]:add a5, a5, a4
[0x8000a930]:lw s10, 1112(a5)
[0x8000a934]:sub a5, a5, a4
[0x8000a938]:lui a4, 1
[0x8000a93c]:addi a4, a4, 2048
[0x8000a940]:add a5, a5, a4
[0x8000a944]:lw s11, 1116(a5)
[0x8000a948]:sub a5, a5, a4
[0x8000a94c]:addi t3, zero, 1
[0x8000a950]:lui t4, 1048448
[0x8000a954]:addi s10, zero, 4095
[0x8000a958]:lui s11, 524544
[0x8000a95c]:addi s11, s11, 4095
[0x8000a960]:addi a4, zero, 0
[0x8000a964]:csrrw zero, fcsr, a4
[0x8000a968]:fmul.d t5, t3, s10, dyn
[0x8000a96c]:csrrs a6, fcsr, zero

[0x8000a968]:fmul.d t5, t3, s10, dyn
[0x8000a96c]:csrrs a6, fcsr, zero
[0x8000a970]:sw t5, 184(ra)
[0x8000a974]:sw t6, 192(ra)
[0x8000a978]:sw t5, 200(ra)
[0x8000a97c]:sw a6, 208(ra)
[0x8000a980]:lui a4, 1
[0x8000a984]:addi a4, a4, 2048
[0x8000a988]:add a5, a5, a4
[0x8000a98c]:lw t3, 1120(a5)
[0x8000a990]:sub a5, a5, a4
[0x8000a994]:lui a4, 1
[0x8000a998]:addi a4, a4, 2048
[0x8000a99c]:add a5, a5, a4
[0x8000a9a0]:lw t4, 1124(a5)
[0x8000a9a4]:sub a5, a5, a4
[0x8000a9a8]:lui a4, 1
[0x8000a9ac]:addi a4, a4, 2048
[0x8000a9b0]:add a5, a5, a4
[0x8000a9b4]:lw s10, 1128(a5)
[0x8000a9b8]:sub a5, a5, a4
[0x8000a9bc]:lui a4, 1
[0x8000a9c0]:addi a4, a4, 2048
[0x8000a9c4]:add a5, a5, a4
[0x8000a9c8]:lw s11, 1132(a5)
[0x8000a9cc]:sub a5, a5, a4
[0x8000a9d0]:addi t3, zero, 1
[0x8000a9d4]:lui t4, 1048448
[0x8000a9d8]:addi s10, zero, 0
[0x8000a9dc]:lui s11, 256
[0x8000a9e0]:addi a4, zero, 0
[0x8000a9e4]:csrrw zero, fcsr, a4
[0x8000a9e8]:fmul.d t5, t3, s10, dyn
[0x8000a9ec]:csrrs a6, fcsr, zero

[0x8000a9e8]:fmul.d t5, t3, s10, dyn
[0x8000a9ec]:csrrs a6, fcsr, zero
[0x8000a9f0]:sw t5, 216(ra)
[0x8000a9f4]:sw t6, 224(ra)
[0x8000a9f8]:sw t5, 232(ra)
[0x8000a9fc]:sw a6, 240(ra)
[0x8000aa00]:lui a4, 1
[0x8000aa04]:addi a4, a4, 2048
[0x8000aa08]:add a5, a5, a4
[0x8000aa0c]:lw t3, 1136(a5)
[0x8000aa10]:sub a5, a5, a4
[0x8000aa14]:lui a4, 1
[0x8000aa18]:addi a4, a4, 2048
[0x8000aa1c]:add a5, a5, a4
[0x8000aa20]:lw t4, 1140(a5)
[0x8000aa24]:sub a5, a5, a4
[0x8000aa28]:lui a4, 1
[0x8000aa2c]:addi a4, a4, 2048
[0x8000aa30]:add a5, a5, a4
[0x8000aa34]:lw s10, 1144(a5)
[0x8000aa38]:sub a5, a5, a4
[0x8000aa3c]:lui a4, 1
[0x8000aa40]:addi a4, a4, 2048
[0x8000aa44]:add a5, a5, a4
[0x8000aa48]:lw s11, 1148(a5)
[0x8000aa4c]:sub a5, a5, a4
[0x8000aa50]:addi t3, zero, 1
[0x8000aa54]:lui t4, 1048448
[0x8000aa58]:addi s10, zero, 0
[0x8000aa5c]:lui s11, 524544
[0x8000aa60]:addi a4, zero, 0
[0x8000aa64]:csrrw zero, fcsr, a4
[0x8000aa68]:fmul.d t5, t3, s10, dyn
[0x8000aa6c]:csrrs a6, fcsr, zero

[0x8000aa68]:fmul.d t5, t3, s10, dyn
[0x8000aa6c]:csrrs a6, fcsr, zero
[0x8000aa70]:sw t5, 248(ra)
[0x8000aa74]:sw t6, 256(ra)
[0x8000aa78]:sw t5, 264(ra)
[0x8000aa7c]:sw a6, 272(ra)
[0x8000aa80]:lui a4, 1
[0x8000aa84]:addi a4, a4, 2048
[0x8000aa88]:add a5, a5, a4
[0x8000aa8c]:lw t3, 1152(a5)
[0x8000aa90]:sub a5, a5, a4
[0x8000aa94]:lui a4, 1
[0x8000aa98]:addi a4, a4, 2048
[0x8000aa9c]:add a5, a5, a4
[0x8000aaa0]:lw t4, 1156(a5)
[0x8000aaa4]:sub a5, a5, a4
[0x8000aaa8]:lui a4, 1
[0x8000aaac]:addi a4, a4, 2048
[0x8000aab0]:add a5, a5, a4
[0x8000aab4]:lw s10, 1160(a5)
[0x8000aab8]:sub a5, a5, a4
[0x8000aabc]:lui a4, 1
[0x8000aac0]:addi a4, a4, 2048
[0x8000aac4]:add a5, a5, a4
[0x8000aac8]:lw s11, 1164(a5)
[0x8000aacc]:sub a5, a5, a4
[0x8000aad0]:addi t3, zero, 1
[0x8000aad4]:lui t4, 1048448
[0x8000aad8]:addi s10, zero, 2
[0x8000aadc]:lui s11, 256
[0x8000aae0]:addi a4, zero, 0
[0x8000aae4]:csrrw zero, fcsr, a4
[0x8000aae8]:fmul.d t5, t3, s10, dyn
[0x8000aaec]:csrrs a6, fcsr, zero

[0x8000aae8]:fmul.d t5, t3, s10, dyn
[0x8000aaec]:csrrs a6, fcsr, zero
[0x8000aaf0]:sw t5, 280(ra)
[0x8000aaf4]:sw t6, 288(ra)
[0x8000aaf8]:sw t5, 296(ra)
[0x8000aafc]:sw a6, 304(ra)
[0x8000ab00]:lui a4, 1
[0x8000ab04]:addi a4, a4, 2048
[0x8000ab08]:add a5, a5, a4
[0x8000ab0c]:lw t3, 1168(a5)
[0x8000ab10]:sub a5, a5, a4
[0x8000ab14]:lui a4, 1
[0x8000ab18]:addi a4, a4, 2048
[0x8000ab1c]:add a5, a5, a4
[0x8000ab20]:lw t4, 1172(a5)
[0x8000ab24]:sub a5, a5, a4
[0x8000ab28]:lui a4, 1
[0x8000ab2c]:addi a4, a4, 2048
[0x8000ab30]:add a5, a5, a4
[0x8000ab34]:lw s10, 1176(a5)
[0x8000ab38]:sub a5, a5, a4
[0x8000ab3c]:lui a4, 1
[0x8000ab40]:addi a4, a4, 2048
[0x8000ab44]:add a5, a5, a4
[0x8000ab48]:lw s11, 1180(a5)
[0x8000ab4c]:sub a5, a5, a4
[0x8000ab50]:addi t3, zero, 1
[0x8000ab54]:lui t4, 1048448
[0x8000ab58]:addi s10, zero, 2
[0x8000ab5c]:lui s11, 524544
[0x8000ab60]:addi a4, zero, 0
[0x8000ab64]:csrrw zero, fcsr, a4
[0x8000ab68]:fmul.d t5, t3, s10, dyn
[0x8000ab6c]:csrrs a6, fcsr, zero

[0x8000ab68]:fmul.d t5, t3, s10, dyn
[0x8000ab6c]:csrrs a6, fcsr, zero
[0x8000ab70]:sw t5, 312(ra)
[0x8000ab74]:sw t6, 320(ra)
[0x8000ab78]:sw t5, 328(ra)
[0x8000ab7c]:sw a6, 336(ra)
[0x8000ab80]:lui a4, 1
[0x8000ab84]:addi a4, a4, 2048
[0x8000ab88]:add a5, a5, a4
[0x8000ab8c]:lw t3, 1184(a5)
[0x8000ab90]:sub a5, a5, a4
[0x8000ab94]:lui a4, 1
[0x8000ab98]:addi a4, a4, 2048
[0x8000ab9c]:add a5, a5, a4
[0x8000aba0]:lw t4, 1188(a5)
[0x8000aba4]:sub a5, a5, a4
[0x8000aba8]:lui a4, 1
[0x8000abac]:addi a4, a4, 2048
[0x8000abb0]:add a5, a5, a4
[0x8000abb4]:lw s10, 1192(a5)
[0x8000abb8]:sub a5, a5, a4
[0x8000abbc]:lui a4, 1
[0x8000abc0]:addi a4, a4, 2048
[0x8000abc4]:add a5, a5, a4
[0x8000abc8]:lw s11, 1196(a5)
[0x8000abcc]:sub a5, a5, a4
[0x8000abd0]:addi t3, zero, 1
[0x8000abd4]:lui t4, 1048448
[0x8000abd8]:addi s10, zero, 4095
[0x8000abdc]:lui s11, 524032
[0x8000abe0]:addi s11, s11, 4095
[0x8000abe4]:addi a4, zero, 0
[0x8000abe8]:csrrw zero, fcsr, a4
[0x8000abec]:fmul.d t5, t3, s10, dyn
[0x8000abf0]:csrrs a6, fcsr, zero

[0x8000abec]:fmul.d t5, t3, s10, dyn
[0x8000abf0]:csrrs a6, fcsr, zero
[0x8000abf4]:sw t5, 344(ra)
[0x8000abf8]:sw t6, 352(ra)
[0x8000abfc]:sw t5, 360(ra)
[0x8000ac00]:sw a6, 368(ra)
[0x8000ac04]:lui a4, 1
[0x8000ac08]:addi a4, a4, 2048
[0x8000ac0c]:add a5, a5, a4
[0x8000ac10]:lw t3, 1200(a5)
[0x8000ac14]:sub a5, a5, a4
[0x8000ac18]:lui a4, 1
[0x8000ac1c]:addi a4, a4, 2048
[0x8000ac20]:add a5, a5, a4
[0x8000ac24]:lw t4, 1204(a5)
[0x8000ac28]:sub a5, a5, a4
[0x8000ac2c]:lui a4, 1
[0x8000ac30]:addi a4, a4, 2048
[0x8000ac34]:add a5, a5, a4
[0x8000ac38]:lw s10, 1208(a5)
[0x8000ac3c]:sub a5, a5, a4
[0x8000ac40]:lui a4, 1
[0x8000ac44]:addi a4, a4, 2048
[0x8000ac48]:add a5, a5, a4
[0x8000ac4c]:lw s11, 1212(a5)
[0x8000ac50]:sub a5, a5, a4
[0x8000ac54]:addi t3, zero, 1
[0x8000ac58]:lui t4, 1048448
[0x8000ac5c]:addi s10, zero, 4095
[0x8000ac60]:lui s11, 1048320
[0x8000ac64]:addi s11, s11, 4095
[0x8000ac68]:addi a4, zero, 0
[0x8000ac6c]:csrrw zero, fcsr, a4
[0x8000ac70]:fmul.d t5, t3, s10, dyn
[0x8000ac74]:csrrs a6, fcsr, zero

[0x8000ac70]:fmul.d t5, t3, s10, dyn
[0x8000ac74]:csrrs a6, fcsr, zero
[0x8000ac78]:sw t5, 376(ra)
[0x8000ac7c]:sw t6, 384(ra)
[0x8000ac80]:sw t5, 392(ra)
[0x8000ac84]:sw a6, 400(ra)
[0x8000ac88]:lui a4, 1
[0x8000ac8c]:addi a4, a4, 2048
[0x8000ac90]:add a5, a5, a4
[0x8000ac94]:lw t3, 1216(a5)
[0x8000ac98]:sub a5, a5, a4
[0x8000ac9c]:lui a4, 1
[0x8000aca0]:addi a4, a4, 2048
[0x8000aca4]:add a5, a5, a4
[0x8000aca8]:lw t4, 1220(a5)
[0x8000acac]:sub a5, a5, a4
[0x8000acb0]:lui a4, 1
[0x8000acb4]:addi a4, a4, 2048
[0x8000acb8]:add a5, a5, a4
[0x8000acbc]:lw s10, 1224(a5)
[0x8000acc0]:sub a5, a5, a4
[0x8000acc4]:lui a4, 1
[0x8000acc8]:addi a4, a4, 2048
[0x8000accc]:add a5, a5, a4
[0x8000acd0]:lw s11, 1228(a5)
[0x8000acd4]:sub a5, a5, a4
[0x8000acd8]:addi t3, zero, 1
[0x8000acdc]:lui t4, 1048448
[0x8000ace0]:addi s10, zero, 0
[0x8000ace4]:lui s11, 524032
[0x8000ace8]:addi a4, zero, 0
[0x8000acec]:csrrw zero, fcsr, a4
[0x8000acf0]:fmul.d t5, t3, s10, dyn
[0x8000acf4]:csrrs a6, fcsr, zero

[0x8000acf0]:fmul.d t5, t3, s10, dyn
[0x8000acf4]:csrrs a6, fcsr, zero
[0x8000acf8]:sw t5, 408(ra)
[0x8000acfc]:sw t6, 416(ra)
[0x8000ad00]:sw t5, 424(ra)
[0x8000ad04]:sw a6, 432(ra)
[0x8000ad08]:lui a4, 1
[0x8000ad0c]:addi a4, a4, 2048
[0x8000ad10]:add a5, a5, a4
[0x8000ad14]:lw t3, 1232(a5)
[0x8000ad18]:sub a5, a5, a4
[0x8000ad1c]:lui a4, 1
[0x8000ad20]:addi a4, a4, 2048
[0x8000ad24]:add a5, a5, a4
[0x8000ad28]:lw t4, 1236(a5)
[0x8000ad2c]:sub a5, a5, a4
[0x8000ad30]:lui a4, 1
[0x8000ad34]:addi a4, a4, 2048
[0x8000ad38]:add a5, a5, a4
[0x8000ad3c]:lw s10, 1240(a5)
[0x8000ad40]:sub a5, a5, a4
[0x8000ad44]:lui a4, 1
[0x8000ad48]:addi a4, a4, 2048
[0x8000ad4c]:add a5, a5, a4
[0x8000ad50]:lw s11, 1244(a5)
[0x8000ad54]:sub a5, a5, a4
[0x8000ad58]:addi t3, zero, 1
[0x8000ad5c]:lui t4, 1048448
[0x8000ad60]:addi s10, zero, 0
[0x8000ad64]:lui s11, 1048320
[0x8000ad68]:addi a4, zero, 0
[0x8000ad6c]:csrrw zero, fcsr, a4
[0x8000ad70]:fmul.d t5, t3, s10, dyn
[0x8000ad74]:csrrs a6, fcsr, zero

[0x8000ad70]:fmul.d t5, t3, s10, dyn
[0x8000ad74]:csrrs a6, fcsr, zero
[0x8000ad78]:sw t5, 440(ra)
[0x8000ad7c]:sw t6, 448(ra)
[0x8000ad80]:sw t5, 456(ra)
[0x8000ad84]:sw a6, 464(ra)
[0x8000ad88]:lui a4, 1
[0x8000ad8c]:addi a4, a4, 2048
[0x8000ad90]:add a5, a5, a4
[0x8000ad94]:lw t3, 1248(a5)
[0x8000ad98]:sub a5, a5, a4
[0x8000ad9c]:lui a4, 1
[0x8000ada0]:addi a4, a4, 2048
[0x8000ada4]:add a5, a5, a4
[0x8000ada8]:lw t4, 1252(a5)
[0x8000adac]:sub a5, a5, a4
[0x8000adb0]:lui a4, 1
[0x8000adb4]:addi a4, a4, 2048
[0x8000adb8]:add a5, a5, a4
[0x8000adbc]:lw s10, 1256(a5)
[0x8000adc0]:sub a5, a5, a4
[0x8000adc4]:lui a4, 1
[0x8000adc8]:addi a4, a4, 2048
[0x8000adcc]:add a5, a5, a4
[0x8000add0]:lw s11, 1260(a5)
[0x8000add4]:sub a5, a5, a4
[0x8000add8]:addi t3, zero, 1
[0x8000addc]:lui t4, 1048448
[0x8000ade0]:addi s10, zero, 0
[0x8000ade4]:lui s11, 524160
[0x8000ade8]:addi a4, zero, 0
[0x8000adec]:csrrw zero, fcsr, a4
[0x8000adf0]:fmul.d t5, t3, s10, dyn
[0x8000adf4]:csrrs a6, fcsr, zero

[0x8000adf0]:fmul.d t5, t3, s10, dyn
[0x8000adf4]:csrrs a6, fcsr, zero
[0x8000adf8]:sw t5, 472(ra)
[0x8000adfc]:sw t6, 480(ra)
[0x8000ae00]:sw t5, 488(ra)
[0x8000ae04]:sw a6, 496(ra)
[0x8000ae08]:lui a4, 1
[0x8000ae0c]:addi a4, a4, 2048
[0x8000ae10]:add a5, a5, a4
[0x8000ae14]:lw t3, 1264(a5)
[0x8000ae18]:sub a5, a5, a4
[0x8000ae1c]:lui a4, 1
[0x8000ae20]:addi a4, a4, 2048
[0x8000ae24]:add a5, a5, a4
[0x8000ae28]:lw t4, 1268(a5)
[0x8000ae2c]:sub a5, a5, a4
[0x8000ae30]:lui a4, 1
[0x8000ae34]:addi a4, a4, 2048
[0x8000ae38]:add a5, a5, a4
[0x8000ae3c]:lw s10, 1272(a5)
[0x8000ae40]:sub a5, a5, a4
[0x8000ae44]:lui a4, 1
[0x8000ae48]:addi a4, a4, 2048
[0x8000ae4c]:add a5, a5, a4
[0x8000ae50]:lw s11, 1276(a5)
[0x8000ae54]:sub a5, a5, a4
[0x8000ae58]:addi t3, zero, 1
[0x8000ae5c]:lui t4, 1048448
[0x8000ae60]:addi s10, zero, 0
[0x8000ae64]:lui s11, 1048448
[0x8000ae68]:addi a4, zero, 0
[0x8000ae6c]:csrrw zero, fcsr, a4
[0x8000ae70]:fmul.d t5, t3, s10, dyn
[0x8000ae74]:csrrs a6, fcsr, zero

[0x8000ae70]:fmul.d t5, t3, s10, dyn
[0x8000ae74]:csrrs a6, fcsr, zero
[0x8000ae78]:sw t5, 504(ra)
[0x8000ae7c]:sw t6, 512(ra)
[0x8000ae80]:sw t5, 520(ra)
[0x8000ae84]:sw a6, 528(ra)
[0x8000ae88]:lui a4, 1
[0x8000ae8c]:addi a4, a4, 2048
[0x8000ae90]:add a5, a5, a4
[0x8000ae94]:lw t3, 1280(a5)
[0x8000ae98]:sub a5, a5, a4
[0x8000ae9c]:lui a4, 1
[0x8000aea0]:addi a4, a4, 2048
[0x8000aea4]:add a5, a5, a4
[0x8000aea8]:lw t4, 1284(a5)
[0x8000aeac]:sub a5, a5, a4
[0x8000aeb0]:lui a4, 1
[0x8000aeb4]:addi a4, a4, 2048
[0x8000aeb8]:add a5, a5, a4
[0x8000aebc]:lw s10, 1288(a5)
[0x8000aec0]:sub a5, a5, a4
[0x8000aec4]:lui a4, 1
[0x8000aec8]:addi a4, a4, 2048
[0x8000aecc]:add a5, a5, a4
[0x8000aed0]:lw s11, 1292(a5)
[0x8000aed4]:sub a5, a5, a4
[0x8000aed8]:addi t3, zero, 1
[0x8000aedc]:lui t4, 1048448
[0x8000aee0]:addi s10, zero, 1
[0x8000aee4]:lui s11, 524160
[0x8000aee8]:addi a4, zero, 0
[0x8000aeec]:csrrw zero, fcsr, a4
[0x8000aef0]:fmul.d t5, t3, s10, dyn
[0x8000aef4]:csrrs a6, fcsr, zero

[0x8000aef0]:fmul.d t5, t3, s10, dyn
[0x8000aef4]:csrrs a6, fcsr, zero
[0x8000aef8]:sw t5, 536(ra)
[0x8000aefc]:sw t6, 544(ra)
[0x8000af00]:sw t5, 552(ra)
[0x8000af04]:sw a6, 560(ra)
[0x8000af08]:lui a4, 1
[0x8000af0c]:addi a4, a4, 2048
[0x8000af10]:add a5, a5, a4
[0x8000af14]:lw t3, 1296(a5)
[0x8000af18]:sub a5, a5, a4
[0x8000af1c]:lui a4, 1
[0x8000af20]:addi a4, a4, 2048
[0x8000af24]:add a5, a5, a4
[0x8000af28]:lw t4, 1300(a5)
[0x8000af2c]:sub a5, a5, a4
[0x8000af30]:lui a4, 1
[0x8000af34]:addi a4, a4, 2048
[0x8000af38]:add a5, a5, a4
[0x8000af3c]:lw s10, 1304(a5)
[0x8000af40]:sub a5, a5, a4
[0x8000af44]:lui a4, 1
[0x8000af48]:addi a4, a4, 2048
[0x8000af4c]:add a5, a5, a4
[0x8000af50]:lw s11, 1308(a5)
[0x8000af54]:sub a5, a5, a4
[0x8000af58]:addi t3, zero, 1
[0x8000af5c]:lui t4, 1048448
[0x8000af60]:addi s10, zero, 1
[0x8000af64]:lui s11, 1048448
[0x8000af68]:addi a4, zero, 0
[0x8000af6c]:csrrw zero, fcsr, a4
[0x8000af70]:fmul.d t5, t3, s10, dyn
[0x8000af74]:csrrs a6, fcsr, zero

[0x8000af70]:fmul.d t5, t3, s10, dyn
[0x8000af74]:csrrs a6, fcsr, zero
[0x8000af78]:sw t5, 568(ra)
[0x8000af7c]:sw t6, 576(ra)
[0x8000af80]:sw t5, 584(ra)
[0x8000af84]:sw a6, 592(ra)
[0x8000af88]:lui a4, 1
[0x8000af8c]:addi a4, a4, 2048
[0x8000af90]:add a5, a5, a4
[0x8000af94]:lw t3, 1312(a5)
[0x8000af98]:sub a5, a5, a4
[0x8000af9c]:lui a4, 1
[0x8000afa0]:addi a4, a4, 2048
[0x8000afa4]:add a5, a5, a4
[0x8000afa8]:lw t4, 1316(a5)
[0x8000afac]:sub a5, a5, a4
[0x8000afb0]:lui a4, 1
[0x8000afb4]:addi a4, a4, 2048
[0x8000afb8]:add a5, a5, a4
[0x8000afbc]:lw s10, 1320(a5)
[0x8000afc0]:sub a5, a5, a4
[0x8000afc4]:lui a4, 1
[0x8000afc8]:addi a4, a4, 2048
[0x8000afcc]:add a5, a5, a4
[0x8000afd0]:lw s11, 1324(a5)
[0x8000afd4]:sub a5, a5, a4
[0x8000afd8]:addi t3, zero, 1
[0x8000afdc]:lui t4, 1048448
[0x8000afe0]:addi s10, zero, 1
[0x8000afe4]:lui s11, 524032
[0x8000afe8]:addi a4, zero, 0
[0x8000afec]:csrrw zero, fcsr, a4
[0x8000aff0]:fmul.d t5, t3, s10, dyn
[0x8000aff4]:csrrs a6, fcsr, zero

[0x8000aff0]:fmul.d t5, t3, s10, dyn
[0x8000aff4]:csrrs a6, fcsr, zero
[0x8000aff8]:sw t5, 600(ra)
[0x8000affc]:sw t6, 608(ra)
[0x8000b000]:sw t5, 616(ra)
[0x8000b004]:sw a6, 624(ra)
[0x8000b008]:lui a4, 1
[0x8000b00c]:addi a4, a4, 2048
[0x8000b010]:add a5, a5, a4
[0x8000b014]:lw t3, 1328(a5)
[0x8000b018]:sub a5, a5, a4
[0x8000b01c]:lui a4, 1
[0x8000b020]:addi a4, a4, 2048
[0x8000b024]:add a5, a5, a4
[0x8000b028]:lw t4, 1332(a5)
[0x8000b02c]:sub a5, a5, a4
[0x8000b030]:lui a4, 1
[0x8000b034]:addi a4, a4, 2048
[0x8000b038]:add a5, a5, a4
[0x8000b03c]:lw s10, 1336(a5)
[0x8000b040]:sub a5, a5, a4
[0x8000b044]:lui a4, 1
[0x8000b048]:addi a4, a4, 2048
[0x8000b04c]:add a5, a5, a4
[0x8000b050]:lw s11, 1340(a5)
[0x8000b054]:sub a5, a5, a4
[0x8000b058]:addi t3, zero, 1
[0x8000b05c]:lui t4, 1048448
[0x8000b060]:addi s10, zero, 1
[0x8000b064]:lui s11, 1048320
[0x8000b068]:addi a4, zero, 0
[0x8000b06c]:csrrw zero, fcsr, a4
[0x8000b070]:fmul.d t5, t3, s10, dyn
[0x8000b074]:csrrs a6, fcsr, zero

[0x8000b070]:fmul.d t5, t3, s10, dyn
[0x8000b074]:csrrs a6, fcsr, zero
[0x8000b078]:sw t5, 632(ra)
[0x8000b07c]:sw t6, 640(ra)
[0x8000b080]:sw t5, 648(ra)
[0x8000b084]:sw a6, 656(ra)
[0x8000b088]:lui a4, 1
[0x8000b08c]:addi a4, a4, 2048
[0x8000b090]:add a5, a5, a4
[0x8000b094]:lw t3, 1344(a5)
[0x8000b098]:sub a5, a5, a4
[0x8000b09c]:lui a4, 1
[0x8000b0a0]:addi a4, a4, 2048
[0x8000b0a4]:add a5, a5, a4
[0x8000b0a8]:lw t4, 1348(a5)
[0x8000b0ac]:sub a5, a5, a4
[0x8000b0b0]:lui a4, 1
[0x8000b0b4]:addi a4, a4, 2048
[0x8000b0b8]:add a5, a5, a4
[0x8000b0bc]:lw s10, 1352(a5)
[0x8000b0c0]:sub a5, a5, a4
[0x8000b0c4]:lui a4, 1
[0x8000b0c8]:addi a4, a4, 2048
[0x8000b0cc]:add a5, a5, a4
[0x8000b0d0]:lw s11, 1356(a5)
[0x8000b0d4]:sub a5, a5, a4
[0x8000b0d8]:addi t3, zero, 1
[0x8000b0dc]:lui t4, 1048448
[0x8000b0e0]:addi s10, zero, 0
[0x8000b0e4]:lui s11, 261888
[0x8000b0e8]:addi a4, zero, 0
[0x8000b0ec]:csrrw zero, fcsr, a4
[0x8000b0f0]:fmul.d t5, t3, s10, dyn
[0x8000b0f4]:csrrs a6, fcsr, zero

[0x8000b0f0]:fmul.d t5, t3, s10, dyn
[0x8000b0f4]:csrrs a6, fcsr, zero
[0x8000b0f8]:sw t5, 664(ra)
[0x8000b0fc]:sw t6, 672(ra)
[0x8000b100]:sw t5, 680(ra)
[0x8000b104]:sw a6, 688(ra)
[0x8000b108]:lui a4, 1
[0x8000b10c]:addi a4, a4, 2048
[0x8000b110]:add a5, a5, a4
[0x8000b114]:lw t3, 1360(a5)
[0x8000b118]:sub a5, a5, a4
[0x8000b11c]:lui a4, 1
[0x8000b120]:addi a4, a4, 2048
[0x8000b124]:add a5, a5, a4
[0x8000b128]:lw t4, 1364(a5)
[0x8000b12c]:sub a5, a5, a4
[0x8000b130]:lui a4, 1
[0x8000b134]:addi a4, a4, 2048
[0x8000b138]:add a5, a5, a4
[0x8000b13c]:lw s10, 1368(a5)
[0x8000b140]:sub a5, a5, a4
[0x8000b144]:lui a4, 1
[0x8000b148]:addi a4, a4, 2048
[0x8000b14c]:add a5, a5, a4
[0x8000b150]:lw s11, 1372(a5)
[0x8000b154]:sub a5, a5, a4
[0x8000b158]:addi t3, zero, 1
[0x8000b15c]:lui t4, 1048448
[0x8000b160]:addi s10, zero, 0
[0x8000b164]:lui s11, 784384
[0x8000b168]:addi a4, zero, 0
[0x8000b16c]:csrrw zero, fcsr, a4
[0x8000b170]:fmul.d t5, t3, s10, dyn
[0x8000b174]:csrrs a6, fcsr, zero

[0x8000b170]:fmul.d t5, t3, s10, dyn
[0x8000b174]:csrrs a6, fcsr, zero
[0x8000b178]:sw t5, 696(ra)
[0x8000b17c]:sw t6, 704(ra)
[0x8000b180]:sw t5, 712(ra)
[0x8000b184]:sw a6, 720(ra)
[0x8000b188]:lui a4, 1
[0x8000b18c]:addi a4, a4, 2048
[0x8000b190]:add a5, a5, a4
[0x8000b194]:lw t3, 1376(a5)
[0x8000b198]:sub a5, a5, a4
[0x8000b19c]:lui a4, 1
[0x8000b1a0]:addi a4, a4, 2048
[0x8000b1a4]:add a5, a5, a4
[0x8000b1a8]:lw t4, 1380(a5)
[0x8000b1ac]:sub a5, a5, a4
[0x8000b1b0]:lui a4, 1
[0x8000b1b4]:addi a4, a4, 2048
[0x8000b1b8]:add a5, a5, a4
[0x8000b1bc]:lw s10, 1384(a5)
[0x8000b1c0]:sub a5, a5, a4
[0x8000b1c4]:lui a4, 1
[0x8000b1c8]:addi a4, a4, 2048
[0x8000b1cc]:add a5, a5, a4
[0x8000b1d0]:lw s11, 1388(a5)
[0x8000b1d4]:sub a5, a5, a4
[0x8000b1d8]:addi t3, zero, 1
[0x8000b1dc]:lui t4, 524032
[0x8000b1e0]:addi s10, zero, 0
[0x8000b1e4]:addi s11, zero, 0
[0x8000b1e8]:addi a4, zero, 0
[0x8000b1ec]:csrrw zero, fcsr, a4
[0x8000b1f0]:fmul.d t5, t3, s10, dyn
[0x8000b1f4]:csrrs a6, fcsr, zero

[0x8000b1f0]:fmul.d t5, t3, s10, dyn
[0x8000b1f4]:csrrs a6, fcsr, zero
[0x8000b1f8]:sw t5, 728(ra)
[0x8000b1fc]:sw t6, 736(ra)
[0x8000b200]:sw t5, 744(ra)
[0x8000b204]:sw a6, 752(ra)
[0x8000b208]:lui a4, 1
[0x8000b20c]:addi a4, a4, 2048
[0x8000b210]:add a5, a5, a4
[0x8000b214]:lw t3, 1392(a5)
[0x8000b218]:sub a5, a5, a4
[0x8000b21c]:lui a4, 1
[0x8000b220]:addi a4, a4, 2048
[0x8000b224]:add a5, a5, a4
[0x8000b228]:lw t4, 1396(a5)
[0x8000b22c]:sub a5, a5, a4
[0x8000b230]:lui a4, 1
[0x8000b234]:addi a4, a4, 2048
[0x8000b238]:add a5, a5, a4
[0x8000b23c]:lw s10, 1400(a5)
[0x8000b240]:sub a5, a5, a4
[0x8000b244]:lui a4, 1
[0x8000b248]:addi a4, a4, 2048
[0x8000b24c]:add a5, a5, a4
[0x8000b250]:lw s11, 1404(a5)
[0x8000b254]:sub a5, a5, a4
[0x8000b258]:addi t3, zero, 1
[0x8000b25c]:lui t4, 524032
[0x8000b260]:addi s10, zero, 0
[0x8000b264]:lui s11, 524288
[0x8000b268]:addi a4, zero, 0
[0x8000b26c]:csrrw zero, fcsr, a4
[0x8000b270]:fmul.d t5, t3, s10, dyn
[0x8000b274]:csrrs a6, fcsr, zero

[0x8000b270]:fmul.d t5, t3, s10, dyn
[0x8000b274]:csrrs a6, fcsr, zero
[0x8000b278]:sw t5, 760(ra)
[0x8000b27c]:sw t6, 768(ra)
[0x8000b280]:sw t5, 776(ra)
[0x8000b284]:sw a6, 784(ra)
[0x8000b288]:lui a4, 1
[0x8000b28c]:addi a4, a4, 2048
[0x8000b290]:add a5, a5, a4
[0x8000b294]:lw t3, 1408(a5)
[0x8000b298]:sub a5, a5, a4
[0x8000b29c]:lui a4, 1
[0x8000b2a0]:addi a4, a4, 2048
[0x8000b2a4]:add a5, a5, a4
[0x8000b2a8]:lw t4, 1412(a5)
[0x8000b2ac]:sub a5, a5, a4
[0x8000b2b0]:lui a4, 1
[0x8000b2b4]:addi a4, a4, 2048
[0x8000b2b8]:add a5, a5, a4
[0x8000b2bc]:lw s10, 1416(a5)
[0x8000b2c0]:sub a5, a5, a4
[0x8000b2c4]:lui a4, 1
[0x8000b2c8]:addi a4, a4, 2048
[0x8000b2cc]:add a5, a5, a4
[0x8000b2d0]:lw s11, 1420(a5)
[0x8000b2d4]:sub a5, a5, a4
[0x8000b2d8]:addi t3, zero, 1
[0x8000b2dc]:lui t4, 524032
[0x8000b2e0]:addi s10, zero, 1
[0x8000b2e4]:addi s11, zero, 0
[0x8000b2e8]:addi a4, zero, 0
[0x8000b2ec]:csrrw zero, fcsr, a4
[0x8000b2f0]:fmul.d t5, t3, s10, dyn
[0x8000b2f4]:csrrs a6, fcsr, zero

[0x8000b2f0]:fmul.d t5, t3, s10, dyn
[0x8000b2f4]:csrrs a6, fcsr, zero
[0x8000b2f8]:sw t5, 792(ra)
[0x8000b2fc]:sw t6, 800(ra)
[0x8000b300]:sw t5, 808(ra)
[0x8000b304]:sw a6, 816(ra)
[0x8000b308]:lui a4, 1
[0x8000b30c]:addi a4, a4, 2048
[0x8000b310]:add a5, a5, a4
[0x8000b314]:lw t3, 1424(a5)
[0x8000b318]:sub a5, a5, a4
[0x8000b31c]:lui a4, 1
[0x8000b320]:addi a4, a4, 2048
[0x8000b324]:add a5, a5, a4
[0x8000b328]:lw t4, 1428(a5)
[0x8000b32c]:sub a5, a5, a4
[0x8000b330]:lui a4, 1
[0x8000b334]:addi a4, a4, 2048
[0x8000b338]:add a5, a5, a4
[0x8000b33c]:lw s10, 1432(a5)
[0x8000b340]:sub a5, a5, a4
[0x8000b344]:lui a4, 1
[0x8000b348]:addi a4, a4, 2048
[0x8000b34c]:add a5, a5, a4
[0x8000b350]:lw s11, 1436(a5)
[0x8000b354]:sub a5, a5, a4
[0x8000b358]:addi t3, zero, 1
[0x8000b35c]:lui t4, 524032
[0x8000b360]:addi s10, zero, 1
[0x8000b364]:lui s11, 524288
[0x8000b368]:addi a4, zero, 0
[0x8000b36c]:csrrw zero, fcsr, a4
[0x8000b370]:fmul.d t5, t3, s10, dyn
[0x8000b374]:csrrs a6, fcsr, zero

[0x8000b370]:fmul.d t5, t3, s10, dyn
[0x8000b374]:csrrs a6, fcsr, zero
[0x8000b378]:sw t5, 824(ra)
[0x8000b37c]:sw t6, 832(ra)
[0x8000b380]:sw t5, 840(ra)
[0x8000b384]:sw a6, 848(ra)
[0x8000b388]:lui a4, 1
[0x8000b38c]:addi a4, a4, 2048
[0x8000b390]:add a5, a5, a4
[0x8000b394]:lw t3, 1440(a5)
[0x8000b398]:sub a5, a5, a4
[0x8000b39c]:lui a4, 1
[0x8000b3a0]:addi a4, a4, 2048
[0x8000b3a4]:add a5, a5, a4
[0x8000b3a8]:lw t4, 1444(a5)
[0x8000b3ac]:sub a5, a5, a4
[0x8000b3b0]:lui a4, 1
[0x8000b3b4]:addi a4, a4, 2048
[0x8000b3b8]:add a5, a5, a4
[0x8000b3bc]:lw s10, 1448(a5)
[0x8000b3c0]:sub a5, a5, a4
[0x8000b3c4]:lui a4, 1
[0x8000b3c8]:addi a4, a4, 2048
[0x8000b3cc]:add a5, a5, a4
[0x8000b3d0]:lw s11, 1452(a5)
[0x8000b3d4]:sub a5, a5, a4
[0x8000b3d8]:addi t3, zero, 1
[0x8000b3dc]:lui t4, 524032
[0x8000b3e0]:addi s10, zero, 2
[0x8000b3e4]:addi s11, zero, 0
[0x8000b3e8]:addi a4, zero, 0
[0x8000b3ec]:csrrw zero, fcsr, a4
[0x8000b3f0]:fmul.d t5, t3, s10, dyn
[0x8000b3f4]:csrrs a6, fcsr, zero

[0x8000b3f0]:fmul.d t5, t3, s10, dyn
[0x8000b3f4]:csrrs a6, fcsr, zero
[0x8000b3f8]:sw t5, 856(ra)
[0x8000b3fc]:sw t6, 864(ra)
[0x8000b400]:sw t5, 872(ra)
[0x8000b404]:sw a6, 880(ra)
[0x8000b408]:lui a4, 1
[0x8000b40c]:addi a4, a4, 2048
[0x8000b410]:add a5, a5, a4
[0x8000b414]:lw t3, 1456(a5)
[0x8000b418]:sub a5, a5, a4
[0x8000b41c]:lui a4, 1
[0x8000b420]:addi a4, a4, 2048
[0x8000b424]:add a5, a5, a4
[0x8000b428]:lw t4, 1460(a5)
[0x8000b42c]:sub a5, a5, a4
[0x8000b430]:lui a4, 1
[0x8000b434]:addi a4, a4, 2048
[0x8000b438]:add a5, a5, a4
[0x8000b43c]:lw s10, 1464(a5)
[0x8000b440]:sub a5, a5, a4
[0x8000b444]:lui a4, 1
[0x8000b448]:addi a4, a4, 2048
[0x8000b44c]:add a5, a5, a4
[0x8000b450]:lw s11, 1468(a5)
[0x8000b454]:sub a5, a5, a4
[0x8000b458]:addi t3, zero, 1
[0x8000b45c]:lui t4, 524032
[0x8000b460]:addi s10, zero, 2
[0x8000b464]:lui s11, 524288
[0x8000b468]:addi a4, zero, 0
[0x8000b46c]:csrrw zero, fcsr, a4
[0x8000b470]:fmul.d t5, t3, s10, dyn
[0x8000b474]:csrrs a6, fcsr, zero

[0x8000b470]:fmul.d t5, t3, s10, dyn
[0x8000b474]:csrrs a6, fcsr, zero
[0x8000b478]:sw t5, 888(ra)
[0x8000b47c]:sw t6, 896(ra)
[0x8000b480]:sw t5, 904(ra)
[0x8000b484]:sw a6, 912(ra)
[0x8000b488]:lui a4, 1
[0x8000b48c]:addi a4, a4, 2048
[0x8000b490]:add a5, a5, a4
[0x8000b494]:lw t3, 1472(a5)
[0x8000b498]:sub a5, a5, a4
[0x8000b49c]:lui a4, 1
[0x8000b4a0]:addi a4, a4, 2048
[0x8000b4a4]:add a5, a5, a4
[0x8000b4a8]:lw t4, 1476(a5)
[0x8000b4ac]:sub a5, a5, a4
[0x8000b4b0]:lui a4, 1
[0x8000b4b4]:addi a4, a4, 2048
[0x8000b4b8]:add a5, a5, a4
[0x8000b4bc]:lw s10, 1480(a5)
[0x8000b4c0]:sub a5, a5, a4
[0x8000b4c4]:lui a4, 1
[0x8000b4c8]:addi a4, a4, 2048
[0x8000b4cc]:add a5, a5, a4
[0x8000b4d0]:lw s11, 1484(a5)
[0x8000b4d4]:sub a5, a5, a4
[0x8000b4d8]:addi t3, zero, 1
[0x8000b4dc]:lui t4, 524032
[0x8000b4e0]:addi s10, zero, 4095
[0x8000b4e4]:lui s11, 256
[0x8000b4e8]:addi s11, s11, 4095
[0x8000b4ec]:addi a4, zero, 0
[0x8000b4f0]:csrrw zero, fcsr, a4
[0x8000b4f4]:fmul.d t5, t3, s10, dyn
[0x8000b4f8]:csrrs a6, fcsr, zero

[0x8000b4f4]:fmul.d t5, t3, s10, dyn
[0x8000b4f8]:csrrs a6, fcsr, zero
[0x8000b4fc]:sw t5, 920(ra)
[0x8000b500]:sw t6, 928(ra)
[0x8000b504]:sw t5, 936(ra)
[0x8000b508]:sw a6, 944(ra)
[0x8000b50c]:lui a4, 1
[0x8000b510]:addi a4, a4, 2048
[0x8000b514]:add a5, a5, a4
[0x8000b518]:lw t3, 1488(a5)
[0x8000b51c]:sub a5, a5, a4
[0x8000b520]:lui a4, 1
[0x8000b524]:addi a4, a4, 2048
[0x8000b528]:add a5, a5, a4
[0x8000b52c]:lw t4, 1492(a5)
[0x8000b530]:sub a5, a5, a4
[0x8000b534]:lui a4, 1
[0x8000b538]:addi a4, a4, 2048
[0x8000b53c]:add a5, a5, a4
[0x8000b540]:lw s10, 1496(a5)
[0x8000b544]:sub a5, a5, a4
[0x8000b548]:lui a4, 1
[0x8000b54c]:addi a4, a4, 2048
[0x8000b550]:add a5, a5, a4
[0x8000b554]:lw s11, 1500(a5)
[0x8000b558]:sub a5, a5, a4
[0x8000b55c]:addi t3, zero, 1
[0x8000b560]:lui t4, 524032
[0x8000b564]:addi s10, zero, 4095
[0x8000b568]:lui s11, 524544
[0x8000b56c]:addi s11, s11, 4095
[0x8000b570]:addi a4, zero, 0
[0x8000b574]:csrrw zero, fcsr, a4
[0x8000b578]:fmul.d t5, t3, s10, dyn
[0x8000b57c]:csrrs a6, fcsr, zero

[0x8000b578]:fmul.d t5, t3, s10, dyn
[0x8000b57c]:csrrs a6, fcsr, zero
[0x8000b580]:sw t5, 952(ra)
[0x8000b584]:sw t6, 960(ra)
[0x8000b588]:sw t5, 968(ra)
[0x8000b58c]:sw a6, 976(ra)
[0x8000b590]:lui a4, 1
[0x8000b594]:addi a4, a4, 2048
[0x8000b598]:add a5, a5, a4
[0x8000b59c]:lw t3, 1504(a5)
[0x8000b5a0]:sub a5, a5, a4
[0x8000b5a4]:lui a4, 1
[0x8000b5a8]:addi a4, a4, 2048
[0x8000b5ac]:add a5, a5, a4
[0x8000b5b0]:lw t4, 1508(a5)
[0x8000b5b4]:sub a5, a5, a4
[0x8000b5b8]:lui a4, 1
[0x8000b5bc]:addi a4, a4, 2048
[0x8000b5c0]:add a5, a5, a4
[0x8000b5c4]:lw s10, 1512(a5)
[0x8000b5c8]:sub a5, a5, a4
[0x8000b5cc]:lui a4, 1
[0x8000b5d0]:addi a4, a4, 2048
[0x8000b5d4]:add a5, a5, a4
[0x8000b5d8]:lw s11, 1516(a5)
[0x8000b5dc]:sub a5, a5, a4
[0x8000b5e0]:addi t3, zero, 1
[0x8000b5e4]:lui t4, 524032
[0x8000b5e8]:addi s10, zero, 0
[0x8000b5ec]:lui s11, 256
[0x8000b5f0]:addi a4, zero, 0
[0x8000b5f4]:csrrw zero, fcsr, a4
[0x8000b5f8]:fmul.d t5, t3, s10, dyn
[0x8000b5fc]:csrrs a6, fcsr, zero

[0x8000b5f8]:fmul.d t5, t3, s10, dyn
[0x8000b5fc]:csrrs a6, fcsr, zero
[0x8000b600]:sw t5, 984(ra)
[0x8000b604]:sw t6, 992(ra)
[0x8000b608]:sw t5, 1000(ra)
[0x8000b60c]:sw a6, 1008(ra)
[0x8000b610]:lui a4, 1
[0x8000b614]:addi a4, a4, 2048
[0x8000b618]:add a5, a5, a4
[0x8000b61c]:lw t3, 1520(a5)
[0x8000b620]:sub a5, a5, a4
[0x8000b624]:lui a4, 1
[0x8000b628]:addi a4, a4, 2048
[0x8000b62c]:add a5, a5, a4
[0x8000b630]:lw t4, 1524(a5)
[0x8000b634]:sub a5, a5, a4
[0x8000b638]:lui a4, 1
[0x8000b63c]:addi a4, a4, 2048
[0x8000b640]:add a5, a5, a4
[0x8000b644]:lw s10, 1528(a5)
[0x8000b648]:sub a5, a5, a4
[0x8000b64c]:lui a4, 1
[0x8000b650]:addi a4, a4, 2048
[0x8000b654]:add a5, a5, a4
[0x8000b658]:lw s11, 1532(a5)
[0x8000b65c]:sub a5, a5, a4
[0x8000b660]:addi t3, zero, 1
[0x8000b664]:lui t4, 524032
[0x8000b668]:addi s10, zero, 0
[0x8000b66c]:lui s11, 524544
[0x8000b670]:addi a4, zero, 0
[0x8000b674]:csrrw zero, fcsr, a4
[0x8000b678]:fmul.d t5, t3, s10, dyn
[0x8000b67c]:csrrs a6, fcsr, zero

[0x8000b678]:fmul.d t5, t3, s10, dyn
[0x8000b67c]:csrrs a6, fcsr, zero
[0x8000b680]:sw t5, 1016(ra)
[0x8000b684]:sw t6, 1024(ra)
[0x8000b688]:sw t5, 1032(ra)
[0x8000b68c]:sw a6, 1040(ra)
[0x8000b690]:lui a4, 1
[0x8000b694]:addi a4, a4, 2048
[0x8000b698]:add a5, a5, a4
[0x8000b69c]:lw t3, 1536(a5)
[0x8000b6a0]:sub a5, a5, a4
[0x8000b6a4]:lui a4, 1
[0x8000b6a8]:addi a4, a4, 2048
[0x8000b6ac]:add a5, a5, a4
[0x8000b6b0]:lw t4, 1540(a5)
[0x8000b6b4]:sub a5, a5, a4
[0x8000b6b8]:lui a4, 1
[0x8000b6bc]:addi a4, a4, 2048
[0x8000b6c0]:add a5, a5, a4
[0x8000b6c4]:lw s10, 1544(a5)
[0x8000b6c8]:sub a5, a5, a4
[0x8000b6cc]:lui a4, 1
[0x8000b6d0]:addi a4, a4, 2048
[0x8000b6d4]:add a5, a5, a4
[0x8000b6d8]:lw s11, 1548(a5)
[0x8000b6dc]:sub a5, a5, a4
[0x8000b6e0]:addi t3, zero, 1
[0x8000b6e4]:lui t4, 524032
[0x8000b6e8]:addi s10, zero, 2
[0x8000b6ec]:lui s11, 256
[0x8000b6f0]:addi a4, zero, 0
[0x8000b6f4]:csrrw zero, fcsr, a4
[0x8000b6f8]:fmul.d t5, t3, s10, dyn
[0x8000b6fc]:csrrs a6, fcsr, zero

[0x8000b6f8]:fmul.d t5, t3, s10, dyn
[0x8000b6fc]:csrrs a6, fcsr, zero
[0x8000b700]:sw t5, 1048(ra)
[0x8000b704]:sw t6, 1056(ra)
[0x8000b708]:sw t5, 1064(ra)
[0x8000b70c]:sw a6, 1072(ra)
[0x8000b710]:lui a4, 1
[0x8000b714]:addi a4, a4, 2048
[0x8000b718]:add a5, a5, a4
[0x8000b71c]:lw t3, 1552(a5)
[0x8000b720]:sub a5, a5, a4
[0x8000b724]:lui a4, 1
[0x8000b728]:addi a4, a4, 2048
[0x8000b72c]:add a5, a5, a4
[0x8000b730]:lw t4, 1556(a5)
[0x8000b734]:sub a5, a5, a4
[0x8000b738]:lui a4, 1
[0x8000b73c]:addi a4, a4, 2048
[0x8000b740]:add a5, a5, a4
[0x8000b744]:lw s10, 1560(a5)
[0x8000b748]:sub a5, a5, a4
[0x8000b74c]:lui a4, 1
[0x8000b750]:addi a4, a4, 2048
[0x8000b754]:add a5, a5, a4
[0x8000b758]:lw s11, 1564(a5)
[0x8000b75c]:sub a5, a5, a4
[0x8000b760]:addi t3, zero, 1
[0x8000b764]:lui t4, 524032
[0x8000b768]:addi s10, zero, 2
[0x8000b76c]:lui s11, 524544
[0x8000b770]:addi a4, zero, 0
[0x8000b774]:csrrw zero, fcsr, a4
[0x8000b778]:fmul.d t5, t3, s10, dyn
[0x8000b77c]:csrrs a6, fcsr, zero

[0x8000b778]:fmul.d t5, t3, s10, dyn
[0x8000b77c]:csrrs a6, fcsr, zero
[0x8000b780]:sw t5, 1080(ra)
[0x8000b784]:sw t6, 1088(ra)
[0x8000b788]:sw t5, 1096(ra)
[0x8000b78c]:sw a6, 1104(ra)
[0x8000b790]:lui a4, 1
[0x8000b794]:addi a4, a4, 2048
[0x8000b798]:add a5, a5, a4
[0x8000b79c]:lw t3, 1568(a5)
[0x8000b7a0]:sub a5, a5, a4
[0x8000b7a4]:lui a4, 1
[0x8000b7a8]:addi a4, a4, 2048
[0x8000b7ac]:add a5, a5, a4
[0x8000b7b0]:lw t4, 1572(a5)
[0x8000b7b4]:sub a5, a5, a4
[0x8000b7b8]:lui a4, 1
[0x8000b7bc]:addi a4, a4, 2048
[0x8000b7c0]:add a5, a5, a4
[0x8000b7c4]:lw s10, 1576(a5)
[0x8000b7c8]:sub a5, a5, a4
[0x8000b7cc]:lui a4, 1
[0x8000b7d0]:addi a4, a4, 2048
[0x8000b7d4]:add a5, a5, a4
[0x8000b7d8]:lw s11, 1580(a5)
[0x8000b7dc]:sub a5, a5, a4
[0x8000b7e0]:addi t3, zero, 1
[0x8000b7e4]:lui t4, 524032
[0x8000b7e8]:addi s10, zero, 4095
[0x8000b7ec]:lui s11, 524032
[0x8000b7f0]:addi s11, s11, 4095
[0x8000b7f4]:addi a4, zero, 0
[0x8000b7f8]:csrrw zero, fcsr, a4
[0x8000b7fc]:fmul.d t5, t3, s10, dyn
[0x8000b800]:csrrs a6, fcsr, zero

[0x8000b7fc]:fmul.d t5, t3, s10, dyn
[0x8000b800]:csrrs a6, fcsr, zero
[0x8000b804]:sw t5, 1112(ra)
[0x8000b808]:sw t6, 1120(ra)
[0x8000b80c]:sw t5, 1128(ra)
[0x8000b810]:sw a6, 1136(ra)
[0x8000b814]:lui a4, 1
[0x8000b818]:addi a4, a4, 2048
[0x8000b81c]:add a5, a5, a4
[0x8000b820]:lw t3, 1584(a5)
[0x8000b824]:sub a5, a5, a4
[0x8000b828]:lui a4, 1
[0x8000b82c]:addi a4, a4, 2048
[0x8000b830]:add a5, a5, a4
[0x8000b834]:lw t4, 1588(a5)
[0x8000b838]:sub a5, a5, a4
[0x8000b83c]:lui a4, 1
[0x8000b840]:addi a4, a4, 2048
[0x8000b844]:add a5, a5, a4
[0x8000b848]:lw s10, 1592(a5)
[0x8000b84c]:sub a5, a5, a4
[0x8000b850]:lui a4, 1
[0x8000b854]:addi a4, a4, 2048
[0x8000b858]:add a5, a5, a4
[0x8000b85c]:lw s11, 1596(a5)
[0x8000b860]:sub a5, a5, a4
[0x8000b864]:addi t3, zero, 1
[0x8000b868]:lui t4, 524032
[0x8000b86c]:addi s10, zero, 4095
[0x8000b870]:lui s11, 1048320
[0x8000b874]:addi s11, s11, 4095
[0x8000b878]:addi a4, zero, 0
[0x8000b87c]:csrrw zero, fcsr, a4
[0x8000b880]:fmul.d t5, t3, s10, dyn
[0x8000b884]:csrrs a6, fcsr, zero

[0x8000b880]:fmul.d t5, t3, s10, dyn
[0x8000b884]:csrrs a6, fcsr, zero
[0x8000b888]:sw t5, 1144(ra)
[0x8000b88c]:sw t6, 1152(ra)
[0x8000b890]:sw t5, 1160(ra)
[0x8000b894]:sw a6, 1168(ra)
[0x8000b898]:lui a4, 1
[0x8000b89c]:addi a4, a4, 2048
[0x8000b8a0]:add a5, a5, a4
[0x8000b8a4]:lw t3, 1600(a5)
[0x8000b8a8]:sub a5, a5, a4
[0x8000b8ac]:lui a4, 1
[0x8000b8b0]:addi a4, a4, 2048
[0x8000b8b4]:add a5, a5, a4
[0x8000b8b8]:lw t4, 1604(a5)
[0x8000b8bc]:sub a5, a5, a4
[0x8000b8c0]:lui a4, 1
[0x8000b8c4]:addi a4, a4, 2048
[0x8000b8c8]:add a5, a5, a4
[0x8000b8cc]:lw s10, 1608(a5)
[0x8000b8d0]:sub a5, a5, a4
[0x8000b8d4]:lui a4, 1
[0x8000b8d8]:addi a4, a4, 2048
[0x8000b8dc]:add a5, a5, a4
[0x8000b8e0]:lw s11, 1612(a5)
[0x8000b8e4]:sub a5, a5, a4
[0x8000b8e8]:addi t3, zero, 1
[0x8000b8ec]:lui t4, 524032
[0x8000b8f0]:addi s10, zero, 0
[0x8000b8f4]:lui s11, 524032
[0x8000b8f8]:addi a4, zero, 0
[0x8000b8fc]:csrrw zero, fcsr, a4
[0x8000b900]:fmul.d t5, t3, s10, dyn
[0x8000b904]:csrrs a6, fcsr, zero

[0x8000b900]:fmul.d t5, t3, s10, dyn
[0x8000b904]:csrrs a6, fcsr, zero
[0x8000b908]:sw t5, 1176(ra)
[0x8000b90c]:sw t6, 1184(ra)
[0x8000b910]:sw t5, 1192(ra)
[0x8000b914]:sw a6, 1200(ra)
[0x8000b918]:lui a4, 1
[0x8000b91c]:addi a4, a4, 2048
[0x8000b920]:add a5, a5, a4
[0x8000b924]:lw t3, 1616(a5)
[0x8000b928]:sub a5, a5, a4
[0x8000b92c]:lui a4, 1
[0x8000b930]:addi a4, a4, 2048
[0x8000b934]:add a5, a5, a4
[0x8000b938]:lw t4, 1620(a5)
[0x8000b93c]:sub a5, a5, a4
[0x8000b940]:lui a4, 1
[0x8000b944]:addi a4, a4, 2048
[0x8000b948]:add a5, a5, a4
[0x8000b94c]:lw s10, 1624(a5)
[0x8000b950]:sub a5, a5, a4
[0x8000b954]:lui a4, 1
[0x8000b958]:addi a4, a4, 2048
[0x8000b95c]:add a5, a5, a4
[0x8000b960]:lw s11, 1628(a5)
[0x8000b964]:sub a5, a5, a4
[0x8000b968]:addi t3, zero, 1
[0x8000b96c]:lui t4, 524032
[0x8000b970]:addi s10, zero, 0
[0x8000b974]:lui s11, 1048320
[0x8000b978]:addi a4, zero, 0
[0x8000b97c]:csrrw zero, fcsr, a4
[0x8000b980]:fmul.d t5, t3, s10, dyn
[0x8000b984]:csrrs a6, fcsr, zero

[0x8000b980]:fmul.d t5, t3, s10, dyn
[0x8000b984]:csrrs a6, fcsr, zero
[0x8000b988]:sw t5, 1208(ra)
[0x8000b98c]:sw t6, 1216(ra)
[0x8000b990]:sw t5, 1224(ra)
[0x8000b994]:sw a6, 1232(ra)
[0x8000b998]:lui a4, 1
[0x8000b99c]:addi a4, a4, 2048
[0x8000b9a0]:add a5, a5, a4
[0x8000b9a4]:lw t3, 1632(a5)
[0x8000b9a8]:sub a5, a5, a4
[0x8000b9ac]:lui a4, 1
[0x8000b9b0]:addi a4, a4, 2048
[0x8000b9b4]:add a5, a5, a4
[0x8000b9b8]:lw t4, 1636(a5)
[0x8000b9bc]:sub a5, a5, a4
[0x8000b9c0]:lui a4, 1
[0x8000b9c4]:addi a4, a4, 2048
[0x8000b9c8]:add a5, a5, a4
[0x8000b9cc]:lw s10, 1640(a5)
[0x8000b9d0]:sub a5, a5, a4
[0x8000b9d4]:lui a4, 1
[0x8000b9d8]:addi a4, a4, 2048
[0x8000b9dc]:add a5, a5, a4
[0x8000b9e0]:lw s11, 1644(a5)
[0x8000b9e4]:sub a5, a5, a4
[0x8000b9e8]:addi t3, zero, 1
[0x8000b9ec]:lui t4, 524032
[0x8000b9f0]:addi s10, zero, 0
[0x8000b9f4]:lui s11, 524160
[0x8000b9f8]:addi a4, zero, 0
[0x8000b9fc]:csrrw zero, fcsr, a4
[0x8000ba00]:fmul.d t5, t3, s10, dyn
[0x8000ba04]:csrrs a6, fcsr, zero

[0x8000ba00]:fmul.d t5, t3, s10, dyn
[0x8000ba04]:csrrs a6, fcsr, zero
[0x8000ba08]:sw t5, 1240(ra)
[0x8000ba0c]:sw t6, 1248(ra)
[0x8000ba10]:sw t5, 1256(ra)
[0x8000ba14]:sw a6, 1264(ra)
[0x8000ba18]:lui a4, 1
[0x8000ba1c]:addi a4, a4, 2048
[0x8000ba20]:add a5, a5, a4
[0x8000ba24]:lw t3, 1648(a5)
[0x8000ba28]:sub a5, a5, a4
[0x8000ba2c]:lui a4, 1
[0x8000ba30]:addi a4, a4, 2048
[0x8000ba34]:add a5, a5, a4
[0x8000ba38]:lw t4, 1652(a5)
[0x8000ba3c]:sub a5, a5, a4
[0x8000ba40]:lui a4, 1
[0x8000ba44]:addi a4, a4, 2048
[0x8000ba48]:add a5, a5, a4
[0x8000ba4c]:lw s10, 1656(a5)
[0x8000ba50]:sub a5, a5, a4
[0x8000ba54]:lui a4, 1
[0x8000ba58]:addi a4, a4, 2048
[0x8000ba5c]:add a5, a5, a4
[0x8000ba60]:lw s11, 1660(a5)
[0x8000ba64]:sub a5, a5, a4
[0x8000ba68]:addi t3, zero, 1
[0x8000ba6c]:lui t4, 524032
[0x8000ba70]:addi s10, zero, 0
[0x8000ba74]:lui s11, 1048448
[0x8000ba78]:addi a4, zero, 0
[0x8000ba7c]:csrrw zero, fcsr, a4
[0x8000ba80]:fmul.d t5, t3, s10, dyn
[0x8000ba84]:csrrs a6, fcsr, zero

[0x8000ba80]:fmul.d t5, t3, s10, dyn
[0x8000ba84]:csrrs a6, fcsr, zero
[0x8000ba88]:sw t5, 1272(ra)
[0x8000ba8c]:sw t6, 1280(ra)
[0x8000ba90]:sw t5, 1288(ra)
[0x8000ba94]:sw a6, 1296(ra)
[0x8000ba98]:lui a4, 1
[0x8000ba9c]:addi a4, a4, 2048
[0x8000baa0]:add a5, a5, a4
[0x8000baa4]:lw t3, 1664(a5)
[0x8000baa8]:sub a5, a5, a4
[0x8000baac]:lui a4, 1
[0x8000bab0]:addi a4, a4, 2048
[0x8000bab4]:add a5, a5, a4
[0x8000bab8]:lw t4, 1668(a5)
[0x8000babc]:sub a5, a5, a4
[0x8000bac0]:lui a4, 1
[0x8000bac4]:addi a4, a4, 2048
[0x8000bac8]:add a5, a5, a4
[0x8000bacc]:lw s10, 1672(a5)
[0x8000bad0]:sub a5, a5, a4
[0x8000bad4]:lui a4, 1
[0x8000bad8]:addi a4, a4, 2048
[0x8000badc]:add a5, a5, a4
[0x8000bae0]:lw s11, 1676(a5)
[0x8000bae4]:sub a5, a5, a4
[0x8000bae8]:addi t3, zero, 1
[0x8000baec]:lui t4, 524032
[0x8000baf0]:addi s10, zero, 1
[0x8000baf4]:lui s11, 524160
[0x8000baf8]:addi a4, zero, 0
[0x8000bafc]:csrrw zero, fcsr, a4
[0x8000bb00]:fmul.d t5, t3, s10, dyn
[0x8000bb04]:csrrs a6, fcsr, zero

[0x8000bb00]:fmul.d t5, t3, s10, dyn
[0x8000bb04]:csrrs a6, fcsr, zero
[0x8000bb08]:sw t5, 1304(ra)
[0x8000bb0c]:sw t6, 1312(ra)
[0x8000bb10]:sw t5, 1320(ra)
[0x8000bb14]:sw a6, 1328(ra)
[0x8000bb18]:lui a4, 1
[0x8000bb1c]:addi a4, a4, 2048
[0x8000bb20]:add a5, a5, a4
[0x8000bb24]:lw t3, 1680(a5)
[0x8000bb28]:sub a5, a5, a4
[0x8000bb2c]:lui a4, 1
[0x8000bb30]:addi a4, a4, 2048
[0x8000bb34]:add a5, a5, a4
[0x8000bb38]:lw t4, 1684(a5)
[0x8000bb3c]:sub a5, a5, a4
[0x8000bb40]:lui a4, 1
[0x8000bb44]:addi a4, a4, 2048
[0x8000bb48]:add a5, a5, a4
[0x8000bb4c]:lw s10, 1688(a5)
[0x8000bb50]:sub a5, a5, a4
[0x8000bb54]:lui a4, 1
[0x8000bb58]:addi a4, a4, 2048
[0x8000bb5c]:add a5, a5, a4
[0x8000bb60]:lw s11, 1692(a5)
[0x8000bb64]:sub a5, a5, a4
[0x8000bb68]:addi t3, zero, 1
[0x8000bb6c]:lui t4, 524032
[0x8000bb70]:addi s10, zero, 1
[0x8000bb74]:lui s11, 1048448
[0x8000bb78]:addi a4, zero, 0
[0x8000bb7c]:csrrw zero, fcsr, a4
[0x8000bb80]:fmul.d t5, t3, s10, dyn
[0x8000bb84]:csrrs a6, fcsr, zero

[0x8000bb80]:fmul.d t5, t3, s10, dyn
[0x8000bb84]:csrrs a6, fcsr, zero
[0x8000bb88]:sw t5, 1336(ra)
[0x8000bb8c]:sw t6, 1344(ra)
[0x8000bb90]:sw t5, 1352(ra)
[0x8000bb94]:sw a6, 1360(ra)
[0x8000bb98]:lui a4, 1
[0x8000bb9c]:addi a4, a4, 2048
[0x8000bba0]:add a5, a5, a4
[0x8000bba4]:lw t3, 1696(a5)
[0x8000bba8]:sub a5, a5, a4
[0x8000bbac]:lui a4, 1
[0x8000bbb0]:addi a4, a4, 2048
[0x8000bbb4]:add a5, a5, a4
[0x8000bbb8]:lw t4, 1700(a5)
[0x8000bbbc]:sub a5, a5, a4
[0x8000bbc0]:lui a4, 1
[0x8000bbc4]:addi a4, a4, 2048
[0x8000bbc8]:add a5, a5, a4
[0x8000bbcc]:lw s10, 1704(a5)
[0x8000bbd0]:sub a5, a5, a4
[0x8000bbd4]:lui a4, 1
[0x8000bbd8]:addi a4, a4, 2048
[0x8000bbdc]:add a5, a5, a4
[0x8000bbe0]:lw s11, 1708(a5)
[0x8000bbe4]:sub a5, a5, a4
[0x8000bbe8]:addi t3, zero, 1
[0x8000bbec]:lui t4, 524032
[0x8000bbf0]:addi s10, zero, 1
[0x8000bbf4]:lui s11, 524032
[0x8000bbf8]:addi a4, zero, 0
[0x8000bbfc]:csrrw zero, fcsr, a4
[0x8000bc00]:fmul.d t5, t3, s10, dyn
[0x8000bc04]:csrrs a6, fcsr, zero

[0x8000bc00]:fmul.d t5, t3, s10, dyn
[0x8000bc04]:csrrs a6, fcsr, zero
[0x8000bc08]:sw t5, 1368(ra)
[0x8000bc0c]:sw t6, 1376(ra)
[0x8000bc10]:sw t5, 1384(ra)
[0x8000bc14]:sw a6, 1392(ra)
[0x8000bc18]:lui a4, 1
[0x8000bc1c]:addi a4, a4, 2048
[0x8000bc20]:add a5, a5, a4
[0x8000bc24]:lw t3, 1712(a5)
[0x8000bc28]:sub a5, a5, a4
[0x8000bc2c]:lui a4, 1
[0x8000bc30]:addi a4, a4, 2048
[0x8000bc34]:add a5, a5, a4
[0x8000bc38]:lw t4, 1716(a5)
[0x8000bc3c]:sub a5, a5, a4
[0x8000bc40]:lui a4, 1
[0x8000bc44]:addi a4, a4, 2048
[0x8000bc48]:add a5, a5, a4
[0x8000bc4c]:lw s10, 1720(a5)
[0x8000bc50]:sub a5, a5, a4
[0x8000bc54]:lui a4, 1
[0x8000bc58]:addi a4, a4, 2048
[0x8000bc5c]:add a5, a5, a4
[0x8000bc60]:lw s11, 1724(a5)
[0x8000bc64]:sub a5, a5, a4
[0x8000bc68]:addi t3, zero, 1
[0x8000bc6c]:lui t4, 524032
[0x8000bc70]:addi s10, zero, 1
[0x8000bc74]:lui s11, 1048320
[0x8000bc78]:addi a4, zero, 0
[0x8000bc7c]:csrrw zero, fcsr, a4
[0x8000bc80]:fmul.d t5, t3, s10, dyn
[0x8000bc84]:csrrs a6, fcsr, zero

[0x8000bc80]:fmul.d t5, t3, s10, dyn
[0x8000bc84]:csrrs a6, fcsr, zero
[0x8000bc88]:sw t5, 1400(ra)
[0x8000bc8c]:sw t6, 1408(ra)
[0x8000bc90]:sw t5, 1416(ra)
[0x8000bc94]:sw a6, 1424(ra)
[0x8000bc98]:lui a4, 1
[0x8000bc9c]:addi a4, a4, 2048
[0x8000bca0]:add a5, a5, a4
[0x8000bca4]:lw t3, 1728(a5)
[0x8000bca8]:sub a5, a5, a4
[0x8000bcac]:lui a4, 1
[0x8000bcb0]:addi a4, a4, 2048
[0x8000bcb4]:add a5, a5, a4
[0x8000bcb8]:lw t4, 1732(a5)
[0x8000bcbc]:sub a5, a5, a4
[0x8000bcc0]:lui a4, 1
[0x8000bcc4]:addi a4, a4, 2048
[0x8000bcc8]:add a5, a5, a4
[0x8000bccc]:lw s10, 1736(a5)
[0x8000bcd0]:sub a5, a5, a4
[0x8000bcd4]:lui a4, 1
[0x8000bcd8]:addi a4, a4, 2048
[0x8000bcdc]:add a5, a5, a4
[0x8000bce0]:lw s11, 1740(a5)
[0x8000bce4]:sub a5, a5, a4
[0x8000bce8]:addi t3, zero, 1
[0x8000bcec]:lui t4, 524032
[0x8000bcf0]:addi s10, zero, 0
[0x8000bcf4]:lui s11, 261888
[0x8000bcf8]:addi a4, zero, 0
[0x8000bcfc]:csrrw zero, fcsr, a4
[0x8000bd00]:fmul.d t5, t3, s10, dyn
[0x8000bd04]:csrrs a6, fcsr, zero

[0x8000bd00]:fmul.d t5, t3, s10, dyn
[0x8000bd04]:csrrs a6, fcsr, zero
[0x8000bd08]:sw t5, 1432(ra)
[0x8000bd0c]:sw t6, 1440(ra)
[0x8000bd10]:sw t5, 1448(ra)
[0x8000bd14]:sw a6, 1456(ra)
[0x8000bd18]:lui a4, 1
[0x8000bd1c]:addi a4, a4, 2048
[0x8000bd20]:add a5, a5, a4
[0x8000bd24]:lw t3, 1744(a5)
[0x8000bd28]:sub a5, a5, a4
[0x8000bd2c]:lui a4, 1
[0x8000bd30]:addi a4, a4, 2048
[0x8000bd34]:add a5, a5, a4
[0x8000bd38]:lw t4, 1748(a5)
[0x8000bd3c]:sub a5, a5, a4
[0x8000bd40]:lui a4, 1
[0x8000bd44]:addi a4, a4, 2048
[0x8000bd48]:add a5, a5, a4
[0x8000bd4c]:lw s10, 1752(a5)
[0x8000bd50]:sub a5, a5, a4
[0x8000bd54]:lui a4, 1
[0x8000bd58]:addi a4, a4, 2048
[0x8000bd5c]:add a5, a5, a4
[0x8000bd60]:lw s11, 1756(a5)
[0x8000bd64]:sub a5, a5, a4
[0x8000bd68]:addi t3, zero, 1
[0x8000bd6c]:lui t4, 524032
[0x8000bd70]:addi s10, zero, 0
[0x8000bd74]:lui s11, 784384
[0x8000bd78]:addi a4, zero, 0
[0x8000bd7c]:csrrw zero, fcsr, a4
[0x8000bd80]:fmul.d t5, t3, s10, dyn
[0x8000bd84]:csrrs a6, fcsr, zero

[0x8000bd80]:fmul.d t5, t3, s10, dyn
[0x8000bd84]:csrrs a6, fcsr, zero
[0x8000bd88]:sw t5, 1464(ra)
[0x8000bd8c]:sw t6, 1472(ra)
[0x8000bd90]:sw t5, 1480(ra)
[0x8000bd94]:sw a6, 1488(ra)
[0x8000bd98]:lui a4, 1
[0x8000bd9c]:addi a4, a4, 2048
[0x8000bda0]:add a5, a5, a4
[0x8000bda4]:lw t3, 1760(a5)
[0x8000bda8]:sub a5, a5, a4
[0x8000bdac]:lui a4, 1
[0x8000bdb0]:addi a4, a4, 2048
[0x8000bdb4]:add a5, a5, a4
[0x8000bdb8]:lw t4, 1764(a5)
[0x8000bdbc]:sub a5, a5, a4
[0x8000bdc0]:lui a4, 1
[0x8000bdc4]:addi a4, a4, 2048
[0x8000bdc8]:add a5, a5, a4
[0x8000bdcc]:lw s10, 1768(a5)
[0x8000bdd0]:sub a5, a5, a4
[0x8000bdd4]:lui a4, 1
[0x8000bdd8]:addi a4, a4, 2048
[0x8000bddc]:add a5, a5, a4
[0x8000bde0]:lw s11, 1772(a5)
[0x8000bde4]:sub a5, a5, a4
[0x8000bde8]:addi t3, zero, 1
[0x8000bdec]:lui t4, 1048320
[0x8000bdf0]:addi s10, zero, 0
[0x8000bdf4]:addi s11, zero, 0
[0x8000bdf8]:addi a4, zero, 0
[0x8000bdfc]:csrrw zero, fcsr, a4
[0x8000be00]:fmul.d t5, t3, s10, dyn
[0x8000be04]:csrrs a6, fcsr, zero

[0x8000be00]:fmul.d t5, t3, s10, dyn
[0x8000be04]:csrrs a6, fcsr, zero
[0x8000be08]:sw t5, 1496(ra)
[0x8000be0c]:sw t6, 1504(ra)
[0x8000be10]:sw t5, 1512(ra)
[0x8000be14]:sw a6, 1520(ra)
[0x8000be18]:lui a4, 1
[0x8000be1c]:addi a4, a4, 2048
[0x8000be20]:add a5, a5, a4
[0x8000be24]:lw t3, 1776(a5)
[0x8000be28]:sub a5, a5, a4
[0x8000be2c]:lui a4, 1
[0x8000be30]:addi a4, a4, 2048
[0x8000be34]:add a5, a5, a4
[0x8000be38]:lw t4, 1780(a5)
[0x8000be3c]:sub a5, a5, a4
[0x8000be40]:lui a4, 1
[0x8000be44]:addi a4, a4, 2048
[0x8000be48]:add a5, a5, a4
[0x8000be4c]:lw s10, 1784(a5)
[0x8000be50]:sub a5, a5, a4
[0x8000be54]:lui a4, 1
[0x8000be58]:addi a4, a4, 2048
[0x8000be5c]:add a5, a5, a4
[0x8000be60]:lw s11, 1788(a5)
[0x8000be64]:sub a5, a5, a4
[0x8000be68]:addi t3, zero, 1
[0x8000be6c]:lui t4, 1048320
[0x8000be70]:addi s10, zero, 0
[0x8000be74]:lui s11, 524288
[0x8000be78]:addi a4, zero, 0
[0x8000be7c]:csrrw zero, fcsr, a4
[0x8000be80]:fmul.d t5, t3, s10, dyn
[0x8000be84]:csrrs a6, fcsr, zero

[0x8000be80]:fmul.d t5, t3, s10, dyn
[0x8000be84]:csrrs a6, fcsr, zero
[0x8000be88]:sw t5, 1528(ra)
[0x8000be8c]:sw t6, 1536(ra)
[0x8000be90]:sw t5, 1544(ra)
[0x8000be94]:sw a6, 1552(ra)
[0x8000be98]:lui a4, 1
[0x8000be9c]:addi a4, a4, 2048
[0x8000bea0]:add a5, a5, a4
[0x8000bea4]:lw t3, 1792(a5)
[0x8000bea8]:sub a5, a5, a4
[0x8000beac]:lui a4, 1
[0x8000beb0]:addi a4, a4, 2048
[0x8000beb4]:add a5, a5, a4
[0x8000beb8]:lw t4, 1796(a5)
[0x8000bebc]:sub a5, a5, a4
[0x8000bec0]:lui a4, 1
[0x8000bec4]:addi a4, a4, 2048
[0x8000bec8]:add a5, a5, a4
[0x8000becc]:lw s10, 1800(a5)
[0x8000bed0]:sub a5, a5, a4
[0x8000bed4]:lui a4, 1
[0x8000bed8]:addi a4, a4, 2048
[0x8000bedc]:add a5, a5, a4
[0x8000bee0]:lw s11, 1804(a5)
[0x8000bee4]:sub a5, a5, a4
[0x8000bee8]:addi t3, zero, 1
[0x8000beec]:lui t4, 1048320
[0x8000bef0]:addi s10, zero, 1
[0x8000bef4]:addi s11, zero, 0
[0x8000bef8]:addi a4, zero, 0
[0x8000befc]:csrrw zero, fcsr, a4
[0x8000bf00]:fmul.d t5, t3, s10, dyn
[0x8000bf04]:csrrs a6, fcsr, zero

[0x8000bf00]:fmul.d t5, t3, s10, dyn
[0x8000bf04]:csrrs a6, fcsr, zero
[0x8000bf08]:sw t5, 1560(ra)
[0x8000bf0c]:sw t6, 1568(ra)
[0x8000bf10]:sw t5, 1576(ra)
[0x8000bf14]:sw a6, 1584(ra)
[0x8000bf18]:lui a4, 1
[0x8000bf1c]:addi a4, a4, 2048
[0x8000bf20]:add a5, a5, a4
[0x8000bf24]:lw t3, 1808(a5)
[0x8000bf28]:sub a5, a5, a4
[0x8000bf2c]:lui a4, 1
[0x8000bf30]:addi a4, a4, 2048
[0x8000bf34]:add a5, a5, a4
[0x8000bf38]:lw t4, 1812(a5)
[0x8000bf3c]:sub a5, a5, a4
[0x8000bf40]:lui a4, 1
[0x8000bf44]:addi a4, a4, 2048
[0x8000bf48]:add a5, a5, a4
[0x8000bf4c]:lw s10, 1816(a5)
[0x8000bf50]:sub a5, a5, a4
[0x8000bf54]:lui a4, 1
[0x8000bf58]:addi a4, a4, 2048
[0x8000bf5c]:add a5, a5, a4
[0x8000bf60]:lw s11, 1820(a5)
[0x8000bf64]:sub a5, a5, a4
[0x8000bf68]:addi t3, zero, 1
[0x8000bf6c]:lui t4, 1048320
[0x8000bf70]:addi s10, zero, 1
[0x8000bf74]:lui s11, 524288
[0x8000bf78]:addi a4, zero, 0
[0x8000bf7c]:csrrw zero, fcsr, a4
[0x8000bf80]:fmul.d t5, t3, s10, dyn
[0x8000bf84]:csrrs a6, fcsr, zero

[0x8000bf80]:fmul.d t5, t3, s10, dyn
[0x8000bf84]:csrrs a6, fcsr, zero
[0x8000bf88]:sw t5, 1592(ra)
[0x8000bf8c]:sw t6, 1600(ra)
[0x8000bf90]:sw t5, 1608(ra)
[0x8000bf94]:sw a6, 1616(ra)
[0x8000bf98]:lui a4, 1
[0x8000bf9c]:addi a4, a4, 2048
[0x8000bfa0]:add a5, a5, a4
[0x8000bfa4]:lw t3, 1824(a5)
[0x8000bfa8]:sub a5, a5, a4
[0x8000bfac]:lui a4, 1
[0x8000bfb0]:addi a4, a4, 2048
[0x8000bfb4]:add a5, a5, a4
[0x8000bfb8]:lw t4, 1828(a5)
[0x8000bfbc]:sub a5, a5, a4
[0x8000bfc0]:lui a4, 1
[0x8000bfc4]:addi a4, a4, 2048
[0x8000bfc8]:add a5, a5, a4
[0x8000bfcc]:lw s10, 1832(a5)
[0x8000bfd0]:sub a5, a5, a4
[0x8000bfd4]:lui a4, 1
[0x8000bfd8]:addi a4, a4, 2048
[0x8000bfdc]:add a5, a5, a4
[0x8000bfe0]:lw s11, 1836(a5)
[0x8000bfe4]:sub a5, a5, a4
[0x8000bfe8]:addi t3, zero, 1
[0x8000bfec]:lui t4, 1048320
[0x8000bff0]:addi s10, zero, 2
[0x8000bff4]:addi s11, zero, 0
[0x8000bff8]:addi a4, zero, 0
[0x8000bffc]:csrrw zero, fcsr, a4
[0x8000c000]:fmul.d t5, t3, s10, dyn
[0x8000c004]:csrrs a6, fcsr, zero

[0x8000c000]:fmul.d t5, t3, s10, dyn
[0x8000c004]:csrrs a6, fcsr, zero
[0x8000c008]:sw t5, 1624(ra)
[0x8000c00c]:sw t6, 1632(ra)
[0x8000c010]:sw t5, 1640(ra)
[0x8000c014]:sw a6, 1648(ra)
[0x8000c018]:lui a4, 1
[0x8000c01c]:addi a4, a4, 2048
[0x8000c020]:add a5, a5, a4
[0x8000c024]:lw t3, 1840(a5)
[0x8000c028]:sub a5, a5, a4
[0x8000c02c]:lui a4, 1
[0x8000c030]:addi a4, a4, 2048
[0x8000c034]:add a5, a5, a4
[0x8000c038]:lw t4, 1844(a5)
[0x8000c03c]:sub a5, a5, a4
[0x8000c040]:lui a4, 1
[0x8000c044]:addi a4, a4, 2048
[0x8000c048]:add a5, a5, a4
[0x8000c04c]:lw s10, 1848(a5)
[0x8000c050]:sub a5, a5, a4
[0x8000c054]:lui a4, 1
[0x8000c058]:addi a4, a4, 2048
[0x8000c05c]:add a5, a5, a4
[0x8000c060]:lw s11, 1852(a5)
[0x8000c064]:sub a5, a5, a4
[0x8000c068]:addi t3, zero, 1
[0x8000c06c]:lui t4, 1048320
[0x8000c070]:addi s10, zero, 2
[0x8000c074]:lui s11, 524288
[0x8000c078]:addi a4, zero, 0
[0x8000c07c]:csrrw zero, fcsr, a4
[0x8000c080]:fmul.d t5, t3, s10, dyn
[0x8000c084]:csrrs a6, fcsr, zero

[0x8000c080]:fmul.d t5, t3, s10, dyn
[0x8000c084]:csrrs a6, fcsr, zero
[0x8000c088]:sw t5, 1656(ra)
[0x8000c08c]:sw t6, 1664(ra)
[0x8000c090]:sw t5, 1672(ra)
[0x8000c094]:sw a6, 1680(ra)
[0x8000c098]:lui a4, 1
[0x8000c09c]:addi a4, a4, 2048
[0x8000c0a0]:add a5, a5, a4
[0x8000c0a4]:lw t3, 1856(a5)
[0x8000c0a8]:sub a5, a5, a4
[0x8000c0ac]:lui a4, 1
[0x8000c0b0]:addi a4, a4, 2048
[0x8000c0b4]:add a5, a5, a4
[0x8000c0b8]:lw t4, 1860(a5)
[0x8000c0bc]:sub a5, a5, a4
[0x8000c0c0]:lui a4, 1
[0x8000c0c4]:addi a4, a4, 2048
[0x8000c0c8]:add a5, a5, a4
[0x8000c0cc]:lw s10, 1864(a5)
[0x8000c0d0]:sub a5, a5, a4
[0x8000c0d4]:lui a4, 1
[0x8000c0d8]:addi a4, a4, 2048
[0x8000c0dc]:add a5, a5, a4
[0x8000c0e0]:lw s11, 1868(a5)
[0x8000c0e4]:sub a5, a5, a4
[0x8000c0e8]:addi t3, zero, 1
[0x8000c0ec]:lui t4, 1048320
[0x8000c0f0]:addi s10, zero, 4095
[0x8000c0f4]:lui s11, 256
[0x8000c0f8]:addi s11, s11, 4095
[0x8000c0fc]:addi a4, zero, 0
[0x8000c100]:csrrw zero, fcsr, a4
[0x8000c104]:fmul.d t5, t3, s10, dyn
[0x8000c108]:csrrs a6, fcsr, zero

[0x8000c104]:fmul.d t5, t3, s10, dyn
[0x8000c108]:csrrs a6, fcsr, zero
[0x8000c10c]:sw t5, 1688(ra)
[0x8000c110]:sw t6, 1696(ra)
[0x8000c114]:sw t5, 1704(ra)
[0x8000c118]:sw a6, 1712(ra)
[0x8000c11c]:lui a4, 1
[0x8000c120]:addi a4, a4, 2048
[0x8000c124]:add a5, a5, a4
[0x8000c128]:lw t3, 1872(a5)
[0x8000c12c]:sub a5, a5, a4
[0x8000c130]:lui a4, 1
[0x8000c134]:addi a4, a4, 2048
[0x8000c138]:add a5, a5, a4
[0x8000c13c]:lw t4, 1876(a5)
[0x8000c140]:sub a5, a5, a4
[0x8000c144]:lui a4, 1
[0x8000c148]:addi a4, a4, 2048
[0x8000c14c]:add a5, a5, a4
[0x8000c150]:lw s10, 1880(a5)
[0x8000c154]:sub a5, a5, a4
[0x8000c158]:lui a4, 1
[0x8000c15c]:addi a4, a4, 2048
[0x8000c160]:add a5, a5, a4
[0x8000c164]:lw s11, 1884(a5)
[0x8000c168]:sub a5, a5, a4
[0x8000c16c]:addi t3, zero, 1
[0x8000c170]:lui t4, 1048320
[0x8000c174]:addi s10, zero, 4095
[0x8000c178]:lui s11, 524544
[0x8000c17c]:addi s11, s11, 4095
[0x8000c180]:addi a4, zero, 0
[0x8000c184]:csrrw zero, fcsr, a4
[0x8000c188]:fmul.d t5, t3, s10, dyn
[0x8000c18c]:csrrs a6, fcsr, zero

[0x8000c188]:fmul.d t5, t3, s10, dyn
[0x8000c18c]:csrrs a6, fcsr, zero
[0x8000c190]:sw t5, 1720(ra)
[0x8000c194]:sw t6, 1728(ra)
[0x8000c198]:sw t5, 1736(ra)
[0x8000c19c]:sw a6, 1744(ra)
[0x8000c1a0]:lui a4, 1
[0x8000c1a4]:addi a4, a4, 2048
[0x8000c1a8]:add a5, a5, a4
[0x8000c1ac]:lw t3, 1888(a5)
[0x8000c1b0]:sub a5, a5, a4
[0x8000c1b4]:lui a4, 1
[0x8000c1b8]:addi a4, a4, 2048
[0x8000c1bc]:add a5, a5, a4
[0x8000c1c0]:lw t4, 1892(a5)
[0x8000c1c4]:sub a5, a5, a4
[0x8000c1c8]:lui a4, 1
[0x8000c1cc]:addi a4, a4, 2048
[0x8000c1d0]:add a5, a5, a4
[0x8000c1d4]:lw s10, 1896(a5)
[0x8000c1d8]:sub a5, a5, a4
[0x8000c1dc]:lui a4, 1
[0x8000c1e0]:addi a4, a4, 2048
[0x8000c1e4]:add a5, a5, a4
[0x8000c1e8]:lw s11, 1900(a5)
[0x8000c1ec]:sub a5, a5, a4
[0x8000c1f0]:addi t3, zero, 1
[0x8000c1f4]:lui t4, 1048320
[0x8000c1f8]:addi s10, zero, 0
[0x8000c1fc]:lui s11, 256
[0x8000c200]:addi a4, zero, 0
[0x8000c204]:csrrw zero, fcsr, a4
[0x8000c208]:fmul.d t5, t3, s10, dyn
[0x8000c20c]:csrrs a6, fcsr, zero

[0x8000c208]:fmul.d t5, t3, s10, dyn
[0x8000c20c]:csrrs a6, fcsr, zero
[0x8000c210]:sw t5, 1752(ra)
[0x8000c214]:sw t6, 1760(ra)
[0x8000c218]:sw t5, 1768(ra)
[0x8000c21c]:sw a6, 1776(ra)
[0x8000c220]:lui a4, 1
[0x8000c224]:addi a4, a4, 2048
[0x8000c228]:add a5, a5, a4
[0x8000c22c]:lw t3, 1904(a5)
[0x8000c230]:sub a5, a5, a4
[0x8000c234]:lui a4, 1
[0x8000c238]:addi a4, a4, 2048
[0x8000c23c]:add a5, a5, a4
[0x8000c240]:lw t4, 1908(a5)
[0x8000c244]:sub a5, a5, a4
[0x8000c248]:lui a4, 1
[0x8000c24c]:addi a4, a4, 2048
[0x8000c250]:add a5, a5, a4
[0x8000c254]:lw s10, 1912(a5)
[0x8000c258]:sub a5, a5, a4
[0x8000c25c]:lui a4, 1
[0x8000c260]:addi a4, a4, 2048
[0x8000c264]:add a5, a5, a4
[0x8000c268]:lw s11, 1916(a5)
[0x8000c26c]:sub a5, a5, a4
[0x8000c270]:addi t3, zero, 1
[0x8000c274]:lui t4, 1048320
[0x8000c278]:addi s10, zero, 0
[0x8000c27c]:lui s11, 524544
[0x8000c280]:addi a4, zero, 0
[0x8000c284]:csrrw zero, fcsr, a4
[0x8000c288]:fmul.d t5, t3, s10, dyn
[0x8000c28c]:csrrs a6, fcsr, zero

[0x8000c288]:fmul.d t5, t3, s10, dyn
[0x8000c28c]:csrrs a6, fcsr, zero
[0x8000c290]:sw t5, 1784(ra)
[0x8000c294]:sw t6, 1792(ra)
[0x8000c298]:sw t5, 1800(ra)
[0x8000c29c]:sw a6, 1808(ra)
[0x8000c2a0]:lui a4, 1
[0x8000c2a4]:addi a4, a4, 2048
[0x8000c2a8]:add a5, a5, a4
[0x8000c2ac]:lw t3, 1920(a5)
[0x8000c2b0]:sub a5, a5, a4
[0x8000c2b4]:lui a4, 1
[0x8000c2b8]:addi a4, a4, 2048
[0x8000c2bc]:add a5, a5, a4
[0x8000c2c0]:lw t4, 1924(a5)
[0x8000c2c4]:sub a5, a5, a4
[0x8000c2c8]:lui a4, 1
[0x8000c2cc]:addi a4, a4, 2048
[0x8000c2d0]:add a5, a5, a4
[0x8000c2d4]:lw s10, 1928(a5)
[0x8000c2d8]:sub a5, a5, a4
[0x8000c2dc]:lui a4, 1
[0x8000c2e0]:addi a4, a4, 2048
[0x8000c2e4]:add a5, a5, a4
[0x8000c2e8]:lw s11, 1932(a5)
[0x8000c2ec]:sub a5, a5, a4
[0x8000c2f0]:addi t3, zero, 1
[0x8000c2f4]:lui t4, 1048320
[0x8000c2f8]:addi s10, zero, 2
[0x8000c2fc]:lui s11, 256
[0x8000c300]:addi a4, zero, 0
[0x8000c304]:csrrw zero, fcsr, a4
[0x8000c308]:fmul.d t5, t3, s10, dyn
[0x8000c30c]:csrrs a6, fcsr, zero

[0x8000c308]:fmul.d t5, t3, s10, dyn
[0x8000c30c]:csrrs a6, fcsr, zero
[0x8000c310]:sw t5, 1816(ra)
[0x8000c314]:sw t6, 1824(ra)
[0x8000c318]:sw t5, 1832(ra)
[0x8000c31c]:sw a6, 1840(ra)
[0x8000c320]:lui a4, 1
[0x8000c324]:addi a4, a4, 2048
[0x8000c328]:add a5, a5, a4
[0x8000c32c]:lw t3, 1936(a5)
[0x8000c330]:sub a5, a5, a4
[0x8000c334]:lui a4, 1
[0x8000c338]:addi a4, a4, 2048
[0x8000c33c]:add a5, a5, a4
[0x8000c340]:lw t4, 1940(a5)
[0x8000c344]:sub a5, a5, a4
[0x8000c348]:lui a4, 1
[0x8000c34c]:addi a4, a4, 2048
[0x8000c350]:add a5, a5, a4
[0x8000c354]:lw s10, 1944(a5)
[0x8000c358]:sub a5, a5, a4
[0x8000c35c]:lui a4, 1
[0x8000c360]:addi a4, a4, 2048
[0x8000c364]:add a5, a5, a4
[0x8000c368]:lw s11, 1948(a5)
[0x8000c36c]:sub a5, a5, a4
[0x8000c370]:addi t3, zero, 1
[0x8000c374]:lui t4, 1048320
[0x8000c378]:addi s10, zero, 2
[0x8000c37c]:lui s11, 524544
[0x8000c380]:addi a4, zero, 0
[0x8000c384]:csrrw zero, fcsr, a4
[0x8000c388]:fmul.d t5, t3, s10, dyn
[0x8000c38c]:csrrs a6, fcsr, zero

[0x8000c388]:fmul.d t5, t3, s10, dyn
[0x8000c38c]:csrrs a6, fcsr, zero
[0x8000c390]:sw t5, 1848(ra)
[0x8000c394]:sw t6, 1856(ra)
[0x8000c398]:sw t5, 1864(ra)
[0x8000c39c]:sw a6, 1872(ra)
[0x8000c3a0]:lui a4, 1
[0x8000c3a4]:addi a4, a4, 2048
[0x8000c3a8]:add a5, a5, a4
[0x8000c3ac]:lw t3, 1952(a5)
[0x8000c3b0]:sub a5, a5, a4
[0x8000c3b4]:lui a4, 1
[0x8000c3b8]:addi a4, a4, 2048
[0x8000c3bc]:add a5, a5, a4
[0x8000c3c0]:lw t4, 1956(a5)
[0x8000c3c4]:sub a5, a5, a4
[0x8000c3c8]:lui a4, 1
[0x8000c3cc]:addi a4, a4, 2048
[0x8000c3d0]:add a5, a5, a4
[0x8000c3d4]:lw s10, 1960(a5)
[0x8000c3d8]:sub a5, a5, a4
[0x8000c3dc]:lui a4, 1
[0x8000c3e0]:addi a4, a4, 2048
[0x8000c3e4]:add a5, a5, a4
[0x8000c3e8]:lw s11, 1964(a5)
[0x8000c3ec]:sub a5, a5, a4
[0x8000c3f0]:addi t3, zero, 1
[0x8000c3f4]:lui t4, 1048320
[0x8000c3f8]:addi s10, zero, 4095
[0x8000c3fc]:lui s11, 524032
[0x8000c400]:addi s11, s11, 4095
[0x8000c404]:addi a4, zero, 0
[0x8000c408]:csrrw zero, fcsr, a4
[0x8000c40c]:fmul.d t5, t3, s10, dyn
[0x8000c410]:csrrs a6, fcsr, zero

[0x8000c40c]:fmul.d t5, t3, s10, dyn
[0x8000c410]:csrrs a6, fcsr, zero
[0x8000c414]:sw t5, 1880(ra)
[0x8000c418]:sw t6, 1888(ra)
[0x8000c41c]:sw t5, 1896(ra)
[0x8000c420]:sw a6, 1904(ra)
[0x8000c424]:lui a4, 1
[0x8000c428]:addi a4, a4, 2048
[0x8000c42c]:add a5, a5, a4
[0x8000c430]:lw t3, 1968(a5)
[0x8000c434]:sub a5, a5, a4
[0x8000c438]:lui a4, 1
[0x8000c43c]:addi a4, a4, 2048
[0x8000c440]:add a5, a5, a4
[0x8000c444]:lw t4, 1972(a5)
[0x8000c448]:sub a5, a5, a4
[0x8000c44c]:lui a4, 1
[0x8000c450]:addi a4, a4, 2048
[0x8000c454]:add a5, a5, a4
[0x8000c458]:lw s10, 1976(a5)
[0x8000c45c]:sub a5, a5, a4
[0x8000c460]:lui a4, 1
[0x8000c464]:addi a4, a4, 2048
[0x8000c468]:add a5, a5, a4
[0x8000c46c]:lw s11, 1980(a5)
[0x8000c470]:sub a5, a5, a4
[0x8000c474]:addi t3, zero, 1
[0x8000c478]:lui t4, 1048320
[0x8000c47c]:addi s10, zero, 4095
[0x8000c480]:lui s11, 1048320
[0x8000c484]:addi s11, s11, 4095
[0x8000c488]:addi a4, zero, 0
[0x8000c48c]:csrrw zero, fcsr, a4
[0x8000c490]:fmul.d t5, t3, s10, dyn
[0x8000c494]:csrrs a6, fcsr, zero

[0x8000c490]:fmul.d t5, t3, s10, dyn
[0x8000c494]:csrrs a6, fcsr, zero
[0x8000c498]:sw t5, 1912(ra)
[0x8000c49c]:sw t6, 1920(ra)
[0x8000c4a0]:sw t5, 1928(ra)
[0x8000c4a4]:sw a6, 1936(ra)
[0x8000c4a8]:lui a4, 1
[0x8000c4ac]:addi a4, a4, 2048
[0x8000c4b0]:add a5, a5, a4
[0x8000c4b4]:lw t3, 1984(a5)
[0x8000c4b8]:sub a5, a5, a4
[0x8000c4bc]:lui a4, 1
[0x8000c4c0]:addi a4, a4, 2048
[0x8000c4c4]:add a5, a5, a4
[0x8000c4c8]:lw t4, 1988(a5)
[0x8000c4cc]:sub a5, a5, a4
[0x8000c4d0]:lui a4, 1
[0x8000c4d4]:addi a4, a4, 2048
[0x8000c4d8]:add a5, a5, a4
[0x8000c4dc]:lw s10, 1992(a5)
[0x8000c4e0]:sub a5, a5, a4
[0x8000c4e4]:lui a4, 1
[0x8000c4e8]:addi a4, a4, 2048
[0x8000c4ec]:add a5, a5, a4
[0x8000c4f0]:lw s11, 1996(a5)
[0x8000c4f4]:sub a5, a5, a4
[0x8000c4f8]:addi t3, zero, 1
[0x8000c4fc]:lui t4, 1048320
[0x8000c500]:addi s10, zero, 0
[0x8000c504]:lui s11, 524032
[0x8000c508]:addi a4, zero, 0
[0x8000c50c]:csrrw zero, fcsr, a4
[0x8000c510]:fmul.d t5, t3, s10, dyn
[0x8000c514]:csrrs a6, fcsr, zero

[0x8000c510]:fmul.d t5, t3, s10, dyn
[0x8000c514]:csrrs a6, fcsr, zero
[0x8000c518]:sw t5, 1944(ra)
[0x8000c51c]:sw t6, 1952(ra)
[0x8000c520]:sw t5, 1960(ra)
[0x8000c524]:sw a6, 1968(ra)
[0x8000c528]:lui a4, 1
[0x8000c52c]:addi a4, a4, 2048
[0x8000c530]:add a5, a5, a4
[0x8000c534]:lw t3, 2000(a5)
[0x8000c538]:sub a5, a5, a4
[0x8000c53c]:lui a4, 1
[0x8000c540]:addi a4, a4, 2048
[0x8000c544]:add a5, a5, a4
[0x8000c548]:lw t4, 2004(a5)
[0x8000c54c]:sub a5, a5, a4
[0x8000c550]:lui a4, 1
[0x8000c554]:addi a4, a4, 2048
[0x8000c558]:add a5, a5, a4
[0x8000c55c]:lw s10, 2008(a5)
[0x8000c560]:sub a5, a5, a4
[0x8000c564]:lui a4, 1
[0x8000c568]:addi a4, a4, 2048
[0x8000c56c]:add a5, a5, a4
[0x8000c570]:lw s11, 2012(a5)
[0x8000c574]:sub a5, a5, a4
[0x8000c578]:addi t3, zero, 1
[0x8000c57c]:lui t4, 1048320
[0x8000c580]:addi s10, zero, 0
[0x8000c584]:lui s11, 1048320
[0x8000c588]:addi a4, zero, 0
[0x8000c58c]:csrrw zero, fcsr, a4
[0x8000c590]:fmul.d t5, t3, s10, dyn
[0x8000c594]:csrrs a6, fcsr, zero

[0x8000c590]:fmul.d t5, t3, s10, dyn
[0x8000c594]:csrrs a6, fcsr, zero
[0x8000c598]:sw t5, 1976(ra)
[0x8000c59c]:sw t6, 1984(ra)
[0x8000c5a0]:sw t5, 1992(ra)
[0x8000c5a4]:sw a6, 2000(ra)
[0x8000c5a8]:lui a4, 1
[0x8000c5ac]:addi a4, a4, 2048
[0x8000c5b0]:add a5, a5, a4
[0x8000c5b4]:lw t3, 2016(a5)
[0x8000c5b8]:sub a5, a5, a4
[0x8000c5bc]:lui a4, 1
[0x8000c5c0]:addi a4, a4, 2048
[0x8000c5c4]:add a5, a5, a4
[0x8000c5c8]:lw t4, 2020(a5)
[0x8000c5cc]:sub a5, a5, a4
[0x8000c5d0]:lui a4, 1
[0x8000c5d4]:addi a4, a4, 2048
[0x8000c5d8]:add a5, a5, a4
[0x8000c5dc]:lw s10, 2024(a5)
[0x8000c5e0]:sub a5, a5, a4
[0x8000c5e4]:lui a4, 1
[0x8000c5e8]:addi a4, a4, 2048
[0x8000c5ec]:add a5, a5, a4
[0x8000c5f0]:lw s11, 2028(a5)
[0x8000c5f4]:sub a5, a5, a4
[0x8000c5f8]:addi t3, zero, 1
[0x8000c5fc]:lui t4, 1048320
[0x8000c600]:addi s10, zero, 0
[0x8000c604]:lui s11, 524160
[0x8000c608]:addi a4, zero, 0
[0x8000c60c]:csrrw zero, fcsr, a4
[0x8000c610]:fmul.d t5, t3, s10, dyn
[0x8000c614]:csrrs a6, fcsr, zero

[0x8000c610]:fmul.d t5, t3, s10, dyn
[0x8000c614]:csrrs a6, fcsr, zero
[0x8000c618]:sw t5, 2008(ra)
[0x8000c61c]:sw t6, 2016(ra)
[0x8000c620]:sw t5, 2024(ra)
[0x8000c624]:sw a6, 2032(ra)
[0x8000c628]:lui a4, 1
[0x8000c62c]:addi a4, a4, 2048
[0x8000c630]:add a5, a5, a4
[0x8000c634]:lw t3, 2032(a5)
[0x8000c638]:sub a5, a5, a4
[0x8000c63c]:lui a4, 1
[0x8000c640]:addi a4, a4, 2048
[0x8000c644]:add a5, a5, a4
[0x8000c648]:lw t4, 2036(a5)
[0x8000c64c]:sub a5, a5, a4
[0x8000c650]:lui a4, 1
[0x8000c654]:addi a4, a4, 2048
[0x8000c658]:add a5, a5, a4
[0x8000c65c]:lw s10, 2040(a5)
[0x8000c660]:sub a5, a5, a4
[0x8000c664]:lui a4, 1
[0x8000c668]:addi a4, a4, 2048
[0x8000c66c]:add a5, a5, a4
[0x8000c670]:lw s11, 2044(a5)
[0x8000c674]:sub a5, a5, a4
[0x8000c678]:addi t3, zero, 1
[0x8000c67c]:lui t4, 1048320
[0x8000c680]:addi s10, zero, 0
[0x8000c684]:lui s11, 1048448
[0x8000c688]:addi a4, zero, 0
[0x8000c68c]:csrrw zero, fcsr, a4
[0x8000c690]:fmul.d t5, t3, s10, dyn
[0x8000c694]:csrrs a6, fcsr, zero

[0x8000c690]:fmul.d t5, t3, s10, dyn
[0x8000c694]:csrrs a6, fcsr, zero
[0x8000c698]:sw t5, 2040(ra)
[0x8000c69c]:addi ra, ra, 2040
[0x8000c6a0]:sw t6, 8(ra)
[0x8000c6a4]:sw t5, 16(ra)
[0x8000c6a8]:sw a6, 24(ra)
[0x8000c6ac]:auipc ra, 7
[0x8000c6b0]:addi ra, ra, 12
[0x8000c6b4]:lw t3, 0(a5)
[0x8000c6b8]:lw t4, 4(a5)
[0x8000c6bc]:lw s10, 8(a5)
[0x8000c6c0]:lw s11, 12(a5)
[0x8000c6c4]:addi t3, zero, 1
[0x8000c6c8]:lui t4, 1048320
[0x8000c6cc]:addi s10, zero, 1
[0x8000c6d0]:lui s11, 524160
[0x8000c6d4]:addi a4, zero, 0
[0x8000c6d8]:csrrw zero, fcsr, a4
[0x8000c6dc]:fmul.d t5, t3, s10, dyn
[0x8000c6e0]:csrrs a6, fcsr, zero

[0x8000cdec]:fmul.d t5, t3, s10, dyn
[0x8000cdf0]:csrrs a6, fcsr, zero
[0x8000cdf4]:sw t5, 896(ra)
[0x8000cdf8]:sw t6, 904(ra)
[0x8000cdfc]:sw t5, 912(ra)
[0x8000ce00]:sw a6, 920(ra)
[0x8000ce04]:lw t3, 464(a5)
[0x8000ce08]:lw t4, 468(a5)
[0x8000ce0c]:lw s10, 472(a5)
[0x8000ce10]:lw s11, 476(a5)
[0x8000ce14]:addi t3, zero, 0
[0x8000ce18]:lui t4, 261888
[0x8000ce1c]:addi s10, zero, 0
[0x8000ce20]:lui s11, 784384
[0x8000ce24]:addi a4, zero, 0
[0x8000ce28]:csrrw zero, fcsr, a4
[0x8000ce2c]:fmul.d t5, t3, s10, dyn
[0x8000ce30]:csrrs a6, fcsr, zero

[0x8000ce2c]:fmul.d t5, t3, s10, dyn
[0x8000ce30]:csrrs a6, fcsr, zero
[0x8000ce34]:sw t5, 928(ra)
[0x8000ce38]:sw t6, 936(ra)
[0x8000ce3c]:sw t5, 944(ra)
[0x8000ce40]:sw a6, 952(ra)
[0x8000ce44]:lw t3, 480(a5)
[0x8000ce48]:lw t4, 484(a5)
[0x8000ce4c]:lw s10, 488(a5)
[0x8000ce50]:lw s11, 492(a5)
[0x8000ce54]:addi t3, zero, 0
[0x8000ce58]:lui t4, 784384
[0x8000ce5c]:addi s10, zero, 0
[0x8000ce60]:addi s11, zero, 0
[0x8000ce64]:addi a4, zero, 0
[0x8000ce68]:csrrw zero, fcsr, a4
[0x8000ce6c]:fmul.d t5, t3, s10, dyn
[0x8000ce70]:csrrs a6, fcsr, zero

[0x8000ce6c]:fmul.d t5, t3, s10, dyn
[0x8000ce70]:csrrs a6, fcsr, zero
[0x8000ce74]:sw t5, 960(ra)
[0x8000ce78]:sw t6, 968(ra)
[0x8000ce7c]:sw t5, 976(ra)
[0x8000ce80]:sw a6, 984(ra)
[0x8000ce84]:lw t3, 496(a5)
[0x8000ce88]:lw t4, 500(a5)
[0x8000ce8c]:lw s10, 504(a5)
[0x8000ce90]:lw s11, 508(a5)
[0x8000ce94]:addi t3, zero, 0
[0x8000ce98]:lui t4, 784384
[0x8000ce9c]:addi s10, zero, 0
[0x8000cea0]:lui s11, 524288
[0x8000cea4]:addi a4, zero, 0
[0x8000cea8]:csrrw zero, fcsr, a4
[0x8000ceac]:fmul.d t5, t3, s10, dyn
[0x8000ceb0]:csrrs a6, fcsr, zero

[0x8000ceac]:fmul.d t5, t3, s10, dyn
[0x8000ceb0]:csrrs a6, fcsr, zero
[0x8000ceb4]:sw t5, 992(ra)
[0x8000ceb8]:sw t6, 1000(ra)
[0x8000cebc]:sw t5, 1008(ra)
[0x8000cec0]:sw a6, 1016(ra)
[0x8000cec4]:lw t3, 512(a5)
[0x8000cec8]:lw t4, 516(a5)
[0x8000cecc]:lw s10, 520(a5)
[0x8000ced0]:lw s11, 524(a5)
[0x8000ced4]:addi t3, zero, 0
[0x8000ced8]:lui t4, 784384
[0x8000cedc]:addi s10, zero, 1
[0x8000cee0]:addi s11, zero, 0
[0x8000cee4]:addi a4, zero, 0
[0x8000cee8]:csrrw zero, fcsr, a4
[0x8000ceec]:fmul.d t5, t3, s10, dyn
[0x8000cef0]:csrrs a6, fcsr, zero

[0x8000ceec]:fmul.d t5, t3, s10, dyn
[0x8000cef0]:csrrs a6, fcsr, zero
[0x8000cef4]:sw t5, 1024(ra)
[0x8000cef8]:sw t6, 1032(ra)
[0x8000cefc]:sw t5, 1040(ra)
[0x8000cf00]:sw a6, 1048(ra)
[0x8000cf04]:lw t3, 528(a5)
[0x8000cf08]:lw t4, 532(a5)
[0x8000cf0c]:lw s10, 536(a5)
[0x8000cf10]:lw s11, 540(a5)
[0x8000cf14]:addi t3, zero, 0
[0x8000cf18]:lui t4, 784384
[0x8000cf1c]:addi s10, zero, 1
[0x8000cf20]:lui s11, 524288
[0x8000cf24]:addi a4, zero, 0
[0x8000cf28]:csrrw zero, fcsr, a4
[0x8000cf2c]:fmul.d t5, t3, s10, dyn
[0x8000cf30]:csrrs a6, fcsr, zero

[0x8000cf2c]:fmul.d t5, t3, s10, dyn
[0x8000cf30]:csrrs a6, fcsr, zero
[0x8000cf34]:sw t5, 1056(ra)
[0x8000cf38]:sw t6, 1064(ra)
[0x8000cf3c]:sw t5, 1072(ra)
[0x8000cf40]:sw a6, 1080(ra)
[0x8000cf44]:lw t3, 544(a5)
[0x8000cf48]:lw t4, 548(a5)
[0x8000cf4c]:lw s10, 552(a5)
[0x8000cf50]:lw s11, 556(a5)
[0x8000cf54]:addi t3, zero, 0
[0x8000cf58]:lui t4, 784384
[0x8000cf5c]:addi s10, zero, 2
[0x8000cf60]:addi s11, zero, 0
[0x8000cf64]:addi a4, zero, 0
[0x8000cf68]:csrrw zero, fcsr, a4
[0x8000cf6c]:fmul.d t5, t3, s10, dyn
[0x8000cf70]:csrrs a6, fcsr, zero

[0x8000cf6c]:fmul.d t5, t3, s10, dyn
[0x8000cf70]:csrrs a6, fcsr, zero
[0x8000cf74]:sw t5, 1088(ra)
[0x8000cf78]:sw t6, 1096(ra)
[0x8000cf7c]:sw t5, 1104(ra)
[0x8000cf80]:sw a6, 1112(ra)
[0x8000cf84]:lw t3, 560(a5)
[0x8000cf88]:lw t4, 564(a5)
[0x8000cf8c]:lw s10, 568(a5)
[0x8000cf90]:lw s11, 572(a5)
[0x8000cf94]:addi t3, zero, 0
[0x8000cf98]:lui t4, 784384
[0x8000cf9c]:addi s10, zero, 2
[0x8000cfa0]:lui s11, 524288
[0x8000cfa4]:addi a4, zero, 0
[0x8000cfa8]:csrrw zero, fcsr, a4
[0x8000cfac]:fmul.d t5, t3, s10, dyn
[0x8000cfb0]:csrrs a6, fcsr, zero

[0x8000cfac]:fmul.d t5, t3, s10, dyn
[0x8000cfb0]:csrrs a6, fcsr, zero
[0x8000cfb4]:sw t5, 1120(ra)
[0x8000cfb8]:sw t6, 1128(ra)
[0x8000cfbc]:sw t5, 1136(ra)
[0x8000cfc0]:sw a6, 1144(ra)
[0x8000cfc4]:lw t3, 576(a5)
[0x8000cfc8]:lw t4, 580(a5)
[0x8000cfcc]:lw s10, 584(a5)
[0x8000cfd0]:lw s11, 588(a5)
[0x8000cfd4]:addi t3, zero, 0
[0x8000cfd8]:lui t4, 784384
[0x8000cfdc]:addi s10, zero, 4095
[0x8000cfe0]:lui s11, 256
[0x8000cfe4]:addi s11, s11, 4095
[0x8000cfe8]:addi a4, zero, 0
[0x8000cfec]:csrrw zero, fcsr, a4
[0x8000cff0]:fmul.d t5, t3, s10, dyn
[0x8000cff4]:csrrs a6, fcsr, zero

[0x8000cff0]:fmul.d t5, t3, s10, dyn
[0x8000cff4]:csrrs a6, fcsr, zero
[0x8000cff8]:sw t5, 1152(ra)
[0x8000cffc]:sw t6, 1160(ra)
[0x8000d000]:sw t5, 1168(ra)
[0x8000d004]:sw a6, 1176(ra)
[0x8000d008]:lw t3, 592(a5)
[0x8000d00c]:lw t4, 596(a5)
[0x8000d010]:lw s10, 600(a5)
[0x8000d014]:lw s11, 604(a5)
[0x8000d018]:addi t3, zero, 0
[0x8000d01c]:lui t4, 784384
[0x8000d020]:addi s10, zero, 4095
[0x8000d024]:lui s11, 524544
[0x8000d028]:addi s11, s11, 4095
[0x8000d02c]:addi a4, zero, 0
[0x8000d030]:csrrw zero, fcsr, a4
[0x8000d034]:fmul.d t5, t3, s10, dyn
[0x8000d038]:csrrs a6, fcsr, zero

[0x8000d034]:fmul.d t5, t3, s10, dyn
[0x8000d038]:csrrs a6, fcsr, zero
[0x8000d03c]:sw t5, 1184(ra)
[0x8000d040]:sw t6, 1192(ra)
[0x8000d044]:sw t5, 1200(ra)
[0x8000d048]:sw a6, 1208(ra)
[0x8000d04c]:lw t3, 608(a5)
[0x8000d050]:lw t4, 612(a5)
[0x8000d054]:lw s10, 616(a5)
[0x8000d058]:lw s11, 620(a5)
[0x8000d05c]:addi t3, zero, 0
[0x8000d060]:lui t4, 784384
[0x8000d064]:addi s10, zero, 0
[0x8000d068]:lui s11, 256
[0x8000d06c]:addi a4, zero, 0
[0x8000d070]:csrrw zero, fcsr, a4
[0x8000d074]:fmul.d t5, t3, s10, dyn
[0x8000d078]:csrrs a6, fcsr, zero

[0x8000d074]:fmul.d t5, t3, s10, dyn
[0x8000d078]:csrrs a6, fcsr, zero
[0x8000d07c]:sw t5, 1216(ra)
[0x8000d080]:sw t6, 1224(ra)
[0x8000d084]:sw t5, 1232(ra)
[0x8000d088]:sw a6, 1240(ra)
[0x8000d08c]:lw t3, 624(a5)
[0x8000d090]:lw t4, 628(a5)
[0x8000d094]:lw s10, 632(a5)
[0x8000d098]:lw s11, 636(a5)
[0x8000d09c]:addi t3, zero, 0
[0x8000d0a0]:lui t4, 784384
[0x8000d0a4]:addi s10, zero, 0
[0x8000d0a8]:lui s11, 524544
[0x8000d0ac]:addi a4, zero, 0
[0x8000d0b0]:csrrw zero, fcsr, a4
[0x8000d0b4]:fmul.d t5, t3, s10, dyn
[0x8000d0b8]:csrrs a6, fcsr, zero

[0x8000d0b4]:fmul.d t5, t3, s10, dyn
[0x8000d0b8]:csrrs a6, fcsr, zero
[0x8000d0bc]:sw t5, 1248(ra)
[0x8000d0c0]:sw t6, 1256(ra)
[0x8000d0c4]:sw t5, 1264(ra)
[0x8000d0c8]:sw a6, 1272(ra)
[0x8000d0cc]:lw t3, 640(a5)
[0x8000d0d0]:lw t4, 644(a5)
[0x8000d0d4]:lw s10, 648(a5)
[0x8000d0d8]:lw s11, 652(a5)
[0x8000d0dc]:addi t3, zero, 0
[0x8000d0e0]:lui t4, 784384
[0x8000d0e4]:addi s10, zero, 2
[0x8000d0e8]:lui s11, 256
[0x8000d0ec]:addi a4, zero, 0
[0x8000d0f0]:csrrw zero, fcsr, a4
[0x8000d0f4]:fmul.d t5, t3, s10, dyn
[0x8000d0f8]:csrrs a6, fcsr, zero

[0x8000d0f4]:fmul.d t5, t3, s10, dyn
[0x8000d0f8]:csrrs a6, fcsr, zero
[0x8000d0fc]:sw t5, 1280(ra)
[0x8000d100]:sw t6, 1288(ra)
[0x8000d104]:sw t5, 1296(ra)
[0x8000d108]:sw a6, 1304(ra)
[0x8000d10c]:lw t3, 656(a5)
[0x8000d110]:lw t4, 660(a5)
[0x8000d114]:lw s10, 664(a5)
[0x8000d118]:lw s11, 668(a5)
[0x8000d11c]:addi t3, zero, 0
[0x8000d120]:lui t4, 784384
[0x8000d124]:addi s10, zero, 2
[0x8000d128]:lui s11, 524544
[0x8000d12c]:addi a4, zero, 0
[0x8000d130]:csrrw zero, fcsr, a4
[0x8000d134]:fmul.d t5, t3, s10, dyn
[0x8000d138]:csrrs a6, fcsr, zero

[0x8000d134]:fmul.d t5, t3, s10, dyn
[0x8000d138]:csrrs a6, fcsr, zero
[0x8000d13c]:sw t5, 1312(ra)
[0x8000d140]:sw t6, 1320(ra)
[0x8000d144]:sw t5, 1328(ra)
[0x8000d148]:sw a6, 1336(ra)
[0x8000d14c]:lw t3, 672(a5)
[0x8000d150]:lw t4, 676(a5)
[0x8000d154]:lw s10, 680(a5)
[0x8000d158]:lw s11, 684(a5)
[0x8000d15c]:addi t3, zero, 0
[0x8000d160]:lui t4, 784384
[0x8000d164]:addi s10, zero, 4095
[0x8000d168]:lui s11, 524032
[0x8000d16c]:addi s11, s11, 4095
[0x8000d170]:addi a4, zero, 0
[0x8000d174]:csrrw zero, fcsr, a4
[0x8000d178]:fmul.d t5, t3, s10, dyn
[0x8000d17c]:csrrs a6, fcsr, zero

[0x8000d178]:fmul.d t5, t3, s10, dyn
[0x8000d17c]:csrrs a6, fcsr, zero
[0x8000d180]:sw t5, 1344(ra)
[0x8000d184]:sw t6, 1352(ra)
[0x8000d188]:sw t5, 1360(ra)
[0x8000d18c]:sw a6, 1368(ra)
[0x8000d190]:lw t3, 688(a5)
[0x8000d194]:lw t4, 692(a5)
[0x8000d198]:lw s10, 696(a5)
[0x8000d19c]:lw s11, 700(a5)
[0x8000d1a0]:addi t3, zero, 0
[0x8000d1a4]:lui t4, 784384
[0x8000d1a8]:addi s10, zero, 4095
[0x8000d1ac]:lui s11, 1048320
[0x8000d1b0]:addi s11, s11, 4095
[0x8000d1b4]:addi a4, zero, 0
[0x8000d1b8]:csrrw zero, fcsr, a4
[0x8000d1bc]:fmul.d t5, t3, s10, dyn
[0x8000d1c0]:csrrs a6, fcsr, zero

[0x8000d1bc]:fmul.d t5, t3, s10, dyn
[0x8000d1c0]:csrrs a6, fcsr, zero
[0x8000d1c4]:sw t5, 1376(ra)
[0x8000d1c8]:sw t6, 1384(ra)
[0x8000d1cc]:sw t5, 1392(ra)
[0x8000d1d0]:sw a6, 1400(ra)
[0x8000d1d4]:lw t3, 704(a5)
[0x8000d1d8]:lw t4, 708(a5)
[0x8000d1dc]:lw s10, 712(a5)
[0x8000d1e0]:lw s11, 716(a5)
[0x8000d1e4]:addi t3, zero, 0
[0x8000d1e8]:lui t4, 784384
[0x8000d1ec]:addi s10, zero, 0
[0x8000d1f0]:lui s11, 524032
[0x8000d1f4]:addi a4, zero, 0
[0x8000d1f8]:csrrw zero, fcsr, a4
[0x8000d1fc]:fmul.d t5, t3, s10, dyn
[0x8000d200]:csrrs a6, fcsr, zero

[0x8000d1fc]:fmul.d t5, t3, s10, dyn
[0x8000d200]:csrrs a6, fcsr, zero
[0x8000d204]:sw t5, 1408(ra)
[0x8000d208]:sw t6, 1416(ra)
[0x8000d20c]:sw t5, 1424(ra)
[0x8000d210]:sw a6, 1432(ra)
[0x8000d214]:lw t3, 720(a5)
[0x8000d218]:lw t4, 724(a5)
[0x8000d21c]:lw s10, 728(a5)
[0x8000d220]:lw s11, 732(a5)
[0x8000d224]:addi t3, zero, 0
[0x8000d228]:lui t4, 784384
[0x8000d22c]:addi s10, zero, 0
[0x8000d230]:lui s11, 1048320
[0x8000d234]:addi a4, zero, 0
[0x8000d238]:csrrw zero, fcsr, a4
[0x8000d23c]:fmul.d t5, t3, s10, dyn
[0x8000d240]:csrrs a6, fcsr, zero

[0x8000d23c]:fmul.d t5, t3, s10, dyn
[0x8000d240]:csrrs a6, fcsr, zero
[0x8000d244]:sw t5, 1440(ra)
[0x8000d248]:sw t6, 1448(ra)
[0x8000d24c]:sw t5, 1456(ra)
[0x8000d250]:sw a6, 1464(ra)
[0x8000d254]:lw t3, 736(a5)
[0x8000d258]:lw t4, 740(a5)
[0x8000d25c]:lw s10, 744(a5)
[0x8000d260]:lw s11, 748(a5)
[0x8000d264]:addi t3, zero, 0
[0x8000d268]:lui t4, 784384
[0x8000d26c]:addi s10, zero, 0
[0x8000d270]:lui s11, 524160
[0x8000d274]:addi a4, zero, 0
[0x8000d278]:csrrw zero, fcsr, a4
[0x8000d27c]:fmul.d t5, t3, s10, dyn
[0x8000d280]:csrrs a6, fcsr, zero

[0x8000d27c]:fmul.d t5, t3, s10, dyn
[0x8000d280]:csrrs a6, fcsr, zero
[0x8000d284]:sw t5, 1472(ra)
[0x8000d288]:sw t6, 1480(ra)
[0x8000d28c]:sw t5, 1488(ra)
[0x8000d290]:sw a6, 1496(ra)
[0x8000d294]:lw t3, 752(a5)
[0x8000d298]:lw t4, 756(a5)
[0x8000d29c]:lw s10, 760(a5)
[0x8000d2a0]:lw s11, 764(a5)
[0x8000d2a4]:addi t3, zero, 0
[0x8000d2a8]:lui t4, 784384
[0x8000d2ac]:addi s10, zero, 0
[0x8000d2b0]:lui s11, 1048448
[0x8000d2b4]:addi a4, zero, 0
[0x8000d2b8]:csrrw zero, fcsr, a4
[0x8000d2bc]:fmul.d t5, t3, s10, dyn
[0x8000d2c0]:csrrs a6, fcsr, zero

[0x8000d2bc]:fmul.d t5, t3, s10, dyn
[0x8000d2c0]:csrrs a6, fcsr, zero
[0x8000d2c4]:sw t5, 1504(ra)
[0x8000d2c8]:sw t6, 1512(ra)
[0x8000d2cc]:sw t5, 1520(ra)
[0x8000d2d0]:sw a6, 1528(ra)
[0x8000d2d4]:lw t3, 768(a5)
[0x8000d2d8]:lw t4, 772(a5)
[0x8000d2dc]:lw s10, 776(a5)
[0x8000d2e0]:lw s11, 780(a5)
[0x8000d2e4]:addi t3, zero, 0
[0x8000d2e8]:lui t4, 784384
[0x8000d2ec]:addi s10, zero, 1
[0x8000d2f0]:lui s11, 524160
[0x8000d2f4]:addi a4, zero, 0
[0x8000d2f8]:csrrw zero, fcsr, a4
[0x8000d2fc]:fmul.d t5, t3, s10, dyn
[0x8000d300]:csrrs a6, fcsr, zero

[0x8000d2fc]:fmul.d t5, t3, s10, dyn
[0x8000d300]:csrrs a6, fcsr, zero
[0x8000d304]:sw t5, 1536(ra)
[0x8000d308]:sw t6, 1544(ra)
[0x8000d30c]:sw t5, 1552(ra)
[0x8000d310]:sw a6, 1560(ra)
[0x8000d314]:lw t3, 784(a5)
[0x8000d318]:lw t4, 788(a5)
[0x8000d31c]:lw s10, 792(a5)
[0x8000d320]:lw s11, 796(a5)
[0x8000d324]:addi t3, zero, 0
[0x8000d328]:lui t4, 784384
[0x8000d32c]:addi s10, zero, 1
[0x8000d330]:lui s11, 1048448
[0x8000d334]:addi a4, zero, 0
[0x8000d338]:csrrw zero, fcsr, a4
[0x8000d33c]:fmul.d t5, t3, s10, dyn
[0x8000d340]:csrrs a6, fcsr, zero

[0x8000d33c]:fmul.d t5, t3, s10, dyn
[0x8000d340]:csrrs a6, fcsr, zero
[0x8000d344]:sw t5, 1568(ra)
[0x8000d348]:sw t6, 1576(ra)
[0x8000d34c]:sw t5, 1584(ra)
[0x8000d350]:sw a6, 1592(ra)
[0x8000d354]:lw t3, 800(a5)
[0x8000d358]:lw t4, 804(a5)
[0x8000d35c]:lw s10, 808(a5)
[0x8000d360]:lw s11, 812(a5)
[0x8000d364]:addi t3, zero, 0
[0x8000d368]:lui t4, 784384
[0x8000d36c]:addi s10, zero, 1
[0x8000d370]:lui s11, 524032
[0x8000d374]:addi a4, zero, 0
[0x8000d378]:csrrw zero, fcsr, a4
[0x8000d37c]:fmul.d t5, t3, s10, dyn
[0x8000d380]:csrrs a6, fcsr, zero

[0x8000d37c]:fmul.d t5, t3, s10, dyn
[0x8000d380]:csrrs a6, fcsr, zero
[0x8000d384]:sw t5, 1600(ra)
[0x8000d388]:sw t6, 1608(ra)
[0x8000d38c]:sw t5, 1616(ra)
[0x8000d390]:sw a6, 1624(ra)
[0x8000d394]:lw t3, 816(a5)
[0x8000d398]:lw t4, 820(a5)
[0x8000d39c]:lw s10, 824(a5)
[0x8000d3a0]:lw s11, 828(a5)
[0x8000d3a4]:addi t3, zero, 0
[0x8000d3a8]:lui t4, 784384
[0x8000d3ac]:addi s10, zero, 1
[0x8000d3b0]:lui s11, 1048320
[0x8000d3b4]:addi a4, zero, 0
[0x8000d3b8]:csrrw zero, fcsr, a4
[0x8000d3bc]:fmul.d t5, t3, s10, dyn
[0x8000d3c0]:csrrs a6, fcsr, zero

[0x8000d3bc]:fmul.d t5, t3, s10, dyn
[0x8000d3c0]:csrrs a6, fcsr, zero
[0x8000d3c4]:sw t5, 1632(ra)
[0x8000d3c8]:sw t6, 1640(ra)
[0x8000d3cc]:sw t5, 1648(ra)
[0x8000d3d0]:sw a6, 1656(ra)
[0x8000d3d4]:lw t3, 832(a5)
[0x8000d3d8]:lw t4, 836(a5)
[0x8000d3dc]:lw s10, 840(a5)
[0x8000d3e0]:lw s11, 844(a5)
[0x8000d3e4]:addi t3, zero, 0
[0x8000d3e8]:lui t4, 784384
[0x8000d3ec]:addi s10, zero, 0
[0x8000d3f0]:lui s11, 261888
[0x8000d3f4]:addi a4, zero, 0
[0x8000d3f8]:csrrw zero, fcsr, a4
[0x8000d3fc]:fmul.d t5, t3, s10, dyn
[0x8000d400]:csrrs a6, fcsr, zero

[0x8000d3fc]:fmul.d t5, t3, s10, dyn
[0x8000d400]:csrrs a6, fcsr, zero
[0x8000d404]:sw t5, 1664(ra)
[0x8000d408]:sw t6, 1672(ra)
[0x8000d40c]:sw t5, 1680(ra)
[0x8000d410]:sw a6, 1688(ra)
[0x8000d414]:lw t3, 848(a5)
[0x8000d418]:lw t4, 852(a5)
[0x8000d41c]:lw s10, 856(a5)
[0x8000d420]:lw s11, 860(a5)
[0x8000d424]:addi t3, zero, 0
[0x8000d428]:lui t4, 784384
[0x8000d42c]:addi s10, zero, 0
[0x8000d430]:lui s11, 784384
[0x8000d434]:addi a4, zero, 0
[0x8000d438]:csrrw zero, fcsr, a4
[0x8000d43c]:fmul.d t5, t3, s10, dyn
[0x8000d440]:csrrs a6, fcsr, zero

[0x8000d43c]:fmul.d t5, t3, s10, dyn
[0x8000d440]:csrrs a6, fcsr, zero
[0x8000d444]:sw t5, 1696(ra)
[0x8000d448]:sw t6, 1704(ra)
[0x8000d44c]:sw t5, 1712(ra)
[0x8000d450]:sw a6, 1720(ra)
[0x8000d454]:lw t3, 864(a5)
[0x8000d458]:lw t4, 868(a5)
[0x8000d45c]:lw s10, 872(a5)
[0x8000d460]:lw s11, 876(a5)
[0x8000d464]:addi t3, zero, 0
[0x8000d468]:addi t4, zero, 0
[0x8000d46c]:addi s10, zero, 1
[0x8000d470]:addi s11, zero, 0
[0x8000d474]:addi a4, zero, 0
[0x8000d478]:csrrw zero, fcsr, a4
[0x8000d47c]:fmul.d t5, t3, s10, dyn
[0x8000d480]:csrrs a6, fcsr, zero

[0x8000d47c]:fmul.d t5, t3, s10, dyn
[0x8000d480]:csrrs a6, fcsr, zero
[0x8000d484]:sw t5, 1728(ra)
[0x8000d488]:sw t6, 1736(ra)
[0x8000d48c]:sw t5, 1744(ra)
[0x8000d490]:sw a6, 1752(ra)
[0x8000d494]:lw t3, 880(a5)
[0x8000d498]:lw t4, 884(a5)
[0x8000d49c]:lw s10, 888(a5)
[0x8000d4a0]:lw s11, 892(a5)
[0x8000d4a4]:addi t3, zero, 0
[0x8000d4a8]:addi t4, zero, 0
[0x8000d4ac]:addi s10, zero, 1
[0x8000d4b0]:lui s11, 524288
[0x8000d4b4]:addi a4, zero, 0
[0x8000d4b8]:csrrw zero, fcsr, a4
[0x8000d4bc]:fmul.d t5, t3, s10, dyn
[0x8000d4c0]:csrrs a6, fcsr, zero

[0x8000d4bc]:fmul.d t5, t3, s10, dyn
[0x8000d4c0]:csrrs a6, fcsr, zero
[0x8000d4c4]:sw t5, 1760(ra)
[0x8000d4c8]:sw t6, 1768(ra)
[0x8000d4cc]:sw t5, 1776(ra)
[0x8000d4d0]:sw a6, 1784(ra)
[0x8000d4d4]:addi zero, zero, 0
[0x8000d4d8]:addi zero, zero, 0
[0x8000d4dc]:addi zero, zero, 0



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fmul.d', 'rs1 : x28', 'rs2 : x26', 'rd : x30', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000013c]:fmul.d t5, t3, s10, dyn
	-[0x80000140]:csrrs tp, fcsr, zero
	-[0x80000144]:sw t5, 0(ra)
	-[0x80000148]:sw t6, 8(ra)
Current Store : [0x80000148] : sw t6, 8(ra) -- Store: [0x80011620]:0xFBB6FAB7




Last Coverpoint : ['mnemonic : fmul.d', 'rs1 : x28', 'rs2 : x26', 'rd : x30', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000013c]:fmul.d t5, t3, s10, dyn
	-[0x80000140]:csrrs tp, fcsr, zero
	-[0x80000144]:sw t5, 0(ra)
	-[0x80000148]:sw t6, 8(ra)
	-[0x8000014c]:sw t5, 16(ra)
Current Store : [0x8000014c] : sw t5, 16(ra) -- Store: [0x80011628]:0x00000000




Last Coverpoint : ['rs1 : x26', 'rs2 : x30', 'rd : x26', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000017c]:fmul.d s10, s10, t5, dyn
	-[0x80000180]:csrrs tp, fcsr, zero
	-[0x80000184]:sw s10, 32(ra)
	-[0x80000188]:sw s11, 40(ra)
Current Store : [0x80000188] : sw s11, 40(ra) -- Store: [0x80011640]:0x00000000




Last Coverpoint : ['rs1 : x26', 'rs2 : x30', 'rd : x26', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000017c]:fmul.d s10, s10, t5, dyn
	-[0x80000180]:csrrs tp, fcsr, zero
	-[0x80000184]:sw s10, 32(ra)
	-[0x80000188]:sw s11, 40(ra)
	-[0x8000018c]:sw s10, 48(ra)
Current Store : [0x8000018c] : sw s10, 48(ra) -- Store: [0x80011648]:0x00000000




Last Coverpoint : ['rs1 : x24', 'rs2 : x24', 'rd : x24', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x800001bc]:fmul.d s8, s8, s8, dyn
	-[0x800001c0]:csrrs tp, fcsr, zero
	-[0x800001c4]:sw s8, 64(ra)
	-[0x800001c8]:sw s9, 72(ra)
Current Store : [0x800001c8] : sw s9, 72(ra) -- Store: [0x80011660]:0x00000000




Last Coverpoint : ['rs1 : x24', 'rs2 : x24', 'rd : x24', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x800001bc]:fmul.d s8, s8, s8, dyn
	-[0x800001c0]:csrrs tp, fcsr, zero
	-[0x800001c4]:sw s8, 64(ra)
	-[0x800001c8]:sw s9, 72(ra)
	-[0x800001cc]:sw s8, 80(ra)
Current Store : [0x800001cc] : sw s8, 80(ra) -- Store: [0x80011668]:0x00000000




Last Coverpoint : ['rs1 : x22', 'rs2 : x22', 'rd : x28', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x800001fc]:fmul.d t3, s6, s6, dyn
	-[0x80000200]:csrrs tp, fcsr, zero
	-[0x80000204]:sw t3, 96(ra)
	-[0x80000208]:sw t4, 104(ra)
Current Store : [0x80000208] : sw t4, 104(ra) -- Store: [0x80011680]:0x00000000




Last Coverpoint : ['rs1 : x22', 'rs2 : x22', 'rd : x28', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x800001fc]:fmul.d t3, s6, s6, dyn
	-[0x80000200]:csrrs tp, fcsr, zero
	-[0x80000204]:sw t3, 96(ra)
	-[0x80000208]:sw t4, 104(ra)
	-[0x8000020c]:sw t3, 112(ra)
Current Store : [0x8000020c] : sw t3, 112(ra) -- Store: [0x80011688]:0x00000000




Last Coverpoint : ['rs1 : x30', 'rs2 : x20', 'rd : x20', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000023c]:fmul.d s4, t5, s4, dyn
	-[0x80000240]:csrrs tp, fcsr, zero
	-[0x80000244]:sw s4, 128(ra)
	-[0x80000248]:sw s5, 136(ra)
Current Store : [0x80000248] : sw s5, 136(ra) -- Store: [0x800116a0]:0x00000000




Last Coverpoint : ['rs1 : x30', 'rs2 : x20', 'rd : x20', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000023c]:fmul.d s4, t5, s4, dyn
	-[0x80000240]:csrrs tp, fcsr, zero
	-[0x80000244]:sw s4, 128(ra)
	-[0x80000248]:sw s5, 136(ra)
	-[0x8000024c]:sw s4, 144(ra)
Current Store : [0x8000024c] : sw s4, 144(ra) -- Store: [0x800116a8]:0x00000000




Last Coverpoint : ['rs1 : x20', 'rs2 : x28', 'rd : x22', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000027c]:fmul.d s6, s4, t3, dyn
	-[0x80000280]:csrrs tp, fcsr, zero
	-[0x80000284]:sw s6, 160(ra)
	-[0x80000288]:sw s7, 168(ra)
Current Store : [0x80000288] : sw s7, 168(ra) -- Store: [0x800116c0]:0x00000000




Last Coverpoint : ['rs1 : x20', 'rs2 : x28', 'rd : x22', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000027c]:fmul.d s6, s4, t3, dyn
	-[0x80000280]:csrrs tp, fcsr, zero
	-[0x80000284]:sw s6, 160(ra)
	-[0x80000288]:sw s7, 168(ra)
	-[0x8000028c]:sw s6, 176(ra)
Current Store : [0x8000028c] : sw s6, 176(ra) -- Store: [0x800116c8]:0x00000000




Last Coverpoint : ['rs1 : x16', 'rs2 : x14', 'rd : x18', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002c0]:fmul.d s2, a6, a4, dyn
	-[0x800002c4]:csrrs tp, fcsr, zero
	-[0x800002c8]:sw s2, 192(ra)
	-[0x800002cc]:sw s3, 200(ra)
Current Store : [0x800002cc] : sw s3, 200(ra) -- Store: [0x800116e0]:0x6FAB7FBB




Last Coverpoint : ['rs1 : x16', 'rs2 : x14', 'rd : x18', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002c0]:fmul.d s2, a6, a4, dyn
	-[0x800002c4]:csrrs tp, fcsr, zero
	-[0x800002c8]:sw s2, 192(ra)
	-[0x800002cc]:sw s3, 200(ra)
	-[0x800002d0]:sw s2, 208(ra)
Current Store : [0x800002d0] : sw s2, 208(ra) -- Store: [0x800116e8]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rs2 : x18', 'rd : x16', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000304]:fmul.d a6, a4, s2, dyn
	-[0x80000308]:csrrs tp, fcsr, zero
	-[0x8000030c]:sw a6, 224(ra)
	-[0x80000310]:sw a7, 232(ra)
Current Store : [0x80000310] : sw a7, 232(ra) -- Store: [0x80011700]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rs2 : x18', 'rd : x16', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000304]:fmul.d a6, a4, s2, dyn
	-[0x80000308]:csrrs tp, fcsr, zero
	-[0x8000030c]:sw a6, 224(ra)
	-[0x80000310]:sw a7, 232(ra)
	-[0x80000314]:sw a6, 240(ra)
Current Store : [0x80000314] : sw a6, 240(ra) -- Store: [0x80011708]:0x00000000




Last Coverpoint : ['rs1 : x18', 'rs2 : x16', 'rd : x14', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000344]:fmul.d a4, s2, a6, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:sw a4, 256(ra)
	-[0x80000350]:sw a5, 264(ra)
Current Store : [0x80000350] : sw a5, 264(ra) -- Store: [0x80011720]:0x00000000




Last Coverpoint : ['rs1 : x18', 'rs2 : x16', 'rd : x14', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000344]:fmul.d a4, s2, a6, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:sw a4, 256(ra)
	-[0x80000350]:sw a5, 264(ra)
	-[0x80000354]:sw a4, 272(ra)
Current Store : [0x80000354] : sw a4, 272(ra) -- Store: [0x80011728]:0x00000000




Last Coverpoint : ['rs1 : x10', 'rs2 : x8', 'rd : x12', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000384]:fmul.d a2, a0, fp, dyn
	-[0x80000388]:csrrs tp, fcsr, zero
	-[0x8000038c]:sw a2, 288(ra)
	-[0x80000390]:sw a3, 296(ra)
Current Store : [0x80000390] : sw a3, 296(ra) -- Store: [0x80011740]:0xEADFEEDB




Last Coverpoint : ['rs1 : x10', 'rs2 : x8', 'rd : x12', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000384]:fmul.d a2, a0, fp, dyn
	-[0x80000388]:csrrs tp, fcsr, zero
	-[0x8000038c]:sw a2, 288(ra)
	-[0x80000390]:sw a3, 296(ra)
	-[0x80000394]:sw a2, 304(ra)
Current Store : [0x80000394] : sw a2, 304(ra) -- Store: [0x80011748]:0x00000000




Last Coverpoint : ['rs1 : x8', 'rs2 : x12', 'rd : x10', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003d4]:fmul.d a0, fp, a2, dyn
	-[0x800003d8]:csrrs a6, fcsr, zero
	-[0x800003dc]:sw a0, 0(ra)
	-[0x800003e0]:sw a1, 8(ra)
Current Store : [0x800003e0] : sw a1, 8(ra) -- Store: [0x800116c0]:0x00000000




Last Coverpoint : ['rs1 : x8', 'rs2 : x12', 'rd : x10', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003d4]:fmul.d a0, fp, a2, dyn
	-[0x800003d8]:csrrs a6, fcsr, zero
	-[0x800003dc]:sw a0, 0(ra)
	-[0x800003e0]:sw a1, 8(ra)
	-[0x800003e4]:sw a0, 16(ra)
Current Store : [0x800003e4] : sw a0, 16(ra) -- Store: [0x800116c8]:0x00000000




Last Coverpoint : ['rs1 : x12', 'rs2 : x10', 'rd : x8', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000414]:fmul.d fp, a2, a0, dyn
	-[0x80000418]:csrrs a6, fcsr, zero
	-[0x8000041c]:sw fp, 32(ra)
	-[0x80000420]:sw s1, 40(ra)
Current Store : [0x80000420] : sw s1, 40(ra) -- Store: [0x800116e0]:0x00000000




Last Coverpoint : ['rs1 : x12', 'rs2 : x10', 'rd : x8', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000414]:fmul.d fp, a2, a0, dyn
	-[0x80000418]:csrrs a6, fcsr, zero
	-[0x8000041c]:sw fp, 32(ra)
	-[0x80000420]:sw s1, 40(ra)
	-[0x80000424]:sw fp, 48(ra)
Current Store : [0x80000424] : sw fp, 48(ra) -- Store: [0x800116e8]:0x00000000




Last Coverpoint : ['rs1 : x4', 'rs2 : x2', 'rd : x6', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000458]:fmul.d t1, tp, sp, dyn
	-[0x8000045c]:csrrs a6, fcsr, zero
	-[0x80000460]:sw t1, 64(ra)
	-[0x80000464]:sw t2, 72(ra)
Current Store : [0x80000464] : sw t2, 72(ra) -- Store: [0x80011700]:0xB7FBB6FA




Last Coverpoint : ['rs1 : x4', 'rs2 : x2', 'rd : x6', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000458]:fmul.d t1, tp, sp, dyn
	-[0x8000045c]:csrrs a6, fcsr, zero
	-[0x80000460]:sw t1, 64(ra)
	-[0x80000464]:sw t2, 72(ra)
	-[0x80000468]:sw t1, 80(ra)
Current Store : [0x80000468] : sw t1, 80(ra) -- Store: [0x80011708]:0x00000000




Last Coverpoint : ['rs1 : x2', 'rs2 : x6', 'rd : x4', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000049c]:fmul.d tp, sp, t1, dyn
	-[0x800004a0]:csrrs a6, fcsr, zero
	-[0x800004a4]:sw tp, 96(ra)
	-[0x800004a8]:sw t0, 104(ra)
Current Store : [0x800004a8] : sw t0, 104(ra) -- Store: [0x80011720]:0x00000000




Last Coverpoint : ['rs1 : x2', 'rs2 : x6', 'rd : x4', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000049c]:fmul.d tp, sp, t1, dyn
	-[0x800004a0]:csrrs a6, fcsr, zero
	-[0x800004a4]:sw tp, 96(ra)
	-[0x800004a8]:sw t0, 104(ra)
	-[0x800004ac]:sw tp, 112(ra)
Current Store : [0x800004ac] : sw tp, 112(ra) -- Store: [0x80011728]:0x00000000




Last Coverpoint : ['rs1 : x6', 'rs2 : x4', 'rd : x2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004dc]:fmul.d sp, t1, tp, dyn
	-[0x800004e0]:csrrs a6, fcsr, zero
	-[0x800004e4]:sw sp, 128(ra)
	-[0x800004e8]:sw gp, 136(ra)
Current Store : [0x800004e8] : sw gp, 136(ra) -- Store: [0x80011740]:0x00000000




Last Coverpoint : ['rs1 : x6', 'rs2 : x4', 'rd : x2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004dc]:fmul.d sp, t1, tp, dyn
	-[0x800004e0]:csrrs a6, fcsr, zero
	-[0x800004e4]:sw sp, 128(ra)
	-[0x800004e8]:sw gp, 136(ra)
	-[0x800004ec]:sw sp, 144(ra)
Current Store : [0x800004ec] : sw sp, 144(ra) -- Store: [0x80011748]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000051c]:fmul.d t5, t3, s10, dyn
	-[0x80000520]:csrrs a6, fcsr, zero
	-[0x80000524]:sw t5, 160(ra)
	-[0x80000528]:sw t6, 168(ra)
Current Store : [0x80000528] : sw t6, 168(ra) -- Store: [0x80011760]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000051c]:fmul.d t5, t3, s10, dyn
	-[0x80000520]:csrrs a6, fcsr, zero
	-[0x80000524]:sw t5, 160(ra)
	-[0x80000528]:sw t6, 168(ra)
	-[0x8000052c]:sw t5, 176(ra)
Current Store : [0x8000052c] : sw t5, 176(ra) -- Store: [0x80011768]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000055c]:fmul.d t5, t3, s10, dyn
	-[0x80000560]:csrrs a6, fcsr, zero
	-[0x80000564]:sw t5, 192(ra)
	-[0x80000568]:sw t6, 200(ra)
Current Store : [0x80000568] : sw t6, 200(ra) -- Store: [0x80011780]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000055c]:fmul.d t5, t3, s10, dyn
	-[0x80000560]:csrrs a6, fcsr, zero
	-[0x80000564]:sw t5, 192(ra)
	-[0x80000568]:sw t6, 200(ra)
	-[0x8000056c]:sw t5, 208(ra)
Current Store : [0x8000056c] : sw t5, 208(ra) -- Store: [0x80011788]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000059c]:fmul.d t5, t3, s10, dyn
	-[0x800005a0]:csrrs a6, fcsr, zero
	-[0x800005a4]:sw t5, 224(ra)
	-[0x800005a8]:sw t6, 232(ra)
Current Store : [0x800005a8] : sw t6, 232(ra) -- Store: [0x800117a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000059c]:fmul.d t5, t3, s10, dyn
	-[0x800005a0]:csrrs a6, fcsr, zero
	-[0x800005a4]:sw t5, 224(ra)
	-[0x800005a8]:sw t6, 232(ra)
	-[0x800005ac]:sw t5, 240(ra)
Current Store : [0x800005ac] : sw t5, 240(ra) -- Store: [0x800117a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005dc]:fmul.d t5, t3, s10, dyn
	-[0x800005e0]:csrrs a6, fcsr, zero
	-[0x800005e4]:sw t5, 256(ra)
	-[0x800005e8]:sw t6, 264(ra)
Current Store : [0x800005e8] : sw t6, 264(ra) -- Store: [0x800117c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005dc]:fmul.d t5, t3, s10, dyn
	-[0x800005e0]:csrrs a6, fcsr, zero
	-[0x800005e4]:sw t5, 256(ra)
	-[0x800005e8]:sw t6, 264(ra)
	-[0x800005ec]:sw t5, 272(ra)
Current Store : [0x800005ec] : sw t5, 272(ra) -- Store: [0x800117c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000061c]:fmul.d t5, t3, s10, dyn
	-[0x80000620]:csrrs a6, fcsr, zero
	-[0x80000624]:sw t5, 288(ra)
	-[0x80000628]:sw t6, 296(ra)
Current Store : [0x80000628] : sw t6, 296(ra) -- Store: [0x800117e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000061c]:fmul.d t5, t3, s10, dyn
	-[0x80000620]:csrrs a6, fcsr, zero
	-[0x80000624]:sw t5, 288(ra)
	-[0x80000628]:sw t6, 296(ra)
	-[0x8000062c]:sw t5, 304(ra)
Current Store : [0x8000062c] : sw t5, 304(ra) -- Store: [0x800117e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000065c]:fmul.d t5, t3, s10, dyn
	-[0x80000660]:csrrs a6, fcsr, zero
	-[0x80000664]:sw t5, 320(ra)
	-[0x80000668]:sw t6, 328(ra)
Current Store : [0x80000668] : sw t6, 328(ra) -- Store: [0x80011800]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000065c]:fmul.d t5, t3, s10, dyn
	-[0x80000660]:csrrs a6, fcsr, zero
	-[0x80000664]:sw t5, 320(ra)
	-[0x80000668]:sw t6, 328(ra)
	-[0x8000066c]:sw t5, 336(ra)
Current Store : [0x8000066c] : sw t5, 336(ra) -- Store: [0x80011808]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000069c]:fmul.d t5, t3, s10, dyn
	-[0x800006a0]:csrrs a6, fcsr, zero
	-[0x800006a4]:sw t5, 352(ra)
	-[0x800006a8]:sw t6, 360(ra)
Current Store : [0x800006a8] : sw t6, 360(ra) -- Store: [0x80011820]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000069c]:fmul.d t5, t3, s10, dyn
	-[0x800006a0]:csrrs a6, fcsr, zero
	-[0x800006a4]:sw t5, 352(ra)
	-[0x800006a8]:sw t6, 360(ra)
	-[0x800006ac]:sw t5, 368(ra)
Current Store : [0x800006ac] : sw t5, 368(ra) -- Store: [0x80011828]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006dc]:fmul.d t5, t3, s10, dyn
	-[0x800006e0]:csrrs a6, fcsr, zero
	-[0x800006e4]:sw t5, 384(ra)
	-[0x800006e8]:sw t6, 392(ra)
Current Store : [0x800006e8] : sw t6, 392(ra) -- Store: [0x80011840]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006dc]:fmul.d t5, t3, s10, dyn
	-[0x800006e0]:csrrs a6, fcsr, zero
	-[0x800006e4]:sw t5, 384(ra)
	-[0x800006e8]:sw t6, 392(ra)
	-[0x800006ec]:sw t5, 400(ra)
Current Store : [0x800006ec] : sw t5, 400(ra) -- Store: [0x80011848]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000071c]:fmul.d t5, t3, s10, dyn
	-[0x80000720]:csrrs a6, fcsr, zero
	-[0x80000724]:sw t5, 416(ra)
	-[0x80000728]:sw t6, 424(ra)
Current Store : [0x80000728] : sw t6, 424(ra) -- Store: [0x80011860]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000071c]:fmul.d t5, t3, s10, dyn
	-[0x80000720]:csrrs a6, fcsr, zero
	-[0x80000724]:sw t5, 416(ra)
	-[0x80000728]:sw t6, 424(ra)
	-[0x8000072c]:sw t5, 432(ra)
Current Store : [0x8000072c] : sw t5, 432(ra) -- Store: [0x80011868]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000075c]:fmul.d t5, t3, s10, dyn
	-[0x80000760]:csrrs a6, fcsr, zero
	-[0x80000764]:sw t5, 448(ra)
	-[0x80000768]:sw t6, 456(ra)
Current Store : [0x80000768] : sw t6, 456(ra) -- Store: [0x80011880]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000075c]:fmul.d t5, t3, s10, dyn
	-[0x80000760]:csrrs a6, fcsr, zero
	-[0x80000764]:sw t5, 448(ra)
	-[0x80000768]:sw t6, 456(ra)
	-[0x8000076c]:sw t5, 464(ra)
Current Store : [0x8000076c] : sw t5, 464(ra) -- Store: [0x80011888]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000079c]:fmul.d t5, t3, s10, dyn
	-[0x800007a0]:csrrs a6, fcsr, zero
	-[0x800007a4]:sw t5, 480(ra)
	-[0x800007a8]:sw t6, 488(ra)
Current Store : [0x800007a8] : sw t6, 488(ra) -- Store: [0x800118a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000079c]:fmul.d t5, t3, s10, dyn
	-[0x800007a0]:csrrs a6, fcsr, zero
	-[0x800007a4]:sw t5, 480(ra)
	-[0x800007a8]:sw t6, 488(ra)
	-[0x800007ac]:sw t5, 496(ra)
Current Store : [0x800007ac] : sw t5, 496(ra) -- Store: [0x800118a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007dc]:fmul.d t5, t3, s10, dyn
	-[0x800007e0]:csrrs a6, fcsr, zero
	-[0x800007e4]:sw t5, 512(ra)
	-[0x800007e8]:sw t6, 520(ra)
Current Store : [0x800007e8] : sw t6, 520(ra) -- Store: [0x800118c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007dc]:fmul.d t5, t3, s10, dyn
	-[0x800007e0]:csrrs a6, fcsr, zero
	-[0x800007e4]:sw t5, 512(ra)
	-[0x800007e8]:sw t6, 520(ra)
	-[0x800007ec]:sw t5, 528(ra)
Current Store : [0x800007ec] : sw t5, 528(ra) -- Store: [0x800118c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000081c]:fmul.d t5, t3, s10, dyn
	-[0x80000820]:csrrs a6, fcsr, zero
	-[0x80000824]:sw t5, 544(ra)
	-[0x80000828]:sw t6, 552(ra)
Current Store : [0x80000828] : sw t6, 552(ra) -- Store: [0x800118e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000081c]:fmul.d t5, t3, s10, dyn
	-[0x80000820]:csrrs a6, fcsr, zero
	-[0x80000824]:sw t5, 544(ra)
	-[0x80000828]:sw t6, 552(ra)
	-[0x8000082c]:sw t5, 560(ra)
Current Store : [0x8000082c] : sw t5, 560(ra) -- Store: [0x800118e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000085c]:fmul.d t5, t3, s10, dyn
	-[0x80000860]:csrrs a6, fcsr, zero
	-[0x80000864]:sw t5, 576(ra)
	-[0x80000868]:sw t6, 584(ra)
Current Store : [0x80000868] : sw t6, 584(ra) -- Store: [0x80011900]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000085c]:fmul.d t5, t3, s10, dyn
	-[0x80000860]:csrrs a6, fcsr, zero
	-[0x80000864]:sw t5, 576(ra)
	-[0x80000868]:sw t6, 584(ra)
	-[0x8000086c]:sw t5, 592(ra)
Current Store : [0x8000086c] : sw t5, 592(ra) -- Store: [0x80011908]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000089c]:fmul.d t5, t3, s10, dyn
	-[0x800008a0]:csrrs a6, fcsr, zero
	-[0x800008a4]:sw t5, 608(ra)
	-[0x800008a8]:sw t6, 616(ra)
Current Store : [0x800008a8] : sw t6, 616(ra) -- Store: [0x80011920]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000089c]:fmul.d t5, t3, s10, dyn
	-[0x800008a0]:csrrs a6, fcsr, zero
	-[0x800008a4]:sw t5, 608(ra)
	-[0x800008a8]:sw t6, 616(ra)
	-[0x800008ac]:sw t5, 624(ra)
Current Store : [0x800008ac] : sw t5, 624(ra) -- Store: [0x80011928]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008e0]:fmul.d t5, t3, s10, dyn
	-[0x800008e4]:csrrs a6, fcsr, zero
	-[0x800008e8]:sw t5, 640(ra)
	-[0x800008ec]:sw t6, 648(ra)
Current Store : [0x800008ec] : sw t6, 648(ra) -- Store: [0x80011940]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008e0]:fmul.d t5, t3, s10, dyn
	-[0x800008e4]:csrrs a6, fcsr, zero
	-[0x800008e8]:sw t5, 640(ra)
	-[0x800008ec]:sw t6, 648(ra)
	-[0x800008f0]:sw t5, 656(ra)
Current Store : [0x800008f0] : sw t5, 656(ra) -- Store: [0x80011948]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000924]:fmul.d t5, t3, s10, dyn
	-[0x80000928]:csrrs a6, fcsr, zero
	-[0x8000092c]:sw t5, 672(ra)
	-[0x80000930]:sw t6, 680(ra)
Current Store : [0x80000930] : sw t6, 680(ra) -- Store: [0x80011960]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000924]:fmul.d t5, t3, s10, dyn
	-[0x80000928]:csrrs a6, fcsr, zero
	-[0x8000092c]:sw t5, 672(ra)
	-[0x80000930]:sw t6, 680(ra)
	-[0x80000934]:sw t5, 688(ra)
Current Store : [0x80000934] : sw t5, 688(ra) -- Store: [0x80011968]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000964]:fmul.d t5, t3, s10, dyn
	-[0x80000968]:csrrs a6, fcsr, zero
	-[0x8000096c]:sw t5, 704(ra)
	-[0x80000970]:sw t6, 712(ra)
Current Store : [0x80000970] : sw t6, 712(ra) -- Store: [0x80011980]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000964]:fmul.d t5, t3, s10, dyn
	-[0x80000968]:csrrs a6, fcsr, zero
	-[0x8000096c]:sw t5, 704(ra)
	-[0x80000970]:sw t6, 712(ra)
	-[0x80000974]:sw t5, 720(ra)
Current Store : [0x80000974] : sw t5, 720(ra) -- Store: [0x80011988]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009a4]:fmul.d t5, t3, s10, dyn
	-[0x800009a8]:csrrs a6, fcsr, zero
	-[0x800009ac]:sw t5, 736(ra)
	-[0x800009b0]:sw t6, 744(ra)
Current Store : [0x800009b0] : sw t6, 744(ra) -- Store: [0x800119a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009a4]:fmul.d t5, t3, s10, dyn
	-[0x800009a8]:csrrs a6, fcsr, zero
	-[0x800009ac]:sw t5, 736(ra)
	-[0x800009b0]:sw t6, 744(ra)
	-[0x800009b4]:sw t5, 752(ra)
Current Store : [0x800009b4] : sw t5, 752(ra) -- Store: [0x800119a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009e4]:fmul.d t5, t3, s10, dyn
	-[0x800009e8]:csrrs a6, fcsr, zero
	-[0x800009ec]:sw t5, 768(ra)
	-[0x800009f0]:sw t6, 776(ra)
Current Store : [0x800009f0] : sw t6, 776(ra) -- Store: [0x800119c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009e4]:fmul.d t5, t3, s10, dyn
	-[0x800009e8]:csrrs a6, fcsr, zero
	-[0x800009ec]:sw t5, 768(ra)
	-[0x800009f0]:sw t6, 776(ra)
	-[0x800009f4]:sw t5, 784(ra)
Current Store : [0x800009f4] : sw t5, 784(ra) -- Store: [0x800119c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a24]:fmul.d t5, t3, s10, dyn
	-[0x80000a28]:csrrs a6, fcsr, zero
	-[0x80000a2c]:sw t5, 800(ra)
	-[0x80000a30]:sw t6, 808(ra)
Current Store : [0x80000a30] : sw t6, 808(ra) -- Store: [0x800119e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a24]:fmul.d t5, t3, s10, dyn
	-[0x80000a28]:csrrs a6, fcsr, zero
	-[0x80000a2c]:sw t5, 800(ra)
	-[0x80000a30]:sw t6, 808(ra)
	-[0x80000a34]:sw t5, 816(ra)
Current Store : [0x80000a34] : sw t5, 816(ra) -- Store: [0x800119e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a68]:fmul.d t5, t3, s10, dyn
	-[0x80000a6c]:csrrs a6, fcsr, zero
	-[0x80000a70]:sw t5, 832(ra)
	-[0x80000a74]:sw t6, 840(ra)
Current Store : [0x80000a74] : sw t6, 840(ra) -- Store: [0x80011a00]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a68]:fmul.d t5, t3, s10, dyn
	-[0x80000a6c]:csrrs a6, fcsr, zero
	-[0x80000a70]:sw t5, 832(ra)
	-[0x80000a74]:sw t6, 840(ra)
	-[0x80000a78]:sw t5, 848(ra)
Current Store : [0x80000a78] : sw t5, 848(ra) -- Store: [0x80011a08]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000aac]:fmul.d t5, t3, s10, dyn
	-[0x80000ab0]:csrrs a6, fcsr, zero
	-[0x80000ab4]:sw t5, 864(ra)
	-[0x80000ab8]:sw t6, 872(ra)
Current Store : [0x80000ab8] : sw t6, 872(ra) -- Store: [0x80011a20]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000aac]:fmul.d t5, t3, s10, dyn
	-[0x80000ab0]:csrrs a6, fcsr, zero
	-[0x80000ab4]:sw t5, 864(ra)
	-[0x80000ab8]:sw t6, 872(ra)
	-[0x80000abc]:sw t5, 880(ra)
Current Store : [0x80000abc] : sw t5, 880(ra) -- Store: [0x80011a28]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000aec]:fmul.d t5, t3, s10, dyn
	-[0x80000af0]:csrrs a6, fcsr, zero
	-[0x80000af4]:sw t5, 896(ra)
	-[0x80000af8]:sw t6, 904(ra)
Current Store : [0x80000af8] : sw t6, 904(ra) -- Store: [0x80011a40]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000aec]:fmul.d t5, t3, s10, dyn
	-[0x80000af0]:csrrs a6, fcsr, zero
	-[0x80000af4]:sw t5, 896(ra)
	-[0x80000af8]:sw t6, 904(ra)
	-[0x80000afc]:sw t5, 912(ra)
Current Store : [0x80000afc] : sw t5, 912(ra) -- Store: [0x80011a48]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b2c]:fmul.d t5, t3, s10, dyn
	-[0x80000b30]:csrrs a6, fcsr, zero
	-[0x80000b34]:sw t5, 928(ra)
	-[0x80000b38]:sw t6, 936(ra)
Current Store : [0x80000b38] : sw t6, 936(ra) -- Store: [0x80011a60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b2c]:fmul.d t5, t3, s10, dyn
	-[0x80000b30]:csrrs a6, fcsr, zero
	-[0x80000b34]:sw t5, 928(ra)
	-[0x80000b38]:sw t6, 936(ra)
	-[0x80000b3c]:sw t5, 944(ra)
Current Store : [0x80000b3c] : sw t5, 944(ra) -- Store: [0x80011a68]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b6c]:fmul.d t5, t3, s10, dyn
	-[0x80000b70]:csrrs a6, fcsr, zero
	-[0x80000b74]:sw t5, 960(ra)
	-[0x80000b78]:sw t6, 968(ra)
Current Store : [0x80000b78] : sw t6, 968(ra) -- Store: [0x80011a80]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b6c]:fmul.d t5, t3, s10, dyn
	-[0x80000b70]:csrrs a6, fcsr, zero
	-[0x80000b74]:sw t5, 960(ra)
	-[0x80000b78]:sw t6, 968(ra)
	-[0x80000b7c]:sw t5, 976(ra)
Current Store : [0x80000b7c] : sw t5, 976(ra) -- Store: [0x80011a88]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bac]:fmul.d t5, t3, s10, dyn
	-[0x80000bb0]:csrrs a6, fcsr, zero
	-[0x80000bb4]:sw t5, 992(ra)
	-[0x80000bb8]:sw t6, 1000(ra)
Current Store : [0x80000bb8] : sw t6, 1000(ra) -- Store: [0x80011aa0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bac]:fmul.d t5, t3, s10, dyn
	-[0x80000bb0]:csrrs a6, fcsr, zero
	-[0x80000bb4]:sw t5, 992(ra)
	-[0x80000bb8]:sw t6, 1000(ra)
	-[0x80000bbc]:sw t5, 1008(ra)
Current Store : [0x80000bbc] : sw t5, 1008(ra) -- Store: [0x80011aa8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bec]:fmul.d t5, t3, s10, dyn
	-[0x80000bf0]:csrrs a6, fcsr, zero
	-[0x80000bf4]:sw t5, 1024(ra)
	-[0x80000bf8]:sw t6, 1032(ra)
Current Store : [0x80000bf8] : sw t6, 1032(ra) -- Store: [0x80011ac0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bec]:fmul.d t5, t3, s10, dyn
	-[0x80000bf0]:csrrs a6, fcsr, zero
	-[0x80000bf4]:sw t5, 1024(ra)
	-[0x80000bf8]:sw t6, 1032(ra)
	-[0x80000bfc]:sw t5, 1040(ra)
Current Store : [0x80000bfc] : sw t5, 1040(ra) -- Store: [0x80011ac8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c2c]:fmul.d t5, t3, s10, dyn
	-[0x80000c30]:csrrs a6, fcsr, zero
	-[0x80000c34]:sw t5, 1056(ra)
	-[0x80000c38]:sw t6, 1064(ra)
Current Store : [0x80000c38] : sw t6, 1064(ra) -- Store: [0x80011ae0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c2c]:fmul.d t5, t3, s10, dyn
	-[0x80000c30]:csrrs a6, fcsr, zero
	-[0x80000c34]:sw t5, 1056(ra)
	-[0x80000c38]:sw t6, 1064(ra)
	-[0x80000c3c]:sw t5, 1072(ra)
Current Store : [0x80000c3c] : sw t5, 1072(ra) -- Store: [0x80011ae8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c6c]:fmul.d t5, t3, s10, dyn
	-[0x80000c70]:csrrs a6, fcsr, zero
	-[0x80000c74]:sw t5, 1088(ra)
	-[0x80000c78]:sw t6, 1096(ra)
Current Store : [0x80000c78] : sw t6, 1096(ra) -- Store: [0x80011b00]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c6c]:fmul.d t5, t3, s10, dyn
	-[0x80000c70]:csrrs a6, fcsr, zero
	-[0x80000c74]:sw t5, 1088(ra)
	-[0x80000c78]:sw t6, 1096(ra)
	-[0x80000c7c]:sw t5, 1104(ra)
Current Store : [0x80000c7c] : sw t5, 1104(ra) -- Store: [0x80011b08]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cac]:fmul.d t5, t3, s10, dyn
	-[0x80000cb0]:csrrs a6, fcsr, zero
	-[0x80000cb4]:sw t5, 1120(ra)
	-[0x80000cb8]:sw t6, 1128(ra)
Current Store : [0x80000cb8] : sw t6, 1128(ra) -- Store: [0x80011b20]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cac]:fmul.d t5, t3, s10, dyn
	-[0x80000cb0]:csrrs a6, fcsr, zero
	-[0x80000cb4]:sw t5, 1120(ra)
	-[0x80000cb8]:sw t6, 1128(ra)
	-[0x80000cbc]:sw t5, 1136(ra)
Current Store : [0x80000cbc] : sw t5, 1136(ra) -- Store: [0x80011b28]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cec]:fmul.d t5, t3, s10, dyn
	-[0x80000cf0]:csrrs a6, fcsr, zero
	-[0x80000cf4]:sw t5, 1152(ra)
	-[0x80000cf8]:sw t6, 1160(ra)
Current Store : [0x80000cf8] : sw t6, 1160(ra) -- Store: [0x80011b40]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cec]:fmul.d t5, t3, s10, dyn
	-[0x80000cf0]:csrrs a6, fcsr, zero
	-[0x80000cf4]:sw t5, 1152(ra)
	-[0x80000cf8]:sw t6, 1160(ra)
	-[0x80000cfc]:sw t5, 1168(ra)
Current Store : [0x80000cfc] : sw t5, 1168(ra) -- Store: [0x80011b48]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d2c]:fmul.d t5, t3, s10, dyn
	-[0x80000d30]:csrrs a6, fcsr, zero
	-[0x80000d34]:sw t5, 1184(ra)
	-[0x80000d38]:sw t6, 1192(ra)
Current Store : [0x80000d38] : sw t6, 1192(ra) -- Store: [0x80011b60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d2c]:fmul.d t5, t3, s10, dyn
	-[0x80000d30]:csrrs a6, fcsr, zero
	-[0x80000d34]:sw t5, 1184(ra)
	-[0x80000d38]:sw t6, 1192(ra)
	-[0x80000d3c]:sw t5, 1200(ra)
Current Store : [0x80000d3c] : sw t5, 1200(ra) -- Store: [0x80011b68]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d6c]:fmul.d t5, t3, s10, dyn
	-[0x80000d70]:csrrs a6, fcsr, zero
	-[0x80000d74]:sw t5, 1216(ra)
	-[0x80000d78]:sw t6, 1224(ra)
Current Store : [0x80000d78] : sw t6, 1224(ra) -- Store: [0x80011b80]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d6c]:fmul.d t5, t3, s10, dyn
	-[0x80000d70]:csrrs a6, fcsr, zero
	-[0x80000d74]:sw t5, 1216(ra)
	-[0x80000d78]:sw t6, 1224(ra)
	-[0x80000d7c]:sw t5, 1232(ra)
Current Store : [0x80000d7c] : sw t5, 1232(ra) -- Store: [0x80011b88]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000dac]:fmul.d t5, t3, s10, dyn
	-[0x80000db0]:csrrs a6, fcsr, zero
	-[0x80000db4]:sw t5, 1248(ra)
	-[0x80000db8]:sw t6, 1256(ra)
Current Store : [0x80000db8] : sw t6, 1256(ra) -- Store: [0x80011ba0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000dac]:fmul.d t5, t3, s10, dyn
	-[0x80000db0]:csrrs a6, fcsr, zero
	-[0x80000db4]:sw t5, 1248(ra)
	-[0x80000db8]:sw t6, 1256(ra)
	-[0x80000dbc]:sw t5, 1264(ra)
Current Store : [0x80000dbc] : sw t5, 1264(ra) -- Store: [0x80011ba8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000dec]:fmul.d t5, t3, s10, dyn
	-[0x80000df0]:csrrs a6, fcsr, zero
	-[0x80000df4]:sw t5, 1280(ra)
	-[0x80000df8]:sw t6, 1288(ra)
Current Store : [0x80000df8] : sw t6, 1288(ra) -- Store: [0x80011bc0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000dec]:fmul.d t5, t3, s10, dyn
	-[0x80000df0]:csrrs a6, fcsr, zero
	-[0x80000df4]:sw t5, 1280(ra)
	-[0x80000df8]:sw t6, 1288(ra)
	-[0x80000dfc]:sw t5, 1296(ra)
Current Store : [0x80000dfc] : sw t5, 1296(ra) -- Store: [0x80011bc8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e2c]:fmul.d t5, t3, s10, dyn
	-[0x80000e30]:csrrs a6, fcsr, zero
	-[0x80000e34]:sw t5, 1312(ra)
	-[0x80000e38]:sw t6, 1320(ra)
Current Store : [0x80000e38] : sw t6, 1320(ra) -- Store: [0x80011be0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e2c]:fmul.d t5, t3, s10, dyn
	-[0x80000e30]:csrrs a6, fcsr, zero
	-[0x80000e34]:sw t5, 1312(ra)
	-[0x80000e38]:sw t6, 1320(ra)
	-[0x80000e3c]:sw t5, 1328(ra)
Current Store : [0x80000e3c] : sw t5, 1328(ra) -- Store: [0x80011be8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e6c]:fmul.d t5, t3, s10, dyn
	-[0x80000e70]:csrrs a6, fcsr, zero
	-[0x80000e74]:sw t5, 1344(ra)
	-[0x80000e78]:sw t6, 1352(ra)
Current Store : [0x80000e78] : sw t6, 1352(ra) -- Store: [0x80011c00]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e6c]:fmul.d t5, t3, s10, dyn
	-[0x80000e70]:csrrs a6, fcsr, zero
	-[0x80000e74]:sw t5, 1344(ra)
	-[0x80000e78]:sw t6, 1352(ra)
	-[0x80000e7c]:sw t5, 1360(ra)
Current Store : [0x80000e7c] : sw t5, 1360(ra) -- Store: [0x80011c08]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000eac]:fmul.d t5, t3, s10, dyn
	-[0x80000eb0]:csrrs a6, fcsr, zero
	-[0x80000eb4]:sw t5, 1376(ra)
	-[0x80000eb8]:sw t6, 1384(ra)
Current Store : [0x80000eb8] : sw t6, 1384(ra) -- Store: [0x80011c20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000eac]:fmul.d t5, t3, s10, dyn
	-[0x80000eb0]:csrrs a6, fcsr, zero
	-[0x80000eb4]:sw t5, 1376(ra)
	-[0x80000eb8]:sw t6, 1384(ra)
	-[0x80000ebc]:sw t5, 1392(ra)
Current Store : [0x80000ebc] : sw t5, 1392(ra) -- Store: [0x80011c28]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ef0]:fmul.d t5, t3, s10, dyn
	-[0x80000ef4]:csrrs a6, fcsr, zero
	-[0x80000ef8]:sw t5, 1408(ra)
	-[0x80000efc]:sw t6, 1416(ra)
Current Store : [0x80000efc] : sw t6, 1416(ra) -- Store: [0x80011c40]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ef0]:fmul.d t5, t3, s10, dyn
	-[0x80000ef4]:csrrs a6, fcsr, zero
	-[0x80000ef8]:sw t5, 1408(ra)
	-[0x80000efc]:sw t6, 1416(ra)
	-[0x80000f00]:sw t5, 1424(ra)
Current Store : [0x80000f00] : sw t5, 1424(ra) -- Store: [0x80011c48]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f34]:fmul.d t5, t3, s10, dyn
	-[0x80000f38]:csrrs a6, fcsr, zero
	-[0x80000f3c]:sw t5, 1440(ra)
	-[0x80000f40]:sw t6, 1448(ra)
Current Store : [0x80000f40] : sw t6, 1448(ra) -- Store: [0x80011c60]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f34]:fmul.d t5, t3, s10, dyn
	-[0x80000f38]:csrrs a6, fcsr, zero
	-[0x80000f3c]:sw t5, 1440(ra)
	-[0x80000f40]:sw t6, 1448(ra)
	-[0x80000f44]:sw t5, 1456(ra)
Current Store : [0x80000f44] : sw t5, 1456(ra) -- Store: [0x80011c68]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f74]:fmul.d t5, t3, s10, dyn
	-[0x80000f78]:csrrs a6, fcsr, zero
	-[0x80000f7c]:sw t5, 1472(ra)
	-[0x80000f80]:sw t6, 1480(ra)
Current Store : [0x80000f80] : sw t6, 1480(ra) -- Store: [0x80011c80]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f74]:fmul.d t5, t3, s10, dyn
	-[0x80000f78]:csrrs a6, fcsr, zero
	-[0x80000f7c]:sw t5, 1472(ra)
	-[0x80000f80]:sw t6, 1480(ra)
	-[0x80000f84]:sw t5, 1488(ra)
Current Store : [0x80000f84] : sw t5, 1488(ra) -- Store: [0x80011c88]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fb4]:fmul.d t5, t3, s10, dyn
	-[0x80000fb8]:csrrs a6, fcsr, zero
	-[0x80000fbc]:sw t5, 1504(ra)
	-[0x80000fc0]:sw t6, 1512(ra)
Current Store : [0x80000fc0] : sw t6, 1512(ra) -- Store: [0x80011ca0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fb4]:fmul.d t5, t3, s10, dyn
	-[0x80000fb8]:csrrs a6, fcsr, zero
	-[0x80000fbc]:sw t5, 1504(ra)
	-[0x80000fc0]:sw t6, 1512(ra)
	-[0x80000fc4]:sw t5, 1520(ra)
Current Store : [0x80000fc4] : sw t5, 1520(ra) -- Store: [0x80011ca8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ff4]:fmul.d t5, t3, s10, dyn
	-[0x80000ff8]:csrrs a6, fcsr, zero
	-[0x80000ffc]:sw t5, 1536(ra)
	-[0x80001000]:sw t6, 1544(ra)
Current Store : [0x80001000] : sw t6, 1544(ra) -- Store: [0x80011cc0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ff4]:fmul.d t5, t3, s10, dyn
	-[0x80000ff8]:csrrs a6, fcsr, zero
	-[0x80000ffc]:sw t5, 1536(ra)
	-[0x80001000]:sw t6, 1544(ra)
	-[0x80001004]:sw t5, 1552(ra)
Current Store : [0x80001004] : sw t5, 1552(ra) -- Store: [0x80011cc8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001034]:fmul.d t5, t3, s10, dyn
	-[0x80001038]:csrrs a6, fcsr, zero
	-[0x8000103c]:sw t5, 1568(ra)
	-[0x80001040]:sw t6, 1576(ra)
Current Store : [0x80001040] : sw t6, 1576(ra) -- Store: [0x80011ce0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001034]:fmul.d t5, t3, s10, dyn
	-[0x80001038]:csrrs a6, fcsr, zero
	-[0x8000103c]:sw t5, 1568(ra)
	-[0x80001040]:sw t6, 1576(ra)
	-[0x80001044]:sw t5, 1584(ra)
Current Store : [0x80001044] : sw t5, 1584(ra) -- Store: [0x80011ce8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001078]:fmul.d t5, t3, s10, dyn
	-[0x8000107c]:csrrs a6, fcsr, zero
	-[0x80001080]:sw t5, 1600(ra)
	-[0x80001084]:sw t6, 1608(ra)
Current Store : [0x80001084] : sw t6, 1608(ra) -- Store: [0x80011d00]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001078]:fmul.d t5, t3, s10, dyn
	-[0x8000107c]:csrrs a6, fcsr, zero
	-[0x80001080]:sw t5, 1600(ra)
	-[0x80001084]:sw t6, 1608(ra)
	-[0x80001088]:sw t5, 1616(ra)
Current Store : [0x80001088] : sw t5, 1616(ra) -- Store: [0x80011d08]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010bc]:fmul.d t5, t3, s10, dyn
	-[0x800010c0]:csrrs a6, fcsr, zero
	-[0x800010c4]:sw t5, 1632(ra)
	-[0x800010c8]:sw t6, 1640(ra)
Current Store : [0x800010c8] : sw t6, 1640(ra) -- Store: [0x80011d20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010bc]:fmul.d t5, t3, s10, dyn
	-[0x800010c0]:csrrs a6, fcsr, zero
	-[0x800010c4]:sw t5, 1632(ra)
	-[0x800010c8]:sw t6, 1640(ra)
	-[0x800010cc]:sw t5, 1648(ra)
Current Store : [0x800010cc] : sw t5, 1648(ra) -- Store: [0x80011d28]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010fc]:fmul.d t5, t3, s10, dyn
	-[0x80001100]:csrrs a6, fcsr, zero
	-[0x80001104]:sw t5, 1664(ra)
	-[0x80001108]:sw t6, 1672(ra)
Current Store : [0x80001108] : sw t6, 1672(ra) -- Store: [0x80011d40]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010fc]:fmul.d t5, t3, s10, dyn
	-[0x80001100]:csrrs a6, fcsr, zero
	-[0x80001104]:sw t5, 1664(ra)
	-[0x80001108]:sw t6, 1672(ra)
	-[0x8000110c]:sw t5, 1680(ra)
Current Store : [0x8000110c] : sw t5, 1680(ra) -- Store: [0x80011d48]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000113c]:fmul.d t5, t3, s10, dyn
	-[0x80001140]:csrrs a6, fcsr, zero
	-[0x80001144]:sw t5, 1696(ra)
	-[0x80001148]:sw t6, 1704(ra)
Current Store : [0x80001148] : sw t6, 1704(ra) -- Store: [0x80011d60]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000113c]:fmul.d t5, t3, s10, dyn
	-[0x80001140]:csrrs a6, fcsr, zero
	-[0x80001144]:sw t5, 1696(ra)
	-[0x80001148]:sw t6, 1704(ra)
	-[0x8000114c]:sw t5, 1712(ra)
Current Store : [0x8000114c] : sw t5, 1712(ra) -- Store: [0x80011d68]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000117c]:fmul.d t5, t3, s10, dyn
	-[0x80001180]:csrrs a6, fcsr, zero
	-[0x80001184]:sw t5, 1728(ra)
	-[0x80001188]:sw t6, 1736(ra)
Current Store : [0x80001188] : sw t6, 1736(ra) -- Store: [0x80011d80]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000117c]:fmul.d t5, t3, s10, dyn
	-[0x80001180]:csrrs a6, fcsr, zero
	-[0x80001184]:sw t5, 1728(ra)
	-[0x80001188]:sw t6, 1736(ra)
	-[0x8000118c]:sw t5, 1744(ra)
Current Store : [0x8000118c] : sw t5, 1744(ra) -- Store: [0x80011d88]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011bc]:fmul.d t5, t3, s10, dyn
	-[0x800011c0]:csrrs a6, fcsr, zero
	-[0x800011c4]:sw t5, 1760(ra)
	-[0x800011c8]:sw t6, 1768(ra)
Current Store : [0x800011c8] : sw t6, 1768(ra) -- Store: [0x80011da0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011bc]:fmul.d t5, t3, s10, dyn
	-[0x800011c0]:csrrs a6, fcsr, zero
	-[0x800011c4]:sw t5, 1760(ra)
	-[0x800011c8]:sw t6, 1768(ra)
	-[0x800011cc]:sw t5, 1776(ra)
Current Store : [0x800011cc] : sw t5, 1776(ra) -- Store: [0x80011da8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011fc]:fmul.d t5, t3, s10, dyn
	-[0x80001200]:csrrs a6, fcsr, zero
	-[0x80001204]:sw t5, 1792(ra)
	-[0x80001208]:sw t6, 1800(ra)
Current Store : [0x80001208] : sw t6, 1800(ra) -- Store: [0x80011dc0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011fc]:fmul.d t5, t3, s10, dyn
	-[0x80001200]:csrrs a6, fcsr, zero
	-[0x80001204]:sw t5, 1792(ra)
	-[0x80001208]:sw t6, 1800(ra)
	-[0x8000120c]:sw t5, 1808(ra)
Current Store : [0x8000120c] : sw t5, 1808(ra) -- Store: [0x80011dc8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000123c]:fmul.d t5, t3, s10, dyn
	-[0x80001240]:csrrs a6, fcsr, zero
	-[0x80001244]:sw t5, 1824(ra)
	-[0x80001248]:sw t6, 1832(ra)
Current Store : [0x80001248] : sw t6, 1832(ra) -- Store: [0x80011de0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000123c]:fmul.d t5, t3, s10, dyn
	-[0x80001240]:csrrs a6, fcsr, zero
	-[0x80001244]:sw t5, 1824(ra)
	-[0x80001248]:sw t6, 1832(ra)
	-[0x8000124c]:sw t5, 1840(ra)
Current Store : [0x8000124c] : sw t5, 1840(ra) -- Store: [0x80011de8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000127c]:fmul.d t5, t3, s10, dyn
	-[0x80001280]:csrrs a6, fcsr, zero
	-[0x80001284]:sw t5, 1856(ra)
	-[0x80001288]:sw t6, 1864(ra)
Current Store : [0x80001288] : sw t6, 1864(ra) -- Store: [0x80011e00]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000127c]:fmul.d t5, t3, s10, dyn
	-[0x80001280]:csrrs a6, fcsr, zero
	-[0x80001284]:sw t5, 1856(ra)
	-[0x80001288]:sw t6, 1864(ra)
	-[0x8000128c]:sw t5, 1872(ra)
Current Store : [0x8000128c] : sw t5, 1872(ra) -- Store: [0x80011e08]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012bc]:fmul.d t5, t3, s10, dyn
	-[0x800012c0]:csrrs a6, fcsr, zero
	-[0x800012c4]:sw t5, 1888(ra)
	-[0x800012c8]:sw t6, 1896(ra)
Current Store : [0x800012c8] : sw t6, 1896(ra) -- Store: [0x80011e20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012bc]:fmul.d t5, t3, s10, dyn
	-[0x800012c0]:csrrs a6, fcsr, zero
	-[0x800012c4]:sw t5, 1888(ra)
	-[0x800012c8]:sw t6, 1896(ra)
	-[0x800012cc]:sw t5, 1904(ra)
Current Store : [0x800012cc] : sw t5, 1904(ra) -- Store: [0x80011e28]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012fc]:fmul.d t5, t3, s10, dyn
	-[0x80001300]:csrrs a6, fcsr, zero
	-[0x80001304]:sw t5, 1920(ra)
	-[0x80001308]:sw t6, 1928(ra)
Current Store : [0x80001308] : sw t6, 1928(ra) -- Store: [0x80011e40]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012fc]:fmul.d t5, t3, s10, dyn
	-[0x80001300]:csrrs a6, fcsr, zero
	-[0x80001304]:sw t5, 1920(ra)
	-[0x80001308]:sw t6, 1928(ra)
	-[0x8000130c]:sw t5, 1936(ra)
Current Store : [0x8000130c] : sw t5, 1936(ra) -- Store: [0x80011e48]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000133c]:fmul.d t5, t3, s10, dyn
	-[0x80001340]:csrrs a6, fcsr, zero
	-[0x80001344]:sw t5, 1952(ra)
	-[0x80001348]:sw t6, 1960(ra)
Current Store : [0x80001348] : sw t6, 1960(ra) -- Store: [0x80011e60]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000133c]:fmul.d t5, t3, s10, dyn
	-[0x80001340]:csrrs a6, fcsr, zero
	-[0x80001344]:sw t5, 1952(ra)
	-[0x80001348]:sw t6, 1960(ra)
	-[0x8000134c]:sw t5, 1968(ra)
Current Store : [0x8000134c] : sw t5, 1968(ra) -- Store: [0x80011e68]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000137c]:fmul.d t5, t3, s10, dyn
	-[0x80001380]:csrrs a6, fcsr, zero
	-[0x80001384]:sw t5, 1984(ra)
	-[0x80001388]:sw t6, 1992(ra)
Current Store : [0x80001388] : sw t6, 1992(ra) -- Store: [0x80011e80]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000137c]:fmul.d t5, t3, s10, dyn
	-[0x80001380]:csrrs a6, fcsr, zero
	-[0x80001384]:sw t5, 1984(ra)
	-[0x80001388]:sw t6, 1992(ra)
	-[0x8000138c]:sw t5, 2000(ra)
Current Store : [0x8000138c] : sw t5, 2000(ra) -- Store: [0x80011e88]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013bc]:fmul.d t5, t3, s10, dyn
	-[0x800013c0]:csrrs a6, fcsr, zero
	-[0x800013c4]:sw t5, 2016(ra)
	-[0x800013c8]:sw t6, 2024(ra)
Current Store : [0x800013c8] : sw t6, 2024(ra) -- Store: [0x80011ea0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013bc]:fmul.d t5, t3, s10, dyn
	-[0x800013c0]:csrrs a6, fcsr, zero
	-[0x800013c4]:sw t5, 2016(ra)
	-[0x800013c8]:sw t6, 2024(ra)
	-[0x800013cc]:sw t5, 2032(ra)
Current Store : [0x800013cc] : sw t5, 2032(ra) -- Store: [0x80011ea8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013fc]:fmul.d t5, t3, s10, dyn
	-[0x80001400]:csrrs a6, fcsr, zero
	-[0x80001404]:addi ra, ra, 2040
	-[0x80001408]:sw t5, 8(ra)
	-[0x8000140c]:sw t6, 16(ra)
Current Store : [0x8000140c] : sw t6, 16(ra) -- Store: [0x80011ec0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013fc]:fmul.d t5, t3, s10, dyn
	-[0x80001400]:csrrs a6, fcsr, zero
	-[0x80001404]:addi ra, ra, 2040
	-[0x80001408]:sw t5, 8(ra)
	-[0x8000140c]:sw t6, 16(ra)
	-[0x80001410]:sw t5, 24(ra)
Current Store : [0x80001410] : sw t5, 24(ra) -- Store: [0x80011ec8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001440]:fmul.d t5, t3, s10, dyn
	-[0x80001444]:csrrs a6, fcsr, zero
	-[0x80001448]:sw t5, 40(ra)
	-[0x8000144c]:sw t6, 48(ra)
Current Store : [0x8000144c] : sw t6, 48(ra) -- Store: [0x80011ee0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001440]:fmul.d t5, t3, s10, dyn
	-[0x80001444]:csrrs a6, fcsr, zero
	-[0x80001448]:sw t5, 40(ra)
	-[0x8000144c]:sw t6, 48(ra)
	-[0x80001450]:sw t5, 56(ra)
Current Store : [0x80001450] : sw t5, 56(ra) -- Store: [0x80011ee8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001480]:fmul.d t5, t3, s10, dyn
	-[0x80001484]:csrrs a6, fcsr, zero
	-[0x80001488]:sw t5, 72(ra)
	-[0x8000148c]:sw t6, 80(ra)
Current Store : [0x8000148c] : sw t6, 80(ra) -- Store: [0x80011f00]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001480]:fmul.d t5, t3, s10, dyn
	-[0x80001484]:csrrs a6, fcsr, zero
	-[0x80001488]:sw t5, 72(ra)
	-[0x8000148c]:sw t6, 80(ra)
	-[0x80001490]:sw t5, 88(ra)
Current Store : [0x80001490] : sw t5, 88(ra) -- Store: [0x80011f08]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014c0]:fmul.d t5, t3, s10, dyn
	-[0x800014c4]:csrrs a6, fcsr, zero
	-[0x800014c8]:sw t5, 104(ra)
	-[0x800014cc]:sw t6, 112(ra)
Current Store : [0x800014cc] : sw t6, 112(ra) -- Store: [0x80011f20]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014c0]:fmul.d t5, t3, s10, dyn
	-[0x800014c4]:csrrs a6, fcsr, zero
	-[0x800014c8]:sw t5, 104(ra)
	-[0x800014cc]:sw t6, 112(ra)
	-[0x800014d0]:sw t5, 120(ra)
Current Store : [0x800014d0] : sw t5, 120(ra) -- Store: [0x80011f28]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001504]:fmul.d t5, t3, s10, dyn
	-[0x80001508]:csrrs a6, fcsr, zero
	-[0x8000150c]:sw t5, 136(ra)
	-[0x80001510]:sw t6, 144(ra)
Current Store : [0x80001510] : sw t6, 144(ra) -- Store: [0x80011f40]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001504]:fmul.d t5, t3, s10, dyn
	-[0x80001508]:csrrs a6, fcsr, zero
	-[0x8000150c]:sw t5, 136(ra)
	-[0x80001510]:sw t6, 144(ra)
	-[0x80001514]:sw t5, 152(ra)
Current Store : [0x80001514] : sw t5, 152(ra) -- Store: [0x80011f48]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001548]:fmul.d t5, t3, s10, dyn
	-[0x8000154c]:csrrs a6, fcsr, zero
	-[0x80001550]:sw t5, 168(ra)
	-[0x80001554]:sw t6, 176(ra)
Current Store : [0x80001554] : sw t6, 176(ra) -- Store: [0x80011f60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001548]:fmul.d t5, t3, s10, dyn
	-[0x8000154c]:csrrs a6, fcsr, zero
	-[0x80001550]:sw t5, 168(ra)
	-[0x80001554]:sw t6, 176(ra)
	-[0x80001558]:sw t5, 184(ra)
Current Store : [0x80001558] : sw t5, 184(ra) -- Store: [0x80011f68]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001588]:fmul.d t5, t3, s10, dyn
	-[0x8000158c]:csrrs a6, fcsr, zero
	-[0x80001590]:sw t5, 200(ra)
	-[0x80001594]:sw t6, 208(ra)
Current Store : [0x80001594] : sw t6, 208(ra) -- Store: [0x80011f80]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001588]:fmul.d t5, t3, s10, dyn
	-[0x8000158c]:csrrs a6, fcsr, zero
	-[0x80001590]:sw t5, 200(ra)
	-[0x80001594]:sw t6, 208(ra)
	-[0x80001598]:sw t5, 216(ra)
Current Store : [0x80001598] : sw t5, 216(ra) -- Store: [0x80011f88]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015c8]:fmul.d t5, t3, s10, dyn
	-[0x800015cc]:csrrs a6, fcsr, zero
	-[0x800015d0]:sw t5, 232(ra)
	-[0x800015d4]:sw t6, 240(ra)
Current Store : [0x800015d4] : sw t6, 240(ra) -- Store: [0x80011fa0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015c8]:fmul.d t5, t3, s10, dyn
	-[0x800015cc]:csrrs a6, fcsr, zero
	-[0x800015d0]:sw t5, 232(ra)
	-[0x800015d4]:sw t6, 240(ra)
	-[0x800015d8]:sw t5, 248(ra)
Current Store : [0x800015d8] : sw t5, 248(ra) -- Store: [0x80011fa8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001608]:fmul.d t5, t3, s10, dyn
	-[0x8000160c]:csrrs a6, fcsr, zero
	-[0x80001610]:sw t5, 264(ra)
	-[0x80001614]:sw t6, 272(ra)
Current Store : [0x80001614] : sw t6, 272(ra) -- Store: [0x80011fc0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001608]:fmul.d t5, t3, s10, dyn
	-[0x8000160c]:csrrs a6, fcsr, zero
	-[0x80001610]:sw t5, 264(ra)
	-[0x80001614]:sw t6, 272(ra)
	-[0x80001618]:sw t5, 280(ra)
Current Store : [0x80001618] : sw t5, 280(ra) -- Store: [0x80011fc8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001648]:fmul.d t5, t3, s10, dyn
	-[0x8000164c]:csrrs a6, fcsr, zero
	-[0x80001650]:sw t5, 296(ra)
	-[0x80001654]:sw t6, 304(ra)
Current Store : [0x80001654] : sw t6, 304(ra) -- Store: [0x80011fe0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001648]:fmul.d t5, t3, s10, dyn
	-[0x8000164c]:csrrs a6, fcsr, zero
	-[0x80001650]:sw t5, 296(ra)
	-[0x80001654]:sw t6, 304(ra)
	-[0x80001658]:sw t5, 312(ra)
Current Store : [0x80001658] : sw t5, 312(ra) -- Store: [0x80011fe8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000168c]:fmul.d t5, t3, s10, dyn
	-[0x80001690]:csrrs a6, fcsr, zero
	-[0x80001694]:sw t5, 328(ra)
	-[0x80001698]:sw t6, 336(ra)
Current Store : [0x80001698] : sw t6, 336(ra) -- Store: [0x80012000]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000168c]:fmul.d t5, t3, s10, dyn
	-[0x80001690]:csrrs a6, fcsr, zero
	-[0x80001694]:sw t5, 328(ra)
	-[0x80001698]:sw t6, 336(ra)
	-[0x8000169c]:sw t5, 344(ra)
Current Store : [0x8000169c] : sw t5, 344(ra) -- Store: [0x80012008]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016d0]:fmul.d t5, t3, s10, dyn
	-[0x800016d4]:csrrs a6, fcsr, zero
	-[0x800016d8]:sw t5, 360(ra)
	-[0x800016dc]:sw t6, 368(ra)
Current Store : [0x800016dc] : sw t6, 368(ra) -- Store: [0x80012020]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016d0]:fmul.d t5, t3, s10, dyn
	-[0x800016d4]:csrrs a6, fcsr, zero
	-[0x800016d8]:sw t5, 360(ra)
	-[0x800016dc]:sw t6, 368(ra)
	-[0x800016e0]:sw t5, 376(ra)
Current Store : [0x800016e0] : sw t5, 376(ra) -- Store: [0x80012028]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001710]:fmul.d t5, t3, s10, dyn
	-[0x80001714]:csrrs a6, fcsr, zero
	-[0x80001718]:sw t5, 392(ra)
	-[0x8000171c]:sw t6, 400(ra)
Current Store : [0x8000171c] : sw t6, 400(ra) -- Store: [0x80012040]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001710]:fmul.d t5, t3, s10, dyn
	-[0x80001714]:csrrs a6, fcsr, zero
	-[0x80001718]:sw t5, 392(ra)
	-[0x8000171c]:sw t6, 400(ra)
	-[0x80001720]:sw t5, 408(ra)
Current Store : [0x80001720] : sw t5, 408(ra) -- Store: [0x80012048]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001750]:fmul.d t5, t3, s10, dyn
	-[0x80001754]:csrrs a6, fcsr, zero
	-[0x80001758]:sw t5, 424(ra)
	-[0x8000175c]:sw t6, 432(ra)
Current Store : [0x8000175c] : sw t6, 432(ra) -- Store: [0x80012060]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001750]:fmul.d t5, t3, s10, dyn
	-[0x80001754]:csrrs a6, fcsr, zero
	-[0x80001758]:sw t5, 424(ra)
	-[0x8000175c]:sw t6, 432(ra)
	-[0x80001760]:sw t5, 440(ra)
Current Store : [0x80001760] : sw t5, 440(ra) -- Store: [0x80012068]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001790]:fmul.d t5, t3, s10, dyn
	-[0x80001794]:csrrs a6, fcsr, zero
	-[0x80001798]:sw t5, 456(ra)
	-[0x8000179c]:sw t6, 464(ra)
Current Store : [0x8000179c] : sw t6, 464(ra) -- Store: [0x80012080]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001790]:fmul.d t5, t3, s10, dyn
	-[0x80001794]:csrrs a6, fcsr, zero
	-[0x80001798]:sw t5, 456(ra)
	-[0x8000179c]:sw t6, 464(ra)
	-[0x800017a0]:sw t5, 472(ra)
Current Store : [0x800017a0] : sw t5, 472(ra) -- Store: [0x80012088]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017d0]:fmul.d t5, t3, s10, dyn
	-[0x800017d4]:csrrs a6, fcsr, zero
	-[0x800017d8]:sw t5, 488(ra)
	-[0x800017dc]:sw t6, 496(ra)
Current Store : [0x800017dc] : sw t6, 496(ra) -- Store: [0x800120a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017d0]:fmul.d t5, t3, s10, dyn
	-[0x800017d4]:csrrs a6, fcsr, zero
	-[0x800017d8]:sw t5, 488(ra)
	-[0x800017dc]:sw t6, 496(ra)
	-[0x800017e0]:sw t5, 504(ra)
Current Store : [0x800017e0] : sw t5, 504(ra) -- Store: [0x800120a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001810]:fmul.d t5, t3, s10, dyn
	-[0x80001814]:csrrs a6, fcsr, zero
	-[0x80001818]:sw t5, 520(ra)
	-[0x8000181c]:sw t6, 528(ra)
Current Store : [0x8000181c] : sw t6, 528(ra) -- Store: [0x800120c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001810]:fmul.d t5, t3, s10, dyn
	-[0x80001814]:csrrs a6, fcsr, zero
	-[0x80001818]:sw t5, 520(ra)
	-[0x8000181c]:sw t6, 528(ra)
	-[0x80001820]:sw t5, 536(ra)
Current Store : [0x80001820] : sw t5, 536(ra) -- Store: [0x800120c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001850]:fmul.d t5, t3, s10, dyn
	-[0x80001854]:csrrs a6, fcsr, zero
	-[0x80001858]:sw t5, 552(ra)
	-[0x8000185c]:sw t6, 560(ra)
Current Store : [0x8000185c] : sw t6, 560(ra) -- Store: [0x800120e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001850]:fmul.d t5, t3, s10, dyn
	-[0x80001854]:csrrs a6, fcsr, zero
	-[0x80001858]:sw t5, 552(ra)
	-[0x8000185c]:sw t6, 560(ra)
	-[0x80001860]:sw t5, 568(ra)
Current Store : [0x80001860] : sw t5, 568(ra) -- Store: [0x800120e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001890]:fmul.d t5, t3, s10, dyn
	-[0x80001894]:csrrs a6, fcsr, zero
	-[0x80001898]:sw t5, 584(ra)
	-[0x8000189c]:sw t6, 592(ra)
Current Store : [0x8000189c] : sw t6, 592(ra) -- Store: [0x80012100]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001890]:fmul.d t5, t3, s10, dyn
	-[0x80001894]:csrrs a6, fcsr, zero
	-[0x80001898]:sw t5, 584(ra)
	-[0x8000189c]:sw t6, 592(ra)
	-[0x800018a0]:sw t5, 600(ra)
Current Store : [0x800018a0] : sw t5, 600(ra) -- Store: [0x80012108]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018d0]:fmul.d t5, t3, s10, dyn
	-[0x800018d4]:csrrs a6, fcsr, zero
	-[0x800018d8]:sw t5, 616(ra)
	-[0x800018dc]:sw t6, 624(ra)
Current Store : [0x800018dc] : sw t6, 624(ra) -- Store: [0x80012120]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018d0]:fmul.d t5, t3, s10, dyn
	-[0x800018d4]:csrrs a6, fcsr, zero
	-[0x800018d8]:sw t5, 616(ra)
	-[0x800018dc]:sw t6, 624(ra)
	-[0x800018e0]:sw t5, 632(ra)
Current Store : [0x800018e0] : sw t5, 632(ra) -- Store: [0x80012128]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001910]:fmul.d t5, t3, s10, dyn
	-[0x80001914]:csrrs a6, fcsr, zero
	-[0x80001918]:sw t5, 648(ra)
	-[0x8000191c]:sw t6, 656(ra)
Current Store : [0x8000191c] : sw t6, 656(ra) -- Store: [0x80012140]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001910]:fmul.d t5, t3, s10, dyn
	-[0x80001914]:csrrs a6, fcsr, zero
	-[0x80001918]:sw t5, 648(ra)
	-[0x8000191c]:sw t6, 656(ra)
	-[0x80001920]:sw t5, 664(ra)
Current Store : [0x80001920] : sw t5, 664(ra) -- Store: [0x80012148]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001950]:fmul.d t5, t3, s10, dyn
	-[0x80001954]:csrrs a6, fcsr, zero
	-[0x80001958]:sw t5, 680(ra)
	-[0x8000195c]:sw t6, 688(ra)
Current Store : [0x8000195c] : sw t6, 688(ra) -- Store: [0x80012160]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001950]:fmul.d t5, t3, s10, dyn
	-[0x80001954]:csrrs a6, fcsr, zero
	-[0x80001958]:sw t5, 680(ra)
	-[0x8000195c]:sw t6, 688(ra)
	-[0x80001960]:sw t5, 696(ra)
Current Store : [0x80001960] : sw t5, 696(ra) -- Store: [0x80012168]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001990]:fmul.d t5, t3, s10, dyn
	-[0x80001994]:csrrs a6, fcsr, zero
	-[0x80001998]:sw t5, 712(ra)
	-[0x8000199c]:sw t6, 720(ra)
Current Store : [0x8000199c] : sw t6, 720(ra) -- Store: [0x80012180]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001990]:fmul.d t5, t3, s10, dyn
	-[0x80001994]:csrrs a6, fcsr, zero
	-[0x80001998]:sw t5, 712(ra)
	-[0x8000199c]:sw t6, 720(ra)
	-[0x800019a0]:sw t5, 728(ra)
Current Store : [0x800019a0] : sw t5, 728(ra) -- Store: [0x80012188]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800019d0]:fmul.d t5, t3, s10, dyn
	-[0x800019d4]:csrrs a6, fcsr, zero
	-[0x800019d8]:sw t5, 744(ra)
	-[0x800019dc]:sw t6, 752(ra)
Current Store : [0x800019dc] : sw t6, 752(ra) -- Store: [0x800121a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800019d0]:fmul.d t5, t3, s10, dyn
	-[0x800019d4]:csrrs a6, fcsr, zero
	-[0x800019d8]:sw t5, 744(ra)
	-[0x800019dc]:sw t6, 752(ra)
	-[0x800019e0]:sw t5, 760(ra)
Current Store : [0x800019e0] : sw t5, 760(ra) -- Store: [0x800121a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a10]:fmul.d t5, t3, s10, dyn
	-[0x80001a14]:csrrs a6, fcsr, zero
	-[0x80001a18]:sw t5, 776(ra)
	-[0x80001a1c]:sw t6, 784(ra)
Current Store : [0x80001a1c] : sw t6, 784(ra) -- Store: [0x800121c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a10]:fmul.d t5, t3, s10, dyn
	-[0x80001a14]:csrrs a6, fcsr, zero
	-[0x80001a18]:sw t5, 776(ra)
	-[0x80001a1c]:sw t6, 784(ra)
	-[0x80001a20]:sw t5, 792(ra)
Current Store : [0x80001a20] : sw t5, 792(ra) -- Store: [0x800121c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a50]:fmul.d t5, t3, s10, dyn
	-[0x80001a54]:csrrs a6, fcsr, zero
	-[0x80001a58]:sw t5, 808(ra)
	-[0x80001a5c]:sw t6, 816(ra)
Current Store : [0x80001a5c] : sw t6, 816(ra) -- Store: [0x800121e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a50]:fmul.d t5, t3, s10, dyn
	-[0x80001a54]:csrrs a6, fcsr, zero
	-[0x80001a58]:sw t5, 808(ra)
	-[0x80001a5c]:sw t6, 816(ra)
	-[0x80001a60]:sw t5, 824(ra)
Current Store : [0x80001a60] : sw t5, 824(ra) -- Store: [0x800121e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a90]:fmul.d t5, t3, s10, dyn
	-[0x80001a94]:csrrs a6, fcsr, zero
	-[0x80001a98]:sw t5, 840(ra)
	-[0x80001a9c]:sw t6, 848(ra)
Current Store : [0x80001a9c] : sw t6, 848(ra) -- Store: [0x80012200]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a90]:fmul.d t5, t3, s10, dyn
	-[0x80001a94]:csrrs a6, fcsr, zero
	-[0x80001a98]:sw t5, 840(ra)
	-[0x80001a9c]:sw t6, 848(ra)
	-[0x80001aa0]:sw t5, 856(ra)
Current Store : [0x80001aa0] : sw t5, 856(ra) -- Store: [0x80012208]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ad0]:fmul.d t5, t3, s10, dyn
	-[0x80001ad4]:csrrs a6, fcsr, zero
	-[0x80001ad8]:sw t5, 872(ra)
	-[0x80001adc]:sw t6, 880(ra)
Current Store : [0x80001adc] : sw t6, 880(ra) -- Store: [0x80012220]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ad0]:fmul.d t5, t3, s10, dyn
	-[0x80001ad4]:csrrs a6, fcsr, zero
	-[0x80001ad8]:sw t5, 872(ra)
	-[0x80001adc]:sw t6, 880(ra)
	-[0x80001ae0]:sw t5, 888(ra)
Current Store : [0x80001ae0] : sw t5, 888(ra) -- Store: [0x80012228]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b14]:fmul.d t5, t3, s10, dyn
	-[0x80001b18]:csrrs a6, fcsr, zero
	-[0x80001b1c]:sw t5, 904(ra)
	-[0x80001b20]:sw t6, 912(ra)
Current Store : [0x80001b20] : sw t6, 912(ra) -- Store: [0x80012240]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b14]:fmul.d t5, t3, s10, dyn
	-[0x80001b18]:csrrs a6, fcsr, zero
	-[0x80001b1c]:sw t5, 904(ra)
	-[0x80001b20]:sw t6, 912(ra)
	-[0x80001b24]:sw t5, 920(ra)
Current Store : [0x80001b24] : sw t5, 920(ra) -- Store: [0x80012248]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b58]:fmul.d t5, t3, s10, dyn
	-[0x80001b5c]:csrrs a6, fcsr, zero
	-[0x80001b60]:sw t5, 936(ra)
	-[0x80001b64]:sw t6, 944(ra)
Current Store : [0x80001b64] : sw t6, 944(ra) -- Store: [0x80012260]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b58]:fmul.d t5, t3, s10, dyn
	-[0x80001b5c]:csrrs a6, fcsr, zero
	-[0x80001b60]:sw t5, 936(ra)
	-[0x80001b64]:sw t6, 944(ra)
	-[0x80001b68]:sw t5, 952(ra)
Current Store : [0x80001b68] : sw t5, 952(ra) -- Store: [0x80012268]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b98]:fmul.d t5, t3, s10, dyn
	-[0x80001b9c]:csrrs a6, fcsr, zero
	-[0x80001ba0]:sw t5, 968(ra)
	-[0x80001ba4]:sw t6, 976(ra)
Current Store : [0x80001ba4] : sw t6, 976(ra) -- Store: [0x80012280]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b98]:fmul.d t5, t3, s10, dyn
	-[0x80001b9c]:csrrs a6, fcsr, zero
	-[0x80001ba0]:sw t5, 968(ra)
	-[0x80001ba4]:sw t6, 976(ra)
	-[0x80001ba8]:sw t5, 984(ra)
Current Store : [0x80001ba8] : sw t5, 984(ra) -- Store: [0x80012288]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001bd8]:fmul.d t5, t3, s10, dyn
	-[0x80001bdc]:csrrs a6, fcsr, zero
	-[0x80001be0]:sw t5, 1000(ra)
	-[0x80001be4]:sw t6, 1008(ra)
Current Store : [0x80001be4] : sw t6, 1008(ra) -- Store: [0x800122a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001bd8]:fmul.d t5, t3, s10, dyn
	-[0x80001bdc]:csrrs a6, fcsr, zero
	-[0x80001be0]:sw t5, 1000(ra)
	-[0x80001be4]:sw t6, 1008(ra)
	-[0x80001be8]:sw t5, 1016(ra)
Current Store : [0x80001be8] : sw t5, 1016(ra) -- Store: [0x800122a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c18]:fmul.d t5, t3, s10, dyn
	-[0x80001c1c]:csrrs a6, fcsr, zero
	-[0x80001c20]:sw t5, 1032(ra)
	-[0x80001c24]:sw t6, 1040(ra)
Current Store : [0x80001c24] : sw t6, 1040(ra) -- Store: [0x800122c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c18]:fmul.d t5, t3, s10, dyn
	-[0x80001c1c]:csrrs a6, fcsr, zero
	-[0x80001c20]:sw t5, 1032(ra)
	-[0x80001c24]:sw t6, 1040(ra)
	-[0x80001c28]:sw t5, 1048(ra)
Current Store : [0x80001c28] : sw t5, 1048(ra) -- Store: [0x800122c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c58]:fmul.d t5, t3, s10, dyn
	-[0x80001c5c]:csrrs a6, fcsr, zero
	-[0x80001c60]:sw t5, 1064(ra)
	-[0x80001c64]:sw t6, 1072(ra)
Current Store : [0x80001c64] : sw t6, 1072(ra) -- Store: [0x800122e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c58]:fmul.d t5, t3, s10, dyn
	-[0x80001c5c]:csrrs a6, fcsr, zero
	-[0x80001c60]:sw t5, 1064(ra)
	-[0x80001c64]:sw t6, 1072(ra)
	-[0x80001c68]:sw t5, 1080(ra)
Current Store : [0x80001c68] : sw t5, 1080(ra) -- Store: [0x800122e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c9c]:fmul.d t5, t3, s10, dyn
	-[0x80001ca0]:csrrs a6, fcsr, zero
	-[0x80001ca4]:sw t5, 1096(ra)
	-[0x80001ca8]:sw t6, 1104(ra)
Current Store : [0x80001ca8] : sw t6, 1104(ra) -- Store: [0x80012300]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c9c]:fmul.d t5, t3, s10, dyn
	-[0x80001ca0]:csrrs a6, fcsr, zero
	-[0x80001ca4]:sw t5, 1096(ra)
	-[0x80001ca8]:sw t6, 1104(ra)
	-[0x80001cac]:sw t5, 1112(ra)
Current Store : [0x80001cac] : sw t5, 1112(ra) -- Store: [0x80012308]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ce0]:fmul.d t5, t3, s10, dyn
	-[0x80001ce4]:csrrs a6, fcsr, zero
	-[0x80001ce8]:sw t5, 1128(ra)
	-[0x80001cec]:sw t6, 1136(ra)
Current Store : [0x80001cec] : sw t6, 1136(ra) -- Store: [0x80012320]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ce0]:fmul.d t5, t3, s10, dyn
	-[0x80001ce4]:csrrs a6, fcsr, zero
	-[0x80001ce8]:sw t5, 1128(ra)
	-[0x80001cec]:sw t6, 1136(ra)
	-[0x80001cf0]:sw t5, 1144(ra)
Current Store : [0x80001cf0] : sw t5, 1144(ra) -- Store: [0x80012328]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d20]:fmul.d t5, t3, s10, dyn
	-[0x80001d24]:csrrs a6, fcsr, zero
	-[0x80001d28]:sw t5, 1160(ra)
	-[0x80001d2c]:sw t6, 1168(ra)
Current Store : [0x80001d2c] : sw t6, 1168(ra) -- Store: [0x80012340]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d20]:fmul.d t5, t3, s10, dyn
	-[0x80001d24]:csrrs a6, fcsr, zero
	-[0x80001d28]:sw t5, 1160(ra)
	-[0x80001d2c]:sw t6, 1168(ra)
	-[0x80001d30]:sw t5, 1176(ra)
Current Store : [0x80001d30] : sw t5, 1176(ra) -- Store: [0x80012348]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d60]:fmul.d t5, t3, s10, dyn
	-[0x80001d64]:csrrs a6, fcsr, zero
	-[0x80001d68]:sw t5, 1192(ra)
	-[0x80001d6c]:sw t6, 1200(ra)
Current Store : [0x80001d6c] : sw t6, 1200(ra) -- Store: [0x80012360]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d60]:fmul.d t5, t3, s10, dyn
	-[0x80001d64]:csrrs a6, fcsr, zero
	-[0x80001d68]:sw t5, 1192(ra)
	-[0x80001d6c]:sw t6, 1200(ra)
	-[0x80001d70]:sw t5, 1208(ra)
Current Store : [0x80001d70] : sw t5, 1208(ra) -- Store: [0x80012368]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001da0]:fmul.d t5, t3, s10, dyn
	-[0x80001da4]:csrrs a6, fcsr, zero
	-[0x80001da8]:sw t5, 1224(ra)
	-[0x80001dac]:sw t6, 1232(ra)
Current Store : [0x80001dac] : sw t6, 1232(ra) -- Store: [0x80012380]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001da0]:fmul.d t5, t3, s10, dyn
	-[0x80001da4]:csrrs a6, fcsr, zero
	-[0x80001da8]:sw t5, 1224(ra)
	-[0x80001dac]:sw t6, 1232(ra)
	-[0x80001db0]:sw t5, 1240(ra)
Current Store : [0x80001db0] : sw t5, 1240(ra) -- Store: [0x80012388]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001de0]:fmul.d t5, t3, s10, dyn
	-[0x80001de4]:csrrs a6, fcsr, zero
	-[0x80001de8]:sw t5, 1256(ra)
	-[0x80001dec]:sw t6, 1264(ra)
Current Store : [0x80001dec] : sw t6, 1264(ra) -- Store: [0x800123a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001de0]:fmul.d t5, t3, s10, dyn
	-[0x80001de4]:csrrs a6, fcsr, zero
	-[0x80001de8]:sw t5, 1256(ra)
	-[0x80001dec]:sw t6, 1264(ra)
	-[0x80001df0]:sw t5, 1272(ra)
Current Store : [0x80001df0] : sw t5, 1272(ra) -- Store: [0x800123a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e20]:fmul.d t5, t3, s10, dyn
	-[0x80001e24]:csrrs a6, fcsr, zero
	-[0x80001e28]:sw t5, 1288(ra)
	-[0x80001e2c]:sw t6, 1296(ra)
Current Store : [0x80001e2c] : sw t6, 1296(ra) -- Store: [0x800123c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e20]:fmul.d t5, t3, s10, dyn
	-[0x80001e24]:csrrs a6, fcsr, zero
	-[0x80001e28]:sw t5, 1288(ra)
	-[0x80001e2c]:sw t6, 1296(ra)
	-[0x80001e30]:sw t5, 1304(ra)
Current Store : [0x80001e30] : sw t5, 1304(ra) -- Store: [0x800123c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e60]:fmul.d t5, t3, s10, dyn
	-[0x80001e64]:csrrs a6, fcsr, zero
	-[0x80001e68]:sw t5, 1320(ra)
	-[0x80001e6c]:sw t6, 1328(ra)
Current Store : [0x80001e6c] : sw t6, 1328(ra) -- Store: [0x800123e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e60]:fmul.d t5, t3, s10, dyn
	-[0x80001e64]:csrrs a6, fcsr, zero
	-[0x80001e68]:sw t5, 1320(ra)
	-[0x80001e6c]:sw t6, 1328(ra)
	-[0x80001e70]:sw t5, 1336(ra)
Current Store : [0x80001e70] : sw t5, 1336(ra) -- Store: [0x800123e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ea0]:fmul.d t5, t3, s10, dyn
	-[0x80001ea4]:csrrs a6, fcsr, zero
	-[0x80001ea8]:sw t5, 1352(ra)
	-[0x80001eac]:sw t6, 1360(ra)
Current Store : [0x80001eac] : sw t6, 1360(ra) -- Store: [0x80012400]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ea0]:fmul.d t5, t3, s10, dyn
	-[0x80001ea4]:csrrs a6, fcsr, zero
	-[0x80001ea8]:sw t5, 1352(ra)
	-[0x80001eac]:sw t6, 1360(ra)
	-[0x80001eb0]:sw t5, 1368(ra)
Current Store : [0x80001eb0] : sw t5, 1368(ra) -- Store: [0x80012408]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ee0]:fmul.d t5, t3, s10, dyn
	-[0x80001ee4]:csrrs a6, fcsr, zero
	-[0x80001ee8]:sw t5, 1384(ra)
	-[0x80001eec]:sw t6, 1392(ra)
Current Store : [0x80001eec] : sw t6, 1392(ra) -- Store: [0x80012420]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ee0]:fmul.d t5, t3, s10, dyn
	-[0x80001ee4]:csrrs a6, fcsr, zero
	-[0x80001ee8]:sw t5, 1384(ra)
	-[0x80001eec]:sw t6, 1392(ra)
	-[0x80001ef0]:sw t5, 1400(ra)
Current Store : [0x80001ef0] : sw t5, 1400(ra) -- Store: [0x80012428]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f20]:fmul.d t5, t3, s10, dyn
	-[0x80001f24]:csrrs a6, fcsr, zero
	-[0x80001f28]:sw t5, 1416(ra)
	-[0x80001f2c]:sw t6, 1424(ra)
Current Store : [0x80001f2c] : sw t6, 1424(ra) -- Store: [0x80012440]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f20]:fmul.d t5, t3, s10, dyn
	-[0x80001f24]:csrrs a6, fcsr, zero
	-[0x80001f28]:sw t5, 1416(ra)
	-[0x80001f2c]:sw t6, 1424(ra)
	-[0x80001f30]:sw t5, 1432(ra)
Current Store : [0x80001f30] : sw t5, 1432(ra) -- Store: [0x80012448]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f60]:fmul.d t5, t3, s10, dyn
	-[0x80001f64]:csrrs a6, fcsr, zero
	-[0x80001f68]:sw t5, 1448(ra)
	-[0x80001f6c]:sw t6, 1456(ra)
Current Store : [0x80001f6c] : sw t6, 1456(ra) -- Store: [0x80012460]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f60]:fmul.d t5, t3, s10, dyn
	-[0x80001f64]:csrrs a6, fcsr, zero
	-[0x80001f68]:sw t5, 1448(ra)
	-[0x80001f6c]:sw t6, 1456(ra)
	-[0x80001f70]:sw t5, 1464(ra)
Current Store : [0x80001f70] : sw t5, 1464(ra) -- Store: [0x80012468]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001fa0]:fmul.d t5, t3, s10, dyn
	-[0x80001fa4]:csrrs a6, fcsr, zero
	-[0x80001fa8]:sw t5, 1480(ra)
	-[0x80001fac]:sw t6, 1488(ra)
Current Store : [0x80001fac] : sw t6, 1488(ra) -- Store: [0x80012480]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001fa0]:fmul.d t5, t3, s10, dyn
	-[0x80001fa4]:csrrs a6, fcsr, zero
	-[0x80001fa8]:sw t5, 1480(ra)
	-[0x80001fac]:sw t6, 1488(ra)
	-[0x80001fb0]:sw t5, 1496(ra)
Current Store : [0x80001fb0] : sw t5, 1496(ra) -- Store: [0x80012488]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001fe0]:fmul.d t5, t3, s10, dyn
	-[0x80001fe4]:csrrs a6, fcsr, zero
	-[0x80001fe8]:sw t5, 1512(ra)
	-[0x80001fec]:sw t6, 1520(ra)
Current Store : [0x80001fec] : sw t6, 1520(ra) -- Store: [0x800124a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001fe0]:fmul.d t5, t3, s10, dyn
	-[0x80001fe4]:csrrs a6, fcsr, zero
	-[0x80001fe8]:sw t5, 1512(ra)
	-[0x80001fec]:sw t6, 1520(ra)
	-[0x80001ff0]:sw t5, 1528(ra)
Current Store : [0x80001ff0] : sw t5, 1528(ra) -- Store: [0x800124a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002020]:fmul.d t5, t3, s10, dyn
	-[0x80002024]:csrrs a6, fcsr, zero
	-[0x80002028]:sw t5, 1544(ra)
	-[0x8000202c]:sw t6, 1552(ra)
Current Store : [0x8000202c] : sw t6, 1552(ra) -- Store: [0x800124c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002020]:fmul.d t5, t3, s10, dyn
	-[0x80002024]:csrrs a6, fcsr, zero
	-[0x80002028]:sw t5, 1544(ra)
	-[0x8000202c]:sw t6, 1552(ra)
	-[0x80002030]:sw t5, 1560(ra)
Current Store : [0x80002030] : sw t5, 1560(ra) -- Store: [0x800124c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002060]:fmul.d t5, t3, s10, dyn
	-[0x80002064]:csrrs a6, fcsr, zero
	-[0x80002068]:sw t5, 1576(ra)
	-[0x8000206c]:sw t6, 1584(ra)
Current Store : [0x8000206c] : sw t6, 1584(ra) -- Store: [0x800124e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002060]:fmul.d t5, t3, s10, dyn
	-[0x80002064]:csrrs a6, fcsr, zero
	-[0x80002068]:sw t5, 1576(ra)
	-[0x8000206c]:sw t6, 1584(ra)
	-[0x80002070]:sw t5, 1592(ra)
Current Store : [0x80002070] : sw t5, 1592(ra) -- Store: [0x800124e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800020a0]:fmul.d t5, t3, s10, dyn
	-[0x800020a4]:csrrs a6, fcsr, zero
	-[0x800020a8]:sw t5, 1608(ra)
	-[0x800020ac]:sw t6, 1616(ra)
Current Store : [0x800020ac] : sw t6, 1616(ra) -- Store: [0x80012500]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800020a0]:fmul.d t5, t3, s10, dyn
	-[0x800020a4]:csrrs a6, fcsr, zero
	-[0x800020a8]:sw t5, 1608(ra)
	-[0x800020ac]:sw t6, 1616(ra)
	-[0x800020b0]:sw t5, 1624(ra)
Current Store : [0x800020b0] : sw t5, 1624(ra) -- Store: [0x80012508]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800020e0]:fmul.d t5, t3, s10, dyn
	-[0x800020e4]:csrrs a6, fcsr, zero
	-[0x800020e8]:sw t5, 1640(ra)
	-[0x800020ec]:sw t6, 1648(ra)
Current Store : [0x800020ec] : sw t6, 1648(ra) -- Store: [0x80012520]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800020e0]:fmul.d t5, t3, s10, dyn
	-[0x800020e4]:csrrs a6, fcsr, zero
	-[0x800020e8]:sw t5, 1640(ra)
	-[0x800020ec]:sw t6, 1648(ra)
	-[0x800020f0]:sw t5, 1656(ra)
Current Store : [0x800020f0] : sw t5, 1656(ra) -- Store: [0x80012528]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002124]:fmul.d t5, t3, s10, dyn
	-[0x80002128]:csrrs a6, fcsr, zero
	-[0x8000212c]:sw t5, 1672(ra)
	-[0x80002130]:sw t6, 1680(ra)
Current Store : [0x80002130] : sw t6, 1680(ra) -- Store: [0x80012540]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002124]:fmul.d t5, t3, s10, dyn
	-[0x80002128]:csrrs a6, fcsr, zero
	-[0x8000212c]:sw t5, 1672(ra)
	-[0x80002130]:sw t6, 1680(ra)
	-[0x80002134]:sw t5, 1688(ra)
Current Store : [0x80002134] : sw t5, 1688(ra) -- Store: [0x80012548]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002168]:fmul.d t5, t3, s10, dyn
	-[0x8000216c]:csrrs a6, fcsr, zero
	-[0x80002170]:sw t5, 1704(ra)
	-[0x80002174]:sw t6, 1712(ra)
Current Store : [0x80002174] : sw t6, 1712(ra) -- Store: [0x80012560]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002168]:fmul.d t5, t3, s10, dyn
	-[0x8000216c]:csrrs a6, fcsr, zero
	-[0x80002170]:sw t5, 1704(ra)
	-[0x80002174]:sw t6, 1712(ra)
	-[0x80002178]:sw t5, 1720(ra)
Current Store : [0x80002178] : sw t5, 1720(ra) -- Store: [0x80012568]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800021a8]:fmul.d t5, t3, s10, dyn
	-[0x800021ac]:csrrs a6, fcsr, zero
	-[0x800021b0]:sw t5, 1736(ra)
	-[0x800021b4]:sw t6, 1744(ra)
Current Store : [0x800021b4] : sw t6, 1744(ra) -- Store: [0x80012580]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800021a8]:fmul.d t5, t3, s10, dyn
	-[0x800021ac]:csrrs a6, fcsr, zero
	-[0x800021b0]:sw t5, 1736(ra)
	-[0x800021b4]:sw t6, 1744(ra)
	-[0x800021b8]:sw t5, 1752(ra)
Current Store : [0x800021b8] : sw t5, 1752(ra) -- Store: [0x80012588]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800021e8]:fmul.d t5, t3, s10, dyn
	-[0x800021ec]:csrrs a6, fcsr, zero
	-[0x800021f0]:sw t5, 1768(ra)
	-[0x800021f4]:sw t6, 1776(ra)
Current Store : [0x800021f4] : sw t6, 1776(ra) -- Store: [0x800125a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800021e8]:fmul.d t5, t3, s10, dyn
	-[0x800021ec]:csrrs a6, fcsr, zero
	-[0x800021f0]:sw t5, 1768(ra)
	-[0x800021f4]:sw t6, 1776(ra)
	-[0x800021f8]:sw t5, 1784(ra)
Current Store : [0x800021f8] : sw t5, 1784(ra) -- Store: [0x800125a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002228]:fmul.d t5, t3, s10, dyn
	-[0x8000222c]:csrrs a6, fcsr, zero
	-[0x80002230]:sw t5, 1800(ra)
	-[0x80002234]:sw t6, 1808(ra)
Current Store : [0x80002234] : sw t6, 1808(ra) -- Store: [0x800125c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002228]:fmul.d t5, t3, s10, dyn
	-[0x8000222c]:csrrs a6, fcsr, zero
	-[0x80002230]:sw t5, 1800(ra)
	-[0x80002234]:sw t6, 1808(ra)
	-[0x80002238]:sw t5, 1816(ra)
Current Store : [0x80002238] : sw t5, 1816(ra) -- Store: [0x800125c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002268]:fmul.d t5, t3, s10, dyn
	-[0x8000226c]:csrrs a6, fcsr, zero
	-[0x80002270]:sw t5, 1832(ra)
	-[0x80002274]:sw t6, 1840(ra)
Current Store : [0x80002274] : sw t6, 1840(ra) -- Store: [0x800125e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002268]:fmul.d t5, t3, s10, dyn
	-[0x8000226c]:csrrs a6, fcsr, zero
	-[0x80002270]:sw t5, 1832(ra)
	-[0x80002274]:sw t6, 1840(ra)
	-[0x80002278]:sw t5, 1848(ra)
Current Store : [0x80002278] : sw t5, 1848(ra) -- Store: [0x800125e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800022ac]:fmul.d t5, t3, s10, dyn
	-[0x800022b0]:csrrs a6, fcsr, zero
	-[0x800022b4]:sw t5, 1864(ra)
	-[0x800022b8]:sw t6, 1872(ra)
Current Store : [0x800022b8] : sw t6, 1872(ra) -- Store: [0x80012600]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800022ac]:fmul.d t5, t3, s10, dyn
	-[0x800022b0]:csrrs a6, fcsr, zero
	-[0x800022b4]:sw t5, 1864(ra)
	-[0x800022b8]:sw t6, 1872(ra)
	-[0x800022bc]:sw t5, 1880(ra)
Current Store : [0x800022bc] : sw t5, 1880(ra) -- Store: [0x80012608]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800022f0]:fmul.d t5, t3, s10, dyn
	-[0x800022f4]:csrrs a6, fcsr, zero
	-[0x800022f8]:sw t5, 1896(ra)
	-[0x800022fc]:sw t6, 1904(ra)
Current Store : [0x800022fc] : sw t6, 1904(ra) -- Store: [0x80012620]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800022f0]:fmul.d t5, t3, s10, dyn
	-[0x800022f4]:csrrs a6, fcsr, zero
	-[0x800022f8]:sw t5, 1896(ra)
	-[0x800022fc]:sw t6, 1904(ra)
	-[0x80002300]:sw t5, 1912(ra)
Current Store : [0x80002300] : sw t5, 1912(ra) -- Store: [0x80012628]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002330]:fmul.d t5, t3, s10, dyn
	-[0x80002334]:csrrs a6, fcsr, zero
	-[0x80002338]:sw t5, 1928(ra)
	-[0x8000233c]:sw t6, 1936(ra)
Current Store : [0x8000233c] : sw t6, 1936(ra) -- Store: [0x80012640]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002330]:fmul.d t5, t3, s10, dyn
	-[0x80002334]:csrrs a6, fcsr, zero
	-[0x80002338]:sw t5, 1928(ra)
	-[0x8000233c]:sw t6, 1936(ra)
	-[0x80002340]:sw t5, 1944(ra)
Current Store : [0x80002340] : sw t5, 1944(ra) -- Store: [0x80012648]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002370]:fmul.d t5, t3, s10, dyn
	-[0x80002374]:csrrs a6, fcsr, zero
	-[0x80002378]:sw t5, 1960(ra)
	-[0x8000237c]:sw t6, 1968(ra)
Current Store : [0x8000237c] : sw t6, 1968(ra) -- Store: [0x80012660]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002370]:fmul.d t5, t3, s10, dyn
	-[0x80002374]:csrrs a6, fcsr, zero
	-[0x80002378]:sw t5, 1960(ra)
	-[0x8000237c]:sw t6, 1968(ra)
	-[0x80002380]:sw t5, 1976(ra)
Current Store : [0x80002380] : sw t5, 1976(ra) -- Store: [0x80012668]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800023b0]:fmul.d t5, t3, s10, dyn
	-[0x800023b4]:csrrs a6, fcsr, zero
	-[0x800023b8]:sw t5, 1992(ra)
	-[0x800023bc]:sw t6, 2000(ra)
Current Store : [0x800023bc] : sw t6, 2000(ra) -- Store: [0x80012680]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800023b0]:fmul.d t5, t3, s10, dyn
	-[0x800023b4]:csrrs a6, fcsr, zero
	-[0x800023b8]:sw t5, 1992(ra)
	-[0x800023bc]:sw t6, 2000(ra)
	-[0x800023c0]:sw t5, 2008(ra)
Current Store : [0x800023c0] : sw t5, 2008(ra) -- Store: [0x80012688]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800023f0]:fmul.d t5, t3, s10, dyn
	-[0x800023f4]:csrrs a6, fcsr, zero
	-[0x800023f8]:sw t5, 2024(ra)
	-[0x800023fc]:sw t6, 2032(ra)
Current Store : [0x800023fc] : sw t6, 2032(ra) -- Store: [0x800126a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800023f0]:fmul.d t5, t3, s10, dyn
	-[0x800023f4]:csrrs a6, fcsr, zero
	-[0x800023f8]:sw t5, 2024(ra)
	-[0x800023fc]:sw t6, 2032(ra)
	-[0x80002400]:sw t5, 2040(ra)
Current Store : [0x80002400] : sw t5, 2040(ra) -- Store: [0x800126a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002474]:fmul.d t5, t3, s10, dyn
	-[0x80002478]:csrrs a6, fcsr, zero
	-[0x8000247c]:sw t5, 16(ra)
	-[0x80002480]:sw t6, 24(ra)
Current Store : [0x80002480] : sw t6, 24(ra) -- Store: [0x800126c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002474]:fmul.d t5, t3, s10, dyn
	-[0x80002478]:csrrs a6, fcsr, zero
	-[0x8000247c]:sw t5, 16(ra)
	-[0x80002480]:sw t6, 24(ra)
	-[0x80002484]:sw t5, 32(ra)
Current Store : [0x80002484] : sw t5, 32(ra) -- Store: [0x800126c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800024f4]:fmul.d t5, t3, s10, dyn
	-[0x800024f8]:csrrs a6, fcsr, zero
	-[0x800024fc]:sw t5, 48(ra)
	-[0x80002500]:sw t6, 56(ra)
Current Store : [0x80002500] : sw t6, 56(ra) -- Store: [0x800126e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800024f4]:fmul.d t5, t3, s10, dyn
	-[0x800024f8]:csrrs a6, fcsr, zero
	-[0x800024fc]:sw t5, 48(ra)
	-[0x80002500]:sw t6, 56(ra)
	-[0x80002504]:sw t5, 64(ra)
Current Store : [0x80002504] : sw t5, 64(ra) -- Store: [0x800126e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002574]:fmul.d t5, t3, s10, dyn
	-[0x80002578]:csrrs a6, fcsr, zero
	-[0x8000257c]:sw t5, 80(ra)
	-[0x80002580]:sw t6, 88(ra)
Current Store : [0x80002580] : sw t6, 88(ra) -- Store: [0x80012700]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002574]:fmul.d t5, t3, s10, dyn
	-[0x80002578]:csrrs a6, fcsr, zero
	-[0x8000257c]:sw t5, 80(ra)
	-[0x80002580]:sw t6, 88(ra)
	-[0x80002584]:sw t5, 96(ra)
Current Store : [0x80002584] : sw t5, 96(ra) -- Store: [0x80012708]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800025f4]:fmul.d t5, t3, s10, dyn
	-[0x800025f8]:csrrs a6, fcsr, zero
	-[0x800025fc]:sw t5, 112(ra)
	-[0x80002600]:sw t6, 120(ra)
Current Store : [0x80002600] : sw t6, 120(ra) -- Store: [0x80012720]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800025f4]:fmul.d t5, t3, s10, dyn
	-[0x800025f8]:csrrs a6, fcsr, zero
	-[0x800025fc]:sw t5, 112(ra)
	-[0x80002600]:sw t6, 120(ra)
	-[0x80002604]:sw t5, 128(ra)
Current Store : [0x80002604] : sw t5, 128(ra) -- Store: [0x80012728]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002674]:fmul.d t5, t3, s10, dyn
	-[0x80002678]:csrrs a6, fcsr, zero
	-[0x8000267c]:sw t5, 144(ra)
	-[0x80002680]:sw t6, 152(ra)
Current Store : [0x80002680] : sw t6, 152(ra) -- Store: [0x80012740]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002674]:fmul.d t5, t3, s10, dyn
	-[0x80002678]:csrrs a6, fcsr, zero
	-[0x8000267c]:sw t5, 144(ra)
	-[0x80002680]:sw t6, 152(ra)
	-[0x80002684]:sw t5, 160(ra)
Current Store : [0x80002684] : sw t5, 160(ra) -- Store: [0x80012748]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800026f4]:fmul.d t5, t3, s10, dyn
	-[0x800026f8]:csrrs a6, fcsr, zero
	-[0x800026fc]:sw t5, 176(ra)
	-[0x80002700]:sw t6, 184(ra)
Current Store : [0x80002700] : sw t6, 184(ra) -- Store: [0x80012760]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800026f4]:fmul.d t5, t3, s10, dyn
	-[0x800026f8]:csrrs a6, fcsr, zero
	-[0x800026fc]:sw t5, 176(ra)
	-[0x80002700]:sw t6, 184(ra)
	-[0x80002704]:sw t5, 192(ra)
Current Store : [0x80002704] : sw t5, 192(ra) -- Store: [0x80012768]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002778]:fmul.d t5, t3, s10, dyn
	-[0x8000277c]:csrrs a6, fcsr, zero
	-[0x80002780]:sw t5, 208(ra)
	-[0x80002784]:sw t6, 216(ra)
Current Store : [0x80002784] : sw t6, 216(ra) -- Store: [0x80012780]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002778]:fmul.d t5, t3, s10, dyn
	-[0x8000277c]:csrrs a6, fcsr, zero
	-[0x80002780]:sw t5, 208(ra)
	-[0x80002784]:sw t6, 216(ra)
	-[0x80002788]:sw t5, 224(ra)
Current Store : [0x80002788] : sw t5, 224(ra) -- Store: [0x80012788]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800027fc]:fmul.d t5, t3, s10, dyn
	-[0x80002800]:csrrs a6, fcsr, zero
	-[0x80002804]:sw t5, 240(ra)
	-[0x80002808]:sw t6, 248(ra)
Current Store : [0x80002808] : sw t6, 248(ra) -- Store: [0x800127a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800027fc]:fmul.d t5, t3, s10, dyn
	-[0x80002800]:csrrs a6, fcsr, zero
	-[0x80002804]:sw t5, 240(ra)
	-[0x80002808]:sw t6, 248(ra)
	-[0x8000280c]:sw t5, 256(ra)
Current Store : [0x8000280c] : sw t5, 256(ra) -- Store: [0x800127a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002880]:fmul.d t5, t3, s10, dyn
	-[0x80002884]:csrrs a6, fcsr, zero
	-[0x80002888]:sw t5, 272(ra)
	-[0x8000288c]:sw t6, 280(ra)
Current Store : [0x8000288c] : sw t6, 280(ra) -- Store: [0x800127c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002880]:fmul.d t5, t3, s10, dyn
	-[0x80002884]:csrrs a6, fcsr, zero
	-[0x80002888]:sw t5, 272(ra)
	-[0x8000288c]:sw t6, 280(ra)
	-[0x80002890]:sw t5, 288(ra)
Current Store : [0x80002890] : sw t5, 288(ra) -- Store: [0x800127c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002904]:fmul.d t5, t3, s10, dyn
	-[0x80002908]:csrrs a6, fcsr, zero
	-[0x8000290c]:sw t5, 304(ra)
	-[0x80002910]:sw t6, 312(ra)
Current Store : [0x80002910] : sw t6, 312(ra) -- Store: [0x800127e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002904]:fmul.d t5, t3, s10, dyn
	-[0x80002908]:csrrs a6, fcsr, zero
	-[0x8000290c]:sw t5, 304(ra)
	-[0x80002910]:sw t6, 312(ra)
	-[0x80002914]:sw t5, 320(ra)
Current Store : [0x80002914] : sw t5, 320(ra) -- Store: [0x800127e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002988]:fmul.d t5, t3, s10, dyn
	-[0x8000298c]:csrrs a6, fcsr, zero
	-[0x80002990]:sw t5, 336(ra)
	-[0x80002994]:sw t6, 344(ra)
Current Store : [0x80002994] : sw t6, 344(ra) -- Store: [0x80012800]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002988]:fmul.d t5, t3, s10, dyn
	-[0x8000298c]:csrrs a6, fcsr, zero
	-[0x80002990]:sw t5, 336(ra)
	-[0x80002994]:sw t6, 344(ra)
	-[0x80002998]:sw t5, 352(ra)
Current Store : [0x80002998] : sw t5, 352(ra) -- Store: [0x80012808]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002a0c]:fmul.d t5, t3, s10, dyn
	-[0x80002a10]:csrrs a6, fcsr, zero
	-[0x80002a14]:sw t5, 368(ra)
	-[0x80002a18]:sw t6, 376(ra)
Current Store : [0x80002a18] : sw t6, 376(ra) -- Store: [0x80012820]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002a0c]:fmul.d t5, t3, s10, dyn
	-[0x80002a10]:csrrs a6, fcsr, zero
	-[0x80002a14]:sw t5, 368(ra)
	-[0x80002a18]:sw t6, 376(ra)
	-[0x80002a1c]:sw t5, 384(ra)
Current Store : [0x80002a1c] : sw t5, 384(ra) -- Store: [0x80012828]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002a94]:fmul.d t5, t3, s10, dyn
	-[0x80002a98]:csrrs a6, fcsr, zero
	-[0x80002a9c]:sw t5, 400(ra)
	-[0x80002aa0]:sw t6, 408(ra)
Current Store : [0x80002aa0] : sw t6, 408(ra) -- Store: [0x80012840]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002a94]:fmul.d t5, t3, s10, dyn
	-[0x80002a98]:csrrs a6, fcsr, zero
	-[0x80002a9c]:sw t5, 400(ra)
	-[0x80002aa0]:sw t6, 408(ra)
	-[0x80002aa4]:sw t5, 416(ra)
Current Store : [0x80002aa4] : sw t5, 416(ra) -- Store: [0x80012848]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002b1c]:fmul.d t5, t3, s10, dyn
	-[0x80002b20]:csrrs a6, fcsr, zero
	-[0x80002b24]:sw t5, 432(ra)
	-[0x80002b28]:sw t6, 440(ra)
Current Store : [0x80002b28] : sw t6, 440(ra) -- Store: [0x80012860]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002b1c]:fmul.d t5, t3, s10, dyn
	-[0x80002b20]:csrrs a6, fcsr, zero
	-[0x80002b24]:sw t5, 432(ra)
	-[0x80002b28]:sw t6, 440(ra)
	-[0x80002b2c]:sw t5, 448(ra)
Current Store : [0x80002b2c] : sw t5, 448(ra) -- Store: [0x80012868]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002ba0]:fmul.d t5, t3, s10, dyn
	-[0x80002ba4]:csrrs a6, fcsr, zero
	-[0x80002ba8]:sw t5, 464(ra)
	-[0x80002bac]:sw t6, 472(ra)
Current Store : [0x80002bac] : sw t6, 472(ra) -- Store: [0x80012880]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002ba0]:fmul.d t5, t3, s10, dyn
	-[0x80002ba4]:csrrs a6, fcsr, zero
	-[0x80002ba8]:sw t5, 464(ra)
	-[0x80002bac]:sw t6, 472(ra)
	-[0x80002bb0]:sw t5, 480(ra)
Current Store : [0x80002bb0] : sw t5, 480(ra) -- Store: [0x80012888]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002c24]:fmul.d t5, t3, s10, dyn
	-[0x80002c28]:csrrs a6, fcsr, zero
	-[0x80002c2c]:sw t5, 496(ra)
	-[0x80002c30]:sw t6, 504(ra)
Current Store : [0x80002c30] : sw t6, 504(ra) -- Store: [0x800128a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002c24]:fmul.d t5, t3, s10, dyn
	-[0x80002c28]:csrrs a6, fcsr, zero
	-[0x80002c2c]:sw t5, 496(ra)
	-[0x80002c30]:sw t6, 504(ra)
	-[0x80002c34]:sw t5, 512(ra)
Current Store : [0x80002c34] : sw t5, 512(ra) -- Store: [0x800128a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002ca8]:fmul.d t5, t3, s10, dyn
	-[0x80002cac]:csrrs a6, fcsr, zero
	-[0x80002cb0]:sw t5, 528(ra)
	-[0x80002cb4]:sw t6, 536(ra)
Current Store : [0x80002cb4] : sw t6, 536(ra) -- Store: [0x800128c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002ca8]:fmul.d t5, t3, s10, dyn
	-[0x80002cac]:csrrs a6, fcsr, zero
	-[0x80002cb0]:sw t5, 528(ra)
	-[0x80002cb4]:sw t6, 536(ra)
	-[0x80002cb8]:sw t5, 544(ra)
Current Store : [0x80002cb8] : sw t5, 544(ra) -- Store: [0x800128c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002d2c]:fmul.d t5, t3, s10, dyn
	-[0x80002d30]:csrrs a6, fcsr, zero
	-[0x80002d34]:sw t5, 560(ra)
	-[0x80002d38]:sw t6, 568(ra)
Current Store : [0x80002d38] : sw t6, 568(ra) -- Store: [0x800128e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002d2c]:fmul.d t5, t3, s10, dyn
	-[0x80002d30]:csrrs a6, fcsr, zero
	-[0x80002d34]:sw t5, 560(ra)
	-[0x80002d38]:sw t6, 568(ra)
	-[0x80002d3c]:sw t5, 576(ra)
Current Store : [0x80002d3c] : sw t5, 576(ra) -- Store: [0x800128e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002db4]:fmul.d t5, t3, s10, dyn
	-[0x80002db8]:csrrs a6, fcsr, zero
	-[0x80002dbc]:sw t5, 592(ra)
	-[0x80002dc0]:sw t6, 600(ra)
Current Store : [0x80002dc0] : sw t6, 600(ra) -- Store: [0x80012900]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002db4]:fmul.d t5, t3, s10, dyn
	-[0x80002db8]:csrrs a6, fcsr, zero
	-[0x80002dbc]:sw t5, 592(ra)
	-[0x80002dc0]:sw t6, 600(ra)
	-[0x80002dc4]:sw t5, 608(ra)
Current Store : [0x80002dc4] : sw t5, 608(ra) -- Store: [0x80012908]:0xFFFFFFFD




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002e3c]:fmul.d t5, t3, s10, dyn
	-[0x80002e40]:csrrs a6, fcsr, zero
	-[0x80002e44]:sw t5, 624(ra)
	-[0x80002e48]:sw t6, 632(ra)
Current Store : [0x80002e48] : sw t6, 632(ra) -- Store: [0x80012920]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002e3c]:fmul.d t5, t3, s10, dyn
	-[0x80002e40]:csrrs a6, fcsr, zero
	-[0x80002e44]:sw t5, 624(ra)
	-[0x80002e48]:sw t6, 632(ra)
	-[0x80002e4c]:sw t5, 640(ra)
Current Store : [0x80002e4c] : sw t5, 640(ra) -- Store: [0x80012928]:0xFFFFFFFD




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002ec0]:fmul.d t5, t3, s10, dyn
	-[0x80002ec4]:csrrs a6, fcsr, zero
	-[0x80002ec8]:sw t5, 656(ra)
	-[0x80002ecc]:sw t6, 664(ra)
Current Store : [0x80002ecc] : sw t6, 664(ra) -- Store: [0x80012940]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002ec0]:fmul.d t5, t3, s10, dyn
	-[0x80002ec4]:csrrs a6, fcsr, zero
	-[0x80002ec8]:sw t5, 656(ra)
	-[0x80002ecc]:sw t6, 664(ra)
	-[0x80002ed0]:sw t5, 672(ra)
Current Store : [0x80002ed0] : sw t5, 672(ra) -- Store: [0x80012948]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002f44]:fmul.d t5, t3, s10, dyn
	-[0x80002f48]:csrrs a6, fcsr, zero
	-[0x80002f4c]:sw t5, 688(ra)
	-[0x80002f50]:sw t6, 696(ra)
Current Store : [0x80002f50] : sw t6, 696(ra) -- Store: [0x80012960]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002f44]:fmul.d t5, t3, s10, dyn
	-[0x80002f48]:csrrs a6, fcsr, zero
	-[0x80002f4c]:sw t5, 688(ra)
	-[0x80002f50]:sw t6, 696(ra)
	-[0x80002f54]:sw t5, 704(ra)
Current Store : [0x80002f54] : sw t5, 704(ra) -- Store: [0x80012968]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002fc8]:fmul.d t5, t3, s10, dyn
	-[0x80002fcc]:csrrs a6, fcsr, zero
	-[0x80002fd0]:sw t5, 720(ra)
	-[0x80002fd4]:sw t6, 728(ra)
Current Store : [0x80002fd4] : sw t6, 728(ra) -- Store: [0x80012980]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002fc8]:fmul.d t5, t3, s10, dyn
	-[0x80002fcc]:csrrs a6, fcsr, zero
	-[0x80002fd0]:sw t5, 720(ra)
	-[0x80002fd4]:sw t6, 728(ra)
	-[0x80002fd8]:sw t5, 736(ra)
Current Store : [0x80002fd8] : sw t5, 736(ra) -- Store: [0x80012988]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000304c]:fmul.d t5, t3, s10, dyn
	-[0x80003050]:csrrs a6, fcsr, zero
	-[0x80003054]:sw t5, 752(ra)
	-[0x80003058]:sw t6, 760(ra)
Current Store : [0x80003058] : sw t6, 760(ra) -- Store: [0x800129a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000304c]:fmul.d t5, t3, s10, dyn
	-[0x80003050]:csrrs a6, fcsr, zero
	-[0x80003054]:sw t5, 752(ra)
	-[0x80003058]:sw t6, 760(ra)
	-[0x8000305c]:sw t5, 768(ra)
Current Store : [0x8000305c] : sw t5, 768(ra) -- Store: [0x800129a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800030d0]:fmul.d t5, t3, s10, dyn
	-[0x800030d4]:csrrs a6, fcsr, zero
	-[0x800030d8]:sw t5, 784(ra)
	-[0x800030dc]:sw t6, 792(ra)
Current Store : [0x800030dc] : sw t6, 792(ra) -- Store: [0x800129c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800030d0]:fmul.d t5, t3, s10, dyn
	-[0x800030d4]:csrrs a6, fcsr, zero
	-[0x800030d8]:sw t5, 784(ra)
	-[0x800030dc]:sw t6, 792(ra)
	-[0x800030e0]:sw t5, 800(ra)
Current Store : [0x800030e0] : sw t5, 800(ra) -- Store: [0x800129c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003154]:fmul.d t5, t3, s10, dyn
	-[0x80003158]:csrrs a6, fcsr, zero
	-[0x8000315c]:sw t5, 816(ra)
	-[0x80003160]:sw t6, 824(ra)
Current Store : [0x80003160] : sw t6, 824(ra) -- Store: [0x800129e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003154]:fmul.d t5, t3, s10, dyn
	-[0x80003158]:csrrs a6, fcsr, zero
	-[0x8000315c]:sw t5, 816(ra)
	-[0x80003160]:sw t6, 824(ra)
	-[0x80003164]:sw t5, 832(ra)
Current Store : [0x80003164] : sw t5, 832(ra) -- Store: [0x800129e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800031d8]:fmul.d t5, t3, s10, dyn
	-[0x800031dc]:csrrs a6, fcsr, zero
	-[0x800031e0]:sw t5, 848(ra)
	-[0x800031e4]:sw t6, 856(ra)
Current Store : [0x800031e4] : sw t6, 856(ra) -- Store: [0x80012a00]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800031d8]:fmul.d t5, t3, s10, dyn
	-[0x800031dc]:csrrs a6, fcsr, zero
	-[0x800031e0]:sw t5, 848(ra)
	-[0x800031e4]:sw t6, 856(ra)
	-[0x800031e8]:sw t5, 864(ra)
Current Store : [0x800031e8] : sw t5, 864(ra) -- Store: [0x80012a08]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000325c]:fmul.d t5, t3, s10, dyn
	-[0x80003260]:csrrs a6, fcsr, zero
	-[0x80003264]:sw t5, 880(ra)
	-[0x80003268]:sw t6, 888(ra)
Current Store : [0x80003268] : sw t6, 888(ra) -- Store: [0x80012a20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000325c]:fmul.d t5, t3, s10, dyn
	-[0x80003260]:csrrs a6, fcsr, zero
	-[0x80003264]:sw t5, 880(ra)
	-[0x80003268]:sw t6, 888(ra)
	-[0x8000326c]:sw t5, 896(ra)
Current Store : [0x8000326c] : sw t5, 896(ra) -- Store: [0x80012a28]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800032e0]:fmul.d t5, t3, s10, dyn
	-[0x800032e4]:csrrs a6, fcsr, zero
	-[0x800032e8]:sw t5, 912(ra)
	-[0x800032ec]:sw t6, 920(ra)
Current Store : [0x800032ec] : sw t6, 920(ra) -- Store: [0x80012a40]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800032e0]:fmul.d t5, t3, s10, dyn
	-[0x800032e4]:csrrs a6, fcsr, zero
	-[0x800032e8]:sw t5, 912(ra)
	-[0x800032ec]:sw t6, 920(ra)
	-[0x800032f0]:sw t5, 928(ra)
Current Store : [0x800032f0] : sw t5, 928(ra) -- Store: [0x80012a48]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003364]:fmul.d t5, t3, s10, dyn
	-[0x80003368]:csrrs a6, fcsr, zero
	-[0x8000336c]:sw t5, 944(ra)
	-[0x80003370]:sw t6, 952(ra)
Current Store : [0x80003370] : sw t6, 952(ra) -- Store: [0x80012a60]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003364]:fmul.d t5, t3, s10, dyn
	-[0x80003368]:csrrs a6, fcsr, zero
	-[0x8000336c]:sw t5, 944(ra)
	-[0x80003370]:sw t6, 952(ra)
	-[0x80003374]:sw t5, 960(ra)
Current Store : [0x80003374] : sw t5, 960(ra) -- Store: [0x80012a68]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800033e8]:fmul.d t5, t3, s10, dyn
	-[0x800033ec]:csrrs a6, fcsr, zero
	-[0x800033f0]:sw t5, 976(ra)
	-[0x800033f4]:sw t6, 984(ra)
Current Store : [0x800033f4] : sw t6, 984(ra) -- Store: [0x80012a80]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800033e8]:fmul.d t5, t3, s10, dyn
	-[0x800033ec]:csrrs a6, fcsr, zero
	-[0x800033f0]:sw t5, 976(ra)
	-[0x800033f4]:sw t6, 984(ra)
	-[0x800033f8]:sw t5, 992(ra)
Current Store : [0x800033f8] : sw t5, 992(ra) -- Store: [0x80012a88]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000346c]:fmul.d t5, t3, s10, dyn
	-[0x80003470]:csrrs a6, fcsr, zero
	-[0x80003474]:sw t5, 1008(ra)
	-[0x80003478]:sw t6, 1016(ra)
Current Store : [0x80003478] : sw t6, 1016(ra) -- Store: [0x80012aa0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000346c]:fmul.d t5, t3, s10, dyn
	-[0x80003470]:csrrs a6, fcsr, zero
	-[0x80003474]:sw t5, 1008(ra)
	-[0x80003478]:sw t6, 1016(ra)
	-[0x8000347c]:sw t5, 1024(ra)
Current Store : [0x8000347c] : sw t5, 1024(ra) -- Store: [0x80012aa8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800034f0]:fmul.d t5, t3, s10, dyn
	-[0x800034f4]:csrrs a6, fcsr, zero
	-[0x800034f8]:sw t5, 1040(ra)
	-[0x800034fc]:sw t6, 1048(ra)
Current Store : [0x800034fc] : sw t6, 1048(ra) -- Store: [0x80012ac0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800034f0]:fmul.d t5, t3, s10, dyn
	-[0x800034f4]:csrrs a6, fcsr, zero
	-[0x800034f8]:sw t5, 1040(ra)
	-[0x800034fc]:sw t6, 1048(ra)
	-[0x80003500]:sw t5, 1056(ra)
Current Store : [0x80003500] : sw t5, 1056(ra) -- Store: [0x80012ac8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003574]:fmul.d t5, t3, s10, dyn
	-[0x80003578]:csrrs a6, fcsr, zero
	-[0x8000357c]:sw t5, 1072(ra)
	-[0x80003580]:sw t6, 1080(ra)
Current Store : [0x80003580] : sw t6, 1080(ra) -- Store: [0x80012ae0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003574]:fmul.d t5, t3, s10, dyn
	-[0x80003578]:csrrs a6, fcsr, zero
	-[0x8000357c]:sw t5, 1072(ra)
	-[0x80003580]:sw t6, 1080(ra)
	-[0x80003584]:sw t5, 1088(ra)
Current Store : [0x80003584] : sw t5, 1088(ra) -- Store: [0x80012ae8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800035f8]:fmul.d t5, t3, s10, dyn
	-[0x800035fc]:csrrs a6, fcsr, zero
	-[0x80003600]:sw t5, 1104(ra)
	-[0x80003604]:sw t6, 1112(ra)
Current Store : [0x80003604] : sw t6, 1112(ra) -- Store: [0x80012b00]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800035f8]:fmul.d t5, t3, s10, dyn
	-[0x800035fc]:csrrs a6, fcsr, zero
	-[0x80003600]:sw t5, 1104(ra)
	-[0x80003604]:sw t6, 1112(ra)
	-[0x80003608]:sw t5, 1120(ra)
Current Store : [0x80003608] : sw t5, 1120(ra) -- Store: [0x80012b08]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000367c]:fmul.d t5, t3, s10, dyn
	-[0x80003680]:csrrs a6, fcsr, zero
	-[0x80003684]:sw t5, 1136(ra)
	-[0x80003688]:sw t6, 1144(ra)
Current Store : [0x80003688] : sw t6, 1144(ra) -- Store: [0x80012b20]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000367c]:fmul.d t5, t3, s10, dyn
	-[0x80003680]:csrrs a6, fcsr, zero
	-[0x80003684]:sw t5, 1136(ra)
	-[0x80003688]:sw t6, 1144(ra)
	-[0x8000368c]:sw t5, 1152(ra)
Current Store : [0x8000368c] : sw t5, 1152(ra) -- Store: [0x80012b28]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003704]:fmul.d t5, t3, s10, dyn
	-[0x80003708]:csrrs a6, fcsr, zero
	-[0x8000370c]:sw t5, 1168(ra)
	-[0x80003710]:sw t6, 1176(ra)
Current Store : [0x80003710] : sw t6, 1176(ra) -- Store: [0x80012b40]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003704]:fmul.d t5, t3, s10, dyn
	-[0x80003708]:csrrs a6, fcsr, zero
	-[0x8000370c]:sw t5, 1168(ra)
	-[0x80003710]:sw t6, 1176(ra)
	-[0x80003714]:sw t5, 1184(ra)
Current Store : [0x80003714] : sw t5, 1184(ra) -- Store: [0x80012b48]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000378c]:fmul.d t5, t3, s10, dyn
	-[0x80003790]:csrrs a6, fcsr, zero
	-[0x80003794]:sw t5, 1200(ra)
	-[0x80003798]:sw t6, 1208(ra)
Current Store : [0x80003798] : sw t6, 1208(ra) -- Store: [0x80012b60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000378c]:fmul.d t5, t3, s10, dyn
	-[0x80003790]:csrrs a6, fcsr, zero
	-[0x80003794]:sw t5, 1200(ra)
	-[0x80003798]:sw t6, 1208(ra)
	-[0x8000379c]:sw t5, 1216(ra)
Current Store : [0x8000379c] : sw t5, 1216(ra) -- Store: [0x80012b68]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003810]:fmul.d t5, t3, s10, dyn
	-[0x80003814]:csrrs a6, fcsr, zero
	-[0x80003818]:sw t5, 1232(ra)
	-[0x8000381c]:sw t6, 1240(ra)
Current Store : [0x8000381c] : sw t6, 1240(ra) -- Store: [0x80012b80]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003810]:fmul.d t5, t3, s10, dyn
	-[0x80003814]:csrrs a6, fcsr, zero
	-[0x80003818]:sw t5, 1232(ra)
	-[0x8000381c]:sw t6, 1240(ra)
	-[0x80003820]:sw t5, 1248(ra)
Current Store : [0x80003820] : sw t5, 1248(ra) -- Store: [0x80012b88]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003894]:fmul.d t5, t3, s10, dyn
	-[0x80003898]:csrrs a6, fcsr, zero
	-[0x8000389c]:sw t5, 1264(ra)
	-[0x800038a0]:sw t6, 1272(ra)
Current Store : [0x800038a0] : sw t6, 1272(ra) -- Store: [0x80012ba0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003894]:fmul.d t5, t3, s10, dyn
	-[0x80003898]:csrrs a6, fcsr, zero
	-[0x8000389c]:sw t5, 1264(ra)
	-[0x800038a0]:sw t6, 1272(ra)
	-[0x800038a4]:sw t5, 1280(ra)
Current Store : [0x800038a4] : sw t5, 1280(ra) -- Store: [0x80012ba8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003918]:fmul.d t5, t3, s10, dyn
	-[0x8000391c]:csrrs a6, fcsr, zero
	-[0x80003920]:sw t5, 1296(ra)
	-[0x80003924]:sw t6, 1304(ra)
Current Store : [0x80003924] : sw t6, 1304(ra) -- Store: [0x80012bc0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003918]:fmul.d t5, t3, s10, dyn
	-[0x8000391c]:csrrs a6, fcsr, zero
	-[0x80003920]:sw t5, 1296(ra)
	-[0x80003924]:sw t6, 1304(ra)
	-[0x80003928]:sw t5, 1312(ra)
Current Store : [0x80003928] : sw t5, 1312(ra) -- Store: [0x80012bc8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000399c]:fmul.d t5, t3, s10, dyn
	-[0x800039a0]:csrrs a6, fcsr, zero
	-[0x800039a4]:sw t5, 1328(ra)
	-[0x800039a8]:sw t6, 1336(ra)
Current Store : [0x800039a8] : sw t6, 1336(ra) -- Store: [0x80012be0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000399c]:fmul.d t5, t3, s10, dyn
	-[0x800039a0]:csrrs a6, fcsr, zero
	-[0x800039a4]:sw t5, 1328(ra)
	-[0x800039a8]:sw t6, 1336(ra)
	-[0x800039ac]:sw t5, 1344(ra)
Current Store : [0x800039ac] : sw t5, 1344(ra) -- Store: [0x80012be8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003a24]:fmul.d t5, t3, s10, dyn
	-[0x80003a28]:csrrs a6, fcsr, zero
	-[0x80003a2c]:sw t5, 1360(ra)
	-[0x80003a30]:sw t6, 1368(ra)
Current Store : [0x80003a30] : sw t6, 1368(ra) -- Store: [0x80012c00]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003a24]:fmul.d t5, t3, s10, dyn
	-[0x80003a28]:csrrs a6, fcsr, zero
	-[0x80003a2c]:sw t5, 1360(ra)
	-[0x80003a30]:sw t6, 1368(ra)
	-[0x80003a34]:sw t5, 1376(ra)
Current Store : [0x80003a34] : sw t5, 1376(ra) -- Store: [0x80012c08]:0xFFFFFFFD




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003aac]:fmul.d t5, t3, s10, dyn
	-[0x80003ab0]:csrrs a6, fcsr, zero
	-[0x80003ab4]:sw t5, 1392(ra)
	-[0x80003ab8]:sw t6, 1400(ra)
Current Store : [0x80003ab8] : sw t6, 1400(ra) -- Store: [0x80012c20]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003aac]:fmul.d t5, t3, s10, dyn
	-[0x80003ab0]:csrrs a6, fcsr, zero
	-[0x80003ab4]:sw t5, 1392(ra)
	-[0x80003ab8]:sw t6, 1400(ra)
	-[0x80003abc]:sw t5, 1408(ra)
Current Store : [0x80003abc] : sw t5, 1408(ra) -- Store: [0x80012c28]:0xFFFFFFFD




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003b30]:fmul.d t5, t3, s10, dyn
	-[0x80003b34]:csrrs a6, fcsr, zero
	-[0x80003b38]:sw t5, 1424(ra)
	-[0x80003b3c]:sw t6, 1432(ra)
Current Store : [0x80003b3c] : sw t6, 1432(ra) -- Store: [0x80012c40]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003b30]:fmul.d t5, t3, s10, dyn
	-[0x80003b34]:csrrs a6, fcsr, zero
	-[0x80003b38]:sw t5, 1424(ra)
	-[0x80003b3c]:sw t6, 1432(ra)
	-[0x80003b40]:sw t5, 1440(ra)
Current Store : [0x80003b40] : sw t5, 1440(ra) -- Store: [0x80012c48]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003bb4]:fmul.d t5, t3, s10, dyn
	-[0x80003bb8]:csrrs a6, fcsr, zero
	-[0x80003bbc]:sw t5, 1456(ra)
	-[0x80003bc0]:sw t6, 1464(ra)
Current Store : [0x80003bc0] : sw t6, 1464(ra) -- Store: [0x80012c60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003bb4]:fmul.d t5, t3, s10, dyn
	-[0x80003bb8]:csrrs a6, fcsr, zero
	-[0x80003bbc]:sw t5, 1456(ra)
	-[0x80003bc0]:sw t6, 1464(ra)
	-[0x80003bc4]:sw t5, 1472(ra)
Current Store : [0x80003bc4] : sw t5, 1472(ra) -- Store: [0x80012c68]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003c38]:fmul.d t5, t3, s10, dyn
	-[0x80003c3c]:csrrs a6, fcsr, zero
	-[0x80003c40]:sw t5, 1488(ra)
	-[0x80003c44]:sw t6, 1496(ra)
Current Store : [0x80003c44] : sw t6, 1496(ra) -- Store: [0x80012c80]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003c38]:fmul.d t5, t3, s10, dyn
	-[0x80003c3c]:csrrs a6, fcsr, zero
	-[0x80003c40]:sw t5, 1488(ra)
	-[0x80003c44]:sw t6, 1496(ra)
	-[0x80003c48]:sw t5, 1504(ra)
Current Store : [0x80003c48] : sw t5, 1504(ra) -- Store: [0x80012c88]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003cbc]:fmul.d t5, t3, s10, dyn
	-[0x80003cc0]:csrrs a6, fcsr, zero
	-[0x80003cc4]:sw t5, 1520(ra)
	-[0x80003cc8]:sw t6, 1528(ra)
Current Store : [0x80003cc8] : sw t6, 1528(ra) -- Store: [0x80012ca0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003cbc]:fmul.d t5, t3, s10, dyn
	-[0x80003cc0]:csrrs a6, fcsr, zero
	-[0x80003cc4]:sw t5, 1520(ra)
	-[0x80003cc8]:sw t6, 1528(ra)
	-[0x80003ccc]:sw t5, 1536(ra)
Current Store : [0x80003ccc] : sw t5, 1536(ra) -- Store: [0x80012ca8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003d40]:fmul.d t5, t3, s10, dyn
	-[0x80003d44]:csrrs a6, fcsr, zero
	-[0x80003d48]:sw t5, 1552(ra)
	-[0x80003d4c]:sw t6, 1560(ra)
Current Store : [0x80003d4c] : sw t6, 1560(ra) -- Store: [0x80012cc0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003d40]:fmul.d t5, t3, s10, dyn
	-[0x80003d44]:csrrs a6, fcsr, zero
	-[0x80003d48]:sw t5, 1552(ra)
	-[0x80003d4c]:sw t6, 1560(ra)
	-[0x80003d50]:sw t5, 1568(ra)
Current Store : [0x80003d50] : sw t5, 1568(ra) -- Store: [0x80012cc8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003dc4]:fmul.d t5, t3, s10, dyn
	-[0x80003dc8]:csrrs a6, fcsr, zero
	-[0x80003dcc]:sw t5, 1584(ra)
	-[0x80003dd0]:sw t6, 1592(ra)
Current Store : [0x80003dd0] : sw t6, 1592(ra) -- Store: [0x80012ce0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003dc4]:fmul.d t5, t3, s10, dyn
	-[0x80003dc8]:csrrs a6, fcsr, zero
	-[0x80003dcc]:sw t5, 1584(ra)
	-[0x80003dd0]:sw t6, 1592(ra)
	-[0x80003dd4]:sw t5, 1600(ra)
Current Store : [0x80003dd4] : sw t5, 1600(ra) -- Store: [0x80012ce8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003e48]:fmul.d t5, t3, s10, dyn
	-[0x80003e4c]:csrrs a6, fcsr, zero
	-[0x80003e50]:sw t5, 1616(ra)
	-[0x80003e54]:sw t6, 1624(ra)
Current Store : [0x80003e54] : sw t6, 1624(ra) -- Store: [0x80012d00]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003e48]:fmul.d t5, t3, s10, dyn
	-[0x80003e4c]:csrrs a6, fcsr, zero
	-[0x80003e50]:sw t5, 1616(ra)
	-[0x80003e54]:sw t6, 1624(ra)
	-[0x80003e58]:sw t5, 1632(ra)
Current Store : [0x80003e58] : sw t5, 1632(ra) -- Store: [0x80012d08]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003ecc]:fmul.d t5, t3, s10, dyn
	-[0x80003ed0]:csrrs a6, fcsr, zero
	-[0x80003ed4]:sw t5, 1648(ra)
	-[0x80003ed8]:sw t6, 1656(ra)
Current Store : [0x80003ed8] : sw t6, 1656(ra) -- Store: [0x80012d20]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003ecc]:fmul.d t5, t3, s10, dyn
	-[0x80003ed0]:csrrs a6, fcsr, zero
	-[0x80003ed4]:sw t5, 1648(ra)
	-[0x80003ed8]:sw t6, 1656(ra)
	-[0x80003edc]:sw t5, 1664(ra)
Current Store : [0x80003edc] : sw t5, 1664(ra) -- Store: [0x80012d28]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003f50]:fmul.d t5, t3, s10, dyn
	-[0x80003f54]:csrrs a6, fcsr, zero
	-[0x80003f58]:sw t5, 1680(ra)
	-[0x80003f5c]:sw t6, 1688(ra)
Current Store : [0x80003f5c] : sw t6, 1688(ra) -- Store: [0x80012d40]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003f50]:fmul.d t5, t3, s10, dyn
	-[0x80003f54]:csrrs a6, fcsr, zero
	-[0x80003f58]:sw t5, 1680(ra)
	-[0x80003f5c]:sw t6, 1688(ra)
	-[0x80003f60]:sw t5, 1696(ra)
Current Store : [0x80003f60] : sw t5, 1696(ra) -- Store: [0x80012d48]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003fd4]:fmul.d t5, t3, s10, dyn
	-[0x80003fd8]:csrrs a6, fcsr, zero
	-[0x80003fdc]:sw t5, 1712(ra)
	-[0x80003fe0]:sw t6, 1720(ra)
Current Store : [0x80003fe0] : sw t6, 1720(ra) -- Store: [0x80012d60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003fd4]:fmul.d t5, t3, s10, dyn
	-[0x80003fd8]:csrrs a6, fcsr, zero
	-[0x80003fdc]:sw t5, 1712(ra)
	-[0x80003fe0]:sw t6, 1720(ra)
	-[0x80003fe4]:sw t5, 1728(ra)
Current Store : [0x80003fe4] : sw t5, 1728(ra) -- Store: [0x80012d68]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004054]:fmul.d t5, t3, s10, dyn
	-[0x80004058]:csrrs a6, fcsr, zero
	-[0x8000405c]:sw t5, 1744(ra)
	-[0x80004060]:sw t6, 1752(ra)
Current Store : [0x80004060] : sw t6, 1752(ra) -- Store: [0x80012d80]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004054]:fmul.d t5, t3, s10, dyn
	-[0x80004058]:csrrs a6, fcsr, zero
	-[0x8000405c]:sw t5, 1744(ra)
	-[0x80004060]:sw t6, 1752(ra)
	-[0x80004064]:sw t5, 1760(ra)
Current Store : [0x80004064] : sw t5, 1760(ra) -- Store: [0x80012d88]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800040d4]:fmul.d t5, t3, s10, dyn
	-[0x800040d8]:csrrs a6, fcsr, zero
	-[0x800040dc]:sw t5, 1776(ra)
	-[0x800040e0]:sw t6, 1784(ra)
Current Store : [0x800040e0] : sw t6, 1784(ra) -- Store: [0x80012da0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800040d4]:fmul.d t5, t3, s10, dyn
	-[0x800040d8]:csrrs a6, fcsr, zero
	-[0x800040dc]:sw t5, 1776(ra)
	-[0x800040e0]:sw t6, 1784(ra)
	-[0x800040e4]:sw t5, 1792(ra)
Current Store : [0x800040e4] : sw t5, 1792(ra) -- Store: [0x80012da8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004154]:fmul.d t5, t3, s10, dyn
	-[0x80004158]:csrrs a6, fcsr, zero
	-[0x8000415c]:sw t5, 1808(ra)
	-[0x80004160]:sw t6, 1816(ra)
Current Store : [0x80004160] : sw t6, 1816(ra) -- Store: [0x80012dc0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004154]:fmul.d t5, t3, s10, dyn
	-[0x80004158]:csrrs a6, fcsr, zero
	-[0x8000415c]:sw t5, 1808(ra)
	-[0x80004160]:sw t6, 1816(ra)
	-[0x80004164]:sw t5, 1824(ra)
Current Store : [0x80004164] : sw t5, 1824(ra) -- Store: [0x80012dc8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800041d4]:fmul.d t5, t3, s10, dyn
	-[0x800041d8]:csrrs a6, fcsr, zero
	-[0x800041dc]:sw t5, 1840(ra)
	-[0x800041e0]:sw t6, 1848(ra)
Current Store : [0x800041e0] : sw t6, 1848(ra) -- Store: [0x80012de0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800041d4]:fmul.d t5, t3, s10, dyn
	-[0x800041d8]:csrrs a6, fcsr, zero
	-[0x800041dc]:sw t5, 1840(ra)
	-[0x800041e0]:sw t6, 1848(ra)
	-[0x800041e4]:sw t5, 1856(ra)
Current Store : [0x800041e4] : sw t5, 1856(ra) -- Store: [0x80012de8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004254]:fmul.d t5, t3, s10, dyn
	-[0x80004258]:csrrs a6, fcsr, zero
	-[0x8000425c]:sw t5, 1872(ra)
	-[0x80004260]:sw t6, 1880(ra)
Current Store : [0x80004260] : sw t6, 1880(ra) -- Store: [0x80012e00]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004254]:fmul.d t5, t3, s10, dyn
	-[0x80004258]:csrrs a6, fcsr, zero
	-[0x8000425c]:sw t5, 1872(ra)
	-[0x80004260]:sw t6, 1880(ra)
	-[0x80004264]:sw t5, 1888(ra)
Current Store : [0x80004264] : sw t5, 1888(ra) -- Store: [0x80012e08]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800042d4]:fmul.d t5, t3, s10, dyn
	-[0x800042d8]:csrrs a6, fcsr, zero
	-[0x800042dc]:sw t5, 1904(ra)
	-[0x800042e0]:sw t6, 1912(ra)
Current Store : [0x800042e0] : sw t6, 1912(ra) -- Store: [0x80012e20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800042d4]:fmul.d t5, t3, s10, dyn
	-[0x800042d8]:csrrs a6, fcsr, zero
	-[0x800042dc]:sw t5, 1904(ra)
	-[0x800042e0]:sw t6, 1912(ra)
	-[0x800042e4]:sw t5, 1920(ra)
Current Store : [0x800042e4] : sw t5, 1920(ra) -- Store: [0x80012e28]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004358]:fmul.d t5, t3, s10, dyn
	-[0x8000435c]:csrrs a6, fcsr, zero
	-[0x80004360]:sw t5, 1936(ra)
	-[0x80004364]:sw t6, 1944(ra)
Current Store : [0x80004364] : sw t6, 1944(ra) -- Store: [0x80012e40]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004358]:fmul.d t5, t3, s10, dyn
	-[0x8000435c]:csrrs a6, fcsr, zero
	-[0x80004360]:sw t5, 1936(ra)
	-[0x80004364]:sw t6, 1944(ra)
	-[0x80004368]:sw t5, 1952(ra)
Current Store : [0x80004368] : sw t5, 1952(ra) -- Store: [0x80012e48]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800043dc]:fmul.d t5, t3, s10, dyn
	-[0x800043e0]:csrrs a6, fcsr, zero
	-[0x800043e4]:sw t5, 1968(ra)
	-[0x800043e8]:sw t6, 1976(ra)
Current Store : [0x800043e8] : sw t6, 1976(ra) -- Store: [0x80012e60]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800043dc]:fmul.d t5, t3, s10, dyn
	-[0x800043e0]:csrrs a6, fcsr, zero
	-[0x800043e4]:sw t5, 1968(ra)
	-[0x800043e8]:sw t6, 1976(ra)
	-[0x800043ec]:sw t5, 1984(ra)
Current Store : [0x800043ec] : sw t5, 1984(ra) -- Store: [0x80012e68]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000445c]:fmul.d t5, t3, s10, dyn
	-[0x80004460]:csrrs a6, fcsr, zero
	-[0x80004464]:sw t5, 2000(ra)
	-[0x80004468]:sw t6, 2008(ra)
Current Store : [0x80004468] : sw t6, 2008(ra) -- Store: [0x80012e80]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000445c]:fmul.d t5, t3, s10, dyn
	-[0x80004460]:csrrs a6, fcsr, zero
	-[0x80004464]:sw t5, 2000(ra)
	-[0x80004468]:sw t6, 2008(ra)
	-[0x8000446c]:sw t5, 2016(ra)
Current Store : [0x8000446c] : sw t5, 2016(ra) -- Store: [0x80012e88]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800044dc]:fmul.d t5, t3, s10, dyn
	-[0x800044e0]:csrrs a6, fcsr, zero
	-[0x800044e4]:sw t5, 2032(ra)
	-[0x800044e8]:sw t6, 2040(ra)
Current Store : [0x800044e8] : sw t6, 2040(ra) -- Store: [0x80012ea0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800044dc]:fmul.d t5, t3, s10, dyn
	-[0x800044e0]:csrrs a6, fcsr, zero
	-[0x800044e4]:sw t5, 2032(ra)
	-[0x800044e8]:sw t6, 2040(ra)
	-[0x800044ec]:addi ra, ra, 2040
	-[0x800044f0]:sw t5, 8(ra)
Current Store : [0x800044f0] : sw t5, 8(ra) -- Store: [0x80012ea8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004560]:fmul.d t5, t3, s10, dyn
	-[0x80004564]:csrrs a6, fcsr, zero
	-[0x80004568]:sw t5, 24(ra)
	-[0x8000456c]:sw t6, 32(ra)
Current Store : [0x8000456c] : sw t6, 32(ra) -- Store: [0x80012ec0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004560]:fmul.d t5, t3, s10, dyn
	-[0x80004564]:csrrs a6, fcsr, zero
	-[0x80004568]:sw t5, 24(ra)
	-[0x8000456c]:sw t6, 32(ra)
	-[0x80004570]:sw t5, 40(ra)
Current Store : [0x80004570] : sw t5, 40(ra) -- Store: [0x80012ec8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800045e0]:fmul.d t5, t3, s10, dyn
	-[0x800045e4]:csrrs a6, fcsr, zero
	-[0x800045e8]:sw t5, 56(ra)
	-[0x800045ec]:sw t6, 64(ra)
Current Store : [0x800045ec] : sw t6, 64(ra) -- Store: [0x80012ee0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800045e0]:fmul.d t5, t3, s10, dyn
	-[0x800045e4]:csrrs a6, fcsr, zero
	-[0x800045e8]:sw t5, 56(ra)
	-[0x800045ec]:sw t6, 64(ra)
	-[0x800045f0]:sw t5, 72(ra)
Current Store : [0x800045f0] : sw t5, 72(ra) -- Store: [0x80012ee8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004664]:fmul.d t5, t3, s10, dyn
	-[0x80004668]:csrrs a6, fcsr, zero
	-[0x8000466c]:sw t5, 88(ra)
	-[0x80004670]:sw t6, 96(ra)
Current Store : [0x80004670] : sw t6, 96(ra) -- Store: [0x80012f00]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004664]:fmul.d t5, t3, s10, dyn
	-[0x80004668]:csrrs a6, fcsr, zero
	-[0x8000466c]:sw t5, 88(ra)
	-[0x80004670]:sw t6, 96(ra)
	-[0x80004674]:sw t5, 104(ra)
Current Store : [0x80004674] : sw t5, 104(ra) -- Store: [0x80012f08]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800046e8]:fmul.d t5, t3, s10, dyn
	-[0x800046ec]:csrrs a6, fcsr, zero
	-[0x800046f0]:sw t5, 120(ra)
	-[0x800046f4]:sw t6, 128(ra)
Current Store : [0x800046f4] : sw t6, 128(ra) -- Store: [0x80012f20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800046e8]:fmul.d t5, t3, s10, dyn
	-[0x800046ec]:csrrs a6, fcsr, zero
	-[0x800046f0]:sw t5, 120(ra)
	-[0x800046f4]:sw t6, 128(ra)
	-[0x800046f8]:sw t5, 136(ra)
Current Store : [0x800046f8] : sw t5, 136(ra) -- Store: [0x80012f28]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004768]:fmul.d t5, t3, s10, dyn
	-[0x8000476c]:csrrs a6, fcsr, zero
	-[0x80004770]:sw t5, 152(ra)
	-[0x80004774]:sw t6, 160(ra)
Current Store : [0x80004774] : sw t6, 160(ra) -- Store: [0x80012f40]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004768]:fmul.d t5, t3, s10, dyn
	-[0x8000476c]:csrrs a6, fcsr, zero
	-[0x80004770]:sw t5, 152(ra)
	-[0x80004774]:sw t6, 160(ra)
	-[0x80004778]:sw t5, 168(ra)
Current Store : [0x80004778] : sw t5, 168(ra) -- Store: [0x80012f48]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800047e8]:fmul.d t5, t3, s10, dyn
	-[0x800047ec]:csrrs a6, fcsr, zero
	-[0x800047f0]:sw t5, 184(ra)
	-[0x800047f4]:sw t6, 192(ra)
Current Store : [0x800047f4] : sw t6, 192(ra) -- Store: [0x80012f60]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800047e8]:fmul.d t5, t3, s10, dyn
	-[0x800047ec]:csrrs a6, fcsr, zero
	-[0x800047f0]:sw t5, 184(ra)
	-[0x800047f4]:sw t6, 192(ra)
	-[0x800047f8]:sw t5, 200(ra)
Current Store : [0x800047f8] : sw t5, 200(ra) -- Store: [0x80012f68]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004868]:fmul.d t5, t3, s10, dyn
	-[0x8000486c]:csrrs a6, fcsr, zero
	-[0x80004870]:sw t5, 216(ra)
	-[0x80004874]:sw t6, 224(ra)
Current Store : [0x80004874] : sw t6, 224(ra) -- Store: [0x80012f80]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004868]:fmul.d t5, t3, s10, dyn
	-[0x8000486c]:csrrs a6, fcsr, zero
	-[0x80004870]:sw t5, 216(ra)
	-[0x80004874]:sw t6, 224(ra)
	-[0x80004878]:sw t5, 232(ra)
Current Store : [0x80004878] : sw t5, 232(ra) -- Store: [0x80012f88]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800048e8]:fmul.d t5, t3, s10, dyn
	-[0x800048ec]:csrrs a6, fcsr, zero
	-[0x800048f0]:sw t5, 248(ra)
	-[0x800048f4]:sw t6, 256(ra)
Current Store : [0x800048f4] : sw t6, 256(ra) -- Store: [0x80012fa0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800048e8]:fmul.d t5, t3, s10, dyn
	-[0x800048ec]:csrrs a6, fcsr, zero
	-[0x800048f0]:sw t5, 248(ra)
	-[0x800048f4]:sw t6, 256(ra)
	-[0x800048f8]:sw t5, 264(ra)
Current Store : [0x800048f8] : sw t5, 264(ra) -- Store: [0x80012fa8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004968]:fmul.d t5, t3, s10, dyn
	-[0x8000496c]:csrrs a6, fcsr, zero
	-[0x80004970]:sw t5, 280(ra)
	-[0x80004974]:sw t6, 288(ra)
Current Store : [0x80004974] : sw t6, 288(ra) -- Store: [0x80012fc0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004968]:fmul.d t5, t3, s10, dyn
	-[0x8000496c]:csrrs a6, fcsr, zero
	-[0x80004970]:sw t5, 280(ra)
	-[0x80004974]:sw t6, 288(ra)
	-[0x80004978]:sw t5, 296(ra)
Current Store : [0x80004978] : sw t5, 296(ra) -- Store: [0x80012fc8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800049e8]:fmul.d t5, t3, s10, dyn
	-[0x800049ec]:csrrs a6, fcsr, zero
	-[0x800049f0]:sw t5, 312(ra)
	-[0x800049f4]:sw t6, 320(ra)
Current Store : [0x800049f4] : sw t6, 320(ra) -- Store: [0x80012fe0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800049e8]:fmul.d t5, t3, s10, dyn
	-[0x800049ec]:csrrs a6, fcsr, zero
	-[0x800049f0]:sw t5, 312(ra)
	-[0x800049f4]:sw t6, 320(ra)
	-[0x800049f8]:sw t5, 328(ra)
Current Store : [0x800049f8] : sw t5, 328(ra) -- Store: [0x80012fe8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004a68]:fmul.d t5, t3, s10, dyn
	-[0x80004a6c]:csrrs a6, fcsr, zero
	-[0x80004a70]:sw t5, 344(ra)
	-[0x80004a74]:sw t6, 352(ra)
Current Store : [0x80004a74] : sw t6, 352(ra) -- Store: [0x80013000]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004a68]:fmul.d t5, t3, s10, dyn
	-[0x80004a6c]:csrrs a6, fcsr, zero
	-[0x80004a70]:sw t5, 344(ra)
	-[0x80004a74]:sw t6, 352(ra)
	-[0x80004a78]:sw t5, 360(ra)
Current Store : [0x80004a78] : sw t5, 360(ra) -- Store: [0x80013008]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004ae8]:fmul.d t5, t3, s10, dyn
	-[0x80004aec]:csrrs a6, fcsr, zero
	-[0x80004af0]:sw t5, 376(ra)
	-[0x80004af4]:sw t6, 384(ra)
Current Store : [0x80004af4] : sw t6, 384(ra) -- Store: [0x80013020]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004ae8]:fmul.d t5, t3, s10, dyn
	-[0x80004aec]:csrrs a6, fcsr, zero
	-[0x80004af0]:sw t5, 376(ra)
	-[0x80004af4]:sw t6, 384(ra)
	-[0x80004af8]:sw t5, 392(ra)
Current Store : [0x80004af8] : sw t5, 392(ra) -- Store: [0x80013028]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004b68]:fmul.d t5, t3, s10, dyn
	-[0x80004b6c]:csrrs a6, fcsr, zero
	-[0x80004b70]:sw t5, 408(ra)
	-[0x80004b74]:sw t6, 416(ra)
Current Store : [0x80004b74] : sw t6, 416(ra) -- Store: [0x80013040]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004b68]:fmul.d t5, t3, s10, dyn
	-[0x80004b6c]:csrrs a6, fcsr, zero
	-[0x80004b70]:sw t5, 408(ra)
	-[0x80004b74]:sw t6, 416(ra)
	-[0x80004b78]:sw t5, 424(ra)
Current Store : [0x80004b78] : sw t5, 424(ra) -- Store: [0x80013048]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004be8]:fmul.d t5, t3, s10, dyn
	-[0x80004bec]:csrrs a6, fcsr, zero
	-[0x80004bf0]:sw t5, 440(ra)
	-[0x80004bf4]:sw t6, 448(ra)
Current Store : [0x80004bf4] : sw t6, 448(ra) -- Store: [0x80013060]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004be8]:fmul.d t5, t3, s10, dyn
	-[0x80004bec]:csrrs a6, fcsr, zero
	-[0x80004bf0]:sw t5, 440(ra)
	-[0x80004bf4]:sw t6, 448(ra)
	-[0x80004bf8]:sw t5, 456(ra)
Current Store : [0x80004bf8] : sw t5, 456(ra) -- Store: [0x80013068]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004c68]:fmul.d t5, t3, s10, dyn
	-[0x80004c6c]:csrrs a6, fcsr, zero
	-[0x80004c70]:sw t5, 472(ra)
	-[0x80004c74]:sw t6, 480(ra)
Current Store : [0x80004c74] : sw t6, 480(ra) -- Store: [0x80013080]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004c68]:fmul.d t5, t3, s10, dyn
	-[0x80004c6c]:csrrs a6, fcsr, zero
	-[0x80004c70]:sw t5, 472(ra)
	-[0x80004c74]:sw t6, 480(ra)
	-[0x80004c78]:sw t5, 488(ra)
Current Store : [0x80004c78] : sw t5, 488(ra) -- Store: [0x80013088]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004ce8]:fmul.d t5, t3, s10, dyn
	-[0x80004cec]:csrrs a6, fcsr, zero
	-[0x80004cf0]:sw t5, 504(ra)
	-[0x80004cf4]:sw t6, 512(ra)
Current Store : [0x80004cf4] : sw t6, 512(ra) -- Store: [0x800130a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004ce8]:fmul.d t5, t3, s10, dyn
	-[0x80004cec]:csrrs a6, fcsr, zero
	-[0x80004cf0]:sw t5, 504(ra)
	-[0x80004cf4]:sw t6, 512(ra)
	-[0x80004cf8]:sw t5, 520(ra)
Current Store : [0x80004cf8] : sw t5, 520(ra) -- Store: [0x800130a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004d68]:fmul.d t5, t3, s10, dyn
	-[0x80004d6c]:csrrs a6, fcsr, zero
	-[0x80004d70]:sw t5, 536(ra)
	-[0x80004d74]:sw t6, 544(ra)
Current Store : [0x80004d74] : sw t6, 544(ra) -- Store: [0x800130c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004d68]:fmul.d t5, t3, s10, dyn
	-[0x80004d6c]:csrrs a6, fcsr, zero
	-[0x80004d70]:sw t5, 536(ra)
	-[0x80004d74]:sw t6, 544(ra)
	-[0x80004d78]:sw t5, 552(ra)
Current Store : [0x80004d78] : sw t5, 552(ra) -- Store: [0x800130c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004de8]:fmul.d t5, t3, s10, dyn
	-[0x80004dec]:csrrs a6, fcsr, zero
	-[0x80004df0]:sw t5, 568(ra)
	-[0x80004df4]:sw t6, 576(ra)
Current Store : [0x80004df4] : sw t6, 576(ra) -- Store: [0x800130e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004de8]:fmul.d t5, t3, s10, dyn
	-[0x80004dec]:csrrs a6, fcsr, zero
	-[0x80004df0]:sw t5, 568(ra)
	-[0x80004df4]:sw t6, 576(ra)
	-[0x80004df8]:sw t5, 584(ra)
Current Store : [0x80004df8] : sw t5, 584(ra) -- Store: [0x800130e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004e68]:fmul.d t5, t3, s10, dyn
	-[0x80004e6c]:csrrs a6, fcsr, zero
	-[0x80004e70]:sw t5, 600(ra)
	-[0x80004e74]:sw t6, 608(ra)
Current Store : [0x80004e74] : sw t6, 608(ra) -- Store: [0x80013100]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004e68]:fmul.d t5, t3, s10, dyn
	-[0x80004e6c]:csrrs a6, fcsr, zero
	-[0x80004e70]:sw t5, 600(ra)
	-[0x80004e74]:sw t6, 608(ra)
	-[0x80004e78]:sw t5, 616(ra)
Current Store : [0x80004e78] : sw t5, 616(ra) -- Store: [0x80013108]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004ee8]:fmul.d t5, t3, s10, dyn
	-[0x80004eec]:csrrs a6, fcsr, zero
	-[0x80004ef0]:sw t5, 632(ra)
	-[0x80004ef4]:sw t6, 640(ra)
Current Store : [0x80004ef4] : sw t6, 640(ra) -- Store: [0x80013120]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004ee8]:fmul.d t5, t3, s10, dyn
	-[0x80004eec]:csrrs a6, fcsr, zero
	-[0x80004ef0]:sw t5, 632(ra)
	-[0x80004ef4]:sw t6, 640(ra)
	-[0x80004ef8]:sw t5, 648(ra)
Current Store : [0x80004ef8] : sw t5, 648(ra) -- Store: [0x80013128]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004f6c]:fmul.d t5, t3, s10, dyn
	-[0x80004f70]:csrrs a6, fcsr, zero
	-[0x80004f74]:sw t5, 664(ra)
	-[0x80004f78]:sw t6, 672(ra)
Current Store : [0x80004f78] : sw t6, 672(ra) -- Store: [0x80013140]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004f6c]:fmul.d t5, t3, s10, dyn
	-[0x80004f70]:csrrs a6, fcsr, zero
	-[0x80004f74]:sw t5, 664(ra)
	-[0x80004f78]:sw t6, 672(ra)
	-[0x80004f7c]:sw t5, 680(ra)
Current Store : [0x80004f7c] : sw t5, 680(ra) -- Store: [0x80013148]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004ff0]:fmul.d t5, t3, s10, dyn
	-[0x80004ff4]:csrrs a6, fcsr, zero
	-[0x80004ff8]:sw t5, 696(ra)
	-[0x80004ffc]:sw t6, 704(ra)
Current Store : [0x80004ffc] : sw t6, 704(ra) -- Store: [0x80013160]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004ff0]:fmul.d t5, t3, s10, dyn
	-[0x80004ff4]:csrrs a6, fcsr, zero
	-[0x80004ff8]:sw t5, 696(ra)
	-[0x80004ffc]:sw t6, 704(ra)
	-[0x80005000]:sw t5, 712(ra)
Current Store : [0x80005000] : sw t5, 712(ra) -- Store: [0x80013168]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005070]:fmul.d t5, t3, s10, dyn
	-[0x80005074]:csrrs a6, fcsr, zero
	-[0x80005078]:sw t5, 728(ra)
	-[0x8000507c]:sw t6, 736(ra)
Current Store : [0x8000507c] : sw t6, 736(ra) -- Store: [0x80013180]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005070]:fmul.d t5, t3, s10, dyn
	-[0x80005074]:csrrs a6, fcsr, zero
	-[0x80005078]:sw t5, 728(ra)
	-[0x8000507c]:sw t6, 736(ra)
	-[0x80005080]:sw t5, 744(ra)
Current Store : [0x80005080] : sw t5, 744(ra) -- Store: [0x80013188]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800050f0]:fmul.d t5, t3, s10, dyn
	-[0x800050f4]:csrrs a6, fcsr, zero
	-[0x800050f8]:sw t5, 760(ra)
	-[0x800050fc]:sw t6, 768(ra)
Current Store : [0x800050fc] : sw t6, 768(ra) -- Store: [0x800131a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800050f0]:fmul.d t5, t3, s10, dyn
	-[0x800050f4]:csrrs a6, fcsr, zero
	-[0x800050f8]:sw t5, 760(ra)
	-[0x800050fc]:sw t6, 768(ra)
	-[0x80005100]:sw t5, 776(ra)
Current Store : [0x80005100] : sw t5, 776(ra) -- Store: [0x800131a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005170]:fmul.d t5, t3, s10, dyn
	-[0x80005174]:csrrs a6, fcsr, zero
	-[0x80005178]:sw t5, 792(ra)
	-[0x8000517c]:sw t6, 800(ra)
Current Store : [0x8000517c] : sw t6, 800(ra) -- Store: [0x800131c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005170]:fmul.d t5, t3, s10, dyn
	-[0x80005174]:csrrs a6, fcsr, zero
	-[0x80005178]:sw t5, 792(ra)
	-[0x8000517c]:sw t6, 800(ra)
	-[0x80005180]:sw t5, 808(ra)
Current Store : [0x80005180] : sw t5, 808(ra) -- Store: [0x800131c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800051f0]:fmul.d t5, t3, s10, dyn
	-[0x800051f4]:csrrs a6, fcsr, zero
	-[0x800051f8]:sw t5, 824(ra)
	-[0x800051fc]:sw t6, 832(ra)
Current Store : [0x800051fc] : sw t6, 832(ra) -- Store: [0x800131e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800051f0]:fmul.d t5, t3, s10, dyn
	-[0x800051f4]:csrrs a6, fcsr, zero
	-[0x800051f8]:sw t5, 824(ra)
	-[0x800051fc]:sw t6, 832(ra)
	-[0x80005200]:sw t5, 840(ra)
Current Store : [0x80005200] : sw t5, 840(ra) -- Store: [0x800131e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005274]:fmul.d t5, t3, s10, dyn
	-[0x80005278]:csrrs a6, fcsr, zero
	-[0x8000527c]:sw t5, 856(ra)
	-[0x80005280]:sw t6, 864(ra)
Current Store : [0x80005280] : sw t6, 864(ra) -- Store: [0x80013200]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005274]:fmul.d t5, t3, s10, dyn
	-[0x80005278]:csrrs a6, fcsr, zero
	-[0x8000527c]:sw t5, 856(ra)
	-[0x80005280]:sw t6, 864(ra)
	-[0x80005284]:sw t5, 872(ra)
Current Store : [0x80005284] : sw t5, 872(ra) -- Store: [0x80013208]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800052f8]:fmul.d t5, t3, s10, dyn
	-[0x800052fc]:csrrs a6, fcsr, zero
	-[0x80005300]:sw t5, 888(ra)
	-[0x80005304]:sw t6, 896(ra)
Current Store : [0x80005304] : sw t6, 896(ra) -- Store: [0x80013220]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800052f8]:fmul.d t5, t3, s10, dyn
	-[0x800052fc]:csrrs a6, fcsr, zero
	-[0x80005300]:sw t5, 888(ra)
	-[0x80005304]:sw t6, 896(ra)
	-[0x80005308]:sw t5, 904(ra)
Current Store : [0x80005308] : sw t5, 904(ra) -- Store: [0x80013228]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005378]:fmul.d t5, t3, s10, dyn
	-[0x8000537c]:csrrs a6, fcsr, zero
	-[0x80005380]:sw t5, 920(ra)
	-[0x80005384]:sw t6, 928(ra)
Current Store : [0x80005384] : sw t6, 928(ra) -- Store: [0x80013240]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005378]:fmul.d t5, t3, s10, dyn
	-[0x8000537c]:csrrs a6, fcsr, zero
	-[0x80005380]:sw t5, 920(ra)
	-[0x80005384]:sw t6, 928(ra)
	-[0x80005388]:sw t5, 936(ra)
Current Store : [0x80005388] : sw t5, 936(ra) -- Store: [0x80013248]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800053f8]:fmul.d t5, t3, s10, dyn
	-[0x800053fc]:csrrs a6, fcsr, zero
	-[0x80005400]:sw t5, 952(ra)
	-[0x80005404]:sw t6, 960(ra)
Current Store : [0x80005404] : sw t6, 960(ra) -- Store: [0x80013260]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800053f8]:fmul.d t5, t3, s10, dyn
	-[0x800053fc]:csrrs a6, fcsr, zero
	-[0x80005400]:sw t5, 952(ra)
	-[0x80005404]:sw t6, 960(ra)
	-[0x80005408]:sw t5, 968(ra)
Current Store : [0x80005408] : sw t5, 968(ra) -- Store: [0x80013268]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005478]:fmul.d t5, t3, s10, dyn
	-[0x8000547c]:csrrs a6, fcsr, zero
	-[0x80005480]:sw t5, 984(ra)
	-[0x80005484]:sw t6, 992(ra)
Current Store : [0x80005484] : sw t6, 992(ra) -- Store: [0x80013280]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005478]:fmul.d t5, t3, s10, dyn
	-[0x8000547c]:csrrs a6, fcsr, zero
	-[0x80005480]:sw t5, 984(ra)
	-[0x80005484]:sw t6, 992(ra)
	-[0x80005488]:sw t5, 1000(ra)
Current Store : [0x80005488] : sw t5, 1000(ra) -- Store: [0x80013288]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800054f8]:fmul.d t5, t3, s10, dyn
	-[0x800054fc]:csrrs a6, fcsr, zero
	-[0x80005500]:sw t5, 1016(ra)
	-[0x80005504]:sw t6, 1024(ra)
Current Store : [0x80005504] : sw t6, 1024(ra) -- Store: [0x800132a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800054f8]:fmul.d t5, t3, s10, dyn
	-[0x800054fc]:csrrs a6, fcsr, zero
	-[0x80005500]:sw t5, 1016(ra)
	-[0x80005504]:sw t6, 1024(ra)
	-[0x80005508]:sw t5, 1032(ra)
Current Store : [0x80005508] : sw t5, 1032(ra) -- Store: [0x800132a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005578]:fmul.d t5, t3, s10, dyn
	-[0x8000557c]:csrrs a6, fcsr, zero
	-[0x80005580]:sw t5, 1048(ra)
	-[0x80005584]:sw t6, 1056(ra)
Current Store : [0x80005584] : sw t6, 1056(ra) -- Store: [0x800132c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005578]:fmul.d t5, t3, s10, dyn
	-[0x8000557c]:csrrs a6, fcsr, zero
	-[0x80005580]:sw t5, 1048(ra)
	-[0x80005584]:sw t6, 1056(ra)
	-[0x80005588]:sw t5, 1064(ra)
Current Store : [0x80005588] : sw t5, 1064(ra) -- Store: [0x800132c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800055f8]:fmul.d t5, t3, s10, dyn
	-[0x800055fc]:csrrs a6, fcsr, zero
	-[0x80005600]:sw t5, 1080(ra)
	-[0x80005604]:sw t6, 1088(ra)
Current Store : [0x80005604] : sw t6, 1088(ra) -- Store: [0x800132e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800055f8]:fmul.d t5, t3, s10, dyn
	-[0x800055fc]:csrrs a6, fcsr, zero
	-[0x80005600]:sw t5, 1080(ra)
	-[0x80005604]:sw t6, 1088(ra)
	-[0x80005608]:sw t5, 1096(ra)
Current Store : [0x80005608] : sw t5, 1096(ra) -- Store: [0x800132e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005678]:fmul.d t5, t3, s10, dyn
	-[0x8000567c]:csrrs a6, fcsr, zero
	-[0x80005680]:sw t5, 1112(ra)
	-[0x80005684]:sw t6, 1120(ra)
Current Store : [0x80005684] : sw t6, 1120(ra) -- Store: [0x80013300]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005678]:fmul.d t5, t3, s10, dyn
	-[0x8000567c]:csrrs a6, fcsr, zero
	-[0x80005680]:sw t5, 1112(ra)
	-[0x80005684]:sw t6, 1120(ra)
	-[0x80005688]:sw t5, 1128(ra)
Current Store : [0x80005688] : sw t5, 1128(ra) -- Store: [0x80013308]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800056f8]:fmul.d t5, t3, s10, dyn
	-[0x800056fc]:csrrs a6, fcsr, zero
	-[0x80005700]:sw t5, 1144(ra)
	-[0x80005704]:sw t6, 1152(ra)
Current Store : [0x80005704] : sw t6, 1152(ra) -- Store: [0x80013320]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800056f8]:fmul.d t5, t3, s10, dyn
	-[0x800056fc]:csrrs a6, fcsr, zero
	-[0x80005700]:sw t5, 1144(ra)
	-[0x80005704]:sw t6, 1152(ra)
	-[0x80005708]:sw t5, 1160(ra)
Current Store : [0x80005708] : sw t5, 1160(ra) -- Store: [0x80013328]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005778]:fmul.d t5, t3, s10, dyn
	-[0x8000577c]:csrrs a6, fcsr, zero
	-[0x80005780]:sw t5, 1176(ra)
	-[0x80005784]:sw t6, 1184(ra)
Current Store : [0x80005784] : sw t6, 1184(ra) -- Store: [0x80013340]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005778]:fmul.d t5, t3, s10, dyn
	-[0x8000577c]:csrrs a6, fcsr, zero
	-[0x80005780]:sw t5, 1176(ra)
	-[0x80005784]:sw t6, 1184(ra)
	-[0x80005788]:sw t5, 1192(ra)
Current Store : [0x80005788] : sw t5, 1192(ra) -- Store: [0x80013348]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800057f8]:fmul.d t5, t3, s10, dyn
	-[0x800057fc]:csrrs a6, fcsr, zero
	-[0x80005800]:sw t5, 1208(ra)
	-[0x80005804]:sw t6, 1216(ra)
Current Store : [0x80005804] : sw t6, 1216(ra) -- Store: [0x80013360]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800057f8]:fmul.d t5, t3, s10, dyn
	-[0x800057fc]:csrrs a6, fcsr, zero
	-[0x80005800]:sw t5, 1208(ra)
	-[0x80005804]:sw t6, 1216(ra)
	-[0x80005808]:sw t5, 1224(ra)
Current Store : [0x80005808] : sw t5, 1224(ra) -- Store: [0x80013368]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005878]:fmul.d t5, t3, s10, dyn
	-[0x8000587c]:csrrs a6, fcsr, zero
	-[0x80005880]:sw t5, 1240(ra)
	-[0x80005884]:sw t6, 1248(ra)
Current Store : [0x80005884] : sw t6, 1248(ra) -- Store: [0x80013380]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005878]:fmul.d t5, t3, s10, dyn
	-[0x8000587c]:csrrs a6, fcsr, zero
	-[0x80005880]:sw t5, 1240(ra)
	-[0x80005884]:sw t6, 1248(ra)
	-[0x80005888]:sw t5, 1256(ra)
Current Store : [0x80005888] : sw t5, 1256(ra) -- Store: [0x80013388]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800058f8]:fmul.d t5, t3, s10, dyn
	-[0x800058fc]:csrrs a6, fcsr, zero
	-[0x80005900]:sw t5, 1272(ra)
	-[0x80005904]:sw t6, 1280(ra)
Current Store : [0x80005904] : sw t6, 1280(ra) -- Store: [0x800133a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800058f8]:fmul.d t5, t3, s10, dyn
	-[0x800058fc]:csrrs a6, fcsr, zero
	-[0x80005900]:sw t5, 1272(ra)
	-[0x80005904]:sw t6, 1280(ra)
	-[0x80005908]:sw t5, 1288(ra)
Current Store : [0x80005908] : sw t5, 1288(ra) -- Store: [0x800133a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005978]:fmul.d t5, t3, s10, dyn
	-[0x8000597c]:csrrs a6, fcsr, zero
	-[0x80005980]:sw t5, 1304(ra)
	-[0x80005984]:sw t6, 1312(ra)
Current Store : [0x80005984] : sw t6, 1312(ra) -- Store: [0x800133c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005978]:fmul.d t5, t3, s10, dyn
	-[0x8000597c]:csrrs a6, fcsr, zero
	-[0x80005980]:sw t5, 1304(ra)
	-[0x80005984]:sw t6, 1312(ra)
	-[0x80005988]:sw t5, 1320(ra)
Current Store : [0x80005988] : sw t5, 1320(ra) -- Store: [0x800133c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800059f8]:fmul.d t5, t3, s10, dyn
	-[0x800059fc]:csrrs a6, fcsr, zero
	-[0x80005a00]:sw t5, 1336(ra)
	-[0x80005a04]:sw t6, 1344(ra)
Current Store : [0x80005a04] : sw t6, 1344(ra) -- Store: [0x800133e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800059f8]:fmul.d t5, t3, s10, dyn
	-[0x800059fc]:csrrs a6, fcsr, zero
	-[0x80005a00]:sw t5, 1336(ra)
	-[0x80005a04]:sw t6, 1344(ra)
	-[0x80005a08]:sw t5, 1352(ra)
Current Store : [0x80005a08] : sw t5, 1352(ra) -- Store: [0x800133e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005a78]:fmul.d t5, t3, s10, dyn
	-[0x80005a7c]:csrrs a6, fcsr, zero
	-[0x80005a80]:sw t5, 1368(ra)
	-[0x80005a84]:sw t6, 1376(ra)
Current Store : [0x80005a84] : sw t6, 1376(ra) -- Store: [0x80013400]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005a78]:fmul.d t5, t3, s10, dyn
	-[0x80005a7c]:csrrs a6, fcsr, zero
	-[0x80005a80]:sw t5, 1368(ra)
	-[0x80005a84]:sw t6, 1376(ra)
	-[0x80005a88]:sw t5, 1384(ra)
Current Store : [0x80005a88] : sw t5, 1384(ra) -- Store: [0x80013408]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005af8]:fmul.d t5, t3, s10, dyn
	-[0x80005afc]:csrrs a6, fcsr, zero
	-[0x80005b00]:sw t5, 1400(ra)
	-[0x80005b04]:sw t6, 1408(ra)
Current Store : [0x80005b04] : sw t6, 1408(ra) -- Store: [0x80013420]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005af8]:fmul.d t5, t3, s10, dyn
	-[0x80005afc]:csrrs a6, fcsr, zero
	-[0x80005b00]:sw t5, 1400(ra)
	-[0x80005b04]:sw t6, 1408(ra)
	-[0x80005b08]:sw t5, 1416(ra)
Current Store : [0x80005b08] : sw t5, 1416(ra) -- Store: [0x80013428]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005b7c]:fmul.d t5, t3, s10, dyn
	-[0x80005b80]:csrrs a6, fcsr, zero
	-[0x80005b84]:sw t5, 1432(ra)
	-[0x80005b88]:sw t6, 1440(ra)
Current Store : [0x80005b88] : sw t6, 1440(ra) -- Store: [0x80013440]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005b7c]:fmul.d t5, t3, s10, dyn
	-[0x80005b80]:csrrs a6, fcsr, zero
	-[0x80005b84]:sw t5, 1432(ra)
	-[0x80005b88]:sw t6, 1440(ra)
	-[0x80005b8c]:sw t5, 1448(ra)
Current Store : [0x80005b8c] : sw t5, 1448(ra) -- Store: [0x80013448]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005c00]:fmul.d t5, t3, s10, dyn
	-[0x80005c04]:csrrs a6, fcsr, zero
	-[0x80005c08]:sw t5, 1464(ra)
	-[0x80005c0c]:sw t6, 1472(ra)
Current Store : [0x80005c0c] : sw t6, 1472(ra) -- Store: [0x80013460]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005c00]:fmul.d t5, t3, s10, dyn
	-[0x80005c04]:csrrs a6, fcsr, zero
	-[0x80005c08]:sw t5, 1464(ra)
	-[0x80005c0c]:sw t6, 1472(ra)
	-[0x80005c10]:sw t5, 1480(ra)
Current Store : [0x80005c10] : sw t5, 1480(ra) -- Store: [0x80013468]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005c80]:fmul.d t5, t3, s10, dyn
	-[0x80005c84]:csrrs a6, fcsr, zero
	-[0x80005c88]:sw t5, 1496(ra)
	-[0x80005c8c]:sw t6, 1504(ra)
Current Store : [0x80005c8c] : sw t6, 1504(ra) -- Store: [0x80013480]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005c80]:fmul.d t5, t3, s10, dyn
	-[0x80005c84]:csrrs a6, fcsr, zero
	-[0x80005c88]:sw t5, 1496(ra)
	-[0x80005c8c]:sw t6, 1504(ra)
	-[0x80005c90]:sw t5, 1512(ra)
Current Store : [0x80005c90] : sw t5, 1512(ra) -- Store: [0x80013488]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005d00]:fmul.d t5, t3, s10, dyn
	-[0x80005d04]:csrrs a6, fcsr, zero
	-[0x80005d08]:sw t5, 1528(ra)
	-[0x80005d0c]:sw t6, 1536(ra)
Current Store : [0x80005d0c] : sw t6, 1536(ra) -- Store: [0x800134a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005d00]:fmul.d t5, t3, s10, dyn
	-[0x80005d04]:csrrs a6, fcsr, zero
	-[0x80005d08]:sw t5, 1528(ra)
	-[0x80005d0c]:sw t6, 1536(ra)
	-[0x80005d10]:sw t5, 1544(ra)
Current Store : [0x80005d10] : sw t5, 1544(ra) -- Store: [0x800134a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005d80]:fmul.d t5, t3, s10, dyn
	-[0x80005d84]:csrrs a6, fcsr, zero
	-[0x80005d88]:sw t5, 1560(ra)
	-[0x80005d8c]:sw t6, 1568(ra)
Current Store : [0x80005d8c] : sw t6, 1568(ra) -- Store: [0x800134c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005d80]:fmul.d t5, t3, s10, dyn
	-[0x80005d84]:csrrs a6, fcsr, zero
	-[0x80005d88]:sw t5, 1560(ra)
	-[0x80005d8c]:sw t6, 1568(ra)
	-[0x80005d90]:sw t5, 1576(ra)
Current Store : [0x80005d90] : sw t5, 1576(ra) -- Store: [0x800134c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005e00]:fmul.d t5, t3, s10, dyn
	-[0x80005e04]:csrrs a6, fcsr, zero
	-[0x80005e08]:sw t5, 1592(ra)
	-[0x80005e0c]:sw t6, 1600(ra)
Current Store : [0x80005e0c] : sw t6, 1600(ra) -- Store: [0x800134e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005e00]:fmul.d t5, t3, s10, dyn
	-[0x80005e04]:csrrs a6, fcsr, zero
	-[0x80005e08]:sw t5, 1592(ra)
	-[0x80005e0c]:sw t6, 1600(ra)
	-[0x80005e10]:sw t5, 1608(ra)
Current Store : [0x80005e10] : sw t5, 1608(ra) -- Store: [0x800134e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005e84]:fmul.d t5, t3, s10, dyn
	-[0x80005e88]:csrrs a6, fcsr, zero
	-[0x80005e8c]:sw t5, 1624(ra)
	-[0x80005e90]:sw t6, 1632(ra)
Current Store : [0x80005e90] : sw t6, 1632(ra) -- Store: [0x80013500]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005e84]:fmul.d t5, t3, s10, dyn
	-[0x80005e88]:csrrs a6, fcsr, zero
	-[0x80005e8c]:sw t5, 1624(ra)
	-[0x80005e90]:sw t6, 1632(ra)
	-[0x80005e94]:sw t5, 1640(ra)
Current Store : [0x80005e94] : sw t5, 1640(ra) -- Store: [0x80013508]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005f08]:fmul.d t5, t3, s10, dyn
	-[0x80005f0c]:csrrs a6, fcsr, zero
	-[0x80005f10]:sw t5, 1656(ra)
	-[0x80005f14]:sw t6, 1664(ra)
Current Store : [0x80005f14] : sw t6, 1664(ra) -- Store: [0x80013520]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005f08]:fmul.d t5, t3, s10, dyn
	-[0x80005f0c]:csrrs a6, fcsr, zero
	-[0x80005f10]:sw t5, 1656(ra)
	-[0x80005f14]:sw t6, 1664(ra)
	-[0x80005f18]:sw t5, 1672(ra)
Current Store : [0x80005f18] : sw t5, 1672(ra) -- Store: [0x80013528]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005f88]:fmul.d t5, t3, s10, dyn
	-[0x80005f8c]:csrrs a6, fcsr, zero
	-[0x80005f90]:sw t5, 1688(ra)
	-[0x80005f94]:sw t6, 1696(ra)
Current Store : [0x80005f94] : sw t6, 1696(ra) -- Store: [0x80013540]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005f88]:fmul.d t5, t3, s10, dyn
	-[0x80005f8c]:csrrs a6, fcsr, zero
	-[0x80005f90]:sw t5, 1688(ra)
	-[0x80005f94]:sw t6, 1696(ra)
	-[0x80005f98]:sw t5, 1704(ra)
Current Store : [0x80005f98] : sw t5, 1704(ra) -- Store: [0x80013548]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006008]:fmul.d t5, t3, s10, dyn
	-[0x8000600c]:csrrs a6, fcsr, zero
	-[0x80006010]:sw t5, 1720(ra)
	-[0x80006014]:sw t6, 1728(ra)
Current Store : [0x80006014] : sw t6, 1728(ra) -- Store: [0x80013560]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006008]:fmul.d t5, t3, s10, dyn
	-[0x8000600c]:csrrs a6, fcsr, zero
	-[0x80006010]:sw t5, 1720(ra)
	-[0x80006014]:sw t6, 1728(ra)
	-[0x80006018]:sw t5, 1736(ra)
Current Store : [0x80006018] : sw t5, 1736(ra) -- Store: [0x80013568]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006088]:fmul.d t5, t3, s10, dyn
	-[0x8000608c]:csrrs a6, fcsr, zero
	-[0x80006090]:sw t5, 1752(ra)
	-[0x80006094]:sw t6, 1760(ra)
Current Store : [0x80006094] : sw t6, 1760(ra) -- Store: [0x80013580]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006088]:fmul.d t5, t3, s10, dyn
	-[0x8000608c]:csrrs a6, fcsr, zero
	-[0x80006090]:sw t5, 1752(ra)
	-[0x80006094]:sw t6, 1760(ra)
	-[0x80006098]:sw t5, 1768(ra)
Current Store : [0x80006098] : sw t5, 1768(ra) -- Store: [0x80013588]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006108]:fmul.d t5, t3, s10, dyn
	-[0x8000610c]:csrrs a6, fcsr, zero
	-[0x80006110]:sw t5, 1784(ra)
	-[0x80006114]:sw t6, 1792(ra)
Current Store : [0x80006114] : sw t6, 1792(ra) -- Store: [0x800135a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006108]:fmul.d t5, t3, s10, dyn
	-[0x8000610c]:csrrs a6, fcsr, zero
	-[0x80006110]:sw t5, 1784(ra)
	-[0x80006114]:sw t6, 1792(ra)
	-[0x80006118]:sw t5, 1800(ra)
Current Store : [0x80006118] : sw t5, 1800(ra) -- Store: [0x800135a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006188]:fmul.d t5, t3, s10, dyn
	-[0x8000618c]:csrrs a6, fcsr, zero
	-[0x80006190]:sw t5, 1816(ra)
	-[0x80006194]:sw t6, 1824(ra)
Current Store : [0x80006194] : sw t6, 1824(ra) -- Store: [0x800135c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006188]:fmul.d t5, t3, s10, dyn
	-[0x8000618c]:csrrs a6, fcsr, zero
	-[0x80006190]:sw t5, 1816(ra)
	-[0x80006194]:sw t6, 1824(ra)
	-[0x80006198]:sw t5, 1832(ra)
Current Store : [0x80006198] : sw t5, 1832(ra) -- Store: [0x800135c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006208]:fmul.d t5, t3, s10, dyn
	-[0x8000620c]:csrrs a6, fcsr, zero
	-[0x80006210]:sw t5, 1848(ra)
	-[0x80006214]:sw t6, 1856(ra)
Current Store : [0x80006214] : sw t6, 1856(ra) -- Store: [0x800135e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006208]:fmul.d t5, t3, s10, dyn
	-[0x8000620c]:csrrs a6, fcsr, zero
	-[0x80006210]:sw t5, 1848(ra)
	-[0x80006214]:sw t6, 1856(ra)
	-[0x80006218]:sw t5, 1864(ra)
Current Store : [0x80006218] : sw t5, 1864(ra) -- Store: [0x800135e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006288]:fmul.d t5, t3, s10, dyn
	-[0x8000628c]:csrrs a6, fcsr, zero
	-[0x80006290]:sw t5, 1880(ra)
	-[0x80006294]:sw t6, 1888(ra)
Current Store : [0x80006294] : sw t6, 1888(ra) -- Store: [0x80013600]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006288]:fmul.d t5, t3, s10, dyn
	-[0x8000628c]:csrrs a6, fcsr, zero
	-[0x80006290]:sw t5, 1880(ra)
	-[0x80006294]:sw t6, 1888(ra)
	-[0x80006298]:sw t5, 1896(ra)
Current Store : [0x80006298] : sw t5, 1896(ra) -- Store: [0x80013608]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006308]:fmul.d t5, t3, s10, dyn
	-[0x8000630c]:csrrs a6, fcsr, zero
	-[0x80006310]:sw t5, 1912(ra)
	-[0x80006314]:sw t6, 1920(ra)
Current Store : [0x80006314] : sw t6, 1920(ra) -- Store: [0x80013620]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006308]:fmul.d t5, t3, s10, dyn
	-[0x8000630c]:csrrs a6, fcsr, zero
	-[0x80006310]:sw t5, 1912(ra)
	-[0x80006314]:sw t6, 1920(ra)
	-[0x80006318]:sw t5, 1928(ra)
Current Store : [0x80006318] : sw t5, 1928(ra) -- Store: [0x80013628]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006388]:fmul.d t5, t3, s10, dyn
	-[0x8000638c]:csrrs a6, fcsr, zero
	-[0x80006390]:sw t5, 1944(ra)
	-[0x80006394]:sw t6, 1952(ra)
Current Store : [0x80006394] : sw t6, 1952(ra) -- Store: [0x80013640]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006388]:fmul.d t5, t3, s10, dyn
	-[0x8000638c]:csrrs a6, fcsr, zero
	-[0x80006390]:sw t5, 1944(ra)
	-[0x80006394]:sw t6, 1952(ra)
	-[0x80006398]:sw t5, 1960(ra)
Current Store : [0x80006398] : sw t5, 1960(ra) -- Store: [0x80013648]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006408]:fmul.d t5, t3, s10, dyn
	-[0x8000640c]:csrrs a6, fcsr, zero
	-[0x80006410]:sw t5, 1976(ra)
	-[0x80006414]:sw t6, 1984(ra)
Current Store : [0x80006414] : sw t6, 1984(ra) -- Store: [0x80013660]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006408]:fmul.d t5, t3, s10, dyn
	-[0x8000640c]:csrrs a6, fcsr, zero
	-[0x80006410]:sw t5, 1976(ra)
	-[0x80006414]:sw t6, 1984(ra)
	-[0x80006418]:sw t5, 1992(ra)
Current Store : [0x80006418] : sw t5, 1992(ra) -- Store: [0x80013668]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006488]:fmul.d t5, t3, s10, dyn
	-[0x8000648c]:csrrs a6, fcsr, zero
	-[0x80006490]:sw t5, 2008(ra)
	-[0x80006494]:sw t6, 2016(ra)
Current Store : [0x80006494] : sw t6, 2016(ra) -- Store: [0x80013680]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006488]:fmul.d t5, t3, s10, dyn
	-[0x8000648c]:csrrs a6, fcsr, zero
	-[0x80006490]:sw t5, 2008(ra)
	-[0x80006494]:sw t6, 2016(ra)
	-[0x80006498]:sw t5, 2024(ra)
Current Store : [0x80006498] : sw t5, 2024(ra) -- Store: [0x80013688]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006508]:fmul.d t5, t3, s10, dyn
	-[0x8000650c]:csrrs a6, fcsr, zero
	-[0x80006510]:sw t5, 2040(ra)
	-[0x80006514]:addi ra, ra, 2040
	-[0x80006518]:sw t6, 8(ra)
Current Store : [0x80006518] : sw t6, 8(ra) -- Store: [0x800136a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006508]:fmul.d t5, t3, s10, dyn
	-[0x8000650c]:csrrs a6, fcsr, zero
	-[0x80006510]:sw t5, 2040(ra)
	-[0x80006514]:addi ra, ra, 2040
	-[0x80006518]:sw t6, 8(ra)
	-[0x8000651c]:sw t5, 16(ra)
Current Store : [0x8000651c] : sw t5, 16(ra) -- Store: [0x800136a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006554]:fmul.d t5, t3, s10, dyn
	-[0x80006558]:csrrs a6, fcsr, zero
	-[0x8000655c]:sw t5, 0(ra)
	-[0x80006560]:sw t6, 8(ra)
Current Store : [0x80006560] : sw t6, 8(ra) -- Store: [0x800126c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006554]:fmul.d t5, t3, s10, dyn
	-[0x80006558]:csrrs a6, fcsr, zero
	-[0x8000655c]:sw t5, 0(ra)
	-[0x80006560]:sw t6, 8(ra)
	-[0x80006564]:sw t5, 16(ra)
Current Store : [0x80006564] : sw t5, 16(ra) -- Store: [0x800126c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006594]:fmul.d t5, t3, s10, dyn
	-[0x80006598]:csrrs a6, fcsr, zero
	-[0x8000659c]:sw t5, 32(ra)
	-[0x800065a0]:sw t6, 40(ra)
Current Store : [0x800065a0] : sw t6, 40(ra) -- Store: [0x800126e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006594]:fmul.d t5, t3, s10, dyn
	-[0x80006598]:csrrs a6, fcsr, zero
	-[0x8000659c]:sw t5, 32(ra)
	-[0x800065a0]:sw t6, 40(ra)
	-[0x800065a4]:sw t5, 48(ra)
Current Store : [0x800065a4] : sw t5, 48(ra) -- Store: [0x800126e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800065d4]:fmul.d t5, t3, s10, dyn
	-[0x800065d8]:csrrs a6, fcsr, zero
	-[0x800065dc]:sw t5, 64(ra)
	-[0x800065e0]:sw t6, 72(ra)
Current Store : [0x800065e0] : sw t6, 72(ra) -- Store: [0x80012700]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800065d4]:fmul.d t5, t3, s10, dyn
	-[0x800065d8]:csrrs a6, fcsr, zero
	-[0x800065dc]:sw t5, 64(ra)
	-[0x800065e0]:sw t6, 72(ra)
	-[0x800065e4]:sw t5, 80(ra)
Current Store : [0x800065e4] : sw t5, 80(ra) -- Store: [0x80012708]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006614]:fmul.d t5, t3, s10, dyn
	-[0x80006618]:csrrs a6, fcsr, zero
	-[0x8000661c]:sw t5, 96(ra)
	-[0x80006620]:sw t6, 104(ra)
Current Store : [0x80006620] : sw t6, 104(ra) -- Store: [0x80012720]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006614]:fmul.d t5, t3, s10, dyn
	-[0x80006618]:csrrs a6, fcsr, zero
	-[0x8000661c]:sw t5, 96(ra)
	-[0x80006620]:sw t6, 104(ra)
	-[0x80006624]:sw t5, 112(ra)
Current Store : [0x80006624] : sw t5, 112(ra) -- Store: [0x80012728]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006658]:fmul.d t5, t3, s10, dyn
	-[0x8000665c]:csrrs a6, fcsr, zero
	-[0x80006660]:sw t5, 128(ra)
	-[0x80006664]:sw t6, 136(ra)
Current Store : [0x80006664] : sw t6, 136(ra) -- Store: [0x80012740]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006658]:fmul.d t5, t3, s10, dyn
	-[0x8000665c]:csrrs a6, fcsr, zero
	-[0x80006660]:sw t5, 128(ra)
	-[0x80006664]:sw t6, 136(ra)
	-[0x80006668]:sw t5, 144(ra)
Current Store : [0x80006668] : sw t5, 144(ra) -- Store: [0x80012748]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000669c]:fmul.d t5, t3, s10, dyn
	-[0x800066a0]:csrrs a6, fcsr, zero
	-[0x800066a4]:sw t5, 160(ra)
	-[0x800066a8]:sw t6, 168(ra)
Current Store : [0x800066a8] : sw t6, 168(ra) -- Store: [0x80012760]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000669c]:fmul.d t5, t3, s10, dyn
	-[0x800066a0]:csrrs a6, fcsr, zero
	-[0x800066a4]:sw t5, 160(ra)
	-[0x800066a8]:sw t6, 168(ra)
	-[0x800066ac]:sw t5, 176(ra)
Current Store : [0x800066ac] : sw t5, 176(ra) -- Store: [0x80012768]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800066dc]:fmul.d t5, t3, s10, dyn
	-[0x800066e0]:csrrs a6, fcsr, zero
	-[0x800066e4]:sw t5, 192(ra)
	-[0x800066e8]:sw t6, 200(ra)
Current Store : [0x800066e8] : sw t6, 200(ra) -- Store: [0x80012780]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800066dc]:fmul.d t5, t3, s10, dyn
	-[0x800066e0]:csrrs a6, fcsr, zero
	-[0x800066e4]:sw t5, 192(ra)
	-[0x800066e8]:sw t6, 200(ra)
	-[0x800066ec]:sw t5, 208(ra)
Current Store : [0x800066ec] : sw t5, 208(ra) -- Store: [0x80012788]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000671c]:fmul.d t5, t3, s10, dyn
	-[0x80006720]:csrrs a6, fcsr, zero
	-[0x80006724]:sw t5, 224(ra)
	-[0x80006728]:sw t6, 232(ra)
Current Store : [0x80006728] : sw t6, 232(ra) -- Store: [0x800127a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000671c]:fmul.d t5, t3, s10, dyn
	-[0x80006720]:csrrs a6, fcsr, zero
	-[0x80006724]:sw t5, 224(ra)
	-[0x80006728]:sw t6, 232(ra)
	-[0x8000672c]:sw t5, 240(ra)
Current Store : [0x8000672c] : sw t5, 240(ra) -- Store: [0x800127a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000675c]:fmul.d t5, t3, s10, dyn
	-[0x80006760]:csrrs a6, fcsr, zero
	-[0x80006764]:sw t5, 256(ra)
	-[0x80006768]:sw t6, 264(ra)
Current Store : [0x80006768] : sw t6, 264(ra) -- Store: [0x800127c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000675c]:fmul.d t5, t3, s10, dyn
	-[0x80006760]:csrrs a6, fcsr, zero
	-[0x80006764]:sw t5, 256(ra)
	-[0x80006768]:sw t6, 264(ra)
	-[0x8000676c]:sw t5, 272(ra)
Current Store : [0x8000676c] : sw t5, 272(ra) -- Store: [0x800127c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000679c]:fmul.d t5, t3, s10, dyn
	-[0x800067a0]:csrrs a6, fcsr, zero
	-[0x800067a4]:sw t5, 288(ra)
	-[0x800067a8]:sw t6, 296(ra)
Current Store : [0x800067a8] : sw t6, 296(ra) -- Store: [0x800127e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000679c]:fmul.d t5, t3, s10, dyn
	-[0x800067a0]:csrrs a6, fcsr, zero
	-[0x800067a4]:sw t5, 288(ra)
	-[0x800067a8]:sw t6, 296(ra)
	-[0x800067ac]:sw t5, 304(ra)
Current Store : [0x800067ac] : sw t5, 304(ra) -- Store: [0x800127e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800067e0]:fmul.d t5, t3, s10, dyn
	-[0x800067e4]:csrrs a6, fcsr, zero
	-[0x800067e8]:sw t5, 320(ra)
	-[0x800067ec]:sw t6, 328(ra)
Current Store : [0x800067ec] : sw t6, 328(ra) -- Store: [0x80012800]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800067e0]:fmul.d t5, t3, s10, dyn
	-[0x800067e4]:csrrs a6, fcsr, zero
	-[0x800067e8]:sw t5, 320(ra)
	-[0x800067ec]:sw t6, 328(ra)
	-[0x800067f0]:sw t5, 336(ra)
Current Store : [0x800067f0] : sw t5, 336(ra) -- Store: [0x80012808]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006824]:fmul.d t5, t3, s10, dyn
	-[0x80006828]:csrrs a6, fcsr, zero
	-[0x8000682c]:sw t5, 352(ra)
	-[0x80006830]:sw t6, 360(ra)
Current Store : [0x80006830] : sw t6, 360(ra) -- Store: [0x80012820]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006824]:fmul.d t5, t3, s10, dyn
	-[0x80006828]:csrrs a6, fcsr, zero
	-[0x8000682c]:sw t5, 352(ra)
	-[0x80006830]:sw t6, 360(ra)
	-[0x80006834]:sw t5, 368(ra)
Current Store : [0x80006834] : sw t5, 368(ra) -- Store: [0x80012828]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006864]:fmul.d t5, t3, s10, dyn
	-[0x80006868]:csrrs a6, fcsr, zero
	-[0x8000686c]:sw t5, 384(ra)
	-[0x80006870]:sw t6, 392(ra)
Current Store : [0x80006870] : sw t6, 392(ra) -- Store: [0x80012840]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006864]:fmul.d t5, t3, s10, dyn
	-[0x80006868]:csrrs a6, fcsr, zero
	-[0x8000686c]:sw t5, 384(ra)
	-[0x80006870]:sw t6, 392(ra)
	-[0x80006874]:sw t5, 400(ra)
Current Store : [0x80006874] : sw t5, 400(ra) -- Store: [0x80012848]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800068a4]:fmul.d t5, t3, s10, dyn
	-[0x800068a8]:csrrs a6, fcsr, zero
	-[0x800068ac]:sw t5, 416(ra)
	-[0x800068b0]:sw t6, 424(ra)
Current Store : [0x800068b0] : sw t6, 424(ra) -- Store: [0x80012860]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800068a4]:fmul.d t5, t3, s10, dyn
	-[0x800068a8]:csrrs a6, fcsr, zero
	-[0x800068ac]:sw t5, 416(ra)
	-[0x800068b0]:sw t6, 424(ra)
	-[0x800068b4]:sw t5, 432(ra)
Current Store : [0x800068b4] : sw t5, 432(ra) -- Store: [0x80012868]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800068e4]:fmul.d t5, t3, s10, dyn
	-[0x800068e8]:csrrs a6, fcsr, zero
	-[0x800068ec]:sw t5, 448(ra)
	-[0x800068f0]:sw t6, 456(ra)
Current Store : [0x800068f0] : sw t6, 456(ra) -- Store: [0x80012880]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800068e4]:fmul.d t5, t3, s10, dyn
	-[0x800068e8]:csrrs a6, fcsr, zero
	-[0x800068ec]:sw t5, 448(ra)
	-[0x800068f0]:sw t6, 456(ra)
	-[0x800068f4]:sw t5, 464(ra)
Current Store : [0x800068f4] : sw t5, 464(ra) -- Store: [0x80012888]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006924]:fmul.d t5, t3, s10, dyn
	-[0x80006928]:csrrs a6, fcsr, zero
	-[0x8000692c]:sw t5, 480(ra)
	-[0x80006930]:sw t6, 488(ra)
Current Store : [0x80006930] : sw t6, 488(ra) -- Store: [0x800128a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006924]:fmul.d t5, t3, s10, dyn
	-[0x80006928]:csrrs a6, fcsr, zero
	-[0x8000692c]:sw t5, 480(ra)
	-[0x80006930]:sw t6, 488(ra)
	-[0x80006934]:sw t5, 496(ra)
Current Store : [0x80006934] : sw t5, 496(ra) -- Store: [0x800128a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006964]:fmul.d t5, t3, s10, dyn
	-[0x80006968]:csrrs a6, fcsr, zero
	-[0x8000696c]:sw t5, 512(ra)
	-[0x80006970]:sw t6, 520(ra)
Current Store : [0x80006970] : sw t6, 520(ra) -- Store: [0x800128c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006964]:fmul.d t5, t3, s10, dyn
	-[0x80006968]:csrrs a6, fcsr, zero
	-[0x8000696c]:sw t5, 512(ra)
	-[0x80006970]:sw t6, 520(ra)
	-[0x80006974]:sw t5, 528(ra)
Current Store : [0x80006974] : sw t5, 528(ra) -- Store: [0x800128c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800069a4]:fmul.d t5, t3, s10, dyn
	-[0x800069a8]:csrrs a6, fcsr, zero
	-[0x800069ac]:sw t5, 544(ra)
	-[0x800069b0]:sw t6, 552(ra)
Current Store : [0x800069b0] : sw t6, 552(ra) -- Store: [0x800128e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800069a4]:fmul.d t5, t3, s10, dyn
	-[0x800069a8]:csrrs a6, fcsr, zero
	-[0x800069ac]:sw t5, 544(ra)
	-[0x800069b0]:sw t6, 552(ra)
	-[0x800069b4]:sw t5, 560(ra)
Current Store : [0x800069b4] : sw t5, 560(ra) -- Store: [0x800128e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800069e4]:fmul.d t5, t3, s10, dyn
	-[0x800069e8]:csrrs a6, fcsr, zero
	-[0x800069ec]:sw t5, 576(ra)
	-[0x800069f0]:sw t6, 584(ra)
Current Store : [0x800069f0] : sw t6, 584(ra) -- Store: [0x80012900]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800069e4]:fmul.d t5, t3, s10, dyn
	-[0x800069e8]:csrrs a6, fcsr, zero
	-[0x800069ec]:sw t5, 576(ra)
	-[0x800069f0]:sw t6, 584(ra)
	-[0x800069f4]:sw t5, 592(ra)
Current Store : [0x800069f4] : sw t5, 592(ra) -- Store: [0x80012908]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006a24]:fmul.d t5, t3, s10, dyn
	-[0x80006a28]:csrrs a6, fcsr, zero
	-[0x80006a2c]:sw t5, 608(ra)
	-[0x80006a30]:sw t6, 616(ra)
Current Store : [0x80006a30] : sw t6, 616(ra) -- Store: [0x80012920]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006a24]:fmul.d t5, t3, s10, dyn
	-[0x80006a28]:csrrs a6, fcsr, zero
	-[0x80006a2c]:sw t5, 608(ra)
	-[0x80006a30]:sw t6, 616(ra)
	-[0x80006a34]:sw t5, 624(ra)
Current Store : [0x80006a34] : sw t5, 624(ra) -- Store: [0x80012928]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006a64]:fmul.d t5, t3, s10, dyn
	-[0x80006a68]:csrrs a6, fcsr, zero
	-[0x80006a6c]:sw t5, 640(ra)
	-[0x80006a70]:sw t6, 648(ra)
Current Store : [0x80006a70] : sw t6, 648(ra) -- Store: [0x80012940]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006a64]:fmul.d t5, t3, s10, dyn
	-[0x80006a68]:csrrs a6, fcsr, zero
	-[0x80006a6c]:sw t5, 640(ra)
	-[0x80006a70]:sw t6, 648(ra)
	-[0x80006a74]:sw t5, 656(ra)
Current Store : [0x80006a74] : sw t5, 656(ra) -- Store: [0x80012948]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006aa4]:fmul.d t5, t3, s10, dyn
	-[0x80006aa8]:csrrs a6, fcsr, zero
	-[0x80006aac]:sw t5, 672(ra)
	-[0x80006ab0]:sw t6, 680(ra)
Current Store : [0x80006ab0] : sw t6, 680(ra) -- Store: [0x80012960]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006aa4]:fmul.d t5, t3, s10, dyn
	-[0x80006aa8]:csrrs a6, fcsr, zero
	-[0x80006aac]:sw t5, 672(ra)
	-[0x80006ab0]:sw t6, 680(ra)
	-[0x80006ab4]:sw t5, 688(ra)
Current Store : [0x80006ab4] : sw t5, 688(ra) -- Store: [0x80012968]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006ae8]:fmul.d t5, t3, s10, dyn
	-[0x80006aec]:csrrs a6, fcsr, zero
	-[0x80006af0]:sw t5, 704(ra)
	-[0x80006af4]:sw t6, 712(ra)
Current Store : [0x80006af4] : sw t6, 712(ra) -- Store: [0x80012980]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006ae8]:fmul.d t5, t3, s10, dyn
	-[0x80006aec]:csrrs a6, fcsr, zero
	-[0x80006af0]:sw t5, 704(ra)
	-[0x80006af4]:sw t6, 712(ra)
	-[0x80006af8]:sw t5, 720(ra)
Current Store : [0x80006af8] : sw t5, 720(ra) -- Store: [0x80012988]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006b2c]:fmul.d t5, t3, s10, dyn
	-[0x80006b30]:csrrs a6, fcsr, zero
	-[0x80006b34]:sw t5, 736(ra)
	-[0x80006b38]:sw t6, 744(ra)
Current Store : [0x80006b38] : sw t6, 744(ra) -- Store: [0x800129a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006b2c]:fmul.d t5, t3, s10, dyn
	-[0x80006b30]:csrrs a6, fcsr, zero
	-[0x80006b34]:sw t5, 736(ra)
	-[0x80006b38]:sw t6, 744(ra)
	-[0x80006b3c]:sw t5, 752(ra)
Current Store : [0x80006b3c] : sw t5, 752(ra) -- Store: [0x800129a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006b70]:fmul.d t5, t3, s10, dyn
	-[0x80006b74]:csrrs a6, fcsr, zero
	-[0x80006b78]:sw t5, 768(ra)
	-[0x80006b7c]:sw t6, 776(ra)
Current Store : [0x80006b7c] : sw t6, 776(ra) -- Store: [0x800129c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006b70]:fmul.d t5, t3, s10, dyn
	-[0x80006b74]:csrrs a6, fcsr, zero
	-[0x80006b78]:sw t5, 768(ra)
	-[0x80006b7c]:sw t6, 776(ra)
	-[0x80006b80]:sw t5, 784(ra)
Current Store : [0x80006b80] : sw t5, 784(ra) -- Store: [0x800129c8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006bb4]:fmul.d t5, t3, s10, dyn
	-[0x80006bb8]:csrrs a6, fcsr, zero
	-[0x80006bbc]:sw t5, 800(ra)
	-[0x80006bc0]:sw t6, 808(ra)
Current Store : [0x80006bc0] : sw t6, 808(ra) -- Store: [0x800129e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006bb4]:fmul.d t5, t3, s10, dyn
	-[0x80006bb8]:csrrs a6, fcsr, zero
	-[0x80006bbc]:sw t5, 800(ra)
	-[0x80006bc0]:sw t6, 808(ra)
	-[0x80006bc4]:sw t5, 816(ra)
Current Store : [0x80006bc4] : sw t5, 816(ra) -- Store: [0x800129e8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006bf8]:fmul.d t5, t3, s10, dyn
	-[0x80006bfc]:csrrs a6, fcsr, zero
	-[0x80006c00]:sw t5, 832(ra)
	-[0x80006c04]:sw t6, 840(ra)
Current Store : [0x80006c04] : sw t6, 840(ra) -- Store: [0x80012a00]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006bf8]:fmul.d t5, t3, s10, dyn
	-[0x80006bfc]:csrrs a6, fcsr, zero
	-[0x80006c00]:sw t5, 832(ra)
	-[0x80006c04]:sw t6, 840(ra)
	-[0x80006c08]:sw t5, 848(ra)
Current Store : [0x80006c08] : sw t5, 848(ra) -- Store: [0x80012a08]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006c3c]:fmul.d t5, t3, s10, dyn
	-[0x80006c40]:csrrs a6, fcsr, zero
	-[0x80006c44]:sw t5, 864(ra)
	-[0x80006c48]:sw t6, 872(ra)
Current Store : [0x80006c48] : sw t6, 872(ra) -- Store: [0x80012a20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006c3c]:fmul.d t5, t3, s10, dyn
	-[0x80006c40]:csrrs a6, fcsr, zero
	-[0x80006c44]:sw t5, 864(ra)
	-[0x80006c48]:sw t6, 872(ra)
	-[0x80006c4c]:sw t5, 880(ra)
Current Store : [0x80006c4c] : sw t5, 880(ra) -- Store: [0x80012a28]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006c84]:fmul.d t5, t3, s10, dyn
	-[0x80006c88]:csrrs a6, fcsr, zero
	-[0x80006c8c]:sw t5, 896(ra)
	-[0x80006c90]:sw t6, 904(ra)
Current Store : [0x80006c90] : sw t6, 904(ra) -- Store: [0x80012a40]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006c84]:fmul.d t5, t3, s10, dyn
	-[0x80006c88]:csrrs a6, fcsr, zero
	-[0x80006c8c]:sw t5, 896(ra)
	-[0x80006c90]:sw t6, 904(ra)
	-[0x80006c94]:sw t5, 912(ra)
Current Store : [0x80006c94] : sw t5, 912(ra) -- Store: [0x80012a48]:0xFFFFFFFD




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006ccc]:fmul.d t5, t3, s10, dyn
	-[0x80006cd0]:csrrs a6, fcsr, zero
	-[0x80006cd4]:sw t5, 928(ra)
	-[0x80006cd8]:sw t6, 936(ra)
Current Store : [0x80006cd8] : sw t6, 936(ra) -- Store: [0x80012a60]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006ccc]:fmul.d t5, t3, s10, dyn
	-[0x80006cd0]:csrrs a6, fcsr, zero
	-[0x80006cd4]:sw t5, 928(ra)
	-[0x80006cd8]:sw t6, 936(ra)
	-[0x80006cdc]:sw t5, 944(ra)
Current Store : [0x80006cdc] : sw t5, 944(ra) -- Store: [0x80012a68]:0xFFFFFFFD




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006d10]:fmul.d t5, t3, s10, dyn
	-[0x80006d14]:csrrs a6, fcsr, zero
	-[0x80006d18]:sw t5, 960(ra)
	-[0x80006d1c]:sw t6, 968(ra)
Current Store : [0x80006d1c] : sw t6, 968(ra) -- Store: [0x80012a80]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006d10]:fmul.d t5, t3, s10, dyn
	-[0x80006d14]:csrrs a6, fcsr, zero
	-[0x80006d18]:sw t5, 960(ra)
	-[0x80006d1c]:sw t6, 968(ra)
	-[0x80006d20]:sw t5, 976(ra)
Current Store : [0x80006d20] : sw t5, 976(ra) -- Store: [0x80012a88]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006d54]:fmul.d t5, t3, s10, dyn
	-[0x80006d58]:csrrs a6, fcsr, zero
	-[0x80006d5c]:sw t5, 992(ra)
	-[0x80006d60]:sw t6, 1000(ra)
Current Store : [0x80006d60] : sw t6, 1000(ra) -- Store: [0x80012aa0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006d54]:fmul.d t5, t3, s10, dyn
	-[0x80006d58]:csrrs a6, fcsr, zero
	-[0x80006d5c]:sw t5, 992(ra)
	-[0x80006d60]:sw t6, 1000(ra)
	-[0x80006d64]:sw t5, 1008(ra)
Current Store : [0x80006d64] : sw t5, 1008(ra) -- Store: [0x80012aa8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006d98]:fmul.d t5, t3, s10, dyn
	-[0x80006d9c]:csrrs a6, fcsr, zero
	-[0x80006da0]:sw t5, 1024(ra)
	-[0x80006da4]:sw t6, 1032(ra)
Current Store : [0x80006da4] : sw t6, 1032(ra) -- Store: [0x80012ac0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006d98]:fmul.d t5, t3, s10, dyn
	-[0x80006d9c]:csrrs a6, fcsr, zero
	-[0x80006da0]:sw t5, 1024(ra)
	-[0x80006da4]:sw t6, 1032(ra)
	-[0x80006da8]:sw t5, 1040(ra)
Current Store : [0x80006da8] : sw t5, 1040(ra) -- Store: [0x80012ac8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006ddc]:fmul.d t5, t3, s10, dyn
	-[0x80006de0]:csrrs a6, fcsr, zero
	-[0x80006de4]:sw t5, 1056(ra)
	-[0x80006de8]:sw t6, 1064(ra)
Current Store : [0x80006de8] : sw t6, 1064(ra) -- Store: [0x80012ae0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006ddc]:fmul.d t5, t3, s10, dyn
	-[0x80006de0]:csrrs a6, fcsr, zero
	-[0x80006de4]:sw t5, 1056(ra)
	-[0x80006de8]:sw t6, 1064(ra)
	-[0x80006dec]:sw t5, 1072(ra)
Current Store : [0x80006dec] : sw t5, 1072(ra) -- Store: [0x80012ae8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006e24]:fmul.d t5, t3, s10, dyn
	-[0x80006e28]:csrrs a6, fcsr, zero
	-[0x80006e2c]:sw t5, 1088(ra)
	-[0x80006e30]:sw t6, 1096(ra)
Current Store : [0x80006e30] : sw t6, 1096(ra) -- Store: [0x80012b00]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006e24]:fmul.d t5, t3, s10, dyn
	-[0x80006e28]:csrrs a6, fcsr, zero
	-[0x80006e2c]:sw t5, 1088(ra)
	-[0x80006e30]:sw t6, 1096(ra)
	-[0x80006e34]:sw t5, 1104(ra)
Current Store : [0x80006e34] : sw t5, 1104(ra) -- Store: [0x80012b08]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006e6c]:fmul.d t5, t3, s10, dyn
	-[0x80006e70]:csrrs a6, fcsr, zero
	-[0x80006e74]:sw t5, 1120(ra)
	-[0x80006e78]:sw t6, 1128(ra)
Current Store : [0x80006e78] : sw t6, 1128(ra) -- Store: [0x80012b20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006e6c]:fmul.d t5, t3, s10, dyn
	-[0x80006e70]:csrrs a6, fcsr, zero
	-[0x80006e74]:sw t5, 1120(ra)
	-[0x80006e78]:sw t6, 1128(ra)
	-[0x80006e7c]:sw t5, 1136(ra)
Current Store : [0x80006e7c] : sw t5, 1136(ra) -- Store: [0x80012b28]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006eb0]:fmul.d t5, t3, s10, dyn
	-[0x80006eb4]:csrrs a6, fcsr, zero
	-[0x80006eb8]:sw t5, 1152(ra)
	-[0x80006ebc]:sw t6, 1160(ra)
Current Store : [0x80006ebc] : sw t6, 1160(ra) -- Store: [0x80012b40]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006eb0]:fmul.d t5, t3, s10, dyn
	-[0x80006eb4]:csrrs a6, fcsr, zero
	-[0x80006eb8]:sw t5, 1152(ra)
	-[0x80006ebc]:sw t6, 1160(ra)
	-[0x80006ec0]:sw t5, 1168(ra)
Current Store : [0x80006ec0] : sw t5, 1168(ra) -- Store: [0x80012b48]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006ef4]:fmul.d t5, t3, s10, dyn
	-[0x80006ef8]:csrrs a6, fcsr, zero
	-[0x80006efc]:sw t5, 1184(ra)
	-[0x80006f00]:sw t6, 1192(ra)
Current Store : [0x80006f00] : sw t6, 1192(ra) -- Store: [0x80012b60]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006ef4]:fmul.d t5, t3, s10, dyn
	-[0x80006ef8]:csrrs a6, fcsr, zero
	-[0x80006efc]:sw t5, 1184(ra)
	-[0x80006f00]:sw t6, 1192(ra)
	-[0x80006f04]:sw t5, 1200(ra)
Current Store : [0x80006f04] : sw t5, 1200(ra) -- Store: [0x80012b68]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006f38]:fmul.d t5, t3, s10, dyn
	-[0x80006f3c]:csrrs a6, fcsr, zero
	-[0x80006f40]:sw t5, 1216(ra)
	-[0x80006f44]:sw t6, 1224(ra)
Current Store : [0x80006f44] : sw t6, 1224(ra) -- Store: [0x80012b80]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006f38]:fmul.d t5, t3, s10, dyn
	-[0x80006f3c]:csrrs a6, fcsr, zero
	-[0x80006f40]:sw t5, 1216(ra)
	-[0x80006f44]:sw t6, 1224(ra)
	-[0x80006f48]:sw t5, 1232(ra)
Current Store : [0x80006f48] : sw t5, 1232(ra) -- Store: [0x80012b88]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006f7c]:fmul.d t5, t3, s10, dyn
	-[0x80006f80]:csrrs a6, fcsr, zero
	-[0x80006f84]:sw t5, 1248(ra)
	-[0x80006f88]:sw t6, 1256(ra)
Current Store : [0x80006f88] : sw t6, 1256(ra) -- Store: [0x80012ba0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006f7c]:fmul.d t5, t3, s10, dyn
	-[0x80006f80]:csrrs a6, fcsr, zero
	-[0x80006f84]:sw t5, 1248(ra)
	-[0x80006f88]:sw t6, 1256(ra)
	-[0x80006f8c]:sw t5, 1264(ra)
Current Store : [0x80006f8c] : sw t5, 1264(ra) -- Store: [0x80012ba8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006fc0]:fmul.d t5, t3, s10, dyn
	-[0x80006fc4]:csrrs a6, fcsr, zero
	-[0x80006fc8]:sw t5, 1280(ra)
	-[0x80006fcc]:sw t6, 1288(ra)
Current Store : [0x80006fcc] : sw t6, 1288(ra) -- Store: [0x80012bc0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006fc0]:fmul.d t5, t3, s10, dyn
	-[0x80006fc4]:csrrs a6, fcsr, zero
	-[0x80006fc8]:sw t5, 1280(ra)
	-[0x80006fcc]:sw t6, 1288(ra)
	-[0x80006fd0]:sw t5, 1296(ra)
Current Store : [0x80006fd0] : sw t5, 1296(ra) -- Store: [0x80012bc8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007004]:fmul.d t5, t3, s10, dyn
	-[0x80007008]:csrrs a6, fcsr, zero
	-[0x8000700c]:sw t5, 1312(ra)
	-[0x80007010]:sw t6, 1320(ra)
Current Store : [0x80007010] : sw t6, 1320(ra) -- Store: [0x80012be0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007004]:fmul.d t5, t3, s10, dyn
	-[0x80007008]:csrrs a6, fcsr, zero
	-[0x8000700c]:sw t5, 1312(ra)
	-[0x80007010]:sw t6, 1320(ra)
	-[0x80007014]:sw t5, 1328(ra)
Current Store : [0x80007014] : sw t5, 1328(ra) -- Store: [0x80012be8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007048]:fmul.d t5, t3, s10, dyn
	-[0x8000704c]:csrrs a6, fcsr, zero
	-[0x80007050]:sw t5, 1344(ra)
	-[0x80007054]:sw t6, 1352(ra)
Current Store : [0x80007054] : sw t6, 1352(ra) -- Store: [0x80012c00]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007048]:fmul.d t5, t3, s10, dyn
	-[0x8000704c]:csrrs a6, fcsr, zero
	-[0x80007050]:sw t5, 1344(ra)
	-[0x80007054]:sw t6, 1352(ra)
	-[0x80007058]:sw t5, 1360(ra)
Current Store : [0x80007058] : sw t5, 1360(ra) -- Store: [0x80012c08]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000708c]:fmul.d t5, t3, s10, dyn
	-[0x80007090]:csrrs a6, fcsr, zero
	-[0x80007094]:sw t5, 1376(ra)
	-[0x80007098]:sw t6, 1384(ra)
Current Store : [0x80007098] : sw t6, 1384(ra) -- Store: [0x80012c20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000708c]:fmul.d t5, t3, s10, dyn
	-[0x80007090]:csrrs a6, fcsr, zero
	-[0x80007094]:sw t5, 1376(ra)
	-[0x80007098]:sw t6, 1384(ra)
	-[0x8000709c]:sw t5, 1392(ra)
Current Store : [0x8000709c] : sw t5, 1392(ra) -- Store: [0x80012c28]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800070d0]:fmul.d t5, t3, s10, dyn
	-[0x800070d4]:csrrs a6, fcsr, zero
	-[0x800070d8]:sw t5, 1408(ra)
	-[0x800070dc]:sw t6, 1416(ra)
Current Store : [0x800070dc] : sw t6, 1416(ra) -- Store: [0x80012c40]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800070d0]:fmul.d t5, t3, s10, dyn
	-[0x800070d4]:csrrs a6, fcsr, zero
	-[0x800070d8]:sw t5, 1408(ra)
	-[0x800070dc]:sw t6, 1416(ra)
	-[0x800070e0]:sw t5, 1424(ra)
Current Store : [0x800070e0] : sw t5, 1424(ra) -- Store: [0x80012c48]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007114]:fmul.d t5, t3, s10, dyn
	-[0x80007118]:csrrs a6, fcsr, zero
	-[0x8000711c]:sw t5, 1440(ra)
	-[0x80007120]:sw t6, 1448(ra)
Current Store : [0x80007120] : sw t6, 1448(ra) -- Store: [0x80012c60]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007114]:fmul.d t5, t3, s10, dyn
	-[0x80007118]:csrrs a6, fcsr, zero
	-[0x8000711c]:sw t5, 1440(ra)
	-[0x80007120]:sw t6, 1448(ra)
	-[0x80007124]:sw t5, 1456(ra)
Current Store : [0x80007124] : sw t5, 1456(ra) -- Store: [0x80012c68]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007158]:fmul.d t5, t3, s10, dyn
	-[0x8000715c]:csrrs a6, fcsr, zero
	-[0x80007160]:sw t5, 1472(ra)
	-[0x80007164]:sw t6, 1480(ra)
Current Store : [0x80007164] : sw t6, 1480(ra) -- Store: [0x80012c80]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007158]:fmul.d t5, t3, s10, dyn
	-[0x8000715c]:csrrs a6, fcsr, zero
	-[0x80007160]:sw t5, 1472(ra)
	-[0x80007164]:sw t6, 1480(ra)
	-[0x80007168]:sw t5, 1488(ra)
Current Store : [0x80007168] : sw t5, 1488(ra) -- Store: [0x80012c88]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000719c]:fmul.d t5, t3, s10, dyn
	-[0x800071a0]:csrrs a6, fcsr, zero
	-[0x800071a4]:sw t5, 1504(ra)
	-[0x800071a8]:sw t6, 1512(ra)
Current Store : [0x800071a8] : sw t6, 1512(ra) -- Store: [0x80012ca0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000719c]:fmul.d t5, t3, s10, dyn
	-[0x800071a0]:csrrs a6, fcsr, zero
	-[0x800071a4]:sw t5, 1504(ra)
	-[0x800071a8]:sw t6, 1512(ra)
	-[0x800071ac]:sw t5, 1520(ra)
Current Store : [0x800071ac] : sw t5, 1520(ra) -- Store: [0x80012ca8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800071e0]:fmul.d t5, t3, s10, dyn
	-[0x800071e4]:csrrs a6, fcsr, zero
	-[0x800071e8]:sw t5, 1536(ra)
	-[0x800071ec]:sw t6, 1544(ra)
Current Store : [0x800071ec] : sw t6, 1544(ra) -- Store: [0x80012cc0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800071e0]:fmul.d t5, t3, s10, dyn
	-[0x800071e4]:csrrs a6, fcsr, zero
	-[0x800071e8]:sw t5, 1536(ra)
	-[0x800071ec]:sw t6, 1544(ra)
	-[0x800071f0]:sw t5, 1552(ra)
Current Store : [0x800071f0] : sw t5, 1552(ra) -- Store: [0x80012cc8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007224]:fmul.d t5, t3, s10, dyn
	-[0x80007228]:csrrs a6, fcsr, zero
	-[0x8000722c]:sw t5, 1568(ra)
	-[0x80007230]:sw t6, 1576(ra)
Current Store : [0x80007230] : sw t6, 1576(ra) -- Store: [0x80012ce0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007224]:fmul.d t5, t3, s10, dyn
	-[0x80007228]:csrrs a6, fcsr, zero
	-[0x8000722c]:sw t5, 1568(ra)
	-[0x80007230]:sw t6, 1576(ra)
	-[0x80007234]:sw t5, 1584(ra)
Current Store : [0x80007234] : sw t5, 1584(ra) -- Store: [0x80012ce8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007268]:fmul.d t5, t3, s10, dyn
	-[0x8000726c]:csrrs a6, fcsr, zero
	-[0x80007270]:sw t5, 1600(ra)
	-[0x80007274]:sw t6, 1608(ra)
Current Store : [0x80007274] : sw t6, 1608(ra) -- Store: [0x80012d00]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007268]:fmul.d t5, t3, s10, dyn
	-[0x8000726c]:csrrs a6, fcsr, zero
	-[0x80007270]:sw t5, 1600(ra)
	-[0x80007274]:sw t6, 1608(ra)
	-[0x80007278]:sw t5, 1616(ra)
Current Store : [0x80007278] : sw t5, 1616(ra) -- Store: [0x80012d08]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800072ac]:fmul.d t5, t3, s10, dyn
	-[0x800072b0]:csrrs a6, fcsr, zero
	-[0x800072b4]:sw t5, 1632(ra)
	-[0x800072b8]:sw t6, 1640(ra)
Current Store : [0x800072b8] : sw t6, 1640(ra) -- Store: [0x80012d20]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800072ac]:fmul.d t5, t3, s10, dyn
	-[0x800072b0]:csrrs a6, fcsr, zero
	-[0x800072b4]:sw t5, 1632(ra)
	-[0x800072b8]:sw t6, 1640(ra)
	-[0x800072bc]:sw t5, 1648(ra)
Current Store : [0x800072bc] : sw t5, 1648(ra) -- Store: [0x80012d28]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800072f4]:fmul.d t5, t3, s10, dyn
	-[0x800072f8]:csrrs a6, fcsr, zero
	-[0x800072fc]:sw t5, 1664(ra)
	-[0x80007300]:sw t6, 1672(ra)
Current Store : [0x80007300] : sw t6, 1672(ra) -- Store: [0x80012d40]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800072f4]:fmul.d t5, t3, s10, dyn
	-[0x800072f8]:csrrs a6, fcsr, zero
	-[0x800072fc]:sw t5, 1664(ra)
	-[0x80007300]:sw t6, 1672(ra)
	-[0x80007304]:sw t5, 1680(ra)
Current Store : [0x80007304] : sw t5, 1680(ra) -- Store: [0x80012d48]:0xFFFFFFFD




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000733c]:fmul.d t5, t3, s10, dyn
	-[0x80007340]:csrrs a6, fcsr, zero
	-[0x80007344]:sw t5, 1696(ra)
	-[0x80007348]:sw t6, 1704(ra)
Current Store : [0x80007348] : sw t6, 1704(ra) -- Store: [0x80012d60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000733c]:fmul.d t5, t3, s10, dyn
	-[0x80007340]:csrrs a6, fcsr, zero
	-[0x80007344]:sw t5, 1696(ra)
	-[0x80007348]:sw t6, 1704(ra)
	-[0x8000734c]:sw t5, 1712(ra)
Current Store : [0x8000734c] : sw t5, 1712(ra) -- Store: [0x80012d68]:0xFFFFFFFD




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007380]:fmul.d t5, t3, s10, dyn
	-[0x80007384]:csrrs a6, fcsr, zero
	-[0x80007388]:sw t5, 1728(ra)
	-[0x8000738c]:sw t6, 1736(ra)
Current Store : [0x8000738c] : sw t6, 1736(ra) -- Store: [0x80012d80]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007380]:fmul.d t5, t3, s10, dyn
	-[0x80007384]:csrrs a6, fcsr, zero
	-[0x80007388]:sw t5, 1728(ra)
	-[0x8000738c]:sw t6, 1736(ra)
	-[0x80007390]:sw t5, 1744(ra)
Current Store : [0x80007390] : sw t5, 1744(ra) -- Store: [0x80012d88]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800073c4]:fmul.d t5, t3, s10, dyn
	-[0x800073c8]:csrrs a6, fcsr, zero
	-[0x800073cc]:sw t5, 1760(ra)
	-[0x800073d0]:sw t6, 1768(ra)
Current Store : [0x800073d0] : sw t6, 1768(ra) -- Store: [0x80012da0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800073c4]:fmul.d t5, t3, s10, dyn
	-[0x800073c8]:csrrs a6, fcsr, zero
	-[0x800073cc]:sw t5, 1760(ra)
	-[0x800073d0]:sw t6, 1768(ra)
	-[0x800073d4]:sw t5, 1776(ra)
Current Store : [0x800073d4] : sw t5, 1776(ra) -- Store: [0x80012da8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007408]:fmul.d t5, t3, s10, dyn
	-[0x8000740c]:csrrs a6, fcsr, zero
	-[0x80007410]:sw t5, 1792(ra)
	-[0x80007414]:sw t6, 1800(ra)
Current Store : [0x80007414] : sw t6, 1800(ra) -- Store: [0x80012dc0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007408]:fmul.d t5, t3, s10, dyn
	-[0x8000740c]:csrrs a6, fcsr, zero
	-[0x80007410]:sw t5, 1792(ra)
	-[0x80007414]:sw t6, 1800(ra)
	-[0x80007418]:sw t5, 1808(ra)
Current Store : [0x80007418] : sw t5, 1808(ra) -- Store: [0x80012dc8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000744c]:fmul.d t5, t3, s10, dyn
	-[0x80007450]:csrrs a6, fcsr, zero
	-[0x80007454]:sw t5, 1824(ra)
	-[0x80007458]:sw t6, 1832(ra)
Current Store : [0x80007458] : sw t6, 1832(ra) -- Store: [0x80012de0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000744c]:fmul.d t5, t3, s10, dyn
	-[0x80007450]:csrrs a6, fcsr, zero
	-[0x80007454]:sw t5, 1824(ra)
	-[0x80007458]:sw t6, 1832(ra)
	-[0x8000745c]:sw t5, 1840(ra)
Current Store : [0x8000745c] : sw t5, 1840(ra) -- Store: [0x80012de8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007494]:fmul.d t5, t3, s10, dyn
	-[0x80007498]:csrrs a6, fcsr, zero
	-[0x8000749c]:sw t5, 1856(ra)
	-[0x800074a0]:sw t6, 1864(ra)
Current Store : [0x800074a0] : sw t6, 1864(ra) -- Store: [0x80012e00]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007494]:fmul.d t5, t3, s10, dyn
	-[0x80007498]:csrrs a6, fcsr, zero
	-[0x8000749c]:sw t5, 1856(ra)
	-[0x800074a0]:sw t6, 1864(ra)
	-[0x800074a4]:sw t5, 1872(ra)
Current Store : [0x800074a4] : sw t5, 1872(ra) -- Store: [0x80012e08]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800074dc]:fmul.d t5, t3, s10, dyn
	-[0x800074e0]:csrrs a6, fcsr, zero
	-[0x800074e4]:sw t5, 1888(ra)
	-[0x800074e8]:sw t6, 1896(ra)
Current Store : [0x800074e8] : sw t6, 1896(ra) -- Store: [0x80012e20]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800074dc]:fmul.d t5, t3, s10, dyn
	-[0x800074e0]:csrrs a6, fcsr, zero
	-[0x800074e4]:sw t5, 1888(ra)
	-[0x800074e8]:sw t6, 1896(ra)
	-[0x800074ec]:sw t5, 1904(ra)
Current Store : [0x800074ec] : sw t5, 1904(ra) -- Store: [0x80012e28]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007520]:fmul.d t5, t3, s10, dyn
	-[0x80007524]:csrrs a6, fcsr, zero
	-[0x80007528]:sw t5, 1920(ra)
	-[0x8000752c]:sw t6, 1928(ra)
Current Store : [0x8000752c] : sw t6, 1928(ra) -- Store: [0x80012e40]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007520]:fmul.d t5, t3, s10, dyn
	-[0x80007524]:csrrs a6, fcsr, zero
	-[0x80007528]:sw t5, 1920(ra)
	-[0x8000752c]:sw t6, 1928(ra)
	-[0x80007530]:sw t5, 1936(ra)
Current Store : [0x80007530] : sw t5, 1936(ra) -- Store: [0x80012e48]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007564]:fmul.d t5, t3, s10, dyn
	-[0x80007568]:csrrs a6, fcsr, zero
	-[0x8000756c]:sw t5, 1952(ra)
	-[0x80007570]:sw t6, 1960(ra)
Current Store : [0x80007570] : sw t6, 1960(ra) -- Store: [0x80012e60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007564]:fmul.d t5, t3, s10, dyn
	-[0x80007568]:csrrs a6, fcsr, zero
	-[0x8000756c]:sw t5, 1952(ra)
	-[0x80007570]:sw t6, 1960(ra)
	-[0x80007574]:sw t5, 1968(ra)
Current Store : [0x80007574] : sw t5, 1968(ra) -- Store: [0x80012e68]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800075a8]:fmul.d t5, t3, s10, dyn
	-[0x800075ac]:csrrs a6, fcsr, zero
	-[0x800075b0]:sw t5, 1984(ra)
	-[0x800075b4]:sw t6, 1992(ra)
Current Store : [0x800075b4] : sw t6, 1992(ra) -- Store: [0x80012e80]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800075a8]:fmul.d t5, t3, s10, dyn
	-[0x800075ac]:csrrs a6, fcsr, zero
	-[0x800075b0]:sw t5, 1984(ra)
	-[0x800075b4]:sw t6, 1992(ra)
	-[0x800075b8]:sw t5, 2000(ra)
Current Store : [0x800075b8] : sw t5, 2000(ra) -- Store: [0x80012e88]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800075ec]:fmul.d t5, t3, s10, dyn
	-[0x800075f0]:csrrs a6, fcsr, zero
	-[0x800075f4]:sw t5, 2016(ra)
	-[0x800075f8]:sw t6, 2024(ra)
Current Store : [0x800075f8] : sw t6, 2024(ra) -- Store: [0x80012ea0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800075ec]:fmul.d t5, t3, s10, dyn
	-[0x800075f0]:csrrs a6, fcsr, zero
	-[0x800075f4]:sw t5, 2016(ra)
	-[0x800075f8]:sw t6, 2024(ra)
	-[0x800075fc]:sw t5, 2032(ra)
Current Store : [0x800075fc] : sw t5, 2032(ra) -- Store: [0x80012ea8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007630]:fmul.d t5, t3, s10, dyn
	-[0x80007634]:csrrs a6, fcsr, zero
	-[0x80007638]:addi ra, ra, 2040
	-[0x8000763c]:sw t5, 8(ra)
	-[0x80007640]:sw t6, 16(ra)
Current Store : [0x80007640] : sw t6, 16(ra) -- Store: [0x80012ec0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007630]:fmul.d t5, t3, s10, dyn
	-[0x80007634]:csrrs a6, fcsr, zero
	-[0x80007638]:addi ra, ra, 2040
	-[0x8000763c]:sw t5, 8(ra)
	-[0x80007640]:sw t6, 16(ra)
	-[0x80007644]:sw t5, 24(ra)
Current Store : [0x80007644] : sw t5, 24(ra) -- Store: [0x80012ec8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007678]:fmul.d t5, t3, s10, dyn
	-[0x8000767c]:csrrs a6, fcsr, zero
	-[0x80007680]:sw t5, 40(ra)
	-[0x80007684]:sw t6, 48(ra)
Current Store : [0x80007684] : sw t6, 48(ra) -- Store: [0x80012ee0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007678]:fmul.d t5, t3, s10, dyn
	-[0x8000767c]:csrrs a6, fcsr, zero
	-[0x80007680]:sw t5, 40(ra)
	-[0x80007684]:sw t6, 48(ra)
	-[0x80007688]:sw t5, 56(ra)
Current Store : [0x80007688] : sw t5, 56(ra) -- Store: [0x80012ee8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800076bc]:fmul.d t5, t3, s10, dyn
	-[0x800076c0]:csrrs a6, fcsr, zero
	-[0x800076c4]:sw t5, 72(ra)
	-[0x800076c8]:sw t6, 80(ra)
Current Store : [0x800076c8] : sw t6, 80(ra) -- Store: [0x80012f00]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800076bc]:fmul.d t5, t3, s10, dyn
	-[0x800076c0]:csrrs a6, fcsr, zero
	-[0x800076c4]:sw t5, 72(ra)
	-[0x800076c8]:sw t6, 80(ra)
	-[0x800076cc]:sw t5, 88(ra)
Current Store : [0x800076cc] : sw t5, 88(ra) -- Store: [0x80012f08]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007700]:fmul.d t5, t3, s10, dyn
	-[0x80007704]:csrrs a6, fcsr, zero
	-[0x80007708]:sw t5, 104(ra)
	-[0x8000770c]:sw t6, 112(ra)
Current Store : [0x8000770c] : sw t6, 112(ra) -- Store: [0x80012f20]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007700]:fmul.d t5, t3, s10, dyn
	-[0x80007704]:csrrs a6, fcsr, zero
	-[0x80007708]:sw t5, 104(ra)
	-[0x8000770c]:sw t6, 112(ra)
	-[0x80007710]:sw t5, 120(ra)
Current Store : [0x80007710] : sw t5, 120(ra) -- Store: [0x80012f28]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007744]:fmul.d t5, t3, s10, dyn
	-[0x80007748]:csrrs a6, fcsr, zero
	-[0x8000774c]:sw t5, 136(ra)
	-[0x80007750]:sw t6, 144(ra)
Current Store : [0x80007750] : sw t6, 144(ra) -- Store: [0x80012f40]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007744]:fmul.d t5, t3, s10, dyn
	-[0x80007748]:csrrs a6, fcsr, zero
	-[0x8000774c]:sw t5, 136(ra)
	-[0x80007750]:sw t6, 144(ra)
	-[0x80007754]:sw t5, 152(ra)
Current Store : [0x80007754] : sw t5, 152(ra) -- Store: [0x80012f48]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007788]:fmul.d t5, t3, s10, dyn
	-[0x8000778c]:csrrs a6, fcsr, zero
	-[0x80007790]:sw t5, 168(ra)
	-[0x80007794]:sw t6, 176(ra)
Current Store : [0x80007794] : sw t6, 176(ra) -- Store: [0x80012f60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007788]:fmul.d t5, t3, s10, dyn
	-[0x8000778c]:csrrs a6, fcsr, zero
	-[0x80007790]:sw t5, 168(ra)
	-[0x80007794]:sw t6, 176(ra)
	-[0x80007798]:sw t5, 184(ra)
Current Store : [0x80007798] : sw t5, 184(ra) -- Store: [0x80012f68]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800077c8]:fmul.d t5, t3, s10, dyn
	-[0x800077cc]:csrrs a6, fcsr, zero
	-[0x800077d0]:sw t5, 200(ra)
	-[0x800077d4]:sw t6, 208(ra)
Current Store : [0x800077d4] : sw t6, 208(ra) -- Store: [0x80012f80]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800077c8]:fmul.d t5, t3, s10, dyn
	-[0x800077cc]:csrrs a6, fcsr, zero
	-[0x800077d0]:sw t5, 200(ra)
	-[0x800077d4]:sw t6, 208(ra)
	-[0x800077d8]:sw t5, 216(ra)
Current Store : [0x800077d8] : sw t5, 216(ra) -- Store: [0x80012f88]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007808]:fmul.d t5, t3, s10, dyn
	-[0x8000780c]:csrrs a6, fcsr, zero
	-[0x80007810]:sw t5, 232(ra)
	-[0x80007814]:sw t6, 240(ra)
Current Store : [0x80007814] : sw t6, 240(ra) -- Store: [0x80012fa0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007808]:fmul.d t5, t3, s10, dyn
	-[0x8000780c]:csrrs a6, fcsr, zero
	-[0x80007810]:sw t5, 232(ra)
	-[0x80007814]:sw t6, 240(ra)
	-[0x80007818]:sw t5, 248(ra)
Current Store : [0x80007818] : sw t5, 248(ra) -- Store: [0x80012fa8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007848]:fmul.d t5, t3, s10, dyn
	-[0x8000784c]:csrrs a6, fcsr, zero
	-[0x80007850]:sw t5, 264(ra)
	-[0x80007854]:sw t6, 272(ra)
Current Store : [0x80007854] : sw t6, 272(ra) -- Store: [0x80012fc0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007848]:fmul.d t5, t3, s10, dyn
	-[0x8000784c]:csrrs a6, fcsr, zero
	-[0x80007850]:sw t5, 264(ra)
	-[0x80007854]:sw t6, 272(ra)
	-[0x80007858]:sw t5, 280(ra)
Current Store : [0x80007858] : sw t5, 280(ra) -- Store: [0x80012fc8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007888]:fmul.d t5, t3, s10, dyn
	-[0x8000788c]:csrrs a6, fcsr, zero
	-[0x80007890]:sw t5, 296(ra)
	-[0x80007894]:sw t6, 304(ra)
Current Store : [0x80007894] : sw t6, 304(ra) -- Store: [0x80012fe0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007888]:fmul.d t5, t3, s10, dyn
	-[0x8000788c]:csrrs a6, fcsr, zero
	-[0x80007890]:sw t5, 296(ra)
	-[0x80007894]:sw t6, 304(ra)
	-[0x80007898]:sw t5, 312(ra)
Current Store : [0x80007898] : sw t5, 312(ra) -- Store: [0x80012fe8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800078c8]:fmul.d t5, t3, s10, dyn
	-[0x800078cc]:csrrs a6, fcsr, zero
	-[0x800078d0]:sw t5, 328(ra)
	-[0x800078d4]:sw t6, 336(ra)
Current Store : [0x800078d4] : sw t6, 336(ra) -- Store: [0x80013000]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800078c8]:fmul.d t5, t3, s10, dyn
	-[0x800078cc]:csrrs a6, fcsr, zero
	-[0x800078d0]:sw t5, 328(ra)
	-[0x800078d4]:sw t6, 336(ra)
	-[0x800078d8]:sw t5, 344(ra)
Current Store : [0x800078d8] : sw t5, 344(ra) -- Store: [0x80013008]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007908]:fmul.d t5, t3, s10, dyn
	-[0x8000790c]:csrrs a6, fcsr, zero
	-[0x80007910]:sw t5, 360(ra)
	-[0x80007914]:sw t6, 368(ra)
Current Store : [0x80007914] : sw t6, 368(ra) -- Store: [0x80013020]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007908]:fmul.d t5, t3, s10, dyn
	-[0x8000790c]:csrrs a6, fcsr, zero
	-[0x80007910]:sw t5, 360(ra)
	-[0x80007914]:sw t6, 368(ra)
	-[0x80007918]:sw t5, 376(ra)
Current Store : [0x80007918] : sw t5, 376(ra) -- Store: [0x80013028]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000794c]:fmul.d t5, t3, s10, dyn
	-[0x80007950]:csrrs a6, fcsr, zero
	-[0x80007954]:sw t5, 392(ra)
	-[0x80007958]:sw t6, 400(ra)
Current Store : [0x80007958] : sw t6, 400(ra) -- Store: [0x80013040]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000794c]:fmul.d t5, t3, s10, dyn
	-[0x80007950]:csrrs a6, fcsr, zero
	-[0x80007954]:sw t5, 392(ra)
	-[0x80007958]:sw t6, 400(ra)
	-[0x8000795c]:sw t5, 408(ra)
Current Store : [0x8000795c] : sw t5, 408(ra) -- Store: [0x80013048]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007990]:fmul.d t5, t3, s10, dyn
	-[0x80007994]:csrrs a6, fcsr, zero
	-[0x80007998]:sw t5, 424(ra)
	-[0x8000799c]:sw t6, 432(ra)
Current Store : [0x8000799c] : sw t6, 432(ra) -- Store: [0x80013060]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007990]:fmul.d t5, t3, s10, dyn
	-[0x80007994]:csrrs a6, fcsr, zero
	-[0x80007998]:sw t5, 424(ra)
	-[0x8000799c]:sw t6, 432(ra)
	-[0x800079a0]:sw t5, 440(ra)
Current Store : [0x800079a0] : sw t5, 440(ra) -- Store: [0x80013068]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800079d0]:fmul.d t5, t3, s10, dyn
	-[0x800079d4]:csrrs a6, fcsr, zero
	-[0x800079d8]:sw t5, 456(ra)
	-[0x800079dc]:sw t6, 464(ra)
Current Store : [0x800079dc] : sw t6, 464(ra) -- Store: [0x80013080]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800079d0]:fmul.d t5, t3, s10, dyn
	-[0x800079d4]:csrrs a6, fcsr, zero
	-[0x800079d8]:sw t5, 456(ra)
	-[0x800079dc]:sw t6, 464(ra)
	-[0x800079e0]:sw t5, 472(ra)
Current Store : [0x800079e0] : sw t5, 472(ra) -- Store: [0x80013088]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007a10]:fmul.d t5, t3, s10, dyn
	-[0x80007a14]:csrrs a6, fcsr, zero
	-[0x80007a18]:sw t5, 488(ra)
	-[0x80007a1c]:sw t6, 496(ra)
Current Store : [0x80007a1c] : sw t6, 496(ra) -- Store: [0x800130a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007a10]:fmul.d t5, t3, s10, dyn
	-[0x80007a14]:csrrs a6, fcsr, zero
	-[0x80007a18]:sw t5, 488(ra)
	-[0x80007a1c]:sw t6, 496(ra)
	-[0x80007a20]:sw t5, 504(ra)
Current Store : [0x80007a20] : sw t5, 504(ra) -- Store: [0x800130a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007a50]:fmul.d t5, t3, s10, dyn
	-[0x80007a54]:csrrs a6, fcsr, zero
	-[0x80007a58]:sw t5, 520(ra)
	-[0x80007a5c]:sw t6, 528(ra)
Current Store : [0x80007a5c] : sw t6, 528(ra) -- Store: [0x800130c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007a50]:fmul.d t5, t3, s10, dyn
	-[0x80007a54]:csrrs a6, fcsr, zero
	-[0x80007a58]:sw t5, 520(ra)
	-[0x80007a5c]:sw t6, 528(ra)
	-[0x80007a60]:sw t5, 536(ra)
Current Store : [0x80007a60] : sw t5, 536(ra) -- Store: [0x800130c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007a90]:fmul.d t5, t3, s10, dyn
	-[0x80007a94]:csrrs a6, fcsr, zero
	-[0x80007a98]:sw t5, 552(ra)
	-[0x80007a9c]:sw t6, 560(ra)
Current Store : [0x80007a9c] : sw t6, 560(ra) -- Store: [0x800130e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007a90]:fmul.d t5, t3, s10, dyn
	-[0x80007a94]:csrrs a6, fcsr, zero
	-[0x80007a98]:sw t5, 552(ra)
	-[0x80007a9c]:sw t6, 560(ra)
	-[0x80007aa0]:sw t5, 568(ra)
Current Store : [0x80007aa0] : sw t5, 568(ra) -- Store: [0x800130e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007ad4]:fmul.d t5, t3, s10, dyn
	-[0x80007ad8]:csrrs a6, fcsr, zero
	-[0x80007adc]:sw t5, 584(ra)
	-[0x80007ae0]:sw t6, 592(ra)
Current Store : [0x80007ae0] : sw t6, 592(ra) -- Store: [0x80013100]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007ad4]:fmul.d t5, t3, s10, dyn
	-[0x80007ad8]:csrrs a6, fcsr, zero
	-[0x80007adc]:sw t5, 584(ra)
	-[0x80007ae0]:sw t6, 592(ra)
	-[0x80007ae4]:sw t5, 600(ra)
Current Store : [0x80007ae4] : sw t5, 600(ra) -- Store: [0x80013108]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007b18]:fmul.d t5, t3, s10, dyn
	-[0x80007b1c]:csrrs a6, fcsr, zero
	-[0x80007b20]:sw t5, 616(ra)
	-[0x80007b24]:sw t6, 624(ra)
Current Store : [0x80007b24] : sw t6, 624(ra) -- Store: [0x80013120]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007b18]:fmul.d t5, t3, s10, dyn
	-[0x80007b1c]:csrrs a6, fcsr, zero
	-[0x80007b20]:sw t5, 616(ra)
	-[0x80007b24]:sw t6, 624(ra)
	-[0x80007b28]:sw t5, 632(ra)
Current Store : [0x80007b28] : sw t5, 632(ra) -- Store: [0x80013128]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007b58]:fmul.d t5, t3, s10, dyn
	-[0x80007b5c]:csrrs a6, fcsr, zero
	-[0x80007b60]:sw t5, 648(ra)
	-[0x80007b64]:sw t6, 656(ra)
Current Store : [0x80007b64] : sw t6, 656(ra) -- Store: [0x80013140]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007b58]:fmul.d t5, t3, s10, dyn
	-[0x80007b5c]:csrrs a6, fcsr, zero
	-[0x80007b60]:sw t5, 648(ra)
	-[0x80007b64]:sw t6, 656(ra)
	-[0x80007b68]:sw t5, 664(ra)
Current Store : [0x80007b68] : sw t5, 664(ra) -- Store: [0x80013148]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007b98]:fmul.d t5, t3, s10, dyn
	-[0x80007b9c]:csrrs a6, fcsr, zero
	-[0x80007ba0]:sw t5, 680(ra)
	-[0x80007ba4]:sw t6, 688(ra)
Current Store : [0x80007ba4] : sw t6, 688(ra) -- Store: [0x80013160]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007b98]:fmul.d t5, t3, s10, dyn
	-[0x80007b9c]:csrrs a6, fcsr, zero
	-[0x80007ba0]:sw t5, 680(ra)
	-[0x80007ba4]:sw t6, 688(ra)
	-[0x80007ba8]:sw t5, 696(ra)
Current Store : [0x80007ba8] : sw t5, 696(ra) -- Store: [0x80013168]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007bd8]:fmul.d t5, t3, s10, dyn
	-[0x80007bdc]:csrrs a6, fcsr, zero
	-[0x80007be0]:sw t5, 712(ra)
	-[0x80007be4]:sw t6, 720(ra)
Current Store : [0x80007be4] : sw t6, 720(ra) -- Store: [0x80013180]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007bd8]:fmul.d t5, t3, s10, dyn
	-[0x80007bdc]:csrrs a6, fcsr, zero
	-[0x80007be0]:sw t5, 712(ra)
	-[0x80007be4]:sw t6, 720(ra)
	-[0x80007be8]:sw t5, 728(ra)
Current Store : [0x80007be8] : sw t5, 728(ra) -- Store: [0x80013188]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007c18]:fmul.d t5, t3, s10, dyn
	-[0x80007c1c]:csrrs a6, fcsr, zero
	-[0x80007c20]:sw t5, 744(ra)
	-[0x80007c24]:sw t6, 752(ra)
Current Store : [0x80007c24] : sw t6, 752(ra) -- Store: [0x800131a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007c18]:fmul.d t5, t3, s10, dyn
	-[0x80007c1c]:csrrs a6, fcsr, zero
	-[0x80007c20]:sw t5, 744(ra)
	-[0x80007c24]:sw t6, 752(ra)
	-[0x80007c28]:sw t5, 760(ra)
Current Store : [0x80007c28] : sw t5, 760(ra) -- Store: [0x800131a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007c58]:fmul.d t5, t3, s10, dyn
	-[0x80007c5c]:csrrs a6, fcsr, zero
	-[0x80007c60]:sw t5, 776(ra)
	-[0x80007c64]:sw t6, 784(ra)
Current Store : [0x80007c64] : sw t6, 784(ra) -- Store: [0x800131c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007c58]:fmul.d t5, t3, s10, dyn
	-[0x80007c5c]:csrrs a6, fcsr, zero
	-[0x80007c60]:sw t5, 776(ra)
	-[0x80007c64]:sw t6, 784(ra)
	-[0x80007c68]:sw t5, 792(ra)
Current Store : [0x80007c68] : sw t5, 792(ra) -- Store: [0x800131c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007c98]:fmul.d t5, t3, s10, dyn
	-[0x80007c9c]:csrrs a6, fcsr, zero
	-[0x80007ca0]:sw t5, 808(ra)
	-[0x80007ca4]:sw t6, 816(ra)
Current Store : [0x80007ca4] : sw t6, 816(ra) -- Store: [0x800131e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007c98]:fmul.d t5, t3, s10, dyn
	-[0x80007c9c]:csrrs a6, fcsr, zero
	-[0x80007ca0]:sw t5, 808(ra)
	-[0x80007ca4]:sw t6, 816(ra)
	-[0x80007ca8]:sw t5, 824(ra)
Current Store : [0x80007ca8] : sw t5, 824(ra) -- Store: [0x800131e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007cd8]:fmul.d t5, t3, s10, dyn
	-[0x80007cdc]:csrrs a6, fcsr, zero
	-[0x80007ce0]:sw t5, 840(ra)
	-[0x80007ce4]:sw t6, 848(ra)
Current Store : [0x80007ce4] : sw t6, 848(ra) -- Store: [0x80013200]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007cd8]:fmul.d t5, t3, s10, dyn
	-[0x80007cdc]:csrrs a6, fcsr, zero
	-[0x80007ce0]:sw t5, 840(ra)
	-[0x80007ce4]:sw t6, 848(ra)
	-[0x80007ce8]:sw t5, 856(ra)
Current Store : [0x80007ce8] : sw t5, 856(ra) -- Store: [0x80013208]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007d18]:fmul.d t5, t3, s10, dyn
	-[0x80007d1c]:csrrs a6, fcsr, zero
	-[0x80007d20]:sw t5, 872(ra)
	-[0x80007d24]:sw t6, 880(ra)
Current Store : [0x80007d24] : sw t6, 880(ra) -- Store: [0x80013220]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007d18]:fmul.d t5, t3, s10, dyn
	-[0x80007d1c]:csrrs a6, fcsr, zero
	-[0x80007d20]:sw t5, 872(ra)
	-[0x80007d24]:sw t6, 880(ra)
	-[0x80007d28]:sw t5, 888(ra)
Current Store : [0x80007d28] : sw t5, 888(ra) -- Store: [0x80013228]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007d58]:fmul.d t5, t3, s10, dyn
	-[0x80007d5c]:csrrs a6, fcsr, zero
	-[0x80007d60]:sw t5, 904(ra)
	-[0x80007d64]:sw t6, 912(ra)
Current Store : [0x80007d64] : sw t6, 912(ra) -- Store: [0x80013240]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007d58]:fmul.d t5, t3, s10, dyn
	-[0x80007d5c]:csrrs a6, fcsr, zero
	-[0x80007d60]:sw t5, 904(ra)
	-[0x80007d64]:sw t6, 912(ra)
	-[0x80007d68]:sw t5, 920(ra)
Current Store : [0x80007d68] : sw t5, 920(ra) -- Store: [0x80013248]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007d98]:fmul.d t5, t3, s10, dyn
	-[0x80007d9c]:csrrs a6, fcsr, zero
	-[0x80007da0]:sw t5, 936(ra)
	-[0x80007da4]:sw t6, 944(ra)
Current Store : [0x80007da4] : sw t6, 944(ra) -- Store: [0x80013260]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007d98]:fmul.d t5, t3, s10, dyn
	-[0x80007d9c]:csrrs a6, fcsr, zero
	-[0x80007da0]:sw t5, 936(ra)
	-[0x80007da4]:sw t6, 944(ra)
	-[0x80007da8]:sw t5, 952(ra)
Current Store : [0x80007da8] : sw t5, 952(ra) -- Store: [0x80013268]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007dd8]:fmul.d t5, t3, s10, dyn
	-[0x80007ddc]:csrrs a6, fcsr, zero
	-[0x80007de0]:sw t5, 968(ra)
	-[0x80007de4]:sw t6, 976(ra)
Current Store : [0x80007de4] : sw t6, 976(ra) -- Store: [0x80013280]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007dd8]:fmul.d t5, t3, s10, dyn
	-[0x80007ddc]:csrrs a6, fcsr, zero
	-[0x80007de0]:sw t5, 968(ra)
	-[0x80007de4]:sw t6, 976(ra)
	-[0x80007de8]:sw t5, 984(ra)
Current Store : [0x80007de8] : sw t5, 984(ra) -- Store: [0x80013288]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007e18]:fmul.d t5, t3, s10, dyn
	-[0x80007e1c]:csrrs a6, fcsr, zero
	-[0x80007e20]:sw t5, 1000(ra)
	-[0x80007e24]:sw t6, 1008(ra)
Current Store : [0x80007e24] : sw t6, 1008(ra) -- Store: [0x800132a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007e18]:fmul.d t5, t3, s10, dyn
	-[0x80007e1c]:csrrs a6, fcsr, zero
	-[0x80007e20]:sw t5, 1000(ra)
	-[0x80007e24]:sw t6, 1008(ra)
	-[0x80007e28]:sw t5, 1016(ra)
Current Store : [0x80007e28] : sw t5, 1016(ra) -- Store: [0x800132a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007e58]:fmul.d t5, t3, s10, dyn
	-[0x80007e5c]:csrrs a6, fcsr, zero
	-[0x80007e60]:sw t5, 1032(ra)
	-[0x80007e64]:sw t6, 1040(ra)
Current Store : [0x80007e64] : sw t6, 1040(ra) -- Store: [0x800132c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007e58]:fmul.d t5, t3, s10, dyn
	-[0x80007e5c]:csrrs a6, fcsr, zero
	-[0x80007e60]:sw t5, 1032(ra)
	-[0x80007e64]:sw t6, 1040(ra)
	-[0x80007e68]:sw t5, 1048(ra)
Current Store : [0x80007e68] : sw t5, 1048(ra) -- Store: [0x800132c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007e98]:fmul.d t5, t3, s10, dyn
	-[0x80007e9c]:csrrs a6, fcsr, zero
	-[0x80007ea0]:sw t5, 1064(ra)
	-[0x80007ea4]:sw t6, 1072(ra)
Current Store : [0x80007ea4] : sw t6, 1072(ra) -- Store: [0x800132e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007e98]:fmul.d t5, t3, s10, dyn
	-[0x80007e9c]:csrrs a6, fcsr, zero
	-[0x80007ea0]:sw t5, 1064(ra)
	-[0x80007ea4]:sw t6, 1072(ra)
	-[0x80007ea8]:sw t5, 1080(ra)
Current Store : [0x80007ea8] : sw t5, 1080(ra) -- Store: [0x800132e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007ed8]:fmul.d t5, t3, s10, dyn
	-[0x80007edc]:csrrs a6, fcsr, zero
	-[0x80007ee0]:sw t5, 1096(ra)
	-[0x80007ee4]:sw t6, 1104(ra)
Current Store : [0x80007ee4] : sw t6, 1104(ra) -- Store: [0x80013300]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007ed8]:fmul.d t5, t3, s10, dyn
	-[0x80007edc]:csrrs a6, fcsr, zero
	-[0x80007ee0]:sw t5, 1096(ra)
	-[0x80007ee4]:sw t6, 1104(ra)
	-[0x80007ee8]:sw t5, 1112(ra)
Current Store : [0x80007ee8] : sw t5, 1112(ra) -- Store: [0x80013308]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007f18]:fmul.d t5, t3, s10, dyn
	-[0x80007f1c]:csrrs a6, fcsr, zero
	-[0x80007f20]:sw t5, 1128(ra)
	-[0x80007f24]:sw t6, 1136(ra)
Current Store : [0x80007f24] : sw t6, 1136(ra) -- Store: [0x80013320]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007f18]:fmul.d t5, t3, s10, dyn
	-[0x80007f1c]:csrrs a6, fcsr, zero
	-[0x80007f20]:sw t5, 1128(ra)
	-[0x80007f24]:sw t6, 1136(ra)
	-[0x80007f28]:sw t5, 1144(ra)
Current Store : [0x80007f28] : sw t5, 1144(ra) -- Store: [0x80013328]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007f5c]:fmul.d t5, t3, s10, dyn
	-[0x80007f60]:csrrs a6, fcsr, zero
	-[0x80007f64]:sw t5, 1160(ra)
	-[0x80007f68]:sw t6, 1168(ra)
Current Store : [0x80007f68] : sw t6, 1168(ra) -- Store: [0x80013340]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007f5c]:fmul.d t5, t3, s10, dyn
	-[0x80007f60]:csrrs a6, fcsr, zero
	-[0x80007f64]:sw t5, 1160(ra)
	-[0x80007f68]:sw t6, 1168(ra)
	-[0x80007f6c]:sw t5, 1176(ra)
Current Store : [0x80007f6c] : sw t5, 1176(ra) -- Store: [0x80013348]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007fa0]:fmul.d t5, t3, s10, dyn
	-[0x80007fa4]:csrrs a6, fcsr, zero
	-[0x80007fa8]:sw t5, 1192(ra)
	-[0x80007fac]:sw t6, 1200(ra)
Current Store : [0x80007fac] : sw t6, 1200(ra) -- Store: [0x80013360]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007fa0]:fmul.d t5, t3, s10, dyn
	-[0x80007fa4]:csrrs a6, fcsr, zero
	-[0x80007fa8]:sw t5, 1192(ra)
	-[0x80007fac]:sw t6, 1200(ra)
	-[0x80007fb0]:sw t5, 1208(ra)
Current Store : [0x80007fb0] : sw t5, 1208(ra) -- Store: [0x80013368]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007fe0]:fmul.d t5, t3, s10, dyn
	-[0x80007fe4]:csrrs a6, fcsr, zero
	-[0x80007fe8]:sw t5, 1224(ra)
	-[0x80007fec]:sw t6, 1232(ra)
Current Store : [0x80007fec] : sw t6, 1232(ra) -- Store: [0x80013380]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007fe0]:fmul.d t5, t3, s10, dyn
	-[0x80007fe4]:csrrs a6, fcsr, zero
	-[0x80007fe8]:sw t5, 1224(ra)
	-[0x80007fec]:sw t6, 1232(ra)
	-[0x80007ff0]:sw t5, 1240(ra)
Current Store : [0x80007ff0] : sw t5, 1240(ra) -- Store: [0x80013388]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008020]:fmul.d t5, t3, s10, dyn
	-[0x80008024]:csrrs a6, fcsr, zero
	-[0x80008028]:sw t5, 1256(ra)
	-[0x8000802c]:sw t6, 1264(ra)
Current Store : [0x8000802c] : sw t6, 1264(ra) -- Store: [0x800133a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008020]:fmul.d t5, t3, s10, dyn
	-[0x80008024]:csrrs a6, fcsr, zero
	-[0x80008028]:sw t5, 1256(ra)
	-[0x8000802c]:sw t6, 1264(ra)
	-[0x80008030]:sw t5, 1272(ra)
Current Store : [0x80008030] : sw t5, 1272(ra) -- Store: [0x800133a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008060]:fmul.d t5, t3, s10, dyn
	-[0x80008064]:csrrs a6, fcsr, zero
	-[0x80008068]:sw t5, 1288(ra)
	-[0x8000806c]:sw t6, 1296(ra)
Current Store : [0x8000806c] : sw t6, 1296(ra) -- Store: [0x800133c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008060]:fmul.d t5, t3, s10, dyn
	-[0x80008064]:csrrs a6, fcsr, zero
	-[0x80008068]:sw t5, 1288(ra)
	-[0x8000806c]:sw t6, 1296(ra)
	-[0x80008070]:sw t5, 1304(ra)
Current Store : [0x80008070] : sw t5, 1304(ra) -- Store: [0x800133c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800080a0]:fmul.d t5, t3, s10, dyn
	-[0x800080a4]:csrrs a6, fcsr, zero
	-[0x800080a8]:sw t5, 1320(ra)
	-[0x800080ac]:sw t6, 1328(ra)
Current Store : [0x800080ac] : sw t6, 1328(ra) -- Store: [0x800133e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800080a0]:fmul.d t5, t3, s10, dyn
	-[0x800080a4]:csrrs a6, fcsr, zero
	-[0x800080a8]:sw t5, 1320(ra)
	-[0x800080ac]:sw t6, 1328(ra)
	-[0x800080b0]:sw t5, 1336(ra)
Current Store : [0x800080b0] : sw t5, 1336(ra) -- Store: [0x800133e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800080e4]:fmul.d t5, t3, s10, dyn
	-[0x800080e8]:csrrs a6, fcsr, zero
	-[0x800080ec]:sw t5, 1352(ra)
	-[0x800080f0]:sw t6, 1360(ra)
Current Store : [0x800080f0] : sw t6, 1360(ra) -- Store: [0x80013400]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800080e4]:fmul.d t5, t3, s10, dyn
	-[0x800080e8]:csrrs a6, fcsr, zero
	-[0x800080ec]:sw t5, 1352(ra)
	-[0x800080f0]:sw t6, 1360(ra)
	-[0x800080f4]:sw t5, 1368(ra)
Current Store : [0x800080f4] : sw t5, 1368(ra) -- Store: [0x80013408]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008128]:fmul.d t5, t3, s10, dyn
	-[0x8000812c]:csrrs a6, fcsr, zero
	-[0x80008130]:sw t5, 1384(ra)
	-[0x80008134]:sw t6, 1392(ra)
Current Store : [0x80008134] : sw t6, 1392(ra) -- Store: [0x80013420]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008128]:fmul.d t5, t3, s10, dyn
	-[0x8000812c]:csrrs a6, fcsr, zero
	-[0x80008130]:sw t5, 1384(ra)
	-[0x80008134]:sw t6, 1392(ra)
	-[0x80008138]:sw t5, 1400(ra)
Current Store : [0x80008138] : sw t5, 1400(ra) -- Store: [0x80013428]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008168]:fmul.d t5, t3, s10, dyn
	-[0x8000816c]:csrrs a6, fcsr, zero
	-[0x80008170]:sw t5, 1416(ra)
	-[0x80008174]:sw t6, 1424(ra)
Current Store : [0x80008174] : sw t6, 1424(ra) -- Store: [0x80013440]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008168]:fmul.d t5, t3, s10, dyn
	-[0x8000816c]:csrrs a6, fcsr, zero
	-[0x80008170]:sw t5, 1416(ra)
	-[0x80008174]:sw t6, 1424(ra)
	-[0x80008178]:sw t5, 1432(ra)
Current Store : [0x80008178] : sw t5, 1432(ra) -- Store: [0x80013448]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800081a8]:fmul.d t5, t3, s10, dyn
	-[0x800081ac]:csrrs a6, fcsr, zero
	-[0x800081b0]:sw t5, 1448(ra)
	-[0x800081b4]:sw t6, 1456(ra)
Current Store : [0x800081b4] : sw t6, 1456(ra) -- Store: [0x80013460]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800081a8]:fmul.d t5, t3, s10, dyn
	-[0x800081ac]:csrrs a6, fcsr, zero
	-[0x800081b0]:sw t5, 1448(ra)
	-[0x800081b4]:sw t6, 1456(ra)
	-[0x800081b8]:sw t5, 1464(ra)
Current Store : [0x800081b8] : sw t5, 1464(ra) -- Store: [0x80013468]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800081e8]:fmul.d t5, t3, s10, dyn
	-[0x800081ec]:csrrs a6, fcsr, zero
	-[0x800081f0]:sw t5, 1480(ra)
	-[0x800081f4]:sw t6, 1488(ra)
Current Store : [0x800081f4] : sw t6, 1488(ra) -- Store: [0x80013480]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800081e8]:fmul.d t5, t3, s10, dyn
	-[0x800081ec]:csrrs a6, fcsr, zero
	-[0x800081f0]:sw t5, 1480(ra)
	-[0x800081f4]:sw t6, 1488(ra)
	-[0x800081f8]:sw t5, 1496(ra)
Current Store : [0x800081f8] : sw t5, 1496(ra) -- Store: [0x80013488]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008228]:fmul.d t5, t3, s10, dyn
	-[0x8000822c]:csrrs a6, fcsr, zero
	-[0x80008230]:sw t5, 1512(ra)
	-[0x80008234]:sw t6, 1520(ra)
Current Store : [0x80008234] : sw t6, 1520(ra) -- Store: [0x800134a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008228]:fmul.d t5, t3, s10, dyn
	-[0x8000822c]:csrrs a6, fcsr, zero
	-[0x80008230]:sw t5, 1512(ra)
	-[0x80008234]:sw t6, 1520(ra)
	-[0x80008238]:sw t5, 1528(ra)
Current Store : [0x80008238] : sw t5, 1528(ra) -- Store: [0x800134a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008268]:fmul.d t5, t3, s10, dyn
	-[0x8000826c]:csrrs a6, fcsr, zero
	-[0x80008270]:sw t5, 1544(ra)
	-[0x80008274]:sw t6, 1552(ra)
Current Store : [0x80008274] : sw t6, 1552(ra) -- Store: [0x800134c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008268]:fmul.d t5, t3, s10, dyn
	-[0x8000826c]:csrrs a6, fcsr, zero
	-[0x80008270]:sw t5, 1544(ra)
	-[0x80008274]:sw t6, 1552(ra)
	-[0x80008278]:sw t5, 1560(ra)
Current Store : [0x80008278] : sw t5, 1560(ra) -- Store: [0x800134c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800082a8]:fmul.d t5, t3, s10, dyn
	-[0x800082ac]:csrrs a6, fcsr, zero
	-[0x800082b0]:sw t5, 1576(ra)
	-[0x800082b4]:sw t6, 1584(ra)
Current Store : [0x800082b4] : sw t6, 1584(ra) -- Store: [0x800134e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800082a8]:fmul.d t5, t3, s10, dyn
	-[0x800082ac]:csrrs a6, fcsr, zero
	-[0x800082b0]:sw t5, 1576(ra)
	-[0x800082b4]:sw t6, 1584(ra)
	-[0x800082b8]:sw t5, 1592(ra)
Current Store : [0x800082b8] : sw t5, 1592(ra) -- Store: [0x800134e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800082e8]:fmul.d t5, t3, s10, dyn
	-[0x800082ec]:csrrs a6, fcsr, zero
	-[0x800082f0]:sw t5, 1608(ra)
	-[0x800082f4]:sw t6, 1616(ra)
Current Store : [0x800082f4] : sw t6, 1616(ra) -- Store: [0x80013500]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800082e8]:fmul.d t5, t3, s10, dyn
	-[0x800082ec]:csrrs a6, fcsr, zero
	-[0x800082f0]:sw t5, 1608(ra)
	-[0x800082f4]:sw t6, 1616(ra)
	-[0x800082f8]:sw t5, 1624(ra)
Current Store : [0x800082f8] : sw t5, 1624(ra) -- Store: [0x80013508]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008328]:fmul.d t5, t3, s10, dyn
	-[0x8000832c]:csrrs a6, fcsr, zero
	-[0x80008330]:sw t5, 1640(ra)
	-[0x80008334]:sw t6, 1648(ra)
Current Store : [0x80008334] : sw t6, 1648(ra) -- Store: [0x80013520]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008328]:fmul.d t5, t3, s10, dyn
	-[0x8000832c]:csrrs a6, fcsr, zero
	-[0x80008330]:sw t5, 1640(ra)
	-[0x80008334]:sw t6, 1648(ra)
	-[0x80008338]:sw t5, 1656(ra)
Current Store : [0x80008338] : sw t5, 1656(ra) -- Store: [0x80013528]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008368]:fmul.d t5, t3, s10, dyn
	-[0x8000836c]:csrrs a6, fcsr, zero
	-[0x80008370]:sw t5, 1672(ra)
	-[0x80008374]:sw t6, 1680(ra)
Current Store : [0x80008374] : sw t6, 1680(ra) -- Store: [0x80013540]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008368]:fmul.d t5, t3, s10, dyn
	-[0x8000836c]:csrrs a6, fcsr, zero
	-[0x80008370]:sw t5, 1672(ra)
	-[0x80008374]:sw t6, 1680(ra)
	-[0x80008378]:sw t5, 1688(ra)
Current Store : [0x80008378] : sw t5, 1688(ra) -- Store: [0x80013548]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800083a8]:fmul.d t5, t3, s10, dyn
	-[0x800083ac]:csrrs a6, fcsr, zero
	-[0x800083b0]:sw t5, 1704(ra)
	-[0x800083b4]:sw t6, 1712(ra)
Current Store : [0x800083b4] : sw t6, 1712(ra) -- Store: [0x80013560]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800083a8]:fmul.d t5, t3, s10, dyn
	-[0x800083ac]:csrrs a6, fcsr, zero
	-[0x800083b0]:sw t5, 1704(ra)
	-[0x800083b4]:sw t6, 1712(ra)
	-[0x800083b8]:sw t5, 1720(ra)
Current Store : [0x800083b8] : sw t5, 1720(ra) -- Store: [0x80013568]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800083e8]:fmul.d t5, t3, s10, dyn
	-[0x800083ec]:csrrs a6, fcsr, zero
	-[0x800083f0]:sw t5, 1736(ra)
	-[0x800083f4]:sw t6, 1744(ra)
Current Store : [0x800083f4] : sw t6, 1744(ra) -- Store: [0x80013580]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800083e8]:fmul.d t5, t3, s10, dyn
	-[0x800083ec]:csrrs a6, fcsr, zero
	-[0x800083f0]:sw t5, 1736(ra)
	-[0x800083f4]:sw t6, 1744(ra)
	-[0x800083f8]:sw t5, 1752(ra)
Current Store : [0x800083f8] : sw t5, 1752(ra) -- Store: [0x80013588]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008428]:fmul.d t5, t3, s10, dyn
	-[0x8000842c]:csrrs a6, fcsr, zero
	-[0x80008430]:sw t5, 1768(ra)
	-[0x80008434]:sw t6, 1776(ra)
Current Store : [0x80008434] : sw t6, 1776(ra) -- Store: [0x800135a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008428]:fmul.d t5, t3, s10, dyn
	-[0x8000842c]:csrrs a6, fcsr, zero
	-[0x80008430]:sw t5, 1768(ra)
	-[0x80008434]:sw t6, 1776(ra)
	-[0x80008438]:sw t5, 1784(ra)
Current Store : [0x80008438] : sw t5, 1784(ra) -- Store: [0x800135a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008468]:fmul.d t5, t3, s10, dyn
	-[0x8000846c]:csrrs a6, fcsr, zero
	-[0x80008470]:sw t5, 1800(ra)
	-[0x80008474]:sw t6, 1808(ra)
Current Store : [0x80008474] : sw t6, 1808(ra) -- Store: [0x800135c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008468]:fmul.d t5, t3, s10, dyn
	-[0x8000846c]:csrrs a6, fcsr, zero
	-[0x80008470]:sw t5, 1800(ra)
	-[0x80008474]:sw t6, 1808(ra)
	-[0x80008478]:sw t5, 1816(ra)
Current Store : [0x80008478] : sw t5, 1816(ra) -- Store: [0x800135c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800084a8]:fmul.d t5, t3, s10, dyn
	-[0x800084ac]:csrrs a6, fcsr, zero
	-[0x800084b0]:sw t5, 1832(ra)
	-[0x800084b4]:sw t6, 1840(ra)
Current Store : [0x800084b4] : sw t6, 1840(ra) -- Store: [0x800135e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800084a8]:fmul.d t5, t3, s10, dyn
	-[0x800084ac]:csrrs a6, fcsr, zero
	-[0x800084b0]:sw t5, 1832(ra)
	-[0x800084b4]:sw t6, 1840(ra)
	-[0x800084b8]:sw t5, 1848(ra)
Current Store : [0x800084b8] : sw t5, 1848(ra) -- Store: [0x800135e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800084e8]:fmul.d t5, t3, s10, dyn
	-[0x800084ec]:csrrs a6, fcsr, zero
	-[0x800084f0]:sw t5, 1864(ra)
	-[0x800084f4]:sw t6, 1872(ra)
Current Store : [0x800084f4] : sw t6, 1872(ra) -- Store: [0x80013600]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800084e8]:fmul.d t5, t3, s10, dyn
	-[0x800084ec]:csrrs a6, fcsr, zero
	-[0x800084f0]:sw t5, 1864(ra)
	-[0x800084f4]:sw t6, 1872(ra)
	-[0x800084f8]:sw t5, 1880(ra)
Current Store : [0x800084f8] : sw t5, 1880(ra) -- Store: [0x80013608]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008528]:fmul.d t5, t3, s10, dyn
	-[0x8000852c]:csrrs a6, fcsr, zero
	-[0x80008530]:sw t5, 1896(ra)
	-[0x80008534]:sw t6, 1904(ra)
Current Store : [0x80008534] : sw t6, 1904(ra) -- Store: [0x80013620]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008528]:fmul.d t5, t3, s10, dyn
	-[0x8000852c]:csrrs a6, fcsr, zero
	-[0x80008530]:sw t5, 1896(ra)
	-[0x80008534]:sw t6, 1904(ra)
	-[0x80008538]:sw t5, 1912(ra)
Current Store : [0x80008538] : sw t5, 1912(ra) -- Store: [0x80013628]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000856c]:fmul.d t5, t3, s10, dyn
	-[0x80008570]:csrrs a6, fcsr, zero
	-[0x80008574]:sw t5, 1928(ra)
	-[0x80008578]:sw t6, 1936(ra)
Current Store : [0x80008578] : sw t6, 1936(ra) -- Store: [0x80013640]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000856c]:fmul.d t5, t3, s10, dyn
	-[0x80008570]:csrrs a6, fcsr, zero
	-[0x80008574]:sw t5, 1928(ra)
	-[0x80008578]:sw t6, 1936(ra)
	-[0x8000857c]:sw t5, 1944(ra)
Current Store : [0x8000857c] : sw t5, 1944(ra) -- Store: [0x80013648]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800085b0]:fmul.d t5, t3, s10, dyn
	-[0x800085b4]:csrrs a6, fcsr, zero
	-[0x800085b8]:sw t5, 1960(ra)
	-[0x800085bc]:sw t6, 1968(ra)
Current Store : [0x800085bc] : sw t6, 1968(ra) -- Store: [0x80013660]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800085b0]:fmul.d t5, t3, s10, dyn
	-[0x800085b4]:csrrs a6, fcsr, zero
	-[0x800085b8]:sw t5, 1960(ra)
	-[0x800085bc]:sw t6, 1968(ra)
	-[0x800085c0]:sw t5, 1976(ra)
Current Store : [0x800085c0] : sw t5, 1976(ra) -- Store: [0x80013668]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800085f0]:fmul.d t5, t3, s10, dyn
	-[0x800085f4]:csrrs a6, fcsr, zero
	-[0x800085f8]:sw t5, 1992(ra)
	-[0x800085fc]:sw t6, 2000(ra)
Current Store : [0x800085fc] : sw t6, 2000(ra) -- Store: [0x80013680]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800085f0]:fmul.d t5, t3, s10, dyn
	-[0x800085f4]:csrrs a6, fcsr, zero
	-[0x800085f8]:sw t5, 1992(ra)
	-[0x800085fc]:sw t6, 2000(ra)
	-[0x80008600]:sw t5, 2008(ra)
Current Store : [0x80008600] : sw t5, 2008(ra) -- Store: [0x80013688]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008630]:fmul.d t5, t3, s10, dyn
	-[0x80008634]:csrrs a6, fcsr, zero
	-[0x80008638]:sw t5, 2024(ra)
	-[0x8000863c]:sw t6, 2032(ra)
Current Store : [0x8000863c] : sw t6, 2032(ra) -- Store: [0x800136a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008630]:fmul.d t5, t3, s10, dyn
	-[0x80008634]:csrrs a6, fcsr, zero
	-[0x80008638]:sw t5, 2024(ra)
	-[0x8000863c]:sw t6, 2032(ra)
	-[0x80008640]:sw t5, 2040(ra)
Current Store : [0x80008640] : sw t5, 2040(ra) -- Store: [0x800136a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800086b4]:fmul.d t5, t3, s10, dyn
	-[0x800086b8]:csrrs a6, fcsr, zero
	-[0x800086bc]:sw t5, 16(ra)
	-[0x800086c0]:sw t6, 24(ra)
Current Store : [0x800086c0] : sw t6, 24(ra) -- Store: [0x800136c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800086b4]:fmul.d t5, t3, s10, dyn
	-[0x800086b8]:csrrs a6, fcsr, zero
	-[0x800086bc]:sw t5, 16(ra)
	-[0x800086c0]:sw t6, 24(ra)
	-[0x800086c4]:sw t5, 32(ra)
Current Store : [0x800086c4] : sw t5, 32(ra) -- Store: [0x800136c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008734]:fmul.d t5, t3, s10, dyn
	-[0x80008738]:csrrs a6, fcsr, zero
	-[0x8000873c]:sw t5, 48(ra)
	-[0x80008740]:sw t6, 56(ra)
Current Store : [0x80008740] : sw t6, 56(ra) -- Store: [0x800136e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008734]:fmul.d t5, t3, s10, dyn
	-[0x80008738]:csrrs a6, fcsr, zero
	-[0x8000873c]:sw t5, 48(ra)
	-[0x80008740]:sw t6, 56(ra)
	-[0x80008744]:sw t5, 64(ra)
Current Store : [0x80008744] : sw t5, 64(ra) -- Store: [0x800136e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800087b8]:fmul.d t5, t3, s10, dyn
	-[0x800087bc]:csrrs a6, fcsr, zero
	-[0x800087c0]:sw t5, 80(ra)
	-[0x800087c4]:sw t6, 88(ra)
Current Store : [0x800087c4] : sw t6, 88(ra) -- Store: [0x80013700]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800087b8]:fmul.d t5, t3, s10, dyn
	-[0x800087bc]:csrrs a6, fcsr, zero
	-[0x800087c0]:sw t5, 80(ra)
	-[0x800087c4]:sw t6, 88(ra)
	-[0x800087c8]:sw t5, 96(ra)
Current Store : [0x800087c8] : sw t5, 96(ra) -- Store: [0x80013708]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000883c]:fmul.d t5, t3, s10, dyn
	-[0x80008840]:csrrs a6, fcsr, zero
	-[0x80008844]:sw t5, 112(ra)
	-[0x80008848]:sw t6, 120(ra)
Current Store : [0x80008848] : sw t6, 120(ra) -- Store: [0x80013720]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000883c]:fmul.d t5, t3, s10, dyn
	-[0x80008840]:csrrs a6, fcsr, zero
	-[0x80008844]:sw t5, 112(ra)
	-[0x80008848]:sw t6, 120(ra)
	-[0x8000884c]:sw t5, 128(ra)
Current Store : [0x8000884c] : sw t5, 128(ra) -- Store: [0x80013728]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800088bc]:fmul.d t5, t3, s10, dyn
	-[0x800088c0]:csrrs a6, fcsr, zero
	-[0x800088c4]:sw t5, 144(ra)
	-[0x800088c8]:sw t6, 152(ra)
Current Store : [0x800088c8] : sw t6, 152(ra) -- Store: [0x80013740]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800088bc]:fmul.d t5, t3, s10, dyn
	-[0x800088c0]:csrrs a6, fcsr, zero
	-[0x800088c4]:sw t5, 144(ra)
	-[0x800088c8]:sw t6, 152(ra)
	-[0x800088cc]:sw t5, 160(ra)
Current Store : [0x800088cc] : sw t5, 160(ra) -- Store: [0x80013748]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000893c]:fmul.d t5, t3, s10, dyn
	-[0x80008940]:csrrs a6, fcsr, zero
	-[0x80008944]:sw t5, 176(ra)
	-[0x80008948]:sw t6, 184(ra)
Current Store : [0x80008948] : sw t6, 184(ra) -- Store: [0x80013760]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000893c]:fmul.d t5, t3, s10, dyn
	-[0x80008940]:csrrs a6, fcsr, zero
	-[0x80008944]:sw t5, 176(ra)
	-[0x80008948]:sw t6, 184(ra)
	-[0x8000894c]:sw t5, 192(ra)
Current Store : [0x8000894c] : sw t5, 192(ra) -- Store: [0x80013768]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800089bc]:fmul.d t5, t3, s10, dyn
	-[0x800089c0]:csrrs a6, fcsr, zero
	-[0x800089c4]:sw t5, 208(ra)
	-[0x800089c8]:sw t6, 216(ra)
Current Store : [0x800089c8] : sw t6, 216(ra) -- Store: [0x80013780]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800089bc]:fmul.d t5, t3, s10, dyn
	-[0x800089c0]:csrrs a6, fcsr, zero
	-[0x800089c4]:sw t5, 208(ra)
	-[0x800089c8]:sw t6, 216(ra)
	-[0x800089cc]:sw t5, 224(ra)
Current Store : [0x800089cc] : sw t5, 224(ra) -- Store: [0x80013788]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008a3c]:fmul.d t5, t3, s10, dyn
	-[0x80008a40]:csrrs a6, fcsr, zero
	-[0x80008a44]:sw t5, 240(ra)
	-[0x80008a48]:sw t6, 248(ra)
Current Store : [0x80008a48] : sw t6, 248(ra) -- Store: [0x800137a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008a3c]:fmul.d t5, t3, s10, dyn
	-[0x80008a40]:csrrs a6, fcsr, zero
	-[0x80008a44]:sw t5, 240(ra)
	-[0x80008a48]:sw t6, 248(ra)
	-[0x80008a4c]:sw t5, 256(ra)
Current Store : [0x80008a4c] : sw t5, 256(ra) -- Store: [0x800137a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008abc]:fmul.d t5, t3, s10, dyn
	-[0x80008ac0]:csrrs a6, fcsr, zero
	-[0x80008ac4]:sw t5, 272(ra)
	-[0x80008ac8]:sw t6, 280(ra)
Current Store : [0x80008ac8] : sw t6, 280(ra) -- Store: [0x800137c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008abc]:fmul.d t5, t3, s10, dyn
	-[0x80008ac0]:csrrs a6, fcsr, zero
	-[0x80008ac4]:sw t5, 272(ra)
	-[0x80008ac8]:sw t6, 280(ra)
	-[0x80008acc]:sw t5, 288(ra)
Current Store : [0x80008acc] : sw t5, 288(ra) -- Store: [0x800137c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008b3c]:fmul.d t5, t3, s10, dyn
	-[0x80008b40]:csrrs a6, fcsr, zero
	-[0x80008b44]:sw t5, 304(ra)
	-[0x80008b48]:sw t6, 312(ra)
Current Store : [0x80008b48] : sw t6, 312(ra) -- Store: [0x800137e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008b3c]:fmul.d t5, t3, s10, dyn
	-[0x80008b40]:csrrs a6, fcsr, zero
	-[0x80008b44]:sw t5, 304(ra)
	-[0x80008b48]:sw t6, 312(ra)
	-[0x80008b4c]:sw t5, 320(ra)
Current Store : [0x80008b4c] : sw t5, 320(ra) -- Store: [0x800137e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008bbc]:fmul.d t5, t3, s10, dyn
	-[0x80008bc0]:csrrs a6, fcsr, zero
	-[0x80008bc4]:sw t5, 336(ra)
	-[0x80008bc8]:sw t6, 344(ra)
Current Store : [0x80008bc8] : sw t6, 344(ra) -- Store: [0x80013800]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008bbc]:fmul.d t5, t3, s10, dyn
	-[0x80008bc0]:csrrs a6, fcsr, zero
	-[0x80008bc4]:sw t5, 336(ra)
	-[0x80008bc8]:sw t6, 344(ra)
	-[0x80008bcc]:sw t5, 352(ra)
Current Store : [0x80008bcc] : sw t5, 352(ra) -- Store: [0x80013808]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008c3c]:fmul.d t5, t3, s10, dyn
	-[0x80008c40]:csrrs a6, fcsr, zero
	-[0x80008c44]:sw t5, 368(ra)
	-[0x80008c48]:sw t6, 376(ra)
Current Store : [0x80008c48] : sw t6, 376(ra) -- Store: [0x80013820]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008c3c]:fmul.d t5, t3, s10, dyn
	-[0x80008c40]:csrrs a6, fcsr, zero
	-[0x80008c44]:sw t5, 368(ra)
	-[0x80008c48]:sw t6, 376(ra)
	-[0x80008c4c]:sw t5, 384(ra)
Current Store : [0x80008c4c] : sw t5, 384(ra) -- Store: [0x80013828]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008cbc]:fmul.d t5, t3, s10, dyn
	-[0x80008cc0]:csrrs a6, fcsr, zero
	-[0x80008cc4]:sw t5, 400(ra)
	-[0x80008cc8]:sw t6, 408(ra)
Current Store : [0x80008cc8] : sw t6, 408(ra) -- Store: [0x80013840]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008cbc]:fmul.d t5, t3, s10, dyn
	-[0x80008cc0]:csrrs a6, fcsr, zero
	-[0x80008cc4]:sw t5, 400(ra)
	-[0x80008cc8]:sw t6, 408(ra)
	-[0x80008ccc]:sw t5, 416(ra)
Current Store : [0x80008ccc] : sw t5, 416(ra) -- Store: [0x80013848]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008d3c]:fmul.d t5, t3, s10, dyn
	-[0x80008d40]:csrrs a6, fcsr, zero
	-[0x80008d44]:sw t5, 432(ra)
	-[0x80008d48]:sw t6, 440(ra)
Current Store : [0x80008d48] : sw t6, 440(ra) -- Store: [0x80013860]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008d3c]:fmul.d t5, t3, s10, dyn
	-[0x80008d40]:csrrs a6, fcsr, zero
	-[0x80008d44]:sw t5, 432(ra)
	-[0x80008d48]:sw t6, 440(ra)
	-[0x80008d4c]:sw t5, 448(ra)
Current Store : [0x80008d4c] : sw t5, 448(ra) -- Store: [0x80013868]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008dbc]:fmul.d t5, t3, s10, dyn
	-[0x80008dc0]:csrrs a6, fcsr, zero
	-[0x80008dc4]:sw t5, 464(ra)
	-[0x80008dc8]:sw t6, 472(ra)
Current Store : [0x80008dc8] : sw t6, 472(ra) -- Store: [0x80013880]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008dbc]:fmul.d t5, t3, s10, dyn
	-[0x80008dc0]:csrrs a6, fcsr, zero
	-[0x80008dc4]:sw t5, 464(ra)
	-[0x80008dc8]:sw t6, 472(ra)
	-[0x80008dcc]:sw t5, 480(ra)
Current Store : [0x80008dcc] : sw t5, 480(ra) -- Store: [0x80013888]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008e3c]:fmul.d t5, t3, s10, dyn
	-[0x80008e40]:csrrs a6, fcsr, zero
	-[0x80008e44]:sw t5, 496(ra)
	-[0x80008e48]:sw t6, 504(ra)
Current Store : [0x80008e48] : sw t6, 504(ra) -- Store: [0x800138a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008e3c]:fmul.d t5, t3, s10, dyn
	-[0x80008e40]:csrrs a6, fcsr, zero
	-[0x80008e44]:sw t5, 496(ra)
	-[0x80008e48]:sw t6, 504(ra)
	-[0x80008e4c]:sw t5, 512(ra)
Current Store : [0x80008e4c] : sw t5, 512(ra) -- Store: [0x800138a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008ebc]:fmul.d t5, t3, s10, dyn
	-[0x80008ec0]:csrrs a6, fcsr, zero
	-[0x80008ec4]:sw t5, 528(ra)
	-[0x80008ec8]:sw t6, 536(ra)
Current Store : [0x80008ec8] : sw t6, 536(ra) -- Store: [0x800138c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008ebc]:fmul.d t5, t3, s10, dyn
	-[0x80008ec0]:csrrs a6, fcsr, zero
	-[0x80008ec4]:sw t5, 528(ra)
	-[0x80008ec8]:sw t6, 536(ra)
	-[0x80008ecc]:sw t5, 544(ra)
Current Store : [0x80008ecc] : sw t5, 544(ra) -- Store: [0x800138c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008f3c]:fmul.d t5, t3, s10, dyn
	-[0x80008f40]:csrrs a6, fcsr, zero
	-[0x80008f44]:sw t5, 560(ra)
	-[0x80008f48]:sw t6, 568(ra)
Current Store : [0x80008f48] : sw t6, 568(ra) -- Store: [0x800138e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008f3c]:fmul.d t5, t3, s10, dyn
	-[0x80008f40]:csrrs a6, fcsr, zero
	-[0x80008f44]:sw t5, 560(ra)
	-[0x80008f48]:sw t6, 568(ra)
	-[0x80008f4c]:sw t5, 576(ra)
Current Store : [0x80008f4c] : sw t5, 576(ra) -- Store: [0x800138e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008fbc]:fmul.d t5, t3, s10, dyn
	-[0x80008fc0]:csrrs a6, fcsr, zero
	-[0x80008fc4]:sw t5, 592(ra)
	-[0x80008fc8]:sw t6, 600(ra)
Current Store : [0x80008fc8] : sw t6, 600(ra) -- Store: [0x80013900]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008fbc]:fmul.d t5, t3, s10, dyn
	-[0x80008fc0]:csrrs a6, fcsr, zero
	-[0x80008fc4]:sw t5, 592(ra)
	-[0x80008fc8]:sw t6, 600(ra)
	-[0x80008fcc]:sw t5, 608(ra)
Current Store : [0x80008fcc] : sw t5, 608(ra) -- Store: [0x80013908]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000903c]:fmul.d t5, t3, s10, dyn
	-[0x80009040]:csrrs a6, fcsr, zero
	-[0x80009044]:sw t5, 624(ra)
	-[0x80009048]:sw t6, 632(ra)
Current Store : [0x80009048] : sw t6, 632(ra) -- Store: [0x80013920]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000903c]:fmul.d t5, t3, s10, dyn
	-[0x80009040]:csrrs a6, fcsr, zero
	-[0x80009044]:sw t5, 624(ra)
	-[0x80009048]:sw t6, 632(ra)
	-[0x8000904c]:sw t5, 640(ra)
Current Store : [0x8000904c] : sw t5, 640(ra) -- Store: [0x80013928]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800090c0]:fmul.d t5, t3, s10, dyn
	-[0x800090c4]:csrrs a6, fcsr, zero
	-[0x800090c8]:sw t5, 656(ra)
	-[0x800090cc]:sw t6, 664(ra)
Current Store : [0x800090cc] : sw t6, 664(ra) -- Store: [0x80013940]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800090c0]:fmul.d t5, t3, s10, dyn
	-[0x800090c4]:csrrs a6, fcsr, zero
	-[0x800090c8]:sw t5, 656(ra)
	-[0x800090cc]:sw t6, 664(ra)
	-[0x800090d0]:sw t5, 672(ra)
Current Store : [0x800090d0] : sw t5, 672(ra) -- Store: [0x80013948]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009144]:fmul.d t5, t3, s10, dyn
	-[0x80009148]:csrrs a6, fcsr, zero
	-[0x8000914c]:sw t5, 688(ra)
	-[0x80009150]:sw t6, 696(ra)
Current Store : [0x80009150] : sw t6, 696(ra) -- Store: [0x80013960]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009144]:fmul.d t5, t3, s10, dyn
	-[0x80009148]:csrrs a6, fcsr, zero
	-[0x8000914c]:sw t5, 688(ra)
	-[0x80009150]:sw t6, 696(ra)
	-[0x80009154]:sw t5, 704(ra)
Current Store : [0x80009154] : sw t5, 704(ra) -- Store: [0x80013968]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800091c4]:fmul.d t5, t3, s10, dyn
	-[0x800091c8]:csrrs a6, fcsr, zero
	-[0x800091cc]:sw t5, 720(ra)
	-[0x800091d0]:sw t6, 728(ra)
Current Store : [0x800091d0] : sw t6, 728(ra) -- Store: [0x80013980]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800091c4]:fmul.d t5, t3, s10, dyn
	-[0x800091c8]:csrrs a6, fcsr, zero
	-[0x800091cc]:sw t5, 720(ra)
	-[0x800091d0]:sw t6, 728(ra)
	-[0x800091d4]:sw t5, 736(ra)
Current Store : [0x800091d4] : sw t5, 736(ra) -- Store: [0x80013988]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009244]:fmul.d t5, t3, s10, dyn
	-[0x80009248]:csrrs a6, fcsr, zero
	-[0x8000924c]:sw t5, 752(ra)
	-[0x80009250]:sw t6, 760(ra)
Current Store : [0x80009250] : sw t6, 760(ra) -- Store: [0x800139a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009244]:fmul.d t5, t3, s10, dyn
	-[0x80009248]:csrrs a6, fcsr, zero
	-[0x8000924c]:sw t5, 752(ra)
	-[0x80009250]:sw t6, 760(ra)
	-[0x80009254]:sw t5, 768(ra)
Current Store : [0x80009254] : sw t5, 768(ra) -- Store: [0x800139a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800092c4]:fmul.d t5, t3, s10, dyn
	-[0x800092c8]:csrrs a6, fcsr, zero
	-[0x800092cc]:sw t5, 784(ra)
	-[0x800092d0]:sw t6, 792(ra)
Current Store : [0x800092d0] : sw t6, 792(ra) -- Store: [0x800139c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800092c4]:fmul.d t5, t3, s10, dyn
	-[0x800092c8]:csrrs a6, fcsr, zero
	-[0x800092cc]:sw t5, 784(ra)
	-[0x800092d0]:sw t6, 792(ra)
	-[0x800092d4]:sw t5, 800(ra)
Current Store : [0x800092d4] : sw t5, 800(ra) -- Store: [0x800139c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009344]:fmul.d t5, t3, s10, dyn
	-[0x80009348]:csrrs a6, fcsr, zero
	-[0x8000934c]:sw t5, 816(ra)
	-[0x80009350]:sw t6, 824(ra)
Current Store : [0x80009350] : sw t6, 824(ra) -- Store: [0x800139e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009344]:fmul.d t5, t3, s10, dyn
	-[0x80009348]:csrrs a6, fcsr, zero
	-[0x8000934c]:sw t5, 816(ra)
	-[0x80009350]:sw t6, 824(ra)
	-[0x80009354]:sw t5, 832(ra)
Current Store : [0x80009354] : sw t5, 832(ra) -- Store: [0x800139e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800093c8]:fmul.d t5, t3, s10, dyn
	-[0x800093cc]:csrrs a6, fcsr, zero
	-[0x800093d0]:sw t5, 848(ra)
	-[0x800093d4]:sw t6, 856(ra)
Current Store : [0x800093d4] : sw t6, 856(ra) -- Store: [0x80013a00]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800093c8]:fmul.d t5, t3, s10, dyn
	-[0x800093cc]:csrrs a6, fcsr, zero
	-[0x800093d0]:sw t5, 848(ra)
	-[0x800093d4]:sw t6, 856(ra)
	-[0x800093d8]:sw t5, 864(ra)
Current Store : [0x800093d8] : sw t5, 864(ra) -- Store: [0x80013a08]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000944c]:fmul.d t5, t3, s10, dyn
	-[0x80009450]:csrrs a6, fcsr, zero
	-[0x80009454]:sw t5, 880(ra)
	-[0x80009458]:sw t6, 888(ra)
Current Store : [0x80009458] : sw t6, 888(ra) -- Store: [0x80013a20]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000944c]:fmul.d t5, t3, s10, dyn
	-[0x80009450]:csrrs a6, fcsr, zero
	-[0x80009454]:sw t5, 880(ra)
	-[0x80009458]:sw t6, 888(ra)
	-[0x8000945c]:sw t5, 896(ra)
Current Store : [0x8000945c] : sw t5, 896(ra) -- Store: [0x80013a28]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800094cc]:fmul.d t5, t3, s10, dyn
	-[0x800094d0]:csrrs a6, fcsr, zero
	-[0x800094d4]:sw t5, 912(ra)
	-[0x800094d8]:sw t6, 920(ra)
Current Store : [0x800094d8] : sw t6, 920(ra) -- Store: [0x80013a40]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c6dc]:fmul.d t5, t3, s10, dyn
	-[0x8000c6e0]:csrrs a6, fcsr, zero
	-[0x8000c6e4]:sw t5, 0(ra)
	-[0x8000c6e8]:sw t6, 8(ra)
Current Store : [0x8000c6e8] : sw t6, 8(ra) -- Store: [0x800136c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c6dc]:fmul.d t5, t3, s10, dyn
	-[0x8000c6e0]:csrrs a6, fcsr, zero
	-[0x8000c6e4]:sw t5, 0(ra)
	-[0x8000c6e8]:sw t6, 8(ra)
	-[0x8000c6ec]:sw t5, 16(ra)
Current Store : [0x8000c6ec] : sw t5, 16(ra) -- Store: [0x800136c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c71c]:fmul.d t5, t3, s10, dyn
	-[0x8000c720]:csrrs a6, fcsr, zero
	-[0x8000c724]:sw t5, 32(ra)
	-[0x8000c728]:sw t6, 40(ra)
Current Store : [0x8000c728] : sw t6, 40(ra) -- Store: [0x800136e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c71c]:fmul.d t5, t3, s10, dyn
	-[0x8000c720]:csrrs a6, fcsr, zero
	-[0x8000c724]:sw t5, 32(ra)
	-[0x8000c728]:sw t6, 40(ra)
	-[0x8000c72c]:sw t5, 48(ra)
Current Store : [0x8000c72c] : sw t5, 48(ra) -- Store: [0x800136e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c75c]:fmul.d t5, t3, s10, dyn
	-[0x8000c760]:csrrs a6, fcsr, zero
	-[0x8000c764]:sw t5, 64(ra)
	-[0x8000c768]:sw t6, 72(ra)
Current Store : [0x8000c768] : sw t6, 72(ra) -- Store: [0x80013700]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c75c]:fmul.d t5, t3, s10, dyn
	-[0x8000c760]:csrrs a6, fcsr, zero
	-[0x8000c764]:sw t5, 64(ra)
	-[0x8000c768]:sw t6, 72(ra)
	-[0x8000c76c]:sw t5, 80(ra)
Current Store : [0x8000c76c] : sw t5, 80(ra) -- Store: [0x80013708]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c79c]:fmul.d t5, t3, s10, dyn
	-[0x8000c7a0]:csrrs a6, fcsr, zero
	-[0x8000c7a4]:sw t5, 96(ra)
	-[0x8000c7a8]:sw t6, 104(ra)
Current Store : [0x8000c7a8] : sw t6, 104(ra) -- Store: [0x80013720]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c79c]:fmul.d t5, t3, s10, dyn
	-[0x8000c7a0]:csrrs a6, fcsr, zero
	-[0x8000c7a4]:sw t5, 96(ra)
	-[0x8000c7a8]:sw t6, 104(ra)
	-[0x8000c7ac]:sw t5, 112(ra)
Current Store : [0x8000c7ac] : sw t5, 112(ra) -- Store: [0x80013728]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c7dc]:fmul.d t5, t3, s10, dyn
	-[0x8000c7e0]:csrrs a6, fcsr, zero
	-[0x8000c7e4]:sw t5, 128(ra)
	-[0x8000c7e8]:sw t6, 136(ra)
Current Store : [0x8000c7e8] : sw t6, 136(ra) -- Store: [0x80013740]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c7dc]:fmul.d t5, t3, s10, dyn
	-[0x8000c7e0]:csrrs a6, fcsr, zero
	-[0x8000c7e4]:sw t5, 128(ra)
	-[0x8000c7e8]:sw t6, 136(ra)
	-[0x8000c7ec]:sw t5, 144(ra)
Current Store : [0x8000c7ec] : sw t5, 144(ra) -- Store: [0x80013748]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c81c]:fmul.d t5, t3, s10, dyn
	-[0x8000c820]:csrrs a6, fcsr, zero
	-[0x8000c824]:sw t5, 160(ra)
	-[0x8000c828]:sw t6, 168(ra)
Current Store : [0x8000c828] : sw t6, 168(ra) -- Store: [0x80013760]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c81c]:fmul.d t5, t3, s10, dyn
	-[0x8000c820]:csrrs a6, fcsr, zero
	-[0x8000c824]:sw t5, 160(ra)
	-[0x8000c828]:sw t6, 168(ra)
	-[0x8000c82c]:sw t5, 176(ra)
Current Store : [0x8000c82c] : sw t5, 176(ra) -- Store: [0x80013768]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c85c]:fmul.d t5, t3, s10, dyn
	-[0x8000c860]:csrrs a6, fcsr, zero
	-[0x8000c864]:sw t5, 192(ra)
	-[0x8000c868]:sw t6, 200(ra)
Current Store : [0x8000c868] : sw t6, 200(ra) -- Store: [0x80013780]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c85c]:fmul.d t5, t3, s10, dyn
	-[0x8000c860]:csrrs a6, fcsr, zero
	-[0x8000c864]:sw t5, 192(ra)
	-[0x8000c868]:sw t6, 200(ra)
	-[0x8000c86c]:sw t5, 208(ra)
Current Store : [0x8000c86c] : sw t5, 208(ra) -- Store: [0x80013788]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c89c]:fmul.d t5, t3, s10, dyn
	-[0x8000c8a0]:csrrs a6, fcsr, zero
	-[0x8000c8a4]:sw t5, 224(ra)
	-[0x8000c8a8]:sw t6, 232(ra)
Current Store : [0x8000c8a8] : sw t6, 232(ra) -- Store: [0x800137a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c89c]:fmul.d t5, t3, s10, dyn
	-[0x8000c8a0]:csrrs a6, fcsr, zero
	-[0x8000c8a4]:sw t5, 224(ra)
	-[0x8000c8a8]:sw t6, 232(ra)
	-[0x8000c8ac]:sw t5, 240(ra)
Current Store : [0x8000c8ac] : sw t5, 240(ra) -- Store: [0x800137a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c8dc]:fmul.d t5, t3, s10, dyn
	-[0x8000c8e0]:csrrs a6, fcsr, zero
	-[0x8000c8e4]:sw t5, 256(ra)
	-[0x8000c8e8]:sw t6, 264(ra)
Current Store : [0x8000c8e8] : sw t6, 264(ra) -- Store: [0x800137c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c8dc]:fmul.d t5, t3, s10, dyn
	-[0x8000c8e0]:csrrs a6, fcsr, zero
	-[0x8000c8e4]:sw t5, 256(ra)
	-[0x8000c8e8]:sw t6, 264(ra)
	-[0x8000c8ec]:sw t5, 272(ra)
Current Store : [0x8000c8ec] : sw t5, 272(ra) -- Store: [0x800137c8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c91c]:fmul.d t5, t3, s10, dyn
	-[0x8000c920]:csrrs a6, fcsr, zero
	-[0x8000c924]:sw t5, 288(ra)
	-[0x8000c928]:sw t6, 296(ra)
Current Store : [0x8000c928] : sw t6, 296(ra) -- Store: [0x800137e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c91c]:fmul.d t5, t3, s10, dyn
	-[0x8000c920]:csrrs a6, fcsr, zero
	-[0x8000c924]:sw t5, 288(ra)
	-[0x8000c928]:sw t6, 296(ra)
	-[0x8000c92c]:sw t5, 304(ra)
Current Store : [0x8000c92c] : sw t5, 304(ra) -- Store: [0x800137e8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c95c]:fmul.d t5, t3, s10, dyn
	-[0x8000c960]:csrrs a6, fcsr, zero
	-[0x8000c964]:sw t5, 320(ra)
	-[0x8000c968]:sw t6, 328(ra)
Current Store : [0x8000c968] : sw t6, 328(ra) -- Store: [0x80013800]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c95c]:fmul.d t5, t3, s10, dyn
	-[0x8000c960]:csrrs a6, fcsr, zero
	-[0x8000c964]:sw t5, 320(ra)
	-[0x8000c968]:sw t6, 328(ra)
	-[0x8000c96c]:sw t5, 336(ra)
Current Store : [0x8000c96c] : sw t5, 336(ra) -- Store: [0x80013808]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c99c]:fmul.d t5, t3, s10, dyn
	-[0x8000c9a0]:csrrs a6, fcsr, zero
	-[0x8000c9a4]:sw t5, 352(ra)
	-[0x8000c9a8]:sw t6, 360(ra)
Current Store : [0x8000c9a8] : sw t6, 360(ra) -- Store: [0x80013820]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c99c]:fmul.d t5, t3, s10, dyn
	-[0x8000c9a0]:csrrs a6, fcsr, zero
	-[0x8000c9a4]:sw t5, 352(ra)
	-[0x8000c9a8]:sw t6, 360(ra)
	-[0x8000c9ac]:sw t5, 368(ra)
Current Store : [0x8000c9ac] : sw t5, 368(ra) -- Store: [0x80013828]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c9e0]:fmul.d t5, t3, s10, dyn
	-[0x8000c9e4]:csrrs a6, fcsr, zero
	-[0x8000c9e8]:sw t5, 384(ra)
	-[0x8000c9ec]:sw t6, 392(ra)
Current Store : [0x8000c9ec] : sw t6, 392(ra) -- Store: [0x80013840]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c9e0]:fmul.d t5, t3, s10, dyn
	-[0x8000c9e4]:csrrs a6, fcsr, zero
	-[0x8000c9e8]:sw t5, 384(ra)
	-[0x8000c9ec]:sw t6, 392(ra)
	-[0x8000c9f0]:sw t5, 400(ra)
Current Store : [0x8000c9f0] : sw t5, 400(ra) -- Store: [0x80013848]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ca24]:fmul.d t5, t3, s10, dyn
	-[0x8000ca28]:csrrs a6, fcsr, zero
	-[0x8000ca2c]:sw t5, 416(ra)
	-[0x8000ca30]:sw t6, 424(ra)
Current Store : [0x8000ca30] : sw t6, 424(ra) -- Store: [0x80013860]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ca24]:fmul.d t5, t3, s10, dyn
	-[0x8000ca28]:csrrs a6, fcsr, zero
	-[0x8000ca2c]:sw t5, 416(ra)
	-[0x8000ca30]:sw t6, 424(ra)
	-[0x8000ca34]:sw t5, 432(ra)
Current Store : [0x8000ca34] : sw t5, 432(ra) -- Store: [0x80013868]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ca64]:fmul.d t5, t3, s10, dyn
	-[0x8000ca68]:csrrs a6, fcsr, zero
	-[0x8000ca6c]:sw t5, 448(ra)
	-[0x8000ca70]:sw t6, 456(ra)
Current Store : [0x8000ca70] : sw t6, 456(ra) -- Store: [0x80013880]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ca64]:fmul.d t5, t3, s10, dyn
	-[0x8000ca68]:csrrs a6, fcsr, zero
	-[0x8000ca6c]:sw t5, 448(ra)
	-[0x8000ca70]:sw t6, 456(ra)
	-[0x8000ca74]:sw t5, 464(ra)
Current Store : [0x8000ca74] : sw t5, 464(ra) -- Store: [0x80013888]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000caa4]:fmul.d t5, t3, s10, dyn
	-[0x8000caa8]:csrrs a6, fcsr, zero
	-[0x8000caac]:sw t5, 480(ra)
	-[0x8000cab0]:sw t6, 488(ra)
Current Store : [0x8000cab0] : sw t6, 488(ra) -- Store: [0x800138a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000caa4]:fmul.d t5, t3, s10, dyn
	-[0x8000caa8]:csrrs a6, fcsr, zero
	-[0x8000caac]:sw t5, 480(ra)
	-[0x8000cab0]:sw t6, 488(ra)
	-[0x8000cab4]:sw t5, 496(ra)
Current Store : [0x8000cab4] : sw t5, 496(ra) -- Store: [0x800138a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cae4]:fmul.d t5, t3, s10, dyn
	-[0x8000cae8]:csrrs a6, fcsr, zero
	-[0x8000caec]:sw t5, 512(ra)
	-[0x8000caf0]:sw t6, 520(ra)
Current Store : [0x8000caf0] : sw t6, 520(ra) -- Store: [0x800138c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cae4]:fmul.d t5, t3, s10, dyn
	-[0x8000cae8]:csrrs a6, fcsr, zero
	-[0x8000caec]:sw t5, 512(ra)
	-[0x8000caf0]:sw t6, 520(ra)
	-[0x8000caf4]:sw t5, 528(ra)
Current Store : [0x8000caf4] : sw t5, 528(ra) -- Store: [0x800138c8]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cb24]:fmul.d t5, t3, s10, dyn
	-[0x8000cb28]:csrrs a6, fcsr, zero
	-[0x8000cb2c]:sw t5, 544(ra)
	-[0x8000cb30]:sw t6, 552(ra)
Current Store : [0x8000cb30] : sw t6, 552(ra) -- Store: [0x800138e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cb24]:fmul.d t5, t3, s10, dyn
	-[0x8000cb28]:csrrs a6, fcsr, zero
	-[0x8000cb2c]:sw t5, 544(ra)
	-[0x8000cb30]:sw t6, 552(ra)
	-[0x8000cb34]:sw t5, 560(ra)
Current Store : [0x8000cb34] : sw t5, 560(ra) -- Store: [0x800138e8]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cb68]:fmul.d t5, t3, s10, dyn
	-[0x8000cb6c]:csrrs a6, fcsr, zero
	-[0x8000cb70]:sw t5, 576(ra)
	-[0x8000cb74]:sw t6, 584(ra)
Current Store : [0x8000cb74] : sw t6, 584(ra) -- Store: [0x80013900]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cb68]:fmul.d t5, t3, s10, dyn
	-[0x8000cb6c]:csrrs a6, fcsr, zero
	-[0x8000cb70]:sw t5, 576(ra)
	-[0x8000cb74]:sw t6, 584(ra)
	-[0x8000cb78]:sw t5, 592(ra)
Current Store : [0x8000cb78] : sw t5, 592(ra) -- Store: [0x80013908]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cbac]:fmul.d t5, t3, s10, dyn
	-[0x8000cbb0]:csrrs a6, fcsr, zero
	-[0x8000cbb4]:sw t5, 608(ra)
	-[0x8000cbb8]:sw t6, 616(ra)
Current Store : [0x8000cbb8] : sw t6, 616(ra) -- Store: [0x80013920]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cbac]:fmul.d t5, t3, s10, dyn
	-[0x8000cbb0]:csrrs a6, fcsr, zero
	-[0x8000cbb4]:sw t5, 608(ra)
	-[0x8000cbb8]:sw t6, 616(ra)
	-[0x8000cbbc]:sw t5, 624(ra)
Current Store : [0x8000cbbc] : sw t5, 624(ra) -- Store: [0x80013928]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cbec]:fmul.d t5, t3, s10, dyn
	-[0x8000cbf0]:csrrs a6, fcsr, zero
	-[0x8000cbf4]:sw t5, 640(ra)
	-[0x8000cbf8]:sw t6, 648(ra)
Current Store : [0x8000cbf8] : sw t6, 648(ra) -- Store: [0x80013940]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cbec]:fmul.d t5, t3, s10, dyn
	-[0x8000cbf0]:csrrs a6, fcsr, zero
	-[0x8000cbf4]:sw t5, 640(ra)
	-[0x8000cbf8]:sw t6, 648(ra)
	-[0x8000cbfc]:sw t5, 656(ra)
Current Store : [0x8000cbfc] : sw t5, 656(ra) -- Store: [0x80013948]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cc2c]:fmul.d t5, t3, s10, dyn
	-[0x8000cc30]:csrrs a6, fcsr, zero
	-[0x8000cc34]:sw t5, 672(ra)
	-[0x8000cc38]:sw t6, 680(ra)
Current Store : [0x8000cc38] : sw t6, 680(ra) -- Store: [0x80013960]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cc2c]:fmul.d t5, t3, s10, dyn
	-[0x8000cc30]:csrrs a6, fcsr, zero
	-[0x8000cc34]:sw t5, 672(ra)
	-[0x8000cc38]:sw t6, 680(ra)
	-[0x8000cc3c]:sw t5, 688(ra)
Current Store : [0x8000cc3c] : sw t5, 688(ra) -- Store: [0x80013968]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cc6c]:fmul.d t5, t3, s10, dyn
	-[0x8000cc70]:csrrs a6, fcsr, zero
	-[0x8000cc74]:sw t5, 704(ra)
	-[0x8000cc78]:sw t6, 712(ra)
Current Store : [0x8000cc78] : sw t6, 712(ra) -- Store: [0x80013980]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cc6c]:fmul.d t5, t3, s10, dyn
	-[0x8000cc70]:csrrs a6, fcsr, zero
	-[0x8000cc74]:sw t5, 704(ra)
	-[0x8000cc78]:sw t6, 712(ra)
	-[0x8000cc7c]:sw t5, 720(ra)
Current Store : [0x8000cc7c] : sw t5, 720(ra) -- Store: [0x80013988]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ccac]:fmul.d t5, t3, s10, dyn
	-[0x8000ccb0]:csrrs a6, fcsr, zero
	-[0x8000ccb4]:sw t5, 736(ra)
	-[0x8000ccb8]:sw t6, 744(ra)
Current Store : [0x8000ccb8] : sw t6, 744(ra) -- Store: [0x800139a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ccac]:fmul.d t5, t3, s10, dyn
	-[0x8000ccb0]:csrrs a6, fcsr, zero
	-[0x8000ccb4]:sw t5, 736(ra)
	-[0x8000ccb8]:sw t6, 744(ra)
	-[0x8000ccbc]:sw t5, 752(ra)
Current Store : [0x8000ccbc] : sw t5, 752(ra) -- Store: [0x800139a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ccec]:fmul.d t5, t3, s10, dyn
	-[0x8000ccf0]:csrrs a6, fcsr, zero
	-[0x8000ccf4]:sw t5, 768(ra)
	-[0x8000ccf8]:sw t6, 776(ra)
Current Store : [0x8000ccf8] : sw t6, 776(ra) -- Store: [0x800139c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ccec]:fmul.d t5, t3, s10, dyn
	-[0x8000ccf0]:csrrs a6, fcsr, zero
	-[0x8000ccf4]:sw t5, 768(ra)
	-[0x8000ccf8]:sw t6, 776(ra)
	-[0x8000ccfc]:sw t5, 784(ra)
Current Store : [0x8000ccfc] : sw t5, 784(ra) -- Store: [0x800139c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cd2c]:fmul.d t5, t3, s10, dyn
	-[0x8000cd30]:csrrs a6, fcsr, zero
	-[0x8000cd34]:sw t5, 800(ra)
	-[0x8000cd38]:sw t6, 808(ra)
Current Store : [0x8000cd38] : sw t6, 808(ra) -- Store: [0x800139e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cd2c]:fmul.d t5, t3, s10, dyn
	-[0x8000cd30]:csrrs a6, fcsr, zero
	-[0x8000cd34]:sw t5, 800(ra)
	-[0x8000cd38]:sw t6, 808(ra)
	-[0x8000cd3c]:sw t5, 816(ra)
Current Store : [0x8000cd3c] : sw t5, 816(ra) -- Store: [0x800139e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cd6c]:fmul.d t5, t3, s10, dyn
	-[0x8000cd70]:csrrs a6, fcsr, zero
	-[0x8000cd74]:sw t5, 832(ra)
	-[0x8000cd78]:sw t6, 840(ra)
Current Store : [0x8000cd78] : sw t6, 840(ra) -- Store: [0x80013a00]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cd6c]:fmul.d t5, t3, s10, dyn
	-[0x8000cd70]:csrrs a6, fcsr, zero
	-[0x8000cd74]:sw t5, 832(ra)
	-[0x8000cd78]:sw t6, 840(ra)
	-[0x8000cd7c]:sw t5, 848(ra)
Current Store : [0x8000cd7c] : sw t5, 848(ra) -- Store: [0x80013a08]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cdac]:fmul.d t5, t3, s10, dyn
	-[0x8000cdb0]:csrrs a6, fcsr, zero
	-[0x8000cdb4]:sw t5, 864(ra)
	-[0x8000cdb8]:sw t6, 872(ra)
Current Store : [0x8000cdb8] : sw t6, 872(ra) -- Store: [0x80013a20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cdac]:fmul.d t5, t3, s10, dyn
	-[0x8000cdb0]:csrrs a6, fcsr, zero
	-[0x8000cdb4]:sw t5, 864(ra)
	-[0x8000cdb8]:sw t6, 872(ra)
	-[0x8000cdbc]:sw t5, 880(ra)
Current Store : [0x8000cdbc] : sw t5, 880(ra) -- Store: [0x80013a28]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cdec]:fmul.d t5, t3, s10, dyn
	-[0x8000cdf0]:csrrs a6, fcsr, zero
	-[0x8000cdf4]:sw t5, 896(ra)
	-[0x8000cdf8]:sw t6, 904(ra)
Current Store : [0x8000cdf8] : sw t6, 904(ra) -- Store: [0x80013a40]:0x00000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                          signature                           |                                                                                                                                    coverpoints                                                                                                                                    |                                                                                                                       code                                                                                                                        |
|---:|--------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80011618]<br>0x00000000<br> [0x80011630]<br>0x00000000<br> |- mnemonic : fmul.d<br> - rs1 : x28<br> - rs2 : x26<br> - rd : x30<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x8000013c]:fmul.d t5, t3, s10, dyn<br> [0x80000140]:csrrs tp, fcsr, zero<br> [0x80000144]:sw t5, 0(ra)<br> [0x80000148]:sw t6, 8(ra)<br> [0x8000014c]:sw t5, 16(ra)<br> [0x80000150]:sw tp, 24(ra)<br>                                           |
|   2|[0x80011638]<br>0x00000000<br> [0x80011650]<br>0x00000000<br> |- rs1 : x26<br> - rs2 : x30<br> - rd : x26<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x8000017c]:fmul.d s10, s10, t5, dyn<br> [0x80000180]:csrrs tp, fcsr, zero<br> [0x80000184]:sw s10, 32(ra)<br> [0x80000188]:sw s11, 40(ra)<br> [0x8000018c]:sw s10, 48(ra)<br> [0x80000190]:sw tp, 56(ra)<br>                                     |
|   3|[0x80011658]<br>0x00000000<br> [0x80011670]<br>0x00000000<br> |- rs1 : x24<br> - rs2 : x24<br> - rd : x24<br> - rs1 == rs2 == rd<br>                                                                                                                                                                                                              |[0x800001bc]:fmul.d s8, s8, s8, dyn<br> [0x800001c0]:csrrs tp, fcsr, zero<br> [0x800001c4]:sw s8, 64(ra)<br> [0x800001c8]:sw s9, 72(ra)<br> [0x800001cc]:sw s8, 80(ra)<br> [0x800001d0]:sw tp, 88(ra)<br>                                          |
|   4|[0x80011678]<br>0x00000000<br> [0x80011690]<br>0x00000000<br> |- rs1 : x22<br> - rs2 : x22<br> - rd : x28<br> - rs1 == rs2 != rd<br>                                                                                                                                                                                                              |[0x800001fc]:fmul.d t3, s6, s6, dyn<br> [0x80000200]:csrrs tp, fcsr, zero<br> [0x80000204]:sw t3, 96(ra)<br> [0x80000208]:sw t4, 104(ra)<br> [0x8000020c]:sw t3, 112(ra)<br> [0x80000210]:sw tp, 120(ra)<br>                                       |
|   5|[0x80011698]<br>0x00000000<br> [0x800116b0]<br>0x00000000<br> |- rs1 : x30<br> - rs2 : x20<br> - rd : x20<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x8000023c]:fmul.d s4, t5, s4, dyn<br> [0x80000240]:csrrs tp, fcsr, zero<br> [0x80000244]:sw s4, 128(ra)<br> [0x80000248]:sw s5, 136(ra)<br> [0x8000024c]:sw s4, 144(ra)<br> [0x80000250]:sw tp, 152(ra)<br>                                      |
|   6|[0x800116b8]<br>0x00000000<br> [0x800116d0]<br>0x00000000<br> |- rs1 : x20<br> - rs2 : x28<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                       |[0x8000027c]:fmul.d s6, s4, t3, dyn<br> [0x80000280]:csrrs tp, fcsr, zero<br> [0x80000284]:sw s6, 160(ra)<br> [0x80000288]:sw s7, 168(ra)<br> [0x8000028c]:sw s6, 176(ra)<br> [0x80000290]:sw tp, 184(ra)<br>                                      |
|   7|[0x800116d8]<br>0x00000000<br> [0x800116f0]<br>0x00000000<br> |- rs1 : x16<br> - rs2 : x14<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                       |[0x800002c0]:fmul.d s2, a6, a4, dyn<br> [0x800002c4]:csrrs tp, fcsr, zero<br> [0x800002c8]:sw s2, 192(ra)<br> [0x800002cc]:sw s3, 200(ra)<br> [0x800002d0]:sw s2, 208(ra)<br> [0x800002d4]:sw tp, 216(ra)<br>                                      |
|   8|[0x800116f8]<br>0x00000000<br> [0x80011710]<br>0x00000000<br> |- rs1 : x14<br> - rs2 : x18<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                       |[0x80000304]:fmul.d a6, a4, s2, dyn<br> [0x80000308]:csrrs tp, fcsr, zero<br> [0x8000030c]:sw a6, 224(ra)<br> [0x80000310]:sw a7, 232(ra)<br> [0x80000314]:sw a6, 240(ra)<br> [0x80000318]:sw tp, 248(ra)<br>                                      |
|   9|[0x80011718]<br>0x00000000<br> [0x80011730]<br>0x00000000<br> |- rs1 : x18<br> - rs2 : x16<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                       |[0x80000344]:fmul.d a4, s2, a6, dyn<br> [0x80000348]:csrrs tp, fcsr, zero<br> [0x8000034c]:sw a4, 256(ra)<br> [0x80000350]:sw a5, 264(ra)<br> [0x80000354]:sw a4, 272(ra)<br> [0x80000358]:sw tp, 280(ra)<br>                                      |
|  10|[0x80011738]<br>0x00000000<br> [0x80011750]<br>0x00000000<br> |- rs1 : x10<br> - rs2 : x8<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                        |[0x80000384]:fmul.d a2, a0, fp, dyn<br> [0x80000388]:csrrs tp, fcsr, zero<br> [0x8000038c]:sw a2, 288(ra)<br> [0x80000390]:sw a3, 296(ra)<br> [0x80000394]:sw a2, 304(ra)<br> [0x80000398]:sw tp, 312(ra)<br>                                      |
|  11|[0x800116b8]<br>0x00000000<br> [0x800116d0]<br>0x00000000<br> |- rs1 : x8<br> - rs2 : x12<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                        |[0x800003d4]:fmul.d a0, fp, a2, dyn<br> [0x800003d8]:csrrs a6, fcsr, zero<br> [0x800003dc]:sw a0, 0(ra)<br> [0x800003e0]:sw a1, 8(ra)<br> [0x800003e4]:sw a0, 16(ra)<br> [0x800003e8]:sw a6, 24(ra)<br>                                            |
|  12|[0x800116d8]<br>0x00000000<br> [0x800116f0]<br>0x00000000<br> |- rs1 : x12<br> - rs2 : x10<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                        |[0x80000414]:fmul.d fp, a2, a0, dyn<br> [0x80000418]:csrrs a6, fcsr, zero<br> [0x8000041c]:sw fp, 32(ra)<br> [0x80000420]:sw s1, 40(ra)<br> [0x80000424]:sw fp, 48(ra)<br> [0x80000428]:sw a6, 56(ra)<br>                                          |
|  13|[0x800116f8]<br>0x00000000<br> [0x80011710]<br>0x00000000<br> |- rs1 : x4<br> - rs2 : x2<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                          |[0x80000458]:fmul.d t1, tp, sp, dyn<br> [0x8000045c]:csrrs a6, fcsr, zero<br> [0x80000460]:sw t1, 64(ra)<br> [0x80000464]:sw t2, 72(ra)<br> [0x80000468]:sw t1, 80(ra)<br> [0x8000046c]:sw a6, 88(ra)<br>                                          |
|  14|[0x80011718]<br>0x00000000<br> [0x80011730]<br>0x00000000<br> |- rs1 : x2<br> - rs2 : x6<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                          |[0x8000049c]:fmul.d tp, sp, t1, dyn<br> [0x800004a0]:csrrs a6, fcsr, zero<br> [0x800004a4]:sw tp, 96(ra)<br> [0x800004a8]:sw t0, 104(ra)<br> [0x800004ac]:sw tp, 112(ra)<br> [0x800004b0]:sw a6, 120(ra)<br>                                       |
|  15|[0x80011738]<br>0x00000000<br> [0x80011750]<br>0x00000010<br> |- rs1 : x6<br> - rs2 : x4<br> - rd : x2<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                          |[0x800004dc]:fmul.d sp, t1, tp, dyn<br> [0x800004e0]:csrrs a6, fcsr, zero<br> [0x800004e4]:sw sp, 128(ra)<br> [0x800004e8]:sw gp, 136(ra)<br> [0x800004ec]:sw sp, 144(ra)<br> [0x800004f0]:sw a6, 152(ra)<br>                                      |
|  16|[0x80011758]<br>0x00000000<br> [0x80011770]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000051c]:fmul.d t5, t3, s10, dyn<br> [0x80000520]:csrrs a6, fcsr, zero<br> [0x80000524]:sw t5, 160(ra)<br> [0x80000528]:sw t6, 168(ra)<br> [0x8000052c]:sw t5, 176(ra)<br> [0x80000530]:sw a6, 184(ra)<br>                                     |
|  17|[0x80011778]<br>0x00000000<br> [0x80011790]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000055c]:fmul.d t5, t3, s10, dyn<br> [0x80000560]:csrrs a6, fcsr, zero<br> [0x80000564]:sw t5, 192(ra)<br> [0x80000568]:sw t6, 200(ra)<br> [0x8000056c]:sw t5, 208(ra)<br> [0x80000570]:sw a6, 216(ra)<br>                                     |
|  18|[0x80011798]<br>0x00000000<br> [0x800117b0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000059c]:fmul.d t5, t3, s10, dyn<br> [0x800005a0]:csrrs a6, fcsr, zero<br> [0x800005a4]:sw t5, 224(ra)<br> [0x800005a8]:sw t6, 232(ra)<br> [0x800005ac]:sw t5, 240(ra)<br> [0x800005b0]:sw a6, 248(ra)<br>                                     |
|  19|[0x800117b8]<br>0x00000000<br> [0x800117d0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800005dc]:fmul.d t5, t3, s10, dyn<br> [0x800005e0]:csrrs a6, fcsr, zero<br> [0x800005e4]:sw t5, 256(ra)<br> [0x800005e8]:sw t6, 264(ra)<br> [0x800005ec]:sw t5, 272(ra)<br> [0x800005f0]:sw a6, 280(ra)<br>                                     |
|  20|[0x800117d8]<br>0x00000000<br> [0x800117f0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000061c]:fmul.d t5, t3, s10, dyn<br> [0x80000620]:csrrs a6, fcsr, zero<br> [0x80000624]:sw t5, 288(ra)<br> [0x80000628]:sw t6, 296(ra)<br> [0x8000062c]:sw t5, 304(ra)<br> [0x80000630]:sw a6, 312(ra)<br>                                     |
|  21|[0x800117f8]<br>0x00000000<br> [0x80011810]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000065c]:fmul.d t5, t3, s10, dyn<br> [0x80000660]:csrrs a6, fcsr, zero<br> [0x80000664]:sw t5, 320(ra)<br> [0x80000668]:sw t6, 328(ra)<br> [0x8000066c]:sw t5, 336(ra)<br> [0x80000670]:sw a6, 344(ra)<br>                                     |
|  22|[0x80011818]<br>0x00000000<br> [0x80011830]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000069c]:fmul.d t5, t3, s10, dyn<br> [0x800006a0]:csrrs a6, fcsr, zero<br> [0x800006a4]:sw t5, 352(ra)<br> [0x800006a8]:sw t6, 360(ra)<br> [0x800006ac]:sw t5, 368(ra)<br> [0x800006b0]:sw a6, 376(ra)<br>                                     |
|  23|[0x80011838]<br>0x00000000<br> [0x80011850]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800006dc]:fmul.d t5, t3, s10, dyn<br> [0x800006e0]:csrrs a6, fcsr, zero<br> [0x800006e4]:sw t5, 384(ra)<br> [0x800006e8]:sw t6, 392(ra)<br> [0x800006ec]:sw t5, 400(ra)<br> [0x800006f0]:sw a6, 408(ra)<br>                                     |
|  24|[0x80011858]<br>0x00000000<br> [0x80011870]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000071c]:fmul.d t5, t3, s10, dyn<br> [0x80000720]:csrrs a6, fcsr, zero<br> [0x80000724]:sw t5, 416(ra)<br> [0x80000728]:sw t6, 424(ra)<br> [0x8000072c]:sw t5, 432(ra)<br> [0x80000730]:sw a6, 440(ra)<br>                                     |
|  25|[0x80011878]<br>0x00000000<br> [0x80011890]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000075c]:fmul.d t5, t3, s10, dyn<br> [0x80000760]:csrrs a6, fcsr, zero<br> [0x80000764]:sw t5, 448(ra)<br> [0x80000768]:sw t6, 456(ra)<br> [0x8000076c]:sw t5, 464(ra)<br> [0x80000770]:sw a6, 472(ra)<br>                                     |
|  26|[0x80011898]<br>0x00000000<br> [0x800118b0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000079c]:fmul.d t5, t3, s10, dyn<br> [0x800007a0]:csrrs a6, fcsr, zero<br> [0x800007a4]:sw t5, 480(ra)<br> [0x800007a8]:sw t6, 488(ra)<br> [0x800007ac]:sw t5, 496(ra)<br> [0x800007b0]:sw a6, 504(ra)<br>                                     |
|  27|[0x800118b8]<br>0x00000000<br> [0x800118d0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800007dc]:fmul.d t5, t3, s10, dyn<br> [0x800007e0]:csrrs a6, fcsr, zero<br> [0x800007e4]:sw t5, 512(ra)<br> [0x800007e8]:sw t6, 520(ra)<br> [0x800007ec]:sw t5, 528(ra)<br> [0x800007f0]:sw a6, 536(ra)<br>                                     |
|  28|[0x800118d8]<br>0x00000000<br> [0x800118f0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000081c]:fmul.d t5, t3, s10, dyn<br> [0x80000820]:csrrs a6, fcsr, zero<br> [0x80000824]:sw t5, 544(ra)<br> [0x80000828]:sw t6, 552(ra)<br> [0x8000082c]:sw t5, 560(ra)<br> [0x80000830]:sw a6, 568(ra)<br>                                     |
|  29|[0x800118f8]<br>0x00000000<br> [0x80011910]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000085c]:fmul.d t5, t3, s10, dyn<br> [0x80000860]:csrrs a6, fcsr, zero<br> [0x80000864]:sw t5, 576(ra)<br> [0x80000868]:sw t6, 584(ra)<br> [0x8000086c]:sw t5, 592(ra)<br> [0x80000870]:sw a6, 600(ra)<br>                                     |
|  30|[0x80011918]<br>0x00000000<br> [0x80011930]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000089c]:fmul.d t5, t3, s10, dyn<br> [0x800008a0]:csrrs a6, fcsr, zero<br> [0x800008a4]:sw t5, 608(ra)<br> [0x800008a8]:sw t6, 616(ra)<br> [0x800008ac]:sw t5, 624(ra)<br> [0x800008b0]:sw a6, 632(ra)<br>                                     |
|  31|[0x80011938]<br>0x00000000<br> [0x80011950]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800008e0]:fmul.d t5, t3, s10, dyn<br> [0x800008e4]:csrrs a6, fcsr, zero<br> [0x800008e8]:sw t5, 640(ra)<br> [0x800008ec]:sw t6, 648(ra)<br> [0x800008f0]:sw t5, 656(ra)<br> [0x800008f4]:sw a6, 664(ra)<br>                                     |
|  32|[0x80011958]<br>0x00000000<br> [0x80011970]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000924]:fmul.d t5, t3, s10, dyn<br> [0x80000928]:csrrs a6, fcsr, zero<br> [0x8000092c]:sw t5, 672(ra)<br> [0x80000930]:sw t6, 680(ra)<br> [0x80000934]:sw t5, 688(ra)<br> [0x80000938]:sw a6, 696(ra)<br>                                     |
|  33|[0x80011978]<br>0x00000000<br> [0x80011990]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000964]:fmul.d t5, t3, s10, dyn<br> [0x80000968]:csrrs a6, fcsr, zero<br> [0x8000096c]:sw t5, 704(ra)<br> [0x80000970]:sw t6, 712(ra)<br> [0x80000974]:sw t5, 720(ra)<br> [0x80000978]:sw a6, 728(ra)<br>                                     |
|  34|[0x80011998]<br>0x00000000<br> [0x800119b0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800009a4]:fmul.d t5, t3, s10, dyn<br> [0x800009a8]:csrrs a6, fcsr, zero<br> [0x800009ac]:sw t5, 736(ra)<br> [0x800009b0]:sw t6, 744(ra)<br> [0x800009b4]:sw t5, 752(ra)<br> [0x800009b8]:sw a6, 760(ra)<br>                                     |
|  35|[0x800119b8]<br>0x00000000<br> [0x800119d0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800009e4]:fmul.d t5, t3, s10, dyn<br> [0x800009e8]:csrrs a6, fcsr, zero<br> [0x800009ec]:sw t5, 768(ra)<br> [0x800009f0]:sw t6, 776(ra)<br> [0x800009f4]:sw t5, 784(ra)<br> [0x800009f8]:sw a6, 792(ra)<br>                                     |
|  36|[0x800119d8]<br>0x00000000<br> [0x800119f0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000a24]:fmul.d t5, t3, s10, dyn<br> [0x80000a28]:csrrs a6, fcsr, zero<br> [0x80000a2c]:sw t5, 800(ra)<br> [0x80000a30]:sw t6, 808(ra)<br> [0x80000a34]:sw t5, 816(ra)<br> [0x80000a38]:sw a6, 824(ra)<br>                                     |
|  37|[0x800119f8]<br>0x00000000<br> [0x80011a10]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000a68]:fmul.d t5, t3, s10, dyn<br> [0x80000a6c]:csrrs a6, fcsr, zero<br> [0x80000a70]:sw t5, 832(ra)<br> [0x80000a74]:sw t6, 840(ra)<br> [0x80000a78]:sw t5, 848(ra)<br> [0x80000a7c]:sw a6, 856(ra)<br>                                     |
|  38|[0x80011a18]<br>0x00000000<br> [0x80011a30]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000aac]:fmul.d t5, t3, s10, dyn<br> [0x80000ab0]:csrrs a6, fcsr, zero<br> [0x80000ab4]:sw t5, 864(ra)<br> [0x80000ab8]:sw t6, 872(ra)<br> [0x80000abc]:sw t5, 880(ra)<br> [0x80000ac0]:sw a6, 888(ra)<br>                                     |
|  39|[0x80011a38]<br>0x00000000<br> [0x80011a50]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000aec]:fmul.d t5, t3, s10, dyn<br> [0x80000af0]:csrrs a6, fcsr, zero<br> [0x80000af4]:sw t5, 896(ra)<br> [0x80000af8]:sw t6, 904(ra)<br> [0x80000afc]:sw t5, 912(ra)<br> [0x80000b00]:sw a6, 920(ra)<br>                                     |
|  40|[0x80011a58]<br>0x00000000<br> [0x80011a70]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000b2c]:fmul.d t5, t3, s10, dyn<br> [0x80000b30]:csrrs a6, fcsr, zero<br> [0x80000b34]:sw t5, 928(ra)<br> [0x80000b38]:sw t6, 936(ra)<br> [0x80000b3c]:sw t5, 944(ra)<br> [0x80000b40]:sw a6, 952(ra)<br>                                     |
|  41|[0x80011a78]<br>0x00000000<br> [0x80011a90]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000b6c]:fmul.d t5, t3, s10, dyn<br> [0x80000b70]:csrrs a6, fcsr, zero<br> [0x80000b74]:sw t5, 960(ra)<br> [0x80000b78]:sw t6, 968(ra)<br> [0x80000b7c]:sw t5, 976(ra)<br> [0x80000b80]:sw a6, 984(ra)<br>                                     |
|  42|[0x80011a98]<br>0x00000000<br> [0x80011ab0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000bac]:fmul.d t5, t3, s10, dyn<br> [0x80000bb0]:csrrs a6, fcsr, zero<br> [0x80000bb4]:sw t5, 992(ra)<br> [0x80000bb8]:sw t6, 1000(ra)<br> [0x80000bbc]:sw t5, 1008(ra)<br> [0x80000bc0]:sw a6, 1016(ra)<br>                                  |
|  43|[0x80011ab8]<br>0x00000000<br> [0x80011ad0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000bec]:fmul.d t5, t3, s10, dyn<br> [0x80000bf0]:csrrs a6, fcsr, zero<br> [0x80000bf4]:sw t5, 1024(ra)<br> [0x80000bf8]:sw t6, 1032(ra)<br> [0x80000bfc]:sw t5, 1040(ra)<br> [0x80000c00]:sw a6, 1048(ra)<br>                                 |
|  44|[0x80011ad8]<br>0x00000000<br> [0x80011af0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000c2c]:fmul.d t5, t3, s10, dyn<br> [0x80000c30]:csrrs a6, fcsr, zero<br> [0x80000c34]:sw t5, 1056(ra)<br> [0x80000c38]:sw t6, 1064(ra)<br> [0x80000c3c]:sw t5, 1072(ra)<br> [0x80000c40]:sw a6, 1080(ra)<br>                                 |
|  45|[0x80011af8]<br>0x00000000<br> [0x80011b10]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000c6c]:fmul.d t5, t3, s10, dyn<br> [0x80000c70]:csrrs a6, fcsr, zero<br> [0x80000c74]:sw t5, 1088(ra)<br> [0x80000c78]:sw t6, 1096(ra)<br> [0x80000c7c]:sw t5, 1104(ra)<br> [0x80000c80]:sw a6, 1112(ra)<br>                                 |
|  46|[0x80011b18]<br>0x00000000<br> [0x80011b30]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000cac]:fmul.d t5, t3, s10, dyn<br> [0x80000cb0]:csrrs a6, fcsr, zero<br> [0x80000cb4]:sw t5, 1120(ra)<br> [0x80000cb8]:sw t6, 1128(ra)<br> [0x80000cbc]:sw t5, 1136(ra)<br> [0x80000cc0]:sw a6, 1144(ra)<br>                                 |
|  47|[0x80011b38]<br>0x00000000<br> [0x80011b50]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000cec]:fmul.d t5, t3, s10, dyn<br> [0x80000cf0]:csrrs a6, fcsr, zero<br> [0x80000cf4]:sw t5, 1152(ra)<br> [0x80000cf8]:sw t6, 1160(ra)<br> [0x80000cfc]:sw t5, 1168(ra)<br> [0x80000d00]:sw a6, 1176(ra)<br>                                 |
|  48|[0x80011b58]<br>0x00000000<br> [0x80011b70]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000d2c]:fmul.d t5, t3, s10, dyn<br> [0x80000d30]:csrrs a6, fcsr, zero<br> [0x80000d34]:sw t5, 1184(ra)<br> [0x80000d38]:sw t6, 1192(ra)<br> [0x80000d3c]:sw t5, 1200(ra)<br> [0x80000d40]:sw a6, 1208(ra)<br>                                 |
|  49|[0x80011b78]<br>0x00000000<br> [0x80011b90]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000d6c]:fmul.d t5, t3, s10, dyn<br> [0x80000d70]:csrrs a6, fcsr, zero<br> [0x80000d74]:sw t5, 1216(ra)<br> [0x80000d78]:sw t6, 1224(ra)<br> [0x80000d7c]:sw t5, 1232(ra)<br> [0x80000d80]:sw a6, 1240(ra)<br>                                 |
|  50|[0x80011b98]<br>0x00000000<br> [0x80011bb0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000dac]:fmul.d t5, t3, s10, dyn<br> [0x80000db0]:csrrs a6, fcsr, zero<br> [0x80000db4]:sw t5, 1248(ra)<br> [0x80000db8]:sw t6, 1256(ra)<br> [0x80000dbc]:sw t5, 1264(ra)<br> [0x80000dc0]:sw a6, 1272(ra)<br>                                 |
|  51|[0x80011bb8]<br>0x00000000<br> [0x80011bd0]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000dec]:fmul.d t5, t3, s10, dyn<br> [0x80000df0]:csrrs a6, fcsr, zero<br> [0x80000df4]:sw t5, 1280(ra)<br> [0x80000df8]:sw t6, 1288(ra)<br> [0x80000dfc]:sw t5, 1296(ra)<br> [0x80000e00]:sw a6, 1304(ra)<br>                                 |
|  52|[0x80011bd8]<br>0x00000000<br> [0x80011bf0]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000e2c]:fmul.d t5, t3, s10, dyn<br> [0x80000e30]:csrrs a6, fcsr, zero<br> [0x80000e34]:sw t5, 1312(ra)<br> [0x80000e38]:sw t6, 1320(ra)<br> [0x80000e3c]:sw t5, 1328(ra)<br> [0x80000e40]:sw a6, 1336(ra)<br>                                 |
|  53|[0x80011bf8]<br>0x00000000<br> [0x80011c10]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000e6c]:fmul.d t5, t3, s10, dyn<br> [0x80000e70]:csrrs a6, fcsr, zero<br> [0x80000e74]:sw t5, 1344(ra)<br> [0x80000e78]:sw t6, 1352(ra)<br> [0x80000e7c]:sw t5, 1360(ra)<br> [0x80000e80]:sw a6, 1368(ra)<br>                                 |
|  54|[0x80011c18]<br>0x00000000<br> [0x80011c30]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000eac]:fmul.d t5, t3, s10, dyn<br> [0x80000eb0]:csrrs a6, fcsr, zero<br> [0x80000eb4]:sw t5, 1376(ra)<br> [0x80000eb8]:sw t6, 1384(ra)<br> [0x80000ebc]:sw t5, 1392(ra)<br> [0x80000ec0]:sw a6, 1400(ra)<br>                                 |
|  55|[0x80011c38]<br>0x00000000<br> [0x80011c50]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000ef0]:fmul.d t5, t3, s10, dyn<br> [0x80000ef4]:csrrs a6, fcsr, zero<br> [0x80000ef8]:sw t5, 1408(ra)<br> [0x80000efc]:sw t6, 1416(ra)<br> [0x80000f00]:sw t5, 1424(ra)<br> [0x80000f04]:sw a6, 1432(ra)<br>                                 |
|  56|[0x80011c58]<br>0x00000000<br> [0x80011c70]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000f34]:fmul.d t5, t3, s10, dyn<br> [0x80000f38]:csrrs a6, fcsr, zero<br> [0x80000f3c]:sw t5, 1440(ra)<br> [0x80000f40]:sw t6, 1448(ra)<br> [0x80000f44]:sw t5, 1456(ra)<br> [0x80000f48]:sw a6, 1464(ra)<br>                                 |
|  57|[0x80011c78]<br>0x00000000<br> [0x80011c90]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000f74]:fmul.d t5, t3, s10, dyn<br> [0x80000f78]:csrrs a6, fcsr, zero<br> [0x80000f7c]:sw t5, 1472(ra)<br> [0x80000f80]:sw t6, 1480(ra)<br> [0x80000f84]:sw t5, 1488(ra)<br> [0x80000f88]:sw a6, 1496(ra)<br>                                 |
|  58|[0x80011c98]<br>0x00000000<br> [0x80011cb0]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000fb4]:fmul.d t5, t3, s10, dyn<br> [0x80000fb8]:csrrs a6, fcsr, zero<br> [0x80000fbc]:sw t5, 1504(ra)<br> [0x80000fc0]:sw t6, 1512(ra)<br> [0x80000fc4]:sw t5, 1520(ra)<br> [0x80000fc8]:sw a6, 1528(ra)<br>                                 |
|  59|[0x80011cb8]<br>0x00000000<br> [0x80011cd0]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80000ff4]:fmul.d t5, t3, s10, dyn<br> [0x80000ff8]:csrrs a6, fcsr, zero<br> [0x80000ffc]:sw t5, 1536(ra)<br> [0x80001000]:sw t6, 1544(ra)<br> [0x80001004]:sw t5, 1552(ra)<br> [0x80001008]:sw a6, 1560(ra)<br>                                 |
|  60|[0x80011cd8]<br>0x00000000<br> [0x80011cf0]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001034]:fmul.d t5, t3, s10, dyn<br> [0x80001038]:csrrs a6, fcsr, zero<br> [0x8000103c]:sw t5, 1568(ra)<br> [0x80001040]:sw t6, 1576(ra)<br> [0x80001044]:sw t5, 1584(ra)<br> [0x80001048]:sw a6, 1592(ra)<br>                                 |
|  61|[0x80011cf8]<br>0xFFFFFFFF<br> [0x80011d10]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001078]:fmul.d t5, t3, s10, dyn<br> [0x8000107c]:csrrs a6, fcsr, zero<br> [0x80001080]:sw t5, 1600(ra)<br> [0x80001084]:sw t6, 1608(ra)<br> [0x80001088]:sw t5, 1616(ra)<br> [0x8000108c]:sw a6, 1624(ra)<br>                                 |
|  62|[0x80011d18]<br>0xFFFFFFFF<br> [0x80011d30]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800010bc]:fmul.d t5, t3, s10, dyn<br> [0x800010c0]:csrrs a6, fcsr, zero<br> [0x800010c4]:sw t5, 1632(ra)<br> [0x800010c8]:sw t6, 1640(ra)<br> [0x800010cc]:sw t5, 1648(ra)<br> [0x800010d0]:sw a6, 1656(ra)<br>                                 |
|  63|[0x80011d38]<br>0x00000000<br> [0x80011d50]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800010fc]:fmul.d t5, t3, s10, dyn<br> [0x80001100]:csrrs a6, fcsr, zero<br> [0x80001104]:sw t5, 1664(ra)<br> [0x80001108]:sw t6, 1672(ra)<br> [0x8000110c]:sw t5, 1680(ra)<br> [0x80001110]:sw a6, 1688(ra)<br>                                 |
|  64|[0x80011d58]<br>0x00000000<br> [0x80011d70]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000113c]:fmul.d t5, t3, s10, dyn<br> [0x80001140]:csrrs a6, fcsr, zero<br> [0x80001144]:sw t5, 1696(ra)<br> [0x80001148]:sw t6, 1704(ra)<br> [0x8000114c]:sw t5, 1712(ra)<br> [0x80001150]:sw a6, 1720(ra)<br>                                 |
|  65|[0x80011d78]<br>0x00000000<br> [0x80011d90]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000117c]:fmul.d t5, t3, s10, dyn<br> [0x80001180]:csrrs a6, fcsr, zero<br> [0x80001184]:sw t5, 1728(ra)<br> [0x80001188]:sw t6, 1736(ra)<br> [0x8000118c]:sw t5, 1744(ra)<br> [0x80001190]:sw a6, 1752(ra)<br>                                 |
|  66|[0x80011d98]<br>0x00000000<br> [0x80011db0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800011bc]:fmul.d t5, t3, s10, dyn<br> [0x800011c0]:csrrs a6, fcsr, zero<br> [0x800011c4]:sw t5, 1760(ra)<br> [0x800011c8]:sw t6, 1768(ra)<br> [0x800011cc]:sw t5, 1776(ra)<br> [0x800011d0]:sw a6, 1784(ra)<br>                                 |
|  67|[0x80011db8]<br>0x00000000<br> [0x80011dd0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800011fc]:fmul.d t5, t3, s10, dyn<br> [0x80001200]:csrrs a6, fcsr, zero<br> [0x80001204]:sw t5, 1792(ra)<br> [0x80001208]:sw t6, 1800(ra)<br> [0x8000120c]:sw t5, 1808(ra)<br> [0x80001210]:sw a6, 1816(ra)<br>                                 |
|  68|[0x80011dd8]<br>0x00000000<br> [0x80011df0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000123c]:fmul.d t5, t3, s10, dyn<br> [0x80001240]:csrrs a6, fcsr, zero<br> [0x80001244]:sw t5, 1824(ra)<br> [0x80001248]:sw t6, 1832(ra)<br> [0x8000124c]:sw t5, 1840(ra)<br> [0x80001250]:sw a6, 1848(ra)<br>                                 |
|  69|[0x80011df8]<br>0x00000000<br> [0x80011e10]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000127c]:fmul.d t5, t3, s10, dyn<br> [0x80001280]:csrrs a6, fcsr, zero<br> [0x80001284]:sw t5, 1856(ra)<br> [0x80001288]:sw t6, 1864(ra)<br> [0x8000128c]:sw t5, 1872(ra)<br> [0x80001290]:sw a6, 1880(ra)<br>                                 |
|  70|[0x80011e18]<br>0x00000000<br> [0x80011e30]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800012bc]:fmul.d t5, t3, s10, dyn<br> [0x800012c0]:csrrs a6, fcsr, zero<br> [0x800012c4]:sw t5, 1888(ra)<br> [0x800012c8]:sw t6, 1896(ra)<br> [0x800012cc]:sw t5, 1904(ra)<br> [0x800012d0]:sw a6, 1912(ra)<br>                                 |
|  71|[0x80011e38]<br>0x00000001<br> [0x80011e50]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800012fc]:fmul.d t5, t3, s10, dyn<br> [0x80001300]:csrrs a6, fcsr, zero<br> [0x80001304]:sw t5, 1920(ra)<br> [0x80001308]:sw t6, 1928(ra)<br> [0x8000130c]:sw t5, 1936(ra)<br> [0x80001310]:sw a6, 1944(ra)<br>                                 |
|  72|[0x80011e58]<br>0x00000000<br> [0x80011e70]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000133c]:fmul.d t5, t3, s10, dyn<br> [0x80001340]:csrrs a6, fcsr, zero<br> [0x80001344]:sw t5, 1952(ra)<br> [0x80001348]:sw t6, 1960(ra)<br> [0x8000134c]:sw t5, 1968(ra)<br> [0x80001350]:sw a6, 1976(ra)<br>                                 |
|  73|[0x80011e78]<br>0x00000000<br> [0x80011e90]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000137c]:fmul.d t5, t3, s10, dyn<br> [0x80001380]:csrrs a6, fcsr, zero<br> [0x80001384]:sw t5, 1984(ra)<br> [0x80001388]:sw t6, 1992(ra)<br> [0x8000138c]:sw t5, 2000(ra)<br> [0x80001390]:sw a6, 2008(ra)<br>                                 |
|  74|[0x80011e98]<br>0x00000000<br> [0x80011eb0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800013bc]:fmul.d t5, t3, s10, dyn<br> [0x800013c0]:csrrs a6, fcsr, zero<br> [0x800013c4]:sw t5, 2016(ra)<br> [0x800013c8]:sw t6, 2024(ra)<br> [0x800013cc]:sw t5, 2032(ra)<br> [0x800013d0]:sw a6, 2040(ra)<br>                                 |
|  75|[0x80011eb8]<br>0x00000000<br> [0x80011ed0]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800013fc]:fmul.d t5, t3, s10, dyn<br> [0x80001400]:csrrs a6, fcsr, zero<br> [0x80001404]:addi ra, ra, 2040<br> [0x80001408]:sw t5, 8(ra)<br> [0x8000140c]:sw t6, 16(ra)<br> [0x80001410]:sw t5, 24(ra)<br> [0x80001414]:sw a6, 32(ra)<br>       |
|  76|[0x80011ed8]<br>0x00000000<br> [0x80011ef0]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001440]:fmul.d t5, t3, s10, dyn<br> [0x80001444]:csrrs a6, fcsr, zero<br> [0x80001448]:sw t5, 40(ra)<br> [0x8000144c]:sw t6, 48(ra)<br> [0x80001450]:sw t5, 56(ra)<br> [0x80001454]:sw a6, 64(ra)<br>                                         |
|  77|[0x80011ef8]<br>0x00000000<br> [0x80011f10]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001480]:fmul.d t5, t3, s10, dyn<br> [0x80001484]:csrrs a6, fcsr, zero<br> [0x80001488]:sw t5, 72(ra)<br> [0x8000148c]:sw t6, 80(ra)<br> [0x80001490]:sw t5, 88(ra)<br> [0x80001494]:sw a6, 96(ra)<br>                                         |
|  78|[0x80011f18]<br>0x00000000<br> [0x80011f30]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800014c0]:fmul.d t5, t3, s10, dyn<br> [0x800014c4]:csrrs a6, fcsr, zero<br> [0x800014c8]:sw t5, 104(ra)<br> [0x800014cc]:sw t6, 112(ra)<br> [0x800014d0]:sw t5, 120(ra)<br> [0x800014d4]:sw a6, 128(ra)<br>                                     |
|  79|[0x80011f38]<br>0x00000000<br> [0x80011f50]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001504]:fmul.d t5, t3, s10, dyn<br> [0x80001508]:csrrs a6, fcsr, zero<br> [0x8000150c]:sw t5, 136(ra)<br> [0x80001510]:sw t6, 144(ra)<br> [0x80001514]:sw t5, 152(ra)<br> [0x80001518]:sw a6, 160(ra)<br>                                     |
|  80|[0x80011f58]<br>0x00000000<br> [0x80011f70]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001548]:fmul.d t5, t3, s10, dyn<br> [0x8000154c]:csrrs a6, fcsr, zero<br> [0x80001550]:sw t5, 168(ra)<br> [0x80001554]:sw t6, 176(ra)<br> [0x80001558]:sw t5, 184(ra)<br> [0x8000155c]:sw a6, 192(ra)<br>                                     |
|  81|[0x80011f78]<br>0x00000000<br> [0x80011f90]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001588]:fmul.d t5, t3, s10, dyn<br> [0x8000158c]:csrrs a6, fcsr, zero<br> [0x80001590]:sw t5, 200(ra)<br> [0x80001594]:sw t6, 208(ra)<br> [0x80001598]:sw t5, 216(ra)<br> [0x8000159c]:sw a6, 224(ra)<br>                                     |
|  82|[0x80011f98]<br>0x00000000<br> [0x80011fb0]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800015c8]:fmul.d t5, t3, s10, dyn<br> [0x800015cc]:csrrs a6, fcsr, zero<br> [0x800015d0]:sw t5, 232(ra)<br> [0x800015d4]:sw t6, 240(ra)<br> [0x800015d8]:sw t5, 248(ra)<br> [0x800015dc]:sw a6, 256(ra)<br>                                     |
|  83|[0x80011fb8]<br>0x00000000<br> [0x80011fd0]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001608]:fmul.d t5, t3, s10, dyn<br> [0x8000160c]:csrrs a6, fcsr, zero<br> [0x80001610]:sw t5, 264(ra)<br> [0x80001614]:sw t6, 272(ra)<br> [0x80001618]:sw t5, 280(ra)<br> [0x8000161c]:sw a6, 288(ra)<br>                                     |
|  84|[0x80011fd8]<br>0x00000000<br> [0x80011ff0]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001648]:fmul.d t5, t3, s10, dyn<br> [0x8000164c]:csrrs a6, fcsr, zero<br> [0x80001650]:sw t5, 296(ra)<br> [0x80001654]:sw t6, 304(ra)<br> [0x80001658]:sw t5, 312(ra)<br> [0x8000165c]:sw a6, 320(ra)<br>                                     |
|  85|[0x80011ff8]<br>0xFFFFFFFF<br> [0x80012010]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000168c]:fmul.d t5, t3, s10, dyn<br> [0x80001690]:csrrs a6, fcsr, zero<br> [0x80001694]:sw t5, 328(ra)<br> [0x80001698]:sw t6, 336(ra)<br> [0x8000169c]:sw t5, 344(ra)<br> [0x800016a0]:sw a6, 352(ra)<br>                                     |
|  86|[0x80012018]<br>0xFFFFFFFF<br> [0x80012030]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800016d0]:fmul.d t5, t3, s10, dyn<br> [0x800016d4]:csrrs a6, fcsr, zero<br> [0x800016d8]:sw t5, 360(ra)<br> [0x800016dc]:sw t6, 368(ra)<br> [0x800016e0]:sw t5, 376(ra)<br> [0x800016e4]:sw a6, 384(ra)<br>                                     |
|  87|[0x80012038]<br>0x00000000<br> [0x80012050]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001710]:fmul.d t5, t3, s10, dyn<br> [0x80001714]:csrrs a6, fcsr, zero<br> [0x80001718]:sw t5, 392(ra)<br> [0x8000171c]:sw t6, 400(ra)<br> [0x80001720]:sw t5, 408(ra)<br> [0x80001724]:sw a6, 416(ra)<br>                                     |
|  88|[0x80012058]<br>0x00000000<br> [0x80012070]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001750]:fmul.d t5, t3, s10, dyn<br> [0x80001754]:csrrs a6, fcsr, zero<br> [0x80001758]:sw t5, 424(ra)<br> [0x8000175c]:sw t6, 432(ra)<br> [0x80001760]:sw t5, 440(ra)<br> [0x80001764]:sw a6, 448(ra)<br>                                     |
|  89|[0x80012078]<br>0x00000000<br> [0x80012090]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001790]:fmul.d t5, t3, s10, dyn<br> [0x80001794]:csrrs a6, fcsr, zero<br> [0x80001798]:sw t5, 456(ra)<br> [0x8000179c]:sw t6, 464(ra)<br> [0x800017a0]:sw t5, 472(ra)<br> [0x800017a4]:sw a6, 480(ra)<br>                                     |
|  90|[0x80012098]<br>0x00000000<br> [0x800120b0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800017d0]:fmul.d t5, t3, s10, dyn<br> [0x800017d4]:csrrs a6, fcsr, zero<br> [0x800017d8]:sw t5, 488(ra)<br> [0x800017dc]:sw t6, 496(ra)<br> [0x800017e0]:sw t5, 504(ra)<br> [0x800017e4]:sw a6, 512(ra)<br>                                     |
|  91|[0x800120b8]<br>0x00000000<br> [0x800120d0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001810]:fmul.d t5, t3, s10, dyn<br> [0x80001814]:csrrs a6, fcsr, zero<br> [0x80001818]:sw t5, 520(ra)<br> [0x8000181c]:sw t6, 528(ra)<br> [0x80001820]:sw t5, 536(ra)<br> [0x80001824]:sw a6, 544(ra)<br>                                     |
|  92|[0x800120d8]<br>0x00000000<br> [0x800120f0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001850]:fmul.d t5, t3, s10, dyn<br> [0x80001854]:csrrs a6, fcsr, zero<br> [0x80001858]:sw t5, 552(ra)<br> [0x8000185c]:sw t6, 560(ra)<br> [0x80001860]:sw t5, 568(ra)<br> [0x80001864]:sw a6, 576(ra)<br>                                     |
|  93|[0x800120f8]<br>0x00000000<br> [0x80012110]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001890]:fmul.d t5, t3, s10, dyn<br> [0x80001894]:csrrs a6, fcsr, zero<br> [0x80001898]:sw t5, 584(ra)<br> [0x8000189c]:sw t6, 592(ra)<br> [0x800018a0]:sw t5, 600(ra)<br> [0x800018a4]:sw a6, 608(ra)<br>                                     |
|  94|[0x80012118]<br>0x00000000<br> [0x80012130]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800018d0]:fmul.d t5, t3, s10, dyn<br> [0x800018d4]:csrrs a6, fcsr, zero<br> [0x800018d8]:sw t5, 616(ra)<br> [0x800018dc]:sw t6, 624(ra)<br> [0x800018e0]:sw t5, 632(ra)<br> [0x800018e4]:sw a6, 640(ra)<br>                                     |
|  95|[0x80012138]<br>0x00000001<br> [0x80012150]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001910]:fmul.d t5, t3, s10, dyn<br> [0x80001914]:csrrs a6, fcsr, zero<br> [0x80001918]:sw t5, 648(ra)<br> [0x8000191c]:sw t6, 656(ra)<br> [0x80001920]:sw t5, 664(ra)<br> [0x80001924]:sw a6, 672(ra)<br>                                     |
|  96|[0x80012158]<br>0x00000000<br> [0x80012170]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001950]:fmul.d t5, t3, s10, dyn<br> [0x80001954]:csrrs a6, fcsr, zero<br> [0x80001958]:sw t5, 680(ra)<br> [0x8000195c]:sw t6, 688(ra)<br> [0x80001960]:sw t5, 696(ra)<br> [0x80001964]:sw a6, 704(ra)<br>                                     |
|  97|[0x80012178]<br>0x00000000<br> [0x80012190]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001990]:fmul.d t5, t3, s10, dyn<br> [0x80001994]:csrrs a6, fcsr, zero<br> [0x80001998]:sw t5, 712(ra)<br> [0x8000199c]:sw t6, 720(ra)<br> [0x800019a0]:sw t5, 728(ra)<br> [0x800019a4]:sw a6, 736(ra)<br>                                     |
|  98|[0x80012198]<br>0x00000000<br> [0x800121b0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800019d0]:fmul.d t5, t3, s10, dyn<br> [0x800019d4]:csrrs a6, fcsr, zero<br> [0x800019d8]:sw t5, 744(ra)<br> [0x800019dc]:sw t6, 752(ra)<br> [0x800019e0]:sw t5, 760(ra)<br> [0x800019e4]:sw a6, 768(ra)<br>                                     |
|  99|[0x800121b8]<br>0x00000000<br> [0x800121d0]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001a10]:fmul.d t5, t3, s10, dyn<br> [0x80001a14]:csrrs a6, fcsr, zero<br> [0x80001a18]:sw t5, 776(ra)<br> [0x80001a1c]:sw t6, 784(ra)<br> [0x80001a20]:sw t5, 792(ra)<br> [0x80001a24]:sw a6, 800(ra)<br>                                     |
| 100|[0x800121d8]<br>0x00000000<br> [0x800121f0]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001a50]:fmul.d t5, t3, s10, dyn<br> [0x80001a54]:csrrs a6, fcsr, zero<br> [0x80001a58]:sw t5, 808(ra)<br> [0x80001a5c]:sw t6, 816(ra)<br> [0x80001a60]:sw t5, 824(ra)<br> [0x80001a64]:sw a6, 832(ra)<br>                                     |
| 101|[0x800121f8]<br>0x00000000<br> [0x80012210]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001a90]:fmul.d t5, t3, s10, dyn<br> [0x80001a94]:csrrs a6, fcsr, zero<br> [0x80001a98]:sw t5, 840(ra)<br> [0x80001a9c]:sw t6, 848(ra)<br> [0x80001aa0]:sw t5, 856(ra)<br> [0x80001aa4]:sw a6, 864(ra)<br>                                     |
| 102|[0x80012218]<br>0x00000000<br> [0x80012230]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001ad0]:fmul.d t5, t3, s10, dyn<br> [0x80001ad4]:csrrs a6, fcsr, zero<br> [0x80001ad8]:sw t5, 872(ra)<br> [0x80001adc]:sw t6, 880(ra)<br> [0x80001ae0]:sw t5, 888(ra)<br> [0x80001ae4]:sw a6, 896(ra)<br>                                     |
| 103|[0x80012238]<br>0x00000000<br> [0x80012250]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001b14]:fmul.d t5, t3, s10, dyn<br> [0x80001b18]:csrrs a6, fcsr, zero<br> [0x80001b1c]:sw t5, 904(ra)<br> [0x80001b20]:sw t6, 912(ra)<br> [0x80001b24]:sw t5, 920(ra)<br> [0x80001b28]:sw a6, 928(ra)<br>                                     |
| 104|[0x80012258]<br>0x00000000<br> [0x80012270]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001b58]:fmul.d t5, t3, s10, dyn<br> [0x80001b5c]:csrrs a6, fcsr, zero<br> [0x80001b60]:sw t5, 936(ra)<br> [0x80001b64]:sw t6, 944(ra)<br> [0x80001b68]:sw t5, 952(ra)<br> [0x80001b6c]:sw a6, 960(ra)<br>                                     |
| 105|[0x80012278]<br>0x00000000<br> [0x80012290]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001b98]:fmul.d t5, t3, s10, dyn<br> [0x80001b9c]:csrrs a6, fcsr, zero<br> [0x80001ba0]:sw t5, 968(ra)<br> [0x80001ba4]:sw t6, 976(ra)<br> [0x80001ba8]:sw t5, 984(ra)<br> [0x80001bac]:sw a6, 992(ra)<br>                                     |
| 106|[0x80012298]<br>0x00000000<br> [0x800122b0]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001bd8]:fmul.d t5, t3, s10, dyn<br> [0x80001bdc]:csrrs a6, fcsr, zero<br> [0x80001be0]:sw t5, 1000(ra)<br> [0x80001be4]:sw t6, 1008(ra)<br> [0x80001be8]:sw t5, 1016(ra)<br> [0x80001bec]:sw a6, 1024(ra)<br>                                 |
| 107|[0x800122b8]<br>0x00000000<br> [0x800122d0]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001c18]:fmul.d t5, t3, s10, dyn<br> [0x80001c1c]:csrrs a6, fcsr, zero<br> [0x80001c20]:sw t5, 1032(ra)<br> [0x80001c24]:sw t6, 1040(ra)<br> [0x80001c28]:sw t5, 1048(ra)<br> [0x80001c2c]:sw a6, 1056(ra)<br>                                 |
| 108|[0x800122d8]<br>0x00000000<br> [0x800122f0]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001c58]:fmul.d t5, t3, s10, dyn<br> [0x80001c5c]:csrrs a6, fcsr, zero<br> [0x80001c60]:sw t5, 1064(ra)<br> [0x80001c64]:sw t6, 1072(ra)<br> [0x80001c68]:sw t5, 1080(ra)<br> [0x80001c6c]:sw a6, 1088(ra)<br>                                 |
| 109|[0x800122f8]<br>0xFFFFFFFF<br> [0x80012310]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001c9c]:fmul.d t5, t3, s10, dyn<br> [0x80001ca0]:csrrs a6, fcsr, zero<br> [0x80001ca4]:sw t5, 1096(ra)<br> [0x80001ca8]:sw t6, 1104(ra)<br> [0x80001cac]:sw t5, 1112(ra)<br> [0x80001cb0]:sw a6, 1120(ra)<br>                                 |
| 110|[0x80012318]<br>0xFFFFFFFF<br> [0x80012330]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001ce0]:fmul.d t5, t3, s10, dyn<br> [0x80001ce4]:csrrs a6, fcsr, zero<br> [0x80001ce8]:sw t5, 1128(ra)<br> [0x80001cec]:sw t6, 1136(ra)<br> [0x80001cf0]:sw t5, 1144(ra)<br> [0x80001cf4]:sw a6, 1152(ra)<br>                                 |
| 111|[0x80012338]<br>0x00000000<br> [0x80012350]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001d20]:fmul.d t5, t3, s10, dyn<br> [0x80001d24]:csrrs a6, fcsr, zero<br> [0x80001d28]:sw t5, 1160(ra)<br> [0x80001d2c]:sw t6, 1168(ra)<br> [0x80001d30]:sw t5, 1176(ra)<br> [0x80001d34]:sw a6, 1184(ra)<br>                                 |
| 112|[0x80012358]<br>0x00000000<br> [0x80012370]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001d60]:fmul.d t5, t3, s10, dyn<br> [0x80001d64]:csrrs a6, fcsr, zero<br> [0x80001d68]:sw t5, 1192(ra)<br> [0x80001d6c]:sw t6, 1200(ra)<br> [0x80001d70]:sw t5, 1208(ra)<br> [0x80001d74]:sw a6, 1216(ra)<br>                                 |
| 113|[0x80012378]<br>0x00000000<br> [0x80012390]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001da0]:fmul.d t5, t3, s10, dyn<br> [0x80001da4]:csrrs a6, fcsr, zero<br> [0x80001da8]:sw t5, 1224(ra)<br> [0x80001dac]:sw t6, 1232(ra)<br> [0x80001db0]:sw t5, 1240(ra)<br> [0x80001db4]:sw a6, 1248(ra)<br>                                 |
| 114|[0x80012398]<br>0x00000000<br> [0x800123b0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001de0]:fmul.d t5, t3, s10, dyn<br> [0x80001de4]:csrrs a6, fcsr, zero<br> [0x80001de8]:sw t5, 1256(ra)<br> [0x80001dec]:sw t6, 1264(ra)<br> [0x80001df0]:sw t5, 1272(ra)<br> [0x80001df4]:sw a6, 1280(ra)<br>                                 |
| 115|[0x800123b8]<br>0x00000000<br> [0x800123d0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001e20]:fmul.d t5, t3, s10, dyn<br> [0x80001e24]:csrrs a6, fcsr, zero<br> [0x80001e28]:sw t5, 1288(ra)<br> [0x80001e2c]:sw t6, 1296(ra)<br> [0x80001e30]:sw t5, 1304(ra)<br> [0x80001e34]:sw a6, 1312(ra)<br>                                 |
| 116|[0x800123d8]<br>0x00000000<br> [0x800123f0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001e60]:fmul.d t5, t3, s10, dyn<br> [0x80001e64]:csrrs a6, fcsr, zero<br> [0x80001e68]:sw t5, 1320(ra)<br> [0x80001e6c]:sw t6, 1328(ra)<br> [0x80001e70]:sw t5, 1336(ra)<br> [0x80001e74]:sw a6, 1344(ra)<br>                                 |
| 117|[0x800123f8]<br>0x00000000<br> [0x80012410]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001ea0]:fmul.d t5, t3, s10, dyn<br> [0x80001ea4]:csrrs a6, fcsr, zero<br> [0x80001ea8]:sw t5, 1352(ra)<br> [0x80001eac]:sw t6, 1360(ra)<br> [0x80001eb0]:sw t5, 1368(ra)<br> [0x80001eb4]:sw a6, 1376(ra)<br>                                 |
| 118|[0x80012418]<br>0x00000000<br> [0x80012430]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001ee0]:fmul.d t5, t3, s10, dyn<br> [0x80001ee4]:csrrs a6, fcsr, zero<br> [0x80001ee8]:sw t5, 1384(ra)<br> [0x80001eec]:sw t6, 1392(ra)<br> [0x80001ef0]:sw t5, 1400(ra)<br> [0x80001ef4]:sw a6, 1408(ra)<br>                                 |
| 119|[0x80012438]<br>0x00000002<br> [0x80012450]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001f20]:fmul.d t5, t3, s10, dyn<br> [0x80001f24]:csrrs a6, fcsr, zero<br> [0x80001f28]:sw t5, 1416(ra)<br> [0x80001f2c]:sw t6, 1424(ra)<br> [0x80001f30]:sw t5, 1432(ra)<br> [0x80001f34]:sw a6, 1440(ra)<br>                                 |
| 120|[0x80012458]<br>0x00000000<br> [0x80012470]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001f60]:fmul.d t5, t3, s10, dyn<br> [0x80001f64]:csrrs a6, fcsr, zero<br> [0x80001f68]:sw t5, 1448(ra)<br> [0x80001f6c]:sw t6, 1456(ra)<br> [0x80001f70]:sw t5, 1464(ra)<br> [0x80001f74]:sw a6, 1472(ra)<br>                                 |
| 121|[0x80012478]<br>0x00000000<br> [0x80012490]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001fa0]:fmul.d t5, t3, s10, dyn<br> [0x80001fa4]:csrrs a6, fcsr, zero<br> [0x80001fa8]:sw t5, 1480(ra)<br> [0x80001fac]:sw t6, 1488(ra)<br> [0x80001fb0]:sw t5, 1496(ra)<br> [0x80001fb4]:sw a6, 1504(ra)<br>                                 |
| 122|[0x80012498]<br>0x00000000<br> [0x800124b0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80001fe0]:fmul.d t5, t3, s10, dyn<br> [0x80001fe4]:csrrs a6, fcsr, zero<br> [0x80001fe8]:sw t5, 1512(ra)<br> [0x80001fec]:sw t6, 1520(ra)<br> [0x80001ff0]:sw t5, 1528(ra)<br> [0x80001ff4]:sw a6, 1536(ra)<br>                                 |
| 123|[0x800124b8]<br>0x00000000<br> [0x800124d0]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002020]:fmul.d t5, t3, s10, dyn<br> [0x80002024]:csrrs a6, fcsr, zero<br> [0x80002028]:sw t5, 1544(ra)<br> [0x8000202c]:sw t6, 1552(ra)<br> [0x80002030]:sw t5, 1560(ra)<br> [0x80002034]:sw a6, 1568(ra)<br>                                 |
| 124|[0x800124d8]<br>0x00000000<br> [0x800124f0]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002060]:fmul.d t5, t3, s10, dyn<br> [0x80002064]:csrrs a6, fcsr, zero<br> [0x80002068]:sw t5, 1576(ra)<br> [0x8000206c]:sw t6, 1584(ra)<br> [0x80002070]:sw t5, 1592(ra)<br> [0x80002074]:sw a6, 1600(ra)<br>                                 |
| 125|[0x800124f8]<br>0x00000000<br> [0x80012510]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800020a0]:fmul.d t5, t3, s10, dyn<br> [0x800020a4]:csrrs a6, fcsr, zero<br> [0x800020a8]:sw t5, 1608(ra)<br> [0x800020ac]:sw t6, 1616(ra)<br> [0x800020b0]:sw t5, 1624(ra)<br> [0x800020b4]:sw a6, 1632(ra)<br>                                 |
| 126|[0x80012518]<br>0x00000000<br> [0x80012530]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800020e0]:fmul.d t5, t3, s10, dyn<br> [0x800020e4]:csrrs a6, fcsr, zero<br> [0x800020e8]:sw t5, 1640(ra)<br> [0x800020ec]:sw t6, 1648(ra)<br> [0x800020f0]:sw t5, 1656(ra)<br> [0x800020f4]:sw a6, 1664(ra)<br>                                 |
| 127|[0x80012538]<br>0x00000000<br> [0x80012550]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002124]:fmul.d t5, t3, s10, dyn<br> [0x80002128]:csrrs a6, fcsr, zero<br> [0x8000212c]:sw t5, 1672(ra)<br> [0x80002130]:sw t6, 1680(ra)<br> [0x80002134]:sw t5, 1688(ra)<br> [0x80002138]:sw a6, 1696(ra)<br>                                 |
| 128|[0x80012558]<br>0x00000000<br> [0x80012570]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002168]:fmul.d t5, t3, s10, dyn<br> [0x8000216c]:csrrs a6, fcsr, zero<br> [0x80002170]:sw t5, 1704(ra)<br> [0x80002174]:sw t6, 1712(ra)<br> [0x80002178]:sw t5, 1720(ra)<br> [0x8000217c]:sw a6, 1728(ra)<br>                                 |
| 129|[0x80012578]<br>0x00000000<br> [0x80012590]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800021a8]:fmul.d t5, t3, s10, dyn<br> [0x800021ac]:csrrs a6, fcsr, zero<br> [0x800021b0]:sw t5, 1736(ra)<br> [0x800021b4]:sw t6, 1744(ra)<br> [0x800021b8]:sw t5, 1752(ra)<br> [0x800021bc]:sw a6, 1760(ra)<br>                                 |
| 130|[0x80012598]<br>0x00000000<br> [0x800125b0]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800021e8]:fmul.d t5, t3, s10, dyn<br> [0x800021ec]:csrrs a6, fcsr, zero<br> [0x800021f0]:sw t5, 1768(ra)<br> [0x800021f4]:sw t6, 1776(ra)<br> [0x800021f8]:sw t5, 1784(ra)<br> [0x800021fc]:sw a6, 1792(ra)<br>                                 |
| 131|[0x800125b8]<br>0x00000000<br> [0x800125d0]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002228]:fmul.d t5, t3, s10, dyn<br> [0x8000222c]:csrrs a6, fcsr, zero<br> [0x80002230]:sw t5, 1800(ra)<br> [0x80002234]:sw t6, 1808(ra)<br> [0x80002238]:sw t5, 1816(ra)<br> [0x8000223c]:sw a6, 1824(ra)<br>                                 |
| 132|[0x800125d8]<br>0x00000000<br> [0x800125f0]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002268]:fmul.d t5, t3, s10, dyn<br> [0x8000226c]:csrrs a6, fcsr, zero<br> [0x80002270]:sw t5, 1832(ra)<br> [0x80002274]:sw t6, 1840(ra)<br> [0x80002278]:sw t5, 1848(ra)<br> [0x8000227c]:sw a6, 1856(ra)<br>                                 |
| 133|[0x800125f8]<br>0xFFFFFFFF<br> [0x80012610]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800022ac]:fmul.d t5, t3, s10, dyn<br> [0x800022b0]:csrrs a6, fcsr, zero<br> [0x800022b4]:sw t5, 1864(ra)<br> [0x800022b8]:sw t6, 1872(ra)<br> [0x800022bc]:sw t5, 1880(ra)<br> [0x800022c0]:sw a6, 1888(ra)<br>                                 |
| 134|[0x80012618]<br>0xFFFFFFFF<br> [0x80012630]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800022f0]:fmul.d t5, t3, s10, dyn<br> [0x800022f4]:csrrs a6, fcsr, zero<br> [0x800022f8]:sw t5, 1896(ra)<br> [0x800022fc]:sw t6, 1904(ra)<br> [0x80002300]:sw t5, 1912(ra)<br> [0x80002304]:sw a6, 1920(ra)<br>                                 |
| 135|[0x80012638]<br>0x00000000<br> [0x80012650]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002330]:fmul.d t5, t3, s10, dyn<br> [0x80002334]:csrrs a6, fcsr, zero<br> [0x80002338]:sw t5, 1928(ra)<br> [0x8000233c]:sw t6, 1936(ra)<br> [0x80002340]:sw t5, 1944(ra)<br> [0x80002344]:sw a6, 1952(ra)<br>                                 |
| 136|[0x80012658]<br>0x00000000<br> [0x80012670]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002370]:fmul.d t5, t3, s10, dyn<br> [0x80002374]:csrrs a6, fcsr, zero<br> [0x80002378]:sw t5, 1960(ra)<br> [0x8000237c]:sw t6, 1968(ra)<br> [0x80002380]:sw t5, 1976(ra)<br> [0x80002384]:sw a6, 1984(ra)<br>                                 |
| 137|[0x80012678]<br>0x00000000<br> [0x80012690]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800023b0]:fmul.d t5, t3, s10, dyn<br> [0x800023b4]:csrrs a6, fcsr, zero<br> [0x800023b8]:sw t5, 1992(ra)<br> [0x800023bc]:sw t6, 2000(ra)<br> [0x800023c0]:sw t5, 2008(ra)<br> [0x800023c4]:sw a6, 2016(ra)<br>                                 |
| 138|[0x80012698]<br>0x00000000<br> [0x800126b0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800023f0]:fmul.d t5, t3, s10, dyn<br> [0x800023f4]:csrrs a6, fcsr, zero<br> [0x800023f8]:sw t5, 2024(ra)<br> [0x800023fc]:sw t6, 2032(ra)<br> [0x80002400]:sw t5, 2040(ra)<br> [0x80002404]:addi ra, ra, 2040<br> [0x80002408]:sw a6, 8(ra)<br> |
| 139|[0x800126b8]<br>0x00000000<br> [0x800126d0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002474]:fmul.d t5, t3, s10, dyn<br> [0x80002478]:csrrs a6, fcsr, zero<br> [0x8000247c]:sw t5, 16(ra)<br> [0x80002480]:sw t6, 24(ra)<br> [0x80002484]:sw t5, 32(ra)<br> [0x80002488]:sw a6, 40(ra)<br>                                         |
| 140|[0x800126d8]<br>0x00000000<br> [0x800126f0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800024f4]:fmul.d t5, t3, s10, dyn<br> [0x800024f8]:csrrs a6, fcsr, zero<br> [0x800024fc]:sw t5, 48(ra)<br> [0x80002500]:sw t6, 56(ra)<br> [0x80002504]:sw t5, 64(ra)<br> [0x80002508]:sw a6, 72(ra)<br>                                         |
| 141|[0x800126f8]<br>0x00000000<br> [0x80012710]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002574]:fmul.d t5, t3, s10, dyn<br> [0x80002578]:csrrs a6, fcsr, zero<br> [0x8000257c]:sw t5, 80(ra)<br> [0x80002580]:sw t6, 88(ra)<br> [0x80002584]:sw t5, 96(ra)<br> [0x80002588]:sw a6, 104(ra)<br>                                        |
| 142|[0x80012718]<br>0x00000000<br> [0x80012730]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800025f4]:fmul.d t5, t3, s10, dyn<br> [0x800025f8]:csrrs a6, fcsr, zero<br> [0x800025fc]:sw t5, 112(ra)<br> [0x80002600]:sw t6, 120(ra)<br> [0x80002604]:sw t5, 128(ra)<br> [0x80002608]:sw a6, 136(ra)<br>                                     |
| 143|[0x80012738]<br>0x00000002<br> [0x80012750]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002674]:fmul.d t5, t3, s10, dyn<br> [0x80002678]:csrrs a6, fcsr, zero<br> [0x8000267c]:sw t5, 144(ra)<br> [0x80002680]:sw t6, 152(ra)<br> [0x80002684]:sw t5, 160(ra)<br> [0x80002688]:sw a6, 168(ra)<br>                                     |
| 144|[0x80012758]<br>0x00000000<br> [0x80012770]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800026f4]:fmul.d t5, t3, s10, dyn<br> [0x800026f8]:csrrs a6, fcsr, zero<br> [0x800026fc]:sw t5, 176(ra)<br> [0x80002700]:sw t6, 184(ra)<br> [0x80002704]:sw t5, 192(ra)<br> [0x80002708]:sw a6, 200(ra)<br>                                     |
| 145|[0x80012778]<br>0x00000000<br> [0x80012790]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002778]:fmul.d t5, t3, s10, dyn<br> [0x8000277c]:csrrs a6, fcsr, zero<br> [0x80002780]:sw t5, 208(ra)<br> [0x80002784]:sw t6, 216(ra)<br> [0x80002788]:sw t5, 224(ra)<br> [0x8000278c]:sw a6, 232(ra)<br>                                     |
| 146|[0x80012798]<br>0x00000000<br> [0x800127b0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800027fc]:fmul.d t5, t3, s10, dyn<br> [0x80002800]:csrrs a6, fcsr, zero<br> [0x80002804]:sw t5, 240(ra)<br> [0x80002808]:sw t6, 248(ra)<br> [0x8000280c]:sw t5, 256(ra)<br> [0x80002810]:sw a6, 264(ra)<br>                                     |
| 147|[0x800127b8]<br>0x00000000<br> [0x800127d0]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002880]:fmul.d t5, t3, s10, dyn<br> [0x80002884]:csrrs a6, fcsr, zero<br> [0x80002888]:sw t5, 272(ra)<br> [0x8000288c]:sw t6, 280(ra)<br> [0x80002890]:sw t5, 288(ra)<br> [0x80002894]:sw a6, 296(ra)<br>                                     |
| 148|[0x800127d8]<br>0x00000000<br> [0x800127f0]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002904]:fmul.d t5, t3, s10, dyn<br> [0x80002908]:csrrs a6, fcsr, zero<br> [0x8000290c]:sw t5, 304(ra)<br> [0x80002910]:sw t6, 312(ra)<br> [0x80002914]:sw t5, 320(ra)<br> [0x80002918]:sw a6, 328(ra)<br>                                     |
| 149|[0x800127f8]<br>0x00000000<br> [0x80012810]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002988]:fmul.d t5, t3, s10, dyn<br> [0x8000298c]:csrrs a6, fcsr, zero<br> [0x80002990]:sw t5, 336(ra)<br> [0x80002994]:sw t6, 344(ra)<br> [0x80002998]:sw t5, 352(ra)<br> [0x8000299c]:sw a6, 360(ra)<br>                                     |
| 150|[0x80012818]<br>0x00000000<br> [0x80012830]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002a0c]:fmul.d t5, t3, s10, dyn<br> [0x80002a10]:csrrs a6, fcsr, zero<br> [0x80002a14]:sw t5, 368(ra)<br> [0x80002a18]:sw t6, 376(ra)<br> [0x80002a1c]:sw t5, 384(ra)<br> [0x80002a20]:sw a6, 392(ra)<br>                                     |
| 151|[0x80012838]<br>0x00000000<br> [0x80012850]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002a94]:fmul.d t5, t3, s10, dyn<br> [0x80002a98]:csrrs a6, fcsr, zero<br> [0x80002a9c]:sw t5, 400(ra)<br> [0x80002aa0]:sw t6, 408(ra)<br> [0x80002aa4]:sw t5, 416(ra)<br> [0x80002aa8]:sw a6, 424(ra)<br>                                     |
| 152|[0x80012858]<br>0x00000000<br> [0x80012870]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002b1c]:fmul.d t5, t3, s10, dyn<br> [0x80002b20]:csrrs a6, fcsr, zero<br> [0x80002b24]:sw t5, 432(ra)<br> [0x80002b28]:sw t6, 440(ra)<br> [0x80002b2c]:sw t5, 448(ra)<br> [0x80002b30]:sw a6, 456(ra)<br>                                     |
| 153|[0x80012878]<br>0x00000000<br> [0x80012890]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002ba0]:fmul.d t5, t3, s10, dyn<br> [0x80002ba4]:csrrs a6, fcsr, zero<br> [0x80002ba8]:sw t5, 464(ra)<br> [0x80002bac]:sw t6, 472(ra)<br> [0x80002bb0]:sw t5, 480(ra)<br> [0x80002bb4]:sw a6, 488(ra)<br>                                     |
| 154|[0x80012898]<br>0x00000000<br> [0x800128b0]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002c24]:fmul.d t5, t3, s10, dyn<br> [0x80002c28]:csrrs a6, fcsr, zero<br> [0x80002c2c]:sw t5, 496(ra)<br> [0x80002c30]:sw t6, 504(ra)<br> [0x80002c34]:sw t5, 512(ra)<br> [0x80002c38]:sw a6, 520(ra)<br>                                     |
| 155|[0x800128b8]<br>0x00000000<br> [0x800128d0]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002ca8]:fmul.d t5, t3, s10, dyn<br> [0x80002cac]:csrrs a6, fcsr, zero<br> [0x80002cb0]:sw t5, 528(ra)<br> [0x80002cb4]:sw t6, 536(ra)<br> [0x80002cb8]:sw t5, 544(ra)<br> [0x80002cbc]:sw a6, 552(ra)<br>                                     |
| 156|[0x800128d8]<br>0x00000000<br> [0x800128f0]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002d2c]:fmul.d t5, t3, s10, dyn<br> [0x80002d30]:csrrs a6, fcsr, zero<br> [0x80002d34]:sw t5, 560(ra)<br> [0x80002d38]:sw t6, 568(ra)<br> [0x80002d3c]:sw t5, 576(ra)<br> [0x80002d40]:sw a6, 584(ra)<br>                                     |
| 157|[0x800128f8]<br>0xFFFFFFFD<br> [0x80012910]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002db4]:fmul.d t5, t3, s10, dyn<br> [0x80002db8]:csrrs a6, fcsr, zero<br> [0x80002dbc]:sw t5, 592(ra)<br> [0x80002dc0]:sw t6, 600(ra)<br> [0x80002dc4]:sw t5, 608(ra)<br> [0x80002dc8]:sw a6, 616(ra)<br>                                     |
| 158|[0x80012918]<br>0xFFFFFFFD<br> [0x80012930]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002e3c]:fmul.d t5, t3, s10, dyn<br> [0x80002e40]:csrrs a6, fcsr, zero<br> [0x80002e44]:sw t5, 624(ra)<br> [0x80002e48]:sw t6, 632(ra)<br> [0x80002e4c]:sw t5, 640(ra)<br> [0x80002e50]:sw a6, 648(ra)<br>                                     |
| 159|[0x80012938]<br>0x00000000<br> [0x80012950]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002ec0]:fmul.d t5, t3, s10, dyn<br> [0x80002ec4]:csrrs a6, fcsr, zero<br> [0x80002ec8]:sw t5, 656(ra)<br> [0x80002ecc]:sw t6, 664(ra)<br> [0x80002ed0]:sw t5, 672(ra)<br> [0x80002ed4]:sw a6, 680(ra)<br>                                     |
| 160|[0x80012958]<br>0x00000000<br> [0x80012970]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002f44]:fmul.d t5, t3, s10, dyn<br> [0x80002f48]:csrrs a6, fcsr, zero<br> [0x80002f4c]:sw t5, 688(ra)<br> [0x80002f50]:sw t6, 696(ra)<br> [0x80002f54]:sw t5, 704(ra)<br> [0x80002f58]:sw a6, 712(ra)<br>                                     |
| 161|[0x80012978]<br>0x00000000<br> [0x80012990]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80002fc8]:fmul.d t5, t3, s10, dyn<br> [0x80002fcc]:csrrs a6, fcsr, zero<br> [0x80002fd0]:sw t5, 720(ra)<br> [0x80002fd4]:sw t6, 728(ra)<br> [0x80002fd8]:sw t5, 736(ra)<br> [0x80002fdc]:sw a6, 744(ra)<br>                                     |
| 162|[0x80012998]<br>0x00000000<br> [0x800129b0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000304c]:fmul.d t5, t3, s10, dyn<br> [0x80003050]:csrrs a6, fcsr, zero<br> [0x80003054]:sw t5, 752(ra)<br> [0x80003058]:sw t6, 760(ra)<br> [0x8000305c]:sw t5, 768(ra)<br> [0x80003060]:sw a6, 776(ra)<br>                                     |
| 163|[0x800129b8]<br>0x00000000<br> [0x800129d0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800030d0]:fmul.d t5, t3, s10, dyn<br> [0x800030d4]:csrrs a6, fcsr, zero<br> [0x800030d8]:sw t5, 784(ra)<br> [0x800030dc]:sw t6, 792(ra)<br> [0x800030e0]:sw t5, 800(ra)<br> [0x800030e4]:sw a6, 808(ra)<br>                                     |
| 164|[0x800129d8]<br>0x00000000<br> [0x800129f0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80003154]:fmul.d t5, t3, s10, dyn<br> [0x80003158]:csrrs a6, fcsr, zero<br> [0x8000315c]:sw t5, 816(ra)<br> [0x80003160]:sw t6, 824(ra)<br> [0x80003164]:sw t5, 832(ra)<br> [0x80003168]:sw a6, 840(ra)<br>                                     |
| 165|[0x800129f8]<br>0x00000000<br> [0x80012a10]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800031d8]:fmul.d t5, t3, s10, dyn<br> [0x800031dc]:csrrs a6, fcsr, zero<br> [0x800031e0]:sw t5, 848(ra)<br> [0x800031e4]:sw t6, 856(ra)<br> [0x800031e8]:sw t5, 864(ra)<br> [0x800031ec]:sw a6, 872(ra)<br>                                     |
| 166|[0x80012a18]<br>0x00000000<br> [0x80012a30]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000325c]:fmul.d t5, t3, s10, dyn<br> [0x80003260]:csrrs a6, fcsr, zero<br> [0x80003264]:sw t5, 880(ra)<br> [0x80003268]:sw t6, 888(ra)<br> [0x8000326c]:sw t5, 896(ra)<br> [0x80003270]:sw a6, 904(ra)<br>                                     |
| 167|[0x80012a38]<br>0xFFFFFFFF<br> [0x80012a50]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800032e0]:fmul.d t5, t3, s10, dyn<br> [0x800032e4]:csrrs a6, fcsr, zero<br> [0x800032e8]:sw t5, 912(ra)<br> [0x800032ec]:sw t6, 920(ra)<br> [0x800032f0]:sw t5, 928(ra)<br> [0x800032f4]:sw a6, 936(ra)<br>                                     |
| 168|[0x80012a58]<br>0x00000000<br> [0x80012a70]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80003364]:fmul.d t5, t3, s10, dyn<br> [0x80003368]:csrrs a6, fcsr, zero<br> [0x8000336c]:sw t5, 944(ra)<br> [0x80003370]:sw t6, 952(ra)<br> [0x80003374]:sw t5, 960(ra)<br> [0x80003378]:sw a6, 968(ra)<br>                                     |
| 169|[0x80012a78]<br>0x00000000<br> [0x80012a90]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800033e8]:fmul.d t5, t3, s10, dyn<br> [0x800033ec]:csrrs a6, fcsr, zero<br> [0x800033f0]:sw t5, 976(ra)<br> [0x800033f4]:sw t6, 984(ra)<br> [0x800033f8]:sw t5, 992(ra)<br> [0x800033fc]:sw a6, 1000(ra)<br>                                    |
| 170|[0x80012a98]<br>0x00000000<br> [0x80012ab0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000346c]:fmul.d t5, t3, s10, dyn<br> [0x80003470]:csrrs a6, fcsr, zero<br> [0x80003474]:sw t5, 1008(ra)<br> [0x80003478]:sw t6, 1016(ra)<br> [0x8000347c]:sw t5, 1024(ra)<br> [0x80003480]:sw a6, 1032(ra)<br>                                 |
| 171|[0x80012ab8]<br>0x00000000<br> [0x80012ad0]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800034f0]:fmul.d t5, t3, s10, dyn<br> [0x800034f4]:csrrs a6, fcsr, zero<br> [0x800034f8]:sw t5, 1040(ra)<br> [0x800034fc]:sw t6, 1048(ra)<br> [0x80003500]:sw t5, 1056(ra)<br> [0x80003504]:sw a6, 1064(ra)<br>                                 |
| 172|[0x80012ad8]<br>0x00000000<br> [0x80012af0]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80003574]:fmul.d t5, t3, s10, dyn<br> [0x80003578]:csrrs a6, fcsr, zero<br> [0x8000357c]:sw t5, 1072(ra)<br> [0x80003580]:sw t6, 1080(ra)<br> [0x80003584]:sw t5, 1088(ra)<br> [0x80003588]:sw a6, 1096(ra)<br>                                 |
| 173|[0x80012af8]<br>0x00000000<br> [0x80012b10]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800035f8]:fmul.d t5, t3, s10, dyn<br> [0x800035fc]:csrrs a6, fcsr, zero<br> [0x80003600]:sw t5, 1104(ra)<br> [0x80003604]:sw t6, 1112(ra)<br> [0x80003608]:sw t5, 1120(ra)<br> [0x8000360c]:sw a6, 1128(ra)<br>                                 |
| 174|[0x80012b18]<br>0x00000000<br> [0x80012b30]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000367c]:fmul.d t5, t3, s10, dyn<br> [0x80003680]:csrrs a6, fcsr, zero<br> [0x80003684]:sw t5, 1136(ra)<br> [0x80003688]:sw t6, 1144(ra)<br> [0x8000368c]:sw t5, 1152(ra)<br> [0x80003690]:sw a6, 1160(ra)<br>                                 |
| 175|[0x80012b38]<br>0x00000000<br> [0x80012b50]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80003704]:fmul.d t5, t3, s10, dyn<br> [0x80003708]:csrrs a6, fcsr, zero<br> [0x8000370c]:sw t5, 1168(ra)<br> [0x80003710]:sw t6, 1176(ra)<br> [0x80003714]:sw t5, 1184(ra)<br> [0x80003718]:sw a6, 1192(ra)<br>                                 |
| 176|[0x80012b58]<br>0x00000000<br> [0x80012b70]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000378c]:fmul.d t5, t3, s10, dyn<br> [0x80003790]:csrrs a6, fcsr, zero<br> [0x80003794]:sw t5, 1200(ra)<br> [0x80003798]:sw t6, 1208(ra)<br> [0x8000379c]:sw t5, 1216(ra)<br> [0x800037a0]:sw a6, 1224(ra)<br>                                 |
| 177|[0x80012b78]<br>0x00000000<br> [0x80012b90]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80003810]:fmul.d t5, t3, s10, dyn<br> [0x80003814]:csrrs a6, fcsr, zero<br> [0x80003818]:sw t5, 1232(ra)<br> [0x8000381c]:sw t6, 1240(ra)<br> [0x80003820]:sw t5, 1248(ra)<br> [0x80003824]:sw a6, 1256(ra)<br>                                 |
| 178|[0x80012b98]<br>0x00000000<br> [0x80012bb0]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80003894]:fmul.d t5, t3, s10, dyn<br> [0x80003898]:csrrs a6, fcsr, zero<br> [0x8000389c]:sw t5, 1264(ra)<br> [0x800038a0]:sw t6, 1272(ra)<br> [0x800038a4]:sw t5, 1280(ra)<br> [0x800038a8]:sw a6, 1288(ra)<br>                                 |
| 179|[0x80012bb8]<br>0x00000000<br> [0x80012bd0]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80003918]:fmul.d t5, t3, s10, dyn<br> [0x8000391c]:csrrs a6, fcsr, zero<br> [0x80003920]:sw t5, 1296(ra)<br> [0x80003924]:sw t6, 1304(ra)<br> [0x80003928]:sw t5, 1312(ra)<br> [0x8000392c]:sw a6, 1320(ra)<br>                                 |
| 180|[0x80012bd8]<br>0x00000000<br> [0x80012bf0]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000399c]:fmul.d t5, t3, s10, dyn<br> [0x800039a0]:csrrs a6, fcsr, zero<br> [0x800039a4]:sw t5, 1328(ra)<br> [0x800039a8]:sw t6, 1336(ra)<br> [0x800039ac]:sw t5, 1344(ra)<br> [0x800039b0]:sw a6, 1352(ra)<br>                                 |
| 181|[0x80012bf8]<br>0xFFFFFFFD<br> [0x80012c10]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80003a24]:fmul.d t5, t3, s10, dyn<br> [0x80003a28]:csrrs a6, fcsr, zero<br> [0x80003a2c]:sw t5, 1360(ra)<br> [0x80003a30]:sw t6, 1368(ra)<br> [0x80003a34]:sw t5, 1376(ra)<br> [0x80003a38]:sw a6, 1384(ra)<br>                                 |
| 182|[0x80012c18]<br>0xFFFFFFFD<br> [0x80012c30]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80003aac]:fmul.d t5, t3, s10, dyn<br> [0x80003ab0]:csrrs a6, fcsr, zero<br> [0x80003ab4]:sw t5, 1392(ra)<br> [0x80003ab8]:sw t6, 1400(ra)<br> [0x80003abc]:sw t5, 1408(ra)<br> [0x80003ac0]:sw a6, 1416(ra)<br>                                 |
| 183|[0x80012c38]<br>0x00000000<br> [0x80012c50]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80003b30]:fmul.d t5, t3, s10, dyn<br> [0x80003b34]:csrrs a6, fcsr, zero<br> [0x80003b38]:sw t5, 1424(ra)<br> [0x80003b3c]:sw t6, 1432(ra)<br> [0x80003b40]:sw t5, 1440(ra)<br> [0x80003b44]:sw a6, 1448(ra)<br>                                 |
| 184|[0x80012c58]<br>0x00000000<br> [0x80012c70]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80003bb4]:fmul.d t5, t3, s10, dyn<br> [0x80003bb8]:csrrs a6, fcsr, zero<br> [0x80003bbc]:sw t5, 1456(ra)<br> [0x80003bc0]:sw t6, 1464(ra)<br> [0x80003bc4]:sw t5, 1472(ra)<br> [0x80003bc8]:sw a6, 1480(ra)<br>                                 |
| 185|[0x80012c78]<br>0x00000000<br> [0x80012c90]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80003c38]:fmul.d t5, t3, s10, dyn<br> [0x80003c3c]:csrrs a6, fcsr, zero<br> [0x80003c40]:sw t5, 1488(ra)<br> [0x80003c44]:sw t6, 1496(ra)<br> [0x80003c48]:sw t5, 1504(ra)<br> [0x80003c4c]:sw a6, 1512(ra)<br>                                 |
| 186|[0x80012c98]<br>0x00000000<br> [0x80012cb0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80003cbc]:fmul.d t5, t3, s10, dyn<br> [0x80003cc0]:csrrs a6, fcsr, zero<br> [0x80003cc4]:sw t5, 1520(ra)<br> [0x80003cc8]:sw t6, 1528(ra)<br> [0x80003ccc]:sw t5, 1536(ra)<br> [0x80003cd0]:sw a6, 1544(ra)<br>                                 |
| 187|[0x80012cb8]<br>0x00000000<br> [0x80012cd0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80003d40]:fmul.d t5, t3, s10, dyn<br> [0x80003d44]:csrrs a6, fcsr, zero<br> [0x80003d48]:sw t5, 1552(ra)<br> [0x80003d4c]:sw t6, 1560(ra)<br> [0x80003d50]:sw t5, 1568(ra)<br> [0x80003d54]:sw a6, 1576(ra)<br>                                 |
| 188|[0x80012cd8]<br>0x00000000<br> [0x80012cf0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80003dc4]:fmul.d t5, t3, s10, dyn<br> [0x80003dc8]:csrrs a6, fcsr, zero<br> [0x80003dcc]:sw t5, 1584(ra)<br> [0x80003dd0]:sw t6, 1592(ra)<br> [0x80003dd4]:sw t5, 1600(ra)<br> [0x80003dd8]:sw a6, 1608(ra)<br>                                 |
| 189|[0x80012cf8]<br>0x00000000<br> [0x80012d10]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80003e48]:fmul.d t5, t3, s10, dyn<br> [0x80003e4c]:csrrs a6, fcsr, zero<br> [0x80003e50]:sw t5, 1616(ra)<br> [0x80003e54]:sw t6, 1624(ra)<br> [0x80003e58]:sw t5, 1632(ra)<br> [0x80003e5c]:sw a6, 1640(ra)<br>                                 |
| 190|[0x80012d18]<br>0x00000000<br> [0x80012d30]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80003ecc]:fmul.d t5, t3, s10, dyn<br> [0x80003ed0]:csrrs a6, fcsr, zero<br> [0x80003ed4]:sw t5, 1648(ra)<br> [0x80003ed8]:sw t6, 1656(ra)<br> [0x80003edc]:sw t5, 1664(ra)<br> [0x80003ee0]:sw a6, 1672(ra)<br>                                 |
| 191|[0x80012d38]<br>0xFFFFFFFF<br> [0x80012d50]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80003f50]:fmul.d t5, t3, s10, dyn<br> [0x80003f54]:csrrs a6, fcsr, zero<br> [0x80003f58]:sw t5, 1680(ra)<br> [0x80003f5c]:sw t6, 1688(ra)<br> [0x80003f60]:sw t5, 1696(ra)<br> [0x80003f64]:sw a6, 1704(ra)<br>                                 |
| 192|[0x80012d58]<br>0x00000000<br> [0x80012d70]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80003fd4]:fmul.d t5, t3, s10, dyn<br> [0x80003fd8]:csrrs a6, fcsr, zero<br> [0x80003fdc]:sw t5, 1712(ra)<br> [0x80003fe0]:sw t6, 1720(ra)<br> [0x80003fe4]:sw t5, 1728(ra)<br> [0x80003fe8]:sw a6, 1736(ra)<br>                                 |
| 193|[0x80012d78]<br>0x00000000<br> [0x80012d90]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80004054]:fmul.d t5, t3, s10, dyn<br> [0x80004058]:csrrs a6, fcsr, zero<br> [0x8000405c]:sw t5, 1744(ra)<br> [0x80004060]:sw t6, 1752(ra)<br> [0x80004064]:sw t5, 1760(ra)<br> [0x80004068]:sw a6, 1768(ra)<br>                                 |
| 194|[0x80012d98]<br>0x00000000<br> [0x80012db0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800040d4]:fmul.d t5, t3, s10, dyn<br> [0x800040d8]:csrrs a6, fcsr, zero<br> [0x800040dc]:sw t5, 1776(ra)<br> [0x800040e0]:sw t6, 1784(ra)<br> [0x800040e4]:sw t5, 1792(ra)<br> [0x800040e8]:sw a6, 1800(ra)<br>                                 |
| 195|[0x80012db8]<br>0x00000000<br> [0x80012dd0]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80004154]:fmul.d t5, t3, s10, dyn<br> [0x80004158]:csrrs a6, fcsr, zero<br> [0x8000415c]:sw t5, 1808(ra)<br> [0x80004160]:sw t6, 1816(ra)<br> [0x80004164]:sw t5, 1824(ra)<br> [0x80004168]:sw a6, 1832(ra)<br>                                 |
| 196|[0x80012dd8]<br>0x00000000<br> [0x80012df0]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800041d4]:fmul.d t5, t3, s10, dyn<br> [0x800041d8]:csrrs a6, fcsr, zero<br> [0x800041dc]:sw t5, 1840(ra)<br> [0x800041e0]:sw t6, 1848(ra)<br> [0x800041e4]:sw t5, 1856(ra)<br> [0x800041e8]:sw a6, 1864(ra)<br>                                 |
| 197|[0x80012df8]<br>0x00000000<br> [0x80012e10]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80004254]:fmul.d t5, t3, s10, dyn<br> [0x80004258]:csrrs a6, fcsr, zero<br> [0x8000425c]:sw t5, 1872(ra)<br> [0x80004260]:sw t6, 1880(ra)<br> [0x80004264]:sw t5, 1888(ra)<br> [0x80004268]:sw a6, 1896(ra)<br>                                 |
| 198|[0x80012e18]<br>0x00000000<br> [0x80012e30]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800042d4]:fmul.d t5, t3, s10, dyn<br> [0x800042d8]:csrrs a6, fcsr, zero<br> [0x800042dc]:sw t5, 1904(ra)<br> [0x800042e0]:sw t6, 1912(ra)<br> [0x800042e4]:sw t5, 1920(ra)<br> [0x800042e8]:sw a6, 1928(ra)<br>                                 |
| 199|[0x80012e38]<br>0x00000000<br> [0x80012e50]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80004358]:fmul.d t5, t3, s10, dyn<br> [0x8000435c]:csrrs a6, fcsr, zero<br> [0x80004360]:sw t5, 1936(ra)<br> [0x80004364]:sw t6, 1944(ra)<br> [0x80004368]:sw t5, 1952(ra)<br> [0x8000436c]:sw a6, 1960(ra)<br>                                 |
| 200|[0x80012e58]<br>0x00000000<br> [0x80012e70]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800043dc]:fmul.d t5, t3, s10, dyn<br> [0x800043e0]:csrrs a6, fcsr, zero<br> [0x800043e4]:sw t5, 1968(ra)<br> [0x800043e8]:sw t6, 1976(ra)<br> [0x800043ec]:sw t5, 1984(ra)<br> [0x800043f0]:sw a6, 1992(ra)<br>                                 |
| 201|[0x80012e78]<br>0x00000000<br> [0x80012e90]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000445c]:fmul.d t5, t3, s10, dyn<br> [0x80004460]:csrrs a6, fcsr, zero<br> [0x80004464]:sw t5, 2000(ra)<br> [0x80004468]:sw t6, 2008(ra)<br> [0x8000446c]:sw t5, 2016(ra)<br> [0x80004470]:sw a6, 2024(ra)<br>                                 |
| 202|[0x80012e98]<br>0x00000000<br> [0x80012eb0]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800044dc]:fmul.d t5, t3, s10, dyn<br> [0x800044e0]:csrrs a6, fcsr, zero<br> [0x800044e4]:sw t5, 2032(ra)<br> [0x800044e8]:sw t6, 2040(ra)<br> [0x800044ec]:addi ra, ra, 2040<br> [0x800044f0]:sw t5, 8(ra)<br> [0x800044f4]:sw a6, 16(ra)<br>   |
| 203|[0x80012eb8]<br>0x00000000<br> [0x80012ed0]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80004560]:fmul.d t5, t3, s10, dyn<br> [0x80004564]:csrrs a6, fcsr, zero<br> [0x80004568]:sw t5, 24(ra)<br> [0x8000456c]:sw t6, 32(ra)<br> [0x80004570]:sw t5, 40(ra)<br> [0x80004574]:sw a6, 48(ra)<br>                                         |
| 204|[0x80012ed8]<br>0x00000000<br> [0x80012ef0]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800045e0]:fmul.d t5, t3, s10, dyn<br> [0x800045e4]:csrrs a6, fcsr, zero<br> [0x800045e8]:sw t5, 56(ra)<br> [0x800045ec]:sw t6, 64(ra)<br> [0x800045f0]:sw t5, 72(ra)<br> [0x800045f4]:sw a6, 80(ra)<br>                                         |
| 205|[0x80012ef8]<br>0xFFFFFFFF<br> [0x80012f10]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80004664]:fmul.d t5, t3, s10, dyn<br> [0x80004668]:csrrs a6, fcsr, zero<br> [0x8000466c]:sw t5, 88(ra)<br> [0x80004670]:sw t6, 96(ra)<br> [0x80004674]:sw t5, 104(ra)<br> [0x80004678]:sw a6, 112(ra)<br>                                       |
| 206|[0x80012f18]<br>0xFFFFFFFF<br> [0x80012f30]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800046e8]:fmul.d t5, t3, s10, dyn<br> [0x800046ec]:csrrs a6, fcsr, zero<br> [0x800046f0]:sw t5, 120(ra)<br> [0x800046f4]:sw t6, 128(ra)<br> [0x800046f8]:sw t5, 136(ra)<br> [0x800046fc]:sw a6, 144(ra)<br>                                     |
| 207|[0x80012f38]<br>0x00000000<br> [0x80012f50]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80004768]:fmul.d t5, t3, s10, dyn<br> [0x8000476c]:csrrs a6, fcsr, zero<br> [0x80004770]:sw t5, 152(ra)<br> [0x80004774]:sw t6, 160(ra)<br> [0x80004778]:sw t5, 168(ra)<br> [0x8000477c]:sw a6, 176(ra)<br>                                     |
| 208|[0x80012f58]<br>0x00000000<br> [0x80012f70]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800047e8]:fmul.d t5, t3, s10, dyn<br> [0x800047ec]:csrrs a6, fcsr, zero<br> [0x800047f0]:sw t5, 184(ra)<br> [0x800047f4]:sw t6, 192(ra)<br> [0x800047f8]:sw t5, 200(ra)<br> [0x800047fc]:sw a6, 208(ra)<br>                                     |
| 209|[0x80012f78]<br>0x00000000<br> [0x80012f90]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80004868]:fmul.d t5, t3, s10, dyn<br> [0x8000486c]:csrrs a6, fcsr, zero<br> [0x80004870]:sw t5, 216(ra)<br> [0x80004874]:sw t6, 224(ra)<br> [0x80004878]:sw t5, 232(ra)<br> [0x8000487c]:sw a6, 240(ra)<br>                                     |
| 210|[0x80012f98]<br>0x00000000<br> [0x80012fb0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800048e8]:fmul.d t5, t3, s10, dyn<br> [0x800048ec]:csrrs a6, fcsr, zero<br> [0x800048f0]:sw t5, 248(ra)<br> [0x800048f4]:sw t6, 256(ra)<br> [0x800048f8]:sw t5, 264(ra)<br> [0x800048fc]:sw a6, 272(ra)<br>                                     |
| 211|[0x80012fb8]<br>0x00000000<br> [0x80012fd0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80004968]:fmul.d t5, t3, s10, dyn<br> [0x8000496c]:csrrs a6, fcsr, zero<br> [0x80004970]:sw t5, 280(ra)<br> [0x80004974]:sw t6, 288(ra)<br> [0x80004978]:sw t5, 296(ra)<br> [0x8000497c]:sw a6, 304(ra)<br>                                     |
| 212|[0x80012fd8]<br>0x00000000<br> [0x80012ff0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800049e8]:fmul.d t5, t3, s10, dyn<br> [0x800049ec]:csrrs a6, fcsr, zero<br> [0x800049f0]:sw t5, 312(ra)<br> [0x800049f4]:sw t6, 320(ra)<br> [0x800049f8]:sw t5, 328(ra)<br> [0x800049fc]:sw a6, 336(ra)<br>                                     |
| 213|[0x80012ff8]<br>0x00000000<br> [0x80013010]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80004a68]:fmul.d t5, t3, s10, dyn<br> [0x80004a6c]:csrrs a6, fcsr, zero<br> [0x80004a70]:sw t5, 344(ra)<br> [0x80004a74]:sw t6, 352(ra)<br> [0x80004a78]:sw t5, 360(ra)<br> [0x80004a7c]:sw a6, 368(ra)<br>                                     |
| 214|[0x80013018]<br>0x00000000<br> [0x80013030]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80004ae8]:fmul.d t5, t3, s10, dyn<br> [0x80004aec]:csrrs a6, fcsr, zero<br> [0x80004af0]:sw t5, 376(ra)<br> [0x80004af4]:sw t6, 384(ra)<br> [0x80004af8]:sw t5, 392(ra)<br> [0x80004afc]:sw a6, 400(ra)<br>                                     |
| 215|[0x80013038]<br>0x00000000<br> [0x80013050]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80004b68]:fmul.d t5, t3, s10, dyn<br> [0x80004b6c]:csrrs a6, fcsr, zero<br> [0x80004b70]:sw t5, 408(ra)<br> [0x80004b74]:sw t6, 416(ra)<br> [0x80004b78]:sw t5, 424(ra)<br> [0x80004b7c]:sw a6, 432(ra)<br>                                     |
| 216|[0x80013058]<br>0x00000000<br> [0x80013070]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80004be8]:fmul.d t5, t3, s10, dyn<br> [0x80004bec]:csrrs a6, fcsr, zero<br> [0x80004bf0]:sw t5, 440(ra)<br> [0x80004bf4]:sw t6, 448(ra)<br> [0x80004bf8]:sw t5, 456(ra)<br> [0x80004bfc]:sw a6, 464(ra)<br>                                     |
| 217|[0x80013078]<br>0x00000000<br> [0x80013090]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80004c68]:fmul.d t5, t3, s10, dyn<br> [0x80004c6c]:csrrs a6, fcsr, zero<br> [0x80004c70]:sw t5, 472(ra)<br> [0x80004c74]:sw t6, 480(ra)<br> [0x80004c78]:sw t5, 488(ra)<br> [0x80004c7c]:sw a6, 496(ra)<br>                                     |
| 218|[0x80013098]<br>0x00000000<br> [0x800130b0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80004ce8]:fmul.d t5, t3, s10, dyn<br> [0x80004cec]:csrrs a6, fcsr, zero<br> [0x80004cf0]:sw t5, 504(ra)<br> [0x80004cf4]:sw t6, 512(ra)<br> [0x80004cf8]:sw t5, 520(ra)<br> [0x80004cfc]:sw a6, 528(ra)<br>                                     |
| 219|[0x800130b8]<br>0x00000000<br> [0x800130d0]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80004d68]:fmul.d t5, t3, s10, dyn<br> [0x80004d6c]:csrrs a6, fcsr, zero<br> [0x80004d70]:sw t5, 536(ra)<br> [0x80004d74]:sw t6, 544(ra)<br> [0x80004d78]:sw t5, 552(ra)<br> [0x80004d7c]:sw a6, 560(ra)<br>                                     |
| 220|[0x800130d8]<br>0x00000000<br> [0x800130f0]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80004de8]:fmul.d t5, t3, s10, dyn<br> [0x80004dec]:csrrs a6, fcsr, zero<br> [0x80004df0]:sw t5, 568(ra)<br> [0x80004df4]:sw t6, 576(ra)<br> [0x80004df8]:sw t5, 584(ra)<br> [0x80004dfc]:sw a6, 592(ra)<br>                                     |
| 221|[0x800130f8]<br>0x00000000<br> [0x80013110]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80004e68]:fmul.d t5, t3, s10, dyn<br> [0x80004e6c]:csrrs a6, fcsr, zero<br> [0x80004e70]:sw t5, 600(ra)<br> [0x80004e74]:sw t6, 608(ra)<br> [0x80004e78]:sw t5, 616(ra)<br> [0x80004e7c]:sw a6, 624(ra)<br>                                     |
| 222|[0x80013118]<br>0x00000000<br> [0x80013130]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80004ee8]:fmul.d t5, t3, s10, dyn<br> [0x80004eec]:csrrs a6, fcsr, zero<br> [0x80004ef0]:sw t5, 632(ra)<br> [0x80004ef4]:sw t6, 640(ra)<br> [0x80004ef8]:sw t5, 648(ra)<br> [0x80004efc]:sw a6, 656(ra)<br>                                     |
| 223|[0x80013138]<br>0x00000000<br> [0x80013150]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80004f6c]:fmul.d t5, t3, s10, dyn<br> [0x80004f70]:csrrs a6, fcsr, zero<br> [0x80004f74]:sw t5, 664(ra)<br> [0x80004f78]:sw t6, 672(ra)<br> [0x80004f7c]:sw t5, 680(ra)<br> [0x80004f80]:sw a6, 688(ra)<br>                                     |
| 224|[0x80013158]<br>0x00000000<br> [0x80013170]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80004ff0]:fmul.d t5, t3, s10, dyn<br> [0x80004ff4]:csrrs a6, fcsr, zero<br> [0x80004ff8]:sw t5, 696(ra)<br> [0x80004ffc]:sw t6, 704(ra)<br> [0x80005000]:sw t5, 712(ra)<br> [0x80005004]:sw a6, 720(ra)<br>                                     |
| 225|[0x80013178]<br>0x00000000<br> [0x80013190]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80005070]:fmul.d t5, t3, s10, dyn<br> [0x80005074]:csrrs a6, fcsr, zero<br> [0x80005078]:sw t5, 728(ra)<br> [0x8000507c]:sw t6, 736(ra)<br> [0x80005080]:sw t5, 744(ra)<br> [0x80005084]:sw a6, 752(ra)<br>                                     |
| 226|[0x80013198]<br>0x00000000<br> [0x800131b0]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800050f0]:fmul.d t5, t3, s10, dyn<br> [0x800050f4]:csrrs a6, fcsr, zero<br> [0x800050f8]:sw t5, 760(ra)<br> [0x800050fc]:sw t6, 768(ra)<br> [0x80005100]:sw t5, 776(ra)<br> [0x80005104]:sw a6, 784(ra)<br>                                     |
| 227|[0x800131b8]<br>0x00000000<br> [0x800131d0]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80005170]:fmul.d t5, t3, s10, dyn<br> [0x80005174]:csrrs a6, fcsr, zero<br> [0x80005178]:sw t5, 792(ra)<br> [0x8000517c]:sw t6, 800(ra)<br> [0x80005180]:sw t5, 808(ra)<br> [0x80005184]:sw a6, 816(ra)<br>                                     |
| 228|[0x800131d8]<br>0x00000000<br> [0x800131f0]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800051f0]:fmul.d t5, t3, s10, dyn<br> [0x800051f4]:csrrs a6, fcsr, zero<br> [0x800051f8]:sw t5, 824(ra)<br> [0x800051fc]:sw t6, 832(ra)<br> [0x80005200]:sw t5, 840(ra)<br> [0x80005204]:sw a6, 848(ra)<br>                                     |
| 229|[0x800131f8]<br>0xFFFFFFFF<br> [0x80013210]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80005274]:fmul.d t5, t3, s10, dyn<br> [0x80005278]:csrrs a6, fcsr, zero<br> [0x8000527c]:sw t5, 856(ra)<br> [0x80005280]:sw t6, 864(ra)<br> [0x80005284]:sw t5, 872(ra)<br> [0x80005288]:sw a6, 880(ra)<br>                                     |
| 230|[0x80013218]<br>0xFFFFFFFF<br> [0x80013230]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800052f8]:fmul.d t5, t3, s10, dyn<br> [0x800052fc]:csrrs a6, fcsr, zero<br> [0x80005300]:sw t5, 888(ra)<br> [0x80005304]:sw t6, 896(ra)<br> [0x80005308]:sw t5, 904(ra)<br> [0x8000530c]:sw a6, 912(ra)<br>                                     |
| 231|[0x80013238]<br>0x00000000<br> [0x80013250]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80005378]:fmul.d t5, t3, s10, dyn<br> [0x8000537c]:csrrs a6, fcsr, zero<br> [0x80005380]:sw t5, 920(ra)<br> [0x80005384]:sw t6, 928(ra)<br> [0x80005388]:sw t5, 936(ra)<br> [0x8000538c]:sw a6, 944(ra)<br>                                     |
| 232|[0x80013258]<br>0x00000000<br> [0x80013270]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800053f8]:fmul.d t5, t3, s10, dyn<br> [0x800053fc]:csrrs a6, fcsr, zero<br> [0x80005400]:sw t5, 952(ra)<br> [0x80005404]:sw t6, 960(ra)<br> [0x80005408]:sw t5, 968(ra)<br> [0x8000540c]:sw a6, 976(ra)<br>                                     |
| 233|[0x80013278]<br>0x00000000<br> [0x80013290]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80005478]:fmul.d t5, t3, s10, dyn<br> [0x8000547c]:csrrs a6, fcsr, zero<br> [0x80005480]:sw t5, 984(ra)<br> [0x80005484]:sw t6, 992(ra)<br> [0x80005488]:sw t5, 1000(ra)<br> [0x8000548c]:sw a6, 1008(ra)<br>                                   |
| 234|[0x80013298]<br>0x00000000<br> [0x800132b0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800054f8]:fmul.d t5, t3, s10, dyn<br> [0x800054fc]:csrrs a6, fcsr, zero<br> [0x80005500]:sw t5, 1016(ra)<br> [0x80005504]:sw t6, 1024(ra)<br> [0x80005508]:sw t5, 1032(ra)<br> [0x8000550c]:sw a6, 1040(ra)<br>                                 |
| 235|[0x800132b8]<br>0x00000000<br> [0x800132d0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80005578]:fmul.d t5, t3, s10, dyn<br> [0x8000557c]:csrrs a6, fcsr, zero<br> [0x80005580]:sw t5, 1048(ra)<br> [0x80005584]:sw t6, 1056(ra)<br> [0x80005588]:sw t5, 1064(ra)<br> [0x8000558c]:sw a6, 1072(ra)<br>                                 |
| 236|[0x800132d8]<br>0x00000000<br> [0x800132f0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800055f8]:fmul.d t5, t3, s10, dyn<br> [0x800055fc]:csrrs a6, fcsr, zero<br> [0x80005600]:sw t5, 1080(ra)<br> [0x80005604]:sw t6, 1088(ra)<br> [0x80005608]:sw t5, 1096(ra)<br> [0x8000560c]:sw a6, 1104(ra)<br>                                 |
| 237|[0x800132f8]<br>0x00000000<br> [0x80013310]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80005678]:fmul.d t5, t3, s10, dyn<br> [0x8000567c]:csrrs a6, fcsr, zero<br> [0x80005680]:sw t5, 1112(ra)<br> [0x80005684]:sw t6, 1120(ra)<br> [0x80005688]:sw t5, 1128(ra)<br> [0x8000568c]:sw a6, 1136(ra)<br>                                 |
| 238|[0x80013318]<br>0x00000000<br> [0x80013330]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800056f8]:fmul.d t5, t3, s10, dyn<br> [0x800056fc]:csrrs a6, fcsr, zero<br> [0x80005700]:sw t5, 1144(ra)<br> [0x80005704]:sw t6, 1152(ra)<br> [0x80005708]:sw t5, 1160(ra)<br> [0x8000570c]:sw a6, 1168(ra)<br>                                 |
| 239|[0x80013338]<br>0x00000000<br> [0x80013350]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80005778]:fmul.d t5, t3, s10, dyn<br> [0x8000577c]:csrrs a6, fcsr, zero<br> [0x80005780]:sw t5, 1176(ra)<br> [0x80005784]:sw t6, 1184(ra)<br> [0x80005788]:sw t5, 1192(ra)<br> [0x8000578c]:sw a6, 1200(ra)<br>                                 |
| 240|[0x80013358]<br>0x00000000<br> [0x80013370]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800057f8]:fmul.d t5, t3, s10, dyn<br> [0x800057fc]:csrrs a6, fcsr, zero<br> [0x80005800]:sw t5, 1208(ra)<br> [0x80005804]:sw t6, 1216(ra)<br> [0x80005808]:sw t5, 1224(ra)<br> [0x8000580c]:sw a6, 1232(ra)<br>                                 |
| 241|[0x80013378]<br>0x00000000<br> [0x80013390]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80005878]:fmul.d t5, t3, s10, dyn<br> [0x8000587c]:csrrs a6, fcsr, zero<br> [0x80005880]:sw t5, 1240(ra)<br> [0x80005884]:sw t6, 1248(ra)<br> [0x80005888]:sw t5, 1256(ra)<br> [0x8000588c]:sw a6, 1264(ra)<br>                                 |
| 242|[0x80013398]<br>0x00000000<br> [0x800133b0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800058f8]:fmul.d t5, t3, s10, dyn<br> [0x800058fc]:csrrs a6, fcsr, zero<br> [0x80005900]:sw t5, 1272(ra)<br> [0x80005904]:sw t6, 1280(ra)<br> [0x80005908]:sw t5, 1288(ra)<br> [0x8000590c]:sw a6, 1296(ra)<br>                                 |
| 243|[0x800133b8]<br>0x00000000<br> [0x800133d0]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80005978]:fmul.d t5, t3, s10, dyn<br> [0x8000597c]:csrrs a6, fcsr, zero<br> [0x80005980]:sw t5, 1304(ra)<br> [0x80005984]:sw t6, 1312(ra)<br> [0x80005988]:sw t5, 1320(ra)<br> [0x8000598c]:sw a6, 1328(ra)<br>                                 |
| 244|[0x800133d8]<br>0x00000000<br> [0x800133f0]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800059f8]:fmul.d t5, t3, s10, dyn<br> [0x800059fc]:csrrs a6, fcsr, zero<br> [0x80005a00]:sw t5, 1336(ra)<br> [0x80005a04]:sw t6, 1344(ra)<br> [0x80005a08]:sw t5, 1352(ra)<br> [0x80005a0c]:sw a6, 1360(ra)<br>                                 |
| 245|[0x800133f8]<br>0x00000000<br> [0x80013410]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80005a78]:fmul.d t5, t3, s10, dyn<br> [0x80005a7c]:csrrs a6, fcsr, zero<br> [0x80005a80]:sw t5, 1368(ra)<br> [0x80005a84]:sw t6, 1376(ra)<br> [0x80005a88]:sw t5, 1384(ra)<br> [0x80005a8c]:sw a6, 1392(ra)<br>                                 |
| 246|[0x80013418]<br>0x00000000<br> [0x80013430]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80005af8]:fmul.d t5, t3, s10, dyn<br> [0x80005afc]:csrrs a6, fcsr, zero<br> [0x80005b00]:sw t5, 1400(ra)<br> [0x80005b04]:sw t6, 1408(ra)<br> [0x80005b08]:sw t5, 1416(ra)<br> [0x80005b0c]:sw a6, 1424(ra)<br>                                 |
| 247|[0x80013438]<br>0x00000000<br> [0x80013450]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80005b7c]:fmul.d t5, t3, s10, dyn<br> [0x80005b80]:csrrs a6, fcsr, zero<br> [0x80005b84]:sw t5, 1432(ra)<br> [0x80005b88]:sw t6, 1440(ra)<br> [0x80005b8c]:sw t5, 1448(ra)<br> [0x80005b90]:sw a6, 1456(ra)<br>                                 |
| 248|[0x80013458]<br>0x00000000<br> [0x80013470]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80005c00]:fmul.d t5, t3, s10, dyn<br> [0x80005c04]:csrrs a6, fcsr, zero<br> [0x80005c08]:sw t5, 1464(ra)<br> [0x80005c0c]:sw t6, 1472(ra)<br> [0x80005c10]:sw t5, 1480(ra)<br> [0x80005c14]:sw a6, 1488(ra)<br>                                 |
| 249|[0x80013478]<br>0x00000000<br> [0x80013490]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80005c80]:fmul.d t5, t3, s10, dyn<br> [0x80005c84]:csrrs a6, fcsr, zero<br> [0x80005c88]:sw t5, 1496(ra)<br> [0x80005c8c]:sw t6, 1504(ra)<br> [0x80005c90]:sw t5, 1512(ra)<br> [0x80005c94]:sw a6, 1520(ra)<br>                                 |
| 250|[0x80013498]<br>0x00000000<br> [0x800134b0]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80005d00]:fmul.d t5, t3, s10, dyn<br> [0x80005d04]:csrrs a6, fcsr, zero<br> [0x80005d08]:sw t5, 1528(ra)<br> [0x80005d0c]:sw t6, 1536(ra)<br> [0x80005d10]:sw t5, 1544(ra)<br> [0x80005d14]:sw a6, 1552(ra)<br>                                 |
| 251|[0x800134b8]<br>0x00000000<br> [0x800134d0]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80005d80]:fmul.d t5, t3, s10, dyn<br> [0x80005d84]:csrrs a6, fcsr, zero<br> [0x80005d88]:sw t5, 1560(ra)<br> [0x80005d8c]:sw t6, 1568(ra)<br> [0x80005d90]:sw t5, 1576(ra)<br> [0x80005d94]:sw a6, 1584(ra)<br>                                 |
| 252|[0x800134d8]<br>0x00000000<br> [0x800134f0]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80005e00]:fmul.d t5, t3, s10, dyn<br> [0x80005e04]:csrrs a6, fcsr, zero<br> [0x80005e08]:sw t5, 1592(ra)<br> [0x80005e0c]:sw t6, 1600(ra)<br> [0x80005e10]:sw t5, 1608(ra)<br> [0x80005e14]:sw a6, 1616(ra)<br>                                 |
| 253|[0x800134f8]<br>0x00000001<br> [0x80013510]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80005e84]:fmul.d t5, t3, s10, dyn<br> [0x80005e88]:csrrs a6, fcsr, zero<br> [0x80005e8c]:sw t5, 1624(ra)<br> [0x80005e90]:sw t6, 1632(ra)<br> [0x80005e94]:sw t5, 1640(ra)<br> [0x80005e98]:sw a6, 1648(ra)<br>                                 |
| 254|[0x80013518]<br>0x00000001<br> [0x80013530]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80005f08]:fmul.d t5, t3, s10, dyn<br> [0x80005f0c]:csrrs a6, fcsr, zero<br> [0x80005f10]:sw t5, 1656(ra)<br> [0x80005f14]:sw t6, 1664(ra)<br> [0x80005f18]:sw t5, 1672(ra)<br> [0x80005f1c]:sw a6, 1680(ra)<br>                                 |
| 255|[0x80013538]<br>0x00000000<br> [0x80013550]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80005f88]:fmul.d t5, t3, s10, dyn<br> [0x80005f8c]:csrrs a6, fcsr, zero<br> [0x80005f90]:sw t5, 1688(ra)<br> [0x80005f94]:sw t6, 1696(ra)<br> [0x80005f98]:sw t5, 1704(ra)<br> [0x80005f9c]:sw a6, 1712(ra)<br>                                 |
| 256|[0x80013558]<br>0x00000000<br> [0x80013570]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006008]:fmul.d t5, t3, s10, dyn<br> [0x8000600c]:csrrs a6, fcsr, zero<br> [0x80006010]:sw t5, 1720(ra)<br> [0x80006014]:sw t6, 1728(ra)<br> [0x80006018]:sw t5, 1736(ra)<br> [0x8000601c]:sw a6, 1744(ra)<br>                                 |
| 257|[0x80013578]<br>0x00000000<br> [0x80013590]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006088]:fmul.d t5, t3, s10, dyn<br> [0x8000608c]:csrrs a6, fcsr, zero<br> [0x80006090]:sw t5, 1752(ra)<br> [0x80006094]:sw t6, 1760(ra)<br> [0x80006098]:sw t5, 1768(ra)<br> [0x8000609c]:sw a6, 1776(ra)<br>                                 |
| 258|[0x80013598]<br>0x00000000<br> [0x800135b0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006108]:fmul.d t5, t3, s10, dyn<br> [0x8000610c]:csrrs a6, fcsr, zero<br> [0x80006110]:sw t5, 1784(ra)<br> [0x80006114]:sw t6, 1792(ra)<br> [0x80006118]:sw t5, 1800(ra)<br> [0x8000611c]:sw a6, 1808(ra)<br>                                 |
| 259|[0x800135b8]<br>0x00000000<br> [0x800135d0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006188]:fmul.d t5, t3, s10, dyn<br> [0x8000618c]:csrrs a6, fcsr, zero<br> [0x80006190]:sw t5, 1816(ra)<br> [0x80006194]:sw t6, 1824(ra)<br> [0x80006198]:sw t5, 1832(ra)<br> [0x8000619c]:sw a6, 1840(ra)<br>                                 |
| 260|[0x800135d8]<br>0x00000000<br> [0x800135f0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006208]:fmul.d t5, t3, s10, dyn<br> [0x8000620c]:csrrs a6, fcsr, zero<br> [0x80006210]:sw t5, 1848(ra)<br> [0x80006214]:sw t6, 1856(ra)<br> [0x80006218]:sw t5, 1864(ra)<br> [0x8000621c]:sw a6, 1872(ra)<br>                                 |
| 261|[0x800135f8]<br>0x00000000<br> [0x80013610]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006288]:fmul.d t5, t3, s10, dyn<br> [0x8000628c]:csrrs a6, fcsr, zero<br> [0x80006290]:sw t5, 1880(ra)<br> [0x80006294]:sw t6, 1888(ra)<br> [0x80006298]:sw t5, 1896(ra)<br> [0x8000629c]:sw a6, 1904(ra)<br>                                 |
| 262|[0x80013618]<br>0x00000000<br> [0x80013630]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006308]:fmul.d t5, t3, s10, dyn<br> [0x8000630c]:csrrs a6, fcsr, zero<br> [0x80006310]:sw t5, 1912(ra)<br> [0x80006314]:sw t6, 1920(ra)<br> [0x80006318]:sw t5, 1928(ra)<br> [0x8000631c]:sw a6, 1936(ra)<br>                                 |
| 263|[0x80013638]<br>0x00000002<br> [0x80013650]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006388]:fmul.d t5, t3, s10, dyn<br> [0x8000638c]:csrrs a6, fcsr, zero<br> [0x80006390]:sw t5, 1944(ra)<br> [0x80006394]:sw t6, 1952(ra)<br> [0x80006398]:sw t5, 1960(ra)<br> [0x8000639c]:sw a6, 1968(ra)<br>                                 |
| 264|[0x80013658]<br>0x00000000<br> [0x80013670]<br>0x00000003<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006408]:fmul.d t5, t3, s10, dyn<br> [0x8000640c]:csrrs a6, fcsr, zero<br> [0x80006410]:sw t5, 1976(ra)<br> [0x80006414]:sw t6, 1984(ra)<br> [0x80006418]:sw t5, 1992(ra)<br> [0x8000641c]:sw a6, 2000(ra)<br>                                 |
| 265|[0x80013678]<br>0x00000000<br> [0x80013690]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006488]:fmul.d t5, t3, s10, dyn<br> [0x8000648c]:csrrs a6, fcsr, zero<br> [0x80006490]:sw t5, 2008(ra)<br> [0x80006494]:sw t6, 2016(ra)<br> [0x80006498]:sw t5, 2024(ra)<br> [0x8000649c]:sw a6, 2032(ra)<br>                                 |
| 266|[0x80013698]<br>0x00000000<br> [0x800136b0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006508]:fmul.d t5, t3, s10, dyn<br> [0x8000650c]:csrrs a6, fcsr, zero<br> [0x80006510]:sw t5, 2040(ra)<br> [0x80006514]:addi ra, ra, 2040<br> [0x80006518]:sw t6, 8(ra)<br> [0x8000651c]:sw t5, 16(ra)<br> [0x80006520]:sw a6, 24(ra)<br>     |
| 267|[0x800126b8]<br>0x00000000<br> [0x800126d0]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006554]:fmul.d t5, t3, s10, dyn<br> [0x80006558]:csrrs a6, fcsr, zero<br> [0x8000655c]:sw t5, 0(ra)<br> [0x80006560]:sw t6, 8(ra)<br> [0x80006564]:sw t5, 16(ra)<br> [0x80006568]:sw a6, 24(ra)<br>                                           |
| 268|[0x800126d8]<br>0x00000000<br> [0x800126f0]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006594]:fmul.d t5, t3, s10, dyn<br> [0x80006598]:csrrs a6, fcsr, zero<br> [0x8000659c]:sw t5, 32(ra)<br> [0x800065a0]:sw t6, 40(ra)<br> [0x800065a4]:sw t5, 48(ra)<br> [0x800065a8]:sw a6, 56(ra)<br>                                         |
| 269|[0x800126f8]<br>0x00000000<br> [0x80012710]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800065d4]:fmul.d t5, t3, s10, dyn<br> [0x800065d8]:csrrs a6, fcsr, zero<br> [0x800065dc]:sw t5, 64(ra)<br> [0x800065e0]:sw t6, 72(ra)<br> [0x800065e4]:sw t5, 80(ra)<br> [0x800065e8]:sw a6, 88(ra)<br>                                         |
| 270|[0x80012718]<br>0x00000000<br> [0x80012730]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006614]:fmul.d t5, t3, s10, dyn<br> [0x80006618]:csrrs a6, fcsr, zero<br> [0x8000661c]:sw t5, 96(ra)<br> [0x80006620]:sw t6, 104(ra)<br> [0x80006624]:sw t5, 112(ra)<br> [0x80006628]:sw a6, 120(ra)<br>                                      |
| 271|[0x80012738]<br>0x00000000<br> [0x80012750]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006658]:fmul.d t5, t3, s10, dyn<br> [0x8000665c]:csrrs a6, fcsr, zero<br> [0x80006660]:sw t5, 128(ra)<br> [0x80006664]:sw t6, 136(ra)<br> [0x80006668]:sw t5, 144(ra)<br> [0x8000666c]:sw a6, 152(ra)<br>                                     |
| 272|[0x80012758]<br>0x00000000<br> [0x80012770]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000669c]:fmul.d t5, t3, s10, dyn<br> [0x800066a0]:csrrs a6, fcsr, zero<br> [0x800066a4]:sw t5, 160(ra)<br> [0x800066a8]:sw t6, 168(ra)<br> [0x800066ac]:sw t5, 176(ra)<br> [0x800066b0]:sw a6, 184(ra)<br>                                     |
| 273|[0x80012778]<br>0x00000000<br> [0x80012790]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800066dc]:fmul.d t5, t3, s10, dyn<br> [0x800066e0]:csrrs a6, fcsr, zero<br> [0x800066e4]:sw t5, 192(ra)<br> [0x800066e8]:sw t6, 200(ra)<br> [0x800066ec]:sw t5, 208(ra)<br> [0x800066f0]:sw a6, 216(ra)<br>                                     |
| 274|[0x80012798]<br>0x00000000<br> [0x800127b0]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000671c]:fmul.d t5, t3, s10, dyn<br> [0x80006720]:csrrs a6, fcsr, zero<br> [0x80006724]:sw t5, 224(ra)<br> [0x80006728]:sw t6, 232(ra)<br> [0x8000672c]:sw t5, 240(ra)<br> [0x80006730]:sw a6, 248(ra)<br>                                     |
| 275|[0x800127b8]<br>0x00000000<br> [0x800127d0]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000675c]:fmul.d t5, t3, s10, dyn<br> [0x80006760]:csrrs a6, fcsr, zero<br> [0x80006764]:sw t5, 256(ra)<br> [0x80006768]:sw t6, 264(ra)<br> [0x8000676c]:sw t5, 272(ra)<br> [0x80006770]:sw a6, 280(ra)<br>                                     |
| 276|[0x800127d8]<br>0x00000000<br> [0x800127f0]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000679c]:fmul.d t5, t3, s10, dyn<br> [0x800067a0]:csrrs a6, fcsr, zero<br> [0x800067a4]:sw t5, 288(ra)<br> [0x800067a8]:sw t6, 296(ra)<br> [0x800067ac]:sw t5, 304(ra)<br> [0x800067b0]:sw a6, 312(ra)<br>                                     |
| 277|[0x800127f8]<br>0x00000001<br> [0x80012810]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800067e0]:fmul.d t5, t3, s10, dyn<br> [0x800067e4]:csrrs a6, fcsr, zero<br> [0x800067e8]:sw t5, 320(ra)<br> [0x800067ec]:sw t6, 328(ra)<br> [0x800067f0]:sw t5, 336(ra)<br> [0x800067f4]:sw a6, 344(ra)<br>                                     |
| 278|[0x80012818]<br>0x00000001<br> [0x80012830]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006824]:fmul.d t5, t3, s10, dyn<br> [0x80006828]:csrrs a6, fcsr, zero<br> [0x8000682c]:sw t5, 352(ra)<br> [0x80006830]:sw t6, 360(ra)<br> [0x80006834]:sw t5, 368(ra)<br> [0x80006838]:sw a6, 376(ra)<br>                                     |
| 279|[0x80012838]<br>0x00000000<br> [0x80012850]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006864]:fmul.d t5, t3, s10, dyn<br> [0x80006868]:csrrs a6, fcsr, zero<br> [0x8000686c]:sw t5, 384(ra)<br> [0x80006870]:sw t6, 392(ra)<br> [0x80006874]:sw t5, 400(ra)<br> [0x80006878]:sw a6, 408(ra)<br>                                     |
| 280|[0x80012858]<br>0x00000000<br> [0x80012870]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800068a4]:fmul.d t5, t3, s10, dyn<br> [0x800068a8]:csrrs a6, fcsr, zero<br> [0x800068ac]:sw t5, 416(ra)<br> [0x800068b0]:sw t6, 424(ra)<br> [0x800068b4]:sw t5, 432(ra)<br> [0x800068b8]:sw a6, 440(ra)<br>                                     |
| 281|[0x80012878]<br>0x00000000<br> [0x80012890]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800068e4]:fmul.d t5, t3, s10, dyn<br> [0x800068e8]:csrrs a6, fcsr, zero<br> [0x800068ec]:sw t5, 448(ra)<br> [0x800068f0]:sw t6, 456(ra)<br> [0x800068f4]:sw t5, 464(ra)<br> [0x800068f8]:sw a6, 472(ra)<br>                                     |
| 282|[0x80012898]<br>0x00000000<br> [0x800128b0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006924]:fmul.d t5, t3, s10, dyn<br> [0x80006928]:csrrs a6, fcsr, zero<br> [0x8000692c]:sw t5, 480(ra)<br> [0x80006930]:sw t6, 488(ra)<br> [0x80006934]:sw t5, 496(ra)<br> [0x80006938]:sw a6, 504(ra)<br>                                     |
| 283|[0x800128b8]<br>0x00000000<br> [0x800128d0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006964]:fmul.d t5, t3, s10, dyn<br> [0x80006968]:csrrs a6, fcsr, zero<br> [0x8000696c]:sw t5, 512(ra)<br> [0x80006970]:sw t6, 520(ra)<br> [0x80006974]:sw t5, 528(ra)<br> [0x80006978]:sw a6, 536(ra)<br>                                     |
| 284|[0x800128d8]<br>0x00000000<br> [0x800128f0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800069a4]:fmul.d t5, t3, s10, dyn<br> [0x800069a8]:csrrs a6, fcsr, zero<br> [0x800069ac]:sw t5, 544(ra)<br> [0x800069b0]:sw t6, 552(ra)<br> [0x800069b4]:sw t5, 560(ra)<br> [0x800069b8]:sw a6, 568(ra)<br>                                     |
| 285|[0x800128f8]<br>0x00000000<br> [0x80012910]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800069e4]:fmul.d t5, t3, s10, dyn<br> [0x800069e8]:csrrs a6, fcsr, zero<br> [0x800069ec]:sw t5, 576(ra)<br> [0x800069f0]:sw t6, 584(ra)<br> [0x800069f4]:sw t5, 592(ra)<br> [0x800069f8]:sw a6, 600(ra)<br>                                     |
| 286|[0x80012918]<br>0x00000000<br> [0x80012930]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006a24]:fmul.d t5, t3, s10, dyn<br> [0x80006a28]:csrrs a6, fcsr, zero<br> [0x80006a2c]:sw t5, 608(ra)<br> [0x80006a30]:sw t6, 616(ra)<br> [0x80006a34]:sw t5, 624(ra)<br> [0x80006a38]:sw a6, 632(ra)<br>                                     |
| 287|[0x80012938]<br>0x00000002<br> [0x80012950]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006a64]:fmul.d t5, t3, s10, dyn<br> [0x80006a68]:csrrs a6, fcsr, zero<br> [0x80006a6c]:sw t5, 640(ra)<br> [0x80006a70]:sw t6, 648(ra)<br> [0x80006a74]:sw t5, 656(ra)<br> [0x80006a78]:sw a6, 664(ra)<br>                                     |
| 288|[0x80012958]<br>0x00000000<br> [0x80012970]<br>0x00000003<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006aa4]:fmul.d t5, t3, s10, dyn<br> [0x80006aa8]:csrrs a6, fcsr, zero<br> [0x80006aac]:sw t5, 672(ra)<br> [0x80006ab0]:sw t6, 680(ra)<br> [0x80006ab4]:sw t5, 688(ra)<br> [0x80006ab8]:sw a6, 696(ra)<br>                                     |
| 289|[0x80012978]<br>0x00000000<br> [0x80012990]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006ae8]:fmul.d t5, t3, s10, dyn<br> [0x80006aec]:csrrs a6, fcsr, zero<br> [0x80006af0]:sw t5, 704(ra)<br> [0x80006af4]:sw t6, 712(ra)<br> [0x80006af8]:sw t5, 720(ra)<br> [0x80006afc]:sw a6, 728(ra)<br>                                     |
| 290|[0x80012998]<br>0x00000000<br> [0x800129b0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006b2c]:fmul.d t5, t3, s10, dyn<br> [0x80006b30]:csrrs a6, fcsr, zero<br> [0x80006b34]:sw t5, 736(ra)<br> [0x80006b38]:sw t6, 744(ra)<br> [0x80006b3c]:sw t5, 752(ra)<br> [0x80006b40]:sw a6, 760(ra)<br>                                     |
| 291|[0x800129b8]<br>0xFFFFFFFF<br> [0x800129d0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006b70]:fmul.d t5, t3, s10, dyn<br> [0x80006b74]:csrrs a6, fcsr, zero<br> [0x80006b78]:sw t5, 768(ra)<br> [0x80006b7c]:sw t6, 776(ra)<br> [0x80006b80]:sw t5, 784(ra)<br> [0x80006b84]:sw a6, 792(ra)<br>                                     |
| 292|[0x800129d8]<br>0xFFFFFFFF<br> [0x800129f0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006bb4]:fmul.d t5, t3, s10, dyn<br> [0x80006bb8]:csrrs a6, fcsr, zero<br> [0x80006bbc]:sw t5, 800(ra)<br> [0x80006bc0]:sw t6, 808(ra)<br> [0x80006bc4]:sw t5, 816(ra)<br> [0x80006bc8]:sw a6, 824(ra)<br>                                     |
| 293|[0x800129f8]<br>0xFFFFFFFF<br> [0x80012a10]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006bf8]:fmul.d t5, t3, s10, dyn<br> [0x80006bfc]:csrrs a6, fcsr, zero<br> [0x80006c00]:sw t5, 832(ra)<br> [0x80006c04]:sw t6, 840(ra)<br> [0x80006c08]:sw t5, 848(ra)<br> [0x80006c0c]:sw a6, 856(ra)<br>                                     |
| 294|[0x80012a18]<br>0xFFFFFFFF<br> [0x80012a30]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006c3c]:fmul.d t5, t3, s10, dyn<br> [0x80006c40]:csrrs a6, fcsr, zero<br> [0x80006c44]:sw t5, 864(ra)<br> [0x80006c48]:sw t6, 872(ra)<br> [0x80006c4c]:sw t5, 880(ra)<br> [0x80006c50]:sw a6, 888(ra)<br>                                     |
| 295|[0x80012a38]<br>0xFFFFFFFD<br> [0x80012a50]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006c84]:fmul.d t5, t3, s10, dyn<br> [0x80006c88]:csrrs a6, fcsr, zero<br> [0x80006c8c]:sw t5, 896(ra)<br> [0x80006c90]:sw t6, 904(ra)<br> [0x80006c94]:sw t5, 912(ra)<br> [0x80006c98]:sw a6, 920(ra)<br>                                     |
| 296|[0x80012a58]<br>0xFFFFFFFD<br> [0x80012a70]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006ccc]:fmul.d t5, t3, s10, dyn<br> [0x80006cd0]:csrrs a6, fcsr, zero<br> [0x80006cd4]:sw t5, 928(ra)<br> [0x80006cd8]:sw t6, 936(ra)<br> [0x80006cdc]:sw t5, 944(ra)<br> [0x80006ce0]:sw a6, 952(ra)<br>                                     |
| 297|[0x80012a78]<br>0xFFFFFFFF<br> [0x80012a90]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006d10]:fmul.d t5, t3, s10, dyn<br> [0x80006d14]:csrrs a6, fcsr, zero<br> [0x80006d18]:sw t5, 960(ra)<br> [0x80006d1c]:sw t6, 968(ra)<br> [0x80006d20]:sw t5, 976(ra)<br> [0x80006d24]:sw a6, 984(ra)<br>                                     |
| 298|[0x80012a98]<br>0xFFFFFFFF<br> [0x80012ab0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006d54]:fmul.d t5, t3, s10, dyn<br> [0x80006d58]:csrrs a6, fcsr, zero<br> [0x80006d5c]:sw t5, 992(ra)<br> [0x80006d60]:sw t6, 1000(ra)<br> [0x80006d64]:sw t5, 1008(ra)<br> [0x80006d68]:sw a6, 1016(ra)<br>                                  |
| 299|[0x80012ab8]<br>0x00000001<br> [0x80012ad0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006d98]:fmul.d t5, t3, s10, dyn<br> [0x80006d9c]:csrrs a6, fcsr, zero<br> [0x80006da0]:sw t5, 1024(ra)<br> [0x80006da4]:sw t6, 1032(ra)<br> [0x80006da8]:sw t5, 1040(ra)<br> [0x80006dac]:sw a6, 1048(ra)<br>                                 |
| 300|[0x80012ad8]<br>0x00000001<br> [0x80012af0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006ddc]:fmul.d t5, t3, s10, dyn<br> [0x80006de0]:csrrs a6, fcsr, zero<br> [0x80006de4]:sw t5, 1056(ra)<br> [0x80006de8]:sw t6, 1064(ra)<br> [0x80006dec]:sw t5, 1072(ra)<br> [0x80006df0]:sw a6, 1080(ra)<br>                                 |
| 301|[0x80012af8]<br>0x00000000<br> [0x80012b10]<br>0x00000005<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006e24]:fmul.d t5, t3, s10, dyn<br> [0x80006e28]:csrrs a6, fcsr, zero<br> [0x80006e2c]:sw t5, 1088(ra)<br> [0x80006e30]:sw t6, 1096(ra)<br> [0x80006e34]:sw t5, 1104(ra)<br> [0x80006e38]:sw a6, 1112(ra)<br>                                 |
| 302|[0x80012b18]<br>0x00000000<br> [0x80012b30]<br>0x00000005<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006e6c]:fmul.d t5, t3, s10, dyn<br> [0x80006e70]:csrrs a6, fcsr, zero<br> [0x80006e74]:sw t5, 1120(ra)<br> [0x80006e78]:sw t6, 1128(ra)<br> [0x80006e7c]:sw t5, 1136(ra)<br> [0x80006e80]:sw a6, 1144(ra)<br>                                 |
| 303|[0x80012b38]<br>0x00000000<br> [0x80012b50]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006eb0]:fmul.d t5, t3, s10, dyn<br> [0x80006eb4]:csrrs a6, fcsr, zero<br> [0x80006eb8]:sw t5, 1152(ra)<br> [0x80006ebc]:sw t6, 1160(ra)<br> [0x80006ec0]:sw t5, 1168(ra)<br> [0x80006ec4]:sw a6, 1176(ra)<br>                                 |
| 304|[0x80012b58]<br>0x00000000<br> [0x80012b70]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006ef4]:fmul.d t5, t3, s10, dyn<br> [0x80006ef8]:csrrs a6, fcsr, zero<br> [0x80006efc]:sw t5, 1184(ra)<br> [0x80006f00]:sw t6, 1192(ra)<br> [0x80006f04]:sw t5, 1200(ra)<br> [0x80006f08]:sw a6, 1208(ra)<br>                                 |
| 305|[0x80012b78]<br>0x00000000<br> [0x80012b90]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006f38]:fmul.d t5, t3, s10, dyn<br> [0x80006f3c]:csrrs a6, fcsr, zero<br> [0x80006f40]:sw t5, 1216(ra)<br> [0x80006f44]:sw t6, 1224(ra)<br> [0x80006f48]:sw t5, 1232(ra)<br> [0x80006f4c]:sw a6, 1240(ra)<br>                                 |
| 306|[0x80012b98]<br>0x00000000<br> [0x80012bb0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006f7c]:fmul.d t5, t3, s10, dyn<br> [0x80006f80]:csrrs a6, fcsr, zero<br> [0x80006f84]:sw t5, 1248(ra)<br> [0x80006f88]:sw t6, 1256(ra)<br> [0x80006f8c]:sw t5, 1264(ra)<br> [0x80006f90]:sw a6, 1272(ra)<br>                                 |
| 307|[0x80012bb8]<br>0x00000000<br> [0x80012bd0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80006fc0]:fmul.d t5, t3, s10, dyn<br> [0x80006fc4]:csrrs a6, fcsr, zero<br> [0x80006fc8]:sw t5, 1280(ra)<br> [0x80006fcc]:sw t6, 1288(ra)<br> [0x80006fd0]:sw t5, 1296(ra)<br> [0x80006fd4]:sw a6, 1304(ra)<br>                                 |
| 308|[0x80012bd8]<br>0x00000000<br> [0x80012bf0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007004]:fmul.d t5, t3, s10, dyn<br> [0x80007008]:csrrs a6, fcsr, zero<br> [0x8000700c]:sw t5, 1312(ra)<br> [0x80007010]:sw t6, 1320(ra)<br> [0x80007014]:sw t5, 1328(ra)<br> [0x80007018]:sw a6, 1336(ra)<br>                                 |
| 309|[0x80012bf8]<br>0x00000000<br> [0x80012c10]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007048]:fmul.d t5, t3, s10, dyn<br> [0x8000704c]:csrrs a6, fcsr, zero<br> [0x80007050]:sw t5, 1344(ra)<br> [0x80007054]:sw t6, 1352(ra)<br> [0x80007058]:sw t5, 1360(ra)<br> [0x8000705c]:sw a6, 1368(ra)<br>                                 |
| 310|[0x80012c18]<br>0x00000000<br> [0x80012c30]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000708c]:fmul.d t5, t3, s10, dyn<br> [0x80007090]:csrrs a6, fcsr, zero<br> [0x80007094]:sw t5, 1376(ra)<br> [0x80007098]:sw t6, 1384(ra)<br> [0x8000709c]:sw t5, 1392(ra)<br> [0x800070a0]:sw a6, 1400(ra)<br>                                 |
| 311|[0x80012c38]<br>0xFFFFFFFF<br> [0x80012c50]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800070d0]:fmul.d t5, t3, s10, dyn<br> [0x800070d4]:csrrs a6, fcsr, zero<br> [0x800070d8]:sw t5, 1408(ra)<br> [0x800070dc]:sw t6, 1416(ra)<br> [0x800070e0]:sw t5, 1424(ra)<br> [0x800070e4]:sw a6, 1432(ra)<br>                                 |
| 312|[0x80012c58]<br>0xFFFFFFFF<br> [0x80012c70]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007114]:fmul.d t5, t3, s10, dyn<br> [0x80007118]:csrrs a6, fcsr, zero<br> [0x8000711c]:sw t5, 1440(ra)<br> [0x80007120]:sw t6, 1448(ra)<br> [0x80007124]:sw t5, 1456(ra)<br> [0x80007128]:sw a6, 1464(ra)<br>                                 |
| 313|[0x80012c78]<br>0x00000000<br> [0x80012c90]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007158]:fmul.d t5, t3, s10, dyn<br> [0x8000715c]:csrrs a6, fcsr, zero<br> [0x80007160]:sw t5, 1472(ra)<br> [0x80007164]:sw t6, 1480(ra)<br> [0x80007168]:sw t5, 1488(ra)<br> [0x8000716c]:sw a6, 1496(ra)<br>                                 |
| 314|[0x80012c98]<br>0x00000000<br> [0x80012cb0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000719c]:fmul.d t5, t3, s10, dyn<br> [0x800071a0]:csrrs a6, fcsr, zero<br> [0x800071a4]:sw t5, 1504(ra)<br> [0x800071a8]:sw t6, 1512(ra)<br> [0x800071ac]:sw t5, 1520(ra)<br> [0x800071b0]:sw a6, 1528(ra)<br>                                 |
| 315|[0x80012cb8]<br>0xFFFFFFFF<br> [0x80012cd0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800071e0]:fmul.d t5, t3, s10, dyn<br> [0x800071e4]:csrrs a6, fcsr, zero<br> [0x800071e8]:sw t5, 1536(ra)<br> [0x800071ec]:sw t6, 1544(ra)<br> [0x800071f0]:sw t5, 1552(ra)<br> [0x800071f4]:sw a6, 1560(ra)<br>                                 |
| 316|[0x80012cd8]<br>0xFFFFFFFF<br> [0x80012cf0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007224]:fmul.d t5, t3, s10, dyn<br> [0x80007228]:csrrs a6, fcsr, zero<br> [0x8000722c]:sw t5, 1568(ra)<br> [0x80007230]:sw t6, 1576(ra)<br> [0x80007234]:sw t5, 1584(ra)<br> [0x80007238]:sw a6, 1592(ra)<br>                                 |
| 317|[0x80012cf8]<br>0xFFFFFFFF<br> [0x80012d10]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007268]:fmul.d t5, t3, s10, dyn<br> [0x8000726c]:csrrs a6, fcsr, zero<br> [0x80007270]:sw t5, 1600(ra)<br> [0x80007274]:sw t6, 1608(ra)<br> [0x80007278]:sw t5, 1616(ra)<br> [0x8000727c]:sw a6, 1624(ra)<br>                                 |
| 318|[0x80012d18]<br>0xFFFFFFFF<br> [0x80012d30]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800072ac]:fmul.d t5, t3, s10, dyn<br> [0x800072b0]:csrrs a6, fcsr, zero<br> [0x800072b4]:sw t5, 1632(ra)<br> [0x800072b8]:sw t6, 1640(ra)<br> [0x800072bc]:sw t5, 1648(ra)<br> [0x800072c0]:sw a6, 1656(ra)<br>                                 |
| 319|[0x80012d38]<br>0xFFFFFFFD<br> [0x80012d50]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800072f4]:fmul.d t5, t3, s10, dyn<br> [0x800072f8]:csrrs a6, fcsr, zero<br> [0x800072fc]:sw t5, 1664(ra)<br> [0x80007300]:sw t6, 1672(ra)<br> [0x80007304]:sw t5, 1680(ra)<br> [0x80007308]:sw a6, 1688(ra)<br>                                 |
| 320|[0x80012d58]<br>0xFFFFFFFD<br> [0x80012d70]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000733c]:fmul.d t5, t3, s10, dyn<br> [0x80007340]:csrrs a6, fcsr, zero<br> [0x80007344]:sw t5, 1696(ra)<br> [0x80007348]:sw t6, 1704(ra)<br> [0x8000734c]:sw t5, 1712(ra)<br> [0x80007350]:sw a6, 1720(ra)<br>                                 |
| 321|[0x80012d78]<br>0xFFFFFFFF<br> [0x80012d90]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007380]:fmul.d t5, t3, s10, dyn<br> [0x80007384]:csrrs a6, fcsr, zero<br> [0x80007388]:sw t5, 1728(ra)<br> [0x8000738c]:sw t6, 1736(ra)<br> [0x80007390]:sw t5, 1744(ra)<br> [0x80007394]:sw a6, 1752(ra)<br>                                 |
| 322|[0x80012d98]<br>0xFFFFFFFF<br> [0x80012db0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800073c4]:fmul.d t5, t3, s10, dyn<br> [0x800073c8]:csrrs a6, fcsr, zero<br> [0x800073cc]:sw t5, 1760(ra)<br> [0x800073d0]:sw t6, 1768(ra)<br> [0x800073d4]:sw t5, 1776(ra)<br> [0x800073d8]:sw a6, 1784(ra)<br>                                 |
| 323|[0x80012db8]<br>0x00000001<br> [0x80012dd0]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007408]:fmul.d t5, t3, s10, dyn<br> [0x8000740c]:csrrs a6, fcsr, zero<br> [0x80007410]:sw t5, 1792(ra)<br> [0x80007414]:sw t6, 1800(ra)<br> [0x80007418]:sw t5, 1808(ra)<br> [0x8000741c]:sw a6, 1816(ra)<br>                                 |
| 324|[0x80012dd8]<br>0x00000001<br> [0x80012df0]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000744c]:fmul.d t5, t3, s10, dyn<br> [0x80007450]:csrrs a6, fcsr, zero<br> [0x80007454]:sw t5, 1824(ra)<br> [0x80007458]:sw t6, 1832(ra)<br> [0x8000745c]:sw t5, 1840(ra)<br> [0x80007460]:sw a6, 1848(ra)<br>                                 |
| 325|[0x80012df8]<br>0x00000000<br> [0x80012e10]<br>0x00000005<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007494]:fmul.d t5, t3, s10, dyn<br> [0x80007498]:csrrs a6, fcsr, zero<br> [0x8000749c]:sw t5, 1856(ra)<br> [0x800074a0]:sw t6, 1864(ra)<br> [0x800074a4]:sw t5, 1872(ra)<br> [0x800074a8]:sw a6, 1880(ra)<br>                                 |
| 326|[0x80012e18]<br>0x00000000<br> [0x80012e30]<br>0x00000005<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800074dc]:fmul.d t5, t3, s10, dyn<br> [0x800074e0]:csrrs a6, fcsr, zero<br> [0x800074e4]:sw t5, 1888(ra)<br> [0x800074e8]:sw t6, 1896(ra)<br> [0x800074ec]:sw t5, 1904(ra)<br> [0x800074f0]:sw a6, 1912(ra)<br>                                 |
| 327|[0x80012e38]<br>0x00000000<br> [0x80012e50]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007520]:fmul.d t5, t3, s10, dyn<br> [0x80007524]:csrrs a6, fcsr, zero<br> [0x80007528]:sw t5, 1920(ra)<br> [0x8000752c]:sw t6, 1928(ra)<br> [0x80007530]:sw t5, 1936(ra)<br> [0x80007534]:sw a6, 1944(ra)<br>                                 |
| 328|[0x80012e58]<br>0x00000000<br> [0x80012e70]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007564]:fmul.d t5, t3, s10, dyn<br> [0x80007568]:csrrs a6, fcsr, zero<br> [0x8000756c]:sw t5, 1952(ra)<br> [0x80007570]:sw t6, 1960(ra)<br> [0x80007574]:sw t5, 1968(ra)<br> [0x80007578]:sw a6, 1976(ra)<br>                                 |
| 329|[0x80012e78]<br>0x00000000<br> [0x80012e90]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800075a8]:fmul.d t5, t3, s10, dyn<br> [0x800075ac]:csrrs a6, fcsr, zero<br> [0x800075b0]:sw t5, 1984(ra)<br> [0x800075b4]:sw t6, 1992(ra)<br> [0x800075b8]:sw t5, 2000(ra)<br> [0x800075bc]:sw a6, 2008(ra)<br>                                 |
| 330|[0x80012e98]<br>0x00000000<br> [0x80012eb0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800075ec]:fmul.d t5, t3, s10, dyn<br> [0x800075f0]:csrrs a6, fcsr, zero<br> [0x800075f4]:sw t5, 2016(ra)<br> [0x800075f8]:sw t6, 2024(ra)<br> [0x800075fc]:sw t5, 2032(ra)<br> [0x80007600]:sw a6, 2040(ra)<br>                                 |
| 331|[0x80012eb8]<br>0x00000000<br> [0x80012ed0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007630]:fmul.d t5, t3, s10, dyn<br> [0x80007634]:csrrs a6, fcsr, zero<br> [0x80007638]:addi ra, ra, 2040<br> [0x8000763c]:sw t5, 8(ra)<br> [0x80007640]:sw t6, 16(ra)<br> [0x80007644]:sw t5, 24(ra)<br> [0x80007648]:sw a6, 32(ra)<br>       |
| 332|[0x80012ed8]<br>0x00000000<br> [0x80012ef0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007678]:fmul.d t5, t3, s10, dyn<br> [0x8000767c]:csrrs a6, fcsr, zero<br> [0x80007680]:sw t5, 40(ra)<br> [0x80007684]:sw t6, 48(ra)<br> [0x80007688]:sw t5, 56(ra)<br> [0x8000768c]:sw a6, 64(ra)<br>                                         |
| 333|[0x80012ef8]<br>0x00000000<br> [0x80012f10]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800076bc]:fmul.d t5, t3, s10, dyn<br> [0x800076c0]:csrrs a6, fcsr, zero<br> [0x800076c4]:sw t5, 72(ra)<br> [0x800076c8]:sw t6, 80(ra)<br> [0x800076cc]:sw t5, 88(ra)<br> [0x800076d0]:sw a6, 96(ra)<br>                                         |
| 334|[0x80012f18]<br>0x00000000<br> [0x80012f30]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007700]:fmul.d t5, t3, s10, dyn<br> [0x80007704]:csrrs a6, fcsr, zero<br> [0x80007708]:sw t5, 104(ra)<br> [0x8000770c]:sw t6, 112(ra)<br> [0x80007710]:sw t5, 120(ra)<br> [0x80007714]:sw a6, 128(ra)<br>                                     |
| 335|[0x80012f38]<br>0xFFFFFFFF<br> [0x80012f50]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007744]:fmul.d t5, t3, s10, dyn<br> [0x80007748]:csrrs a6, fcsr, zero<br> [0x8000774c]:sw t5, 136(ra)<br> [0x80007750]:sw t6, 144(ra)<br> [0x80007754]:sw t5, 152(ra)<br> [0x80007758]:sw a6, 160(ra)<br>                                     |
| 336|[0x80012f58]<br>0xFFFFFFFF<br> [0x80012f70]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007788]:fmul.d t5, t3, s10, dyn<br> [0x8000778c]:csrrs a6, fcsr, zero<br> [0x80007790]:sw t5, 168(ra)<br> [0x80007794]:sw t6, 176(ra)<br> [0x80007798]:sw t5, 184(ra)<br> [0x8000779c]:sw a6, 192(ra)<br>                                     |
| 337|[0x80012f78]<br>0x00000000<br> [0x80012f90]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800077c8]:fmul.d t5, t3, s10, dyn<br> [0x800077cc]:csrrs a6, fcsr, zero<br> [0x800077d0]:sw t5, 200(ra)<br> [0x800077d4]:sw t6, 208(ra)<br> [0x800077d8]:sw t5, 216(ra)<br> [0x800077dc]:sw a6, 224(ra)<br>                                     |
| 338|[0x80012f98]<br>0x00000000<br> [0x80012fb0]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007808]:fmul.d t5, t3, s10, dyn<br> [0x8000780c]:csrrs a6, fcsr, zero<br> [0x80007810]:sw t5, 232(ra)<br> [0x80007814]:sw t6, 240(ra)<br> [0x80007818]:sw t5, 248(ra)<br> [0x8000781c]:sw a6, 256(ra)<br>                                     |
| 339|[0x80012fb8]<br>0x00000000<br> [0x80012fd0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007848]:fmul.d t5, t3, s10, dyn<br> [0x8000784c]:csrrs a6, fcsr, zero<br> [0x80007850]:sw t5, 264(ra)<br> [0x80007854]:sw t6, 272(ra)<br> [0x80007858]:sw t5, 280(ra)<br> [0x8000785c]:sw a6, 288(ra)<br>                                     |
| 340|[0x80012fd8]<br>0x00000000<br> [0x80012ff0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007888]:fmul.d t5, t3, s10, dyn<br> [0x8000788c]:csrrs a6, fcsr, zero<br> [0x80007890]:sw t5, 296(ra)<br> [0x80007894]:sw t6, 304(ra)<br> [0x80007898]:sw t5, 312(ra)<br> [0x8000789c]:sw a6, 320(ra)<br>                                     |
| 341|[0x80012ff8]<br>0x00000000<br> [0x80013010]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800078c8]:fmul.d t5, t3, s10, dyn<br> [0x800078cc]:csrrs a6, fcsr, zero<br> [0x800078d0]:sw t5, 328(ra)<br> [0x800078d4]:sw t6, 336(ra)<br> [0x800078d8]:sw t5, 344(ra)<br> [0x800078dc]:sw a6, 352(ra)<br>                                     |
| 342|[0x80013018]<br>0x00000000<br> [0x80013030]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007908]:fmul.d t5, t3, s10, dyn<br> [0x8000790c]:csrrs a6, fcsr, zero<br> [0x80007910]:sw t5, 360(ra)<br> [0x80007914]:sw t6, 368(ra)<br> [0x80007918]:sw t5, 376(ra)<br> [0x8000791c]:sw a6, 384(ra)<br>                                     |
| 343|[0x80013038]<br>0x00000000<br> [0x80013050]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000794c]:fmul.d t5, t3, s10, dyn<br> [0x80007950]:csrrs a6, fcsr, zero<br> [0x80007954]:sw t5, 392(ra)<br> [0x80007958]:sw t6, 400(ra)<br> [0x8000795c]:sw t5, 408(ra)<br> [0x80007960]:sw a6, 416(ra)<br>                                     |
| 344|[0x80013058]<br>0x00000000<br> [0x80013070]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007990]:fmul.d t5, t3, s10, dyn<br> [0x80007994]:csrrs a6, fcsr, zero<br> [0x80007998]:sw t5, 424(ra)<br> [0x8000799c]:sw t6, 432(ra)<br> [0x800079a0]:sw t5, 440(ra)<br> [0x800079a4]:sw a6, 448(ra)<br>                                     |
| 345|[0x80013078]<br>0x00000000<br> [0x80013090]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800079d0]:fmul.d t5, t3, s10, dyn<br> [0x800079d4]:csrrs a6, fcsr, zero<br> [0x800079d8]:sw t5, 456(ra)<br> [0x800079dc]:sw t6, 464(ra)<br> [0x800079e0]:sw t5, 472(ra)<br> [0x800079e4]:sw a6, 480(ra)<br>                                     |
| 346|[0x80013098]<br>0x00000000<br> [0x800130b0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007a10]:fmul.d t5, t3, s10, dyn<br> [0x80007a14]:csrrs a6, fcsr, zero<br> [0x80007a18]:sw t5, 488(ra)<br> [0x80007a1c]:sw t6, 496(ra)<br> [0x80007a20]:sw t5, 504(ra)<br> [0x80007a24]:sw a6, 512(ra)<br>                                     |
| 347|[0x800130b8]<br>0x00000000<br> [0x800130d0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007a50]:fmul.d t5, t3, s10, dyn<br> [0x80007a54]:csrrs a6, fcsr, zero<br> [0x80007a58]:sw t5, 520(ra)<br> [0x80007a5c]:sw t6, 528(ra)<br> [0x80007a60]:sw t5, 536(ra)<br> [0x80007a64]:sw a6, 544(ra)<br>                                     |
| 348|[0x800130d8]<br>0x00000000<br> [0x800130f0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007a90]:fmul.d t5, t3, s10, dyn<br> [0x80007a94]:csrrs a6, fcsr, zero<br> [0x80007a98]:sw t5, 552(ra)<br> [0x80007a9c]:sw t6, 560(ra)<br> [0x80007aa0]:sw t5, 568(ra)<br> [0x80007aa4]:sw a6, 576(ra)<br>                                     |
| 349|[0x800130f8]<br>0x00000000<br> [0x80013110]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007ad4]:fmul.d t5, t3, s10, dyn<br> [0x80007ad8]:csrrs a6, fcsr, zero<br> [0x80007adc]:sw t5, 584(ra)<br> [0x80007ae0]:sw t6, 592(ra)<br> [0x80007ae4]:sw t5, 600(ra)<br> [0x80007ae8]:sw a6, 608(ra)<br>                                     |
| 350|[0x80013118]<br>0x00000000<br> [0x80013130]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007b18]:fmul.d t5, t3, s10, dyn<br> [0x80007b1c]:csrrs a6, fcsr, zero<br> [0x80007b20]:sw t5, 616(ra)<br> [0x80007b24]:sw t6, 624(ra)<br> [0x80007b28]:sw t5, 632(ra)<br> [0x80007b2c]:sw a6, 640(ra)<br>                                     |
| 351|[0x80013138]<br>0x00000000<br> [0x80013150]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007b58]:fmul.d t5, t3, s10, dyn<br> [0x80007b5c]:csrrs a6, fcsr, zero<br> [0x80007b60]:sw t5, 648(ra)<br> [0x80007b64]:sw t6, 656(ra)<br> [0x80007b68]:sw t5, 664(ra)<br> [0x80007b6c]:sw a6, 672(ra)<br>                                     |
| 352|[0x80013158]<br>0x00000000<br> [0x80013170]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007b98]:fmul.d t5, t3, s10, dyn<br> [0x80007b9c]:csrrs a6, fcsr, zero<br> [0x80007ba0]:sw t5, 680(ra)<br> [0x80007ba4]:sw t6, 688(ra)<br> [0x80007ba8]:sw t5, 696(ra)<br> [0x80007bac]:sw a6, 704(ra)<br>                                     |
| 353|[0x80013178]<br>0x00000000<br> [0x80013190]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007bd8]:fmul.d t5, t3, s10, dyn<br> [0x80007bdc]:csrrs a6, fcsr, zero<br> [0x80007be0]:sw t5, 712(ra)<br> [0x80007be4]:sw t6, 720(ra)<br> [0x80007be8]:sw t5, 728(ra)<br> [0x80007bec]:sw a6, 736(ra)<br>                                     |
| 354|[0x80013198]<br>0x00000000<br> [0x800131b0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007c18]:fmul.d t5, t3, s10, dyn<br> [0x80007c1c]:csrrs a6, fcsr, zero<br> [0x80007c20]:sw t5, 744(ra)<br> [0x80007c24]:sw t6, 752(ra)<br> [0x80007c28]:sw t5, 760(ra)<br> [0x80007c2c]:sw a6, 768(ra)<br>                                     |
| 355|[0x800131b8]<br>0x00000000<br> [0x800131d0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007c58]:fmul.d t5, t3, s10, dyn<br> [0x80007c5c]:csrrs a6, fcsr, zero<br> [0x80007c60]:sw t5, 776(ra)<br> [0x80007c64]:sw t6, 784(ra)<br> [0x80007c68]:sw t5, 792(ra)<br> [0x80007c6c]:sw a6, 800(ra)<br>                                     |
| 356|[0x800131d8]<br>0x00000000<br> [0x800131f0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007c98]:fmul.d t5, t3, s10, dyn<br> [0x80007c9c]:csrrs a6, fcsr, zero<br> [0x80007ca0]:sw t5, 808(ra)<br> [0x80007ca4]:sw t6, 816(ra)<br> [0x80007ca8]:sw t5, 824(ra)<br> [0x80007cac]:sw a6, 832(ra)<br>                                     |
| 357|[0x800131f8]<br>0x00000000<br> [0x80013210]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007cd8]:fmul.d t5, t3, s10, dyn<br> [0x80007cdc]:csrrs a6, fcsr, zero<br> [0x80007ce0]:sw t5, 840(ra)<br> [0x80007ce4]:sw t6, 848(ra)<br> [0x80007ce8]:sw t5, 856(ra)<br> [0x80007cec]:sw a6, 864(ra)<br>                                     |
| 358|[0x80013218]<br>0x00000000<br> [0x80013230]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007d18]:fmul.d t5, t3, s10, dyn<br> [0x80007d1c]:csrrs a6, fcsr, zero<br> [0x80007d20]:sw t5, 872(ra)<br> [0x80007d24]:sw t6, 880(ra)<br> [0x80007d28]:sw t5, 888(ra)<br> [0x80007d2c]:sw a6, 896(ra)<br>                                     |
| 359|[0x80013238]<br>0x00000000<br> [0x80013250]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007d58]:fmul.d t5, t3, s10, dyn<br> [0x80007d5c]:csrrs a6, fcsr, zero<br> [0x80007d60]:sw t5, 904(ra)<br> [0x80007d64]:sw t6, 912(ra)<br> [0x80007d68]:sw t5, 920(ra)<br> [0x80007d6c]:sw a6, 928(ra)<br>                                     |
| 360|[0x80013258]<br>0x00000000<br> [0x80013270]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007d98]:fmul.d t5, t3, s10, dyn<br> [0x80007d9c]:csrrs a6, fcsr, zero<br> [0x80007da0]:sw t5, 936(ra)<br> [0x80007da4]:sw t6, 944(ra)<br> [0x80007da8]:sw t5, 952(ra)<br> [0x80007dac]:sw a6, 960(ra)<br>                                     |
| 361|[0x80013278]<br>0x00000000<br> [0x80013290]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007dd8]:fmul.d t5, t3, s10, dyn<br> [0x80007ddc]:csrrs a6, fcsr, zero<br> [0x80007de0]:sw t5, 968(ra)<br> [0x80007de4]:sw t6, 976(ra)<br> [0x80007de8]:sw t5, 984(ra)<br> [0x80007dec]:sw a6, 992(ra)<br>                                     |
| 362|[0x80013298]<br>0x00000000<br> [0x800132b0]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007e18]:fmul.d t5, t3, s10, dyn<br> [0x80007e1c]:csrrs a6, fcsr, zero<br> [0x80007e20]:sw t5, 1000(ra)<br> [0x80007e24]:sw t6, 1008(ra)<br> [0x80007e28]:sw t5, 1016(ra)<br> [0x80007e2c]:sw a6, 1024(ra)<br>                                 |
| 363|[0x800132b8]<br>0x00000000<br> [0x800132d0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007e58]:fmul.d t5, t3, s10, dyn<br> [0x80007e5c]:csrrs a6, fcsr, zero<br> [0x80007e60]:sw t5, 1032(ra)<br> [0x80007e64]:sw t6, 1040(ra)<br> [0x80007e68]:sw t5, 1048(ra)<br> [0x80007e6c]:sw a6, 1056(ra)<br>                                 |
| 364|[0x800132d8]<br>0x00000000<br> [0x800132f0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007e98]:fmul.d t5, t3, s10, dyn<br> [0x80007e9c]:csrrs a6, fcsr, zero<br> [0x80007ea0]:sw t5, 1064(ra)<br> [0x80007ea4]:sw t6, 1072(ra)<br> [0x80007ea8]:sw t5, 1080(ra)<br> [0x80007eac]:sw a6, 1088(ra)<br>                                 |
| 365|[0x800132f8]<br>0x00000000<br> [0x80013310]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007ed8]:fmul.d t5, t3, s10, dyn<br> [0x80007edc]:csrrs a6, fcsr, zero<br> [0x80007ee0]:sw t5, 1096(ra)<br> [0x80007ee4]:sw t6, 1104(ra)<br> [0x80007ee8]:sw t5, 1112(ra)<br> [0x80007eec]:sw a6, 1120(ra)<br>                                 |
| 366|[0x80013318]<br>0x00000000<br> [0x80013330]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007f18]:fmul.d t5, t3, s10, dyn<br> [0x80007f1c]:csrrs a6, fcsr, zero<br> [0x80007f20]:sw t5, 1128(ra)<br> [0x80007f24]:sw t6, 1136(ra)<br> [0x80007f28]:sw t5, 1144(ra)<br> [0x80007f2c]:sw a6, 1152(ra)<br>                                 |
| 367|[0x80013338]<br>0x00000000<br> [0x80013350]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007f5c]:fmul.d t5, t3, s10, dyn<br> [0x80007f60]:csrrs a6, fcsr, zero<br> [0x80007f64]:sw t5, 1160(ra)<br> [0x80007f68]:sw t6, 1168(ra)<br> [0x80007f6c]:sw t5, 1176(ra)<br> [0x80007f70]:sw a6, 1184(ra)<br>                                 |
| 368|[0x80013358]<br>0x00000000<br> [0x80013370]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007fa0]:fmul.d t5, t3, s10, dyn<br> [0x80007fa4]:csrrs a6, fcsr, zero<br> [0x80007fa8]:sw t5, 1192(ra)<br> [0x80007fac]:sw t6, 1200(ra)<br> [0x80007fb0]:sw t5, 1208(ra)<br> [0x80007fb4]:sw a6, 1216(ra)<br>                                 |
| 369|[0x80013378]<br>0x00000000<br> [0x80013390]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80007fe0]:fmul.d t5, t3, s10, dyn<br> [0x80007fe4]:csrrs a6, fcsr, zero<br> [0x80007fe8]:sw t5, 1224(ra)<br> [0x80007fec]:sw t6, 1232(ra)<br> [0x80007ff0]:sw t5, 1240(ra)<br> [0x80007ff4]:sw a6, 1248(ra)<br>                                 |
| 370|[0x80013398]<br>0x00000000<br> [0x800133b0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80008020]:fmul.d t5, t3, s10, dyn<br> [0x80008024]:csrrs a6, fcsr, zero<br> [0x80008028]:sw t5, 1256(ra)<br> [0x8000802c]:sw t6, 1264(ra)<br> [0x80008030]:sw t5, 1272(ra)<br> [0x80008034]:sw a6, 1280(ra)<br>                                 |
| 371|[0x800133b8]<br>0x00000000<br> [0x800133d0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80008060]:fmul.d t5, t3, s10, dyn<br> [0x80008064]:csrrs a6, fcsr, zero<br> [0x80008068]:sw t5, 1288(ra)<br> [0x8000806c]:sw t6, 1296(ra)<br> [0x80008070]:sw t5, 1304(ra)<br> [0x80008074]:sw a6, 1312(ra)<br>                                 |
| 372|[0x800133d8]<br>0x00000000<br> [0x800133f0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800080a0]:fmul.d t5, t3, s10, dyn<br> [0x800080a4]:csrrs a6, fcsr, zero<br> [0x800080a8]:sw t5, 1320(ra)<br> [0x800080ac]:sw t6, 1328(ra)<br> [0x800080b0]:sw t5, 1336(ra)<br> [0x800080b4]:sw a6, 1344(ra)<br>                                 |
| 373|[0x800133f8]<br>0x00000000<br> [0x80013410]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800080e4]:fmul.d t5, t3, s10, dyn<br> [0x800080e8]:csrrs a6, fcsr, zero<br> [0x800080ec]:sw t5, 1352(ra)<br> [0x800080f0]:sw t6, 1360(ra)<br> [0x800080f4]:sw t5, 1368(ra)<br> [0x800080f8]:sw a6, 1376(ra)<br>                                 |
| 374|[0x80013418]<br>0x00000000<br> [0x80013430]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80008128]:fmul.d t5, t3, s10, dyn<br> [0x8000812c]:csrrs a6, fcsr, zero<br> [0x80008130]:sw t5, 1384(ra)<br> [0x80008134]:sw t6, 1392(ra)<br> [0x80008138]:sw t5, 1400(ra)<br> [0x8000813c]:sw a6, 1408(ra)<br>                                 |
| 375|[0x80013438]<br>0x00000000<br> [0x80013450]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80008168]:fmul.d t5, t3, s10, dyn<br> [0x8000816c]:csrrs a6, fcsr, zero<br> [0x80008170]:sw t5, 1416(ra)<br> [0x80008174]:sw t6, 1424(ra)<br> [0x80008178]:sw t5, 1432(ra)<br> [0x8000817c]:sw a6, 1440(ra)<br>                                 |
| 376|[0x80013458]<br>0x00000000<br> [0x80013470]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800081a8]:fmul.d t5, t3, s10, dyn<br> [0x800081ac]:csrrs a6, fcsr, zero<br> [0x800081b0]:sw t5, 1448(ra)<br> [0x800081b4]:sw t6, 1456(ra)<br> [0x800081b8]:sw t5, 1464(ra)<br> [0x800081bc]:sw a6, 1472(ra)<br>                                 |
| 377|[0x80013478]<br>0x00000000<br> [0x80013490]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800081e8]:fmul.d t5, t3, s10, dyn<br> [0x800081ec]:csrrs a6, fcsr, zero<br> [0x800081f0]:sw t5, 1480(ra)<br> [0x800081f4]:sw t6, 1488(ra)<br> [0x800081f8]:sw t5, 1496(ra)<br> [0x800081fc]:sw a6, 1504(ra)<br>                                 |
| 378|[0x80013498]<br>0x00000000<br> [0x800134b0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80008228]:fmul.d t5, t3, s10, dyn<br> [0x8000822c]:csrrs a6, fcsr, zero<br> [0x80008230]:sw t5, 1512(ra)<br> [0x80008234]:sw t6, 1520(ra)<br> [0x80008238]:sw t5, 1528(ra)<br> [0x8000823c]:sw a6, 1536(ra)<br>                                 |
| 379|[0x800134b8]<br>0x00000000<br> [0x800134d0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80008268]:fmul.d t5, t3, s10, dyn<br> [0x8000826c]:csrrs a6, fcsr, zero<br> [0x80008270]:sw t5, 1544(ra)<br> [0x80008274]:sw t6, 1552(ra)<br> [0x80008278]:sw t5, 1560(ra)<br> [0x8000827c]:sw a6, 1568(ra)<br>                                 |
| 380|[0x800134d8]<br>0x00000000<br> [0x800134f0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800082a8]:fmul.d t5, t3, s10, dyn<br> [0x800082ac]:csrrs a6, fcsr, zero<br> [0x800082b0]:sw t5, 1576(ra)<br> [0x800082b4]:sw t6, 1584(ra)<br> [0x800082b8]:sw t5, 1592(ra)<br> [0x800082bc]:sw a6, 1600(ra)<br>                                 |
| 381|[0x800134f8]<br>0x00000000<br> [0x80013510]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800082e8]:fmul.d t5, t3, s10, dyn<br> [0x800082ec]:csrrs a6, fcsr, zero<br> [0x800082f0]:sw t5, 1608(ra)<br> [0x800082f4]:sw t6, 1616(ra)<br> [0x800082f8]:sw t5, 1624(ra)<br> [0x800082fc]:sw a6, 1632(ra)<br>                                 |
| 382|[0x80013518]<br>0x00000000<br> [0x80013530]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80008328]:fmul.d t5, t3, s10, dyn<br> [0x8000832c]:csrrs a6, fcsr, zero<br> [0x80008330]:sw t5, 1640(ra)<br> [0x80008334]:sw t6, 1648(ra)<br> [0x80008338]:sw t5, 1656(ra)<br> [0x8000833c]:sw a6, 1664(ra)<br>                                 |
| 383|[0x80013538]<br>0x00000000<br> [0x80013550]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80008368]:fmul.d t5, t3, s10, dyn<br> [0x8000836c]:csrrs a6, fcsr, zero<br> [0x80008370]:sw t5, 1672(ra)<br> [0x80008374]:sw t6, 1680(ra)<br> [0x80008378]:sw t5, 1688(ra)<br> [0x8000837c]:sw a6, 1696(ra)<br>                                 |
| 384|[0x80013558]<br>0x00000000<br> [0x80013570]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800083a8]:fmul.d t5, t3, s10, dyn<br> [0x800083ac]:csrrs a6, fcsr, zero<br> [0x800083b0]:sw t5, 1704(ra)<br> [0x800083b4]:sw t6, 1712(ra)<br> [0x800083b8]:sw t5, 1720(ra)<br> [0x800083bc]:sw a6, 1728(ra)<br>                                 |
| 385|[0x80013578]<br>0x00000000<br> [0x80013590]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800083e8]:fmul.d t5, t3, s10, dyn<br> [0x800083ec]:csrrs a6, fcsr, zero<br> [0x800083f0]:sw t5, 1736(ra)<br> [0x800083f4]:sw t6, 1744(ra)<br> [0x800083f8]:sw t5, 1752(ra)<br> [0x800083fc]:sw a6, 1760(ra)<br>                                 |
| 386|[0x80013598]<br>0x00000000<br> [0x800135b0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80008428]:fmul.d t5, t3, s10, dyn<br> [0x8000842c]:csrrs a6, fcsr, zero<br> [0x80008430]:sw t5, 1768(ra)<br> [0x80008434]:sw t6, 1776(ra)<br> [0x80008438]:sw t5, 1784(ra)<br> [0x8000843c]:sw a6, 1792(ra)<br>                                 |
| 387|[0x800135b8]<br>0x00000000<br> [0x800135d0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80008468]:fmul.d t5, t3, s10, dyn<br> [0x8000846c]:csrrs a6, fcsr, zero<br> [0x80008470]:sw t5, 1800(ra)<br> [0x80008474]:sw t6, 1808(ra)<br> [0x80008478]:sw t5, 1816(ra)<br> [0x8000847c]:sw a6, 1824(ra)<br>                                 |
| 388|[0x800135d8]<br>0x00000000<br> [0x800135f0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800084a8]:fmul.d t5, t3, s10, dyn<br> [0x800084ac]:csrrs a6, fcsr, zero<br> [0x800084b0]:sw t5, 1832(ra)<br> [0x800084b4]:sw t6, 1840(ra)<br> [0x800084b8]:sw t5, 1848(ra)<br> [0x800084bc]:sw a6, 1856(ra)<br>                                 |
| 389|[0x800135f8]<br>0x00000000<br> [0x80013610]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800084e8]:fmul.d t5, t3, s10, dyn<br> [0x800084ec]:csrrs a6, fcsr, zero<br> [0x800084f0]:sw t5, 1864(ra)<br> [0x800084f4]:sw t6, 1872(ra)<br> [0x800084f8]:sw t5, 1880(ra)<br> [0x800084fc]:sw a6, 1888(ra)<br>                                 |
| 390|[0x80013618]<br>0x00000000<br> [0x80013630]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80008528]:fmul.d t5, t3, s10, dyn<br> [0x8000852c]:csrrs a6, fcsr, zero<br> [0x80008530]:sw t5, 1896(ra)<br> [0x80008534]:sw t6, 1904(ra)<br> [0x80008538]:sw t5, 1912(ra)<br> [0x8000853c]:sw a6, 1920(ra)<br>                                 |
| 391|[0x80013638]<br>0x00000000<br> [0x80013650]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000856c]:fmul.d t5, t3, s10, dyn<br> [0x80008570]:csrrs a6, fcsr, zero<br> [0x80008574]:sw t5, 1928(ra)<br> [0x80008578]:sw t6, 1936(ra)<br> [0x8000857c]:sw t5, 1944(ra)<br> [0x80008580]:sw a6, 1952(ra)<br>                                 |
| 392|[0x80013658]<br>0x00000000<br> [0x80013670]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800085b0]:fmul.d t5, t3, s10, dyn<br> [0x800085b4]:csrrs a6, fcsr, zero<br> [0x800085b8]:sw t5, 1960(ra)<br> [0x800085bc]:sw t6, 1968(ra)<br> [0x800085c0]:sw t5, 1976(ra)<br> [0x800085c4]:sw a6, 1984(ra)<br>                                 |
| 393|[0x80013678]<br>0x00000000<br> [0x80013690]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800085f0]:fmul.d t5, t3, s10, dyn<br> [0x800085f4]:csrrs a6, fcsr, zero<br> [0x800085f8]:sw t5, 1992(ra)<br> [0x800085fc]:sw t6, 2000(ra)<br> [0x80008600]:sw t5, 2008(ra)<br> [0x80008604]:sw a6, 2016(ra)<br>                                 |
| 394|[0x80013698]<br>0x00000000<br> [0x800136b0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80008630]:fmul.d t5, t3, s10, dyn<br> [0x80008634]:csrrs a6, fcsr, zero<br> [0x80008638]:sw t5, 2024(ra)<br> [0x8000863c]:sw t6, 2032(ra)<br> [0x80008640]:sw t5, 2040(ra)<br> [0x80008644]:addi ra, ra, 2040<br> [0x80008648]:sw a6, 8(ra)<br> |
| 395|[0x800136b8]<br>0x00000000<br> [0x800136d0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800086b4]:fmul.d t5, t3, s10, dyn<br> [0x800086b8]:csrrs a6, fcsr, zero<br> [0x800086bc]:sw t5, 16(ra)<br> [0x800086c0]:sw t6, 24(ra)<br> [0x800086c4]:sw t5, 32(ra)<br> [0x800086c8]:sw a6, 40(ra)<br>                                         |
| 396|[0x800136d8]<br>0x00000000<br> [0x800136f0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80008734]:fmul.d t5, t3, s10, dyn<br> [0x80008738]:csrrs a6, fcsr, zero<br> [0x8000873c]:sw t5, 48(ra)<br> [0x80008740]:sw t6, 56(ra)<br> [0x80008744]:sw t5, 64(ra)<br> [0x80008748]:sw a6, 72(ra)<br>                                         |
| 397|[0x800136f8]<br>0x00000000<br> [0x80013710]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800087b8]:fmul.d t5, t3, s10, dyn<br> [0x800087bc]:csrrs a6, fcsr, zero<br> [0x800087c0]:sw t5, 80(ra)<br> [0x800087c4]:sw t6, 88(ra)<br> [0x800087c8]:sw t5, 96(ra)<br> [0x800087cc]:sw a6, 104(ra)<br>                                        |
| 398|[0x80013718]<br>0x00000000<br> [0x80013730]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000883c]:fmul.d t5, t3, s10, dyn<br> [0x80008840]:csrrs a6, fcsr, zero<br> [0x80008844]:sw t5, 112(ra)<br> [0x80008848]:sw t6, 120(ra)<br> [0x8000884c]:sw t5, 128(ra)<br> [0x80008850]:sw a6, 136(ra)<br>                                     |
| 399|[0x80013738]<br>0x00000000<br> [0x80013750]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800088bc]:fmul.d t5, t3, s10, dyn<br> [0x800088c0]:csrrs a6, fcsr, zero<br> [0x800088c4]:sw t5, 144(ra)<br> [0x800088c8]:sw t6, 152(ra)<br> [0x800088cc]:sw t5, 160(ra)<br> [0x800088d0]:sw a6, 168(ra)<br>                                     |
| 400|[0x80013758]<br>0x00000000<br> [0x80013770]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000893c]:fmul.d t5, t3, s10, dyn<br> [0x80008940]:csrrs a6, fcsr, zero<br> [0x80008944]:sw t5, 176(ra)<br> [0x80008948]:sw t6, 184(ra)<br> [0x8000894c]:sw t5, 192(ra)<br> [0x80008950]:sw a6, 200(ra)<br>                                     |
| 401|[0x80013778]<br>0x00000000<br> [0x80013790]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800089bc]:fmul.d t5, t3, s10, dyn<br> [0x800089c0]:csrrs a6, fcsr, zero<br> [0x800089c4]:sw t5, 208(ra)<br> [0x800089c8]:sw t6, 216(ra)<br> [0x800089cc]:sw t5, 224(ra)<br> [0x800089d0]:sw a6, 232(ra)<br>                                     |
| 402|[0x80013798]<br>0x00000000<br> [0x800137b0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80008a3c]:fmul.d t5, t3, s10, dyn<br> [0x80008a40]:csrrs a6, fcsr, zero<br> [0x80008a44]:sw t5, 240(ra)<br> [0x80008a48]:sw t6, 248(ra)<br> [0x80008a4c]:sw t5, 256(ra)<br> [0x80008a50]:sw a6, 264(ra)<br>                                     |
| 403|[0x800137b8]<br>0x00000000<br> [0x800137d0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80008abc]:fmul.d t5, t3, s10, dyn<br> [0x80008ac0]:csrrs a6, fcsr, zero<br> [0x80008ac4]:sw t5, 272(ra)<br> [0x80008ac8]:sw t6, 280(ra)<br> [0x80008acc]:sw t5, 288(ra)<br> [0x80008ad0]:sw a6, 296(ra)<br>                                     |
| 404|[0x800137d8]<br>0x00000000<br> [0x800137f0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80008b3c]:fmul.d t5, t3, s10, dyn<br> [0x80008b40]:csrrs a6, fcsr, zero<br> [0x80008b44]:sw t5, 304(ra)<br> [0x80008b48]:sw t6, 312(ra)<br> [0x80008b4c]:sw t5, 320(ra)<br> [0x80008b50]:sw a6, 328(ra)<br>                                     |
| 405|[0x800137f8]<br>0x00000000<br> [0x80013810]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80008bbc]:fmul.d t5, t3, s10, dyn<br> [0x80008bc0]:csrrs a6, fcsr, zero<br> [0x80008bc4]:sw t5, 336(ra)<br> [0x80008bc8]:sw t6, 344(ra)<br> [0x80008bcc]:sw t5, 352(ra)<br> [0x80008bd0]:sw a6, 360(ra)<br>                                     |
| 406|[0x80013818]<br>0x00000000<br> [0x80013830]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80008c3c]:fmul.d t5, t3, s10, dyn<br> [0x80008c40]:csrrs a6, fcsr, zero<br> [0x80008c44]:sw t5, 368(ra)<br> [0x80008c48]:sw t6, 376(ra)<br> [0x80008c4c]:sw t5, 384(ra)<br> [0x80008c50]:sw a6, 392(ra)<br>                                     |
| 407|[0x80013838]<br>0x00000000<br> [0x80013850]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80008cbc]:fmul.d t5, t3, s10, dyn<br> [0x80008cc0]:csrrs a6, fcsr, zero<br> [0x80008cc4]:sw t5, 400(ra)<br> [0x80008cc8]:sw t6, 408(ra)<br> [0x80008ccc]:sw t5, 416(ra)<br> [0x80008cd0]:sw a6, 424(ra)<br>                                     |
| 408|[0x80013858]<br>0x00000000<br> [0x80013870]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80008d3c]:fmul.d t5, t3, s10, dyn<br> [0x80008d40]:csrrs a6, fcsr, zero<br> [0x80008d44]:sw t5, 432(ra)<br> [0x80008d48]:sw t6, 440(ra)<br> [0x80008d4c]:sw t5, 448(ra)<br> [0x80008d50]:sw a6, 456(ra)<br>                                     |
| 409|[0x80013878]<br>0x00000000<br> [0x80013890]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80008dbc]:fmul.d t5, t3, s10, dyn<br> [0x80008dc0]:csrrs a6, fcsr, zero<br> [0x80008dc4]:sw t5, 464(ra)<br> [0x80008dc8]:sw t6, 472(ra)<br> [0x80008dcc]:sw t5, 480(ra)<br> [0x80008dd0]:sw a6, 488(ra)<br>                                     |
| 410|[0x80013898]<br>0x00000000<br> [0x800138b0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80008e3c]:fmul.d t5, t3, s10, dyn<br> [0x80008e40]:csrrs a6, fcsr, zero<br> [0x80008e44]:sw t5, 496(ra)<br> [0x80008e48]:sw t6, 504(ra)<br> [0x80008e4c]:sw t5, 512(ra)<br> [0x80008e50]:sw a6, 520(ra)<br>                                     |
| 411|[0x800138b8]<br>0x00000000<br> [0x800138d0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80008ebc]:fmul.d t5, t3, s10, dyn<br> [0x80008ec0]:csrrs a6, fcsr, zero<br> [0x80008ec4]:sw t5, 528(ra)<br> [0x80008ec8]:sw t6, 536(ra)<br> [0x80008ecc]:sw t5, 544(ra)<br> [0x80008ed0]:sw a6, 552(ra)<br>                                     |
| 412|[0x800138d8]<br>0x00000000<br> [0x800138f0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80008f3c]:fmul.d t5, t3, s10, dyn<br> [0x80008f40]:csrrs a6, fcsr, zero<br> [0x80008f44]:sw t5, 560(ra)<br> [0x80008f48]:sw t6, 568(ra)<br> [0x80008f4c]:sw t5, 576(ra)<br> [0x80008f50]:sw a6, 584(ra)<br>                                     |
| 413|[0x800138f8]<br>0x00000000<br> [0x80013910]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80008fbc]:fmul.d t5, t3, s10, dyn<br> [0x80008fc0]:csrrs a6, fcsr, zero<br> [0x80008fc4]:sw t5, 592(ra)<br> [0x80008fc8]:sw t6, 600(ra)<br> [0x80008fcc]:sw t5, 608(ra)<br> [0x80008fd0]:sw a6, 616(ra)<br>                                     |
| 414|[0x80013918]<br>0x00000000<br> [0x80013930]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000903c]:fmul.d t5, t3, s10, dyn<br> [0x80009040]:csrrs a6, fcsr, zero<br> [0x80009044]:sw t5, 624(ra)<br> [0x80009048]:sw t6, 632(ra)<br> [0x8000904c]:sw t5, 640(ra)<br> [0x80009050]:sw a6, 648(ra)<br>                                     |
| 415|[0x80013938]<br>0x00000000<br> [0x80013950]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800090c0]:fmul.d t5, t3, s10, dyn<br> [0x800090c4]:csrrs a6, fcsr, zero<br> [0x800090c8]:sw t5, 656(ra)<br> [0x800090cc]:sw t6, 664(ra)<br> [0x800090d0]:sw t5, 672(ra)<br> [0x800090d4]:sw a6, 680(ra)<br>                                     |
| 416|[0x80013958]<br>0x00000000<br> [0x80013970]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80009144]:fmul.d t5, t3, s10, dyn<br> [0x80009148]:csrrs a6, fcsr, zero<br> [0x8000914c]:sw t5, 688(ra)<br> [0x80009150]:sw t6, 696(ra)<br> [0x80009154]:sw t5, 704(ra)<br> [0x80009158]:sw a6, 712(ra)<br>                                     |
| 417|[0x80013978]<br>0x00000000<br> [0x80013990]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800091c4]:fmul.d t5, t3, s10, dyn<br> [0x800091c8]:csrrs a6, fcsr, zero<br> [0x800091cc]:sw t5, 720(ra)<br> [0x800091d0]:sw t6, 728(ra)<br> [0x800091d4]:sw t5, 736(ra)<br> [0x800091d8]:sw a6, 744(ra)<br>                                     |
| 418|[0x80013998]<br>0x00000000<br> [0x800139b0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80009244]:fmul.d t5, t3, s10, dyn<br> [0x80009248]:csrrs a6, fcsr, zero<br> [0x8000924c]:sw t5, 752(ra)<br> [0x80009250]:sw t6, 760(ra)<br> [0x80009254]:sw t5, 768(ra)<br> [0x80009258]:sw a6, 776(ra)<br>                                     |
| 419|[0x800139b8]<br>0x00000000<br> [0x800139d0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800092c4]:fmul.d t5, t3, s10, dyn<br> [0x800092c8]:csrrs a6, fcsr, zero<br> [0x800092cc]:sw t5, 784(ra)<br> [0x800092d0]:sw t6, 792(ra)<br> [0x800092d4]:sw t5, 800(ra)<br> [0x800092d8]:sw a6, 808(ra)<br>                                     |
| 420|[0x800139d8]<br>0x00000000<br> [0x800139f0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x80009344]:fmul.d t5, t3, s10, dyn<br> [0x80009348]:csrrs a6, fcsr, zero<br> [0x8000934c]:sw t5, 816(ra)<br> [0x80009350]:sw t6, 824(ra)<br> [0x80009354]:sw t5, 832(ra)<br> [0x80009358]:sw a6, 840(ra)<br>                                     |
| 421|[0x800139f8]<br>0x00000000<br> [0x80013a10]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x800093c8]:fmul.d t5, t3, s10, dyn<br> [0x800093cc]:csrrs a6, fcsr, zero<br> [0x800093d0]:sw t5, 848(ra)<br> [0x800093d4]:sw t6, 856(ra)<br> [0x800093d8]:sw t5, 864(ra)<br> [0x800093dc]:sw a6, 872(ra)<br>                                     |
| 422|[0x80013a18]<br>0x00000000<br> [0x80013a30]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000944c]:fmul.d t5, t3, s10, dyn<br> [0x80009450]:csrrs a6, fcsr, zero<br> [0x80009454]:sw t5, 880(ra)<br> [0x80009458]:sw t6, 888(ra)<br> [0x8000945c]:sw t5, 896(ra)<br> [0x80009460]:sw a6, 904(ra)<br>                                     |
| 423|[0x800136b8]<br>0x00000000<br> [0x800136d0]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000c6dc]:fmul.d t5, t3, s10, dyn<br> [0x8000c6e0]:csrrs a6, fcsr, zero<br> [0x8000c6e4]:sw t5, 0(ra)<br> [0x8000c6e8]:sw t6, 8(ra)<br> [0x8000c6ec]:sw t5, 16(ra)<br> [0x8000c6f0]:sw a6, 24(ra)<br>                                           |
| 424|[0x800136d8]<br>0x00000000<br> [0x800136f0]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000c71c]:fmul.d t5, t3, s10, dyn<br> [0x8000c720]:csrrs a6, fcsr, zero<br> [0x8000c724]:sw t5, 32(ra)<br> [0x8000c728]:sw t6, 40(ra)<br> [0x8000c72c]:sw t5, 48(ra)<br> [0x8000c730]:sw a6, 56(ra)<br>                                         |
| 425|[0x800136f8]<br>0x00000000<br> [0x80013710]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000c75c]:fmul.d t5, t3, s10, dyn<br> [0x8000c760]:csrrs a6, fcsr, zero<br> [0x8000c764]:sw t5, 64(ra)<br> [0x8000c768]:sw t6, 72(ra)<br> [0x8000c76c]:sw t5, 80(ra)<br> [0x8000c770]:sw a6, 88(ra)<br>                                         |
| 426|[0x80013718]<br>0x00000000<br> [0x80013730]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000c79c]:fmul.d t5, t3, s10, dyn<br> [0x8000c7a0]:csrrs a6, fcsr, zero<br> [0x8000c7a4]:sw t5, 96(ra)<br> [0x8000c7a8]:sw t6, 104(ra)<br> [0x8000c7ac]:sw t5, 112(ra)<br> [0x8000c7b0]:sw a6, 120(ra)<br>                                      |
| 427|[0x80013738]<br>0x00000000<br> [0x80013750]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000c7dc]:fmul.d t5, t3, s10, dyn<br> [0x8000c7e0]:csrrs a6, fcsr, zero<br> [0x8000c7e4]:sw t5, 128(ra)<br> [0x8000c7e8]:sw t6, 136(ra)<br> [0x8000c7ec]:sw t5, 144(ra)<br> [0x8000c7f0]:sw a6, 152(ra)<br>                                     |
| 428|[0x80013758]<br>0x00000000<br> [0x80013770]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000c81c]:fmul.d t5, t3, s10, dyn<br> [0x8000c820]:csrrs a6, fcsr, zero<br> [0x8000c824]:sw t5, 160(ra)<br> [0x8000c828]:sw t6, 168(ra)<br> [0x8000c82c]:sw t5, 176(ra)<br> [0x8000c830]:sw a6, 184(ra)<br>                                     |
| 429|[0x80013778]<br>0x00000000<br> [0x80013790]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000c85c]:fmul.d t5, t3, s10, dyn<br> [0x8000c860]:csrrs a6, fcsr, zero<br> [0x8000c864]:sw t5, 192(ra)<br> [0x8000c868]:sw t6, 200(ra)<br> [0x8000c86c]:sw t5, 208(ra)<br> [0x8000c870]:sw a6, 216(ra)<br>                                     |
| 430|[0x80013798]<br>0x00000000<br> [0x800137b0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000c89c]:fmul.d t5, t3, s10, dyn<br> [0x8000c8a0]:csrrs a6, fcsr, zero<br> [0x8000c8a4]:sw t5, 224(ra)<br> [0x8000c8a8]:sw t6, 232(ra)<br> [0x8000c8ac]:sw t5, 240(ra)<br> [0x8000c8b0]:sw a6, 248(ra)<br>                                     |
| 431|[0x800137b8]<br>0x00000001<br> [0x800137d0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000c8dc]:fmul.d t5, t3, s10, dyn<br> [0x8000c8e0]:csrrs a6, fcsr, zero<br> [0x8000c8e4]:sw t5, 256(ra)<br> [0x8000c8e8]:sw t6, 264(ra)<br> [0x8000c8ec]:sw t5, 272(ra)<br> [0x8000c8f0]:sw a6, 280(ra)<br>                                     |
| 432|[0x800137d8]<br>0x00000001<br> [0x800137f0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000c91c]:fmul.d t5, t3, s10, dyn<br> [0x8000c920]:csrrs a6, fcsr, zero<br> [0x8000c924]:sw t5, 288(ra)<br> [0x8000c928]:sw t6, 296(ra)<br> [0x8000c92c]:sw t5, 304(ra)<br> [0x8000c930]:sw a6, 312(ra)<br>                                     |
| 433|[0x800137f8]<br>0x00000002<br> [0x80013810]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000c95c]:fmul.d t5, t3, s10, dyn<br> [0x8000c960]:csrrs a6, fcsr, zero<br> [0x8000c964]:sw t5, 320(ra)<br> [0x8000c968]:sw t6, 328(ra)<br> [0x8000c96c]:sw t5, 336(ra)<br> [0x8000c970]:sw a6, 344(ra)<br>                                     |
| 434|[0x80013818]<br>0x00000002<br> [0x80013830]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000c99c]:fmul.d t5, t3, s10, dyn<br> [0x8000c9a0]:csrrs a6, fcsr, zero<br> [0x8000c9a4]:sw t5, 352(ra)<br> [0x8000c9a8]:sw t6, 360(ra)<br> [0x8000c9ac]:sw t5, 368(ra)<br> [0x8000c9b0]:sw a6, 376(ra)<br>                                     |
| 435|[0x80013838]<br>0xFFFFFFFF<br> [0x80013850]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000c9e0]:fmul.d t5, t3, s10, dyn<br> [0x8000c9e4]:csrrs a6, fcsr, zero<br> [0x8000c9e8]:sw t5, 384(ra)<br> [0x8000c9ec]:sw t6, 392(ra)<br> [0x8000c9f0]:sw t5, 400(ra)<br> [0x8000c9f4]:sw a6, 408(ra)<br>                                     |
| 436|[0x80013858]<br>0xFFFFFFFF<br> [0x80013870]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000ca24]:fmul.d t5, t3, s10, dyn<br> [0x8000ca28]:csrrs a6, fcsr, zero<br> [0x8000ca2c]:sw t5, 416(ra)<br> [0x8000ca30]:sw t6, 424(ra)<br> [0x8000ca34]:sw t5, 432(ra)<br> [0x8000ca38]:sw a6, 440(ra)<br>                                     |
| 437|[0x80013878]<br>0x00000000<br> [0x80013890]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000ca64]:fmul.d t5, t3, s10, dyn<br> [0x8000ca68]:csrrs a6, fcsr, zero<br> [0x8000ca6c]:sw t5, 448(ra)<br> [0x8000ca70]:sw t6, 456(ra)<br> [0x8000ca74]:sw t5, 464(ra)<br> [0x8000ca78]:sw a6, 472(ra)<br>                                     |
| 438|[0x80013898]<br>0x00000000<br> [0x800138b0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000caa4]:fmul.d t5, t3, s10, dyn<br> [0x8000caa8]:csrrs a6, fcsr, zero<br> [0x8000caac]:sw t5, 480(ra)<br> [0x8000cab0]:sw t6, 488(ra)<br> [0x8000cab4]:sw t5, 496(ra)<br> [0x8000cab8]:sw a6, 504(ra)<br>                                     |
| 439|[0x800138b8]<br>0x00000002<br> [0x800138d0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000cae4]:fmul.d t5, t3, s10, dyn<br> [0x8000cae8]:csrrs a6, fcsr, zero<br> [0x8000caec]:sw t5, 512(ra)<br> [0x8000caf0]:sw t6, 520(ra)<br> [0x8000caf4]:sw t5, 528(ra)<br> [0x8000caf8]:sw a6, 536(ra)<br>                                     |
| 440|[0x800138d8]<br>0x00000002<br> [0x800138f0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000cb24]:fmul.d t5, t3, s10, dyn<br> [0x8000cb28]:csrrs a6, fcsr, zero<br> [0x8000cb2c]:sw t5, 544(ra)<br> [0x8000cb30]:sw t6, 552(ra)<br> [0x8000cb34]:sw t5, 560(ra)<br> [0x8000cb38]:sw a6, 568(ra)<br>                                     |
| 441|[0x800138f8]<br>0xFFFFFFFF<br> [0x80013910]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000cb68]:fmul.d t5, t3, s10, dyn<br> [0x8000cb6c]:csrrs a6, fcsr, zero<br> [0x8000cb70]:sw t5, 576(ra)<br> [0x8000cb74]:sw t6, 584(ra)<br> [0x8000cb78]:sw t5, 592(ra)<br> [0x8000cb7c]:sw a6, 600(ra)<br>                                     |
| 442|[0x80013918]<br>0xFFFFFFFF<br> [0x80013930]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000cbac]:fmul.d t5, t3, s10, dyn<br> [0x8000cbb0]:csrrs a6, fcsr, zero<br> [0x8000cbb4]:sw t5, 608(ra)<br> [0x8000cbb8]:sw t6, 616(ra)<br> [0x8000cbbc]:sw t5, 624(ra)<br> [0x8000cbc0]:sw a6, 632(ra)<br>                                     |
| 443|[0x80013938]<br>0x00000000<br> [0x80013950]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000cbec]:fmul.d t5, t3, s10, dyn<br> [0x8000cbf0]:csrrs a6, fcsr, zero<br> [0x8000cbf4]:sw t5, 640(ra)<br> [0x8000cbf8]:sw t6, 648(ra)<br> [0x8000cbfc]:sw t5, 656(ra)<br> [0x8000cc00]:sw a6, 664(ra)<br>                                     |
| 444|[0x80013958]<br>0x00000000<br> [0x80013970]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000cc2c]:fmul.d t5, t3, s10, dyn<br> [0x8000cc30]:csrrs a6, fcsr, zero<br> [0x8000cc34]:sw t5, 672(ra)<br> [0x8000cc38]:sw t6, 680(ra)<br> [0x8000cc3c]:sw t5, 688(ra)<br> [0x8000cc40]:sw a6, 696(ra)<br>                                     |
| 445|[0x80013978]<br>0x00000000<br> [0x80013990]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000cc6c]:fmul.d t5, t3, s10, dyn<br> [0x8000cc70]:csrrs a6, fcsr, zero<br> [0x8000cc74]:sw t5, 704(ra)<br> [0x8000cc78]:sw t6, 712(ra)<br> [0x8000cc7c]:sw t5, 720(ra)<br> [0x8000cc80]:sw a6, 728(ra)<br>                                     |
| 446|[0x80013998]<br>0x00000000<br> [0x800139b0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000ccac]:fmul.d t5, t3, s10, dyn<br> [0x8000ccb0]:csrrs a6, fcsr, zero<br> [0x8000ccb4]:sw t5, 736(ra)<br> [0x8000ccb8]:sw t6, 744(ra)<br> [0x8000ccbc]:sw t5, 752(ra)<br> [0x8000ccc0]:sw a6, 760(ra)<br>                                     |
| 447|[0x800139b8]<br>0x00000000<br> [0x800139d0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000ccec]:fmul.d t5, t3, s10, dyn<br> [0x8000ccf0]:csrrs a6, fcsr, zero<br> [0x8000ccf4]:sw t5, 768(ra)<br> [0x8000ccf8]:sw t6, 776(ra)<br> [0x8000ccfc]:sw t5, 784(ra)<br> [0x8000cd00]:sw a6, 792(ra)<br>                                     |
| 448|[0x800139d8]<br>0x00000000<br> [0x800139f0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000cd2c]:fmul.d t5, t3, s10, dyn<br> [0x8000cd30]:csrrs a6, fcsr, zero<br> [0x8000cd34]:sw t5, 800(ra)<br> [0x8000cd38]:sw t6, 808(ra)<br> [0x8000cd3c]:sw t5, 816(ra)<br> [0x8000cd40]:sw a6, 824(ra)<br>                                     |
| 449|[0x800139f8]<br>0x00000000<br> [0x80013a10]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000cd6c]:fmul.d t5, t3, s10, dyn<br> [0x8000cd70]:csrrs a6, fcsr, zero<br> [0x8000cd74]:sw t5, 832(ra)<br> [0x8000cd78]:sw t6, 840(ra)<br> [0x8000cd7c]:sw t5, 848(ra)<br> [0x8000cd80]:sw a6, 856(ra)<br>                                     |
| 450|[0x80013a18]<br>0x00000000<br> [0x80013a30]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                                                      |[0x8000cdac]:fmul.d t5, t3, s10, dyn<br> [0x8000cdb0]:csrrs a6, fcsr, zero<br> [0x8000cdb4]:sw t5, 864(ra)<br> [0x8000cdb8]:sw t6, 872(ra)<br> [0x8000cdbc]:sw t5, 880(ra)<br> [0x8000cdc0]:sw a6, 888(ra)<br>                                     |
