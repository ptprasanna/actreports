
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800003c0')]      |
| SIG_REGION                | [('0x80002210', '0x800022a0', '36 words')]      |
| COV_LABELS                | fcvt.w.d_b27      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-fcvtwd/fcvt.w.d_b27-01.S/ref.S    |
| Total Number of coverpoints| 39     |
| Total Coverpoints Hit     | 39      |
| Total Signature Updates   | 23      |
| STAT1                     | 11      |
| STAT2                     | 0      |
| STAT3                     | 5     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x800002a4]:fcvt.w.d a2, a4, dyn
[0x800002a8]:csrrs tp, fcsr, zero
[0x800002ac]:sw a2, 144(ra)
[0x800002b0]:sw tp, 152(ra)
[0x800002b4]:lw fp, 80(gp)
[0x800002b8]:lw s1, 84(gp)
[0x800002bc]:addi fp, zero, 0
[0x800002c0]:addi s1, zero, 0
[0x800002c4]:addi sp, zero, 0
[0x800002c8]:csrrw zero, fcsr, sp
[0x800002cc]:fcvt.w.d a0, fp, dyn
[0x800002d0]:csrrs tp, fcsr, zero
[0x800002d4]:sw a0, 160(ra)
[0x800002d8]:sw tp, 168(ra)
[0x800002dc]:auipc a3, 2
[0x800002e0]:addi a3, a3, 3468
[0x800002e4]:lw a0, 88(a3)
[0x800002e8]:lw a1, 92(a3)
[0x800002ec]:addi a0, zero, 0
[0x800002f0]:addi a1, zero, 0
[0x800002f4]:addi a2, zero, 0
[0x800002f8]:csrrw zero, fcsr, a2
[0x800002fc]:fcvt.w.d fp, a0, dyn
[0x80000300]:csrrs a4, fcsr, zero
[0x80000304]:sw fp, 176(ra)
[0x80000308]:sw a4, 184(ra)
[0x8000030c]:lw tp, 96(a3)
[0x80000310]:lw t0, 100(a3)
[0x80000314]:addi tp, zero, 0
[0x80000318]:addi t0, zero, 0
[0x8000031c]:addi a2, zero, 0
[0x80000320]:csrrw zero, fcsr, a2
[0x80000324]:fcvt.w.d t1, tp, dyn
[0x80000328]:csrrs a4, fcsr, zero
[0x8000032c]:sw t1, 192(ra)
[0x80000330]:sw a4, 200(ra)
[0x80000334]:auipc ra, 2
[0x80000338]:addi ra, ra, 3916
[0x8000033c]:lw t1, 0(a3)
[0x80000340]:lw t2, 4(a3)
[0x80000344]:addi t1, zero, 0
[0x80000348]:addi t2, zero, 0
[0x8000034c]:addi a2, zero, 0
[0x80000350]:csrrw zero, fcsr, a2
[0x80000354]:fcvt.w.d tp, t1, dyn
[0x80000358]:csrrs a4, fcsr, zero
[0x8000035c]:sw tp, 0(ra)
[0x80000360]:sw a4, 8(ra)
[0x80000364]:lw sp, 8(a3)
[0x80000368]:lw gp, 12(a3)
[0x8000036c]:addi sp, zero, 0
[0x80000370]:addi gp, zero, 0
[0x80000374]:addi a2, zero, 0
[0x80000378]:csrrw zero, fcsr, a2
[0x8000037c]:fcvt.w.d t5, sp, dyn
[0x80000380]:csrrs a4, fcsr, zero
[0x80000384]:sw t5, 16(ra)
[0x80000388]:sw a4, 24(ra)
[0x8000038c]:lw t5, 16(a3)
[0x80000390]:lw t6, 20(a3)
[0x80000394]:addi t5, zero, 0
[0x80000398]:addi t6, zero, 0
[0x8000039c]:addi a2, zero, 0
[0x800003a0]:csrrw zero, fcsr, a2
[0x800003a4]:fcvt.w.d sp, t5, dyn
[0x800003a8]:csrrs a4, fcsr, zero
[0x800003ac]:sw sp, 32(ra)
[0x800003b0]:sw a4, 40(ra)
[0x800003b4]:addi zero, zero, 0
[0x800003b8]:addi zero, zero, 0
[0x800003bc]:addi zero, zero, 0

[0x800002cc]:fcvt.w.d a0, fp, dyn
[0x800002d0]:csrrs tp, fcsr, zero
[0x800002d4]:sw a0, 160(ra)
[0x800002d8]:sw tp, 168(ra)
[0x800002dc]:auipc a3, 2
[0x800002e0]:addi a3, a3, 3468
[0x800002e4]:lw a0, 88(a3)
[0x800002e8]:lw a1, 92(a3)
[0x800002ec]:addi a0, zero, 0
[0x800002f0]:addi a1, zero, 0
[0x800002f4]:addi a2, zero, 0
[0x800002f8]:csrrw zero, fcsr, a2
[0x800002fc]:fcvt.w.d fp, a0, dyn
[0x80000300]:csrrs a4, fcsr, zero
[0x80000304]:sw fp, 176(ra)
[0x80000308]:sw a4, 184(ra)
[0x8000030c]:lw tp, 96(a3)
[0x80000310]:lw t0, 100(a3)
[0x80000314]:addi tp, zero, 0
[0x80000318]:addi t0, zero, 0
[0x8000031c]:addi a2, zero, 0
[0x80000320]:csrrw zero, fcsr, a2
[0x80000324]:fcvt.w.d t1, tp, dyn
[0x80000328]:csrrs a4, fcsr, zero
[0x8000032c]:sw t1, 192(ra)
[0x80000330]:sw a4, 200(ra)
[0x80000334]:auipc ra, 2
[0x80000338]:addi ra, ra, 3916
[0x8000033c]:lw t1, 0(a3)
[0x80000340]:lw t2, 4(a3)
[0x80000344]:addi t1, zero, 0
[0x80000348]:addi t2, zero, 0
[0x8000034c]:addi a2, zero, 0
[0x80000350]:csrrw zero, fcsr, a2
[0x80000354]:fcvt.w.d tp, t1, dyn
[0x80000358]:csrrs a4, fcsr, zero
[0x8000035c]:sw tp, 0(ra)
[0x80000360]:sw a4, 8(ra)
[0x80000364]:lw sp, 8(a3)
[0x80000368]:lw gp, 12(a3)
[0x8000036c]:addi sp, zero, 0
[0x80000370]:addi gp, zero, 0
[0x80000374]:addi a2, zero, 0
[0x80000378]:csrrw zero, fcsr, a2
[0x8000037c]:fcvt.w.d t5, sp, dyn
[0x80000380]:csrrs a4, fcsr, zero
[0x80000384]:sw t5, 16(ra)
[0x80000388]:sw a4, 24(ra)
[0x8000038c]:lw t5, 16(a3)
[0x80000390]:lw t6, 20(a3)
[0x80000394]:addi t5, zero, 0
[0x80000398]:addi t6, zero, 0
[0x8000039c]:addi a2, zero, 0
[0x800003a0]:csrrw zero, fcsr, a2
[0x800003a4]:fcvt.w.d sp, t5, dyn
[0x800003a8]:csrrs a4, fcsr, zero
[0x800003ac]:sw sp, 32(ra)
[0x800003b0]:sw a4, 40(ra)
[0x800003b4]:addi zero, zero, 0
[0x800003b8]:addi zero, zero, 0
[0x800003bc]:addi zero, zero, 0

[0x800002fc]:fcvt.w.d fp, a0, dyn
[0x80000300]:csrrs a4, fcsr, zero
[0x80000304]:sw fp, 176(ra)
[0x80000308]:sw a4, 184(ra)
[0x8000030c]:lw tp, 96(a3)
[0x80000310]:lw t0, 100(a3)
[0x80000314]:addi tp, zero, 0
[0x80000318]:addi t0, zero, 0
[0x8000031c]:addi a2, zero, 0
[0x80000320]:csrrw zero, fcsr, a2
[0x80000324]:fcvt.w.d t1, tp, dyn
[0x80000328]:csrrs a4, fcsr, zero
[0x8000032c]:sw t1, 192(ra)
[0x80000330]:sw a4, 200(ra)
[0x80000334]:auipc ra, 2
[0x80000338]:addi ra, ra, 3916
[0x8000033c]:lw t1, 0(a3)
[0x80000340]:lw t2, 4(a3)
[0x80000344]:addi t1, zero, 0
[0x80000348]:addi t2, zero, 0
[0x8000034c]:addi a2, zero, 0
[0x80000350]:csrrw zero, fcsr, a2
[0x80000354]:fcvt.w.d tp, t1, dyn
[0x80000358]:csrrs a4, fcsr, zero
[0x8000035c]:sw tp, 0(ra)
[0x80000360]:sw a4, 8(ra)
[0x80000364]:lw sp, 8(a3)
[0x80000368]:lw gp, 12(a3)
[0x8000036c]:addi sp, zero, 0
[0x80000370]:addi gp, zero, 0
[0x80000374]:addi a2, zero, 0
[0x80000378]:csrrw zero, fcsr, a2
[0x8000037c]:fcvt.w.d t5, sp, dyn
[0x80000380]:csrrs a4, fcsr, zero
[0x80000384]:sw t5, 16(ra)
[0x80000388]:sw a4, 24(ra)
[0x8000038c]:lw t5, 16(a3)
[0x80000390]:lw t6, 20(a3)
[0x80000394]:addi t5, zero, 0
[0x80000398]:addi t6, zero, 0
[0x8000039c]:addi a2, zero, 0
[0x800003a0]:csrrw zero, fcsr, a2
[0x800003a4]:fcvt.w.d sp, t5, dyn
[0x800003a8]:csrrs a4, fcsr, zero
[0x800003ac]:sw sp, 32(ra)
[0x800003b0]:sw a4, 40(ra)
[0x800003b4]:addi zero, zero, 0
[0x800003b8]:addi zero, zero, 0
[0x800003bc]:addi zero, zero, 0

[0x80000324]:fcvt.w.d t1, tp, dyn
[0x80000328]:csrrs a4, fcsr, zero
[0x8000032c]:sw t1, 192(ra)
[0x80000330]:sw a4, 200(ra)
[0x80000334]:auipc ra, 2
[0x80000338]:addi ra, ra, 3916
[0x8000033c]:lw t1, 0(a3)
[0x80000340]:lw t2, 4(a3)
[0x80000344]:addi t1, zero, 0
[0x80000348]:addi t2, zero, 0
[0x8000034c]:addi a2, zero, 0
[0x80000350]:csrrw zero, fcsr, a2
[0x80000354]:fcvt.w.d tp, t1, dyn
[0x80000358]:csrrs a4, fcsr, zero
[0x8000035c]:sw tp, 0(ra)
[0x80000360]:sw a4, 8(ra)
[0x80000364]:lw sp, 8(a3)
[0x80000368]:lw gp, 12(a3)
[0x8000036c]:addi sp, zero, 0
[0x80000370]:addi gp, zero, 0
[0x80000374]:addi a2, zero, 0
[0x80000378]:csrrw zero, fcsr, a2
[0x8000037c]:fcvt.w.d t5, sp, dyn
[0x80000380]:csrrs a4, fcsr, zero
[0x80000384]:sw t5, 16(ra)
[0x80000388]:sw a4, 24(ra)
[0x8000038c]:lw t5, 16(a3)
[0x80000390]:lw t6, 20(a3)
[0x80000394]:addi t5, zero, 0
[0x80000398]:addi t6, zero, 0
[0x8000039c]:addi a2, zero, 0
[0x800003a0]:csrrw zero, fcsr, a2
[0x800003a4]:fcvt.w.d sp, t5, dyn
[0x800003a8]:csrrs a4, fcsr, zero
[0x800003ac]:sw sp, 32(ra)
[0x800003b0]:sw a4, 40(ra)
[0x800003b4]:addi zero, zero, 0
[0x800003b8]:addi zero, zero, 0
[0x800003bc]:addi zero, zero, 0

[0x800003a4]:fcvt.w.d sp, t5, dyn
[0x800003a8]:csrrs a4, fcsr, zero
[0x800003ac]:sw sp, 32(ra)
[0x800003b0]:sw a4, 40(ra)
[0x800003b4]:addi zero, zero, 0
[0x800003b8]:addi zero, zero, 0
[0x800003bc]:addi zero, zero, 0



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                          signature                           |                                                                         coverpoints                                                                          |                                                                    code                                                                    |
|---:|--------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002218]<br>0x7FFFFFFF<br> [0x80002220]<br>0x00000010<br> |- mnemonic : fcvt.w.d<br> - rs1 : x28<br> - rd : x30<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x8000012c]:fcvt.w.d t5, t3, dyn<br> [0x80000130]:csrrs tp, fcsr, zero<br> [0x80000134]:sw t5, 0(ra)<br> [0x80000138]:sw tp, 8(ra)<br>     |
|   2|[0x80002228]<br>0x7FFFFFFF<br> [0x80002230]<br>0x00000010<br> |- rs1 : x30<br> - rd : x28<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                           |[0x80000154]:fcvt.w.d t3, t5, dyn<br> [0x80000158]:csrrs tp, fcsr, zero<br> [0x8000015c]:sw t3, 16(ra)<br> [0x80000160]:sw tp, 24(ra)<br>   |
|   3|[0x80002238]<br>0x7FFFFFFF<br> [0x80002240]<br>0x00000010<br> |- rs1 : x24<br> - rd : x26<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x4aaaaaaaaaaaa and  fcsr == 0x0 and rm_val == 7   #nosat<br>                           |[0x80000184]:fcvt.w.d s10, s8, dyn<br> [0x80000188]:csrrs tp, fcsr, zero<br> [0x8000018c]:sw s10, 32(ra)<br> [0x80000190]:sw tp, 40(ra)<br> |
|   4|[0x80002248]<br>0x7FFFFFFF<br> [0x80002250]<br>0x00000010<br> |- rs1 : x26<br> - rd : x24<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0x4aaaaaaaaaaaa and  fcsr == 0x0 and rm_val == 7   #nosat<br>                           |[0x800001b4]:fcvt.w.d s8, s10, dyn<br> [0x800001b8]:csrrs tp, fcsr, zero<br> [0x800001bc]:sw s8, 48(ra)<br> [0x800001c0]:sw tp, 56(ra)<br>  |
|   5|[0x80002258]<br>0x7FFFFFFF<br> [0x80002260]<br>0x00000010<br> |- rs1 : x20<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                           |[0x800001dc]:fcvt.w.d s6, s4, dyn<br> [0x800001e0]:csrrs tp, fcsr, zero<br> [0x800001e4]:sw s6, 64(ra)<br> [0x800001e8]:sw tp, 72(ra)<br>   |
|   6|[0x80002268]<br>0x7FFFFFFF<br> [0x80002270]<br>0x00000010<br> |- rs1 : x22<br> - rd : x20<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                           |[0x80000204]:fcvt.w.d s4, s6, dyn<br> [0x80000208]:csrrs tp, fcsr, zero<br> [0x8000020c]:sw s4, 80(ra)<br> [0x80000210]:sw tp, 88(ra)<br>   |
|   7|[0x80002278]<br>0x7FFFFFFF<br> [0x80002280]<br>0x00000010<br> |- rs1 : x16<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0xc000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                           |[0x8000022c]:fcvt.w.d s2, a6, dyn<br> [0x80000230]:csrrs tp, fcsr, zero<br> [0x80000234]:sw s2, 96(ra)<br> [0x80000238]:sw tp, 104(ra)<br>  |
|   8|[0x80002288]<br>0x7FFFFFFF<br> [0x80002290]<br>0x00000010<br> |- rs1 : x18<br> - rd : x16<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0xc000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                           |[0x80000254]:fcvt.w.d a6, s2, dyn<br> [0x80000258]:csrrs tp, fcsr, zero<br> [0x8000025c]:sw a6, 112(ra)<br> [0x80000260]:sw tp, 120(ra)<br> |
|   9|[0x80002298]<br>0x00000000<br> [0x800022a0]<br>0x00000000<br> |- rs1 : x12<br> - rd : x14<br>                                                                                                                                |[0x8000027c]:fcvt.w.d a4, a2, dyn<br> [0x80000280]:csrrs tp, fcsr, zero<br> [0x80000284]:sw a4, 128(ra)<br> [0x80000288]:sw tp, 136(ra)<br> |
|  10|[0x80002280]<br>0x00000000<br> [0x80002288]<br>0x00000000<br> |- rs1 : x6<br> - rd : x4<br>                                                                                                                                  |[0x80000354]:fcvt.w.d tp, t1, dyn<br> [0x80000358]:csrrs a4, fcsr, zero<br> [0x8000035c]:sw tp, 0(ra)<br> [0x80000360]:sw a4, 8(ra)<br>     |
|  11|[0x80002290]<br>0x00000000<br> [0x80002298]<br>0x00000000<br> |- rs1 : x2<br>                                                                                                                                                |[0x8000037c]:fcvt.w.d t5, sp, dyn<br> [0x80000380]:csrrs a4, fcsr, zero<br> [0x80000384]:sw t5, 16(ra)<br> [0x80000388]:sw a4, 24(ra)<br>   |
