
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800008b0')]      |
| SIG_REGION                | [('0x80002310', '0x80002460', '84 words')]      |
| COV_LABELS                | fcvt.wu.d_b22      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-fcvtwud/fcvt.wu.d_b22-01.S/ref.S    |
| Total Number of coverpoints| 71     |
| Total Coverpoints Hit     | 71      |
| Total Signature Updates   | 55      |
| STAT1                     | 27      |
| STAT2                     | 0      |
| STAT3                     | 13     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000654]:fcvt.wu.d t5, t3, dyn
[0x80000658]:csrrs a4, fcsr, zero
[0x8000065c]:sw t5, 224(ra)
[0x80000660]:sw a4, 232(ra)
[0x80000664]:lw t3, 120(a3)
[0x80000668]:lw t4, 124(a3)
[0x8000066c]:lui t3, 432926
[0x80000670]:addi t3, t3, 3263
[0x80000674]:lui t4, 268349
[0x80000678]:addi t4, t4, 97
[0x8000067c]:addi a2, zero, 0
[0x80000680]:csrrw zero, fcsr, a2
[0x80000684]:fcvt.wu.d t5, t3, dyn
[0x80000688]:csrrs a4, fcsr, zero

[0x80000684]:fcvt.wu.d t5, t3, dyn
[0x80000688]:csrrs a4, fcsr, zero
[0x8000068c]:sw t5, 240(ra)
[0x80000690]:sw a4, 248(ra)
[0x80000694]:lw t3, 128(a3)
[0x80000698]:lw t4, 132(a3)
[0x8000069c]:lui t3, 33289
[0x800006a0]:addi t3, t3, 3337
[0x800006a4]:lui t4, 268671
[0x800006a8]:addi t4, t4, 534
[0x800006ac]:addi a2, zero, 0
[0x800006b0]:csrrw zero, fcsr, a2
[0x800006b4]:fcvt.wu.d t5, t3, dyn
[0x800006b8]:csrrs a4, fcsr, zero

[0x800006b4]:fcvt.wu.d t5, t3, dyn
[0x800006b8]:csrrs a4, fcsr, zero
[0x800006bc]:sw t5, 256(ra)
[0x800006c0]:sw a4, 264(ra)
[0x800006c4]:lw t3, 136(a3)
[0x800006c8]:lw t4, 140(a3)
[0x800006cc]:lui t3, 857717
[0x800006d0]:addi t3, t3, 826
[0x800006d4]:lui t4, 793243
[0x800006d8]:addi t4, t4, 1267
[0x800006dc]:addi a2, zero, 0
[0x800006e0]:csrrw zero, fcsr, a2
[0x800006e4]:fcvt.wu.d t5, t3, dyn
[0x800006e8]:csrrs a4, fcsr, zero

[0x800006e4]:fcvt.wu.d t5, t3, dyn
[0x800006e8]:csrrs a4, fcsr, zero
[0x800006ec]:sw t5, 272(ra)
[0x800006f0]:sw a4, 280(ra)
[0x800006f4]:lw t3, 144(a3)
[0x800006f8]:lw t4, 148(a3)
[0x800006fc]:lui t3, 75534
[0x80000700]:addi t3, t3, 3810
[0x80000704]:lui t4, 793481
[0x80000708]:addi t4, t4, 2342
[0x8000070c]:addi a2, zero, 0
[0x80000710]:csrrw zero, fcsr, a2
[0x80000714]:fcvt.wu.d t5, t3, dyn
[0x80000718]:csrrs a4, fcsr, zero

[0x80000714]:fcvt.wu.d t5, t3, dyn
[0x80000718]:csrrs a4, fcsr, zero
[0x8000071c]:sw t5, 288(ra)
[0x80000720]:sw a4, 296(ra)
[0x80000724]:lw t3, 152(a3)
[0x80000728]:lw t4, 156(a3)
[0x8000072c]:lui t3, 896664
[0x80000730]:addi t3, t3, 1364
[0x80000734]:lui t4, 269333
[0x80000738]:addi t4, t4, 2961
[0x8000073c]:addi a2, zero, 0
[0x80000740]:csrrw zero, fcsr, a2
[0x80000744]:fcvt.wu.d t5, t3, dyn
[0x80000748]:csrrs a4, fcsr, zero

[0x80000744]:fcvt.wu.d t5, t3, dyn
[0x80000748]:csrrs a4, fcsr, zero
[0x8000074c]:sw t5, 304(ra)
[0x80000750]:sw a4, 312(ra)
[0x80000754]:lw t3, 160(a3)
[0x80000758]:lw t4, 164(a3)
[0x8000075c]:lui t3, 403092
[0x80000760]:addi t3, t3, 1606
[0x80000764]:lui t4, 794001
[0x80000768]:addi t4, t4, 869
[0x8000076c]:addi a2, zero, 0
[0x80000770]:csrrw zero, fcsr, a2
[0x80000774]:fcvt.wu.d t5, t3, dyn
[0x80000778]:csrrs a4, fcsr, zero

[0x80000774]:fcvt.wu.d t5, t3, dyn
[0x80000778]:csrrs a4, fcsr, zero
[0x8000077c]:sw t5, 320(ra)
[0x80000780]:sw a4, 328(ra)
[0x80000784]:lw t3, 168(a3)
[0x80000788]:lw t4, 172(a3)
[0x8000078c]:lui t3, 392351
[0x80000790]:addi t3, t3, 2980
[0x80000794]:lui t4, 794346
[0x80000798]:addi t4, t4, 2942
[0x8000079c]:addi a2, zero, 0
[0x800007a0]:csrrw zero, fcsr, a2
[0x800007a4]:fcvt.wu.d t5, t3, dyn
[0x800007a8]:csrrs a4, fcsr, zero

[0x800007a4]:fcvt.wu.d t5, t3, dyn
[0x800007a8]:csrrs a4, fcsr, zero
[0x800007ac]:sw t5, 336(ra)
[0x800007b0]:sw a4, 344(ra)
[0x800007b4]:lw t3, 176(a3)
[0x800007b8]:lw t4, 180(a3)
[0x800007bc]:lui t3, 156932
[0x800007c0]:addi t3, t3, 2550
[0x800007c4]:lui t4, 794397
[0x800007c8]:addi t4, t4, 3712
[0x800007cc]:addi a2, zero, 0
[0x800007d0]:csrrw zero, fcsr, a2
[0x800007d4]:fcvt.wu.d t5, t3, dyn
[0x800007d8]:csrrs a4, fcsr, zero

[0x800007d4]:fcvt.wu.d t5, t3, dyn
[0x800007d8]:csrrs a4, fcsr, zero
[0x800007dc]:sw t5, 352(ra)
[0x800007e0]:sw a4, 360(ra)
[0x800007e4]:lw t3, 184(a3)
[0x800007e8]:lw t4, 188(a3)
[0x800007ec]:lui t3, 813184
[0x800007f0]:addi t3, t3, 7
[0x800007f4]:lui t4, 270534
[0x800007f8]:addi t4, t4, 3782
[0x800007fc]:addi a2, zero, 0
[0x80000800]:csrrw zero, fcsr, a2
[0x80000804]:fcvt.wu.d t5, t3, dyn
[0x80000808]:csrrs a4, fcsr, zero

[0x80000804]:fcvt.wu.d t5, t3, dyn
[0x80000808]:csrrs a4, fcsr, zero
[0x8000080c]:sw t5, 368(ra)
[0x80000810]:sw a4, 376(ra)
[0x80000814]:lw t3, 192(a3)
[0x80000818]:lw t4, 196(a3)
[0x8000081c]:lui t3, 463000
[0x80000820]:addi t3, t3, 2457
[0x80000824]:lui t4, 794923
[0x80000828]:addi t4, t4, 2413
[0x8000082c]:addi a2, zero, 0
[0x80000830]:csrrw zero, fcsr, a2
[0x80000834]:fcvt.wu.d t5, t3, dyn
[0x80000838]:csrrs a4, fcsr, zero

[0x80000834]:fcvt.wu.d t5, t3, dyn
[0x80000838]:csrrs a4, fcsr, zero
[0x8000083c]:sw t5, 384(ra)
[0x80000840]:sw a4, 392(ra)
[0x80000844]:lw t3, 200(a3)
[0x80000848]:lw t4, 204(a3)
[0x8000084c]:lui t3, 846672
[0x80000850]:addi t3, t3, 1782
[0x80000854]:lui t4, 248369
[0x80000858]:addi t4, t4, 3592
[0x8000085c]:addi a2, zero, 0
[0x80000860]:csrrw zero, fcsr, a2
[0x80000864]:fcvt.wu.d t5, t3, dyn
[0x80000868]:csrrs a4, fcsr, zero

[0x80000864]:fcvt.wu.d t5, t3, dyn
[0x80000868]:csrrs a4, fcsr, zero
[0x8000086c]:sw t5, 400(ra)
[0x80000870]:sw a4, 408(ra)
[0x80000874]:lw t3, 208(a3)
[0x80000878]:lw t4, 212(a3)
[0x8000087c]:lui t3, 454276
[0x80000880]:addi t3, t3, 624
[0x80000884]:lui t4, 379640
[0x80000888]:addi t4, t4, 1820
[0x8000088c]:addi a2, zero, 0
[0x80000890]:csrrw zero, fcsr, a2
[0x80000894]:fcvt.wu.d t5, t3, dyn
[0x80000898]:csrrs a4, fcsr, zero

[0x80000894]:fcvt.wu.d t5, t3, dyn
[0x80000898]:csrrs a4, fcsr, zero
[0x8000089c]:sw t5, 416(ra)
[0x800008a0]:sw a4, 424(ra)
[0x800008a4]:addi zero, zero, 0
[0x800008a8]:addi zero, zero, 0
[0x800008ac]:addi zero, zero, 0



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                          signature                           |                                                                          coverpoints                                                                          |                                                                    code                                                                     |
|---:|--------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002318]<br>0x00000000<br> [0x80002320]<br>0x00000001<br> |- mnemonic : fcvt.wu.d<br> - rs1 : x28<br> - rd : x30<br> - fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08577924770d3 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x80000134]:fcvt.wu.d t5, t3, dyn<br> [0x80000138]:csrrs tp, fcsr, zero<br> [0x8000013c]:sw t5, 0(ra)<br> [0x80000140]:sw tp, 8(ra)<br>     |
|   2|[0x80002328]<br>0x00000000<br> [0x80002330]<br>0x00000001<br> |- rs1 : x30<br> - rd : x28<br> - fs1 == 0 and fe1 == 0x3fd and fm1 == 0x93fdc7b89296c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                            |[0x80000164]:fcvt.wu.d t3, t5, dyn<br> [0x80000168]:csrrs tp, fcsr, zero<br> [0x8000016c]:sw t3, 16(ra)<br> [0x80000170]:sw tp, 24(ra)<br>   |
|   3|[0x80002338]<br>0x00000000<br> [0x80002340]<br>0x00000010<br> |- rs1 : x24<br> - rd : x26<br> - fs1 == 1 and fe1 == 0x3fe and fm1 == 0x766ba34c2da80 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                            |[0x80000194]:fcvt.wu.d s10, s8, dyn<br> [0x80000198]:csrrs tp, fcsr, zero<br> [0x8000019c]:sw s10, 32(ra)<br> [0x800001a0]:sw tp, 40(ra)<br> |
|   4|[0x80002348]<br>0x00000002<br> [0x80002350]<br>0x00000001<br> |- rs1 : x26<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0xd2d6b7dc59a3a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                            |[0x800001c4]:fcvt.wu.d s8, s10, dyn<br> [0x800001c8]:csrrs tp, fcsr, zero<br> [0x800001cc]:sw s8, 48(ra)<br> [0x800001d0]:sw tp, 56(ra)<br>  |
|   5|[0x80002358]<br>0x00000004<br> [0x80002360]<br>0x00000001<br> |- rs1 : x20<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x400 and fm1 == 0xcf84ba749f9c5 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                            |[0x800001f4]:fcvt.wu.d s6, s4, dyn<br> [0x800001f8]:csrrs tp, fcsr, zero<br> [0x800001fc]:sw s6, 64(ra)<br> [0x80000200]:sw tp, 72(ra)<br>   |
|   6|[0x80002368]<br>0x00000006<br> [0x80002370]<br>0x00000001<br> |- rs1 : x22<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x401 and fm1 == 0x854a908ceac39 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                            |[0x80000224]:fcvt.wu.d s4, s6, dyn<br> [0x80000228]:csrrs tp, fcsr, zero<br> [0x8000022c]:sw s4, 80(ra)<br> [0x80000230]:sw tp, 88(ra)<br>   |
|   7|[0x80002378]<br>0x00000009<br> [0x80002380]<br>0x00000001<br> |- rs1 : x16<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x402 and fm1 == 0x137a953e8eb43 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                            |[0x80000254]:fcvt.wu.d s2, a6, dyn<br> [0x80000258]:csrrs tp, fcsr, zero<br> [0x8000025c]:sw s2, 96(ra)<br> [0x80000260]:sw tp, 104(ra)<br>  |
|   8|[0x80002388]<br>0x00000000<br> [0x80002390]<br>0x00000010<br> |- rs1 : x18<br> - rd : x16<br> - fs1 == 1 and fe1 == 0x403 and fm1 == 0xf3ebcf3d06f86 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                            |[0x80000284]:fcvt.wu.d a6, s2, dyn<br> [0x80000288]:csrrs tp, fcsr, zero<br> [0x8000028c]:sw a6, 112(ra)<br> [0x80000290]:sw tp, 120(ra)<br> |
|   9|[0x80002398]<br>0x0000002C<br> [0x800023a0]<br>0x00000001<br> |- rs1 : x12<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x404 and fm1 == 0x5c74eff1e5bef and  fcsr == 0x0 and rm_val == 7   #nosat<br>                            |[0x800002b4]:fcvt.wu.d a4, a2, dyn<br> [0x800002b8]:csrrs tp, fcsr, zero<br> [0x800002bc]:sw a4, 128(ra)<br> [0x800002c0]:sw tp, 136(ra)<br> |
|  10|[0x800023a8]<br>0x00000077<br> [0x800023b0]<br>0x00000001<br> |- rs1 : x14<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x405 and fm1 == 0xdc3386b9f15c4 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                            |[0x800002e4]:fcvt.wu.d a2, a4, dyn<br> [0x800002e8]:csrrs tp, fcsr, zero<br> [0x800002ec]:sw a2, 144(ra)<br> [0x800002f0]:sw tp, 152(ra)<br> |
|  11|[0x800023b8]<br>0x000000AD<br> [0x800023c0]<br>0x00000001<br> |- rs1 : x8<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x406 and fm1 == 0x5ae6a9a6ab329 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                             |[0x80000314]:fcvt.wu.d a0, fp, dyn<br> [0x80000318]:csrrs tp, fcsr, zero<br> [0x8000031c]:sw a0, 160(ra)<br> [0x80000320]:sw tp, 168(ra)<br> |
|  12|[0x800023c8]<br>0x00000000<br> [0x800023d0]<br>0x00000010<br> |- rs1 : x10<br> - rd : x8<br> - fs1 == 1 and fe1 == 0x407 and fm1 == 0x489b36bd7f503 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                             |[0x8000034c]:fcvt.wu.d fp, a0, dyn<br> [0x80000350]:csrrs a4, fcsr, zero<br> [0x80000354]:sw fp, 176(ra)<br> [0x80000358]:sw a4, 184(ra)<br> |
|  13|[0x800023d8]<br>0x00000286<br> [0x800023e0]<br>0x00000001<br> |- rs1 : x4<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x408 and fm1 == 0x43277acca7f0d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                              |[0x8000037c]:fcvt.wu.d t1, tp, dyn<br> [0x80000380]:csrrs a4, fcsr, zero<br> [0x80000384]:sw t1, 192(ra)<br> [0x80000388]:sw a4, 200(ra)<br> |
|  14|[0x80002380]<br>0x000006BE<br> [0x80002388]<br>0x00000001<br> |- rs1 : x6<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x409 and fm1 == 0xaf9492cb7362c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                              |[0x800003b4]:fcvt.wu.d tp, t1, dyn<br> [0x800003b8]:csrrs a4, fcsr, zero<br> [0x800003bc]:sw tp, 0(ra)<br> [0x800003c0]:sw a4, 8(ra)<br>     |
|  15|[0x80002390]<br>0x00000AE7<br> [0x80002398]<br>0x00000001<br> |- rs1 : x2<br> - fs1 == 0 and fe1 == 0x40a and fm1 == 0x5cd28a96ec2b3 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x800003e4]:fcvt.wu.d t5, sp, dyn<br> [0x800003e8]:csrrs a4, fcsr, zero<br> [0x800003ec]:sw t5, 16(ra)<br> [0x800003f0]:sw a4, 24(ra)<br>   |
|  16|[0x800023a0]<br>0x00000000<br> [0x800023a8]<br>0x00000010<br> |- rd : x2<br> - fs1 == 1 and fe1 == 0x40b and fm1 == 0xc491074f942cb and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                             |[0x80000414]:fcvt.wu.d sp, t5, dyn<br> [0x80000418]:csrrs a4, fcsr, zero<br> [0x8000041c]:sw sp, 32(ra)<br> [0x80000420]:sw a4, 40(ra)<br>   |
|  17|[0x800023b0]<br>0x00000000<br> [0x800023b8]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x40c and fm1 == 0x3d480fb7f6f5d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                           |[0x80000444]:fcvt.wu.d t5, t3, dyn<br> [0x80000448]:csrrs a4, fcsr, zero<br> [0x8000044c]:sw t5, 48(ra)<br> [0x80000450]:sw a4, 56(ra)<br>   |
|  18|[0x800023c0]<br>0x00006741<br> [0x800023c8]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x40d and fm1 == 0x9d02f708cc1b6 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                           |[0x80000474]:fcvt.wu.d t5, t3, dyn<br> [0x80000478]:csrrs a4, fcsr, zero<br> [0x8000047c]:sw t5, 64(ra)<br> [0x80000480]:sw a4, 72(ra)<br>   |
|  19|[0x800023d0]<br>0x0000CA9E<br> [0x800023d8]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x40e and fm1 == 0x953b00b54aa22 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                           |[0x800004a4]:fcvt.wu.d t5, t3, dyn<br> [0x800004a8]:csrrs a4, fcsr, zero<br> [0x800004ac]:sw t5, 80(ra)<br> [0x800004b0]:sw a4, 88(ra)<br>   |
|  20|[0x800023e0]<br>0x0001224C<br> [0x800023e8]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x40f and fm1 == 0x224c03c53d0e3 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                           |[0x800004d4]:fcvt.wu.d t5, t3, dyn<br> [0x800004d8]:csrrs a4, fcsr, zero<br> [0x800004dc]:sw t5, 96(ra)<br> [0x800004e0]:sw a4, 104(ra)<br>  |
|  21|[0x800023f0]<br>0x0003D1B6<br> [0x800023f8]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x410 and fm1 == 0xe8dacf0e58650 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                           |[0x80000504]:fcvt.wu.d t5, t3, dyn<br> [0x80000508]:csrrs a4, fcsr, zero<br> [0x8000050c]:sw t5, 112(ra)<br> [0x80000510]:sw a4, 120(ra)<br> |
|  22|[0x80002400]<br>0x00000000<br> [0x80002408]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x411 and fm1 == 0x5dbbb894deab4 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                           |[0x80000534]:fcvt.wu.d t5, t3, dyn<br> [0x80000538]:csrrs a4, fcsr, zero<br> [0x8000053c]:sw t5, 128(ra)<br> [0x80000540]:sw a4, 136(ra)<br> |
|  23|[0x80002410]<br>0x0009EBE5<br> [0x80002418]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x412 and fm1 == 0x3d7c9e5f0307e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                           |[0x80000564]:fcvt.wu.d t5, t3, dyn<br> [0x80000568]:csrrs a4, fcsr, zero<br> [0x8000056c]:sw t5, 144(ra)<br> [0x80000570]:sw a4, 152(ra)<br> |
|  24|[0x80002420]<br>0x0018C8A2<br> [0x80002428]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x413 and fm1 == 0x8c8a1aaac3142 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                           |[0x80000594]:fcvt.wu.d t5, t3, dyn<br> [0x80000598]:csrrs a4, fcsr, zero<br> [0x8000059c]:sw t5, 160(ra)<br> [0x800005a0]:sw a4, 168(ra)<br> |
|  25|[0x80002430]<br>0x002F0A07<br> [0x80002438]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x414 and fm1 == 0x785036f9fb997 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                           |[0x800005c4]:fcvt.wu.d t5, t3, dyn<br> [0x800005c8]:csrrs a4, fcsr, zero<br> [0x800005cc]:sw t5, 176(ra)<br> [0x800005d0]:sw a4, 184(ra)<br> |
|  26|[0x80002440]<br>0x00656937<br> [0x80002448]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x415 and fm1 == 0x95a4da7298c66 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                           |[0x800005f4]:fcvt.wu.d t5, t3, dyn<br> [0x800005f8]:csrrs a4, fcsr, zero<br> [0x800005fc]:sw t5, 192(ra)<br> [0x80000600]:sw a4, 200(ra)<br> |
|  27|[0x80002450]<br>0x00C03ED7<br> [0x80002458]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x416 and fm1 == 0x807dad814d575 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                           |[0x80000624]:fcvt.wu.d t5, t3, dyn<br> [0x80000628]:csrrs a4, fcsr, zero<br> [0x8000062c]:sw t5, 208(ra)<br> [0x80000630]:sw a4, 216(ra)<br> |
