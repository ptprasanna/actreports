
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80001460')]      |
| SIG_REGION                | [('0x80003510', '0x80003870', '216 words')]      |
| COV_LABELS                | fcvt.wu.d_b24      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-fcvtwud/fcvt.wu.d_b24-01.S/ref.S    |
| Total Number of coverpoints| 136     |
| Total Coverpoints Hit     | 136      |
| Total Signature Updates   | 121      |
| STAT1                     | 60      |
| STAT2                     | 0      |
| STAT3                     | 45     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000c0c]:fcvt.wu.d t5, t3, dyn
[0x80000c10]:csrrs a4, fcsr, zero
[0x80000c14]:sw t5, 752(ra)
[0x80000c18]:sw a4, 760(ra)
[0x80000c1c]:lw t3, 384(a3)
[0x80000c20]:lw t4, 388(a3)
[0x80000c24]:lui t3, 796918
[0x80000c28]:addi t3, t3, 3113
[0x80000c2c]:lui t4, 261891
[0x80000c30]:addi t4, t4, 2293
[0x80000c34]:addi a2, zero, 32
[0x80000c38]:csrrw zero, fcsr, a2
[0x80000c3c]:fcvt.wu.d t5, t3, dyn
[0x80000c40]:csrrs a4, fcsr, zero

[0x80000c3c]:fcvt.wu.d t5, t3, dyn
[0x80000c40]:csrrs a4, fcsr, zero
[0x80000c44]:sw t5, 768(ra)
[0x80000c48]:sw a4, 776(ra)
[0x80000c4c]:lw t3, 392(a3)
[0x80000c50]:lw t4, 396(a3)
[0x80000c54]:lui t3, 796918
[0x80000c58]:addi t3, t3, 3113
[0x80000c5c]:lui t4, 261891
[0x80000c60]:addi t4, t4, 2293
[0x80000c64]:addi a2, zero, 64
[0x80000c68]:csrrw zero, fcsr, a2
[0x80000c6c]:fcvt.wu.d t5, t3, dyn
[0x80000c70]:csrrs a4, fcsr, zero

[0x80000c6c]:fcvt.wu.d t5, t3, dyn
[0x80000c70]:csrrs a4, fcsr, zero
[0x80000c74]:sw t5, 784(ra)
[0x80000c78]:sw a4, 792(ra)
[0x80000c7c]:lw t3, 400(a3)
[0x80000c80]:lw t4, 404(a3)
[0x80000c84]:lui t3, 796918
[0x80000c88]:addi t3, t3, 3113
[0x80000c8c]:lui t4, 261891
[0x80000c90]:addi t4, t4, 2293
[0x80000c94]:addi a2, zero, 96
[0x80000c98]:csrrw zero, fcsr, a2
[0x80000c9c]:fcvt.wu.d t5, t3, dyn
[0x80000ca0]:csrrs a4, fcsr, zero

[0x80000c9c]:fcvt.wu.d t5, t3, dyn
[0x80000ca0]:csrrs a4, fcsr, zero
[0x80000ca4]:sw t5, 800(ra)
[0x80000ca8]:sw a4, 808(ra)
[0x80000cac]:lw t3, 408(a3)
[0x80000cb0]:lw t4, 412(a3)
[0x80000cb4]:lui t3, 796918
[0x80000cb8]:addi t3, t3, 3113
[0x80000cbc]:lui t4, 261891
[0x80000cc0]:addi t4, t4, 2293
[0x80000cc4]:addi a2, zero, 128
[0x80000cc8]:csrrw zero, fcsr, a2
[0x80000ccc]:fcvt.wu.d t5, t3, dyn
[0x80000cd0]:csrrs a4, fcsr, zero

[0x80000ccc]:fcvt.wu.d t5, t3, dyn
[0x80000cd0]:csrrs a4, fcsr, zero
[0x80000cd4]:sw t5, 816(ra)
[0x80000cd8]:sw a4, 824(ra)
[0x80000cdc]:lw t3, 416(a3)
[0x80000ce0]:lw t4, 420(a3)
[0x80000ce4]:lui t3, 838861
[0x80000ce8]:addi t3, t3, 3277
[0x80000cec]:lui t4, 786125
[0x80000cf0]:addi t4, t4, 3276
[0x80000cf4]:addi a2, zero, 0
[0x80000cf8]:csrrw zero, fcsr, a2
[0x80000cfc]:fcvt.wu.d t5, t3, dyn
[0x80000d00]:csrrs a4, fcsr, zero

[0x80000cfc]:fcvt.wu.d t5, t3, dyn
[0x80000d00]:csrrs a4, fcsr, zero
[0x80000d04]:sw t5, 832(ra)
[0x80000d08]:sw a4, 840(ra)
[0x80000d0c]:lw t3, 424(a3)
[0x80000d10]:lw t4, 428(a3)
[0x80000d14]:lui t3, 838861
[0x80000d18]:addi t3, t3, 3277
[0x80000d1c]:lui t4, 786125
[0x80000d20]:addi t4, t4, 3276
[0x80000d24]:addi a2, zero, 32
[0x80000d28]:csrrw zero, fcsr, a2
[0x80000d2c]:fcvt.wu.d t5, t3, dyn
[0x80000d30]:csrrs a4, fcsr, zero

[0x80000d2c]:fcvt.wu.d t5, t3, dyn
[0x80000d30]:csrrs a4, fcsr, zero
[0x80000d34]:sw t5, 848(ra)
[0x80000d38]:sw a4, 856(ra)
[0x80000d3c]:lw t3, 432(a3)
[0x80000d40]:lw t4, 436(a3)
[0x80000d44]:lui t3, 838861
[0x80000d48]:addi t3, t3, 3277
[0x80000d4c]:lui t4, 786125
[0x80000d50]:addi t4, t4, 3276
[0x80000d54]:addi a2, zero, 64
[0x80000d58]:csrrw zero, fcsr, a2
[0x80000d5c]:fcvt.wu.d t5, t3, dyn
[0x80000d60]:csrrs a4, fcsr, zero

[0x80000d5c]:fcvt.wu.d t5, t3, dyn
[0x80000d60]:csrrs a4, fcsr, zero
[0x80000d64]:sw t5, 864(ra)
[0x80000d68]:sw a4, 872(ra)
[0x80000d6c]:lw t3, 440(a3)
[0x80000d70]:lw t4, 444(a3)
[0x80000d74]:lui t3, 838861
[0x80000d78]:addi t3, t3, 3277
[0x80000d7c]:lui t4, 786125
[0x80000d80]:addi t4, t4, 3276
[0x80000d84]:addi a2, zero, 96
[0x80000d88]:csrrw zero, fcsr, a2
[0x80000d8c]:fcvt.wu.d t5, t3, dyn
[0x80000d90]:csrrs a4, fcsr, zero

[0x80000d8c]:fcvt.wu.d t5, t3, dyn
[0x80000d90]:csrrs a4, fcsr, zero
[0x80000d94]:sw t5, 880(ra)
[0x80000d98]:sw a4, 888(ra)
[0x80000d9c]:lw t3, 448(a3)
[0x80000da0]:lw t4, 452(a3)
[0x80000da4]:lui t3, 838861
[0x80000da8]:addi t3, t3, 3277
[0x80000dac]:lui t4, 786125
[0x80000db0]:addi t4, t4, 3276
[0x80000db4]:addi a2, zero, 128
[0x80000db8]:csrrw zero, fcsr, a2
[0x80000dbc]:fcvt.wu.d t5, t3, dyn
[0x80000dc0]:csrrs a4, fcsr, zero

[0x80000dbc]:fcvt.wu.d t5, t3, dyn
[0x80000dc0]:csrrs a4, fcsr, zero
[0x80000dc4]:sw t5, 896(ra)
[0x80000dc8]:sw a4, 904(ra)
[0x80000dcc]:lw t3, 456(a3)
[0x80000dd0]:lw t4, 460(a3)
[0x80000dd4]:lui t3, 293601
[0x80000dd8]:addi t3, t3, 1147
[0x80000ddc]:lui t4, 784456
[0x80000de0]:addi t4, t4, 2785
[0x80000de4]:addi a2, zero, 0
[0x80000de8]:csrrw zero, fcsr, a2
[0x80000dec]:fcvt.wu.d t5, t3, dyn
[0x80000df0]:csrrs a4, fcsr, zero

[0x80000dec]:fcvt.wu.d t5, t3, dyn
[0x80000df0]:csrrs a4, fcsr, zero
[0x80000df4]:sw t5, 912(ra)
[0x80000df8]:sw a4, 920(ra)
[0x80000dfc]:lw t3, 464(a3)
[0x80000e00]:lw t4, 468(a3)
[0x80000e04]:lui t3, 293601
[0x80000e08]:addi t3, t3, 1147
[0x80000e0c]:lui t4, 784456
[0x80000e10]:addi t4, t4, 2785
[0x80000e14]:addi a2, zero, 32
[0x80000e18]:csrrw zero, fcsr, a2
[0x80000e1c]:fcvt.wu.d t5, t3, dyn
[0x80000e20]:csrrs a4, fcsr, zero

[0x80000e1c]:fcvt.wu.d t5, t3, dyn
[0x80000e20]:csrrs a4, fcsr, zero
[0x80000e24]:sw t5, 928(ra)
[0x80000e28]:sw a4, 936(ra)
[0x80000e2c]:lw t3, 472(a3)
[0x80000e30]:lw t4, 476(a3)
[0x80000e34]:lui t3, 293601
[0x80000e38]:addi t3, t3, 1147
[0x80000e3c]:lui t4, 784456
[0x80000e40]:addi t4, t4, 2785
[0x80000e44]:addi a2, zero, 64
[0x80000e48]:csrrw zero, fcsr, a2
[0x80000e4c]:fcvt.wu.d t5, t3, dyn
[0x80000e50]:csrrs a4, fcsr, zero

[0x80000e4c]:fcvt.wu.d t5, t3, dyn
[0x80000e50]:csrrs a4, fcsr, zero
[0x80000e54]:sw t5, 944(ra)
[0x80000e58]:sw a4, 952(ra)
[0x80000e5c]:lw t3, 480(a3)
[0x80000e60]:lw t4, 484(a3)
[0x80000e64]:lui t3, 293601
[0x80000e68]:addi t3, t3, 1147
[0x80000e6c]:lui t4, 784456
[0x80000e70]:addi t4, t4, 2785
[0x80000e74]:addi a2, zero, 96
[0x80000e78]:csrrw zero, fcsr, a2
[0x80000e7c]:fcvt.wu.d t5, t3, dyn
[0x80000e80]:csrrs a4, fcsr, zero

[0x80000e7c]:fcvt.wu.d t5, t3, dyn
[0x80000e80]:csrrs a4, fcsr, zero
[0x80000e84]:sw t5, 960(ra)
[0x80000e88]:sw a4, 968(ra)
[0x80000e8c]:lw t3, 488(a3)
[0x80000e90]:lw t4, 492(a3)
[0x80000e94]:lui t3, 293601
[0x80000e98]:addi t3, t3, 1147
[0x80000e9c]:lui t4, 784456
[0x80000ea0]:addi t4, t4, 2785
[0x80000ea4]:addi a2, zero, 128
[0x80000ea8]:csrrw zero, fcsr, a2
[0x80000eac]:fcvt.wu.d t5, t3, dyn
[0x80000eb0]:csrrs a4, fcsr, zero

[0x80000eac]:fcvt.wu.d t5, t3, dyn
[0x80000eb0]:csrrs a4, fcsr, zero
[0x80000eb4]:sw t5, 976(ra)
[0x80000eb8]:sw a4, 984(ra)
[0x80000ebc]:lw t3, 496(a3)
[0x80000ec0]:lw t4, 500(a3)
[0x80000ec4]:lui t3, 293601
[0x80000ec8]:addi t3, t3, 1147
[0x80000ecc]:lui t4, 261832
[0x80000ed0]:addi t4, t4, 2785
[0x80000ed4]:addi a2, zero, 0
[0x80000ed8]:csrrw zero, fcsr, a2
[0x80000edc]:fcvt.wu.d t5, t3, dyn
[0x80000ee0]:csrrs a4, fcsr, zero

[0x80000edc]:fcvt.wu.d t5, t3, dyn
[0x80000ee0]:csrrs a4, fcsr, zero
[0x80000ee4]:sw t5, 992(ra)
[0x80000ee8]:sw a4, 1000(ra)
[0x80000eec]:lw t3, 504(a3)
[0x80000ef0]:lw t4, 508(a3)
[0x80000ef4]:lui t3, 293601
[0x80000ef8]:addi t3, t3, 1147
[0x80000efc]:lui t4, 261832
[0x80000f00]:addi t4, t4, 2785
[0x80000f04]:addi a2, zero, 32
[0x80000f08]:csrrw zero, fcsr, a2
[0x80000f0c]:fcvt.wu.d t5, t3, dyn
[0x80000f10]:csrrs a4, fcsr, zero

[0x80000f0c]:fcvt.wu.d t5, t3, dyn
[0x80000f10]:csrrs a4, fcsr, zero
[0x80000f14]:sw t5, 1008(ra)
[0x80000f18]:sw a4, 1016(ra)
[0x80000f1c]:lw t3, 512(a3)
[0x80000f20]:lw t4, 516(a3)
[0x80000f24]:lui t3, 293601
[0x80000f28]:addi t3, t3, 1147
[0x80000f2c]:lui t4, 261832
[0x80000f30]:addi t4, t4, 2785
[0x80000f34]:addi a2, zero, 64
[0x80000f38]:csrrw zero, fcsr, a2
[0x80000f3c]:fcvt.wu.d t5, t3, dyn
[0x80000f40]:csrrs a4, fcsr, zero

[0x80000f3c]:fcvt.wu.d t5, t3, dyn
[0x80000f40]:csrrs a4, fcsr, zero
[0x80000f44]:sw t5, 1024(ra)
[0x80000f48]:sw a4, 1032(ra)
[0x80000f4c]:lw t3, 520(a3)
[0x80000f50]:lw t4, 524(a3)
[0x80000f54]:lui t3, 293601
[0x80000f58]:addi t3, t3, 1147
[0x80000f5c]:lui t4, 261832
[0x80000f60]:addi t4, t4, 2785
[0x80000f64]:addi a2, zero, 96
[0x80000f68]:csrrw zero, fcsr, a2
[0x80000f6c]:fcvt.wu.d t5, t3, dyn
[0x80000f70]:csrrs a4, fcsr, zero

[0x80000f6c]:fcvt.wu.d t5, t3, dyn
[0x80000f70]:csrrs a4, fcsr, zero
[0x80000f74]:sw t5, 1040(ra)
[0x80000f78]:sw a4, 1048(ra)
[0x80000f7c]:lw t3, 528(a3)
[0x80000f80]:lw t4, 532(a3)
[0x80000f84]:lui t3, 293601
[0x80000f88]:addi t3, t3, 1147
[0x80000f8c]:lui t4, 261832
[0x80000f90]:addi t4, t4, 2785
[0x80000f94]:addi a2, zero, 128
[0x80000f98]:csrrw zero, fcsr, a2
[0x80000f9c]:fcvt.wu.d t5, t3, dyn
[0x80000fa0]:csrrs a4, fcsr, zero

[0x80000f9c]:fcvt.wu.d t5, t3, dyn
[0x80000fa0]:csrrs a4, fcsr, zero
[0x80000fa4]:sw t5, 1056(ra)
[0x80000fa8]:sw a4, 1064(ra)
[0x80000fac]:lw t3, 536(a3)
[0x80000fb0]:lw t4, 540(a3)
[0x80000fb4]:lui t3, 838861
[0x80000fb8]:addi t3, t3, 3277
[0x80000fbc]:lui t4, 261837
[0x80000fc0]:addi t4, t4, 3276
[0x80000fc4]:addi a2, zero, 0
[0x80000fc8]:csrrw zero, fcsr, a2
[0x80000fcc]:fcvt.wu.d t5, t3, dyn
[0x80000fd0]:csrrs a4, fcsr, zero

[0x80000fcc]:fcvt.wu.d t5, t3, dyn
[0x80000fd0]:csrrs a4, fcsr, zero
[0x80000fd4]:sw t5, 1072(ra)
[0x80000fd8]:sw a4, 1080(ra)
[0x80000fdc]:lw t3, 544(a3)
[0x80000fe0]:lw t4, 548(a3)
[0x80000fe4]:lui t3, 838861
[0x80000fe8]:addi t3, t3, 3277
[0x80000fec]:lui t4, 261837
[0x80000ff0]:addi t4, t4, 3276
[0x80000ff4]:addi a2, zero, 32
[0x80000ff8]:csrrw zero, fcsr, a2
[0x80000ffc]:fcvt.wu.d t5, t3, dyn
[0x80001000]:csrrs a4, fcsr, zero

[0x80000ffc]:fcvt.wu.d t5, t3, dyn
[0x80001000]:csrrs a4, fcsr, zero
[0x80001004]:sw t5, 1088(ra)
[0x80001008]:sw a4, 1096(ra)
[0x8000100c]:lw t3, 552(a3)
[0x80001010]:lw t4, 556(a3)
[0x80001014]:lui t3, 838861
[0x80001018]:addi t3, t3, 3277
[0x8000101c]:lui t4, 261837
[0x80001020]:addi t4, t4, 3276
[0x80001024]:addi a2, zero, 64
[0x80001028]:csrrw zero, fcsr, a2
[0x8000102c]:fcvt.wu.d t5, t3, dyn
[0x80001030]:csrrs a4, fcsr, zero

[0x8000102c]:fcvt.wu.d t5, t3, dyn
[0x80001030]:csrrs a4, fcsr, zero
[0x80001034]:sw t5, 1104(ra)
[0x80001038]:sw a4, 1112(ra)
[0x8000103c]:lw t3, 560(a3)
[0x80001040]:lw t4, 564(a3)
[0x80001044]:lui t3, 838861
[0x80001048]:addi t3, t3, 3277
[0x8000104c]:lui t4, 261837
[0x80001050]:addi t4, t4, 3276
[0x80001054]:addi a2, zero, 96
[0x80001058]:csrrw zero, fcsr, a2
[0x8000105c]:fcvt.wu.d t5, t3, dyn
[0x80001060]:csrrs a4, fcsr, zero

[0x8000105c]:fcvt.wu.d t5, t3, dyn
[0x80001060]:csrrs a4, fcsr, zero
[0x80001064]:sw t5, 1120(ra)
[0x80001068]:sw a4, 1128(ra)
[0x8000106c]:lw t3, 568(a3)
[0x80001070]:lw t4, 572(a3)
[0x80001074]:lui t3, 838861
[0x80001078]:addi t3, t3, 3277
[0x8000107c]:lui t4, 261837
[0x80001080]:addi t4, t4, 3276
[0x80001084]:addi a2, zero, 128
[0x80001088]:csrrw zero, fcsr, a2
[0x8000108c]:fcvt.wu.d t5, t3, dyn
[0x80001090]:csrrs a4, fcsr, zero

[0x8000108c]:fcvt.wu.d t5, t3, dyn
[0x80001090]:csrrs a4, fcsr, zero
[0x80001094]:sw t5, 1136(ra)
[0x80001098]:sw a4, 1144(ra)
[0x8000109c]:lw t3, 576(a3)
[0x800010a0]:lw t4, 580(a3)
[0x800010a4]:lui t3, 629146
[0x800010a8]:addi t3, t3, 2458
[0x800010ac]:lui t4, 261018
[0x800010b0]:addi t4, t4, 2457
[0x800010b4]:addi a2, zero, 0
[0x800010b8]:csrrw zero, fcsr, a2
[0x800010bc]:fcvt.wu.d t5, t3, dyn
[0x800010c0]:csrrs a4, fcsr, zero

[0x800010bc]:fcvt.wu.d t5, t3, dyn
[0x800010c0]:csrrs a4, fcsr, zero
[0x800010c4]:sw t5, 1152(ra)
[0x800010c8]:sw a4, 1160(ra)
[0x800010cc]:lw t3, 584(a3)
[0x800010d0]:lw t4, 588(a3)
[0x800010d4]:lui t3, 629146
[0x800010d8]:addi t3, t3, 2458
[0x800010dc]:lui t4, 261018
[0x800010e0]:addi t4, t4, 2457
[0x800010e4]:addi a2, zero, 32
[0x800010e8]:csrrw zero, fcsr, a2
[0x800010ec]:fcvt.wu.d t5, t3, dyn
[0x800010f0]:csrrs a4, fcsr, zero

[0x800010ec]:fcvt.wu.d t5, t3, dyn
[0x800010f0]:csrrs a4, fcsr, zero
[0x800010f4]:sw t5, 1168(ra)
[0x800010f8]:sw a4, 1176(ra)
[0x800010fc]:lw t3, 592(a3)
[0x80001100]:lw t4, 596(a3)
[0x80001104]:lui t3, 629146
[0x80001108]:addi t3, t3, 2458
[0x8000110c]:lui t4, 261018
[0x80001110]:addi t4, t4, 2457
[0x80001114]:addi a2, zero, 64
[0x80001118]:csrrw zero, fcsr, a2
[0x8000111c]:fcvt.wu.d t5, t3, dyn
[0x80001120]:csrrs a4, fcsr, zero

[0x8000111c]:fcvt.wu.d t5, t3, dyn
[0x80001120]:csrrs a4, fcsr, zero
[0x80001124]:sw t5, 1184(ra)
[0x80001128]:sw a4, 1192(ra)
[0x8000112c]:lw t3, 600(a3)
[0x80001130]:lw t4, 604(a3)
[0x80001134]:lui t3, 629146
[0x80001138]:addi t3, t3, 2458
[0x8000113c]:lui t4, 261018
[0x80001140]:addi t4, t4, 2457
[0x80001144]:addi a2, zero, 96
[0x80001148]:csrrw zero, fcsr, a2
[0x8000114c]:fcvt.wu.d t5, t3, dyn
[0x80001150]:csrrs a4, fcsr, zero

[0x8000114c]:fcvt.wu.d t5, t3, dyn
[0x80001150]:csrrs a4, fcsr, zero
[0x80001154]:sw t5, 1200(ra)
[0x80001158]:sw a4, 1208(ra)
[0x8000115c]:lw t3, 608(a3)
[0x80001160]:lw t4, 612(a3)
[0x80001164]:lui t3, 629146
[0x80001168]:addi t3, t3, 2458
[0x8000116c]:lui t4, 261018
[0x80001170]:addi t4, t4, 2457
[0x80001174]:addi a2, zero, 128
[0x80001178]:csrrw zero, fcsr, a2
[0x8000117c]:fcvt.wu.d t5, t3, dyn
[0x80001180]:csrrs a4, fcsr, zero

[0x8000117c]:fcvt.wu.d t5, t3, dyn
[0x80001180]:csrrs a4, fcsr, zero
[0x80001184]:sw t5, 1216(ra)
[0x80001188]:sw a4, 1224(ra)
[0x8000118c]:lw t3, 616(a3)
[0x80001190]:lw t4, 620(a3)
[0x80001194]:lui t3, 629146
[0x80001198]:addi t3, t3, 2458
[0x8000119c]:lui t4, 261914
[0x800011a0]:addi t4, t4, 2457
[0x800011a4]:addi a2, zero, 0
[0x800011a8]:csrrw zero, fcsr, a2
[0x800011ac]:fcvt.wu.d t5, t3, dyn
[0x800011b0]:csrrs a4, fcsr, zero

[0x800011ac]:fcvt.wu.d t5, t3, dyn
[0x800011b0]:csrrs a4, fcsr, zero
[0x800011b4]:sw t5, 1232(ra)
[0x800011b8]:sw a4, 1240(ra)
[0x800011bc]:lw t3, 624(a3)
[0x800011c0]:lw t4, 628(a3)
[0x800011c4]:lui t3, 629146
[0x800011c8]:addi t3, t3, 2458
[0x800011cc]:lui t4, 261914
[0x800011d0]:addi t4, t4, 2457
[0x800011d4]:addi a2, zero, 32
[0x800011d8]:csrrw zero, fcsr, a2
[0x800011dc]:fcvt.wu.d t5, t3, dyn
[0x800011e0]:csrrs a4, fcsr, zero

[0x800011dc]:fcvt.wu.d t5, t3, dyn
[0x800011e0]:csrrs a4, fcsr, zero
[0x800011e4]:sw t5, 1248(ra)
[0x800011e8]:sw a4, 1256(ra)
[0x800011ec]:lw t3, 632(a3)
[0x800011f0]:lw t4, 636(a3)
[0x800011f4]:lui t3, 629146
[0x800011f8]:addi t3, t3, 2458
[0x800011fc]:lui t4, 261914
[0x80001200]:addi t4, t4, 2457
[0x80001204]:addi a2, zero, 64
[0x80001208]:csrrw zero, fcsr, a2
[0x8000120c]:fcvt.wu.d t5, t3, dyn
[0x80001210]:csrrs a4, fcsr, zero

[0x8000120c]:fcvt.wu.d t5, t3, dyn
[0x80001210]:csrrs a4, fcsr, zero
[0x80001214]:sw t5, 1264(ra)
[0x80001218]:sw a4, 1272(ra)
[0x8000121c]:lw t3, 640(a3)
[0x80001220]:lw t4, 644(a3)
[0x80001224]:lui t3, 629146
[0x80001228]:addi t3, t3, 2458
[0x8000122c]:lui t4, 261914
[0x80001230]:addi t4, t4, 2457
[0x80001234]:addi a2, zero, 96
[0x80001238]:csrrw zero, fcsr, a2
[0x8000123c]:fcvt.wu.d t5, t3, dyn
[0x80001240]:csrrs a4, fcsr, zero

[0x8000123c]:fcvt.wu.d t5, t3, dyn
[0x80001240]:csrrs a4, fcsr, zero
[0x80001244]:sw t5, 1280(ra)
[0x80001248]:sw a4, 1288(ra)
[0x8000124c]:lw t3, 648(a3)
[0x80001250]:lw t4, 652(a3)
[0x80001254]:lui t3, 629146
[0x80001258]:addi t3, t3, 2458
[0x8000125c]:lui t4, 261914
[0x80001260]:addi t4, t4, 2457
[0x80001264]:addi a2, zero, 128
[0x80001268]:csrrw zero, fcsr, a2
[0x8000126c]:fcvt.wu.d t5, t3, dyn
[0x80001270]:csrrs a4, fcsr, zero

[0x8000126c]:fcvt.wu.d t5, t3, dyn
[0x80001270]:csrrs a4, fcsr, zero
[0x80001274]:sw t5, 1296(ra)
[0x80001278]:sw a4, 1304(ra)
[0x8000127c]:lw t3, 656(a3)
[0x80001280]:lw t4, 660(a3)
[0x80001284]:lui t3, 503316
[0x80001288]:addi t3, t3, 1966
[0x8000128c]:lui t4, 261883
[0x80001290]:addi t4, t4, 3604
[0x80001294]:addi a2, zero, 0
[0x80001298]:csrrw zero, fcsr, a2
[0x8000129c]:fcvt.wu.d t5, t3, dyn
[0x800012a0]:csrrs a4, fcsr, zero

[0x8000129c]:fcvt.wu.d t5, t3, dyn
[0x800012a0]:csrrs a4, fcsr, zero
[0x800012a4]:sw t5, 1312(ra)
[0x800012a8]:sw a4, 1320(ra)
[0x800012ac]:lw t3, 664(a3)
[0x800012b0]:lw t4, 668(a3)
[0x800012b4]:lui t3, 503316
[0x800012b8]:addi t3, t3, 1966
[0x800012bc]:lui t4, 261883
[0x800012c0]:addi t4, t4, 3604
[0x800012c4]:addi a2, zero, 32
[0x800012c8]:csrrw zero, fcsr, a2
[0x800012cc]:fcvt.wu.d t5, t3, dyn
[0x800012d0]:csrrs a4, fcsr, zero

[0x800012cc]:fcvt.wu.d t5, t3, dyn
[0x800012d0]:csrrs a4, fcsr, zero
[0x800012d4]:sw t5, 1328(ra)
[0x800012d8]:sw a4, 1336(ra)
[0x800012dc]:lw t3, 672(a3)
[0x800012e0]:lw t4, 676(a3)
[0x800012e4]:lui t3, 503316
[0x800012e8]:addi t3, t3, 1966
[0x800012ec]:lui t4, 261883
[0x800012f0]:addi t4, t4, 3604
[0x800012f4]:addi a2, zero, 64
[0x800012f8]:csrrw zero, fcsr, a2
[0x800012fc]:fcvt.wu.d t5, t3, dyn
[0x80001300]:csrrs a4, fcsr, zero

[0x800012fc]:fcvt.wu.d t5, t3, dyn
[0x80001300]:csrrs a4, fcsr, zero
[0x80001304]:sw t5, 1344(ra)
[0x80001308]:sw a4, 1352(ra)
[0x8000130c]:lw t3, 680(a3)
[0x80001310]:lw t4, 684(a3)
[0x80001314]:lui t3, 503316
[0x80001318]:addi t3, t3, 1966
[0x8000131c]:lui t4, 261883
[0x80001320]:addi t4, t4, 3604
[0x80001324]:addi a2, zero, 96
[0x80001328]:csrrw zero, fcsr, a2
[0x8000132c]:fcvt.wu.d t5, t3, dyn
[0x80001330]:csrrs a4, fcsr, zero

[0x8000132c]:fcvt.wu.d t5, t3, dyn
[0x80001330]:csrrs a4, fcsr, zero
[0x80001334]:sw t5, 1360(ra)
[0x80001338]:sw a4, 1368(ra)
[0x8000133c]:lw t3, 688(a3)
[0x80001340]:lw t4, 692(a3)
[0x80001344]:lui t3, 503316
[0x80001348]:addi t3, t3, 1966
[0x8000134c]:lui t4, 261883
[0x80001350]:addi t4, t4, 3604
[0x80001354]:addi a2, zero, 128
[0x80001358]:csrrw zero, fcsr, a2
[0x8000135c]:fcvt.wu.d t5, t3, dyn
[0x80001360]:csrrs a4, fcsr, zero

[0x8000135c]:fcvt.wu.d t5, t3, dyn
[0x80001360]:csrrs a4, fcsr, zero
[0x80001364]:sw t5, 1376(ra)
[0x80001368]:sw a4, 1384(ra)
[0x8000136c]:lw t3, 696(a3)
[0x80001370]:lw t4, 700(a3)
[0x80001374]:lui t3, 796918
[0x80001378]:addi t3, t3, 3113
[0x8000137c]:lui t4, 786179
[0x80001380]:addi t4, t4, 2293
[0x80001384]:addi a2, zero, 0
[0x80001388]:csrrw zero, fcsr, a2
[0x8000138c]:fcvt.wu.d t5, t3, dyn
[0x80001390]:csrrs a4, fcsr, zero

[0x8000138c]:fcvt.wu.d t5, t3, dyn
[0x80001390]:csrrs a4, fcsr, zero
[0x80001394]:sw t5, 1392(ra)
[0x80001398]:sw a4, 1400(ra)
[0x8000139c]:lw t3, 704(a3)
[0x800013a0]:lw t4, 708(a3)
[0x800013a4]:lui t3, 796918
[0x800013a8]:addi t3, t3, 3113
[0x800013ac]:lui t4, 786179
[0x800013b0]:addi t4, t4, 2293
[0x800013b4]:addi a2, zero, 32
[0x800013b8]:csrrw zero, fcsr, a2
[0x800013bc]:fcvt.wu.d t5, t3, dyn
[0x800013c0]:csrrs a4, fcsr, zero

[0x800013bc]:fcvt.wu.d t5, t3, dyn
[0x800013c0]:csrrs a4, fcsr, zero
[0x800013c4]:sw t5, 1408(ra)
[0x800013c8]:sw a4, 1416(ra)
[0x800013cc]:lw t3, 712(a3)
[0x800013d0]:lw t4, 716(a3)
[0x800013d4]:lui t3, 796918
[0x800013d8]:addi t3, t3, 3113
[0x800013dc]:lui t4, 786179
[0x800013e0]:addi t4, t4, 2293
[0x800013e4]:addi a2, zero, 64
[0x800013e8]:csrrw zero, fcsr, a2
[0x800013ec]:fcvt.wu.d t5, t3, dyn
[0x800013f0]:csrrs a4, fcsr, zero

[0x800013ec]:fcvt.wu.d t5, t3, dyn
[0x800013f0]:csrrs a4, fcsr, zero
[0x800013f4]:sw t5, 1424(ra)
[0x800013f8]:sw a4, 1432(ra)
[0x800013fc]:lw t3, 720(a3)
[0x80001400]:lw t4, 724(a3)
[0x80001404]:lui t3, 796918
[0x80001408]:addi t3, t3, 3113
[0x8000140c]:lui t4, 786179
[0x80001410]:addi t4, t4, 2293
[0x80001414]:addi a2, zero, 96
[0x80001418]:csrrw zero, fcsr, a2
[0x8000141c]:fcvt.wu.d t5, t3, dyn
[0x80001420]:csrrs a4, fcsr, zero

[0x8000141c]:fcvt.wu.d t5, t3, dyn
[0x80001420]:csrrs a4, fcsr, zero
[0x80001424]:sw t5, 1440(ra)
[0x80001428]:sw a4, 1448(ra)
[0x8000142c]:lw t3, 728(a3)
[0x80001430]:lw t4, 732(a3)
[0x80001434]:lui t3, 796918
[0x80001438]:addi t3, t3, 3113
[0x8000143c]:lui t4, 786179
[0x80001440]:addi t4, t4, 2293
[0x80001444]:addi a2, zero, 128
[0x80001448]:csrrw zero, fcsr, a2
[0x8000144c]:fcvt.wu.d t5, t3, dyn
[0x80001450]:csrrs a4, fcsr, zero

[0x8000144c]:fcvt.wu.d t5, t3, dyn
[0x80001450]:csrrs a4, fcsr, zero
[0x80001454]:sw t5, 1456(ra)
[0x80001458]:sw a4, 1464(ra)
[0x8000145c]:addi zero, zero, 0



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                          signature                           |                                                                          coverpoints                                                                          |                                                                    code                                                                     |
|---:|--------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80003518]<br>0x00000000<br> [0x80003520]<br>0x00000001<br> |- mnemonic : fcvt.wu.d<br> - rs1 : x28<br> - rd : x30<br> - fs1 == 0 and fe1 == 0x3fb and fm1 == 0xc28f5c28f5c29 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x80000134]:fcvt.wu.d t5, t3, dyn<br> [0x80000138]:csrrs tp, fcsr, zero<br> [0x8000013c]:sw t5, 0(ra)<br> [0x80000140]:sw tp, 8(ra)<br>     |
|   2|[0x80003528]<br>0x00000000<br> [0x80003530]<br>0x00000021<br> |- rs1 : x30<br> - rd : x28<br> - fs1 == 0 and fe1 == 0x3fb and fm1 == 0xc28f5c28f5c29 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                           |[0x80000164]:fcvt.wu.d t3, t5, dyn<br> [0x80000168]:csrrs tp, fcsr, zero<br> [0x8000016c]:sw t3, 16(ra)<br> [0x80000170]:sw tp, 24(ra)<br>   |
|   3|[0x80003538]<br>0x00000000<br> [0x80003540]<br>0x00000041<br> |- rs1 : x24<br> - rd : x26<br> - fs1 == 0 and fe1 == 0x3fb and fm1 == 0xc28f5c28f5c29 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                           |[0x80000194]:fcvt.wu.d s10, s8, dyn<br> [0x80000198]:csrrs tp, fcsr, zero<br> [0x8000019c]:sw s10, 32(ra)<br> [0x800001a0]:sw tp, 40(ra)<br> |
|   4|[0x80003548]<br>0x00000001<br> [0x80003550]<br>0x00000061<br> |- rs1 : x26<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x3fb and fm1 == 0xc28f5c28f5c29 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                           |[0x800001c4]:fcvt.wu.d s8, s10, dyn<br> [0x800001c8]:csrrs tp, fcsr, zero<br> [0x800001cc]:sw s8, 48(ra)<br> [0x800001d0]:sw tp, 56(ra)<br>  |
|   5|[0x80003558]<br>0x00000000<br> [0x80003560]<br>0x00000081<br> |- rs1 : x20<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x3fb and fm1 == 0xc28f5c28f5c29 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                           |[0x800001f4]:fcvt.wu.d s6, s4, dyn<br> [0x800001f8]:csrrs tp, fcsr, zero<br> [0x800001fc]:sw s6, 64(ra)<br> [0x80000200]:sw tp, 72(ra)<br>   |
|   6|[0x80003568]<br>0x00000001<br> [0x80003570]<br>0x00000000<br> |- rs1 : x22<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                            |[0x8000021c]:fcvt.wu.d s4, s6, dyn<br> [0x80000220]:csrrs tp, fcsr, zero<br> [0x80000224]:sw s4, 80(ra)<br> [0x80000228]:sw tp, 88(ra)<br>   |
|   7|[0x80003578]<br>0x00000001<br> [0x80003580]<br>0x00000020<br> |- rs1 : x16<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                           |[0x80000244]:fcvt.wu.d s2, a6, dyn<br> [0x80000248]:csrrs tp, fcsr, zero<br> [0x8000024c]:sw s2, 96(ra)<br> [0x80000250]:sw tp, 104(ra)<br>  |
|   8|[0x80003588]<br>0x00000001<br> [0x80003590]<br>0x00000040<br> |- rs1 : x18<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                           |[0x8000026c]:fcvt.wu.d a6, s2, dyn<br> [0x80000270]:csrrs tp, fcsr, zero<br> [0x80000274]:sw a6, 112(ra)<br> [0x80000278]:sw tp, 120(ra)<br> |
|   9|[0x80003598]<br>0x00000001<br> [0x800035a0]<br>0x00000060<br> |- rs1 : x12<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                           |[0x80000294]:fcvt.wu.d a4, a2, dyn<br> [0x80000298]:csrrs tp, fcsr, zero<br> [0x8000029c]:sw a4, 128(ra)<br> [0x800002a0]:sw tp, 136(ra)<br> |
|  10|[0x800035a8]<br>0x00000001<br> [0x800035b0]<br>0x00000080<br> |- rs1 : x14<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                           |[0x800002bc]:fcvt.wu.d a2, a4, dyn<br> [0x800002c0]:csrrs tp, fcsr, zero<br> [0x800002c4]:sw a2, 144(ra)<br> [0x800002c8]:sw tp, 152(ra)<br> |
|  11|[0x800035b8]<br>0x00000000<br> [0x800035c0]<br>0x00000001<br> |- rs1 : x8<br> - rd : x10<br> - fs1 == 1 and fe1 == 0x3fb and fm1 == 0x999999999999a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                             |[0x800002ec]:fcvt.wu.d a0, fp, dyn<br> [0x800002f0]:csrrs tp, fcsr, zero<br> [0x800002f4]:sw a0, 160(ra)<br> [0x800002f8]:sw tp, 168(ra)<br> |
|  12|[0x800035c8]<br>0x00000000<br> [0x800035d0]<br>0x00000021<br> |- rs1 : x10<br> - rd : x8<br> - fs1 == 1 and fe1 == 0x3fb and fm1 == 0x999999999999a and  fcsr == 0x20 and rm_val == 7   #nosat<br>                            |[0x80000324]:fcvt.wu.d fp, a0, dyn<br> [0x80000328]:csrrs a4, fcsr, zero<br> [0x8000032c]:sw fp, 176(ra)<br> [0x80000330]:sw a4, 184(ra)<br> |
|  13|[0x800035d8]<br>0x00000000<br> [0x800035e0]<br>0x00000050<br> |- rs1 : x4<br> - rd : x6<br> - fs1 == 1 and fe1 == 0x3fb and fm1 == 0x999999999999a and  fcsr == 0x40 and rm_val == 7   #nosat<br>                             |[0x80000354]:fcvt.wu.d t1, tp, dyn<br> [0x80000358]:csrrs a4, fcsr, zero<br> [0x8000035c]:sw t1, 192(ra)<br> [0x80000360]:sw a4, 200(ra)<br> |
|  14|[0x80003580]<br>0x00000000<br> [0x80003588]<br>0x00000061<br> |- rs1 : x6<br> - rd : x4<br> - fs1 == 1 and fe1 == 0x3fb and fm1 == 0x999999999999a and  fcsr == 0x60 and rm_val == 7   #nosat<br>                             |[0x8000038c]:fcvt.wu.d tp, t1, dyn<br> [0x80000390]:csrrs a4, fcsr, zero<br> [0x80000394]:sw tp, 0(ra)<br> [0x80000398]:sw a4, 8(ra)<br>     |
|  15|[0x80003590]<br>0x00000000<br> [0x80003598]<br>0x00000081<br> |- rs1 : x2<br> - fs1 == 1 and fe1 == 0x3fb and fm1 == 0x999999999999a and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                           |[0x800003bc]:fcvt.wu.d t5, sp, dyn<br> [0x800003c0]:csrrs a4, fcsr, zero<br> [0x800003c4]:sw t5, 16(ra)<br> [0x800003c8]:sw a4, 24(ra)<br>   |
|  16|[0x800035a0]<br>0x00000000<br> [0x800035a8]<br>0x00000010<br> |- rd : x2<br> - fs1 == 1 and fe1 == 0x3fe and fm1 == 0xfae147ae147ae and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                             |[0x800003ec]:fcvt.wu.d sp, t5, dyn<br> [0x800003f0]:csrrs a4, fcsr, zero<br> [0x800003f4]:sw sp, 32(ra)<br> [0x800003f8]:sw a4, 40(ra)<br>   |
|  17|[0x800035b0]<br>0x00000000<br> [0x800035b8]<br>0x00000021<br> |- fs1 == 1 and fe1 == 0x3fe and fm1 == 0xfae147ae147ae and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                          |[0x8000041c]:fcvt.wu.d t5, t3, dyn<br> [0x80000420]:csrrs a4, fcsr, zero<br> [0x80000424]:sw t5, 48(ra)<br> [0x80000428]:sw a4, 56(ra)<br>   |
|  18|[0x800035c0]<br>0x00000000<br> [0x800035c8]<br>0x00000050<br> |- fs1 == 1 and fe1 == 0x3fe and fm1 == 0xfae147ae147ae and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                          |[0x8000044c]:fcvt.wu.d t5, t3, dyn<br> [0x80000450]:csrrs a4, fcsr, zero<br> [0x80000454]:sw t5, 64(ra)<br> [0x80000458]:sw a4, 72(ra)<br>   |
|  19|[0x800035d0]<br>0x00000000<br> [0x800035d8]<br>0x00000061<br> |- fs1 == 1 and fe1 == 0x3fe and fm1 == 0xfae147ae147ae and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                          |[0x8000047c]:fcvt.wu.d t5, t3, dyn<br> [0x80000480]:csrrs a4, fcsr, zero<br> [0x80000484]:sw t5, 80(ra)<br> [0x80000488]:sw a4, 88(ra)<br>   |
|  20|[0x800035e0]<br>0x00000000<br> [0x800035e8]<br>0x00000090<br> |- fs1 == 1 and fe1 == 0x3fe and fm1 == 0xfae147ae147ae and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                          |[0x800004ac]:fcvt.wu.d t5, t3, dyn<br> [0x800004b0]:csrrs a4, fcsr, zero<br> [0x800004b4]:sw t5, 96(ra)<br> [0x800004b8]:sw a4, 104(ra)<br>  |
|  21|[0x800035f0]<br>0x00000000<br> [0x800035f8]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x3ff and fm1 == 0x199999999999a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                           |[0x800004dc]:fcvt.wu.d t5, t3, dyn<br> [0x800004e0]:csrrs a4, fcsr, zero<br> [0x800004e4]:sw t5, 112(ra)<br> [0x800004e8]:sw a4, 120(ra)<br> |
|  22|[0x80003600]<br>0x00000000<br> [0x80003608]<br>0x00000030<br> |- fs1 == 1 and fe1 == 0x3ff and fm1 == 0x199999999999a and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                          |[0x8000050c]:fcvt.wu.d t5, t3, dyn<br> [0x80000510]:csrrs a4, fcsr, zero<br> [0x80000514]:sw t5, 128(ra)<br> [0x80000518]:sw a4, 136(ra)<br> |
|  23|[0x80003610]<br>0x00000000<br> [0x80003618]<br>0x00000050<br> |- fs1 == 1 and fe1 == 0x3ff and fm1 == 0x199999999999a and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                          |[0x8000053c]:fcvt.wu.d t5, t3, dyn<br> [0x80000540]:csrrs a4, fcsr, zero<br> [0x80000544]:sw t5, 144(ra)<br> [0x80000548]:sw a4, 152(ra)<br> |
|  24|[0x80003620]<br>0x00000000<br> [0x80003628]<br>0x00000070<br> |- fs1 == 1 and fe1 == 0x3ff and fm1 == 0x199999999999a and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                          |[0x8000056c]:fcvt.wu.d t5, t3, dyn<br> [0x80000570]:csrrs a4, fcsr, zero<br> [0x80000574]:sw t5, 160(ra)<br> [0x80000578]:sw a4, 168(ra)<br> |
|  25|[0x80003630]<br>0x00000000<br> [0x80003638]<br>0x00000090<br> |- fs1 == 1 and fe1 == 0x3ff and fm1 == 0x199999999999a and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                          |[0x8000059c]:fcvt.wu.d t5, t3, dyn<br> [0x800005a0]:csrrs a4, fcsr, zero<br> [0x800005a4]:sw t5, 176(ra)<br> [0x800005a8]:sw a4, 184(ra)<br> |
|  26|[0x80003640]<br>0x00000000<br> [0x80003648]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                           |[0x800005c4]:fcvt.wu.d t5, t3, dyn<br> [0x800005c8]:csrrs a4, fcsr, zero<br> [0x800005cc]:sw t5, 192(ra)<br> [0x800005d0]:sw a4, 200(ra)<br> |
|  27|[0x80003650]<br>0x00000000<br> [0x80003658]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                          |[0x800005ec]:fcvt.wu.d t5, t3, dyn<br> [0x800005f0]:csrrs a4, fcsr, zero<br> [0x800005f4]:sw t5, 208(ra)<br> [0x800005f8]:sw a4, 216(ra)<br> |
|  28|[0x80003660]<br>0x00000000<br> [0x80003668]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                          |[0x80000614]:fcvt.wu.d t5, t3, dyn<br> [0x80000618]:csrrs a4, fcsr, zero<br> [0x8000061c]:sw t5, 224(ra)<br> [0x80000620]:sw a4, 232(ra)<br> |
|  29|[0x80003670]<br>0x00000000<br> [0x80003678]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                          |[0x8000063c]:fcvt.wu.d t5, t3, dyn<br> [0x80000640]:csrrs a4, fcsr, zero<br> [0x80000644]:sw t5, 240(ra)<br> [0x80000648]:sw a4, 248(ra)<br> |
|  30|[0x80003680]<br>0x00000000<br> [0x80003688]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                          |[0x80000664]:fcvt.wu.d t5, t3, dyn<br> [0x80000668]:csrrs a4, fcsr, zero<br> [0x8000066c]:sw t5, 256(ra)<br> [0x80000670]:sw a4, 264(ra)<br> |
|  31|[0x80003690]<br>0x00000000<br> [0x80003698]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x3ff and fm1 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                           |[0x8000068c]:fcvt.wu.d t5, t3, dyn<br> [0x80000690]:csrrs a4, fcsr, zero<br> [0x80000694]:sw t5, 272(ra)<br> [0x80000698]:sw a4, 280(ra)<br> |
|  32|[0x800036a0]<br>0x00000000<br> [0x800036a8]<br>0x00000030<br> |- fs1 == 1 and fe1 == 0x3ff and fm1 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                          |[0x800006b4]:fcvt.wu.d t5, t3, dyn<br> [0x800006b8]:csrrs a4, fcsr, zero<br> [0x800006bc]:sw t5, 288(ra)<br> [0x800006c0]:sw a4, 296(ra)<br> |
|  33|[0x800036b0]<br>0x00000000<br> [0x800036b8]<br>0x00000050<br> |- fs1 == 1 and fe1 == 0x3ff and fm1 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                          |[0x800006dc]:fcvt.wu.d t5, t3, dyn<br> [0x800006e0]:csrrs a4, fcsr, zero<br> [0x800006e4]:sw t5, 304(ra)<br> [0x800006e8]:sw a4, 312(ra)<br> |
|  34|[0x800036c0]<br>0x00000000<br> [0x800036c8]<br>0x00000070<br> |- fs1 == 1 and fe1 == 0x3ff and fm1 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                          |[0x80000704]:fcvt.wu.d t5, t3, dyn<br> [0x80000708]:csrrs a4, fcsr, zero<br> [0x8000070c]:sw t5, 320(ra)<br> [0x80000710]:sw a4, 328(ra)<br> |
|  35|[0x800036d0]<br>0x00000000<br> [0x800036d8]<br>0x00000090<br> |- fs1 == 1 and fe1 == 0x3ff and fm1 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                          |[0x8000072c]:fcvt.wu.d t5, t3, dyn<br> [0x80000730]:csrrs a4, fcsr, zero<br> [0x80000734]:sw t5, 336(ra)<br> [0x80000738]:sw a4, 344(ra)<br> |
|  36|[0x800036e0]<br>0x00000000<br> [0x800036e8]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x3fb and fm1 == 0xc28f5c28f5c29 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                           |[0x8000075c]:fcvt.wu.d t5, t3, dyn<br> [0x80000760]:csrrs a4, fcsr, zero<br> [0x80000764]:sw t5, 352(ra)<br> [0x80000768]:sw a4, 360(ra)<br> |
|  37|[0x800036f0]<br>0x00000000<br> [0x800036f8]<br>0x00000021<br> |- fs1 == 1 and fe1 == 0x3fb and fm1 == 0xc28f5c28f5c29 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                          |[0x8000078c]:fcvt.wu.d t5, t3, dyn<br> [0x80000790]:csrrs a4, fcsr, zero<br> [0x80000794]:sw t5, 368(ra)<br> [0x80000798]:sw a4, 376(ra)<br> |
|  38|[0x80003700]<br>0x00000000<br> [0x80003708]<br>0x00000050<br> |- fs1 == 1 and fe1 == 0x3fb and fm1 == 0xc28f5c28f5c29 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                          |[0x800007bc]:fcvt.wu.d t5, t3, dyn<br> [0x800007c0]:csrrs a4, fcsr, zero<br> [0x800007c4]:sw t5, 384(ra)<br> [0x800007c8]:sw a4, 392(ra)<br> |
|  39|[0x80003710]<br>0x00000000<br> [0x80003718]<br>0x00000061<br> |- fs1 == 1 and fe1 == 0x3fb and fm1 == 0xc28f5c28f5c29 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                          |[0x800007ec]:fcvt.wu.d t5, t3, dyn<br> [0x800007f0]:csrrs a4, fcsr, zero<br> [0x800007f4]:sw t5, 400(ra)<br> [0x800007f8]:sw a4, 408(ra)<br> |
|  40|[0x80003720]<br>0x00000000<br> [0x80003728]<br>0x00000081<br> |- fs1 == 1 and fe1 == 0x3fb and fm1 == 0xc28f5c28f5c29 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                          |[0x8000081c]:fcvt.wu.d t5, t3, dyn<br> [0x80000820]:csrrs a4, fcsr, zero<br> [0x80000824]:sw t5, 416(ra)<br> [0x80000828]:sw a4, 424(ra)<br> |
|  41|[0x80003730]<br>0x00000000<br> [0x80003738]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x3fe and fm1 == 0xc7ae147ae147b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                           |[0x8000084c]:fcvt.wu.d t5, t3, dyn<br> [0x80000850]:csrrs a4, fcsr, zero<br> [0x80000854]:sw t5, 432(ra)<br> [0x80000858]:sw a4, 440(ra)<br> |
|  42|[0x80003740]<br>0x00000000<br> [0x80003748]<br>0x00000021<br> |- fs1 == 1 and fe1 == 0x3fe and fm1 == 0xc7ae147ae147b and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                          |[0x8000087c]:fcvt.wu.d t5, t3, dyn<br> [0x80000880]:csrrs a4, fcsr, zero<br> [0x80000884]:sw t5, 448(ra)<br> [0x80000888]:sw a4, 456(ra)<br> |
|  43|[0x80003750]<br>0x00000000<br> [0x80003758]<br>0x00000050<br> |- fs1 == 1 and fe1 == 0x3fe and fm1 == 0xc7ae147ae147b and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                          |[0x800008ac]:fcvt.wu.d t5, t3, dyn<br> [0x800008b0]:csrrs a4, fcsr, zero<br> [0x800008b4]:sw t5, 464(ra)<br> [0x800008b8]:sw a4, 472(ra)<br> |
|  44|[0x80003760]<br>0x00000000<br> [0x80003768]<br>0x00000061<br> |- fs1 == 1 and fe1 == 0x3fe and fm1 == 0xc7ae147ae147b and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                          |[0x800008dc]:fcvt.wu.d t5, t3, dyn<br> [0x800008e0]:csrrs a4, fcsr, zero<br> [0x800008e4]:sw t5, 480(ra)<br> [0x800008e8]:sw a4, 488(ra)<br> |
|  45|[0x80003770]<br>0x00000000<br> [0x80003778]<br>0x00000090<br> |- fs1 == 1 and fe1 == 0x3fe and fm1 == 0xc7ae147ae147b and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                          |[0x8000090c]:fcvt.wu.d t5, t3, dyn<br> [0x80000910]:csrrs a4, fcsr, zero<br> [0x80000914]:sw t5, 496(ra)<br> [0x80000918]:sw a4, 504(ra)<br> |
|  46|[0x80003780]<br>0x00000000<br> [0x80003788]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x3ff and fm1 == 0x1c28f5c28f5c3 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                           |[0x8000093c]:fcvt.wu.d t5, t3, dyn<br> [0x80000940]:csrrs a4, fcsr, zero<br> [0x80000944]:sw t5, 512(ra)<br> [0x80000948]:sw a4, 520(ra)<br> |
|  47|[0x80003790]<br>0x00000000<br> [0x80003798]<br>0x00000030<br> |- fs1 == 1 and fe1 == 0x3ff and fm1 == 0x1c28f5c28f5c3 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                          |[0x8000096c]:fcvt.wu.d t5, t3, dyn<br> [0x80000970]:csrrs a4, fcsr, zero<br> [0x80000974]:sw t5, 528(ra)<br> [0x80000978]:sw a4, 536(ra)<br> |
|  48|[0x800037a0]<br>0x00000000<br> [0x800037a8]<br>0x00000050<br> |- fs1 == 1 and fe1 == 0x3ff and fm1 == 0x1c28f5c28f5c3 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                          |[0x8000099c]:fcvt.wu.d t5, t3, dyn<br> [0x800009a0]:csrrs a4, fcsr, zero<br> [0x800009a4]:sw t5, 544(ra)<br> [0x800009a8]:sw a4, 552(ra)<br> |
|  49|[0x800037b0]<br>0x00000000<br> [0x800037b8]<br>0x00000070<br> |- fs1 == 1 and fe1 == 0x3ff and fm1 == 0x1c28f5c28f5c3 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                          |[0x800009cc]:fcvt.wu.d t5, t3, dyn<br> [0x800009d0]:csrrs a4, fcsr, zero<br> [0x800009d4]:sw t5, 560(ra)<br> [0x800009d8]:sw a4, 568(ra)<br> |
|  50|[0x800037c0]<br>0x00000000<br> [0x800037c8]<br>0x00000090<br> |- fs1 == 1 and fe1 == 0x3ff and fm1 == 0x1c28f5c28f5c3 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                          |[0x800009fc]:fcvt.wu.d t5, t3, dyn<br> [0x80000a00]:csrrs a4, fcsr, zero<br> [0x80000a04]:sw t5, 576(ra)<br> [0x80000a08]:sw a4, 584(ra)<br> |
|  51|[0x800037d0]<br>0x00000001<br> [0x800037d8]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x1c28f5c28f5c3 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                           |[0x80000a2c]:fcvt.wu.d t5, t3, dyn<br> [0x80000a30]:csrrs a4, fcsr, zero<br> [0x80000a34]:sw t5, 592(ra)<br> [0x80000a38]:sw a4, 600(ra)<br> |
|  52|[0x800037e0]<br>0x00000001<br> [0x800037e8]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x1c28f5c28f5c3 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                          |[0x80000a5c]:fcvt.wu.d t5, t3, dyn<br> [0x80000a60]:csrrs a4, fcsr, zero<br> [0x80000a64]:sw t5, 608(ra)<br> [0x80000a68]:sw a4, 616(ra)<br> |
|  53|[0x800037f0]<br>0x00000001<br> [0x800037f8]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x1c28f5c28f5c3 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                          |[0x80000a8c]:fcvt.wu.d t5, t3, dyn<br> [0x80000a90]:csrrs a4, fcsr, zero<br> [0x80000a94]:sw t5, 624(ra)<br> [0x80000a98]:sw a4, 632(ra)<br> |
|  54|[0x80003800]<br>0x00000002<br> [0x80003808]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x1c28f5c28f5c3 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                          |[0x80000abc]:fcvt.wu.d t5, t3, dyn<br> [0x80000ac0]:csrrs a4, fcsr, zero<br> [0x80000ac4]:sw t5, 640(ra)<br> [0x80000ac8]:sw a4, 648(ra)<br> |
|  55|[0x80003810]<br>0x00000001<br> [0x80003818]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x1c28f5c28f5c3 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                          |[0x80000aec]:fcvt.wu.d t5, t3, dyn<br> [0x80000af0]:csrrs a4, fcsr, zero<br> [0x80000af4]:sw t5, 656(ra)<br> [0x80000af8]:sw a4, 664(ra)<br> |
|  56|[0x80003820]<br>0x00000000<br> [0x80003828]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x47ae147ae147b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                           |[0x80000b1c]:fcvt.wu.d t5, t3, dyn<br> [0x80000b20]:csrrs a4, fcsr, zero<br> [0x80000b24]:sw t5, 672(ra)<br> [0x80000b28]:sw a4, 680(ra)<br> |
|  57|[0x80003830]<br>0x00000000<br> [0x80003838]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x47ae147ae147b and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                          |[0x80000b4c]:fcvt.wu.d t5, t3, dyn<br> [0x80000b50]:csrrs a4, fcsr, zero<br> [0x80000b54]:sw t5, 688(ra)<br> [0x80000b58]:sw a4, 696(ra)<br> |
|  58|[0x80003840]<br>0x00000000<br> [0x80003848]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x47ae147ae147b and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                          |[0x80000b7c]:fcvt.wu.d t5, t3, dyn<br> [0x80000b80]:csrrs a4, fcsr, zero<br> [0x80000b84]:sw t5, 704(ra)<br> [0x80000b88]:sw a4, 712(ra)<br> |
|  59|[0x80003850]<br>0x00000001<br> [0x80003858]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x47ae147ae147b and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                          |[0x80000bac]:fcvt.wu.d t5, t3, dyn<br> [0x80000bb0]:csrrs a4, fcsr, zero<br> [0x80000bb4]:sw t5, 720(ra)<br> [0x80000bb8]:sw a4, 728(ra)<br> |
|  60|[0x80003860]<br>0x00000000<br> [0x80003868]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x47ae147ae147b and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                          |[0x80000bdc]:fcvt.wu.d t5, t3, dyn<br> [0x80000be0]:csrrs a4, fcsr, zero<br> [0x80000be4]:sw t5, 736(ra)<br> [0x80000be8]:sw a4, 744(ra)<br> |
