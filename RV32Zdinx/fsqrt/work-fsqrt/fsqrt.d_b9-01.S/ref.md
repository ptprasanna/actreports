
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80004180')]      |
| SIG_REGION                | [('0x80006e10', '0x80007a20', '772 words')]      |
| COV_LABELS                | fsqrt.d_b9      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-fsqrt1/fsqrt.d_b9-01.S/ref.S    |
| Total Number of coverpoints| 416     |
| Total Coverpoints Hit     | 416      |
| Total Signature Updates   | 516      |
| STAT1                     | 258      |
| STAT2                     | 0      |
| STAT3                     | 125     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80002290]:fsqrt.d t5, t3, dyn
[0x80002294]:csrrs a5, fcsr, zero
[0x80002298]:sw t5, 952(ra)
[0x8000229c]:sw a5, 960(ra)
[0x800022a0]:lw t3, 1504(a4)
[0x800022a4]:lw t4, 1508(a4)
[0x800022a8]:addi t3, zero, 4095
[0x800022ac]:lui t4, 228864
[0x800022b0]:addi t4, t4, 63
[0x800022b4]:addi a0, zero, 0
[0x800022b8]:csrrw zero, fcsr, a0
[0x800022bc]:fsqrt.d t5, t3, dyn
[0x800022c0]:csrrs a5, fcsr, zero

[0x800022bc]:fsqrt.d t5, t3, dyn
[0x800022c0]:csrrs a5, fcsr, zero
[0x800022c4]:sw t5, 968(ra)
[0x800022c8]:sw a5, 976(ra)
[0x800022cc]:lw t3, 1512(a4)
[0x800022d0]:lw t4, 1516(a4)
[0x800022d4]:addi t3, zero, 0
[0x800022d8]:lui t4, 229120
[0x800022dc]:addi t4, t4, 4064
[0x800022e0]:addi a0, zero, 0
[0x800022e4]:csrrw zero, fcsr, a0
[0x800022e8]:fsqrt.d t5, t3, dyn
[0x800022ec]:csrrs a5, fcsr, zero

[0x800022e8]:fsqrt.d t5, t3, dyn
[0x800022ec]:csrrs a5, fcsr, zero
[0x800022f0]:sw t5, 984(ra)
[0x800022f4]:sw a5, 992(ra)
[0x800022f8]:lw t3, 1520(a4)
[0x800022fc]:lw t4, 1524(a4)
[0x80002300]:addi t3, zero, 4095
[0x80002304]:lui t4, 228864
[0x80002308]:addi t4, t4, 31
[0x8000230c]:addi a0, zero, 0
[0x80002310]:csrrw zero, fcsr, a0
[0x80002314]:fsqrt.d t5, t3, dyn
[0x80002318]:csrrs a5, fcsr, zero

[0x80002314]:fsqrt.d t5, t3, dyn
[0x80002318]:csrrs a5, fcsr, zero
[0x8000231c]:sw t5, 1000(ra)
[0x80002320]:sw a5, 1008(ra)
[0x80002324]:lw t3, 1528(a4)
[0x80002328]:lw t4, 1532(a4)
[0x8000232c]:addi t3, zero, 0
[0x80002330]:lui t4, 229120
[0x80002334]:addi t4, t4, 4080
[0x80002338]:addi a0, zero, 0
[0x8000233c]:csrrw zero, fcsr, a0
[0x80002340]:fsqrt.d t5, t3, dyn
[0x80002344]:csrrs a5, fcsr, zero

[0x80002340]:fsqrt.d t5, t3, dyn
[0x80002344]:csrrs a5, fcsr, zero
[0x80002348]:sw t5, 1016(ra)
[0x8000234c]:sw a5, 1024(ra)
[0x80002350]:lw t3, 1536(a4)
[0x80002354]:lw t4, 1540(a4)
[0x80002358]:addi t3, zero, 4095
[0x8000235c]:lui t4, 228864
[0x80002360]:addi t4, t4, 15
[0x80002364]:addi a0, zero, 0
[0x80002368]:csrrw zero, fcsr, a0
[0x8000236c]:fsqrt.d t5, t3, dyn
[0x80002370]:csrrs a5, fcsr, zero

[0x8000236c]:fsqrt.d t5, t3, dyn
[0x80002370]:csrrs a5, fcsr, zero
[0x80002374]:sw t5, 1032(ra)
[0x80002378]:sw a5, 1040(ra)
[0x8000237c]:lw t3, 1544(a4)
[0x80002380]:lw t4, 1548(a4)
[0x80002384]:addi t3, zero, 0
[0x80002388]:lui t4, 229120
[0x8000238c]:addi t4, t4, 4088
[0x80002390]:addi a0, zero, 0
[0x80002394]:csrrw zero, fcsr, a0
[0x80002398]:fsqrt.d t5, t3, dyn
[0x8000239c]:csrrs a5, fcsr, zero

[0x80002398]:fsqrt.d t5, t3, dyn
[0x8000239c]:csrrs a5, fcsr, zero
[0x800023a0]:sw t5, 1048(ra)
[0x800023a4]:sw a5, 1056(ra)
[0x800023a8]:lw t3, 1552(a4)
[0x800023ac]:lw t4, 1556(a4)
[0x800023b0]:addi t3, zero, 4095
[0x800023b4]:lui t4, 228864
[0x800023b8]:addi t4, t4, 7
[0x800023bc]:addi a0, zero, 0
[0x800023c0]:csrrw zero, fcsr, a0
[0x800023c4]:fsqrt.d t5, t3, dyn
[0x800023c8]:csrrs a5, fcsr, zero

[0x800023c4]:fsqrt.d t5, t3, dyn
[0x800023c8]:csrrs a5, fcsr, zero
[0x800023cc]:sw t5, 1064(ra)
[0x800023d0]:sw a5, 1072(ra)
[0x800023d4]:lw t3, 1560(a4)
[0x800023d8]:lw t4, 1564(a4)
[0x800023dc]:addi t3, zero, 0
[0x800023e0]:lui t4, 229120
[0x800023e4]:addi t4, t4, 4092
[0x800023e8]:addi a0, zero, 0
[0x800023ec]:csrrw zero, fcsr, a0
[0x800023f0]:fsqrt.d t5, t3, dyn
[0x800023f4]:csrrs a5, fcsr, zero

[0x800023f0]:fsqrt.d t5, t3, dyn
[0x800023f4]:csrrs a5, fcsr, zero
[0x800023f8]:sw t5, 1080(ra)
[0x800023fc]:sw a5, 1088(ra)
[0x80002400]:lw t3, 1568(a4)
[0x80002404]:lw t4, 1572(a4)
[0x80002408]:addi t3, zero, 4095
[0x8000240c]:lui t4, 228864
[0x80002410]:addi t4, t4, 3
[0x80002414]:addi a0, zero, 0
[0x80002418]:csrrw zero, fcsr, a0
[0x8000241c]:fsqrt.d t5, t3, dyn
[0x80002420]:csrrs a5, fcsr, zero

[0x8000241c]:fsqrt.d t5, t3, dyn
[0x80002420]:csrrs a5, fcsr, zero
[0x80002424]:sw t5, 1096(ra)
[0x80002428]:sw a5, 1104(ra)
[0x8000242c]:lw t3, 1576(a4)
[0x80002430]:lw t4, 1580(a4)
[0x80002434]:addi t3, zero, 0
[0x80002438]:lui t4, 229120
[0x8000243c]:addi t4, t4, 4094
[0x80002440]:addi a0, zero, 0
[0x80002444]:csrrw zero, fcsr, a0
[0x80002448]:fsqrt.d t5, t3, dyn
[0x8000244c]:csrrs a5, fcsr, zero

[0x80002448]:fsqrt.d t5, t3, dyn
[0x8000244c]:csrrs a5, fcsr, zero
[0x80002450]:sw t5, 1112(ra)
[0x80002454]:sw a5, 1120(ra)
[0x80002458]:lw t3, 1584(a4)
[0x8000245c]:lw t4, 1588(a4)
[0x80002460]:addi t3, zero, 4095
[0x80002464]:lui t4, 228864
[0x80002468]:addi t4, t4, 1
[0x8000246c]:addi a0, zero, 0
[0x80002470]:csrrw zero, fcsr, a0
[0x80002474]:fsqrt.d t5, t3, dyn
[0x80002478]:csrrs a5, fcsr, zero

[0x80002474]:fsqrt.d t5, t3, dyn
[0x80002478]:csrrs a5, fcsr, zero
[0x8000247c]:sw t5, 1128(ra)
[0x80002480]:sw a5, 1136(ra)
[0x80002484]:lw t3, 1592(a4)
[0x80002488]:lw t4, 1596(a4)
[0x8000248c]:addi t3, zero, 0
[0x80002490]:lui t4, 229120
[0x80002494]:addi t4, t4, 4095
[0x80002498]:addi a0, zero, 0
[0x8000249c]:csrrw zero, fcsr, a0
[0x800024a0]:fsqrt.d t5, t3, dyn
[0x800024a4]:csrrs a5, fcsr, zero

[0x800024a0]:fsqrt.d t5, t3, dyn
[0x800024a4]:csrrs a5, fcsr, zero
[0x800024a8]:sw t5, 1144(ra)
[0x800024ac]:sw a5, 1152(ra)
[0x800024b0]:lw t3, 1600(a4)
[0x800024b4]:lw t4, 1604(a4)
[0x800024b8]:addi t3, zero, 4095
[0x800024bc]:lui t4, 228864
[0x800024c0]:addi a0, zero, 0
[0x800024c4]:csrrw zero, fcsr, a0
[0x800024c8]:fsqrt.d t5, t3, dyn
[0x800024cc]:csrrs a5, fcsr, zero

[0x800024c8]:fsqrt.d t5, t3, dyn
[0x800024cc]:csrrs a5, fcsr, zero
[0x800024d0]:sw t5, 1160(ra)
[0x800024d4]:sw a5, 1168(ra)
[0x800024d8]:lw t3, 1608(a4)
[0x800024dc]:lw t4, 1612(a4)
[0x800024e0]:lui t3, 524288
[0x800024e4]:lui t4, 229120
[0x800024e8]:addi t4, t4, 4095
[0x800024ec]:addi a0, zero, 0
[0x800024f0]:csrrw zero, fcsr, a0
[0x800024f4]:fsqrt.d t5, t3, dyn
[0x800024f8]:csrrs a5, fcsr, zero

[0x800024f4]:fsqrt.d t5, t3, dyn
[0x800024f8]:csrrs a5, fcsr, zero
[0x800024fc]:sw t5, 1176(ra)
[0x80002500]:sw a5, 1184(ra)
[0x80002504]:lw t3, 1616(a4)
[0x80002508]:lw t4, 1620(a4)
[0x8000250c]:lui t3, 524288
[0x80002510]:addi t3, t3, 4095
[0x80002514]:lui t4, 228864
[0x80002518]:addi a0, zero, 0
[0x8000251c]:csrrw zero, fcsr, a0
[0x80002520]:fsqrt.d t5, t3, dyn
[0x80002524]:csrrs a5, fcsr, zero

[0x80002520]:fsqrt.d t5, t3, dyn
[0x80002524]:csrrs a5, fcsr, zero
[0x80002528]:sw t5, 1192(ra)
[0x8000252c]:sw a5, 1200(ra)
[0x80002530]:lw t3, 1624(a4)
[0x80002534]:lw t4, 1628(a4)
[0x80002538]:lui t3, 786432
[0x8000253c]:lui t4, 229120
[0x80002540]:addi t4, t4, 4095
[0x80002544]:addi a0, zero, 0
[0x80002548]:csrrw zero, fcsr, a0
[0x8000254c]:fsqrt.d t5, t3, dyn
[0x80002550]:csrrs a5, fcsr, zero

[0x8000254c]:fsqrt.d t5, t3, dyn
[0x80002550]:csrrs a5, fcsr, zero
[0x80002554]:sw t5, 1208(ra)
[0x80002558]:sw a5, 1216(ra)
[0x8000255c]:lw t3, 1632(a4)
[0x80002560]:lw t4, 1636(a4)
[0x80002564]:lui t3, 262144
[0x80002568]:addi t3, t3, 4095
[0x8000256c]:lui t4, 228864
[0x80002570]:addi a0, zero, 0
[0x80002574]:csrrw zero, fcsr, a0
[0x80002578]:fsqrt.d t5, t3, dyn
[0x8000257c]:csrrs a5, fcsr, zero

[0x80002578]:fsqrt.d t5, t3, dyn
[0x8000257c]:csrrs a5, fcsr, zero
[0x80002580]:sw t5, 1224(ra)
[0x80002584]:sw a5, 1232(ra)
[0x80002588]:lw t3, 1640(a4)
[0x8000258c]:lw t4, 1644(a4)
[0x80002590]:lui t3, 917504
[0x80002594]:lui t4, 229120
[0x80002598]:addi t4, t4, 4095
[0x8000259c]:addi a0, zero, 0
[0x800025a0]:csrrw zero, fcsr, a0
[0x800025a4]:fsqrt.d t5, t3, dyn
[0x800025a8]:csrrs a5, fcsr, zero

[0x800025a4]:fsqrt.d t5, t3, dyn
[0x800025a8]:csrrs a5, fcsr, zero
[0x800025ac]:sw t5, 1240(ra)
[0x800025b0]:sw a5, 1248(ra)
[0x800025b4]:lw t3, 1648(a4)
[0x800025b8]:lw t4, 1652(a4)
[0x800025bc]:lui t3, 131072
[0x800025c0]:addi t3, t3, 4095
[0x800025c4]:lui t4, 228864
[0x800025c8]:addi a0, zero, 0
[0x800025cc]:csrrw zero, fcsr, a0
[0x800025d0]:fsqrt.d t5, t3, dyn
[0x800025d4]:csrrs a5, fcsr, zero

[0x800025d0]:fsqrt.d t5, t3, dyn
[0x800025d4]:csrrs a5, fcsr, zero
[0x800025d8]:sw t5, 1256(ra)
[0x800025dc]:sw a5, 1264(ra)
[0x800025e0]:lw t3, 1656(a4)
[0x800025e4]:lw t4, 1660(a4)
[0x800025e8]:lui t3, 983040
[0x800025ec]:lui t4, 229120
[0x800025f0]:addi t4, t4, 4095
[0x800025f4]:addi a0, zero, 0
[0x800025f8]:csrrw zero, fcsr, a0
[0x800025fc]:fsqrt.d t5, t3, dyn
[0x80002600]:csrrs a5, fcsr, zero

[0x800025fc]:fsqrt.d t5, t3, dyn
[0x80002600]:csrrs a5, fcsr, zero
[0x80002604]:sw t5, 1272(ra)
[0x80002608]:sw a5, 1280(ra)
[0x8000260c]:lw t3, 1664(a4)
[0x80002610]:lw t4, 1668(a4)
[0x80002614]:lui t3, 65536
[0x80002618]:addi t3, t3, 4095
[0x8000261c]:lui t4, 228864
[0x80002620]:addi a0, zero, 0
[0x80002624]:csrrw zero, fcsr, a0
[0x80002628]:fsqrt.d t5, t3, dyn
[0x8000262c]:csrrs a5, fcsr, zero

[0x80002628]:fsqrt.d t5, t3, dyn
[0x8000262c]:csrrs a5, fcsr, zero
[0x80002630]:sw t5, 1288(ra)
[0x80002634]:sw a5, 1296(ra)
[0x80002638]:lw t3, 1672(a4)
[0x8000263c]:lw t4, 1676(a4)
[0x80002640]:lui t3, 1015808
[0x80002644]:lui t4, 229120
[0x80002648]:addi t4, t4, 4095
[0x8000264c]:addi a0, zero, 0
[0x80002650]:csrrw zero, fcsr, a0
[0x80002654]:fsqrt.d t5, t3, dyn
[0x80002658]:csrrs a5, fcsr, zero

[0x80002654]:fsqrt.d t5, t3, dyn
[0x80002658]:csrrs a5, fcsr, zero
[0x8000265c]:sw t5, 1304(ra)
[0x80002660]:sw a5, 1312(ra)
[0x80002664]:lw t3, 1680(a4)
[0x80002668]:lw t4, 1684(a4)
[0x8000266c]:lui t3, 32768
[0x80002670]:addi t3, t3, 4095
[0x80002674]:lui t4, 228864
[0x80002678]:addi a0, zero, 0
[0x8000267c]:csrrw zero, fcsr, a0
[0x80002680]:fsqrt.d t5, t3, dyn
[0x80002684]:csrrs a5, fcsr, zero

[0x80002680]:fsqrt.d t5, t3, dyn
[0x80002684]:csrrs a5, fcsr, zero
[0x80002688]:sw t5, 1320(ra)
[0x8000268c]:sw a5, 1328(ra)
[0x80002690]:lw t3, 1688(a4)
[0x80002694]:lw t4, 1692(a4)
[0x80002698]:lui t3, 1032192
[0x8000269c]:lui t4, 229120
[0x800026a0]:addi t4, t4, 4095
[0x800026a4]:addi a0, zero, 0
[0x800026a8]:csrrw zero, fcsr, a0
[0x800026ac]:fsqrt.d t5, t3, dyn
[0x800026b0]:csrrs a5, fcsr, zero

[0x800026ac]:fsqrt.d t5, t3, dyn
[0x800026b0]:csrrs a5, fcsr, zero
[0x800026b4]:sw t5, 1336(ra)
[0x800026b8]:sw a5, 1344(ra)
[0x800026bc]:lw t3, 1696(a4)
[0x800026c0]:lw t4, 1700(a4)
[0x800026c4]:lui t3, 16384
[0x800026c8]:addi t3, t3, 4095
[0x800026cc]:lui t4, 228864
[0x800026d0]:addi a0, zero, 0
[0x800026d4]:csrrw zero, fcsr, a0
[0x800026d8]:fsqrt.d t5, t3, dyn
[0x800026dc]:csrrs a5, fcsr, zero

[0x800026d8]:fsqrt.d t5, t3, dyn
[0x800026dc]:csrrs a5, fcsr, zero
[0x800026e0]:sw t5, 1352(ra)
[0x800026e4]:sw a5, 1360(ra)
[0x800026e8]:lw t3, 1704(a4)
[0x800026ec]:lw t4, 1708(a4)
[0x800026f0]:lui t3, 1040384
[0x800026f4]:lui t4, 229120
[0x800026f8]:addi t4, t4, 4095
[0x800026fc]:addi a0, zero, 0
[0x80002700]:csrrw zero, fcsr, a0
[0x80002704]:fsqrt.d t5, t3, dyn
[0x80002708]:csrrs a5, fcsr, zero

[0x80002704]:fsqrt.d t5, t3, dyn
[0x80002708]:csrrs a5, fcsr, zero
[0x8000270c]:sw t5, 1368(ra)
[0x80002710]:sw a5, 1376(ra)
[0x80002714]:lw t3, 1712(a4)
[0x80002718]:lw t4, 1716(a4)
[0x8000271c]:lui t3, 8192
[0x80002720]:addi t3, t3, 4095
[0x80002724]:lui t4, 228864
[0x80002728]:addi a0, zero, 0
[0x8000272c]:csrrw zero, fcsr, a0
[0x80002730]:fsqrt.d t5, t3, dyn
[0x80002734]:csrrs a5, fcsr, zero

[0x80002730]:fsqrt.d t5, t3, dyn
[0x80002734]:csrrs a5, fcsr, zero
[0x80002738]:sw t5, 1384(ra)
[0x8000273c]:sw a5, 1392(ra)
[0x80002740]:lw t3, 1720(a4)
[0x80002744]:lw t4, 1724(a4)
[0x80002748]:lui t3, 1044480
[0x8000274c]:lui t4, 229120
[0x80002750]:addi t4, t4, 4095
[0x80002754]:addi a0, zero, 0
[0x80002758]:csrrw zero, fcsr, a0
[0x8000275c]:fsqrt.d t5, t3, dyn
[0x80002760]:csrrs a5, fcsr, zero

[0x8000275c]:fsqrt.d t5, t3, dyn
[0x80002760]:csrrs a5, fcsr, zero
[0x80002764]:sw t5, 1400(ra)
[0x80002768]:sw a5, 1408(ra)
[0x8000276c]:lw t3, 1728(a4)
[0x80002770]:lw t4, 1732(a4)
[0x80002774]:lui t3, 4096
[0x80002778]:addi t3, t3, 4095
[0x8000277c]:lui t4, 228864
[0x80002780]:addi a0, zero, 0
[0x80002784]:csrrw zero, fcsr, a0
[0x80002788]:fsqrt.d t5, t3, dyn
[0x8000278c]:csrrs a5, fcsr, zero

[0x80002788]:fsqrt.d t5, t3, dyn
[0x8000278c]:csrrs a5, fcsr, zero
[0x80002790]:sw t5, 1416(ra)
[0x80002794]:sw a5, 1424(ra)
[0x80002798]:lw t3, 1736(a4)
[0x8000279c]:lw t4, 1740(a4)
[0x800027a0]:lui t3, 1046528
[0x800027a4]:lui t4, 229120
[0x800027a8]:addi t4, t4, 4095
[0x800027ac]:addi a0, zero, 0
[0x800027b0]:csrrw zero, fcsr, a0
[0x800027b4]:fsqrt.d t5, t3, dyn
[0x800027b8]:csrrs a5, fcsr, zero

[0x800027b4]:fsqrt.d t5, t3, dyn
[0x800027b8]:csrrs a5, fcsr, zero
[0x800027bc]:sw t5, 1432(ra)
[0x800027c0]:sw a5, 1440(ra)
[0x800027c4]:lw t3, 1744(a4)
[0x800027c8]:lw t4, 1748(a4)
[0x800027cc]:lui t3, 2048
[0x800027d0]:addi t3, t3, 4095
[0x800027d4]:lui t4, 228864
[0x800027d8]:addi a0, zero, 0
[0x800027dc]:csrrw zero, fcsr, a0
[0x800027e0]:fsqrt.d t5, t3, dyn
[0x800027e4]:csrrs a5, fcsr, zero

[0x800027e0]:fsqrt.d t5, t3, dyn
[0x800027e4]:csrrs a5, fcsr, zero
[0x800027e8]:sw t5, 1448(ra)
[0x800027ec]:sw a5, 1456(ra)
[0x800027f0]:lw t3, 1752(a4)
[0x800027f4]:lw t4, 1756(a4)
[0x800027f8]:lui t3, 1047552
[0x800027fc]:lui t4, 229120
[0x80002800]:addi t4, t4, 4095
[0x80002804]:addi a0, zero, 0
[0x80002808]:csrrw zero, fcsr, a0
[0x8000280c]:fsqrt.d t5, t3, dyn
[0x80002810]:csrrs a5, fcsr, zero

[0x8000280c]:fsqrt.d t5, t3, dyn
[0x80002810]:csrrs a5, fcsr, zero
[0x80002814]:sw t5, 1464(ra)
[0x80002818]:sw a5, 1472(ra)
[0x8000281c]:lw t3, 1760(a4)
[0x80002820]:lw t4, 1764(a4)
[0x80002824]:lui t3, 1024
[0x80002828]:addi t3, t3, 4095
[0x8000282c]:lui t4, 228864
[0x80002830]:addi a0, zero, 0
[0x80002834]:csrrw zero, fcsr, a0
[0x80002838]:fsqrt.d t5, t3, dyn
[0x8000283c]:csrrs a5, fcsr, zero

[0x80002838]:fsqrt.d t5, t3, dyn
[0x8000283c]:csrrs a5, fcsr, zero
[0x80002840]:sw t5, 1480(ra)
[0x80002844]:sw a5, 1488(ra)
[0x80002848]:lw t3, 1768(a4)
[0x8000284c]:lw t4, 1772(a4)
[0x80002850]:lui t3, 1048064
[0x80002854]:lui t4, 229120
[0x80002858]:addi t4, t4, 4095
[0x8000285c]:addi a0, zero, 0
[0x80002860]:csrrw zero, fcsr, a0
[0x80002864]:fsqrt.d t5, t3, dyn
[0x80002868]:csrrs a5, fcsr, zero

[0x80002864]:fsqrt.d t5, t3, dyn
[0x80002868]:csrrs a5, fcsr, zero
[0x8000286c]:sw t5, 1496(ra)
[0x80002870]:sw a5, 1504(ra)
[0x80002874]:lw t3, 1776(a4)
[0x80002878]:lw t4, 1780(a4)
[0x8000287c]:lui t3, 512
[0x80002880]:addi t3, t3, 4095
[0x80002884]:lui t4, 228864
[0x80002888]:addi a0, zero, 0
[0x8000288c]:csrrw zero, fcsr, a0
[0x80002890]:fsqrt.d t5, t3, dyn
[0x80002894]:csrrs a5, fcsr, zero

[0x80002890]:fsqrt.d t5, t3, dyn
[0x80002894]:csrrs a5, fcsr, zero
[0x80002898]:sw t5, 1512(ra)
[0x8000289c]:sw a5, 1520(ra)
[0x800028a0]:lw t3, 1784(a4)
[0x800028a4]:lw t4, 1788(a4)
[0x800028a8]:lui t3, 1048320
[0x800028ac]:lui t4, 229120
[0x800028b0]:addi t4, t4, 4095
[0x800028b4]:addi a0, zero, 0
[0x800028b8]:csrrw zero, fcsr, a0
[0x800028bc]:fsqrt.d t5, t3, dyn
[0x800028c0]:csrrs a5, fcsr, zero

[0x800028bc]:fsqrt.d t5, t3, dyn
[0x800028c0]:csrrs a5, fcsr, zero
[0x800028c4]:sw t5, 1528(ra)
[0x800028c8]:sw a5, 1536(ra)
[0x800028cc]:lw t3, 1792(a4)
[0x800028d0]:lw t4, 1796(a4)
[0x800028d4]:lui t3, 256
[0x800028d8]:addi t3, t3, 4095
[0x800028dc]:lui t4, 228864
[0x800028e0]:addi a0, zero, 0
[0x800028e4]:csrrw zero, fcsr, a0
[0x800028e8]:fsqrt.d t5, t3, dyn
[0x800028ec]:csrrs a5, fcsr, zero

[0x800028e8]:fsqrt.d t5, t3, dyn
[0x800028ec]:csrrs a5, fcsr, zero
[0x800028f0]:sw t5, 1544(ra)
[0x800028f4]:sw a5, 1552(ra)
[0x800028f8]:lw t3, 1800(a4)
[0x800028fc]:lw t4, 1804(a4)
[0x80002900]:lui t3, 1048448
[0x80002904]:lui t4, 229120
[0x80002908]:addi t4, t4, 4095
[0x8000290c]:addi a0, zero, 0
[0x80002910]:csrrw zero, fcsr, a0
[0x80002914]:fsqrt.d t5, t3, dyn
[0x80002918]:csrrs a5, fcsr, zero

[0x80002914]:fsqrt.d t5, t3, dyn
[0x80002918]:csrrs a5, fcsr, zero
[0x8000291c]:sw t5, 1560(ra)
[0x80002920]:sw a5, 1568(ra)
[0x80002924]:lw t3, 1808(a4)
[0x80002928]:lw t4, 1812(a4)
[0x8000292c]:lui t3, 128
[0x80002930]:addi t3, t3, 4095
[0x80002934]:lui t4, 228864
[0x80002938]:addi a0, zero, 0
[0x8000293c]:csrrw zero, fcsr, a0
[0x80002940]:fsqrt.d t5, t3, dyn
[0x80002944]:csrrs a5, fcsr, zero

[0x80002940]:fsqrt.d t5, t3, dyn
[0x80002944]:csrrs a5, fcsr, zero
[0x80002948]:sw t5, 1576(ra)
[0x8000294c]:sw a5, 1584(ra)
[0x80002950]:lw t3, 1816(a4)
[0x80002954]:lw t4, 1820(a4)
[0x80002958]:lui t3, 1048512
[0x8000295c]:lui t4, 229120
[0x80002960]:addi t4, t4, 4095
[0x80002964]:addi a0, zero, 0
[0x80002968]:csrrw zero, fcsr, a0
[0x8000296c]:fsqrt.d t5, t3, dyn
[0x80002970]:csrrs a5, fcsr, zero

[0x8000296c]:fsqrt.d t5, t3, dyn
[0x80002970]:csrrs a5, fcsr, zero
[0x80002974]:sw t5, 1592(ra)
[0x80002978]:sw a5, 1600(ra)
[0x8000297c]:lw t3, 1824(a4)
[0x80002980]:lw t4, 1828(a4)
[0x80002984]:lui t3, 64
[0x80002988]:addi t3, t3, 4095
[0x8000298c]:lui t4, 228864
[0x80002990]:addi a0, zero, 0
[0x80002994]:csrrw zero, fcsr, a0
[0x80002998]:fsqrt.d t5, t3, dyn
[0x8000299c]:csrrs a5, fcsr, zero

[0x80002998]:fsqrt.d t5, t3, dyn
[0x8000299c]:csrrs a5, fcsr, zero
[0x800029a0]:sw t5, 1608(ra)
[0x800029a4]:sw a5, 1616(ra)
[0x800029a8]:lw t3, 1832(a4)
[0x800029ac]:lw t4, 1836(a4)
[0x800029b0]:lui t3, 1048544
[0x800029b4]:lui t4, 229120
[0x800029b8]:addi t4, t4, 4095
[0x800029bc]:addi a0, zero, 0
[0x800029c0]:csrrw zero, fcsr, a0
[0x800029c4]:fsqrt.d t5, t3, dyn
[0x800029c8]:csrrs a5, fcsr, zero

[0x800029c4]:fsqrt.d t5, t3, dyn
[0x800029c8]:csrrs a5, fcsr, zero
[0x800029cc]:sw t5, 1624(ra)
[0x800029d0]:sw a5, 1632(ra)
[0x800029d4]:lw t3, 1840(a4)
[0x800029d8]:lw t4, 1844(a4)
[0x800029dc]:lui t3, 32
[0x800029e0]:addi t3, t3, 4095
[0x800029e4]:lui t4, 228864
[0x800029e8]:addi a0, zero, 0
[0x800029ec]:csrrw zero, fcsr, a0
[0x800029f0]:fsqrt.d t5, t3, dyn
[0x800029f4]:csrrs a5, fcsr, zero

[0x800029f0]:fsqrt.d t5, t3, dyn
[0x800029f4]:csrrs a5, fcsr, zero
[0x800029f8]:sw t5, 1640(ra)
[0x800029fc]:sw a5, 1648(ra)
[0x80002a00]:lw t3, 1848(a4)
[0x80002a04]:lw t4, 1852(a4)
[0x80002a08]:lui t3, 1048560
[0x80002a0c]:lui t4, 229120
[0x80002a10]:addi t4, t4, 4095
[0x80002a14]:addi a0, zero, 0
[0x80002a18]:csrrw zero, fcsr, a0
[0x80002a1c]:fsqrt.d t5, t3, dyn
[0x80002a20]:csrrs a5, fcsr, zero

[0x80002a1c]:fsqrt.d t5, t3, dyn
[0x80002a20]:csrrs a5, fcsr, zero
[0x80002a24]:sw t5, 1656(ra)
[0x80002a28]:sw a5, 1664(ra)
[0x80002a2c]:lw t3, 1856(a4)
[0x80002a30]:lw t4, 1860(a4)
[0x80002a34]:lui t3, 16
[0x80002a38]:addi t3, t3, 4095
[0x80002a3c]:lui t4, 228864
[0x80002a40]:addi a0, zero, 0
[0x80002a44]:csrrw zero, fcsr, a0
[0x80002a48]:fsqrt.d t5, t3, dyn
[0x80002a4c]:csrrs a5, fcsr, zero

[0x80002a48]:fsqrt.d t5, t3, dyn
[0x80002a4c]:csrrs a5, fcsr, zero
[0x80002a50]:sw t5, 1672(ra)
[0x80002a54]:sw a5, 1680(ra)
[0x80002a58]:lw t3, 1864(a4)
[0x80002a5c]:lw t4, 1868(a4)
[0x80002a60]:lui t3, 1048568
[0x80002a64]:lui t4, 229120
[0x80002a68]:addi t4, t4, 4095
[0x80002a6c]:addi a0, zero, 0
[0x80002a70]:csrrw zero, fcsr, a0
[0x80002a74]:fsqrt.d t5, t3, dyn
[0x80002a78]:csrrs a5, fcsr, zero

[0x80002a74]:fsqrt.d t5, t3, dyn
[0x80002a78]:csrrs a5, fcsr, zero
[0x80002a7c]:sw t5, 1688(ra)
[0x80002a80]:sw a5, 1696(ra)
[0x80002a84]:lw t3, 1872(a4)
[0x80002a88]:lw t4, 1876(a4)
[0x80002a8c]:lui t3, 8
[0x80002a90]:addi t3, t3, 4095
[0x80002a94]:lui t4, 228864
[0x80002a98]:addi a0, zero, 0
[0x80002a9c]:csrrw zero, fcsr, a0
[0x80002aa0]:fsqrt.d t5, t3, dyn
[0x80002aa4]:csrrs a5, fcsr, zero

[0x80002aa0]:fsqrt.d t5, t3, dyn
[0x80002aa4]:csrrs a5, fcsr, zero
[0x80002aa8]:sw t5, 1704(ra)
[0x80002aac]:sw a5, 1712(ra)
[0x80002ab0]:lw t3, 1880(a4)
[0x80002ab4]:lw t4, 1884(a4)
[0x80002ab8]:lui t3, 1048572
[0x80002abc]:lui t4, 229120
[0x80002ac0]:addi t4, t4, 4095
[0x80002ac4]:addi a0, zero, 0
[0x80002ac8]:csrrw zero, fcsr, a0
[0x80002acc]:fsqrt.d t5, t3, dyn
[0x80002ad0]:csrrs a5, fcsr, zero

[0x80002acc]:fsqrt.d t5, t3, dyn
[0x80002ad0]:csrrs a5, fcsr, zero
[0x80002ad4]:sw t5, 1720(ra)
[0x80002ad8]:sw a5, 1728(ra)
[0x80002adc]:lw t3, 1888(a4)
[0x80002ae0]:lw t4, 1892(a4)
[0x80002ae4]:lui t3, 4
[0x80002ae8]:addi t3, t3, 4095
[0x80002aec]:lui t4, 228864
[0x80002af0]:addi a0, zero, 0
[0x80002af4]:csrrw zero, fcsr, a0
[0x80002af8]:fsqrt.d t5, t3, dyn
[0x80002afc]:csrrs a5, fcsr, zero

[0x80002af8]:fsqrt.d t5, t3, dyn
[0x80002afc]:csrrs a5, fcsr, zero
[0x80002b00]:sw t5, 1736(ra)
[0x80002b04]:sw a5, 1744(ra)
[0x80002b08]:lw t3, 1896(a4)
[0x80002b0c]:lw t4, 1900(a4)
[0x80002b10]:lui t3, 1048574
[0x80002b14]:lui t4, 229120
[0x80002b18]:addi t4, t4, 4095
[0x80002b1c]:addi a0, zero, 0
[0x80002b20]:csrrw zero, fcsr, a0
[0x80002b24]:fsqrt.d t5, t3, dyn
[0x80002b28]:csrrs a5, fcsr, zero

[0x80002b24]:fsqrt.d t5, t3, dyn
[0x80002b28]:csrrs a5, fcsr, zero
[0x80002b2c]:sw t5, 1752(ra)
[0x80002b30]:sw a5, 1760(ra)
[0x80002b34]:lw t3, 1904(a4)
[0x80002b38]:lw t4, 1908(a4)
[0x80002b3c]:lui t3, 2
[0x80002b40]:addi t3, t3, 4095
[0x80002b44]:lui t4, 228864
[0x80002b48]:addi a0, zero, 0
[0x80002b4c]:csrrw zero, fcsr, a0
[0x80002b50]:fsqrt.d t5, t3, dyn
[0x80002b54]:csrrs a5, fcsr, zero

[0x80002b50]:fsqrt.d t5, t3, dyn
[0x80002b54]:csrrs a5, fcsr, zero
[0x80002b58]:sw t5, 1768(ra)
[0x80002b5c]:sw a5, 1776(ra)
[0x80002b60]:lw t3, 1912(a4)
[0x80002b64]:lw t4, 1916(a4)
[0x80002b68]:lui t3, 1048575
[0x80002b6c]:lui t4, 229120
[0x80002b70]:addi t4, t4, 4095
[0x80002b74]:addi a0, zero, 0
[0x80002b78]:csrrw zero, fcsr, a0
[0x80002b7c]:fsqrt.d t5, t3, dyn
[0x80002b80]:csrrs a5, fcsr, zero

[0x80002b7c]:fsqrt.d t5, t3, dyn
[0x80002b80]:csrrs a5, fcsr, zero
[0x80002b84]:sw t5, 1784(ra)
[0x80002b88]:sw a5, 1792(ra)
[0x80002b8c]:lw t3, 1920(a4)
[0x80002b90]:lw t4, 1924(a4)
[0x80002b94]:lui t3, 1
[0x80002b98]:addi t3, t3, 4095
[0x80002b9c]:lui t4, 228864
[0x80002ba0]:addi a0, zero, 0
[0x80002ba4]:csrrw zero, fcsr, a0
[0x80002ba8]:fsqrt.d t5, t3, dyn
[0x80002bac]:csrrs a5, fcsr, zero

[0x80002ba8]:fsqrt.d t5, t3, dyn
[0x80002bac]:csrrs a5, fcsr, zero
[0x80002bb0]:sw t5, 1800(ra)
[0x80002bb4]:sw a5, 1808(ra)
[0x80002bb8]:lw t3, 1928(a4)
[0x80002bbc]:lw t4, 1932(a4)
[0x80002bc0]:addi t3, zero, 2048
[0x80002bc4]:lui t4, 229120
[0x80002bc8]:addi t4, t4, 4095
[0x80002bcc]:addi a0, zero, 0
[0x80002bd0]:csrrw zero, fcsr, a0
[0x80002bd4]:fsqrt.d t5, t3, dyn
[0x80002bd8]:csrrs a5, fcsr, zero

[0x80002bd4]:fsqrt.d t5, t3, dyn
[0x80002bd8]:csrrs a5, fcsr, zero
[0x80002bdc]:sw t5, 1816(ra)
[0x80002be0]:sw a5, 1824(ra)
[0x80002be4]:lw t3, 1936(a4)
[0x80002be8]:lw t4, 1940(a4)
[0x80002bec]:addi t3, zero, 2047
[0x80002bf0]:lui t4, 228864
[0x80002bf4]:addi a0, zero, 0
[0x80002bf8]:csrrw zero, fcsr, a0
[0x80002bfc]:fsqrt.d t5, t3, dyn
[0x80002c00]:csrrs a5, fcsr, zero

[0x80002bfc]:fsqrt.d t5, t3, dyn
[0x80002c00]:csrrs a5, fcsr, zero
[0x80002c04]:sw t5, 1832(ra)
[0x80002c08]:sw a5, 1840(ra)
[0x80002c0c]:lw t3, 1944(a4)
[0x80002c10]:lw t4, 1948(a4)
[0x80002c14]:addi t3, zero, 3072
[0x80002c18]:lui t4, 229120
[0x80002c1c]:addi t4, t4, 4095
[0x80002c20]:addi a0, zero, 0
[0x80002c24]:csrrw zero, fcsr, a0
[0x80002c28]:fsqrt.d t5, t3, dyn
[0x80002c2c]:csrrs a5, fcsr, zero

[0x80002c28]:fsqrt.d t5, t3, dyn
[0x80002c2c]:csrrs a5, fcsr, zero
[0x80002c30]:sw t5, 1848(ra)
[0x80002c34]:sw a5, 1856(ra)
[0x80002c38]:lw t3, 1952(a4)
[0x80002c3c]:lw t4, 1956(a4)
[0x80002c40]:addi t3, zero, 1023
[0x80002c44]:lui t4, 228864
[0x80002c48]:addi a0, zero, 0
[0x80002c4c]:csrrw zero, fcsr, a0
[0x80002c50]:fsqrt.d t5, t3, dyn
[0x80002c54]:csrrs a5, fcsr, zero

[0x80002c50]:fsqrt.d t5, t3, dyn
[0x80002c54]:csrrs a5, fcsr, zero
[0x80002c58]:sw t5, 1864(ra)
[0x80002c5c]:sw a5, 1872(ra)
[0x80002c60]:lw t3, 1960(a4)
[0x80002c64]:lw t4, 1964(a4)
[0x80002c68]:addi t3, zero, 3584
[0x80002c6c]:lui t4, 229120
[0x80002c70]:addi t4, t4, 4095
[0x80002c74]:addi a0, zero, 0
[0x80002c78]:csrrw zero, fcsr, a0
[0x80002c7c]:fsqrt.d t5, t3, dyn
[0x80002c80]:csrrs a5, fcsr, zero

[0x80002c7c]:fsqrt.d t5, t3, dyn
[0x80002c80]:csrrs a5, fcsr, zero
[0x80002c84]:sw t5, 1880(ra)
[0x80002c88]:sw a5, 1888(ra)
[0x80002c8c]:lw t3, 1968(a4)
[0x80002c90]:lw t4, 1972(a4)
[0x80002c94]:addi t3, zero, 511
[0x80002c98]:lui t4, 228864
[0x80002c9c]:addi a0, zero, 0
[0x80002ca0]:csrrw zero, fcsr, a0
[0x80002ca4]:fsqrt.d t5, t3, dyn
[0x80002ca8]:csrrs a5, fcsr, zero

[0x80002ca4]:fsqrt.d t5, t3, dyn
[0x80002ca8]:csrrs a5, fcsr, zero
[0x80002cac]:sw t5, 1896(ra)
[0x80002cb0]:sw a5, 1904(ra)
[0x80002cb4]:lw t3, 1976(a4)
[0x80002cb8]:lw t4, 1980(a4)
[0x80002cbc]:addi t3, zero, 3840
[0x80002cc0]:lui t4, 229120
[0x80002cc4]:addi t4, t4, 4095
[0x80002cc8]:addi a0, zero, 0
[0x80002ccc]:csrrw zero, fcsr, a0
[0x80002cd0]:fsqrt.d t5, t3, dyn
[0x80002cd4]:csrrs a5, fcsr, zero

[0x80002cd0]:fsqrt.d t5, t3, dyn
[0x80002cd4]:csrrs a5, fcsr, zero
[0x80002cd8]:sw t5, 1912(ra)
[0x80002cdc]:sw a5, 1920(ra)
[0x80002ce0]:lw t3, 1984(a4)
[0x80002ce4]:lw t4, 1988(a4)
[0x80002ce8]:addi t3, zero, 255
[0x80002cec]:lui t4, 228864
[0x80002cf0]:addi a0, zero, 0
[0x80002cf4]:csrrw zero, fcsr, a0
[0x80002cf8]:fsqrt.d t5, t3, dyn
[0x80002cfc]:csrrs a5, fcsr, zero

[0x80002cf8]:fsqrt.d t5, t3, dyn
[0x80002cfc]:csrrs a5, fcsr, zero
[0x80002d00]:sw t5, 1928(ra)
[0x80002d04]:sw a5, 1936(ra)
[0x80002d08]:lw t3, 1992(a4)
[0x80002d0c]:lw t4, 1996(a4)
[0x80002d10]:addi t3, zero, 3968
[0x80002d14]:lui t4, 229120
[0x80002d18]:addi t4, t4, 4095
[0x80002d1c]:addi a0, zero, 0
[0x80002d20]:csrrw zero, fcsr, a0
[0x80002d24]:fsqrt.d t5, t3, dyn
[0x80002d28]:csrrs a5, fcsr, zero

[0x80002d24]:fsqrt.d t5, t3, dyn
[0x80002d28]:csrrs a5, fcsr, zero
[0x80002d2c]:sw t5, 1944(ra)
[0x80002d30]:sw a5, 1952(ra)
[0x80002d34]:lw t3, 2000(a4)
[0x80002d38]:lw t4, 2004(a4)
[0x80002d3c]:addi t3, zero, 127
[0x80002d40]:lui t4, 228864
[0x80002d44]:addi a0, zero, 0
[0x80002d48]:csrrw zero, fcsr, a0
[0x80002d4c]:fsqrt.d t5, t3, dyn
[0x80002d50]:csrrs a5, fcsr, zero

[0x80002d4c]:fsqrt.d t5, t3, dyn
[0x80002d50]:csrrs a5, fcsr, zero
[0x80002d54]:sw t5, 1960(ra)
[0x80002d58]:sw a5, 1968(ra)
[0x80002d5c]:lw t3, 2008(a4)
[0x80002d60]:lw t4, 2012(a4)
[0x80002d64]:addi t3, zero, 4032
[0x80002d68]:lui t4, 229120
[0x80002d6c]:addi t4, t4, 4095
[0x80002d70]:addi a0, zero, 0
[0x80002d74]:csrrw zero, fcsr, a0
[0x80002d78]:fsqrt.d t5, t3, dyn
[0x80002d7c]:csrrs a5, fcsr, zero

[0x80002d78]:fsqrt.d t5, t3, dyn
[0x80002d7c]:csrrs a5, fcsr, zero
[0x80002d80]:sw t5, 1976(ra)
[0x80002d84]:sw a5, 1984(ra)
[0x80002d88]:lw t3, 2016(a4)
[0x80002d8c]:lw t4, 2020(a4)
[0x80002d90]:addi t3, zero, 63
[0x80002d94]:lui t4, 228864
[0x80002d98]:addi a0, zero, 0
[0x80002d9c]:csrrw zero, fcsr, a0
[0x80002da0]:fsqrt.d t5, t3, dyn
[0x80002da4]:csrrs a5, fcsr, zero

[0x80002da0]:fsqrt.d t5, t3, dyn
[0x80002da4]:csrrs a5, fcsr, zero
[0x80002da8]:sw t5, 1992(ra)
[0x80002dac]:sw a5, 2000(ra)
[0x80002db0]:lw t3, 2024(a4)
[0x80002db4]:lw t4, 2028(a4)
[0x80002db8]:addi t3, zero, 4064
[0x80002dbc]:lui t4, 229120
[0x80002dc0]:addi t4, t4, 4095
[0x80002dc4]:addi a0, zero, 0
[0x80002dc8]:csrrw zero, fcsr, a0
[0x80002dcc]:fsqrt.d t5, t3, dyn
[0x80002dd0]:csrrs a5, fcsr, zero

[0x80002dcc]:fsqrt.d t5, t3, dyn
[0x80002dd0]:csrrs a5, fcsr, zero
[0x80002dd4]:sw t5, 2008(ra)
[0x80002dd8]:sw a5, 2016(ra)
[0x80002ddc]:lw t3, 2032(a4)
[0x80002de0]:lw t4, 2036(a4)
[0x80002de4]:addi t3, zero, 31
[0x80002de8]:lui t4, 228864
[0x80002dec]:addi a0, zero, 0
[0x80002df0]:csrrw zero, fcsr, a0
[0x80002df4]:fsqrt.d t5, t3, dyn
[0x80002df8]:csrrs a5, fcsr, zero

[0x80002df4]:fsqrt.d t5, t3, dyn
[0x80002df8]:csrrs a5, fcsr, zero
[0x80002dfc]:sw t5, 2024(ra)
[0x80002e00]:sw a5, 2032(ra)
[0x80002e04]:lw t3, 2040(a4)
[0x80002e08]:lw t4, 2044(a4)
[0x80002e0c]:addi t3, zero, 4080
[0x80002e10]:lui t4, 229120
[0x80002e14]:addi t4, t4, 4095
[0x80002e18]:addi a0, zero, 0
[0x80002e1c]:csrrw zero, fcsr, a0
[0x80002e20]:fsqrt.d t5, t3, dyn
[0x80002e24]:csrrs a5, fcsr, zero

[0x80002e20]:fsqrt.d t5, t3, dyn
[0x80002e24]:csrrs a5, fcsr, zero
[0x80002e28]:sw t5, 2040(ra)
[0x80002e2c]:addi ra, ra, 2040
[0x80002e30]:sw a5, 8(ra)
[0x80002e34]:auipc ra, 5
[0x80002e38]:addi ra, ra, 2116
[0x80002e3c]:lw t3, 0(a4)
[0x80002e40]:lw t4, 4(a4)
[0x80002e44]:addi t3, zero, 15
[0x80002e48]:lui t4, 228864
[0x80002e4c]:addi a0, zero, 0
[0x80002e50]:csrrw zero, fcsr, a0
[0x80002e54]:fsqrt.d t5, t3, dyn
[0x80002e58]:csrrs a5, fcsr, zero

[0x8000381c]:fsqrt.d t5, t3, dyn
[0x80003820]:csrrs a5, fcsr, zero
[0x80003824]:sw t5, 944(ra)
[0x80003828]:sw a5, 952(ra)
[0x8000382c]:lw t3, 480(a4)
[0x80003830]:lw t4, 484(a4)
[0x80003834]:lui t3, 1024
[0x80003838]:lui t4, 294848
[0x8000383c]:addi a0, zero, 0
[0x80003840]:csrrw zero, fcsr, a0
[0x80003844]:fsqrt.d t5, t3, dyn
[0x80003848]:csrrs a5, fcsr, zero

[0x80003844]:fsqrt.d t5, t3, dyn
[0x80003848]:csrrs a5, fcsr, zero
[0x8000384c]:sw t5, 960(ra)
[0x80003850]:sw a5, 968(ra)
[0x80003854]:lw t3, 488(a4)
[0x80003858]:lw t4, 492(a4)
[0x8000385c]:lui t3, 1047552
[0x80003860]:lui t4, 294880
[0x80003864]:addi t4, t4, 4095
[0x80003868]:addi a0, zero, 0
[0x8000386c]:csrrw zero, fcsr, a0
[0x80003870]:fsqrt.d t5, t3, dyn
[0x80003874]:csrrs a5, fcsr, zero

[0x80003870]:fsqrt.d t5, t3, dyn
[0x80003874]:csrrs a5, fcsr, zero
[0x80003878]:sw t5, 976(ra)
[0x8000387c]:sw a5, 984(ra)
[0x80003880]:lw t3, 496(a4)
[0x80003884]:lw t4, 500(a4)
[0x80003888]:lui t3, 512
[0x8000388c]:lui t4, 294848
[0x80003890]:addi a0, zero, 0
[0x80003894]:csrrw zero, fcsr, a0
[0x80003898]:fsqrt.d t5, t3, dyn
[0x8000389c]:csrrs a5, fcsr, zero

[0x80003898]:fsqrt.d t5, t3, dyn
[0x8000389c]:csrrs a5, fcsr, zero
[0x800038a0]:sw t5, 992(ra)
[0x800038a4]:sw a5, 1000(ra)
[0x800038a8]:lw t3, 504(a4)
[0x800038ac]:lw t4, 508(a4)
[0x800038b0]:lui t3, 1048064
[0x800038b4]:lui t4, 294880
[0x800038b8]:addi t4, t4, 4095
[0x800038bc]:addi a0, zero, 0
[0x800038c0]:csrrw zero, fcsr, a0
[0x800038c4]:fsqrt.d t5, t3, dyn
[0x800038c8]:csrrs a5, fcsr, zero

[0x800038c4]:fsqrt.d t5, t3, dyn
[0x800038c8]:csrrs a5, fcsr, zero
[0x800038cc]:sw t5, 1008(ra)
[0x800038d0]:sw a5, 1016(ra)
[0x800038d4]:lw t3, 512(a4)
[0x800038d8]:lw t4, 516(a4)
[0x800038dc]:lui t3, 256
[0x800038e0]:lui t4, 294848
[0x800038e4]:addi a0, zero, 0
[0x800038e8]:csrrw zero, fcsr, a0
[0x800038ec]:fsqrt.d t5, t3, dyn
[0x800038f0]:csrrs a5, fcsr, zero

[0x800038ec]:fsqrt.d t5, t3, dyn
[0x800038f0]:csrrs a5, fcsr, zero
[0x800038f4]:sw t5, 1024(ra)
[0x800038f8]:sw a5, 1032(ra)
[0x800038fc]:lw t3, 520(a4)
[0x80003900]:lw t4, 524(a4)
[0x80003904]:lui t3, 1048320
[0x80003908]:lui t4, 294880
[0x8000390c]:addi t4, t4, 4095
[0x80003910]:addi a0, zero, 0
[0x80003914]:csrrw zero, fcsr, a0
[0x80003918]:fsqrt.d t5, t3, dyn
[0x8000391c]:csrrs a5, fcsr, zero

[0x80003918]:fsqrt.d t5, t3, dyn
[0x8000391c]:csrrs a5, fcsr, zero
[0x80003920]:sw t5, 1040(ra)
[0x80003924]:sw a5, 1048(ra)
[0x80003928]:lw t3, 528(a4)
[0x8000392c]:lw t4, 532(a4)
[0x80003930]:lui t3, 128
[0x80003934]:lui t4, 294848
[0x80003938]:addi a0, zero, 0
[0x8000393c]:csrrw zero, fcsr, a0
[0x80003940]:fsqrt.d t5, t3, dyn
[0x80003944]:csrrs a5, fcsr, zero

[0x80003940]:fsqrt.d t5, t3, dyn
[0x80003944]:csrrs a5, fcsr, zero
[0x80003948]:sw t5, 1056(ra)
[0x8000394c]:sw a5, 1064(ra)
[0x80003950]:lw t3, 536(a4)
[0x80003954]:lw t4, 540(a4)
[0x80003958]:lui t3, 1048448
[0x8000395c]:lui t4, 294880
[0x80003960]:addi t4, t4, 4095
[0x80003964]:addi a0, zero, 0
[0x80003968]:csrrw zero, fcsr, a0
[0x8000396c]:fsqrt.d t5, t3, dyn
[0x80003970]:csrrs a5, fcsr, zero

[0x8000396c]:fsqrt.d t5, t3, dyn
[0x80003970]:csrrs a5, fcsr, zero
[0x80003974]:sw t5, 1072(ra)
[0x80003978]:sw a5, 1080(ra)
[0x8000397c]:lw t3, 544(a4)
[0x80003980]:lw t4, 548(a4)
[0x80003984]:lui t3, 64
[0x80003988]:lui t4, 294848
[0x8000398c]:addi a0, zero, 0
[0x80003990]:csrrw zero, fcsr, a0
[0x80003994]:fsqrt.d t5, t3, dyn
[0x80003998]:csrrs a5, fcsr, zero

[0x80003994]:fsqrt.d t5, t3, dyn
[0x80003998]:csrrs a5, fcsr, zero
[0x8000399c]:sw t5, 1088(ra)
[0x800039a0]:sw a5, 1096(ra)
[0x800039a4]:lw t3, 552(a4)
[0x800039a8]:lw t4, 556(a4)
[0x800039ac]:lui t3, 1048512
[0x800039b0]:lui t4, 294880
[0x800039b4]:addi t4, t4, 4095
[0x800039b8]:addi a0, zero, 0
[0x800039bc]:csrrw zero, fcsr, a0
[0x800039c0]:fsqrt.d t5, t3, dyn
[0x800039c4]:csrrs a5, fcsr, zero

[0x800039c0]:fsqrt.d t5, t3, dyn
[0x800039c4]:csrrs a5, fcsr, zero
[0x800039c8]:sw t5, 1104(ra)
[0x800039cc]:sw a5, 1112(ra)
[0x800039d0]:lw t3, 560(a4)
[0x800039d4]:lw t4, 564(a4)
[0x800039d8]:lui t3, 32
[0x800039dc]:lui t4, 294848
[0x800039e0]:addi a0, zero, 0
[0x800039e4]:csrrw zero, fcsr, a0
[0x800039e8]:fsqrt.d t5, t3, dyn
[0x800039ec]:csrrs a5, fcsr, zero

[0x800039e8]:fsqrt.d t5, t3, dyn
[0x800039ec]:csrrs a5, fcsr, zero
[0x800039f0]:sw t5, 1120(ra)
[0x800039f4]:sw a5, 1128(ra)
[0x800039f8]:lw t3, 568(a4)
[0x800039fc]:lw t4, 572(a4)
[0x80003a00]:lui t3, 1048544
[0x80003a04]:lui t4, 294880
[0x80003a08]:addi t4, t4, 4095
[0x80003a0c]:addi a0, zero, 0
[0x80003a10]:csrrw zero, fcsr, a0
[0x80003a14]:fsqrt.d t5, t3, dyn
[0x80003a18]:csrrs a5, fcsr, zero

[0x80003a14]:fsqrt.d t5, t3, dyn
[0x80003a18]:csrrs a5, fcsr, zero
[0x80003a1c]:sw t5, 1136(ra)
[0x80003a20]:sw a5, 1144(ra)
[0x80003a24]:lw t3, 576(a4)
[0x80003a28]:lw t4, 580(a4)
[0x80003a2c]:lui t3, 16
[0x80003a30]:lui t4, 294848
[0x80003a34]:addi a0, zero, 0
[0x80003a38]:csrrw zero, fcsr, a0
[0x80003a3c]:fsqrt.d t5, t3, dyn
[0x80003a40]:csrrs a5, fcsr, zero

[0x80003a3c]:fsqrt.d t5, t3, dyn
[0x80003a40]:csrrs a5, fcsr, zero
[0x80003a44]:sw t5, 1152(ra)
[0x80003a48]:sw a5, 1160(ra)
[0x80003a4c]:lw t3, 584(a4)
[0x80003a50]:lw t4, 588(a4)
[0x80003a54]:lui t3, 1048560
[0x80003a58]:lui t4, 294880
[0x80003a5c]:addi t4, t4, 4095
[0x80003a60]:addi a0, zero, 0
[0x80003a64]:csrrw zero, fcsr, a0
[0x80003a68]:fsqrt.d t5, t3, dyn
[0x80003a6c]:csrrs a5, fcsr, zero

[0x80003a68]:fsqrt.d t5, t3, dyn
[0x80003a6c]:csrrs a5, fcsr, zero
[0x80003a70]:sw t5, 1168(ra)
[0x80003a74]:sw a5, 1176(ra)
[0x80003a78]:lw t3, 592(a4)
[0x80003a7c]:lw t4, 596(a4)
[0x80003a80]:lui t3, 8
[0x80003a84]:lui t4, 294848
[0x80003a88]:addi a0, zero, 0
[0x80003a8c]:csrrw zero, fcsr, a0
[0x80003a90]:fsqrt.d t5, t3, dyn
[0x80003a94]:csrrs a5, fcsr, zero

[0x80003a90]:fsqrt.d t5, t3, dyn
[0x80003a94]:csrrs a5, fcsr, zero
[0x80003a98]:sw t5, 1184(ra)
[0x80003a9c]:sw a5, 1192(ra)
[0x80003aa0]:lw t3, 600(a4)
[0x80003aa4]:lw t4, 604(a4)
[0x80003aa8]:lui t3, 1048568
[0x80003aac]:lui t4, 294880
[0x80003ab0]:addi t4, t4, 4095
[0x80003ab4]:addi a0, zero, 0
[0x80003ab8]:csrrw zero, fcsr, a0
[0x80003abc]:fsqrt.d t5, t3, dyn
[0x80003ac0]:csrrs a5, fcsr, zero

[0x80003abc]:fsqrt.d t5, t3, dyn
[0x80003ac0]:csrrs a5, fcsr, zero
[0x80003ac4]:sw t5, 1200(ra)
[0x80003ac8]:sw a5, 1208(ra)
[0x80003acc]:lw t3, 608(a4)
[0x80003ad0]:lw t4, 612(a4)
[0x80003ad4]:lui t3, 4
[0x80003ad8]:lui t4, 294848
[0x80003adc]:addi a0, zero, 0
[0x80003ae0]:csrrw zero, fcsr, a0
[0x80003ae4]:fsqrt.d t5, t3, dyn
[0x80003ae8]:csrrs a5, fcsr, zero

[0x80003ae4]:fsqrt.d t5, t3, dyn
[0x80003ae8]:csrrs a5, fcsr, zero
[0x80003aec]:sw t5, 1216(ra)
[0x80003af0]:sw a5, 1224(ra)
[0x80003af4]:lw t3, 616(a4)
[0x80003af8]:lw t4, 620(a4)
[0x80003afc]:lui t3, 1048572
[0x80003b00]:lui t4, 294880
[0x80003b04]:addi t4, t4, 4095
[0x80003b08]:addi a0, zero, 0
[0x80003b0c]:csrrw zero, fcsr, a0
[0x80003b10]:fsqrt.d t5, t3, dyn
[0x80003b14]:csrrs a5, fcsr, zero

[0x80003b10]:fsqrt.d t5, t3, dyn
[0x80003b14]:csrrs a5, fcsr, zero
[0x80003b18]:sw t5, 1232(ra)
[0x80003b1c]:sw a5, 1240(ra)
[0x80003b20]:lw t3, 624(a4)
[0x80003b24]:lw t4, 628(a4)
[0x80003b28]:lui t3, 2
[0x80003b2c]:lui t4, 294848
[0x80003b30]:addi a0, zero, 0
[0x80003b34]:csrrw zero, fcsr, a0
[0x80003b38]:fsqrt.d t5, t3, dyn
[0x80003b3c]:csrrs a5, fcsr, zero

[0x80003b38]:fsqrt.d t5, t3, dyn
[0x80003b3c]:csrrs a5, fcsr, zero
[0x80003b40]:sw t5, 1248(ra)
[0x80003b44]:sw a5, 1256(ra)
[0x80003b48]:lw t3, 632(a4)
[0x80003b4c]:lw t4, 636(a4)
[0x80003b50]:lui t3, 1048574
[0x80003b54]:lui t4, 294880
[0x80003b58]:addi t4, t4, 4095
[0x80003b5c]:addi a0, zero, 0
[0x80003b60]:csrrw zero, fcsr, a0
[0x80003b64]:fsqrt.d t5, t3, dyn
[0x80003b68]:csrrs a5, fcsr, zero

[0x80003b64]:fsqrt.d t5, t3, dyn
[0x80003b68]:csrrs a5, fcsr, zero
[0x80003b6c]:sw t5, 1264(ra)
[0x80003b70]:sw a5, 1272(ra)
[0x80003b74]:lw t3, 640(a4)
[0x80003b78]:lw t4, 644(a4)
[0x80003b7c]:lui t3, 1
[0x80003b80]:lui t4, 294848
[0x80003b84]:addi a0, zero, 0
[0x80003b88]:csrrw zero, fcsr, a0
[0x80003b8c]:fsqrt.d t5, t3, dyn
[0x80003b90]:csrrs a5, fcsr, zero

[0x80003b8c]:fsqrt.d t5, t3, dyn
[0x80003b90]:csrrs a5, fcsr, zero
[0x80003b94]:sw t5, 1280(ra)
[0x80003b98]:sw a5, 1288(ra)
[0x80003b9c]:lw t3, 648(a4)
[0x80003ba0]:lw t4, 652(a4)
[0x80003ba4]:lui t3, 1048575
[0x80003ba8]:lui t4, 294880
[0x80003bac]:addi t4, t4, 4095
[0x80003bb0]:addi a0, zero, 0
[0x80003bb4]:csrrw zero, fcsr, a0
[0x80003bb8]:fsqrt.d t5, t3, dyn
[0x80003bbc]:csrrs a5, fcsr, zero

[0x80003bb8]:fsqrt.d t5, t3, dyn
[0x80003bbc]:csrrs a5, fcsr, zero
[0x80003bc0]:sw t5, 1296(ra)
[0x80003bc4]:sw a5, 1304(ra)
[0x80003bc8]:lw t3, 656(a4)
[0x80003bcc]:lw t4, 660(a4)
[0x80003bd0]:lui t3, 1
[0x80003bd4]:addi t3, t3, 2048
[0x80003bd8]:lui t4, 294848
[0x80003bdc]:addi a0, zero, 0
[0x80003be0]:csrrw zero, fcsr, a0
[0x80003be4]:fsqrt.d t5, t3, dyn
[0x80003be8]:csrrs a5, fcsr, zero

[0x80003be4]:fsqrt.d t5, t3, dyn
[0x80003be8]:csrrs a5, fcsr, zero
[0x80003bec]:sw t5, 1312(ra)
[0x80003bf0]:sw a5, 1320(ra)
[0x80003bf4]:lw t3, 664(a4)
[0x80003bf8]:lw t4, 668(a4)
[0x80003bfc]:addi t3, zero, 2048
[0x80003c00]:lui t4, 294880
[0x80003c04]:addi t4, t4, 4095
[0x80003c08]:addi a0, zero, 0
[0x80003c0c]:csrrw zero, fcsr, a0
[0x80003c10]:fsqrt.d t5, t3, dyn
[0x80003c14]:csrrs a5, fcsr, zero

[0x80003c10]:fsqrt.d t5, t3, dyn
[0x80003c14]:csrrs a5, fcsr, zero
[0x80003c18]:sw t5, 1328(ra)
[0x80003c1c]:sw a5, 1336(ra)
[0x80003c20]:lw t3, 672(a4)
[0x80003c24]:lw t4, 676(a4)
[0x80003c28]:addi t3, zero, 1024
[0x80003c2c]:lui t4, 294848
[0x80003c30]:addi a0, zero, 0
[0x80003c34]:csrrw zero, fcsr, a0
[0x80003c38]:fsqrt.d t5, t3, dyn
[0x80003c3c]:csrrs a5, fcsr, zero

[0x80003c38]:fsqrt.d t5, t3, dyn
[0x80003c3c]:csrrs a5, fcsr, zero
[0x80003c40]:sw t5, 1344(ra)
[0x80003c44]:sw a5, 1352(ra)
[0x80003c48]:lw t3, 680(a4)
[0x80003c4c]:lw t4, 684(a4)
[0x80003c50]:addi t3, zero, 3072
[0x80003c54]:lui t4, 294880
[0x80003c58]:addi t4, t4, 4095
[0x80003c5c]:addi a0, zero, 0
[0x80003c60]:csrrw zero, fcsr, a0
[0x80003c64]:fsqrt.d t5, t3, dyn
[0x80003c68]:csrrs a5, fcsr, zero

[0x80003c64]:fsqrt.d t5, t3, dyn
[0x80003c68]:csrrs a5, fcsr, zero
[0x80003c6c]:sw t5, 1360(ra)
[0x80003c70]:sw a5, 1368(ra)
[0x80003c74]:lw t3, 688(a4)
[0x80003c78]:lw t4, 692(a4)
[0x80003c7c]:addi t3, zero, 512
[0x80003c80]:lui t4, 294848
[0x80003c84]:addi a0, zero, 0
[0x80003c88]:csrrw zero, fcsr, a0
[0x80003c8c]:fsqrt.d t5, t3, dyn
[0x80003c90]:csrrs a5, fcsr, zero

[0x80003c8c]:fsqrt.d t5, t3, dyn
[0x80003c90]:csrrs a5, fcsr, zero
[0x80003c94]:sw t5, 1376(ra)
[0x80003c98]:sw a5, 1384(ra)
[0x80003c9c]:lw t3, 696(a4)
[0x80003ca0]:lw t4, 700(a4)
[0x80003ca4]:addi t3, zero, 3584
[0x80003ca8]:lui t4, 294880
[0x80003cac]:addi t4, t4, 4095
[0x80003cb0]:addi a0, zero, 0
[0x80003cb4]:csrrw zero, fcsr, a0
[0x80003cb8]:fsqrt.d t5, t3, dyn
[0x80003cbc]:csrrs a5, fcsr, zero

[0x80003cb8]:fsqrt.d t5, t3, dyn
[0x80003cbc]:csrrs a5, fcsr, zero
[0x80003cc0]:sw t5, 1392(ra)
[0x80003cc4]:sw a5, 1400(ra)
[0x80003cc8]:lw t3, 704(a4)
[0x80003ccc]:lw t4, 708(a4)
[0x80003cd0]:addi t3, zero, 256
[0x80003cd4]:lui t4, 294848
[0x80003cd8]:addi a0, zero, 0
[0x80003cdc]:csrrw zero, fcsr, a0
[0x80003ce0]:fsqrt.d t5, t3, dyn
[0x80003ce4]:csrrs a5, fcsr, zero

[0x80003ce0]:fsqrt.d t5, t3, dyn
[0x80003ce4]:csrrs a5, fcsr, zero
[0x80003ce8]:sw t5, 1408(ra)
[0x80003cec]:sw a5, 1416(ra)
[0x80003cf0]:lw t3, 712(a4)
[0x80003cf4]:lw t4, 716(a4)
[0x80003cf8]:addi t3, zero, 3840
[0x80003cfc]:lui t4, 294880
[0x80003d00]:addi t4, t4, 4095
[0x80003d04]:addi a0, zero, 0
[0x80003d08]:csrrw zero, fcsr, a0
[0x80003d0c]:fsqrt.d t5, t3, dyn
[0x80003d10]:csrrs a5, fcsr, zero

[0x80003d0c]:fsqrt.d t5, t3, dyn
[0x80003d10]:csrrs a5, fcsr, zero
[0x80003d14]:sw t5, 1424(ra)
[0x80003d18]:sw a5, 1432(ra)
[0x80003d1c]:lw t3, 720(a4)
[0x80003d20]:lw t4, 724(a4)
[0x80003d24]:addi t3, zero, 128
[0x80003d28]:lui t4, 294848
[0x80003d2c]:addi a0, zero, 0
[0x80003d30]:csrrw zero, fcsr, a0
[0x80003d34]:fsqrt.d t5, t3, dyn
[0x80003d38]:csrrs a5, fcsr, zero

[0x80003d34]:fsqrt.d t5, t3, dyn
[0x80003d38]:csrrs a5, fcsr, zero
[0x80003d3c]:sw t5, 1440(ra)
[0x80003d40]:sw a5, 1448(ra)
[0x80003d44]:lw t3, 728(a4)
[0x80003d48]:lw t4, 732(a4)
[0x80003d4c]:addi t3, zero, 3968
[0x80003d50]:lui t4, 294880
[0x80003d54]:addi t4, t4, 4095
[0x80003d58]:addi a0, zero, 0
[0x80003d5c]:csrrw zero, fcsr, a0
[0x80003d60]:fsqrt.d t5, t3, dyn
[0x80003d64]:csrrs a5, fcsr, zero

[0x80003d60]:fsqrt.d t5, t3, dyn
[0x80003d64]:csrrs a5, fcsr, zero
[0x80003d68]:sw t5, 1456(ra)
[0x80003d6c]:sw a5, 1464(ra)
[0x80003d70]:lw t3, 736(a4)
[0x80003d74]:lw t4, 740(a4)
[0x80003d78]:addi t3, zero, 64
[0x80003d7c]:lui t4, 294848
[0x80003d80]:addi a0, zero, 0
[0x80003d84]:csrrw zero, fcsr, a0
[0x80003d88]:fsqrt.d t5, t3, dyn
[0x80003d8c]:csrrs a5, fcsr, zero

[0x80003d88]:fsqrt.d t5, t3, dyn
[0x80003d8c]:csrrs a5, fcsr, zero
[0x80003d90]:sw t5, 1472(ra)
[0x80003d94]:sw a5, 1480(ra)
[0x80003d98]:lw t3, 744(a4)
[0x80003d9c]:lw t4, 748(a4)
[0x80003da0]:addi t3, zero, 4032
[0x80003da4]:lui t4, 294880
[0x80003da8]:addi t4, t4, 4095
[0x80003dac]:addi a0, zero, 0
[0x80003db0]:csrrw zero, fcsr, a0
[0x80003db4]:fsqrt.d t5, t3, dyn
[0x80003db8]:csrrs a5, fcsr, zero

[0x80003db4]:fsqrt.d t5, t3, dyn
[0x80003db8]:csrrs a5, fcsr, zero
[0x80003dbc]:sw t5, 1488(ra)
[0x80003dc0]:sw a5, 1496(ra)
[0x80003dc4]:lw t3, 752(a4)
[0x80003dc8]:lw t4, 756(a4)
[0x80003dcc]:addi t3, zero, 32
[0x80003dd0]:lui t4, 294848
[0x80003dd4]:addi a0, zero, 0
[0x80003dd8]:csrrw zero, fcsr, a0
[0x80003ddc]:fsqrt.d t5, t3, dyn
[0x80003de0]:csrrs a5, fcsr, zero

[0x80003ddc]:fsqrt.d t5, t3, dyn
[0x80003de0]:csrrs a5, fcsr, zero
[0x80003de4]:sw t5, 1504(ra)
[0x80003de8]:sw a5, 1512(ra)
[0x80003dec]:lw t3, 760(a4)
[0x80003df0]:lw t4, 764(a4)
[0x80003df4]:addi t3, zero, 4064
[0x80003df8]:lui t4, 294880
[0x80003dfc]:addi t4, t4, 4095
[0x80003e00]:addi a0, zero, 0
[0x80003e04]:csrrw zero, fcsr, a0
[0x80003e08]:fsqrt.d t5, t3, dyn
[0x80003e0c]:csrrs a5, fcsr, zero

[0x80003e08]:fsqrt.d t5, t3, dyn
[0x80003e0c]:csrrs a5, fcsr, zero
[0x80003e10]:sw t5, 1520(ra)
[0x80003e14]:sw a5, 1528(ra)
[0x80003e18]:lw t3, 768(a4)
[0x80003e1c]:lw t4, 772(a4)
[0x80003e20]:addi t3, zero, 16
[0x80003e24]:lui t4, 294848
[0x80003e28]:addi a0, zero, 0
[0x80003e2c]:csrrw zero, fcsr, a0
[0x80003e30]:fsqrt.d t5, t3, dyn
[0x80003e34]:csrrs a5, fcsr, zero

[0x80003e30]:fsqrt.d t5, t3, dyn
[0x80003e34]:csrrs a5, fcsr, zero
[0x80003e38]:sw t5, 1536(ra)
[0x80003e3c]:sw a5, 1544(ra)
[0x80003e40]:lw t3, 776(a4)
[0x80003e44]:lw t4, 780(a4)
[0x80003e48]:addi t3, zero, 4080
[0x80003e4c]:lui t4, 294880
[0x80003e50]:addi t4, t4, 4095
[0x80003e54]:addi a0, zero, 0
[0x80003e58]:csrrw zero, fcsr, a0
[0x80003e5c]:fsqrt.d t5, t3, dyn
[0x80003e60]:csrrs a5, fcsr, zero

[0x80003e5c]:fsqrt.d t5, t3, dyn
[0x80003e60]:csrrs a5, fcsr, zero
[0x80003e64]:sw t5, 1552(ra)
[0x80003e68]:sw a5, 1560(ra)
[0x80003e6c]:lw t3, 784(a4)
[0x80003e70]:lw t4, 788(a4)
[0x80003e74]:addi t3, zero, 8
[0x80003e78]:lui t4, 294848
[0x80003e7c]:addi a0, zero, 0
[0x80003e80]:csrrw zero, fcsr, a0
[0x80003e84]:fsqrt.d t5, t3, dyn
[0x80003e88]:csrrs a5, fcsr, zero

[0x80003e84]:fsqrt.d t5, t3, dyn
[0x80003e88]:csrrs a5, fcsr, zero
[0x80003e8c]:sw t5, 1568(ra)
[0x80003e90]:sw a5, 1576(ra)
[0x80003e94]:lw t3, 792(a4)
[0x80003e98]:lw t4, 796(a4)
[0x80003e9c]:addi t3, zero, 4088
[0x80003ea0]:lui t4, 294880
[0x80003ea4]:addi t4, t4, 4095
[0x80003ea8]:addi a0, zero, 0
[0x80003eac]:csrrw zero, fcsr, a0
[0x80003eb0]:fsqrt.d t5, t3, dyn
[0x80003eb4]:csrrs a5, fcsr, zero

[0x80003eb0]:fsqrt.d t5, t3, dyn
[0x80003eb4]:csrrs a5, fcsr, zero
[0x80003eb8]:sw t5, 1584(ra)
[0x80003ebc]:sw a5, 1592(ra)
[0x80003ec0]:lw t3, 800(a4)
[0x80003ec4]:lw t4, 804(a4)
[0x80003ec8]:addi t3, zero, 4
[0x80003ecc]:lui t4, 294848
[0x80003ed0]:addi a0, zero, 0
[0x80003ed4]:csrrw zero, fcsr, a0
[0x80003ed8]:fsqrt.d t5, t3, dyn
[0x80003edc]:csrrs a5, fcsr, zero

[0x80003ed8]:fsqrt.d t5, t3, dyn
[0x80003edc]:csrrs a5, fcsr, zero
[0x80003ee0]:sw t5, 1600(ra)
[0x80003ee4]:sw a5, 1608(ra)
[0x80003ee8]:lw t3, 808(a4)
[0x80003eec]:lw t4, 812(a4)
[0x80003ef0]:addi t3, zero, 4092
[0x80003ef4]:lui t4, 294880
[0x80003ef8]:addi t4, t4, 4095
[0x80003efc]:addi a0, zero, 0
[0x80003f00]:csrrw zero, fcsr, a0
[0x80003f04]:fsqrt.d t5, t3, dyn
[0x80003f08]:csrrs a5, fcsr, zero

[0x80003f04]:fsqrt.d t5, t3, dyn
[0x80003f08]:csrrs a5, fcsr, zero
[0x80003f0c]:sw t5, 1616(ra)
[0x80003f10]:sw a5, 1624(ra)
[0x80003f14]:lw t3, 816(a4)
[0x80003f18]:lw t4, 820(a4)
[0x80003f1c]:addi t3, zero, 2
[0x80003f20]:lui t4, 294848
[0x80003f24]:addi a0, zero, 0
[0x80003f28]:csrrw zero, fcsr, a0
[0x80003f2c]:fsqrt.d t5, t3, dyn
[0x80003f30]:csrrs a5, fcsr, zero

[0x80003f2c]:fsqrt.d t5, t3, dyn
[0x80003f30]:csrrs a5, fcsr, zero
[0x80003f34]:sw t5, 1632(ra)
[0x80003f38]:sw a5, 1640(ra)
[0x80003f3c]:lw t3, 824(a4)
[0x80003f40]:lw t4, 828(a4)
[0x80003f44]:addi t3, zero, 4094
[0x80003f48]:lui t4, 294880
[0x80003f4c]:addi t4, t4, 4095
[0x80003f50]:addi a0, zero, 0
[0x80003f54]:csrrw zero, fcsr, a0
[0x80003f58]:fsqrt.d t5, t3, dyn
[0x80003f5c]:csrrs a5, fcsr, zero

[0x80003f58]:fsqrt.d t5, t3, dyn
[0x80003f5c]:csrrs a5, fcsr, zero
[0x80003f60]:sw t5, 1648(ra)
[0x80003f64]:sw a5, 1656(ra)
[0x80003f68]:lw t3, 832(a4)
[0x80003f6c]:lw t4, 836(a4)
[0x80003f70]:addi t3, zero, 4095
[0x80003f74]:lui t4, 294880
[0x80003f78]:addi t4, t4, 4095
[0x80003f7c]:addi a0, zero, 0
[0x80003f80]:csrrw zero, fcsr, a0
[0x80003f84]:fsqrt.d t5, t3, dyn
[0x80003f88]:csrrs a5, fcsr, zero

[0x80003f84]:fsqrt.d t5, t3, dyn
[0x80003f88]:csrrs a5, fcsr, zero
[0x80003f8c]:sw t5, 1664(ra)
[0x80003f90]:sw a5, 1672(ra)
[0x80003f94]:lw t3, 840(a4)
[0x80003f98]:lw t4, 844(a4)
[0x80003f9c]:lui t3, 748983
[0x80003fa0]:addi t3, t3, 3510
[0x80003fa4]:lui t4, 294862
[0x80003fa8]:addi t4, t4, 2925
[0x80003fac]:addi a0, zero, 0
[0x80003fb0]:csrrw zero, fcsr, a0
[0x80003fb4]:fsqrt.d t5, t3, dyn
[0x80003fb8]:csrrs a5, fcsr, zero

[0x80003fb4]:fsqrt.d t5, t3, dyn
[0x80003fb8]:csrrs a5, fcsr, zero
[0x80003fbc]:sw t5, 1680(ra)
[0x80003fc0]:sw a5, 1688(ra)
[0x80003fc4]:lw t3, 848(a4)
[0x80003fc8]:lw t4, 852(a4)
[0x80003fcc]:lui t3, 449390
[0x80003fd0]:addi t3, t3, 2926
[0x80003fd4]:lui t4, 294875
[0x80003fd8]:addi t4, t4, 1755
[0x80003fdc]:addi a0, zero, 0
[0x80003fe0]:csrrw zero, fcsr, a0
[0x80003fe4]:fsqrt.d t5, t3, dyn
[0x80003fe8]:csrrs a5, fcsr, zero

[0x80003fe4]:fsqrt.d t5, t3, dyn
[0x80003fe8]:csrrs a5, fcsr, zero
[0x80003fec]:sw t5, 1696(ra)
[0x80003ff0]:sw a5, 1704(ra)
[0x80003ff4]:lw t3, 856(a4)
[0x80003ff8]:lw t4, 860(a4)
[0x80003ffc]:lui t3, 419430
[0x80004000]:addi t3, t3, 1638
[0x80004004]:lui t4, 294854
[0x80004008]:addi t4, t4, 1638
[0x8000400c]:addi a0, zero, 0
[0x80004010]:csrrw zero, fcsr, a0
[0x80004014]:fsqrt.d t5, t3, dyn
[0x80004018]:csrrs a5, fcsr, zero

[0x80004014]:fsqrt.d t5, t3, dyn
[0x80004018]:csrrs a5, fcsr, zero
[0x8000401c]:sw t5, 1712(ra)
[0x80004020]:sw a5, 1720(ra)
[0x80004024]:lw t3, 864(a4)
[0x80004028]:lw t4, 868(a4)
[0x8000402c]:lui t3, 629146
[0x80004030]:addi t3, t3, 2458
[0x80004034]:lui t4, 294874
[0x80004038]:addi t4, t4, 2457
[0x8000403c]:addi a0, zero, 0
[0x80004040]:csrrw zero, fcsr, a0
[0x80004044]:fsqrt.d t5, t3, dyn
[0x80004048]:csrrs a5, fcsr, zero

[0x80004044]:fsqrt.d t5, t3, dyn
[0x80004048]:csrrs a5, fcsr, zero
[0x8000404c]:sw t5, 1728(ra)
[0x80004050]:sw a5, 1736(ra)
[0x80004054]:lw t3, 872(a4)
[0x80004058]:lw t4, 876(a4)
[0x8000405c]:lui t3, 978671
[0x80004060]:addi t3, t3, 3822
[0x80004064]:lui t4, 294863
[0x80004068]:addi t4, t4, 3822
[0x8000406c]:addi a0, zero, 0
[0x80004070]:csrrw zero, fcsr, a0
[0x80004074]:fsqrt.d t5, t3, dyn
[0x80004078]:csrrs a5, fcsr, zero

[0x80004074]:fsqrt.d t5, t3, dyn
[0x80004078]:csrrs a5, fcsr, zero
[0x8000407c]:sw t5, 1744(ra)
[0x80004080]:sw a5, 1752(ra)
[0x80004084]:lw t3, 880(a4)
[0x80004088]:lw t4, 884(a4)
[0x8000408c]:lui t3, 69905
[0x80004090]:addi t3, t3, 273
[0x80004094]:lui t4, 294865
[0x80004098]:addi t4, t4, 273
[0x8000409c]:addi a0, zero, 0
[0x800040a0]:csrrw zero, fcsr, a0
[0x800040a4]:fsqrt.d t5, t3, dyn
[0x800040a8]:csrrs a5, fcsr, zero

[0x800040a4]:fsqrt.d t5, t3, dyn
[0x800040a8]:csrrs a5, fcsr, zero
[0x800040ac]:sw t5, 1760(ra)
[0x800040b0]:sw a5, 1768(ra)
[0x800040b4]:lw t3, 888(a4)
[0x800040b8]:lw t4, 892(a4)
[0x800040bc]:lui t3, 149797
[0x800040c0]:addi t3, t3, 2340
[0x800040c4]:lui t4, 294857
[0x800040c8]:addi t4, t4, 585
[0x800040cc]:addi a0, zero, 0
[0x800040d0]:csrrw zero, fcsr, a0
[0x800040d4]:fsqrt.d t5, t3, dyn
[0x800040d8]:csrrs a5, fcsr, zero

[0x800040d4]:fsqrt.d t5, t3, dyn
[0x800040d8]:csrrs a5, fcsr, zero
[0x800040dc]:sw t5, 1776(ra)
[0x800040e0]:sw a5, 1784(ra)
[0x800040e4]:lw t3, 896(a4)
[0x800040e8]:lw t4, 900(a4)
[0x800040ec]:lui t3, 898779
[0x800040f0]:addi t3, t3, 1755
[0x800040f4]:lui t4, 294871
[0x800040f8]:addi t4, t4, 3510
[0x800040fc]:addi a0, zero, 0
[0x80004100]:csrrw zero, fcsr, a0
[0x80004104]:fsqrt.d t5, t3, dyn
[0x80004108]:csrrs a5, fcsr, zero

[0x80004104]:fsqrt.d t5, t3, dyn
[0x80004108]:csrrs a5, fcsr, zero
[0x8000410c]:sw t5, 1792(ra)
[0x80004110]:sw a5, 1800(ra)
[0x80004114]:lw t3, 904(a4)
[0x80004118]:lw t4, 908(a4)
[0x8000411c]:lui t3, 838861
[0x80004120]:addi t3, t3, 3276
[0x80004124]:lui t4, 294861
[0x80004128]:addi t4, t4, 3276
[0x8000412c]:addi a0, zero, 0
[0x80004130]:csrrw zero, fcsr, a0
[0x80004134]:fsqrt.d t5, t3, dyn
[0x80004138]:csrrs a5, fcsr, zero

[0x80004134]:fsqrt.d t5, t3, dyn
[0x80004138]:csrrs a5, fcsr, zero
[0x8000413c]:sw t5, 1808(ra)
[0x80004140]:sw a5, 1816(ra)
[0x80004144]:lw t3, 912(a4)
[0x80004148]:lw t4, 916(a4)
[0x8000414c]:lui t3, 209715
[0x80004150]:addi t3, t3, 819
[0x80004154]:lui t4, 294867
[0x80004158]:addi t4, t4, 819
[0x8000415c]:addi a0, zero, 0
[0x80004160]:csrrw zero, fcsr, a0
[0x80004164]:fsqrt.d t5, t3, dyn
[0x80004168]:csrrs a5, fcsr, zero

[0x80004164]:fsqrt.d t5, t3, dyn
[0x80004168]:csrrs a5, fcsr, zero
[0x8000416c]:sw t5, 1824(ra)
[0x80004170]:sw a5, 1832(ra)
[0x80004174]:addi zero, zero, 0
[0x80004178]:addi zero, zero, 0
[0x8000417c]:addi zero, zero, 0



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                          signature                           |                                                                                 coverpoints                                                                                 |                                                                                   code                                                                                    |
|---:|--------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80006e18]<br>0x3DEB8924<br> [0x80006e20]<br>0x00000001<br> |- mnemonic : fsqrt.d<br> - rs1 : x30<br> - rd : x30<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0x369 and fm1 == 0x6d601ad376ab9 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x80000134]:fsqrt.d t5, t5, dyn<br> [0x80000138]:csrrs tp, fcsr, zero<br> [0x8000013c]:sw t5, 0(ra)<br> [0x80000140]:sw tp, 8(ra)<br>                                     |
|   2|[0x80006e28]<br>0x667F3BCC<br> [0x80006e30]<br>0x00000001<br> |- rs1 : x26<br> - rd : x28<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffffffffe and  fcsr == 0x0 and rm_val == 7   #nosat<br>                          |[0x80000160]:fsqrt.d t3, s10, dyn<br> [0x80000164]:csrrs tp, fcsr, zero<br> [0x80000168]:sw t3, 16(ra)<br> [0x8000016c]:sw tp, 24(ra)<br>                                  |
|   3|[0x80006e38]<br>0x00000000<br> [0x80006e40]<br>0x00000000<br> |- rs1 : x28<br> - rd : x26<br> - fs1 == 0 and fe1 == 0x37d and fm1 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000188]:fsqrt.d s10, t3, dyn<br> [0x8000018c]:csrrs tp, fcsr, zero<br> [0x80000190]:sw s10, 32(ra)<br> [0x80000194]:sw tp, 40(ra)<br>                                 |
|   4|[0x80006e48]<br>0xFFFFFFFE<br> [0x80006e50]<br>0x00000001<br> |- rs1 : x22<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x37c and fm1 == 0xffffffffffffc and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800001b4]:fsqrt.d s8, s6, dyn<br> [0x800001b8]:csrrs tp, fcsr, zero<br> [0x800001bc]:sw s8, 48(ra)<br> [0x800001c0]:sw tp, 56(ra)<br>                                   |
|   5|[0x80006e58]<br>0x1409212E<br> [0x80006e60]<br>0x00000001<br> |- rs1 : x24<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x37d and fm1 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800001dc]:fsqrt.d s6, s8, dyn<br> [0x800001e0]:csrrs tp, fcsr, zero<br> [0x800001e4]:sw s6, 64(ra)<br> [0x800001e8]:sw tp, 72(ra)<br>                                   |
|   6|[0x80006e68]<br>0x667F3BCA<br> [0x80006e70]<br>0x00000001<br> |- rs1 : x18<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x37b and fm1 == 0xffffffffffff8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000208]:fsqrt.d s4, s2, dyn<br> [0x8000020c]:csrrs tp, fcsr, zero<br> [0x80000210]:sw s4, 80(ra)<br> [0x80000214]:sw tp, 88(ra)<br>                                   |
|   7|[0x80006e78]<br>0xA9D2F8EA<br> [0x80006e80]<br>0x00000001<br> |- rs1 : x20<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x37d and fm1 == 0xc000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000230]:fsqrt.d s2, s4, dyn<br> [0x80000234]:csrrs tp, fcsr, zero<br> [0x80000238]:sw s2, 96(ra)<br> [0x8000023c]:sw tp, 104(ra)<br>                                  |
|   8|[0x80006e88]<br>0xFFFFFFF8<br> [0x80006e90]<br>0x00000001<br> |- rs1 : x14<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x37a and fm1 == 0xffffffffffff0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x8000025c]:fsqrt.d a6, a4, dyn<br> [0x80000260]:csrrs tp, fcsr, zero<br> [0x80000264]:sw a6, 112(ra)<br> [0x80000268]:sw tp, 120(ra)<br>                                 |
|   9|[0x80006e98]<br>0xD236A58F<br> [0x80006ea0]<br>0x00000001<br> |- rs1 : x16<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x37d and fm1 == 0xe000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000284]:fsqrt.d a4, a6, dyn<br> [0x80000288]:csrrs tp, fcsr, zero<br> [0x8000028c]:sw a4, 128(ra)<br> [0x80000290]:sw tp, 136(ra)<br>                                 |
|  10|[0x80006ea8]<br>0x667F3BC1<br> [0x80006eb0]<br>0x00000001<br> |- rs1 : x10<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x379 and fm1 == 0xfffffffffffe0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800002b0]:fsqrt.d a2, a0, dyn<br> [0x800002b4]:csrrs tp, fcsr, zero<br> [0x800002b8]:sw a2, 144(ra)<br> [0x800002bc]:sw tp, 152(ra)<br>                                 |
|  11|[0x80006eb8]<br>0x0568C1C3<br> [0x80006ec0]<br>0x00000001<br> |- rs1 : x12<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x37d and fm1 == 0xf000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800002e0]:fsqrt.d a0, a2, dyn<br> [0x800002e4]:csrrs a5, fcsr, zero<br> [0x800002e8]:sw a0, 160(ra)<br> [0x800002ec]:sw a5, 168(ra)<br>                                 |
|  12|[0x80006ec8]<br>0xFFFFFFE0<br> [0x80006ed0]<br>0x00000001<br> |- rs1 : x6<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x378 and fm1 == 0xfffffffffffc0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x8000030c]:fsqrt.d fp, t1, dyn<br> [0x80000310]:csrrs a5, fcsr, zero<br> [0x80000314]:sw fp, 176(ra)<br> [0x80000318]:sw a5, 184(ra)<br>                                 |
|  13|[0x80006e78]<br>0x8D0E2F77<br> [0x80006e80]<br>0x00000001<br> |- rs1 : x8<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x37d and fm1 == 0xf800000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x8000033c]:fsqrt.d t1, fp, dyn<br> [0x80000340]:csrrs a5, fcsr, zero<br> [0x80000344]:sw t1, 0(ra)<br> [0x80000348]:sw a5, 8(ra)<br>                                     |
|  14|[0x80006e88]<br>0x667F3B9F<br> [0x80006e90]<br>0x00000001<br> |- rs1 : x2<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x377 and fm1 == 0xfffffffffff80 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x80000368]:fsqrt.d tp, sp, dyn<br> [0x8000036c]:csrrs a5, fcsr, zero<br> [0x80000370]:sw tp, 16(ra)<br> [0x80000374]:sw a5, 24(ra)<br>                                   |
|  15|[0x80006e98]<br>0x6C6B01D0<br> [0x80006ea0]<br>0x00000001<br> |- rs1 : x4<br> - rd : x2<br> - fs1 == 0 and fe1 == 0x37d and fm1 == 0xfc00000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x80000390]:fsqrt.d sp, tp, dyn<br> [0x80000394]:csrrs a5, fcsr, zero<br> [0x80000398]:sw sp, 32(ra)<br> [0x8000039c]:sw a5, 40(ra)<br>                                   |
|  16|[0x80006ea8]<br>0xFFFFFF80<br> [0x80006eb0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x376 and fm1 == 0xfffffffffff00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800003bc]:fsqrt.d t5, t3, dyn<br> [0x800003c0]:csrrs a5, fcsr, zero<br> [0x800003c4]:sw t5, 48(ra)<br> [0x800003c8]:sw a5, 56(ra)<br>                                   |
|  17|[0x80006eb8]<br>0x41CD4293<br> [0x80006ec0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfe00000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800003e4]:fsqrt.d t5, t3, dyn<br> [0x800003e8]:csrrs a5, fcsr, zero<br> [0x800003ec]:sw t5, 64(ra)<br> [0x800003f0]:sw a5, 72(ra)<br>                                   |
|  18|[0x80006ec8]<br>0x667F3B18<br> [0x80006ed0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x375 and fm1 == 0xffffffffffe00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000410]:fsqrt.d t5, t3, dyn<br> [0x80000414]:csrrs a5, fcsr, zero<br> [0x80000418]:sw t5, 80(ra)<br> [0x8000041c]:sw a5, 88(ra)<br>                                   |
|  19|[0x80006ed8]<br>0x89B35963<br> [0x80006ee0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xff00000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000438]:fsqrt.d t5, t3, dyn<br> [0x8000043c]:csrrs a5, fcsr, zero<br> [0x80000440]:sw t5, 96(ra)<br> [0x80000444]:sw a5, 104(ra)<br>                                  |
|  20|[0x80006ee8]<br>0xFFFFFE00<br> [0x80006ef0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x374 and fm1 == 0xffffffffffc00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000464]:fsqrt.d t5, t3, dyn<br> [0x80000468]:csrrs a5, fcsr, zero<br> [0x8000046c]:sw t5, 112(ra)<br> [0x80000470]:sw a5, 120(ra)<br>                                 |
|  21|[0x80006ef8]<br>0x256B860E<br> [0x80006f00]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xff80000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000490]:fsqrt.d t5, t3, dyn<br> [0x80000494]:csrrs a5, fcsr, zero<br> [0x80000498]:sw t5, 128(ra)<br> [0x8000049c]:sw a5, 136(ra)<br>                                 |
|  22|[0x80006f08]<br>0x667F38F8<br> [0x80006f10]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x373 and fm1 == 0xffffffffff800 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800004bc]:fsqrt.d t5, t3, dyn<br> [0x800004c0]:csrrs a5, fcsr, zero<br> [0x800004c4]:sw t5, 144(ra)<br> [0x800004c8]:sw a5, 152(ra)<br>                                 |
|  23|[0x80006f18]<br>0x5147CF92<br> [0x80006f20]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffc0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800004e8]:fsqrt.d t5, t3, dyn<br> [0x800004ec]:csrrs a5, fcsr, zero<br> [0x800004f0]:sw t5, 160(ra)<br> [0x800004f4]:sw a5, 168(ra)<br>                                 |
|  24|[0x80006f28]<br>0xFFFFF800<br> [0x80006f30]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x372 and fm1 == 0xffffffffff000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000514]:fsqrt.d t5, t3, dyn<br> [0x80000518]:csrrs a5, fcsr, zero<br> [0x8000051c]:sw t5, 176(ra)<br> [0x80000520]:sw a5, 184(ra)<br>                                 |
|  25|[0x80006f38]<br>0x5EB7DD64<br> [0x80006f40]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffe0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000540]:fsqrt.d t5, t3, dyn<br> [0x80000544]:csrrs a5, fcsr, zero<br> [0x80000548]:sw t5, 192(ra)<br> [0x8000054c]:sw a5, 200(ra)<br>                                 |
|  26|[0x80006f48]<br>0x667F307C<br> [0x80006f50]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x371 and fm1 == 0xfffffffffe000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000056c]:fsqrt.d t5, t3, dyn<br> [0x80000570]:csrrs a5, fcsr, zero<br> [0x80000574]:sw t5, 208(ra)<br> [0x80000578]:sw a5, 216(ra)<br>                                 |
|  27|[0x80006f58]<br>0xE3509A08<br> [0x80006f60]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfff0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000598]:fsqrt.d t5, t3, dyn<br> [0x8000059c]:csrrs a5, fcsr, zero<br> [0x800005a0]:sw t5, 224(ra)<br> [0x800005a4]:sw a5, 232(ra)<br>                                 |
|  28|[0x80006f68]<br>0xFFFFE000<br> [0x80006f70]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x370 and fm1 == 0xfffffffffc000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800005c4]:fsqrt.d t5, t3, dyn<br> [0x800005c8]:csrrs a5, fcsr, zero<br> [0x800005cc]:sw t5, 240(ra)<br> [0x800005d0]:sw a5, 248(ra)<br>                                 |
|  29|[0x80006f78]<br>0x25152D37<br> [0x80006f80]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfff8000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800005f0]:fsqrt.d t5, t3, dyn<br> [0x800005f4]:csrrs a5, fcsr, zero<br> [0x800005f8]:sw t5, 256(ra)<br> [0x800005fc]:sw a5, 264(ra)<br>                                 |
|  30|[0x80006f88]<br>0x667F0E8B<br> [0x80006f90]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x36f and fm1 == 0xfffffffff8000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000061c]:fsqrt.d t5, t3, dyn<br> [0x80000620]:csrrs a5, fcsr, zero<br> [0x80000624]:sw t5, 272(ra)<br> [0x80000628]:sw a5, 280(ra)<br>                                 |
|  31|[0x80006f98]<br>0xC5D584F3<br> [0x80006fa0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffc000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000648]:fsqrt.d t5, t3, dyn<br> [0x8000064c]:csrrs a5, fcsr, zero<br> [0x80000650]:sw t5, 288(ra)<br> [0x80000654]:sw a5, 296(ra)<br>                                 |
|  32|[0x80006fa8]<br>0xFFFF8000<br> [0x80006fb0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x36e and fm1 == 0xfffffffff0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000674]:fsqrt.d t5, t3, dyn<br> [0x80000678]:csrrs a5, fcsr, zero<br> [0x8000067c]:sw t5, 304(ra)<br> [0x80000680]:sw a5, 312(ra)<br>                                 |
|  33|[0x80006fb8]<br>0x162D3478<br> [0x80006fc0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffe000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800006a0]:fsqrt.d t5, t3, dyn<br> [0x800006a4]:csrrs a5, fcsr, zero<br> [0x800006a8]:sw t5, 320(ra)<br> [0x800006ac]:sw a5, 328(ra)<br>                                 |
|  34|[0x80006fc8]<br>0x667E86C8<br> [0x80006fd0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x36d and fm1 == 0xffffffffe0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800006cc]:fsqrt.d t5, t3, dyn<br> [0x800006d0]:csrrs a5, fcsr, zero<br> [0x800006d4]:sw t5, 336(ra)<br> [0x800006d8]:sw a5, 344(ra)<br>                                 |
|  35|[0x80006fd8]<br>0xBE56ED28<br> [0x80006fe0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffff000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800006f8]:fsqrt.d t5, t3, dyn<br> [0x800006fc]:csrrs a5, fcsr, zero<br> [0x80000700]:sw t5, 352(ra)<br> [0x80000704]:sw a5, 360(ra)<br>                                 |
|  36|[0x80006fe8]<br>0xFFFE0000<br> [0x80006ff0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x36c and fm1 == 0xffffffffc0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000724]:fsqrt.d t5, t3, dyn<br> [0x80000728]:csrrs a5, fcsr, zero<br> [0x8000072c]:sw t5, 368(ra)<br> [0x80000730]:sw a5, 376(ra)<br>                                 |
|  37|[0x80006ff8]<br>0x926B41BB<br> [0x80007000]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffff800000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000750]:fsqrt.d t5, t3, dyn<br> [0x80000754]:csrrs a5, fcsr, zero<br> [0x80000758]:sw t5, 384(ra)<br> [0x8000075c]:sw a5, 392(ra)<br>                                 |
|  38|[0x80007008]<br>0x667C67B9<br> [0x80007010]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x36b and fm1 == 0xffffffff80000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000077c]:fsqrt.d t5, t3, dyn<br> [0x80000780]:csrrs a5, fcsr, zero<br> [0x80000784]:sw t5, 400(ra)<br> [0x80000788]:sw a5, 408(ra)<br>                                 |
|  39|[0x80007018]<br>0xFC754A14<br> [0x80007020]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffc00000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800007a8]:fsqrt.d t5, t3, dyn<br> [0x800007ac]:csrrs a5, fcsr, zero<br> [0x800007b0]:sw t5, 416(ra)<br> [0x800007b4]:sw a5, 424(ra)<br>                                 |
|  40|[0x80007028]<br>0xFFF80000<br> [0x80007030]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x36a and fm1 == 0xffffffff00000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800007d4]:fsqrt.d t5, t3, dyn<br> [0x800007d8]:csrrs a5, fcsr, zero<br> [0x800007dc]:sw t5, 432(ra)<br> [0x800007e0]:sw a5, 440(ra)<br>                                 |
|  41|[0x80007038]<br>0xB17A45C5<br> [0x80007040]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffe00000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000800]:fsqrt.d t5, t3, dyn<br> [0x80000804]:csrrs a5, fcsr, zero<br> [0x80000808]:sw t5, 448(ra)<br> [0x8000080c]:sw a5, 456(ra)<br>                                 |
|  42|[0x80007048]<br>0x6673EB7D<br> [0x80007050]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x369 and fm1 == 0xfffffffe00000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000082c]:fsqrt.d t5, t3, dyn<br> [0x80000830]:csrrs a5, fcsr, zero<br> [0x80000834]:sw t5, 464(ra)<br> [0x80000838]:sw a5, 472(ra)<br>                                 |
|  43|[0x80007058]<br>0x0BFCC17E<br> [0x80007060]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffff00000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000858]:fsqrt.d t5, t3, dyn<br> [0x8000085c]:csrrs a5, fcsr, zero<br> [0x80000860]:sw t5, 480(ra)<br> [0x80000864]:sw a5, 488(ra)<br>                                 |
|  44|[0x80007068]<br>0x393DFED2<br> [0x80007070]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffff80000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000884]:fsqrt.d t5, t3, dyn<br> [0x80000888]:csrrs a5, fcsr, zero<br> [0x8000088c]:sw t5, 496(ra)<br> [0x80000890]:sw a5, 504(ra)<br>                                 |
|  45|[0x80007078]<br>0x4FDE9D5B<br> [0x80007080]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffc0000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800008b0]:fsqrt.d t5, t3, dyn<br> [0x800008b4]:csrrs a5, fcsr, zero<br> [0x800008b8]:sw t5, 512(ra)<br> [0x800008bc]:sw a5, 520(ra)<br>                                 |
|  46|[0x80007088]<br>0x5B2EEC96<br> [0x80007090]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffe0000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800008dc]:fsqrt.d t5, t3, dyn<br> [0x800008e0]:csrrs a5, fcsr, zero<br> [0x800008e4]:sw t5, 528(ra)<br> [0x800008e8]:sw a5, 536(ra)<br>                                 |
|  47|[0x80007098]<br>0x60D71432<br> [0x800070a0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffff0000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000908]:fsqrt.d t5, t3, dyn<br> [0x8000090c]:csrrs a5, fcsr, zero<br> [0x80000910]:sw t5, 544(ra)<br> [0x80000914]:sw a5, 552(ra)<br>                                 |
|  48|[0x800070a8]<br>0x63AB2800<br> [0x800070b0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffff8000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000934]:fsqrt.d t5, t3, dyn<br> [0x80000938]:csrrs a5, fcsr, zero<br> [0x8000093c]:sw t5, 560(ra)<br> [0x80000940]:sw a5, 568(ra)<br>                                 |
|  49|[0x800070b8]<br>0x651531E6<br> [0x800070c0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffc000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000960]:fsqrt.d t5, t3, dyn<br> [0x80000964]:csrrs a5, fcsr, zero<br> [0x80000968]:sw t5, 576(ra)<br> [0x8000096c]:sw a5, 584(ra)<br>                                 |
|  50|[0x800070c8]<br>0x65CA36D9<br> [0x800070d0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffe000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000098c]:fsqrt.d t5, t3, dyn<br> [0x80000990]:csrrs a5, fcsr, zero<br> [0x80000994]:sw t5, 592(ra)<br> [0x80000998]:sw a5, 600(ra)<br>                                 |
|  51|[0x800070d8]<br>0x6624B953<br> [0x800070e0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffff000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800009b8]:fsqrt.d t5, t3, dyn<br> [0x800009bc]:csrrs a5, fcsr, zero<br> [0x800009c0]:sw t5, 608(ra)<br> [0x800009c4]:sw a5, 616(ra)<br>                                 |
|  52|[0x800070e8]<br>0x6651FA90<br> [0x800070f0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffff800000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800009e4]:fsqrt.d t5, t3, dyn<br> [0x800009e8]:csrrs a5, fcsr, zero<br> [0x800009ec]:sw t5, 624(ra)<br> [0x800009f0]:sw a5, 632(ra)<br>                                 |
|  53|[0x800070f8]<br>0x66689B2E<br> [0x80007100]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffffc00000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000a10]:fsqrt.d t5, t3, dyn<br> [0x80000a14]:csrrs a5, fcsr, zero<br> [0x80000a18]:sw t5, 640(ra)<br> [0x80000a1c]:sw a5, 648(ra)<br>                                 |
|  54|[0x80007108]<br>0x6673EB7D<br> [0x80007110]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffffe00000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000a3c]:fsqrt.d t5, t3, dyn<br> [0x80000a40]:csrrs a5, fcsr, zero<br> [0x80000a44]:sw t5, 656(ra)<br> [0x80000a48]:sw a5, 664(ra)<br>                                 |
|  55|[0x80007118]<br>0x667993A5<br> [0x80007120]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffff00000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000a68]:fsqrt.d t5, t3, dyn<br> [0x80000a6c]:csrrs a5, fcsr, zero<br> [0x80000a70]:sw t5, 672(ra)<br> [0x80000a74]:sw a5, 680(ra)<br>                                 |
|  56|[0x80007128]<br>0x667C67B9<br> [0x80007130]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffff80000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000a94]:fsqrt.d t5, t3, dyn<br> [0x80000a98]:csrrs a5, fcsr, zero<br> [0x80000a9c]:sw t5, 688(ra)<br> [0x80000aa0]:sw a5, 696(ra)<br>                                 |
|  57|[0x80007138]<br>0x667DD1C3<br> [0x80007140]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffffc0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000ac0]:fsqrt.d t5, t3, dyn<br> [0x80000ac4]:csrrs a5, fcsr, zero<br> [0x80000ac8]:sw t5, 704(ra)<br> [0x80000acc]:sw a5, 712(ra)<br>                                 |
|  58|[0x80007148]<br>0x667E86C8<br> [0x80007150]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffffe0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000aec]:fsqrt.d t5, t3, dyn<br> [0x80000af0]:csrrs a5, fcsr, zero<br> [0x80000af4]:sw t5, 720(ra)<br> [0x80000af8]:sw a5, 728(ra)<br>                                 |
|  59|[0x80007158]<br>0x667EE14A<br> [0x80007160]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffffff0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000b18]:fsqrt.d t5, t3, dyn<br> [0x80000b1c]:csrrs a5, fcsr, zero<br> [0x80000b20]:sw t5, 736(ra)<br> [0x80000b24]:sw a5, 744(ra)<br>                                 |
|  60|[0x80007168]<br>0x667F0E8B<br> [0x80007170]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffffff8000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000b44]:fsqrt.d t5, t3, dyn<br> [0x80000b48]:csrrs a5, fcsr, zero<br> [0x80000b4c]:sw t5, 752(ra)<br> [0x80000b50]:sw a5, 760(ra)<br>                                 |
|  61|[0x80007178]<br>0x667F252C<br> [0x80007180]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffffffc000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000b70]:fsqrt.d t5, t3, dyn<br> [0x80000b74]:csrrs a5, fcsr, zero<br> [0x80000b78]:sw t5, 768(ra)<br> [0x80000b7c]:sw a5, 776(ra)<br>                                 |
|  62|[0x80007188]<br>0x667F307C<br> [0x80007190]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffffffe000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000b9c]:fsqrt.d t5, t3, dyn<br> [0x80000ba0]:csrrs a5, fcsr, zero<br> [0x80000ba4]:sw t5, 784(ra)<br> [0x80000ba8]:sw a5, 792(ra)<br>                                 |
|  63|[0x80007198]<br>0x667F3624<br> [0x800071a0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffffff000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000bc8]:fsqrt.d t5, t3, dyn<br> [0x80000bcc]:csrrs a5, fcsr, zero<br> [0x80000bd0]:sw t5, 800(ra)<br> [0x80000bd4]:sw a5, 808(ra)<br>                                 |
|  64|[0x800071a8]<br>0x667F38F8<br> [0x800071b0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffffff800 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000bf4]:fsqrt.d t5, t3, dyn<br> [0x80000bf8]:csrrs a5, fcsr, zero<br> [0x80000bfc]:sw t5, 816(ra)<br> [0x80000c00]:sw a5, 824(ra)<br>                                 |
|  65|[0x800071b8]<br>0x667F3A63<br> [0x800071c0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffffffc00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000c20]:fsqrt.d t5, t3, dyn<br> [0x80000c24]:csrrs a5, fcsr, zero<br> [0x80000c28]:sw t5, 832(ra)<br> [0x80000c2c]:sw a5, 840(ra)<br>                                 |
|  66|[0x800071c8]<br>0x667F3B18<br> [0x800071d0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffffffe00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000c4c]:fsqrt.d t5, t3, dyn<br> [0x80000c50]:csrrs a5, fcsr, zero<br> [0x80000c54]:sw t5, 848(ra)<br> [0x80000c58]:sw a5, 856(ra)<br>                                 |
|  67|[0x800071d8]<br>0x667F3B72<br> [0x800071e0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffffffff00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000c78]:fsqrt.d t5, t3, dyn<br> [0x80000c7c]:csrrs a5, fcsr, zero<br> [0x80000c80]:sw t5, 864(ra)<br> [0x80000c84]:sw a5, 872(ra)<br>                                 |
|  68|[0x800071e8]<br>0x667F3B9F<br> [0x800071f0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffffffff80 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000ca4]:fsqrt.d t5, t3, dyn<br> [0x80000ca8]:csrrs a5, fcsr, zero<br> [0x80000cac]:sw t5, 880(ra)<br> [0x80000cb0]:sw a5, 888(ra)<br>                                 |
|  69|[0x800071f8]<br>0x667F3BB6<br> [0x80007200]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffffffffc0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000cd0]:fsqrt.d t5, t3, dyn<br> [0x80000cd4]:csrrs a5, fcsr, zero<br> [0x80000cd8]:sw t5, 896(ra)<br> [0x80000cdc]:sw a5, 904(ra)<br>                                 |
|  70|[0x80007208]<br>0x667F3BC1<br> [0x80007210]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xfffffffffffe0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000cfc]:fsqrt.d t5, t3, dyn<br> [0x80000d00]:csrrs a5, fcsr, zero<br> [0x80000d04]:sw t5, 912(ra)<br> [0x80000d08]:sw a5, 920(ra)<br>                                 |
|  71|[0x80007218]<br>0x667F3BC7<br> [0x80007220]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffffffff0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000d28]:fsqrt.d t5, t3, dyn<br> [0x80000d2c]:csrrs a5, fcsr, zero<br> [0x80000d30]:sw t5, 928(ra)<br> [0x80000d34]:sw a5, 936(ra)<br>                                 |
|  72|[0x80007228]<br>0x667F3BCA<br> [0x80007230]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffffffff8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000d54]:fsqrt.d t5, t3, dyn<br> [0x80000d58]:csrrs a5, fcsr, zero<br> [0x80000d5c]:sw t5, 944(ra)<br> [0x80000d60]:sw a5, 952(ra)<br>                                 |
|  73|[0x80007238]<br>0x667F3BCB<br> [0x80007240]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37d and fm1 == 0xffffffffffffc and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000d80]:fsqrt.d t5, t3, dyn<br> [0x80000d84]:csrrs a5, fcsr, zero<br> [0x80000d88]:sw t5, 960(ra)<br> [0x80000d8c]:sw a5, 968(ra)<br>                                 |
|  74|[0x80007248]<br>0xD236A58F<br> [0x80007250]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000da8]:fsqrt.d t5, t3, dyn<br> [0x80000dac]:csrrs a5, fcsr, zero<br> [0x80000db0]:sw t5, 976(ra)<br> [0x80000db4]:sw a5, 984(ra)<br>                                 |
|  75|[0x80007258]<br>0x667F3BCD<br> [0x80007260]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x400 and fm1 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000dd0]:fsqrt.d t5, t3, dyn<br> [0x80000dd4]:csrrs a5, fcsr, zero<br> [0x80000dd8]:sw t5, 992(ra)<br> [0x80000ddc]:sw a5, 1000(ra)<br>                                |
|  76|[0x80007268]<br>0x0568C1C3<br> [0x80007270]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xf000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000df8]:fsqrt.d t5, t3, dyn<br> [0x80000dfc]:csrrs a5, fcsr, zero<br> [0x80000e00]:sw t5, 1008(ra)<br> [0x80000e04]:sw a5, 1016(ra)<br>                               |
|  77|[0x80007278]<br>0x8D0E2F77<br> [0x80007280]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xf800000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000e20]:fsqrt.d t5, t3, dyn<br> [0x80000e24]:csrrs a5, fcsr, zero<br> [0x80000e28]:sw t5, 1024(ra)<br> [0x80000e2c]:sw a5, 1032(ra)<br>                               |
|  78|[0x80007288]<br>0x8F2AAA48<br> [0x80007290]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe800000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000e48]:fsqrt.d t5, t3, dyn<br> [0x80000e4c]:csrrs a5, fcsr, zero<br> [0x80000e50]:sw t5, 1040(ra)<br> [0x80000e54]:sw a5, 1048(ra)<br>                               |
|  79|[0x80007298]<br>0x6C6B01D0<br> [0x800072a0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfc00000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000e70]:fsqrt.d t5, t3, dyn<br> [0x80000e74]:csrrs a5, fcsr, zero<br> [0x80000e78]:sw t5, 1056(ra)<br> [0x80000e7c]:sw a5, 1064(ra)<br>                               |
|  80|[0x800072a8]<br>0x00000000<br> [0x800072b0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe400000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000e98]:fsqrt.d t5, t3, dyn<br> [0x80000e9c]:csrrs a5, fcsr, zero<br> [0x80000ea0]:sw t5, 1072(ra)<br> [0x80000ea4]:sw a5, 1080(ra)<br>                               |
|  81|[0x800072b8]<br>0x41CD4293<br> [0x800072c0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfe00000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000ec0]:fsqrt.d t5, t3, dyn<br> [0x80000ec4]:csrrs a5, fcsr, zero<br> [0x80000ec8]:sw t5, 1088(ra)<br> [0x80000ecc]:sw a5, 1096(ra)<br>                               |
|  82|[0x800072c8]<br>0x01D483B4<br> [0x800072d0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe200000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000ee8]:fsqrt.d t5, t3, dyn<br> [0x80000eec]:csrrs a5, fcsr, zero<br> [0x80000ef0]:sw t5, 1104(ra)<br> [0x80000ef4]:sw a5, 1112(ra)<br>                               |
|  83|[0x800072d8]<br>0x89B35963<br> [0x800072e0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xff00000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000f10]:fsqrt.d t5, t3, dyn<br> [0x80000f14]:csrrs a5, fcsr, zero<br> [0x80000f18]:sw t5, 1120(ra)<br> [0x80000f1c]:sw a5, 1128(ra)<br>                               |
|  84|[0x800072e8]<br>0xB0D1F876<br> [0x800072f0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe100000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000f38]:fsqrt.d t5, t3, dyn<br> [0x80000f3c]:csrrs a5, fcsr, zero<br> [0x80000f40]:sw t5, 1136(ra)<br> [0x80000f44]:sw a5, 1144(ra)<br>                               |
|  85|[0x800072f8]<br>0x256B860E<br> [0x80007300]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xff80000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000f64]:fsqrt.d t5, t3, dyn<br> [0x80000f68]:csrrs a5, fcsr, zero<br> [0x80000f6c]:sw t5, 1152(ra)<br> [0x80000f70]:sw a5, 1160(ra)<br>                               |
|  86|[0x80007308]<br>0x734B41EE<br> [0x80007310]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe080000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000f90]:fsqrt.d t5, t3, dyn<br> [0x80000f94]:csrrs a5, fcsr, zero<br> [0x80000f98]:sw t5, 1168(ra)<br> [0x80000f9c]:sw a5, 1176(ra)<br>                               |
|  87|[0x80007318]<br>0x5147CF92<br> [0x80007320]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffc0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000fbc]:fsqrt.d t5, t3, dyn<br> [0x80000fc0]:csrrs a5, fcsr, zero<br> [0x80000fc4]:sw t5, 1184(ra)<br> [0x80000fc8]:sw a5, 1192(ra)<br>                               |
|  88|[0x80007328]<br>0xAF352D2B<br> [0x80007330]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe040000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000fe8]:fsqrt.d t5, t3, dyn<br> [0x80000fec]:csrrs a5, fcsr, zero<br> [0x80000ff0]:sw t5, 1200(ra)<br> [0x80000ff4]:sw a5, 1208(ra)<br>                               |
|  89|[0x80007338]<br>0x5EB7DD64<br> [0x80007340]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffe0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001014]:fsqrt.d t5, t3, dyn<br> [0x80001018]:csrrs a5, fcsr, zero<br> [0x8000101c]:sw t5, 1216(ra)<br> [0x80001020]:sw a5, 1224(ra)<br>                               |
|  90|[0x80007348]<br>0xC3D34765<br> [0x80007350]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe020000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001040]:fsqrt.d t5, t3, dyn<br> [0x80001044]:csrrs a5, fcsr, zero<br> [0x80001048]:sw t5, 1232(ra)<br> [0x8000104c]:sw a5, 1240(ra)<br>                               |
|  91|[0x80007358]<br>0xE3509A08<br> [0x80007360]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfff0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000106c]:fsqrt.d t5, t3, dyn<br> [0x80001070]:csrrs a5, fcsr, zero<br> [0x80001074]:sw t5, 1248(ra)<br> [0x80001078]:sw a5, 1256(ra)<br>                               |
|  92|[0x80007368]<br>0x4BCC57F3<br> [0x80007370]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe010000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001098]:fsqrt.d t5, t3, dyn<br> [0x8000109c]:csrrs a5, fcsr, zero<br> [0x800010a0]:sw t5, 1264(ra)<br> [0x800010a4]:sw a5, 1272(ra)<br>                               |
|  93|[0x80007378]<br>0x25152D37<br> [0x80007380]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfff8000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800010c4]:fsqrt.d t5, t3, dyn<br> [0x800010c8]:csrrs a5, fcsr, zero<br> [0x800010cc]:sw t5, 1280(ra)<br> [0x800010d0]:sw a5, 1288(ra)<br>                               |
|  94|[0x80007388]<br>0x8F33585E<br> [0x80007390]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe008000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800010f0]:fsqrt.d t5, t3, dyn<br> [0x800010f4]:csrrs a5, fcsr, zero<br> [0x800010f8]:sw t5, 1296(ra)<br> [0x800010fc]:sw a5, 1304(ra)<br>                               |
|  95|[0x80007398]<br>0xC5D584F3<br> [0x800073a0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffc000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000111c]:fsqrt.d t5, t3, dyn<br> [0x80001120]:csrrs a5, fcsr, zero<br> [0x80001124]:sw t5, 1312(ra)<br> [0x80001128]:sw a5, 1320(ra)<br>                               |
|  96|[0x800073a8]<br>0x30C17586<br> [0x800073b0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe004000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001148]:fsqrt.d t5, t3, dyn<br> [0x8000114c]:csrrs a5, fcsr, zero<br> [0x80001150]:sw t5, 1328(ra)<br> [0x80001154]:sw a5, 1336(ra)<br>                               |
|  97|[0x800073b8]<br>0x162D3478<br> [0x800073c0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffe000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001174]:fsqrt.d t5, t3, dyn<br> [0x80001178]:csrrs a5, fcsr, zero<br> [0x8000117c]:sw t5, 1344(ra)<br> [0x80001180]:sw a5, 1352(ra)<br>                               |
|  98|[0x800073c8]<br>0x817F2B33<br> [0x800073d0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe002000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800011a0]:fsqrt.d t5, t3, dyn<br> [0x800011a4]:csrrs a5, fcsr, zero<br> [0x800011a8]:sw t5, 1360(ra)<br> [0x800011ac]:sw a5, 1368(ra)<br>                               |
|  99|[0x800073d8]<br>0xBE56ED28<br> [0x800073e0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffff000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800011cc]:fsqrt.d t5, t3, dyn<br> [0x800011d0]:csrrs a5, fcsr, zero<br> [0x800011d4]:sw t5, 1376(ra)<br> [0x800011d8]:sw a5, 1384(ra)<br>                               |
| 100|[0x800073e8]<br>0xA9DBAFCC<br> [0x800073f0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe001000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800011f8]:fsqrt.d t5, t3, dyn<br> [0x800011fc]:csrrs a5, fcsr, zero<br> [0x80001200]:sw t5, 1392(ra)<br> [0x80001204]:sw a5, 1400(ra)<br>                               |
| 101|[0x800073f8]<br>0x926B41BB<br> [0x80007400]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffff800000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001224]:fsqrt.d t5, t3, dyn<br> [0x80001228]:csrrs a5, fcsr, zero<br> [0x8000122c]:sw t5, 1408(ra)<br> [0x80001230]:sw a5, 1416(ra)<br>                               |
| 102|[0x80007408]<br>0xBE095C88<br> [0x80007410]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000800000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001250]:fsqrt.d t5, t3, dyn<br> [0x80001254]:csrrs a5, fcsr, zero<br> [0x80001258]:sw t5, 1424(ra)<br> [0x8000125c]:sw a5, 1432(ra)<br>                               |
| 103|[0x80007418]<br>0xFC754A14<br> [0x80007420]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffc00000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000127c]:fsqrt.d t5, t3, dyn<br> [0x80001280]:csrrs a5, fcsr, zero<br> [0x80001284]:sw t5, 1440(ra)<br> [0x80001288]:sw a5, 1448(ra)<br>                               |
| 104|[0x80007428]<br>0x48200D82<br> [0x80007430]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000400000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800012a8]:fsqrt.d t5, t3, dyn<br> [0x800012ac]:csrrs a5, fcsr, zero<br> [0x800012b0]:sw t5, 1456(ra)<br> [0x800012b4]:sw a5, 1464(ra)<br>                               |
| 105|[0x80007438]<br>0xB17A45C5<br> [0x80007440]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffe00000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800012d4]:fsqrt.d t5, t3, dyn<br> [0x800012d8]:csrrs a5, fcsr, zero<br> [0x800012dc]:sw t5, 1472(ra)<br> [0x800012e0]:sw a5, 1480(ra)<br>                               |
| 106|[0x80007448]<br>0x8D2B5CA6<br> [0x80007450]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000200000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001300]:fsqrt.d t5, t3, dyn<br> [0x80001304]:csrrs a5, fcsr, zero<br> [0x80001308]:sw t5, 1488(ra)<br> [0x8000130c]:sw a5, 1496(ra)<br>                               |
| 107|[0x80007458]<br>0x0BFCC17E<br> [0x80007460]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffff00000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000132c]:fsqrt.d t5, t3, dyn<br> [0x80001330]:csrrs a5, fcsr, zero<br> [0x80001334]:sw t5, 1504(ra)<br> [0x80001338]:sw a5, 1512(ra)<br>                               |
| 108|[0x80007468]<br>0x2FB101E2<br> [0x80007470]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000100000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001358]:fsqrt.d t5, t3, dyn<br> [0x8000135c]:csrrs a5, fcsr, zero<br> [0x80001360]:sw t5, 1520(ra)<br> [0x80001364]:sw a5, 1528(ra)<br>                               |
| 109|[0x80007478]<br>0x393DFED2<br> [0x80007480]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffff80000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001384]:fsqrt.d t5, t3, dyn<br> [0x80001388]:csrrs a5, fcsr, zero<br> [0x8000138c]:sw t5, 1536(ra)<br> [0x80001390]:sw a5, 1544(ra)<br>                               |
| 110|[0x80007488]<br>0x00F3D3EA<br> [0x80007490]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000080000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800013ac]:fsqrt.d t5, t3, dyn<br> [0x800013b0]:csrrs a5, fcsr, zero<br> [0x800013b4]:sw t5, 1552(ra)<br> [0x800013b8]:sw a5, 1560(ra)<br>                               |
| 111|[0x80007498]<br>0x4FDE9D5B<br> [0x800074a0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffc0000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800013d8]:fsqrt.d t5, t3, dyn<br> [0x800013dc]:csrrs a5, fcsr, zero<br> [0x800013e0]:sw t5, 1568(ra)<br> [0x800013e4]:sw a5, 1576(ra)<br>                               |
| 112|[0x800074a8]<br>0xE9953CC9<br> [0x800074b0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000040000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001400]:fsqrt.d t5, t3, dyn<br> [0x80001404]:csrrs a5, fcsr, zero<br> [0x80001408]:sw t5, 1584(ra)<br> [0x8000140c]:sw a5, 1592(ra)<br>                               |
| 113|[0x800074b8]<br>0x5B2EEC96<br> [0x800074c0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffe0000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000142c]:fsqrt.d t5, t3, dyn<br> [0x80001430]:csrrs a5, fcsr, zero<br> [0x80001434]:sw t5, 1600(ra)<br> [0x80001438]:sw a5, 1608(ra)<br>                               |
| 114|[0x800074c8]<br>0xDDE5F12F<br> [0x800074d0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000020000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001454]:fsqrt.d t5, t3, dyn<br> [0x80001458]:csrrs a5, fcsr, zero<br> [0x8000145c]:sw t5, 1616(ra)<br> [0x80001460]:sw a5, 1624(ra)<br>                               |
| 115|[0x800074d8]<br>0x60D71432<br> [0x800074e0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffff0000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001480]:fsqrt.d t5, t3, dyn<br> [0x80001484]:csrrs a5, fcsr, zero<br> [0x80001488]:sw t5, 1632(ra)<br> [0x8000148c]:sw a5, 1640(ra)<br>                               |
| 116|[0x800074e8]<br>0xD80E4B60<br> [0x800074f0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000010000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800014a8]:fsqrt.d t5, t3, dyn<br> [0x800014ac]:csrrs a5, fcsr, zero<br> [0x800014b0]:sw t5, 1648(ra)<br> [0x800014b4]:sw a5, 1656(ra)<br>                               |
| 117|[0x800074f8]<br>0x63AB2800<br> [0x80007500]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffff8000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800014d4]:fsqrt.d t5, t3, dyn<br> [0x800014d8]:csrrs a5, fcsr, zero<br> [0x800014dc]:sw t5, 1664(ra)<br> [0x800014e0]:sw a5, 1672(ra)<br>                               |
| 118|[0x80007508]<br>0xD5227878<br> [0x80007510]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000008000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800014fc]:fsqrt.d t5, t3, dyn<br> [0x80001500]:csrrs a5, fcsr, zero<br> [0x80001504]:sw t5, 1680(ra)<br> [0x80001508]:sw a5, 1688(ra)<br>                               |
| 119|[0x80007518]<br>0x651531E6<br> [0x80007520]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffc000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001528]:fsqrt.d t5, t3, dyn<br> [0x8000152c]:csrrs a5, fcsr, zero<br> [0x80001530]:sw t5, 1696(ra)<br> [0x80001534]:sw a5, 1704(ra)<br>                               |
| 120|[0x80007528]<br>0xD3AC8F03<br> [0x80007530]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000004000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001550]:fsqrt.d t5, t3, dyn<br> [0x80001554]:csrrs a5, fcsr, zero<br> [0x80001558]:sw t5, 1712(ra)<br> [0x8000155c]:sw a5, 1720(ra)<br>                               |
| 121|[0x80007538]<br>0x65CA36D9<br> [0x80007540]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffe000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000157c]:fsqrt.d t5, t3, dyn<br> [0x80001580]:csrrs a5, fcsr, zero<br> [0x80001584]:sw t5, 1728(ra)<br> [0x80001588]:sw a5, 1736(ra)<br>                               |
| 122|[0x80007548]<br>0xD2F19A49<br> [0x80007550]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000002000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800015a4]:fsqrt.d t5, t3, dyn<br> [0x800015a8]:csrrs a5, fcsr, zero<br> [0x800015ac]:sw t5, 1744(ra)<br> [0x800015b0]:sw a5, 1752(ra)<br>                               |
| 123|[0x80007558]<br>0x6624B953<br> [0x80007560]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffff000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800015d0]:fsqrt.d t5, t3, dyn<br> [0x800015d4]:csrrs a5, fcsr, zero<br> [0x800015d8]:sw t5, 1760(ra)<br> [0x800015dc]:sw a5, 1768(ra)<br>                               |
| 124|[0x80007568]<br>0xD2941FEC<br> [0x80007570]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000001000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800015f8]:fsqrt.d t5, t3, dyn<br> [0x800015fc]:csrrs a5, fcsr, zero<br> [0x80001600]:sw t5, 1776(ra)<br> [0x80001604]:sw a5, 1784(ra)<br>                               |
| 125|[0x80007578]<br>0x6651FA90<br> [0x80007580]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffff800000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001624]:fsqrt.d t5, t3, dyn<br> [0x80001628]:csrrs a5, fcsr, zero<br> [0x8000162c]:sw t5, 1792(ra)<br> [0x80001630]:sw a5, 1800(ra)<br>                               |
| 126|[0x80007588]<br>0xD26562BD<br> [0x80007590]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000800000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000164c]:fsqrt.d t5, t3, dyn<br> [0x80001650]:csrrs a5, fcsr, zero<br> [0x80001654]:sw t5, 1808(ra)<br> [0x80001658]:sw a5, 1816(ra)<br>                               |
| 127|[0x80007598]<br>0x66689B2E<br> [0x800075a0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffffc00000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001678]:fsqrt.d t5, t3, dyn<br> [0x8000167c]:csrrs a5, fcsr, zero<br> [0x80001680]:sw t5, 1824(ra)<br> [0x80001684]:sw a5, 1832(ra)<br>                               |
| 128|[0x800075a8]<br>0xD24E0426<br> [0x800075b0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000400000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800016a0]:fsqrt.d t5, t3, dyn<br> [0x800016a4]:csrrs a5, fcsr, zero<br> [0x800016a8]:sw t5, 1840(ra)<br> [0x800016ac]:sw a5, 1848(ra)<br>                               |
| 129|[0x800075b8]<br>0x6673EB7D<br> [0x800075c0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffffe00000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800016cc]:fsqrt.d t5, t3, dyn<br> [0x800016d0]:csrrs a5, fcsr, zero<br> [0x800016d4]:sw t5, 1856(ra)<br> [0x800016d8]:sw a5, 1864(ra)<br>                               |
| 130|[0x800075c8]<br>0xD24254DB<br> [0x800075d0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000200000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800016f4]:fsqrt.d t5, t3, dyn<br> [0x800016f8]:csrrs a5, fcsr, zero<br> [0x800016fc]:sw t5, 1872(ra)<br> [0x80001700]:sw a5, 1880(ra)<br>                               |
| 131|[0x800075d8]<br>0x667993A5<br> [0x800075e0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffff00000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001720]:fsqrt.d t5, t3, dyn<br> [0x80001724]:csrrs a5, fcsr, zero<br> [0x80001728]:sw t5, 1888(ra)<br> [0x8000172c]:sw a5, 1896(ra)<br>                               |
| 132|[0x800075e8]<br>0xD23C7D35<br> [0x800075f0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000100000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001748]:fsqrt.d t5, t3, dyn<br> [0x8000174c]:csrrs a5, fcsr, zero<br> [0x80001750]:sw t5, 1904(ra)<br> [0x80001754]:sw a5, 1912(ra)<br>                               |
| 133|[0x800075f8]<br>0x667C67B9<br> [0x80007600]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffff80000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001774]:fsqrt.d t5, t3, dyn<br> [0x80001778]:csrrs a5, fcsr, zero<br> [0x8000177c]:sw t5, 1920(ra)<br> [0x80001780]:sw a5, 1928(ra)<br>                               |
| 134|[0x80007608]<br>0xD2399162<br> [0x80007610]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000080000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000179c]:fsqrt.d t5, t3, dyn<br> [0x800017a0]:csrrs a5, fcsr, zero<br> [0x800017a4]:sw t5, 1936(ra)<br> [0x800017a8]:sw a5, 1944(ra)<br>                               |
| 135|[0x80007618]<br>0x667DD1C3<br> [0x80007620]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffffc0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800017c8]:fsqrt.d t5, t3, dyn<br> [0x800017cc]:csrrs a5, fcsr, zero<br> [0x800017d0]:sw t5, 1952(ra)<br> [0x800017d4]:sw a5, 1960(ra)<br>                               |
| 136|[0x80007628]<br>0xD2381B78<br> [0x80007630]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000040000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800017f0]:fsqrt.d t5, t3, dyn<br> [0x800017f4]:csrrs a5, fcsr, zero<br> [0x800017f8]:sw t5, 1968(ra)<br> [0x800017fc]:sw a5, 1976(ra)<br>                               |
| 137|[0x80007638]<br>0x667E86C8<br> [0x80007640]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffffe0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000181c]:fsqrt.d t5, t3, dyn<br> [0x80001820]:csrrs a5, fcsr, zero<br> [0x80001824]:sw t5, 1984(ra)<br> [0x80001828]:sw a5, 1992(ra)<br>                               |
| 138|[0x80007648]<br>0xD2376084<br> [0x80007650]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000020000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001844]:fsqrt.d t5, t3, dyn<br> [0x80001848]:csrrs a5, fcsr, zero<br> [0x8000184c]:sw t5, 2000(ra)<br> [0x80001850]:sw a5, 2008(ra)<br>                               |
| 139|[0x80007658]<br>0x667EE14A<br> [0x80007660]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffffff0000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001870]:fsqrt.d t5, t3, dyn<br> [0x80001874]:csrrs a5, fcsr, zero<br> [0x80001878]:sw t5, 2016(ra)<br> [0x8000187c]:sw a5, 2024(ra)<br>                               |
| 140|[0x80007668]<br>0xD2370309<br> [0x80007670]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000010000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001898]:fsqrt.d t5, t3, dyn<br> [0x8000189c]:csrrs a5, fcsr, zero<br> [0x800018a0]:sw t5, 2032(ra)<br> [0x800018a4]:sw a5, 2040(ra)<br>                               |
| 141|[0x80007678]<br>0x667F0E8B<br> [0x80007680]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffffff8000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800018c4]:fsqrt.d t5, t3, dyn<br> [0x800018c8]:csrrs a5, fcsr, zero<br> [0x800018cc]:addi ra, ra, 2040<br> [0x800018d0]:sw t5, 8(ra)<br> [0x800018d4]:sw a5, 16(ra)<br> |
| 142|[0x80007688]<br>0xD236D44C<br> [0x80007690]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000008000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800018f0]:fsqrt.d t5, t3, dyn<br> [0x800018f4]:csrrs a5, fcsr, zero<br> [0x800018f8]:sw t5, 24(ra)<br> [0x800018fc]:sw a5, 32(ra)<br>                                   |
| 143|[0x80007698]<br>0x667F252C<br> [0x800076a0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffffffc000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000191c]:fsqrt.d t5, t3, dyn<br> [0x80001920]:csrrs a5, fcsr, zero<br> [0x80001924]:sw t5, 40(ra)<br> [0x80001928]:sw a5, 48(ra)<br>                                   |
| 144|[0x800076a8]<br>0xD236BCEE<br> [0x800076b0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000004000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001944]:fsqrt.d t5, t3, dyn<br> [0x80001948]:csrrs a5, fcsr, zero<br> [0x8000194c]:sw t5, 56(ra)<br> [0x80001950]:sw a5, 64(ra)<br>                                   |
| 145|[0x800076b8]<br>0x667F307C<br> [0x800076c0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffffffe000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001970]:fsqrt.d t5, t3, dyn<br> [0x80001974]:csrrs a5, fcsr, zero<br> [0x80001978]:sw t5, 72(ra)<br> [0x8000197c]:sw a5, 80(ra)<br>                                   |
| 146|[0x800076c8]<br>0xD236B13E<br> [0x800076d0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000002000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001998]:fsqrt.d t5, t3, dyn<br> [0x8000199c]:csrrs a5, fcsr, zero<br> [0x800019a0]:sw t5, 88(ra)<br> [0x800019a4]:sw a5, 96(ra)<br>                                   |
| 147|[0x800076d8]<br>0x667F3624<br> [0x800076e0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffffff000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800019c4]:fsqrt.d t5, t3, dyn<br> [0x800019c8]:csrrs a5, fcsr, zero<br> [0x800019cc]:sw t5, 104(ra)<br> [0x800019d0]:sw a5, 112(ra)<br>                                 |
| 148|[0x800076e8]<br>0xD236AB67<br> [0x800076f0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000001000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800019ec]:fsqrt.d t5, t3, dyn<br> [0x800019f0]:csrrs a5, fcsr, zero<br> [0x800019f4]:sw t5, 120(ra)<br> [0x800019f8]:sw a5, 128(ra)<br>                                 |
| 149|[0x800076f8]<br>0x667F38F8<br> [0x80007700]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffffff800 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001a18]:fsqrt.d t5, t3, dyn<br> [0x80001a1c]:csrrs a5, fcsr, zero<br> [0x80001a20]:sw t5, 136(ra)<br> [0x80001a24]:sw a5, 144(ra)<br>                                 |
| 150|[0x80007708]<br>0xD236A87B<br> [0x80007710]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000000800 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001a44]:fsqrt.d t5, t3, dyn<br> [0x80001a48]:csrrs a5, fcsr, zero<br> [0x80001a4c]:sw t5, 152(ra)<br> [0x80001a50]:sw a5, 160(ra)<br>                                 |
| 151|[0x80007718]<br>0x667F3A63<br> [0x80007720]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffffffc00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001a70]:fsqrt.d t5, t3, dyn<br> [0x80001a74]:csrrs a5, fcsr, zero<br> [0x80001a78]:sw t5, 168(ra)<br> [0x80001a7c]:sw a5, 176(ra)<br>                                 |
| 152|[0x80007728]<br>0xD236A705<br> [0x80007730]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000000400 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001a98]:fsqrt.d t5, t3, dyn<br> [0x80001a9c]:csrrs a5, fcsr, zero<br> [0x80001aa0]:sw t5, 184(ra)<br> [0x80001aa4]:sw a5, 192(ra)<br>                                 |
| 153|[0x80007738]<br>0x667F3B18<br> [0x80007740]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffffffe00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001ac4]:fsqrt.d t5, t3, dyn<br> [0x80001ac8]:csrrs a5, fcsr, zero<br> [0x80001acc]:sw t5, 200(ra)<br> [0x80001ad0]:sw a5, 208(ra)<br>                                 |
| 154|[0x80007748]<br>0xD236A64A<br> [0x80007750]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000000200 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001aec]:fsqrt.d t5, t3, dyn<br> [0x80001af0]:csrrs a5, fcsr, zero<br> [0x80001af4]:sw t5, 216(ra)<br> [0x80001af8]:sw a5, 224(ra)<br>                                 |
| 155|[0x80007758]<br>0x667F3B72<br> [0x80007760]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffffffff00 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001b18]:fsqrt.d t5, t3, dyn<br> [0x80001b1c]:csrrs a5, fcsr, zero<br> [0x80001b20]:sw t5, 232(ra)<br> [0x80001b24]:sw a5, 240(ra)<br>                                 |
| 156|[0x80007768]<br>0xD236A5EC<br> [0x80007770]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000000100 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001b40]:fsqrt.d t5, t3, dyn<br> [0x80001b44]:csrrs a5, fcsr, zero<br> [0x80001b48]:sw t5, 248(ra)<br> [0x80001b4c]:sw a5, 256(ra)<br>                                 |
| 157|[0x80007778]<br>0x667F3B9F<br> [0x80007780]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffffffff80 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001b6c]:fsqrt.d t5, t3, dyn<br> [0x80001b70]:csrrs a5, fcsr, zero<br> [0x80001b74]:sw t5, 264(ra)<br> [0x80001b78]:sw a5, 272(ra)<br>                                 |
| 158|[0x80007788]<br>0xD236A5BE<br> [0x80007790]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000000080 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001b94]:fsqrt.d t5, t3, dyn<br> [0x80001b98]:csrrs a5, fcsr, zero<br> [0x80001b9c]:sw t5, 280(ra)<br> [0x80001ba0]:sw a5, 288(ra)<br>                                 |
| 159|[0x80007798]<br>0x667F3BB6<br> [0x800077a0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffffffffc0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001bc0]:fsqrt.d t5, t3, dyn<br> [0x80001bc4]:csrrs a5, fcsr, zero<br> [0x80001bc8]:sw t5, 296(ra)<br> [0x80001bcc]:sw a5, 304(ra)<br>                                 |
| 160|[0x800077a8]<br>0xD236A5A6<br> [0x800077b0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000000040 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001be8]:fsqrt.d t5, t3, dyn<br> [0x80001bec]:csrrs a5, fcsr, zero<br> [0x80001bf0]:sw t5, 312(ra)<br> [0x80001bf4]:sw a5, 320(ra)<br>                                 |
| 161|[0x800077b8]<br>0x667F3BC1<br> [0x800077c0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffffffffe0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001c14]:fsqrt.d t5, t3, dyn<br> [0x80001c18]:csrrs a5, fcsr, zero<br> [0x80001c1c]:sw t5, 328(ra)<br> [0x80001c20]:sw a5, 336(ra)<br>                                 |
| 162|[0x800077c8]<br>0xD236A59B<br> [0x800077d0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000000020 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001c3c]:fsqrt.d t5, t3, dyn<br> [0x80001c40]:csrrs a5, fcsr, zero<br> [0x80001c44]:sw t5, 344(ra)<br> [0x80001c48]:sw a5, 352(ra)<br>                                 |
| 163|[0x800077d8]<br>0x667F3BC7<br> [0x800077e0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffffffff0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001c68]:fsqrt.d t5, t3, dyn<br> [0x80001c6c]:csrrs a5, fcsr, zero<br> [0x80001c70]:sw t5, 360(ra)<br> [0x80001c74]:sw a5, 368(ra)<br>                                 |
| 164|[0x800077e8]<br>0xD236A595<br> [0x800077f0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000000010 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001c90]:fsqrt.d t5, t3, dyn<br> [0x80001c94]:csrrs a5, fcsr, zero<br> [0x80001c98]:sw t5, 376(ra)<br> [0x80001c9c]:sw a5, 384(ra)<br>                                 |
| 165|[0x800077f8]<br>0x667F3BCA<br> [0x80007800]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffffffff8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001cbc]:fsqrt.d t5, t3, dyn<br> [0x80001cc0]:csrrs a5, fcsr, zero<br> [0x80001cc4]:sw t5, 392(ra)<br> [0x80001cc8]:sw a5, 400(ra)<br>                                 |
| 166|[0x80007808]<br>0xD236A592<br> [0x80007810]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000000008 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001ce4]:fsqrt.d t5, t3, dyn<br> [0x80001ce8]:csrrs a5, fcsr, zero<br> [0x80001cec]:sw t5, 408(ra)<br> [0x80001cf0]:sw a5, 416(ra)<br>                                 |
| 167|[0x80007818]<br>0x667F3BCB<br> [0x80007820]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffffffffc and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001d10]:fsqrt.d t5, t3, dyn<br> [0x80001d14]:csrrs a5, fcsr, zero<br> [0x80001d18]:sw t5, 424(ra)<br> [0x80001d1c]:sw a5, 432(ra)<br>                                 |
| 168|[0x80007828]<br>0xD236A590<br> [0x80007830]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000000004 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001d38]:fsqrt.d t5, t3, dyn<br> [0x80001d3c]:csrrs a5, fcsr, zero<br> [0x80001d40]:sw t5, 440(ra)<br> [0x80001d44]:sw a5, 448(ra)<br>                                 |
| 169|[0x80007838]<br>0x667F3BCC<br> [0x80007840]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xffffffffffffe and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001d64]:fsqrt.d t5, t3, dyn<br> [0x80001d68]:csrrs a5, fcsr, zero<br> [0x80001d6c]:sw t5, 456(ra)<br> [0x80001d70]:sw a5, 464(ra)<br>                                 |
| 170|[0x80007848]<br>0xD236A590<br> [0x80007850]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xe000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001d8c]:fsqrt.d t5, t3, dyn<br> [0x80001d90]:csrrs a5, fcsr, zero<br> [0x80001d94]:sw t5, 472(ra)<br> [0x80001d98]:sw a5, 480(ra)<br>                                 |
| 171|[0x80007858]<br>0x667F3BCC<br> [0x80007860]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001db8]:fsqrt.d t5, t3, dyn<br> [0x80001dbc]:csrrs a5, fcsr, zero<br> [0x80001dc0]:sw t5, 488(ra)<br> [0x80001dc4]:sw a5, 496(ra)<br>                                 |
| 172|[0x80007868]<br>0x667F3BCD<br> [0x80007870]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001de0]:fsqrt.d t5, t3, dyn<br> [0x80001de4]:csrrs a5, fcsr, zero<br> [0x80001de8]:sw t5, 504(ra)<br> [0x80001dec]:sw a5, 512(ra)<br>                                 |
| 173|[0x80007878]<br>0xFFFFFFFF<br> [0x80007880]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001e0c]:fsqrt.d t5, t3, dyn<br> [0x80001e10]:csrrs a5, fcsr, zero<br> [0x80001e14]:sw t5, 520(ra)<br> [0x80001e18]:sw a5, 528(ra)<br>                                 |
| 174|[0x80007888]<br>0xE8584CAA<br> [0x80007890]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001e34]:fsqrt.d t5, t3, dyn<br> [0x80001e38]:csrrs a5, fcsr, zero<br> [0x80001e3c]:sw t5, 536(ra)<br> [0x80001e40]:sw a5, 544(ra)<br>                                 |
| 175|[0x80007898]<br>0xE8584CAA<br> [0x800078a0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x7ffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001e60]:fsqrt.d t5, t3, dyn<br> [0x80001e64]:csrrs a5, fcsr, zero<br> [0x80001e68]:sw t5, 552(ra)<br> [0x80001e6c]:sw a5, 560(ra)<br>                                 |
| 176|[0x800078a8]<br>0x11683F49<br> [0x800078b0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xc000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001e88]:fsqrt.d t5, t3, dyn<br> [0x80001e8c]:csrrs a5, fcsr, zero<br> [0x80001e90]:sw t5, 568(ra)<br> [0x80001e94]:sw a5, 576(ra)<br>                                 |
| 177|[0x800078b8]<br>0x3ADA5B52<br> [0x800078c0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x3ffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001eb4]:fsqrt.d t5, t3, dyn<br> [0x80001eb8]:csrrs a5, fcsr, zero<br> [0x80001ebc]:sw t5, 584(ra)<br> [0x80001ec0]:sw a5, 592(ra)<br>                                 |
| 178|[0x800078c8]<br>0xB14F4EDA<br> [0x800078d0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xe000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001edc]:fsqrt.d t5, t3, dyn<br> [0x80001ee0]:csrrs a5, fcsr, zero<br> [0x80001ee4]:sw t5, 600(ra)<br> [0x80001ee8]:sw a5, 608(ra)<br>                                 |
| 179|[0x800078d8]<br>0xFFFFFFFF<br> [0x800078e0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x1ffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001f08]:fsqrt.d t5, t3, dyn<br> [0x80001f0c]:csrrs a5, fcsr, zero<br> [0x80001f10]:sw t5, 616(ra)<br> [0x80001f14]:sw a5, 624(ra)<br>                                 |
| 180|[0x800078e8]<br>0xEB8D4F12<br> [0x800078f0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xf000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001f30]:fsqrt.d t5, t3, dyn<br> [0x80001f34]:csrrs a5, fcsr, zero<br> [0x80001f38]:sw t5, 632(ra)<br> [0x80001f3c]:sw a5, 640(ra)<br>                                 |
| 181|[0x800078f8]<br>0x0DB3A3A1<br> [0x80007900]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x0ffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001f5c]:fsqrt.d t5, t3, dyn<br> [0x80001f60]:csrrs a5, fcsr, zero<br> [0x80001f64]:sw t5, 648(ra)<br> [0x80001f68]:sw a5, 656(ra)<br>                                 |
| 182|[0x80007908]<br>0x7EBC755F<br> [0x80007910]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xf800000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001f84]:fsqrt.d t5, t3, dyn<br> [0x80001f88]:csrrs a5, fcsr, zero<br> [0x80001f8c]:sw t5, 664(ra)<br> [0x80001f90]:sw a5, 672(ra)<br>                                 |
| 183|[0x80007918]<br>0xA162D0EF<br> [0x80007920]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x07fffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001fb0]:fsqrt.d t5, t3, dyn<br> [0x80001fb4]:csrrs a5, fcsr, zero<br> [0x80001fb8]:sw t5, 680(ra)<br> [0x80001fbc]:sw a5, 688(ra)<br>                                 |
| 184|[0x80007928]<br>0xEFEBE3D6<br> [0x80007930]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xfc00000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80001fd8]:fsqrt.d t5, t3, dyn<br> [0x80001fdc]:csrrs a5, fcsr, zero<br> [0x80001fe0]:sw t5, 696(ra)<br> [0x80001fe4]:sw a5, 704(ra)<br>                                 |
| 185|[0x80007938]<br>0xBBB212EB<br> [0x80007940]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x03fffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002004]:fsqrt.d t5, t3, dyn<br> [0x80002008]:csrrs a5, fcsr, zero<br> [0x8000200c]:sw t5, 712(ra)<br> [0x80002010]:sw a5, 720(ra)<br>                                 |
| 186|[0x80007948]<br>0xFDFEBF1F<br> [0x80007950]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xfe00000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000202c]:fsqrt.d t5, t3, dyn<br> [0x80002030]:csrrs a5, fcsr, zero<br> [0x80002034]:sw t5, 728(ra)<br> [0x80002038]:sw a5, 736(ra)<br>                                 |
| 187|[0x80007958]<br>0xBFD8C647<br> [0x80007960]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x01fffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002058]:fsqrt.d t5, t3, dyn<br> [0x8000205c]:csrrs a5, fcsr, zero<br> [0x80002060]:sw t5, 744(ra)<br> [0x80002064]:sw a5, 752(ra)<br>                                 |
| 188|[0x80007968]<br>0xFFBFEBF9<br> [0x80007970]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xff00000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002080]:fsqrt.d t5, t3, dyn<br> [0x80002084]:csrrs a5, fcsr, zero<br> [0x80002088]:sw t5, 760(ra)<br> [0x8000208c]:sw a5, 768(ra)<br>                                 |
| 189|[0x80007978]<br>0xE307D6D8<br> [0x80007980]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x00fffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800020ac]:fsqrt.d t5, t3, dyn<br> [0x800020b0]:csrrs a5, fcsr, zero<br> [0x800020b4]:sw t5, 776(ra)<br> [0x800020b8]:sw a5, 784(ra)<br>                                 |
| 190|[0x80007988]<br>0xBFF7FEC0<br> [0x80007990]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xff80000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800020d8]:fsqrt.d t5, t3, dyn<br> [0x800020dc]:csrrs a5, fcsr, zero<br> [0x800020e0]:sw t5, 792(ra)<br> [0x800020e4]:sw a5, 800(ra)<br>                                 |
| 191|[0x80007998]<br>0xD9411B84<br> [0x800079a0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x007ffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002104]:fsqrt.d t5, t3, dyn<br> [0x80002108]:csrrs a5, fcsr, zero<br> [0x8000210c]:sw t5, 808(ra)<br> [0x80002110]:sw a5, 816(ra)<br>                                 |
| 192|[0x800079a8]<br>0xEFFEFFEC<br> [0x800079b0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xffc0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002130]:fsqrt.d t5, t3, dyn<br> [0x80002134]:csrrs a5, fcsr, zero<br> [0x80002138]:sw t5, 824(ra)<br> [0x8000213c]:sw a5, 832(ra)<br>                                 |
| 193|[0x800079b8]<br>0x4D10762C<br> [0x800079c0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x003ffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000215c]:fsqrt.d t5, t3, dyn<br> [0x80002160]:csrrs a5, fcsr, zero<br> [0x80002164]:sw t5, 840(ra)<br> [0x80002168]:sw a5, 848(ra)<br>                                 |
| 194|[0x800079c8]<br>0xFBFFDFFF<br> [0x800079d0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xffe0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002188]:fsqrt.d t5, t3, dyn<br> [0x8000218c]:csrrs a5, fcsr, zero<br> [0x80002190]:sw t5, 856(ra)<br> [0x80002194]:sw a5, 864(ra)<br>                                 |
| 195|[0x800079d8]<br>0x65160983<br> [0x800079e0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x001ffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800021b4]:fsqrt.d t5, t3, dyn<br> [0x800021b8]:csrrs a5, fcsr, zero<br> [0x800021bc]:sw t5, 872(ra)<br> [0x800021c0]:sw a5, 880(ra)<br>                                 |
| 196|[0x800079e8]<br>0xFEFFFC00<br> [0x800079f0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xfff0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800021e0]:fsqrt.d t5, t3, dyn<br> [0x800021e4]:csrrs a5, fcsr, zero<br> [0x800021e8]:sw t5, 888(ra)<br> [0x800021ec]:sw a5, 896(ra)<br>                                 |
| 197|[0x800079f8]<br>0x689E7299<br> [0x80007a00]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x000ffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000220c]:fsqrt.d t5, t3, dyn<br> [0x80002210]:csrrs a5, fcsr, zero<br> [0x80002214]:sw t5, 904(ra)<br> [0x80002218]:sw a5, 912(ra)<br>                                 |
| 198|[0x80007a08]<br>0xFFBFFF80<br> [0x80007a10]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xfff8000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002238]:fsqrt.d t5, t3, dyn<br> [0x8000223c]:csrrs a5, fcsr, zero<br> [0x80002240]:sw t5, 920(ra)<br> [0x80002244]:sw a5, 928(ra)<br>                                 |
| 199|[0x80007a18]<br>0xE843D3AA<br> [0x80007a20]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x0007fffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002264]:fsqrt.d t5, t3, dyn<br> [0x80002268]:csrrs a5, fcsr, zero<br> [0x8000226c]:sw t5, 936(ra)<br> [0x80002270]:sw a5, 944(ra)<br>                                 |
| 200|[0x80007678]<br>0x667F3BD7<br> [0x80007680]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x000000000000f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002e54]:fsqrt.d t5, t3, dyn<br> [0x80002e58]:csrrs a5, fcsr, zero<br> [0x80002e5c]:sw t5, 0(ra)<br> [0x80002e60]:sw a5, 8(ra)<br>                                     |
| 201|[0x80007688]<br>0xFFFFFFFC<br> [0x80007690]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xffffffffffff8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002e80]:fsqrt.d t5, t3, dyn<br> [0x80002e84]:csrrs a5, fcsr, zero<br> [0x80002e88]:sw t5, 16(ra)<br> [0x80002e8c]:sw a5, 24(ra)<br>                                   |
| 202|[0x80007698]<br>0x667F3BD2<br> [0x800076a0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x0000000000007 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002ea8]:fsqrt.d t5, t3, dyn<br> [0x80002eac]:csrrs a5, fcsr, zero<br> [0x80002eb0]:sw t5, 32(ra)<br> [0x80002eb4]:sw a5, 40(ra)<br>                                   |
| 203|[0x800076a8]<br>0xFFFFFFFE<br> [0x800076b0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xffffffffffffc and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002ed4]:fsqrt.d t5, t3, dyn<br> [0x80002ed8]:csrrs a5, fcsr, zero<br> [0x80002edc]:sw t5, 48(ra)<br> [0x80002ee0]:sw a5, 56(ra)<br>                                   |
| 204|[0x800076b8]<br>0x667F3BCF<br> [0x800076c0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x0000000000003 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002efc]:fsqrt.d t5, t3, dyn<br> [0x80002f00]:csrrs a5, fcsr, zero<br> [0x80002f04]:sw t5, 64(ra)<br> [0x80002f08]:sw a5, 72(ra)<br>                                   |
| 205|[0x800076c8]<br>0xFFFFFFFF<br> [0x800076d0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0xffffffffffffe and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002f28]:fsqrt.d t5, t3, dyn<br> [0x80002f2c]:csrrs a5, fcsr, zero<br> [0x80002f30]:sw t5, 80(ra)<br> [0x80002f34]:sw a5, 88(ra)<br>                                   |
| 206|[0x800076d8]<br>0x667F3BCD<br> [0x800076e0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x37e and fm1 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002f50]:fsqrt.d t5, t3, dyn<br> [0x80002f54]:csrrs a5, fcsr, zero<br> [0x80002f58]:sw t5, 96(ra)<br> [0x80002f5c]:sw a5, 104(ra)<br>                                  |
| 207|[0x800076e8]<br>0xD236A58F<br> [0x800076f0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xe000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002f78]:fsqrt.d t5, t3, dyn<br> [0x80002f7c]:csrrs a5, fcsr, zero<br> [0x80002f80]:sw t5, 112(ra)<br> [0x80002f84]:sw a5, 120(ra)<br>                                 |
| 208|[0x800076f8]<br>0xA9D2F8EA<br> [0x80007700]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002fa0]:fsqrt.d t5, t3, dyn<br> [0x80002fa4]:csrrs a5, fcsr, zero<br> [0x80002fa8]:sw t5, 128(ra)<br> [0x80002fac]:sw a5, 136(ra)<br>                                 |
| 209|[0x80007708]<br>0xA4A8D9F3<br> [0x80007710]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xd000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002fc8]:fsqrt.d t5, t3, dyn<br> [0x80002fcc]:csrrs a5, fcsr, zero<br> [0x80002fd0]:sw t5, 144(ra)<br> [0x80002fd4]:sw a5, 152(ra)<br>                                 |
| 210|[0x80007718]<br>0x002A9D5A<br> [0x80007720]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc800000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80002ff0]:fsqrt.d t5, t3, dyn<br> [0x80002ff4]:csrrs a5, fcsr, zero<br> [0x80002ff8]:sw t5, 160(ra)<br> [0x80002ffc]:sw a5, 168(ra)<br>                                 |
| 211|[0x80007728]<br>0x5D52A9DA<br> [0x80007730]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xd800000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80003018]:fsqrt.d t5, t3, dyn<br> [0x8000301c]:csrrs a5, fcsr, zero<br> [0x80003020]:sw t5, 176(ra)<br> [0x80003024]:sw a5, 184(ra)<br>                                 |
| 212|[0x80007738]<br>0x78D2D036<br> [0x80007740]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc400000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80003040]:fsqrt.d t5, t3, dyn<br> [0x80003044]:csrrs a5, fcsr, zero<br> [0x80003048]:sw t5, 192(ra)<br> [0x8000304c]:sw a5, 200(ra)<br>                                 |
| 213|[0x80007748]<br>0xB6DBADC5<br> [0x80007750]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdc00000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80003068]:fsqrt.d t5, t3, dyn<br> [0x8000306c]:csrrs a5, fcsr, zero<br> [0x80003070]:sw t5, 208(ra)<br> [0x80003074]:sw a5, 216(ra)<br>                                 |
| 214|[0x80007758]<br>0x80174810<br> [0x80007760]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc200000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80003090]:fsqrt.d t5, t3, dyn<br> [0x80003094]:csrrs a5, fcsr, zero<br> [0x80003098]:sw t5, 224(ra)<br> [0x8000309c]:sw a5, 232(ra)<br>                                 |
| 215|[0x80007768]<br>0x673B05DF<br> [0x80007770]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xde00000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800030b8]:fsqrt.d t5, t3, dyn<br> [0x800030bc]:csrrs a5, fcsr, zero<br> [0x800030c0]:sw t5, 240(ra)<br> [0x800030c4]:sw a5, 248(ra)<br>                                 |
| 216|[0x80007778]<br>0xF161F4A5<br> [0x80007780]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc100000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800030e0]:fsqrt.d t5, t3, dyn<br> [0x800030e4]:csrrs a5, fcsr, zero<br> [0x800030e8]:sw t5, 256(ra)<br> [0x800030ec]:sw a5, 264(ra)<br>                                 |
| 217|[0x80007788]<br>0x64C44CA1<br> [0x80007790]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdf00000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80003108]:fsqrt.d t5, t3, dyn<br> [0x8000310c]:csrrs a5, fcsr, zero<br> [0x80003110]:sw t5, 272(ra)<br> [0x80003114]:sw a5, 280(ra)<br>                                 |
| 218|[0x80007798]<br>0x84CD4081<br> [0x800077a0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc080000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80003134]:fsqrt.d t5, t3, dyn<br> [0x80003138]:csrrs a5, fcsr, zero<br> [0x8000313c]:sw t5, 288(ra)<br> [0x80003140]:sw a5, 296(ra)<br>                                 |
| 219|[0x800077a8]<br>0xCD6C4E54<br> [0x800077b0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdf80000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80003160]:fsqrt.d t5, t3, dyn<br> [0x80003164]:csrrs a5, fcsr, zero<br> [0x80003168]:sw t5, 304(ra)<br> [0x8000316c]:sw a5, 312(ra)<br>                                 |
| 220|[0x800077b8]<br>0xA51FC391<br> [0x800077c0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc040000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000318c]:fsqrt.d t5, t3, dyn<br> [0x80003190]:csrrs a5, fcsr, zero<br> [0x80003194]:sw t5, 320(ra)<br> [0x80003198]:sw a5, 328(ra)<br>                                 |
| 221|[0x800077c8]<br>0xDC4AAFA7<br> [0x800077d0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdfc0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800031b8]:fsqrt.d t5, t3, dyn<br> [0x800031bc]:csrrs a5, fcsr, zero<br> [0x800031c0]:sw t5, 336(ra)<br> [0x800031c4]:sw a5, 344(ra)<br>                                 |
| 222|[0x800077d8]<br>0x2AEDA69F<br> [0x800077e0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc020000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800031e4]:fsqrt.d t5, t3, dyn<br> [0x800031e8]:csrrs a5, fcsr, zero<br> [0x800031ec]:sw t5, 352(ra)<br> [0x800031f0]:sw a5, 360(ra)<br>                                 |
| 223|[0x800077e8]<br>0xDA5EA82C<br> [0x800077f0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdfe0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80003210]:fsqrt.d t5, t3, dyn<br> [0x80003214]:csrrs a5, fcsr, zero<br> [0x80003218]:sw t5, 368(ra)<br> [0x8000321c]:sw a5, 376(ra)<br>                                 |
| 224|[0x800077f8]<br>0x6B3D6DB4<br> [0x80007800]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc010000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000323c]:fsqrt.d t5, t3, dyn<br> [0x80003240]:csrrs a5, fcsr, zero<br> [0x80003244]:sw t5, 384(ra)<br> [0x80003248]:sw a5, 392(ra)<br>                                 |
| 225|[0x80007808]<br>0x57121C48<br> [0x80007810]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdff0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80003268]:fsqrt.d t5, t3, dyn<br> [0x8000326c]:csrrs a5, fcsr, zero<br> [0x80003270]:sw t5, 400(ra)<br> [0x80003274]:sw a5, 408(ra)<br>                                 |
| 226|[0x80007818]<br>0x0ABF7C46<br> [0x80007820]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc008000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80003294]:fsqrt.d t5, t3, dyn<br> [0x80003298]:csrrs a5, fcsr, zero<br> [0x8000329c]:sw t5, 416(ra)<br> [0x800032a0]:sw a5, 424(ra)<br>                                 |
| 227|[0x80007828]<br>0x14D63D07<br> [0x80007830]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdff8000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800032c0]:fsqrt.d t5, t3, dyn<br> [0x800032c4]:csrrs a5, fcsr, zero<br> [0x800032c8]:sw t5, 432(ra)<br> [0x800032cc]:sw a5, 440(ra)<br>                                 |
| 228|[0x80007838]<br>0xDA570D05<br> [0x80007840]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc004000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800032ec]:fsqrt.d t5, t3, dyn<br> [0x800032f0]:csrrs a5, fcsr, zero<br> [0x800032f4]:sw t5, 448(ra)<br> [0x800032f8]:sw a5, 456(ra)<br>                                 |
| 229|[0x80007848]<br>0x7392E82A<br> [0x80007850]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdffc000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80003318]:fsqrt.d t5, t3, dyn<br> [0x8000331c]:csrrs a5, fcsr, zero<br> [0x80003320]:sw t5, 464(ra)<br> [0x80003324]:sw a5, 472(ra)<br>                                 |
| 230|[0x80007858]<br>0xC2187799<br> [0x80007860]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc002000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80003344]:fsqrt.d t5, t3, dyn<br> [0x80003348]:csrrs a5, fcsr, zero<br> [0x8000334c]:sw t5, 480(ra)<br> [0x80003350]:sw a5, 488(ra)<br>                                 |
| 231|[0x80007868]<br>0x22E7E48F<br> [0x80007870]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdffe000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80003370]:fsqrt.d t5, t3, dyn<br> [0x80003374]:csrrs a5, fcsr, zero<br> [0x80003378]:sw t5, 496(ra)<br> [0x8000337c]:sw a5, 504(ra)<br>                                 |
| 232|[0x80007878]<br>0xB5F6956A<br> [0x80007880]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc001000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000339c]:fsqrt.d t5, t3, dyn<br> [0x800033a0]:csrrs a5, fcsr, zero<br> [0x800033a4]:sw t5, 512(ra)<br> [0x800033a8]:sw a5, 520(ra)<br>                                 |
| 233|[0x80007888]<br>0xFA900C7B<br> [0x80007890]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdfff000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800033c8]:fsqrt.d t5, t3, dyn<br> [0x800033cc]:csrrs a5, fcsr, zero<br> [0x800033d0]:sw t5, 528(ra)<br> [0x800033d4]:sw a5, 536(ra)<br>                                 |
| 234|[0x80007898]<br>0xAFE4FE74<br> [0x800078a0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc000800000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800033f4]:fsqrt.d t5, t3, dyn<br> [0x800033f8]:csrrs a5, fcsr, zero<br> [0x800033fc]:sw t5, 544(ra)<br> [0x80003400]:sw a5, 552(ra)<br>                                 |
| 235|[0x800078a8]<br>0xE6638AE0<br> [0x800078b0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdfff800000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80003420]:fsqrt.d t5, t3, dyn<br> [0x80003424]:csrrs a5, fcsr, zero<br> [0x80003428]:sw t5, 560(ra)<br> [0x8000342c]:sw a5, 568(ra)<br>                                 |
| 236|[0x800078b8]<br>0x2CDC0982<br> [0x800078c0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc000400000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000344c]:fsqrt.d t5, t3, dyn<br> [0x80003450]:csrrs a5, fcsr, zero<br> [0x80003454]:sw t5, 576(ra)<br> [0x80003458]:sw a5, 584(ra)<br>                                 |
| 237|[0x800078c8]<br>0x5C4D24AE<br> [0x800078d0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdfffc00000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80003478]:fsqrt.d t5, t3, dyn<br> [0x8000347c]:csrrs a5, fcsr, zero<br> [0x80003480]:sw t5, 592(ra)<br> [0x80003484]:sw a5, 600(ra)<br>                                 |
| 238|[0x800078d8]<br>0x6B5784AA<br> [0x800078e0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc000200000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800034a4]:fsqrt.d t5, t3, dyn<br> [0x800034a8]:csrrs a5, fcsr, zero<br> [0x800034ac]:sw t5, 608(ra)<br> [0x800034b0]:sw a5, 616(ra)<br>                                 |
| 239|[0x800078e8]<br>0x1741E83C<br> [0x800078f0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdfffe00000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800034d0]:fsqrt.d t5, t3, dyn<br> [0x800034d4]:csrrs a5, fcsr, zero<br> [0x800034d8]:sw t5, 624(ra)<br> [0x800034dc]:sw a5, 632(ra)<br>                                 |
| 240|[0x800078f8]<br>0x0A953FA7<br> [0x80007900]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc000100000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800034fc]:fsqrt.d t5, t3, dyn<br> [0x80003500]:csrrs a5, fcsr, zero<br> [0x80003504]:sw t5, 640(ra)<br> [0x80003508]:sw a5, 648(ra)<br>                                 |
| 241|[0x80007908]<br>0x74BC47AD<br> [0x80007910]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdffff00000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80003528]:fsqrt.d t5, t3, dyn<br> [0x8000352c]:csrrs a5, fcsr, zero<br> [0x80003530]:sw t5, 656(ra)<br> [0x80003534]:sw a5, 664(ra)<br>                                 |
| 242|[0x80007918]<br>0xDA341C80<br> [0x80007920]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc000080000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80003550]:fsqrt.d t5, t3, dyn<br> [0x80003554]:csrrs a5, fcsr, zero<br> [0x80003558]:sw t5, 672(ra)<br> [0x8000355c]:sw a5, 680(ra)<br>                                 |
| 243|[0x80007928]<br>0xA37976D0<br> [0x80007930]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdffff80000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000357c]:fsqrt.d t5, t3, dyn<br> [0x80003580]:csrrs a5, fcsr, zero<br> [0x80003584]:sw t5, 688(ra)<br> [0x80003588]:sw a5, 696(ra)<br>                                 |
| 244|[0x80007938]<br>0xC2038AC3<br> [0x80007940]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc000040000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800035a4]:fsqrt.d t5, t3, dyn<br> [0x800035a8]:csrrs a5, fcsr, zero<br> [0x800035ac]:sw t5, 704(ra)<br> [0x800035b0]:sw a5, 712(ra)<br>                                 |
| 245|[0x80007948]<br>0xBAD80E3C<br> [0x80007950]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdffffc0000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800035d0]:fsqrt.d t5, t3, dyn<br> [0x800035d4]:csrrs a5, fcsr, zero<br> [0x800035d8]:sw t5, 720(ra)<br> [0x800035dc]:sw a5, 728(ra)<br>                                 |
| 246|[0x80007958]<br>0xB5EB41DA<br> [0x80007960]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc000020000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800035f8]:fsqrt.d t5, t3, dyn<br> [0x800035fc]:csrrs a5, fcsr, zero<br> [0x80003600]:sw t5, 736(ra)<br> [0x80003604]:sw a5, 744(ra)<br>                                 |
| 247|[0x80007968]<br>0xC68759E9<br> [0x80007970]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdffffe0000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80003624]:fsqrt.d t5, t3, dyn<br> [0x80003628]:csrrs a5, fcsr, zero<br> [0x8000362c]:sw t5, 752(ra)<br> [0x80003630]:sw a5, 760(ra)<br>                                 |
| 248|[0x80007978]<br>0xAFDF1D63<br> [0x80007980]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc000010000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000364c]:fsqrt.d t5, t3, dyn<br> [0x80003650]:csrrs a5, fcsr, zero<br> [0x80003654]:sw t5, 768(ra)<br> [0x80003658]:sw a5, 776(ra)<br>                                 |
| 249|[0x80007988]<br>0xCC5EFFBD<br> [0x80007990]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdfffff0000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80003678]:fsqrt.d t5, t3, dyn<br> [0x8000367c]:csrrs a5, fcsr, zero<br> [0x80003680]:sw t5, 784(ra)<br> [0x80003684]:sw a5, 792(ra)<br>                                 |
| 250|[0x80007998]<br>0xACD90B26<br> [0x800079a0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc000008000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800036a0]:fsqrt.d t5, t3, dyn<br> [0x800036a4]:csrrs a5, fcsr, zero<br> [0x800036a8]:sw t5, 800(ra)<br> [0x800036ac]:sw a5, 808(ra)<br>                                 |
| 251|[0x800079a8]<br>0xCF4AD2A6<br> [0x800079b0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdfffff8000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800036cc]:fsqrt.d t5, t3, dyn<br> [0x800036d0]:csrrs a5, fcsr, zero<br> [0x800036d4]:sw t5, 816(ra)<br> [0x800036d8]:sw a5, 824(ra)<br>                                 |
| 252|[0x800079b8]<br>0xAB560208<br> [0x800079c0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc000004000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800036f4]:fsqrt.d t5, t3, dyn<br> [0x800036f8]:csrrs a5, fcsr, zero<br> [0x800036fc]:sw t5, 832(ra)<br> [0x80003700]:sw a5, 840(ra)<br>                                 |
| 253|[0x800079c8]<br>0xD0C0BC1A<br> [0x800079d0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdfffffc000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80003720]:fsqrt.d t5, t3, dyn<br> [0x80003724]:csrrs a5, fcsr, zero<br> [0x80003728]:sw t5, 848(ra)<br> [0x8000372c]:sw a5, 856(ra)<br>                                 |
| 254|[0x800079d8]<br>0xAA947D79<br> [0x800079e0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc000002000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80003748]:fsqrt.d t5, t3, dyn<br> [0x8000374c]:csrrs a5, fcsr, zero<br> [0x80003750]:sw t5, 864(ra)<br> [0x80003754]:sw a5, 872(ra)<br>                                 |
| 255|[0x800079e8]<br>0xD17BB0D5<br> [0x800079f0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdfffffe000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80003774]:fsqrt.d t5, t3, dyn<br> [0x80003778]:csrrs a5, fcsr, zero<br> [0x8000377c]:sw t5, 880(ra)<br> [0x80003780]:sw a5, 888(ra)<br>                                 |
| 256|[0x800079f8]<br>0xAA33BB31<br> [0x80007a00]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc000001000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000379c]:fsqrt.d t5, t3, dyn<br> [0x800037a0]:csrrs a5, fcsr, zero<br> [0x800037a4]:sw t5, 896(ra)<br> [0x800037a8]:sw a5, 904(ra)<br>                                 |
| 257|[0x80007a08]<br>0xD1D92B32<br> [0x80007a10]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xdffffff000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800037c8]:fsqrt.d t5, t3, dyn<br> [0x800037cc]:csrrs a5, fcsr, zero<br> [0x800037d0]:sw t5, 912(ra)<br> [0x800037d4]:sw a5, 920(ra)<br>                                 |
| 258|[0x80007a18]<br>0xAA035A0E<br> [0x80007a20]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x47f and fm1 == 0xc000000800000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800037f0]:fsqrt.d t5, t3, dyn<br> [0x800037f4]:csrrs a5, fcsr, zero<br> [0x800037f8]:sw t5, 928(ra)<br> [0x800037fc]:sw a5, 936(ra)<br>                                 |
