
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800008b0')]      |
| SIG_REGION                | [('0x80002310', '0x800024a0', '100 words')]      |
| COV_LABELS                | fsqrt.d_b2      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-fsqrt1/fsqrt.d_b2-01.S/ref.S    |
| Total Number of coverpoints| 81     |
| Total Coverpoints Hit     | 81      |
| Total Signature Updates   | 62      |
| STAT1                     | 31      |
| STAT2                     | 0      |
| STAT3                     | 17     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000618]:fsqrt.d t5, t3, dyn
[0x8000061c]:csrrs a5, fcsr, zero
[0x80000620]:sw t5, 304(ra)
[0x80000624]:sw a5, 312(ra)
[0x80000628]:lw t3, 160(a4)
[0x8000062c]:lw t4, 164(a4)
[0x80000630]:addi t3, zero, 512
[0x80000634]:lui t4, 258304
[0x80000638]:addi a0, zero, 0
[0x8000063c]:csrrw zero, fcsr, a0
[0x80000640]:fsqrt.d t5, t3, dyn
[0x80000644]:csrrs a5, fcsr, zero

[0x80000640]:fsqrt.d t5, t3, dyn
[0x80000644]:csrrs a5, fcsr, zero
[0x80000648]:sw t5, 320(ra)
[0x8000064c]:sw a5, 328(ra)
[0x80000650]:lw t3, 168(a4)
[0x80000654]:lw t4, 172(a4)
[0x80000658]:addi t3, zero, 1024
[0x8000065c]:lui t4, 258304
[0x80000660]:addi a0, zero, 0
[0x80000664]:csrrw zero, fcsr, a0
[0x80000668]:fsqrt.d t5, t3, dyn
[0x8000066c]:csrrs a5, fcsr, zero

[0x80000668]:fsqrt.d t5, t3, dyn
[0x8000066c]:csrrs a5, fcsr, zero
[0x80000670]:sw t5, 336(ra)
[0x80000674]:sw a5, 344(ra)
[0x80000678]:lw t3, 176(a4)
[0x8000067c]:lw t4, 180(a4)
[0x80000680]:lui t3, 1
[0x80000684]:addi t3, t3, 2048
[0x80000688]:lui t4, 258304
[0x8000068c]:addi a0, zero, 0
[0x80000690]:csrrw zero, fcsr, a0
[0x80000694]:fsqrt.d t5, t3, dyn
[0x80000698]:csrrs a5, fcsr, zero

[0x80000694]:fsqrt.d t5, t3, dyn
[0x80000698]:csrrs a5, fcsr, zero
[0x8000069c]:sw t5, 352(ra)
[0x800006a0]:sw a5, 360(ra)
[0x800006a4]:lw t3, 184(a4)
[0x800006a8]:lw t4, 188(a4)
[0x800006ac]:lui t3, 1
[0x800006b0]:lui t4, 258304
[0x800006b4]:addi a0, zero, 0
[0x800006b8]:csrrw zero, fcsr, a0
[0x800006bc]:fsqrt.d t5, t3, dyn
[0x800006c0]:csrrs a5, fcsr, zero

[0x800006bc]:fsqrt.d t5, t3, dyn
[0x800006c0]:csrrs a5, fcsr, zero
[0x800006c4]:sw t5, 368(ra)
[0x800006c8]:sw a5, 376(ra)
[0x800006cc]:lw t3, 192(a4)
[0x800006d0]:lw t4, 196(a4)
[0x800006d4]:lui t3, 2
[0x800006d8]:lui t4, 258304
[0x800006dc]:addi a0, zero, 0
[0x800006e0]:csrrw zero, fcsr, a0
[0x800006e4]:fsqrt.d t5, t3, dyn
[0x800006e8]:csrrs a5, fcsr, zero

[0x800006e4]:fsqrt.d t5, t3, dyn
[0x800006e8]:csrrs a5, fcsr, zero
[0x800006ec]:sw t5, 384(ra)
[0x800006f0]:sw a5, 392(ra)
[0x800006f4]:lw t3, 200(a4)
[0x800006f8]:lw t4, 204(a4)
[0x800006fc]:lui t3, 4
[0x80000700]:lui t4, 258304
[0x80000704]:addi a0, zero, 0
[0x80000708]:csrrw zero, fcsr, a0
[0x8000070c]:fsqrt.d t5, t3, dyn
[0x80000710]:csrrs a5, fcsr, zero

[0x8000070c]:fsqrt.d t5, t3, dyn
[0x80000710]:csrrs a5, fcsr, zero
[0x80000714]:sw t5, 400(ra)
[0x80000718]:sw a5, 408(ra)
[0x8000071c]:lw t3, 208(a4)
[0x80000720]:lw t4, 212(a4)
[0x80000724]:lui t3, 8
[0x80000728]:lui t4, 258304
[0x8000072c]:addi a0, zero, 0
[0x80000730]:csrrw zero, fcsr, a0
[0x80000734]:fsqrt.d t5, t3, dyn
[0x80000738]:csrrs a5, fcsr, zero

[0x80000734]:fsqrt.d t5, t3, dyn
[0x80000738]:csrrs a5, fcsr, zero
[0x8000073c]:sw t5, 416(ra)
[0x80000740]:sw a5, 424(ra)
[0x80000744]:lw t3, 216(a4)
[0x80000748]:lw t4, 220(a4)
[0x8000074c]:lui t3, 16
[0x80000750]:lui t4, 258304
[0x80000754]:addi a0, zero, 0
[0x80000758]:csrrw zero, fcsr, a0
[0x8000075c]:fsqrt.d t5, t3, dyn
[0x80000760]:csrrs a5, fcsr, zero

[0x8000075c]:fsqrt.d t5, t3, dyn
[0x80000760]:csrrs a5, fcsr, zero
[0x80000764]:sw t5, 432(ra)
[0x80000768]:sw a5, 440(ra)
[0x8000076c]:lw t3, 224(a4)
[0x80000770]:lw t4, 228(a4)
[0x80000774]:lui t3, 32
[0x80000778]:lui t4, 258304
[0x8000077c]:addi a0, zero, 0
[0x80000780]:csrrw zero, fcsr, a0
[0x80000784]:fsqrt.d t5, t3, dyn
[0x80000788]:csrrs a5, fcsr, zero

[0x80000784]:fsqrt.d t5, t3, dyn
[0x80000788]:csrrs a5, fcsr, zero
[0x8000078c]:sw t5, 448(ra)
[0x80000790]:sw a5, 456(ra)
[0x80000794]:lw t3, 232(a4)
[0x80000798]:lw t4, 236(a4)
[0x8000079c]:lui t3, 64
[0x800007a0]:lui t4, 258304
[0x800007a4]:addi a0, zero, 0
[0x800007a8]:csrrw zero, fcsr, a0
[0x800007ac]:fsqrt.d t5, t3, dyn
[0x800007b0]:csrrs a5, fcsr, zero

[0x800007ac]:fsqrt.d t5, t3, dyn
[0x800007b0]:csrrs a5, fcsr, zero
[0x800007b4]:sw t5, 464(ra)
[0x800007b8]:sw a5, 472(ra)
[0x800007bc]:lw t3, 240(a4)
[0x800007c0]:lw t4, 244(a4)
[0x800007c4]:lui t3, 128
[0x800007c8]:lui t4, 258304
[0x800007cc]:addi a0, zero, 0
[0x800007d0]:csrrw zero, fcsr, a0
[0x800007d4]:fsqrt.d t5, t3, dyn
[0x800007d8]:csrrs a5, fcsr, zero

[0x800007d4]:fsqrt.d t5, t3, dyn
[0x800007d8]:csrrs a5, fcsr, zero
[0x800007dc]:sw t5, 480(ra)
[0x800007e0]:sw a5, 488(ra)
[0x800007e4]:lw t3, 248(a4)
[0x800007e8]:lw t4, 252(a4)
[0x800007ec]:lui t3, 256
[0x800007f0]:lui t4, 258304
[0x800007f4]:addi a0, zero, 0
[0x800007f8]:csrrw zero, fcsr, a0
[0x800007fc]:fsqrt.d t5, t3, dyn
[0x80000800]:csrrs a5, fcsr, zero

[0x800007fc]:fsqrt.d t5, t3, dyn
[0x80000800]:csrrs a5, fcsr, zero
[0x80000804]:sw t5, 496(ra)
[0x80000808]:sw a5, 504(ra)
[0x8000080c]:lw t3, 256(a4)
[0x80000810]:lw t4, 260(a4)
[0x80000814]:lui t3, 512
[0x80000818]:lui t4, 258304
[0x8000081c]:addi a0, zero, 0
[0x80000820]:csrrw zero, fcsr, a0
[0x80000824]:fsqrt.d t5, t3, dyn
[0x80000828]:csrrs a5, fcsr, zero

[0x80000824]:fsqrt.d t5, t3, dyn
[0x80000828]:csrrs a5, fcsr, zero
[0x8000082c]:sw t5, 512(ra)
[0x80000830]:sw a5, 520(ra)
[0x80000834]:lw t3, 264(a4)
[0x80000838]:lw t4, 268(a4)
[0x8000083c]:lui t3, 1024
[0x80000840]:lui t4, 258304
[0x80000844]:addi a0, zero, 0
[0x80000848]:csrrw zero, fcsr, a0
[0x8000084c]:fsqrt.d t5, t3, dyn
[0x80000850]:csrrs a5, fcsr, zero

[0x8000084c]:fsqrt.d t5, t3, dyn
[0x80000850]:csrrs a5, fcsr, zero
[0x80000854]:sw t5, 528(ra)
[0x80000858]:sw a5, 536(ra)
[0x8000085c]:lw t3, 272(a4)
[0x80000860]:lw t4, 276(a4)
[0x80000864]:lui t3, 2048
[0x80000868]:lui t4, 258304
[0x8000086c]:addi a0, zero, 0
[0x80000870]:csrrw zero, fcsr, a0
[0x80000874]:fsqrt.d t5, t3, dyn
[0x80000878]:csrrs a5, fcsr, zero

[0x80000874]:fsqrt.d t5, t3, dyn
[0x80000878]:csrrs a5, fcsr, zero
[0x8000087c]:sw t5, 544(ra)
[0x80000880]:sw a5, 552(ra)
[0x80000884]:lw t3, 280(a4)
[0x80000888]:lw t4, 284(a4)
[0x8000088c]:addi t3, zero, 0
[0x80000890]:lui t4, 524032
[0x80000894]:addi a0, zero, 0
[0x80000898]:csrrw zero, fcsr, a0
[0x8000089c]:fsqrt.d t5, t3, dyn
[0x800008a0]:csrrs a5, fcsr, zero

[0x8000089c]:fsqrt.d t5, t3, dyn
[0x800008a0]:csrrs a5, fcsr, zero
[0x800008a4]:sw t5, 560(ra)
[0x800008a8]:sw a5, 568(ra)
[0x800008ac]:addi zero, zero, 0



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                          signature                           |                                                                                 coverpoints                                                                                 |                                                                   code                                                                    |
|---:|--------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002318]<br>0x00000000<br> [0x80002320]<br>0x00000000<br> |- mnemonic : fsqrt.d<br> - rs1 : x30<br> - rd : x30<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x8000012c]:fsqrt.d t5, t5, dyn<br> [0x80000130]:csrrs tp, fcsr, zero<br> [0x80000134]:sw t5, 0(ra)<br> [0x80000138]:sw tp, 8(ra)<br>     |
|   2|[0x80002328]<br>0x00000001<br> [0x80002330]<br>0x00000001<br> |- rs1 : x26<br> - rd : x28<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                          |[0x80000154]:fsqrt.d t3, s10, dyn<br> [0x80000158]:csrrs tp, fcsr, zero<br> [0x8000015c]:sw t3, 16(ra)<br> [0x80000160]:sw tp, 24(ra)<br>  |
|   3|[0x80002338]<br>0x00000002<br> [0x80002340]<br>0x00000001<br> |- rs1 : x28<br> - rd : x26<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000004 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x8000017c]:fsqrt.d s10, t3, dyn<br> [0x80000180]:csrrs tp, fcsr, zero<br> [0x80000184]:sw s10, 32(ra)<br> [0x80000188]:sw tp, 40(ra)<br> |
|   4|[0x80002348]<br>0x00000004<br> [0x80002350]<br>0x00000001<br> |- rs1 : x22<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000008 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800001a4]:fsqrt.d s8, s6, dyn<br> [0x800001a8]:csrrs tp, fcsr, zero<br> [0x800001ac]:sw s8, 48(ra)<br> [0x800001b0]:sw tp, 56(ra)<br>   |
|   5|[0x80002358]<br>0x00000008<br> [0x80002360]<br>0x00000001<br> |- rs1 : x24<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000010 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800001cc]:fsqrt.d s6, s8, dyn<br> [0x800001d0]:csrrs tp, fcsr, zero<br> [0x800001d4]:sw s6, 64(ra)<br> [0x800001d8]:sw tp, 72(ra)<br>   |
|   6|[0x80002368]<br>0x00000010<br> [0x80002370]<br>0x00000001<br> |- rs1 : x18<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000020 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800001f4]:fsqrt.d s4, s2, dyn<br> [0x800001f8]:csrrs tp, fcsr, zero<br> [0x800001fc]:sw s4, 80(ra)<br> [0x80000200]:sw tp, 88(ra)<br>   |
|   7|[0x80002378]<br>0x00000020<br> [0x80002380]<br>0x00000001<br> |- rs1 : x20<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000040 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x8000021c]:fsqrt.d s2, s4, dyn<br> [0x80000220]:csrrs tp, fcsr, zero<br> [0x80000224]:sw s2, 96(ra)<br> [0x80000228]:sw tp, 104(ra)<br>  |
|   8|[0x80002388]<br>0x00000040<br> [0x80002390]<br>0x00000001<br> |- rs1 : x14<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000080 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000244]:fsqrt.d a6, a4, dyn<br> [0x80000248]:csrrs tp, fcsr, zero<br> [0x8000024c]:sw a6, 112(ra)<br> [0x80000250]:sw tp, 120(ra)<br> |
|   9|[0x80002398]<br>0x00000080<br> [0x800023a0]<br>0x00000001<br> |- rs1 : x16<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000100 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x8000026c]:fsqrt.d a4, a6, dyn<br> [0x80000270]:csrrs tp, fcsr, zero<br> [0x80000274]:sw a4, 128(ra)<br> [0x80000278]:sw tp, 136(ra)<br> |
|  10|[0x800023a8]<br>0x00000100<br> [0x800023b0]<br>0x00000001<br> |- rs1 : x10<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000200 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000294]:fsqrt.d a2, a0, dyn<br> [0x80000298]:csrrs tp, fcsr, zero<br> [0x8000029c]:sw a2, 144(ra)<br> [0x800002a0]:sw tp, 152(ra)<br> |
|  11|[0x800023b8]<br>0x00000200<br> [0x800023c0]<br>0x00000001<br> |- rs1 : x12<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000400 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800002c4]:fsqrt.d a0, a2, dyn<br> [0x800002c8]:csrrs a5, fcsr, zero<br> [0x800002cc]:sw a0, 160(ra)<br> [0x800002d0]:sw a5, 168(ra)<br> |
|  12|[0x800023c8]<br>0x00000400<br> [0x800023d0]<br>0x00000001<br> |- rs1 : x6<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000800 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x800002f0]:fsqrt.d fp, t1, dyn<br> [0x800002f4]:csrrs a5, fcsr, zero<br> [0x800002f8]:sw fp, 176(ra)<br> [0x800002fc]:sw a5, 184(ra)<br> |
|  13|[0x80002378]<br>0x00000800<br> [0x80002380]<br>0x00000001<br> |- rs1 : x8<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000001000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x80000320]:fsqrt.d t1, fp, dyn<br> [0x80000324]:csrrs a5, fcsr, zero<br> [0x80000328]:sw t1, 0(ra)<br> [0x8000032c]:sw a5, 8(ra)<br>     |
|  14|[0x80002388]<br>0x00001000<br> [0x80002390]<br>0x00000001<br> |- rs1 : x2<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000002000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x80000348]:fsqrt.d tp, sp, dyn<br> [0x8000034c]:csrrs a5, fcsr, zero<br> [0x80000350]:sw tp, 16(ra)<br> [0x80000354]:sw a5, 24(ra)<br>   |
|  15|[0x80002398]<br>0x00002000<br> [0x800023a0]<br>0x00000001<br> |- rs1 : x4<br> - rd : x2<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000004000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x80000370]:fsqrt.d sp, tp, dyn<br> [0x80000374]:csrrs a5, fcsr, zero<br> [0x80000378]:sw sp, 32(ra)<br> [0x8000037c]:sw a5, 40(ra)<br>   |
|  16|[0x800023a8]<br>0x00004000<br> [0x800023b0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000008000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000398]:fsqrt.d t5, t3, dyn<br> [0x8000039c]:csrrs a5, fcsr, zero<br> [0x800003a0]:sw t5, 48(ra)<br> [0x800003a4]:sw a5, 56(ra)<br>   |
|  17|[0x800023b8]<br>0x00008000<br> [0x800023c0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000010000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800003c0]:fsqrt.d t5, t3, dyn<br> [0x800003c4]:csrrs a5, fcsr, zero<br> [0x800003c8]:sw t5, 64(ra)<br> [0x800003cc]:sw a5, 72(ra)<br>   |
|  18|[0x800023c8]<br>0x00010000<br> [0x800023d0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000020000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800003e8]:fsqrt.d t5, t3, dyn<br> [0x800003ec]:csrrs a5, fcsr, zero<br> [0x800003f0]:sw t5, 80(ra)<br> [0x800003f4]:sw a5, 88(ra)<br>   |
|  19|[0x800023d8]<br>0x00020000<br> [0x800023e0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000040000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000410]:fsqrt.d t5, t3, dyn<br> [0x80000414]:csrrs a5, fcsr, zero<br> [0x80000418]:sw t5, 96(ra)<br> [0x8000041c]:sw a5, 104(ra)<br>  |
|  20|[0x800023e8]<br>0x00040000<br> [0x800023f0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000080000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000438]:fsqrt.d t5, t3, dyn<br> [0x8000043c]:csrrs a5, fcsr, zero<br> [0x80000440]:sw t5, 112(ra)<br> [0x80000444]:sw a5, 120(ra)<br> |
|  21|[0x800023f8]<br>0x00080000<br> [0x80002400]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000100000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000460]:fsqrt.d t5, t3, dyn<br> [0x80000464]:csrrs a5, fcsr, zero<br> [0x80000468]:sw t5, 128(ra)<br> [0x8000046c]:sw a5, 136(ra)<br> |
|  22|[0x80002408]<br>0x00100000<br> [0x80002410]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000200000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000488]:fsqrt.d t5, t3, dyn<br> [0x8000048c]:csrrs a5, fcsr, zero<br> [0x80000490]:sw t5, 144(ra)<br> [0x80000494]:sw a5, 152(ra)<br> |
|  23|[0x80002418]<br>0x00200000<br> [0x80002420]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000400000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800004b0]:fsqrt.d t5, t3, dyn<br> [0x800004b4]:csrrs a5, fcsr, zero<br> [0x800004b8]:sw t5, 160(ra)<br> [0x800004bc]:sw a5, 168(ra)<br> |
|  24|[0x80002428]<br>0x00400000<br> [0x80002430]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000800000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800004d8]:fsqrt.d t5, t3, dyn<br> [0x800004dc]:csrrs a5, fcsr, zero<br> [0x800004e0]:sw t5, 176(ra)<br> [0x800004e4]:sw a5, 184(ra)<br> |
|  25|[0x80002438]<br>0x00000001<br> [0x80002440]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000500]:fsqrt.d t5, t3, dyn<br> [0x80000504]:csrrs a5, fcsr, zero<br> [0x80000508]:sw t5, 192(ra)<br> [0x8000050c]:sw a5, 200(ra)<br> |
|  26|[0x80002448]<br>0x00000002<br> [0x80002450]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000000004 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000528]:fsqrt.d t5, t3, dyn<br> [0x8000052c]:csrrs a5, fcsr, zero<br> [0x80000530]:sw t5, 208(ra)<br> [0x80000534]:sw a5, 216(ra)<br> |
|  27|[0x80002458]<br>0x00000004<br> [0x80002460]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000000008 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000550]:fsqrt.d t5, t3, dyn<br> [0x80000554]:csrrs a5, fcsr, zero<br> [0x80000558]:sw t5, 224(ra)<br> [0x8000055c]:sw a5, 232(ra)<br> |
|  28|[0x80002468]<br>0x00000008<br> [0x80002470]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000000010 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000578]:fsqrt.d t5, t3, dyn<br> [0x8000057c]:csrrs a5, fcsr, zero<br> [0x80000580]:sw t5, 240(ra)<br> [0x80000584]:sw a5, 248(ra)<br> |
|  29|[0x80002478]<br>0x00000010<br> [0x80002480]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000000020 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800005a0]:fsqrt.d t5, t3, dyn<br> [0x800005a4]:csrrs a5, fcsr, zero<br> [0x800005a8]:sw t5, 256(ra)<br> [0x800005ac]:sw a5, 264(ra)<br> |
|  30|[0x80002488]<br>0x00000020<br> [0x80002490]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000000040 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800005c8]:fsqrt.d t5, t3, dyn<br> [0x800005cc]:csrrs a5, fcsr, zero<br> [0x800005d0]:sw t5, 272(ra)<br> [0x800005d4]:sw a5, 280(ra)<br> |
|  31|[0x80002498]<br>0x00000040<br> [0x800024a0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000000080 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800005f0]:fsqrt.d t5, t3, dyn<br> [0x800005f4]:csrrs a5, fcsr, zero<br> [0x800005f8]:sw t5, 288(ra)<br> [0x800005fc]:sw a5, 296(ra)<br> |
