
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800019c0')]      |
| SIG_REGION                | [('0x80003610', '0x80003a70', '280 words')]      |
| COV_LABELS                | fsqrt.d_b20      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-fsqrt1/fsqrt.d_b20-01.S/ref.S    |
| Total Number of coverpoints| 170     |
| Total Coverpoints Hit     | 170      |
| Total Signature Updates   | 152      |
| STAT1                     | 76      |
| STAT2                     | 0      |
| STAT3                     | 61     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000e74]:fsqrt.d t5, t3, dyn
[0x80000e78]:csrrs a5, fcsr, zero
[0x80000e7c]:sw t5, 1024(ra)
[0x80000e80]:sw a5, 1032(ra)
[0x80000e84]:lw t3, 520(a4)
[0x80000e88]:lw t4, 524(a4)
[0x80000e8c]:lui t3, 367701
[0x80000e90]:addi t3, t3, 2968
[0x80000e94]:lui t4, 121880
[0x80000e98]:addi t4, t4, 1214
[0x80000e9c]:addi a0, zero, 0
[0x80000ea0]:csrrw zero, fcsr, a0
[0x80000ea4]:fsqrt.d t5, t3, dyn
[0x80000ea8]:csrrs a5, fcsr, zero

[0x80000ea4]:fsqrt.d t5, t3, dyn
[0x80000ea8]:csrrs a5, fcsr, zero
[0x80000eac]:sw t5, 1040(ra)
[0x80000eb0]:sw a5, 1048(ra)
[0x80000eb4]:lw t3, 528(a4)
[0x80000eb8]:lw t4, 532(a4)
[0x80000ebc]:lui t3, 302657
[0x80000ec0]:addi t3, t3, 2543
[0x80000ec4]:lui t4, 378176
[0x80000ec8]:addi t4, t4, 1306
[0x80000ecc]:addi a0, zero, 0
[0x80000ed0]:csrrw zero, fcsr, a0
[0x80000ed4]:fsqrt.d t5, t3, dyn
[0x80000ed8]:csrrs a5, fcsr, zero

[0x80000ed4]:fsqrt.d t5, t3, dyn
[0x80000ed8]:csrrs a5, fcsr, zero
[0x80000edc]:sw t5, 1056(ra)
[0x80000ee0]:sw a5, 1064(ra)
[0x80000ee4]:lw t3, 536(a4)
[0x80000ee8]:lw t4, 540(a4)
[0x80000eec]:lui t3, 358113
[0x80000ef0]:addi t3, t3, 2302
[0x80000ef4]:lui t4, 461738
[0x80000ef8]:addi t4, t4, 3001
[0x80000efc]:addi a0, zero, 0
[0x80000f00]:csrrw zero, fcsr, a0
[0x80000f04]:fsqrt.d t5, t3, dyn
[0x80000f08]:csrrs a5, fcsr, zero

[0x80000f04]:fsqrt.d t5, t3, dyn
[0x80000f08]:csrrs a5, fcsr, zero
[0x80000f0c]:sw t5, 1072(ra)
[0x80000f10]:sw a5, 1080(ra)
[0x80000f14]:lw t3, 544(a4)
[0x80000f18]:lw t4, 548(a4)
[0x80000f1c]:lui t3, 340061
[0x80000f20]:addi t3, t3, 163
[0x80000f24]:lui t4, 128734
[0x80000f28]:addi t4, t4, 4084
[0x80000f2c]:addi a0, zero, 0
[0x80000f30]:csrrw zero, fcsr, a0
[0x80000f34]:fsqrt.d t5, t3, dyn
[0x80000f38]:csrrs a5, fcsr, zero

[0x80000f34]:fsqrt.d t5, t3, dyn
[0x80000f38]:csrrs a5, fcsr, zero
[0x80000f3c]:sw t5, 1088(ra)
[0x80000f40]:sw a5, 1096(ra)
[0x80000f44]:lw t3, 552(a4)
[0x80000f48]:lw t4, 556(a4)
[0x80000f4c]:lui t3, 479080
[0x80000f50]:addi t3, t3, 1844
[0x80000f54]:lui t4, 443539
[0x80000f58]:addi t4, t4, 1775
[0x80000f5c]:addi a0, zero, 0
[0x80000f60]:csrrw zero, fcsr, a0
[0x80000f64]:fsqrt.d t5, t3, dyn
[0x80000f68]:csrrs a5, fcsr, zero

[0x80000f64]:fsqrt.d t5, t3, dyn
[0x80000f68]:csrrs a5, fcsr, zero
[0x80000f6c]:sw t5, 1104(ra)
[0x80000f70]:sw a5, 1112(ra)
[0x80000f74]:lw t3, 560(a4)
[0x80000f78]:lw t4, 564(a4)
[0x80000f7c]:lui t3, 507786
[0x80000f80]:addi t3, t3, 973
[0x80000f84]:lui t4, 168736
[0x80000f88]:addi t4, t4, 2607
[0x80000f8c]:addi a0, zero, 0
[0x80000f90]:csrrw zero, fcsr, a0
[0x80000f94]:fsqrt.d t5, t3, dyn
[0x80000f98]:csrrs a5, fcsr, zero

[0x80000f94]:fsqrt.d t5, t3, dyn
[0x80000f98]:csrrs a5, fcsr, zero
[0x80000f9c]:sw t5, 1120(ra)
[0x80000fa0]:sw a5, 1128(ra)
[0x80000fa4]:lw t3, 568(a4)
[0x80000fa8]:lw t4, 572(a4)
[0x80000fac]:lui t3, 120394
[0x80000fb0]:addi t3, t3, 3461
[0x80000fb4]:lui t4, 223771
[0x80000fb8]:addi t4, t4, 2445
[0x80000fbc]:addi a0, zero, 0
[0x80000fc0]:csrrw zero, fcsr, a0
[0x80000fc4]:fsqrt.d t5, t3, dyn
[0x80000fc8]:csrrs a5, fcsr, zero

[0x80000fc4]:fsqrt.d t5, t3, dyn
[0x80000fc8]:csrrs a5, fcsr, zero
[0x80000fcc]:sw t5, 1136(ra)
[0x80000fd0]:sw a5, 1144(ra)
[0x80000fd4]:lw t3, 576(a4)
[0x80000fd8]:lw t4, 580(a4)
[0x80000fdc]:lui t3, 445088
[0x80000fe0]:addi t3, t3, 3699
[0x80000fe4]:lui t4, 355529
[0x80000fe8]:addi t4, t4, 29
[0x80000fec]:addi a0, zero, 0
[0x80000ff0]:csrrw zero, fcsr, a0
[0x80000ff4]:fsqrt.d t5, t3, dyn
[0x80000ff8]:csrrs a5, fcsr, zero

[0x80000ff4]:fsqrt.d t5, t3, dyn
[0x80000ff8]:csrrs a5, fcsr, zero
[0x80000ffc]:sw t5, 1152(ra)
[0x80001000]:sw a5, 1160(ra)
[0x80001004]:lw t3, 584(a4)
[0x80001008]:lw t4, 588(a4)
[0x8000100c]:lui t3, 556122
[0x80001010]:addi t3, t3, 2929
[0x80001014]:lui t4, 128781
[0x80001018]:addi t4, t4, 2088
[0x8000101c]:addi a0, zero, 0
[0x80001020]:csrrw zero, fcsr, a0
[0x80001024]:fsqrt.d t5, t3, dyn
[0x80001028]:csrrs a5, fcsr, zero

[0x80001024]:fsqrt.d t5, t3, dyn
[0x80001028]:csrrs a5, fcsr, zero
[0x8000102c]:sw t5, 1168(ra)
[0x80001030]:sw a5, 1176(ra)
[0x80001034]:lw t3, 592(a4)
[0x80001038]:lw t4, 596(a4)
[0x8000103c]:lui t3, 1021693
[0x80001040]:addi t3, t3, 2524
[0x80001044]:lui t4, 496863
[0x80001048]:addi t4, t4, 1616
[0x8000104c]:addi a0, zero, 0
[0x80001050]:csrrw zero, fcsr, a0
[0x80001054]:fsqrt.d t5, t3, dyn
[0x80001058]:csrrs a5, fcsr, zero

[0x80001054]:fsqrt.d t5, t3, dyn
[0x80001058]:csrrs a5, fcsr, zero
[0x8000105c]:sw t5, 1184(ra)
[0x80001060]:sw a5, 1192(ra)
[0x80001064]:lw t3, 600(a4)
[0x80001068]:lw t4, 604(a4)
[0x8000106c]:lui t3, 83067
[0x80001070]:addi t3, t3, 2482
[0x80001074]:lui t4, 148963
[0x80001078]:addi t4, t4, 3565
[0x8000107c]:addi a0, zero, 0
[0x80001080]:csrrw zero, fcsr, a0
[0x80001084]:fsqrt.d t5, t3, dyn
[0x80001088]:csrrs a5, fcsr, zero

[0x80001084]:fsqrt.d t5, t3, dyn
[0x80001088]:csrrs a5, fcsr, zero
[0x8000108c]:sw t5, 1200(ra)
[0x80001090]:sw a5, 1208(ra)
[0x80001094]:lw t3, 608(a4)
[0x80001098]:lw t4, 612(a4)
[0x8000109c]:lui t3, 421636
[0x800010a0]:addi t3, t3, 1222
[0x800010a4]:lui t4, 152527
[0x800010a8]:addi t4, t4, 280
[0x800010ac]:addi a0, zero, 0
[0x800010b0]:csrrw zero, fcsr, a0
[0x800010b4]:fsqrt.d t5, t3, dyn
[0x800010b8]:csrrs a5, fcsr, zero

[0x800010b4]:fsqrt.d t5, t3, dyn
[0x800010b8]:csrrs a5, fcsr, zero
[0x800010bc]:sw t5, 1216(ra)
[0x800010c0]:sw a5, 1224(ra)
[0x800010c4]:lw t3, 616(a4)
[0x800010c8]:lw t4, 620(a4)
[0x800010cc]:lui t3, 103277
[0x800010d0]:addi t3, t3, 3663
[0x800010d4]:lui t4, 4418
[0x800010d8]:addi t4, t4, 487
[0x800010dc]:addi a0, zero, 0
[0x800010e0]:csrrw zero, fcsr, a0
[0x800010e4]:fsqrt.d t5, t3, dyn
[0x800010e8]:csrrs a5, fcsr, zero

[0x800010e4]:fsqrt.d t5, t3, dyn
[0x800010e8]:csrrs a5, fcsr, zero
[0x800010ec]:sw t5, 1232(ra)
[0x800010f0]:sw a5, 1240(ra)
[0x800010f4]:lw t3, 624(a4)
[0x800010f8]:lw t4, 628(a4)
[0x800010fc]:lui t3, 173019
[0x80001100]:addi t3, t3, 1083
[0x80001104]:lui t4, 89270
[0x80001108]:addi t4, t4, 465
[0x8000110c]:addi a0, zero, 0
[0x80001110]:csrrw zero, fcsr, a0
[0x80001114]:fsqrt.d t5, t3, dyn
[0x80001118]:csrrs a5, fcsr, zero

[0x80001114]:fsqrt.d t5, t3, dyn
[0x80001118]:csrrs a5, fcsr, zero
[0x8000111c]:sw t5, 1248(ra)
[0x80001120]:sw a5, 1256(ra)
[0x80001124]:lw t3, 632(a4)
[0x80001128]:lw t4, 636(a4)
[0x8000112c]:lui t3, 946829
[0x80001130]:addi t3, t3, 3596
[0x80001134]:lui t4, 429135
[0x80001138]:addi t4, t4, 694
[0x8000113c]:addi a0, zero, 0
[0x80001140]:csrrw zero, fcsr, a0
[0x80001144]:fsqrt.d t5, t3, dyn
[0x80001148]:csrrs a5, fcsr, zero

[0x80001144]:fsqrt.d t5, t3, dyn
[0x80001148]:csrrs a5, fcsr, zero
[0x8000114c]:sw t5, 1264(ra)
[0x80001150]:sw a5, 1272(ra)
[0x80001154]:lw t3, 640(a4)
[0x80001158]:lw t4, 644(a4)
[0x8000115c]:lui t3, 283391
[0x80001160]:addi t3, t3, 1495
[0x80001164]:lui t4, 260212
[0x80001168]:addi t4, t4, 1843
[0x8000116c]:addi a0, zero, 0
[0x80001170]:csrrw zero, fcsr, a0
[0x80001174]:fsqrt.d t5, t3, dyn
[0x80001178]:csrrs a5, fcsr, zero

[0x80001174]:fsqrt.d t5, t3, dyn
[0x80001178]:csrrs a5, fcsr, zero
[0x8000117c]:sw t5, 1280(ra)
[0x80001180]:sw a5, 1288(ra)
[0x80001184]:lw t3, 648(a4)
[0x80001188]:lw t4, 652(a4)
[0x8000118c]:lui t3, 768549
[0x80001190]:addi t3, t3, 2183
[0x80001194]:lui t4, 217705
[0x80001198]:addi t4, t4, 3063
[0x8000119c]:addi a0, zero, 0
[0x800011a0]:csrrw zero, fcsr, a0
[0x800011a4]:fsqrt.d t5, t3, dyn
[0x800011a8]:csrrs a5, fcsr, zero

[0x800011a4]:fsqrt.d t5, t3, dyn
[0x800011a8]:csrrs a5, fcsr, zero
[0x800011ac]:sw t5, 1296(ra)
[0x800011b0]:sw a5, 1304(ra)
[0x800011b4]:lw t3, 656(a4)
[0x800011b8]:lw t4, 660(a4)
[0x800011bc]:lui t3, 421179
[0x800011c0]:addi t3, t3, 3875
[0x800011c4]:lui t4, 19957
[0x800011c8]:addi t4, t4, 1237
[0x800011cc]:addi a0, zero, 0
[0x800011d0]:csrrw zero, fcsr, a0
[0x800011d4]:fsqrt.d t5, t3, dyn
[0x800011d8]:csrrs a5, fcsr, zero

[0x800011d4]:fsqrt.d t5, t3, dyn
[0x800011d8]:csrrs a5, fcsr, zero
[0x800011dc]:sw t5, 1312(ra)
[0x800011e0]:sw a5, 1320(ra)
[0x800011e4]:lw t3, 664(a4)
[0x800011e8]:lw t4, 668(a4)
[0x800011ec]:lui t3, 501715
[0x800011f0]:addi t3, t3, 3942
[0x800011f4]:lui t4, 239246
[0x800011f8]:addi t4, t4, 3744
[0x800011fc]:addi a0, zero, 0
[0x80001200]:csrrw zero, fcsr, a0
[0x80001204]:fsqrt.d t5, t3, dyn
[0x80001208]:csrrs a5, fcsr, zero

[0x80001204]:fsqrt.d t5, t3, dyn
[0x80001208]:csrrs a5, fcsr, zero
[0x8000120c]:sw t5, 1328(ra)
[0x80001210]:sw a5, 1336(ra)
[0x80001214]:lw t3, 672(a4)
[0x80001218]:lw t4, 676(a4)
[0x8000121c]:lui t3, 643450
[0x80001220]:addi t3, t3, 1816
[0x80001224]:lui t4, 10321
[0x80001228]:addi t4, t4, 3420
[0x8000122c]:addi a0, zero, 0
[0x80001230]:csrrw zero, fcsr, a0
[0x80001234]:fsqrt.d t5, t3, dyn
[0x80001238]:csrrs a5, fcsr, zero

[0x80001234]:fsqrt.d t5, t3, dyn
[0x80001238]:csrrs a5, fcsr, zero
[0x8000123c]:sw t5, 1344(ra)
[0x80001240]:sw a5, 1352(ra)
[0x80001244]:lw t3, 680(a4)
[0x80001248]:lw t4, 684(a4)
[0x8000124c]:lui t3, 206398
[0x80001250]:addi t3, t3, 2322
[0x80001254]:lui t4, 450123
[0x80001258]:addi t4, t4, 2121
[0x8000125c]:addi a0, zero, 0
[0x80001260]:csrrw zero, fcsr, a0
[0x80001264]:fsqrt.d t5, t3, dyn
[0x80001268]:csrrs a5, fcsr, zero

[0x80001264]:fsqrt.d t5, t3, dyn
[0x80001268]:csrrs a5, fcsr, zero
[0x8000126c]:sw t5, 1360(ra)
[0x80001270]:sw a5, 1368(ra)
[0x80001274]:lw t3, 688(a4)
[0x80001278]:lw t4, 692(a4)
[0x8000127c]:lui t3, 198746
[0x80001280]:addi t3, t3, 319
[0x80001284]:lui t4, 352131
[0x80001288]:addi t4, t4, 323
[0x8000128c]:addi a0, zero, 0
[0x80001290]:csrrw zero, fcsr, a0
[0x80001294]:fsqrt.d t5, t3, dyn
[0x80001298]:csrrs a5, fcsr, zero

[0x80001294]:fsqrt.d t5, t3, dyn
[0x80001298]:csrrs a5, fcsr, zero
[0x8000129c]:sw t5, 1376(ra)
[0x800012a0]:sw a5, 1384(ra)
[0x800012a4]:lw t3, 696(a4)
[0x800012a8]:lw t4, 700(a4)
[0x800012ac]:lui t3, 675857
[0x800012b0]:addi t3, t3, 1471
[0x800012b4]:lui t4, 175248
[0x800012b8]:addi t4, t4, 1011
[0x800012bc]:addi a0, zero, 0
[0x800012c0]:csrrw zero, fcsr, a0
[0x800012c4]:fsqrt.d t5, t3, dyn
[0x800012c8]:csrrs a5, fcsr, zero

[0x800012c4]:fsqrt.d t5, t3, dyn
[0x800012c8]:csrrs a5, fcsr, zero
[0x800012cc]:sw t5, 1392(ra)
[0x800012d0]:sw a5, 1400(ra)
[0x800012d4]:lw t3, 704(a4)
[0x800012d8]:lw t4, 708(a4)
[0x800012dc]:lui t3, 992095
[0x800012e0]:addi t3, t3, 1816
[0x800012e4]:lui t4, 452310
[0x800012e8]:addi t4, t4, 1420
[0x800012ec]:addi a0, zero, 0
[0x800012f0]:csrrw zero, fcsr, a0
[0x800012f4]:fsqrt.d t5, t3, dyn
[0x800012f8]:csrrs a5, fcsr, zero

[0x800012f4]:fsqrt.d t5, t3, dyn
[0x800012f8]:csrrs a5, fcsr, zero
[0x800012fc]:sw t5, 1408(ra)
[0x80001300]:sw a5, 1416(ra)
[0x80001304]:lw t3, 712(a4)
[0x80001308]:lw t4, 716(a4)
[0x8000130c]:lui t3, 296981
[0x80001310]:addi t3, t3, 445
[0x80001314]:lui t4, 287572
[0x80001318]:addi t4, t4, 4059
[0x8000131c]:addi a0, zero, 0
[0x80001320]:csrrw zero, fcsr, a0
[0x80001324]:fsqrt.d t5, t3, dyn
[0x80001328]:csrrs a5, fcsr, zero

[0x80001324]:fsqrt.d t5, t3, dyn
[0x80001328]:csrrs a5, fcsr, zero
[0x8000132c]:sw t5, 1424(ra)
[0x80001330]:sw a5, 1432(ra)
[0x80001334]:lw t3, 720(a4)
[0x80001338]:lw t4, 724(a4)
[0x8000133c]:lui t3, 920615
[0x80001340]:addi t3, t3, 2773
[0x80001344]:lui t4, 304968
[0x80001348]:addi t4, t4, 192
[0x8000134c]:addi a0, zero, 0
[0x80001350]:csrrw zero, fcsr, a0
[0x80001354]:fsqrt.d t5, t3, dyn
[0x80001358]:csrrs a5, fcsr, zero

[0x80001354]:fsqrt.d t5, t3, dyn
[0x80001358]:csrrs a5, fcsr, zero
[0x8000135c]:sw t5, 1440(ra)
[0x80001360]:sw a5, 1448(ra)
[0x80001364]:lw t3, 728(a4)
[0x80001368]:lw t4, 732(a4)
[0x8000136c]:lui t3, 304106
[0x80001370]:addi t3, t3, 4017
[0x80001374]:lui t4, 279483
[0x80001378]:addi t4, t4, 88
[0x8000137c]:addi a0, zero, 0
[0x80001380]:csrrw zero, fcsr, a0
[0x80001384]:fsqrt.d t5, t3, dyn
[0x80001388]:csrrs a5, fcsr, zero

[0x80001384]:fsqrt.d t5, t3, dyn
[0x80001388]:csrrs a5, fcsr, zero
[0x8000138c]:sw t5, 1456(ra)
[0x80001390]:sw a5, 1464(ra)
[0x80001394]:lw t3, 736(a4)
[0x80001398]:lw t4, 740(a4)
[0x8000139c]:lui t3, 656171
[0x800013a0]:addi t3, t3, 1749
[0x800013a4]:lui t4, 7721
[0x800013a8]:addi t4, t4, 2655
[0x800013ac]:addi a0, zero, 0
[0x800013b0]:csrrw zero, fcsr, a0
[0x800013b4]:fsqrt.d t5, t3, dyn
[0x800013b8]:csrrs a5, fcsr, zero

[0x800013b4]:fsqrt.d t5, t3, dyn
[0x800013b8]:csrrs a5, fcsr, zero
[0x800013bc]:sw t5, 1472(ra)
[0x800013c0]:sw a5, 1480(ra)
[0x800013c4]:lw t3, 744(a4)
[0x800013c8]:lw t4, 748(a4)
[0x800013cc]:lui t3, 785331
[0x800013d0]:addi t3, t3, 3604
[0x800013d4]:lui t4, 221543
[0x800013d8]:addi t4, t4, 23
[0x800013dc]:addi a0, zero, 0
[0x800013e0]:csrrw zero, fcsr, a0
[0x800013e4]:fsqrt.d t5, t3, dyn
[0x800013e8]:csrrs a5, fcsr, zero

[0x800013e4]:fsqrt.d t5, t3, dyn
[0x800013e8]:csrrs a5, fcsr, zero
[0x800013ec]:sw t5, 1488(ra)
[0x800013f0]:sw a5, 1496(ra)
[0x800013f4]:lw t3, 752(a4)
[0x800013f8]:lw t4, 756(a4)
[0x800013fc]:lui t3, 929603
[0x80001400]:addi t3, t3, 2593
[0x80001404]:lui t4, 161674
[0x80001408]:addi t4, t4, 354
[0x8000140c]:addi a0, zero, 0
[0x80001410]:csrrw zero, fcsr, a0
[0x80001414]:fsqrt.d t5, t3, dyn
[0x80001418]:csrrs a5, fcsr, zero

[0x80001414]:fsqrt.d t5, t3, dyn
[0x80001418]:csrrs a5, fcsr, zero
[0x8000141c]:sw t5, 1504(ra)
[0x80001420]:sw a5, 1512(ra)
[0x80001424]:lw t3, 760(a4)
[0x80001428]:lw t4, 764(a4)
[0x8000142c]:lui t3, 961220
[0x80001430]:addi t3, t3, 3969
[0x80001434]:lui t4, 272881
[0x80001438]:addi t4, t4, 293
[0x8000143c]:addi a0, zero, 0
[0x80001440]:csrrw zero, fcsr, a0
[0x80001444]:fsqrt.d t5, t3, dyn
[0x80001448]:csrrs a5, fcsr, zero

[0x80001444]:fsqrt.d t5, t3, dyn
[0x80001448]:csrrs a5, fcsr, zero
[0x8000144c]:sw t5, 1520(ra)
[0x80001450]:sw a5, 1528(ra)
[0x80001454]:lw t3, 768(a4)
[0x80001458]:lw t4, 772(a4)
[0x8000145c]:lui t3, 104788
[0x80001460]:addi t3, t3, 464
[0x80001464]:lui t4, 162719
[0x80001468]:addi t4, t4, 3940
[0x8000146c]:addi a0, zero, 0
[0x80001470]:csrrw zero, fcsr, a0
[0x80001474]:fsqrt.d t5, t3, dyn
[0x80001478]:csrrs a5, fcsr, zero

[0x80001474]:fsqrt.d t5, t3, dyn
[0x80001478]:csrrs a5, fcsr, zero
[0x8000147c]:sw t5, 1536(ra)
[0x80001480]:sw a5, 1544(ra)
[0x80001484]:lw t3, 776(a4)
[0x80001488]:lw t4, 780(a4)
[0x8000148c]:lui t3, 417522
[0x80001490]:addi t3, t3, 2792
[0x80001494]:lui t4, 233563
[0x80001498]:addi t4, t4, 3480
[0x8000149c]:addi a0, zero, 0
[0x800014a0]:csrrw zero, fcsr, a0
[0x800014a4]:fsqrt.d t5, t3, dyn
[0x800014a8]:csrrs a5, fcsr, zero

[0x800014a4]:fsqrt.d t5, t3, dyn
[0x800014a8]:csrrs a5, fcsr, zero
[0x800014ac]:sw t5, 1552(ra)
[0x800014b0]:sw a5, 1560(ra)
[0x800014b4]:lw t3, 784(a4)
[0x800014b8]:lw t4, 788(a4)
[0x800014bc]:lui t3, 828993
[0x800014c0]:addi t3, t3, 733
[0x800014c4]:lui t4, 85045
[0x800014c8]:addi t4, t4, 1083
[0x800014cc]:addi a0, zero, 0
[0x800014d0]:csrrw zero, fcsr, a0
[0x800014d4]:fsqrt.d t5, t3, dyn
[0x800014d8]:csrrs a5, fcsr, zero

[0x800014d4]:fsqrt.d t5, t3, dyn
[0x800014d8]:csrrs a5, fcsr, zero
[0x800014dc]:sw t5, 1568(ra)
[0x800014e0]:sw a5, 1576(ra)
[0x800014e4]:lw t3, 792(a4)
[0x800014e8]:lw t4, 796(a4)
[0x800014ec]:lui t3, 292244
[0x800014f0]:addi t3, t3, 3958
[0x800014f4]:lui t4, 428756
[0x800014f8]:addi t4, t4, 3981
[0x800014fc]:addi a0, zero, 0
[0x80001500]:csrrw zero, fcsr, a0
[0x80001504]:fsqrt.d t5, t3, dyn
[0x80001508]:csrrs a5, fcsr, zero

[0x80001504]:fsqrt.d t5, t3, dyn
[0x80001508]:csrrs a5, fcsr, zero
[0x8000150c]:sw t5, 1584(ra)
[0x80001510]:sw a5, 1592(ra)
[0x80001514]:lw t3, 800(a4)
[0x80001518]:lw t4, 804(a4)
[0x8000151c]:lui t3, 627014
[0x80001520]:addi t3, t3, 780
[0x80001524]:lui t4, 389676
[0x80001528]:addi t4, t4, 1923
[0x8000152c]:addi a0, zero, 0
[0x80001530]:csrrw zero, fcsr, a0
[0x80001534]:fsqrt.d t5, t3, dyn
[0x80001538]:csrrs a5, fcsr, zero

[0x80001534]:fsqrt.d t5, t3, dyn
[0x80001538]:csrrs a5, fcsr, zero
[0x8000153c]:sw t5, 1600(ra)
[0x80001540]:sw a5, 1608(ra)
[0x80001544]:lw t3, 808(a4)
[0x80001548]:lw t4, 812(a4)
[0x8000154c]:lui t3, 125414
[0x80001550]:addi t3, t3, 663
[0x80001554]:lui t4, 437413
[0x80001558]:addi t4, t4, 943
[0x8000155c]:addi a0, zero, 0
[0x80001560]:csrrw zero, fcsr, a0
[0x80001564]:fsqrt.d t5, t3, dyn
[0x80001568]:csrrs a5, fcsr, zero

[0x80001564]:fsqrt.d t5, t3, dyn
[0x80001568]:csrrs a5, fcsr, zero
[0x8000156c]:sw t5, 1616(ra)
[0x80001570]:sw a5, 1624(ra)
[0x80001574]:lw t3, 816(a4)
[0x80001578]:lw t4, 820(a4)
[0x8000157c]:lui t3, 454280
[0x80001580]:addi t3, t3, 1213
[0x80001584]:lui t4, 404166
[0x80001588]:addi t4, t4, 2928
[0x8000158c]:addi a0, zero, 0
[0x80001590]:csrrw zero, fcsr, a0
[0x80001594]:fsqrt.d t5, t3, dyn
[0x80001598]:csrrs a5, fcsr, zero

[0x80001594]:fsqrt.d t5, t3, dyn
[0x80001598]:csrrs a5, fcsr, zero
[0x8000159c]:sw t5, 1632(ra)
[0x800015a0]:sw a5, 1640(ra)
[0x800015a4]:lw t3, 824(a4)
[0x800015a8]:lw t4, 828(a4)
[0x800015ac]:lui t3, 825918
[0x800015b0]:addi t3, t3, 4037
[0x800015b4]:lui t4, 225209
[0x800015b8]:addi t4, t4, 124
[0x800015bc]:addi a0, zero, 0
[0x800015c0]:csrrw zero, fcsr, a0
[0x800015c4]:fsqrt.d t5, t3, dyn
[0x800015c8]:csrrs a5, fcsr, zero

[0x800015c4]:fsqrt.d t5, t3, dyn
[0x800015c8]:csrrs a5, fcsr, zero
[0x800015cc]:sw t5, 1648(ra)
[0x800015d0]:sw a5, 1656(ra)
[0x800015d4]:lw t3, 832(a4)
[0x800015d8]:lw t4, 836(a4)
[0x800015dc]:lui t3, 106354
[0x800015e0]:addi t3, t3, 2392
[0x800015e4]:lui t4, 373819
[0x800015e8]:addi t4, t4, 1722
[0x800015ec]:addi a0, zero, 0
[0x800015f0]:csrrw zero, fcsr, a0
[0x800015f4]:fsqrt.d t5, t3, dyn
[0x800015f8]:csrrs a5, fcsr, zero

[0x800015f4]:fsqrt.d t5, t3, dyn
[0x800015f8]:csrrs a5, fcsr, zero
[0x800015fc]:sw t5, 1664(ra)
[0x80001600]:sw a5, 1672(ra)
[0x80001604]:lw t3, 840(a4)
[0x80001608]:lw t4, 844(a4)
[0x8000160c]:lui t3, 742622
[0x80001610]:addi t3, t3, 3025
[0x80001614]:lui t4, 352985
[0x80001618]:addi t4, t4, 2696
[0x8000161c]:addi a0, zero, 0
[0x80001620]:csrrw zero, fcsr, a0
[0x80001624]:fsqrt.d t5, t3, dyn
[0x80001628]:csrrs a5, fcsr, zero

[0x80001624]:fsqrt.d t5, t3, dyn
[0x80001628]:csrrs a5, fcsr, zero
[0x8000162c]:sw t5, 1680(ra)
[0x80001630]:sw a5, 1688(ra)
[0x80001634]:lw t3, 848(a4)
[0x80001638]:lw t4, 852(a4)
[0x8000163c]:lui t3, 617768
[0x80001640]:addi t3, t3, 1714
[0x80001644]:lui t4, 380164
[0x80001648]:addi t4, t4, 348
[0x8000164c]:addi a0, zero, 0
[0x80001650]:csrrw zero, fcsr, a0
[0x80001654]:fsqrt.d t5, t3, dyn
[0x80001658]:csrrs a5, fcsr, zero

[0x80001654]:fsqrt.d t5, t3, dyn
[0x80001658]:csrrs a5, fcsr, zero
[0x8000165c]:sw t5, 1696(ra)
[0x80001660]:sw a5, 1704(ra)
[0x80001664]:lw t3, 856(a4)
[0x80001668]:lw t4, 860(a4)
[0x8000166c]:lui t3, 604079
[0x80001670]:addi t3, t3, 3856
[0x80001674]:lui t4, 4121
[0x80001678]:addi t4, t4, 327
[0x8000167c]:addi a0, zero, 0
[0x80001680]:csrrw zero, fcsr, a0
[0x80001684]:fsqrt.d t5, t3, dyn
[0x80001688]:csrrs a5, fcsr, zero

[0x80001684]:fsqrt.d t5, t3, dyn
[0x80001688]:csrrs a5, fcsr, zero
[0x8000168c]:sw t5, 1712(ra)
[0x80001690]:sw a5, 1720(ra)
[0x80001694]:lw t3, 864(a4)
[0x80001698]:lw t4, 868(a4)
[0x8000169c]:lui t3, 516747
[0x800016a0]:addi t3, t3, 1750
[0x800016a4]:lui t4, 227681
[0x800016a8]:addi t4, t4, 2490
[0x800016ac]:addi a0, zero, 0
[0x800016b0]:csrrw zero, fcsr, a0
[0x800016b4]:fsqrt.d t5, t3, dyn
[0x800016b8]:csrrs a5, fcsr, zero

[0x800016b4]:fsqrt.d t5, t3, dyn
[0x800016b8]:csrrs a5, fcsr, zero
[0x800016bc]:sw t5, 1728(ra)
[0x800016c0]:sw a5, 1736(ra)
[0x800016c4]:lw t3, 872(a4)
[0x800016c8]:lw t4, 876(a4)
[0x800016cc]:lui t3, 515190
[0x800016d0]:addi t3, t3, 2629
[0x800016d4]:lui t4, 445658
[0x800016d8]:addi t4, t4, 1155
[0x800016dc]:addi a0, zero, 0
[0x800016e0]:csrrw zero, fcsr, a0
[0x800016e4]:fsqrt.d t5, t3, dyn
[0x800016e8]:csrrs a5, fcsr, zero

[0x800016e4]:fsqrt.d t5, t3, dyn
[0x800016e8]:csrrs a5, fcsr, zero
[0x800016ec]:sw t5, 1744(ra)
[0x800016f0]:sw a5, 1752(ra)
[0x800016f4]:lw t3, 880(a4)
[0x800016f8]:lw t4, 884(a4)
[0x800016fc]:lui t3, 668297
[0x80001700]:addi t3, t3, 3806
[0x80001704]:lui t4, 17937
[0x80001708]:addi t4, t4, 2404
[0x8000170c]:addi a0, zero, 0
[0x80001710]:csrrw zero, fcsr, a0
[0x80001714]:fsqrt.d t5, t3, dyn
[0x80001718]:csrrs a5, fcsr, zero

[0x80001714]:fsqrt.d t5, t3, dyn
[0x80001718]:csrrs a5, fcsr, zero
[0x8000171c]:sw t5, 1760(ra)
[0x80001720]:sw a5, 1768(ra)
[0x80001724]:lw t3, 888(a4)
[0x80001728]:lw t4, 892(a4)
[0x8000172c]:lui t3, 411573
[0x80001730]:addi t3, t3, 597
[0x80001734]:lui t4, 162559
[0x80001738]:addi t4, t4, 3531
[0x8000173c]:addi a0, zero, 0
[0x80001740]:csrrw zero, fcsr, a0
[0x80001744]:fsqrt.d t5, t3, dyn
[0x80001748]:csrrs a5, fcsr, zero

[0x80001744]:fsqrt.d t5, t3, dyn
[0x80001748]:csrrs a5, fcsr, zero
[0x8000174c]:sw t5, 1776(ra)
[0x80001750]:sw a5, 1784(ra)
[0x80001754]:lw t3, 896(a4)
[0x80001758]:lw t4, 900(a4)
[0x8000175c]:lui t3, 415536
[0x80001760]:addi t3, t3, 907
[0x80001764]:lui t4, 138272
[0x80001768]:addi t4, t4, 3167
[0x8000176c]:addi a0, zero, 0
[0x80001770]:csrrw zero, fcsr, a0
[0x80001774]:fsqrt.d t5, t3, dyn
[0x80001778]:csrrs a5, fcsr, zero

[0x80001774]:fsqrt.d t5, t3, dyn
[0x80001778]:csrrs a5, fcsr, zero
[0x8000177c]:sw t5, 1792(ra)
[0x80001780]:sw a5, 1800(ra)
[0x80001784]:lw t3, 904(a4)
[0x80001788]:lw t4, 908(a4)
[0x8000178c]:lui t3, 179411
[0x80001790]:addi t3, t3, 3234
[0x80001794]:lui t4, 47207
[0x80001798]:addi t4, t4, 1749
[0x8000179c]:addi a0, zero, 0
[0x800017a0]:csrrw zero, fcsr, a0
[0x800017a4]:fsqrt.d t5, t3, dyn
[0x800017a8]:csrrs a5, fcsr, zero

[0x800017a4]:fsqrt.d t5, t3, dyn
[0x800017a8]:csrrs a5, fcsr, zero
[0x800017ac]:sw t5, 1808(ra)
[0x800017b0]:sw a5, 1816(ra)
[0x800017b4]:lw t3, 912(a4)
[0x800017b8]:lw t4, 916(a4)
[0x800017bc]:lui t3, 811508
[0x800017c0]:addi t3, t3, 1249
[0x800017c4]:lui t4, 322469
[0x800017c8]:addi t4, t4, 703
[0x800017cc]:addi a0, zero, 0
[0x800017d0]:csrrw zero, fcsr, a0
[0x800017d4]:fsqrt.d t5, t3, dyn
[0x800017d8]:csrrs a5, fcsr, zero

[0x800017d4]:fsqrt.d t5, t3, dyn
[0x800017d8]:csrrs a5, fcsr, zero
[0x800017dc]:sw t5, 1824(ra)
[0x800017e0]:sw a5, 1832(ra)
[0x800017e4]:lw t3, 920(a4)
[0x800017e8]:lw t4, 924(a4)
[0x800017ec]:lui t3, 286090
[0x800017f0]:addi t3, t3, 32
[0x800017f4]:lui t4, 122113
[0x800017f8]:addi t4, t4, 3839
[0x800017fc]:addi a0, zero, 0
[0x80001800]:csrrw zero, fcsr, a0
[0x80001804]:fsqrt.d t5, t3, dyn
[0x80001808]:csrrs a5, fcsr, zero

[0x80001804]:fsqrt.d t5, t3, dyn
[0x80001808]:csrrs a5, fcsr, zero
[0x8000180c]:sw t5, 1840(ra)
[0x80001810]:sw a5, 1848(ra)
[0x80001814]:lw t3, 928(a4)
[0x80001818]:lw t4, 932(a4)
[0x8000181c]:lui t3, 20099
[0x80001820]:addi t3, t3, 3851
[0x80001824]:lui t4, 139446
[0x80001828]:addi t4, t4, 344
[0x8000182c]:addi a0, zero, 0
[0x80001830]:csrrw zero, fcsr, a0
[0x80001834]:fsqrt.d t5, t3, dyn
[0x80001838]:csrrs a5, fcsr, zero

[0x80001834]:fsqrt.d t5, t3, dyn
[0x80001838]:csrrs a5, fcsr, zero
[0x8000183c]:sw t5, 1856(ra)
[0x80001840]:sw a5, 1864(ra)
[0x80001844]:lw t3, 936(a4)
[0x80001848]:lw t4, 940(a4)
[0x8000184c]:lui t3, 227696
[0x80001850]:addi t3, t3, 2720
[0x80001854]:lui t4, 418847
[0x80001858]:addi t4, t4, 1343
[0x8000185c]:addi a0, zero, 0
[0x80001860]:csrrw zero, fcsr, a0
[0x80001864]:fsqrt.d t5, t3, dyn
[0x80001868]:csrrs a5, fcsr, zero

[0x80001864]:fsqrt.d t5, t3, dyn
[0x80001868]:csrrs a5, fcsr, zero
[0x8000186c]:sw t5, 1872(ra)
[0x80001870]:sw a5, 1880(ra)
[0x80001874]:lw t3, 944(a4)
[0x80001878]:lw t4, 948(a4)
[0x8000187c]:lui t3, 505301
[0x80001880]:addi t3, t3, 1315
[0x80001884]:lui t4, 302701
[0x80001888]:addi t4, t4, 1706
[0x8000188c]:addi a0, zero, 0
[0x80001890]:csrrw zero, fcsr, a0
[0x80001894]:fsqrt.d t5, t3, dyn
[0x80001898]:csrrs a5, fcsr, zero

[0x80001894]:fsqrt.d t5, t3, dyn
[0x80001898]:csrrs a5, fcsr, zero
[0x8000189c]:sw t5, 1888(ra)
[0x800018a0]:sw a5, 1896(ra)
[0x800018a4]:lw t3, 952(a4)
[0x800018a8]:lw t4, 956(a4)
[0x800018ac]:lui t3, 621982
[0x800018b0]:addi t3, t3, 211
[0x800018b4]:lui t4, 56761
[0x800018b8]:addi t4, t4, 1581
[0x800018bc]:addi a0, zero, 0
[0x800018c0]:csrrw zero, fcsr, a0
[0x800018c4]:fsqrt.d t5, t3, dyn
[0x800018c8]:csrrs a5, fcsr, zero

[0x800018c4]:fsqrt.d t5, t3, dyn
[0x800018c8]:csrrs a5, fcsr, zero
[0x800018cc]:sw t5, 1904(ra)
[0x800018d0]:sw a5, 1912(ra)
[0x800018d4]:lw t3, 960(a4)
[0x800018d8]:lw t4, 964(a4)
[0x800018dc]:lui t3, 777465
[0x800018e0]:addi t3, t3, 1301
[0x800018e4]:lui t4, 150105
[0x800018e8]:addi t4, t4, 1791
[0x800018ec]:addi a0, zero, 0
[0x800018f0]:csrrw zero, fcsr, a0
[0x800018f4]:fsqrt.d t5, t3, dyn
[0x800018f8]:csrrs a5, fcsr, zero

[0x800018f4]:fsqrt.d t5, t3, dyn
[0x800018f8]:csrrs a5, fcsr, zero
[0x800018fc]:sw t5, 1920(ra)
[0x80001900]:sw a5, 1928(ra)
[0x80001904]:lw t3, 968(a4)
[0x80001908]:lw t4, 972(a4)
[0x8000190c]:lui t3, 159795
[0x80001910]:addi t3, t3, 2451
[0x80001914]:lui t4, 148343
[0x80001918]:addi t4, t4, 772
[0x8000191c]:addi a0, zero, 0
[0x80001920]:csrrw zero, fcsr, a0
[0x80001924]:fsqrt.d t5, t3, dyn
[0x80001928]:csrrs a5, fcsr, zero

[0x80001924]:fsqrt.d t5, t3, dyn
[0x80001928]:csrrs a5, fcsr, zero
[0x8000192c]:sw t5, 1936(ra)
[0x80001930]:sw a5, 1944(ra)
[0x80001934]:lw t3, 976(a4)
[0x80001938]:lw t4, 980(a4)
[0x8000193c]:lui t3, 669638
[0x80001940]:addi t3, t3, 400
[0x80001944]:lui t4, 397346
[0x80001948]:addi t4, t4, 3278
[0x8000194c]:addi a0, zero, 0
[0x80001950]:csrrw zero, fcsr, a0
[0x80001954]:fsqrt.d t5, t3, dyn
[0x80001958]:csrrs a5, fcsr, zero

[0x80001954]:fsqrt.d t5, t3, dyn
[0x80001958]:csrrs a5, fcsr, zero
[0x8000195c]:sw t5, 1952(ra)
[0x80001960]:sw a5, 1960(ra)
[0x80001964]:lw t3, 984(a4)
[0x80001968]:lw t4, 988(a4)
[0x8000196c]:lui t3, 338764
[0x80001970]:addi t3, t3, 3196
[0x80001974]:lui t4, 120505
[0x80001978]:addi t4, t4, 852
[0x8000197c]:addi a0, zero, 0
[0x80001980]:csrrw zero, fcsr, a0
[0x80001984]:fsqrt.d t5, t3, dyn
[0x80001988]:csrrs a5, fcsr, zero

[0x80001984]:fsqrt.d t5, t3, dyn
[0x80001988]:csrrs a5, fcsr, zero
[0x8000198c]:sw t5, 1968(ra)
[0x80001990]:sw a5, 1976(ra)
[0x80001994]:lw t3, 992(a4)
[0x80001998]:lw t4, 996(a4)
[0x8000199c]:addi t3, zero, 0
[0x800019a0]:lui t4, 82176
[0x800019a4]:addi a0, zero, 0
[0x800019a8]:csrrw zero, fcsr, a0
[0x800019ac]:fsqrt.d t5, t3, dyn
[0x800019b0]:csrrs a5, fcsr, zero

[0x800019ac]:fsqrt.d t5, t3, dyn
[0x800019b0]:csrrs a5, fcsr, zero
[0x800019b4]:sw t5, 1984(ra)
[0x800019b8]:sw a5, 1992(ra)
[0x800019bc]:addi zero, zero, 0



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                          signature                           |                                                                                 coverpoints                                                                                 |                                                                    code                                                                     |
|---:|--------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80003618]<br>0x00000000<br> [0x80003620]<br>0x00000000<br> |- mnemonic : fsqrt.d<br> - rs1 : x30<br> - rd : x30<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x8000012c]:fsqrt.d t5, t5, dyn<br> [0x80000130]:csrrs tp, fcsr, zero<br> [0x80000134]:sw t5, 0(ra)<br> [0x80000138]:sw tp, 8(ra)<br>       |
|   2|[0x80003628]<br>0x00000000<br> [0x80003630]<br>0x00000000<br> |- rs1 : x26<br> - rd : x28<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                          |[0x80000154]:fsqrt.d t3, s10, dyn<br> [0x80000158]:csrrs tp, fcsr, zero<br> [0x8000015c]:sw t3, 16(ra)<br> [0x80000160]:sw tp, 24(ra)<br>    |
|   3|[0x80003638]<br>0x00000000<br> [0x80003640]<br>0x00000000<br> |- rs1 : x28<br> - rd : x26<br> - fs1 == 0 and fe1 == 0x1c7 and fm1 == 0x9000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x8000017c]:fsqrt.d s10, t3, dyn<br> [0x80000180]:csrrs tp, fcsr, zero<br> [0x80000184]:sw s10, 32(ra)<br> [0x80000188]:sw tp, 40(ra)<br>   |
|   4|[0x80003648]<br>0x00000000<br> [0x80003650]<br>0x00000000<br> |- rs1 : x22<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x7c7 and fm1 == 0x9000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800001a4]:fsqrt.d s8, s6, dyn<br> [0x800001a8]:csrrs tp, fcsr, zero<br> [0x800001ac]:sw s8, 48(ra)<br> [0x800001b0]:sw tp, 56(ra)<br>     |
|   5|[0x80003658]<br>0x00000000<br> [0x80003660]<br>0x00000000<br> |- rs1 : x24<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x5b1 and fm1 == 0xe400000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800001cc]:fsqrt.d s6, s8, dyn<br> [0x800001d0]:csrrs tp, fcsr, zero<br> [0x800001d4]:sw s6, 64(ra)<br> [0x800001d8]:sw tp, 72(ra)<br>     |
|   6|[0x80003668]<br>0x00000000<br> [0x80003670]<br>0x00000000<br> |- rs1 : x18<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x637 and fm1 == 0xe400000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800001f4]:fsqrt.d s4, s2, dyn<br> [0x800001f8]:csrrs tp, fcsr, zero<br> [0x800001fc]:sw s4, 80(ra)<br> [0x80000200]:sw tp, 88(ra)<br>     |
|   7|[0x80003678]<br>0x00000000<br> [0x80003680]<br>0x00000000<br> |- rs1 : x20<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x792 and fm1 == 0xc200000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x8000021c]:fsqrt.d s2, s4, dyn<br> [0x80000220]:csrrs tp, fcsr, zero<br> [0x80000224]:sw s2, 96(ra)<br> [0x80000228]:sw tp, 104(ra)<br>    |
|   8|[0x80003688]<br>0x00000000<br> [0x80003690]<br>0x00000000<br> |- rs1 : x14<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x426 and fm1 == 0xe080000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000248]:fsqrt.d a6, a4, dyn<br> [0x8000024c]:csrrs tp, fcsr, zero<br> [0x80000250]:sw a6, 112(ra)<br> [0x80000254]:sw tp, 120(ra)<br>   |
|   9|[0x80003698]<br>0x00000000<br> [0x800036a0]<br>0x00000000<br> |- rs1 : x16<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x4b5 and fm1 == 0xb900000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x80000270]:fsqrt.d a4, a6, dyn<br> [0x80000274]:csrrs tp, fcsr, zero<br> [0x80000278]:sw a4, 128(ra)<br> [0x8000027c]:sw tp, 136(ra)<br>   |
|  10|[0x800036a8]<br>0x00000000<br> [0x800036b0]<br>0x00000000<br> |- rs1 : x10<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x40a and fm1 == 0x0880000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x8000029c]:fsqrt.d a2, a0, dyn<br> [0x800002a0]:csrrs tp, fcsr, zero<br> [0x800002a4]:sw a2, 144(ra)<br> [0x800002a8]:sw tp, 152(ra)<br>   |
|  11|[0x800036b8]<br>0x00000000<br> [0x800036c0]<br>0x00000000<br> |- rs1 : x12<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x1d3 and fm1 == 0x2100000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                          |[0x800002cc]:fsqrt.d a0, a2, dyn<br> [0x800002d0]:csrrs a5, fcsr, zero<br> [0x800002d4]:sw a0, 160(ra)<br> [0x800002d8]:sw a5, 168(ra)<br>   |
|  12|[0x800036c8]<br>0x00000000<br> [0x800036d0]<br>0x00000000<br> |- rs1 : x6<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x11a and fm1 == 0xd120000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x800002f8]:fsqrt.d fp, t1, dyn<br> [0x800002fc]:csrrs a5, fcsr, zero<br> [0x80000300]:sw fp, 176(ra)<br> [0x80000304]:sw a5, 184(ra)<br>   |
|  13|[0x80003678]<br>0x00000000<br> [0x80003680]<br>0x00000000<br> |- rs1 : x8<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x2c7 and fm1 == 0xfa40000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x8000032c]:fsqrt.d t1, fp, dyn<br> [0x80000330]:csrrs a5, fcsr, zero<br> [0x80000334]:sw t1, 0(ra)<br> [0x80000338]:sw a5, 8(ra)<br>       |
|  14|[0x80003688]<br>0x00000000<br> [0x80003690]<br>0x00000000<br> |- rs1 : x2<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x46a and fm1 == 0xd120000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x80000358]:fsqrt.d tp, sp, dyn<br> [0x8000035c]:csrrs a5, fcsr, zero<br> [0x80000360]:sw tp, 16(ra)<br> [0x80000364]:sw a5, 24(ra)<br>     |
|  15|[0x80003698]<br>0x00000000<br> [0x800036a0]<br>0x00000000<br> |- rs1 : x4<br> - rd : x2<br> - fs1 == 0 and fe1 == 0x42a and fm1 == 0x2608000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                            |[0x80000384]:fsqrt.d sp, tp, dyn<br> [0x80000388]:csrrs a5, fcsr, zero<br> [0x8000038c]:sw sp, 32(ra)<br> [0x80000390]:sw a5, 40(ra)<br>     |
|  16|[0x800036a8]<br>0x00000000<br> [0x800036b0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x43a and fm1 == 0xf808000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800003b0]:fsqrt.d t5, t3, dyn<br> [0x800003b4]:csrrs a5, fcsr, zero<br> [0x800003b8]:sw t5, 48(ra)<br> [0x800003bc]:sw a5, 56(ra)<br>     |
|  17|[0x800036b8]<br>0x00000000<br> [0x800036c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x322 and fm1 == 0xc988000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800003dc]:fsqrt.d t5, t3, dyn<br> [0x800003e0]:csrrs a5, fcsr, zero<br> [0x800003e4]:sw t5, 64(ra)<br> [0x800003e8]:sw a5, 72(ra)<br>     |
|  18|[0x800036c8]<br>0x00000000<br> [0x800036d0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7c1 and fm1 == 0xffe4000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000408]:fsqrt.d t5, t3, dyn<br> [0x8000040c]:csrrs a5, fcsr, zero<br> [0x80000410]:sw t5, 80(ra)<br> [0x80000414]:sw a5, 88(ra)<br>     |
|  19|[0x800036d8]<br>0x00000000<br> [0x800036e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1cb and fm1 == 0xd3a4000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000434]:fsqrt.d t5, t3, dyn<br> [0x80000438]:csrrs a5, fcsr, zero<br> [0x8000043c]:sw t5, 96(ra)<br> [0x80000440]:sw a5, 104(ra)<br>    |
|  20|[0x800036e8]<br>0x00000000<br> [0x800036f0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x771 and fm1 == 0xf771000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000460]:fsqrt.d t5, t3, dyn<br> [0x80000464]:csrrs a5, fcsr, zero<br> [0x80000468]:sw t5, 112(ra)<br> [0x8000046c]:sw a5, 120(ra)<br>   |
|  21|[0x800036f8]<br>0x00000000<br> [0x80003700]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x67c and fm1 == 0x12a8800000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000048c]:fsqrt.d t5, t3, dyn<br> [0x80000490]:csrrs a5, fcsr, zero<br> [0x80000494]:sw t5, 128(ra)<br> [0x80000498]:sw a5, 136(ra)<br>   |
|  22|[0x80003708]<br>0x00000000<br> [0x80003710]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3d7 and fm1 == 0x8d81000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800004b8]:fsqrt.d t5, t3, dyn<br> [0x800004bc]:csrrs a5, fcsr, zero<br> [0x800004c0]:sw t5, 144(ra)<br> [0x800004c4]:sw a5, 152(ra)<br>   |
|  23|[0x80003718]<br>0x00000000<br> [0x80003720]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3fa and fm1 == 0xdab4800000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800004e4]:fsqrt.d t5, t3, dyn<br> [0x800004e8]:csrrs a5, fcsr, zero<br> [0x800004ec]:sw t5, 160(ra)<br> [0x800004f0]:sw a5, 168(ra)<br>   |
|  24|[0x80003728]<br>0x00000000<br> [0x80003730]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x697 and fm1 == 0xd4fe400000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000510]:fsqrt.d t5, t3, dyn<br> [0x80000514]:csrrs a5, fcsr, zero<br> [0x80000518]:sw t5, 176(ra)<br> [0x8000051c]:sw a5, 184(ra)<br>   |
|  25|[0x80003738]<br>0x00000000<br> [0x80003740]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x19e and fm1 == 0x11ed200000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000053c]:fsqrt.d t5, t3, dyn<br> [0x80000540]:csrrs a5, fcsr, zero<br> [0x80000544]:sw t5, 192(ra)<br> [0x80000548]:sw a5, 200(ra)<br>   |
|  26|[0x80003748]<br>0x00000000<br> [0x80003750]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x350 and fm1 == 0x7c46c80000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000568]:fsqrt.d t5, t3, dyn<br> [0x8000056c]:csrrs a5, fcsr, zero<br> [0x80000570]:sw t5, 208(ra)<br> [0x80000574]:sw a5, 216(ra)<br>   |
|  27|[0x80003758]<br>0x00000000<br> [0x80003760]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x5de and fm1 == 0xd19a080000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000594]:fsqrt.d t5, t3, dyn<br> [0x80000598]:csrrs a5, fcsr, zero<br> [0x8000059c]:sw t5, 224(ra)<br> [0x800005a0]:sw a5, 232(ra)<br>   |
|  28|[0x80003768]<br>0x00000000<br> [0x80003770]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x0db and fm1 == 0xb6b4c40000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800005c0]:fsqrt.d t5, t3, dyn<br> [0x800005c4]:csrrs a5, fcsr, zero<br> [0x800005c8]:sw t5, 240(ra)<br> [0x800005cc]:sw a5, 248(ra)<br>   |
|  29|[0x80003778]<br>0x00000000<br> [0x80003780]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x74e and fm1 == 0x9e67b20000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800005ec]:fsqrt.d t5, t3, dyn<br> [0x800005f0]:csrrs a5, fcsr, zero<br> [0x800005f4]:sw t5, 256(ra)<br> [0x800005f8]:sw a5, 264(ra)<br>   |
|  30|[0x80003788]<br>0x00000000<br> [0x80003790]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x500 and fm1 == 0x21b0a20000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000618]:fsqrt.d t5, t3, dyn<br> [0x8000061c]:csrrs a5, fcsr, zero<br> [0x80000620]:sw t5, 272(ra)<br> [0x80000624]:sw a5, 280(ra)<br>   |
|  31|[0x80003798]<br>0x00000000<br> [0x800037a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x66e and fm1 == 0x22d1c20000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000644]:fsqrt.d t5, t3, dyn<br> [0x80000648]:csrrs a5, fcsr, zero<br> [0x8000064c]:sw t5, 288(ra)<br> [0x80000650]:sw a5, 296(ra)<br>   |
|  32|[0x800037a8]<br>0x00000000<br> [0x800037b0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x282 and fm1 == 0x42d6888000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000670]:fsqrt.d t5, t3, dyn<br> [0x80000674]:csrrs a5, fcsr, zero<br> [0x80000678]:sw t5, 304(ra)<br> [0x8000067c]:sw a5, 312(ra)<br>   |
|  33|[0x800037b8]<br>0x00000000<br> [0x800037c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1de and fm1 == 0x249cb08000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000069c]:fsqrt.d t5, t3, dyn<br> [0x800006a0]:csrrs a5, fcsr, zero<br> [0x800006a4]:sw t5, 320(ra)<br> [0x800006a8]:sw a5, 328(ra)<br>   |
|  34|[0x800037c8]<br>0x00000000<br> [0x800037d0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x6b21548000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800006c8]:fsqrt.d t5, t3, dyn<br> [0x800006cc]:csrrs a5, fcsr, zero<br> [0x800006d0]:sw t5, 336(ra)<br> [0x800006d4]:sw a5, 344(ra)<br>   |
|  35|[0x800037d8]<br>0x00000000<br> [0x800037e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x79d and fm1 == 0xcca7da4000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800006f4]:fsqrt.d t5, t3, dyn<br> [0x800006f8]:csrrs a5, fcsr, zero<br> [0x800006fc]:sw t5, 352(ra)<br> [0x80000700]:sw a5, 360(ra)<br>   |
|  36|[0x800037e8]<br>0x00000000<br> [0x800037f0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ec and fm1 == 0xfe704e2000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000720]:fsqrt.d t5, t3, dyn<br> [0x80000724]:csrrs a5, fcsr, zero<br> [0x80000728]:sw t5, 368(ra)<br> [0x8000072c]:sw a5, 376(ra)<br>   |
|  37|[0x800037f8]<br>0x00000000<br> [0x80003800]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x531 and fm1 == 0x1ef1f04000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000074c]:fsqrt.d t5, t3, dyn<br> [0x80000750]:csrrs a5, fcsr, zero<br> [0x80000754]:sw t5, 384(ra)<br> [0x80000758]:sw a5, 392(ra)<br>   |
|  38|[0x80003808]<br>0x00000000<br> [0x80003810]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x05f and fm1 == 0x19ad084000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000778]:fsqrt.d t5, t3, dyn<br> [0x8000077c]:csrrs a5, fcsr, zero<br> [0x80000780]:sw t5, 400(ra)<br> [0x80000784]:sw a5, 408(ra)<br>   |
|  39|[0x80003818]<br>0x00000000<br> [0x80003820]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x6eb and fm1 == 0x812dd01000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800007a4]:fsqrt.d t5, t3, dyn<br> [0x800007a8]:csrrs a5, fcsr, zero<br> [0x800007ac]:sw t5, 416(ra)<br> [0x800007b0]:sw a5, 424(ra)<br>   |
|  40|[0x80003828]<br>0x00000000<br> [0x80003830]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x00000668b9824 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800007d0]:fsqrt.d t5, t3, dyn<br> [0x800007d4]:csrrs a5, fcsr, zero<br> [0x800007d8]:sw t5, 432(ra)<br> [0x800007dc]:sw a5, 440(ra)<br>   |
|  41|[0x80003838]<br>0x00000000<br> [0x80003840]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x2c5 and fm1 == 0x1fdb0a6400000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800007fc]:fsqrt.d t5, t3, dyn<br> [0x80000800]:csrrs a5, fcsr, zero<br> [0x80000804]:sw t5, 448(ra)<br> [0x80000808]:sw a5, 456(ra)<br>   |
|  42|[0x80003848]<br>0x00000000<br> [0x80003850]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1cd and fm1 == 0x1de7626400000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000828]:fsqrt.d t5, t3, dyn<br> [0x8000082c]:csrrs a5, fcsr, zero<br> [0x80000830]:sw t5, 464(ra)<br> [0x80000834]:sw a5, 472(ra)<br>   |
|  43|[0x80003858]<br>0x00000000<br> [0x80003860]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x15f94b0040000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000854]:fsqrt.d t5, t3, dyn<br> [0x80000858]:csrrs a5, fcsr, zero<br> [0x8000085c]:sw t5, 480(ra)<br> [0x80000860]:sw a5, 488(ra)<br>   |
|  44|[0x80003868]<br>0x00000000<br> [0x80003870]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ab and fm1 == 0x155b835100000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000880]:fsqrt.d t5, t3, dyn<br> [0x80000884]:csrrs a5, fcsr, zero<br> [0x80000888]:sw t5, 496(ra)<br> [0x8000088c]:sw a5, 504(ra)<br>   |
|  45|[0x80003878]<br>0x00000000<br> [0x80003880]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f9 and fm1 == 0x0aef451100000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800008ac]:fsqrt.d t5, t3, dyn<br> [0x800008b0]:csrrs a5, fcsr, zero<br> [0x800008b4]:sw t5, 512(ra)<br> [0x800008b8]:sw a5, 520(ra)<br>   |
|  46|[0x80003888]<br>0x00000000<br> [0x80003890]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7c1 and fm1 == 0x56c3dcb100000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800008d8]:fsqrt.d t5, t3, dyn<br> [0x800008dc]:csrrs a5, fcsr, zero<br> [0x800008e0]:sw t5, 528(ra)<br> [0x800008e4]:sw a5, 536(ra)<br>   |
|  47|[0x80003898]<br>0x00000000<br> [0x800038a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x712 and fm1 == 0x28efb9fd20000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000904]:fsqrt.d t5, t3, dyn<br> [0x80000908]:csrrs a5, fcsr, zero<br> [0x8000090c]:sw t5, 544(ra)<br> [0x80000910]:sw a5, 552(ra)<br>   |
|  48|[0x800038a8]<br>0x00000000<br> [0x800038b0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x56f and fm1 == 0xeb77b14440000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000930]:fsqrt.d t5, t3, dyn<br> [0x80000934]:csrrs a5, fcsr, zero<br> [0x80000938]:sw t5, 560(ra)<br> [0x8000093c]:sw a5, 568(ra)<br>   |
|  49|[0x800038b8]<br>0x00000000<br> [0x800038c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x181 and fm1 == 0x0226265640000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x8000095c]:fsqrt.d t5, t3, dyn<br> [0x80000960]:csrrs a5, fcsr, zero<br> [0x80000964]:sw t5, 576(ra)<br> [0x80000968]:sw a5, 584(ra)<br>   |
|  50|[0x800038c8]<br>0x00000000<br> [0x800038d0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x60c and fm1 == 0x4df3876008000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000988]:fsqrt.d t5, t3, dyn<br> [0x8000098c]:csrrs a5, fcsr, zero<br> [0x80000990]:sw t5, 592(ra)<br> [0x80000994]:sw a5, 600(ra)<br>   |
|  51|[0x800038d8]<br>0x00000000<br> [0x800038e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3d1 and fm1 == 0xc9c3e06610000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800009b4]:fsqrt.d t5, t3, dyn<br> [0x800009b8]:csrrs a5, fcsr, zero<br> [0x800009bc]:sw t5, 608(ra)<br> [0x800009c0]:sw a5, 616(ra)<br>   |
|  52|[0x800038e8]<br>0x00000000<br> [0x800038f0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x32d and fm1 == 0xb63d043d10000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x800009e0]:fsqrt.d t5, t3, dyn<br> [0x800009e4]:csrrs a5, fcsr, zero<br> [0x800009e8]:sw t5, 624(ra)<br> [0x800009ec]:sw a5, 632(ra)<br>   |
|  53|[0x800038f8]<br>0x00000000<br> [0x80003900]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x5b0 and fm1 == 0x8f302c02c8000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000a0c]:fsqrt.d t5, t3, dyn<br> [0x80000a10]:csrrs a5, fcsr, zero<br> [0x80000a14]:sw t5, 640(ra)<br> [0x80000a18]:sw a5, 648(ra)<br>   |
|  54|[0x80003908]<br>0x00000000<br> [0x80003910]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x26e and fm1 == 0x8a8a8502e2000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000a38]:fsqrt.d t5, t3, dyn<br> [0x80000a3c]:csrrs a5, fcsr, zero<br> [0x80000a40]:sw t5, 656(ra)<br> [0x80000a44]:sw a5, 664(ra)<br>   |
|  55|[0x80003918]<br>0x00000000<br> [0x80003920]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x74a and fm1 == 0x5095cd3c62000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000a64]:fsqrt.d t5, t3, dyn<br> [0x80000a68]:csrrs a5, fcsr, zero<br> [0x80000a6c]:sw t5, 672(ra)<br> [0x80000a70]:sw a5, 680(ra)<br>   |
|  56|[0x80003928]<br>0x00000000<br> [0x80003930]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x693 and fm1 == 0x32159f7764000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000a90]:fsqrt.d t5, t3, dyn<br> [0x80000a94]:csrrs a5, fcsr, zero<br> [0x80000a98]:sw t5, 688(ra)<br> [0x80000a9c]:sw a5, 696(ra)<br>   |
|  57|[0x80003938]<br>0x00000000<br> [0x80003940]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x32b and fm1 == 0x0c8ac416c9000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000abc]:fsqrt.d t5, t3, dyn<br> [0x80000ac0]:csrrs a5, fcsr, zero<br> [0x80000ac4]:sw t5, 704(ra)<br> [0x80000ac8]:sw a5, 712(ra)<br>   |
|  58|[0x80003948]<br>0x00000000<br> [0x80003950]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x55c and fm1 == 0x27109d2e38800 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000aec]:fsqrt.d t5, t3, dyn<br> [0x80000af0]:csrrs a5, fcsr, zero<br> [0x80000af4]:sw t5, 720(ra)<br> [0x80000af8]:sw a5, 728(ra)<br>   |
|  59|[0x80003958]<br>0x00000000<br> [0x80003960]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3cb and fm1 == 0xf0b8ab6b51000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000b18]:fsqrt.d t5, t3, dyn<br> [0x80000b1c]:csrrs a5, fcsr, zero<br> [0x80000b20]:sw t5, 736(ra)<br> [0x80000b24]:sw a5, 744(ra)<br>   |
|  60|[0x80003968]<br>0x80000000<br> [0x80003970]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x176 and fm1 == 0xeb971282f8200 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000b48]:fsqrt.d t5, t3, dyn<br> [0x80000b4c]:csrrs a5, fcsr, zero<br> [0x80000b50]:sw t5, 752(ra)<br> [0x80000b54]:sw a5, 760(ra)<br>   |
|  61|[0x80003978]<br>0x80000000<br> [0x80003980]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x5b3 and fm1 == 0x979ca2ec8c400 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000b78]:fsqrt.d t5, t3, dyn<br> [0x80000b7c]:csrrs a5, fcsr, zero<br> [0x80000b80]:sw t5, 768(ra)<br> [0x80000b84]:sw a5, 776(ra)<br>   |
|  62|[0x80003988]<br>0x80000000<br> [0x80003990]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x674 and fm1 == 0x0202a3cf79200 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000ba8]:fsqrt.d t5, t3, dyn<br> [0x80000bac]:csrrs a5, fcsr, zero<br> [0x80000bb0]:sw t5, 784(ra)<br> [0x80000bb4]:sw a5, 792(ra)<br>   |
|  63|[0x80003998]<br>0x80000000<br> [0x800039a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x4ee and fm1 == 0x71b0e933c2200 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000bd8]:fsqrt.d t5, t3, dyn<br> [0x80000bdc]:csrrs a5, fcsr, zero<br> [0x80000be0]:sw t5, 800(ra)<br> [0x80000be4]:sw a5, 808(ra)<br>   |
|  64|[0x800039a8]<br>0xC0000000<br> [0x800039b0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x6bb and fm1 == 0x4d07b1ed41100 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000c08]:fsqrt.d t5, t3, dyn<br> [0x80000c0c]:csrrs a5, fcsr, zero<br> [0x80000c10]:sw t5, 816(ra)<br> [0x80000c14]:sw a5, 824(ra)<br>   |
|  65|[0x800039b8]<br>0xC0000000<br> [0x800039c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x62a and fm1 == 0x0e613a46ac880 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000c38]:fsqrt.d t5, t3, dyn<br> [0x80000c3c]:csrrs a5, fcsr, zero<br> [0x80000c40]:sw t5, 832(ra)<br> [0x80000c44]:sw a5, 840(ra)<br>   |
|  66|[0x800039c8]<br>0x60000272<br> [0x800039d0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000fe99b3b666 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000c64]:fsqrt.d t5, t3, dyn<br> [0x80000c68]:csrrs a5, fcsr, zero<br> [0x80000c6c]:sw t5, 848(ra)<br> [0x80000c70]:sw a5, 856(ra)<br>   |
|  67|[0x800039d8]<br>0x60000000<br> [0x800039e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x31e and fm1 == 0x77fad24880120 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000c94]:fsqrt.d t5, t3, dyn<br> [0x80000c98]:csrrs a5, fcsr, zero<br> [0x80000c9c]:sw t5, 864(ra)<br> [0x80000ca0]:sw a5, 872(ra)<br>   |
|  68|[0x800039e8]<br>0x60000000<br> [0x800039f0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x5fd and fm1 == 0x822bf1e14a240 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000cc4]:fsqrt.d t5, t3, dyn<br> [0x80000cc8]:csrrs a5, fcsr, zero<br> [0x80000ccc]:sw t5, 880(ra)<br> [0x80000cd0]:sw a5, 888(ra)<br>   |
|  69|[0x800039f8]<br>0x50000000<br> [0x80003a00]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x5fc and fm1 == 0x9ed0caa415ec8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000cf4]:fsqrt.d t5, t3, dyn<br> [0x80000cf8]:csrrs a5, fcsr, zero<br> [0x80000cfc]:sw t5, 896(ra)<br> [0x80000d00]:sw a5, 904(ra)<br>   |
|  70|[0x80003a08]<br>0x10000000<br> [0x80003a10]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01b and fm1 == 0xd960e82d4b810 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000d24]:fsqrt.d t5, t3, dyn<br> [0x80000d28]:csrrs a5, fcsr, zero<br> [0x80000d2c]:sw t5, 912(ra)<br> [0x80000d30]:sw a5, 920(ra)<br>   |
|  71|[0x80003a18]<br>0x70000000<br> [0x80003a20]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x076 and fm1 == 0x033274a480488 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000d54]:fsqrt.d t5, t3, dyn<br> [0x80000d58]:csrrs a5, fcsr, zero<br> [0x80000d5c]:sw t5, 928(ra)<br> [0x80000d60]:sw a5, 936(ra)<br>   |
|  72|[0x80003a28]<br>0xD0000000<br> [0x80003a30]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x227 and fm1 == 0x127f90c3b9090 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000d84]:fsqrt.d t5, t3, dyn<br> [0x80000d88]:csrrs a5, fcsr, zero<br> [0x80000d8c]:sw t5, 944(ra)<br> [0x80000d90]:sw a5, 952(ra)<br>   |
|  73|[0x80003a38]<br>0xA4000000<br> [0x80003a40]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x6b8 and fm1 == 0x28048e71f9d08 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000db4]:fsqrt.d t5, t3, dyn<br> [0x80000db8]:csrrs a5, fcsr, zero<br> [0x80000dbc]:sw t5, 960(ra)<br> [0x80000dc0]:sw a5, 968(ra)<br>   |
|  74|[0x80003a48]<br>0x54000000<br> [0x80003a50]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x0f6 and fm1 == 0x0e8dcc21fc0dc and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000de4]:fsqrt.d t5, t3, dyn<br> [0x80000de8]:csrrs a5, fcsr, zero<br> [0x80000dec]:sw t5, 976(ra)<br> [0x80000df0]:sw a5, 984(ra)<br>   |
|  75|[0x80003a58]<br>0xD4000000<br> [0x80003a60]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x216 and fm1 == 0x5901f1856027c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000e14]:fsqrt.d t5, t3, dyn<br> [0x80000e18]:csrrs a5, fcsr, zero<br> [0x80000e1c]:sw t5, 992(ra)<br> [0x80000e20]:sw a5, 1000(ra)<br>  |
|  76|[0x80003a68]<br>0xE6000000<br> [0x80003a70]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x2f2 and fm1 == 0xa186bad3f3b95 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                         |[0x80000e44]:fsqrt.d t5, t3, dyn<br> [0x80000e48]:csrrs a5, fcsr, zero<br> [0x80000e4c]:sw t5, 1008(ra)<br> [0x80000e50]:sw a5, 1016(ra)<br> |
