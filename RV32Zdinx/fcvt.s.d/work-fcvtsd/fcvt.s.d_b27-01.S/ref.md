
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000390')]      |
| SIG_REGION                | [('0x80002210', '0x800022a0', '36 words')]      |
| COV_LABELS                | fcvt.s.d_b27      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-fcvtsd/fcvt.s.d_b27-01.S/ref.S    |
| Total Number of coverpoints| 41     |
| Total Coverpoints Hit     | 41      |
| Total Signature Updates   | 24      |
| STAT1                     | 12      |
| STAT2                     | 0      |
| STAT3                     | 3     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x800002a4]:fcvt.s.d a2, a0, dyn
[0x800002a8]:csrrs tp, fcsr, zero
[0x800002ac]:sw a2, 144(ra)
[0x800002b0]:sw tp, 152(ra)
[0x800002b4]:auipc a4, 2
[0x800002b8]:addi a4, a4, 3500
[0x800002bc]:lw a2, 80(a4)
[0x800002c0]:lw a3, 84(a4)
[0x800002c4]:addi a2, zero, 0
[0x800002c8]:addi a3, zero, 0
[0x800002cc]:addi sp, zero, 0
[0x800002d0]:csrrw zero, fcsr, sp
[0x800002d4]:fcvt.s.d a0, a2, dyn
[0x800002d8]:csrrs a5, fcsr, zero
[0x800002dc]:sw a0, 160(ra)
[0x800002e0]:sw a5, 168(ra)
[0x800002e4]:lw t1, 88(a4)
[0x800002e8]:lw t2, 92(a4)
[0x800002ec]:addi t1, zero, 0
[0x800002f0]:addi t2, zero, 0
[0x800002f4]:addi sp, zero, 0
[0x800002f8]:csrrw zero, fcsr, sp
[0x800002fc]:fcvt.s.d fp, t1, dyn
[0x80000300]:csrrs a5, fcsr, zero
[0x80000304]:sw fp, 176(ra)
[0x80000308]:sw a5, 184(ra)
[0x8000030c]:auipc ra, 2
[0x80000310]:addi ra, ra, 3948
[0x80000314]:lw fp, 0(a4)
[0x80000318]:lw s1, 4(a4)
[0x8000031c]:addi fp, zero, 0
[0x80000320]:addi s1, zero, 0
[0x80000324]:addi a0, zero, 0
[0x80000328]:csrrw zero, fcsr, a0
[0x8000032c]:fcvt.s.d t1, fp, dyn
[0x80000330]:csrrs a5, fcsr, zero
[0x80000334]:sw t1, 0(ra)
[0x80000338]:sw a5, 8(ra)
[0x8000033c]:lw sp, 8(a4)
[0x80000340]:lw gp, 12(a4)
[0x80000344]:addi sp, zero, 0
[0x80000348]:addi gp, zero, 0
[0x8000034c]:addi a0, zero, 0
[0x80000350]:csrrw zero, fcsr, a0
[0x80000354]:fcvt.s.d tp, sp, dyn
[0x80000358]:csrrs a5, fcsr, zero
[0x8000035c]:sw tp, 16(ra)
[0x80000360]:sw a5, 24(ra)
[0x80000364]:lw tp, 16(a4)
[0x80000368]:lw t0, 20(a4)
[0x8000036c]:addi tp, zero, 0
[0x80000370]:addi t0, zero, 0
[0x80000374]:addi a0, zero, 0
[0x80000378]:csrrw zero, fcsr, a0
[0x8000037c]:fcvt.s.d sp, tp, dyn
[0x80000380]:csrrs a5, fcsr, zero
[0x80000384]:sw sp, 32(ra)
[0x80000388]:sw a5, 40(ra)
[0x8000038c]:addi zero, zero, 0

[0x800002d4]:fcvt.s.d a0, a2, dyn
[0x800002d8]:csrrs a5, fcsr, zero
[0x800002dc]:sw a0, 160(ra)
[0x800002e0]:sw a5, 168(ra)
[0x800002e4]:lw t1, 88(a4)
[0x800002e8]:lw t2, 92(a4)
[0x800002ec]:addi t1, zero, 0
[0x800002f0]:addi t2, zero, 0
[0x800002f4]:addi sp, zero, 0
[0x800002f8]:csrrw zero, fcsr, sp
[0x800002fc]:fcvt.s.d fp, t1, dyn
[0x80000300]:csrrs a5, fcsr, zero
[0x80000304]:sw fp, 176(ra)
[0x80000308]:sw a5, 184(ra)
[0x8000030c]:auipc ra, 2
[0x80000310]:addi ra, ra, 3948
[0x80000314]:lw fp, 0(a4)
[0x80000318]:lw s1, 4(a4)
[0x8000031c]:addi fp, zero, 0
[0x80000320]:addi s1, zero, 0
[0x80000324]:addi a0, zero, 0
[0x80000328]:csrrw zero, fcsr, a0
[0x8000032c]:fcvt.s.d t1, fp, dyn
[0x80000330]:csrrs a5, fcsr, zero
[0x80000334]:sw t1, 0(ra)
[0x80000338]:sw a5, 8(ra)
[0x8000033c]:lw sp, 8(a4)
[0x80000340]:lw gp, 12(a4)
[0x80000344]:addi sp, zero, 0
[0x80000348]:addi gp, zero, 0
[0x8000034c]:addi a0, zero, 0
[0x80000350]:csrrw zero, fcsr, a0
[0x80000354]:fcvt.s.d tp, sp, dyn
[0x80000358]:csrrs a5, fcsr, zero
[0x8000035c]:sw tp, 16(ra)
[0x80000360]:sw a5, 24(ra)
[0x80000364]:lw tp, 16(a4)
[0x80000368]:lw t0, 20(a4)
[0x8000036c]:addi tp, zero, 0
[0x80000370]:addi t0, zero, 0
[0x80000374]:addi a0, zero, 0
[0x80000378]:csrrw zero, fcsr, a0
[0x8000037c]:fcvt.s.d sp, tp, dyn
[0x80000380]:csrrs a5, fcsr, zero
[0x80000384]:sw sp, 32(ra)
[0x80000388]:sw a5, 40(ra)
[0x8000038c]:addi zero, zero, 0

[0x800002fc]:fcvt.s.d fp, t1, dyn
[0x80000300]:csrrs a5, fcsr, zero
[0x80000304]:sw fp, 176(ra)
[0x80000308]:sw a5, 184(ra)
[0x8000030c]:auipc ra, 2
[0x80000310]:addi ra, ra, 3948
[0x80000314]:lw fp, 0(a4)
[0x80000318]:lw s1, 4(a4)
[0x8000031c]:addi fp, zero, 0
[0x80000320]:addi s1, zero, 0
[0x80000324]:addi a0, zero, 0
[0x80000328]:csrrw zero, fcsr, a0
[0x8000032c]:fcvt.s.d t1, fp, dyn
[0x80000330]:csrrs a5, fcsr, zero
[0x80000334]:sw t1, 0(ra)
[0x80000338]:sw a5, 8(ra)
[0x8000033c]:lw sp, 8(a4)
[0x80000340]:lw gp, 12(a4)
[0x80000344]:addi sp, zero, 0
[0x80000348]:addi gp, zero, 0
[0x8000034c]:addi a0, zero, 0
[0x80000350]:csrrw zero, fcsr, a0
[0x80000354]:fcvt.s.d tp, sp, dyn
[0x80000358]:csrrs a5, fcsr, zero
[0x8000035c]:sw tp, 16(ra)
[0x80000360]:sw a5, 24(ra)
[0x80000364]:lw tp, 16(a4)
[0x80000368]:lw t0, 20(a4)
[0x8000036c]:addi tp, zero, 0
[0x80000370]:addi t0, zero, 0
[0x80000374]:addi a0, zero, 0
[0x80000378]:csrrw zero, fcsr, a0
[0x8000037c]:fcvt.s.d sp, tp, dyn
[0x80000380]:csrrs a5, fcsr, zero
[0x80000384]:sw sp, 32(ra)
[0x80000388]:sw a5, 40(ra)
[0x8000038c]:addi zero, zero, 0



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                          signature                           |                                                                                 coverpoints                                                                                  |                                                                    code                                                                    |
|---:|--------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002218]<br>0x7FC00000<br> [0x80002220]<br>0x00000010<br> |- mnemonic : fcvt.s.d<br> - rs1 : x30<br> - rd : x30<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x8000012c]:fcvt.s.d t5, t5, dyn<br> [0x80000130]:csrrs tp, fcsr, zero<br> [0x80000134]:sw t5, 0(ra)<br> [0x80000138]:sw tp, 8(ra)<br>     |
|   2|[0x80002228]<br>0x7FC00000<br> [0x80002230]<br>0x00000010<br> |- rs1 : x26<br> - rd : x28<br> - rs1 != rd<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                           |[0x80000154]:fcvt.s.d t3, s10, dyn<br> [0x80000158]:csrrs tp, fcsr, zero<br> [0x8000015c]:sw t3, 16(ra)<br> [0x80000160]:sw tp, 24(ra)<br>  |
|   3|[0x80002238]<br>0x7FC00000<br> [0x80002240]<br>0x00000010<br> |- rs1 : x28<br> - rd : x26<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x4aaaaaaaaaaaa and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x80000184]:fcvt.s.d s10, t3, dyn<br> [0x80000188]:csrrs tp, fcsr, zero<br> [0x8000018c]:sw s10, 32(ra)<br> [0x80000190]:sw tp, 40(ra)<br> |
|   4|[0x80002248]<br>0x7FC00000<br> [0x80002250]<br>0x00000010<br> |- rs1 : x22<br> - rd : x24<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0x4aaaaaaaaaaaa and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x800001b4]:fcvt.s.d s8, s6, dyn<br> [0x800001b8]:csrrs tp, fcsr, zero<br> [0x800001bc]:sw s8, 48(ra)<br> [0x800001c0]:sw tp, 56(ra)<br>   |
|   5|[0x80002258]<br>0x7FC00000<br> [0x80002260]<br>0x00000000<br> |- rs1 : x24<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x800001dc]:fcvt.s.d s6, s8, dyn<br> [0x800001e0]:csrrs tp, fcsr, zero<br> [0x800001e4]:sw s6, 64(ra)<br> [0x800001e8]:sw tp, 72(ra)<br>   |
|   6|[0x80002268]<br>0x7FC00000<br> [0x80002270]<br>0x00000000<br> |- rs1 : x18<br> - rd : x20<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x80000204]:fcvt.s.d s4, s2, dyn<br> [0x80000208]:csrrs tp, fcsr, zero<br> [0x8000020c]:sw s4, 80(ra)<br> [0x80000210]:sw tp, 88(ra)<br>   |
|   7|[0x80002278]<br>0x7FC00000<br> [0x80002280]<br>0x00000000<br> |- rs1 : x20<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0xc000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x8000022c]:fcvt.s.d s2, s4, dyn<br> [0x80000230]:csrrs tp, fcsr, zero<br> [0x80000234]:sw s2, 96(ra)<br> [0x80000238]:sw tp, 104(ra)<br>  |
|   8|[0x80002288]<br>0x7FC00000<br> [0x80002290]<br>0x00000000<br> |- rs1 : x14<br> - rd : x16<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0xc000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x80000254]:fcvt.s.d a6, a4, dyn<br> [0x80000258]:csrrs tp, fcsr, zero<br> [0x8000025c]:sw a6, 112(ra)<br> [0x80000260]:sw tp, 120(ra)<br> |
|   9|[0x80002298]<br>0x00000000<br> [0x800022a0]<br>0x00000000<br> |- rs1 : x16<br> - rd : x14<br>                                                                                                                                                |[0x8000027c]:fcvt.s.d a4, a6, dyn<br> [0x80000280]:csrrs tp, fcsr, zero<br> [0x80000284]:sw a4, 128(ra)<br> [0x80000288]:sw tp, 136(ra)<br> |
|  10|[0x80002278]<br>0x00000000<br> [0x80002280]<br>0x00000000<br> |- rs1 : x8<br> - rd : x6<br>                                                                                                                                                  |[0x8000032c]:fcvt.s.d t1, fp, dyn<br> [0x80000330]:csrrs a5, fcsr, zero<br> [0x80000334]:sw t1, 0(ra)<br> [0x80000338]:sw a5, 8(ra)<br>     |
|  11|[0x80002288]<br>0x00000000<br> [0x80002290]<br>0x00000000<br> |- rs1 : x2<br> - rd : x4<br>                                                                                                                                                  |[0x80000354]:fcvt.s.d tp, sp, dyn<br> [0x80000358]:csrrs a5, fcsr, zero<br> [0x8000035c]:sw tp, 16(ra)<br> [0x80000360]:sw a5, 24(ra)<br>   |
|  12|[0x80002298]<br>0x00000000<br> [0x800022a0]<br>0x00000000<br> |- rs1 : x4<br> - rd : x2<br>                                                                                                                                                  |[0x8000037c]:fcvt.s.d sp, tp, dyn<br> [0x80000380]:csrrs a5, fcsr, zero<br> [0x80000384]:sw sp, 32(ra)<br> [0x80000388]:sw a5, 40(ra)<br>   |
