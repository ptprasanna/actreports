
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800005b0')]      |
| SIG_REGION                | [('0x80002210', '0x80002300', '60 words')]      |
| COV_LABELS                | fcvt.s.d_b28      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-fcvtsd/fcvt.s.d_b28-01.S/ref.S    |
| Total Number of coverpoints| 61     |
| Total Coverpoints Hit     | 61      |
| Total Signature Updates   | 42      |
| STAT1                     | 21      |
| STAT2                     | 0      |
| STAT3                     | 7     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x8000049c]:fcvt.s.d t5, t3, dyn
[0x800004a0]:csrrs a5, fcsr, zero
[0x800004a4]:sw t5, 144(ra)
[0x800004a8]:sw a5, 152(ra)
[0x800004ac]:lw t3, 80(a4)
[0x800004b0]:lw t4, 84(a4)
[0x800004b4]:addi t3, zero, 0
[0x800004b8]:lui t4, 786368
[0x800004bc]:addi a0, zero, 0
[0x800004c0]:csrrw zero, fcsr, a0
[0x800004c4]:fcvt.s.d t5, t3, dyn
[0x800004c8]:csrrs a5, fcsr, zero

[0x800004c4]:fcvt.s.d t5, t3, dyn
[0x800004c8]:csrrs a5, fcsr, zero
[0x800004cc]:sw t5, 160(ra)
[0x800004d0]:sw a5, 168(ra)
[0x800004d4]:lw t3, 88(a4)
[0x800004d8]:lw t4, 92(a4)
[0x800004dc]:addi t3, zero, 0
[0x800004e0]:lui t4, 786304
[0x800004e4]:addi a0, zero, 0
[0x800004e8]:csrrw zero, fcsr, a0
[0x800004ec]:fcvt.s.d t5, t3, dyn
[0x800004f0]:csrrs a5, fcsr, zero

[0x800004ec]:fcvt.s.d t5, t3, dyn
[0x800004f0]:csrrs a5, fcsr, zero
[0x800004f4]:sw t5, 176(ra)
[0x800004f8]:sw a5, 184(ra)
[0x800004fc]:lw t3, 96(a4)
[0x80000500]:lw t4, 100(a4)
[0x80000504]:addi t3, zero, 0
[0x80000508]:lui t4, 786240
[0x8000050c]:addi a0, zero, 0
[0x80000510]:csrrw zero, fcsr, a0
[0x80000514]:fcvt.s.d t5, t3, dyn
[0x80000518]:csrrs a5, fcsr, zero

[0x80000514]:fcvt.s.d t5, t3, dyn
[0x80000518]:csrrs a5, fcsr, zero
[0x8000051c]:sw t5, 192(ra)
[0x80000520]:sw a5, 200(ra)
[0x80000524]:lw t3, 104(a4)
[0x80000528]:lw t4, 108(a4)
[0x8000052c]:lui t3, 713317
[0x80000530]:addi t3, t3, 332
[0x80000534]:lui t4, 802198
[0x80000538]:addi t4, t4, 1956
[0x8000053c]:addi a0, zero, 0
[0x80000540]:csrrw zero, fcsr, a0
[0x80000544]:fcvt.s.d t5, t3, dyn
[0x80000548]:csrrs a5, fcsr, zero

[0x80000544]:fcvt.s.d t5, t3, dyn
[0x80000548]:csrrs a5, fcsr, zero
[0x8000054c]:sw t5, 208(ra)
[0x80000550]:sw a5, 216(ra)
[0x80000554]:lw t3, 112(a4)
[0x80000558]:lw t4, 116(a4)
[0x8000055c]:addi t3, zero, 0
[0x80000560]:lui t4, 802304
[0x80000564]:addi a0, zero, 0
[0x80000568]:csrrw zero, fcsr, a0
[0x8000056c]:fcvt.s.d t5, t3, dyn
[0x80000570]:csrrs a5, fcsr, zero

[0x8000056c]:fcvt.s.d t5, t3, dyn
[0x80000570]:csrrs a5, fcsr, zero
[0x80000574]:sw t5, 224(ra)
[0x80000578]:sw a5, 232(ra)
[0x8000057c]:lw t3, 120(a4)
[0x80000580]:lw t4, 124(a4)
[0x80000584]:addi t3, zero, 0
[0x80000588]:lui t4, 1048320
[0x8000058c]:addi a0, zero, 0
[0x80000590]:csrrw zero, fcsr, a0
[0x80000594]:fcvt.s.d t5, t3, dyn
[0x80000598]:csrrs a5, fcsr, zero

[0x80000594]:fcvt.s.d t5, t3, dyn
[0x80000598]:csrrs a5, fcsr, zero
[0x8000059c]:sw t5, 240(ra)
[0x800005a0]:sw a5, 248(ra)
[0x800005a4]:addi zero, zero, 0
[0x800005a8]:addi zero, zero, 0
[0x800005ac]:addi zero, zero, 0



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                          signature                           |                                                                                 coverpoints                                                                                  |                                                                    code                                                                    |
|---:|--------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002218]<br>0x00000000<br> [0x80002220]<br>0x00000000<br> |- mnemonic : fcvt.s.d<br> - rs1 : x30<br> - rd : x30<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x8000012c]:fcvt.s.d t5, t5, dyn<br> [0x80000130]:csrrs tp, fcsr, zero<br> [0x80000134]:sw t5, 0(ra)<br> [0x80000138]:sw tp, 8(ra)<br>     |
|   2|[0x80002228]<br>0x3F124771<br> [0x80002230]<br>0x00000001<br> |- rs1 : x26<br> - rd : x28<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0x3fe and fm1 == 0x248ee18215dfa and  fcsr == 0x0 and rm_val == 7   #nosat<br>                           |[0x8000015c]:fcvt.s.d t3, s10, dyn<br> [0x80000160]:csrrs tp, fcsr, zero<br> [0x80000164]:sw t3, 16(ra)<br> [0x80000168]:sw tp, 24(ra)<br>  |
|   3|[0x80002238]<br>0x3F800000<br> [0x80002240]<br>0x00000000<br> |- rs1 : x28<br> - rd : x26<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x80000184]:fcvt.s.d s10, t3, dyn<br> [0x80000188]:csrrs tp, fcsr, zero<br> [0x8000018c]:sw s10, 32(ra)<br> [0x80000190]:sw tp, 40(ra)<br> |
|   4|[0x80002248]<br>0x3FA00000<br> [0x80002250]<br>0x00000000<br> |- rs1 : x22<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x4000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x800001ac]:fcvt.s.d s8, s6, dyn<br> [0x800001b0]:csrrs tp, fcsr, zero<br> [0x800001b4]:sw s8, 48(ra)<br> [0x800001b8]:sw tp, 56(ra)<br>   |
|   5|[0x80002258]<br>0x3FC00000<br> [0x80002260]<br>0x00000000<br> |- rs1 : x24<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x800001d4]:fcvt.s.d s6, s8, dyn<br> [0x800001d8]:csrrs tp, fcsr, zero<br> [0x800001dc]:sw s6, 64(ra)<br> [0x800001e0]:sw tp, 72(ra)<br>   |
|   6|[0x80002268]<br>0x3FE00000<br> [0x80002270]<br>0x00000000<br> |- rs1 : x18<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0xc000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x800001fc]:fcvt.s.d s4, s2, dyn<br> [0x80000200]:csrrs tp, fcsr, zero<br> [0x80000204]:sw s4, 80(ra)<br> [0x80000208]:sw tp, 88(ra)<br>   |
|   7|[0x80002278]<br>0x40000000<br> [0x80002280]<br>0x00000000<br> |- rs1 : x20<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x400 and fm1 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x80000224]:fcvt.s.d s2, s4, dyn<br> [0x80000228]:csrrs tp, fcsr, zero<br> [0x8000022c]:sw s2, 96(ra)<br> [0x80000230]:sw tp, 104(ra)<br>  |
|   8|[0x80002288]<br>0x40100000<br> [0x80002290]<br>0x00000000<br> |- rs1 : x14<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x400 and fm1 == 0x2000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x8000024c]:fcvt.s.d a6, a4, dyn<br> [0x80000250]:csrrs tp, fcsr, zero<br> [0x80000254]:sw a6, 112(ra)<br> [0x80000258]:sw tp, 120(ra)<br> |
|   9|[0x80002298]<br>0x40200000<br> [0x800022a0]<br>0x00000000<br> |- rs1 : x16<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x400 and fm1 == 0x4000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x80000274]:fcvt.s.d a4, a6, dyn<br> [0x80000278]:csrrs tp, fcsr, zero<br> [0x8000027c]:sw a4, 128(ra)<br> [0x80000280]:sw tp, 136(ra)<br> |
|  10|[0x800022a8]<br>0x40300000<br> [0x800022b0]<br>0x00000000<br> |- rs1 : x10<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x400 and fm1 == 0x6000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x8000029c]:fcvt.s.d a2, a0, dyn<br> [0x800002a0]:csrrs tp, fcsr, zero<br> [0x800002a4]:sw a2, 144(ra)<br> [0x800002a8]:sw tp, 152(ra)<br> |
|  11|[0x800022b8]<br>0x5E5B9759<br> [0x800022c0]<br>0x00000001<br> |- rs1 : x12<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x43c and fm1 == 0xb72eb13dc494a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x800002d4]:fcvt.s.d a0, a2, dyn<br> [0x800002d8]:csrrs a5, fcsr, zero<br> [0x800002dc]:sw a0, 160(ra)<br> [0x800002e0]:sw a5, 168(ra)<br> |
|  12|[0x800022c8]<br>0x5F000000<br> [0x800022d0]<br>0x00000000<br> |- rs1 : x6<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                             |[0x800002fc]:fcvt.s.d fp, t1, dyn<br> [0x80000300]:csrrs a5, fcsr, zero<br> [0x80000304]:sw fp, 176(ra)<br> [0x80000308]:sw a5, 184(ra)<br> |
|  13|[0x80002278]<br>0x7F800000<br> [0x80002280]<br>0x00000000<br> |- rs1 : x8<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                             |[0x8000032c]:fcvt.s.d t1, fp, dyn<br> [0x80000330]:csrrs a5, fcsr, zero<br> [0x80000334]:sw t1, 0(ra)<br> [0x80000338]:sw a5, 8(ra)<br>     |
|  14|[0x80002288]<br>0x7FC00000<br> [0x80002290]<br>0x00000010<br> |- rs1 : x2<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                             |[0x80000354]:fcvt.s.d tp, sp, dyn<br> [0x80000358]:csrrs a5, fcsr, zero<br> [0x8000035c]:sw tp, 16(ra)<br> [0x80000360]:sw a5, 24(ra)<br>   |
|  15|[0x80002298]<br>0x7FC00000<br> [0x800022a0]<br>0x00000000<br> |- rs1 : x4<br> - rd : x2<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                             |[0x8000037c]:fcvt.s.d sp, tp, dyn<br> [0x80000380]:csrrs a5, fcsr, zero<br> [0x80000384]:sw sp, 32(ra)<br> [0x80000388]:sw a5, 40(ra)<br>   |
|  16|[0x800022a8]<br>0x80000000<br> [0x800022b0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                          |[0x800003a4]:fcvt.s.d t5, t3, dyn<br> [0x800003a8]:csrrs a5, fcsr, zero<br> [0x800003ac]:sw t5, 48(ra)<br> [0x800003b0]:sw a5, 56(ra)<br>   |
|  17|[0x800022b8]<br>0xBED8046B<br> [0x800022c0]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x3fd and fm1 == 0xb008d57e19f88 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                          |[0x800003d4]:fcvt.s.d t5, t3, dyn<br> [0x800003d8]:csrrs a5, fcsr, zero<br> [0x800003dc]:sw t5, 64(ra)<br> [0x800003e0]:sw a5, 72(ra)<br>   |
|  18|[0x800022c8]<br>0xBC000000<br> [0x800022d0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                          |[0x800003fc]:fcvt.s.d t5, t3, dyn<br> [0x80000400]:csrrs a5, fcsr, zero<br> [0x80000404]:sw t5, 80(ra)<br> [0x80000408]:sw a5, 88(ra)<br>   |
|  19|[0x800022d8]<br>0xC0300000<br> [0x800022e0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x400 and fm1 == 0x6000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                          |[0x80000424]:fcvt.s.d t5, t3, dyn<br> [0x80000428]:csrrs a5, fcsr, zero<br> [0x8000042c]:sw t5, 96(ra)<br> [0x80000430]:sw a5, 104(ra)<br>  |
|  20|[0x800022e8]<br>0xC0200000<br> [0x800022f0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x400 and fm1 == 0x4000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                          |[0x8000044c]:fcvt.s.d t5, t3, dyn<br> [0x80000450]:csrrs a5, fcsr, zero<br> [0x80000454]:sw t5, 112(ra)<br> [0x80000458]:sw a5, 120(ra)<br> |
|  21|[0x800022f8]<br>0xC0100000<br> [0x80002300]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x400 and fm1 == 0x2000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                          |[0x80000474]:fcvt.s.d t5, t3, dyn<br> [0x80000478]:csrrs a5, fcsr, zero<br> [0x8000047c]:sw t5, 128(ra)<br> [0x80000480]:sw a5, 136(ra)<br> |
