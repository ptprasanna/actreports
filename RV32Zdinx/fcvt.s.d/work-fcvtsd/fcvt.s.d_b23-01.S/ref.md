
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000880')]      |
| SIG_REGION                | [('0x80002310', '0x80002490', '96 words')]      |
| COV_LABELS                | fcvt.s.d_b23      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-fcvtsd/fcvt.s.d_b23-01.S/ref.S    |
| Total Number of coverpoints| 78     |
| Total Coverpoints Hit     | 78      |
| Total Signature Updates   | 60      |
| STAT1                     | 30      |
| STAT2                     | 0      |
| STAT3                     | 15     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x8000063c]:fcvt.s.d t5, t3, dyn
[0x80000640]:csrrs a5, fcsr, zero
[0x80000644]:sw t5, 288(ra)
[0x80000648]:sw a5, 296(ra)
[0x8000064c]:lw t3, 152(a4)
[0x80000650]:lw t4, 156(a4)
[0x80000654]:addi t3, zero, 2
[0x80000658]:lui t4, 278016
[0x8000065c]:addi a0, zero, 32
[0x80000660]:csrrw zero, fcsr, a0
[0x80000664]:fcvt.s.d t5, t3, dyn
[0x80000668]:csrrs a5, fcsr, zero

[0x80000664]:fcvt.s.d t5, t3, dyn
[0x80000668]:csrrs a5, fcsr, zero
[0x8000066c]:sw t5, 304(ra)
[0x80000670]:sw a5, 312(ra)
[0x80000674]:lw t3, 160(a4)
[0x80000678]:lw t4, 164(a4)
[0x8000067c]:addi t3, zero, 2
[0x80000680]:lui t4, 278016
[0x80000684]:addi a0, zero, 64
[0x80000688]:csrrw zero, fcsr, a0
[0x8000068c]:fcvt.s.d t5, t3, dyn
[0x80000690]:csrrs a5, fcsr, zero

[0x8000068c]:fcvt.s.d t5, t3, dyn
[0x80000690]:csrrs a5, fcsr, zero
[0x80000694]:sw t5, 320(ra)
[0x80000698]:sw a5, 328(ra)
[0x8000069c]:lw t3, 168(a4)
[0x800006a0]:lw t4, 172(a4)
[0x800006a4]:addi t3, zero, 2
[0x800006a8]:lui t4, 278016
[0x800006ac]:addi a0, zero, 96
[0x800006b0]:csrrw zero, fcsr, a0
[0x800006b4]:fcvt.s.d t5, t3, dyn
[0x800006b8]:csrrs a5, fcsr, zero

[0x800006b4]:fcvt.s.d t5, t3, dyn
[0x800006b8]:csrrs a5, fcsr, zero
[0x800006bc]:sw t5, 336(ra)
[0x800006c0]:sw a5, 344(ra)
[0x800006c4]:lw t3, 176(a4)
[0x800006c8]:lw t4, 180(a4)
[0x800006cc]:addi t3, zero, 2
[0x800006d0]:lui t4, 278016
[0x800006d4]:addi a0, zero, 128
[0x800006d8]:csrrw zero, fcsr, a0
[0x800006dc]:fcvt.s.d t5, t3, dyn
[0x800006e0]:csrrs a5, fcsr, zero

[0x800006dc]:fcvt.s.d t5, t3, dyn
[0x800006e0]:csrrs a5, fcsr, zero
[0x800006e4]:sw t5, 352(ra)
[0x800006e8]:sw a5, 360(ra)
[0x800006ec]:lw t3, 184(a4)
[0x800006f0]:lw t4, 188(a4)
[0x800006f4]:addi t3, zero, 3
[0x800006f8]:lui t4, 278016
[0x800006fc]:addi a0, zero, 0
[0x80000700]:csrrw zero, fcsr, a0
[0x80000704]:fcvt.s.d t5, t3, dyn
[0x80000708]:csrrs a5, fcsr, zero

[0x80000704]:fcvt.s.d t5, t3, dyn
[0x80000708]:csrrs a5, fcsr, zero
[0x8000070c]:sw t5, 368(ra)
[0x80000710]:sw a5, 376(ra)
[0x80000714]:lw t3, 192(a4)
[0x80000718]:lw t4, 196(a4)
[0x8000071c]:addi t3, zero, 3
[0x80000720]:lui t4, 278016
[0x80000724]:addi a0, zero, 32
[0x80000728]:csrrw zero, fcsr, a0
[0x8000072c]:fcvt.s.d t5, t3, dyn
[0x80000730]:csrrs a5, fcsr, zero

[0x8000072c]:fcvt.s.d t5, t3, dyn
[0x80000730]:csrrs a5, fcsr, zero
[0x80000734]:sw t5, 384(ra)
[0x80000738]:sw a5, 392(ra)
[0x8000073c]:lw t3, 200(a4)
[0x80000740]:lw t4, 204(a4)
[0x80000744]:addi t3, zero, 3
[0x80000748]:lui t4, 278016
[0x8000074c]:addi a0, zero, 64
[0x80000750]:csrrw zero, fcsr, a0
[0x80000754]:fcvt.s.d t5, t3, dyn
[0x80000758]:csrrs a5, fcsr, zero

[0x80000754]:fcvt.s.d t5, t3, dyn
[0x80000758]:csrrs a5, fcsr, zero
[0x8000075c]:sw t5, 400(ra)
[0x80000760]:sw a5, 408(ra)
[0x80000764]:lw t3, 208(a4)
[0x80000768]:lw t4, 212(a4)
[0x8000076c]:addi t3, zero, 3
[0x80000770]:lui t4, 278016
[0x80000774]:addi a0, zero, 96
[0x80000778]:csrrw zero, fcsr, a0
[0x8000077c]:fcvt.s.d t5, t3, dyn
[0x80000780]:csrrs a5, fcsr, zero

[0x8000077c]:fcvt.s.d t5, t3, dyn
[0x80000780]:csrrs a5, fcsr, zero
[0x80000784]:sw t5, 416(ra)
[0x80000788]:sw a5, 424(ra)
[0x8000078c]:lw t3, 216(a4)
[0x80000790]:lw t4, 220(a4)
[0x80000794]:addi t3, zero, 3
[0x80000798]:lui t4, 278016
[0x8000079c]:addi a0, zero, 128
[0x800007a0]:csrrw zero, fcsr, a0
[0x800007a4]:fcvt.s.d t5, t3, dyn
[0x800007a8]:csrrs a5, fcsr, zero

[0x800007a4]:fcvt.s.d t5, t3, dyn
[0x800007a8]:csrrs a5, fcsr, zero
[0x800007ac]:sw t5, 432(ra)
[0x800007b0]:sw a5, 440(ra)
[0x800007b4]:lw t3, 224(a4)
[0x800007b8]:lw t4, 228(a4)
[0x800007bc]:addi t3, zero, 4
[0x800007c0]:lui t4, 278016
[0x800007c4]:addi a0, zero, 0
[0x800007c8]:csrrw zero, fcsr, a0
[0x800007cc]:fcvt.s.d t5, t3, dyn
[0x800007d0]:csrrs a5, fcsr, zero

[0x800007cc]:fcvt.s.d t5, t3, dyn
[0x800007d0]:csrrs a5, fcsr, zero
[0x800007d4]:sw t5, 448(ra)
[0x800007d8]:sw a5, 456(ra)
[0x800007dc]:lw t3, 232(a4)
[0x800007e0]:lw t4, 236(a4)
[0x800007e4]:addi t3, zero, 4
[0x800007e8]:lui t4, 278016
[0x800007ec]:addi a0, zero, 32
[0x800007f0]:csrrw zero, fcsr, a0
[0x800007f4]:fcvt.s.d t5, t3, dyn
[0x800007f8]:csrrs a5, fcsr, zero

[0x800007f4]:fcvt.s.d t5, t3, dyn
[0x800007f8]:csrrs a5, fcsr, zero
[0x800007fc]:sw t5, 464(ra)
[0x80000800]:sw a5, 472(ra)
[0x80000804]:lw t3, 240(a4)
[0x80000808]:lw t4, 244(a4)
[0x8000080c]:addi t3, zero, 4
[0x80000810]:lui t4, 278016
[0x80000814]:addi a0, zero, 64
[0x80000818]:csrrw zero, fcsr, a0
[0x8000081c]:fcvt.s.d t5, t3, dyn
[0x80000820]:csrrs a5, fcsr, zero

[0x8000081c]:fcvt.s.d t5, t3, dyn
[0x80000820]:csrrs a5, fcsr, zero
[0x80000824]:sw t5, 480(ra)
[0x80000828]:sw a5, 488(ra)
[0x8000082c]:lw t3, 248(a4)
[0x80000830]:lw t4, 252(a4)
[0x80000834]:addi t3, zero, 4
[0x80000838]:lui t4, 278016
[0x8000083c]:addi a0, zero, 96
[0x80000840]:csrrw zero, fcsr, a0
[0x80000844]:fcvt.s.d t5, t3, dyn
[0x80000848]:csrrs a5, fcsr, zero

[0x80000844]:fcvt.s.d t5, t3, dyn
[0x80000848]:csrrs a5, fcsr, zero
[0x8000084c]:sw t5, 496(ra)
[0x80000850]:sw a5, 504(ra)
[0x80000854]:lw t3, 256(a4)
[0x80000858]:lw t4, 260(a4)
[0x8000085c]:addi t3, zero, 4
[0x80000860]:lui t4, 278016
[0x80000864]:addi a0, zero, 128
[0x80000868]:csrrw zero, fcsr, a0
[0x8000086c]:fcvt.s.d t5, t3, dyn
[0x80000870]:csrrs a5, fcsr, zero

[0x8000086c]:fcvt.s.d t5, t3, dyn
[0x80000870]:csrrs a5, fcsr, zero
[0x80000874]:sw t5, 512(ra)
[0x80000878]:sw a5, 520(ra)
[0x8000087c]:addi zero, zero, 0



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                          signature                           |                                                                                 coverpoints                                                                                  |                                                                    code                                                                    |
|---:|--------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002318]<br>0x5F000000<br> [0x80002320]<br>0x00000001<br> |- mnemonic : fcvt.s.d<br> - rs1 : x30<br> - rd : x30<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x80000130]:fcvt.s.d t5, t5, dyn<br> [0x80000134]:csrrs tp, fcsr, zero<br> [0x80000138]:sw t5, 0(ra)<br> [0x8000013c]:sw tp, 8(ra)<br>     |
|   2|[0x80002328]<br>0x5EFFFFFF<br> [0x80002330]<br>0x00000021<br> |- rs1 : x26<br> - rd : x28<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and  fcsr == 0x20 and rm_val == 7   #nosat<br>                          |[0x8000015c]:fcvt.s.d t3, s10, dyn<br> [0x80000160]:csrrs tp, fcsr, zero<br> [0x80000164]:sw t3, 16(ra)<br> [0x80000168]:sw tp, 24(ra)<br>  |
|   3|[0x80002338]<br>0x5EFFFFFF<br> [0x80002340]<br>0x00000041<br> |- rs1 : x28<br> - rd : x26<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                          |[0x80000188]:fcvt.s.d s10, t3, dyn<br> [0x8000018c]:csrrs tp, fcsr, zero<br> [0x80000190]:sw s10, 32(ra)<br> [0x80000194]:sw tp, 40(ra)<br> |
|   4|[0x80002348]<br>0x5F000000<br> [0x80002350]<br>0x00000061<br> |- rs1 : x22<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                          |[0x800001b4]:fcvt.s.d s8, s6, dyn<br> [0x800001b8]:csrrs tp, fcsr, zero<br> [0x800001bc]:sw s8, 48(ra)<br> [0x800001c0]:sw tp, 56(ra)<br>   |
|   5|[0x80002358]<br>0x5F000000<br> [0x80002360]<br>0x00000081<br> |- rs1 : x24<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                          |[0x800001e0]:fcvt.s.d s6, s8, dyn<br> [0x800001e4]:csrrs tp, fcsr, zero<br> [0x800001e8]:sw s6, 64(ra)<br> [0x800001ec]:sw tp, 72(ra)<br>   |
|   6|[0x80002368]<br>0x5F000000<br> [0x80002370]<br>0x00000001<br> |- rs1 : x18<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffd and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x8000020c]:fcvt.s.d s4, s2, dyn<br> [0x80000210]:csrrs tp, fcsr, zero<br> [0x80000214]:sw s4, 80(ra)<br> [0x80000218]:sw tp, 88(ra)<br>   |
|   7|[0x80002378]<br>0x5EFFFFFF<br> [0x80002380]<br>0x00000021<br> |- rs1 : x20<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffd and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                          |[0x80000238]:fcvt.s.d s2, s4, dyn<br> [0x8000023c]:csrrs tp, fcsr, zero<br> [0x80000240]:sw s2, 96(ra)<br> [0x80000244]:sw tp, 104(ra)<br>  |
|   8|[0x80002388]<br>0x5EFFFFFF<br> [0x80002390]<br>0x00000041<br> |- rs1 : x14<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffd and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                          |[0x80000264]:fcvt.s.d a6, a4, dyn<br> [0x80000268]:csrrs tp, fcsr, zero<br> [0x8000026c]:sw a6, 112(ra)<br> [0x80000270]:sw tp, 120(ra)<br> |
|   9|[0x80002398]<br>0x5F000000<br> [0x800023a0]<br>0x00000061<br> |- rs1 : x16<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffd and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                          |[0x80000290]:fcvt.s.d a4, a6, dyn<br> [0x80000294]:csrrs tp, fcsr, zero<br> [0x80000298]:sw a4, 128(ra)<br> [0x8000029c]:sw tp, 136(ra)<br> |
|  10|[0x800023a8]<br>0x5F000000<br> [0x800023b0]<br>0x00000081<br> |- rs1 : x10<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffd and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                          |[0x800002bc]:fcvt.s.d a2, a0, dyn<br> [0x800002c0]:csrrs tp, fcsr, zero<br> [0x800002c4]:sw a2, 144(ra)<br> [0x800002c8]:sw tp, 152(ra)<br> |
|  11|[0x800023b8]<br>0x5F000000<br> [0x800023c0]<br>0x00000001<br> |- rs1 : x12<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffe and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x800002f0]:fcvt.s.d a0, a2, dyn<br> [0x800002f4]:csrrs a5, fcsr, zero<br> [0x800002f8]:sw a0, 160(ra)<br> [0x800002fc]:sw a5, 168(ra)<br> |
|  12|[0x800023c8]<br>0x5EFFFFFF<br> [0x800023d0]<br>0x00000021<br> |- rs1 : x6<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffe and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                            |[0x8000031c]:fcvt.s.d fp, t1, dyn<br> [0x80000320]:csrrs a5, fcsr, zero<br> [0x80000324]:sw fp, 176(ra)<br> [0x80000328]:sw a5, 184(ra)<br> |
|  13|[0x80002378]<br>0x5EFFFFFF<br> [0x80002380]<br>0x00000041<br> |- rs1 : x8<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffe and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                            |[0x80000350]:fcvt.s.d t1, fp, dyn<br> [0x80000354]:csrrs a5, fcsr, zero<br> [0x80000358]:sw t1, 0(ra)<br> [0x8000035c]:sw a5, 8(ra)<br>     |
|  14|[0x80002388]<br>0x5F000000<br> [0x80002390]<br>0x00000061<br> |- rs1 : x2<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffe and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                            |[0x8000037c]:fcvt.s.d tp, sp, dyn<br> [0x80000380]:csrrs a5, fcsr, zero<br> [0x80000384]:sw tp, 16(ra)<br> [0x80000388]:sw a5, 24(ra)<br>   |
|  15|[0x80002398]<br>0x5F000000<br> [0x800023a0]<br>0x00000081<br> |- rs1 : x4<br> - rd : x2<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffe and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                            |[0x800003a8]:fcvt.s.d sp, tp, dyn<br> [0x800003ac]:csrrs a5, fcsr, zero<br> [0x800003b0]:sw sp, 32(ra)<br> [0x800003b4]:sw a5, 40(ra)<br>   |
|  16|[0x800023a8]<br>0x5F000000<br> [0x800023b0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                          |[0x800003d4]:fcvt.s.d t5, t3, dyn<br> [0x800003d8]:csrrs a5, fcsr, zero<br> [0x800003dc]:sw t5, 48(ra)<br> [0x800003e0]:sw a5, 56(ra)<br>   |
|  17|[0x800023b8]<br>0x5EFFFFFF<br> [0x800023c0]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                         |[0x80000400]:fcvt.s.d t5, t3, dyn<br> [0x80000404]:csrrs a5, fcsr, zero<br> [0x80000408]:sw t5, 64(ra)<br> [0x8000040c]:sw a5, 72(ra)<br>   |
|  18|[0x800023c8]<br>0x5EFFFFFF<br> [0x800023d0]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                         |[0x8000042c]:fcvt.s.d t5, t3, dyn<br> [0x80000430]:csrrs a5, fcsr, zero<br> [0x80000434]:sw t5, 80(ra)<br> [0x80000438]:sw a5, 88(ra)<br>   |
|  19|[0x800023d8]<br>0x5F000000<br> [0x800023e0]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                         |[0x80000458]:fcvt.s.d t5, t3, dyn<br> [0x8000045c]:csrrs a5, fcsr, zero<br> [0x80000460]:sw t5, 96(ra)<br> [0x80000464]:sw a5, 104(ra)<br>  |
|  20|[0x800023e8]<br>0x5F000000<br> [0x800023f0]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                         |[0x80000484]:fcvt.s.d t5, t3, dyn<br> [0x80000488]:csrrs a5, fcsr, zero<br> [0x8000048c]:sw t5, 112(ra)<br> [0x80000490]:sw a5, 120(ra)<br> |
|  21|[0x800023f8]<br>0x5F000000<br> [0x80002400]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                          |[0x800004ac]:fcvt.s.d t5, t3, dyn<br> [0x800004b0]:csrrs a5, fcsr, zero<br> [0x800004b4]:sw t5, 128(ra)<br> [0x800004b8]:sw a5, 136(ra)<br> |
|  22|[0x80002408]<br>0x5F000000<br> [0x80002410]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                         |[0x800004d4]:fcvt.s.d t5, t3, dyn<br> [0x800004d8]:csrrs a5, fcsr, zero<br> [0x800004dc]:sw t5, 144(ra)<br> [0x800004e0]:sw a5, 152(ra)<br> |
|  23|[0x80002418]<br>0x5F000000<br> [0x80002420]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                         |[0x800004fc]:fcvt.s.d t5, t3, dyn<br> [0x80000500]:csrrs a5, fcsr, zero<br> [0x80000504]:sw t5, 160(ra)<br> [0x80000508]:sw a5, 168(ra)<br> |
|  24|[0x80002428]<br>0x5F000000<br> [0x80002430]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                         |[0x80000524]:fcvt.s.d t5, t3, dyn<br> [0x80000528]:csrrs a5, fcsr, zero<br> [0x8000052c]:sw t5, 176(ra)<br> [0x80000530]:sw a5, 184(ra)<br> |
|  25|[0x80002438]<br>0x5F000000<br> [0x80002440]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                         |[0x8000054c]:fcvt.s.d t5, t3, dyn<br> [0x80000550]:csrrs a5, fcsr, zero<br> [0x80000554]:sw t5, 192(ra)<br> [0x80000558]:sw a5, 200(ra)<br> |
|  26|[0x80002448]<br>0x5F000000<br> [0x80002450]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                          |[0x80000574]:fcvt.s.d t5, t3, dyn<br> [0x80000578]:csrrs a5, fcsr, zero<br> [0x8000057c]:sw t5, 208(ra)<br> [0x80000580]:sw a5, 216(ra)<br> |
|  27|[0x80002458]<br>0x5F000000<br> [0x80002460]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                         |[0x8000059c]:fcvt.s.d t5, t3, dyn<br> [0x800005a0]:csrrs a5, fcsr, zero<br> [0x800005a4]:sw t5, 224(ra)<br> [0x800005a8]:sw a5, 232(ra)<br> |
|  28|[0x80002468]<br>0x5F000000<br> [0x80002470]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                         |[0x800005c4]:fcvt.s.d t5, t3, dyn<br> [0x800005c8]:csrrs a5, fcsr, zero<br> [0x800005cc]:sw t5, 240(ra)<br> [0x800005d0]:sw a5, 248(ra)<br> |
|  29|[0x80002478]<br>0x5F000001<br> [0x80002480]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                         |[0x800005ec]:fcvt.s.d t5, t3, dyn<br> [0x800005f0]:csrrs a5, fcsr, zero<br> [0x800005f4]:sw t5, 256(ra)<br> [0x800005f8]:sw a5, 264(ra)<br> |
|  30|[0x80002488]<br>0x5F000000<br> [0x80002490]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                         |[0x80000614]:fcvt.s.d t5, t3, dyn<br> [0x80000618]:csrrs a5, fcsr, zero<br> [0x8000061c]:sw t5, 272(ra)<br> [0x80000620]:sw a5, 280(ra)<br> |
