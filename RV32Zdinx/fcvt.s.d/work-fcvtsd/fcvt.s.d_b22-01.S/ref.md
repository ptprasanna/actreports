
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800003c0')]      |
| SIG_REGION                | [('0x80002210', '0x800022a0', '36 words')]      |
| COV_LABELS                | fcvt.s.d_b22      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-fcvtsd/fcvt.s.d_b22-01.S/ref.S    |
| Total Number of coverpoints| 41     |
| Total Coverpoints Hit     | 41      |
| Total Signature Updates   | 24      |
| STAT1                     | 12      |
| STAT2                     | 0      |
| STAT3                     | 3     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x800002d4]:fcvt.s.d a2, a0, dyn
[0x800002d8]:csrrs tp, fcsr, zero
[0x800002dc]:sw a2, 144(ra)
[0x800002e0]:sw tp, 152(ra)
[0x800002e4]:auipc a4, 2
[0x800002e8]:addi a4, a4, 3452
[0x800002ec]:lw a2, 80(a4)
[0x800002f0]:lw a3, 84(a4)
[0x800002f4]:addi a2, zero, 0
[0x800002f8]:addi a3, zero, 0
[0x800002fc]:addi sp, zero, 0
[0x80000300]:csrrw zero, fcsr, sp
[0x80000304]:fcvt.s.d a0, a2, dyn
[0x80000308]:csrrs a5, fcsr, zero
[0x8000030c]:sw a0, 160(ra)
[0x80000310]:sw a5, 168(ra)
[0x80000314]:lw t1, 88(a4)
[0x80000318]:lw t2, 92(a4)
[0x8000031c]:addi t1, zero, 0
[0x80000320]:addi t2, zero, 0
[0x80000324]:addi sp, zero, 0
[0x80000328]:csrrw zero, fcsr, sp
[0x8000032c]:fcvt.s.d fp, t1, dyn
[0x80000330]:csrrs a5, fcsr, zero
[0x80000334]:sw fp, 176(ra)
[0x80000338]:sw a5, 184(ra)
[0x8000033c]:auipc ra, 2
[0x80000340]:addi ra, ra, 3900
[0x80000344]:lw fp, 0(a4)
[0x80000348]:lw s1, 4(a4)
[0x8000034c]:addi fp, zero, 0
[0x80000350]:addi s1, zero, 0
[0x80000354]:addi a0, zero, 0
[0x80000358]:csrrw zero, fcsr, a0
[0x8000035c]:fcvt.s.d t1, fp, dyn
[0x80000360]:csrrs a5, fcsr, zero
[0x80000364]:sw t1, 0(ra)
[0x80000368]:sw a5, 8(ra)
[0x8000036c]:lw sp, 8(a4)
[0x80000370]:lw gp, 12(a4)
[0x80000374]:addi sp, zero, 0
[0x80000378]:addi gp, zero, 0
[0x8000037c]:addi a0, zero, 0
[0x80000380]:csrrw zero, fcsr, a0
[0x80000384]:fcvt.s.d tp, sp, dyn
[0x80000388]:csrrs a5, fcsr, zero
[0x8000038c]:sw tp, 16(ra)
[0x80000390]:sw a5, 24(ra)
[0x80000394]:lw tp, 16(a4)
[0x80000398]:lw t0, 20(a4)
[0x8000039c]:addi tp, zero, 0
[0x800003a0]:addi t0, zero, 0
[0x800003a4]:addi a0, zero, 0
[0x800003a8]:csrrw zero, fcsr, a0
[0x800003ac]:fcvt.s.d sp, tp, dyn
[0x800003b0]:csrrs a5, fcsr, zero
[0x800003b4]:sw sp, 32(ra)
[0x800003b8]:sw a5, 40(ra)
[0x800003bc]:addi zero, zero, 0

[0x80000304]:fcvt.s.d a0, a2, dyn
[0x80000308]:csrrs a5, fcsr, zero
[0x8000030c]:sw a0, 160(ra)
[0x80000310]:sw a5, 168(ra)
[0x80000314]:lw t1, 88(a4)
[0x80000318]:lw t2, 92(a4)
[0x8000031c]:addi t1, zero, 0
[0x80000320]:addi t2, zero, 0
[0x80000324]:addi sp, zero, 0
[0x80000328]:csrrw zero, fcsr, sp
[0x8000032c]:fcvt.s.d fp, t1, dyn
[0x80000330]:csrrs a5, fcsr, zero
[0x80000334]:sw fp, 176(ra)
[0x80000338]:sw a5, 184(ra)
[0x8000033c]:auipc ra, 2
[0x80000340]:addi ra, ra, 3900
[0x80000344]:lw fp, 0(a4)
[0x80000348]:lw s1, 4(a4)
[0x8000034c]:addi fp, zero, 0
[0x80000350]:addi s1, zero, 0
[0x80000354]:addi a0, zero, 0
[0x80000358]:csrrw zero, fcsr, a0
[0x8000035c]:fcvt.s.d t1, fp, dyn
[0x80000360]:csrrs a5, fcsr, zero
[0x80000364]:sw t1, 0(ra)
[0x80000368]:sw a5, 8(ra)
[0x8000036c]:lw sp, 8(a4)
[0x80000370]:lw gp, 12(a4)
[0x80000374]:addi sp, zero, 0
[0x80000378]:addi gp, zero, 0
[0x8000037c]:addi a0, zero, 0
[0x80000380]:csrrw zero, fcsr, a0
[0x80000384]:fcvt.s.d tp, sp, dyn
[0x80000388]:csrrs a5, fcsr, zero
[0x8000038c]:sw tp, 16(ra)
[0x80000390]:sw a5, 24(ra)
[0x80000394]:lw tp, 16(a4)
[0x80000398]:lw t0, 20(a4)
[0x8000039c]:addi tp, zero, 0
[0x800003a0]:addi t0, zero, 0
[0x800003a4]:addi a0, zero, 0
[0x800003a8]:csrrw zero, fcsr, a0
[0x800003ac]:fcvt.s.d sp, tp, dyn
[0x800003b0]:csrrs a5, fcsr, zero
[0x800003b4]:sw sp, 32(ra)
[0x800003b8]:sw a5, 40(ra)
[0x800003bc]:addi zero, zero, 0

[0x8000032c]:fcvt.s.d fp, t1, dyn
[0x80000330]:csrrs a5, fcsr, zero
[0x80000334]:sw fp, 176(ra)
[0x80000338]:sw a5, 184(ra)
[0x8000033c]:auipc ra, 2
[0x80000340]:addi ra, ra, 3900
[0x80000344]:lw fp, 0(a4)
[0x80000348]:lw s1, 4(a4)
[0x8000034c]:addi fp, zero, 0
[0x80000350]:addi s1, zero, 0
[0x80000354]:addi a0, zero, 0
[0x80000358]:csrrw zero, fcsr, a0
[0x8000035c]:fcvt.s.d t1, fp, dyn
[0x80000360]:csrrs a5, fcsr, zero
[0x80000364]:sw t1, 0(ra)
[0x80000368]:sw a5, 8(ra)
[0x8000036c]:lw sp, 8(a4)
[0x80000370]:lw gp, 12(a4)
[0x80000374]:addi sp, zero, 0
[0x80000378]:addi gp, zero, 0
[0x8000037c]:addi a0, zero, 0
[0x80000380]:csrrw zero, fcsr, a0
[0x80000384]:fcvt.s.d tp, sp, dyn
[0x80000388]:csrrs a5, fcsr, zero
[0x8000038c]:sw tp, 16(ra)
[0x80000390]:sw a5, 24(ra)
[0x80000394]:lw tp, 16(a4)
[0x80000398]:lw t0, 20(a4)
[0x8000039c]:addi tp, zero, 0
[0x800003a0]:addi t0, zero, 0
[0x800003a4]:addi a0, zero, 0
[0x800003a8]:csrrw zero, fcsr, a0
[0x800003ac]:fcvt.s.d sp, tp, dyn
[0x800003b0]:csrrs a5, fcsr, zero
[0x800003b4]:sw sp, 32(ra)
[0x800003b8]:sw a5, 40(ra)
[0x800003bc]:addi zero, zero, 0



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                          signature                           |                                                                                 coverpoints                                                                                  |                                                                    code                                                                    |
|---:|--------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002218]<br>0x3E042BBD<br> [0x80002220]<br>0x00000001<br> |- mnemonic : fcvt.s.d<br> - rs1 : x30<br> - rd : x30<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08577924770d3 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x80000134]:fcvt.s.d t5, t5, dyn<br> [0x80000138]:csrrs tp, fcsr, zero<br> [0x8000013c]:sw t5, 0(ra)<br> [0x80000140]:sw tp, 8(ra)<br>     |
|   2|[0x80002228]<br>0x3EC9FEE4<br> [0x80002230]<br>0x00000001<br> |- rs1 : x26<br> - rd : x28<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0x3fd and fm1 == 0x93fdc7b89296c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                           |[0x80000164]:fcvt.s.d t3, s10, dyn<br> [0x80000168]:csrrs tp, fcsr, zero<br> [0x8000016c]:sw t3, 16(ra)<br> [0x80000170]:sw tp, 24(ra)<br>  |
|   3|[0x80002238]<br>0xBF3B35D2<br> [0x80002240]<br>0x00000001<br> |- rs1 : x28<br> - rd : x26<br> - fs1 == 1 and fe1 == 0x3fe and fm1 == 0x766ba34c2da80 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x80000194]:fcvt.s.d s10, t3, dyn<br> [0x80000198]:csrrs tp, fcsr, zero<br> [0x8000019c]:sw s10, 32(ra)<br> [0x800001a0]:sw tp, 40(ra)<br> |
|   4|[0x80002248]<br>0x3FE96B5C<br> [0x80002250]<br>0x00000001<br> |- rs1 : x22<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0xd2d6b7dc59a3a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x800001c4]:fcvt.s.d s8, s6, dyn<br> [0x800001c8]:csrrs tp, fcsr, zero<br> [0x800001cc]:sw s8, 48(ra)<br> [0x800001d0]:sw tp, 56(ra)<br>   |
|   5|[0x80002258]<br>0x4067C25D<br> [0x80002260]<br>0x00000001<br> |- rs1 : x24<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x400 and fm1 == 0xcf84ba749f9c5 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x800001f4]:fcvt.s.d s6, s8, dyn<br> [0x800001f8]:csrrs tp, fcsr, zero<br> [0x800001fc]:sw s6, 64(ra)<br> [0x80000200]:sw tp, 72(ra)<br>   |
|   6|[0x80002268]<br>0x40C2A548<br> [0x80002270]<br>0x00000001<br> |- rs1 : x18<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x401 and fm1 == 0x854a908ceac39 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x80000224]:fcvt.s.d s4, s2, dyn<br> [0x80000228]:csrrs tp, fcsr, zero<br> [0x8000022c]:sw s4, 80(ra)<br> [0x80000230]:sw tp, 88(ra)<br>   |
|   7|[0x80002278]<br>0x80000000<br> [0x80002280]<br>0x00000003<br> |- rs1 : x20<br> - rd : x18<br> - fs1 == 1 and fe1 == 0x0ff and fm1 == 0x137a953e8eb43 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x80000254]:fcvt.s.d s2, s4, dyn<br> [0x80000258]:csrrs tp, fcsr, zero<br> [0x8000025c]:sw s2, 96(ra)<br> [0x80000260]:sw tp, 104(ra)<br>  |
|   8|[0x80002288]<br>0x7F800000<br> [0x80002290]<br>0x00000005<br> |- rs1 : x14<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbedc2f3ebcf12 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x80000284]:fcvt.s.d a6, a4, dyn<br> [0x80000288]:csrrs tp, fcsr, zero<br> [0x8000028c]:sw a6, 112(ra)<br> [0x80000290]:sw tp, 120(ra)<br> |
|   9|[0x80002298]<br>0x00000000<br> [0x800022a0]<br>0x00000000<br> |- rs1 : x16<br> - rd : x14<br>                                                                                                                                                |[0x800002ac]:fcvt.s.d a4, a6, dyn<br> [0x800002b0]:csrrs tp, fcsr, zero<br> [0x800002b4]:sw a4, 128(ra)<br> [0x800002b8]:sw tp, 136(ra)<br> |
|  10|[0x80002278]<br>0x00000000<br> [0x80002280]<br>0x00000000<br> |- rs1 : x8<br> - rd : x6<br>                                                                                                                                                  |[0x8000035c]:fcvt.s.d t1, fp, dyn<br> [0x80000360]:csrrs a5, fcsr, zero<br> [0x80000364]:sw t1, 0(ra)<br> [0x80000368]:sw a5, 8(ra)<br>     |
|  11|[0x80002288]<br>0x00000000<br> [0x80002290]<br>0x00000000<br> |- rs1 : x2<br> - rd : x4<br>                                                                                                                                                  |[0x80000384]:fcvt.s.d tp, sp, dyn<br> [0x80000388]:csrrs a5, fcsr, zero<br> [0x8000038c]:sw tp, 16(ra)<br> [0x80000390]:sw a5, 24(ra)<br>   |
|  12|[0x80002298]<br>0x00000000<br> [0x800022a0]<br>0x00000000<br> |- rs1 : x4<br> - rd : x2<br>                                                                                                                                                  |[0x800003ac]:fcvt.s.d sp, tp, dyn<br> [0x800003b0]:csrrs a5, fcsr, zero<br> [0x800003b4]:sw sp, 32(ra)<br> [0x800003b8]:sw a5, 40(ra)<br>   |
