
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80001030')]      |
| SIG_REGION                | [('0x80003410', '0x800036a0', '164 words')]      |
| COV_LABELS                | fcvt.s.d_b29      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-fcvtsd/fcvt.s.d_b29-01.S/ref.S    |
| Total Number of coverpoints| 113     |
| Total Coverpoints Hit     | 113      |
| Total Signature Updates   | 94      |
| STAT1                     | 47      |
| STAT2                     | 0      |
| STAT3                     | 33     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000a14]:fcvt.s.d t5, t3, dyn
[0x80000a18]:csrrs a5, fcsr, zero
[0x80000a1c]:sw t5, 560(ra)
[0x80000a20]:sw a5, 568(ra)
[0x80000a24]:lw t3, 288(a4)
[0x80000a28]:lw t4, 292(a4)
[0x80000a2c]:lui t3, 598968
[0x80000a30]:addi t3, t3, 1689
[0x80000a34]:lui t4, 785416
[0x80000a38]:addi t4, t4, 1396
[0x80000a3c]:addi a0, zero, 96
[0x80000a40]:csrrw zero, fcsr, a0
[0x80000a44]:fcvt.s.d t5, t3, dyn
[0x80000a48]:csrrs a5, fcsr, zero

[0x80000a44]:fcvt.s.d t5, t3, dyn
[0x80000a48]:csrrs a5, fcsr, zero
[0x80000a4c]:sw t5, 576(ra)
[0x80000a50]:sw a5, 584(ra)
[0x80000a54]:lw t3, 296(a4)
[0x80000a58]:lw t4, 300(a4)
[0x80000a5c]:lui t3, 598968
[0x80000a60]:addi t3, t3, 1689
[0x80000a64]:lui t4, 785416
[0x80000a68]:addi t4, t4, 1396
[0x80000a6c]:addi a0, zero, 128
[0x80000a70]:csrrw zero, fcsr, a0
[0x80000a74]:fcvt.s.d t5, t3, dyn
[0x80000a78]:csrrs a5, fcsr, zero

[0x80000a74]:fcvt.s.d t5, t3, dyn
[0x80000a78]:csrrs a5, fcsr, zero
[0x80000a7c]:sw t5, 592(ra)
[0x80000a80]:sw a5, 600(ra)
[0x80000a84]:lw t3, 304(a4)
[0x80000a88]:lw t4, 308(a4)
[0x80000a8c]:lui t3, 598968
[0x80000a90]:addi t3, t3, 1690
[0x80000a94]:lui t4, 785416
[0x80000a98]:addi t4, t4, 1396
[0x80000a9c]:addi a0, zero, 0
[0x80000aa0]:csrrw zero, fcsr, a0
[0x80000aa4]:fcvt.s.d t5, t3, dyn
[0x80000aa8]:csrrs a5, fcsr, zero

[0x80000aa4]:fcvt.s.d t5, t3, dyn
[0x80000aa8]:csrrs a5, fcsr, zero
[0x80000aac]:sw t5, 608(ra)
[0x80000ab0]:sw a5, 616(ra)
[0x80000ab4]:lw t3, 312(a4)
[0x80000ab8]:lw t4, 316(a4)
[0x80000abc]:lui t3, 598968
[0x80000ac0]:addi t3, t3, 1690
[0x80000ac4]:lui t4, 785416
[0x80000ac8]:addi t4, t4, 1396
[0x80000acc]:addi a0, zero, 32
[0x80000ad0]:csrrw zero, fcsr, a0
[0x80000ad4]:fcvt.s.d t5, t3, dyn
[0x80000ad8]:csrrs a5, fcsr, zero

[0x80000ad4]:fcvt.s.d t5, t3, dyn
[0x80000ad8]:csrrs a5, fcsr, zero
[0x80000adc]:sw t5, 624(ra)
[0x80000ae0]:sw a5, 632(ra)
[0x80000ae4]:lw t3, 320(a4)
[0x80000ae8]:lw t4, 324(a4)
[0x80000aec]:lui t3, 598968
[0x80000af0]:addi t3, t3, 1690
[0x80000af4]:lui t4, 785416
[0x80000af8]:addi t4, t4, 1396
[0x80000afc]:addi a0, zero, 64
[0x80000b00]:csrrw zero, fcsr, a0
[0x80000b04]:fcvt.s.d t5, t3, dyn
[0x80000b08]:csrrs a5, fcsr, zero

[0x80000b04]:fcvt.s.d t5, t3, dyn
[0x80000b08]:csrrs a5, fcsr, zero
[0x80000b0c]:sw t5, 640(ra)
[0x80000b10]:sw a5, 648(ra)
[0x80000b14]:lw t3, 328(a4)
[0x80000b18]:lw t4, 332(a4)
[0x80000b1c]:lui t3, 598968
[0x80000b20]:addi t3, t3, 1690
[0x80000b24]:lui t4, 785416
[0x80000b28]:addi t4, t4, 1396
[0x80000b2c]:addi a0, zero, 96
[0x80000b30]:csrrw zero, fcsr, a0
[0x80000b34]:fcvt.s.d t5, t3, dyn
[0x80000b38]:csrrs a5, fcsr, zero

[0x80000b34]:fcvt.s.d t5, t3, dyn
[0x80000b38]:csrrs a5, fcsr, zero
[0x80000b3c]:sw t5, 656(ra)
[0x80000b40]:sw a5, 664(ra)
[0x80000b44]:lw t3, 336(a4)
[0x80000b48]:lw t4, 340(a4)
[0x80000b4c]:lui t3, 598968
[0x80000b50]:addi t3, t3, 1690
[0x80000b54]:lui t4, 785416
[0x80000b58]:addi t4, t4, 1396
[0x80000b5c]:addi a0, zero, 128
[0x80000b60]:csrrw zero, fcsr, a0
[0x80000b64]:fcvt.s.d t5, t3, dyn
[0x80000b68]:csrrs a5, fcsr, zero

[0x80000b64]:fcvt.s.d t5, t3, dyn
[0x80000b68]:csrrs a5, fcsr, zero
[0x80000b6c]:sw t5, 672(ra)
[0x80000b70]:sw a5, 680(ra)
[0x80000b74]:lw t3, 344(a4)
[0x80000b78]:lw t4, 348(a4)
[0x80000b7c]:lui t3, 598968
[0x80000b80]:addi t3, t3, 1691
[0x80000b84]:lui t4, 785416
[0x80000b88]:addi t4, t4, 1396
[0x80000b8c]:addi a0, zero, 0
[0x80000b90]:csrrw zero, fcsr, a0
[0x80000b94]:fcvt.s.d t5, t3, dyn
[0x80000b98]:csrrs a5, fcsr, zero

[0x80000b94]:fcvt.s.d t5, t3, dyn
[0x80000b98]:csrrs a5, fcsr, zero
[0x80000b9c]:sw t5, 688(ra)
[0x80000ba0]:sw a5, 696(ra)
[0x80000ba4]:lw t3, 352(a4)
[0x80000ba8]:lw t4, 356(a4)
[0x80000bac]:lui t3, 598968
[0x80000bb0]:addi t3, t3, 1691
[0x80000bb4]:lui t4, 785416
[0x80000bb8]:addi t4, t4, 1396
[0x80000bbc]:addi a0, zero, 32
[0x80000bc0]:csrrw zero, fcsr, a0
[0x80000bc4]:fcvt.s.d t5, t3, dyn
[0x80000bc8]:csrrs a5, fcsr, zero

[0x80000bc4]:fcvt.s.d t5, t3, dyn
[0x80000bc8]:csrrs a5, fcsr, zero
[0x80000bcc]:sw t5, 704(ra)
[0x80000bd0]:sw a5, 712(ra)
[0x80000bd4]:lw t3, 360(a4)
[0x80000bd8]:lw t4, 364(a4)
[0x80000bdc]:lui t3, 598968
[0x80000be0]:addi t3, t3, 1691
[0x80000be4]:lui t4, 785416
[0x80000be8]:addi t4, t4, 1396
[0x80000bec]:addi a0, zero, 64
[0x80000bf0]:csrrw zero, fcsr, a0
[0x80000bf4]:fcvt.s.d t5, t3, dyn
[0x80000bf8]:csrrs a5, fcsr, zero

[0x80000bf4]:fcvt.s.d t5, t3, dyn
[0x80000bf8]:csrrs a5, fcsr, zero
[0x80000bfc]:sw t5, 720(ra)
[0x80000c00]:sw a5, 728(ra)
[0x80000c04]:lw t3, 368(a4)
[0x80000c08]:lw t4, 372(a4)
[0x80000c0c]:lui t3, 598968
[0x80000c10]:addi t3, t3, 1691
[0x80000c14]:lui t4, 785416
[0x80000c18]:addi t4, t4, 1396
[0x80000c1c]:addi a0, zero, 96
[0x80000c20]:csrrw zero, fcsr, a0
[0x80000c24]:fcvt.s.d t5, t3, dyn
[0x80000c28]:csrrs a5, fcsr, zero

[0x80000c24]:fcvt.s.d t5, t3, dyn
[0x80000c28]:csrrs a5, fcsr, zero
[0x80000c2c]:sw t5, 736(ra)
[0x80000c30]:sw a5, 744(ra)
[0x80000c34]:lw t3, 376(a4)
[0x80000c38]:lw t4, 380(a4)
[0x80000c3c]:lui t3, 598968
[0x80000c40]:addi t3, t3, 1691
[0x80000c44]:lui t4, 785416
[0x80000c48]:addi t4, t4, 1396
[0x80000c4c]:addi a0, zero, 128
[0x80000c50]:csrrw zero, fcsr, a0
[0x80000c54]:fcvt.s.d t5, t3, dyn
[0x80000c58]:csrrs a5, fcsr, zero

[0x80000c54]:fcvt.s.d t5, t3, dyn
[0x80000c58]:csrrs a5, fcsr, zero
[0x80000c5c]:sw t5, 752(ra)
[0x80000c60]:sw a5, 760(ra)
[0x80000c64]:lw t3, 384(a4)
[0x80000c68]:lw t4, 388(a4)
[0x80000c6c]:lui t3, 598968
[0x80000c70]:addi t3, t3, 1692
[0x80000c74]:lui t4, 785416
[0x80000c78]:addi t4, t4, 1396
[0x80000c7c]:addi a0, zero, 0
[0x80000c80]:csrrw zero, fcsr, a0
[0x80000c84]:fcvt.s.d t5, t3, dyn
[0x80000c88]:csrrs a5, fcsr, zero

[0x80000c84]:fcvt.s.d t5, t3, dyn
[0x80000c88]:csrrs a5, fcsr, zero
[0x80000c8c]:sw t5, 768(ra)
[0x80000c90]:sw a5, 776(ra)
[0x80000c94]:lw t3, 392(a4)
[0x80000c98]:lw t4, 396(a4)
[0x80000c9c]:lui t3, 598968
[0x80000ca0]:addi t3, t3, 1692
[0x80000ca4]:lui t4, 785416
[0x80000ca8]:addi t4, t4, 1396
[0x80000cac]:addi a0, zero, 32
[0x80000cb0]:csrrw zero, fcsr, a0
[0x80000cb4]:fcvt.s.d t5, t3, dyn
[0x80000cb8]:csrrs a5, fcsr, zero

[0x80000cb4]:fcvt.s.d t5, t3, dyn
[0x80000cb8]:csrrs a5, fcsr, zero
[0x80000cbc]:sw t5, 784(ra)
[0x80000cc0]:sw a5, 792(ra)
[0x80000cc4]:lw t3, 400(a4)
[0x80000cc8]:lw t4, 404(a4)
[0x80000ccc]:lui t3, 598968
[0x80000cd0]:addi t3, t3, 1692
[0x80000cd4]:lui t4, 785416
[0x80000cd8]:addi t4, t4, 1396
[0x80000cdc]:addi a0, zero, 64
[0x80000ce0]:csrrw zero, fcsr, a0
[0x80000ce4]:fcvt.s.d t5, t3, dyn
[0x80000ce8]:csrrs a5, fcsr, zero

[0x80000ce4]:fcvt.s.d t5, t3, dyn
[0x80000ce8]:csrrs a5, fcsr, zero
[0x80000cec]:sw t5, 800(ra)
[0x80000cf0]:sw a5, 808(ra)
[0x80000cf4]:lw t3, 408(a4)
[0x80000cf8]:lw t4, 412(a4)
[0x80000cfc]:lui t3, 598968
[0x80000d00]:addi t3, t3, 1692
[0x80000d04]:lui t4, 785416
[0x80000d08]:addi t4, t4, 1396
[0x80000d0c]:addi a0, zero, 96
[0x80000d10]:csrrw zero, fcsr, a0
[0x80000d14]:fcvt.s.d t5, t3, dyn
[0x80000d18]:csrrs a5, fcsr, zero

[0x80000d14]:fcvt.s.d t5, t3, dyn
[0x80000d18]:csrrs a5, fcsr, zero
[0x80000d1c]:sw t5, 816(ra)
[0x80000d20]:sw a5, 824(ra)
[0x80000d24]:lw t3, 416(a4)
[0x80000d28]:lw t4, 420(a4)
[0x80000d2c]:lui t3, 598968
[0x80000d30]:addi t3, t3, 1692
[0x80000d34]:lui t4, 785416
[0x80000d38]:addi t4, t4, 1396
[0x80000d3c]:addi a0, zero, 128
[0x80000d40]:csrrw zero, fcsr, a0
[0x80000d44]:fcvt.s.d t5, t3, dyn
[0x80000d48]:csrrs a5, fcsr, zero

[0x80000d44]:fcvt.s.d t5, t3, dyn
[0x80000d48]:csrrs a5, fcsr, zero
[0x80000d4c]:sw t5, 832(ra)
[0x80000d50]:sw a5, 840(ra)
[0x80000d54]:lw t3, 424(a4)
[0x80000d58]:lw t4, 428(a4)
[0x80000d5c]:lui t3, 598968
[0x80000d60]:addi t3, t3, 1693
[0x80000d64]:lui t4, 785416
[0x80000d68]:addi t4, t4, 1396
[0x80000d6c]:addi a0, zero, 0
[0x80000d70]:csrrw zero, fcsr, a0
[0x80000d74]:fcvt.s.d t5, t3, dyn
[0x80000d78]:csrrs a5, fcsr, zero

[0x80000d74]:fcvt.s.d t5, t3, dyn
[0x80000d78]:csrrs a5, fcsr, zero
[0x80000d7c]:sw t5, 848(ra)
[0x80000d80]:sw a5, 856(ra)
[0x80000d84]:lw t3, 432(a4)
[0x80000d88]:lw t4, 436(a4)
[0x80000d8c]:lui t3, 598968
[0x80000d90]:addi t3, t3, 1693
[0x80000d94]:lui t4, 785416
[0x80000d98]:addi t4, t4, 1396
[0x80000d9c]:addi a0, zero, 32
[0x80000da0]:csrrw zero, fcsr, a0
[0x80000da4]:fcvt.s.d t5, t3, dyn
[0x80000da8]:csrrs a5, fcsr, zero

[0x80000da4]:fcvt.s.d t5, t3, dyn
[0x80000da8]:csrrs a5, fcsr, zero
[0x80000dac]:sw t5, 864(ra)
[0x80000db0]:sw a5, 872(ra)
[0x80000db4]:lw t3, 440(a4)
[0x80000db8]:lw t4, 444(a4)
[0x80000dbc]:lui t3, 598968
[0x80000dc0]:addi t3, t3, 1693
[0x80000dc4]:lui t4, 785416
[0x80000dc8]:addi t4, t4, 1396
[0x80000dcc]:addi a0, zero, 64
[0x80000dd0]:csrrw zero, fcsr, a0
[0x80000dd4]:fcvt.s.d t5, t3, dyn
[0x80000dd8]:csrrs a5, fcsr, zero

[0x80000dd4]:fcvt.s.d t5, t3, dyn
[0x80000dd8]:csrrs a5, fcsr, zero
[0x80000ddc]:sw t5, 880(ra)
[0x80000de0]:sw a5, 888(ra)
[0x80000de4]:lw t3, 448(a4)
[0x80000de8]:lw t4, 452(a4)
[0x80000dec]:lui t3, 598968
[0x80000df0]:addi t3, t3, 1693
[0x80000df4]:lui t4, 785416
[0x80000df8]:addi t4, t4, 1396
[0x80000dfc]:addi a0, zero, 96
[0x80000e00]:csrrw zero, fcsr, a0
[0x80000e04]:fcvt.s.d t5, t3, dyn
[0x80000e08]:csrrs a5, fcsr, zero

[0x80000e04]:fcvt.s.d t5, t3, dyn
[0x80000e08]:csrrs a5, fcsr, zero
[0x80000e0c]:sw t5, 896(ra)
[0x80000e10]:sw a5, 904(ra)
[0x80000e14]:lw t3, 456(a4)
[0x80000e18]:lw t4, 460(a4)
[0x80000e1c]:lui t3, 598968
[0x80000e20]:addi t3, t3, 1693
[0x80000e24]:lui t4, 785416
[0x80000e28]:addi t4, t4, 1396
[0x80000e2c]:addi a0, zero, 128
[0x80000e30]:csrrw zero, fcsr, a0
[0x80000e34]:fcvt.s.d t5, t3, dyn
[0x80000e38]:csrrs a5, fcsr, zero

[0x80000e34]:fcvt.s.d t5, t3, dyn
[0x80000e38]:csrrs a5, fcsr, zero
[0x80000e3c]:sw t5, 912(ra)
[0x80000e40]:sw a5, 920(ra)
[0x80000e44]:lw t3, 464(a4)
[0x80000e48]:lw t4, 468(a4)
[0x80000e4c]:lui t3, 598968
[0x80000e50]:addi t3, t3, 1694
[0x80000e54]:lui t4, 785416
[0x80000e58]:addi t4, t4, 1396
[0x80000e5c]:addi a0, zero, 0
[0x80000e60]:csrrw zero, fcsr, a0
[0x80000e64]:fcvt.s.d t5, t3, dyn
[0x80000e68]:csrrs a5, fcsr, zero

[0x80000e64]:fcvt.s.d t5, t3, dyn
[0x80000e68]:csrrs a5, fcsr, zero
[0x80000e6c]:sw t5, 928(ra)
[0x80000e70]:sw a5, 936(ra)
[0x80000e74]:lw t3, 472(a4)
[0x80000e78]:lw t4, 476(a4)
[0x80000e7c]:lui t3, 598968
[0x80000e80]:addi t3, t3, 1694
[0x80000e84]:lui t4, 785416
[0x80000e88]:addi t4, t4, 1396
[0x80000e8c]:addi a0, zero, 32
[0x80000e90]:csrrw zero, fcsr, a0
[0x80000e94]:fcvt.s.d t5, t3, dyn
[0x80000e98]:csrrs a5, fcsr, zero

[0x80000e94]:fcvt.s.d t5, t3, dyn
[0x80000e98]:csrrs a5, fcsr, zero
[0x80000e9c]:sw t5, 944(ra)
[0x80000ea0]:sw a5, 952(ra)
[0x80000ea4]:lw t3, 480(a4)
[0x80000ea8]:lw t4, 484(a4)
[0x80000eac]:lui t3, 598968
[0x80000eb0]:addi t3, t3, 1694
[0x80000eb4]:lui t4, 785416
[0x80000eb8]:addi t4, t4, 1396
[0x80000ebc]:addi a0, zero, 64
[0x80000ec0]:csrrw zero, fcsr, a0
[0x80000ec4]:fcvt.s.d t5, t3, dyn
[0x80000ec8]:csrrs a5, fcsr, zero

[0x80000ec4]:fcvt.s.d t5, t3, dyn
[0x80000ec8]:csrrs a5, fcsr, zero
[0x80000ecc]:sw t5, 960(ra)
[0x80000ed0]:sw a5, 968(ra)
[0x80000ed4]:lw t3, 488(a4)
[0x80000ed8]:lw t4, 492(a4)
[0x80000edc]:lui t3, 598968
[0x80000ee0]:addi t3, t3, 1694
[0x80000ee4]:lui t4, 785416
[0x80000ee8]:addi t4, t4, 1396
[0x80000eec]:addi a0, zero, 96
[0x80000ef0]:csrrw zero, fcsr, a0
[0x80000ef4]:fcvt.s.d t5, t3, dyn
[0x80000ef8]:csrrs a5, fcsr, zero

[0x80000ef4]:fcvt.s.d t5, t3, dyn
[0x80000ef8]:csrrs a5, fcsr, zero
[0x80000efc]:sw t5, 976(ra)
[0x80000f00]:sw a5, 984(ra)
[0x80000f04]:lw t3, 496(a4)
[0x80000f08]:lw t4, 500(a4)
[0x80000f0c]:lui t3, 598968
[0x80000f10]:addi t3, t3, 1694
[0x80000f14]:lui t4, 785416
[0x80000f18]:addi t4, t4, 1396
[0x80000f1c]:addi a0, zero, 128
[0x80000f20]:csrrw zero, fcsr, a0
[0x80000f24]:fcvt.s.d t5, t3, dyn
[0x80000f28]:csrrs a5, fcsr, zero

[0x80000f24]:fcvt.s.d t5, t3, dyn
[0x80000f28]:csrrs a5, fcsr, zero
[0x80000f2c]:sw t5, 992(ra)
[0x80000f30]:sw a5, 1000(ra)
[0x80000f34]:lw t3, 504(a4)
[0x80000f38]:lw t4, 508(a4)
[0x80000f3c]:lui t3, 598968
[0x80000f40]:addi t3, t3, 1695
[0x80000f44]:lui t4, 785416
[0x80000f48]:addi t4, t4, 1396
[0x80000f4c]:addi a0, zero, 0
[0x80000f50]:csrrw zero, fcsr, a0
[0x80000f54]:fcvt.s.d t5, t3, dyn
[0x80000f58]:csrrs a5, fcsr, zero

[0x80000f54]:fcvt.s.d t5, t3, dyn
[0x80000f58]:csrrs a5, fcsr, zero
[0x80000f5c]:sw t5, 1008(ra)
[0x80000f60]:sw a5, 1016(ra)
[0x80000f64]:lw t3, 512(a4)
[0x80000f68]:lw t4, 516(a4)
[0x80000f6c]:lui t3, 598968
[0x80000f70]:addi t3, t3, 1695
[0x80000f74]:lui t4, 785416
[0x80000f78]:addi t4, t4, 1396
[0x80000f7c]:addi a0, zero, 32
[0x80000f80]:csrrw zero, fcsr, a0
[0x80000f84]:fcvt.s.d t5, t3, dyn
[0x80000f88]:csrrs a5, fcsr, zero

[0x80000f84]:fcvt.s.d t5, t3, dyn
[0x80000f88]:csrrs a5, fcsr, zero
[0x80000f8c]:sw t5, 1024(ra)
[0x80000f90]:sw a5, 1032(ra)
[0x80000f94]:lw t3, 520(a4)
[0x80000f98]:lw t4, 524(a4)
[0x80000f9c]:lui t3, 598968
[0x80000fa0]:addi t3, t3, 1695
[0x80000fa4]:lui t4, 785416
[0x80000fa8]:addi t4, t4, 1396
[0x80000fac]:addi a0, zero, 64
[0x80000fb0]:csrrw zero, fcsr, a0
[0x80000fb4]:fcvt.s.d t5, t3, dyn
[0x80000fb8]:csrrs a5, fcsr, zero

[0x80000fb4]:fcvt.s.d t5, t3, dyn
[0x80000fb8]:csrrs a5, fcsr, zero
[0x80000fbc]:sw t5, 1040(ra)
[0x80000fc0]:sw a5, 1048(ra)
[0x80000fc4]:lw t3, 528(a4)
[0x80000fc8]:lw t4, 532(a4)
[0x80000fcc]:lui t3, 598968
[0x80000fd0]:addi t3, t3, 1695
[0x80000fd4]:lui t4, 785416
[0x80000fd8]:addi t4, t4, 1396
[0x80000fdc]:addi a0, zero, 96
[0x80000fe0]:csrrw zero, fcsr, a0
[0x80000fe4]:fcvt.s.d t5, t3, dyn
[0x80000fe8]:csrrs a5, fcsr, zero

[0x80000fe4]:fcvt.s.d t5, t3, dyn
[0x80000fe8]:csrrs a5, fcsr, zero
[0x80000fec]:sw t5, 1056(ra)
[0x80000ff0]:sw a5, 1064(ra)
[0x80000ff4]:lw t3, 536(a4)
[0x80000ff8]:lw t4, 540(a4)
[0x80000ffc]:lui t3, 598968
[0x80001000]:addi t3, t3, 1695
[0x80001004]:lui t4, 785416
[0x80001008]:addi t4, t4, 1396
[0x8000100c]:addi a0, zero, 128
[0x80001010]:csrrw zero, fcsr, a0
[0x80001014]:fcvt.s.d t5, t3, dyn
[0x80001018]:csrrs a5, fcsr, zero

[0x80001014]:fcvt.s.d t5, t3, dyn
[0x80001018]:csrrs a5, fcsr, zero
[0x8000101c]:sw t5, 1072(ra)
[0x80001020]:sw a5, 1080(ra)
[0x80001024]:addi zero, zero, 0
[0x80001028]:addi zero, zero, 0
[0x8000102c]:addi zero, zero, 0



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                          signature                           |                                                                                 coverpoints                                                                                  |                                                                    code                                                                    |
|---:|--------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80003418]<br>0x3E042BA5<br> [0x80003420]<br>0x00000001<br> |- mnemonic : fcvt.s.d<br> - rs1 : x30<br> - rd : x30<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b8698 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x80000134]:fcvt.s.d t5, t5, dyn<br> [0x80000138]:csrrs tp, fcsr, zero<br> [0x8000013c]:sw t5, 0(ra)<br> [0x80000140]:sw tp, 8(ra)<br>     |
|   2|[0x80003428]<br>0x3E042BA4<br> [0x80003430]<br>0x00000021<br> |- rs1 : x26<br> - rd : x28<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b8698 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                          |[0x80000164]:fcvt.s.d t3, s10, dyn<br> [0x80000168]:csrrs tp, fcsr, zero<br> [0x8000016c]:sw t3, 16(ra)<br> [0x80000170]:sw tp, 24(ra)<br>  |
|   3|[0x80003438]<br>0x3E042BA4<br> [0x80003440]<br>0x00000041<br> |- rs1 : x28<br> - rd : x26<br> - fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b8698 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                          |[0x80000194]:fcvt.s.d s10, t3, dyn<br> [0x80000198]:csrrs tp, fcsr, zero<br> [0x8000019c]:sw s10, 32(ra)<br> [0x800001a0]:sw tp, 40(ra)<br> |
|   4|[0x80003448]<br>0x3E042BA5<br> [0x80003450]<br>0x00000061<br> |- rs1 : x22<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b8698 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                          |[0x800001c4]:fcvt.s.d s8, s6, dyn<br> [0x800001c8]:csrrs tp, fcsr, zero<br> [0x800001cc]:sw s8, 48(ra)<br> [0x800001d0]:sw tp, 56(ra)<br>   |
|   5|[0x80003458]<br>0x3E042BA5<br> [0x80003460]<br>0x00000081<br> |- rs1 : x24<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b8698 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                          |[0x800001f4]:fcvt.s.d s6, s8, dyn<br> [0x800001f8]:csrrs tp, fcsr, zero<br> [0x800001fc]:sw s6, 64(ra)<br> [0x80000200]:sw tp, 72(ra)<br>   |
|   6|[0x80003468]<br>0x3E042BA5<br> [0x80003470]<br>0x00000001<br> |- rs1 : x18<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b8699 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x80000224]:fcvt.s.d s4, s2, dyn<br> [0x80000228]:csrrs tp, fcsr, zero<br> [0x8000022c]:sw s4, 80(ra)<br> [0x80000230]:sw tp, 88(ra)<br>   |
|   7|[0x80003478]<br>0x3E042BA4<br> [0x80003480]<br>0x00000021<br> |- rs1 : x20<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b8699 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                          |[0x80000254]:fcvt.s.d s2, s4, dyn<br> [0x80000258]:csrrs tp, fcsr, zero<br> [0x8000025c]:sw s2, 96(ra)<br> [0x80000260]:sw tp, 104(ra)<br>  |
|   8|[0x80003488]<br>0x3E042BA4<br> [0x80003490]<br>0x00000041<br> |- rs1 : x14<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b8699 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                          |[0x80000284]:fcvt.s.d a6, a4, dyn<br> [0x80000288]:csrrs tp, fcsr, zero<br> [0x8000028c]:sw a6, 112(ra)<br> [0x80000290]:sw tp, 120(ra)<br> |
|   9|[0x80003498]<br>0x3E042BA5<br> [0x800034a0]<br>0x00000061<br> |- rs1 : x16<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b8699 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                          |[0x800002b4]:fcvt.s.d a4, a6, dyn<br> [0x800002b8]:csrrs tp, fcsr, zero<br> [0x800002bc]:sw a4, 128(ra)<br> [0x800002c0]:sw tp, 136(ra)<br> |
|  10|[0x800034a8]<br>0x3E042BA5<br> [0x800034b0]<br>0x00000081<br> |- rs1 : x10<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b8699 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                          |[0x800002e4]:fcvt.s.d a2, a0, dyn<br> [0x800002e8]:csrrs tp, fcsr, zero<br> [0x800002ec]:sw a2, 144(ra)<br> [0x800002f0]:sw tp, 152(ra)<br> |
|  11|[0x800034b8]<br>0x3E042BA5<br> [0x800034c0]<br>0x00000001<br> |- rs1 : x12<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                           |[0x8000031c]:fcvt.s.d a0, a2, dyn<br> [0x80000320]:csrrs a5, fcsr, zero<br> [0x80000324]:sw a0, 160(ra)<br> [0x80000328]:sw a5, 168(ra)<br> |
|  12|[0x800034c8]<br>0x3E042BA4<br> [0x800034d0]<br>0x00000021<br> |- rs1 : x6<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869a and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                            |[0x8000034c]:fcvt.s.d fp, t1, dyn<br> [0x80000350]:csrrs a5, fcsr, zero<br> [0x80000354]:sw fp, 176(ra)<br> [0x80000358]:sw a5, 184(ra)<br> |
|  13|[0x80003478]<br>0x3E042BA4<br> [0x80003480]<br>0x00000041<br> |- rs1 : x8<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869a and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                            |[0x80000384]:fcvt.s.d t1, fp, dyn<br> [0x80000388]:csrrs a5, fcsr, zero<br> [0x8000038c]:sw t1, 0(ra)<br> [0x80000390]:sw a5, 8(ra)<br>     |
|  14|[0x80003488]<br>0x3E042BA5<br> [0x80003490]<br>0x00000061<br> |- rs1 : x2<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869a and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                            |[0x800003b4]:fcvt.s.d tp, sp, dyn<br> [0x800003b8]:csrrs a5, fcsr, zero<br> [0x800003bc]:sw tp, 16(ra)<br> [0x800003c0]:sw a5, 24(ra)<br>   |
|  15|[0x80003498]<br>0x3E042BA5<br> [0x800034a0]<br>0x00000081<br> |- rs1 : x4<br> - rd : x2<br> - fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869a and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                            |[0x800003e4]:fcvt.s.d sp, tp, dyn<br> [0x800003e8]:csrrs a5, fcsr, zero<br> [0x800003ec]:sw sp, 32(ra)<br> [0x800003f0]:sw a5, 40(ra)<br>   |
|  16|[0x800034a8]<br>0x3E042BA5<br> [0x800034b0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                          |[0x80000414]:fcvt.s.d t5, t3, dyn<br> [0x80000418]:csrrs a5, fcsr, zero<br> [0x8000041c]:sw t5, 48(ra)<br> [0x80000420]:sw a5, 56(ra)<br>   |
|  17|[0x800034b8]<br>0x3E042BA4<br> [0x800034c0]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869b and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                         |[0x80000444]:fcvt.s.d t5, t3, dyn<br> [0x80000448]:csrrs a5, fcsr, zero<br> [0x8000044c]:sw t5, 64(ra)<br> [0x80000450]:sw a5, 72(ra)<br>   |
|  18|[0x800034c8]<br>0x3E042BA4<br> [0x800034d0]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869b and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                         |[0x80000474]:fcvt.s.d t5, t3, dyn<br> [0x80000478]:csrrs a5, fcsr, zero<br> [0x8000047c]:sw t5, 80(ra)<br> [0x80000480]:sw a5, 88(ra)<br>   |
|  19|[0x800034d8]<br>0x3E042BA5<br> [0x800034e0]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869b and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                         |[0x800004a4]:fcvt.s.d t5, t3, dyn<br> [0x800004a8]:csrrs a5, fcsr, zero<br> [0x800004ac]:sw t5, 96(ra)<br> [0x800004b0]:sw a5, 104(ra)<br>  |
|  20|[0x800034e8]<br>0x3E042BA5<br> [0x800034f0]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869b and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                         |[0x800004d4]:fcvt.s.d t5, t3, dyn<br> [0x800004d8]:csrrs a5, fcsr, zero<br> [0x800004dc]:sw t5, 112(ra)<br> [0x800004e0]:sw a5, 120(ra)<br> |
|  21|[0x800034f8]<br>0x3E042BA5<br> [0x80003500]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                          |[0x80000504]:fcvt.s.d t5, t3, dyn<br> [0x80000508]:csrrs a5, fcsr, zero<br> [0x8000050c]:sw t5, 128(ra)<br> [0x80000510]:sw a5, 136(ra)<br> |
|  22|[0x80003508]<br>0x3E042BA4<br> [0x80003510]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869c and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                         |[0x80000534]:fcvt.s.d t5, t3, dyn<br> [0x80000538]:csrrs a5, fcsr, zero<br> [0x8000053c]:sw t5, 144(ra)<br> [0x80000540]:sw a5, 152(ra)<br> |
|  23|[0x80003518]<br>0x3E042BA4<br> [0x80003520]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869c and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                         |[0x80000564]:fcvt.s.d t5, t3, dyn<br> [0x80000568]:csrrs a5, fcsr, zero<br> [0x8000056c]:sw t5, 160(ra)<br> [0x80000570]:sw a5, 168(ra)<br> |
|  24|[0x80003528]<br>0x3E042BA5<br> [0x80003530]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869c and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                         |[0x80000594]:fcvt.s.d t5, t3, dyn<br> [0x80000598]:csrrs a5, fcsr, zero<br> [0x8000059c]:sw t5, 176(ra)<br> [0x800005a0]:sw a5, 184(ra)<br> |
|  25|[0x80003538]<br>0x3E042BA5<br> [0x80003540]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869c and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                         |[0x800005c4]:fcvt.s.d t5, t3, dyn<br> [0x800005c8]:csrrs a5, fcsr, zero<br> [0x800005cc]:sw t5, 192(ra)<br> [0x800005d0]:sw a5, 200(ra)<br> |
|  26|[0x80003548]<br>0x3E042BA5<br> [0x80003550]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                          |[0x800005f4]:fcvt.s.d t5, t3, dyn<br> [0x800005f8]:csrrs a5, fcsr, zero<br> [0x800005fc]:sw t5, 208(ra)<br> [0x80000600]:sw a5, 216(ra)<br> |
|  27|[0x80003558]<br>0x3E042BA4<br> [0x80003560]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869d and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                         |[0x80000624]:fcvt.s.d t5, t3, dyn<br> [0x80000628]:csrrs a5, fcsr, zero<br> [0x8000062c]:sw t5, 224(ra)<br> [0x80000630]:sw a5, 232(ra)<br> |
|  28|[0x80003568]<br>0x3E042BA4<br> [0x80003570]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869d and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                         |[0x80000654]:fcvt.s.d t5, t3, dyn<br> [0x80000658]:csrrs a5, fcsr, zero<br> [0x8000065c]:sw t5, 240(ra)<br> [0x80000660]:sw a5, 248(ra)<br> |
|  29|[0x80003578]<br>0x3E042BA5<br> [0x80003580]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869d and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                         |[0x80000684]:fcvt.s.d t5, t3, dyn<br> [0x80000688]:csrrs a5, fcsr, zero<br> [0x8000068c]:sw t5, 256(ra)<br> [0x80000690]:sw a5, 264(ra)<br> |
|  30|[0x80003588]<br>0x3E042BA5<br> [0x80003590]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869d and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                         |[0x800006b4]:fcvt.s.d t5, t3, dyn<br> [0x800006b8]:csrrs a5, fcsr, zero<br> [0x800006bc]:sw t5, 272(ra)<br> [0x800006c0]:sw a5, 280(ra)<br> |
|  31|[0x80003598]<br>0x3E042BA5<br> [0x800035a0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                          |[0x800006e4]:fcvt.s.d t5, t3, dyn<br> [0x800006e8]:csrrs a5, fcsr, zero<br> [0x800006ec]:sw t5, 288(ra)<br> [0x800006f0]:sw a5, 296(ra)<br> |
|  32|[0x800035a8]<br>0x3E042BA4<br> [0x800035b0]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869e and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                         |[0x80000714]:fcvt.s.d t5, t3, dyn<br> [0x80000718]:csrrs a5, fcsr, zero<br> [0x8000071c]:sw t5, 304(ra)<br> [0x80000720]:sw a5, 312(ra)<br> |
|  33|[0x800035b8]<br>0x3E042BA4<br> [0x800035c0]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869e and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                         |[0x80000744]:fcvt.s.d t5, t3, dyn<br> [0x80000748]:csrrs a5, fcsr, zero<br> [0x8000074c]:sw t5, 320(ra)<br> [0x80000750]:sw a5, 328(ra)<br> |
|  34|[0x800035c8]<br>0x3E042BA5<br> [0x800035d0]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869e and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                         |[0x80000774]:fcvt.s.d t5, t3, dyn<br> [0x80000778]:csrrs a5, fcsr, zero<br> [0x8000077c]:sw t5, 336(ra)<br> [0x80000780]:sw a5, 344(ra)<br> |
|  35|[0x800035d8]<br>0x3E042BA5<br> [0x800035e0]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869e and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                         |[0x800007a4]:fcvt.s.d t5, t3, dyn<br> [0x800007a8]:csrrs a5, fcsr, zero<br> [0x800007ac]:sw t5, 352(ra)<br> [0x800007b0]:sw a5, 360(ra)<br> |
|  36|[0x800035e8]<br>0x3E042BA5<br> [0x800035f0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                          |[0x800007d4]:fcvt.s.d t5, t3, dyn<br> [0x800007d8]:csrrs a5, fcsr, zero<br> [0x800007dc]:sw t5, 368(ra)<br> [0x800007e0]:sw a5, 376(ra)<br> |
|  37|[0x800035f8]<br>0x3E042BA4<br> [0x80003600]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869f and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                         |[0x80000804]:fcvt.s.d t5, t3, dyn<br> [0x80000808]:csrrs a5, fcsr, zero<br> [0x8000080c]:sw t5, 384(ra)<br> [0x80000810]:sw a5, 392(ra)<br> |
|  38|[0x80003608]<br>0x3E042BA4<br> [0x80003610]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869f and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                         |[0x80000834]:fcvt.s.d t5, t3, dyn<br> [0x80000838]:csrrs a5, fcsr, zero<br> [0x8000083c]:sw t5, 400(ra)<br> [0x80000840]:sw a5, 408(ra)<br> |
|  39|[0x80003618]<br>0x3E042BA5<br> [0x80003620]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                         |[0x80000864]:fcvt.s.d t5, t3, dyn<br> [0x80000868]:csrrs a5, fcsr, zero<br> [0x8000086c]:sw t5, 416(ra)<br> [0x80000870]:sw a5, 424(ra)<br> |
|  40|[0x80003628]<br>0x3E042BA5<br> [0x80003630]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869f and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                         |[0x80000894]:fcvt.s.d t5, t3, dyn<br> [0x80000898]:csrrs a5, fcsr, zero<br> [0x8000089c]:sw t5, 432(ra)<br> [0x800008a0]:sw a5, 440(ra)<br> |
|  41|[0x80003638]<br>0xBE042BA5<br> [0x80003640]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8698 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                          |[0x800008c4]:fcvt.s.d t5, t3, dyn<br> [0x800008c8]:csrrs a5, fcsr, zero<br> [0x800008cc]:sw t5, 448(ra)<br> [0x800008d0]:sw a5, 456(ra)<br> |
|  42|[0x80003648]<br>0xBE042BA4<br> [0x80003650]<br>0x00000021<br> |- fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8698 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                         |[0x800008f4]:fcvt.s.d t5, t3, dyn<br> [0x800008f8]:csrrs a5, fcsr, zero<br> [0x800008fc]:sw t5, 464(ra)<br> [0x80000900]:sw a5, 472(ra)<br> |
|  43|[0x80003658]<br>0xBE042BA5<br> [0x80003660]<br>0x00000041<br> |- fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8698 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                         |[0x80000924]:fcvt.s.d t5, t3, dyn<br> [0x80000928]:csrrs a5, fcsr, zero<br> [0x8000092c]:sw t5, 480(ra)<br> [0x80000930]:sw a5, 488(ra)<br> |
|  44|[0x80003668]<br>0xBE042BA4<br> [0x80003670]<br>0x00000061<br> |- fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8698 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                         |[0x80000954]:fcvt.s.d t5, t3, dyn<br> [0x80000958]:csrrs a5, fcsr, zero<br> [0x8000095c]:sw t5, 496(ra)<br> [0x80000960]:sw a5, 504(ra)<br> |
|  45|[0x80003678]<br>0xBE042BA5<br> [0x80003680]<br>0x00000081<br> |- fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8698 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                         |[0x80000984]:fcvt.s.d t5, t3, dyn<br> [0x80000988]:csrrs a5, fcsr, zero<br> [0x8000098c]:sw t5, 512(ra)<br> [0x80000990]:sw a5, 520(ra)<br> |
|  46|[0x80003688]<br>0xBE042BA5<br> [0x80003690]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8699 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                          |[0x800009b4]:fcvt.s.d t5, t3, dyn<br> [0x800009b8]:csrrs a5, fcsr, zero<br> [0x800009bc]:sw t5, 528(ra)<br> [0x800009c0]:sw a5, 536(ra)<br> |
|  47|[0x80003698]<br>0xBE042BA4<br> [0x800036a0]<br>0x00000021<br> |- fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8699 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                         |[0x800009e4]:fcvt.s.d t5, t3, dyn<br> [0x800009e8]:csrrs a5, fcsr, zero<br> [0x800009ec]:sw t5, 544(ra)<br> [0x800009f0]:sw a5, 552(ra)<br> |
