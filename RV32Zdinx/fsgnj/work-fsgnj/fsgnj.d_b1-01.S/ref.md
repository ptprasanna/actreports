
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x8000d4e0')]      |
| SIG_REGION                | [('0x80011610', '0x80013a40', '2316 words')]      |
| COV_LABELS                | fsgnj.d_b1      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-fsgnj6/fsgnj.d_b1-01.S/ref.S    |
| Total Number of coverpoints| 627     |
| Total Coverpoints Hit     | 627      |
| Total Signature Updates   | 1804      |
| STAT1                     | 452      |
| STAT2                     | 0      |
| STAT3                     | 126     |
| STAT4                     | 1352     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80009554]:fsgnj.d t5, t3, s10
[0x80009558]:csrrs gp, fcsr, zero
[0x8000955c]:sw t5, 912(ra)
[0x80009560]:sw t6, 920(ra)
[0x80009564]:sw t5, 928(ra)
[0x80009568]:sw gp, 936(ra)
[0x8000956c]:lui a6, 1
[0x80009570]:addi a6, a6, 2048
[0x80009574]:add sp, sp, a6
[0x80009578]:lw t3, 464(sp)
[0x8000957c]:sub sp, sp, a6
[0x80009580]:lui a6, 1
[0x80009584]:addi a6, a6, 2048
[0x80009588]:add sp, sp, a6
[0x8000958c]:lw t4, 468(sp)
[0x80009590]:sub sp, sp, a6
[0x80009594]:lui a6, 1
[0x80009598]:addi a6, a6, 2048
[0x8000959c]:add sp, sp, a6
[0x800095a0]:lw s10, 472(sp)
[0x800095a4]:sub sp, sp, a6
[0x800095a8]:lui a6, 1
[0x800095ac]:addi a6, a6, 2048
[0x800095b0]:add sp, sp, a6
[0x800095b4]:lw s11, 476(sp)
[0x800095b8]:sub sp, sp, a6
[0x800095bc]:addi t3, zero, 0
[0x800095c0]:lui t4, 1048448
[0x800095c4]:addi s10, zero, 0
[0x800095c8]:lui s11, 1048448
[0x800095cc]:addi a6, zero, 0
[0x800095d0]:csrrw zero, fcsr, a6
[0x800095d4]:fsgnj.d t5, t3, s10

[0x800095d4]:fsgnj.d t5, t3, s10
[0x800095d8]:csrrs gp, fcsr, zero
[0x800095dc]:sw t5, 944(ra)
[0x800095e0]:sw t6, 952(ra)
[0x800095e4]:sw t5, 960(ra)
[0x800095e8]:sw gp, 968(ra)
[0x800095ec]:lui a6, 1
[0x800095f0]:addi a6, a6, 2048
[0x800095f4]:add sp, sp, a6
[0x800095f8]:lw t3, 480(sp)
[0x800095fc]:sub sp, sp, a6
[0x80009600]:lui a6, 1
[0x80009604]:addi a6, a6, 2048
[0x80009608]:add sp, sp, a6
[0x8000960c]:lw t4, 484(sp)
[0x80009610]:sub sp, sp, a6
[0x80009614]:lui a6, 1
[0x80009618]:addi a6, a6, 2048
[0x8000961c]:add sp, sp, a6
[0x80009620]:lw s10, 488(sp)
[0x80009624]:sub sp, sp, a6
[0x80009628]:lui a6, 1
[0x8000962c]:addi a6, a6, 2048
[0x80009630]:add sp, sp, a6
[0x80009634]:lw s11, 492(sp)
[0x80009638]:sub sp, sp, a6
[0x8000963c]:addi t3, zero, 0
[0x80009640]:lui t4, 1048448
[0x80009644]:addi s10, zero, 1
[0x80009648]:lui s11, 524160
[0x8000964c]:addi a6, zero, 0
[0x80009650]:csrrw zero, fcsr, a6
[0x80009654]:fsgnj.d t5, t3, s10

[0x80009654]:fsgnj.d t5, t3, s10
[0x80009658]:csrrs gp, fcsr, zero
[0x8000965c]:sw t5, 976(ra)
[0x80009660]:sw t6, 984(ra)
[0x80009664]:sw t5, 992(ra)
[0x80009668]:sw gp, 1000(ra)
[0x8000966c]:lui a6, 1
[0x80009670]:addi a6, a6, 2048
[0x80009674]:add sp, sp, a6
[0x80009678]:lw t3, 496(sp)
[0x8000967c]:sub sp, sp, a6
[0x80009680]:lui a6, 1
[0x80009684]:addi a6, a6, 2048
[0x80009688]:add sp, sp, a6
[0x8000968c]:lw t4, 500(sp)
[0x80009690]:sub sp, sp, a6
[0x80009694]:lui a6, 1
[0x80009698]:addi a6, a6, 2048
[0x8000969c]:add sp, sp, a6
[0x800096a0]:lw s10, 504(sp)
[0x800096a4]:sub sp, sp, a6
[0x800096a8]:lui a6, 1
[0x800096ac]:addi a6, a6, 2048
[0x800096b0]:add sp, sp, a6
[0x800096b4]:lw s11, 508(sp)
[0x800096b8]:sub sp, sp, a6
[0x800096bc]:addi t3, zero, 0
[0x800096c0]:lui t4, 1048448
[0x800096c4]:addi s10, zero, 1
[0x800096c8]:lui s11, 1048448
[0x800096cc]:addi a6, zero, 0
[0x800096d0]:csrrw zero, fcsr, a6
[0x800096d4]:fsgnj.d t5, t3, s10

[0x800096d4]:fsgnj.d t5, t3, s10
[0x800096d8]:csrrs gp, fcsr, zero
[0x800096dc]:sw t5, 1008(ra)
[0x800096e0]:sw t6, 1016(ra)
[0x800096e4]:sw t5, 1024(ra)
[0x800096e8]:sw gp, 1032(ra)
[0x800096ec]:lui a6, 1
[0x800096f0]:addi a6, a6, 2048
[0x800096f4]:add sp, sp, a6
[0x800096f8]:lw t3, 512(sp)
[0x800096fc]:sub sp, sp, a6
[0x80009700]:lui a6, 1
[0x80009704]:addi a6, a6, 2048
[0x80009708]:add sp, sp, a6
[0x8000970c]:lw t4, 516(sp)
[0x80009710]:sub sp, sp, a6
[0x80009714]:lui a6, 1
[0x80009718]:addi a6, a6, 2048
[0x8000971c]:add sp, sp, a6
[0x80009720]:lw s10, 520(sp)
[0x80009724]:sub sp, sp, a6
[0x80009728]:lui a6, 1
[0x8000972c]:addi a6, a6, 2048
[0x80009730]:add sp, sp, a6
[0x80009734]:lw s11, 524(sp)
[0x80009738]:sub sp, sp, a6
[0x8000973c]:addi t3, zero, 0
[0x80009740]:lui t4, 1048448
[0x80009744]:addi s10, zero, 1
[0x80009748]:lui s11, 524032
[0x8000974c]:addi a6, zero, 0
[0x80009750]:csrrw zero, fcsr, a6
[0x80009754]:fsgnj.d t5, t3, s10

[0x80009754]:fsgnj.d t5, t3, s10
[0x80009758]:csrrs gp, fcsr, zero
[0x8000975c]:sw t5, 1040(ra)
[0x80009760]:sw t6, 1048(ra)
[0x80009764]:sw t5, 1056(ra)
[0x80009768]:sw gp, 1064(ra)
[0x8000976c]:lui a6, 1
[0x80009770]:addi a6, a6, 2048
[0x80009774]:add sp, sp, a6
[0x80009778]:lw t3, 528(sp)
[0x8000977c]:sub sp, sp, a6
[0x80009780]:lui a6, 1
[0x80009784]:addi a6, a6, 2048
[0x80009788]:add sp, sp, a6
[0x8000978c]:lw t4, 532(sp)
[0x80009790]:sub sp, sp, a6
[0x80009794]:lui a6, 1
[0x80009798]:addi a6, a6, 2048
[0x8000979c]:add sp, sp, a6
[0x800097a0]:lw s10, 536(sp)
[0x800097a4]:sub sp, sp, a6
[0x800097a8]:lui a6, 1
[0x800097ac]:addi a6, a6, 2048
[0x800097b0]:add sp, sp, a6
[0x800097b4]:lw s11, 540(sp)
[0x800097b8]:sub sp, sp, a6
[0x800097bc]:addi t3, zero, 0
[0x800097c0]:lui t4, 1048448
[0x800097c4]:addi s10, zero, 1
[0x800097c8]:lui s11, 1048320
[0x800097cc]:addi a6, zero, 0
[0x800097d0]:csrrw zero, fcsr, a6
[0x800097d4]:fsgnj.d t5, t3, s10

[0x800097d4]:fsgnj.d t5, t3, s10
[0x800097d8]:csrrs gp, fcsr, zero
[0x800097dc]:sw t5, 1072(ra)
[0x800097e0]:sw t6, 1080(ra)
[0x800097e4]:sw t5, 1088(ra)
[0x800097e8]:sw gp, 1096(ra)
[0x800097ec]:lui a6, 1
[0x800097f0]:addi a6, a6, 2048
[0x800097f4]:add sp, sp, a6
[0x800097f8]:lw t3, 544(sp)
[0x800097fc]:sub sp, sp, a6
[0x80009800]:lui a6, 1
[0x80009804]:addi a6, a6, 2048
[0x80009808]:add sp, sp, a6
[0x8000980c]:lw t4, 548(sp)
[0x80009810]:sub sp, sp, a6
[0x80009814]:lui a6, 1
[0x80009818]:addi a6, a6, 2048
[0x8000981c]:add sp, sp, a6
[0x80009820]:lw s10, 552(sp)
[0x80009824]:sub sp, sp, a6
[0x80009828]:lui a6, 1
[0x8000982c]:addi a6, a6, 2048
[0x80009830]:add sp, sp, a6
[0x80009834]:lw s11, 556(sp)
[0x80009838]:sub sp, sp, a6
[0x8000983c]:addi t3, zero, 0
[0x80009840]:lui t4, 1048448
[0x80009844]:addi s10, zero, 0
[0x80009848]:lui s11, 261888
[0x8000984c]:addi a6, zero, 0
[0x80009850]:csrrw zero, fcsr, a6
[0x80009854]:fsgnj.d t5, t3, s10

[0x80009854]:fsgnj.d t5, t3, s10
[0x80009858]:csrrs gp, fcsr, zero
[0x8000985c]:sw t5, 1104(ra)
[0x80009860]:sw t6, 1112(ra)
[0x80009864]:sw t5, 1120(ra)
[0x80009868]:sw gp, 1128(ra)
[0x8000986c]:lui a6, 1
[0x80009870]:addi a6, a6, 2048
[0x80009874]:add sp, sp, a6
[0x80009878]:lw t3, 560(sp)
[0x8000987c]:sub sp, sp, a6
[0x80009880]:lui a6, 1
[0x80009884]:addi a6, a6, 2048
[0x80009888]:add sp, sp, a6
[0x8000988c]:lw t4, 564(sp)
[0x80009890]:sub sp, sp, a6
[0x80009894]:lui a6, 1
[0x80009898]:addi a6, a6, 2048
[0x8000989c]:add sp, sp, a6
[0x800098a0]:lw s10, 568(sp)
[0x800098a4]:sub sp, sp, a6
[0x800098a8]:lui a6, 1
[0x800098ac]:addi a6, a6, 2048
[0x800098b0]:add sp, sp, a6
[0x800098b4]:lw s11, 572(sp)
[0x800098b8]:sub sp, sp, a6
[0x800098bc]:addi t3, zero, 0
[0x800098c0]:lui t4, 1048448
[0x800098c4]:addi s10, zero, 0
[0x800098c8]:lui s11, 784384
[0x800098cc]:addi a6, zero, 0
[0x800098d0]:csrrw zero, fcsr, a6
[0x800098d4]:fsgnj.d t5, t3, s10

[0x800098d4]:fsgnj.d t5, t3, s10
[0x800098d8]:csrrs gp, fcsr, zero
[0x800098dc]:sw t5, 1136(ra)
[0x800098e0]:sw t6, 1144(ra)
[0x800098e4]:sw t5, 1152(ra)
[0x800098e8]:sw gp, 1160(ra)
[0x800098ec]:lui a6, 1
[0x800098f0]:addi a6, a6, 2048
[0x800098f4]:add sp, sp, a6
[0x800098f8]:lw t3, 576(sp)
[0x800098fc]:sub sp, sp, a6
[0x80009900]:lui a6, 1
[0x80009904]:addi a6, a6, 2048
[0x80009908]:add sp, sp, a6
[0x8000990c]:lw t4, 580(sp)
[0x80009910]:sub sp, sp, a6
[0x80009914]:lui a6, 1
[0x80009918]:addi a6, a6, 2048
[0x8000991c]:add sp, sp, a6
[0x80009920]:lw s10, 584(sp)
[0x80009924]:sub sp, sp, a6
[0x80009928]:lui a6, 1
[0x8000992c]:addi a6, a6, 2048
[0x80009930]:add sp, sp, a6
[0x80009934]:lw s11, 588(sp)
[0x80009938]:sub sp, sp, a6
[0x8000993c]:addi t3, zero, 1
[0x80009940]:lui t4, 524160
[0x80009944]:addi s10, zero, 0
[0x80009948]:addi s11, zero, 0
[0x8000994c]:addi a6, zero, 0
[0x80009950]:csrrw zero, fcsr, a6
[0x80009954]:fsgnj.d t5, t3, s10

[0x80009954]:fsgnj.d t5, t3, s10
[0x80009958]:csrrs gp, fcsr, zero
[0x8000995c]:sw t5, 1168(ra)
[0x80009960]:sw t6, 1176(ra)
[0x80009964]:sw t5, 1184(ra)
[0x80009968]:sw gp, 1192(ra)
[0x8000996c]:lui a6, 1
[0x80009970]:addi a6, a6, 2048
[0x80009974]:add sp, sp, a6
[0x80009978]:lw t3, 592(sp)
[0x8000997c]:sub sp, sp, a6
[0x80009980]:lui a6, 1
[0x80009984]:addi a6, a6, 2048
[0x80009988]:add sp, sp, a6
[0x8000998c]:lw t4, 596(sp)
[0x80009990]:sub sp, sp, a6
[0x80009994]:lui a6, 1
[0x80009998]:addi a6, a6, 2048
[0x8000999c]:add sp, sp, a6
[0x800099a0]:lw s10, 600(sp)
[0x800099a4]:sub sp, sp, a6
[0x800099a8]:lui a6, 1
[0x800099ac]:addi a6, a6, 2048
[0x800099b0]:add sp, sp, a6
[0x800099b4]:lw s11, 604(sp)
[0x800099b8]:sub sp, sp, a6
[0x800099bc]:addi t3, zero, 1
[0x800099c0]:lui t4, 524160
[0x800099c4]:addi s10, zero, 0
[0x800099c8]:lui s11, 524288
[0x800099cc]:addi a6, zero, 0
[0x800099d0]:csrrw zero, fcsr, a6
[0x800099d4]:fsgnj.d t5, t3, s10

[0x800099d4]:fsgnj.d t5, t3, s10
[0x800099d8]:csrrs gp, fcsr, zero
[0x800099dc]:sw t5, 1200(ra)
[0x800099e0]:sw t6, 1208(ra)
[0x800099e4]:sw t5, 1216(ra)
[0x800099e8]:sw gp, 1224(ra)
[0x800099ec]:lui a6, 1
[0x800099f0]:addi a6, a6, 2048
[0x800099f4]:add sp, sp, a6
[0x800099f8]:lw t3, 608(sp)
[0x800099fc]:sub sp, sp, a6
[0x80009a00]:lui a6, 1
[0x80009a04]:addi a6, a6, 2048
[0x80009a08]:add sp, sp, a6
[0x80009a0c]:lw t4, 612(sp)
[0x80009a10]:sub sp, sp, a6
[0x80009a14]:lui a6, 1
[0x80009a18]:addi a6, a6, 2048
[0x80009a1c]:add sp, sp, a6
[0x80009a20]:lw s10, 616(sp)
[0x80009a24]:sub sp, sp, a6
[0x80009a28]:lui a6, 1
[0x80009a2c]:addi a6, a6, 2048
[0x80009a30]:add sp, sp, a6
[0x80009a34]:lw s11, 620(sp)
[0x80009a38]:sub sp, sp, a6
[0x80009a3c]:addi t3, zero, 1
[0x80009a40]:lui t4, 524160
[0x80009a44]:addi s10, zero, 1
[0x80009a48]:addi s11, zero, 0
[0x80009a4c]:addi a6, zero, 0
[0x80009a50]:csrrw zero, fcsr, a6
[0x80009a54]:fsgnj.d t5, t3, s10

[0x80009a54]:fsgnj.d t5, t3, s10
[0x80009a58]:csrrs gp, fcsr, zero
[0x80009a5c]:sw t5, 1232(ra)
[0x80009a60]:sw t6, 1240(ra)
[0x80009a64]:sw t5, 1248(ra)
[0x80009a68]:sw gp, 1256(ra)
[0x80009a6c]:lui a6, 1
[0x80009a70]:addi a6, a6, 2048
[0x80009a74]:add sp, sp, a6
[0x80009a78]:lw t3, 624(sp)
[0x80009a7c]:sub sp, sp, a6
[0x80009a80]:lui a6, 1
[0x80009a84]:addi a6, a6, 2048
[0x80009a88]:add sp, sp, a6
[0x80009a8c]:lw t4, 628(sp)
[0x80009a90]:sub sp, sp, a6
[0x80009a94]:lui a6, 1
[0x80009a98]:addi a6, a6, 2048
[0x80009a9c]:add sp, sp, a6
[0x80009aa0]:lw s10, 632(sp)
[0x80009aa4]:sub sp, sp, a6
[0x80009aa8]:lui a6, 1
[0x80009aac]:addi a6, a6, 2048
[0x80009ab0]:add sp, sp, a6
[0x80009ab4]:lw s11, 636(sp)
[0x80009ab8]:sub sp, sp, a6
[0x80009abc]:addi t3, zero, 1
[0x80009ac0]:lui t4, 524160
[0x80009ac4]:addi s10, zero, 1
[0x80009ac8]:lui s11, 524288
[0x80009acc]:addi a6, zero, 0
[0x80009ad0]:csrrw zero, fcsr, a6
[0x80009ad4]:fsgnj.d t5, t3, s10

[0x80009ad4]:fsgnj.d t5, t3, s10
[0x80009ad8]:csrrs gp, fcsr, zero
[0x80009adc]:sw t5, 1264(ra)
[0x80009ae0]:sw t6, 1272(ra)
[0x80009ae4]:sw t5, 1280(ra)
[0x80009ae8]:sw gp, 1288(ra)
[0x80009aec]:lui a6, 1
[0x80009af0]:addi a6, a6, 2048
[0x80009af4]:add sp, sp, a6
[0x80009af8]:lw t3, 640(sp)
[0x80009afc]:sub sp, sp, a6
[0x80009b00]:lui a6, 1
[0x80009b04]:addi a6, a6, 2048
[0x80009b08]:add sp, sp, a6
[0x80009b0c]:lw t4, 644(sp)
[0x80009b10]:sub sp, sp, a6
[0x80009b14]:lui a6, 1
[0x80009b18]:addi a6, a6, 2048
[0x80009b1c]:add sp, sp, a6
[0x80009b20]:lw s10, 648(sp)
[0x80009b24]:sub sp, sp, a6
[0x80009b28]:lui a6, 1
[0x80009b2c]:addi a6, a6, 2048
[0x80009b30]:add sp, sp, a6
[0x80009b34]:lw s11, 652(sp)
[0x80009b38]:sub sp, sp, a6
[0x80009b3c]:addi t3, zero, 1
[0x80009b40]:lui t4, 524160
[0x80009b44]:addi s10, zero, 2
[0x80009b48]:addi s11, zero, 0
[0x80009b4c]:addi a6, zero, 0
[0x80009b50]:csrrw zero, fcsr, a6
[0x80009b54]:fsgnj.d t5, t3, s10

[0x80009b54]:fsgnj.d t5, t3, s10
[0x80009b58]:csrrs gp, fcsr, zero
[0x80009b5c]:sw t5, 1296(ra)
[0x80009b60]:sw t6, 1304(ra)
[0x80009b64]:sw t5, 1312(ra)
[0x80009b68]:sw gp, 1320(ra)
[0x80009b6c]:lui a6, 1
[0x80009b70]:addi a6, a6, 2048
[0x80009b74]:add sp, sp, a6
[0x80009b78]:lw t3, 656(sp)
[0x80009b7c]:sub sp, sp, a6
[0x80009b80]:lui a6, 1
[0x80009b84]:addi a6, a6, 2048
[0x80009b88]:add sp, sp, a6
[0x80009b8c]:lw t4, 660(sp)
[0x80009b90]:sub sp, sp, a6
[0x80009b94]:lui a6, 1
[0x80009b98]:addi a6, a6, 2048
[0x80009b9c]:add sp, sp, a6
[0x80009ba0]:lw s10, 664(sp)
[0x80009ba4]:sub sp, sp, a6
[0x80009ba8]:lui a6, 1
[0x80009bac]:addi a6, a6, 2048
[0x80009bb0]:add sp, sp, a6
[0x80009bb4]:lw s11, 668(sp)
[0x80009bb8]:sub sp, sp, a6
[0x80009bbc]:addi t3, zero, 1
[0x80009bc0]:lui t4, 524160
[0x80009bc4]:addi s10, zero, 2
[0x80009bc8]:lui s11, 524288
[0x80009bcc]:addi a6, zero, 0
[0x80009bd0]:csrrw zero, fcsr, a6
[0x80009bd4]:fsgnj.d t5, t3, s10

[0x80009bd4]:fsgnj.d t5, t3, s10
[0x80009bd8]:csrrs gp, fcsr, zero
[0x80009bdc]:sw t5, 1328(ra)
[0x80009be0]:sw t6, 1336(ra)
[0x80009be4]:sw t5, 1344(ra)
[0x80009be8]:sw gp, 1352(ra)
[0x80009bec]:lui a6, 1
[0x80009bf0]:addi a6, a6, 2048
[0x80009bf4]:add sp, sp, a6
[0x80009bf8]:lw t3, 672(sp)
[0x80009bfc]:sub sp, sp, a6
[0x80009c00]:lui a6, 1
[0x80009c04]:addi a6, a6, 2048
[0x80009c08]:add sp, sp, a6
[0x80009c0c]:lw t4, 676(sp)
[0x80009c10]:sub sp, sp, a6
[0x80009c14]:lui a6, 1
[0x80009c18]:addi a6, a6, 2048
[0x80009c1c]:add sp, sp, a6
[0x80009c20]:lw s10, 680(sp)
[0x80009c24]:sub sp, sp, a6
[0x80009c28]:lui a6, 1
[0x80009c2c]:addi a6, a6, 2048
[0x80009c30]:add sp, sp, a6
[0x80009c34]:lw s11, 684(sp)
[0x80009c38]:sub sp, sp, a6
[0x80009c3c]:addi t3, zero, 1
[0x80009c40]:lui t4, 524160
[0x80009c44]:addi s10, zero, 4095
[0x80009c48]:lui s11, 256
[0x80009c4c]:addi s11, s11, 4095
[0x80009c50]:addi a6, zero, 0
[0x80009c54]:csrrw zero, fcsr, a6
[0x80009c58]:fsgnj.d t5, t3, s10

[0x80009c58]:fsgnj.d t5, t3, s10
[0x80009c5c]:csrrs gp, fcsr, zero
[0x80009c60]:sw t5, 1360(ra)
[0x80009c64]:sw t6, 1368(ra)
[0x80009c68]:sw t5, 1376(ra)
[0x80009c6c]:sw gp, 1384(ra)
[0x80009c70]:lui a6, 1
[0x80009c74]:addi a6, a6, 2048
[0x80009c78]:add sp, sp, a6
[0x80009c7c]:lw t3, 688(sp)
[0x80009c80]:sub sp, sp, a6
[0x80009c84]:lui a6, 1
[0x80009c88]:addi a6, a6, 2048
[0x80009c8c]:add sp, sp, a6
[0x80009c90]:lw t4, 692(sp)
[0x80009c94]:sub sp, sp, a6
[0x80009c98]:lui a6, 1
[0x80009c9c]:addi a6, a6, 2048
[0x80009ca0]:add sp, sp, a6
[0x80009ca4]:lw s10, 696(sp)
[0x80009ca8]:sub sp, sp, a6
[0x80009cac]:lui a6, 1
[0x80009cb0]:addi a6, a6, 2048
[0x80009cb4]:add sp, sp, a6
[0x80009cb8]:lw s11, 700(sp)
[0x80009cbc]:sub sp, sp, a6
[0x80009cc0]:addi t3, zero, 1
[0x80009cc4]:lui t4, 524160
[0x80009cc8]:addi s10, zero, 4095
[0x80009ccc]:lui s11, 524544
[0x80009cd0]:addi s11, s11, 4095
[0x80009cd4]:addi a6, zero, 0
[0x80009cd8]:csrrw zero, fcsr, a6
[0x80009cdc]:fsgnj.d t5, t3, s10

[0x80009cdc]:fsgnj.d t5, t3, s10
[0x80009ce0]:csrrs gp, fcsr, zero
[0x80009ce4]:sw t5, 1392(ra)
[0x80009ce8]:sw t6, 1400(ra)
[0x80009cec]:sw t5, 1408(ra)
[0x80009cf0]:sw gp, 1416(ra)
[0x80009cf4]:lui a6, 1
[0x80009cf8]:addi a6, a6, 2048
[0x80009cfc]:add sp, sp, a6
[0x80009d00]:lw t3, 704(sp)
[0x80009d04]:sub sp, sp, a6
[0x80009d08]:lui a6, 1
[0x80009d0c]:addi a6, a6, 2048
[0x80009d10]:add sp, sp, a6
[0x80009d14]:lw t4, 708(sp)
[0x80009d18]:sub sp, sp, a6
[0x80009d1c]:lui a6, 1
[0x80009d20]:addi a6, a6, 2048
[0x80009d24]:add sp, sp, a6
[0x80009d28]:lw s10, 712(sp)
[0x80009d2c]:sub sp, sp, a6
[0x80009d30]:lui a6, 1
[0x80009d34]:addi a6, a6, 2048
[0x80009d38]:add sp, sp, a6
[0x80009d3c]:lw s11, 716(sp)
[0x80009d40]:sub sp, sp, a6
[0x80009d44]:addi t3, zero, 1
[0x80009d48]:lui t4, 524160
[0x80009d4c]:addi s10, zero, 0
[0x80009d50]:lui s11, 256
[0x80009d54]:addi a6, zero, 0
[0x80009d58]:csrrw zero, fcsr, a6
[0x80009d5c]:fsgnj.d t5, t3, s10

[0x80009d5c]:fsgnj.d t5, t3, s10
[0x80009d60]:csrrs gp, fcsr, zero
[0x80009d64]:sw t5, 1424(ra)
[0x80009d68]:sw t6, 1432(ra)
[0x80009d6c]:sw t5, 1440(ra)
[0x80009d70]:sw gp, 1448(ra)
[0x80009d74]:lui a6, 1
[0x80009d78]:addi a6, a6, 2048
[0x80009d7c]:add sp, sp, a6
[0x80009d80]:lw t3, 720(sp)
[0x80009d84]:sub sp, sp, a6
[0x80009d88]:lui a6, 1
[0x80009d8c]:addi a6, a6, 2048
[0x80009d90]:add sp, sp, a6
[0x80009d94]:lw t4, 724(sp)
[0x80009d98]:sub sp, sp, a6
[0x80009d9c]:lui a6, 1
[0x80009da0]:addi a6, a6, 2048
[0x80009da4]:add sp, sp, a6
[0x80009da8]:lw s10, 728(sp)
[0x80009dac]:sub sp, sp, a6
[0x80009db0]:lui a6, 1
[0x80009db4]:addi a6, a6, 2048
[0x80009db8]:add sp, sp, a6
[0x80009dbc]:lw s11, 732(sp)
[0x80009dc0]:sub sp, sp, a6
[0x80009dc4]:addi t3, zero, 1
[0x80009dc8]:lui t4, 524160
[0x80009dcc]:addi s10, zero, 0
[0x80009dd0]:lui s11, 524544
[0x80009dd4]:addi a6, zero, 0
[0x80009dd8]:csrrw zero, fcsr, a6
[0x80009ddc]:fsgnj.d t5, t3, s10

[0x80009ddc]:fsgnj.d t5, t3, s10
[0x80009de0]:csrrs gp, fcsr, zero
[0x80009de4]:sw t5, 1456(ra)
[0x80009de8]:sw t6, 1464(ra)
[0x80009dec]:sw t5, 1472(ra)
[0x80009df0]:sw gp, 1480(ra)
[0x80009df4]:lui a6, 1
[0x80009df8]:addi a6, a6, 2048
[0x80009dfc]:add sp, sp, a6
[0x80009e00]:lw t3, 736(sp)
[0x80009e04]:sub sp, sp, a6
[0x80009e08]:lui a6, 1
[0x80009e0c]:addi a6, a6, 2048
[0x80009e10]:add sp, sp, a6
[0x80009e14]:lw t4, 740(sp)
[0x80009e18]:sub sp, sp, a6
[0x80009e1c]:lui a6, 1
[0x80009e20]:addi a6, a6, 2048
[0x80009e24]:add sp, sp, a6
[0x80009e28]:lw s10, 744(sp)
[0x80009e2c]:sub sp, sp, a6
[0x80009e30]:lui a6, 1
[0x80009e34]:addi a6, a6, 2048
[0x80009e38]:add sp, sp, a6
[0x80009e3c]:lw s11, 748(sp)
[0x80009e40]:sub sp, sp, a6
[0x80009e44]:addi t3, zero, 1
[0x80009e48]:lui t4, 524160
[0x80009e4c]:addi s10, zero, 2
[0x80009e50]:lui s11, 256
[0x80009e54]:addi a6, zero, 0
[0x80009e58]:csrrw zero, fcsr, a6
[0x80009e5c]:fsgnj.d t5, t3, s10

[0x80009e5c]:fsgnj.d t5, t3, s10
[0x80009e60]:csrrs gp, fcsr, zero
[0x80009e64]:sw t5, 1488(ra)
[0x80009e68]:sw t6, 1496(ra)
[0x80009e6c]:sw t5, 1504(ra)
[0x80009e70]:sw gp, 1512(ra)
[0x80009e74]:lui a6, 1
[0x80009e78]:addi a6, a6, 2048
[0x80009e7c]:add sp, sp, a6
[0x80009e80]:lw t3, 752(sp)
[0x80009e84]:sub sp, sp, a6
[0x80009e88]:lui a6, 1
[0x80009e8c]:addi a6, a6, 2048
[0x80009e90]:add sp, sp, a6
[0x80009e94]:lw t4, 756(sp)
[0x80009e98]:sub sp, sp, a6
[0x80009e9c]:lui a6, 1
[0x80009ea0]:addi a6, a6, 2048
[0x80009ea4]:add sp, sp, a6
[0x80009ea8]:lw s10, 760(sp)
[0x80009eac]:sub sp, sp, a6
[0x80009eb0]:lui a6, 1
[0x80009eb4]:addi a6, a6, 2048
[0x80009eb8]:add sp, sp, a6
[0x80009ebc]:lw s11, 764(sp)
[0x80009ec0]:sub sp, sp, a6
[0x80009ec4]:addi t3, zero, 1
[0x80009ec8]:lui t4, 524160
[0x80009ecc]:addi s10, zero, 2
[0x80009ed0]:lui s11, 524544
[0x80009ed4]:addi a6, zero, 0
[0x80009ed8]:csrrw zero, fcsr, a6
[0x80009edc]:fsgnj.d t5, t3, s10

[0x80009edc]:fsgnj.d t5, t3, s10
[0x80009ee0]:csrrs gp, fcsr, zero
[0x80009ee4]:sw t5, 1520(ra)
[0x80009ee8]:sw t6, 1528(ra)
[0x80009eec]:sw t5, 1536(ra)
[0x80009ef0]:sw gp, 1544(ra)
[0x80009ef4]:lui a6, 1
[0x80009ef8]:addi a6, a6, 2048
[0x80009efc]:add sp, sp, a6
[0x80009f00]:lw t3, 768(sp)
[0x80009f04]:sub sp, sp, a6
[0x80009f08]:lui a6, 1
[0x80009f0c]:addi a6, a6, 2048
[0x80009f10]:add sp, sp, a6
[0x80009f14]:lw t4, 772(sp)
[0x80009f18]:sub sp, sp, a6
[0x80009f1c]:lui a6, 1
[0x80009f20]:addi a6, a6, 2048
[0x80009f24]:add sp, sp, a6
[0x80009f28]:lw s10, 776(sp)
[0x80009f2c]:sub sp, sp, a6
[0x80009f30]:lui a6, 1
[0x80009f34]:addi a6, a6, 2048
[0x80009f38]:add sp, sp, a6
[0x80009f3c]:lw s11, 780(sp)
[0x80009f40]:sub sp, sp, a6
[0x80009f44]:addi t3, zero, 1
[0x80009f48]:lui t4, 524160
[0x80009f4c]:addi s10, zero, 4095
[0x80009f50]:lui s11, 524032
[0x80009f54]:addi s11, s11, 4095
[0x80009f58]:addi a6, zero, 0
[0x80009f5c]:csrrw zero, fcsr, a6
[0x80009f60]:fsgnj.d t5, t3, s10

[0x80009f60]:fsgnj.d t5, t3, s10
[0x80009f64]:csrrs gp, fcsr, zero
[0x80009f68]:sw t5, 1552(ra)
[0x80009f6c]:sw t6, 1560(ra)
[0x80009f70]:sw t5, 1568(ra)
[0x80009f74]:sw gp, 1576(ra)
[0x80009f78]:lui a6, 1
[0x80009f7c]:addi a6, a6, 2048
[0x80009f80]:add sp, sp, a6
[0x80009f84]:lw t3, 784(sp)
[0x80009f88]:sub sp, sp, a6
[0x80009f8c]:lui a6, 1
[0x80009f90]:addi a6, a6, 2048
[0x80009f94]:add sp, sp, a6
[0x80009f98]:lw t4, 788(sp)
[0x80009f9c]:sub sp, sp, a6
[0x80009fa0]:lui a6, 1
[0x80009fa4]:addi a6, a6, 2048
[0x80009fa8]:add sp, sp, a6
[0x80009fac]:lw s10, 792(sp)
[0x80009fb0]:sub sp, sp, a6
[0x80009fb4]:lui a6, 1
[0x80009fb8]:addi a6, a6, 2048
[0x80009fbc]:add sp, sp, a6
[0x80009fc0]:lw s11, 796(sp)
[0x80009fc4]:sub sp, sp, a6
[0x80009fc8]:addi t3, zero, 1
[0x80009fcc]:lui t4, 524160
[0x80009fd0]:addi s10, zero, 4095
[0x80009fd4]:lui s11, 1048320
[0x80009fd8]:addi s11, s11, 4095
[0x80009fdc]:addi a6, zero, 0
[0x80009fe0]:csrrw zero, fcsr, a6
[0x80009fe4]:fsgnj.d t5, t3, s10

[0x80009fe4]:fsgnj.d t5, t3, s10
[0x80009fe8]:csrrs gp, fcsr, zero
[0x80009fec]:sw t5, 1584(ra)
[0x80009ff0]:sw t6, 1592(ra)
[0x80009ff4]:sw t5, 1600(ra)
[0x80009ff8]:sw gp, 1608(ra)
[0x80009ffc]:lui a6, 1
[0x8000a000]:addi a6, a6, 2048
[0x8000a004]:add sp, sp, a6
[0x8000a008]:lw t3, 800(sp)
[0x8000a00c]:sub sp, sp, a6
[0x8000a010]:lui a6, 1
[0x8000a014]:addi a6, a6, 2048
[0x8000a018]:add sp, sp, a6
[0x8000a01c]:lw t4, 804(sp)
[0x8000a020]:sub sp, sp, a6
[0x8000a024]:lui a6, 1
[0x8000a028]:addi a6, a6, 2048
[0x8000a02c]:add sp, sp, a6
[0x8000a030]:lw s10, 808(sp)
[0x8000a034]:sub sp, sp, a6
[0x8000a038]:lui a6, 1
[0x8000a03c]:addi a6, a6, 2048
[0x8000a040]:add sp, sp, a6
[0x8000a044]:lw s11, 812(sp)
[0x8000a048]:sub sp, sp, a6
[0x8000a04c]:addi t3, zero, 1
[0x8000a050]:lui t4, 524160
[0x8000a054]:addi s10, zero, 0
[0x8000a058]:lui s11, 524032
[0x8000a05c]:addi a6, zero, 0
[0x8000a060]:csrrw zero, fcsr, a6
[0x8000a064]:fsgnj.d t5, t3, s10

[0x8000a064]:fsgnj.d t5, t3, s10
[0x8000a068]:csrrs gp, fcsr, zero
[0x8000a06c]:sw t5, 1616(ra)
[0x8000a070]:sw t6, 1624(ra)
[0x8000a074]:sw t5, 1632(ra)
[0x8000a078]:sw gp, 1640(ra)
[0x8000a07c]:lui a6, 1
[0x8000a080]:addi a6, a6, 2048
[0x8000a084]:add sp, sp, a6
[0x8000a088]:lw t3, 816(sp)
[0x8000a08c]:sub sp, sp, a6
[0x8000a090]:lui a6, 1
[0x8000a094]:addi a6, a6, 2048
[0x8000a098]:add sp, sp, a6
[0x8000a09c]:lw t4, 820(sp)
[0x8000a0a0]:sub sp, sp, a6
[0x8000a0a4]:lui a6, 1
[0x8000a0a8]:addi a6, a6, 2048
[0x8000a0ac]:add sp, sp, a6
[0x8000a0b0]:lw s10, 824(sp)
[0x8000a0b4]:sub sp, sp, a6
[0x8000a0b8]:lui a6, 1
[0x8000a0bc]:addi a6, a6, 2048
[0x8000a0c0]:add sp, sp, a6
[0x8000a0c4]:lw s11, 828(sp)
[0x8000a0c8]:sub sp, sp, a6
[0x8000a0cc]:addi t3, zero, 1
[0x8000a0d0]:lui t4, 524160
[0x8000a0d4]:addi s10, zero, 0
[0x8000a0d8]:lui s11, 1048320
[0x8000a0dc]:addi a6, zero, 0
[0x8000a0e0]:csrrw zero, fcsr, a6
[0x8000a0e4]:fsgnj.d t5, t3, s10

[0x8000a0e4]:fsgnj.d t5, t3, s10
[0x8000a0e8]:csrrs gp, fcsr, zero
[0x8000a0ec]:sw t5, 1648(ra)
[0x8000a0f0]:sw t6, 1656(ra)
[0x8000a0f4]:sw t5, 1664(ra)
[0x8000a0f8]:sw gp, 1672(ra)
[0x8000a0fc]:lui a6, 1
[0x8000a100]:addi a6, a6, 2048
[0x8000a104]:add sp, sp, a6
[0x8000a108]:lw t3, 832(sp)
[0x8000a10c]:sub sp, sp, a6
[0x8000a110]:lui a6, 1
[0x8000a114]:addi a6, a6, 2048
[0x8000a118]:add sp, sp, a6
[0x8000a11c]:lw t4, 836(sp)
[0x8000a120]:sub sp, sp, a6
[0x8000a124]:lui a6, 1
[0x8000a128]:addi a6, a6, 2048
[0x8000a12c]:add sp, sp, a6
[0x8000a130]:lw s10, 840(sp)
[0x8000a134]:sub sp, sp, a6
[0x8000a138]:lui a6, 1
[0x8000a13c]:addi a6, a6, 2048
[0x8000a140]:add sp, sp, a6
[0x8000a144]:lw s11, 844(sp)
[0x8000a148]:sub sp, sp, a6
[0x8000a14c]:addi t3, zero, 1
[0x8000a150]:lui t4, 524160
[0x8000a154]:addi s10, zero, 0
[0x8000a158]:lui s11, 524160
[0x8000a15c]:addi a6, zero, 0
[0x8000a160]:csrrw zero, fcsr, a6
[0x8000a164]:fsgnj.d t5, t3, s10

[0x8000a164]:fsgnj.d t5, t3, s10
[0x8000a168]:csrrs gp, fcsr, zero
[0x8000a16c]:sw t5, 1680(ra)
[0x8000a170]:sw t6, 1688(ra)
[0x8000a174]:sw t5, 1696(ra)
[0x8000a178]:sw gp, 1704(ra)
[0x8000a17c]:lui a6, 1
[0x8000a180]:addi a6, a6, 2048
[0x8000a184]:add sp, sp, a6
[0x8000a188]:lw t3, 848(sp)
[0x8000a18c]:sub sp, sp, a6
[0x8000a190]:lui a6, 1
[0x8000a194]:addi a6, a6, 2048
[0x8000a198]:add sp, sp, a6
[0x8000a19c]:lw t4, 852(sp)
[0x8000a1a0]:sub sp, sp, a6
[0x8000a1a4]:lui a6, 1
[0x8000a1a8]:addi a6, a6, 2048
[0x8000a1ac]:add sp, sp, a6
[0x8000a1b0]:lw s10, 856(sp)
[0x8000a1b4]:sub sp, sp, a6
[0x8000a1b8]:lui a6, 1
[0x8000a1bc]:addi a6, a6, 2048
[0x8000a1c0]:add sp, sp, a6
[0x8000a1c4]:lw s11, 860(sp)
[0x8000a1c8]:sub sp, sp, a6
[0x8000a1cc]:addi t3, zero, 1
[0x8000a1d0]:lui t4, 524160
[0x8000a1d4]:addi s10, zero, 0
[0x8000a1d8]:lui s11, 1048448
[0x8000a1dc]:addi a6, zero, 0
[0x8000a1e0]:csrrw zero, fcsr, a6
[0x8000a1e4]:fsgnj.d t5, t3, s10

[0x8000a1e4]:fsgnj.d t5, t3, s10
[0x8000a1e8]:csrrs gp, fcsr, zero
[0x8000a1ec]:sw t5, 1712(ra)
[0x8000a1f0]:sw t6, 1720(ra)
[0x8000a1f4]:sw t5, 1728(ra)
[0x8000a1f8]:sw gp, 1736(ra)
[0x8000a1fc]:lui a6, 1
[0x8000a200]:addi a6, a6, 2048
[0x8000a204]:add sp, sp, a6
[0x8000a208]:lw t3, 864(sp)
[0x8000a20c]:sub sp, sp, a6
[0x8000a210]:lui a6, 1
[0x8000a214]:addi a6, a6, 2048
[0x8000a218]:add sp, sp, a6
[0x8000a21c]:lw t4, 868(sp)
[0x8000a220]:sub sp, sp, a6
[0x8000a224]:lui a6, 1
[0x8000a228]:addi a6, a6, 2048
[0x8000a22c]:add sp, sp, a6
[0x8000a230]:lw s10, 872(sp)
[0x8000a234]:sub sp, sp, a6
[0x8000a238]:lui a6, 1
[0x8000a23c]:addi a6, a6, 2048
[0x8000a240]:add sp, sp, a6
[0x8000a244]:lw s11, 876(sp)
[0x8000a248]:sub sp, sp, a6
[0x8000a24c]:addi t3, zero, 1
[0x8000a250]:lui t4, 524160
[0x8000a254]:addi s10, zero, 1
[0x8000a258]:lui s11, 524160
[0x8000a25c]:addi a6, zero, 0
[0x8000a260]:csrrw zero, fcsr, a6
[0x8000a264]:fsgnj.d t5, t3, s10

[0x8000a264]:fsgnj.d t5, t3, s10
[0x8000a268]:csrrs gp, fcsr, zero
[0x8000a26c]:sw t5, 1744(ra)
[0x8000a270]:sw t6, 1752(ra)
[0x8000a274]:sw t5, 1760(ra)
[0x8000a278]:sw gp, 1768(ra)
[0x8000a27c]:lui a6, 1
[0x8000a280]:addi a6, a6, 2048
[0x8000a284]:add sp, sp, a6
[0x8000a288]:lw t3, 880(sp)
[0x8000a28c]:sub sp, sp, a6
[0x8000a290]:lui a6, 1
[0x8000a294]:addi a6, a6, 2048
[0x8000a298]:add sp, sp, a6
[0x8000a29c]:lw t4, 884(sp)
[0x8000a2a0]:sub sp, sp, a6
[0x8000a2a4]:lui a6, 1
[0x8000a2a8]:addi a6, a6, 2048
[0x8000a2ac]:add sp, sp, a6
[0x8000a2b0]:lw s10, 888(sp)
[0x8000a2b4]:sub sp, sp, a6
[0x8000a2b8]:lui a6, 1
[0x8000a2bc]:addi a6, a6, 2048
[0x8000a2c0]:add sp, sp, a6
[0x8000a2c4]:lw s11, 892(sp)
[0x8000a2c8]:sub sp, sp, a6
[0x8000a2cc]:addi t3, zero, 1
[0x8000a2d0]:lui t4, 524160
[0x8000a2d4]:addi s10, zero, 1
[0x8000a2d8]:lui s11, 1048448
[0x8000a2dc]:addi a6, zero, 0
[0x8000a2e0]:csrrw zero, fcsr, a6
[0x8000a2e4]:fsgnj.d t5, t3, s10

[0x8000a2e4]:fsgnj.d t5, t3, s10
[0x8000a2e8]:csrrs gp, fcsr, zero
[0x8000a2ec]:sw t5, 1776(ra)
[0x8000a2f0]:sw t6, 1784(ra)
[0x8000a2f4]:sw t5, 1792(ra)
[0x8000a2f8]:sw gp, 1800(ra)
[0x8000a2fc]:lui a6, 1
[0x8000a300]:addi a6, a6, 2048
[0x8000a304]:add sp, sp, a6
[0x8000a308]:lw t3, 896(sp)
[0x8000a30c]:sub sp, sp, a6
[0x8000a310]:lui a6, 1
[0x8000a314]:addi a6, a6, 2048
[0x8000a318]:add sp, sp, a6
[0x8000a31c]:lw t4, 900(sp)
[0x8000a320]:sub sp, sp, a6
[0x8000a324]:lui a6, 1
[0x8000a328]:addi a6, a6, 2048
[0x8000a32c]:add sp, sp, a6
[0x8000a330]:lw s10, 904(sp)
[0x8000a334]:sub sp, sp, a6
[0x8000a338]:lui a6, 1
[0x8000a33c]:addi a6, a6, 2048
[0x8000a340]:add sp, sp, a6
[0x8000a344]:lw s11, 908(sp)
[0x8000a348]:sub sp, sp, a6
[0x8000a34c]:addi t3, zero, 1
[0x8000a350]:lui t4, 524160
[0x8000a354]:addi s10, zero, 1
[0x8000a358]:lui s11, 524032
[0x8000a35c]:addi a6, zero, 0
[0x8000a360]:csrrw zero, fcsr, a6
[0x8000a364]:fsgnj.d t5, t3, s10

[0x8000a364]:fsgnj.d t5, t3, s10
[0x8000a368]:csrrs gp, fcsr, zero
[0x8000a36c]:sw t5, 1808(ra)
[0x8000a370]:sw t6, 1816(ra)
[0x8000a374]:sw t5, 1824(ra)
[0x8000a378]:sw gp, 1832(ra)
[0x8000a37c]:lui a6, 1
[0x8000a380]:addi a6, a6, 2048
[0x8000a384]:add sp, sp, a6
[0x8000a388]:lw t3, 912(sp)
[0x8000a38c]:sub sp, sp, a6
[0x8000a390]:lui a6, 1
[0x8000a394]:addi a6, a6, 2048
[0x8000a398]:add sp, sp, a6
[0x8000a39c]:lw t4, 916(sp)
[0x8000a3a0]:sub sp, sp, a6
[0x8000a3a4]:lui a6, 1
[0x8000a3a8]:addi a6, a6, 2048
[0x8000a3ac]:add sp, sp, a6
[0x8000a3b0]:lw s10, 920(sp)
[0x8000a3b4]:sub sp, sp, a6
[0x8000a3b8]:lui a6, 1
[0x8000a3bc]:addi a6, a6, 2048
[0x8000a3c0]:add sp, sp, a6
[0x8000a3c4]:lw s11, 924(sp)
[0x8000a3c8]:sub sp, sp, a6
[0x8000a3cc]:addi t3, zero, 1
[0x8000a3d0]:lui t4, 524160
[0x8000a3d4]:addi s10, zero, 1
[0x8000a3d8]:lui s11, 1048320
[0x8000a3dc]:addi a6, zero, 0
[0x8000a3e0]:csrrw zero, fcsr, a6
[0x8000a3e4]:fsgnj.d t5, t3, s10

[0x8000a3e4]:fsgnj.d t5, t3, s10
[0x8000a3e8]:csrrs gp, fcsr, zero
[0x8000a3ec]:sw t5, 1840(ra)
[0x8000a3f0]:sw t6, 1848(ra)
[0x8000a3f4]:sw t5, 1856(ra)
[0x8000a3f8]:sw gp, 1864(ra)
[0x8000a3fc]:lui a6, 1
[0x8000a400]:addi a6, a6, 2048
[0x8000a404]:add sp, sp, a6
[0x8000a408]:lw t3, 928(sp)
[0x8000a40c]:sub sp, sp, a6
[0x8000a410]:lui a6, 1
[0x8000a414]:addi a6, a6, 2048
[0x8000a418]:add sp, sp, a6
[0x8000a41c]:lw t4, 932(sp)
[0x8000a420]:sub sp, sp, a6
[0x8000a424]:lui a6, 1
[0x8000a428]:addi a6, a6, 2048
[0x8000a42c]:add sp, sp, a6
[0x8000a430]:lw s10, 936(sp)
[0x8000a434]:sub sp, sp, a6
[0x8000a438]:lui a6, 1
[0x8000a43c]:addi a6, a6, 2048
[0x8000a440]:add sp, sp, a6
[0x8000a444]:lw s11, 940(sp)
[0x8000a448]:sub sp, sp, a6
[0x8000a44c]:addi t3, zero, 1
[0x8000a450]:lui t4, 524160
[0x8000a454]:addi s10, zero, 0
[0x8000a458]:lui s11, 261888
[0x8000a45c]:addi a6, zero, 0
[0x8000a460]:csrrw zero, fcsr, a6
[0x8000a464]:fsgnj.d t5, t3, s10

[0x8000a464]:fsgnj.d t5, t3, s10
[0x8000a468]:csrrs gp, fcsr, zero
[0x8000a46c]:sw t5, 1872(ra)
[0x8000a470]:sw t6, 1880(ra)
[0x8000a474]:sw t5, 1888(ra)
[0x8000a478]:sw gp, 1896(ra)
[0x8000a47c]:lui a6, 1
[0x8000a480]:addi a6, a6, 2048
[0x8000a484]:add sp, sp, a6
[0x8000a488]:lw t3, 944(sp)
[0x8000a48c]:sub sp, sp, a6
[0x8000a490]:lui a6, 1
[0x8000a494]:addi a6, a6, 2048
[0x8000a498]:add sp, sp, a6
[0x8000a49c]:lw t4, 948(sp)
[0x8000a4a0]:sub sp, sp, a6
[0x8000a4a4]:lui a6, 1
[0x8000a4a8]:addi a6, a6, 2048
[0x8000a4ac]:add sp, sp, a6
[0x8000a4b0]:lw s10, 952(sp)
[0x8000a4b4]:sub sp, sp, a6
[0x8000a4b8]:lui a6, 1
[0x8000a4bc]:addi a6, a6, 2048
[0x8000a4c0]:add sp, sp, a6
[0x8000a4c4]:lw s11, 956(sp)
[0x8000a4c8]:sub sp, sp, a6
[0x8000a4cc]:addi t3, zero, 1
[0x8000a4d0]:lui t4, 524160
[0x8000a4d4]:addi s10, zero, 0
[0x8000a4d8]:lui s11, 784384
[0x8000a4dc]:addi a6, zero, 0
[0x8000a4e0]:csrrw zero, fcsr, a6
[0x8000a4e4]:fsgnj.d t5, t3, s10

[0x8000a4e4]:fsgnj.d t5, t3, s10
[0x8000a4e8]:csrrs gp, fcsr, zero
[0x8000a4ec]:sw t5, 1904(ra)
[0x8000a4f0]:sw t6, 1912(ra)
[0x8000a4f4]:sw t5, 1920(ra)
[0x8000a4f8]:sw gp, 1928(ra)
[0x8000a4fc]:lui a6, 1
[0x8000a500]:addi a6, a6, 2048
[0x8000a504]:add sp, sp, a6
[0x8000a508]:lw t3, 960(sp)
[0x8000a50c]:sub sp, sp, a6
[0x8000a510]:lui a6, 1
[0x8000a514]:addi a6, a6, 2048
[0x8000a518]:add sp, sp, a6
[0x8000a51c]:lw t4, 964(sp)
[0x8000a520]:sub sp, sp, a6
[0x8000a524]:lui a6, 1
[0x8000a528]:addi a6, a6, 2048
[0x8000a52c]:add sp, sp, a6
[0x8000a530]:lw s10, 968(sp)
[0x8000a534]:sub sp, sp, a6
[0x8000a538]:lui a6, 1
[0x8000a53c]:addi a6, a6, 2048
[0x8000a540]:add sp, sp, a6
[0x8000a544]:lw s11, 972(sp)
[0x8000a548]:sub sp, sp, a6
[0x8000a54c]:addi t3, zero, 1
[0x8000a550]:lui t4, 1048448
[0x8000a554]:addi s10, zero, 0
[0x8000a558]:addi s11, zero, 0
[0x8000a55c]:addi a6, zero, 0
[0x8000a560]:csrrw zero, fcsr, a6
[0x8000a564]:fsgnj.d t5, t3, s10

[0x8000a564]:fsgnj.d t5, t3, s10
[0x8000a568]:csrrs gp, fcsr, zero
[0x8000a56c]:sw t5, 1936(ra)
[0x8000a570]:sw t6, 1944(ra)
[0x8000a574]:sw t5, 1952(ra)
[0x8000a578]:sw gp, 1960(ra)
[0x8000a57c]:lui a6, 1
[0x8000a580]:addi a6, a6, 2048
[0x8000a584]:add sp, sp, a6
[0x8000a588]:lw t3, 976(sp)
[0x8000a58c]:sub sp, sp, a6
[0x8000a590]:lui a6, 1
[0x8000a594]:addi a6, a6, 2048
[0x8000a598]:add sp, sp, a6
[0x8000a59c]:lw t4, 980(sp)
[0x8000a5a0]:sub sp, sp, a6
[0x8000a5a4]:lui a6, 1
[0x8000a5a8]:addi a6, a6, 2048
[0x8000a5ac]:add sp, sp, a6
[0x8000a5b0]:lw s10, 984(sp)
[0x8000a5b4]:sub sp, sp, a6
[0x8000a5b8]:lui a6, 1
[0x8000a5bc]:addi a6, a6, 2048
[0x8000a5c0]:add sp, sp, a6
[0x8000a5c4]:lw s11, 988(sp)
[0x8000a5c8]:sub sp, sp, a6
[0x8000a5cc]:addi t3, zero, 1
[0x8000a5d0]:lui t4, 1048448
[0x8000a5d4]:addi s10, zero, 0
[0x8000a5d8]:lui s11, 524288
[0x8000a5dc]:addi a6, zero, 0
[0x8000a5e0]:csrrw zero, fcsr, a6
[0x8000a5e4]:fsgnj.d t5, t3, s10

[0x8000a5e4]:fsgnj.d t5, t3, s10
[0x8000a5e8]:csrrs gp, fcsr, zero
[0x8000a5ec]:sw t5, 1968(ra)
[0x8000a5f0]:sw t6, 1976(ra)
[0x8000a5f4]:sw t5, 1984(ra)
[0x8000a5f8]:sw gp, 1992(ra)
[0x8000a5fc]:lui a6, 1
[0x8000a600]:addi a6, a6, 2048
[0x8000a604]:add sp, sp, a6
[0x8000a608]:lw t3, 992(sp)
[0x8000a60c]:sub sp, sp, a6
[0x8000a610]:lui a6, 1
[0x8000a614]:addi a6, a6, 2048
[0x8000a618]:add sp, sp, a6
[0x8000a61c]:lw t4, 996(sp)
[0x8000a620]:sub sp, sp, a6
[0x8000a624]:lui a6, 1
[0x8000a628]:addi a6, a6, 2048
[0x8000a62c]:add sp, sp, a6
[0x8000a630]:lw s10, 1000(sp)
[0x8000a634]:sub sp, sp, a6
[0x8000a638]:lui a6, 1
[0x8000a63c]:addi a6, a6, 2048
[0x8000a640]:add sp, sp, a6
[0x8000a644]:lw s11, 1004(sp)
[0x8000a648]:sub sp, sp, a6
[0x8000a64c]:addi t3, zero, 1
[0x8000a650]:lui t4, 1048448
[0x8000a654]:addi s10, zero, 1
[0x8000a658]:addi s11, zero, 0
[0x8000a65c]:addi a6, zero, 0
[0x8000a660]:csrrw zero, fcsr, a6
[0x8000a664]:fsgnj.d t5, t3, s10

[0x8000a664]:fsgnj.d t5, t3, s10
[0x8000a668]:csrrs gp, fcsr, zero
[0x8000a66c]:sw t5, 2000(ra)
[0x8000a670]:sw t6, 2008(ra)
[0x8000a674]:sw t5, 2016(ra)
[0x8000a678]:sw gp, 2024(ra)
[0x8000a67c]:lui a6, 1
[0x8000a680]:addi a6, a6, 2048
[0x8000a684]:add sp, sp, a6
[0x8000a688]:lw t3, 1008(sp)
[0x8000a68c]:sub sp, sp, a6
[0x8000a690]:lui a6, 1
[0x8000a694]:addi a6, a6, 2048
[0x8000a698]:add sp, sp, a6
[0x8000a69c]:lw t4, 1012(sp)
[0x8000a6a0]:sub sp, sp, a6
[0x8000a6a4]:lui a6, 1
[0x8000a6a8]:addi a6, a6, 2048
[0x8000a6ac]:add sp, sp, a6
[0x8000a6b0]:lw s10, 1016(sp)
[0x8000a6b4]:sub sp, sp, a6
[0x8000a6b8]:lui a6, 1
[0x8000a6bc]:addi a6, a6, 2048
[0x8000a6c0]:add sp, sp, a6
[0x8000a6c4]:lw s11, 1020(sp)
[0x8000a6c8]:sub sp, sp, a6
[0x8000a6cc]:addi t3, zero, 1
[0x8000a6d0]:lui t4, 1048448
[0x8000a6d4]:addi s10, zero, 1
[0x8000a6d8]:lui s11, 524288
[0x8000a6dc]:addi a6, zero, 0
[0x8000a6e0]:csrrw zero, fcsr, a6
[0x8000a6e4]:fsgnj.d t5, t3, s10

[0x8000a6e4]:fsgnj.d t5, t3, s10
[0x8000a6e8]:csrrs gp, fcsr, zero
[0x8000a6ec]:sw t5, 2032(ra)
[0x8000a6f0]:sw t6, 2040(ra)
[0x8000a6f4]:addi ra, ra, 2040
[0x8000a6f8]:sw t5, 8(ra)
[0x8000a6fc]:sw gp, 16(ra)
[0x8000a700]:lui a6, 1
[0x8000a704]:addi a6, a6, 2048
[0x8000a708]:add sp, sp, a6
[0x8000a70c]:lw t3, 1024(sp)
[0x8000a710]:sub sp, sp, a6
[0x8000a714]:lui a6, 1
[0x8000a718]:addi a6, a6, 2048
[0x8000a71c]:add sp, sp, a6
[0x8000a720]:lw t4, 1028(sp)
[0x8000a724]:sub sp, sp, a6
[0x8000a728]:lui a6, 1
[0x8000a72c]:addi a6, a6, 2048
[0x8000a730]:add sp, sp, a6
[0x8000a734]:lw s10, 1032(sp)
[0x8000a738]:sub sp, sp, a6
[0x8000a73c]:lui a6, 1
[0x8000a740]:addi a6, a6, 2048
[0x8000a744]:add sp, sp, a6
[0x8000a748]:lw s11, 1036(sp)
[0x8000a74c]:sub sp, sp, a6
[0x8000a750]:addi t3, zero, 1
[0x8000a754]:lui t4, 1048448
[0x8000a758]:addi s10, zero, 2
[0x8000a75c]:addi s11, zero, 0
[0x8000a760]:addi a6, zero, 0
[0x8000a764]:csrrw zero, fcsr, a6
[0x8000a768]:fsgnj.d t5, t3, s10

[0x8000a768]:fsgnj.d t5, t3, s10
[0x8000a76c]:csrrs gp, fcsr, zero
[0x8000a770]:sw t5, 24(ra)
[0x8000a774]:sw t6, 32(ra)
[0x8000a778]:sw t5, 40(ra)
[0x8000a77c]:sw gp, 48(ra)
[0x8000a780]:lui a6, 1
[0x8000a784]:addi a6, a6, 2048
[0x8000a788]:add sp, sp, a6
[0x8000a78c]:lw t3, 1040(sp)
[0x8000a790]:sub sp, sp, a6
[0x8000a794]:lui a6, 1
[0x8000a798]:addi a6, a6, 2048
[0x8000a79c]:add sp, sp, a6
[0x8000a7a0]:lw t4, 1044(sp)
[0x8000a7a4]:sub sp, sp, a6
[0x8000a7a8]:lui a6, 1
[0x8000a7ac]:addi a6, a6, 2048
[0x8000a7b0]:add sp, sp, a6
[0x8000a7b4]:lw s10, 1048(sp)
[0x8000a7b8]:sub sp, sp, a6
[0x8000a7bc]:lui a6, 1
[0x8000a7c0]:addi a6, a6, 2048
[0x8000a7c4]:add sp, sp, a6
[0x8000a7c8]:lw s11, 1052(sp)
[0x8000a7cc]:sub sp, sp, a6
[0x8000a7d0]:addi t3, zero, 1
[0x8000a7d4]:lui t4, 1048448
[0x8000a7d8]:addi s10, zero, 2
[0x8000a7dc]:lui s11, 524288
[0x8000a7e0]:addi a6, zero, 0
[0x8000a7e4]:csrrw zero, fcsr, a6
[0x8000a7e8]:fsgnj.d t5, t3, s10

[0x8000a7e8]:fsgnj.d t5, t3, s10
[0x8000a7ec]:csrrs gp, fcsr, zero
[0x8000a7f0]:sw t5, 56(ra)
[0x8000a7f4]:sw t6, 64(ra)
[0x8000a7f8]:sw t5, 72(ra)
[0x8000a7fc]:sw gp, 80(ra)
[0x8000a800]:lui a6, 1
[0x8000a804]:addi a6, a6, 2048
[0x8000a808]:add sp, sp, a6
[0x8000a80c]:lw t3, 1056(sp)
[0x8000a810]:sub sp, sp, a6
[0x8000a814]:lui a6, 1
[0x8000a818]:addi a6, a6, 2048
[0x8000a81c]:add sp, sp, a6
[0x8000a820]:lw t4, 1060(sp)
[0x8000a824]:sub sp, sp, a6
[0x8000a828]:lui a6, 1
[0x8000a82c]:addi a6, a6, 2048
[0x8000a830]:add sp, sp, a6
[0x8000a834]:lw s10, 1064(sp)
[0x8000a838]:sub sp, sp, a6
[0x8000a83c]:lui a6, 1
[0x8000a840]:addi a6, a6, 2048
[0x8000a844]:add sp, sp, a6
[0x8000a848]:lw s11, 1068(sp)
[0x8000a84c]:sub sp, sp, a6
[0x8000a850]:addi t3, zero, 1
[0x8000a854]:lui t4, 1048448
[0x8000a858]:addi s10, zero, 4095
[0x8000a85c]:lui s11, 256
[0x8000a860]:addi s11, s11, 4095
[0x8000a864]:addi a6, zero, 0
[0x8000a868]:csrrw zero, fcsr, a6
[0x8000a86c]:fsgnj.d t5, t3, s10

[0x8000a86c]:fsgnj.d t5, t3, s10
[0x8000a870]:csrrs gp, fcsr, zero
[0x8000a874]:sw t5, 88(ra)
[0x8000a878]:sw t6, 96(ra)
[0x8000a87c]:sw t5, 104(ra)
[0x8000a880]:sw gp, 112(ra)
[0x8000a884]:lui a6, 1
[0x8000a888]:addi a6, a6, 2048
[0x8000a88c]:add sp, sp, a6
[0x8000a890]:lw t3, 1072(sp)
[0x8000a894]:sub sp, sp, a6
[0x8000a898]:lui a6, 1
[0x8000a89c]:addi a6, a6, 2048
[0x8000a8a0]:add sp, sp, a6
[0x8000a8a4]:lw t4, 1076(sp)
[0x8000a8a8]:sub sp, sp, a6
[0x8000a8ac]:lui a6, 1
[0x8000a8b0]:addi a6, a6, 2048
[0x8000a8b4]:add sp, sp, a6
[0x8000a8b8]:lw s10, 1080(sp)
[0x8000a8bc]:sub sp, sp, a6
[0x8000a8c0]:lui a6, 1
[0x8000a8c4]:addi a6, a6, 2048
[0x8000a8c8]:add sp, sp, a6
[0x8000a8cc]:lw s11, 1084(sp)
[0x8000a8d0]:sub sp, sp, a6
[0x8000a8d4]:addi t3, zero, 1
[0x8000a8d8]:lui t4, 1048448
[0x8000a8dc]:addi s10, zero, 4095
[0x8000a8e0]:lui s11, 524544
[0x8000a8e4]:addi s11, s11, 4095
[0x8000a8e8]:addi a6, zero, 0
[0x8000a8ec]:csrrw zero, fcsr, a6
[0x8000a8f0]:fsgnj.d t5, t3, s10

[0x8000a8f0]:fsgnj.d t5, t3, s10
[0x8000a8f4]:csrrs gp, fcsr, zero
[0x8000a8f8]:sw t5, 120(ra)
[0x8000a8fc]:sw t6, 128(ra)
[0x8000a900]:sw t5, 136(ra)
[0x8000a904]:sw gp, 144(ra)
[0x8000a908]:lui a6, 1
[0x8000a90c]:addi a6, a6, 2048
[0x8000a910]:add sp, sp, a6
[0x8000a914]:lw t3, 1088(sp)
[0x8000a918]:sub sp, sp, a6
[0x8000a91c]:lui a6, 1
[0x8000a920]:addi a6, a6, 2048
[0x8000a924]:add sp, sp, a6
[0x8000a928]:lw t4, 1092(sp)
[0x8000a92c]:sub sp, sp, a6
[0x8000a930]:lui a6, 1
[0x8000a934]:addi a6, a6, 2048
[0x8000a938]:add sp, sp, a6
[0x8000a93c]:lw s10, 1096(sp)
[0x8000a940]:sub sp, sp, a6
[0x8000a944]:lui a6, 1
[0x8000a948]:addi a6, a6, 2048
[0x8000a94c]:add sp, sp, a6
[0x8000a950]:lw s11, 1100(sp)
[0x8000a954]:sub sp, sp, a6
[0x8000a958]:addi t3, zero, 1
[0x8000a95c]:lui t4, 1048448
[0x8000a960]:addi s10, zero, 0
[0x8000a964]:lui s11, 256
[0x8000a968]:addi a6, zero, 0
[0x8000a96c]:csrrw zero, fcsr, a6
[0x8000a970]:fsgnj.d t5, t3, s10

[0x8000a970]:fsgnj.d t5, t3, s10
[0x8000a974]:csrrs gp, fcsr, zero
[0x8000a978]:sw t5, 152(ra)
[0x8000a97c]:sw t6, 160(ra)
[0x8000a980]:sw t5, 168(ra)
[0x8000a984]:sw gp, 176(ra)
[0x8000a988]:lui a6, 1
[0x8000a98c]:addi a6, a6, 2048
[0x8000a990]:add sp, sp, a6
[0x8000a994]:lw t3, 1104(sp)
[0x8000a998]:sub sp, sp, a6
[0x8000a99c]:lui a6, 1
[0x8000a9a0]:addi a6, a6, 2048
[0x8000a9a4]:add sp, sp, a6
[0x8000a9a8]:lw t4, 1108(sp)
[0x8000a9ac]:sub sp, sp, a6
[0x8000a9b0]:lui a6, 1
[0x8000a9b4]:addi a6, a6, 2048
[0x8000a9b8]:add sp, sp, a6
[0x8000a9bc]:lw s10, 1112(sp)
[0x8000a9c0]:sub sp, sp, a6
[0x8000a9c4]:lui a6, 1
[0x8000a9c8]:addi a6, a6, 2048
[0x8000a9cc]:add sp, sp, a6
[0x8000a9d0]:lw s11, 1116(sp)
[0x8000a9d4]:sub sp, sp, a6
[0x8000a9d8]:addi t3, zero, 1
[0x8000a9dc]:lui t4, 1048448
[0x8000a9e0]:addi s10, zero, 0
[0x8000a9e4]:lui s11, 524544
[0x8000a9e8]:addi a6, zero, 0
[0x8000a9ec]:csrrw zero, fcsr, a6
[0x8000a9f0]:fsgnj.d t5, t3, s10

[0x8000a9f0]:fsgnj.d t5, t3, s10
[0x8000a9f4]:csrrs gp, fcsr, zero
[0x8000a9f8]:sw t5, 184(ra)
[0x8000a9fc]:sw t6, 192(ra)
[0x8000aa00]:sw t5, 200(ra)
[0x8000aa04]:sw gp, 208(ra)
[0x8000aa08]:lui a6, 1
[0x8000aa0c]:addi a6, a6, 2048
[0x8000aa10]:add sp, sp, a6
[0x8000aa14]:lw t3, 1120(sp)
[0x8000aa18]:sub sp, sp, a6
[0x8000aa1c]:lui a6, 1
[0x8000aa20]:addi a6, a6, 2048
[0x8000aa24]:add sp, sp, a6
[0x8000aa28]:lw t4, 1124(sp)
[0x8000aa2c]:sub sp, sp, a6
[0x8000aa30]:lui a6, 1
[0x8000aa34]:addi a6, a6, 2048
[0x8000aa38]:add sp, sp, a6
[0x8000aa3c]:lw s10, 1128(sp)
[0x8000aa40]:sub sp, sp, a6
[0x8000aa44]:lui a6, 1
[0x8000aa48]:addi a6, a6, 2048
[0x8000aa4c]:add sp, sp, a6
[0x8000aa50]:lw s11, 1132(sp)
[0x8000aa54]:sub sp, sp, a6
[0x8000aa58]:addi t3, zero, 1
[0x8000aa5c]:lui t4, 1048448
[0x8000aa60]:addi s10, zero, 2
[0x8000aa64]:lui s11, 256
[0x8000aa68]:addi a6, zero, 0
[0x8000aa6c]:csrrw zero, fcsr, a6
[0x8000aa70]:fsgnj.d t5, t3, s10

[0x8000aa70]:fsgnj.d t5, t3, s10
[0x8000aa74]:csrrs gp, fcsr, zero
[0x8000aa78]:sw t5, 216(ra)
[0x8000aa7c]:sw t6, 224(ra)
[0x8000aa80]:sw t5, 232(ra)
[0x8000aa84]:sw gp, 240(ra)
[0x8000aa88]:lui a6, 1
[0x8000aa8c]:addi a6, a6, 2048
[0x8000aa90]:add sp, sp, a6
[0x8000aa94]:lw t3, 1136(sp)
[0x8000aa98]:sub sp, sp, a6
[0x8000aa9c]:lui a6, 1
[0x8000aaa0]:addi a6, a6, 2048
[0x8000aaa4]:add sp, sp, a6
[0x8000aaa8]:lw t4, 1140(sp)
[0x8000aaac]:sub sp, sp, a6
[0x8000aab0]:lui a6, 1
[0x8000aab4]:addi a6, a6, 2048
[0x8000aab8]:add sp, sp, a6
[0x8000aabc]:lw s10, 1144(sp)
[0x8000aac0]:sub sp, sp, a6
[0x8000aac4]:lui a6, 1
[0x8000aac8]:addi a6, a6, 2048
[0x8000aacc]:add sp, sp, a6
[0x8000aad0]:lw s11, 1148(sp)
[0x8000aad4]:sub sp, sp, a6
[0x8000aad8]:addi t3, zero, 1
[0x8000aadc]:lui t4, 1048448
[0x8000aae0]:addi s10, zero, 2
[0x8000aae4]:lui s11, 524544
[0x8000aae8]:addi a6, zero, 0
[0x8000aaec]:csrrw zero, fcsr, a6
[0x8000aaf0]:fsgnj.d t5, t3, s10

[0x8000aaf0]:fsgnj.d t5, t3, s10
[0x8000aaf4]:csrrs gp, fcsr, zero
[0x8000aaf8]:sw t5, 248(ra)
[0x8000aafc]:sw t6, 256(ra)
[0x8000ab00]:sw t5, 264(ra)
[0x8000ab04]:sw gp, 272(ra)
[0x8000ab08]:lui a6, 1
[0x8000ab0c]:addi a6, a6, 2048
[0x8000ab10]:add sp, sp, a6
[0x8000ab14]:lw t3, 1152(sp)
[0x8000ab18]:sub sp, sp, a6
[0x8000ab1c]:lui a6, 1
[0x8000ab20]:addi a6, a6, 2048
[0x8000ab24]:add sp, sp, a6
[0x8000ab28]:lw t4, 1156(sp)
[0x8000ab2c]:sub sp, sp, a6
[0x8000ab30]:lui a6, 1
[0x8000ab34]:addi a6, a6, 2048
[0x8000ab38]:add sp, sp, a6
[0x8000ab3c]:lw s10, 1160(sp)
[0x8000ab40]:sub sp, sp, a6
[0x8000ab44]:lui a6, 1
[0x8000ab48]:addi a6, a6, 2048
[0x8000ab4c]:add sp, sp, a6
[0x8000ab50]:lw s11, 1164(sp)
[0x8000ab54]:sub sp, sp, a6
[0x8000ab58]:addi t3, zero, 1
[0x8000ab5c]:lui t4, 1048448
[0x8000ab60]:addi s10, zero, 4095
[0x8000ab64]:lui s11, 524032
[0x8000ab68]:addi s11, s11, 4095
[0x8000ab6c]:addi a6, zero, 0
[0x8000ab70]:csrrw zero, fcsr, a6
[0x8000ab74]:fsgnj.d t5, t3, s10

[0x8000ab74]:fsgnj.d t5, t3, s10
[0x8000ab78]:csrrs gp, fcsr, zero
[0x8000ab7c]:sw t5, 280(ra)
[0x8000ab80]:sw t6, 288(ra)
[0x8000ab84]:sw t5, 296(ra)
[0x8000ab88]:sw gp, 304(ra)
[0x8000ab8c]:lui a6, 1
[0x8000ab90]:addi a6, a6, 2048
[0x8000ab94]:add sp, sp, a6
[0x8000ab98]:lw t3, 1168(sp)
[0x8000ab9c]:sub sp, sp, a6
[0x8000aba0]:lui a6, 1
[0x8000aba4]:addi a6, a6, 2048
[0x8000aba8]:add sp, sp, a6
[0x8000abac]:lw t4, 1172(sp)
[0x8000abb0]:sub sp, sp, a6
[0x8000abb4]:lui a6, 1
[0x8000abb8]:addi a6, a6, 2048
[0x8000abbc]:add sp, sp, a6
[0x8000abc0]:lw s10, 1176(sp)
[0x8000abc4]:sub sp, sp, a6
[0x8000abc8]:lui a6, 1
[0x8000abcc]:addi a6, a6, 2048
[0x8000abd0]:add sp, sp, a6
[0x8000abd4]:lw s11, 1180(sp)
[0x8000abd8]:sub sp, sp, a6
[0x8000abdc]:addi t3, zero, 1
[0x8000abe0]:lui t4, 1048448
[0x8000abe4]:addi s10, zero, 4095
[0x8000abe8]:lui s11, 1048320
[0x8000abec]:addi s11, s11, 4095
[0x8000abf0]:addi a6, zero, 0
[0x8000abf4]:csrrw zero, fcsr, a6
[0x8000abf8]:fsgnj.d t5, t3, s10

[0x8000abf8]:fsgnj.d t5, t3, s10
[0x8000abfc]:csrrs gp, fcsr, zero
[0x8000ac00]:sw t5, 312(ra)
[0x8000ac04]:sw t6, 320(ra)
[0x8000ac08]:sw t5, 328(ra)
[0x8000ac0c]:sw gp, 336(ra)
[0x8000ac10]:lui a6, 1
[0x8000ac14]:addi a6, a6, 2048
[0x8000ac18]:add sp, sp, a6
[0x8000ac1c]:lw t3, 1184(sp)
[0x8000ac20]:sub sp, sp, a6
[0x8000ac24]:lui a6, 1
[0x8000ac28]:addi a6, a6, 2048
[0x8000ac2c]:add sp, sp, a6
[0x8000ac30]:lw t4, 1188(sp)
[0x8000ac34]:sub sp, sp, a6
[0x8000ac38]:lui a6, 1
[0x8000ac3c]:addi a6, a6, 2048
[0x8000ac40]:add sp, sp, a6
[0x8000ac44]:lw s10, 1192(sp)
[0x8000ac48]:sub sp, sp, a6
[0x8000ac4c]:lui a6, 1
[0x8000ac50]:addi a6, a6, 2048
[0x8000ac54]:add sp, sp, a6
[0x8000ac58]:lw s11, 1196(sp)
[0x8000ac5c]:sub sp, sp, a6
[0x8000ac60]:addi t3, zero, 1
[0x8000ac64]:lui t4, 1048448
[0x8000ac68]:addi s10, zero, 0
[0x8000ac6c]:lui s11, 524032
[0x8000ac70]:addi a6, zero, 0
[0x8000ac74]:csrrw zero, fcsr, a6
[0x8000ac78]:fsgnj.d t5, t3, s10

[0x8000ac78]:fsgnj.d t5, t3, s10
[0x8000ac7c]:csrrs gp, fcsr, zero
[0x8000ac80]:sw t5, 344(ra)
[0x8000ac84]:sw t6, 352(ra)
[0x8000ac88]:sw t5, 360(ra)
[0x8000ac8c]:sw gp, 368(ra)
[0x8000ac90]:lui a6, 1
[0x8000ac94]:addi a6, a6, 2048
[0x8000ac98]:add sp, sp, a6
[0x8000ac9c]:lw t3, 1200(sp)
[0x8000aca0]:sub sp, sp, a6
[0x8000aca4]:lui a6, 1
[0x8000aca8]:addi a6, a6, 2048
[0x8000acac]:add sp, sp, a6
[0x8000acb0]:lw t4, 1204(sp)
[0x8000acb4]:sub sp, sp, a6
[0x8000acb8]:lui a6, 1
[0x8000acbc]:addi a6, a6, 2048
[0x8000acc0]:add sp, sp, a6
[0x8000acc4]:lw s10, 1208(sp)
[0x8000acc8]:sub sp, sp, a6
[0x8000accc]:lui a6, 1
[0x8000acd0]:addi a6, a6, 2048
[0x8000acd4]:add sp, sp, a6
[0x8000acd8]:lw s11, 1212(sp)
[0x8000acdc]:sub sp, sp, a6
[0x8000ace0]:addi t3, zero, 1
[0x8000ace4]:lui t4, 1048448
[0x8000ace8]:addi s10, zero, 0
[0x8000acec]:lui s11, 1048320
[0x8000acf0]:addi a6, zero, 0
[0x8000acf4]:csrrw zero, fcsr, a6
[0x8000acf8]:fsgnj.d t5, t3, s10

[0x8000acf8]:fsgnj.d t5, t3, s10
[0x8000acfc]:csrrs gp, fcsr, zero
[0x8000ad00]:sw t5, 376(ra)
[0x8000ad04]:sw t6, 384(ra)
[0x8000ad08]:sw t5, 392(ra)
[0x8000ad0c]:sw gp, 400(ra)
[0x8000ad10]:lui a6, 1
[0x8000ad14]:addi a6, a6, 2048
[0x8000ad18]:add sp, sp, a6
[0x8000ad1c]:lw t3, 1216(sp)
[0x8000ad20]:sub sp, sp, a6
[0x8000ad24]:lui a6, 1
[0x8000ad28]:addi a6, a6, 2048
[0x8000ad2c]:add sp, sp, a6
[0x8000ad30]:lw t4, 1220(sp)
[0x8000ad34]:sub sp, sp, a6
[0x8000ad38]:lui a6, 1
[0x8000ad3c]:addi a6, a6, 2048
[0x8000ad40]:add sp, sp, a6
[0x8000ad44]:lw s10, 1224(sp)
[0x8000ad48]:sub sp, sp, a6
[0x8000ad4c]:lui a6, 1
[0x8000ad50]:addi a6, a6, 2048
[0x8000ad54]:add sp, sp, a6
[0x8000ad58]:lw s11, 1228(sp)
[0x8000ad5c]:sub sp, sp, a6
[0x8000ad60]:addi t3, zero, 1
[0x8000ad64]:lui t4, 1048448
[0x8000ad68]:addi s10, zero, 0
[0x8000ad6c]:lui s11, 524160
[0x8000ad70]:addi a6, zero, 0
[0x8000ad74]:csrrw zero, fcsr, a6
[0x8000ad78]:fsgnj.d t5, t3, s10

[0x8000ad78]:fsgnj.d t5, t3, s10
[0x8000ad7c]:csrrs gp, fcsr, zero
[0x8000ad80]:sw t5, 408(ra)
[0x8000ad84]:sw t6, 416(ra)
[0x8000ad88]:sw t5, 424(ra)
[0x8000ad8c]:sw gp, 432(ra)
[0x8000ad90]:lui a6, 1
[0x8000ad94]:addi a6, a6, 2048
[0x8000ad98]:add sp, sp, a6
[0x8000ad9c]:lw t3, 1232(sp)
[0x8000ada0]:sub sp, sp, a6
[0x8000ada4]:lui a6, 1
[0x8000ada8]:addi a6, a6, 2048
[0x8000adac]:add sp, sp, a6
[0x8000adb0]:lw t4, 1236(sp)
[0x8000adb4]:sub sp, sp, a6
[0x8000adb8]:lui a6, 1
[0x8000adbc]:addi a6, a6, 2048
[0x8000adc0]:add sp, sp, a6
[0x8000adc4]:lw s10, 1240(sp)
[0x8000adc8]:sub sp, sp, a6
[0x8000adcc]:lui a6, 1
[0x8000add0]:addi a6, a6, 2048
[0x8000add4]:add sp, sp, a6
[0x8000add8]:lw s11, 1244(sp)
[0x8000addc]:sub sp, sp, a6
[0x8000ade0]:addi t3, zero, 1
[0x8000ade4]:lui t4, 1048448
[0x8000ade8]:addi s10, zero, 0
[0x8000adec]:lui s11, 1048448
[0x8000adf0]:addi a6, zero, 0
[0x8000adf4]:csrrw zero, fcsr, a6
[0x8000adf8]:fsgnj.d t5, t3, s10

[0x8000adf8]:fsgnj.d t5, t3, s10
[0x8000adfc]:csrrs gp, fcsr, zero
[0x8000ae00]:sw t5, 440(ra)
[0x8000ae04]:sw t6, 448(ra)
[0x8000ae08]:sw t5, 456(ra)
[0x8000ae0c]:sw gp, 464(ra)
[0x8000ae10]:lui a6, 1
[0x8000ae14]:addi a6, a6, 2048
[0x8000ae18]:add sp, sp, a6
[0x8000ae1c]:lw t3, 1248(sp)
[0x8000ae20]:sub sp, sp, a6
[0x8000ae24]:lui a6, 1
[0x8000ae28]:addi a6, a6, 2048
[0x8000ae2c]:add sp, sp, a6
[0x8000ae30]:lw t4, 1252(sp)
[0x8000ae34]:sub sp, sp, a6
[0x8000ae38]:lui a6, 1
[0x8000ae3c]:addi a6, a6, 2048
[0x8000ae40]:add sp, sp, a6
[0x8000ae44]:lw s10, 1256(sp)
[0x8000ae48]:sub sp, sp, a6
[0x8000ae4c]:lui a6, 1
[0x8000ae50]:addi a6, a6, 2048
[0x8000ae54]:add sp, sp, a6
[0x8000ae58]:lw s11, 1260(sp)
[0x8000ae5c]:sub sp, sp, a6
[0x8000ae60]:addi t3, zero, 1
[0x8000ae64]:lui t4, 1048448
[0x8000ae68]:addi s10, zero, 1
[0x8000ae6c]:lui s11, 524160
[0x8000ae70]:addi a6, zero, 0
[0x8000ae74]:csrrw zero, fcsr, a6
[0x8000ae78]:fsgnj.d t5, t3, s10

[0x8000ae78]:fsgnj.d t5, t3, s10
[0x8000ae7c]:csrrs gp, fcsr, zero
[0x8000ae80]:sw t5, 472(ra)
[0x8000ae84]:sw t6, 480(ra)
[0x8000ae88]:sw t5, 488(ra)
[0x8000ae8c]:sw gp, 496(ra)
[0x8000ae90]:lui a6, 1
[0x8000ae94]:addi a6, a6, 2048
[0x8000ae98]:add sp, sp, a6
[0x8000ae9c]:lw t3, 1264(sp)
[0x8000aea0]:sub sp, sp, a6
[0x8000aea4]:lui a6, 1
[0x8000aea8]:addi a6, a6, 2048
[0x8000aeac]:add sp, sp, a6
[0x8000aeb0]:lw t4, 1268(sp)
[0x8000aeb4]:sub sp, sp, a6
[0x8000aeb8]:lui a6, 1
[0x8000aebc]:addi a6, a6, 2048
[0x8000aec0]:add sp, sp, a6
[0x8000aec4]:lw s10, 1272(sp)
[0x8000aec8]:sub sp, sp, a6
[0x8000aecc]:lui a6, 1
[0x8000aed0]:addi a6, a6, 2048
[0x8000aed4]:add sp, sp, a6
[0x8000aed8]:lw s11, 1276(sp)
[0x8000aedc]:sub sp, sp, a6
[0x8000aee0]:addi t3, zero, 1
[0x8000aee4]:lui t4, 1048448
[0x8000aee8]:addi s10, zero, 1
[0x8000aeec]:lui s11, 1048448
[0x8000aef0]:addi a6, zero, 0
[0x8000aef4]:csrrw zero, fcsr, a6
[0x8000aef8]:fsgnj.d t5, t3, s10

[0x8000aef8]:fsgnj.d t5, t3, s10
[0x8000aefc]:csrrs gp, fcsr, zero
[0x8000af00]:sw t5, 504(ra)
[0x8000af04]:sw t6, 512(ra)
[0x8000af08]:sw t5, 520(ra)
[0x8000af0c]:sw gp, 528(ra)
[0x8000af10]:lui a6, 1
[0x8000af14]:addi a6, a6, 2048
[0x8000af18]:add sp, sp, a6
[0x8000af1c]:lw t3, 1280(sp)
[0x8000af20]:sub sp, sp, a6
[0x8000af24]:lui a6, 1
[0x8000af28]:addi a6, a6, 2048
[0x8000af2c]:add sp, sp, a6
[0x8000af30]:lw t4, 1284(sp)
[0x8000af34]:sub sp, sp, a6
[0x8000af38]:lui a6, 1
[0x8000af3c]:addi a6, a6, 2048
[0x8000af40]:add sp, sp, a6
[0x8000af44]:lw s10, 1288(sp)
[0x8000af48]:sub sp, sp, a6
[0x8000af4c]:lui a6, 1
[0x8000af50]:addi a6, a6, 2048
[0x8000af54]:add sp, sp, a6
[0x8000af58]:lw s11, 1292(sp)
[0x8000af5c]:sub sp, sp, a6
[0x8000af60]:addi t3, zero, 1
[0x8000af64]:lui t4, 1048448
[0x8000af68]:addi s10, zero, 1
[0x8000af6c]:lui s11, 524032
[0x8000af70]:addi a6, zero, 0
[0x8000af74]:csrrw zero, fcsr, a6
[0x8000af78]:fsgnj.d t5, t3, s10

[0x8000af78]:fsgnj.d t5, t3, s10
[0x8000af7c]:csrrs gp, fcsr, zero
[0x8000af80]:sw t5, 536(ra)
[0x8000af84]:sw t6, 544(ra)
[0x8000af88]:sw t5, 552(ra)
[0x8000af8c]:sw gp, 560(ra)
[0x8000af90]:lui a6, 1
[0x8000af94]:addi a6, a6, 2048
[0x8000af98]:add sp, sp, a6
[0x8000af9c]:lw t3, 1296(sp)
[0x8000afa0]:sub sp, sp, a6
[0x8000afa4]:lui a6, 1
[0x8000afa8]:addi a6, a6, 2048
[0x8000afac]:add sp, sp, a6
[0x8000afb0]:lw t4, 1300(sp)
[0x8000afb4]:sub sp, sp, a6
[0x8000afb8]:lui a6, 1
[0x8000afbc]:addi a6, a6, 2048
[0x8000afc0]:add sp, sp, a6
[0x8000afc4]:lw s10, 1304(sp)
[0x8000afc8]:sub sp, sp, a6
[0x8000afcc]:lui a6, 1
[0x8000afd0]:addi a6, a6, 2048
[0x8000afd4]:add sp, sp, a6
[0x8000afd8]:lw s11, 1308(sp)
[0x8000afdc]:sub sp, sp, a6
[0x8000afe0]:addi t3, zero, 1
[0x8000afe4]:lui t4, 1048448
[0x8000afe8]:addi s10, zero, 1
[0x8000afec]:lui s11, 1048320
[0x8000aff0]:addi a6, zero, 0
[0x8000aff4]:csrrw zero, fcsr, a6
[0x8000aff8]:fsgnj.d t5, t3, s10

[0x8000aff8]:fsgnj.d t5, t3, s10
[0x8000affc]:csrrs gp, fcsr, zero
[0x8000b000]:sw t5, 568(ra)
[0x8000b004]:sw t6, 576(ra)
[0x8000b008]:sw t5, 584(ra)
[0x8000b00c]:sw gp, 592(ra)
[0x8000b010]:lui a6, 1
[0x8000b014]:addi a6, a6, 2048
[0x8000b018]:add sp, sp, a6
[0x8000b01c]:lw t3, 1312(sp)
[0x8000b020]:sub sp, sp, a6
[0x8000b024]:lui a6, 1
[0x8000b028]:addi a6, a6, 2048
[0x8000b02c]:add sp, sp, a6
[0x8000b030]:lw t4, 1316(sp)
[0x8000b034]:sub sp, sp, a6
[0x8000b038]:lui a6, 1
[0x8000b03c]:addi a6, a6, 2048
[0x8000b040]:add sp, sp, a6
[0x8000b044]:lw s10, 1320(sp)
[0x8000b048]:sub sp, sp, a6
[0x8000b04c]:lui a6, 1
[0x8000b050]:addi a6, a6, 2048
[0x8000b054]:add sp, sp, a6
[0x8000b058]:lw s11, 1324(sp)
[0x8000b05c]:sub sp, sp, a6
[0x8000b060]:addi t3, zero, 1
[0x8000b064]:lui t4, 1048448
[0x8000b068]:addi s10, zero, 0
[0x8000b06c]:lui s11, 261888
[0x8000b070]:addi a6, zero, 0
[0x8000b074]:csrrw zero, fcsr, a6
[0x8000b078]:fsgnj.d t5, t3, s10

[0x8000b078]:fsgnj.d t5, t3, s10
[0x8000b07c]:csrrs gp, fcsr, zero
[0x8000b080]:sw t5, 600(ra)
[0x8000b084]:sw t6, 608(ra)
[0x8000b088]:sw t5, 616(ra)
[0x8000b08c]:sw gp, 624(ra)
[0x8000b090]:lui a6, 1
[0x8000b094]:addi a6, a6, 2048
[0x8000b098]:add sp, sp, a6
[0x8000b09c]:lw t3, 1328(sp)
[0x8000b0a0]:sub sp, sp, a6
[0x8000b0a4]:lui a6, 1
[0x8000b0a8]:addi a6, a6, 2048
[0x8000b0ac]:add sp, sp, a6
[0x8000b0b0]:lw t4, 1332(sp)
[0x8000b0b4]:sub sp, sp, a6
[0x8000b0b8]:lui a6, 1
[0x8000b0bc]:addi a6, a6, 2048
[0x8000b0c0]:add sp, sp, a6
[0x8000b0c4]:lw s10, 1336(sp)
[0x8000b0c8]:sub sp, sp, a6
[0x8000b0cc]:lui a6, 1
[0x8000b0d0]:addi a6, a6, 2048
[0x8000b0d4]:add sp, sp, a6
[0x8000b0d8]:lw s11, 1340(sp)
[0x8000b0dc]:sub sp, sp, a6
[0x8000b0e0]:addi t3, zero, 1
[0x8000b0e4]:lui t4, 1048448
[0x8000b0e8]:addi s10, zero, 0
[0x8000b0ec]:lui s11, 784384
[0x8000b0f0]:addi a6, zero, 0
[0x8000b0f4]:csrrw zero, fcsr, a6
[0x8000b0f8]:fsgnj.d t5, t3, s10

[0x8000b0f8]:fsgnj.d t5, t3, s10
[0x8000b0fc]:csrrs gp, fcsr, zero
[0x8000b100]:sw t5, 632(ra)
[0x8000b104]:sw t6, 640(ra)
[0x8000b108]:sw t5, 648(ra)
[0x8000b10c]:sw gp, 656(ra)
[0x8000b110]:lui a6, 1
[0x8000b114]:addi a6, a6, 2048
[0x8000b118]:add sp, sp, a6
[0x8000b11c]:lw t3, 1344(sp)
[0x8000b120]:sub sp, sp, a6
[0x8000b124]:lui a6, 1
[0x8000b128]:addi a6, a6, 2048
[0x8000b12c]:add sp, sp, a6
[0x8000b130]:lw t4, 1348(sp)
[0x8000b134]:sub sp, sp, a6
[0x8000b138]:lui a6, 1
[0x8000b13c]:addi a6, a6, 2048
[0x8000b140]:add sp, sp, a6
[0x8000b144]:lw s10, 1352(sp)
[0x8000b148]:sub sp, sp, a6
[0x8000b14c]:lui a6, 1
[0x8000b150]:addi a6, a6, 2048
[0x8000b154]:add sp, sp, a6
[0x8000b158]:lw s11, 1356(sp)
[0x8000b15c]:sub sp, sp, a6
[0x8000b160]:addi t3, zero, 1
[0x8000b164]:lui t4, 524032
[0x8000b168]:addi s10, zero, 0
[0x8000b16c]:addi s11, zero, 0
[0x8000b170]:addi a6, zero, 0
[0x8000b174]:csrrw zero, fcsr, a6
[0x8000b178]:fsgnj.d t5, t3, s10

[0x8000b178]:fsgnj.d t5, t3, s10
[0x8000b17c]:csrrs gp, fcsr, zero
[0x8000b180]:sw t5, 664(ra)
[0x8000b184]:sw t6, 672(ra)
[0x8000b188]:sw t5, 680(ra)
[0x8000b18c]:sw gp, 688(ra)
[0x8000b190]:lui a6, 1
[0x8000b194]:addi a6, a6, 2048
[0x8000b198]:add sp, sp, a6
[0x8000b19c]:lw t3, 1360(sp)
[0x8000b1a0]:sub sp, sp, a6
[0x8000b1a4]:lui a6, 1
[0x8000b1a8]:addi a6, a6, 2048
[0x8000b1ac]:add sp, sp, a6
[0x8000b1b0]:lw t4, 1364(sp)
[0x8000b1b4]:sub sp, sp, a6
[0x8000b1b8]:lui a6, 1
[0x8000b1bc]:addi a6, a6, 2048
[0x8000b1c0]:add sp, sp, a6
[0x8000b1c4]:lw s10, 1368(sp)
[0x8000b1c8]:sub sp, sp, a6
[0x8000b1cc]:lui a6, 1
[0x8000b1d0]:addi a6, a6, 2048
[0x8000b1d4]:add sp, sp, a6
[0x8000b1d8]:lw s11, 1372(sp)
[0x8000b1dc]:sub sp, sp, a6
[0x8000b1e0]:addi t3, zero, 1
[0x8000b1e4]:lui t4, 524032
[0x8000b1e8]:addi s10, zero, 0
[0x8000b1ec]:lui s11, 524288
[0x8000b1f0]:addi a6, zero, 0
[0x8000b1f4]:csrrw zero, fcsr, a6
[0x8000b1f8]:fsgnj.d t5, t3, s10

[0x8000b1f8]:fsgnj.d t5, t3, s10
[0x8000b1fc]:csrrs gp, fcsr, zero
[0x8000b200]:sw t5, 696(ra)
[0x8000b204]:sw t6, 704(ra)
[0x8000b208]:sw t5, 712(ra)
[0x8000b20c]:sw gp, 720(ra)
[0x8000b210]:lui a6, 1
[0x8000b214]:addi a6, a6, 2048
[0x8000b218]:add sp, sp, a6
[0x8000b21c]:lw t3, 1376(sp)
[0x8000b220]:sub sp, sp, a6
[0x8000b224]:lui a6, 1
[0x8000b228]:addi a6, a6, 2048
[0x8000b22c]:add sp, sp, a6
[0x8000b230]:lw t4, 1380(sp)
[0x8000b234]:sub sp, sp, a6
[0x8000b238]:lui a6, 1
[0x8000b23c]:addi a6, a6, 2048
[0x8000b240]:add sp, sp, a6
[0x8000b244]:lw s10, 1384(sp)
[0x8000b248]:sub sp, sp, a6
[0x8000b24c]:lui a6, 1
[0x8000b250]:addi a6, a6, 2048
[0x8000b254]:add sp, sp, a6
[0x8000b258]:lw s11, 1388(sp)
[0x8000b25c]:sub sp, sp, a6
[0x8000b260]:addi t3, zero, 1
[0x8000b264]:lui t4, 524032
[0x8000b268]:addi s10, zero, 1
[0x8000b26c]:addi s11, zero, 0
[0x8000b270]:addi a6, zero, 0
[0x8000b274]:csrrw zero, fcsr, a6
[0x8000b278]:fsgnj.d t5, t3, s10

[0x8000b278]:fsgnj.d t5, t3, s10
[0x8000b27c]:csrrs gp, fcsr, zero
[0x8000b280]:sw t5, 728(ra)
[0x8000b284]:sw t6, 736(ra)
[0x8000b288]:sw t5, 744(ra)
[0x8000b28c]:sw gp, 752(ra)
[0x8000b290]:lui a6, 1
[0x8000b294]:addi a6, a6, 2048
[0x8000b298]:add sp, sp, a6
[0x8000b29c]:lw t3, 1392(sp)
[0x8000b2a0]:sub sp, sp, a6
[0x8000b2a4]:lui a6, 1
[0x8000b2a8]:addi a6, a6, 2048
[0x8000b2ac]:add sp, sp, a6
[0x8000b2b0]:lw t4, 1396(sp)
[0x8000b2b4]:sub sp, sp, a6
[0x8000b2b8]:lui a6, 1
[0x8000b2bc]:addi a6, a6, 2048
[0x8000b2c0]:add sp, sp, a6
[0x8000b2c4]:lw s10, 1400(sp)
[0x8000b2c8]:sub sp, sp, a6
[0x8000b2cc]:lui a6, 1
[0x8000b2d0]:addi a6, a6, 2048
[0x8000b2d4]:add sp, sp, a6
[0x8000b2d8]:lw s11, 1404(sp)
[0x8000b2dc]:sub sp, sp, a6
[0x8000b2e0]:addi t3, zero, 1
[0x8000b2e4]:lui t4, 524032
[0x8000b2e8]:addi s10, zero, 1
[0x8000b2ec]:lui s11, 524288
[0x8000b2f0]:addi a6, zero, 0
[0x8000b2f4]:csrrw zero, fcsr, a6
[0x8000b2f8]:fsgnj.d t5, t3, s10

[0x8000b2f8]:fsgnj.d t5, t3, s10
[0x8000b2fc]:csrrs gp, fcsr, zero
[0x8000b300]:sw t5, 760(ra)
[0x8000b304]:sw t6, 768(ra)
[0x8000b308]:sw t5, 776(ra)
[0x8000b30c]:sw gp, 784(ra)
[0x8000b310]:lui a6, 1
[0x8000b314]:addi a6, a6, 2048
[0x8000b318]:add sp, sp, a6
[0x8000b31c]:lw t3, 1408(sp)
[0x8000b320]:sub sp, sp, a6
[0x8000b324]:lui a6, 1
[0x8000b328]:addi a6, a6, 2048
[0x8000b32c]:add sp, sp, a6
[0x8000b330]:lw t4, 1412(sp)
[0x8000b334]:sub sp, sp, a6
[0x8000b338]:lui a6, 1
[0x8000b33c]:addi a6, a6, 2048
[0x8000b340]:add sp, sp, a6
[0x8000b344]:lw s10, 1416(sp)
[0x8000b348]:sub sp, sp, a6
[0x8000b34c]:lui a6, 1
[0x8000b350]:addi a6, a6, 2048
[0x8000b354]:add sp, sp, a6
[0x8000b358]:lw s11, 1420(sp)
[0x8000b35c]:sub sp, sp, a6
[0x8000b360]:addi t3, zero, 1
[0x8000b364]:lui t4, 524032
[0x8000b368]:addi s10, zero, 2
[0x8000b36c]:addi s11, zero, 0
[0x8000b370]:addi a6, zero, 0
[0x8000b374]:csrrw zero, fcsr, a6
[0x8000b378]:fsgnj.d t5, t3, s10

[0x8000b378]:fsgnj.d t5, t3, s10
[0x8000b37c]:csrrs gp, fcsr, zero
[0x8000b380]:sw t5, 792(ra)
[0x8000b384]:sw t6, 800(ra)
[0x8000b388]:sw t5, 808(ra)
[0x8000b38c]:sw gp, 816(ra)
[0x8000b390]:lui a6, 1
[0x8000b394]:addi a6, a6, 2048
[0x8000b398]:add sp, sp, a6
[0x8000b39c]:lw t3, 1424(sp)
[0x8000b3a0]:sub sp, sp, a6
[0x8000b3a4]:lui a6, 1
[0x8000b3a8]:addi a6, a6, 2048
[0x8000b3ac]:add sp, sp, a6
[0x8000b3b0]:lw t4, 1428(sp)
[0x8000b3b4]:sub sp, sp, a6
[0x8000b3b8]:lui a6, 1
[0x8000b3bc]:addi a6, a6, 2048
[0x8000b3c0]:add sp, sp, a6
[0x8000b3c4]:lw s10, 1432(sp)
[0x8000b3c8]:sub sp, sp, a6
[0x8000b3cc]:lui a6, 1
[0x8000b3d0]:addi a6, a6, 2048
[0x8000b3d4]:add sp, sp, a6
[0x8000b3d8]:lw s11, 1436(sp)
[0x8000b3dc]:sub sp, sp, a6
[0x8000b3e0]:addi t3, zero, 1
[0x8000b3e4]:lui t4, 524032
[0x8000b3e8]:addi s10, zero, 2
[0x8000b3ec]:lui s11, 524288
[0x8000b3f0]:addi a6, zero, 0
[0x8000b3f4]:csrrw zero, fcsr, a6
[0x8000b3f8]:fsgnj.d t5, t3, s10

[0x8000b3f8]:fsgnj.d t5, t3, s10
[0x8000b3fc]:csrrs gp, fcsr, zero
[0x8000b400]:sw t5, 824(ra)
[0x8000b404]:sw t6, 832(ra)
[0x8000b408]:sw t5, 840(ra)
[0x8000b40c]:sw gp, 848(ra)
[0x8000b410]:lui a6, 1
[0x8000b414]:addi a6, a6, 2048
[0x8000b418]:add sp, sp, a6
[0x8000b41c]:lw t3, 1440(sp)
[0x8000b420]:sub sp, sp, a6
[0x8000b424]:lui a6, 1
[0x8000b428]:addi a6, a6, 2048
[0x8000b42c]:add sp, sp, a6
[0x8000b430]:lw t4, 1444(sp)
[0x8000b434]:sub sp, sp, a6
[0x8000b438]:lui a6, 1
[0x8000b43c]:addi a6, a6, 2048
[0x8000b440]:add sp, sp, a6
[0x8000b444]:lw s10, 1448(sp)
[0x8000b448]:sub sp, sp, a6
[0x8000b44c]:lui a6, 1
[0x8000b450]:addi a6, a6, 2048
[0x8000b454]:add sp, sp, a6
[0x8000b458]:lw s11, 1452(sp)
[0x8000b45c]:sub sp, sp, a6
[0x8000b460]:addi t3, zero, 1
[0x8000b464]:lui t4, 524032
[0x8000b468]:addi s10, zero, 4095
[0x8000b46c]:lui s11, 256
[0x8000b470]:addi s11, s11, 4095
[0x8000b474]:addi a6, zero, 0
[0x8000b478]:csrrw zero, fcsr, a6
[0x8000b47c]:fsgnj.d t5, t3, s10

[0x8000b47c]:fsgnj.d t5, t3, s10
[0x8000b480]:csrrs gp, fcsr, zero
[0x8000b484]:sw t5, 856(ra)
[0x8000b488]:sw t6, 864(ra)
[0x8000b48c]:sw t5, 872(ra)
[0x8000b490]:sw gp, 880(ra)
[0x8000b494]:lui a6, 1
[0x8000b498]:addi a6, a6, 2048
[0x8000b49c]:add sp, sp, a6
[0x8000b4a0]:lw t3, 1456(sp)
[0x8000b4a4]:sub sp, sp, a6
[0x8000b4a8]:lui a6, 1
[0x8000b4ac]:addi a6, a6, 2048
[0x8000b4b0]:add sp, sp, a6
[0x8000b4b4]:lw t4, 1460(sp)
[0x8000b4b8]:sub sp, sp, a6
[0x8000b4bc]:lui a6, 1
[0x8000b4c0]:addi a6, a6, 2048
[0x8000b4c4]:add sp, sp, a6
[0x8000b4c8]:lw s10, 1464(sp)
[0x8000b4cc]:sub sp, sp, a6
[0x8000b4d0]:lui a6, 1
[0x8000b4d4]:addi a6, a6, 2048
[0x8000b4d8]:add sp, sp, a6
[0x8000b4dc]:lw s11, 1468(sp)
[0x8000b4e0]:sub sp, sp, a6
[0x8000b4e4]:addi t3, zero, 1
[0x8000b4e8]:lui t4, 524032
[0x8000b4ec]:addi s10, zero, 4095
[0x8000b4f0]:lui s11, 524544
[0x8000b4f4]:addi s11, s11, 4095
[0x8000b4f8]:addi a6, zero, 0
[0x8000b4fc]:csrrw zero, fcsr, a6
[0x8000b500]:fsgnj.d t5, t3, s10

[0x8000b500]:fsgnj.d t5, t3, s10
[0x8000b504]:csrrs gp, fcsr, zero
[0x8000b508]:sw t5, 888(ra)
[0x8000b50c]:sw t6, 896(ra)
[0x8000b510]:sw t5, 904(ra)
[0x8000b514]:sw gp, 912(ra)
[0x8000b518]:lui a6, 1
[0x8000b51c]:addi a6, a6, 2048
[0x8000b520]:add sp, sp, a6
[0x8000b524]:lw t3, 1472(sp)
[0x8000b528]:sub sp, sp, a6
[0x8000b52c]:lui a6, 1
[0x8000b530]:addi a6, a6, 2048
[0x8000b534]:add sp, sp, a6
[0x8000b538]:lw t4, 1476(sp)
[0x8000b53c]:sub sp, sp, a6
[0x8000b540]:lui a6, 1
[0x8000b544]:addi a6, a6, 2048
[0x8000b548]:add sp, sp, a6
[0x8000b54c]:lw s10, 1480(sp)
[0x8000b550]:sub sp, sp, a6
[0x8000b554]:lui a6, 1
[0x8000b558]:addi a6, a6, 2048
[0x8000b55c]:add sp, sp, a6
[0x8000b560]:lw s11, 1484(sp)
[0x8000b564]:sub sp, sp, a6
[0x8000b568]:addi t3, zero, 1
[0x8000b56c]:lui t4, 524032
[0x8000b570]:addi s10, zero, 0
[0x8000b574]:lui s11, 256
[0x8000b578]:addi a6, zero, 0
[0x8000b57c]:csrrw zero, fcsr, a6
[0x8000b580]:fsgnj.d t5, t3, s10

[0x8000b580]:fsgnj.d t5, t3, s10
[0x8000b584]:csrrs gp, fcsr, zero
[0x8000b588]:sw t5, 920(ra)
[0x8000b58c]:sw t6, 928(ra)
[0x8000b590]:sw t5, 936(ra)
[0x8000b594]:sw gp, 944(ra)
[0x8000b598]:lui a6, 1
[0x8000b59c]:addi a6, a6, 2048
[0x8000b5a0]:add sp, sp, a6
[0x8000b5a4]:lw t3, 1488(sp)
[0x8000b5a8]:sub sp, sp, a6
[0x8000b5ac]:lui a6, 1
[0x8000b5b0]:addi a6, a6, 2048
[0x8000b5b4]:add sp, sp, a6
[0x8000b5b8]:lw t4, 1492(sp)
[0x8000b5bc]:sub sp, sp, a6
[0x8000b5c0]:lui a6, 1
[0x8000b5c4]:addi a6, a6, 2048
[0x8000b5c8]:add sp, sp, a6
[0x8000b5cc]:lw s10, 1496(sp)
[0x8000b5d0]:sub sp, sp, a6
[0x8000b5d4]:lui a6, 1
[0x8000b5d8]:addi a6, a6, 2048
[0x8000b5dc]:add sp, sp, a6
[0x8000b5e0]:lw s11, 1500(sp)
[0x8000b5e4]:sub sp, sp, a6
[0x8000b5e8]:addi t3, zero, 1
[0x8000b5ec]:lui t4, 524032
[0x8000b5f0]:addi s10, zero, 0
[0x8000b5f4]:lui s11, 524544
[0x8000b5f8]:addi a6, zero, 0
[0x8000b5fc]:csrrw zero, fcsr, a6
[0x8000b600]:fsgnj.d t5, t3, s10

[0x8000b600]:fsgnj.d t5, t3, s10
[0x8000b604]:csrrs gp, fcsr, zero
[0x8000b608]:sw t5, 952(ra)
[0x8000b60c]:sw t6, 960(ra)
[0x8000b610]:sw t5, 968(ra)
[0x8000b614]:sw gp, 976(ra)
[0x8000b618]:lui a6, 1
[0x8000b61c]:addi a6, a6, 2048
[0x8000b620]:add sp, sp, a6
[0x8000b624]:lw t3, 1504(sp)
[0x8000b628]:sub sp, sp, a6
[0x8000b62c]:lui a6, 1
[0x8000b630]:addi a6, a6, 2048
[0x8000b634]:add sp, sp, a6
[0x8000b638]:lw t4, 1508(sp)
[0x8000b63c]:sub sp, sp, a6
[0x8000b640]:lui a6, 1
[0x8000b644]:addi a6, a6, 2048
[0x8000b648]:add sp, sp, a6
[0x8000b64c]:lw s10, 1512(sp)
[0x8000b650]:sub sp, sp, a6
[0x8000b654]:lui a6, 1
[0x8000b658]:addi a6, a6, 2048
[0x8000b65c]:add sp, sp, a6
[0x8000b660]:lw s11, 1516(sp)
[0x8000b664]:sub sp, sp, a6
[0x8000b668]:addi t3, zero, 1
[0x8000b66c]:lui t4, 524032
[0x8000b670]:addi s10, zero, 2
[0x8000b674]:lui s11, 256
[0x8000b678]:addi a6, zero, 0
[0x8000b67c]:csrrw zero, fcsr, a6
[0x8000b680]:fsgnj.d t5, t3, s10

[0x8000b680]:fsgnj.d t5, t3, s10
[0x8000b684]:csrrs gp, fcsr, zero
[0x8000b688]:sw t5, 984(ra)
[0x8000b68c]:sw t6, 992(ra)
[0x8000b690]:sw t5, 1000(ra)
[0x8000b694]:sw gp, 1008(ra)
[0x8000b698]:lui a6, 1
[0x8000b69c]:addi a6, a6, 2048
[0x8000b6a0]:add sp, sp, a6
[0x8000b6a4]:lw t3, 1520(sp)
[0x8000b6a8]:sub sp, sp, a6
[0x8000b6ac]:lui a6, 1
[0x8000b6b0]:addi a6, a6, 2048
[0x8000b6b4]:add sp, sp, a6
[0x8000b6b8]:lw t4, 1524(sp)
[0x8000b6bc]:sub sp, sp, a6
[0x8000b6c0]:lui a6, 1
[0x8000b6c4]:addi a6, a6, 2048
[0x8000b6c8]:add sp, sp, a6
[0x8000b6cc]:lw s10, 1528(sp)
[0x8000b6d0]:sub sp, sp, a6
[0x8000b6d4]:lui a6, 1
[0x8000b6d8]:addi a6, a6, 2048
[0x8000b6dc]:add sp, sp, a6
[0x8000b6e0]:lw s11, 1532(sp)
[0x8000b6e4]:sub sp, sp, a6
[0x8000b6e8]:addi t3, zero, 1
[0x8000b6ec]:lui t4, 524032
[0x8000b6f0]:addi s10, zero, 2
[0x8000b6f4]:lui s11, 524544
[0x8000b6f8]:addi a6, zero, 0
[0x8000b6fc]:csrrw zero, fcsr, a6
[0x8000b700]:fsgnj.d t5, t3, s10

[0x8000b700]:fsgnj.d t5, t3, s10
[0x8000b704]:csrrs gp, fcsr, zero
[0x8000b708]:sw t5, 1016(ra)
[0x8000b70c]:sw t6, 1024(ra)
[0x8000b710]:sw t5, 1032(ra)
[0x8000b714]:sw gp, 1040(ra)
[0x8000b718]:lui a6, 1
[0x8000b71c]:addi a6, a6, 2048
[0x8000b720]:add sp, sp, a6
[0x8000b724]:lw t3, 1536(sp)
[0x8000b728]:sub sp, sp, a6
[0x8000b72c]:lui a6, 1
[0x8000b730]:addi a6, a6, 2048
[0x8000b734]:add sp, sp, a6
[0x8000b738]:lw t4, 1540(sp)
[0x8000b73c]:sub sp, sp, a6
[0x8000b740]:lui a6, 1
[0x8000b744]:addi a6, a6, 2048
[0x8000b748]:add sp, sp, a6
[0x8000b74c]:lw s10, 1544(sp)
[0x8000b750]:sub sp, sp, a6
[0x8000b754]:lui a6, 1
[0x8000b758]:addi a6, a6, 2048
[0x8000b75c]:add sp, sp, a6
[0x8000b760]:lw s11, 1548(sp)
[0x8000b764]:sub sp, sp, a6
[0x8000b768]:addi t3, zero, 1
[0x8000b76c]:lui t4, 524032
[0x8000b770]:addi s10, zero, 4095
[0x8000b774]:lui s11, 524032
[0x8000b778]:addi s11, s11, 4095
[0x8000b77c]:addi a6, zero, 0
[0x8000b780]:csrrw zero, fcsr, a6
[0x8000b784]:fsgnj.d t5, t3, s10

[0x8000b784]:fsgnj.d t5, t3, s10
[0x8000b788]:csrrs gp, fcsr, zero
[0x8000b78c]:sw t5, 1048(ra)
[0x8000b790]:sw t6, 1056(ra)
[0x8000b794]:sw t5, 1064(ra)
[0x8000b798]:sw gp, 1072(ra)
[0x8000b79c]:lui a6, 1
[0x8000b7a0]:addi a6, a6, 2048
[0x8000b7a4]:add sp, sp, a6
[0x8000b7a8]:lw t3, 1552(sp)
[0x8000b7ac]:sub sp, sp, a6
[0x8000b7b0]:lui a6, 1
[0x8000b7b4]:addi a6, a6, 2048
[0x8000b7b8]:add sp, sp, a6
[0x8000b7bc]:lw t4, 1556(sp)
[0x8000b7c0]:sub sp, sp, a6
[0x8000b7c4]:lui a6, 1
[0x8000b7c8]:addi a6, a6, 2048
[0x8000b7cc]:add sp, sp, a6
[0x8000b7d0]:lw s10, 1560(sp)
[0x8000b7d4]:sub sp, sp, a6
[0x8000b7d8]:lui a6, 1
[0x8000b7dc]:addi a6, a6, 2048
[0x8000b7e0]:add sp, sp, a6
[0x8000b7e4]:lw s11, 1564(sp)
[0x8000b7e8]:sub sp, sp, a6
[0x8000b7ec]:addi t3, zero, 1
[0x8000b7f0]:lui t4, 524032
[0x8000b7f4]:addi s10, zero, 4095
[0x8000b7f8]:lui s11, 1048320
[0x8000b7fc]:addi s11, s11, 4095
[0x8000b800]:addi a6, zero, 0
[0x8000b804]:csrrw zero, fcsr, a6
[0x8000b808]:fsgnj.d t5, t3, s10

[0x8000b808]:fsgnj.d t5, t3, s10
[0x8000b80c]:csrrs gp, fcsr, zero
[0x8000b810]:sw t5, 1080(ra)
[0x8000b814]:sw t6, 1088(ra)
[0x8000b818]:sw t5, 1096(ra)
[0x8000b81c]:sw gp, 1104(ra)
[0x8000b820]:lui a6, 1
[0x8000b824]:addi a6, a6, 2048
[0x8000b828]:add sp, sp, a6
[0x8000b82c]:lw t3, 1568(sp)
[0x8000b830]:sub sp, sp, a6
[0x8000b834]:lui a6, 1
[0x8000b838]:addi a6, a6, 2048
[0x8000b83c]:add sp, sp, a6
[0x8000b840]:lw t4, 1572(sp)
[0x8000b844]:sub sp, sp, a6
[0x8000b848]:lui a6, 1
[0x8000b84c]:addi a6, a6, 2048
[0x8000b850]:add sp, sp, a6
[0x8000b854]:lw s10, 1576(sp)
[0x8000b858]:sub sp, sp, a6
[0x8000b85c]:lui a6, 1
[0x8000b860]:addi a6, a6, 2048
[0x8000b864]:add sp, sp, a6
[0x8000b868]:lw s11, 1580(sp)
[0x8000b86c]:sub sp, sp, a6
[0x8000b870]:addi t3, zero, 1
[0x8000b874]:lui t4, 524032
[0x8000b878]:addi s10, zero, 0
[0x8000b87c]:lui s11, 524032
[0x8000b880]:addi a6, zero, 0
[0x8000b884]:csrrw zero, fcsr, a6
[0x8000b888]:fsgnj.d t5, t3, s10

[0x8000b888]:fsgnj.d t5, t3, s10
[0x8000b88c]:csrrs gp, fcsr, zero
[0x8000b890]:sw t5, 1112(ra)
[0x8000b894]:sw t6, 1120(ra)
[0x8000b898]:sw t5, 1128(ra)
[0x8000b89c]:sw gp, 1136(ra)
[0x8000b8a0]:lui a6, 1
[0x8000b8a4]:addi a6, a6, 2048
[0x8000b8a8]:add sp, sp, a6
[0x8000b8ac]:lw t3, 1584(sp)
[0x8000b8b0]:sub sp, sp, a6
[0x8000b8b4]:lui a6, 1
[0x8000b8b8]:addi a6, a6, 2048
[0x8000b8bc]:add sp, sp, a6
[0x8000b8c0]:lw t4, 1588(sp)
[0x8000b8c4]:sub sp, sp, a6
[0x8000b8c8]:lui a6, 1
[0x8000b8cc]:addi a6, a6, 2048
[0x8000b8d0]:add sp, sp, a6
[0x8000b8d4]:lw s10, 1592(sp)
[0x8000b8d8]:sub sp, sp, a6
[0x8000b8dc]:lui a6, 1
[0x8000b8e0]:addi a6, a6, 2048
[0x8000b8e4]:add sp, sp, a6
[0x8000b8e8]:lw s11, 1596(sp)
[0x8000b8ec]:sub sp, sp, a6
[0x8000b8f0]:addi t3, zero, 1
[0x8000b8f4]:lui t4, 524032
[0x8000b8f8]:addi s10, zero, 0
[0x8000b8fc]:lui s11, 1048320
[0x8000b900]:addi a6, zero, 0
[0x8000b904]:csrrw zero, fcsr, a6
[0x8000b908]:fsgnj.d t5, t3, s10

[0x8000b908]:fsgnj.d t5, t3, s10
[0x8000b90c]:csrrs gp, fcsr, zero
[0x8000b910]:sw t5, 1144(ra)
[0x8000b914]:sw t6, 1152(ra)
[0x8000b918]:sw t5, 1160(ra)
[0x8000b91c]:sw gp, 1168(ra)
[0x8000b920]:lui a6, 1
[0x8000b924]:addi a6, a6, 2048
[0x8000b928]:add sp, sp, a6
[0x8000b92c]:lw t3, 1600(sp)
[0x8000b930]:sub sp, sp, a6
[0x8000b934]:lui a6, 1
[0x8000b938]:addi a6, a6, 2048
[0x8000b93c]:add sp, sp, a6
[0x8000b940]:lw t4, 1604(sp)
[0x8000b944]:sub sp, sp, a6
[0x8000b948]:lui a6, 1
[0x8000b94c]:addi a6, a6, 2048
[0x8000b950]:add sp, sp, a6
[0x8000b954]:lw s10, 1608(sp)
[0x8000b958]:sub sp, sp, a6
[0x8000b95c]:lui a6, 1
[0x8000b960]:addi a6, a6, 2048
[0x8000b964]:add sp, sp, a6
[0x8000b968]:lw s11, 1612(sp)
[0x8000b96c]:sub sp, sp, a6
[0x8000b970]:addi t3, zero, 1
[0x8000b974]:lui t4, 524032
[0x8000b978]:addi s10, zero, 0
[0x8000b97c]:lui s11, 524160
[0x8000b980]:addi a6, zero, 0
[0x8000b984]:csrrw zero, fcsr, a6
[0x8000b988]:fsgnj.d t5, t3, s10

[0x8000b988]:fsgnj.d t5, t3, s10
[0x8000b98c]:csrrs gp, fcsr, zero
[0x8000b990]:sw t5, 1176(ra)
[0x8000b994]:sw t6, 1184(ra)
[0x8000b998]:sw t5, 1192(ra)
[0x8000b99c]:sw gp, 1200(ra)
[0x8000b9a0]:lui a6, 1
[0x8000b9a4]:addi a6, a6, 2048
[0x8000b9a8]:add sp, sp, a6
[0x8000b9ac]:lw t3, 1616(sp)
[0x8000b9b0]:sub sp, sp, a6
[0x8000b9b4]:lui a6, 1
[0x8000b9b8]:addi a6, a6, 2048
[0x8000b9bc]:add sp, sp, a6
[0x8000b9c0]:lw t4, 1620(sp)
[0x8000b9c4]:sub sp, sp, a6
[0x8000b9c8]:lui a6, 1
[0x8000b9cc]:addi a6, a6, 2048
[0x8000b9d0]:add sp, sp, a6
[0x8000b9d4]:lw s10, 1624(sp)
[0x8000b9d8]:sub sp, sp, a6
[0x8000b9dc]:lui a6, 1
[0x8000b9e0]:addi a6, a6, 2048
[0x8000b9e4]:add sp, sp, a6
[0x8000b9e8]:lw s11, 1628(sp)
[0x8000b9ec]:sub sp, sp, a6
[0x8000b9f0]:addi t3, zero, 1
[0x8000b9f4]:lui t4, 524032
[0x8000b9f8]:addi s10, zero, 0
[0x8000b9fc]:lui s11, 1048448
[0x8000ba00]:addi a6, zero, 0
[0x8000ba04]:csrrw zero, fcsr, a6
[0x8000ba08]:fsgnj.d t5, t3, s10

[0x8000ba08]:fsgnj.d t5, t3, s10
[0x8000ba0c]:csrrs gp, fcsr, zero
[0x8000ba10]:sw t5, 1208(ra)
[0x8000ba14]:sw t6, 1216(ra)
[0x8000ba18]:sw t5, 1224(ra)
[0x8000ba1c]:sw gp, 1232(ra)
[0x8000ba20]:lui a6, 1
[0x8000ba24]:addi a6, a6, 2048
[0x8000ba28]:add sp, sp, a6
[0x8000ba2c]:lw t3, 1632(sp)
[0x8000ba30]:sub sp, sp, a6
[0x8000ba34]:lui a6, 1
[0x8000ba38]:addi a6, a6, 2048
[0x8000ba3c]:add sp, sp, a6
[0x8000ba40]:lw t4, 1636(sp)
[0x8000ba44]:sub sp, sp, a6
[0x8000ba48]:lui a6, 1
[0x8000ba4c]:addi a6, a6, 2048
[0x8000ba50]:add sp, sp, a6
[0x8000ba54]:lw s10, 1640(sp)
[0x8000ba58]:sub sp, sp, a6
[0x8000ba5c]:lui a6, 1
[0x8000ba60]:addi a6, a6, 2048
[0x8000ba64]:add sp, sp, a6
[0x8000ba68]:lw s11, 1644(sp)
[0x8000ba6c]:sub sp, sp, a6
[0x8000ba70]:addi t3, zero, 1
[0x8000ba74]:lui t4, 524032
[0x8000ba78]:addi s10, zero, 1
[0x8000ba7c]:lui s11, 524160
[0x8000ba80]:addi a6, zero, 0
[0x8000ba84]:csrrw zero, fcsr, a6
[0x8000ba88]:fsgnj.d t5, t3, s10

[0x8000ba88]:fsgnj.d t5, t3, s10
[0x8000ba8c]:csrrs gp, fcsr, zero
[0x8000ba90]:sw t5, 1240(ra)
[0x8000ba94]:sw t6, 1248(ra)
[0x8000ba98]:sw t5, 1256(ra)
[0x8000ba9c]:sw gp, 1264(ra)
[0x8000baa0]:lui a6, 1
[0x8000baa4]:addi a6, a6, 2048
[0x8000baa8]:add sp, sp, a6
[0x8000baac]:lw t3, 1648(sp)
[0x8000bab0]:sub sp, sp, a6
[0x8000bab4]:lui a6, 1
[0x8000bab8]:addi a6, a6, 2048
[0x8000babc]:add sp, sp, a6
[0x8000bac0]:lw t4, 1652(sp)
[0x8000bac4]:sub sp, sp, a6
[0x8000bac8]:lui a6, 1
[0x8000bacc]:addi a6, a6, 2048
[0x8000bad0]:add sp, sp, a6
[0x8000bad4]:lw s10, 1656(sp)
[0x8000bad8]:sub sp, sp, a6
[0x8000badc]:lui a6, 1
[0x8000bae0]:addi a6, a6, 2048
[0x8000bae4]:add sp, sp, a6
[0x8000bae8]:lw s11, 1660(sp)
[0x8000baec]:sub sp, sp, a6
[0x8000baf0]:addi t3, zero, 1
[0x8000baf4]:lui t4, 524032
[0x8000baf8]:addi s10, zero, 1
[0x8000bafc]:lui s11, 1048448
[0x8000bb00]:addi a6, zero, 0
[0x8000bb04]:csrrw zero, fcsr, a6
[0x8000bb08]:fsgnj.d t5, t3, s10

[0x8000bb08]:fsgnj.d t5, t3, s10
[0x8000bb0c]:csrrs gp, fcsr, zero
[0x8000bb10]:sw t5, 1272(ra)
[0x8000bb14]:sw t6, 1280(ra)
[0x8000bb18]:sw t5, 1288(ra)
[0x8000bb1c]:sw gp, 1296(ra)
[0x8000bb20]:lui a6, 1
[0x8000bb24]:addi a6, a6, 2048
[0x8000bb28]:add sp, sp, a6
[0x8000bb2c]:lw t3, 1664(sp)
[0x8000bb30]:sub sp, sp, a6
[0x8000bb34]:lui a6, 1
[0x8000bb38]:addi a6, a6, 2048
[0x8000bb3c]:add sp, sp, a6
[0x8000bb40]:lw t4, 1668(sp)
[0x8000bb44]:sub sp, sp, a6
[0x8000bb48]:lui a6, 1
[0x8000bb4c]:addi a6, a6, 2048
[0x8000bb50]:add sp, sp, a6
[0x8000bb54]:lw s10, 1672(sp)
[0x8000bb58]:sub sp, sp, a6
[0x8000bb5c]:lui a6, 1
[0x8000bb60]:addi a6, a6, 2048
[0x8000bb64]:add sp, sp, a6
[0x8000bb68]:lw s11, 1676(sp)
[0x8000bb6c]:sub sp, sp, a6
[0x8000bb70]:addi t3, zero, 1
[0x8000bb74]:lui t4, 524032
[0x8000bb78]:addi s10, zero, 1
[0x8000bb7c]:lui s11, 524032
[0x8000bb80]:addi a6, zero, 0
[0x8000bb84]:csrrw zero, fcsr, a6
[0x8000bb88]:fsgnj.d t5, t3, s10

[0x8000bb88]:fsgnj.d t5, t3, s10
[0x8000bb8c]:csrrs gp, fcsr, zero
[0x8000bb90]:sw t5, 1304(ra)
[0x8000bb94]:sw t6, 1312(ra)
[0x8000bb98]:sw t5, 1320(ra)
[0x8000bb9c]:sw gp, 1328(ra)
[0x8000bba0]:lui a6, 1
[0x8000bba4]:addi a6, a6, 2048
[0x8000bba8]:add sp, sp, a6
[0x8000bbac]:lw t3, 1680(sp)
[0x8000bbb0]:sub sp, sp, a6
[0x8000bbb4]:lui a6, 1
[0x8000bbb8]:addi a6, a6, 2048
[0x8000bbbc]:add sp, sp, a6
[0x8000bbc0]:lw t4, 1684(sp)
[0x8000bbc4]:sub sp, sp, a6
[0x8000bbc8]:lui a6, 1
[0x8000bbcc]:addi a6, a6, 2048
[0x8000bbd0]:add sp, sp, a6
[0x8000bbd4]:lw s10, 1688(sp)
[0x8000bbd8]:sub sp, sp, a6
[0x8000bbdc]:lui a6, 1
[0x8000bbe0]:addi a6, a6, 2048
[0x8000bbe4]:add sp, sp, a6
[0x8000bbe8]:lw s11, 1692(sp)
[0x8000bbec]:sub sp, sp, a6
[0x8000bbf0]:addi t3, zero, 1
[0x8000bbf4]:lui t4, 524032
[0x8000bbf8]:addi s10, zero, 1
[0x8000bbfc]:lui s11, 1048320
[0x8000bc00]:addi a6, zero, 0
[0x8000bc04]:csrrw zero, fcsr, a6
[0x8000bc08]:fsgnj.d t5, t3, s10

[0x8000bc08]:fsgnj.d t5, t3, s10
[0x8000bc0c]:csrrs gp, fcsr, zero
[0x8000bc10]:sw t5, 1336(ra)
[0x8000bc14]:sw t6, 1344(ra)
[0x8000bc18]:sw t5, 1352(ra)
[0x8000bc1c]:sw gp, 1360(ra)
[0x8000bc20]:lui a6, 1
[0x8000bc24]:addi a6, a6, 2048
[0x8000bc28]:add sp, sp, a6
[0x8000bc2c]:lw t3, 1696(sp)
[0x8000bc30]:sub sp, sp, a6
[0x8000bc34]:lui a6, 1
[0x8000bc38]:addi a6, a6, 2048
[0x8000bc3c]:add sp, sp, a6
[0x8000bc40]:lw t4, 1700(sp)
[0x8000bc44]:sub sp, sp, a6
[0x8000bc48]:lui a6, 1
[0x8000bc4c]:addi a6, a6, 2048
[0x8000bc50]:add sp, sp, a6
[0x8000bc54]:lw s10, 1704(sp)
[0x8000bc58]:sub sp, sp, a6
[0x8000bc5c]:lui a6, 1
[0x8000bc60]:addi a6, a6, 2048
[0x8000bc64]:add sp, sp, a6
[0x8000bc68]:lw s11, 1708(sp)
[0x8000bc6c]:sub sp, sp, a6
[0x8000bc70]:addi t3, zero, 1
[0x8000bc74]:lui t4, 524032
[0x8000bc78]:addi s10, zero, 0
[0x8000bc7c]:lui s11, 261888
[0x8000bc80]:addi a6, zero, 0
[0x8000bc84]:csrrw zero, fcsr, a6
[0x8000bc88]:fsgnj.d t5, t3, s10

[0x8000bc88]:fsgnj.d t5, t3, s10
[0x8000bc8c]:csrrs gp, fcsr, zero
[0x8000bc90]:sw t5, 1368(ra)
[0x8000bc94]:sw t6, 1376(ra)
[0x8000bc98]:sw t5, 1384(ra)
[0x8000bc9c]:sw gp, 1392(ra)
[0x8000bca0]:lui a6, 1
[0x8000bca4]:addi a6, a6, 2048
[0x8000bca8]:add sp, sp, a6
[0x8000bcac]:lw t3, 1712(sp)
[0x8000bcb0]:sub sp, sp, a6
[0x8000bcb4]:lui a6, 1
[0x8000bcb8]:addi a6, a6, 2048
[0x8000bcbc]:add sp, sp, a6
[0x8000bcc0]:lw t4, 1716(sp)
[0x8000bcc4]:sub sp, sp, a6
[0x8000bcc8]:lui a6, 1
[0x8000bccc]:addi a6, a6, 2048
[0x8000bcd0]:add sp, sp, a6
[0x8000bcd4]:lw s10, 1720(sp)
[0x8000bcd8]:sub sp, sp, a6
[0x8000bcdc]:lui a6, 1
[0x8000bce0]:addi a6, a6, 2048
[0x8000bce4]:add sp, sp, a6
[0x8000bce8]:lw s11, 1724(sp)
[0x8000bcec]:sub sp, sp, a6
[0x8000bcf0]:addi t3, zero, 1
[0x8000bcf4]:lui t4, 524032
[0x8000bcf8]:addi s10, zero, 0
[0x8000bcfc]:lui s11, 784384
[0x8000bd00]:addi a6, zero, 0
[0x8000bd04]:csrrw zero, fcsr, a6
[0x8000bd08]:fsgnj.d t5, t3, s10

[0x8000bd08]:fsgnj.d t5, t3, s10
[0x8000bd0c]:csrrs gp, fcsr, zero
[0x8000bd10]:sw t5, 1400(ra)
[0x8000bd14]:sw t6, 1408(ra)
[0x8000bd18]:sw t5, 1416(ra)
[0x8000bd1c]:sw gp, 1424(ra)
[0x8000bd20]:lui a6, 1
[0x8000bd24]:addi a6, a6, 2048
[0x8000bd28]:add sp, sp, a6
[0x8000bd2c]:lw t3, 1728(sp)
[0x8000bd30]:sub sp, sp, a6
[0x8000bd34]:lui a6, 1
[0x8000bd38]:addi a6, a6, 2048
[0x8000bd3c]:add sp, sp, a6
[0x8000bd40]:lw t4, 1732(sp)
[0x8000bd44]:sub sp, sp, a6
[0x8000bd48]:lui a6, 1
[0x8000bd4c]:addi a6, a6, 2048
[0x8000bd50]:add sp, sp, a6
[0x8000bd54]:lw s10, 1736(sp)
[0x8000bd58]:sub sp, sp, a6
[0x8000bd5c]:lui a6, 1
[0x8000bd60]:addi a6, a6, 2048
[0x8000bd64]:add sp, sp, a6
[0x8000bd68]:lw s11, 1740(sp)
[0x8000bd6c]:sub sp, sp, a6
[0x8000bd70]:addi t3, zero, 1
[0x8000bd74]:lui t4, 1048320
[0x8000bd78]:addi s10, zero, 0
[0x8000bd7c]:addi s11, zero, 0
[0x8000bd80]:addi a6, zero, 0
[0x8000bd84]:csrrw zero, fcsr, a6
[0x8000bd88]:fsgnj.d t5, t3, s10

[0x8000bd88]:fsgnj.d t5, t3, s10
[0x8000bd8c]:csrrs gp, fcsr, zero
[0x8000bd90]:sw t5, 1432(ra)
[0x8000bd94]:sw t6, 1440(ra)
[0x8000bd98]:sw t5, 1448(ra)
[0x8000bd9c]:sw gp, 1456(ra)
[0x8000bda0]:lui a6, 1
[0x8000bda4]:addi a6, a6, 2048
[0x8000bda8]:add sp, sp, a6
[0x8000bdac]:lw t3, 1744(sp)
[0x8000bdb0]:sub sp, sp, a6
[0x8000bdb4]:lui a6, 1
[0x8000bdb8]:addi a6, a6, 2048
[0x8000bdbc]:add sp, sp, a6
[0x8000bdc0]:lw t4, 1748(sp)
[0x8000bdc4]:sub sp, sp, a6
[0x8000bdc8]:lui a6, 1
[0x8000bdcc]:addi a6, a6, 2048
[0x8000bdd0]:add sp, sp, a6
[0x8000bdd4]:lw s10, 1752(sp)
[0x8000bdd8]:sub sp, sp, a6
[0x8000bddc]:lui a6, 1
[0x8000bde0]:addi a6, a6, 2048
[0x8000bde4]:add sp, sp, a6
[0x8000bde8]:lw s11, 1756(sp)
[0x8000bdec]:sub sp, sp, a6
[0x8000bdf0]:addi t3, zero, 1
[0x8000bdf4]:lui t4, 1048320
[0x8000bdf8]:addi s10, zero, 0
[0x8000bdfc]:lui s11, 524288
[0x8000be00]:addi a6, zero, 0
[0x8000be04]:csrrw zero, fcsr, a6
[0x8000be08]:fsgnj.d t5, t3, s10

[0x8000be08]:fsgnj.d t5, t3, s10
[0x8000be0c]:csrrs gp, fcsr, zero
[0x8000be10]:sw t5, 1464(ra)
[0x8000be14]:sw t6, 1472(ra)
[0x8000be18]:sw t5, 1480(ra)
[0x8000be1c]:sw gp, 1488(ra)
[0x8000be20]:lui a6, 1
[0x8000be24]:addi a6, a6, 2048
[0x8000be28]:add sp, sp, a6
[0x8000be2c]:lw t3, 1760(sp)
[0x8000be30]:sub sp, sp, a6
[0x8000be34]:lui a6, 1
[0x8000be38]:addi a6, a6, 2048
[0x8000be3c]:add sp, sp, a6
[0x8000be40]:lw t4, 1764(sp)
[0x8000be44]:sub sp, sp, a6
[0x8000be48]:lui a6, 1
[0x8000be4c]:addi a6, a6, 2048
[0x8000be50]:add sp, sp, a6
[0x8000be54]:lw s10, 1768(sp)
[0x8000be58]:sub sp, sp, a6
[0x8000be5c]:lui a6, 1
[0x8000be60]:addi a6, a6, 2048
[0x8000be64]:add sp, sp, a6
[0x8000be68]:lw s11, 1772(sp)
[0x8000be6c]:sub sp, sp, a6
[0x8000be70]:addi t3, zero, 1
[0x8000be74]:lui t4, 1048320
[0x8000be78]:addi s10, zero, 1
[0x8000be7c]:addi s11, zero, 0
[0x8000be80]:addi a6, zero, 0
[0x8000be84]:csrrw zero, fcsr, a6
[0x8000be88]:fsgnj.d t5, t3, s10

[0x8000be88]:fsgnj.d t5, t3, s10
[0x8000be8c]:csrrs gp, fcsr, zero
[0x8000be90]:sw t5, 1496(ra)
[0x8000be94]:sw t6, 1504(ra)
[0x8000be98]:sw t5, 1512(ra)
[0x8000be9c]:sw gp, 1520(ra)
[0x8000bea0]:lui a6, 1
[0x8000bea4]:addi a6, a6, 2048
[0x8000bea8]:add sp, sp, a6
[0x8000beac]:lw t3, 1776(sp)
[0x8000beb0]:sub sp, sp, a6
[0x8000beb4]:lui a6, 1
[0x8000beb8]:addi a6, a6, 2048
[0x8000bebc]:add sp, sp, a6
[0x8000bec0]:lw t4, 1780(sp)
[0x8000bec4]:sub sp, sp, a6
[0x8000bec8]:lui a6, 1
[0x8000becc]:addi a6, a6, 2048
[0x8000bed0]:add sp, sp, a6
[0x8000bed4]:lw s10, 1784(sp)
[0x8000bed8]:sub sp, sp, a6
[0x8000bedc]:lui a6, 1
[0x8000bee0]:addi a6, a6, 2048
[0x8000bee4]:add sp, sp, a6
[0x8000bee8]:lw s11, 1788(sp)
[0x8000beec]:sub sp, sp, a6
[0x8000bef0]:addi t3, zero, 1
[0x8000bef4]:lui t4, 1048320
[0x8000bef8]:addi s10, zero, 1
[0x8000befc]:lui s11, 524288
[0x8000bf00]:addi a6, zero, 0
[0x8000bf04]:csrrw zero, fcsr, a6
[0x8000bf08]:fsgnj.d t5, t3, s10

[0x8000bf08]:fsgnj.d t5, t3, s10
[0x8000bf0c]:csrrs gp, fcsr, zero
[0x8000bf10]:sw t5, 1528(ra)
[0x8000bf14]:sw t6, 1536(ra)
[0x8000bf18]:sw t5, 1544(ra)
[0x8000bf1c]:sw gp, 1552(ra)
[0x8000bf20]:lui a6, 1
[0x8000bf24]:addi a6, a6, 2048
[0x8000bf28]:add sp, sp, a6
[0x8000bf2c]:lw t3, 1792(sp)
[0x8000bf30]:sub sp, sp, a6
[0x8000bf34]:lui a6, 1
[0x8000bf38]:addi a6, a6, 2048
[0x8000bf3c]:add sp, sp, a6
[0x8000bf40]:lw t4, 1796(sp)
[0x8000bf44]:sub sp, sp, a6
[0x8000bf48]:lui a6, 1
[0x8000bf4c]:addi a6, a6, 2048
[0x8000bf50]:add sp, sp, a6
[0x8000bf54]:lw s10, 1800(sp)
[0x8000bf58]:sub sp, sp, a6
[0x8000bf5c]:lui a6, 1
[0x8000bf60]:addi a6, a6, 2048
[0x8000bf64]:add sp, sp, a6
[0x8000bf68]:lw s11, 1804(sp)
[0x8000bf6c]:sub sp, sp, a6
[0x8000bf70]:addi t3, zero, 1
[0x8000bf74]:lui t4, 1048320
[0x8000bf78]:addi s10, zero, 2
[0x8000bf7c]:addi s11, zero, 0
[0x8000bf80]:addi a6, zero, 0
[0x8000bf84]:csrrw zero, fcsr, a6
[0x8000bf88]:fsgnj.d t5, t3, s10

[0x8000bf88]:fsgnj.d t5, t3, s10
[0x8000bf8c]:csrrs gp, fcsr, zero
[0x8000bf90]:sw t5, 1560(ra)
[0x8000bf94]:sw t6, 1568(ra)
[0x8000bf98]:sw t5, 1576(ra)
[0x8000bf9c]:sw gp, 1584(ra)
[0x8000bfa0]:lui a6, 1
[0x8000bfa4]:addi a6, a6, 2048
[0x8000bfa8]:add sp, sp, a6
[0x8000bfac]:lw t3, 1808(sp)
[0x8000bfb0]:sub sp, sp, a6
[0x8000bfb4]:lui a6, 1
[0x8000bfb8]:addi a6, a6, 2048
[0x8000bfbc]:add sp, sp, a6
[0x8000bfc0]:lw t4, 1812(sp)
[0x8000bfc4]:sub sp, sp, a6
[0x8000bfc8]:lui a6, 1
[0x8000bfcc]:addi a6, a6, 2048
[0x8000bfd0]:add sp, sp, a6
[0x8000bfd4]:lw s10, 1816(sp)
[0x8000bfd8]:sub sp, sp, a6
[0x8000bfdc]:lui a6, 1
[0x8000bfe0]:addi a6, a6, 2048
[0x8000bfe4]:add sp, sp, a6
[0x8000bfe8]:lw s11, 1820(sp)
[0x8000bfec]:sub sp, sp, a6
[0x8000bff0]:addi t3, zero, 1
[0x8000bff4]:lui t4, 1048320
[0x8000bff8]:addi s10, zero, 2
[0x8000bffc]:lui s11, 524288
[0x8000c000]:addi a6, zero, 0
[0x8000c004]:csrrw zero, fcsr, a6
[0x8000c008]:fsgnj.d t5, t3, s10

[0x8000c008]:fsgnj.d t5, t3, s10
[0x8000c00c]:csrrs gp, fcsr, zero
[0x8000c010]:sw t5, 1592(ra)
[0x8000c014]:sw t6, 1600(ra)
[0x8000c018]:sw t5, 1608(ra)
[0x8000c01c]:sw gp, 1616(ra)
[0x8000c020]:lui a6, 1
[0x8000c024]:addi a6, a6, 2048
[0x8000c028]:add sp, sp, a6
[0x8000c02c]:lw t3, 1824(sp)
[0x8000c030]:sub sp, sp, a6
[0x8000c034]:lui a6, 1
[0x8000c038]:addi a6, a6, 2048
[0x8000c03c]:add sp, sp, a6
[0x8000c040]:lw t4, 1828(sp)
[0x8000c044]:sub sp, sp, a6
[0x8000c048]:lui a6, 1
[0x8000c04c]:addi a6, a6, 2048
[0x8000c050]:add sp, sp, a6
[0x8000c054]:lw s10, 1832(sp)
[0x8000c058]:sub sp, sp, a6
[0x8000c05c]:lui a6, 1
[0x8000c060]:addi a6, a6, 2048
[0x8000c064]:add sp, sp, a6
[0x8000c068]:lw s11, 1836(sp)
[0x8000c06c]:sub sp, sp, a6
[0x8000c070]:addi t3, zero, 1
[0x8000c074]:lui t4, 1048320
[0x8000c078]:addi s10, zero, 4095
[0x8000c07c]:lui s11, 256
[0x8000c080]:addi s11, s11, 4095
[0x8000c084]:addi a6, zero, 0
[0x8000c088]:csrrw zero, fcsr, a6
[0x8000c08c]:fsgnj.d t5, t3, s10

[0x8000c08c]:fsgnj.d t5, t3, s10
[0x8000c090]:csrrs gp, fcsr, zero
[0x8000c094]:sw t5, 1624(ra)
[0x8000c098]:sw t6, 1632(ra)
[0x8000c09c]:sw t5, 1640(ra)
[0x8000c0a0]:sw gp, 1648(ra)
[0x8000c0a4]:lui a6, 1
[0x8000c0a8]:addi a6, a6, 2048
[0x8000c0ac]:add sp, sp, a6
[0x8000c0b0]:lw t3, 1840(sp)
[0x8000c0b4]:sub sp, sp, a6
[0x8000c0b8]:lui a6, 1
[0x8000c0bc]:addi a6, a6, 2048
[0x8000c0c0]:add sp, sp, a6
[0x8000c0c4]:lw t4, 1844(sp)
[0x8000c0c8]:sub sp, sp, a6
[0x8000c0cc]:lui a6, 1
[0x8000c0d0]:addi a6, a6, 2048
[0x8000c0d4]:add sp, sp, a6
[0x8000c0d8]:lw s10, 1848(sp)
[0x8000c0dc]:sub sp, sp, a6
[0x8000c0e0]:lui a6, 1
[0x8000c0e4]:addi a6, a6, 2048
[0x8000c0e8]:add sp, sp, a6
[0x8000c0ec]:lw s11, 1852(sp)
[0x8000c0f0]:sub sp, sp, a6
[0x8000c0f4]:addi t3, zero, 1
[0x8000c0f8]:lui t4, 1048320
[0x8000c0fc]:addi s10, zero, 4095
[0x8000c100]:lui s11, 524544
[0x8000c104]:addi s11, s11, 4095
[0x8000c108]:addi a6, zero, 0
[0x8000c10c]:csrrw zero, fcsr, a6
[0x8000c110]:fsgnj.d t5, t3, s10

[0x8000c110]:fsgnj.d t5, t3, s10
[0x8000c114]:csrrs gp, fcsr, zero
[0x8000c118]:sw t5, 1656(ra)
[0x8000c11c]:sw t6, 1664(ra)
[0x8000c120]:sw t5, 1672(ra)
[0x8000c124]:sw gp, 1680(ra)
[0x8000c128]:lui a6, 1
[0x8000c12c]:addi a6, a6, 2048
[0x8000c130]:add sp, sp, a6
[0x8000c134]:lw t3, 1856(sp)
[0x8000c138]:sub sp, sp, a6
[0x8000c13c]:lui a6, 1
[0x8000c140]:addi a6, a6, 2048
[0x8000c144]:add sp, sp, a6
[0x8000c148]:lw t4, 1860(sp)
[0x8000c14c]:sub sp, sp, a6
[0x8000c150]:lui a6, 1
[0x8000c154]:addi a6, a6, 2048
[0x8000c158]:add sp, sp, a6
[0x8000c15c]:lw s10, 1864(sp)
[0x8000c160]:sub sp, sp, a6
[0x8000c164]:lui a6, 1
[0x8000c168]:addi a6, a6, 2048
[0x8000c16c]:add sp, sp, a6
[0x8000c170]:lw s11, 1868(sp)
[0x8000c174]:sub sp, sp, a6
[0x8000c178]:addi t3, zero, 1
[0x8000c17c]:lui t4, 1048320
[0x8000c180]:addi s10, zero, 0
[0x8000c184]:lui s11, 256
[0x8000c188]:addi a6, zero, 0
[0x8000c18c]:csrrw zero, fcsr, a6
[0x8000c190]:fsgnj.d t5, t3, s10

[0x8000c190]:fsgnj.d t5, t3, s10
[0x8000c194]:csrrs gp, fcsr, zero
[0x8000c198]:sw t5, 1688(ra)
[0x8000c19c]:sw t6, 1696(ra)
[0x8000c1a0]:sw t5, 1704(ra)
[0x8000c1a4]:sw gp, 1712(ra)
[0x8000c1a8]:lui a6, 1
[0x8000c1ac]:addi a6, a6, 2048
[0x8000c1b0]:add sp, sp, a6
[0x8000c1b4]:lw t3, 1872(sp)
[0x8000c1b8]:sub sp, sp, a6
[0x8000c1bc]:lui a6, 1
[0x8000c1c0]:addi a6, a6, 2048
[0x8000c1c4]:add sp, sp, a6
[0x8000c1c8]:lw t4, 1876(sp)
[0x8000c1cc]:sub sp, sp, a6
[0x8000c1d0]:lui a6, 1
[0x8000c1d4]:addi a6, a6, 2048
[0x8000c1d8]:add sp, sp, a6
[0x8000c1dc]:lw s10, 1880(sp)
[0x8000c1e0]:sub sp, sp, a6
[0x8000c1e4]:lui a6, 1
[0x8000c1e8]:addi a6, a6, 2048
[0x8000c1ec]:add sp, sp, a6
[0x8000c1f0]:lw s11, 1884(sp)
[0x8000c1f4]:sub sp, sp, a6
[0x8000c1f8]:addi t3, zero, 1
[0x8000c1fc]:lui t4, 1048320
[0x8000c200]:addi s10, zero, 0
[0x8000c204]:lui s11, 524544
[0x8000c208]:addi a6, zero, 0
[0x8000c20c]:csrrw zero, fcsr, a6
[0x8000c210]:fsgnj.d t5, t3, s10

[0x8000c210]:fsgnj.d t5, t3, s10
[0x8000c214]:csrrs gp, fcsr, zero
[0x8000c218]:sw t5, 1720(ra)
[0x8000c21c]:sw t6, 1728(ra)
[0x8000c220]:sw t5, 1736(ra)
[0x8000c224]:sw gp, 1744(ra)
[0x8000c228]:lui a6, 1
[0x8000c22c]:addi a6, a6, 2048
[0x8000c230]:add sp, sp, a6
[0x8000c234]:lw t3, 1888(sp)
[0x8000c238]:sub sp, sp, a6
[0x8000c23c]:lui a6, 1
[0x8000c240]:addi a6, a6, 2048
[0x8000c244]:add sp, sp, a6
[0x8000c248]:lw t4, 1892(sp)
[0x8000c24c]:sub sp, sp, a6
[0x8000c250]:lui a6, 1
[0x8000c254]:addi a6, a6, 2048
[0x8000c258]:add sp, sp, a6
[0x8000c25c]:lw s10, 1896(sp)
[0x8000c260]:sub sp, sp, a6
[0x8000c264]:lui a6, 1
[0x8000c268]:addi a6, a6, 2048
[0x8000c26c]:add sp, sp, a6
[0x8000c270]:lw s11, 1900(sp)
[0x8000c274]:sub sp, sp, a6
[0x8000c278]:addi t3, zero, 1
[0x8000c27c]:lui t4, 1048320
[0x8000c280]:addi s10, zero, 2
[0x8000c284]:lui s11, 256
[0x8000c288]:addi a6, zero, 0
[0x8000c28c]:csrrw zero, fcsr, a6
[0x8000c290]:fsgnj.d t5, t3, s10

[0x8000c290]:fsgnj.d t5, t3, s10
[0x8000c294]:csrrs gp, fcsr, zero
[0x8000c298]:sw t5, 1752(ra)
[0x8000c29c]:sw t6, 1760(ra)
[0x8000c2a0]:sw t5, 1768(ra)
[0x8000c2a4]:sw gp, 1776(ra)
[0x8000c2a8]:lui a6, 1
[0x8000c2ac]:addi a6, a6, 2048
[0x8000c2b0]:add sp, sp, a6
[0x8000c2b4]:lw t3, 1904(sp)
[0x8000c2b8]:sub sp, sp, a6
[0x8000c2bc]:lui a6, 1
[0x8000c2c0]:addi a6, a6, 2048
[0x8000c2c4]:add sp, sp, a6
[0x8000c2c8]:lw t4, 1908(sp)
[0x8000c2cc]:sub sp, sp, a6
[0x8000c2d0]:lui a6, 1
[0x8000c2d4]:addi a6, a6, 2048
[0x8000c2d8]:add sp, sp, a6
[0x8000c2dc]:lw s10, 1912(sp)
[0x8000c2e0]:sub sp, sp, a6
[0x8000c2e4]:lui a6, 1
[0x8000c2e8]:addi a6, a6, 2048
[0x8000c2ec]:add sp, sp, a6
[0x8000c2f0]:lw s11, 1916(sp)
[0x8000c2f4]:sub sp, sp, a6
[0x8000c2f8]:addi t3, zero, 1
[0x8000c2fc]:lui t4, 1048320
[0x8000c300]:addi s10, zero, 2
[0x8000c304]:lui s11, 524544
[0x8000c308]:addi a6, zero, 0
[0x8000c30c]:csrrw zero, fcsr, a6
[0x8000c310]:fsgnj.d t5, t3, s10

[0x8000c310]:fsgnj.d t5, t3, s10
[0x8000c314]:csrrs gp, fcsr, zero
[0x8000c318]:sw t5, 1784(ra)
[0x8000c31c]:sw t6, 1792(ra)
[0x8000c320]:sw t5, 1800(ra)
[0x8000c324]:sw gp, 1808(ra)
[0x8000c328]:lui a6, 1
[0x8000c32c]:addi a6, a6, 2048
[0x8000c330]:add sp, sp, a6
[0x8000c334]:lw t3, 1920(sp)
[0x8000c338]:sub sp, sp, a6
[0x8000c33c]:lui a6, 1
[0x8000c340]:addi a6, a6, 2048
[0x8000c344]:add sp, sp, a6
[0x8000c348]:lw t4, 1924(sp)
[0x8000c34c]:sub sp, sp, a6
[0x8000c350]:lui a6, 1
[0x8000c354]:addi a6, a6, 2048
[0x8000c358]:add sp, sp, a6
[0x8000c35c]:lw s10, 1928(sp)
[0x8000c360]:sub sp, sp, a6
[0x8000c364]:lui a6, 1
[0x8000c368]:addi a6, a6, 2048
[0x8000c36c]:add sp, sp, a6
[0x8000c370]:lw s11, 1932(sp)
[0x8000c374]:sub sp, sp, a6
[0x8000c378]:addi t3, zero, 1
[0x8000c37c]:lui t4, 1048320
[0x8000c380]:addi s10, zero, 4095
[0x8000c384]:lui s11, 524032
[0x8000c388]:addi s11, s11, 4095
[0x8000c38c]:addi a6, zero, 0
[0x8000c390]:csrrw zero, fcsr, a6
[0x8000c394]:fsgnj.d t5, t3, s10

[0x8000c394]:fsgnj.d t5, t3, s10
[0x8000c398]:csrrs gp, fcsr, zero
[0x8000c39c]:sw t5, 1816(ra)
[0x8000c3a0]:sw t6, 1824(ra)
[0x8000c3a4]:sw t5, 1832(ra)
[0x8000c3a8]:sw gp, 1840(ra)
[0x8000c3ac]:lui a6, 1
[0x8000c3b0]:addi a6, a6, 2048
[0x8000c3b4]:add sp, sp, a6
[0x8000c3b8]:lw t3, 1936(sp)
[0x8000c3bc]:sub sp, sp, a6
[0x8000c3c0]:lui a6, 1
[0x8000c3c4]:addi a6, a6, 2048
[0x8000c3c8]:add sp, sp, a6
[0x8000c3cc]:lw t4, 1940(sp)
[0x8000c3d0]:sub sp, sp, a6
[0x8000c3d4]:lui a6, 1
[0x8000c3d8]:addi a6, a6, 2048
[0x8000c3dc]:add sp, sp, a6
[0x8000c3e0]:lw s10, 1944(sp)
[0x8000c3e4]:sub sp, sp, a6
[0x8000c3e8]:lui a6, 1
[0x8000c3ec]:addi a6, a6, 2048
[0x8000c3f0]:add sp, sp, a6
[0x8000c3f4]:lw s11, 1948(sp)
[0x8000c3f8]:sub sp, sp, a6
[0x8000c3fc]:addi t3, zero, 1
[0x8000c400]:lui t4, 1048320
[0x8000c404]:addi s10, zero, 4095
[0x8000c408]:lui s11, 1048320
[0x8000c40c]:addi s11, s11, 4095
[0x8000c410]:addi a6, zero, 0
[0x8000c414]:csrrw zero, fcsr, a6
[0x8000c418]:fsgnj.d t5, t3, s10

[0x8000c418]:fsgnj.d t5, t3, s10
[0x8000c41c]:csrrs gp, fcsr, zero
[0x8000c420]:sw t5, 1848(ra)
[0x8000c424]:sw t6, 1856(ra)
[0x8000c428]:sw t5, 1864(ra)
[0x8000c42c]:sw gp, 1872(ra)
[0x8000c430]:lui a6, 1
[0x8000c434]:addi a6, a6, 2048
[0x8000c438]:add sp, sp, a6
[0x8000c43c]:lw t3, 1952(sp)
[0x8000c440]:sub sp, sp, a6
[0x8000c444]:lui a6, 1
[0x8000c448]:addi a6, a6, 2048
[0x8000c44c]:add sp, sp, a6
[0x8000c450]:lw t4, 1956(sp)
[0x8000c454]:sub sp, sp, a6
[0x8000c458]:lui a6, 1
[0x8000c45c]:addi a6, a6, 2048
[0x8000c460]:add sp, sp, a6
[0x8000c464]:lw s10, 1960(sp)
[0x8000c468]:sub sp, sp, a6
[0x8000c46c]:lui a6, 1
[0x8000c470]:addi a6, a6, 2048
[0x8000c474]:add sp, sp, a6
[0x8000c478]:lw s11, 1964(sp)
[0x8000c47c]:sub sp, sp, a6
[0x8000c480]:addi t3, zero, 1
[0x8000c484]:lui t4, 1048320
[0x8000c488]:addi s10, zero, 0
[0x8000c48c]:lui s11, 524032
[0x8000c490]:addi a6, zero, 0
[0x8000c494]:csrrw zero, fcsr, a6
[0x8000c498]:fsgnj.d t5, t3, s10

[0x8000c498]:fsgnj.d t5, t3, s10
[0x8000c49c]:csrrs gp, fcsr, zero
[0x8000c4a0]:sw t5, 1880(ra)
[0x8000c4a4]:sw t6, 1888(ra)
[0x8000c4a8]:sw t5, 1896(ra)
[0x8000c4ac]:sw gp, 1904(ra)
[0x8000c4b0]:lui a6, 1
[0x8000c4b4]:addi a6, a6, 2048
[0x8000c4b8]:add sp, sp, a6
[0x8000c4bc]:lw t3, 1968(sp)
[0x8000c4c0]:sub sp, sp, a6
[0x8000c4c4]:lui a6, 1
[0x8000c4c8]:addi a6, a6, 2048
[0x8000c4cc]:add sp, sp, a6
[0x8000c4d0]:lw t4, 1972(sp)
[0x8000c4d4]:sub sp, sp, a6
[0x8000c4d8]:lui a6, 1
[0x8000c4dc]:addi a6, a6, 2048
[0x8000c4e0]:add sp, sp, a6
[0x8000c4e4]:lw s10, 1976(sp)
[0x8000c4e8]:sub sp, sp, a6
[0x8000c4ec]:lui a6, 1
[0x8000c4f0]:addi a6, a6, 2048
[0x8000c4f4]:add sp, sp, a6
[0x8000c4f8]:lw s11, 1980(sp)
[0x8000c4fc]:sub sp, sp, a6
[0x8000c500]:addi t3, zero, 1
[0x8000c504]:lui t4, 1048320
[0x8000c508]:addi s10, zero, 0
[0x8000c50c]:lui s11, 1048320
[0x8000c510]:addi a6, zero, 0
[0x8000c514]:csrrw zero, fcsr, a6
[0x8000c518]:fsgnj.d t5, t3, s10

[0x8000c518]:fsgnj.d t5, t3, s10
[0x8000c51c]:csrrs gp, fcsr, zero
[0x8000c520]:sw t5, 1912(ra)
[0x8000c524]:sw t6, 1920(ra)
[0x8000c528]:sw t5, 1928(ra)
[0x8000c52c]:sw gp, 1936(ra)
[0x8000c530]:lui a6, 1
[0x8000c534]:addi a6, a6, 2048
[0x8000c538]:add sp, sp, a6
[0x8000c53c]:lw t3, 1984(sp)
[0x8000c540]:sub sp, sp, a6
[0x8000c544]:lui a6, 1
[0x8000c548]:addi a6, a6, 2048
[0x8000c54c]:add sp, sp, a6
[0x8000c550]:lw t4, 1988(sp)
[0x8000c554]:sub sp, sp, a6
[0x8000c558]:lui a6, 1
[0x8000c55c]:addi a6, a6, 2048
[0x8000c560]:add sp, sp, a6
[0x8000c564]:lw s10, 1992(sp)
[0x8000c568]:sub sp, sp, a6
[0x8000c56c]:lui a6, 1
[0x8000c570]:addi a6, a6, 2048
[0x8000c574]:add sp, sp, a6
[0x8000c578]:lw s11, 1996(sp)
[0x8000c57c]:sub sp, sp, a6
[0x8000c580]:addi t3, zero, 1
[0x8000c584]:lui t4, 1048320
[0x8000c588]:addi s10, zero, 0
[0x8000c58c]:lui s11, 524160
[0x8000c590]:addi a6, zero, 0
[0x8000c594]:csrrw zero, fcsr, a6
[0x8000c598]:fsgnj.d t5, t3, s10

[0x8000c598]:fsgnj.d t5, t3, s10
[0x8000c59c]:csrrs gp, fcsr, zero
[0x8000c5a0]:sw t5, 1944(ra)
[0x8000c5a4]:sw t6, 1952(ra)
[0x8000c5a8]:sw t5, 1960(ra)
[0x8000c5ac]:sw gp, 1968(ra)
[0x8000c5b0]:lui a6, 1
[0x8000c5b4]:addi a6, a6, 2048
[0x8000c5b8]:add sp, sp, a6
[0x8000c5bc]:lw t3, 2000(sp)
[0x8000c5c0]:sub sp, sp, a6
[0x8000c5c4]:lui a6, 1
[0x8000c5c8]:addi a6, a6, 2048
[0x8000c5cc]:add sp, sp, a6
[0x8000c5d0]:lw t4, 2004(sp)
[0x8000c5d4]:sub sp, sp, a6
[0x8000c5d8]:lui a6, 1
[0x8000c5dc]:addi a6, a6, 2048
[0x8000c5e0]:add sp, sp, a6
[0x8000c5e4]:lw s10, 2008(sp)
[0x8000c5e8]:sub sp, sp, a6
[0x8000c5ec]:lui a6, 1
[0x8000c5f0]:addi a6, a6, 2048
[0x8000c5f4]:add sp, sp, a6
[0x8000c5f8]:lw s11, 2012(sp)
[0x8000c5fc]:sub sp, sp, a6
[0x8000c600]:addi t3, zero, 1
[0x8000c604]:lui t4, 1048320
[0x8000c608]:addi s10, zero, 0
[0x8000c60c]:lui s11, 1048448
[0x8000c610]:addi a6, zero, 0
[0x8000c614]:csrrw zero, fcsr, a6
[0x8000c618]:fsgnj.d t5, t3, s10

[0x8000c618]:fsgnj.d t5, t3, s10
[0x8000c61c]:csrrs gp, fcsr, zero
[0x8000c620]:sw t5, 1976(ra)
[0x8000c624]:sw t6, 1984(ra)
[0x8000c628]:sw t5, 1992(ra)
[0x8000c62c]:sw gp, 2000(ra)
[0x8000c630]:lui a6, 1
[0x8000c634]:addi a6, a6, 2048
[0x8000c638]:add sp, sp, a6
[0x8000c63c]:lw t3, 2016(sp)
[0x8000c640]:sub sp, sp, a6
[0x8000c644]:lui a6, 1
[0x8000c648]:addi a6, a6, 2048
[0x8000c64c]:add sp, sp, a6
[0x8000c650]:lw t4, 2020(sp)
[0x8000c654]:sub sp, sp, a6
[0x8000c658]:lui a6, 1
[0x8000c65c]:addi a6, a6, 2048
[0x8000c660]:add sp, sp, a6
[0x8000c664]:lw s10, 2024(sp)
[0x8000c668]:sub sp, sp, a6
[0x8000c66c]:lui a6, 1
[0x8000c670]:addi a6, a6, 2048
[0x8000c674]:add sp, sp, a6
[0x8000c678]:lw s11, 2028(sp)
[0x8000c67c]:sub sp, sp, a6
[0x8000c680]:addi t3, zero, 1
[0x8000c684]:lui t4, 1048320
[0x8000c688]:addi s10, zero, 1
[0x8000c68c]:lui s11, 524160
[0x8000c690]:addi a6, zero, 0
[0x8000c694]:csrrw zero, fcsr, a6
[0x8000c698]:fsgnj.d t5, t3, s10

[0x8000c698]:fsgnj.d t5, t3, s10
[0x8000c69c]:csrrs gp, fcsr, zero
[0x8000c6a0]:sw t5, 2008(ra)
[0x8000c6a4]:sw t6, 2016(ra)
[0x8000c6a8]:sw t5, 2024(ra)
[0x8000c6ac]:sw gp, 2032(ra)
[0x8000c6b0]:lui a6, 1
[0x8000c6b4]:addi a6, a6, 2048
[0x8000c6b8]:add sp, sp, a6
[0x8000c6bc]:lw t3, 2032(sp)
[0x8000c6c0]:sub sp, sp, a6
[0x8000c6c4]:lui a6, 1
[0x8000c6c8]:addi a6, a6, 2048
[0x8000c6cc]:add sp, sp, a6
[0x8000c6d0]:lw t4, 2036(sp)
[0x8000c6d4]:sub sp, sp, a6
[0x8000c6d8]:lui a6, 1
[0x8000c6dc]:addi a6, a6, 2048
[0x8000c6e0]:add sp, sp, a6
[0x8000c6e4]:lw s10, 2040(sp)
[0x8000c6e8]:sub sp, sp, a6
[0x8000c6ec]:lui a6, 1
[0x8000c6f0]:addi a6, a6, 2048
[0x8000c6f4]:add sp, sp, a6
[0x8000c6f8]:lw s11, 2044(sp)
[0x8000c6fc]:sub sp, sp, a6
[0x8000c700]:addi t3, zero, 1
[0x8000c704]:lui t4, 1048320
[0x8000c708]:addi s10, zero, 1
[0x8000c70c]:lui s11, 1048448
[0x8000c710]:addi a6, zero, 0
[0x8000c714]:csrrw zero, fcsr, a6
[0x8000c718]:fsgnj.d t5, t3, s10

[0x8000c718]:fsgnj.d t5, t3, s10
[0x8000c71c]:csrrs gp, fcsr, zero
[0x8000c720]:sw t5, 2040(ra)
[0x8000c724]:addi ra, ra, 2040
[0x8000c728]:sw t6, 8(ra)
[0x8000c72c]:sw t5, 16(ra)
[0x8000c730]:sw gp, 24(ra)
[0x8000c734]:auipc ra, 7
[0x8000c738]:addi ra, ra, 4004
[0x8000c73c]:lw t3, 0(sp)
[0x8000c740]:lw t4, 4(sp)
[0x8000c744]:lw s10, 8(sp)
[0x8000c748]:lw s11, 12(sp)
[0x8000c74c]:addi t3, zero, 1
[0x8000c750]:lui t4, 1048320
[0x8000c754]:addi s10, zero, 1
[0x8000c758]:lui s11, 524032
[0x8000c75c]:addi a6, zero, 0
[0x8000c760]:csrrw zero, fcsr, a6
[0x8000c764]:fsgnj.d t5, t3, s10

[0x8000ce74]:fsgnj.d t5, t3, s10
[0x8000ce78]:csrrs gp, fcsr, zero
[0x8000ce7c]:sw t5, 896(ra)
[0x8000ce80]:sw t6, 904(ra)
[0x8000ce84]:sw t5, 912(ra)
[0x8000ce88]:sw gp, 920(ra)
[0x8000ce8c]:lw t3, 464(sp)
[0x8000ce90]:lw t4, 468(sp)
[0x8000ce94]:lw s10, 472(sp)
[0x8000ce98]:lw s11, 476(sp)
[0x8000ce9c]:addi t3, zero, 0
[0x8000cea0]:lui t4, 784384
[0x8000cea4]:addi s10, zero, 0
[0x8000cea8]:lui s11, 524288
[0x8000ceac]:addi a6, zero, 0
[0x8000ceb0]:csrrw zero, fcsr, a6
[0x8000ceb4]:fsgnj.d t5, t3, s10

[0x8000ceb4]:fsgnj.d t5, t3, s10
[0x8000ceb8]:csrrs gp, fcsr, zero
[0x8000cebc]:sw t5, 928(ra)
[0x8000cec0]:sw t6, 936(ra)
[0x8000cec4]:sw t5, 944(ra)
[0x8000cec8]:sw gp, 952(ra)
[0x8000cecc]:lw t3, 480(sp)
[0x8000ced0]:lw t4, 484(sp)
[0x8000ced4]:lw s10, 488(sp)
[0x8000ced8]:lw s11, 492(sp)
[0x8000cedc]:addi t3, zero, 0
[0x8000cee0]:lui t4, 784384
[0x8000cee4]:addi s10, zero, 1
[0x8000cee8]:addi s11, zero, 0
[0x8000ceec]:addi a6, zero, 0
[0x8000cef0]:csrrw zero, fcsr, a6
[0x8000cef4]:fsgnj.d t5, t3, s10

[0x8000cef4]:fsgnj.d t5, t3, s10
[0x8000cef8]:csrrs gp, fcsr, zero
[0x8000cefc]:sw t5, 960(ra)
[0x8000cf00]:sw t6, 968(ra)
[0x8000cf04]:sw t5, 976(ra)
[0x8000cf08]:sw gp, 984(ra)
[0x8000cf0c]:lw t3, 496(sp)
[0x8000cf10]:lw t4, 500(sp)
[0x8000cf14]:lw s10, 504(sp)
[0x8000cf18]:lw s11, 508(sp)
[0x8000cf1c]:addi t3, zero, 0
[0x8000cf20]:lui t4, 784384
[0x8000cf24]:addi s10, zero, 1
[0x8000cf28]:lui s11, 524288
[0x8000cf2c]:addi a6, zero, 0
[0x8000cf30]:csrrw zero, fcsr, a6
[0x8000cf34]:fsgnj.d t5, t3, s10

[0x8000cf34]:fsgnj.d t5, t3, s10
[0x8000cf38]:csrrs gp, fcsr, zero
[0x8000cf3c]:sw t5, 992(ra)
[0x8000cf40]:sw t6, 1000(ra)
[0x8000cf44]:sw t5, 1008(ra)
[0x8000cf48]:sw gp, 1016(ra)
[0x8000cf4c]:lw t3, 512(sp)
[0x8000cf50]:lw t4, 516(sp)
[0x8000cf54]:lw s10, 520(sp)
[0x8000cf58]:lw s11, 524(sp)
[0x8000cf5c]:addi t3, zero, 0
[0x8000cf60]:lui t4, 784384
[0x8000cf64]:addi s10, zero, 2
[0x8000cf68]:addi s11, zero, 0
[0x8000cf6c]:addi a6, zero, 0
[0x8000cf70]:csrrw zero, fcsr, a6
[0x8000cf74]:fsgnj.d t5, t3, s10

[0x8000cf74]:fsgnj.d t5, t3, s10
[0x8000cf78]:csrrs gp, fcsr, zero
[0x8000cf7c]:sw t5, 1024(ra)
[0x8000cf80]:sw t6, 1032(ra)
[0x8000cf84]:sw t5, 1040(ra)
[0x8000cf88]:sw gp, 1048(ra)
[0x8000cf8c]:lw t3, 528(sp)
[0x8000cf90]:lw t4, 532(sp)
[0x8000cf94]:lw s10, 536(sp)
[0x8000cf98]:lw s11, 540(sp)
[0x8000cf9c]:addi t3, zero, 0
[0x8000cfa0]:lui t4, 784384
[0x8000cfa4]:addi s10, zero, 2
[0x8000cfa8]:lui s11, 524288
[0x8000cfac]:addi a6, zero, 0
[0x8000cfb0]:csrrw zero, fcsr, a6
[0x8000cfb4]:fsgnj.d t5, t3, s10

[0x8000cfb4]:fsgnj.d t5, t3, s10
[0x8000cfb8]:csrrs gp, fcsr, zero
[0x8000cfbc]:sw t5, 1056(ra)
[0x8000cfc0]:sw t6, 1064(ra)
[0x8000cfc4]:sw t5, 1072(ra)
[0x8000cfc8]:sw gp, 1080(ra)
[0x8000cfcc]:lw t3, 544(sp)
[0x8000cfd0]:lw t4, 548(sp)
[0x8000cfd4]:lw s10, 552(sp)
[0x8000cfd8]:lw s11, 556(sp)
[0x8000cfdc]:addi t3, zero, 0
[0x8000cfe0]:lui t4, 784384
[0x8000cfe4]:addi s10, zero, 4095
[0x8000cfe8]:lui s11, 256
[0x8000cfec]:addi s11, s11, 4095
[0x8000cff0]:addi a6, zero, 0
[0x8000cff4]:csrrw zero, fcsr, a6
[0x8000cff8]:fsgnj.d t5, t3, s10

[0x8000cff8]:fsgnj.d t5, t3, s10
[0x8000cffc]:csrrs gp, fcsr, zero
[0x8000d000]:sw t5, 1088(ra)
[0x8000d004]:sw t6, 1096(ra)
[0x8000d008]:sw t5, 1104(ra)
[0x8000d00c]:sw gp, 1112(ra)
[0x8000d010]:lw t3, 560(sp)
[0x8000d014]:lw t4, 564(sp)
[0x8000d018]:lw s10, 568(sp)
[0x8000d01c]:lw s11, 572(sp)
[0x8000d020]:addi t3, zero, 0
[0x8000d024]:lui t4, 784384
[0x8000d028]:addi s10, zero, 4095
[0x8000d02c]:lui s11, 524544
[0x8000d030]:addi s11, s11, 4095
[0x8000d034]:addi a6, zero, 0
[0x8000d038]:csrrw zero, fcsr, a6
[0x8000d03c]:fsgnj.d t5, t3, s10

[0x8000d03c]:fsgnj.d t5, t3, s10
[0x8000d040]:csrrs gp, fcsr, zero
[0x8000d044]:sw t5, 1120(ra)
[0x8000d048]:sw t6, 1128(ra)
[0x8000d04c]:sw t5, 1136(ra)
[0x8000d050]:sw gp, 1144(ra)
[0x8000d054]:lw t3, 576(sp)
[0x8000d058]:lw t4, 580(sp)
[0x8000d05c]:lw s10, 584(sp)
[0x8000d060]:lw s11, 588(sp)
[0x8000d064]:addi t3, zero, 0
[0x8000d068]:lui t4, 784384
[0x8000d06c]:addi s10, zero, 0
[0x8000d070]:lui s11, 256
[0x8000d074]:addi a6, zero, 0
[0x8000d078]:csrrw zero, fcsr, a6
[0x8000d07c]:fsgnj.d t5, t3, s10

[0x8000d07c]:fsgnj.d t5, t3, s10
[0x8000d080]:csrrs gp, fcsr, zero
[0x8000d084]:sw t5, 1152(ra)
[0x8000d088]:sw t6, 1160(ra)
[0x8000d08c]:sw t5, 1168(ra)
[0x8000d090]:sw gp, 1176(ra)
[0x8000d094]:lw t3, 592(sp)
[0x8000d098]:lw t4, 596(sp)
[0x8000d09c]:lw s10, 600(sp)
[0x8000d0a0]:lw s11, 604(sp)
[0x8000d0a4]:addi t3, zero, 0
[0x8000d0a8]:lui t4, 784384
[0x8000d0ac]:addi s10, zero, 0
[0x8000d0b0]:lui s11, 524544
[0x8000d0b4]:addi a6, zero, 0
[0x8000d0b8]:csrrw zero, fcsr, a6
[0x8000d0bc]:fsgnj.d t5, t3, s10

[0x8000d0bc]:fsgnj.d t5, t3, s10
[0x8000d0c0]:csrrs gp, fcsr, zero
[0x8000d0c4]:sw t5, 1184(ra)
[0x8000d0c8]:sw t6, 1192(ra)
[0x8000d0cc]:sw t5, 1200(ra)
[0x8000d0d0]:sw gp, 1208(ra)
[0x8000d0d4]:lw t3, 608(sp)
[0x8000d0d8]:lw t4, 612(sp)
[0x8000d0dc]:lw s10, 616(sp)
[0x8000d0e0]:lw s11, 620(sp)
[0x8000d0e4]:addi t3, zero, 0
[0x8000d0e8]:lui t4, 784384
[0x8000d0ec]:addi s10, zero, 2
[0x8000d0f0]:lui s11, 256
[0x8000d0f4]:addi a6, zero, 0
[0x8000d0f8]:csrrw zero, fcsr, a6
[0x8000d0fc]:fsgnj.d t5, t3, s10

[0x8000d0fc]:fsgnj.d t5, t3, s10
[0x8000d100]:csrrs gp, fcsr, zero
[0x8000d104]:sw t5, 1216(ra)
[0x8000d108]:sw t6, 1224(ra)
[0x8000d10c]:sw t5, 1232(ra)
[0x8000d110]:sw gp, 1240(ra)
[0x8000d114]:lw t3, 624(sp)
[0x8000d118]:lw t4, 628(sp)
[0x8000d11c]:lw s10, 632(sp)
[0x8000d120]:lw s11, 636(sp)
[0x8000d124]:addi t3, zero, 0
[0x8000d128]:lui t4, 784384
[0x8000d12c]:addi s10, zero, 2
[0x8000d130]:lui s11, 524544
[0x8000d134]:addi a6, zero, 0
[0x8000d138]:csrrw zero, fcsr, a6
[0x8000d13c]:fsgnj.d t5, t3, s10

[0x8000d13c]:fsgnj.d t5, t3, s10
[0x8000d140]:csrrs gp, fcsr, zero
[0x8000d144]:sw t5, 1248(ra)
[0x8000d148]:sw t6, 1256(ra)
[0x8000d14c]:sw t5, 1264(ra)
[0x8000d150]:sw gp, 1272(ra)
[0x8000d154]:lw t3, 640(sp)
[0x8000d158]:lw t4, 644(sp)
[0x8000d15c]:lw s10, 648(sp)
[0x8000d160]:lw s11, 652(sp)
[0x8000d164]:addi t3, zero, 0
[0x8000d168]:lui t4, 784384
[0x8000d16c]:addi s10, zero, 4095
[0x8000d170]:lui s11, 524032
[0x8000d174]:addi s11, s11, 4095
[0x8000d178]:addi a6, zero, 0
[0x8000d17c]:csrrw zero, fcsr, a6
[0x8000d180]:fsgnj.d t5, t3, s10

[0x8000d180]:fsgnj.d t5, t3, s10
[0x8000d184]:csrrs gp, fcsr, zero
[0x8000d188]:sw t5, 1280(ra)
[0x8000d18c]:sw t6, 1288(ra)
[0x8000d190]:sw t5, 1296(ra)
[0x8000d194]:sw gp, 1304(ra)
[0x8000d198]:lw t3, 656(sp)
[0x8000d19c]:lw t4, 660(sp)
[0x8000d1a0]:lw s10, 664(sp)
[0x8000d1a4]:lw s11, 668(sp)
[0x8000d1a8]:addi t3, zero, 0
[0x8000d1ac]:lui t4, 784384
[0x8000d1b0]:addi s10, zero, 4095
[0x8000d1b4]:lui s11, 1048320
[0x8000d1b8]:addi s11, s11, 4095
[0x8000d1bc]:addi a6, zero, 0
[0x8000d1c0]:csrrw zero, fcsr, a6
[0x8000d1c4]:fsgnj.d t5, t3, s10

[0x8000d1c4]:fsgnj.d t5, t3, s10
[0x8000d1c8]:csrrs gp, fcsr, zero
[0x8000d1cc]:sw t5, 1312(ra)
[0x8000d1d0]:sw t6, 1320(ra)
[0x8000d1d4]:sw t5, 1328(ra)
[0x8000d1d8]:sw gp, 1336(ra)
[0x8000d1dc]:lw t3, 672(sp)
[0x8000d1e0]:lw t4, 676(sp)
[0x8000d1e4]:lw s10, 680(sp)
[0x8000d1e8]:lw s11, 684(sp)
[0x8000d1ec]:addi t3, zero, 0
[0x8000d1f0]:lui t4, 784384
[0x8000d1f4]:addi s10, zero, 0
[0x8000d1f8]:lui s11, 524032
[0x8000d1fc]:addi a6, zero, 0
[0x8000d200]:csrrw zero, fcsr, a6
[0x8000d204]:fsgnj.d t5, t3, s10

[0x8000d204]:fsgnj.d t5, t3, s10
[0x8000d208]:csrrs gp, fcsr, zero
[0x8000d20c]:sw t5, 1344(ra)
[0x8000d210]:sw t6, 1352(ra)
[0x8000d214]:sw t5, 1360(ra)
[0x8000d218]:sw gp, 1368(ra)
[0x8000d21c]:lw t3, 688(sp)
[0x8000d220]:lw t4, 692(sp)
[0x8000d224]:lw s10, 696(sp)
[0x8000d228]:lw s11, 700(sp)
[0x8000d22c]:addi t3, zero, 0
[0x8000d230]:lui t4, 784384
[0x8000d234]:addi s10, zero, 0
[0x8000d238]:lui s11, 1048320
[0x8000d23c]:addi a6, zero, 0
[0x8000d240]:csrrw zero, fcsr, a6
[0x8000d244]:fsgnj.d t5, t3, s10

[0x8000d244]:fsgnj.d t5, t3, s10
[0x8000d248]:csrrs gp, fcsr, zero
[0x8000d24c]:sw t5, 1376(ra)
[0x8000d250]:sw t6, 1384(ra)
[0x8000d254]:sw t5, 1392(ra)
[0x8000d258]:sw gp, 1400(ra)
[0x8000d25c]:lw t3, 704(sp)
[0x8000d260]:lw t4, 708(sp)
[0x8000d264]:lw s10, 712(sp)
[0x8000d268]:lw s11, 716(sp)
[0x8000d26c]:addi t3, zero, 0
[0x8000d270]:lui t4, 784384
[0x8000d274]:addi s10, zero, 0
[0x8000d278]:lui s11, 524160
[0x8000d27c]:addi a6, zero, 0
[0x8000d280]:csrrw zero, fcsr, a6
[0x8000d284]:fsgnj.d t5, t3, s10

[0x8000d284]:fsgnj.d t5, t3, s10
[0x8000d288]:csrrs gp, fcsr, zero
[0x8000d28c]:sw t5, 1408(ra)
[0x8000d290]:sw t6, 1416(ra)
[0x8000d294]:sw t5, 1424(ra)
[0x8000d298]:sw gp, 1432(ra)
[0x8000d29c]:lw t3, 720(sp)
[0x8000d2a0]:lw t4, 724(sp)
[0x8000d2a4]:lw s10, 728(sp)
[0x8000d2a8]:lw s11, 732(sp)
[0x8000d2ac]:addi t3, zero, 0
[0x8000d2b0]:lui t4, 784384
[0x8000d2b4]:addi s10, zero, 0
[0x8000d2b8]:lui s11, 1048448
[0x8000d2bc]:addi a6, zero, 0
[0x8000d2c0]:csrrw zero, fcsr, a6
[0x8000d2c4]:fsgnj.d t5, t3, s10

[0x8000d2c4]:fsgnj.d t5, t3, s10
[0x8000d2c8]:csrrs gp, fcsr, zero
[0x8000d2cc]:sw t5, 1440(ra)
[0x8000d2d0]:sw t6, 1448(ra)
[0x8000d2d4]:sw t5, 1456(ra)
[0x8000d2d8]:sw gp, 1464(ra)
[0x8000d2dc]:lw t3, 736(sp)
[0x8000d2e0]:lw t4, 740(sp)
[0x8000d2e4]:lw s10, 744(sp)
[0x8000d2e8]:lw s11, 748(sp)
[0x8000d2ec]:addi t3, zero, 0
[0x8000d2f0]:lui t4, 784384
[0x8000d2f4]:addi s10, zero, 1
[0x8000d2f8]:lui s11, 524160
[0x8000d2fc]:addi a6, zero, 0
[0x8000d300]:csrrw zero, fcsr, a6
[0x8000d304]:fsgnj.d t5, t3, s10

[0x8000d304]:fsgnj.d t5, t3, s10
[0x8000d308]:csrrs gp, fcsr, zero
[0x8000d30c]:sw t5, 1472(ra)
[0x8000d310]:sw t6, 1480(ra)
[0x8000d314]:sw t5, 1488(ra)
[0x8000d318]:sw gp, 1496(ra)
[0x8000d31c]:lw t3, 752(sp)
[0x8000d320]:lw t4, 756(sp)
[0x8000d324]:lw s10, 760(sp)
[0x8000d328]:lw s11, 764(sp)
[0x8000d32c]:addi t3, zero, 0
[0x8000d330]:lui t4, 784384
[0x8000d334]:addi s10, zero, 1
[0x8000d338]:lui s11, 1048448
[0x8000d33c]:addi a6, zero, 0
[0x8000d340]:csrrw zero, fcsr, a6
[0x8000d344]:fsgnj.d t5, t3, s10

[0x8000d344]:fsgnj.d t5, t3, s10
[0x8000d348]:csrrs gp, fcsr, zero
[0x8000d34c]:sw t5, 1504(ra)
[0x8000d350]:sw t6, 1512(ra)
[0x8000d354]:sw t5, 1520(ra)
[0x8000d358]:sw gp, 1528(ra)
[0x8000d35c]:lw t3, 768(sp)
[0x8000d360]:lw t4, 772(sp)
[0x8000d364]:lw s10, 776(sp)
[0x8000d368]:lw s11, 780(sp)
[0x8000d36c]:addi t3, zero, 0
[0x8000d370]:lui t4, 784384
[0x8000d374]:addi s10, zero, 1
[0x8000d378]:lui s11, 524032
[0x8000d37c]:addi a6, zero, 0
[0x8000d380]:csrrw zero, fcsr, a6
[0x8000d384]:fsgnj.d t5, t3, s10

[0x8000d384]:fsgnj.d t5, t3, s10
[0x8000d388]:csrrs gp, fcsr, zero
[0x8000d38c]:sw t5, 1536(ra)
[0x8000d390]:sw t6, 1544(ra)
[0x8000d394]:sw t5, 1552(ra)
[0x8000d398]:sw gp, 1560(ra)
[0x8000d39c]:lw t3, 784(sp)
[0x8000d3a0]:lw t4, 788(sp)
[0x8000d3a4]:lw s10, 792(sp)
[0x8000d3a8]:lw s11, 796(sp)
[0x8000d3ac]:addi t3, zero, 0
[0x8000d3b0]:lui t4, 784384
[0x8000d3b4]:addi s10, zero, 1
[0x8000d3b8]:lui s11, 1048320
[0x8000d3bc]:addi a6, zero, 0
[0x8000d3c0]:csrrw zero, fcsr, a6
[0x8000d3c4]:fsgnj.d t5, t3, s10

[0x8000d3c4]:fsgnj.d t5, t3, s10
[0x8000d3c8]:csrrs gp, fcsr, zero
[0x8000d3cc]:sw t5, 1568(ra)
[0x8000d3d0]:sw t6, 1576(ra)
[0x8000d3d4]:sw t5, 1584(ra)
[0x8000d3d8]:sw gp, 1592(ra)
[0x8000d3dc]:lw t3, 800(sp)
[0x8000d3e0]:lw t4, 804(sp)
[0x8000d3e4]:lw s10, 808(sp)
[0x8000d3e8]:lw s11, 812(sp)
[0x8000d3ec]:addi t3, zero, 0
[0x8000d3f0]:lui t4, 784384
[0x8000d3f4]:addi s10, zero, 0
[0x8000d3f8]:lui s11, 261888
[0x8000d3fc]:addi a6, zero, 0
[0x8000d400]:csrrw zero, fcsr, a6
[0x8000d404]:fsgnj.d t5, t3, s10

[0x8000d404]:fsgnj.d t5, t3, s10
[0x8000d408]:csrrs gp, fcsr, zero
[0x8000d40c]:sw t5, 1600(ra)
[0x8000d410]:sw t6, 1608(ra)
[0x8000d414]:sw t5, 1616(ra)
[0x8000d418]:sw gp, 1624(ra)
[0x8000d41c]:lw t3, 816(sp)
[0x8000d420]:lw t4, 820(sp)
[0x8000d424]:lw s10, 824(sp)
[0x8000d428]:lw s11, 828(sp)
[0x8000d42c]:addi t3, zero, 0
[0x8000d430]:lui t4, 784384
[0x8000d434]:addi s10, zero, 0
[0x8000d438]:lui s11, 784384
[0x8000d43c]:addi a6, zero, 0
[0x8000d440]:csrrw zero, fcsr, a6
[0x8000d444]:fsgnj.d t5, t3, s10

[0x8000d444]:fsgnj.d t5, t3, s10
[0x8000d448]:csrrs gp, fcsr, zero
[0x8000d44c]:sw t5, 1632(ra)
[0x8000d450]:sw t6, 1640(ra)
[0x8000d454]:sw t5, 1648(ra)
[0x8000d458]:sw gp, 1656(ra)
[0x8000d45c]:lw t3, 832(sp)
[0x8000d460]:lw t4, 836(sp)
[0x8000d464]:lw s10, 840(sp)
[0x8000d468]:lw s11, 844(sp)
[0x8000d46c]:addi t3, zero, 0
[0x8000d470]:addi t4, zero, 0
[0x8000d474]:addi s10, zero, 1
[0x8000d478]:addi s11, zero, 0
[0x8000d47c]:addi a6, zero, 0
[0x8000d480]:csrrw zero, fcsr, a6
[0x8000d484]:fsgnj.d t5, t3, s10

[0x8000d484]:fsgnj.d t5, t3, s10
[0x8000d488]:csrrs gp, fcsr, zero
[0x8000d48c]:sw t5, 1664(ra)
[0x8000d490]:sw t6, 1672(ra)
[0x8000d494]:sw t5, 1680(ra)
[0x8000d498]:sw gp, 1688(ra)
[0x8000d49c]:lw t3, 848(sp)
[0x8000d4a0]:lw t4, 852(sp)
[0x8000d4a4]:lw s10, 856(sp)
[0x8000d4a8]:lw s11, 860(sp)
[0x8000d4ac]:addi t3, zero, 0
[0x8000d4b0]:addi t4, zero, 0
[0x8000d4b4]:addi s10, zero, 2
[0x8000d4b8]:addi s11, zero, 0
[0x8000d4bc]:addi a6, zero, 0
[0x8000d4c0]:csrrw zero, fcsr, a6
[0x8000d4c4]:fsgnj.d t5, t3, s10

[0x8000d4c4]:fsgnj.d t5, t3, s10
[0x8000d4c8]:csrrs gp, fcsr, zero
[0x8000d4cc]:sw t5, 1696(ra)
[0x8000d4d0]:sw t6, 1704(ra)
[0x8000d4d4]:sw t5, 1712(ra)
[0x8000d4d8]:sw gp, 1720(ra)
[0x8000d4dc]:addi zero, zero, 0



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fsgnj.d', 'rs1 : x28', 'rs2 : x30', 'rd : x30', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000013c]:fsgnj.d t5, t3, t5
	-[0x80000140]:csrrs tp, fcsr, zero
	-[0x80000144]:sw t5, 0(ra)
Current Store : [0x80000148] : sw t6, 8(ra) -- Store: [0x80011620]:0x00000000




Last Coverpoint : ['mnemonic : fsgnj.d', 'rs1 : x28', 'rs2 : x30', 'rd : x30', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000013c]:fsgnj.d t5, t3, t5
	-[0x80000140]:csrrs tp, fcsr, zero
	-[0x80000144]:sw t5, 0(ra)
Current Store : [0x8000014c] : sw t5, 16(ra) -- Store: [0x80011628]:0x00000000




Last Coverpoint : ['mnemonic : fsgnj.d', 'rs1 : x28', 'rs2 : x30', 'rd : x30', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000013c]:fsgnj.d t5, t3, t5
	-[0x80000140]:csrrs tp, fcsr, zero
	-[0x80000144]:sw t5, 0(ra)
Current Store : [0x80000150] : sw tp, 24(ra) -- Store: [0x80011630]:0x00000000




Last Coverpoint : ['rs1 : x26', 'rs2 : x28', 'rd : x26', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000017c]:fsgnj.d s10, s10, t3
	-[0x80000180]:csrrs tp, fcsr, zero
	-[0x80000184]:sw s10, 32(ra)
Current Store : [0x80000188] : sw s11, 40(ra) -- Store: [0x80011640]:0x00000000




Last Coverpoint : ['rs1 : x26', 'rs2 : x28', 'rd : x26', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000017c]:fsgnj.d s10, s10, t3
	-[0x80000180]:csrrs tp, fcsr, zero
	-[0x80000184]:sw s10, 32(ra)
Current Store : [0x8000018c] : sw s10, 48(ra) -- Store: [0x80011648]:0x00000000




Last Coverpoint : ['rs1 : x26', 'rs2 : x28', 'rd : x26', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000017c]:fsgnj.d s10, s10, t3
	-[0x80000180]:csrrs tp, fcsr, zero
	-[0x80000184]:sw s10, 32(ra)
Current Store : [0x80000190] : sw tp, 56(ra) -- Store: [0x80011650]:0x00000000




Last Coverpoint : ['rs1 : x24', 'rs2 : x24', 'rd : x28', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x800001bc]:fsgnj.d t3, s8, s8
	-[0x800001c0]:csrrs tp, fcsr, zero
	-[0x800001c4]:sw t3, 64(ra)
Current Store : [0x800001c8] : sw t4, 72(ra) -- Store: [0x80011660]:0x80000000




Last Coverpoint : ['rs1 : x24', 'rs2 : x24', 'rd : x28', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x800001bc]:fsgnj.d t3, s8, s8
	-[0x800001c0]:csrrs tp, fcsr, zero
	-[0x800001c4]:sw t3, 64(ra)
Current Store : [0x800001cc] : sw t3, 80(ra) -- Store: [0x80011668]:0x00000000




Last Coverpoint : ['rs1 : x24', 'rs2 : x24', 'rd : x28', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x800001bc]:fsgnj.d t3, s8, s8
	-[0x800001c0]:csrrs tp, fcsr, zero
	-[0x800001c4]:sw t3, 64(ra)
Current Store : [0x800001d0] : sw tp, 88(ra) -- Store: [0x80011670]:0x00000000




Last Coverpoint : ['rs1 : x30', 'rs2 : x26', 'rd : x24', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800001fc]:fsgnj.d s8, t5, s10
	-[0x80000200]:csrrs tp, fcsr, zero
	-[0x80000204]:sw s8, 96(ra)
Current Store : [0x80000208] : sw s9, 104(ra) -- Store: [0x80011680]:0x00000000




Last Coverpoint : ['rs1 : x30', 'rs2 : x26', 'rd : x24', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800001fc]:fsgnj.d s8, t5, s10
	-[0x80000200]:csrrs tp, fcsr, zero
	-[0x80000204]:sw s8, 96(ra)
Current Store : [0x8000020c] : sw s8, 112(ra) -- Store: [0x80011688]:0x00000000




Last Coverpoint : ['rs1 : x30', 'rs2 : x26', 'rd : x24', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800001fc]:fsgnj.d s8, t5, s10
	-[0x80000200]:csrrs tp, fcsr, zero
	-[0x80000204]:sw s8, 96(ra)
Current Store : [0x80000210] : sw tp, 120(ra) -- Store: [0x80011690]:0x00000000




Last Coverpoint : ['rs1 : x22', 'rs2 : x22', 'rd : x22', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x8000023c]:fsgnj.d s6, s6, s6
	-[0x80000240]:csrrs tp, fcsr, zero
	-[0x80000244]:sw s6, 128(ra)
Current Store : [0x80000248] : sw s7, 136(ra) -- Store: [0x800116a0]:0x00000000




Last Coverpoint : ['rs1 : x22', 'rs2 : x22', 'rd : x22', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x8000023c]:fsgnj.d s6, s6, s6
	-[0x80000240]:csrrs tp, fcsr, zero
	-[0x80000244]:sw s6, 128(ra)
Current Store : [0x8000024c] : sw s6, 144(ra) -- Store: [0x800116a8]:0x00000000




Last Coverpoint : ['rs1 : x22', 'rs2 : x22', 'rd : x22', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x8000023c]:fsgnj.d s6, s6, s6
	-[0x80000240]:csrrs tp, fcsr, zero
	-[0x80000244]:sw s6, 128(ra)
Current Store : [0x80000250] : sw tp, 152(ra) -- Store: [0x800116b0]:0x00000000




Last Coverpoint : ['rs1 : x18', 'rs2 : x16', 'rd : x20', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000027c]:fsgnj.d s4, s2, a6
	-[0x80000280]:csrrs tp, fcsr, zero
	-[0x80000284]:sw s4, 160(ra)
Current Store : [0x80000288] : sw s5, 168(ra) -- Store: [0x800116c0]:0xDBEADFEE




Last Coverpoint : ['rs1 : x18', 'rs2 : x16', 'rd : x20', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000027c]:fsgnj.d s4, s2, a6
	-[0x80000280]:csrrs tp, fcsr, zero
	-[0x80000284]:sw s4, 160(ra)
Current Store : [0x8000028c] : sw s4, 176(ra) -- Store: [0x800116c8]:0x00000000




Last Coverpoint : ['rs1 : x18', 'rs2 : x16', 'rd : x20', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000027c]:fsgnj.d s4, s2, a6
	-[0x80000280]:csrrs tp, fcsr, zero
	-[0x80000284]:sw s4, 160(ra)
Current Store : [0x80000290] : sw tp, 184(ra) -- Store: [0x800116d0]:0x00000000




Last Coverpoint : ['rs1 : x16', 'rs2 : x20', 'rd : x18', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800002c0]:fsgnj.d s2, a6, s4
	-[0x800002c4]:csrrs tp, fcsr, zero
	-[0x800002c8]:sw s2, 192(ra)
Current Store : [0x800002cc] : sw s3, 200(ra) -- Store: [0x800116e0]:0x00000000




Last Coverpoint : ['rs1 : x16', 'rs2 : x20', 'rd : x18', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800002c0]:fsgnj.d s2, a6, s4
	-[0x800002c4]:csrrs tp, fcsr, zero
	-[0x800002c8]:sw s2, 192(ra)
Current Store : [0x800002d0] : sw s2, 208(ra) -- Store: [0x800116e8]:0x00000000




Last Coverpoint : ['rs1 : x16', 'rs2 : x20', 'rd : x18', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800002c0]:fsgnj.d s2, a6, s4
	-[0x800002c4]:csrrs tp, fcsr, zero
	-[0x800002c8]:sw s2, 192(ra)
Current Store : [0x800002d4] : sw tp, 216(ra) -- Store: [0x800116f0]:0x00000000




Last Coverpoint : ['rs1 : x20', 'rs2 : x18', 'rd : x16', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000304]:fsgnj.d a6, s4, s2
	-[0x80000308]:csrrs tp, fcsr, zero
	-[0x8000030c]:sw a6, 224(ra)
Current Store : [0x80000310] : sw a7, 232(ra) -- Store: [0x80011700]:0x00000000




Last Coverpoint : ['rs1 : x20', 'rs2 : x18', 'rd : x16', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000304]:fsgnj.d a6, s4, s2
	-[0x80000308]:csrrs tp, fcsr, zero
	-[0x8000030c]:sw a6, 224(ra)
Current Store : [0x80000314] : sw a6, 240(ra) -- Store: [0x80011708]:0x00000000




Last Coverpoint : ['rs1 : x20', 'rs2 : x18', 'rd : x16', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000304]:fsgnj.d a6, s4, s2
	-[0x80000308]:csrrs tp, fcsr, zero
	-[0x8000030c]:sw a6, 224(ra)
Current Store : [0x80000318] : sw tp, 248(ra) -- Store: [0x80011710]:0x00000000




Last Coverpoint : ['rs1 : x12', 'rs2 : x10', 'rd : x14', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000344]:fsgnj.d a4, a2, a0
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:sw a4, 256(ra)
Current Store : [0x80000350] : sw a5, 264(ra) -- Store: [0x80011720]:0xFAB7FBB6




Last Coverpoint : ['rs1 : x12', 'rs2 : x10', 'rd : x14', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000344]:fsgnj.d a4, a2, a0
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:sw a4, 256(ra)
Current Store : [0x80000354] : sw a4, 272(ra) -- Store: [0x80011728]:0x00000000




Last Coverpoint : ['rs1 : x12', 'rs2 : x10', 'rd : x14', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000344]:fsgnj.d a4, a2, a0
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:sw a4, 256(ra)
Current Store : [0x80000358] : sw tp, 280(ra) -- Store: [0x80011730]:0x00000000




Last Coverpoint : ['rs1 : x10', 'rs2 : x14', 'rd : x12', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000038c]:fsgnj.d a2, a0, a4
	-[0x80000390]:csrrs s2, fcsr, zero
	-[0x80000394]:sw a2, 288(ra)
Current Store : [0x80000398] : sw a3, 296(ra) -- Store: [0x80011740]:0x00000000




Last Coverpoint : ['rs1 : x10', 'rs2 : x14', 'rd : x12', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000038c]:fsgnj.d a2, a0, a4
	-[0x80000390]:csrrs s2, fcsr, zero
	-[0x80000394]:sw a2, 288(ra)
Current Store : [0x8000039c] : sw a2, 304(ra) -- Store: [0x80011748]:0x00000000




Last Coverpoint : ['rs1 : x10', 'rs2 : x14', 'rd : x12', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000038c]:fsgnj.d a2, a0, a4
	-[0x80000390]:csrrs s2, fcsr, zero
	-[0x80000394]:sw a2, 288(ra)
Current Store : [0x800003a0] : sw s2, 312(ra) -- Store: [0x80011750]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rs2 : x12', 'rd : x10', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800003cc]:fsgnj.d a0, a4, a2
	-[0x800003d0]:csrrs s2, fcsr, zero
	-[0x800003d4]:sw a0, 320(ra)
Current Store : [0x800003d8] : sw a1, 328(ra) -- Store: [0x80011760]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rs2 : x12', 'rd : x10', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800003cc]:fsgnj.d a0, a4, a2
	-[0x800003d0]:csrrs s2, fcsr, zero
	-[0x800003d4]:sw a0, 320(ra)
Current Store : [0x800003dc] : sw a0, 336(ra) -- Store: [0x80011768]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rs2 : x12', 'rd : x10', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800003cc]:fsgnj.d a0, a4, a2
	-[0x800003d0]:csrrs s2, fcsr, zero
	-[0x800003d4]:sw a0, 320(ra)
Current Store : [0x800003e0] : sw s2, 344(ra) -- Store: [0x80011770]:0x00000000




Last Coverpoint : ['rs1 : x6', 'rs2 : x4', 'rd : x8', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000040c]:fsgnj.d fp, t1, tp
	-[0x80000410]:csrrs s2, fcsr, zero
	-[0x80000414]:sw fp, 352(ra)
Current Store : [0x80000418] : sw s1, 360(ra) -- Store: [0x80011780]:0xADFEEDBE




Last Coverpoint : ['rs1 : x6', 'rs2 : x4', 'rd : x8', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000040c]:fsgnj.d fp, t1, tp
	-[0x80000410]:csrrs s2, fcsr, zero
	-[0x80000414]:sw fp, 352(ra)
Current Store : [0x8000041c] : sw fp, 368(ra) -- Store: [0x80011788]:0x00000000




Last Coverpoint : ['rs1 : x6', 'rs2 : x4', 'rd : x8', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000040c]:fsgnj.d fp, t1, tp
	-[0x80000410]:csrrs s2, fcsr, zero
	-[0x80000414]:sw fp, 352(ra)
Current Store : [0x80000420] : sw s2, 376(ra) -- Store: [0x80011790]:0x00000000




Last Coverpoint : ['rs1 : x4', 'rs2 : x8', 'rd : x6', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000458]:fsgnj.d t1, tp, fp
	-[0x8000045c]:csrrs s2, fcsr, zero
	-[0x80000460]:sw t1, 0(ra)
Current Store : [0x80000464] : sw t2, 8(ra) -- Store: [0x800116e0]:0x00000000




Last Coverpoint : ['rs1 : x4', 'rs2 : x8', 'rd : x6', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000458]:fsgnj.d t1, tp, fp
	-[0x8000045c]:csrrs s2, fcsr, zero
	-[0x80000460]:sw t1, 0(ra)
Current Store : [0x80000468] : sw t1, 16(ra) -- Store: [0x800116e8]:0x00000000




Last Coverpoint : ['rs1 : x4', 'rs2 : x8', 'rd : x6', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000458]:fsgnj.d t1, tp, fp
	-[0x8000045c]:csrrs s2, fcsr, zero
	-[0x80000460]:sw t1, 0(ra)
Current Store : [0x8000046c] : sw s2, 24(ra) -- Store: [0x800116f0]:0x00000000




Last Coverpoint : ['rs1 : x8', 'rs2 : x6', 'rd : x4', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000049c]:fsgnj.d tp, fp, t1
	-[0x800004a0]:csrrs s2, fcsr, zero
	-[0x800004a4]:sw tp, 32(ra)
Current Store : [0x800004a8] : sw t0, 40(ra) -- Store: [0x80011700]:0x00000000




Last Coverpoint : ['rs1 : x8', 'rs2 : x6', 'rd : x4', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000049c]:fsgnj.d tp, fp, t1
	-[0x800004a0]:csrrs s2, fcsr, zero
	-[0x800004a4]:sw tp, 32(ra)
Current Store : [0x800004ac] : sw tp, 48(ra) -- Store: [0x80011708]:0x00000000




Last Coverpoint : ['rs1 : x8', 'rs2 : x6', 'rd : x4', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000049c]:fsgnj.d tp, fp, t1
	-[0x800004a0]:csrrs s2, fcsr, zero
	-[0x800004a4]:sw tp, 32(ra)
Current Store : [0x800004b0] : sw s2, 56(ra) -- Store: [0x80011710]:0x00000000




Last Coverpoint : ['rs1 : x2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800004dc]:fsgnj.d t5, sp, t3
	-[0x800004e0]:csrrs s2, fcsr, zero
	-[0x800004e4]:sw t5, 64(ra)
Current Store : [0x800004e8] : sw t6, 72(ra) -- Store: [0x80011720]:0x00000000




Last Coverpoint : ['rs1 : x2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800004dc]:fsgnj.d t5, sp, t3
	-[0x800004e0]:csrrs s2, fcsr, zero
	-[0x800004e4]:sw t5, 64(ra)
Current Store : [0x800004ec] : sw t5, 80(ra) -- Store: [0x80011728]:0x00000000




Last Coverpoint : ['rs1 : x2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800004dc]:fsgnj.d t5, sp, t3
	-[0x800004e0]:csrrs s2, fcsr, zero
	-[0x800004e4]:sw t5, 64(ra)
Current Store : [0x800004f0] : sw s2, 88(ra) -- Store: [0x80011730]:0x00000000




Last Coverpoint : ['rs2 : x2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000051c]:fsgnj.d t5, t3, sp
	-[0x80000520]:csrrs s2, fcsr, zero
	-[0x80000524]:sw t5, 96(ra)
Current Store : [0x80000528] : sw t6, 104(ra) -- Store: [0x80011740]:0x00000000




Last Coverpoint : ['rs2 : x2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000051c]:fsgnj.d t5, t3, sp
	-[0x80000520]:csrrs s2, fcsr, zero
	-[0x80000524]:sw t5, 96(ra)
Current Store : [0x8000052c] : sw t5, 112(ra) -- Store: [0x80011748]:0x00000000




Last Coverpoint : ['rs2 : x2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000051c]:fsgnj.d t5, t3, sp
	-[0x80000520]:csrrs s2, fcsr, zero
	-[0x80000524]:sw t5, 96(ra)
Current Store : [0x80000530] : sw s2, 120(ra) -- Store: [0x80011750]:0x00000000




Last Coverpoint : ['rd : x2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000055c]:fsgnj.d sp, t5, t3
	-[0x80000560]:csrrs s2, fcsr, zero
	-[0x80000564]:sw sp, 128(ra)
Current Store : [0x80000568] : sw gp, 136(ra) -- Store: [0x80011760]:0xFFF00000




Last Coverpoint : ['rd : x2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000055c]:fsgnj.d sp, t5, t3
	-[0x80000560]:csrrs s2, fcsr, zero
	-[0x80000564]:sw sp, 128(ra)
Current Store : [0x8000056c] : sw sp, 144(ra) -- Store: [0x80011768]:0x00000000




Last Coverpoint : ['rd : x2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000055c]:fsgnj.d sp, t5, t3
	-[0x80000560]:csrrs s2, fcsr, zero
	-[0x80000564]:sw sp, 128(ra)
Current Store : [0x80000570] : sw s2, 152(ra) -- Store: [0x80011770]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000059c]:fsgnj.d t5, t3, s10
	-[0x800005a0]:csrrs s2, fcsr, zero
	-[0x800005a4]:sw t5, 160(ra)
Current Store : [0x800005a8] : sw t6, 168(ra) -- Store: [0x80011780]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000059c]:fsgnj.d t5, t3, s10
	-[0x800005a0]:csrrs s2, fcsr, zero
	-[0x800005a4]:sw t5, 160(ra)
Current Store : [0x800005ac] : sw t5, 176(ra) -- Store: [0x80011788]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000059c]:fsgnj.d t5, t3, s10
	-[0x800005a0]:csrrs s2, fcsr, zero
	-[0x800005a4]:sw t5, 160(ra)
Current Store : [0x800005b0] : sw s2, 184(ra) -- Store: [0x80011790]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800005e4]:fsgnj.d t5, t3, s10
	-[0x800005e8]:csrrs gp, fcsr, zero
	-[0x800005ec]:sw t5, 192(ra)
Current Store : [0x800005f0] : sw t6, 200(ra) -- Store: [0x800117a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800005e4]:fsgnj.d t5, t3, s10
	-[0x800005e8]:csrrs gp, fcsr, zero
	-[0x800005ec]:sw t5, 192(ra)
Current Store : [0x800005f4] : sw t5, 208(ra) -- Store: [0x800117a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800005e4]:fsgnj.d t5, t3, s10
	-[0x800005e8]:csrrs gp, fcsr, zero
	-[0x800005ec]:sw t5, 192(ra)
Current Store : [0x800005f8] : sw gp, 216(ra) -- Store: [0x800117b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000624]:fsgnj.d t5, t3, s10
	-[0x80000628]:csrrs gp, fcsr, zero
	-[0x8000062c]:sw t5, 224(ra)
Current Store : [0x80000630] : sw t6, 232(ra) -- Store: [0x800117c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000624]:fsgnj.d t5, t3, s10
	-[0x80000628]:csrrs gp, fcsr, zero
	-[0x8000062c]:sw t5, 224(ra)
Current Store : [0x80000634] : sw t5, 240(ra) -- Store: [0x800117c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000624]:fsgnj.d t5, t3, s10
	-[0x80000628]:csrrs gp, fcsr, zero
	-[0x8000062c]:sw t5, 224(ra)
Current Store : [0x80000638] : sw gp, 248(ra) -- Store: [0x800117d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000664]:fsgnj.d t5, t3, s10
	-[0x80000668]:csrrs gp, fcsr, zero
	-[0x8000066c]:sw t5, 256(ra)
Current Store : [0x80000670] : sw t6, 264(ra) -- Store: [0x800117e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000664]:fsgnj.d t5, t3, s10
	-[0x80000668]:csrrs gp, fcsr, zero
	-[0x8000066c]:sw t5, 256(ra)
Current Store : [0x80000674] : sw t5, 272(ra) -- Store: [0x800117e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000664]:fsgnj.d t5, t3, s10
	-[0x80000668]:csrrs gp, fcsr, zero
	-[0x8000066c]:sw t5, 256(ra)
Current Store : [0x80000678] : sw gp, 280(ra) -- Store: [0x800117f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800006a4]:fsgnj.d t5, t3, s10
	-[0x800006a8]:csrrs gp, fcsr, zero
	-[0x800006ac]:sw t5, 288(ra)
Current Store : [0x800006b0] : sw t6, 296(ra) -- Store: [0x80011800]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800006a4]:fsgnj.d t5, t3, s10
	-[0x800006a8]:csrrs gp, fcsr, zero
	-[0x800006ac]:sw t5, 288(ra)
Current Store : [0x800006b4] : sw t5, 304(ra) -- Store: [0x80011808]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800006a4]:fsgnj.d t5, t3, s10
	-[0x800006a8]:csrrs gp, fcsr, zero
	-[0x800006ac]:sw t5, 288(ra)
Current Store : [0x800006b8] : sw gp, 312(ra) -- Store: [0x80011810]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800006e4]:fsgnj.d t5, t3, s10
	-[0x800006e8]:csrrs gp, fcsr, zero
	-[0x800006ec]:sw t5, 320(ra)
Current Store : [0x800006f0] : sw t6, 328(ra) -- Store: [0x80011820]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800006e4]:fsgnj.d t5, t3, s10
	-[0x800006e8]:csrrs gp, fcsr, zero
	-[0x800006ec]:sw t5, 320(ra)
Current Store : [0x800006f4] : sw t5, 336(ra) -- Store: [0x80011828]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800006e4]:fsgnj.d t5, t3, s10
	-[0x800006e8]:csrrs gp, fcsr, zero
	-[0x800006ec]:sw t5, 320(ra)
Current Store : [0x800006f8] : sw gp, 344(ra) -- Store: [0x80011830]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000724]:fsgnj.d t5, t3, s10
	-[0x80000728]:csrrs gp, fcsr, zero
	-[0x8000072c]:sw t5, 352(ra)
Current Store : [0x80000730] : sw t6, 360(ra) -- Store: [0x80011840]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000724]:fsgnj.d t5, t3, s10
	-[0x80000728]:csrrs gp, fcsr, zero
	-[0x8000072c]:sw t5, 352(ra)
Current Store : [0x80000734] : sw t5, 368(ra) -- Store: [0x80011848]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000724]:fsgnj.d t5, t3, s10
	-[0x80000728]:csrrs gp, fcsr, zero
	-[0x8000072c]:sw t5, 352(ra)
Current Store : [0x80000738] : sw gp, 376(ra) -- Store: [0x80011850]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000764]:fsgnj.d t5, t3, s10
	-[0x80000768]:csrrs gp, fcsr, zero
	-[0x8000076c]:sw t5, 384(ra)
Current Store : [0x80000770] : sw t6, 392(ra) -- Store: [0x80011860]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000764]:fsgnj.d t5, t3, s10
	-[0x80000768]:csrrs gp, fcsr, zero
	-[0x8000076c]:sw t5, 384(ra)
Current Store : [0x80000774] : sw t5, 400(ra) -- Store: [0x80011868]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000764]:fsgnj.d t5, t3, s10
	-[0x80000768]:csrrs gp, fcsr, zero
	-[0x8000076c]:sw t5, 384(ra)
Current Store : [0x80000778] : sw gp, 408(ra) -- Store: [0x80011870]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800007a4]:fsgnj.d t5, t3, s10
	-[0x800007a8]:csrrs gp, fcsr, zero
	-[0x800007ac]:sw t5, 416(ra)
Current Store : [0x800007b0] : sw t6, 424(ra) -- Store: [0x80011880]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800007a4]:fsgnj.d t5, t3, s10
	-[0x800007a8]:csrrs gp, fcsr, zero
	-[0x800007ac]:sw t5, 416(ra)
Current Store : [0x800007b4] : sw t5, 432(ra) -- Store: [0x80011888]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800007a4]:fsgnj.d t5, t3, s10
	-[0x800007a8]:csrrs gp, fcsr, zero
	-[0x800007ac]:sw t5, 416(ra)
Current Store : [0x800007b8] : sw gp, 440(ra) -- Store: [0x80011890]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800007e4]:fsgnj.d t5, t3, s10
	-[0x800007e8]:csrrs gp, fcsr, zero
	-[0x800007ec]:sw t5, 448(ra)
Current Store : [0x800007f0] : sw t6, 456(ra) -- Store: [0x800118a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800007e4]:fsgnj.d t5, t3, s10
	-[0x800007e8]:csrrs gp, fcsr, zero
	-[0x800007ec]:sw t5, 448(ra)
Current Store : [0x800007f4] : sw t5, 464(ra) -- Store: [0x800118a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800007e4]:fsgnj.d t5, t3, s10
	-[0x800007e8]:csrrs gp, fcsr, zero
	-[0x800007ec]:sw t5, 448(ra)
Current Store : [0x800007f8] : sw gp, 472(ra) -- Store: [0x800118b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000824]:fsgnj.d t5, t3, s10
	-[0x80000828]:csrrs gp, fcsr, zero
	-[0x8000082c]:sw t5, 480(ra)
Current Store : [0x80000830] : sw t6, 488(ra) -- Store: [0x800118c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000824]:fsgnj.d t5, t3, s10
	-[0x80000828]:csrrs gp, fcsr, zero
	-[0x8000082c]:sw t5, 480(ra)
Current Store : [0x80000834] : sw t5, 496(ra) -- Store: [0x800118c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000824]:fsgnj.d t5, t3, s10
	-[0x80000828]:csrrs gp, fcsr, zero
	-[0x8000082c]:sw t5, 480(ra)
Current Store : [0x80000838] : sw gp, 504(ra) -- Store: [0x800118d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000864]:fsgnj.d t5, t3, s10
	-[0x80000868]:csrrs gp, fcsr, zero
	-[0x8000086c]:sw t5, 512(ra)
Current Store : [0x80000870] : sw t6, 520(ra) -- Store: [0x800118e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000864]:fsgnj.d t5, t3, s10
	-[0x80000868]:csrrs gp, fcsr, zero
	-[0x8000086c]:sw t5, 512(ra)
Current Store : [0x80000874] : sw t5, 528(ra) -- Store: [0x800118e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000864]:fsgnj.d t5, t3, s10
	-[0x80000868]:csrrs gp, fcsr, zero
	-[0x8000086c]:sw t5, 512(ra)
Current Store : [0x80000878] : sw gp, 536(ra) -- Store: [0x800118f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800008a4]:fsgnj.d t5, t3, s10
	-[0x800008a8]:csrrs gp, fcsr, zero
	-[0x800008ac]:sw t5, 544(ra)
Current Store : [0x800008b0] : sw t6, 552(ra) -- Store: [0x80011900]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800008a4]:fsgnj.d t5, t3, s10
	-[0x800008a8]:csrrs gp, fcsr, zero
	-[0x800008ac]:sw t5, 544(ra)
Current Store : [0x800008b4] : sw t5, 560(ra) -- Store: [0x80011908]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800008a4]:fsgnj.d t5, t3, s10
	-[0x800008a8]:csrrs gp, fcsr, zero
	-[0x800008ac]:sw t5, 544(ra)
Current Store : [0x800008b8] : sw gp, 568(ra) -- Store: [0x80011910]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800008e8]:fsgnj.d t5, t3, s10
	-[0x800008ec]:csrrs gp, fcsr, zero
	-[0x800008f0]:sw t5, 576(ra)
Current Store : [0x800008f4] : sw t6, 584(ra) -- Store: [0x80011920]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800008e8]:fsgnj.d t5, t3, s10
	-[0x800008ec]:csrrs gp, fcsr, zero
	-[0x800008f0]:sw t5, 576(ra)
Current Store : [0x800008f8] : sw t5, 592(ra) -- Store: [0x80011928]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800008e8]:fsgnj.d t5, t3, s10
	-[0x800008ec]:csrrs gp, fcsr, zero
	-[0x800008f0]:sw t5, 576(ra)
Current Store : [0x800008fc] : sw gp, 600(ra) -- Store: [0x80011930]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000092c]:fsgnj.d t5, t3, s10
	-[0x80000930]:csrrs gp, fcsr, zero
	-[0x80000934]:sw t5, 608(ra)
Current Store : [0x80000938] : sw t6, 616(ra) -- Store: [0x80011940]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000092c]:fsgnj.d t5, t3, s10
	-[0x80000930]:csrrs gp, fcsr, zero
	-[0x80000934]:sw t5, 608(ra)
Current Store : [0x8000093c] : sw t5, 624(ra) -- Store: [0x80011948]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000092c]:fsgnj.d t5, t3, s10
	-[0x80000930]:csrrs gp, fcsr, zero
	-[0x80000934]:sw t5, 608(ra)
Current Store : [0x80000940] : sw gp, 632(ra) -- Store: [0x80011950]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000096c]:fsgnj.d t5, t3, s10
	-[0x80000970]:csrrs gp, fcsr, zero
	-[0x80000974]:sw t5, 640(ra)
Current Store : [0x80000978] : sw t6, 648(ra) -- Store: [0x80011960]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000096c]:fsgnj.d t5, t3, s10
	-[0x80000970]:csrrs gp, fcsr, zero
	-[0x80000974]:sw t5, 640(ra)
Current Store : [0x8000097c] : sw t5, 656(ra) -- Store: [0x80011968]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000096c]:fsgnj.d t5, t3, s10
	-[0x80000970]:csrrs gp, fcsr, zero
	-[0x80000974]:sw t5, 640(ra)
Current Store : [0x80000980] : sw gp, 664(ra) -- Store: [0x80011970]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800009ac]:fsgnj.d t5, t3, s10
	-[0x800009b0]:csrrs gp, fcsr, zero
	-[0x800009b4]:sw t5, 672(ra)
Current Store : [0x800009b8] : sw t6, 680(ra) -- Store: [0x80011980]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800009ac]:fsgnj.d t5, t3, s10
	-[0x800009b0]:csrrs gp, fcsr, zero
	-[0x800009b4]:sw t5, 672(ra)
Current Store : [0x800009bc] : sw t5, 688(ra) -- Store: [0x80011988]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800009ac]:fsgnj.d t5, t3, s10
	-[0x800009b0]:csrrs gp, fcsr, zero
	-[0x800009b4]:sw t5, 672(ra)
Current Store : [0x800009c0] : sw gp, 696(ra) -- Store: [0x80011990]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800009ec]:fsgnj.d t5, t3, s10
	-[0x800009f0]:csrrs gp, fcsr, zero
	-[0x800009f4]:sw t5, 704(ra)
Current Store : [0x800009f8] : sw t6, 712(ra) -- Store: [0x800119a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800009ec]:fsgnj.d t5, t3, s10
	-[0x800009f0]:csrrs gp, fcsr, zero
	-[0x800009f4]:sw t5, 704(ra)
Current Store : [0x800009fc] : sw t5, 720(ra) -- Store: [0x800119a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800009ec]:fsgnj.d t5, t3, s10
	-[0x800009f0]:csrrs gp, fcsr, zero
	-[0x800009f4]:sw t5, 704(ra)
Current Store : [0x80000a00] : sw gp, 728(ra) -- Store: [0x800119b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000a2c]:fsgnj.d t5, t3, s10
	-[0x80000a30]:csrrs gp, fcsr, zero
	-[0x80000a34]:sw t5, 736(ra)
Current Store : [0x80000a38] : sw t6, 744(ra) -- Store: [0x800119c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000a2c]:fsgnj.d t5, t3, s10
	-[0x80000a30]:csrrs gp, fcsr, zero
	-[0x80000a34]:sw t5, 736(ra)
Current Store : [0x80000a3c] : sw t5, 752(ra) -- Store: [0x800119c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000a2c]:fsgnj.d t5, t3, s10
	-[0x80000a30]:csrrs gp, fcsr, zero
	-[0x80000a34]:sw t5, 736(ra)
Current Store : [0x80000a40] : sw gp, 760(ra) -- Store: [0x800119d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000a70]:fsgnj.d t5, t3, s10
	-[0x80000a74]:csrrs gp, fcsr, zero
	-[0x80000a78]:sw t5, 768(ra)
Current Store : [0x80000a7c] : sw t6, 776(ra) -- Store: [0x800119e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000a70]:fsgnj.d t5, t3, s10
	-[0x80000a74]:csrrs gp, fcsr, zero
	-[0x80000a78]:sw t5, 768(ra)
Current Store : [0x80000a80] : sw t5, 784(ra) -- Store: [0x800119e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000a70]:fsgnj.d t5, t3, s10
	-[0x80000a74]:csrrs gp, fcsr, zero
	-[0x80000a78]:sw t5, 768(ra)
Current Store : [0x80000a84] : sw gp, 792(ra) -- Store: [0x800119f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000ab4]:fsgnj.d t5, t3, s10
	-[0x80000ab8]:csrrs gp, fcsr, zero
	-[0x80000abc]:sw t5, 800(ra)
Current Store : [0x80000ac0] : sw t6, 808(ra) -- Store: [0x80011a00]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000ab4]:fsgnj.d t5, t3, s10
	-[0x80000ab8]:csrrs gp, fcsr, zero
	-[0x80000abc]:sw t5, 800(ra)
Current Store : [0x80000ac4] : sw t5, 816(ra) -- Store: [0x80011a08]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000ab4]:fsgnj.d t5, t3, s10
	-[0x80000ab8]:csrrs gp, fcsr, zero
	-[0x80000abc]:sw t5, 800(ra)
Current Store : [0x80000ac8] : sw gp, 824(ra) -- Store: [0x80011a10]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000af4]:fsgnj.d t5, t3, s10
	-[0x80000af8]:csrrs gp, fcsr, zero
	-[0x80000afc]:sw t5, 832(ra)
Current Store : [0x80000b00] : sw t6, 840(ra) -- Store: [0x80011a20]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000af4]:fsgnj.d t5, t3, s10
	-[0x80000af8]:csrrs gp, fcsr, zero
	-[0x80000afc]:sw t5, 832(ra)
Current Store : [0x80000b04] : sw t5, 848(ra) -- Store: [0x80011a28]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000af4]:fsgnj.d t5, t3, s10
	-[0x80000af8]:csrrs gp, fcsr, zero
	-[0x80000afc]:sw t5, 832(ra)
Current Store : [0x80000b08] : sw gp, 856(ra) -- Store: [0x80011a30]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000b34]:fsgnj.d t5, t3, s10
	-[0x80000b38]:csrrs gp, fcsr, zero
	-[0x80000b3c]:sw t5, 864(ra)
Current Store : [0x80000b40] : sw t6, 872(ra) -- Store: [0x80011a40]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000b34]:fsgnj.d t5, t3, s10
	-[0x80000b38]:csrrs gp, fcsr, zero
	-[0x80000b3c]:sw t5, 864(ra)
Current Store : [0x80000b44] : sw t5, 880(ra) -- Store: [0x80011a48]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000b34]:fsgnj.d t5, t3, s10
	-[0x80000b38]:csrrs gp, fcsr, zero
	-[0x80000b3c]:sw t5, 864(ra)
Current Store : [0x80000b48] : sw gp, 888(ra) -- Store: [0x80011a50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000b74]:fsgnj.d t5, t3, s10
	-[0x80000b78]:csrrs gp, fcsr, zero
	-[0x80000b7c]:sw t5, 896(ra)
Current Store : [0x80000b80] : sw t6, 904(ra) -- Store: [0x80011a60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000b74]:fsgnj.d t5, t3, s10
	-[0x80000b78]:csrrs gp, fcsr, zero
	-[0x80000b7c]:sw t5, 896(ra)
Current Store : [0x80000b84] : sw t5, 912(ra) -- Store: [0x80011a68]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000b74]:fsgnj.d t5, t3, s10
	-[0x80000b78]:csrrs gp, fcsr, zero
	-[0x80000b7c]:sw t5, 896(ra)
Current Store : [0x80000b88] : sw gp, 920(ra) -- Store: [0x80011a70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000bb4]:fsgnj.d t5, t3, s10
	-[0x80000bb8]:csrrs gp, fcsr, zero
	-[0x80000bbc]:sw t5, 928(ra)
Current Store : [0x80000bc0] : sw t6, 936(ra) -- Store: [0x80011a80]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000bb4]:fsgnj.d t5, t3, s10
	-[0x80000bb8]:csrrs gp, fcsr, zero
	-[0x80000bbc]:sw t5, 928(ra)
Current Store : [0x80000bc4] : sw t5, 944(ra) -- Store: [0x80011a88]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000bb4]:fsgnj.d t5, t3, s10
	-[0x80000bb8]:csrrs gp, fcsr, zero
	-[0x80000bbc]:sw t5, 928(ra)
Current Store : [0x80000bc8] : sw gp, 952(ra) -- Store: [0x80011a90]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000bf4]:fsgnj.d t5, t3, s10
	-[0x80000bf8]:csrrs gp, fcsr, zero
	-[0x80000bfc]:sw t5, 960(ra)
Current Store : [0x80000c00] : sw t6, 968(ra) -- Store: [0x80011aa0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000bf4]:fsgnj.d t5, t3, s10
	-[0x80000bf8]:csrrs gp, fcsr, zero
	-[0x80000bfc]:sw t5, 960(ra)
Current Store : [0x80000c04] : sw t5, 976(ra) -- Store: [0x80011aa8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000bf4]:fsgnj.d t5, t3, s10
	-[0x80000bf8]:csrrs gp, fcsr, zero
	-[0x80000bfc]:sw t5, 960(ra)
Current Store : [0x80000c08] : sw gp, 984(ra) -- Store: [0x80011ab0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000c34]:fsgnj.d t5, t3, s10
	-[0x80000c38]:csrrs gp, fcsr, zero
	-[0x80000c3c]:sw t5, 992(ra)
Current Store : [0x80000c40] : sw t6, 1000(ra) -- Store: [0x80011ac0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000c34]:fsgnj.d t5, t3, s10
	-[0x80000c38]:csrrs gp, fcsr, zero
	-[0x80000c3c]:sw t5, 992(ra)
Current Store : [0x80000c44] : sw t5, 1008(ra) -- Store: [0x80011ac8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000c34]:fsgnj.d t5, t3, s10
	-[0x80000c38]:csrrs gp, fcsr, zero
	-[0x80000c3c]:sw t5, 992(ra)
Current Store : [0x80000c48] : sw gp, 1016(ra) -- Store: [0x80011ad0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000c74]:fsgnj.d t5, t3, s10
	-[0x80000c78]:csrrs gp, fcsr, zero
	-[0x80000c7c]:sw t5, 1024(ra)
Current Store : [0x80000c80] : sw t6, 1032(ra) -- Store: [0x80011ae0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000c74]:fsgnj.d t5, t3, s10
	-[0x80000c78]:csrrs gp, fcsr, zero
	-[0x80000c7c]:sw t5, 1024(ra)
Current Store : [0x80000c84] : sw t5, 1040(ra) -- Store: [0x80011ae8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000c74]:fsgnj.d t5, t3, s10
	-[0x80000c78]:csrrs gp, fcsr, zero
	-[0x80000c7c]:sw t5, 1024(ra)
Current Store : [0x80000c88] : sw gp, 1048(ra) -- Store: [0x80011af0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000cb4]:fsgnj.d t5, t3, s10
	-[0x80000cb8]:csrrs gp, fcsr, zero
	-[0x80000cbc]:sw t5, 1056(ra)
Current Store : [0x80000cc0] : sw t6, 1064(ra) -- Store: [0x80011b00]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000cb4]:fsgnj.d t5, t3, s10
	-[0x80000cb8]:csrrs gp, fcsr, zero
	-[0x80000cbc]:sw t5, 1056(ra)
Current Store : [0x80000cc4] : sw t5, 1072(ra) -- Store: [0x80011b08]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000cb4]:fsgnj.d t5, t3, s10
	-[0x80000cb8]:csrrs gp, fcsr, zero
	-[0x80000cbc]:sw t5, 1056(ra)
Current Store : [0x80000cc8] : sw gp, 1080(ra) -- Store: [0x80011b10]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000cf4]:fsgnj.d t5, t3, s10
	-[0x80000cf8]:csrrs gp, fcsr, zero
	-[0x80000cfc]:sw t5, 1088(ra)
Current Store : [0x80000d00] : sw t6, 1096(ra) -- Store: [0x80011b20]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000cf4]:fsgnj.d t5, t3, s10
	-[0x80000cf8]:csrrs gp, fcsr, zero
	-[0x80000cfc]:sw t5, 1088(ra)
Current Store : [0x80000d04] : sw t5, 1104(ra) -- Store: [0x80011b28]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000cf4]:fsgnj.d t5, t3, s10
	-[0x80000cf8]:csrrs gp, fcsr, zero
	-[0x80000cfc]:sw t5, 1088(ra)
Current Store : [0x80000d08] : sw gp, 1112(ra) -- Store: [0x80011b30]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000d34]:fsgnj.d t5, t3, s10
	-[0x80000d38]:csrrs gp, fcsr, zero
	-[0x80000d3c]:sw t5, 1120(ra)
Current Store : [0x80000d40] : sw t6, 1128(ra) -- Store: [0x80011b40]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000d34]:fsgnj.d t5, t3, s10
	-[0x80000d38]:csrrs gp, fcsr, zero
	-[0x80000d3c]:sw t5, 1120(ra)
Current Store : [0x80000d44] : sw t5, 1136(ra) -- Store: [0x80011b48]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000d34]:fsgnj.d t5, t3, s10
	-[0x80000d38]:csrrs gp, fcsr, zero
	-[0x80000d3c]:sw t5, 1120(ra)
Current Store : [0x80000d48] : sw gp, 1144(ra) -- Store: [0x80011b50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000d74]:fsgnj.d t5, t3, s10
	-[0x80000d78]:csrrs gp, fcsr, zero
	-[0x80000d7c]:sw t5, 1152(ra)
Current Store : [0x80000d80] : sw t6, 1160(ra) -- Store: [0x80011b60]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000d74]:fsgnj.d t5, t3, s10
	-[0x80000d78]:csrrs gp, fcsr, zero
	-[0x80000d7c]:sw t5, 1152(ra)
Current Store : [0x80000d84] : sw t5, 1168(ra) -- Store: [0x80011b68]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000d74]:fsgnj.d t5, t3, s10
	-[0x80000d78]:csrrs gp, fcsr, zero
	-[0x80000d7c]:sw t5, 1152(ra)
Current Store : [0x80000d88] : sw gp, 1176(ra) -- Store: [0x80011b70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000db4]:fsgnj.d t5, t3, s10
	-[0x80000db8]:csrrs gp, fcsr, zero
	-[0x80000dbc]:sw t5, 1184(ra)
Current Store : [0x80000dc0] : sw t6, 1192(ra) -- Store: [0x80011b80]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000db4]:fsgnj.d t5, t3, s10
	-[0x80000db8]:csrrs gp, fcsr, zero
	-[0x80000dbc]:sw t5, 1184(ra)
Current Store : [0x80000dc4] : sw t5, 1200(ra) -- Store: [0x80011b88]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000db4]:fsgnj.d t5, t3, s10
	-[0x80000db8]:csrrs gp, fcsr, zero
	-[0x80000dbc]:sw t5, 1184(ra)
Current Store : [0x80000dc8] : sw gp, 1208(ra) -- Store: [0x80011b90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000df4]:fsgnj.d t5, t3, s10
	-[0x80000df8]:csrrs gp, fcsr, zero
	-[0x80000dfc]:sw t5, 1216(ra)
Current Store : [0x80000e00] : sw t6, 1224(ra) -- Store: [0x80011ba0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000df4]:fsgnj.d t5, t3, s10
	-[0x80000df8]:csrrs gp, fcsr, zero
	-[0x80000dfc]:sw t5, 1216(ra)
Current Store : [0x80000e04] : sw t5, 1232(ra) -- Store: [0x80011ba8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000df4]:fsgnj.d t5, t3, s10
	-[0x80000df8]:csrrs gp, fcsr, zero
	-[0x80000dfc]:sw t5, 1216(ra)
Current Store : [0x80000e08] : sw gp, 1240(ra) -- Store: [0x80011bb0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000e34]:fsgnj.d t5, t3, s10
	-[0x80000e38]:csrrs gp, fcsr, zero
	-[0x80000e3c]:sw t5, 1248(ra)
Current Store : [0x80000e40] : sw t6, 1256(ra) -- Store: [0x80011bc0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000e34]:fsgnj.d t5, t3, s10
	-[0x80000e38]:csrrs gp, fcsr, zero
	-[0x80000e3c]:sw t5, 1248(ra)
Current Store : [0x80000e44] : sw t5, 1264(ra) -- Store: [0x80011bc8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000e34]:fsgnj.d t5, t3, s10
	-[0x80000e38]:csrrs gp, fcsr, zero
	-[0x80000e3c]:sw t5, 1248(ra)
Current Store : [0x80000e48] : sw gp, 1272(ra) -- Store: [0x80011bd0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000e74]:fsgnj.d t5, t3, s10
	-[0x80000e78]:csrrs gp, fcsr, zero
	-[0x80000e7c]:sw t5, 1280(ra)
Current Store : [0x80000e80] : sw t6, 1288(ra) -- Store: [0x80011be0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000e74]:fsgnj.d t5, t3, s10
	-[0x80000e78]:csrrs gp, fcsr, zero
	-[0x80000e7c]:sw t5, 1280(ra)
Current Store : [0x80000e84] : sw t5, 1296(ra) -- Store: [0x80011be8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000e74]:fsgnj.d t5, t3, s10
	-[0x80000e78]:csrrs gp, fcsr, zero
	-[0x80000e7c]:sw t5, 1280(ra)
Current Store : [0x80000e88] : sw gp, 1304(ra) -- Store: [0x80011bf0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000eb4]:fsgnj.d t5, t3, s10
	-[0x80000eb8]:csrrs gp, fcsr, zero
	-[0x80000ebc]:sw t5, 1312(ra)
Current Store : [0x80000ec0] : sw t6, 1320(ra) -- Store: [0x80011c00]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000eb4]:fsgnj.d t5, t3, s10
	-[0x80000eb8]:csrrs gp, fcsr, zero
	-[0x80000ebc]:sw t5, 1312(ra)
Current Store : [0x80000ec4] : sw t5, 1328(ra) -- Store: [0x80011c08]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000eb4]:fsgnj.d t5, t3, s10
	-[0x80000eb8]:csrrs gp, fcsr, zero
	-[0x80000ebc]:sw t5, 1312(ra)
Current Store : [0x80000ec8] : sw gp, 1336(ra) -- Store: [0x80011c10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000ef8]:fsgnj.d t5, t3, s10
	-[0x80000efc]:csrrs gp, fcsr, zero
	-[0x80000f00]:sw t5, 1344(ra)
Current Store : [0x80000f04] : sw t6, 1352(ra) -- Store: [0x80011c20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000ef8]:fsgnj.d t5, t3, s10
	-[0x80000efc]:csrrs gp, fcsr, zero
	-[0x80000f00]:sw t5, 1344(ra)
Current Store : [0x80000f08] : sw t5, 1360(ra) -- Store: [0x80011c28]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000ef8]:fsgnj.d t5, t3, s10
	-[0x80000efc]:csrrs gp, fcsr, zero
	-[0x80000f00]:sw t5, 1344(ra)
Current Store : [0x80000f0c] : sw gp, 1368(ra) -- Store: [0x80011c30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000f3c]:fsgnj.d t5, t3, s10
	-[0x80000f40]:csrrs gp, fcsr, zero
	-[0x80000f44]:sw t5, 1376(ra)
Current Store : [0x80000f48] : sw t6, 1384(ra) -- Store: [0x80011c40]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000f3c]:fsgnj.d t5, t3, s10
	-[0x80000f40]:csrrs gp, fcsr, zero
	-[0x80000f44]:sw t5, 1376(ra)
Current Store : [0x80000f4c] : sw t5, 1392(ra) -- Store: [0x80011c48]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000f3c]:fsgnj.d t5, t3, s10
	-[0x80000f40]:csrrs gp, fcsr, zero
	-[0x80000f44]:sw t5, 1376(ra)
Current Store : [0x80000f50] : sw gp, 1400(ra) -- Store: [0x80011c50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000f7c]:fsgnj.d t5, t3, s10
	-[0x80000f80]:csrrs gp, fcsr, zero
	-[0x80000f84]:sw t5, 1408(ra)
Current Store : [0x80000f88] : sw t6, 1416(ra) -- Store: [0x80011c60]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000f7c]:fsgnj.d t5, t3, s10
	-[0x80000f80]:csrrs gp, fcsr, zero
	-[0x80000f84]:sw t5, 1408(ra)
Current Store : [0x80000f8c] : sw t5, 1424(ra) -- Store: [0x80011c68]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000f7c]:fsgnj.d t5, t3, s10
	-[0x80000f80]:csrrs gp, fcsr, zero
	-[0x80000f84]:sw t5, 1408(ra)
Current Store : [0x80000f90] : sw gp, 1432(ra) -- Store: [0x80011c70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000fbc]:fsgnj.d t5, t3, s10
	-[0x80000fc0]:csrrs gp, fcsr, zero
	-[0x80000fc4]:sw t5, 1440(ra)
Current Store : [0x80000fc8] : sw t6, 1448(ra) -- Store: [0x80011c80]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000fbc]:fsgnj.d t5, t3, s10
	-[0x80000fc0]:csrrs gp, fcsr, zero
	-[0x80000fc4]:sw t5, 1440(ra)
Current Store : [0x80000fcc] : sw t5, 1456(ra) -- Store: [0x80011c88]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000fbc]:fsgnj.d t5, t3, s10
	-[0x80000fc0]:csrrs gp, fcsr, zero
	-[0x80000fc4]:sw t5, 1440(ra)
Current Store : [0x80000fd0] : sw gp, 1464(ra) -- Store: [0x80011c90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000ffc]:fsgnj.d t5, t3, s10
	-[0x80001000]:csrrs gp, fcsr, zero
	-[0x80001004]:sw t5, 1472(ra)
Current Store : [0x80001008] : sw t6, 1480(ra) -- Store: [0x80011ca0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000ffc]:fsgnj.d t5, t3, s10
	-[0x80001000]:csrrs gp, fcsr, zero
	-[0x80001004]:sw t5, 1472(ra)
Current Store : [0x8000100c] : sw t5, 1488(ra) -- Store: [0x80011ca8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000ffc]:fsgnj.d t5, t3, s10
	-[0x80001000]:csrrs gp, fcsr, zero
	-[0x80001004]:sw t5, 1472(ra)
Current Store : [0x80001010] : sw gp, 1496(ra) -- Store: [0x80011cb0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000103c]:fsgnj.d t5, t3, s10
	-[0x80001040]:csrrs gp, fcsr, zero
	-[0x80001044]:sw t5, 1504(ra)
Current Store : [0x80001048] : sw t6, 1512(ra) -- Store: [0x80011cc0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000103c]:fsgnj.d t5, t3, s10
	-[0x80001040]:csrrs gp, fcsr, zero
	-[0x80001044]:sw t5, 1504(ra)
Current Store : [0x8000104c] : sw t5, 1520(ra) -- Store: [0x80011cc8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000103c]:fsgnj.d t5, t3, s10
	-[0x80001040]:csrrs gp, fcsr, zero
	-[0x80001044]:sw t5, 1504(ra)
Current Store : [0x80001050] : sw gp, 1528(ra) -- Store: [0x80011cd0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001080]:fsgnj.d t5, t3, s10
	-[0x80001084]:csrrs gp, fcsr, zero
	-[0x80001088]:sw t5, 1536(ra)
Current Store : [0x8000108c] : sw t6, 1544(ra) -- Store: [0x80011ce0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001080]:fsgnj.d t5, t3, s10
	-[0x80001084]:csrrs gp, fcsr, zero
	-[0x80001088]:sw t5, 1536(ra)
Current Store : [0x80001090] : sw t5, 1552(ra) -- Store: [0x80011ce8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001080]:fsgnj.d t5, t3, s10
	-[0x80001084]:csrrs gp, fcsr, zero
	-[0x80001088]:sw t5, 1536(ra)
Current Store : [0x80001094] : sw gp, 1560(ra) -- Store: [0x80011cf0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800010c4]:fsgnj.d t5, t3, s10
	-[0x800010c8]:csrrs gp, fcsr, zero
	-[0x800010cc]:sw t5, 1568(ra)
Current Store : [0x800010d0] : sw t6, 1576(ra) -- Store: [0x80011d00]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800010c4]:fsgnj.d t5, t3, s10
	-[0x800010c8]:csrrs gp, fcsr, zero
	-[0x800010cc]:sw t5, 1568(ra)
Current Store : [0x800010d4] : sw t5, 1584(ra) -- Store: [0x80011d08]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800010c4]:fsgnj.d t5, t3, s10
	-[0x800010c8]:csrrs gp, fcsr, zero
	-[0x800010cc]:sw t5, 1568(ra)
Current Store : [0x800010d8] : sw gp, 1592(ra) -- Store: [0x80011d10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001104]:fsgnj.d t5, t3, s10
	-[0x80001108]:csrrs gp, fcsr, zero
	-[0x8000110c]:sw t5, 1600(ra)
Current Store : [0x80001110] : sw t6, 1608(ra) -- Store: [0x80011d20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001104]:fsgnj.d t5, t3, s10
	-[0x80001108]:csrrs gp, fcsr, zero
	-[0x8000110c]:sw t5, 1600(ra)
Current Store : [0x80001114] : sw t5, 1616(ra) -- Store: [0x80011d28]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001104]:fsgnj.d t5, t3, s10
	-[0x80001108]:csrrs gp, fcsr, zero
	-[0x8000110c]:sw t5, 1600(ra)
Current Store : [0x80001118] : sw gp, 1624(ra) -- Store: [0x80011d30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001144]:fsgnj.d t5, t3, s10
	-[0x80001148]:csrrs gp, fcsr, zero
	-[0x8000114c]:sw t5, 1632(ra)
Current Store : [0x80001150] : sw t6, 1640(ra) -- Store: [0x80011d40]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001144]:fsgnj.d t5, t3, s10
	-[0x80001148]:csrrs gp, fcsr, zero
	-[0x8000114c]:sw t5, 1632(ra)
Current Store : [0x80001154] : sw t5, 1648(ra) -- Store: [0x80011d48]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001144]:fsgnj.d t5, t3, s10
	-[0x80001148]:csrrs gp, fcsr, zero
	-[0x8000114c]:sw t5, 1632(ra)
Current Store : [0x80001158] : sw gp, 1656(ra) -- Store: [0x80011d50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001184]:fsgnj.d t5, t3, s10
	-[0x80001188]:csrrs gp, fcsr, zero
	-[0x8000118c]:sw t5, 1664(ra)
Current Store : [0x80001190] : sw t6, 1672(ra) -- Store: [0x80011d60]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001184]:fsgnj.d t5, t3, s10
	-[0x80001188]:csrrs gp, fcsr, zero
	-[0x8000118c]:sw t5, 1664(ra)
Current Store : [0x80001194] : sw t5, 1680(ra) -- Store: [0x80011d68]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001184]:fsgnj.d t5, t3, s10
	-[0x80001188]:csrrs gp, fcsr, zero
	-[0x8000118c]:sw t5, 1664(ra)
Current Store : [0x80001198] : sw gp, 1688(ra) -- Store: [0x80011d70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800011c4]:fsgnj.d t5, t3, s10
	-[0x800011c8]:csrrs gp, fcsr, zero
	-[0x800011cc]:sw t5, 1696(ra)
Current Store : [0x800011d0] : sw t6, 1704(ra) -- Store: [0x80011d80]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800011c4]:fsgnj.d t5, t3, s10
	-[0x800011c8]:csrrs gp, fcsr, zero
	-[0x800011cc]:sw t5, 1696(ra)
Current Store : [0x800011d4] : sw t5, 1712(ra) -- Store: [0x80011d88]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800011c4]:fsgnj.d t5, t3, s10
	-[0x800011c8]:csrrs gp, fcsr, zero
	-[0x800011cc]:sw t5, 1696(ra)
Current Store : [0x800011d8] : sw gp, 1720(ra) -- Store: [0x80011d90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001204]:fsgnj.d t5, t3, s10
	-[0x80001208]:csrrs gp, fcsr, zero
	-[0x8000120c]:sw t5, 1728(ra)
Current Store : [0x80001210] : sw t6, 1736(ra) -- Store: [0x80011da0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001204]:fsgnj.d t5, t3, s10
	-[0x80001208]:csrrs gp, fcsr, zero
	-[0x8000120c]:sw t5, 1728(ra)
Current Store : [0x80001214] : sw t5, 1744(ra) -- Store: [0x80011da8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001204]:fsgnj.d t5, t3, s10
	-[0x80001208]:csrrs gp, fcsr, zero
	-[0x8000120c]:sw t5, 1728(ra)
Current Store : [0x80001218] : sw gp, 1752(ra) -- Store: [0x80011db0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001244]:fsgnj.d t5, t3, s10
	-[0x80001248]:csrrs gp, fcsr, zero
	-[0x8000124c]:sw t5, 1760(ra)
Current Store : [0x80001250] : sw t6, 1768(ra) -- Store: [0x80011dc0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001244]:fsgnj.d t5, t3, s10
	-[0x80001248]:csrrs gp, fcsr, zero
	-[0x8000124c]:sw t5, 1760(ra)
Current Store : [0x80001254] : sw t5, 1776(ra) -- Store: [0x80011dc8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001244]:fsgnj.d t5, t3, s10
	-[0x80001248]:csrrs gp, fcsr, zero
	-[0x8000124c]:sw t5, 1760(ra)
Current Store : [0x80001258] : sw gp, 1784(ra) -- Store: [0x80011dd0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001284]:fsgnj.d t5, t3, s10
	-[0x80001288]:csrrs gp, fcsr, zero
	-[0x8000128c]:sw t5, 1792(ra)
Current Store : [0x80001290] : sw t6, 1800(ra) -- Store: [0x80011de0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001284]:fsgnj.d t5, t3, s10
	-[0x80001288]:csrrs gp, fcsr, zero
	-[0x8000128c]:sw t5, 1792(ra)
Current Store : [0x80001294] : sw t5, 1808(ra) -- Store: [0x80011de8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001284]:fsgnj.d t5, t3, s10
	-[0x80001288]:csrrs gp, fcsr, zero
	-[0x8000128c]:sw t5, 1792(ra)
Current Store : [0x80001298] : sw gp, 1816(ra) -- Store: [0x80011df0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800012c4]:fsgnj.d t5, t3, s10
	-[0x800012c8]:csrrs gp, fcsr, zero
	-[0x800012cc]:sw t5, 1824(ra)
Current Store : [0x800012d0] : sw t6, 1832(ra) -- Store: [0x80011e00]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800012c4]:fsgnj.d t5, t3, s10
	-[0x800012c8]:csrrs gp, fcsr, zero
	-[0x800012cc]:sw t5, 1824(ra)
Current Store : [0x800012d4] : sw t5, 1840(ra) -- Store: [0x80011e08]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800012c4]:fsgnj.d t5, t3, s10
	-[0x800012c8]:csrrs gp, fcsr, zero
	-[0x800012cc]:sw t5, 1824(ra)
Current Store : [0x800012d8] : sw gp, 1848(ra) -- Store: [0x80011e10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001304]:fsgnj.d t5, t3, s10
	-[0x80001308]:csrrs gp, fcsr, zero
	-[0x8000130c]:sw t5, 1856(ra)
Current Store : [0x80001310] : sw t6, 1864(ra) -- Store: [0x80011e20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001304]:fsgnj.d t5, t3, s10
	-[0x80001308]:csrrs gp, fcsr, zero
	-[0x8000130c]:sw t5, 1856(ra)
Current Store : [0x80001314] : sw t5, 1872(ra) -- Store: [0x80011e28]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001304]:fsgnj.d t5, t3, s10
	-[0x80001308]:csrrs gp, fcsr, zero
	-[0x8000130c]:sw t5, 1856(ra)
Current Store : [0x80001318] : sw gp, 1880(ra) -- Store: [0x80011e30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001344]:fsgnj.d t5, t3, s10
	-[0x80001348]:csrrs gp, fcsr, zero
	-[0x8000134c]:sw t5, 1888(ra)
Current Store : [0x80001350] : sw t6, 1896(ra) -- Store: [0x80011e40]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001344]:fsgnj.d t5, t3, s10
	-[0x80001348]:csrrs gp, fcsr, zero
	-[0x8000134c]:sw t5, 1888(ra)
Current Store : [0x80001354] : sw t5, 1904(ra) -- Store: [0x80011e48]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001344]:fsgnj.d t5, t3, s10
	-[0x80001348]:csrrs gp, fcsr, zero
	-[0x8000134c]:sw t5, 1888(ra)
Current Store : [0x80001358] : sw gp, 1912(ra) -- Store: [0x80011e50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001384]:fsgnj.d t5, t3, s10
	-[0x80001388]:csrrs gp, fcsr, zero
	-[0x8000138c]:sw t5, 1920(ra)
Current Store : [0x80001390] : sw t6, 1928(ra) -- Store: [0x80011e60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001384]:fsgnj.d t5, t3, s10
	-[0x80001388]:csrrs gp, fcsr, zero
	-[0x8000138c]:sw t5, 1920(ra)
Current Store : [0x80001394] : sw t5, 1936(ra) -- Store: [0x80011e68]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001384]:fsgnj.d t5, t3, s10
	-[0x80001388]:csrrs gp, fcsr, zero
	-[0x8000138c]:sw t5, 1920(ra)
Current Store : [0x80001398] : sw gp, 1944(ra) -- Store: [0x80011e70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800013c4]:fsgnj.d t5, t3, s10
	-[0x800013c8]:csrrs gp, fcsr, zero
	-[0x800013cc]:sw t5, 1952(ra)
Current Store : [0x800013d0] : sw t6, 1960(ra) -- Store: [0x80011e80]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800013c4]:fsgnj.d t5, t3, s10
	-[0x800013c8]:csrrs gp, fcsr, zero
	-[0x800013cc]:sw t5, 1952(ra)
Current Store : [0x800013d4] : sw t5, 1968(ra) -- Store: [0x80011e88]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800013c4]:fsgnj.d t5, t3, s10
	-[0x800013c8]:csrrs gp, fcsr, zero
	-[0x800013cc]:sw t5, 1952(ra)
Current Store : [0x800013d8] : sw gp, 1976(ra) -- Store: [0x80011e90]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001404]:fsgnj.d t5, t3, s10
	-[0x80001408]:csrrs gp, fcsr, zero
	-[0x8000140c]:sw t5, 1984(ra)
Current Store : [0x80001410] : sw t6, 1992(ra) -- Store: [0x80011ea0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001404]:fsgnj.d t5, t3, s10
	-[0x80001408]:csrrs gp, fcsr, zero
	-[0x8000140c]:sw t5, 1984(ra)
Current Store : [0x80001414] : sw t5, 2000(ra) -- Store: [0x80011ea8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001404]:fsgnj.d t5, t3, s10
	-[0x80001408]:csrrs gp, fcsr, zero
	-[0x8000140c]:sw t5, 1984(ra)
Current Store : [0x80001418] : sw gp, 2008(ra) -- Store: [0x80011eb0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001444]:fsgnj.d t5, t3, s10
	-[0x80001448]:csrrs gp, fcsr, zero
	-[0x8000144c]:sw t5, 2016(ra)
Current Store : [0x80001450] : sw t6, 2024(ra) -- Store: [0x80011ec0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001444]:fsgnj.d t5, t3, s10
	-[0x80001448]:csrrs gp, fcsr, zero
	-[0x8000144c]:sw t5, 2016(ra)
Current Store : [0x80001454] : sw t5, 2032(ra) -- Store: [0x80011ec8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001444]:fsgnj.d t5, t3, s10
	-[0x80001448]:csrrs gp, fcsr, zero
	-[0x8000144c]:sw t5, 2016(ra)
Current Store : [0x80001458] : sw gp, 2040(ra) -- Store: [0x80011ed0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001484]:fsgnj.d t5, t3, s10
	-[0x80001488]:csrrs gp, fcsr, zero
	-[0x8000148c]:addi ra, ra, 2040
	-[0x80001490]:sw t5, 8(ra)
Current Store : [0x80001494] : sw t6, 16(ra) -- Store: [0x80011ee0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001484]:fsgnj.d t5, t3, s10
	-[0x80001488]:csrrs gp, fcsr, zero
	-[0x8000148c]:addi ra, ra, 2040
	-[0x80001490]:sw t5, 8(ra)
Current Store : [0x80001498] : sw t5, 24(ra) -- Store: [0x80011ee8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001484]:fsgnj.d t5, t3, s10
	-[0x80001488]:csrrs gp, fcsr, zero
	-[0x8000148c]:addi ra, ra, 2040
	-[0x80001490]:sw t5, 8(ra)
Current Store : [0x8000149c] : sw gp, 32(ra) -- Store: [0x80011ef0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800014c8]:fsgnj.d t5, t3, s10
	-[0x800014cc]:csrrs gp, fcsr, zero
	-[0x800014d0]:sw t5, 40(ra)
Current Store : [0x800014d4] : sw t6, 48(ra) -- Store: [0x80011f00]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800014c8]:fsgnj.d t5, t3, s10
	-[0x800014cc]:csrrs gp, fcsr, zero
	-[0x800014d0]:sw t5, 40(ra)
Current Store : [0x800014d8] : sw t5, 56(ra) -- Store: [0x80011f08]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800014c8]:fsgnj.d t5, t3, s10
	-[0x800014cc]:csrrs gp, fcsr, zero
	-[0x800014d0]:sw t5, 40(ra)
Current Store : [0x800014dc] : sw gp, 64(ra) -- Store: [0x80011f10]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000150c]:fsgnj.d t5, t3, s10
	-[0x80001510]:csrrs gp, fcsr, zero
	-[0x80001514]:sw t5, 72(ra)
Current Store : [0x80001518] : sw t6, 80(ra) -- Store: [0x80011f20]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000150c]:fsgnj.d t5, t3, s10
	-[0x80001510]:csrrs gp, fcsr, zero
	-[0x80001514]:sw t5, 72(ra)
Current Store : [0x8000151c] : sw t5, 88(ra) -- Store: [0x80011f28]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000150c]:fsgnj.d t5, t3, s10
	-[0x80001510]:csrrs gp, fcsr, zero
	-[0x80001514]:sw t5, 72(ra)
Current Store : [0x80001520] : sw gp, 96(ra) -- Store: [0x80011f30]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001550]:fsgnj.d t5, t3, s10
	-[0x80001554]:csrrs gp, fcsr, zero
	-[0x80001558]:sw t5, 104(ra)
Current Store : [0x8000155c] : sw t6, 112(ra) -- Store: [0x80011f40]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001550]:fsgnj.d t5, t3, s10
	-[0x80001554]:csrrs gp, fcsr, zero
	-[0x80001558]:sw t5, 104(ra)
Current Store : [0x80001560] : sw t5, 120(ra) -- Store: [0x80011f48]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001550]:fsgnj.d t5, t3, s10
	-[0x80001554]:csrrs gp, fcsr, zero
	-[0x80001558]:sw t5, 104(ra)
Current Store : [0x80001564] : sw gp, 128(ra) -- Store: [0x80011f50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001590]:fsgnj.d t5, t3, s10
	-[0x80001594]:csrrs gp, fcsr, zero
	-[0x80001598]:sw t5, 136(ra)
Current Store : [0x8000159c] : sw t6, 144(ra) -- Store: [0x80011f60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001590]:fsgnj.d t5, t3, s10
	-[0x80001594]:csrrs gp, fcsr, zero
	-[0x80001598]:sw t5, 136(ra)
Current Store : [0x800015a0] : sw t5, 152(ra) -- Store: [0x80011f68]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001590]:fsgnj.d t5, t3, s10
	-[0x80001594]:csrrs gp, fcsr, zero
	-[0x80001598]:sw t5, 136(ra)
Current Store : [0x800015a4] : sw gp, 160(ra) -- Store: [0x80011f70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800015d0]:fsgnj.d t5, t3, s10
	-[0x800015d4]:csrrs gp, fcsr, zero
	-[0x800015d8]:sw t5, 168(ra)
Current Store : [0x800015dc] : sw t6, 176(ra) -- Store: [0x80011f80]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800015d0]:fsgnj.d t5, t3, s10
	-[0x800015d4]:csrrs gp, fcsr, zero
	-[0x800015d8]:sw t5, 168(ra)
Current Store : [0x800015e0] : sw t5, 184(ra) -- Store: [0x80011f88]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800015d0]:fsgnj.d t5, t3, s10
	-[0x800015d4]:csrrs gp, fcsr, zero
	-[0x800015d8]:sw t5, 168(ra)
Current Store : [0x800015e4] : sw gp, 192(ra) -- Store: [0x80011f90]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001610]:fsgnj.d t5, t3, s10
	-[0x80001614]:csrrs gp, fcsr, zero
	-[0x80001618]:sw t5, 200(ra)
Current Store : [0x8000161c] : sw t6, 208(ra) -- Store: [0x80011fa0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001610]:fsgnj.d t5, t3, s10
	-[0x80001614]:csrrs gp, fcsr, zero
	-[0x80001618]:sw t5, 200(ra)
Current Store : [0x80001620] : sw t5, 216(ra) -- Store: [0x80011fa8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001610]:fsgnj.d t5, t3, s10
	-[0x80001614]:csrrs gp, fcsr, zero
	-[0x80001618]:sw t5, 200(ra)
Current Store : [0x80001624] : sw gp, 224(ra) -- Store: [0x80011fb0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001650]:fsgnj.d t5, t3, s10
	-[0x80001654]:csrrs gp, fcsr, zero
	-[0x80001658]:sw t5, 232(ra)
Current Store : [0x8000165c] : sw t6, 240(ra) -- Store: [0x80011fc0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001650]:fsgnj.d t5, t3, s10
	-[0x80001654]:csrrs gp, fcsr, zero
	-[0x80001658]:sw t5, 232(ra)
Current Store : [0x80001660] : sw t5, 248(ra) -- Store: [0x80011fc8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001650]:fsgnj.d t5, t3, s10
	-[0x80001654]:csrrs gp, fcsr, zero
	-[0x80001658]:sw t5, 232(ra)
Current Store : [0x80001664] : sw gp, 256(ra) -- Store: [0x80011fd0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001694]:fsgnj.d t5, t3, s10
	-[0x80001698]:csrrs gp, fcsr, zero
	-[0x8000169c]:sw t5, 264(ra)
Current Store : [0x800016a0] : sw t6, 272(ra) -- Store: [0x80011fe0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001694]:fsgnj.d t5, t3, s10
	-[0x80001698]:csrrs gp, fcsr, zero
	-[0x8000169c]:sw t5, 264(ra)
Current Store : [0x800016a4] : sw t5, 280(ra) -- Store: [0x80011fe8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001694]:fsgnj.d t5, t3, s10
	-[0x80001698]:csrrs gp, fcsr, zero
	-[0x8000169c]:sw t5, 264(ra)
Current Store : [0x800016a8] : sw gp, 288(ra) -- Store: [0x80011ff0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800016d8]:fsgnj.d t5, t3, s10
	-[0x800016dc]:csrrs gp, fcsr, zero
	-[0x800016e0]:sw t5, 296(ra)
Current Store : [0x800016e4] : sw t6, 304(ra) -- Store: [0x80012000]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800016d8]:fsgnj.d t5, t3, s10
	-[0x800016dc]:csrrs gp, fcsr, zero
	-[0x800016e0]:sw t5, 296(ra)
Current Store : [0x800016e8] : sw t5, 312(ra) -- Store: [0x80012008]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800016d8]:fsgnj.d t5, t3, s10
	-[0x800016dc]:csrrs gp, fcsr, zero
	-[0x800016e0]:sw t5, 296(ra)
Current Store : [0x800016ec] : sw gp, 320(ra) -- Store: [0x80012010]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001718]:fsgnj.d t5, t3, s10
	-[0x8000171c]:csrrs gp, fcsr, zero
	-[0x80001720]:sw t5, 328(ra)
Current Store : [0x80001724] : sw t6, 336(ra) -- Store: [0x80012020]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001718]:fsgnj.d t5, t3, s10
	-[0x8000171c]:csrrs gp, fcsr, zero
	-[0x80001720]:sw t5, 328(ra)
Current Store : [0x80001728] : sw t5, 344(ra) -- Store: [0x80012028]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001718]:fsgnj.d t5, t3, s10
	-[0x8000171c]:csrrs gp, fcsr, zero
	-[0x80001720]:sw t5, 328(ra)
Current Store : [0x8000172c] : sw gp, 352(ra) -- Store: [0x80012030]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001758]:fsgnj.d t5, t3, s10
	-[0x8000175c]:csrrs gp, fcsr, zero
	-[0x80001760]:sw t5, 360(ra)
Current Store : [0x80001764] : sw t6, 368(ra) -- Store: [0x80012040]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001758]:fsgnj.d t5, t3, s10
	-[0x8000175c]:csrrs gp, fcsr, zero
	-[0x80001760]:sw t5, 360(ra)
Current Store : [0x80001768] : sw t5, 376(ra) -- Store: [0x80012048]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001758]:fsgnj.d t5, t3, s10
	-[0x8000175c]:csrrs gp, fcsr, zero
	-[0x80001760]:sw t5, 360(ra)
Current Store : [0x8000176c] : sw gp, 384(ra) -- Store: [0x80012050]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001798]:fsgnj.d t5, t3, s10
	-[0x8000179c]:csrrs gp, fcsr, zero
	-[0x800017a0]:sw t5, 392(ra)
Current Store : [0x800017a4] : sw t6, 400(ra) -- Store: [0x80012060]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001798]:fsgnj.d t5, t3, s10
	-[0x8000179c]:csrrs gp, fcsr, zero
	-[0x800017a0]:sw t5, 392(ra)
Current Store : [0x800017a8] : sw t5, 408(ra) -- Store: [0x80012068]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001798]:fsgnj.d t5, t3, s10
	-[0x8000179c]:csrrs gp, fcsr, zero
	-[0x800017a0]:sw t5, 392(ra)
Current Store : [0x800017ac] : sw gp, 416(ra) -- Store: [0x80012070]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800017d8]:fsgnj.d t5, t3, s10
	-[0x800017dc]:csrrs gp, fcsr, zero
	-[0x800017e0]:sw t5, 424(ra)
Current Store : [0x800017e4] : sw t6, 432(ra) -- Store: [0x80012080]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800017d8]:fsgnj.d t5, t3, s10
	-[0x800017dc]:csrrs gp, fcsr, zero
	-[0x800017e0]:sw t5, 424(ra)
Current Store : [0x800017e8] : sw t5, 440(ra) -- Store: [0x80012088]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800017d8]:fsgnj.d t5, t3, s10
	-[0x800017dc]:csrrs gp, fcsr, zero
	-[0x800017e0]:sw t5, 424(ra)
Current Store : [0x800017ec] : sw gp, 448(ra) -- Store: [0x80012090]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001818]:fsgnj.d t5, t3, s10
	-[0x8000181c]:csrrs gp, fcsr, zero
	-[0x80001820]:sw t5, 456(ra)
Current Store : [0x80001824] : sw t6, 464(ra) -- Store: [0x800120a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001818]:fsgnj.d t5, t3, s10
	-[0x8000181c]:csrrs gp, fcsr, zero
	-[0x80001820]:sw t5, 456(ra)
Current Store : [0x80001828] : sw t5, 472(ra) -- Store: [0x800120a8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001818]:fsgnj.d t5, t3, s10
	-[0x8000181c]:csrrs gp, fcsr, zero
	-[0x80001820]:sw t5, 456(ra)
Current Store : [0x8000182c] : sw gp, 480(ra) -- Store: [0x800120b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001858]:fsgnj.d t5, t3, s10
	-[0x8000185c]:csrrs gp, fcsr, zero
	-[0x80001860]:sw t5, 488(ra)
Current Store : [0x80001864] : sw t6, 496(ra) -- Store: [0x800120c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001858]:fsgnj.d t5, t3, s10
	-[0x8000185c]:csrrs gp, fcsr, zero
	-[0x80001860]:sw t5, 488(ra)
Current Store : [0x80001868] : sw t5, 504(ra) -- Store: [0x800120c8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001858]:fsgnj.d t5, t3, s10
	-[0x8000185c]:csrrs gp, fcsr, zero
	-[0x80001860]:sw t5, 488(ra)
Current Store : [0x8000186c] : sw gp, 512(ra) -- Store: [0x800120d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001898]:fsgnj.d t5, t3, s10
	-[0x8000189c]:csrrs gp, fcsr, zero
	-[0x800018a0]:sw t5, 520(ra)
Current Store : [0x800018a4] : sw t6, 528(ra) -- Store: [0x800120e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001898]:fsgnj.d t5, t3, s10
	-[0x8000189c]:csrrs gp, fcsr, zero
	-[0x800018a0]:sw t5, 520(ra)
Current Store : [0x800018a8] : sw t5, 536(ra) -- Store: [0x800120e8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001898]:fsgnj.d t5, t3, s10
	-[0x8000189c]:csrrs gp, fcsr, zero
	-[0x800018a0]:sw t5, 520(ra)
Current Store : [0x800018ac] : sw gp, 544(ra) -- Store: [0x800120f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800018d8]:fsgnj.d t5, t3, s10
	-[0x800018dc]:csrrs gp, fcsr, zero
	-[0x800018e0]:sw t5, 552(ra)
Current Store : [0x800018e4] : sw t6, 560(ra) -- Store: [0x80012100]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800018d8]:fsgnj.d t5, t3, s10
	-[0x800018dc]:csrrs gp, fcsr, zero
	-[0x800018e0]:sw t5, 552(ra)
Current Store : [0x800018e8] : sw t5, 568(ra) -- Store: [0x80012108]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800018d8]:fsgnj.d t5, t3, s10
	-[0x800018dc]:csrrs gp, fcsr, zero
	-[0x800018e0]:sw t5, 552(ra)
Current Store : [0x800018ec] : sw gp, 576(ra) -- Store: [0x80012110]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001918]:fsgnj.d t5, t3, s10
	-[0x8000191c]:csrrs gp, fcsr, zero
	-[0x80001920]:sw t5, 584(ra)
Current Store : [0x80001924] : sw t6, 592(ra) -- Store: [0x80012120]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001918]:fsgnj.d t5, t3, s10
	-[0x8000191c]:csrrs gp, fcsr, zero
	-[0x80001920]:sw t5, 584(ra)
Current Store : [0x80001928] : sw t5, 600(ra) -- Store: [0x80012128]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001918]:fsgnj.d t5, t3, s10
	-[0x8000191c]:csrrs gp, fcsr, zero
	-[0x80001920]:sw t5, 584(ra)
Current Store : [0x8000192c] : sw gp, 608(ra) -- Store: [0x80012130]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001958]:fsgnj.d t5, t3, s10
	-[0x8000195c]:csrrs gp, fcsr, zero
	-[0x80001960]:sw t5, 616(ra)
Current Store : [0x80001964] : sw t6, 624(ra) -- Store: [0x80012140]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001958]:fsgnj.d t5, t3, s10
	-[0x8000195c]:csrrs gp, fcsr, zero
	-[0x80001960]:sw t5, 616(ra)
Current Store : [0x80001968] : sw t5, 632(ra) -- Store: [0x80012148]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001958]:fsgnj.d t5, t3, s10
	-[0x8000195c]:csrrs gp, fcsr, zero
	-[0x80001960]:sw t5, 616(ra)
Current Store : [0x8000196c] : sw gp, 640(ra) -- Store: [0x80012150]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001998]:fsgnj.d t5, t3, s10
	-[0x8000199c]:csrrs gp, fcsr, zero
	-[0x800019a0]:sw t5, 648(ra)
Current Store : [0x800019a4] : sw t6, 656(ra) -- Store: [0x80012160]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001998]:fsgnj.d t5, t3, s10
	-[0x8000199c]:csrrs gp, fcsr, zero
	-[0x800019a0]:sw t5, 648(ra)
Current Store : [0x800019a8] : sw t5, 664(ra) -- Store: [0x80012168]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001998]:fsgnj.d t5, t3, s10
	-[0x8000199c]:csrrs gp, fcsr, zero
	-[0x800019a0]:sw t5, 648(ra)
Current Store : [0x800019ac] : sw gp, 672(ra) -- Store: [0x80012170]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800019d8]:fsgnj.d t5, t3, s10
	-[0x800019dc]:csrrs gp, fcsr, zero
	-[0x800019e0]:sw t5, 680(ra)
Current Store : [0x800019e4] : sw t6, 688(ra) -- Store: [0x80012180]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800019d8]:fsgnj.d t5, t3, s10
	-[0x800019dc]:csrrs gp, fcsr, zero
	-[0x800019e0]:sw t5, 680(ra)
Current Store : [0x800019e8] : sw t5, 696(ra) -- Store: [0x80012188]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800019d8]:fsgnj.d t5, t3, s10
	-[0x800019dc]:csrrs gp, fcsr, zero
	-[0x800019e0]:sw t5, 680(ra)
Current Store : [0x800019ec] : sw gp, 704(ra) -- Store: [0x80012190]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001a18]:fsgnj.d t5, t3, s10
	-[0x80001a1c]:csrrs gp, fcsr, zero
	-[0x80001a20]:sw t5, 712(ra)
Current Store : [0x80001a24] : sw t6, 720(ra) -- Store: [0x800121a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001a18]:fsgnj.d t5, t3, s10
	-[0x80001a1c]:csrrs gp, fcsr, zero
	-[0x80001a20]:sw t5, 712(ra)
Current Store : [0x80001a28] : sw t5, 728(ra) -- Store: [0x800121a8]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001a18]:fsgnj.d t5, t3, s10
	-[0x80001a1c]:csrrs gp, fcsr, zero
	-[0x80001a20]:sw t5, 712(ra)
Current Store : [0x80001a2c] : sw gp, 736(ra) -- Store: [0x800121b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001a58]:fsgnj.d t5, t3, s10
	-[0x80001a5c]:csrrs gp, fcsr, zero
	-[0x80001a60]:sw t5, 744(ra)
Current Store : [0x80001a64] : sw t6, 752(ra) -- Store: [0x800121c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001a58]:fsgnj.d t5, t3, s10
	-[0x80001a5c]:csrrs gp, fcsr, zero
	-[0x80001a60]:sw t5, 744(ra)
Current Store : [0x80001a68] : sw t5, 760(ra) -- Store: [0x800121c8]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001a58]:fsgnj.d t5, t3, s10
	-[0x80001a5c]:csrrs gp, fcsr, zero
	-[0x80001a60]:sw t5, 744(ra)
Current Store : [0x80001a6c] : sw gp, 768(ra) -- Store: [0x800121d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001a98]:fsgnj.d t5, t3, s10
	-[0x80001a9c]:csrrs gp, fcsr, zero
	-[0x80001aa0]:sw t5, 776(ra)
Current Store : [0x80001aa4] : sw t6, 784(ra) -- Store: [0x800121e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001a98]:fsgnj.d t5, t3, s10
	-[0x80001a9c]:csrrs gp, fcsr, zero
	-[0x80001aa0]:sw t5, 776(ra)
Current Store : [0x80001aa8] : sw t5, 792(ra) -- Store: [0x800121e8]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001a98]:fsgnj.d t5, t3, s10
	-[0x80001a9c]:csrrs gp, fcsr, zero
	-[0x80001aa0]:sw t5, 776(ra)
Current Store : [0x80001aac] : sw gp, 800(ra) -- Store: [0x800121f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001ad8]:fsgnj.d t5, t3, s10
	-[0x80001adc]:csrrs gp, fcsr, zero
	-[0x80001ae0]:sw t5, 808(ra)
Current Store : [0x80001ae4] : sw t6, 816(ra) -- Store: [0x80012200]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001ad8]:fsgnj.d t5, t3, s10
	-[0x80001adc]:csrrs gp, fcsr, zero
	-[0x80001ae0]:sw t5, 808(ra)
Current Store : [0x80001ae8] : sw t5, 824(ra) -- Store: [0x80012208]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001ad8]:fsgnj.d t5, t3, s10
	-[0x80001adc]:csrrs gp, fcsr, zero
	-[0x80001ae0]:sw t5, 808(ra)
Current Store : [0x80001aec] : sw gp, 832(ra) -- Store: [0x80012210]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001b1c]:fsgnj.d t5, t3, s10
	-[0x80001b20]:csrrs gp, fcsr, zero
	-[0x80001b24]:sw t5, 840(ra)
Current Store : [0x80001b28] : sw t6, 848(ra) -- Store: [0x80012220]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001b1c]:fsgnj.d t5, t3, s10
	-[0x80001b20]:csrrs gp, fcsr, zero
	-[0x80001b24]:sw t5, 840(ra)
Current Store : [0x80001b2c] : sw t5, 856(ra) -- Store: [0x80012228]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001b1c]:fsgnj.d t5, t3, s10
	-[0x80001b20]:csrrs gp, fcsr, zero
	-[0x80001b24]:sw t5, 840(ra)
Current Store : [0x80001b30] : sw gp, 864(ra) -- Store: [0x80012230]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001b60]:fsgnj.d t5, t3, s10
	-[0x80001b64]:csrrs gp, fcsr, zero
	-[0x80001b68]:sw t5, 872(ra)
Current Store : [0x80001b6c] : sw t6, 880(ra) -- Store: [0x80012240]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001b60]:fsgnj.d t5, t3, s10
	-[0x80001b64]:csrrs gp, fcsr, zero
	-[0x80001b68]:sw t5, 872(ra)
Current Store : [0x80001b70] : sw t5, 888(ra) -- Store: [0x80012248]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001b60]:fsgnj.d t5, t3, s10
	-[0x80001b64]:csrrs gp, fcsr, zero
	-[0x80001b68]:sw t5, 872(ra)
Current Store : [0x80001b74] : sw gp, 896(ra) -- Store: [0x80012250]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001ba0]:fsgnj.d t5, t3, s10
	-[0x80001ba4]:csrrs gp, fcsr, zero
	-[0x80001ba8]:sw t5, 904(ra)
Current Store : [0x80001bac] : sw t6, 912(ra) -- Store: [0x80012260]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001ba0]:fsgnj.d t5, t3, s10
	-[0x80001ba4]:csrrs gp, fcsr, zero
	-[0x80001ba8]:sw t5, 904(ra)
Current Store : [0x80001bb0] : sw t5, 920(ra) -- Store: [0x80012268]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001ba0]:fsgnj.d t5, t3, s10
	-[0x80001ba4]:csrrs gp, fcsr, zero
	-[0x80001ba8]:sw t5, 904(ra)
Current Store : [0x80001bb4] : sw gp, 928(ra) -- Store: [0x80012270]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001be0]:fsgnj.d t5, t3, s10
	-[0x80001be4]:csrrs gp, fcsr, zero
	-[0x80001be8]:sw t5, 936(ra)
Current Store : [0x80001bec] : sw t6, 944(ra) -- Store: [0x80012280]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001be0]:fsgnj.d t5, t3, s10
	-[0x80001be4]:csrrs gp, fcsr, zero
	-[0x80001be8]:sw t5, 936(ra)
Current Store : [0x80001bf0] : sw t5, 952(ra) -- Store: [0x80012288]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001be0]:fsgnj.d t5, t3, s10
	-[0x80001be4]:csrrs gp, fcsr, zero
	-[0x80001be8]:sw t5, 936(ra)
Current Store : [0x80001bf4] : sw gp, 960(ra) -- Store: [0x80012290]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001c20]:fsgnj.d t5, t3, s10
	-[0x80001c24]:csrrs gp, fcsr, zero
	-[0x80001c28]:sw t5, 968(ra)
Current Store : [0x80001c2c] : sw t6, 976(ra) -- Store: [0x800122a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001c20]:fsgnj.d t5, t3, s10
	-[0x80001c24]:csrrs gp, fcsr, zero
	-[0x80001c28]:sw t5, 968(ra)
Current Store : [0x80001c30] : sw t5, 984(ra) -- Store: [0x800122a8]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001c20]:fsgnj.d t5, t3, s10
	-[0x80001c24]:csrrs gp, fcsr, zero
	-[0x80001c28]:sw t5, 968(ra)
Current Store : [0x80001c34] : sw gp, 992(ra) -- Store: [0x800122b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001c60]:fsgnj.d t5, t3, s10
	-[0x80001c64]:csrrs gp, fcsr, zero
	-[0x80001c68]:sw t5, 1000(ra)
Current Store : [0x80001c6c] : sw t6, 1008(ra) -- Store: [0x800122c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001c60]:fsgnj.d t5, t3, s10
	-[0x80001c64]:csrrs gp, fcsr, zero
	-[0x80001c68]:sw t5, 1000(ra)
Current Store : [0x80001c70] : sw t5, 1016(ra) -- Store: [0x800122c8]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001c60]:fsgnj.d t5, t3, s10
	-[0x80001c64]:csrrs gp, fcsr, zero
	-[0x80001c68]:sw t5, 1000(ra)
Current Store : [0x80001c74] : sw gp, 1024(ra) -- Store: [0x800122d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001ca4]:fsgnj.d t5, t3, s10
	-[0x80001ca8]:csrrs gp, fcsr, zero
	-[0x80001cac]:sw t5, 1032(ra)
Current Store : [0x80001cb0] : sw t6, 1040(ra) -- Store: [0x800122e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001ca4]:fsgnj.d t5, t3, s10
	-[0x80001ca8]:csrrs gp, fcsr, zero
	-[0x80001cac]:sw t5, 1032(ra)
Current Store : [0x80001cb4] : sw t5, 1048(ra) -- Store: [0x800122e8]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001ca4]:fsgnj.d t5, t3, s10
	-[0x80001ca8]:csrrs gp, fcsr, zero
	-[0x80001cac]:sw t5, 1032(ra)
Current Store : [0x80001cb8] : sw gp, 1056(ra) -- Store: [0x800122f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001ce8]:fsgnj.d t5, t3, s10
	-[0x80001cec]:csrrs gp, fcsr, zero
	-[0x80001cf0]:sw t5, 1064(ra)
Current Store : [0x80001cf4] : sw t6, 1072(ra) -- Store: [0x80012300]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001ce8]:fsgnj.d t5, t3, s10
	-[0x80001cec]:csrrs gp, fcsr, zero
	-[0x80001cf0]:sw t5, 1064(ra)
Current Store : [0x80001cf8] : sw t5, 1080(ra) -- Store: [0x80012308]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001ce8]:fsgnj.d t5, t3, s10
	-[0x80001cec]:csrrs gp, fcsr, zero
	-[0x80001cf0]:sw t5, 1064(ra)
Current Store : [0x80001cfc] : sw gp, 1088(ra) -- Store: [0x80012310]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001d28]:fsgnj.d t5, t3, s10
	-[0x80001d2c]:csrrs gp, fcsr, zero
	-[0x80001d30]:sw t5, 1096(ra)
Current Store : [0x80001d34] : sw t6, 1104(ra) -- Store: [0x80012320]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001d28]:fsgnj.d t5, t3, s10
	-[0x80001d2c]:csrrs gp, fcsr, zero
	-[0x80001d30]:sw t5, 1096(ra)
Current Store : [0x80001d38] : sw t5, 1112(ra) -- Store: [0x80012328]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001d28]:fsgnj.d t5, t3, s10
	-[0x80001d2c]:csrrs gp, fcsr, zero
	-[0x80001d30]:sw t5, 1096(ra)
Current Store : [0x80001d3c] : sw gp, 1120(ra) -- Store: [0x80012330]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001d68]:fsgnj.d t5, t3, s10
	-[0x80001d6c]:csrrs gp, fcsr, zero
	-[0x80001d70]:sw t5, 1128(ra)
Current Store : [0x80001d74] : sw t6, 1136(ra) -- Store: [0x80012340]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001d68]:fsgnj.d t5, t3, s10
	-[0x80001d6c]:csrrs gp, fcsr, zero
	-[0x80001d70]:sw t5, 1128(ra)
Current Store : [0x80001d78] : sw t5, 1144(ra) -- Store: [0x80012348]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001d68]:fsgnj.d t5, t3, s10
	-[0x80001d6c]:csrrs gp, fcsr, zero
	-[0x80001d70]:sw t5, 1128(ra)
Current Store : [0x80001d7c] : sw gp, 1152(ra) -- Store: [0x80012350]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001da8]:fsgnj.d t5, t3, s10
	-[0x80001dac]:csrrs gp, fcsr, zero
	-[0x80001db0]:sw t5, 1160(ra)
Current Store : [0x80001db4] : sw t6, 1168(ra) -- Store: [0x80012360]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001da8]:fsgnj.d t5, t3, s10
	-[0x80001dac]:csrrs gp, fcsr, zero
	-[0x80001db0]:sw t5, 1160(ra)
Current Store : [0x80001db8] : sw t5, 1176(ra) -- Store: [0x80012368]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001da8]:fsgnj.d t5, t3, s10
	-[0x80001dac]:csrrs gp, fcsr, zero
	-[0x80001db0]:sw t5, 1160(ra)
Current Store : [0x80001dbc] : sw gp, 1184(ra) -- Store: [0x80012370]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001de8]:fsgnj.d t5, t3, s10
	-[0x80001dec]:csrrs gp, fcsr, zero
	-[0x80001df0]:sw t5, 1192(ra)
Current Store : [0x80001df4] : sw t6, 1200(ra) -- Store: [0x80012380]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001de8]:fsgnj.d t5, t3, s10
	-[0x80001dec]:csrrs gp, fcsr, zero
	-[0x80001df0]:sw t5, 1192(ra)
Current Store : [0x80001df8] : sw t5, 1208(ra) -- Store: [0x80012388]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001de8]:fsgnj.d t5, t3, s10
	-[0x80001dec]:csrrs gp, fcsr, zero
	-[0x80001df0]:sw t5, 1192(ra)
Current Store : [0x80001dfc] : sw gp, 1216(ra) -- Store: [0x80012390]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001e28]:fsgnj.d t5, t3, s10
	-[0x80001e2c]:csrrs gp, fcsr, zero
	-[0x80001e30]:sw t5, 1224(ra)
Current Store : [0x80001e34] : sw t6, 1232(ra) -- Store: [0x800123a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001e28]:fsgnj.d t5, t3, s10
	-[0x80001e2c]:csrrs gp, fcsr, zero
	-[0x80001e30]:sw t5, 1224(ra)
Current Store : [0x80001e38] : sw t5, 1240(ra) -- Store: [0x800123a8]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001e28]:fsgnj.d t5, t3, s10
	-[0x80001e2c]:csrrs gp, fcsr, zero
	-[0x80001e30]:sw t5, 1224(ra)
Current Store : [0x80001e3c] : sw gp, 1248(ra) -- Store: [0x800123b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001e68]:fsgnj.d t5, t3, s10
	-[0x80001e6c]:csrrs gp, fcsr, zero
	-[0x80001e70]:sw t5, 1256(ra)
Current Store : [0x80001e74] : sw t6, 1264(ra) -- Store: [0x800123c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001e68]:fsgnj.d t5, t3, s10
	-[0x80001e6c]:csrrs gp, fcsr, zero
	-[0x80001e70]:sw t5, 1256(ra)
Current Store : [0x80001e78] : sw t5, 1272(ra) -- Store: [0x800123c8]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001e68]:fsgnj.d t5, t3, s10
	-[0x80001e6c]:csrrs gp, fcsr, zero
	-[0x80001e70]:sw t5, 1256(ra)
Current Store : [0x80001e7c] : sw gp, 1280(ra) -- Store: [0x800123d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001ea8]:fsgnj.d t5, t3, s10
	-[0x80001eac]:csrrs gp, fcsr, zero
	-[0x80001eb0]:sw t5, 1288(ra)
Current Store : [0x80001eb4] : sw t6, 1296(ra) -- Store: [0x800123e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001ea8]:fsgnj.d t5, t3, s10
	-[0x80001eac]:csrrs gp, fcsr, zero
	-[0x80001eb0]:sw t5, 1288(ra)
Current Store : [0x80001eb8] : sw t5, 1304(ra) -- Store: [0x800123e8]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001ea8]:fsgnj.d t5, t3, s10
	-[0x80001eac]:csrrs gp, fcsr, zero
	-[0x80001eb0]:sw t5, 1288(ra)
Current Store : [0x80001ebc] : sw gp, 1312(ra) -- Store: [0x800123f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001ee8]:fsgnj.d t5, t3, s10
	-[0x80001eec]:csrrs gp, fcsr, zero
	-[0x80001ef0]:sw t5, 1320(ra)
Current Store : [0x80001ef4] : sw t6, 1328(ra) -- Store: [0x80012400]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001ee8]:fsgnj.d t5, t3, s10
	-[0x80001eec]:csrrs gp, fcsr, zero
	-[0x80001ef0]:sw t5, 1320(ra)
Current Store : [0x80001ef8] : sw t5, 1336(ra) -- Store: [0x80012408]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001ee8]:fsgnj.d t5, t3, s10
	-[0x80001eec]:csrrs gp, fcsr, zero
	-[0x80001ef0]:sw t5, 1320(ra)
Current Store : [0x80001efc] : sw gp, 1344(ra) -- Store: [0x80012410]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001f28]:fsgnj.d t5, t3, s10
	-[0x80001f2c]:csrrs gp, fcsr, zero
	-[0x80001f30]:sw t5, 1352(ra)
Current Store : [0x80001f34] : sw t6, 1360(ra) -- Store: [0x80012420]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001f28]:fsgnj.d t5, t3, s10
	-[0x80001f2c]:csrrs gp, fcsr, zero
	-[0x80001f30]:sw t5, 1352(ra)
Current Store : [0x80001f38] : sw t5, 1368(ra) -- Store: [0x80012428]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001f28]:fsgnj.d t5, t3, s10
	-[0x80001f2c]:csrrs gp, fcsr, zero
	-[0x80001f30]:sw t5, 1352(ra)
Current Store : [0x80001f3c] : sw gp, 1376(ra) -- Store: [0x80012430]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001f68]:fsgnj.d t5, t3, s10
	-[0x80001f6c]:csrrs gp, fcsr, zero
	-[0x80001f70]:sw t5, 1384(ra)
Current Store : [0x80001f74] : sw t6, 1392(ra) -- Store: [0x80012440]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001f68]:fsgnj.d t5, t3, s10
	-[0x80001f6c]:csrrs gp, fcsr, zero
	-[0x80001f70]:sw t5, 1384(ra)
Current Store : [0x80001f78] : sw t5, 1400(ra) -- Store: [0x80012448]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001f68]:fsgnj.d t5, t3, s10
	-[0x80001f6c]:csrrs gp, fcsr, zero
	-[0x80001f70]:sw t5, 1384(ra)
Current Store : [0x80001f7c] : sw gp, 1408(ra) -- Store: [0x80012450]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001fa8]:fsgnj.d t5, t3, s10
	-[0x80001fac]:csrrs gp, fcsr, zero
	-[0x80001fb0]:sw t5, 1416(ra)
Current Store : [0x80001fb4] : sw t6, 1424(ra) -- Store: [0x80012460]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001fa8]:fsgnj.d t5, t3, s10
	-[0x80001fac]:csrrs gp, fcsr, zero
	-[0x80001fb0]:sw t5, 1416(ra)
Current Store : [0x80001fb8] : sw t5, 1432(ra) -- Store: [0x80012468]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001fa8]:fsgnj.d t5, t3, s10
	-[0x80001fac]:csrrs gp, fcsr, zero
	-[0x80001fb0]:sw t5, 1416(ra)
Current Store : [0x80001fbc] : sw gp, 1440(ra) -- Store: [0x80012470]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001fe8]:fsgnj.d t5, t3, s10
	-[0x80001fec]:csrrs gp, fcsr, zero
	-[0x80001ff0]:sw t5, 1448(ra)
Current Store : [0x80001ff4] : sw t6, 1456(ra) -- Store: [0x80012480]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001fe8]:fsgnj.d t5, t3, s10
	-[0x80001fec]:csrrs gp, fcsr, zero
	-[0x80001ff0]:sw t5, 1448(ra)
Current Store : [0x80001ff8] : sw t5, 1464(ra) -- Store: [0x80012488]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80001fe8]:fsgnj.d t5, t3, s10
	-[0x80001fec]:csrrs gp, fcsr, zero
	-[0x80001ff0]:sw t5, 1448(ra)
Current Store : [0x80001ffc] : sw gp, 1472(ra) -- Store: [0x80012490]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002028]:fsgnj.d t5, t3, s10
	-[0x8000202c]:csrrs gp, fcsr, zero
	-[0x80002030]:sw t5, 1480(ra)
Current Store : [0x80002034] : sw t6, 1488(ra) -- Store: [0x800124a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002028]:fsgnj.d t5, t3, s10
	-[0x8000202c]:csrrs gp, fcsr, zero
	-[0x80002030]:sw t5, 1480(ra)
Current Store : [0x80002038] : sw t5, 1496(ra) -- Store: [0x800124a8]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002028]:fsgnj.d t5, t3, s10
	-[0x8000202c]:csrrs gp, fcsr, zero
	-[0x80002030]:sw t5, 1480(ra)
Current Store : [0x8000203c] : sw gp, 1504(ra) -- Store: [0x800124b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002068]:fsgnj.d t5, t3, s10
	-[0x8000206c]:csrrs gp, fcsr, zero
	-[0x80002070]:sw t5, 1512(ra)
Current Store : [0x80002074] : sw t6, 1520(ra) -- Store: [0x800124c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002068]:fsgnj.d t5, t3, s10
	-[0x8000206c]:csrrs gp, fcsr, zero
	-[0x80002070]:sw t5, 1512(ra)
Current Store : [0x80002078] : sw t5, 1528(ra) -- Store: [0x800124c8]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002068]:fsgnj.d t5, t3, s10
	-[0x8000206c]:csrrs gp, fcsr, zero
	-[0x80002070]:sw t5, 1512(ra)
Current Store : [0x8000207c] : sw gp, 1536(ra) -- Store: [0x800124d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800020a8]:fsgnj.d t5, t3, s10
	-[0x800020ac]:csrrs gp, fcsr, zero
	-[0x800020b0]:sw t5, 1544(ra)
Current Store : [0x800020b4] : sw t6, 1552(ra) -- Store: [0x800124e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800020a8]:fsgnj.d t5, t3, s10
	-[0x800020ac]:csrrs gp, fcsr, zero
	-[0x800020b0]:sw t5, 1544(ra)
Current Store : [0x800020b8] : sw t5, 1560(ra) -- Store: [0x800124e8]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800020a8]:fsgnj.d t5, t3, s10
	-[0x800020ac]:csrrs gp, fcsr, zero
	-[0x800020b0]:sw t5, 1544(ra)
Current Store : [0x800020bc] : sw gp, 1568(ra) -- Store: [0x800124f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800020e8]:fsgnj.d t5, t3, s10
	-[0x800020ec]:csrrs gp, fcsr, zero
	-[0x800020f0]:sw t5, 1576(ra)
Current Store : [0x800020f4] : sw t6, 1584(ra) -- Store: [0x80012500]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800020e8]:fsgnj.d t5, t3, s10
	-[0x800020ec]:csrrs gp, fcsr, zero
	-[0x800020f0]:sw t5, 1576(ra)
Current Store : [0x800020f8] : sw t5, 1592(ra) -- Store: [0x80012508]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800020e8]:fsgnj.d t5, t3, s10
	-[0x800020ec]:csrrs gp, fcsr, zero
	-[0x800020f0]:sw t5, 1576(ra)
Current Store : [0x800020fc] : sw gp, 1600(ra) -- Store: [0x80012510]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000212c]:fsgnj.d t5, t3, s10
	-[0x80002130]:csrrs gp, fcsr, zero
	-[0x80002134]:sw t5, 1608(ra)
Current Store : [0x80002138] : sw t6, 1616(ra) -- Store: [0x80012520]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000212c]:fsgnj.d t5, t3, s10
	-[0x80002130]:csrrs gp, fcsr, zero
	-[0x80002134]:sw t5, 1608(ra)
Current Store : [0x8000213c] : sw t5, 1624(ra) -- Store: [0x80012528]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000212c]:fsgnj.d t5, t3, s10
	-[0x80002130]:csrrs gp, fcsr, zero
	-[0x80002134]:sw t5, 1608(ra)
Current Store : [0x80002140] : sw gp, 1632(ra) -- Store: [0x80012530]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002170]:fsgnj.d t5, t3, s10
	-[0x80002174]:csrrs gp, fcsr, zero
	-[0x80002178]:sw t5, 1640(ra)
Current Store : [0x8000217c] : sw t6, 1648(ra) -- Store: [0x80012540]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002170]:fsgnj.d t5, t3, s10
	-[0x80002174]:csrrs gp, fcsr, zero
	-[0x80002178]:sw t5, 1640(ra)
Current Store : [0x80002180] : sw t5, 1656(ra) -- Store: [0x80012548]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002170]:fsgnj.d t5, t3, s10
	-[0x80002174]:csrrs gp, fcsr, zero
	-[0x80002178]:sw t5, 1640(ra)
Current Store : [0x80002184] : sw gp, 1664(ra) -- Store: [0x80012550]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800021b0]:fsgnj.d t5, t3, s10
	-[0x800021b4]:csrrs gp, fcsr, zero
	-[0x800021b8]:sw t5, 1672(ra)
Current Store : [0x800021bc] : sw t6, 1680(ra) -- Store: [0x80012560]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800021b0]:fsgnj.d t5, t3, s10
	-[0x800021b4]:csrrs gp, fcsr, zero
	-[0x800021b8]:sw t5, 1672(ra)
Current Store : [0x800021c0] : sw t5, 1688(ra) -- Store: [0x80012568]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800021b0]:fsgnj.d t5, t3, s10
	-[0x800021b4]:csrrs gp, fcsr, zero
	-[0x800021b8]:sw t5, 1672(ra)
Current Store : [0x800021c4] : sw gp, 1696(ra) -- Store: [0x80012570]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800021f0]:fsgnj.d t5, t3, s10
	-[0x800021f4]:csrrs gp, fcsr, zero
	-[0x800021f8]:sw t5, 1704(ra)
Current Store : [0x800021fc] : sw t6, 1712(ra) -- Store: [0x80012580]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800021f0]:fsgnj.d t5, t3, s10
	-[0x800021f4]:csrrs gp, fcsr, zero
	-[0x800021f8]:sw t5, 1704(ra)
Current Store : [0x80002200] : sw t5, 1720(ra) -- Store: [0x80012588]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800021f0]:fsgnj.d t5, t3, s10
	-[0x800021f4]:csrrs gp, fcsr, zero
	-[0x800021f8]:sw t5, 1704(ra)
Current Store : [0x80002204] : sw gp, 1728(ra) -- Store: [0x80012590]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002230]:fsgnj.d t5, t3, s10
	-[0x80002234]:csrrs gp, fcsr, zero
	-[0x80002238]:sw t5, 1736(ra)
Current Store : [0x8000223c] : sw t6, 1744(ra) -- Store: [0x800125a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002230]:fsgnj.d t5, t3, s10
	-[0x80002234]:csrrs gp, fcsr, zero
	-[0x80002238]:sw t5, 1736(ra)
Current Store : [0x80002240] : sw t5, 1752(ra) -- Store: [0x800125a8]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002230]:fsgnj.d t5, t3, s10
	-[0x80002234]:csrrs gp, fcsr, zero
	-[0x80002238]:sw t5, 1736(ra)
Current Store : [0x80002244] : sw gp, 1760(ra) -- Store: [0x800125b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002270]:fsgnj.d t5, t3, s10
	-[0x80002274]:csrrs gp, fcsr, zero
	-[0x80002278]:sw t5, 1768(ra)
Current Store : [0x8000227c] : sw t6, 1776(ra) -- Store: [0x800125c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002270]:fsgnj.d t5, t3, s10
	-[0x80002274]:csrrs gp, fcsr, zero
	-[0x80002278]:sw t5, 1768(ra)
Current Store : [0x80002280] : sw t5, 1784(ra) -- Store: [0x800125c8]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002270]:fsgnj.d t5, t3, s10
	-[0x80002274]:csrrs gp, fcsr, zero
	-[0x80002278]:sw t5, 1768(ra)
Current Store : [0x80002284] : sw gp, 1792(ra) -- Store: [0x800125d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800022b4]:fsgnj.d t5, t3, s10
	-[0x800022b8]:csrrs gp, fcsr, zero
	-[0x800022bc]:sw t5, 1800(ra)
Current Store : [0x800022c0] : sw t6, 1808(ra) -- Store: [0x800125e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800022b4]:fsgnj.d t5, t3, s10
	-[0x800022b8]:csrrs gp, fcsr, zero
	-[0x800022bc]:sw t5, 1800(ra)
Current Store : [0x800022c4] : sw t5, 1816(ra) -- Store: [0x800125e8]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800022b4]:fsgnj.d t5, t3, s10
	-[0x800022b8]:csrrs gp, fcsr, zero
	-[0x800022bc]:sw t5, 1800(ra)
Current Store : [0x800022c8] : sw gp, 1824(ra) -- Store: [0x800125f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800022f8]:fsgnj.d t5, t3, s10
	-[0x800022fc]:csrrs gp, fcsr, zero
	-[0x80002300]:sw t5, 1832(ra)
Current Store : [0x80002304] : sw t6, 1840(ra) -- Store: [0x80012600]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800022f8]:fsgnj.d t5, t3, s10
	-[0x800022fc]:csrrs gp, fcsr, zero
	-[0x80002300]:sw t5, 1832(ra)
Current Store : [0x80002308] : sw t5, 1848(ra) -- Store: [0x80012608]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800022f8]:fsgnj.d t5, t3, s10
	-[0x800022fc]:csrrs gp, fcsr, zero
	-[0x80002300]:sw t5, 1832(ra)
Current Store : [0x8000230c] : sw gp, 1856(ra) -- Store: [0x80012610]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002338]:fsgnj.d t5, t3, s10
	-[0x8000233c]:csrrs gp, fcsr, zero
	-[0x80002340]:sw t5, 1864(ra)
Current Store : [0x80002344] : sw t6, 1872(ra) -- Store: [0x80012620]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002338]:fsgnj.d t5, t3, s10
	-[0x8000233c]:csrrs gp, fcsr, zero
	-[0x80002340]:sw t5, 1864(ra)
Current Store : [0x80002348] : sw t5, 1880(ra) -- Store: [0x80012628]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002338]:fsgnj.d t5, t3, s10
	-[0x8000233c]:csrrs gp, fcsr, zero
	-[0x80002340]:sw t5, 1864(ra)
Current Store : [0x8000234c] : sw gp, 1888(ra) -- Store: [0x80012630]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002378]:fsgnj.d t5, t3, s10
	-[0x8000237c]:csrrs gp, fcsr, zero
	-[0x80002380]:sw t5, 1896(ra)
Current Store : [0x80002384] : sw t6, 1904(ra) -- Store: [0x80012640]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002378]:fsgnj.d t5, t3, s10
	-[0x8000237c]:csrrs gp, fcsr, zero
	-[0x80002380]:sw t5, 1896(ra)
Current Store : [0x80002388] : sw t5, 1912(ra) -- Store: [0x80012648]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002378]:fsgnj.d t5, t3, s10
	-[0x8000237c]:csrrs gp, fcsr, zero
	-[0x80002380]:sw t5, 1896(ra)
Current Store : [0x8000238c] : sw gp, 1920(ra) -- Store: [0x80012650]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800023b8]:fsgnj.d t5, t3, s10
	-[0x800023bc]:csrrs gp, fcsr, zero
	-[0x800023c0]:sw t5, 1928(ra)
Current Store : [0x800023c4] : sw t6, 1936(ra) -- Store: [0x80012660]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800023b8]:fsgnj.d t5, t3, s10
	-[0x800023bc]:csrrs gp, fcsr, zero
	-[0x800023c0]:sw t5, 1928(ra)
Current Store : [0x800023c8] : sw t5, 1944(ra) -- Store: [0x80012668]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800023b8]:fsgnj.d t5, t3, s10
	-[0x800023bc]:csrrs gp, fcsr, zero
	-[0x800023c0]:sw t5, 1928(ra)
Current Store : [0x800023cc] : sw gp, 1952(ra) -- Store: [0x80012670]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800023f8]:fsgnj.d t5, t3, s10
	-[0x800023fc]:csrrs gp, fcsr, zero
	-[0x80002400]:sw t5, 1960(ra)
Current Store : [0x80002404] : sw t6, 1968(ra) -- Store: [0x80012680]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800023f8]:fsgnj.d t5, t3, s10
	-[0x800023fc]:csrrs gp, fcsr, zero
	-[0x80002400]:sw t5, 1960(ra)
Current Store : [0x80002408] : sw t5, 1976(ra) -- Store: [0x80012688]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800023f8]:fsgnj.d t5, t3, s10
	-[0x800023fc]:csrrs gp, fcsr, zero
	-[0x80002400]:sw t5, 1960(ra)
Current Store : [0x8000240c] : sw gp, 1984(ra) -- Store: [0x80012690]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002438]:fsgnj.d t5, t3, s10
	-[0x8000243c]:csrrs gp, fcsr, zero
	-[0x80002440]:sw t5, 1992(ra)
Current Store : [0x80002444] : sw t6, 2000(ra) -- Store: [0x800126a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002438]:fsgnj.d t5, t3, s10
	-[0x8000243c]:csrrs gp, fcsr, zero
	-[0x80002440]:sw t5, 1992(ra)
Current Store : [0x80002448] : sw t5, 2008(ra) -- Store: [0x800126a8]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002438]:fsgnj.d t5, t3, s10
	-[0x8000243c]:csrrs gp, fcsr, zero
	-[0x80002440]:sw t5, 1992(ra)
Current Store : [0x8000244c] : sw gp, 2016(ra) -- Store: [0x800126b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002478]:fsgnj.d t5, t3, s10
	-[0x8000247c]:csrrs gp, fcsr, zero
	-[0x80002480]:sw t5, 2024(ra)
Current Store : [0x80002484] : sw t6, 2032(ra) -- Store: [0x800126c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002478]:fsgnj.d t5, t3, s10
	-[0x8000247c]:csrrs gp, fcsr, zero
	-[0x80002480]:sw t5, 2024(ra)
Current Store : [0x80002488] : sw t5, 2040(ra) -- Store: [0x800126c8]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002478]:fsgnj.d t5, t3, s10
	-[0x8000247c]:csrrs gp, fcsr, zero
	-[0x80002480]:sw t5, 2024(ra)
Current Store : [0x80002490] : sw gp, 8(ra) -- Store: [0x800126d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800024fc]:fsgnj.d t5, t3, s10
	-[0x80002500]:csrrs gp, fcsr, zero
	-[0x80002504]:sw t5, 16(ra)
Current Store : [0x80002508] : sw t6, 24(ra) -- Store: [0x800126e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800024fc]:fsgnj.d t5, t3, s10
	-[0x80002500]:csrrs gp, fcsr, zero
	-[0x80002504]:sw t5, 16(ra)
Current Store : [0x8000250c] : sw t5, 32(ra) -- Store: [0x800126e8]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800024fc]:fsgnj.d t5, t3, s10
	-[0x80002500]:csrrs gp, fcsr, zero
	-[0x80002504]:sw t5, 16(ra)
Current Store : [0x80002510] : sw gp, 40(ra) -- Store: [0x800126f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000257c]:fsgnj.d t5, t3, s10
	-[0x80002580]:csrrs gp, fcsr, zero
	-[0x80002584]:sw t5, 48(ra)
Current Store : [0x80002588] : sw t6, 56(ra) -- Store: [0x80012700]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000257c]:fsgnj.d t5, t3, s10
	-[0x80002580]:csrrs gp, fcsr, zero
	-[0x80002584]:sw t5, 48(ra)
Current Store : [0x8000258c] : sw t5, 64(ra) -- Store: [0x80012708]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000257c]:fsgnj.d t5, t3, s10
	-[0x80002580]:csrrs gp, fcsr, zero
	-[0x80002584]:sw t5, 48(ra)
Current Store : [0x80002590] : sw gp, 72(ra) -- Store: [0x80012710]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800025fc]:fsgnj.d t5, t3, s10
	-[0x80002600]:csrrs gp, fcsr, zero
	-[0x80002604]:sw t5, 80(ra)
Current Store : [0x80002608] : sw t6, 88(ra) -- Store: [0x80012720]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800025fc]:fsgnj.d t5, t3, s10
	-[0x80002600]:csrrs gp, fcsr, zero
	-[0x80002604]:sw t5, 80(ra)
Current Store : [0x8000260c] : sw t5, 96(ra) -- Store: [0x80012728]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800025fc]:fsgnj.d t5, t3, s10
	-[0x80002600]:csrrs gp, fcsr, zero
	-[0x80002604]:sw t5, 80(ra)
Current Store : [0x80002610] : sw gp, 104(ra) -- Store: [0x80012730]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000267c]:fsgnj.d t5, t3, s10
	-[0x80002680]:csrrs gp, fcsr, zero
	-[0x80002684]:sw t5, 112(ra)
Current Store : [0x80002688] : sw t6, 120(ra) -- Store: [0x80012740]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000267c]:fsgnj.d t5, t3, s10
	-[0x80002680]:csrrs gp, fcsr, zero
	-[0x80002684]:sw t5, 112(ra)
Current Store : [0x8000268c] : sw t5, 128(ra) -- Store: [0x80012748]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000267c]:fsgnj.d t5, t3, s10
	-[0x80002680]:csrrs gp, fcsr, zero
	-[0x80002684]:sw t5, 112(ra)
Current Store : [0x80002690] : sw gp, 136(ra) -- Store: [0x80012750]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002700]:fsgnj.d t5, t3, s10
	-[0x80002704]:csrrs gp, fcsr, zero
	-[0x80002708]:sw t5, 144(ra)
Current Store : [0x8000270c] : sw t6, 152(ra) -- Store: [0x80012760]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002700]:fsgnj.d t5, t3, s10
	-[0x80002704]:csrrs gp, fcsr, zero
	-[0x80002708]:sw t5, 144(ra)
Current Store : [0x80002710] : sw t5, 160(ra) -- Store: [0x80012768]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002700]:fsgnj.d t5, t3, s10
	-[0x80002704]:csrrs gp, fcsr, zero
	-[0x80002708]:sw t5, 144(ra)
Current Store : [0x80002714] : sw gp, 168(ra) -- Store: [0x80012770]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002784]:fsgnj.d t5, t3, s10
	-[0x80002788]:csrrs gp, fcsr, zero
	-[0x8000278c]:sw t5, 176(ra)
Current Store : [0x80002790] : sw t6, 184(ra) -- Store: [0x80012780]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002784]:fsgnj.d t5, t3, s10
	-[0x80002788]:csrrs gp, fcsr, zero
	-[0x8000278c]:sw t5, 176(ra)
Current Store : [0x80002794] : sw t5, 192(ra) -- Store: [0x80012788]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002784]:fsgnj.d t5, t3, s10
	-[0x80002788]:csrrs gp, fcsr, zero
	-[0x8000278c]:sw t5, 176(ra)
Current Store : [0x80002798] : sw gp, 200(ra) -- Store: [0x80012790]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002808]:fsgnj.d t5, t3, s10
	-[0x8000280c]:csrrs gp, fcsr, zero
	-[0x80002810]:sw t5, 208(ra)
Current Store : [0x80002814] : sw t6, 216(ra) -- Store: [0x800127a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002808]:fsgnj.d t5, t3, s10
	-[0x8000280c]:csrrs gp, fcsr, zero
	-[0x80002810]:sw t5, 208(ra)
Current Store : [0x80002818] : sw t5, 224(ra) -- Store: [0x800127a8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002808]:fsgnj.d t5, t3, s10
	-[0x8000280c]:csrrs gp, fcsr, zero
	-[0x80002810]:sw t5, 208(ra)
Current Store : [0x8000281c] : sw gp, 232(ra) -- Store: [0x800127b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000288c]:fsgnj.d t5, t3, s10
	-[0x80002890]:csrrs gp, fcsr, zero
	-[0x80002894]:sw t5, 240(ra)
Current Store : [0x80002898] : sw t6, 248(ra) -- Store: [0x800127c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000288c]:fsgnj.d t5, t3, s10
	-[0x80002890]:csrrs gp, fcsr, zero
	-[0x80002894]:sw t5, 240(ra)
Current Store : [0x8000289c] : sw t5, 256(ra) -- Store: [0x800127c8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000288c]:fsgnj.d t5, t3, s10
	-[0x80002890]:csrrs gp, fcsr, zero
	-[0x80002894]:sw t5, 240(ra)
Current Store : [0x800028a0] : sw gp, 264(ra) -- Store: [0x800127d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002910]:fsgnj.d t5, t3, s10
	-[0x80002914]:csrrs gp, fcsr, zero
	-[0x80002918]:sw t5, 272(ra)
Current Store : [0x8000291c] : sw t6, 280(ra) -- Store: [0x800127e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002910]:fsgnj.d t5, t3, s10
	-[0x80002914]:csrrs gp, fcsr, zero
	-[0x80002918]:sw t5, 272(ra)
Current Store : [0x80002920] : sw t5, 288(ra) -- Store: [0x800127e8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002910]:fsgnj.d t5, t3, s10
	-[0x80002914]:csrrs gp, fcsr, zero
	-[0x80002918]:sw t5, 272(ra)
Current Store : [0x80002924] : sw gp, 296(ra) -- Store: [0x800127f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002994]:fsgnj.d t5, t3, s10
	-[0x80002998]:csrrs gp, fcsr, zero
	-[0x8000299c]:sw t5, 304(ra)
Current Store : [0x800029a0] : sw t6, 312(ra) -- Store: [0x80012800]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002994]:fsgnj.d t5, t3, s10
	-[0x80002998]:csrrs gp, fcsr, zero
	-[0x8000299c]:sw t5, 304(ra)
Current Store : [0x800029a4] : sw t5, 320(ra) -- Store: [0x80012808]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002994]:fsgnj.d t5, t3, s10
	-[0x80002998]:csrrs gp, fcsr, zero
	-[0x8000299c]:sw t5, 304(ra)
Current Store : [0x800029a8] : sw gp, 328(ra) -- Store: [0x80012810]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002a1c]:fsgnj.d t5, t3, s10
	-[0x80002a20]:csrrs gp, fcsr, zero
	-[0x80002a24]:sw t5, 336(ra)
Current Store : [0x80002a28] : sw t6, 344(ra) -- Store: [0x80012820]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002a1c]:fsgnj.d t5, t3, s10
	-[0x80002a20]:csrrs gp, fcsr, zero
	-[0x80002a24]:sw t5, 336(ra)
Current Store : [0x80002a2c] : sw t5, 352(ra) -- Store: [0x80012828]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002a1c]:fsgnj.d t5, t3, s10
	-[0x80002a20]:csrrs gp, fcsr, zero
	-[0x80002a24]:sw t5, 336(ra)
Current Store : [0x80002a30] : sw gp, 360(ra) -- Store: [0x80012830]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002aa4]:fsgnj.d t5, t3, s10
	-[0x80002aa8]:csrrs gp, fcsr, zero
	-[0x80002aac]:sw t5, 368(ra)
Current Store : [0x80002ab0] : sw t6, 376(ra) -- Store: [0x80012840]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002aa4]:fsgnj.d t5, t3, s10
	-[0x80002aa8]:csrrs gp, fcsr, zero
	-[0x80002aac]:sw t5, 368(ra)
Current Store : [0x80002ab4] : sw t5, 384(ra) -- Store: [0x80012848]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002aa4]:fsgnj.d t5, t3, s10
	-[0x80002aa8]:csrrs gp, fcsr, zero
	-[0x80002aac]:sw t5, 368(ra)
Current Store : [0x80002ab8] : sw gp, 392(ra) -- Store: [0x80012850]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002b28]:fsgnj.d t5, t3, s10
	-[0x80002b2c]:csrrs gp, fcsr, zero
	-[0x80002b30]:sw t5, 400(ra)
Current Store : [0x80002b34] : sw t6, 408(ra) -- Store: [0x80012860]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002b28]:fsgnj.d t5, t3, s10
	-[0x80002b2c]:csrrs gp, fcsr, zero
	-[0x80002b30]:sw t5, 400(ra)
Current Store : [0x80002b38] : sw t5, 416(ra) -- Store: [0x80012868]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002b28]:fsgnj.d t5, t3, s10
	-[0x80002b2c]:csrrs gp, fcsr, zero
	-[0x80002b30]:sw t5, 400(ra)
Current Store : [0x80002b3c] : sw gp, 424(ra) -- Store: [0x80012870]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002bac]:fsgnj.d t5, t3, s10
	-[0x80002bb0]:csrrs gp, fcsr, zero
	-[0x80002bb4]:sw t5, 432(ra)
Current Store : [0x80002bb8] : sw t6, 440(ra) -- Store: [0x80012880]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002bac]:fsgnj.d t5, t3, s10
	-[0x80002bb0]:csrrs gp, fcsr, zero
	-[0x80002bb4]:sw t5, 432(ra)
Current Store : [0x80002bbc] : sw t5, 448(ra) -- Store: [0x80012888]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002bac]:fsgnj.d t5, t3, s10
	-[0x80002bb0]:csrrs gp, fcsr, zero
	-[0x80002bb4]:sw t5, 432(ra)
Current Store : [0x80002bc0] : sw gp, 456(ra) -- Store: [0x80012890]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002c30]:fsgnj.d t5, t3, s10
	-[0x80002c34]:csrrs gp, fcsr, zero
	-[0x80002c38]:sw t5, 464(ra)
Current Store : [0x80002c3c] : sw t6, 472(ra) -- Store: [0x800128a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002c30]:fsgnj.d t5, t3, s10
	-[0x80002c34]:csrrs gp, fcsr, zero
	-[0x80002c38]:sw t5, 464(ra)
Current Store : [0x80002c40] : sw t5, 480(ra) -- Store: [0x800128a8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002c30]:fsgnj.d t5, t3, s10
	-[0x80002c34]:csrrs gp, fcsr, zero
	-[0x80002c38]:sw t5, 464(ra)
Current Store : [0x80002c44] : sw gp, 488(ra) -- Store: [0x800128b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002cb4]:fsgnj.d t5, t3, s10
	-[0x80002cb8]:csrrs gp, fcsr, zero
	-[0x80002cbc]:sw t5, 496(ra)
Current Store : [0x80002cc0] : sw t6, 504(ra) -- Store: [0x800128c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002cb4]:fsgnj.d t5, t3, s10
	-[0x80002cb8]:csrrs gp, fcsr, zero
	-[0x80002cbc]:sw t5, 496(ra)
Current Store : [0x80002cc4] : sw t5, 512(ra) -- Store: [0x800128c8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002cb4]:fsgnj.d t5, t3, s10
	-[0x80002cb8]:csrrs gp, fcsr, zero
	-[0x80002cbc]:sw t5, 496(ra)
Current Store : [0x80002cc8] : sw gp, 520(ra) -- Store: [0x800128d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002d3c]:fsgnj.d t5, t3, s10
	-[0x80002d40]:csrrs gp, fcsr, zero
	-[0x80002d44]:sw t5, 528(ra)
Current Store : [0x80002d48] : sw t6, 536(ra) -- Store: [0x800128e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002d3c]:fsgnj.d t5, t3, s10
	-[0x80002d40]:csrrs gp, fcsr, zero
	-[0x80002d44]:sw t5, 528(ra)
Current Store : [0x80002d4c] : sw t5, 544(ra) -- Store: [0x800128e8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002d3c]:fsgnj.d t5, t3, s10
	-[0x80002d40]:csrrs gp, fcsr, zero
	-[0x80002d44]:sw t5, 528(ra)
Current Store : [0x80002d50] : sw gp, 552(ra) -- Store: [0x800128f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002dc4]:fsgnj.d t5, t3, s10
	-[0x80002dc8]:csrrs gp, fcsr, zero
	-[0x80002dcc]:sw t5, 560(ra)
Current Store : [0x80002dd0] : sw t6, 568(ra) -- Store: [0x80012900]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002dc4]:fsgnj.d t5, t3, s10
	-[0x80002dc8]:csrrs gp, fcsr, zero
	-[0x80002dcc]:sw t5, 560(ra)
Current Store : [0x80002dd4] : sw t5, 576(ra) -- Store: [0x80012908]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002dc4]:fsgnj.d t5, t3, s10
	-[0x80002dc8]:csrrs gp, fcsr, zero
	-[0x80002dcc]:sw t5, 560(ra)
Current Store : [0x80002dd8] : sw gp, 584(ra) -- Store: [0x80012910]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002e48]:fsgnj.d t5, t3, s10
	-[0x80002e4c]:csrrs gp, fcsr, zero
	-[0x80002e50]:sw t5, 592(ra)
Current Store : [0x80002e54] : sw t6, 600(ra) -- Store: [0x80012920]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002e48]:fsgnj.d t5, t3, s10
	-[0x80002e4c]:csrrs gp, fcsr, zero
	-[0x80002e50]:sw t5, 592(ra)
Current Store : [0x80002e58] : sw t5, 608(ra) -- Store: [0x80012928]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002e48]:fsgnj.d t5, t3, s10
	-[0x80002e4c]:csrrs gp, fcsr, zero
	-[0x80002e50]:sw t5, 592(ra)
Current Store : [0x80002e5c] : sw gp, 616(ra) -- Store: [0x80012930]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002ecc]:fsgnj.d t5, t3, s10
	-[0x80002ed0]:csrrs gp, fcsr, zero
	-[0x80002ed4]:sw t5, 624(ra)
Current Store : [0x80002ed8] : sw t6, 632(ra) -- Store: [0x80012940]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002ecc]:fsgnj.d t5, t3, s10
	-[0x80002ed0]:csrrs gp, fcsr, zero
	-[0x80002ed4]:sw t5, 624(ra)
Current Store : [0x80002edc] : sw t5, 640(ra) -- Store: [0x80012948]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002ecc]:fsgnj.d t5, t3, s10
	-[0x80002ed0]:csrrs gp, fcsr, zero
	-[0x80002ed4]:sw t5, 624(ra)
Current Store : [0x80002ee0] : sw gp, 648(ra) -- Store: [0x80012950]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002f50]:fsgnj.d t5, t3, s10
	-[0x80002f54]:csrrs gp, fcsr, zero
	-[0x80002f58]:sw t5, 656(ra)
Current Store : [0x80002f5c] : sw t6, 664(ra) -- Store: [0x80012960]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002f50]:fsgnj.d t5, t3, s10
	-[0x80002f54]:csrrs gp, fcsr, zero
	-[0x80002f58]:sw t5, 656(ra)
Current Store : [0x80002f60] : sw t5, 672(ra) -- Store: [0x80012968]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002f50]:fsgnj.d t5, t3, s10
	-[0x80002f54]:csrrs gp, fcsr, zero
	-[0x80002f58]:sw t5, 656(ra)
Current Store : [0x80002f64] : sw gp, 680(ra) -- Store: [0x80012970]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002fd4]:fsgnj.d t5, t3, s10
	-[0x80002fd8]:csrrs gp, fcsr, zero
	-[0x80002fdc]:sw t5, 688(ra)
Current Store : [0x80002fe0] : sw t6, 696(ra) -- Store: [0x80012980]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002fd4]:fsgnj.d t5, t3, s10
	-[0x80002fd8]:csrrs gp, fcsr, zero
	-[0x80002fdc]:sw t5, 688(ra)
Current Store : [0x80002fe4] : sw t5, 704(ra) -- Store: [0x80012988]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80002fd4]:fsgnj.d t5, t3, s10
	-[0x80002fd8]:csrrs gp, fcsr, zero
	-[0x80002fdc]:sw t5, 688(ra)
Current Store : [0x80002fe8] : sw gp, 712(ra) -- Store: [0x80012990]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003058]:fsgnj.d t5, t3, s10
	-[0x8000305c]:csrrs gp, fcsr, zero
	-[0x80003060]:sw t5, 720(ra)
Current Store : [0x80003064] : sw t6, 728(ra) -- Store: [0x800129a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003058]:fsgnj.d t5, t3, s10
	-[0x8000305c]:csrrs gp, fcsr, zero
	-[0x80003060]:sw t5, 720(ra)
Current Store : [0x80003068] : sw t5, 736(ra) -- Store: [0x800129a8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003058]:fsgnj.d t5, t3, s10
	-[0x8000305c]:csrrs gp, fcsr, zero
	-[0x80003060]:sw t5, 720(ra)
Current Store : [0x8000306c] : sw gp, 744(ra) -- Store: [0x800129b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800030dc]:fsgnj.d t5, t3, s10
	-[0x800030e0]:csrrs gp, fcsr, zero
	-[0x800030e4]:sw t5, 752(ra)
Current Store : [0x800030e8] : sw t6, 760(ra) -- Store: [0x800129c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800030dc]:fsgnj.d t5, t3, s10
	-[0x800030e0]:csrrs gp, fcsr, zero
	-[0x800030e4]:sw t5, 752(ra)
Current Store : [0x800030ec] : sw t5, 768(ra) -- Store: [0x800129c8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800030dc]:fsgnj.d t5, t3, s10
	-[0x800030e0]:csrrs gp, fcsr, zero
	-[0x800030e4]:sw t5, 752(ra)
Current Store : [0x800030f0] : sw gp, 776(ra) -- Store: [0x800129d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003160]:fsgnj.d t5, t3, s10
	-[0x80003164]:csrrs gp, fcsr, zero
	-[0x80003168]:sw t5, 784(ra)
Current Store : [0x8000316c] : sw t6, 792(ra) -- Store: [0x800129e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003160]:fsgnj.d t5, t3, s10
	-[0x80003164]:csrrs gp, fcsr, zero
	-[0x80003168]:sw t5, 784(ra)
Current Store : [0x80003170] : sw t5, 800(ra) -- Store: [0x800129e8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003160]:fsgnj.d t5, t3, s10
	-[0x80003164]:csrrs gp, fcsr, zero
	-[0x80003168]:sw t5, 784(ra)
Current Store : [0x80003174] : sw gp, 808(ra) -- Store: [0x800129f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800031e4]:fsgnj.d t5, t3, s10
	-[0x800031e8]:csrrs gp, fcsr, zero
	-[0x800031ec]:sw t5, 816(ra)
Current Store : [0x800031f0] : sw t6, 824(ra) -- Store: [0x80012a00]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800031e4]:fsgnj.d t5, t3, s10
	-[0x800031e8]:csrrs gp, fcsr, zero
	-[0x800031ec]:sw t5, 816(ra)
Current Store : [0x800031f4] : sw t5, 832(ra) -- Store: [0x80012a08]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800031e4]:fsgnj.d t5, t3, s10
	-[0x800031e8]:csrrs gp, fcsr, zero
	-[0x800031ec]:sw t5, 816(ra)
Current Store : [0x800031f8] : sw gp, 840(ra) -- Store: [0x80012a10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003268]:fsgnj.d t5, t3, s10
	-[0x8000326c]:csrrs gp, fcsr, zero
	-[0x80003270]:sw t5, 848(ra)
Current Store : [0x80003274] : sw t6, 856(ra) -- Store: [0x80012a20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003268]:fsgnj.d t5, t3, s10
	-[0x8000326c]:csrrs gp, fcsr, zero
	-[0x80003270]:sw t5, 848(ra)
Current Store : [0x80003278] : sw t5, 864(ra) -- Store: [0x80012a28]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003268]:fsgnj.d t5, t3, s10
	-[0x8000326c]:csrrs gp, fcsr, zero
	-[0x80003270]:sw t5, 848(ra)
Current Store : [0x8000327c] : sw gp, 872(ra) -- Store: [0x80012a30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800032ec]:fsgnj.d t5, t3, s10
	-[0x800032f0]:csrrs gp, fcsr, zero
	-[0x800032f4]:sw t5, 880(ra)
Current Store : [0x800032f8] : sw t6, 888(ra) -- Store: [0x80012a40]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800032ec]:fsgnj.d t5, t3, s10
	-[0x800032f0]:csrrs gp, fcsr, zero
	-[0x800032f4]:sw t5, 880(ra)
Current Store : [0x800032fc] : sw t5, 896(ra) -- Store: [0x80012a48]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800032ec]:fsgnj.d t5, t3, s10
	-[0x800032f0]:csrrs gp, fcsr, zero
	-[0x800032f4]:sw t5, 880(ra)
Current Store : [0x80003300] : sw gp, 904(ra) -- Store: [0x80012a50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003370]:fsgnj.d t5, t3, s10
	-[0x80003374]:csrrs gp, fcsr, zero
	-[0x80003378]:sw t5, 912(ra)
Current Store : [0x8000337c] : sw t6, 920(ra) -- Store: [0x80012a60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003370]:fsgnj.d t5, t3, s10
	-[0x80003374]:csrrs gp, fcsr, zero
	-[0x80003378]:sw t5, 912(ra)
Current Store : [0x80003380] : sw t5, 928(ra) -- Store: [0x80012a68]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003370]:fsgnj.d t5, t3, s10
	-[0x80003374]:csrrs gp, fcsr, zero
	-[0x80003378]:sw t5, 912(ra)
Current Store : [0x80003384] : sw gp, 936(ra) -- Store: [0x80012a70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800033f4]:fsgnj.d t5, t3, s10
	-[0x800033f8]:csrrs gp, fcsr, zero
	-[0x800033fc]:sw t5, 944(ra)
Current Store : [0x80003400] : sw t6, 952(ra) -- Store: [0x80012a80]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800033f4]:fsgnj.d t5, t3, s10
	-[0x800033f8]:csrrs gp, fcsr, zero
	-[0x800033fc]:sw t5, 944(ra)
Current Store : [0x80003404] : sw t5, 960(ra) -- Store: [0x80012a88]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800033f4]:fsgnj.d t5, t3, s10
	-[0x800033f8]:csrrs gp, fcsr, zero
	-[0x800033fc]:sw t5, 944(ra)
Current Store : [0x80003408] : sw gp, 968(ra) -- Store: [0x80012a90]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003478]:fsgnj.d t5, t3, s10
	-[0x8000347c]:csrrs gp, fcsr, zero
	-[0x80003480]:sw t5, 976(ra)
Current Store : [0x80003484] : sw t6, 984(ra) -- Store: [0x80012aa0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003478]:fsgnj.d t5, t3, s10
	-[0x8000347c]:csrrs gp, fcsr, zero
	-[0x80003480]:sw t5, 976(ra)
Current Store : [0x80003488] : sw t5, 992(ra) -- Store: [0x80012aa8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003478]:fsgnj.d t5, t3, s10
	-[0x8000347c]:csrrs gp, fcsr, zero
	-[0x80003480]:sw t5, 976(ra)
Current Store : [0x8000348c] : sw gp, 1000(ra) -- Store: [0x80012ab0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800034fc]:fsgnj.d t5, t3, s10
	-[0x80003500]:csrrs gp, fcsr, zero
	-[0x80003504]:sw t5, 1008(ra)
Current Store : [0x80003508] : sw t6, 1016(ra) -- Store: [0x80012ac0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800034fc]:fsgnj.d t5, t3, s10
	-[0x80003500]:csrrs gp, fcsr, zero
	-[0x80003504]:sw t5, 1008(ra)
Current Store : [0x8000350c] : sw t5, 1024(ra) -- Store: [0x80012ac8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800034fc]:fsgnj.d t5, t3, s10
	-[0x80003500]:csrrs gp, fcsr, zero
	-[0x80003504]:sw t5, 1008(ra)
Current Store : [0x80003510] : sw gp, 1032(ra) -- Store: [0x80012ad0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003580]:fsgnj.d t5, t3, s10
	-[0x80003584]:csrrs gp, fcsr, zero
	-[0x80003588]:sw t5, 1040(ra)
Current Store : [0x8000358c] : sw t6, 1048(ra) -- Store: [0x80012ae0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003580]:fsgnj.d t5, t3, s10
	-[0x80003584]:csrrs gp, fcsr, zero
	-[0x80003588]:sw t5, 1040(ra)
Current Store : [0x80003590] : sw t5, 1056(ra) -- Store: [0x80012ae8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003580]:fsgnj.d t5, t3, s10
	-[0x80003584]:csrrs gp, fcsr, zero
	-[0x80003588]:sw t5, 1040(ra)
Current Store : [0x80003594] : sw gp, 1064(ra) -- Store: [0x80012af0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003604]:fsgnj.d t5, t3, s10
	-[0x80003608]:csrrs gp, fcsr, zero
	-[0x8000360c]:sw t5, 1072(ra)
Current Store : [0x80003610] : sw t6, 1080(ra) -- Store: [0x80012b00]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003604]:fsgnj.d t5, t3, s10
	-[0x80003608]:csrrs gp, fcsr, zero
	-[0x8000360c]:sw t5, 1072(ra)
Current Store : [0x80003614] : sw t5, 1088(ra) -- Store: [0x80012b08]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003604]:fsgnj.d t5, t3, s10
	-[0x80003608]:csrrs gp, fcsr, zero
	-[0x8000360c]:sw t5, 1072(ra)
Current Store : [0x80003618] : sw gp, 1096(ra) -- Store: [0x80012b10]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000368c]:fsgnj.d t5, t3, s10
	-[0x80003690]:csrrs gp, fcsr, zero
	-[0x80003694]:sw t5, 1104(ra)
Current Store : [0x80003698] : sw t6, 1112(ra) -- Store: [0x80012b20]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000368c]:fsgnj.d t5, t3, s10
	-[0x80003690]:csrrs gp, fcsr, zero
	-[0x80003694]:sw t5, 1104(ra)
Current Store : [0x8000369c] : sw t5, 1120(ra) -- Store: [0x80012b28]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000368c]:fsgnj.d t5, t3, s10
	-[0x80003690]:csrrs gp, fcsr, zero
	-[0x80003694]:sw t5, 1104(ra)
Current Store : [0x800036a0] : sw gp, 1128(ra) -- Store: [0x80012b30]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003714]:fsgnj.d t5, t3, s10
	-[0x80003718]:csrrs gp, fcsr, zero
	-[0x8000371c]:sw t5, 1136(ra)
Current Store : [0x80003720] : sw t6, 1144(ra) -- Store: [0x80012b40]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003714]:fsgnj.d t5, t3, s10
	-[0x80003718]:csrrs gp, fcsr, zero
	-[0x8000371c]:sw t5, 1136(ra)
Current Store : [0x80003724] : sw t5, 1152(ra) -- Store: [0x80012b48]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003714]:fsgnj.d t5, t3, s10
	-[0x80003718]:csrrs gp, fcsr, zero
	-[0x8000371c]:sw t5, 1136(ra)
Current Store : [0x80003728] : sw gp, 1160(ra) -- Store: [0x80012b50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003798]:fsgnj.d t5, t3, s10
	-[0x8000379c]:csrrs gp, fcsr, zero
	-[0x800037a0]:sw t5, 1168(ra)
Current Store : [0x800037a4] : sw t6, 1176(ra) -- Store: [0x80012b60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003798]:fsgnj.d t5, t3, s10
	-[0x8000379c]:csrrs gp, fcsr, zero
	-[0x800037a0]:sw t5, 1168(ra)
Current Store : [0x800037a8] : sw t5, 1184(ra) -- Store: [0x80012b68]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003798]:fsgnj.d t5, t3, s10
	-[0x8000379c]:csrrs gp, fcsr, zero
	-[0x800037a0]:sw t5, 1168(ra)
Current Store : [0x800037ac] : sw gp, 1192(ra) -- Store: [0x80012b70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000381c]:fsgnj.d t5, t3, s10
	-[0x80003820]:csrrs gp, fcsr, zero
	-[0x80003824]:sw t5, 1200(ra)
Current Store : [0x80003828] : sw t6, 1208(ra) -- Store: [0x80012b80]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000381c]:fsgnj.d t5, t3, s10
	-[0x80003820]:csrrs gp, fcsr, zero
	-[0x80003824]:sw t5, 1200(ra)
Current Store : [0x8000382c] : sw t5, 1216(ra) -- Store: [0x80012b88]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000381c]:fsgnj.d t5, t3, s10
	-[0x80003820]:csrrs gp, fcsr, zero
	-[0x80003824]:sw t5, 1200(ra)
Current Store : [0x80003830] : sw gp, 1224(ra) -- Store: [0x80012b90]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800038a0]:fsgnj.d t5, t3, s10
	-[0x800038a4]:csrrs gp, fcsr, zero
	-[0x800038a8]:sw t5, 1232(ra)
Current Store : [0x800038ac] : sw t6, 1240(ra) -- Store: [0x80012ba0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800038a0]:fsgnj.d t5, t3, s10
	-[0x800038a4]:csrrs gp, fcsr, zero
	-[0x800038a8]:sw t5, 1232(ra)
Current Store : [0x800038b0] : sw t5, 1248(ra) -- Store: [0x80012ba8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800038a0]:fsgnj.d t5, t3, s10
	-[0x800038a4]:csrrs gp, fcsr, zero
	-[0x800038a8]:sw t5, 1232(ra)
Current Store : [0x800038b4] : sw gp, 1256(ra) -- Store: [0x80012bb0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003924]:fsgnj.d t5, t3, s10
	-[0x80003928]:csrrs gp, fcsr, zero
	-[0x8000392c]:sw t5, 1264(ra)
Current Store : [0x80003930] : sw t6, 1272(ra) -- Store: [0x80012bc0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003924]:fsgnj.d t5, t3, s10
	-[0x80003928]:csrrs gp, fcsr, zero
	-[0x8000392c]:sw t5, 1264(ra)
Current Store : [0x80003934] : sw t5, 1280(ra) -- Store: [0x80012bc8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003924]:fsgnj.d t5, t3, s10
	-[0x80003928]:csrrs gp, fcsr, zero
	-[0x8000392c]:sw t5, 1264(ra)
Current Store : [0x80003938] : sw gp, 1288(ra) -- Store: [0x80012bd0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800039ac]:fsgnj.d t5, t3, s10
	-[0x800039b0]:csrrs gp, fcsr, zero
	-[0x800039b4]:sw t5, 1296(ra)
Current Store : [0x800039b8] : sw t6, 1304(ra) -- Store: [0x80012be0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800039ac]:fsgnj.d t5, t3, s10
	-[0x800039b0]:csrrs gp, fcsr, zero
	-[0x800039b4]:sw t5, 1296(ra)
Current Store : [0x800039bc] : sw t5, 1312(ra) -- Store: [0x80012be8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800039ac]:fsgnj.d t5, t3, s10
	-[0x800039b0]:csrrs gp, fcsr, zero
	-[0x800039b4]:sw t5, 1296(ra)
Current Store : [0x800039c0] : sw gp, 1320(ra) -- Store: [0x80012bf0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003a34]:fsgnj.d t5, t3, s10
	-[0x80003a38]:csrrs gp, fcsr, zero
	-[0x80003a3c]:sw t5, 1328(ra)
Current Store : [0x80003a40] : sw t6, 1336(ra) -- Store: [0x80012c00]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003a34]:fsgnj.d t5, t3, s10
	-[0x80003a38]:csrrs gp, fcsr, zero
	-[0x80003a3c]:sw t5, 1328(ra)
Current Store : [0x80003a44] : sw t5, 1344(ra) -- Store: [0x80012c08]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003a34]:fsgnj.d t5, t3, s10
	-[0x80003a38]:csrrs gp, fcsr, zero
	-[0x80003a3c]:sw t5, 1328(ra)
Current Store : [0x80003a48] : sw gp, 1352(ra) -- Store: [0x80012c10]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003ab8]:fsgnj.d t5, t3, s10
	-[0x80003abc]:csrrs gp, fcsr, zero
	-[0x80003ac0]:sw t5, 1360(ra)
Current Store : [0x80003ac4] : sw t6, 1368(ra) -- Store: [0x80012c20]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003ab8]:fsgnj.d t5, t3, s10
	-[0x80003abc]:csrrs gp, fcsr, zero
	-[0x80003ac0]:sw t5, 1360(ra)
Current Store : [0x80003ac8] : sw t5, 1376(ra) -- Store: [0x80012c28]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003ab8]:fsgnj.d t5, t3, s10
	-[0x80003abc]:csrrs gp, fcsr, zero
	-[0x80003ac0]:sw t5, 1360(ra)
Current Store : [0x80003acc] : sw gp, 1384(ra) -- Store: [0x80012c30]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003b3c]:fsgnj.d t5, t3, s10
	-[0x80003b40]:csrrs gp, fcsr, zero
	-[0x80003b44]:sw t5, 1392(ra)
Current Store : [0x80003b48] : sw t6, 1400(ra) -- Store: [0x80012c40]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003b3c]:fsgnj.d t5, t3, s10
	-[0x80003b40]:csrrs gp, fcsr, zero
	-[0x80003b44]:sw t5, 1392(ra)
Current Store : [0x80003b4c] : sw t5, 1408(ra) -- Store: [0x80012c48]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003b3c]:fsgnj.d t5, t3, s10
	-[0x80003b40]:csrrs gp, fcsr, zero
	-[0x80003b44]:sw t5, 1392(ra)
Current Store : [0x80003b50] : sw gp, 1416(ra) -- Store: [0x80012c50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003bc0]:fsgnj.d t5, t3, s10
	-[0x80003bc4]:csrrs gp, fcsr, zero
	-[0x80003bc8]:sw t5, 1424(ra)
Current Store : [0x80003bcc] : sw t6, 1432(ra) -- Store: [0x80012c60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003bc0]:fsgnj.d t5, t3, s10
	-[0x80003bc4]:csrrs gp, fcsr, zero
	-[0x80003bc8]:sw t5, 1424(ra)
Current Store : [0x80003bd0] : sw t5, 1440(ra) -- Store: [0x80012c68]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003bc0]:fsgnj.d t5, t3, s10
	-[0x80003bc4]:csrrs gp, fcsr, zero
	-[0x80003bc8]:sw t5, 1424(ra)
Current Store : [0x80003bd4] : sw gp, 1448(ra) -- Store: [0x80012c70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003c44]:fsgnj.d t5, t3, s10
	-[0x80003c48]:csrrs gp, fcsr, zero
	-[0x80003c4c]:sw t5, 1456(ra)
Current Store : [0x80003c50] : sw t6, 1464(ra) -- Store: [0x80012c80]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003c44]:fsgnj.d t5, t3, s10
	-[0x80003c48]:csrrs gp, fcsr, zero
	-[0x80003c4c]:sw t5, 1456(ra)
Current Store : [0x80003c54] : sw t5, 1472(ra) -- Store: [0x80012c88]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003c44]:fsgnj.d t5, t3, s10
	-[0x80003c48]:csrrs gp, fcsr, zero
	-[0x80003c4c]:sw t5, 1456(ra)
Current Store : [0x80003c58] : sw gp, 1480(ra) -- Store: [0x80012c90]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003cc8]:fsgnj.d t5, t3, s10
	-[0x80003ccc]:csrrs gp, fcsr, zero
	-[0x80003cd0]:sw t5, 1488(ra)
Current Store : [0x80003cd4] : sw t6, 1496(ra) -- Store: [0x80012ca0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003cc8]:fsgnj.d t5, t3, s10
	-[0x80003ccc]:csrrs gp, fcsr, zero
	-[0x80003cd0]:sw t5, 1488(ra)
Current Store : [0x80003cd8] : sw t5, 1504(ra) -- Store: [0x80012ca8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003cc8]:fsgnj.d t5, t3, s10
	-[0x80003ccc]:csrrs gp, fcsr, zero
	-[0x80003cd0]:sw t5, 1488(ra)
Current Store : [0x80003cdc] : sw gp, 1512(ra) -- Store: [0x80012cb0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003d4c]:fsgnj.d t5, t3, s10
	-[0x80003d50]:csrrs gp, fcsr, zero
	-[0x80003d54]:sw t5, 1520(ra)
Current Store : [0x80003d58] : sw t6, 1528(ra) -- Store: [0x80012cc0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003d4c]:fsgnj.d t5, t3, s10
	-[0x80003d50]:csrrs gp, fcsr, zero
	-[0x80003d54]:sw t5, 1520(ra)
Current Store : [0x80003d5c] : sw t5, 1536(ra) -- Store: [0x80012cc8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003d4c]:fsgnj.d t5, t3, s10
	-[0x80003d50]:csrrs gp, fcsr, zero
	-[0x80003d54]:sw t5, 1520(ra)
Current Store : [0x80003d60] : sw gp, 1544(ra) -- Store: [0x80012cd0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003dd0]:fsgnj.d t5, t3, s10
	-[0x80003dd4]:csrrs gp, fcsr, zero
	-[0x80003dd8]:sw t5, 1552(ra)
Current Store : [0x80003ddc] : sw t6, 1560(ra) -- Store: [0x80012ce0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003dd0]:fsgnj.d t5, t3, s10
	-[0x80003dd4]:csrrs gp, fcsr, zero
	-[0x80003dd8]:sw t5, 1552(ra)
Current Store : [0x80003de0] : sw t5, 1568(ra) -- Store: [0x80012ce8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003dd0]:fsgnj.d t5, t3, s10
	-[0x80003dd4]:csrrs gp, fcsr, zero
	-[0x80003dd8]:sw t5, 1552(ra)
Current Store : [0x80003de4] : sw gp, 1576(ra) -- Store: [0x80012cf0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003e54]:fsgnj.d t5, t3, s10
	-[0x80003e58]:csrrs gp, fcsr, zero
	-[0x80003e5c]:sw t5, 1584(ra)
Current Store : [0x80003e60] : sw t6, 1592(ra) -- Store: [0x80012d00]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003e54]:fsgnj.d t5, t3, s10
	-[0x80003e58]:csrrs gp, fcsr, zero
	-[0x80003e5c]:sw t5, 1584(ra)
Current Store : [0x80003e64] : sw t5, 1600(ra) -- Store: [0x80012d08]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003e54]:fsgnj.d t5, t3, s10
	-[0x80003e58]:csrrs gp, fcsr, zero
	-[0x80003e5c]:sw t5, 1584(ra)
Current Store : [0x80003e68] : sw gp, 1608(ra) -- Store: [0x80012d10]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003ed8]:fsgnj.d t5, t3, s10
	-[0x80003edc]:csrrs gp, fcsr, zero
	-[0x80003ee0]:sw t5, 1616(ra)
Current Store : [0x80003ee4] : sw t6, 1624(ra) -- Store: [0x80012d20]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003ed8]:fsgnj.d t5, t3, s10
	-[0x80003edc]:csrrs gp, fcsr, zero
	-[0x80003ee0]:sw t5, 1616(ra)
Current Store : [0x80003ee8] : sw t5, 1632(ra) -- Store: [0x80012d28]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003ed8]:fsgnj.d t5, t3, s10
	-[0x80003edc]:csrrs gp, fcsr, zero
	-[0x80003ee0]:sw t5, 1616(ra)
Current Store : [0x80003eec] : sw gp, 1640(ra) -- Store: [0x80012d30]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003f5c]:fsgnj.d t5, t3, s10
	-[0x80003f60]:csrrs gp, fcsr, zero
	-[0x80003f64]:sw t5, 1648(ra)
Current Store : [0x80003f68] : sw t6, 1656(ra) -- Store: [0x80012d40]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003f5c]:fsgnj.d t5, t3, s10
	-[0x80003f60]:csrrs gp, fcsr, zero
	-[0x80003f64]:sw t5, 1648(ra)
Current Store : [0x80003f6c] : sw t5, 1664(ra) -- Store: [0x80012d48]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003f5c]:fsgnj.d t5, t3, s10
	-[0x80003f60]:csrrs gp, fcsr, zero
	-[0x80003f64]:sw t5, 1648(ra)
Current Store : [0x80003f70] : sw gp, 1672(ra) -- Store: [0x80012d50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003fdc]:fsgnj.d t5, t3, s10
	-[0x80003fe0]:csrrs gp, fcsr, zero
	-[0x80003fe4]:sw t5, 1680(ra)
Current Store : [0x80003fe8] : sw t6, 1688(ra) -- Store: [0x80012d60]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003fdc]:fsgnj.d t5, t3, s10
	-[0x80003fe0]:csrrs gp, fcsr, zero
	-[0x80003fe4]:sw t5, 1680(ra)
Current Store : [0x80003fec] : sw t5, 1696(ra) -- Store: [0x80012d68]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80003fdc]:fsgnj.d t5, t3, s10
	-[0x80003fe0]:csrrs gp, fcsr, zero
	-[0x80003fe4]:sw t5, 1680(ra)
Current Store : [0x80003ff0] : sw gp, 1704(ra) -- Store: [0x80012d70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000405c]:fsgnj.d t5, t3, s10
	-[0x80004060]:csrrs gp, fcsr, zero
	-[0x80004064]:sw t5, 1712(ra)
Current Store : [0x80004068] : sw t6, 1720(ra) -- Store: [0x80012d80]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000405c]:fsgnj.d t5, t3, s10
	-[0x80004060]:csrrs gp, fcsr, zero
	-[0x80004064]:sw t5, 1712(ra)
Current Store : [0x8000406c] : sw t5, 1728(ra) -- Store: [0x80012d88]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000405c]:fsgnj.d t5, t3, s10
	-[0x80004060]:csrrs gp, fcsr, zero
	-[0x80004064]:sw t5, 1712(ra)
Current Store : [0x80004070] : sw gp, 1736(ra) -- Store: [0x80012d90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800040dc]:fsgnj.d t5, t3, s10
	-[0x800040e0]:csrrs gp, fcsr, zero
	-[0x800040e4]:sw t5, 1744(ra)
Current Store : [0x800040e8] : sw t6, 1752(ra) -- Store: [0x80012da0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800040dc]:fsgnj.d t5, t3, s10
	-[0x800040e0]:csrrs gp, fcsr, zero
	-[0x800040e4]:sw t5, 1744(ra)
Current Store : [0x800040ec] : sw t5, 1760(ra) -- Store: [0x80012da8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800040dc]:fsgnj.d t5, t3, s10
	-[0x800040e0]:csrrs gp, fcsr, zero
	-[0x800040e4]:sw t5, 1744(ra)
Current Store : [0x800040f0] : sw gp, 1768(ra) -- Store: [0x80012db0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000415c]:fsgnj.d t5, t3, s10
	-[0x80004160]:csrrs gp, fcsr, zero
	-[0x80004164]:sw t5, 1776(ra)
Current Store : [0x80004168] : sw t6, 1784(ra) -- Store: [0x80012dc0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000415c]:fsgnj.d t5, t3, s10
	-[0x80004160]:csrrs gp, fcsr, zero
	-[0x80004164]:sw t5, 1776(ra)
Current Store : [0x8000416c] : sw t5, 1792(ra) -- Store: [0x80012dc8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000415c]:fsgnj.d t5, t3, s10
	-[0x80004160]:csrrs gp, fcsr, zero
	-[0x80004164]:sw t5, 1776(ra)
Current Store : [0x80004170] : sw gp, 1800(ra) -- Store: [0x80012dd0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800041dc]:fsgnj.d t5, t3, s10
	-[0x800041e0]:csrrs gp, fcsr, zero
	-[0x800041e4]:sw t5, 1808(ra)
Current Store : [0x800041e8] : sw t6, 1816(ra) -- Store: [0x80012de0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800041dc]:fsgnj.d t5, t3, s10
	-[0x800041e0]:csrrs gp, fcsr, zero
	-[0x800041e4]:sw t5, 1808(ra)
Current Store : [0x800041ec] : sw t5, 1824(ra) -- Store: [0x80012de8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800041dc]:fsgnj.d t5, t3, s10
	-[0x800041e0]:csrrs gp, fcsr, zero
	-[0x800041e4]:sw t5, 1808(ra)
Current Store : [0x800041f0] : sw gp, 1832(ra) -- Store: [0x80012df0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000425c]:fsgnj.d t5, t3, s10
	-[0x80004260]:csrrs gp, fcsr, zero
	-[0x80004264]:sw t5, 1840(ra)
Current Store : [0x80004268] : sw t6, 1848(ra) -- Store: [0x80012e00]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000425c]:fsgnj.d t5, t3, s10
	-[0x80004260]:csrrs gp, fcsr, zero
	-[0x80004264]:sw t5, 1840(ra)
Current Store : [0x8000426c] : sw t5, 1856(ra) -- Store: [0x80012e08]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000425c]:fsgnj.d t5, t3, s10
	-[0x80004260]:csrrs gp, fcsr, zero
	-[0x80004264]:sw t5, 1840(ra)
Current Store : [0x80004270] : sw gp, 1864(ra) -- Store: [0x80012e10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800042e0]:fsgnj.d t5, t3, s10
	-[0x800042e4]:csrrs gp, fcsr, zero
	-[0x800042e8]:sw t5, 1872(ra)
Current Store : [0x800042ec] : sw t6, 1880(ra) -- Store: [0x80012e20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800042e0]:fsgnj.d t5, t3, s10
	-[0x800042e4]:csrrs gp, fcsr, zero
	-[0x800042e8]:sw t5, 1872(ra)
Current Store : [0x800042f0] : sw t5, 1888(ra) -- Store: [0x80012e28]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800042e0]:fsgnj.d t5, t3, s10
	-[0x800042e4]:csrrs gp, fcsr, zero
	-[0x800042e8]:sw t5, 1872(ra)
Current Store : [0x800042f4] : sw gp, 1896(ra) -- Store: [0x80012e30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004364]:fsgnj.d t5, t3, s10
	-[0x80004368]:csrrs gp, fcsr, zero
	-[0x8000436c]:sw t5, 1904(ra)
Current Store : [0x80004370] : sw t6, 1912(ra) -- Store: [0x80012e40]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004364]:fsgnj.d t5, t3, s10
	-[0x80004368]:csrrs gp, fcsr, zero
	-[0x8000436c]:sw t5, 1904(ra)
Current Store : [0x80004374] : sw t5, 1920(ra) -- Store: [0x80012e48]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004364]:fsgnj.d t5, t3, s10
	-[0x80004368]:csrrs gp, fcsr, zero
	-[0x8000436c]:sw t5, 1904(ra)
Current Store : [0x80004378] : sw gp, 1928(ra) -- Store: [0x80012e50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800043e4]:fsgnj.d t5, t3, s10
	-[0x800043e8]:csrrs gp, fcsr, zero
	-[0x800043ec]:sw t5, 1936(ra)
Current Store : [0x800043f0] : sw t6, 1944(ra) -- Store: [0x80012e60]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800043e4]:fsgnj.d t5, t3, s10
	-[0x800043e8]:csrrs gp, fcsr, zero
	-[0x800043ec]:sw t5, 1936(ra)
Current Store : [0x800043f4] : sw t5, 1952(ra) -- Store: [0x80012e68]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800043e4]:fsgnj.d t5, t3, s10
	-[0x800043e8]:csrrs gp, fcsr, zero
	-[0x800043ec]:sw t5, 1936(ra)
Current Store : [0x800043f8] : sw gp, 1960(ra) -- Store: [0x80012e70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004464]:fsgnj.d t5, t3, s10
	-[0x80004468]:csrrs gp, fcsr, zero
	-[0x8000446c]:sw t5, 1968(ra)
Current Store : [0x80004470] : sw t6, 1976(ra) -- Store: [0x80012e80]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004464]:fsgnj.d t5, t3, s10
	-[0x80004468]:csrrs gp, fcsr, zero
	-[0x8000446c]:sw t5, 1968(ra)
Current Store : [0x80004474] : sw t5, 1984(ra) -- Store: [0x80012e88]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004464]:fsgnj.d t5, t3, s10
	-[0x80004468]:csrrs gp, fcsr, zero
	-[0x8000446c]:sw t5, 1968(ra)
Current Store : [0x80004478] : sw gp, 1992(ra) -- Store: [0x80012e90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800044e4]:fsgnj.d t5, t3, s10
	-[0x800044e8]:csrrs gp, fcsr, zero
	-[0x800044ec]:sw t5, 2000(ra)
Current Store : [0x800044f0] : sw t6, 2008(ra) -- Store: [0x80012ea0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800044e4]:fsgnj.d t5, t3, s10
	-[0x800044e8]:csrrs gp, fcsr, zero
	-[0x800044ec]:sw t5, 2000(ra)
Current Store : [0x800044f4] : sw t5, 2016(ra) -- Store: [0x80012ea8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800044e4]:fsgnj.d t5, t3, s10
	-[0x800044e8]:csrrs gp, fcsr, zero
	-[0x800044ec]:sw t5, 2000(ra)
Current Store : [0x800044f8] : sw gp, 2024(ra) -- Store: [0x80012eb0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004564]:fsgnj.d t5, t3, s10
	-[0x80004568]:csrrs gp, fcsr, zero
	-[0x8000456c]:sw t5, 2032(ra)
Current Store : [0x80004570] : sw t6, 2040(ra) -- Store: [0x80012ec0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004564]:fsgnj.d t5, t3, s10
	-[0x80004568]:csrrs gp, fcsr, zero
	-[0x8000456c]:sw t5, 2032(ra)
Current Store : [0x80004578] : sw t5, 8(ra) -- Store: [0x80012ec8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004564]:fsgnj.d t5, t3, s10
	-[0x80004568]:csrrs gp, fcsr, zero
	-[0x8000456c]:sw t5, 2032(ra)
Current Store : [0x8000457c] : sw gp, 16(ra) -- Store: [0x80012ed0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800045ec]:fsgnj.d t5, t3, s10
	-[0x800045f0]:csrrs gp, fcsr, zero
	-[0x800045f4]:sw t5, 24(ra)
Current Store : [0x800045f8] : sw t6, 32(ra) -- Store: [0x80012ee0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800045ec]:fsgnj.d t5, t3, s10
	-[0x800045f0]:csrrs gp, fcsr, zero
	-[0x800045f4]:sw t5, 24(ra)
Current Store : [0x800045fc] : sw t5, 40(ra) -- Store: [0x80012ee8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800045ec]:fsgnj.d t5, t3, s10
	-[0x800045f0]:csrrs gp, fcsr, zero
	-[0x800045f4]:sw t5, 24(ra)
Current Store : [0x80004600] : sw gp, 48(ra) -- Store: [0x80012ef0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004670]:fsgnj.d t5, t3, s10
	-[0x80004674]:csrrs gp, fcsr, zero
	-[0x80004678]:sw t5, 56(ra)
Current Store : [0x8000467c] : sw t6, 64(ra) -- Store: [0x80012f00]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004670]:fsgnj.d t5, t3, s10
	-[0x80004674]:csrrs gp, fcsr, zero
	-[0x80004678]:sw t5, 56(ra)
Current Store : [0x80004680] : sw t5, 72(ra) -- Store: [0x80012f08]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004670]:fsgnj.d t5, t3, s10
	-[0x80004674]:csrrs gp, fcsr, zero
	-[0x80004678]:sw t5, 56(ra)
Current Store : [0x80004684] : sw gp, 80(ra) -- Store: [0x80012f10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800046f0]:fsgnj.d t5, t3, s10
	-[0x800046f4]:csrrs gp, fcsr, zero
	-[0x800046f8]:sw t5, 88(ra)
Current Store : [0x800046fc] : sw t6, 96(ra) -- Store: [0x80012f20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800046f0]:fsgnj.d t5, t3, s10
	-[0x800046f4]:csrrs gp, fcsr, zero
	-[0x800046f8]:sw t5, 88(ra)
Current Store : [0x80004700] : sw t5, 104(ra) -- Store: [0x80012f28]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800046f0]:fsgnj.d t5, t3, s10
	-[0x800046f4]:csrrs gp, fcsr, zero
	-[0x800046f8]:sw t5, 88(ra)
Current Store : [0x80004704] : sw gp, 112(ra) -- Store: [0x80012f30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004770]:fsgnj.d t5, t3, s10
	-[0x80004774]:csrrs gp, fcsr, zero
	-[0x80004778]:sw t5, 120(ra)
Current Store : [0x8000477c] : sw t6, 128(ra) -- Store: [0x80012f40]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004770]:fsgnj.d t5, t3, s10
	-[0x80004774]:csrrs gp, fcsr, zero
	-[0x80004778]:sw t5, 120(ra)
Current Store : [0x80004780] : sw t5, 136(ra) -- Store: [0x80012f48]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004770]:fsgnj.d t5, t3, s10
	-[0x80004774]:csrrs gp, fcsr, zero
	-[0x80004778]:sw t5, 120(ra)
Current Store : [0x80004784] : sw gp, 144(ra) -- Store: [0x80012f50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800047f0]:fsgnj.d t5, t3, s10
	-[0x800047f4]:csrrs gp, fcsr, zero
	-[0x800047f8]:sw t5, 152(ra)
Current Store : [0x800047fc] : sw t6, 160(ra) -- Store: [0x80012f60]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800047f0]:fsgnj.d t5, t3, s10
	-[0x800047f4]:csrrs gp, fcsr, zero
	-[0x800047f8]:sw t5, 152(ra)
Current Store : [0x80004800] : sw t5, 168(ra) -- Store: [0x80012f68]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800047f0]:fsgnj.d t5, t3, s10
	-[0x800047f4]:csrrs gp, fcsr, zero
	-[0x800047f8]:sw t5, 152(ra)
Current Store : [0x80004804] : sw gp, 176(ra) -- Store: [0x80012f70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004870]:fsgnj.d t5, t3, s10
	-[0x80004874]:csrrs gp, fcsr, zero
	-[0x80004878]:sw t5, 184(ra)
Current Store : [0x8000487c] : sw t6, 192(ra) -- Store: [0x80012f80]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004870]:fsgnj.d t5, t3, s10
	-[0x80004874]:csrrs gp, fcsr, zero
	-[0x80004878]:sw t5, 184(ra)
Current Store : [0x80004880] : sw t5, 200(ra) -- Store: [0x80012f88]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004870]:fsgnj.d t5, t3, s10
	-[0x80004874]:csrrs gp, fcsr, zero
	-[0x80004878]:sw t5, 184(ra)
Current Store : [0x80004884] : sw gp, 208(ra) -- Store: [0x80012f90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800048f0]:fsgnj.d t5, t3, s10
	-[0x800048f4]:csrrs gp, fcsr, zero
	-[0x800048f8]:sw t5, 216(ra)
Current Store : [0x800048fc] : sw t6, 224(ra) -- Store: [0x80012fa0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800048f0]:fsgnj.d t5, t3, s10
	-[0x800048f4]:csrrs gp, fcsr, zero
	-[0x800048f8]:sw t5, 216(ra)
Current Store : [0x80004900] : sw t5, 232(ra) -- Store: [0x80012fa8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800048f0]:fsgnj.d t5, t3, s10
	-[0x800048f4]:csrrs gp, fcsr, zero
	-[0x800048f8]:sw t5, 216(ra)
Current Store : [0x80004904] : sw gp, 240(ra) -- Store: [0x80012fb0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004970]:fsgnj.d t5, t3, s10
	-[0x80004974]:csrrs gp, fcsr, zero
	-[0x80004978]:sw t5, 248(ra)
Current Store : [0x8000497c] : sw t6, 256(ra) -- Store: [0x80012fc0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004970]:fsgnj.d t5, t3, s10
	-[0x80004974]:csrrs gp, fcsr, zero
	-[0x80004978]:sw t5, 248(ra)
Current Store : [0x80004980] : sw t5, 264(ra) -- Store: [0x80012fc8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004970]:fsgnj.d t5, t3, s10
	-[0x80004974]:csrrs gp, fcsr, zero
	-[0x80004978]:sw t5, 248(ra)
Current Store : [0x80004984] : sw gp, 272(ra) -- Store: [0x80012fd0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800049f0]:fsgnj.d t5, t3, s10
	-[0x800049f4]:csrrs gp, fcsr, zero
	-[0x800049f8]:sw t5, 280(ra)
Current Store : [0x800049fc] : sw t6, 288(ra) -- Store: [0x80012fe0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800049f0]:fsgnj.d t5, t3, s10
	-[0x800049f4]:csrrs gp, fcsr, zero
	-[0x800049f8]:sw t5, 280(ra)
Current Store : [0x80004a00] : sw t5, 296(ra) -- Store: [0x80012fe8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800049f0]:fsgnj.d t5, t3, s10
	-[0x800049f4]:csrrs gp, fcsr, zero
	-[0x800049f8]:sw t5, 280(ra)
Current Store : [0x80004a04] : sw gp, 304(ra) -- Store: [0x80012ff0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004a70]:fsgnj.d t5, t3, s10
	-[0x80004a74]:csrrs gp, fcsr, zero
	-[0x80004a78]:sw t5, 312(ra)
Current Store : [0x80004a7c] : sw t6, 320(ra) -- Store: [0x80013000]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004a70]:fsgnj.d t5, t3, s10
	-[0x80004a74]:csrrs gp, fcsr, zero
	-[0x80004a78]:sw t5, 312(ra)
Current Store : [0x80004a80] : sw t5, 328(ra) -- Store: [0x80013008]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004a70]:fsgnj.d t5, t3, s10
	-[0x80004a74]:csrrs gp, fcsr, zero
	-[0x80004a78]:sw t5, 312(ra)
Current Store : [0x80004a84] : sw gp, 336(ra) -- Store: [0x80013010]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004af0]:fsgnj.d t5, t3, s10
	-[0x80004af4]:csrrs gp, fcsr, zero
	-[0x80004af8]:sw t5, 344(ra)
Current Store : [0x80004afc] : sw t6, 352(ra) -- Store: [0x80013020]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004af0]:fsgnj.d t5, t3, s10
	-[0x80004af4]:csrrs gp, fcsr, zero
	-[0x80004af8]:sw t5, 344(ra)
Current Store : [0x80004b00] : sw t5, 360(ra) -- Store: [0x80013028]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004af0]:fsgnj.d t5, t3, s10
	-[0x80004af4]:csrrs gp, fcsr, zero
	-[0x80004af8]:sw t5, 344(ra)
Current Store : [0x80004b04] : sw gp, 368(ra) -- Store: [0x80013030]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004b70]:fsgnj.d t5, t3, s10
	-[0x80004b74]:csrrs gp, fcsr, zero
	-[0x80004b78]:sw t5, 376(ra)
Current Store : [0x80004b7c] : sw t6, 384(ra) -- Store: [0x80013040]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004b70]:fsgnj.d t5, t3, s10
	-[0x80004b74]:csrrs gp, fcsr, zero
	-[0x80004b78]:sw t5, 376(ra)
Current Store : [0x80004b80] : sw t5, 392(ra) -- Store: [0x80013048]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004b70]:fsgnj.d t5, t3, s10
	-[0x80004b74]:csrrs gp, fcsr, zero
	-[0x80004b78]:sw t5, 376(ra)
Current Store : [0x80004b84] : sw gp, 400(ra) -- Store: [0x80013050]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004bf0]:fsgnj.d t5, t3, s10
	-[0x80004bf4]:csrrs gp, fcsr, zero
	-[0x80004bf8]:sw t5, 408(ra)
Current Store : [0x80004bfc] : sw t6, 416(ra) -- Store: [0x80013060]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004bf0]:fsgnj.d t5, t3, s10
	-[0x80004bf4]:csrrs gp, fcsr, zero
	-[0x80004bf8]:sw t5, 408(ra)
Current Store : [0x80004c00] : sw t5, 424(ra) -- Store: [0x80013068]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004bf0]:fsgnj.d t5, t3, s10
	-[0x80004bf4]:csrrs gp, fcsr, zero
	-[0x80004bf8]:sw t5, 408(ra)
Current Store : [0x80004c04] : sw gp, 432(ra) -- Store: [0x80013070]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004c70]:fsgnj.d t5, t3, s10
	-[0x80004c74]:csrrs gp, fcsr, zero
	-[0x80004c78]:sw t5, 440(ra)
Current Store : [0x80004c7c] : sw t6, 448(ra) -- Store: [0x80013080]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004c70]:fsgnj.d t5, t3, s10
	-[0x80004c74]:csrrs gp, fcsr, zero
	-[0x80004c78]:sw t5, 440(ra)
Current Store : [0x80004c80] : sw t5, 456(ra) -- Store: [0x80013088]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004c70]:fsgnj.d t5, t3, s10
	-[0x80004c74]:csrrs gp, fcsr, zero
	-[0x80004c78]:sw t5, 440(ra)
Current Store : [0x80004c84] : sw gp, 464(ra) -- Store: [0x80013090]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004cf0]:fsgnj.d t5, t3, s10
	-[0x80004cf4]:csrrs gp, fcsr, zero
	-[0x80004cf8]:sw t5, 472(ra)
Current Store : [0x80004cfc] : sw t6, 480(ra) -- Store: [0x800130a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004cf0]:fsgnj.d t5, t3, s10
	-[0x80004cf4]:csrrs gp, fcsr, zero
	-[0x80004cf8]:sw t5, 472(ra)
Current Store : [0x80004d00] : sw t5, 488(ra) -- Store: [0x800130a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004cf0]:fsgnj.d t5, t3, s10
	-[0x80004cf4]:csrrs gp, fcsr, zero
	-[0x80004cf8]:sw t5, 472(ra)
Current Store : [0x80004d04] : sw gp, 496(ra) -- Store: [0x800130b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004d70]:fsgnj.d t5, t3, s10
	-[0x80004d74]:csrrs gp, fcsr, zero
	-[0x80004d78]:sw t5, 504(ra)
Current Store : [0x80004d7c] : sw t6, 512(ra) -- Store: [0x800130c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004d70]:fsgnj.d t5, t3, s10
	-[0x80004d74]:csrrs gp, fcsr, zero
	-[0x80004d78]:sw t5, 504(ra)
Current Store : [0x80004d80] : sw t5, 520(ra) -- Store: [0x800130c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004d70]:fsgnj.d t5, t3, s10
	-[0x80004d74]:csrrs gp, fcsr, zero
	-[0x80004d78]:sw t5, 504(ra)
Current Store : [0x80004d84] : sw gp, 528(ra) -- Store: [0x800130d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004df0]:fsgnj.d t5, t3, s10
	-[0x80004df4]:csrrs gp, fcsr, zero
	-[0x80004df8]:sw t5, 536(ra)
Current Store : [0x80004dfc] : sw t6, 544(ra) -- Store: [0x800130e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004df0]:fsgnj.d t5, t3, s10
	-[0x80004df4]:csrrs gp, fcsr, zero
	-[0x80004df8]:sw t5, 536(ra)
Current Store : [0x80004e00] : sw t5, 552(ra) -- Store: [0x800130e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004df0]:fsgnj.d t5, t3, s10
	-[0x80004df4]:csrrs gp, fcsr, zero
	-[0x80004df8]:sw t5, 536(ra)
Current Store : [0x80004e04] : sw gp, 560(ra) -- Store: [0x800130f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004e70]:fsgnj.d t5, t3, s10
	-[0x80004e74]:csrrs gp, fcsr, zero
	-[0x80004e78]:sw t5, 568(ra)
Current Store : [0x80004e7c] : sw t6, 576(ra) -- Store: [0x80013100]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004e70]:fsgnj.d t5, t3, s10
	-[0x80004e74]:csrrs gp, fcsr, zero
	-[0x80004e78]:sw t5, 568(ra)
Current Store : [0x80004e80] : sw t5, 584(ra) -- Store: [0x80013108]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004e70]:fsgnj.d t5, t3, s10
	-[0x80004e74]:csrrs gp, fcsr, zero
	-[0x80004e78]:sw t5, 568(ra)
Current Store : [0x80004e84] : sw gp, 592(ra) -- Store: [0x80013110]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004ef4]:fsgnj.d t5, t3, s10
	-[0x80004ef8]:csrrs gp, fcsr, zero
	-[0x80004efc]:sw t5, 600(ra)
Current Store : [0x80004f00] : sw t6, 608(ra) -- Store: [0x80013120]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004ef4]:fsgnj.d t5, t3, s10
	-[0x80004ef8]:csrrs gp, fcsr, zero
	-[0x80004efc]:sw t5, 600(ra)
Current Store : [0x80004f04] : sw t5, 616(ra) -- Store: [0x80013128]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004ef4]:fsgnj.d t5, t3, s10
	-[0x80004ef8]:csrrs gp, fcsr, zero
	-[0x80004efc]:sw t5, 600(ra)
Current Store : [0x80004f08] : sw gp, 624(ra) -- Store: [0x80013130]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004f78]:fsgnj.d t5, t3, s10
	-[0x80004f7c]:csrrs gp, fcsr, zero
	-[0x80004f80]:sw t5, 632(ra)
Current Store : [0x80004f84] : sw t6, 640(ra) -- Store: [0x80013140]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004f78]:fsgnj.d t5, t3, s10
	-[0x80004f7c]:csrrs gp, fcsr, zero
	-[0x80004f80]:sw t5, 632(ra)
Current Store : [0x80004f88] : sw t5, 648(ra) -- Store: [0x80013148]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004f78]:fsgnj.d t5, t3, s10
	-[0x80004f7c]:csrrs gp, fcsr, zero
	-[0x80004f80]:sw t5, 632(ra)
Current Store : [0x80004f8c] : sw gp, 656(ra) -- Store: [0x80013150]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004ff8]:fsgnj.d t5, t3, s10
	-[0x80004ffc]:csrrs gp, fcsr, zero
	-[0x80005000]:sw t5, 664(ra)
Current Store : [0x80005004] : sw t6, 672(ra) -- Store: [0x80013160]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004ff8]:fsgnj.d t5, t3, s10
	-[0x80004ffc]:csrrs gp, fcsr, zero
	-[0x80005000]:sw t5, 664(ra)
Current Store : [0x80005008] : sw t5, 680(ra) -- Store: [0x80013168]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80004ff8]:fsgnj.d t5, t3, s10
	-[0x80004ffc]:csrrs gp, fcsr, zero
	-[0x80005000]:sw t5, 664(ra)
Current Store : [0x8000500c] : sw gp, 688(ra) -- Store: [0x80013170]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005078]:fsgnj.d t5, t3, s10
	-[0x8000507c]:csrrs gp, fcsr, zero
	-[0x80005080]:sw t5, 696(ra)
Current Store : [0x80005084] : sw t6, 704(ra) -- Store: [0x80013180]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005078]:fsgnj.d t5, t3, s10
	-[0x8000507c]:csrrs gp, fcsr, zero
	-[0x80005080]:sw t5, 696(ra)
Current Store : [0x80005088] : sw t5, 712(ra) -- Store: [0x80013188]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005078]:fsgnj.d t5, t3, s10
	-[0x8000507c]:csrrs gp, fcsr, zero
	-[0x80005080]:sw t5, 696(ra)
Current Store : [0x8000508c] : sw gp, 720(ra) -- Store: [0x80013190]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800050f8]:fsgnj.d t5, t3, s10
	-[0x800050fc]:csrrs gp, fcsr, zero
	-[0x80005100]:sw t5, 728(ra)
Current Store : [0x80005104] : sw t6, 736(ra) -- Store: [0x800131a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800050f8]:fsgnj.d t5, t3, s10
	-[0x800050fc]:csrrs gp, fcsr, zero
	-[0x80005100]:sw t5, 728(ra)
Current Store : [0x80005108] : sw t5, 744(ra) -- Store: [0x800131a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800050f8]:fsgnj.d t5, t3, s10
	-[0x800050fc]:csrrs gp, fcsr, zero
	-[0x80005100]:sw t5, 728(ra)
Current Store : [0x8000510c] : sw gp, 752(ra) -- Store: [0x800131b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005178]:fsgnj.d t5, t3, s10
	-[0x8000517c]:csrrs gp, fcsr, zero
	-[0x80005180]:sw t5, 760(ra)
Current Store : [0x80005184] : sw t6, 768(ra) -- Store: [0x800131c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005178]:fsgnj.d t5, t3, s10
	-[0x8000517c]:csrrs gp, fcsr, zero
	-[0x80005180]:sw t5, 760(ra)
Current Store : [0x80005188] : sw t5, 776(ra) -- Store: [0x800131c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005178]:fsgnj.d t5, t3, s10
	-[0x8000517c]:csrrs gp, fcsr, zero
	-[0x80005180]:sw t5, 760(ra)
Current Store : [0x8000518c] : sw gp, 784(ra) -- Store: [0x800131d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800051fc]:fsgnj.d t5, t3, s10
	-[0x80005200]:csrrs gp, fcsr, zero
	-[0x80005204]:sw t5, 792(ra)
Current Store : [0x80005208] : sw t6, 800(ra) -- Store: [0x800131e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800051fc]:fsgnj.d t5, t3, s10
	-[0x80005200]:csrrs gp, fcsr, zero
	-[0x80005204]:sw t5, 792(ra)
Current Store : [0x8000520c] : sw t5, 808(ra) -- Store: [0x800131e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800051fc]:fsgnj.d t5, t3, s10
	-[0x80005200]:csrrs gp, fcsr, zero
	-[0x80005204]:sw t5, 792(ra)
Current Store : [0x80005210] : sw gp, 816(ra) -- Store: [0x800131f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005280]:fsgnj.d t5, t3, s10
	-[0x80005284]:csrrs gp, fcsr, zero
	-[0x80005288]:sw t5, 824(ra)
Current Store : [0x8000528c] : sw t6, 832(ra) -- Store: [0x80013200]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005280]:fsgnj.d t5, t3, s10
	-[0x80005284]:csrrs gp, fcsr, zero
	-[0x80005288]:sw t5, 824(ra)
Current Store : [0x80005290] : sw t5, 840(ra) -- Store: [0x80013208]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005280]:fsgnj.d t5, t3, s10
	-[0x80005284]:csrrs gp, fcsr, zero
	-[0x80005288]:sw t5, 824(ra)
Current Store : [0x80005294] : sw gp, 848(ra) -- Store: [0x80013210]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005300]:fsgnj.d t5, t3, s10
	-[0x80005304]:csrrs gp, fcsr, zero
	-[0x80005308]:sw t5, 856(ra)
Current Store : [0x8000530c] : sw t6, 864(ra) -- Store: [0x80013220]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005300]:fsgnj.d t5, t3, s10
	-[0x80005304]:csrrs gp, fcsr, zero
	-[0x80005308]:sw t5, 856(ra)
Current Store : [0x80005310] : sw t5, 872(ra) -- Store: [0x80013228]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005300]:fsgnj.d t5, t3, s10
	-[0x80005304]:csrrs gp, fcsr, zero
	-[0x80005308]:sw t5, 856(ra)
Current Store : [0x80005314] : sw gp, 880(ra) -- Store: [0x80013230]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005380]:fsgnj.d t5, t3, s10
	-[0x80005384]:csrrs gp, fcsr, zero
	-[0x80005388]:sw t5, 888(ra)
Current Store : [0x8000538c] : sw t6, 896(ra) -- Store: [0x80013240]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005380]:fsgnj.d t5, t3, s10
	-[0x80005384]:csrrs gp, fcsr, zero
	-[0x80005388]:sw t5, 888(ra)
Current Store : [0x80005390] : sw t5, 904(ra) -- Store: [0x80013248]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005380]:fsgnj.d t5, t3, s10
	-[0x80005384]:csrrs gp, fcsr, zero
	-[0x80005388]:sw t5, 888(ra)
Current Store : [0x80005394] : sw gp, 912(ra) -- Store: [0x80013250]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005400]:fsgnj.d t5, t3, s10
	-[0x80005404]:csrrs gp, fcsr, zero
	-[0x80005408]:sw t5, 920(ra)
Current Store : [0x8000540c] : sw t6, 928(ra) -- Store: [0x80013260]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005400]:fsgnj.d t5, t3, s10
	-[0x80005404]:csrrs gp, fcsr, zero
	-[0x80005408]:sw t5, 920(ra)
Current Store : [0x80005410] : sw t5, 936(ra) -- Store: [0x80013268]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005400]:fsgnj.d t5, t3, s10
	-[0x80005404]:csrrs gp, fcsr, zero
	-[0x80005408]:sw t5, 920(ra)
Current Store : [0x80005414] : sw gp, 944(ra) -- Store: [0x80013270]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005480]:fsgnj.d t5, t3, s10
	-[0x80005484]:csrrs gp, fcsr, zero
	-[0x80005488]:sw t5, 952(ra)
Current Store : [0x8000548c] : sw t6, 960(ra) -- Store: [0x80013280]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005480]:fsgnj.d t5, t3, s10
	-[0x80005484]:csrrs gp, fcsr, zero
	-[0x80005488]:sw t5, 952(ra)
Current Store : [0x80005490] : sw t5, 968(ra) -- Store: [0x80013288]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005480]:fsgnj.d t5, t3, s10
	-[0x80005484]:csrrs gp, fcsr, zero
	-[0x80005488]:sw t5, 952(ra)
Current Store : [0x80005494] : sw gp, 976(ra) -- Store: [0x80013290]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005500]:fsgnj.d t5, t3, s10
	-[0x80005504]:csrrs gp, fcsr, zero
	-[0x80005508]:sw t5, 984(ra)
Current Store : [0x8000550c] : sw t6, 992(ra) -- Store: [0x800132a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005500]:fsgnj.d t5, t3, s10
	-[0x80005504]:csrrs gp, fcsr, zero
	-[0x80005508]:sw t5, 984(ra)
Current Store : [0x80005510] : sw t5, 1000(ra) -- Store: [0x800132a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005500]:fsgnj.d t5, t3, s10
	-[0x80005504]:csrrs gp, fcsr, zero
	-[0x80005508]:sw t5, 984(ra)
Current Store : [0x80005514] : sw gp, 1008(ra) -- Store: [0x800132b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005580]:fsgnj.d t5, t3, s10
	-[0x80005584]:csrrs gp, fcsr, zero
	-[0x80005588]:sw t5, 1016(ra)
Current Store : [0x8000558c] : sw t6, 1024(ra) -- Store: [0x800132c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005580]:fsgnj.d t5, t3, s10
	-[0x80005584]:csrrs gp, fcsr, zero
	-[0x80005588]:sw t5, 1016(ra)
Current Store : [0x80005590] : sw t5, 1032(ra) -- Store: [0x800132c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005580]:fsgnj.d t5, t3, s10
	-[0x80005584]:csrrs gp, fcsr, zero
	-[0x80005588]:sw t5, 1016(ra)
Current Store : [0x80005594] : sw gp, 1040(ra) -- Store: [0x800132d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005600]:fsgnj.d t5, t3, s10
	-[0x80005604]:csrrs gp, fcsr, zero
	-[0x80005608]:sw t5, 1048(ra)
Current Store : [0x8000560c] : sw t6, 1056(ra) -- Store: [0x800132e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005600]:fsgnj.d t5, t3, s10
	-[0x80005604]:csrrs gp, fcsr, zero
	-[0x80005608]:sw t5, 1048(ra)
Current Store : [0x80005610] : sw t5, 1064(ra) -- Store: [0x800132e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005600]:fsgnj.d t5, t3, s10
	-[0x80005604]:csrrs gp, fcsr, zero
	-[0x80005608]:sw t5, 1048(ra)
Current Store : [0x80005614] : sw gp, 1072(ra) -- Store: [0x800132f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005680]:fsgnj.d t5, t3, s10
	-[0x80005684]:csrrs gp, fcsr, zero
	-[0x80005688]:sw t5, 1080(ra)
Current Store : [0x8000568c] : sw t6, 1088(ra) -- Store: [0x80013300]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005680]:fsgnj.d t5, t3, s10
	-[0x80005684]:csrrs gp, fcsr, zero
	-[0x80005688]:sw t5, 1080(ra)
Current Store : [0x80005690] : sw t5, 1096(ra) -- Store: [0x80013308]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005680]:fsgnj.d t5, t3, s10
	-[0x80005684]:csrrs gp, fcsr, zero
	-[0x80005688]:sw t5, 1080(ra)
Current Store : [0x80005694] : sw gp, 1104(ra) -- Store: [0x80013310]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005700]:fsgnj.d t5, t3, s10
	-[0x80005704]:csrrs gp, fcsr, zero
	-[0x80005708]:sw t5, 1112(ra)
Current Store : [0x8000570c] : sw t6, 1120(ra) -- Store: [0x80013320]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005700]:fsgnj.d t5, t3, s10
	-[0x80005704]:csrrs gp, fcsr, zero
	-[0x80005708]:sw t5, 1112(ra)
Current Store : [0x80005710] : sw t5, 1128(ra) -- Store: [0x80013328]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005700]:fsgnj.d t5, t3, s10
	-[0x80005704]:csrrs gp, fcsr, zero
	-[0x80005708]:sw t5, 1112(ra)
Current Store : [0x80005714] : sw gp, 1136(ra) -- Store: [0x80013330]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005780]:fsgnj.d t5, t3, s10
	-[0x80005784]:csrrs gp, fcsr, zero
	-[0x80005788]:sw t5, 1144(ra)
Current Store : [0x8000578c] : sw t6, 1152(ra) -- Store: [0x80013340]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005780]:fsgnj.d t5, t3, s10
	-[0x80005784]:csrrs gp, fcsr, zero
	-[0x80005788]:sw t5, 1144(ra)
Current Store : [0x80005790] : sw t5, 1160(ra) -- Store: [0x80013348]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005780]:fsgnj.d t5, t3, s10
	-[0x80005784]:csrrs gp, fcsr, zero
	-[0x80005788]:sw t5, 1144(ra)
Current Store : [0x80005794] : sw gp, 1168(ra) -- Store: [0x80013350]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005800]:fsgnj.d t5, t3, s10
	-[0x80005804]:csrrs gp, fcsr, zero
	-[0x80005808]:sw t5, 1176(ra)
Current Store : [0x8000580c] : sw t6, 1184(ra) -- Store: [0x80013360]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005800]:fsgnj.d t5, t3, s10
	-[0x80005804]:csrrs gp, fcsr, zero
	-[0x80005808]:sw t5, 1176(ra)
Current Store : [0x80005810] : sw t5, 1192(ra) -- Store: [0x80013368]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005800]:fsgnj.d t5, t3, s10
	-[0x80005804]:csrrs gp, fcsr, zero
	-[0x80005808]:sw t5, 1176(ra)
Current Store : [0x80005814] : sw gp, 1200(ra) -- Store: [0x80013370]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005880]:fsgnj.d t5, t3, s10
	-[0x80005884]:csrrs gp, fcsr, zero
	-[0x80005888]:sw t5, 1208(ra)
Current Store : [0x8000588c] : sw t6, 1216(ra) -- Store: [0x80013380]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005880]:fsgnj.d t5, t3, s10
	-[0x80005884]:csrrs gp, fcsr, zero
	-[0x80005888]:sw t5, 1208(ra)
Current Store : [0x80005890] : sw t5, 1224(ra) -- Store: [0x80013388]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005880]:fsgnj.d t5, t3, s10
	-[0x80005884]:csrrs gp, fcsr, zero
	-[0x80005888]:sw t5, 1208(ra)
Current Store : [0x80005894] : sw gp, 1232(ra) -- Store: [0x80013390]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005900]:fsgnj.d t5, t3, s10
	-[0x80005904]:csrrs gp, fcsr, zero
	-[0x80005908]:sw t5, 1240(ra)
Current Store : [0x8000590c] : sw t6, 1248(ra) -- Store: [0x800133a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005900]:fsgnj.d t5, t3, s10
	-[0x80005904]:csrrs gp, fcsr, zero
	-[0x80005908]:sw t5, 1240(ra)
Current Store : [0x80005910] : sw t5, 1256(ra) -- Store: [0x800133a8]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005900]:fsgnj.d t5, t3, s10
	-[0x80005904]:csrrs gp, fcsr, zero
	-[0x80005908]:sw t5, 1240(ra)
Current Store : [0x80005914] : sw gp, 1264(ra) -- Store: [0x800133b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005980]:fsgnj.d t5, t3, s10
	-[0x80005984]:csrrs gp, fcsr, zero
	-[0x80005988]:sw t5, 1272(ra)
Current Store : [0x8000598c] : sw t6, 1280(ra) -- Store: [0x800133c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005980]:fsgnj.d t5, t3, s10
	-[0x80005984]:csrrs gp, fcsr, zero
	-[0x80005988]:sw t5, 1272(ra)
Current Store : [0x80005990] : sw t5, 1288(ra) -- Store: [0x800133c8]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005980]:fsgnj.d t5, t3, s10
	-[0x80005984]:csrrs gp, fcsr, zero
	-[0x80005988]:sw t5, 1272(ra)
Current Store : [0x80005994] : sw gp, 1296(ra) -- Store: [0x800133d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005a00]:fsgnj.d t5, t3, s10
	-[0x80005a04]:csrrs gp, fcsr, zero
	-[0x80005a08]:sw t5, 1304(ra)
Current Store : [0x80005a0c] : sw t6, 1312(ra) -- Store: [0x800133e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005a00]:fsgnj.d t5, t3, s10
	-[0x80005a04]:csrrs gp, fcsr, zero
	-[0x80005a08]:sw t5, 1304(ra)
Current Store : [0x80005a10] : sw t5, 1320(ra) -- Store: [0x800133e8]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005a00]:fsgnj.d t5, t3, s10
	-[0x80005a04]:csrrs gp, fcsr, zero
	-[0x80005a08]:sw t5, 1304(ra)
Current Store : [0x80005a14] : sw gp, 1328(ra) -- Store: [0x800133f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005a80]:fsgnj.d t5, t3, s10
	-[0x80005a84]:csrrs gp, fcsr, zero
	-[0x80005a88]:sw t5, 1336(ra)
Current Store : [0x80005a8c] : sw t6, 1344(ra) -- Store: [0x80013400]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005a80]:fsgnj.d t5, t3, s10
	-[0x80005a84]:csrrs gp, fcsr, zero
	-[0x80005a88]:sw t5, 1336(ra)
Current Store : [0x80005a90] : sw t5, 1352(ra) -- Store: [0x80013408]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005a80]:fsgnj.d t5, t3, s10
	-[0x80005a84]:csrrs gp, fcsr, zero
	-[0x80005a88]:sw t5, 1336(ra)
Current Store : [0x80005a94] : sw gp, 1360(ra) -- Store: [0x80013410]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005b04]:fsgnj.d t5, t3, s10
	-[0x80005b08]:csrrs gp, fcsr, zero
	-[0x80005b0c]:sw t5, 1368(ra)
Current Store : [0x80005b10] : sw t6, 1376(ra) -- Store: [0x80013420]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005b04]:fsgnj.d t5, t3, s10
	-[0x80005b08]:csrrs gp, fcsr, zero
	-[0x80005b0c]:sw t5, 1368(ra)
Current Store : [0x80005b14] : sw t5, 1384(ra) -- Store: [0x80013428]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005b04]:fsgnj.d t5, t3, s10
	-[0x80005b08]:csrrs gp, fcsr, zero
	-[0x80005b0c]:sw t5, 1368(ra)
Current Store : [0x80005b18] : sw gp, 1392(ra) -- Store: [0x80013430]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005b88]:fsgnj.d t5, t3, s10
	-[0x80005b8c]:csrrs gp, fcsr, zero
	-[0x80005b90]:sw t5, 1400(ra)
Current Store : [0x80005b94] : sw t6, 1408(ra) -- Store: [0x80013440]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005b88]:fsgnj.d t5, t3, s10
	-[0x80005b8c]:csrrs gp, fcsr, zero
	-[0x80005b90]:sw t5, 1400(ra)
Current Store : [0x80005b98] : sw t5, 1416(ra) -- Store: [0x80013448]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005b88]:fsgnj.d t5, t3, s10
	-[0x80005b8c]:csrrs gp, fcsr, zero
	-[0x80005b90]:sw t5, 1400(ra)
Current Store : [0x80005b9c] : sw gp, 1424(ra) -- Store: [0x80013450]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005c08]:fsgnj.d t5, t3, s10
	-[0x80005c0c]:csrrs gp, fcsr, zero
	-[0x80005c10]:sw t5, 1432(ra)
Current Store : [0x80005c14] : sw t6, 1440(ra) -- Store: [0x80013460]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005c08]:fsgnj.d t5, t3, s10
	-[0x80005c0c]:csrrs gp, fcsr, zero
	-[0x80005c10]:sw t5, 1432(ra)
Current Store : [0x80005c18] : sw t5, 1448(ra) -- Store: [0x80013468]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005c08]:fsgnj.d t5, t3, s10
	-[0x80005c0c]:csrrs gp, fcsr, zero
	-[0x80005c10]:sw t5, 1432(ra)
Current Store : [0x80005c1c] : sw gp, 1456(ra) -- Store: [0x80013470]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005c88]:fsgnj.d t5, t3, s10
	-[0x80005c8c]:csrrs gp, fcsr, zero
	-[0x80005c90]:sw t5, 1464(ra)
Current Store : [0x80005c94] : sw t6, 1472(ra) -- Store: [0x80013480]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005c88]:fsgnj.d t5, t3, s10
	-[0x80005c8c]:csrrs gp, fcsr, zero
	-[0x80005c90]:sw t5, 1464(ra)
Current Store : [0x80005c98] : sw t5, 1480(ra) -- Store: [0x80013488]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005c88]:fsgnj.d t5, t3, s10
	-[0x80005c8c]:csrrs gp, fcsr, zero
	-[0x80005c90]:sw t5, 1464(ra)
Current Store : [0x80005c9c] : sw gp, 1488(ra) -- Store: [0x80013490]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005d08]:fsgnj.d t5, t3, s10
	-[0x80005d0c]:csrrs gp, fcsr, zero
	-[0x80005d10]:sw t5, 1496(ra)
Current Store : [0x80005d14] : sw t6, 1504(ra) -- Store: [0x800134a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005d08]:fsgnj.d t5, t3, s10
	-[0x80005d0c]:csrrs gp, fcsr, zero
	-[0x80005d10]:sw t5, 1496(ra)
Current Store : [0x80005d18] : sw t5, 1512(ra) -- Store: [0x800134a8]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005d08]:fsgnj.d t5, t3, s10
	-[0x80005d0c]:csrrs gp, fcsr, zero
	-[0x80005d10]:sw t5, 1496(ra)
Current Store : [0x80005d1c] : sw gp, 1520(ra) -- Store: [0x800134b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005d88]:fsgnj.d t5, t3, s10
	-[0x80005d8c]:csrrs gp, fcsr, zero
	-[0x80005d90]:sw t5, 1528(ra)
Current Store : [0x80005d94] : sw t6, 1536(ra) -- Store: [0x800134c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005d88]:fsgnj.d t5, t3, s10
	-[0x80005d8c]:csrrs gp, fcsr, zero
	-[0x80005d90]:sw t5, 1528(ra)
Current Store : [0x80005d98] : sw t5, 1544(ra) -- Store: [0x800134c8]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005d88]:fsgnj.d t5, t3, s10
	-[0x80005d8c]:csrrs gp, fcsr, zero
	-[0x80005d90]:sw t5, 1528(ra)
Current Store : [0x80005d9c] : sw gp, 1552(ra) -- Store: [0x800134d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005e0c]:fsgnj.d t5, t3, s10
	-[0x80005e10]:csrrs gp, fcsr, zero
	-[0x80005e14]:sw t5, 1560(ra)
Current Store : [0x80005e18] : sw t6, 1568(ra) -- Store: [0x800134e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005e0c]:fsgnj.d t5, t3, s10
	-[0x80005e10]:csrrs gp, fcsr, zero
	-[0x80005e14]:sw t5, 1560(ra)
Current Store : [0x80005e1c] : sw t5, 1576(ra) -- Store: [0x800134e8]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005e0c]:fsgnj.d t5, t3, s10
	-[0x80005e10]:csrrs gp, fcsr, zero
	-[0x80005e14]:sw t5, 1560(ra)
Current Store : [0x80005e20] : sw gp, 1584(ra) -- Store: [0x800134f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005e90]:fsgnj.d t5, t3, s10
	-[0x80005e94]:csrrs gp, fcsr, zero
	-[0x80005e98]:sw t5, 1592(ra)
Current Store : [0x80005e9c] : sw t6, 1600(ra) -- Store: [0x80013500]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005e90]:fsgnj.d t5, t3, s10
	-[0x80005e94]:csrrs gp, fcsr, zero
	-[0x80005e98]:sw t5, 1592(ra)
Current Store : [0x80005ea0] : sw t5, 1608(ra) -- Store: [0x80013508]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005e90]:fsgnj.d t5, t3, s10
	-[0x80005e94]:csrrs gp, fcsr, zero
	-[0x80005e98]:sw t5, 1592(ra)
Current Store : [0x80005ea4] : sw gp, 1616(ra) -- Store: [0x80013510]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005f10]:fsgnj.d t5, t3, s10
	-[0x80005f14]:csrrs gp, fcsr, zero
	-[0x80005f18]:sw t5, 1624(ra)
Current Store : [0x80005f1c] : sw t6, 1632(ra) -- Store: [0x80013520]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005f10]:fsgnj.d t5, t3, s10
	-[0x80005f14]:csrrs gp, fcsr, zero
	-[0x80005f18]:sw t5, 1624(ra)
Current Store : [0x80005f20] : sw t5, 1640(ra) -- Store: [0x80013528]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005f10]:fsgnj.d t5, t3, s10
	-[0x80005f14]:csrrs gp, fcsr, zero
	-[0x80005f18]:sw t5, 1624(ra)
Current Store : [0x80005f24] : sw gp, 1648(ra) -- Store: [0x80013530]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005f90]:fsgnj.d t5, t3, s10
	-[0x80005f94]:csrrs gp, fcsr, zero
	-[0x80005f98]:sw t5, 1656(ra)
Current Store : [0x80005f9c] : sw t6, 1664(ra) -- Store: [0x80013540]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005f90]:fsgnj.d t5, t3, s10
	-[0x80005f94]:csrrs gp, fcsr, zero
	-[0x80005f98]:sw t5, 1656(ra)
Current Store : [0x80005fa0] : sw t5, 1672(ra) -- Store: [0x80013548]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80005f90]:fsgnj.d t5, t3, s10
	-[0x80005f94]:csrrs gp, fcsr, zero
	-[0x80005f98]:sw t5, 1656(ra)
Current Store : [0x80005fa4] : sw gp, 1680(ra) -- Store: [0x80013550]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006010]:fsgnj.d t5, t3, s10
	-[0x80006014]:csrrs gp, fcsr, zero
	-[0x80006018]:sw t5, 1688(ra)
Current Store : [0x8000601c] : sw t6, 1696(ra) -- Store: [0x80013560]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006010]:fsgnj.d t5, t3, s10
	-[0x80006014]:csrrs gp, fcsr, zero
	-[0x80006018]:sw t5, 1688(ra)
Current Store : [0x80006020] : sw t5, 1704(ra) -- Store: [0x80013568]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006010]:fsgnj.d t5, t3, s10
	-[0x80006014]:csrrs gp, fcsr, zero
	-[0x80006018]:sw t5, 1688(ra)
Current Store : [0x80006024] : sw gp, 1712(ra) -- Store: [0x80013570]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006090]:fsgnj.d t5, t3, s10
	-[0x80006094]:csrrs gp, fcsr, zero
	-[0x80006098]:sw t5, 1720(ra)
Current Store : [0x8000609c] : sw t6, 1728(ra) -- Store: [0x80013580]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006090]:fsgnj.d t5, t3, s10
	-[0x80006094]:csrrs gp, fcsr, zero
	-[0x80006098]:sw t5, 1720(ra)
Current Store : [0x800060a0] : sw t5, 1736(ra) -- Store: [0x80013588]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006090]:fsgnj.d t5, t3, s10
	-[0x80006094]:csrrs gp, fcsr, zero
	-[0x80006098]:sw t5, 1720(ra)
Current Store : [0x800060a4] : sw gp, 1744(ra) -- Store: [0x80013590]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006110]:fsgnj.d t5, t3, s10
	-[0x80006114]:csrrs gp, fcsr, zero
	-[0x80006118]:sw t5, 1752(ra)
Current Store : [0x8000611c] : sw t6, 1760(ra) -- Store: [0x800135a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006110]:fsgnj.d t5, t3, s10
	-[0x80006114]:csrrs gp, fcsr, zero
	-[0x80006118]:sw t5, 1752(ra)
Current Store : [0x80006120] : sw t5, 1768(ra) -- Store: [0x800135a8]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006110]:fsgnj.d t5, t3, s10
	-[0x80006114]:csrrs gp, fcsr, zero
	-[0x80006118]:sw t5, 1752(ra)
Current Store : [0x80006124] : sw gp, 1776(ra) -- Store: [0x800135b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006190]:fsgnj.d t5, t3, s10
	-[0x80006194]:csrrs gp, fcsr, zero
	-[0x80006198]:sw t5, 1784(ra)
Current Store : [0x8000619c] : sw t6, 1792(ra) -- Store: [0x800135c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006190]:fsgnj.d t5, t3, s10
	-[0x80006194]:csrrs gp, fcsr, zero
	-[0x80006198]:sw t5, 1784(ra)
Current Store : [0x800061a0] : sw t5, 1800(ra) -- Store: [0x800135c8]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006190]:fsgnj.d t5, t3, s10
	-[0x80006194]:csrrs gp, fcsr, zero
	-[0x80006198]:sw t5, 1784(ra)
Current Store : [0x800061a4] : sw gp, 1808(ra) -- Store: [0x800135d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006210]:fsgnj.d t5, t3, s10
	-[0x80006214]:csrrs gp, fcsr, zero
	-[0x80006218]:sw t5, 1816(ra)
Current Store : [0x8000621c] : sw t6, 1824(ra) -- Store: [0x800135e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006210]:fsgnj.d t5, t3, s10
	-[0x80006214]:csrrs gp, fcsr, zero
	-[0x80006218]:sw t5, 1816(ra)
Current Store : [0x80006220] : sw t5, 1832(ra) -- Store: [0x800135e8]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006210]:fsgnj.d t5, t3, s10
	-[0x80006214]:csrrs gp, fcsr, zero
	-[0x80006218]:sw t5, 1816(ra)
Current Store : [0x80006224] : sw gp, 1840(ra) -- Store: [0x800135f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006290]:fsgnj.d t5, t3, s10
	-[0x80006294]:csrrs gp, fcsr, zero
	-[0x80006298]:sw t5, 1848(ra)
Current Store : [0x8000629c] : sw t6, 1856(ra) -- Store: [0x80013600]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006290]:fsgnj.d t5, t3, s10
	-[0x80006294]:csrrs gp, fcsr, zero
	-[0x80006298]:sw t5, 1848(ra)
Current Store : [0x800062a0] : sw t5, 1864(ra) -- Store: [0x80013608]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006290]:fsgnj.d t5, t3, s10
	-[0x80006294]:csrrs gp, fcsr, zero
	-[0x80006298]:sw t5, 1848(ra)
Current Store : [0x800062a4] : sw gp, 1872(ra) -- Store: [0x80013610]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006310]:fsgnj.d t5, t3, s10
	-[0x80006314]:csrrs gp, fcsr, zero
	-[0x80006318]:sw t5, 1880(ra)
Current Store : [0x8000631c] : sw t6, 1888(ra) -- Store: [0x80013620]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006310]:fsgnj.d t5, t3, s10
	-[0x80006314]:csrrs gp, fcsr, zero
	-[0x80006318]:sw t5, 1880(ra)
Current Store : [0x80006320] : sw t5, 1896(ra) -- Store: [0x80013628]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006310]:fsgnj.d t5, t3, s10
	-[0x80006314]:csrrs gp, fcsr, zero
	-[0x80006318]:sw t5, 1880(ra)
Current Store : [0x80006324] : sw gp, 1904(ra) -- Store: [0x80013630]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006390]:fsgnj.d t5, t3, s10
	-[0x80006394]:csrrs gp, fcsr, zero
	-[0x80006398]:sw t5, 1912(ra)
Current Store : [0x8000639c] : sw t6, 1920(ra) -- Store: [0x80013640]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006390]:fsgnj.d t5, t3, s10
	-[0x80006394]:csrrs gp, fcsr, zero
	-[0x80006398]:sw t5, 1912(ra)
Current Store : [0x800063a0] : sw t5, 1928(ra) -- Store: [0x80013648]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006390]:fsgnj.d t5, t3, s10
	-[0x80006394]:csrrs gp, fcsr, zero
	-[0x80006398]:sw t5, 1912(ra)
Current Store : [0x800063a4] : sw gp, 1936(ra) -- Store: [0x80013650]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006410]:fsgnj.d t5, t3, s10
	-[0x80006414]:csrrs gp, fcsr, zero
	-[0x80006418]:sw t5, 1944(ra)
Current Store : [0x8000641c] : sw t6, 1952(ra) -- Store: [0x80013660]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006410]:fsgnj.d t5, t3, s10
	-[0x80006414]:csrrs gp, fcsr, zero
	-[0x80006418]:sw t5, 1944(ra)
Current Store : [0x80006420] : sw t5, 1960(ra) -- Store: [0x80013668]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006410]:fsgnj.d t5, t3, s10
	-[0x80006414]:csrrs gp, fcsr, zero
	-[0x80006418]:sw t5, 1944(ra)
Current Store : [0x80006424] : sw gp, 1968(ra) -- Store: [0x80013670]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006490]:fsgnj.d t5, t3, s10
	-[0x80006494]:csrrs gp, fcsr, zero
	-[0x80006498]:sw t5, 1976(ra)
Current Store : [0x8000649c] : sw t6, 1984(ra) -- Store: [0x80013680]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006490]:fsgnj.d t5, t3, s10
	-[0x80006494]:csrrs gp, fcsr, zero
	-[0x80006498]:sw t5, 1976(ra)
Current Store : [0x800064a0] : sw t5, 1992(ra) -- Store: [0x80013688]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006490]:fsgnj.d t5, t3, s10
	-[0x80006494]:csrrs gp, fcsr, zero
	-[0x80006498]:sw t5, 1976(ra)
Current Store : [0x800064a4] : sw gp, 2000(ra) -- Store: [0x80013690]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006510]:fsgnj.d t5, t3, s10
	-[0x80006514]:csrrs gp, fcsr, zero
	-[0x80006518]:sw t5, 2008(ra)
Current Store : [0x8000651c] : sw t6, 2016(ra) -- Store: [0x800136a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006510]:fsgnj.d t5, t3, s10
	-[0x80006514]:csrrs gp, fcsr, zero
	-[0x80006518]:sw t5, 2008(ra)
Current Store : [0x80006520] : sw t5, 2024(ra) -- Store: [0x800136a8]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006510]:fsgnj.d t5, t3, s10
	-[0x80006514]:csrrs gp, fcsr, zero
	-[0x80006518]:sw t5, 2008(ra)
Current Store : [0x80006524] : sw gp, 2032(ra) -- Store: [0x800136b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006590]:fsgnj.d t5, t3, s10
	-[0x80006594]:csrrs gp, fcsr, zero
	-[0x80006598]:sw t5, 2040(ra)
Current Store : [0x800065a0] : sw t6, 8(ra) -- Store: [0x800136c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006590]:fsgnj.d t5, t3, s10
	-[0x80006594]:csrrs gp, fcsr, zero
	-[0x80006598]:sw t5, 2040(ra)
Current Store : [0x800065a4] : sw t5, 16(ra) -- Store: [0x800136c8]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006590]:fsgnj.d t5, t3, s10
	-[0x80006594]:csrrs gp, fcsr, zero
	-[0x80006598]:sw t5, 2040(ra)
Current Store : [0x800065a8] : sw gp, 24(ra) -- Store: [0x800136d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800065dc]:fsgnj.d t5, t3, s10
	-[0x800065e0]:csrrs gp, fcsr, zero
	-[0x800065e4]:sw t5, 0(ra)
Current Store : [0x800065e8] : sw t6, 8(ra) -- Store: [0x800126e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800065dc]:fsgnj.d t5, t3, s10
	-[0x800065e0]:csrrs gp, fcsr, zero
	-[0x800065e4]:sw t5, 0(ra)
Current Store : [0x800065ec] : sw t5, 16(ra) -- Store: [0x800126e8]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800065dc]:fsgnj.d t5, t3, s10
	-[0x800065e0]:csrrs gp, fcsr, zero
	-[0x800065e4]:sw t5, 0(ra)
Current Store : [0x800065f0] : sw gp, 24(ra) -- Store: [0x800126f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000661c]:fsgnj.d t5, t3, s10
	-[0x80006620]:csrrs gp, fcsr, zero
	-[0x80006624]:sw t5, 32(ra)
Current Store : [0x80006628] : sw t6, 40(ra) -- Store: [0x80012700]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000661c]:fsgnj.d t5, t3, s10
	-[0x80006620]:csrrs gp, fcsr, zero
	-[0x80006624]:sw t5, 32(ra)
Current Store : [0x8000662c] : sw t5, 48(ra) -- Store: [0x80012708]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000661c]:fsgnj.d t5, t3, s10
	-[0x80006620]:csrrs gp, fcsr, zero
	-[0x80006624]:sw t5, 32(ra)
Current Store : [0x80006630] : sw gp, 56(ra) -- Store: [0x80012710]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006660]:fsgnj.d t5, t3, s10
	-[0x80006664]:csrrs gp, fcsr, zero
	-[0x80006668]:sw t5, 64(ra)
Current Store : [0x8000666c] : sw t6, 72(ra) -- Store: [0x80012720]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006660]:fsgnj.d t5, t3, s10
	-[0x80006664]:csrrs gp, fcsr, zero
	-[0x80006668]:sw t5, 64(ra)
Current Store : [0x80006670] : sw t5, 80(ra) -- Store: [0x80012728]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006660]:fsgnj.d t5, t3, s10
	-[0x80006664]:csrrs gp, fcsr, zero
	-[0x80006668]:sw t5, 64(ra)
Current Store : [0x80006674] : sw gp, 88(ra) -- Store: [0x80012730]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800066a4]:fsgnj.d t5, t3, s10
	-[0x800066a8]:csrrs gp, fcsr, zero
	-[0x800066ac]:sw t5, 96(ra)
Current Store : [0x800066b0] : sw t6, 104(ra) -- Store: [0x80012740]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800066a4]:fsgnj.d t5, t3, s10
	-[0x800066a8]:csrrs gp, fcsr, zero
	-[0x800066ac]:sw t5, 96(ra)
Current Store : [0x800066b4] : sw t5, 112(ra) -- Store: [0x80012748]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800066a4]:fsgnj.d t5, t3, s10
	-[0x800066a8]:csrrs gp, fcsr, zero
	-[0x800066ac]:sw t5, 96(ra)
Current Store : [0x800066b8] : sw gp, 120(ra) -- Store: [0x80012750]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800066e4]:fsgnj.d t5, t3, s10
	-[0x800066e8]:csrrs gp, fcsr, zero
	-[0x800066ec]:sw t5, 128(ra)
Current Store : [0x800066f0] : sw t6, 136(ra) -- Store: [0x80012760]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800066e4]:fsgnj.d t5, t3, s10
	-[0x800066e8]:csrrs gp, fcsr, zero
	-[0x800066ec]:sw t5, 128(ra)
Current Store : [0x800066f4] : sw t5, 144(ra) -- Store: [0x80012768]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800066e4]:fsgnj.d t5, t3, s10
	-[0x800066e8]:csrrs gp, fcsr, zero
	-[0x800066ec]:sw t5, 128(ra)
Current Store : [0x800066f8] : sw gp, 152(ra) -- Store: [0x80012770]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006724]:fsgnj.d t5, t3, s10
	-[0x80006728]:csrrs gp, fcsr, zero
	-[0x8000672c]:sw t5, 160(ra)
Current Store : [0x80006730] : sw t6, 168(ra) -- Store: [0x80012780]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006724]:fsgnj.d t5, t3, s10
	-[0x80006728]:csrrs gp, fcsr, zero
	-[0x8000672c]:sw t5, 160(ra)
Current Store : [0x80006734] : sw t5, 176(ra) -- Store: [0x80012788]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006724]:fsgnj.d t5, t3, s10
	-[0x80006728]:csrrs gp, fcsr, zero
	-[0x8000672c]:sw t5, 160(ra)
Current Store : [0x80006738] : sw gp, 184(ra) -- Store: [0x80012790]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006764]:fsgnj.d t5, t3, s10
	-[0x80006768]:csrrs gp, fcsr, zero
	-[0x8000676c]:sw t5, 192(ra)
Current Store : [0x80006770] : sw t6, 200(ra) -- Store: [0x800127a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006764]:fsgnj.d t5, t3, s10
	-[0x80006768]:csrrs gp, fcsr, zero
	-[0x8000676c]:sw t5, 192(ra)
Current Store : [0x80006774] : sw t5, 208(ra) -- Store: [0x800127a8]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006764]:fsgnj.d t5, t3, s10
	-[0x80006768]:csrrs gp, fcsr, zero
	-[0x8000676c]:sw t5, 192(ra)
Current Store : [0x80006778] : sw gp, 216(ra) -- Store: [0x800127b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800067a4]:fsgnj.d t5, t3, s10
	-[0x800067a8]:csrrs gp, fcsr, zero
	-[0x800067ac]:sw t5, 224(ra)
Current Store : [0x800067b0] : sw t6, 232(ra) -- Store: [0x800127c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800067a4]:fsgnj.d t5, t3, s10
	-[0x800067a8]:csrrs gp, fcsr, zero
	-[0x800067ac]:sw t5, 224(ra)
Current Store : [0x800067b4] : sw t5, 240(ra) -- Store: [0x800127c8]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800067a4]:fsgnj.d t5, t3, s10
	-[0x800067a8]:csrrs gp, fcsr, zero
	-[0x800067ac]:sw t5, 224(ra)
Current Store : [0x800067b8] : sw gp, 248(ra) -- Store: [0x800127d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800067e8]:fsgnj.d t5, t3, s10
	-[0x800067ec]:csrrs gp, fcsr, zero
	-[0x800067f0]:sw t5, 256(ra)
Current Store : [0x800067f4] : sw t6, 264(ra) -- Store: [0x800127e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800067e8]:fsgnj.d t5, t3, s10
	-[0x800067ec]:csrrs gp, fcsr, zero
	-[0x800067f0]:sw t5, 256(ra)
Current Store : [0x800067f8] : sw t5, 272(ra) -- Store: [0x800127e8]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800067e8]:fsgnj.d t5, t3, s10
	-[0x800067ec]:csrrs gp, fcsr, zero
	-[0x800067f0]:sw t5, 256(ra)
Current Store : [0x800067fc] : sw gp, 280(ra) -- Store: [0x800127f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000682c]:fsgnj.d t5, t3, s10
	-[0x80006830]:csrrs gp, fcsr, zero
	-[0x80006834]:sw t5, 288(ra)
Current Store : [0x80006838] : sw t6, 296(ra) -- Store: [0x80012800]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000682c]:fsgnj.d t5, t3, s10
	-[0x80006830]:csrrs gp, fcsr, zero
	-[0x80006834]:sw t5, 288(ra)
Current Store : [0x8000683c] : sw t5, 304(ra) -- Store: [0x80012808]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000682c]:fsgnj.d t5, t3, s10
	-[0x80006830]:csrrs gp, fcsr, zero
	-[0x80006834]:sw t5, 288(ra)
Current Store : [0x80006840] : sw gp, 312(ra) -- Store: [0x80012810]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000686c]:fsgnj.d t5, t3, s10
	-[0x80006870]:csrrs gp, fcsr, zero
	-[0x80006874]:sw t5, 320(ra)
Current Store : [0x80006878] : sw t6, 328(ra) -- Store: [0x80012820]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000686c]:fsgnj.d t5, t3, s10
	-[0x80006870]:csrrs gp, fcsr, zero
	-[0x80006874]:sw t5, 320(ra)
Current Store : [0x8000687c] : sw t5, 336(ra) -- Store: [0x80012828]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000686c]:fsgnj.d t5, t3, s10
	-[0x80006870]:csrrs gp, fcsr, zero
	-[0x80006874]:sw t5, 320(ra)
Current Store : [0x80006880] : sw gp, 344(ra) -- Store: [0x80012830]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800068ac]:fsgnj.d t5, t3, s10
	-[0x800068b0]:csrrs gp, fcsr, zero
	-[0x800068b4]:sw t5, 352(ra)
Current Store : [0x800068b8] : sw t6, 360(ra) -- Store: [0x80012840]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800068ac]:fsgnj.d t5, t3, s10
	-[0x800068b0]:csrrs gp, fcsr, zero
	-[0x800068b4]:sw t5, 352(ra)
Current Store : [0x800068bc] : sw t5, 368(ra) -- Store: [0x80012848]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800068ac]:fsgnj.d t5, t3, s10
	-[0x800068b0]:csrrs gp, fcsr, zero
	-[0x800068b4]:sw t5, 352(ra)
Current Store : [0x800068c0] : sw gp, 376(ra) -- Store: [0x80012850]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800068ec]:fsgnj.d t5, t3, s10
	-[0x800068f0]:csrrs gp, fcsr, zero
	-[0x800068f4]:sw t5, 384(ra)
Current Store : [0x800068f8] : sw t6, 392(ra) -- Store: [0x80012860]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800068ec]:fsgnj.d t5, t3, s10
	-[0x800068f0]:csrrs gp, fcsr, zero
	-[0x800068f4]:sw t5, 384(ra)
Current Store : [0x800068fc] : sw t5, 400(ra) -- Store: [0x80012868]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800068ec]:fsgnj.d t5, t3, s10
	-[0x800068f0]:csrrs gp, fcsr, zero
	-[0x800068f4]:sw t5, 384(ra)
Current Store : [0x80006900] : sw gp, 408(ra) -- Store: [0x80012870]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000692c]:fsgnj.d t5, t3, s10
	-[0x80006930]:csrrs gp, fcsr, zero
	-[0x80006934]:sw t5, 416(ra)
Current Store : [0x80006938] : sw t6, 424(ra) -- Store: [0x80012880]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000692c]:fsgnj.d t5, t3, s10
	-[0x80006930]:csrrs gp, fcsr, zero
	-[0x80006934]:sw t5, 416(ra)
Current Store : [0x8000693c] : sw t5, 432(ra) -- Store: [0x80012888]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000692c]:fsgnj.d t5, t3, s10
	-[0x80006930]:csrrs gp, fcsr, zero
	-[0x80006934]:sw t5, 416(ra)
Current Store : [0x80006940] : sw gp, 440(ra) -- Store: [0x80012890]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000696c]:fsgnj.d t5, t3, s10
	-[0x80006970]:csrrs gp, fcsr, zero
	-[0x80006974]:sw t5, 448(ra)
Current Store : [0x80006978] : sw t6, 456(ra) -- Store: [0x800128a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000696c]:fsgnj.d t5, t3, s10
	-[0x80006970]:csrrs gp, fcsr, zero
	-[0x80006974]:sw t5, 448(ra)
Current Store : [0x8000697c] : sw t5, 464(ra) -- Store: [0x800128a8]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000696c]:fsgnj.d t5, t3, s10
	-[0x80006970]:csrrs gp, fcsr, zero
	-[0x80006974]:sw t5, 448(ra)
Current Store : [0x80006980] : sw gp, 472(ra) -- Store: [0x800128b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800069ac]:fsgnj.d t5, t3, s10
	-[0x800069b0]:csrrs gp, fcsr, zero
	-[0x800069b4]:sw t5, 480(ra)
Current Store : [0x800069b8] : sw t6, 488(ra) -- Store: [0x800128c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800069ac]:fsgnj.d t5, t3, s10
	-[0x800069b0]:csrrs gp, fcsr, zero
	-[0x800069b4]:sw t5, 480(ra)
Current Store : [0x800069bc] : sw t5, 496(ra) -- Store: [0x800128c8]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800069ac]:fsgnj.d t5, t3, s10
	-[0x800069b0]:csrrs gp, fcsr, zero
	-[0x800069b4]:sw t5, 480(ra)
Current Store : [0x800069c0] : sw gp, 504(ra) -- Store: [0x800128d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800069ec]:fsgnj.d t5, t3, s10
	-[0x800069f0]:csrrs gp, fcsr, zero
	-[0x800069f4]:sw t5, 512(ra)
Current Store : [0x800069f8] : sw t6, 520(ra) -- Store: [0x800128e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800069ec]:fsgnj.d t5, t3, s10
	-[0x800069f0]:csrrs gp, fcsr, zero
	-[0x800069f4]:sw t5, 512(ra)
Current Store : [0x800069fc] : sw t5, 528(ra) -- Store: [0x800128e8]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800069ec]:fsgnj.d t5, t3, s10
	-[0x800069f0]:csrrs gp, fcsr, zero
	-[0x800069f4]:sw t5, 512(ra)
Current Store : [0x80006a00] : sw gp, 536(ra) -- Store: [0x800128f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006a2c]:fsgnj.d t5, t3, s10
	-[0x80006a30]:csrrs gp, fcsr, zero
	-[0x80006a34]:sw t5, 544(ra)
Current Store : [0x80006a38] : sw t6, 552(ra) -- Store: [0x80012900]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006a2c]:fsgnj.d t5, t3, s10
	-[0x80006a30]:csrrs gp, fcsr, zero
	-[0x80006a34]:sw t5, 544(ra)
Current Store : [0x80006a3c] : sw t5, 560(ra) -- Store: [0x80012908]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006a2c]:fsgnj.d t5, t3, s10
	-[0x80006a30]:csrrs gp, fcsr, zero
	-[0x80006a34]:sw t5, 544(ra)
Current Store : [0x80006a40] : sw gp, 568(ra) -- Store: [0x80012910]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006a6c]:fsgnj.d t5, t3, s10
	-[0x80006a70]:csrrs gp, fcsr, zero
	-[0x80006a74]:sw t5, 576(ra)
Current Store : [0x80006a78] : sw t6, 584(ra) -- Store: [0x80012920]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006a6c]:fsgnj.d t5, t3, s10
	-[0x80006a70]:csrrs gp, fcsr, zero
	-[0x80006a74]:sw t5, 576(ra)
Current Store : [0x80006a7c] : sw t5, 592(ra) -- Store: [0x80012928]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006a6c]:fsgnj.d t5, t3, s10
	-[0x80006a70]:csrrs gp, fcsr, zero
	-[0x80006a74]:sw t5, 576(ra)
Current Store : [0x80006a80] : sw gp, 600(ra) -- Store: [0x80012930]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006aac]:fsgnj.d t5, t3, s10
	-[0x80006ab0]:csrrs gp, fcsr, zero
	-[0x80006ab4]:sw t5, 608(ra)
Current Store : [0x80006ab8] : sw t6, 616(ra) -- Store: [0x80012940]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006aac]:fsgnj.d t5, t3, s10
	-[0x80006ab0]:csrrs gp, fcsr, zero
	-[0x80006ab4]:sw t5, 608(ra)
Current Store : [0x80006abc] : sw t5, 624(ra) -- Store: [0x80012948]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006aac]:fsgnj.d t5, t3, s10
	-[0x80006ab0]:csrrs gp, fcsr, zero
	-[0x80006ab4]:sw t5, 608(ra)
Current Store : [0x80006ac0] : sw gp, 632(ra) -- Store: [0x80012950]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006af0]:fsgnj.d t5, t3, s10
	-[0x80006af4]:csrrs gp, fcsr, zero
	-[0x80006af8]:sw t5, 640(ra)
Current Store : [0x80006afc] : sw t6, 648(ra) -- Store: [0x80012960]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006af0]:fsgnj.d t5, t3, s10
	-[0x80006af4]:csrrs gp, fcsr, zero
	-[0x80006af8]:sw t5, 640(ra)
Current Store : [0x80006b00] : sw t5, 656(ra) -- Store: [0x80012968]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006af0]:fsgnj.d t5, t3, s10
	-[0x80006af4]:csrrs gp, fcsr, zero
	-[0x80006af8]:sw t5, 640(ra)
Current Store : [0x80006b04] : sw gp, 664(ra) -- Store: [0x80012970]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006b34]:fsgnj.d t5, t3, s10
	-[0x80006b38]:csrrs gp, fcsr, zero
	-[0x80006b3c]:sw t5, 672(ra)
Current Store : [0x80006b40] : sw t6, 680(ra) -- Store: [0x80012980]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006b34]:fsgnj.d t5, t3, s10
	-[0x80006b38]:csrrs gp, fcsr, zero
	-[0x80006b3c]:sw t5, 672(ra)
Current Store : [0x80006b44] : sw t5, 688(ra) -- Store: [0x80012988]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006b34]:fsgnj.d t5, t3, s10
	-[0x80006b38]:csrrs gp, fcsr, zero
	-[0x80006b3c]:sw t5, 672(ra)
Current Store : [0x80006b48] : sw gp, 696(ra) -- Store: [0x80012990]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006b78]:fsgnj.d t5, t3, s10
	-[0x80006b7c]:csrrs gp, fcsr, zero
	-[0x80006b80]:sw t5, 704(ra)
Current Store : [0x80006b84] : sw t6, 712(ra) -- Store: [0x800129a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006b78]:fsgnj.d t5, t3, s10
	-[0x80006b7c]:csrrs gp, fcsr, zero
	-[0x80006b80]:sw t5, 704(ra)
Current Store : [0x80006b88] : sw t5, 720(ra) -- Store: [0x800129a8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006b78]:fsgnj.d t5, t3, s10
	-[0x80006b7c]:csrrs gp, fcsr, zero
	-[0x80006b80]:sw t5, 704(ra)
Current Store : [0x80006b8c] : sw gp, 728(ra) -- Store: [0x800129b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006bbc]:fsgnj.d t5, t3, s10
	-[0x80006bc0]:csrrs gp, fcsr, zero
	-[0x80006bc4]:sw t5, 736(ra)
Current Store : [0x80006bc8] : sw t6, 744(ra) -- Store: [0x800129c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006bbc]:fsgnj.d t5, t3, s10
	-[0x80006bc0]:csrrs gp, fcsr, zero
	-[0x80006bc4]:sw t5, 736(ra)
Current Store : [0x80006bcc] : sw t5, 752(ra) -- Store: [0x800129c8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006bbc]:fsgnj.d t5, t3, s10
	-[0x80006bc0]:csrrs gp, fcsr, zero
	-[0x80006bc4]:sw t5, 736(ra)
Current Store : [0x80006bd0] : sw gp, 760(ra) -- Store: [0x800129d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006c00]:fsgnj.d t5, t3, s10
	-[0x80006c04]:csrrs gp, fcsr, zero
	-[0x80006c08]:sw t5, 768(ra)
Current Store : [0x80006c0c] : sw t6, 776(ra) -- Store: [0x800129e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006c00]:fsgnj.d t5, t3, s10
	-[0x80006c04]:csrrs gp, fcsr, zero
	-[0x80006c08]:sw t5, 768(ra)
Current Store : [0x80006c10] : sw t5, 784(ra) -- Store: [0x800129e8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006c00]:fsgnj.d t5, t3, s10
	-[0x80006c04]:csrrs gp, fcsr, zero
	-[0x80006c08]:sw t5, 768(ra)
Current Store : [0x80006c14] : sw gp, 792(ra) -- Store: [0x800129f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006c44]:fsgnj.d t5, t3, s10
	-[0x80006c48]:csrrs gp, fcsr, zero
	-[0x80006c4c]:sw t5, 800(ra)
Current Store : [0x80006c50] : sw t6, 808(ra) -- Store: [0x80012a00]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006c44]:fsgnj.d t5, t3, s10
	-[0x80006c48]:csrrs gp, fcsr, zero
	-[0x80006c4c]:sw t5, 800(ra)
Current Store : [0x80006c54] : sw t5, 816(ra) -- Store: [0x80012a08]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006c44]:fsgnj.d t5, t3, s10
	-[0x80006c48]:csrrs gp, fcsr, zero
	-[0x80006c4c]:sw t5, 800(ra)
Current Store : [0x80006c58] : sw gp, 824(ra) -- Store: [0x80012a10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006c8c]:fsgnj.d t5, t3, s10
	-[0x80006c90]:csrrs gp, fcsr, zero
	-[0x80006c94]:sw t5, 832(ra)
Current Store : [0x80006c98] : sw t6, 840(ra) -- Store: [0x80012a20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006c8c]:fsgnj.d t5, t3, s10
	-[0x80006c90]:csrrs gp, fcsr, zero
	-[0x80006c94]:sw t5, 832(ra)
Current Store : [0x80006c9c] : sw t5, 848(ra) -- Store: [0x80012a28]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006c8c]:fsgnj.d t5, t3, s10
	-[0x80006c90]:csrrs gp, fcsr, zero
	-[0x80006c94]:sw t5, 832(ra)
Current Store : [0x80006ca0] : sw gp, 856(ra) -- Store: [0x80012a30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006cd4]:fsgnj.d t5, t3, s10
	-[0x80006cd8]:csrrs gp, fcsr, zero
	-[0x80006cdc]:sw t5, 864(ra)
Current Store : [0x80006ce0] : sw t6, 872(ra) -- Store: [0x80012a40]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006cd4]:fsgnj.d t5, t3, s10
	-[0x80006cd8]:csrrs gp, fcsr, zero
	-[0x80006cdc]:sw t5, 864(ra)
Current Store : [0x80006ce4] : sw t5, 880(ra) -- Store: [0x80012a48]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006cd4]:fsgnj.d t5, t3, s10
	-[0x80006cd8]:csrrs gp, fcsr, zero
	-[0x80006cdc]:sw t5, 864(ra)
Current Store : [0x80006ce8] : sw gp, 888(ra) -- Store: [0x80012a50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006d18]:fsgnj.d t5, t3, s10
	-[0x80006d1c]:csrrs gp, fcsr, zero
	-[0x80006d20]:sw t5, 896(ra)
Current Store : [0x80006d24] : sw t6, 904(ra) -- Store: [0x80012a60]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006d18]:fsgnj.d t5, t3, s10
	-[0x80006d1c]:csrrs gp, fcsr, zero
	-[0x80006d20]:sw t5, 896(ra)
Current Store : [0x80006d28] : sw t5, 912(ra) -- Store: [0x80012a68]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006d18]:fsgnj.d t5, t3, s10
	-[0x80006d1c]:csrrs gp, fcsr, zero
	-[0x80006d20]:sw t5, 896(ra)
Current Store : [0x80006d2c] : sw gp, 920(ra) -- Store: [0x80012a70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006d5c]:fsgnj.d t5, t3, s10
	-[0x80006d60]:csrrs gp, fcsr, zero
	-[0x80006d64]:sw t5, 928(ra)
Current Store : [0x80006d68] : sw t6, 936(ra) -- Store: [0x80012a80]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006d5c]:fsgnj.d t5, t3, s10
	-[0x80006d60]:csrrs gp, fcsr, zero
	-[0x80006d64]:sw t5, 928(ra)
Current Store : [0x80006d6c] : sw t5, 944(ra) -- Store: [0x80012a88]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006d5c]:fsgnj.d t5, t3, s10
	-[0x80006d60]:csrrs gp, fcsr, zero
	-[0x80006d64]:sw t5, 928(ra)
Current Store : [0x80006d70] : sw gp, 952(ra) -- Store: [0x80012a90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006da0]:fsgnj.d t5, t3, s10
	-[0x80006da4]:csrrs gp, fcsr, zero
	-[0x80006da8]:sw t5, 960(ra)
Current Store : [0x80006dac] : sw t6, 968(ra) -- Store: [0x80012aa0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006da0]:fsgnj.d t5, t3, s10
	-[0x80006da4]:csrrs gp, fcsr, zero
	-[0x80006da8]:sw t5, 960(ra)
Current Store : [0x80006db0] : sw t5, 976(ra) -- Store: [0x80012aa8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006da0]:fsgnj.d t5, t3, s10
	-[0x80006da4]:csrrs gp, fcsr, zero
	-[0x80006da8]:sw t5, 960(ra)
Current Store : [0x80006db4] : sw gp, 984(ra) -- Store: [0x80012ab0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006de4]:fsgnj.d t5, t3, s10
	-[0x80006de8]:csrrs gp, fcsr, zero
	-[0x80006dec]:sw t5, 992(ra)
Current Store : [0x80006df0] : sw t6, 1000(ra) -- Store: [0x80012ac0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006de4]:fsgnj.d t5, t3, s10
	-[0x80006de8]:csrrs gp, fcsr, zero
	-[0x80006dec]:sw t5, 992(ra)
Current Store : [0x80006df4] : sw t5, 1008(ra) -- Store: [0x80012ac8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006de4]:fsgnj.d t5, t3, s10
	-[0x80006de8]:csrrs gp, fcsr, zero
	-[0x80006dec]:sw t5, 992(ra)
Current Store : [0x80006df8] : sw gp, 1016(ra) -- Store: [0x80012ad0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006e2c]:fsgnj.d t5, t3, s10
	-[0x80006e30]:csrrs gp, fcsr, zero
	-[0x80006e34]:sw t5, 1024(ra)
Current Store : [0x80006e38] : sw t6, 1032(ra) -- Store: [0x80012ae0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006e2c]:fsgnj.d t5, t3, s10
	-[0x80006e30]:csrrs gp, fcsr, zero
	-[0x80006e34]:sw t5, 1024(ra)
Current Store : [0x80006e3c] : sw t5, 1040(ra) -- Store: [0x80012ae8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006e2c]:fsgnj.d t5, t3, s10
	-[0x80006e30]:csrrs gp, fcsr, zero
	-[0x80006e34]:sw t5, 1024(ra)
Current Store : [0x80006e40] : sw gp, 1048(ra) -- Store: [0x80012af0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006e74]:fsgnj.d t5, t3, s10
	-[0x80006e78]:csrrs gp, fcsr, zero
	-[0x80006e7c]:sw t5, 1056(ra)
Current Store : [0x80006e80] : sw t6, 1064(ra) -- Store: [0x80012b00]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006e74]:fsgnj.d t5, t3, s10
	-[0x80006e78]:csrrs gp, fcsr, zero
	-[0x80006e7c]:sw t5, 1056(ra)
Current Store : [0x80006e84] : sw t5, 1072(ra) -- Store: [0x80012b08]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006e74]:fsgnj.d t5, t3, s10
	-[0x80006e78]:csrrs gp, fcsr, zero
	-[0x80006e7c]:sw t5, 1056(ra)
Current Store : [0x80006e88] : sw gp, 1080(ra) -- Store: [0x80012b10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006eb8]:fsgnj.d t5, t3, s10
	-[0x80006ebc]:csrrs gp, fcsr, zero
	-[0x80006ec0]:sw t5, 1088(ra)
Current Store : [0x80006ec4] : sw t6, 1096(ra) -- Store: [0x80012b20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006eb8]:fsgnj.d t5, t3, s10
	-[0x80006ebc]:csrrs gp, fcsr, zero
	-[0x80006ec0]:sw t5, 1088(ra)
Current Store : [0x80006ec8] : sw t5, 1104(ra) -- Store: [0x80012b28]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006eb8]:fsgnj.d t5, t3, s10
	-[0x80006ebc]:csrrs gp, fcsr, zero
	-[0x80006ec0]:sw t5, 1088(ra)
Current Store : [0x80006ecc] : sw gp, 1112(ra) -- Store: [0x80012b30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006efc]:fsgnj.d t5, t3, s10
	-[0x80006f00]:csrrs gp, fcsr, zero
	-[0x80006f04]:sw t5, 1120(ra)
Current Store : [0x80006f08] : sw t6, 1128(ra) -- Store: [0x80012b40]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006efc]:fsgnj.d t5, t3, s10
	-[0x80006f00]:csrrs gp, fcsr, zero
	-[0x80006f04]:sw t5, 1120(ra)
Current Store : [0x80006f0c] : sw t5, 1136(ra) -- Store: [0x80012b48]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006efc]:fsgnj.d t5, t3, s10
	-[0x80006f00]:csrrs gp, fcsr, zero
	-[0x80006f04]:sw t5, 1120(ra)
Current Store : [0x80006f10] : sw gp, 1144(ra) -- Store: [0x80012b50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006f40]:fsgnj.d t5, t3, s10
	-[0x80006f44]:csrrs gp, fcsr, zero
	-[0x80006f48]:sw t5, 1152(ra)
Current Store : [0x80006f4c] : sw t6, 1160(ra) -- Store: [0x80012b60]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006f40]:fsgnj.d t5, t3, s10
	-[0x80006f44]:csrrs gp, fcsr, zero
	-[0x80006f48]:sw t5, 1152(ra)
Current Store : [0x80006f50] : sw t5, 1168(ra) -- Store: [0x80012b68]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006f40]:fsgnj.d t5, t3, s10
	-[0x80006f44]:csrrs gp, fcsr, zero
	-[0x80006f48]:sw t5, 1152(ra)
Current Store : [0x80006f54] : sw gp, 1176(ra) -- Store: [0x80012b70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006f84]:fsgnj.d t5, t3, s10
	-[0x80006f88]:csrrs gp, fcsr, zero
	-[0x80006f8c]:sw t5, 1184(ra)
Current Store : [0x80006f90] : sw t6, 1192(ra) -- Store: [0x80012b80]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006f84]:fsgnj.d t5, t3, s10
	-[0x80006f88]:csrrs gp, fcsr, zero
	-[0x80006f8c]:sw t5, 1184(ra)
Current Store : [0x80006f94] : sw t5, 1200(ra) -- Store: [0x80012b88]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006f84]:fsgnj.d t5, t3, s10
	-[0x80006f88]:csrrs gp, fcsr, zero
	-[0x80006f8c]:sw t5, 1184(ra)
Current Store : [0x80006f98] : sw gp, 1208(ra) -- Store: [0x80012b90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006fc8]:fsgnj.d t5, t3, s10
	-[0x80006fcc]:csrrs gp, fcsr, zero
	-[0x80006fd0]:sw t5, 1216(ra)
Current Store : [0x80006fd4] : sw t6, 1224(ra) -- Store: [0x80012ba0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006fc8]:fsgnj.d t5, t3, s10
	-[0x80006fcc]:csrrs gp, fcsr, zero
	-[0x80006fd0]:sw t5, 1216(ra)
Current Store : [0x80006fd8] : sw t5, 1232(ra) -- Store: [0x80012ba8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80006fc8]:fsgnj.d t5, t3, s10
	-[0x80006fcc]:csrrs gp, fcsr, zero
	-[0x80006fd0]:sw t5, 1216(ra)
Current Store : [0x80006fdc] : sw gp, 1240(ra) -- Store: [0x80012bb0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000700c]:fsgnj.d t5, t3, s10
	-[0x80007010]:csrrs gp, fcsr, zero
	-[0x80007014]:sw t5, 1248(ra)
Current Store : [0x80007018] : sw t6, 1256(ra) -- Store: [0x80012bc0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000700c]:fsgnj.d t5, t3, s10
	-[0x80007010]:csrrs gp, fcsr, zero
	-[0x80007014]:sw t5, 1248(ra)
Current Store : [0x8000701c] : sw t5, 1264(ra) -- Store: [0x80012bc8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000700c]:fsgnj.d t5, t3, s10
	-[0x80007010]:csrrs gp, fcsr, zero
	-[0x80007014]:sw t5, 1248(ra)
Current Store : [0x80007020] : sw gp, 1272(ra) -- Store: [0x80012bd0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007050]:fsgnj.d t5, t3, s10
	-[0x80007054]:csrrs gp, fcsr, zero
	-[0x80007058]:sw t5, 1280(ra)
Current Store : [0x8000705c] : sw t6, 1288(ra) -- Store: [0x80012be0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007050]:fsgnj.d t5, t3, s10
	-[0x80007054]:csrrs gp, fcsr, zero
	-[0x80007058]:sw t5, 1280(ra)
Current Store : [0x80007060] : sw t5, 1296(ra) -- Store: [0x80012be8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007050]:fsgnj.d t5, t3, s10
	-[0x80007054]:csrrs gp, fcsr, zero
	-[0x80007058]:sw t5, 1280(ra)
Current Store : [0x80007064] : sw gp, 1304(ra) -- Store: [0x80012bf0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007094]:fsgnj.d t5, t3, s10
	-[0x80007098]:csrrs gp, fcsr, zero
	-[0x8000709c]:sw t5, 1312(ra)
Current Store : [0x800070a0] : sw t6, 1320(ra) -- Store: [0x80012c00]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007094]:fsgnj.d t5, t3, s10
	-[0x80007098]:csrrs gp, fcsr, zero
	-[0x8000709c]:sw t5, 1312(ra)
Current Store : [0x800070a4] : sw t5, 1328(ra) -- Store: [0x80012c08]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007094]:fsgnj.d t5, t3, s10
	-[0x80007098]:csrrs gp, fcsr, zero
	-[0x8000709c]:sw t5, 1312(ra)
Current Store : [0x800070a8] : sw gp, 1336(ra) -- Store: [0x80012c10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800070d8]:fsgnj.d t5, t3, s10
	-[0x800070dc]:csrrs gp, fcsr, zero
	-[0x800070e0]:sw t5, 1344(ra)
Current Store : [0x800070e4] : sw t6, 1352(ra) -- Store: [0x80012c20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800070d8]:fsgnj.d t5, t3, s10
	-[0x800070dc]:csrrs gp, fcsr, zero
	-[0x800070e0]:sw t5, 1344(ra)
Current Store : [0x800070e8] : sw t5, 1360(ra) -- Store: [0x80012c28]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800070d8]:fsgnj.d t5, t3, s10
	-[0x800070dc]:csrrs gp, fcsr, zero
	-[0x800070e0]:sw t5, 1344(ra)
Current Store : [0x800070ec] : sw gp, 1368(ra) -- Store: [0x80012c30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000711c]:fsgnj.d t5, t3, s10
	-[0x80007120]:csrrs gp, fcsr, zero
	-[0x80007124]:sw t5, 1376(ra)
Current Store : [0x80007128] : sw t6, 1384(ra) -- Store: [0x80012c40]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000711c]:fsgnj.d t5, t3, s10
	-[0x80007120]:csrrs gp, fcsr, zero
	-[0x80007124]:sw t5, 1376(ra)
Current Store : [0x8000712c] : sw t5, 1392(ra) -- Store: [0x80012c48]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000711c]:fsgnj.d t5, t3, s10
	-[0x80007120]:csrrs gp, fcsr, zero
	-[0x80007124]:sw t5, 1376(ra)
Current Store : [0x80007130] : sw gp, 1400(ra) -- Store: [0x80012c50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007160]:fsgnj.d t5, t3, s10
	-[0x80007164]:csrrs gp, fcsr, zero
	-[0x80007168]:sw t5, 1408(ra)
Current Store : [0x8000716c] : sw t6, 1416(ra) -- Store: [0x80012c60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007160]:fsgnj.d t5, t3, s10
	-[0x80007164]:csrrs gp, fcsr, zero
	-[0x80007168]:sw t5, 1408(ra)
Current Store : [0x80007170] : sw t5, 1424(ra) -- Store: [0x80012c68]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007160]:fsgnj.d t5, t3, s10
	-[0x80007164]:csrrs gp, fcsr, zero
	-[0x80007168]:sw t5, 1408(ra)
Current Store : [0x80007174] : sw gp, 1432(ra) -- Store: [0x80012c70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800071a4]:fsgnj.d t5, t3, s10
	-[0x800071a8]:csrrs gp, fcsr, zero
	-[0x800071ac]:sw t5, 1440(ra)
Current Store : [0x800071b0] : sw t6, 1448(ra) -- Store: [0x80012c80]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800071a4]:fsgnj.d t5, t3, s10
	-[0x800071a8]:csrrs gp, fcsr, zero
	-[0x800071ac]:sw t5, 1440(ra)
Current Store : [0x800071b4] : sw t5, 1456(ra) -- Store: [0x80012c88]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800071a4]:fsgnj.d t5, t3, s10
	-[0x800071a8]:csrrs gp, fcsr, zero
	-[0x800071ac]:sw t5, 1440(ra)
Current Store : [0x800071b8] : sw gp, 1464(ra) -- Store: [0x80012c90]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800071e8]:fsgnj.d t5, t3, s10
	-[0x800071ec]:csrrs gp, fcsr, zero
	-[0x800071f0]:sw t5, 1472(ra)
Current Store : [0x800071f4] : sw t6, 1480(ra) -- Store: [0x80012ca0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800071e8]:fsgnj.d t5, t3, s10
	-[0x800071ec]:csrrs gp, fcsr, zero
	-[0x800071f0]:sw t5, 1472(ra)
Current Store : [0x800071f8] : sw t5, 1488(ra) -- Store: [0x80012ca8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800071e8]:fsgnj.d t5, t3, s10
	-[0x800071ec]:csrrs gp, fcsr, zero
	-[0x800071f0]:sw t5, 1472(ra)
Current Store : [0x800071fc] : sw gp, 1496(ra) -- Store: [0x80012cb0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000722c]:fsgnj.d t5, t3, s10
	-[0x80007230]:csrrs gp, fcsr, zero
	-[0x80007234]:sw t5, 1504(ra)
Current Store : [0x80007238] : sw t6, 1512(ra) -- Store: [0x80012cc0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000722c]:fsgnj.d t5, t3, s10
	-[0x80007230]:csrrs gp, fcsr, zero
	-[0x80007234]:sw t5, 1504(ra)
Current Store : [0x8000723c] : sw t5, 1520(ra) -- Store: [0x80012cc8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000722c]:fsgnj.d t5, t3, s10
	-[0x80007230]:csrrs gp, fcsr, zero
	-[0x80007234]:sw t5, 1504(ra)
Current Store : [0x80007240] : sw gp, 1528(ra) -- Store: [0x80012cd0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007270]:fsgnj.d t5, t3, s10
	-[0x80007274]:csrrs gp, fcsr, zero
	-[0x80007278]:sw t5, 1536(ra)
Current Store : [0x8000727c] : sw t6, 1544(ra) -- Store: [0x80012ce0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007270]:fsgnj.d t5, t3, s10
	-[0x80007274]:csrrs gp, fcsr, zero
	-[0x80007278]:sw t5, 1536(ra)
Current Store : [0x80007280] : sw t5, 1552(ra) -- Store: [0x80012ce8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007270]:fsgnj.d t5, t3, s10
	-[0x80007274]:csrrs gp, fcsr, zero
	-[0x80007278]:sw t5, 1536(ra)
Current Store : [0x80007284] : sw gp, 1560(ra) -- Store: [0x80012cf0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800072b4]:fsgnj.d t5, t3, s10
	-[0x800072b8]:csrrs gp, fcsr, zero
	-[0x800072bc]:sw t5, 1568(ra)
Current Store : [0x800072c0] : sw t6, 1576(ra) -- Store: [0x80012d00]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800072b4]:fsgnj.d t5, t3, s10
	-[0x800072b8]:csrrs gp, fcsr, zero
	-[0x800072bc]:sw t5, 1568(ra)
Current Store : [0x800072c4] : sw t5, 1584(ra) -- Store: [0x80012d08]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800072b4]:fsgnj.d t5, t3, s10
	-[0x800072b8]:csrrs gp, fcsr, zero
	-[0x800072bc]:sw t5, 1568(ra)
Current Store : [0x800072c8] : sw gp, 1592(ra) -- Store: [0x80012d10]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800072fc]:fsgnj.d t5, t3, s10
	-[0x80007300]:csrrs gp, fcsr, zero
	-[0x80007304]:sw t5, 1600(ra)
Current Store : [0x80007308] : sw t6, 1608(ra) -- Store: [0x80012d20]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800072fc]:fsgnj.d t5, t3, s10
	-[0x80007300]:csrrs gp, fcsr, zero
	-[0x80007304]:sw t5, 1600(ra)
Current Store : [0x8000730c] : sw t5, 1616(ra) -- Store: [0x80012d28]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800072fc]:fsgnj.d t5, t3, s10
	-[0x80007300]:csrrs gp, fcsr, zero
	-[0x80007304]:sw t5, 1600(ra)
Current Store : [0x80007310] : sw gp, 1624(ra) -- Store: [0x80012d30]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007344]:fsgnj.d t5, t3, s10
	-[0x80007348]:csrrs gp, fcsr, zero
	-[0x8000734c]:sw t5, 1632(ra)
Current Store : [0x80007350] : sw t6, 1640(ra) -- Store: [0x80012d40]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007344]:fsgnj.d t5, t3, s10
	-[0x80007348]:csrrs gp, fcsr, zero
	-[0x8000734c]:sw t5, 1632(ra)
Current Store : [0x80007354] : sw t5, 1648(ra) -- Store: [0x80012d48]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007344]:fsgnj.d t5, t3, s10
	-[0x80007348]:csrrs gp, fcsr, zero
	-[0x8000734c]:sw t5, 1632(ra)
Current Store : [0x80007358] : sw gp, 1656(ra) -- Store: [0x80012d50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007388]:fsgnj.d t5, t3, s10
	-[0x8000738c]:csrrs gp, fcsr, zero
	-[0x80007390]:sw t5, 1664(ra)
Current Store : [0x80007394] : sw t6, 1672(ra) -- Store: [0x80012d60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007388]:fsgnj.d t5, t3, s10
	-[0x8000738c]:csrrs gp, fcsr, zero
	-[0x80007390]:sw t5, 1664(ra)
Current Store : [0x80007398] : sw t5, 1680(ra) -- Store: [0x80012d68]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007388]:fsgnj.d t5, t3, s10
	-[0x8000738c]:csrrs gp, fcsr, zero
	-[0x80007390]:sw t5, 1664(ra)
Current Store : [0x8000739c] : sw gp, 1688(ra) -- Store: [0x80012d70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800073cc]:fsgnj.d t5, t3, s10
	-[0x800073d0]:csrrs gp, fcsr, zero
	-[0x800073d4]:sw t5, 1696(ra)
Current Store : [0x800073d8] : sw t6, 1704(ra) -- Store: [0x80012d80]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800073cc]:fsgnj.d t5, t3, s10
	-[0x800073d0]:csrrs gp, fcsr, zero
	-[0x800073d4]:sw t5, 1696(ra)
Current Store : [0x800073dc] : sw t5, 1712(ra) -- Store: [0x80012d88]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800073cc]:fsgnj.d t5, t3, s10
	-[0x800073d0]:csrrs gp, fcsr, zero
	-[0x800073d4]:sw t5, 1696(ra)
Current Store : [0x800073e0] : sw gp, 1720(ra) -- Store: [0x80012d90]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007410]:fsgnj.d t5, t3, s10
	-[0x80007414]:csrrs gp, fcsr, zero
	-[0x80007418]:sw t5, 1728(ra)
Current Store : [0x8000741c] : sw t6, 1736(ra) -- Store: [0x80012da0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007410]:fsgnj.d t5, t3, s10
	-[0x80007414]:csrrs gp, fcsr, zero
	-[0x80007418]:sw t5, 1728(ra)
Current Store : [0x80007420] : sw t5, 1744(ra) -- Store: [0x80012da8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007410]:fsgnj.d t5, t3, s10
	-[0x80007414]:csrrs gp, fcsr, zero
	-[0x80007418]:sw t5, 1728(ra)
Current Store : [0x80007424] : sw gp, 1752(ra) -- Store: [0x80012db0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007454]:fsgnj.d t5, t3, s10
	-[0x80007458]:csrrs gp, fcsr, zero
	-[0x8000745c]:sw t5, 1760(ra)
Current Store : [0x80007460] : sw t6, 1768(ra) -- Store: [0x80012dc0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007454]:fsgnj.d t5, t3, s10
	-[0x80007458]:csrrs gp, fcsr, zero
	-[0x8000745c]:sw t5, 1760(ra)
Current Store : [0x80007464] : sw t5, 1776(ra) -- Store: [0x80012dc8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007454]:fsgnj.d t5, t3, s10
	-[0x80007458]:csrrs gp, fcsr, zero
	-[0x8000745c]:sw t5, 1760(ra)
Current Store : [0x80007468] : sw gp, 1784(ra) -- Store: [0x80012dd0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000749c]:fsgnj.d t5, t3, s10
	-[0x800074a0]:csrrs gp, fcsr, zero
	-[0x800074a4]:sw t5, 1792(ra)
Current Store : [0x800074a8] : sw t6, 1800(ra) -- Store: [0x80012de0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000749c]:fsgnj.d t5, t3, s10
	-[0x800074a0]:csrrs gp, fcsr, zero
	-[0x800074a4]:sw t5, 1792(ra)
Current Store : [0x800074ac] : sw t5, 1808(ra) -- Store: [0x80012de8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000749c]:fsgnj.d t5, t3, s10
	-[0x800074a0]:csrrs gp, fcsr, zero
	-[0x800074a4]:sw t5, 1792(ra)
Current Store : [0x800074b0] : sw gp, 1816(ra) -- Store: [0x80012df0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800074e4]:fsgnj.d t5, t3, s10
	-[0x800074e8]:csrrs gp, fcsr, zero
	-[0x800074ec]:sw t5, 1824(ra)
Current Store : [0x800074f0] : sw t6, 1832(ra) -- Store: [0x80012e00]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800074e4]:fsgnj.d t5, t3, s10
	-[0x800074e8]:csrrs gp, fcsr, zero
	-[0x800074ec]:sw t5, 1824(ra)
Current Store : [0x800074f4] : sw t5, 1840(ra) -- Store: [0x80012e08]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800074e4]:fsgnj.d t5, t3, s10
	-[0x800074e8]:csrrs gp, fcsr, zero
	-[0x800074ec]:sw t5, 1824(ra)
Current Store : [0x800074f8] : sw gp, 1848(ra) -- Store: [0x80012e10]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007528]:fsgnj.d t5, t3, s10
	-[0x8000752c]:csrrs gp, fcsr, zero
	-[0x80007530]:sw t5, 1856(ra)
Current Store : [0x80007534] : sw t6, 1864(ra) -- Store: [0x80012e20]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007528]:fsgnj.d t5, t3, s10
	-[0x8000752c]:csrrs gp, fcsr, zero
	-[0x80007530]:sw t5, 1856(ra)
Current Store : [0x80007538] : sw t5, 1872(ra) -- Store: [0x80012e28]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007528]:fsgnj.d t5, t3, s10
	-[0x8000752c]:csrrs gp, fcsr, zero
	-[0x80007530]:sw t5, 1856(ra)
Current Store : [0x8000753c] : sw gp, 1880(ra) -- Store: [0x80012e30]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000756c]:fsgnj.d t5, t3, s10
	-[0x80007570]:csrrs gp, fcsr, zero
	-[0x80007574]:sw t5, 1888(ra)
Current Store : [0x80007578] : sw t6, 1896(ra) -- Store: [0x80012e40]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000756c]:fsgnj.d t5, t3, s10
	-[0x80007570]:csrrs gp, fcsr, zero
	-[0x80007574]:sw t5, 1888(ra)
Current Store : [0x8000757c] : sw t5, 1904(ra) -- Store: [0x80012e48]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000756c]:fsgnj.d t5, t3, s10
	-[0x80007570]:csrrs gp, fcsr, zero
	-[0x80007574]:sw t5, 1888(ra)
Current Store : [0x80007580] : sw gp, 1912(ra) -- Store: [0x80012e50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800075b0]:fsgnj.d t5, t3, s10
	-[0x800075b4]:csrrs gp, fcsr, zero
	-[0x800075b8]:sw t5, 1920(ra)
Current Store : [0x800075bc] : sw t6, 1928(ra) -- Store: [0x80012e60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800075b0]:fsgnj.d t5, t3, s10
	-[0x800075b4]:csrrs gp, fcsr, zero
	-[0x800075b8]:sw t5, 1920(ra)
Current Store : [0x800075c0] : sw t5, 1936(ra) -- Store: [0x80012e68]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800075b0]:fsgnj.d t5, t3, s10
	-[0x800075b4]:csrrs gp, fcsr, zero
	-[0x800075b8]:sw t5, 1920(ra)
Current Store : [0x800075c4] : sw gp, 1944(ra) -- Store: [0x80012e70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800075f4]:fsgnj.d t5, t3, s10
	-[0x800075f8]:csrrs gp, fcsr, zero
	-[0x800075fc]:sw t5, 1952(ra)
Current Store : [0x80007600] : sw t6, 1960(ra) -- Store: [0x80012e80]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800075f4]:fsgnj.d t5, t3, s10
	-[0x800075f8]:csrrs gp, fcsr, zero
	-[0x800075fc]:sw t5, 1952(ra)
Current Store : [0x80007604] : sw t5, 1968(ra) -- Store: [0x80012e88]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800075f4]:fsgnj.d t5, t3, s10
	-[0x800075f8]:csrrs gp, fcsr, zero
	-[0x800075fc]:sw t5, 1952(ra)
Current Store : [0x80007608] : sw gp, 1976(ra) -- Store: [0x80012e90]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007638]:fsgnj.d t5, t3, s10
	-[0x8000763c]:csrrs gp, fcsr, zero
	-[0x80007640]:sw t5, 1984(ra)
Current Store : [0x80007644] : sw t6, 1992(ra) -- Store: [0x80012ea0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007638]:fsgnj.d t5, t3, s10
	-[0x8000763c]:csrrs gp, fcsr, zero
	-[0x80007640]:sw t5, 1984(ra)
Current Store : [0x80007648] : sw t5, 2000(ra) -- Store: [0x80012ea8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007638]:fsgnj.d t5, t3, s10
	-[0x8000763c]:csrrs gp, fcsr, zero
	-[0x80007640]:sw t5, 1984(ra)
Current Store : [0x8000764c] : sw gp, 2008(ra) -- Store: [0x80012eb0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000767c]:fsgnj.d t5, t3, s10
	-[0x80007680]:csrrs gp, fcsr, zero
	-[0x80007684]:sw t5, 2016(ra)
Current Store : [0x80007688] : sw t6, 2024(ra) -- Store: [0x80012ec0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000767c]:fsgnj.d t5, t3, s10
	-[0x80007680]:csrrs gp, fcsr, zero
	-[0x80007684]:sw t5, 2016(ra)
Current Store : [0x8000768c] : sw t5, 2032(ra) -- Store: [0x80012ec8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000767c]:fsgnj.d t5, t3, s10
	-[0x80007680]:csrrs gp, fcsr, zero
	-[0x80007684]:sw t5, 2016(ra)
Current Store : [0x80007690] : sw gp, 2040(ra) -- Store: [0x80012ed0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800076c0]:fsgnj.d t5, t3, s10
	-[0x800076c4]:csrrs gp, fcsr, zero
	-[0x800076c8]:addi ra, ra, 2040
	-[0x800076cc]:sw t5, 8(ra)
Current Store : [0x800076d0] : sw t6, 16(ra) -- Store: [0x80012ee0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800076c0]:fsgnj.d t5, t3, s10
	-[0x800076c4]:csrrs gp, fcsr, zero
	-[0x800076c8]:addi ra, ra, 2040
	-[0x800076cc]:sw t5, 8(ra)
Current Store : [0x800076d4] : sw t5, 24(ra) -- Store: [0x80012ee8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800076c0]:fsgnj.d t5, t3, s10
	-[0x800076c4]:csrrs gp, fcsr, zero
	-[0x800076c8]:addi ra, ra, 2040
	-[0x800076cc]:sw t5, 8(ra)
Current Store : [0x800076d8] : sw gp, 32(ra) -- Store: [0x80012ef0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007708]:fsgnj.d t5, t3, s10
	-[0x8000770c]:csrrs gp, fcsr, zero
	-[0x80007710]:sw t5, 40(ra)
Current Store : [0x80007714] : sw t6, 48(ra) -- Store: [0x80012f00]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007708]:fsgnj.d t5, t3, s10
	-[0x8000770c]:csrrs gp, fcsr, zero
	-[0x80007710]:sw t5, 40(ra)
Current Store : [0x80007718] : sw t5, 56(ra) -- Store: [0x80012f08]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007708]:fsgnj.d t5, t3, s10
	-[0x8000770c]:csrrs gp, fcsr, zero
	-[0x80007710]:sw t5, 40(ra)
Current Store : [0x8000771c] : sw gp, 64(ra) -- Store: [0x80012f10]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000774c]:fsgnj.d t5, t3, s10
	-[0x80007750]:csrrs gp, fcsr, zero
	-[0x80007754]:sw t5, 72(ra)
Current Store : [0x80007758] : sw t6, 80(ra) -- Store: [0x80012f20]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000774c]:fsgnj.d t5, t3, s10
	-[0x80007750]:csrrs gp, fcsr, zero
	-[0x80007754]:sw t5, 72(ra)
Current Store : [0x8000775c] : sw t5, 88(ra) -- Store: [0x80012f28]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000774c]:fsgnj.d t5, t3, s10
	-[0x80007750]:csrrs gp, fcsr, zero
	-[0x80007754]:sw t5, 72(ra)
Current Store : [0x80007760] : sw gp, 96(ra) -- Store: [0x80012f30]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007790]:fsgnj.d t5, t3, s10
	-[0x80007794]:csrrs gp, fcsr, zero
	-[0x80007798]:sw t5, 104(ra)
Current Store : [0x8000779c] : sw t6, 112(ra) -- Store: [0x80012f40]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007790]:fsgnj.d t5, t3, s10
	-[0x80007794]:csrrs gp, fcsr, zero
	-[0x80007798]:sw t5, 104(ra)
Current Store : [0x800077a0] : sw t5, 120(ra) -- Store: [0x80012f48]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007790]:fsgnj.d t5, t3, s10
	-[0x80007794]:csrrs gp, fcsr, zero
	-[0x80007798]:sw t5, 104(ra)
Current Store : [0x800077a4] : sw gp, 128(ra) -- Store: [0x80012f50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800077d0]:fsgnj.d t5, t3, s10
	-[0x800077d4]:csrrs gp, fcsr, zero
	-[0x800077d8]:sw t5, 136(ra)
Current Store : [0x800077dc] : sw t6, 144(ra) -- Store: [0x80012f60]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800077d0]:fsgnj.d t5, t3, s10
	-[0x800077d4]:csrrs gp, fcsr, zero
	-[0x800077d8]:sw t5, 136(ra)
Current Store : [0x800077e0] : sw t5, 152(ra) -- Store: [0x80012f68]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800077d0]:fsgnj.d t5, t3, s10
	-[0x800077d4]:csrrs gp, fcsr, zero
	-[0x800077d8]:sw t5, 136(ra)
Current Store : [0x800077e4] : sw gp, 160(ra) -- Store: [0x80012f70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007810]:fsgnj.d t5, t3, s10
	-[0x80007814]:csrrs gp, fcsr, zero
	-[0x80007818]:sw t5, 168(ra)
Current Store : [0x8000781c] : sw t6, 176(ra) -- Store: [0x80012f80]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007810]:fsgnj.d t5, t3, s10
	-[0x80007814]:csrrs gp, fcsr, zero
	-[0x80007818]:sw t5, 168(ra)
Current Store : [0x80007820] : sw t5, 184(ra) -- Store: [0x80012f88]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007810]:fsgnj.d t5, t3, s10
	-[0x80007814]:csrrs gp, fcsr, zero
	-[0x80007818]:sw t5, 168(ra)
Current Store : [0x80007824] : sw gp, 192(ra) -- Store: [0x80012f90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007850]:fsgnj.d t5, t3, s10
	-[0x80007854]:csrrs gp, fcsr, zero
	-[0x80007858]:sw t5, 200(ra)
Current Store : [0x8000785c] : sw t6, 208(ra) -- Store: [0x80012fa0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007850]:fsgnj.d t5, t3, s10
	-[0x80007854]:csrrs gp, fcsr, zero
	-[0x80007858]:sw t5, 200(ra)
Current Store : [0x80007860] : sw t5, 216(ra) -- Store: [0x80012fa8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007850]:fsgnj.d t5, t3, s10
	-[0x80007854]:csrrs gp, fcsr, zero
	-[0x80007858]:sw t5, 200(ra)
Current Store : [0x80007864] : sw gp, 224(ra) -- Store: [0x80012fb0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007890]:fsgnj.d t5, t3, s10
	-[0x80007894]:csrrs gp, fcsr, zero
	-[0x80007898]:sw t5, 232(ra)
Current Store : [0x8000789c] : sw t6, 240(ra) -- Store: [0x80012fc0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007890]:fsgnj.d t5, t3, s10
	-[0x80007894]:csrrs gp, fcsr, zero
	-[0x80007898]:sw t5, 232(ra)
Current Store : [0x800078a0] : sw t5, 248(ra) -- Store: [0x80012fc8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007890]:fsgnj.d t5, t3, s10
	-[0x80007894]:csrrs gp, fcsr, zero
	-[0x80007898]:sw t5, 232(ra)
Current Store : [0x800078a4] : sw gp, 256(ra) -- Store: [0x80012fd0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800078d0]:fsgnj.d t5, t3, s10
	-[0x800078d4]:csrrs gp, fcsr, zero
	-[0x800078d8]:sw t5, 264(ra)
Current Store : [0x800078dc] : sw t6, 272(ra) -- Store: [0x80012fe0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800078d0]:fsgnj.d t5, t3, s10
	-[0x800078d4]:csrrs gp, fcsr, zero
	-[0x800078d8]:sw t5, 264(ra)
Current Store : [0x800078e0] : sw t5, 280(ra) -- Store: [0x80012fe8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800078d0]:fsgnj.d t5, t3, s10
	-[0x800078d4]:csrrs gp, fcsr, zero
	-[0x800078d8]:sw t5, 264(ra)
Current Store : [0x800078e4] : sw gp, 288(ra) -- Store: [0x80012ff0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007910]:fsgnj.d t5, t3, s10
	-[0x80007914]:csrrs gp, fcsr, zero
	-[0x80007918]:sw t5, 296(ra)
Current Store : [0x8000791c] : sw t6, 304(ra) -- Store: [0x80013000]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007910]:fsgnj.d t5, t3, s10
	-[0x80007914]:csrrs gp, fcsr, zero
	-[0x80007918]:sw t5, 296(ra)
Current Store : [0x80007920] : sw t5, 312(ra) -- Store: [0x80013008]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007910]:fsgnj.d t5, t3, s10
	-[0x80007914]:csrrs gp, fcsr, zero
	-[0x80007918]:sw t5, 296(ra)
Current Store : [0x80007924] : sw gp, 320(ra) -- Store: [0x80013010]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007954]:fsgnj.d t5, t3, s10
	-[0x80007958]:csrrs gp, fcsr, zero
	-[0x8000795c]:sw t5, 328(ra)
Current Store : [0x80007960] : sw t6, 336(ra) -- Store: [0x80013020]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007954]:fsgnj.d t5, t3, s10
	-[0x80007958]:csrrs gp, fcsr, zero
	-[0x8000795c]:sw t5, 328(ra)
Current Store : [0x80007964] : sw t5, 344(ra) -- Store: [0x80013028]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007954]:fsgnj.d t5, t3, s10
	-[0x80007958]:csrrs gp, fcsr, zero
	-[0x8000795c]:sw t5, 328(ra)
Current Store : [0x80007968] : sw gp, 352(ra) -- Store: [0x80013030]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007998]:fsgnj.d t5, t3, s10
	-[0x8000799c]:csrrs gp, fcsr, zero
	-[0x800079a0]:sw t5, 360(ra)
Current Store : [0x800079a4] : sw t6, 368(ra) -- Store: [0x80013040]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007998]:fsgnj.d t5, t3, s10
	-[0x8000799c]:csrrs gp, fcsr, zero
	-[0x800079a0]:sw t5, 360(ra)
Current Store : [0x800079a8] : sw t5, 376(ra) -- Store: [0x80013048]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007998]:fsgnj.d t5, t3, s10
	-[0x8000799c]:csrrs gp, fcsr, zero
	-[0x800079a0]:sw t5, 360(ra)
Current Store : [0x800079ac] : sw gp, 384(ra) -- Store: [0x80013050]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800079d8]:fsgnj.d t5, t3, s10
	-[0x800079dc]:csrrs gp, fcsr, zero
	-[0x800079e0]:sw t5, 392(ra)
Current Store : [0x800079e4] : sw t6, 400(ra) -- Store: [0x80013060]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800079d8]:fsgnj.d t5, t3, s10
	-[0x800079dc]:csrrs gp, fcsr, zero
	-[0x800079e0]:sw t5, 392(ra)
Current Store : [0x800079e8] : sw t5, 408(ra) -- Store: [0x80013068]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800079d8]:fsgnj.d t5, t3, s10
	-[0x800079dc]:csrrs gp, fcsr, zero
	-[0x800079e0]:sw t5, 392(ra)
Current Store : [0x800079ec] : sw gp, 416(ra) -- Store: [0x80013070]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007a18]:fsgnj.d t5, t3, s10
	-[0x80007a1c]:csrrs gp, fcsr, zero
	-[0x80007a20]:sw t5, 424(ra)
Current Store : [0x80007a24] : sw t6, 432(ra) -- Store: [0x80013080]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007a18]:fsgnj.d t5, t3, s10
	-[0x80007a1c]:csrrs gp, fcsr, zero
	-[0x80007a20]:sw t5, 424(ra)
Current Store : [0x80007a28] : sw t5, 440(ra) -- Store: [0x80013088]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007a18]:fsgnj.d t5, t3, s10
	-[0x80007a1c]:csrrs gp, fcsr, zero
	-[0x80007a20]:sw t5, 424(ra)
Current Store : [0x80007a2c] : sw gp, 448(ra) -- Store: [0x80013090]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007a58]:fsgnj.d t5, t3, s10
	-[0x80007a5c]:csrrs gp, fcsr, zero
	-[0x80007a60]:sw t5, 456(ra)
Current Store : [0x80007a64] : sw t6, 464(ra) -- Store: [0x800130a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007a58]:fsgnj.d t5, t3, s10
	-[0x80007a5c]:csrrs gp, fcsr, zero
	-[0x80007a60]:sw t5, 456(ra)
Current Store : [0x80007a68] : sw t5, 472(ra) -- Store: [0x800130a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007a58]:fsgnj.d t5, t3, s10
	-[0x80007a5c]:csrrs gp, fcsr, zero
	-[0x80007a60]:sw t5, 456(ra)
Current Store : [0x80007a6c] : sw gp, 480(ra) -- Store: [0x800130b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007a98]:fsgnj.d t5, t3, s10
	-[0x80007a9c]:csrrs gp, fcsr, zero
	-[0x80007aa0]:sw t5, 488(ra)
Current Store : [0x80007aa4] : sw t6, 496(ra) -- Store: [0x800130c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007a98]:fsgnj.d t5, t3, s10
	-[0x80007a9c]:csrrs gp, fcsr, zero
	-[0x80007aa0]:sw t5, 488(ra)
Current Store : [0x80007aa8] : sw t5, 504(ra) -- Store: [0x800130c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007a98]:fsgnj.d t5, t3, s10
	-[0x80007a9c]:csrrs gp, fcsr, zero
	-[0x80007aa0]:sw t5, 488(ra)
Current Store : [0x80007aac] : sw gp, 512(ra) -- Store: [0x800130d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007adc]:fsgnj.d t5, t3, s10
	-[0x80007ae0]:csrrs gp, fcsr, zero
	-[0x80007ae4]:sw t5, 520(ra)
Current Store : [0x80007ae8] : sw t6, 528(ra) -- Store: [0x800130e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007adc]:fsgnj.d t5, t3, s10
	-[0x80007ae0]:csrrs gp, fcsr, zero
	-[0x80007ae4]:sw t5, 520(ra)
Current Store : [0x80007aec] : sw t5, 536(ra) -- Store: [0x800130e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007adc]:fsgnj.d t5, t3, s10
	-[0x80007ae0]:csrrs gp, fcsr, zero
	-[0x80007ae4]:sw t5, 520(ra)
Current Store : [0x80007af0] : sw gp, 544(ra) -- Store: [0x800130f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007b20]:fsgnj.d t5, t3, s10
	-[0x80007b24]:csrrs gp, fcsr, zero
	-[0x80007b28]:sw t5, 552(ra)
Current Store : [0x80007b2c] : sw t6, 560(ra) -- Store: [0x80013100]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007b20]:fsgnj.d t5, t3, s10
	-[0x80007b24]:csrrs gp, fcsr, zero
	-[0x80007b28]:sw t5, 552(ra)
Current Store : [0x80007b30] : sw t5, 568(ra) -- Store: [0x80013108]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007b20]:fsgnj.d t5, t3, s10
	-[0x80007b24]:csrrs gp, fcsr, zero
	-[0x80007b28]:sw t5, 552(ra)
Current Store : [0x80007b34] : sw gp, 576(ra) -- Store: [0x80013110]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007b60]:fsgnj.d t5, t3, s10
	-[0x80007b64]:csrrs gp, fcsr, zero
	-[0x80007b68]:sw t5, 584(ra)
Current Store : [0x80007b6c] : sw t6, 592(ra) -- Store: [0x80013120]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007b60]:fsgnj.d t5, t3, s10
	-[0x80007b64]:csrrs gp, fcsr, zero
	-[0x80007b68]:sw t5, 584(ra)
Current Store : [0x80007b70] : sw t5, 600(ra) -- Store: [0x80013128]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007b60]:fsgnj.d t5, t3, s10
	-[0x80007b64]:csrrs gp, fcsr, zero
	-[0x80007b68]:sw t5, 584(ra)
Current Store : [0x80007b74] : sw gp, 608(ra) -- Store: [0x80013130]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007ba0]:fsgnj.d t5, t3, s10
	-[0x80007ba4]:csrrs gp, fcsr, zero
	-[0x80007ba8]:sw t5, 616(ra)
Current Store : [0x80007bac] : sw t6, 624(ra) -- Store: [0x80013140]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007ba0]:fsgnj.d t5, t3, s10
	-[0x80007ba4]:csrrs gp, fcsr, zero
	-[0x80007ba8]:sw t5, 616(ra)
Current Store : [0x80007bb0] : sw t5, 632(ra) -- Store: [0x80013148]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007ba0]:fsgnj.d t5, t3, s10
	-[0x80007ba4]:csrrs gp, fcsr, zero
	-[0x80007ba8]:sw t5, 616(ra)
Current Store : [0x80007bb4] : sw gp, 640(ra) -- Store: [0x80013150]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007be0]:fsgnj.d t5, t3, s10
	-[0x80007be4]:csrrs gp, fcsr, zero
	-[0x80007be8]:sw t5, 648(ra)
Current Store : [0x80007bec] : sw t6, 656(ra) -- Store: [0x80013160]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007be0]:fsgnj.d t5, t3, s10
	-[0x80007be4]:csrrs gp, fcsr, zero
	-[0x80007be8]:sw t5, 648(ra)
Current Store : [0x80007bf0] : sw t5, 664(ra) -- Store: [0x80013168]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007be0]:fsgnj.d t5, t3, s10
	-[0x80007be4]:csrrs gp, fcsr, zero
	-[0x80007be8]:sw t5, 648(ra)
Current Store : [0x80007bf4] : sw gp, 672(ra) -- Store: [0x80013170]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007c20]:fsgnj.d t5, t3, s10
	-[0x80007c24]:csrrs gp, fcsr, zero
	-[0x80007c28]:sw t5, 680(ra)
Current Store : [0x80007c2c] : sw t6, 688(ra) -- Store: [0x80013180]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007c20]:fsgnj.d t5, t3, s10
	-[0x80007c24]:csrrs gp, fcsr, zero
	-[0x80007c28]:sw t5, 680(ra)
Current Store : [0x80007c30] : sw t5, 696(ra) -- Store: [0x80013188]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007c20]:fsgnj.d t5, t3, s10
	-[0x80007c24]:csrrs gp, fcsr, zero
	-[0x80007c28]:sw t5, 680(ra)
Current Store : [0x80007c34] : sw gp, 704(ra) -- Store: [0x80013190]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007c60]:fsgnj.d t5, t3, s10
	-[0x80007c64]:csrrs gp, fcsr, zero
	-[0x80007c68]:sw t5, 712(ra)
Current Store : [0x80007c6c] : sw t6, 720(ra) -- Store: [0x800131a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007c60]:fsgnj.d t5, t3, s10
	-[0x80007c64]:csrrs gp, fcsr, zero
	-[0x80007c68]:sw t5, 712(ra)
Current Store : [0x80007c70] : sw t5, 728(ra) -- Store: [0x800131a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007c60]:fsgnj.d t5, t3, s10
	-[0x80007c64]:csrrs gp, fcsr, zero
	-[0x80007c68]:sw t5, 712(ra)
Current Store : [0x80007c74] : sw gp, 736(ra) -- Store: [0x800131b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007ca0]:fsgnj.d t5, t3, s10
	-[0x80007ca4]:csrrs gp, fcsr, zero
	-[0x80007ca8]:sw t5, 744(ra)
Current Store : [0x80007cac] : sw t6, 752(ra) -- Store: [0x800131c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007ca0]:fsgnj.d t5, t3, s10
	-[0x80007ca4]:csrrs gp, fcsr, zero
	-[0x80007ca8]:sw t5, 744(ra)
Current Store : [0x80007cb0] : sw t5, 760(ra) -- Store: [0x800131c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007ca0]:fsgnj.d t5, t3, s10
	-[0x80007ca4]:csrrs gp, fcsr, zero
	-[0x80007ca8]:sw t5, 744(ra)
Current Store : [0x80007cb4] : sw gp, 768(ra) -- Store: [0x800131d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007ce0]:fsgnj.d t5, t3, s10
	-[0x80007ce4]:csrrs gp, fcsr, zero
	-[0x80007ce8]:sw t5, 776(ra)
Current Store : [0x80007cec] : sw t6, 784(ra) -- Store: [0x800131e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007ce0]:fsgnj.d t5, t3, s10
	-[0x80007ce4]:csrrs gp, fcsr, zero
	-[0x80007ce8]:sw t5, 776(ra)
Current Store : [0x80007cf0] : sw t5, 792(ra) -- Store: [0x800131e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007ce0]:fsgnj.d t5, t3, s10
	-[0x80007ce4]:csrrs gp, fcsr, zero
	-[0x80007ce8]:sw t5, 776(ra)
Current Store : [0x80007cf4] : sw gp, 800(ra) -- Store: [0x800131f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007d20]:fsgnj.d t5, t3, s10
	-[0x80007d24]:csrrs gp, fcsr, zero
	-[0x80007d28]:sw t5, 808(ra)
Current Store : [0x80007d2c] : sw t6, 816(ra) -- Store: [0x80013200]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007d20]:fsgnj.d t5, t3, s10
	-[0x80007d24]:csrrs gp, fcsr, zero
	-[0x80007d28]:sw t5, 808(ra)
Current Store : [0x80007d30] : sw t5, 824(ra) -- Store: [0x80013208]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007d20]:fsgnj.d t5, t3, s10
	-[0x80007d24]:csrrs gp, fcsr, zero
	-[0x80007d28]:sw t5, 808(ra)
Current Store : [0x80007d34] : sw gp, 832(ra) -- Store: [0x80013210]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007d60]:fsgnj.d t5, t3, s10
	-[0x80007d64]:csrrs gp, fcsr, zero
	-[0x80007d68]:sw t5, 840(ra)
Current Store : [0x80007d6c] : sw t6, 848(ra) -- Store: [0x80013220]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007d60]:fsgnj.d t5, t3, s10
	-[0x80007d64]:csrrs gp, fcsr, zero
	-[0x80007d68]:sw t5, 840(ra)
Current Store : [0x80007d70] : sw t5, 856(ra) -- Store: [0x80013228]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007d60]:fsgnj.d t5, t3, s10
	-[0x80007d64]:csrrs gp, fcsr, zero
	-[0x80007d68]:sw t5, 840(ra)
Current Store : [0x80007d74] : sw gp, 864(ra) -- Store: [0x80013230]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007da0]:fsgnj.d t5, t3, s10
	-[0x80007da4]:csrrs gp, fcsr, zero
	-[0x80007da8]:sw t5, 872(ra)
Current Store : [0x80007dac] : sw t6, 880(ra) -- Store: [0x80013240]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007da0]:fsgnj.d t5, t3, s10
	-[0x80007da4]:csrrs gp, fcsr, zero
	-[0x80007da8]:sw t5, 872(ra)
Current Store : [0x80007db0] : sw t5, 888(ra) -- Store: [0x80013248]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007da0]:fsgnj.d t5, t3, s10
	-[0x80007da4]:csrrs gp, fcsr, zero
	-[0x80007da8]:sw t5, 872(ra)
Current Store : [0x80007db4] : sw gp, 896(ra) -- Store: [0x80013250]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007de0]:fsgnj.d t5, t3, s10
	-[0x80007de4]:csrrs gp, fcsr, zero
	-[0x80007de8]:sw t5, 904(ra)
Current Store : [0x80007dec] : sw t6, 912(ra) -- Store: [0x80013260]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007de0]:fsgnj.d t5, t3, s10
	-[0x80007de4]:csrrs gp, fcsr, zero
	-[0x80007de8]:sw t5, 904(ra)
Current Store : [0x80007df0] : sw t5, 920(ra) -- Store: [0x80013268]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007de0]:fsgnj.d t5, t3, s10
	-[0x80007de4]:csrrs gp, fcsr, zero
	-[0x80007de8]:sw t5, 904(ra)
Current Store : [0x80007df4] : sw gp, 928(ra) -- Store: [0x80013270]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007e20]:fsgnj.d t5, t3, s10
	-[0x80007e24]:csrrs gp, fcsr, zero
	-[0x80007e28]:sw t5, 936(ra)
Current Store : [0x80007e2c] : sw t6, 944(ra) -- Store: [0x80013280]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007e20]:fsgnj.d t5, t3, s10
	-[0x80007e24]:csrrs gp, fcsr, zero
	-[0x80007e28]:sw t5, 936(ra)
Current Store : [0x80007e30] : sw t5, 952(ra) -- Store: [0x80013288]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007e20]:fsgnj.d t5, t3, s10
	-[0x80007e24]:csrrs gp, fcsr, zero
	-[0x80007e28]:sw t5, 936(ra)
Current Store : [0x80007e34] : sw gp, 960(ra) -- Store: [0x80013290]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007e60]:fsgnj.d t5, t3, s10
	-[0x80007e64]:csrrs gp, fcsr, zero
	-[0x80007e68]:sw t5, 968(ra)
Current Store : [0x80007e6c] : sw t6, 976(ra) -- Store: [0x800132a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007e60]:fsgnj.d t5, t3, s10
	-[0x80007e64]:csrrs gp, fcsr, zero
	-[0x80007e68]:sw t5, 968(ra)
Current Store : [0x80007e70] : sw t5, 984(ra) -- Store: [0x800132a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007e60]:fsgnj.d t5, t3, s10
	-[0x80007e64]:csrrs gp, fcsr, zero
	-[0x80007e68]:sw t5, 968(ra)
Current Store : [0x80007e74] : sw gp, 992(ra) -- Store: [0x800132b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007ea0]:fsgnj.d t5, t3, s10
	-[0x80007ea4]:csrrs gp, fcsr, zero
	-[0x80007ea8]:sw t5, 1000(ra)
Current Store : [0x80007eac] : sw t6, 1008(ra) -- Store: [0x800132c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007ea0]:fsgnj.d t5, t3, s10
	-[0x80007ea4]:csrrs gp, fcsr, zero
	-[0x80007ea8]:sw t5, 1000(ra)
Current Store : [0x80007eb0] : sw t5, 1016(ra) -- Store: [0x800132c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007ea0]:fsgnj.d t5, t3, s10
	-[0x80007ea4]:csrrs gp, fcsr, zero
	-[0x80007ea8]:sw t5, 1000(ra)
Current Store : [0x80007eb4] : sw gp, 1024(ra) -- Store: [0x800132d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007ee0]:fsgnj.d t5, t3, s10
	-[0x80007ee4]:csrrs gp, fcsr, zero
	-[0x80007ee8]:sw t5, 1032(ra)
Current Store : [0x80007eec] : sw t6, 1040(ra) -- Store: [0x800132e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007ee0]:fsgnj.d t5, t3, s10
	-[0x80007ee4]:csrrs gp, fcsr, zero
	-[0x80007ee8]:sw t5, 1032(ra)
Current Store : [0x80007ef0] : sw t5, 1048(ra) -- Store: [0x800132e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007ee0]:fsgnj.d t5, t3, s10
	-[0x80007ee4]:csrrs gp, fcsr, zero
	-[0x80007ee8]:sw t5, 1032(ra)
Current Store : [0x80007ef4] : sw gp, 1056(ra) -- Store: [0x800132f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007f20]:fsgnj.d t5, t3, s10
	-[0x80007f24]:csrrs gp, fcsr, zero
	-[0x80007f28]:sw t5, 1064(ra)
Current Store : [0x80007f2c] : sw t6, 1072(ra) -- Store: [0x80013300]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007f20]:fsgnj.d t5, t3, s10
	-[0x80007f24]:csrrs gp, fcsr, zero
	-[0x80007f28]:sw t5, 1064(ra)
Current Store : [0x80007f30] : sw t5, 1080(ra) -- Store: [0x80013308]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007f20]:fsgnj.d t5, t3, s10
	-[0x80007f24]:csrrs gp, fcsr, zero
	-[0x80007f28]:sw t5, 1064(ra)
Current Store : [0x80007f34] : sw gp, 1088(ra) -- Store: [0x80013310]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007f64]:fsgnj.d t5, t3, s10
	-[0x80007f68]:csrrs gp, fcsr, zero
	-[0x80007f6c]:sw t5, 1096(ra)
Current Store : [0x80007f70] : sw t6, 1104(ra) -- Store: [0x80013320]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007f64]:fsgnj.d t5, t3, s10
	-[0x80007f68]:csrrs gp, fcsr, zero
	-[0x80007f6c]:sw t5, 1096(ra)
Current Store : [0x80007f74] : sw t5, 1112(ra) -- Store: [0x80013328]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007f64]:fsgnj.d t5, t3, s10
	-[0x80007f68]:csrrs gp, fcsr, zero
	-[0x80007f6c]:sw t5, 1096(ra)
Current Store : [0x80007f78] : sw gp, 1120(ra) -- Store: [0x80013330]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007fa8]:fsgnj.d t5, t3, s10
	-[0x80007fac]:csrrs gp, fcsr, zero
	-[0x80007fb0]:sw t5, 1128(ra)
Current Store : [0x80007fb4] : sw t6, 1136(ra) -- Store: [0x80013340]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007fa8]:fsgnj.d t5, t3, s10
	-[0x80007fac]:csrrs gp, fcsr, zero
	-[0x80007fb0]:sw t5, 1128(ra)
Current Store : [0x80007fb8] : sw t5, 1144(ra) -- Store: [0x80013348]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007fa8]:fsgnj.d t5, t3, s10
	-[0x80007fac]:csrrs gp, fcsr, zero
	-[0x80007fb0]:sw t5, 1128(ra)
Current Store : [0x80007fbc] : sw gp, 1152(ra) -- Store: [0x80013350]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007fe8]:fsgnj.d t5, t3, s10
	-[0x80007fec]:csrrs gp, fcsr, zero
	-[0x80007ff0]:sw t5, 1160(ra)
Current Store : [0x80007ff4] : sw t6, 1168(ra) -- Store: [0x80013360]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007fe8]:fsgnj.d t5, t3, s10
	-[0x80007fec]:csrrs gp, fcsr, zero
	-[0x80007ff0]:sw t5, 1160(ra)
Current Store : [0x80007ff8] : sw t5, 1176(ra) -- Store: [0x80013368]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80007fe8]:fsgnj.d t5, t3, s10
	-[0x80007fec]:csrrs gp, fcsr, zero
	-[0x80007ff0]:sw t5, 1160(ra)
Current Store : [0x80007ffc] : sw gp, 1184(ra) -- Store: [0x80013370]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008028]:fsgnj.d t5, t3, s10
	-[0x8000802c]:csrrs gp, fcsr, zero
	-[0x80008030]:sw t5, 1192(ra)
Current Store : [0x80008034] : sw t6, 1200(ra) -- Store: [0x80013380]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008028]:fsgnj.d t5, t3, s10
	-[0x8000802c]:csrrs gp, fcsr, zero
	-[0x80008030]:sw t5, 1192(ra)
Current Store : [0x80008038] : sw t5, 1208(ra) -- Store: [0x80013388]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008028]:fsgnj.d t5, t3, s10
	-[0x8000802c]:csrrs gp, fcsr, zero
	-[0x80008030]:sw t5, 1192(ra)
Current Store : [0x8000803c] : sw gp, 1216(ra) -- Store: [0x80013390]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008068]:fsgnj.d t5, t3, s10
	-[0x8000806c]:csrrs gp, fcsr, zero
	-[0x80008070]:sw t5, 1224(ra)
Current Store : [0x80008074] : sw t6, 1232(ra) -- Store: [0x800133a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008068]:fsgnj.d t5, t3, s10
	-[0x8000806c]:csrrs gp, fcsr, zero
	-[0x80008070]:sw t5, 1224(ra)
Current Store : [0x80008078] : sw t5, 1240(ra) -- Store: [0x800133a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008068]:fsgnj.d t5, t3, s10
	-[0x8000806c]:csrrs gp, fcsr, zero
	-[0x80008070]:sw t5, 1224(ra)
Current Store : [0x8000807c] : sw gp, 1248(ra) -- Store: [0x800133b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800080a8]:fsgnj.d t5, t3, s10
	-[0x800080ac]:csrrs gp, fcsr, zero
	-[0x800080b0]:sw t5, 1256(ra)
Current Store : [0x800080b4] : sw t6, 1264(ra) -- Store: [0x800133c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800080a8]:fsgnj.d t5, t3, s10
	-[0x800080ac]:csrrs gp, fcsr, zero
	-[0x800080b0]:sw t5, 1256(ra)
Current Store : [0x800080b8] : sw t5, 1272(ra) -- Store: [0x800133c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800080a8]:fsgnj.d t5, t3, s10
	-[0x800080ac]:csrrs gp, fcsr, zero
	-[0x800080b0]:sw t5, 1256(ra)
Current Store : [0x800080bc] : sw gp, 1280(ra) -- Store: [0x800133d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800080ec]:fsgnj.d t5, t3, s10
	-[0x800080f0]:csrrs gp, fcsr, zero
	-[0x800080f4]:sw t5, 1288(ra)
Current Store : [0x800080f8] : sw t6, 1296(ra) -- Store: [0x800133e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800080ec]:fsgnj.d t5, t3, s10
	-[0x800080f0]:csrrs gp, fcsr, zero
	-[0x800080f4]:sw t5, 1288(ra)
Current Store : [0x800080fc] : sw t5, 1304(ra) -- Store: [0x800133e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800080ec]:fsgnj.d t5, t3, s10
	-[0x800080f0]:csrrs gp, fcsr, zero
	-[0x800080f4]:sw t5, 1288(ra)
Current Store : [0x80008100] : sw gp, 1312(ra) -- Store: [0x800133f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008130]:fsgnj.d t5, t3, s10
	-[0x80008134]:csrrs gp, fcsr, zero
	-[0x80008138]:sw t5, 1320(ra)
Current Store : [0x8000813c] : sw t6, 1328(ra) -- Store: [0x80013400]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008130]:fsgnj.d t5, t3, s10
	-[0x80008134]:csrrs gp, fcsr, zero
	-[0x80008138]:sw t5, 1320(ra)
Current Store : [0x80008140] : sw t5, 1336(ra) -- Store: [0x80013408]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008130]:fsgnj.d t5, t3, s10
	-[0x80008134]:csrrs gp, fcsr, zero
	-[0x80008138]:sw t5, 1320(ra)
Current Store : [0x80008144] : sw gp, 1344(ra) -- Store: [0x80013410]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008170]:fsgnj.d t5, t3, s10
	-[0x80008174]:csrrs gp, fcsr, zero
	-[0x80008178]:sw t5, 1352(ra)
Current Store : [0x8000817c] : sw t6, 1360(ra) -- Store: [0x80013420]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008170]:fsgnj.d t5, t3, s10
	-[0x80008174]:csrrs gp, fcsr, zero
	-[0x80008178]:sw t5, 1352(ra)
Current Store : [0x80008180] : sw t5, 1368(ra) -- Store: [0x80013428]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008170]:fsgnj.d t5, t3, s10
	-[0x80008174]:csrrs gp, fcsr, zero
	-[0x80008178]:sw t5, 1352(ra)
Current Store : [0x80008184] : sw gp, 1376(ra) -- Store: [0x80013430]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800081b0]:fsgnj.d t5, t3, s10
	-[0x800081b4]:csrrs gp, fcsr, zero
	-[0x800081b8]:sw t5, 1384(ra)
Current Store : [0x800081bc] : sw t6, 1392(ra) -- Store: [0x80013440]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800081b0]:fsgnj.d t5, t3, s10
	-[0x800081b4]:csrrs gp, fcsr, zero
	-[0x800081b8]:sw t5, 1384(ra)
Current Store : [0x800081c0] : sw t5, 1400(ra) -- Store: [0x80013448]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800081b0]:fsgnj.d t5, t3, s10
	-[0x800081b4]:csrrs gp, fcsr, zero
	-[0x800081b8]:sw t5, 1384(ra)
Current Store : [0x800081c4] : sw gp, 1408(ra) -- Store: [0x80013450]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800081f0]:fsgnj.d t5, t3, s10
	-[0x800081f4]:csrrs gp, fcsr, zero
	-[0x800081f8]:sw t5, 1416(ra)
Current Store : [0x800081fc] : sw t6, 1424(ra) -- Store: [0x80013460]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800081f0]:fsgnj.d t5, t3, s10
	-[0x800081f4]:csrrs gp, fcsr, zero
	-[0x800081f8]:sw t5, 1416(ra)
Current Store : [0x80008200] : sw t5, 1432(ra) -- Store: [0x80013468]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800081f0]:fsgnj.d t5, t3, s10
	-[0x800081f4]:csrrs gp, fcsr, zero
	-[0x800081f8]:sw t5, 1416(ra)
Current Store : [0x80008204] : sw gp, 1440(ra) -- Store: [0x80013470]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008230]:fsgnj.d t5, t3, s10
	-[0x80008234]:csrrs gp, fcsr, zero
	-[0x80008238]:sw t5, 1448(ra)
Current Store : [0x8000823c] : sw t6, 1456(ra) -- Store: [0x80013480]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008230]:fsgnj.d t5, t3, s10
	-[0x80008234]:csrrs gp, fcsr, zero
	-[0x80008238]:sw t5, 1448(ra)
Current Store : [0x80008240] : sw t5, 1464(ra) -- Store: [0x80013488]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008230]:fsgnj.d t5, t3, s10
	-[0x80008234]:csrrs gp, fcsr, zero
	-[0x80008238]:sw t5, 1448(ra)
Current Store : [0x80008244] : sw gp, 1472(ra) -- Store: [0x80013490]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008270]:fsgnj.d t5, t3, s10
	-[0x80008274]:csrrs gp, fcsr, zero
	-[0x80008278]:sw t5, 1480(ra)
Current Store : [0x8000827c] : sw t6, 1488(ra) -- Store: [0x800134a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008270]:fsgnj.d t5, t3, s10
	-[0x80008274]:csrrs gp, fcsr, zero
	-[0x80008278]:sw t5, 1480(ra)
Current Store : [0x80008280] : sw t5, 1496(ra) -- Store: [0x800134a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008270]:fsgnj.d t5, t3, s10
	-[0x80008274]:csrrs gp, fcsr, zero
	-[0x80008278]:sw t5, 1480(ra)
Current Store : [0x80008284] : sw gp, 1504(ra) -- Store: [0x800134b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800082b0]:fsgnj.d t5, t3, s10
	-[0x800082b4]:csrrs gp, fcsr, zero
	-[0x800082b8]:sw t5, 1512(ra)
Current Store : [0x800082bc] : sw t6, 1520(ra) -- Store: [0x800134c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800082b0]:fsgnj.d t5, t3, s10
	-[0x800082b4]:csrrs gp, fcsr, zero
	-[0x800082b8]:sw t5, 1512(ra)
Current Store : [0x800082c0] : sw t5, 1528(ra) -- Store: [0x800134c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800082b0]:fsgnj.d t5, t3, s10
	-[0x800082b4]:csrrs gp, fcsr, zero
	-[0x800082b8]:sw t5, 1512(ra)
Current Store : [0x800082c4] : sw gp, 1536(ra) -- Store: [0x800134d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800082f0]:fsgnj.d t5, t3, s10
	-[0x800082f4]:csrrs gp, fcsr, zero
	-[0x800082f8]:sw t5, 1544(ra)
Current Store : [0x800082fc] : sw t6, 1552(ra) -- Store: [0x800134e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800082f0]:fsgnj.d t5, t3, s10
	-[0x800082f4]:csrrs gp, fcsr, zero
	-[0x800082f8]:sw t5, 1544(ra)
Current Store : [0x80008300] : sw t5, 1560(ra) -- Store: [0x800134e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800082f0]:fsgnj.d t5, t3, s10
	-[0x800082f4]:csrrs gp, fcsr, zero
	-[0x800082f8]:sw t5, 1544(ra)
Current Store : [0x80008304] : sw gp, 1568(ra) -- Store: [0x800134f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008330]:fsgnj.d t5, t3, s10
	-[0x80008334]:csrrs gp, fcsr, zero
	-[0x80008338]:sw t5, 1576(ra)
Current Store : [0x8000833c] : sw t6, 1584(ra) -- Store: [0x80013500]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008330]:fsgnj.d t5, t3, s10
	-[0x80008334]:csrrs gp, fcsr, zero
	-[0x80008338]:sw t5, 1576(ra)
Current Store : [0x80008340] : sw t5, 1592(ra) -- Store: [0x80013508]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008330]:fsgnj.d t5, t3, s10
	-[0x80008334]:csrrs gp, fcsr, zero
	-[0x80008338]:sw t5, 1576(ra)
Current Store : [0x80008344] : sw gp, 1600(ra) -- Store: [0x80013510]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008370]:fsgnj.d t5, t3, s10
	-[0x80008374]:csrrs gp, fcsr, zero
	-[0x80008378]:sw t5, 1608(ra)
Current Store : [0x8000837c] : sw t6, 1616(ra) -- Store: [0x80013520]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008370]:fsgnj.d t5, t3, s10
	-[0x80008374]:csrrs gp, fcsr, zero
	-[0x80008378]:sw t5, 1608(ra)
Current Store : [0x80008380] : sw t5, 1624(ra) -- Store: [0x80013528]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008370]:fsgnj.d t5, t3, s10
	-[0x80008374]:csrrs gp, fcsr, zero
	-[0x80008378]:sw t5, 1608(ra)
Current Store : [0x80008384] : sw gp, 1632(ra) -- Store: [0x80013530]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800083b0]:fsgnj.d t5, t3, s10
	-[0x800083b4]:csrrs gp, fcsr, zero
	-[0x800083b8]:sw t5, 1640(ra)
Current Store : [0x800083bc] : sw t6, 1648(ra) -- Store: [0x80013540]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800083b0]:fsgnj.d t5, t3, s10
	-[0x800083b4]:csrrs gp, fcsr, zero
	-[0x800083b8]:sw t5, 1640(ra)
Current Store : [0x800083c0] : sw t5, 1656(ra) -- Store: [0x80013548]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800083b0]:fsgnj.d t5, t3, s10
	-[0x800083b4]:csrrs gp, fcsr, zero
	-[0x800083b8]:sw t5, 1640(ra)
Current Store : [0x800083c4] : sw gp, 1664(ra) -- Store: [0x80013550]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800083f0]:fsgnj.d t5, t3, s10
	-[0x800083f4]:csrrs gp, fcsr, zero
	-[0x800083f8]:sw t5, 1672(ra)
Current Store : [0x800083fc] : sw t6, 1680(ra) -- Store: [0x80013560]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800083f0]:fsgnj.d t5, t3, s10
	-[0x800083f4]:csrrs gp, fcsr, zero
	-[0x800083f8]:sw t5, 1672(ra)
Current Store : [0x80008400] : sw t5, 1688(ra) -- Store: [0x80013568]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800083f0]:fsgnj.d t5, t3, s10
	-[0x800083f4]:csrrs gp, fcsr, zero
	-[0x800083f8]:sw t5, 1672(ra)
Current Store : [0x80008404] : sw gp, 1696(ra) -- Store: [0x80013570]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008430]:fsgnj.d t5, t3, s10
	-[0x80008434]:csrrs gp, fcsr, zero
	-[0x80008438]:sw t5, 1704(ra)
Current Store : [0x8000843c] : sw t6, 1712(ra) -- Store: [0x80013580]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008430]:fsgnj.d t5, t3, s10
	-[0x80008434]:csrrs gp, fcsr, zero
	-[0x80008438]:sw t5, 1704(ra)
Current Store : [0x80008440] : sw t5, 1720(ra) -- Store: [0x80013588]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008430]:fsgnj.d t5, t3, s10
	-[0x80008434]:csrrs gp, fcsr, zero
	-[0x80008438]:sw t5, 1704(ra)
Current Store : [0x80008444] : sw gp, 1728(ra) -- Store: [0x80013590]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008470]:fsgnj.d t5, t3, s10
	-[0x80008474]:csrrs gp, fcsr, zero
	-[0x80008478]:sw t5, 1736(ra)
Current Store : [0x8000847c] : sw t6, 1744(ra) -- Store: [0x800135a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008470]:fsgnj.d t5, t3, s10
	-[0x80008474]:csrrs gp, fcsr, zero
	-[0x80008478]:sw t5, 1736(ra)
Current Store : [0x80008480] : sw t5, 1752(ra) -- Store: [0x800135a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008470]:fsgnj.d t5, t3, s10
	-[0x80008474]:csrrs gp, fcsr, zero
	-[0x80008478]:sw t5, 1736(ra)
Current Store : [0x80008484] : sw gp, 1760(ra) -- Store: [0x800135b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800084b0]:fsgnj.d t5, t3, s10
	-[0x800084b4]:csrrs gp, fcsr, zero
	-[0x800084b8]:sw t5, 1768(ra)
Current Store : [0x800084bc] : sw t6, 1776(ra) -- Store: [0x800135c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800084b0]:fsgnj.d t5, t3, s10
	-[0x800084b4]:csrrs gp, fcsr, zero
	-[0x800084b8]:sw t5, 1768(ra)
Current Store : [0x800084c0] : sw t5, 1784(ra) -- Store: [0x800135c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800084b0]:fsgnj.d t5, t3, s10
	-[0x800084b4]:csrrs gp, fcsr, zero
	-[0x800084b8]:sw t5, 1768(ra)
Current Store : [0x800084c4] : sw gp, 1792(ra) -- Store: [0x800135d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800084f0]:fsgnj.d t5, t3, s10
	-[0x800084f4]:csrrs gp, fcsr, zero
	-[0x800084f8]:sw t5, 1800(ra)
Current Store : [0x800084fc] : sw t6, 1808(ra) -- Store: [0x800135e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800084f0]:fsgnj.d t5, t3, s10
	-[0x800084f4]:csrrs gp, fcsr, zero
	-[0x800084f8]:sw t5, 1800(ra)
Current Store : [0x80008500] : sw t5, 1816(ra) -- Store: [0x800135e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800084f0]:fsgnj.d t5, t3, s10
	-[0x800084f4]:csrrs gp, fcsr, zero
	-[0x800084f8]:sw t5, 1800(ra)
Current Store : [0x80008504] : sw gp, 1824(ra) -- Store: [0x800135f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008530]:fsgnj.d t5, t3, s10
	-[0x80008534]:csrrs gp, fcsr, zero
	-[0x80008538]:sw t5, 1832(ra)
Current Store : [0x8000853c] : sw t6, 1840(ra) -- Store: [0x80013600]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008530]:fsgnj.d t5, t3, s10
	-[0x80008534]:csrrs gp, fcsr, zero
	-[0x80008538]:sw t5, 1832(ra)
Current Store : [0x80008540] : sw t5, 1848(ra) -- Store: [0x80013608]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008530]:fsgnj.d t5, t3, s10
	-[0x80008534]:csrrs gp, fcsr, zero
	-[0x80008538]:sw t5, 1832(ra)
Current Store : [0x80008544] : sw gp, 1856(ra) -- Store: [0x80013610]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008574]:fsgnj.d t5, t3, s10
	-[0x80008578]:csrrs gp, fcsr, zero
	-[0x8000857c]:sw t5, 1864(ra)
Current Store : [0x80008580] : sw t6, 1872(ra) -- Store: [0x80013620]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008574]:fsgnj.d t5, t3, s10
	-[0x80008578]:csrrs gp, fcsr, zero
	-[0x8000857c]:sw t5, 1864(ra)
Current Store : [0x80008584] : sw t5, 1880(ra) -- Store: [0x80013628]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008574]:fsgnj.d t5, t3, s10
	-[0x80008578]:csrrs gp, fcsr, zero
	-[0x8000857c]:sw t5, 1864(ra)
Current Store : [0x80008588] : sw gp, 1888(ra) -- Store: [0x80013630]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800085b8]:fsgnj.d t5, t3, s10
	-[0x800085bc]:csrrs gp, fcsr, zero
	-[0x800085c0]:sw t5, 1896(ra)
Current Store : [0x800085c4] : sw t6, 1904(ra) -- Store: [0x80013640]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800085b8]:fsgnj.d t5, t3, s10
	-[0x800085bc]:csrrs gp, fcsr, zero
	-[0x800085c0]:sw t5, 1896(ra)
Current Store : [0x800085c8] : sw t5, 1912(ra) -- Store: [0x80013648]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800085b8]:fsgnj.d t5, t3, s10
	-[0x800085bc]:csrrs gp, fcsr, zero
	-[0x800085c0]:sw t5, 1896(ra)
Current Store : [0x800085cc] : sw gp, 1920(ra) -- Store: [0x80013650]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800085f8]:fsgnj.d t5, t3, s10
	-[0x800085fc]:csrrs gp, fcsr, zero
	-[0x80008600]:sw t5, 1928(ra)
Current Store : [0x80008604] : sw t6, 1936(ra) -- Store: [0x80013660]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800085f8]:fsgnj.d t5, t3, s10
	-[0x800085fc]:csrrs gp, fcsr, zero
	-[0x80008600]:sw t5, 1928(ra)
Current Store : [0x80008608] : sw t5, 1944(ra) -- Store: [0x80013668]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800085f8]:fsgnj.d t5, t3, s10
	-[0x800085fc]:csrrs gp, fcsr, zero
	-[0x80008600]:sw t5, 1928(ra)
Current Store : [0x8000860c] : sw gp, 1952(ra) -- Store: [0x80013670]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008638]:fsgnj.d t5, t3, s10
	-[0x8000863c]:csrrs gp, fcsr, zero
	-[0x80008640]:sw t5, 1960(ra)
Current Store : [0x80008644] : sw t6, 1968(ra) -- Store: [0x80013680]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008638]:fsgnj.d t5, t3, s10
	-[0x8000863c]:csrrs gp, fcsr, zero
	-[0x80008640]:sw t5, 1960(ra)
Current Store : [0x80008648] : sw t5, 1976(ra) -- Store: [0x80013688]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008638]:fsgnj.d t5, t3, s10
	-[0x8000863c]:csrrs gp, fcsr, zero
	-[0x80008640]:sw t5, 1960(ra)
Current Store : [0x8000864c] : sw gp, 1984(ra) -- Store: [0x80013690]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008678]:fsgnj.d t5, t3, s10
	-[0x8000867c]:csrrs gp, fcsr, zero
	-[0x80008680]:sw t5, 1992(ra)
Current Store : [0x80008684] : sw t6, 2000(ra) -- Store: [0x800136a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008678]:fsgnj.d t5, t3, s10
	-[0x8000867c]:csrrs gp, fcsr, zero
	-[0x80008680]:sw t5, 1992(ra)
Current Store : [0x80008688] : sw t5, 2008(ra) -- Store: [0x800136a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008678]:fsgnj.d t5, t3, s10
	-[0x8000867c]:csrrs gp, fcsr, zero
	-[0x80008680]:sw t5, 1992(ra)
Current Store : [0x8000868c] : sw gp, 2016(ra) -- Store: [0x800136b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800086b8]:fsgnj.d t5, t3, s10
	-[0x800086bc]:csrrs gp, fcsr, zero
	-[0x800086c0]:sw t5, 2024(ra)
Current Store : [0x800086c4] : sw t6, 2032(ra) -- Store: [0x800136c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800086b8]:fsgnj.d t5, t3, s10
	-[0x800086bc]:csrrs gp, fcsr, zero
	-[0x800086c0]:sw t5, 2024(ra)
Current Store : [0x800086c8] : sw t5, 2040(ra) -- Store: [0x800136c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800086b8]:fsgnj.d t5, t3, s10
	-[0x800086bc]:csrrs gp, fcsr, zero
	-[0x800086c0]:sw t5, 2024(ra)
Current Store : [0x800086d0] : sw gp, 8(ra) -- Store: [0x800136d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008740]:fsgnj.d t5, t3, s10
	-[0x80008744]:csrrs gp, fcsr, zero
	-[0x80008748]:sw t5, 16(ra)
Current Store : [0x8000874c] : sw t6, 24(ra) -- Store: [0x800136e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008740]:fsgnj.d t5, t3, s10
	-[0x80008744]:csrrs gp, fcsr, zero
	-[0x80008748]:sw t5, 16(ra)
Current Store : [0x80008750] : sw t5, 32(ra) -- Store: [0x800136e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008740]:fsgnj.d t5, t3, s10
	-[0x80008744]:csrrs gp, fcsr, zero
	-[0x80008748]:sw t5, 16(ra)
Current Store : [0x80008754] : sw gp, 40(ra) -- Store: [0x800136f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800087c4]:fsgnj.d t5, t3, s10
	-[0x800087c8]:csrrs gp, fcsr, zero
	-[0x800087cc]:sw t5, 48(ra)
Current Store : [0x800087d0] : sw t6, 56(ra) -- Store: [0x80013700]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800087c4]:fsgnj.d t5, t3, s10
	-[0x800087c8]:csrrs gp, fcsr, zero
	-[0x800087cc]:sw t5, 48(ra)
Current Store : [0x800087d4] : sw t5, 64(ra) -- Store: [0x80013708]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800087c4]:fsgnj.d t5, t3, s10
	-[0x800087c8]:csrrs gp, fcsr, zero
	-[0x800087cc]:sw t5, 48(ra)
Current Store : [0x800087d8] : sw gp, 72(ra) -- Store: [0x80013710]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008844]:fsgnj.d t5, t3, s10
	-[0x80008848]:csrrs gp, fcsr, zero
	-[0x8000884c]:sw t5, 80(ra)
Current Store : [0x80008850] : sw t6, 88(ra) -- Store: [0x80013720]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008844]:fsgnj.d t5, t3, s10
	-[0x80008848]:csrrs gp, fcsr, zero
	-[0x8000884c]:sw t5, 80(ra)
Current Store : [0x80008854] : sw t5, 96(ra) -- Store: [0x80013728]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008844]:fsgnj.d t5, t3, s10
	-[0x80008848]:csrrs gp, fcsr, zero
	-[0x8000884c]:sw t5, 80(ra)
Current Store : [0x80008858] : sw gp, 104(ra) -- Store: [0x80013730]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800088c4]:fsgnj.d t5, t3, s10
	-[0x800088c8]:csrrs gp, fcsr, zero
	-[0x800088cc]:sw t5, 112(ra)
Current Store : [0x800088d0] : sw t6, 120(ra) -- Store: [0x80013740]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800088c4]:fsgnj.d t5, t3, s10
	-[0x800088c8]:csrrs gp, fcsr, zero
	-[0x800088cc]:sw t5, 112(ra)
Current Store : [0x800088d4] : sw t5, 128(ra) -- Store: [0x80013748]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800088c4]:fsgnj.d t5, t3, s10
	-[0x800088c8]:csrrs gp, fcsr, zero
	-[0x800088cc]:sw t5, 112(ra)
Current Store : [0x800088d8] : sw gp, 136(ra) -- Store: [0x80013750]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008944]:fsgnj.d t5, t3, s10
	-[0x80008948]:csrrs gp, fcsr, zero
	-[0x8000894c]:sw t5, 144(ra)
Current Store : [0x80008950] : sw t6, 152(ra) -- Store: [0x80013760]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008944]:fsgnj.d t5, t3, s10
	-[0x80008948]:csrrs gp, fcsr, zero
	-[0x8000894c]:sw t5, 144(ra)
Current Store : [0x80008954] : sw t5, 160(ra) -- Store: [0x80013768]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008944]:fsgnj.d t5, t3, s10
	-[0x80008948]:csrrs gp, fcsr, zero
	-[0x8000894c]:sw t5, 144(ra)
Current Store : [0x80008958] : sw gp, 168(ra) -- Store: [0x80013770]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800089c4]:fsgnj.d t5, t3, s10
	-[0x800089c8]:csrrs gp, fcsr, zero
	-[0x800089cc]:sw t5, 176(ra)
Current Store : [0x800089d0] : sw t6, 184(ra) -- Store: [0x80013780]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800089c4]:fsgnj.d t5, t3, s10
	-[0x800089c8]:csrrs gp, fcsr, zero
	-[0x800089cc]:sw t5, 176(ra)
Current Store : [0x800089d4] : sw t5, 192(ra) -- Store: [0x80013788]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800089c4]:fsgnj.d t5, t3, s10
	-[0x800089c8]:csrrs gp, fcsr, zero
	-[0x800089cc]:sw t5, 176(ra)
Current Store : [0x800089d8] : sw gp, 200(ra) -- Store: [0x80013790]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008a44]:fsgnj.d t5, t3, s10
	-[0x80008a48]:csrrs gp, fcsr, zero
	-[0x80008a4c]:sw t5, 208(ra)
Current Store : [0x80008a50] : sw t6, 216(ra) -- Store: [0x800137a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008a44]:fsgnj.d t5, t3, s10
	-[0x80008a48]:csrrs gp, fcsr, zero
	-[0x80008a4c]:sw t5, 208(ra)
Current Store : [0x80008a54] : sw t5, 224(ra) -- Store: [0x800137a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008a44]:fsgnj.d t5, t3, s10
	-[0x80008a48]:csrrs gp, fcsr, zero
	-[0x80008a4c]:sw t5, 208(ra)
Current Store : [0x80008a58] : sw gp, 232(ra) -- Store: [0x800137b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008ac4]:fsgnj.d t5, t3, s10
	-[0x80008ac8]:csrrs gp, fcsr, zero
	-[0x80008acc]:sw t5, 240(ra)
Current Store : [0x80008ad0] : sw t6, 248(ra) -- Store: [0x800137c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008ac4]:fsgnj.d t5, t3, s10
	-[0x80008ac8]:csrrs gp, fcsr, zero
	-[0x80008acc]:sw t5, 240(ra)
Current Store : [0x80008ad4] : sw t5, 256(ra) -- Store: [0x800137c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008ac4]:fsgnj.d t5, t3, s10
	-[0x80008ac8]:csrrs gp, fcsr, zero
	-[0x80008acc]:sw t5, 240(ra)
Current Store : [0x80008ad8] : sw gp, 264(ra) -- Store: [0x800137d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008b44]:fsgnj.d t5, t3, s10
	-[0x80008b48]:csrrs gp, fcsr, zero
	-[0x80008b4c]:sw t5, 272(ra)
Current Store : [0x80008b50] : sw t6, 280(ra) -- Store: [0x800137e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008b44]:fsgnj.d t5, t3, s10
	-[0x80008b48]:csrrs gp, fcsr, zero
	-[0x80008b4c]:sw t5, 272(ra)
Current Store : [0x80008b54] : sw t5, 288(ra) -- Store: [0x800137e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008b44]:fsgnj.d t5, t3, s10
	-[0x80008b48]:csrrs gp, fcsr, zero
	-[0x80008b4c]:sw t5, 272(ra)
Current Store : [0x80008b58] : sw gp, 296(ra) -- Store: [0x800137f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008bc4]:fsgnj.d t5, t3, s10
	-[0x80008bc8]:csrrs gp, fcsr, zero
	-[0x80008bcc]:sw t5, 304(ra)
Current Store : [0x80008bd0] : sw t6, 312(ra) -- Store: [0x80013800]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008bc4]:fsgnj.d t5, t3, s10
	-[0x80008bc8]:csrrs gp, fcsr, zero
	-[0x80008bcc]:sw t5, 304(ra)
Current Store : [0x80008bd4] : sw t5, 320(ra) -- Store: [0x80013808]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008bc4]:fsgnj.d t5, t3, s10
	-[0x80008bc8]:csrrs gp, fcsr, zero
	-[0x80008bcc]:sw t5, 304(ra)
Current Store : [0x80008bd8] : sw gp, 328(ra) -- Store: [0x80013810]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008c44]:fsgnj.d t5, t3, s10
	-[0x80008c48]:csrrs gp, fcsr, zero
	-[0x80008c4c]:sw t5, 336(ra)
Current Store : [0x80008c50] : sw t6, 344(ra) -- Store: [0x80013820]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008c44]:fsgnj.d t5, t3, s10
	-[0x80008c48]:csrrs gp, fcsr, zero
	-[0x80008c4c]:sw t5, 336(ra)
Current Store : [0x80008c54] : sw t5, 352(ra) -- Store: [0x80013828]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008c44]:fsgnj.d t5, t3, s10
	-[0x80008c48]:csrrs gp, fcsr, zero
	-[0x80008c4c]:sw t5, 336(ra)
Current Store : [0x80008c58] : sw gp, 360(ra) -- Store: [0x80013830]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008cc4]:fsgnj.d t5, t3, s10
	-[0x80008cc8]:csrrs gp, fcsr, zero
	-[0x80008ccc]:sw t5, 368(ra)
Current Store : [0x80008cd0] : sw t6, 376(ra) -- Store: [0x80013840]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008cc4]:fsgnj.d t5, t3, s10
	-[0x80008cc8]:csrrs gp, fcsr, zero
	-[0x80008ccc]:sw t5, 368(ra)
Current Store : [0x80008cd4] : sw t5, 384(ra) -- Store: [0x80013848]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008cc4]:fsgnj.d t5, t3, s10
	-[0x80008cc8]:csrrs gp, fcsr, zero
	-[0x80008ccc]:sw t5, 368(ra)
Current Store : [0x80008cd8] : sw gp, 392(ra) -- Store: [0x80013850]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008d44]:fsgnj.d t5, t3, s10
	-[0x80008d48]:csrrs gp, fcsr, zero
	-[0x80008d4c]:sw t5, 400(ra)
Current Store : [0x80008d50] : sw t6, 408(ra) -- Store: [0x80013860]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008d44]:fsgnj.d t5, t3, s10
	-[0x80008d48]:csrrs gp, fcsr, zero
	-[0x80008d4c]:sw t5, 400(ra)
Current Store : [0x80008d54] : sw t5, 416(ra) -- Store: [0x80013868]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008d44]:fsgnj.d t5, t3, s10
	-[0x80008d48]:csrrs gp, fcsr, zero
	-[0x80008d4c]:sw t5, 400(ra)
Current Store : [0x80008d58] : sw gp, 424(ra) -- Store: [0x80013870]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008dc4]:fsgnj.d t5, t3, s10
	-[0x80008dc8]:csrrs gp, fcsr, zero
	-[0x80008dcc]:sw t5, 432(ra)
Current Store : [0x80008dd0] : sw t6, 440(ra) -- Store: [0x80013880]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008dc4]:fsgnj.d t5, t3, s10
	-[0x80008dc8]:csrrs gp, fcsr, zero
	-[0x80008dcc]:sw t5, 432(ra)
Current Store : [0x80008dd4] : sw t5, 448(ra) -- Store: [0x80013888]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008dc4]:fsgnj.d t5, t3, s10
	-[0x80008dc8]:csrrs gp, fcsr, zero
	-[0x80008dcc]:sw t5, 432(ra)
Current Store : [0x80008dd8] : sw gp, 456(ra) -- Store: [0x80013890]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008e44]:fsgnj.d t5, t3, s10
	-[0x80008e48]:csrrs gp, fcsr, zero
	-[0x80008e4c]:sw t5, 464(ra)
Current Store : [0x80008e50] : sw t6, 472(ra) -- Store: [0x800138a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008e44]:fsgnj.d t5, t3, s10
	-[0x80008e48]:csrrs gp, fcsr, zero
	-[0x80008e4c]:sw t5, 464(ra)
Current Store : [0x80008e54] : sw t5, 480(ra) -- Store: [0x800138a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008e44]:fsgnj.d t5, t3, s10
	-[0x80008e48]:csrrs gp, fcsr, zero
	-[0x80008e4c]:sw t5, 464(ra)
Current Store : [0x80008e58] : sw gp, 488(ra) -- Store: [0x800138b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008ec4]:fsgnj.d t5, t3, s10
	-[0x80008ec8]:csrrs gp, fcsr, zero
	-[0x80008ecc]:sw t5, 496(ra)
Current Store : [0x80008ed0] : sw t6, 504(ra) -- Store: [0x800138c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008ec4]:fsgnj.d t5, t3, s10
	-[0x80008ec8]:csrrs gp, fcsr, zero
	-[0x80008ecc]:sw t5, 496(ra)
Current Store : [0x80008ed4] : sw t5, 512(ra) -- Store: [0x800138c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008ec4]:fsgnj.d t5, t3, s10
	-[0x80008ec8]:csrrs gp, fcsr, zero
	-[0x80008ecc]:sw t5, 496(ra)
Current Store : [0x80008ed8] : sw gp, 520(ra) -- Store: [0x800138d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008f44]:fsgnj.d t5, t3, s10
	-[0x80008f48]:csrrs gp, fcsr, zero
	-[0x80008f4c]:sw t5, 528(ra)
Current Store : [0x80008f50] : sw t6, 536(ra) -- Store: [0x800138e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008f44]:fsgnj.d t5, t3, s10
	-[0x80008f48]:csrrs gp, fcsr, zero
	-[0x80008f4c]:sw t5, 528(ra)
Current Store : [0x80008f54] : sw t5, 544(ra) -- Store: [0x800138e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008f44]:fsgnj.d t5, t3, s10
	-[0x80008f48]:csrrs gp, fcsr, zero
	-[0x80008f4c]:sw t5, 528(ra)
Current Store : [0x80008f58] : sw gp, 552(ra) -- Store: [0x800138f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008fc4]:fsgnj.d t5, t3, s10
	-[0x80008fc8]:csrrs gp, fcsr, zero
	-[0x80008fcc]:sw t5, 560(ra)
Current Store : [0x80008fd0] : sw t6, 568(ra) -- Store: [0x80013900]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008fc4]:fsgnj.d t5, t3, s10
	-[0x80008fc8]:csrrs gp, fcsr, zero
	-[0x80008fcc]:sw t5, 560(ra)
Current Store : [0x80008fd4] : sw t5, 576(ra) -- Store: [0x80013908]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80008fc4]:fsgnj.d t5, t3, s10
	-[0x80008fc8]:csrrs gp, fcsr, zero
	-[0x80008fcc]:sw t5, 560(ra)
Current Store : [0x80008fd8] : sw gp, 584(ra) -- Store: [0x80013910]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80009048]:fsgnj.d t5, t3, s10
	-[0x8000904c]:csrrs gp, fcsr, zero
	-[0x80009050]:sw t5, 592(ra)
Current Store : [0x80009054] : sw t6, 600(ra) -- Store: [0x80013920]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80009048]:fsgnj.d t5, t3, s10
	-[0x8000904c]:csrrs gp, fcsr, zero
	-[0x80009050]:sw t5, 592(ra)
Current Store : [0x80009058] : sw t5, 608(ra) -- Store: [0x80013928]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80009048]:fsgnj.d t5, t3, s10
	-[0x8000904c]:csrrs gp, fcsr, zero
	-[0x80009050]:sw t5, 592(ra)
Current Store : [0x8000905c] : sw gp, 616(ra) -- Store: [0x80013930]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800090cc]:fsgnj.d t5, t3, s10
	-[0x800090d0]:csrrs gp, fcsr, zero
	-[0x800090d4]:sw t5, 624(ra)
Current Store : [0x800090d8] : sw t6, 632(ra) -- Store: [0x80013940]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800090cc]:fsgnj.d t5, t3, s10
	-[0x800090d0]:csrrs gp, fcsr, zero
	-[0x800090d4]:sw t5, 624(ra)
Current Store : [0x800090dc] : sw t5, 640(ra) -- Store: [0x80013948]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800090cc]:fsgnj.d t5, t3, s10
	-[0x800090d0]:csrrs gp, fcsr, zero
	-[0x800090d4]:sw t5, 624(ra)
Current Store : [0x800090e0] : sw gp, 648(ra) -- Store: [0x80013950]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000914c]:fsgnj.d t5, t3, s10
	-[0x80009150]:csrrs gp, fcsr, zero
	-[0x80009154]:sw t5, 656(ra)
Current Store : [0x80009158] : sw t6, 664(ra) -- Store: [0x80013960]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000914c]:fsgnj.d t5, t3, s10
	-[0x80009150]:csrrs gp, fcsr, zero
	-[0x80009154]:sw t5, 656(ra)
Current Store : [0x8000915c] : sw t5, 672(ra) -- Store: [0x80013968]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000914c]:fsgnj.d t5, t3, s10
	-[0x80009150]:csrrs gp, fcsr, zero
	-[0x80009154]:sw t5, 656(ra)
Current Store : [0x80009160] : sw gp, 680(ra) -- Store: [0x80013970]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800091cc]:fsgnj.d t5, t3, s10
	-[0x800091d0]:csrrs gp, fcsr, zero
	-[0x800091d4]:sw t5, 688(ra)
Current Store : [0x800091d8] : sw t6, 696(ra) -- Store: [0x80013980]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800091cc]:fsgnj.d t5, t3, s10
	-[0x800091d0]:csrrs gp, fcsr, zero
	-[0x800091d4]:sw t5, 688(ra)
Current Store : [0x800091dc] : sw t5, 704(ra) -- Store: [0x80013988]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800091cc]:fsgnj.d t5, t3, s10
	-[0x800091d0]:csrrs gp, fcsr, zero
	-[0x800091d4]:sw t5, 688(ra)
Current Store : [0x800091e0] : sw gp, 712(ra) -- Store: [0x80013990]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000924c]:fsgnj.d t5, t3, s10
	-[0x80009250]:csrrs gp, fcsr, zero
	-[0x80009254]:sw t5, 720(ra)
Current Store : [0x80009258] : sw t6, 728(ra) -- Store: [0x800139a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000924c]:fsgnj.d t5, t3, s10
	-[0x80009250]:csrrs gp, fcsr, zero
	-[0x80009254]:sw t5, 720(ra)
Current Store : [0x8000925c] : sw t5, 736(ra) -- Store: [0x800139a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000924c]:fsgnj.d t5, t3, s10
	-[0x80009250]:csrrs gp, fcsr, zero
	-[0x80009254]:sw t5, 720(ra)
Current Store : [0x80009260] : sw gp, 744(ra) -- Store: [0x800139b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800092cc]:fsgnj.d t5, t3, s10
	-[0x800092d0]:csrrs gp, fcsr, zero
	-[0x800092d4]:sw t5, 752(ra)
Current Store : [0x800092d8] : sw t6, 760(ra) -- Store: [0x800139c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800092cc]:fsgnj.d t5, t3, s10
	-[0x800092d0]:csrrs gp, fcsr, zero
	-[0x800092d4]:sw t5, 752(ra)
Current Store : [0x800092dc] : sw t5, 768(ra) -- Store: [0x800139c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800092cc]:fsgnj.d t5, t3, s10
	-[0x800092d0]:csrrs gp, fcsr, zero
	-[0x800092d4]:sw t5, 752(ra)
Current Store : [0x800092e0] : sw gp, 776(ra) -- Store: [0x800139d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80009350]:fsgnj.d t5, t3, s10
	-[0x80009354]:csrrs gp, fcsr, zero
	-[0x80009358]:sw t5, 784(ra)
Current Store : [0x8000935c] : sw t6, 792(ra) -- Store: [0x800139e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80009350]:fsgnj.d t5, t3, s10
	-[0x80009354]:csrrs gp, fcsr, zero
	-[0x80009358]:sw t5, 784(ra)
Current Store : [0x80009360] : sw t5, 800(ra) -- Store: [0x800139e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80009350]:fsgnj.d t5, t3, s10
	-[0x80009354]:csrrs gp, fcsr, zero
	-[0x80009358]:sw t5, 784(ra)
Current Store : [0x80009364] : sw gp, 808(ra) -- Store: [0x800139f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800093d4]:fsgnj.d t5, t3, s10
	-[0x800093d8]:csrrs gp, fcsr, zero
	-[0x800093dc]:sw t5, 816(ra)
Current Store : [0x800093e0] : sw t6, 824(ra) -- Store: [0x80013a00]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800093d4]:fsgnj.d t5, t3, s10
	-[0x800093d8]:csrrs gp, fcsr, zero
	-[0x800093dc]:sw t5, 816(ra)
Current Store : [0x800093e4] : sw t5, 832(ra) -- Store: [0x80013a08]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800093d4]:fsgnj.d t5, t3, s10
	-[0x800093d8]:csrrs gp, fcsr, zero
	-[0x800093dc]:sw t5, 816(ra)
Current Store : [0x800093e8] : sw gp, 840(ra) -- Store: [0x80013a10]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80009454]:fsgnj.d t5, t3, s10
	-[0x80009458]:csrrs gp, fcsr, zero
	-[0x8000945c]:sw t5, 848(ra)
Current Store : [0x80009460] : sw t6, 856(ra) -- Store: [0x80013a20]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80009454]:fsgnj.d t5, t3, s10
	-[0x80009458]:csrrs gp, fcsr, zero
	-[0x8000945c]:sw t5, 848(ra)
Current Store : [0x80009464] : sw t5, 864(ra) -- Store: [0x80013a28]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80009454]:fsgnj.d t5, t3, s10
	-[0x80009458]:csrrs gp, fcsr, zero
	-[0x8000945c]:sw t5, 848(ra)
Current Store : [0x80009468] : sw gp, 872(ra) -- Store: [0x80013a30]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800094d4]:fsgnj.d t5, t3, s10
	-[0x800094d8]:csrrs gp, fcsr, zero
	-[0x800094dc]:sw t5, 880(ra)
Current Store : [0x800094e0] : sw t6, 888(ra) -- Store: [0x80013a40]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c764]:fsgnj.d t5, t3, s10
	-[0x8000c768]:csrrs gp, fcsr, zero
	-[0x8000c76c]:sw t5, 0(ra)
Current Store : [0x8000c770] : sw t6, 8(ra) -- Store: [0x800136e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c764]:fsgnj.d t5, t3, s10
	-[0x8000c768]:csrrs gp, fcsr, zero
	-[0x8000c76c]:sw t5, 0(ra)
Current Store : [0x8000c774] : sw t5, 16(ra) -- Store: [0x800136e8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c764]:fsgnj.d t5, t3, s10
	-[0x8000c768]:csrrs gp, fcsr, zero
	-[0x8000c76c]:sw t5, 0(ra)
Current Store : [0x8000c778] : sw gp, 24(ra) -- Store: [0x800136f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c7a4]:fsgnj.d t5, t3, s10
	-[0x8000c7a8]:csrrs gp, fcsr, zero
	-[0x8000c7ac]:sw t5, 32(ra)
Current Store : [0x8000c7b0] : sw t6, 40(ra) -- Store: [0x80013700]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c7a4]:fsgnj.d t5, t3, s10
	-[0x8000c7a8]:csrrs gp, fcsr, zero
	-[0x8000c7ac]:sw t5, 32(ra)
Current Store : [0x8000c7b4] : sw t5, 48(ra) -- Store: [0x80013708]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c7a4]:fsgnj.d t5, t3, s10
	-[0x8000c7a8]:csrrs gp, fcsr, zero
	-[0x8000c7ac]:sw t5, 32(ra)
Current Store : [0x8000c7b8] : sw gp, 56(ra) -- Store: [0x80013710]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c7e4]:fsgnj.d t5, t3, s10
	-[0x8000c7e8]:csrrs gp, fcsr, zero
	-[0x8000c7ec]:sw t5, 64(ra)
Current Store : [0x8000c7f0] : sw t6, 72(ra) -- Store: [0x80013720]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c7e4]:fsgnj.d t5, t3, s10
	-[0x8000c7e8]:csrrs gp, fcsr, zero
	-[0x8000c7ec]:sw t5, 64(ra)
Current Store : [0x8000c7f4] : sw t5, 80(ra) -- Store: [0x80013728]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c7e4]:fsgnj.d t5, t3, s10
	-[0x8000c7e8]:csrrs gp, fcsr, zero
	-[0x8000c7ec]:sw t5, 64(ra)
Current Store : [0x8000c7f8] : sw gp, 88(ra) -- Store: [0x80013730]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c824]:fsgnj.d t5, t3, s10
	-[0x8000c828]:csrrs gp, fcsr, zero
	-[0x8000c82c]:sw t5, 96(ra)
Current Store : [0x8000c830] : sw t6, 104(ra) -- Store: [0x80013740]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c824]:fsgnj.d t5, t3, s10
	-[0x8000c828]:csrrs gp, fcsr, zero
	-[0x8000c82c]:sw t5, 96(ra)
Current Store : [0x8000c834] : sw t5, 112(ra) -- Store: [0x80013748]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c824]:fsgnj.d t5, t3, s10
	-[0x8000c828]:csrrs gp, fcsr, zero
	-[0x8000c82c]:sw t5, 96(ra)
Current Store : [0x8000c838] : sw gp, 120(ra) -- Store: [0x80013750]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c864]:fsgnj.d t5, t3, s10
	-[0x8000c868]:csrrs gp, fcsr, zero
	-[0x8000c86c]:sw t5, 128(ra)
Current Store : [0x8000c870] : sw t6, 136(ra) -- Store: [0x80013760]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c864]:fsgnj.d t5, t3, s10
	-[0x8000c868]:csrrs gp, fcsr, zero
	-[0x8000c86c]:sw t5, 128(ra)
Current Store : [0x8000c874] : sw t5, 144(ra) -- Store: [0x80013768]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c864]:fsgnj.d t5, t3, s10
	-[0x8000c868]:csrrs gp, fcsr, zero
	-[0x8000c86c]:sw t5, 128(ra)
Current Store : [0x8000c878] : sw gp, 152(ra) -- Store: [0x80013770]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c8a4]:fsgnj.d t5, t3, s10
	-[0x8000c8a8]:csrrs gp, fcsr, zero
	-[0x8000c8ac]:sw t5, 160(ra)
Current Store : [0x8000c8b0] : sw t6, 168(ra) -- Store: [0x80013780]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c8a4]:fsgnj.d t5, t3, s10
	-[0x8000c8a8]:csrrs gp, fcsr, zero
	-[0x8000c8ac]:sw t5, 160(ra)
Current Store : [0x8000c8b4] : sw t5, 176(ra) -- Store: [0x80013788]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c8a4]:fsgnj.d t5, t3, s10
	-[0x8000c8a8]:csrrs gp, fcsr, zero
	-[0x8000c8ac]:sw t5, 160(ra)
Current Store : [0x8000c8b8] : sw gp, 184(ra) -- Store: [0x80013790]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c8e4]:fsgnj.d t5, t3, s10
	-[0x8000c8e8]:csrrs gp, fcsr, zero
	-[0x8000c8ec]:sw t5, 192(ra)
Current Store : [0x8000c8f0] : sw t6, 200(ra) -- Store: [0x800137a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c8e4]:fsgnj.d t5, t3, s10
	-[0x8000c8e8]:csrrs gp, fcsr, zero
	-[0x8000c8ec]:sw t5, 192(ra)
Current Store : [0x8000c8f4] : sw t5, 208(ra) -- Store: [0x800137a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c8e4]:fsgnj.d t5, t3, s10
	-[0x8000c8e8]:csrrs gp, fcsr, zero
	-[0x8000c8ec]:sw t5, 192(ra)
Current Store : [0x8000c8f8] : sw gp, 216(ra) -- Store: [0x800137b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c924]:fsgnj.d t5, t3, s10
	-[0x8000c928]:csrrs gp, fcsr, zero
	-[0x8000c92c]:sw t5, 224(ra)
Current Store : [0x8000c930] : sw t6, 232(ra) -- Store: [0x800137c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c924]:fsgnj.d t5, t3, s10
	-[0x8000c928]:csrrs gp, fcsr, zero
	-[0x8000c92c]:sw t5, 224(ra)
Current Store : [0x8000c934] : sw t5, 240(ra) -- Store: [0x800137c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c924]:fsgnj.d t5, t3, s10
	-[0x8000c928]:csrrs gp, fcsr, zero
	-[0x8000c92c]:sw t5, 224(ra)
Current Store : [0x8000c938] : sw gp, 248(ra) -- Store: [0x800137d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c964]:fsgnj.d t5, t3, s10
	-[0x8000c968]:csrrs gp, fcsr, zero
	-[0x8000c96c]:sw t5, 256(ra)
Current Store : [0x8000c970] : sw t6, 264(ra) -- Store: [0x800137e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c964]:fsgnj.d t5, t3, s10
	-[0x8000c968]:csrrs gp, fcsr, zero
	-[0x8000c96c]:sw t5, 256(ra)
Current Store : [0x8000c974] : sw t5, 272(ra) -- Store: [0x800137e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c964]:fsgnj.d t5, t3, s10
	-[0x8000c968]:csrrs gp, fcsr, zero
	-[0x8000c96c]:sw t5, 256(ra)
Current Store : [0x8000c978] : sw gp, 280(ra) -- Store: [0x800137f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c9a4]:fsgnj.d t5, t3, s10
	-[0x8000c9a8]:csrrs gp, fcsr, zero
	-[0x8000c9ac]:sw t5, 288(ra)
Current Store : [0x8000c9b0] : sw t6, 296(ra) -- Store: [0x80013800]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c9a4]:fsgnj.d t5, t3, s10
	-[0x8000c9a8]:csrrs gp, fcsr, zero
	-[0x8000c9ac]:sw t5, 288(ra)
Current Store : [0x8000c9b4] : sw t5, 304(ra) -- Store: [0x80013808]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c9a4]:fsgnj.d t5, t3, s10
	-[0x8000c9a8]:csrrs gp, fcsr, zero
	-[0x8000c9ac]:sw t5, 288(ra)
Current Store : [0x8000c9b8] : sw gp, 312(ra) -- Store: [0x80013810]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c9e8]:fsgnj.d t5, t3, s10
	-[0x8000c9ec]:csrrs gp, fcsr, zero
	-[0x8000c9f0]:sw t5, 320(ra)
Current Store : [0x8000c9f4] : sw t6, 328(ra) -- Store: [0x80013820]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c9e8]:fsgnj.d t5, t3, s10
	-[0x8000c9ec]:csrrs gp, fcsr, zero
	-[0x8000c9f0]:sw t5, 320(ra)
Current Store : [0x8000c9f8] : sw t5, 336(ra) -- Store: [0x80013828]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000c9e8]:fsgnj.d t5, t3, s10
	-[0x8000c9ec]:csrrs gp, fcsr, zero
	-[0x8000c9f0]:sw t5, 320(ra)
Current Store : [0x8000c9fc] : sw gp, 344(ra) -- Store: [0x80013830]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000ca2c]:fsgnj.d t5, t3, s10
	-[0x8000ca30]:csrrs gp, fcsr, zero
	-[0x8000ca34]:sw t5, 352(ra)
Current Store : [0x8000ca38] : sw t6, 360(ra) -- Store: [0x80013840]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000ca2c]:fsgnj.d t5, t3, s10
	-[0x8000ca30]:csrrs gp, fcsr, zero
	-[0x8000ca34]:sw t5, 352(ra)
Current Store : [0x8000ca3c] : sw t5, 368(ra) -- Store: [0x80013848]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000ca2c]:fsgnj.d t5, t3, s10
	-[0x8000ca30]:csrrs gp, fcsr, zero
	-[0x8000ca34]:sw t5, 352(ra)
Current Store : [0x8000ca40] : sw gp, 376(ra) -- Store: [0x80013850]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000ca6c]:fsgnj.d t5, t3, s10
	-[0x8000ca70]:csrrs gp, fcsr, zero
	-[0x8000ca74]:sw t5, 384(ra)
Current Store : [0x8000ca78] : sw t6, 392(ra) -- Store: [0x80013860]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000ca6c]:fsgnj.d t5, t3, s10
	-[0x8000ca70]:csrrs gp, fcsr, zero
	-[0x8000ca74]:sw t5, 384(ra)
Current Store : [0x8000ca7c] : sw t5, 400(ra) -- Store: [0x80013868]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000ca6c]:fsgnj.d t5, t3, s10
	-[0x8000ca70]:csrrs gp, fcsr, zero
	-[0x8000ca74]:sw t5, 384(ra)
Current Store : [0x8000ca80] : sw gp, 408(ra) -- Store: [0x80013870]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000caac]:fsgnj.d t5, t3, s10
	-[0x8000cab0]:csrrs gp, fcsr, zero
	-[0x8000cab4]:sw t5, 416(ra)
Current Store : [0x8000cab8] : sw t6, 424(ra) -- Store: [0x80013880]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000caac]:fsgnj.d t5, t3, s10
	-[0x8000cab0]:csrrs gp, fcsr, zero
	-[0x8000cab4]:sw t5, 416(ra)
Current Store : [0x8000cabc] : sw t5, 432(ra) -- Store: [0x80013888]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000caac]:fsgnj.d t5, t3, s10
	-[0x8000cab0]:csrrs gp, fcsr, zero
	-[0x8000cab4]:sw t5, 416(ra)
Current Store : [0x8000cac0] : sw gp, 440(ra) -- Store: [0x80013890]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000caec]:fsgnj.d t5, t3, s10
	-[0x8000caf0]:csrrs gp, fcsr, zero
	-[0x8000caf4]:sw t5, 448(ra)
Current Store : [0x8000caf8] : sw t6, 456(ra) -- Store: [0x800138a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000caec]:fsgnj.d t5, t3, s10
	-[0x8000caf0]:csrrs gp, fcsr, zero
	-[0x8000caf4]:sw t5, 448(ra)
Current Store : [0x8000cafc] : sw t5, 464(ra) -- Store: [0x800138a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000caec]:fsgnj.d t5, t3, s10
	-[0x8000caf0]:csrrs gp, fcsr, zero
	-[0x8000caf4]:sw t5, 448(ra)
Current Store : [0x8000cb00] : sw gp, 472(ra) -- Store: [0x800138b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cb2c]:fsgnj.d t5, t3, s10
	-[0x8000cb30]:csrrs gp, fcsr, zero
	-[0x8000cb34]:sw t5, 480(ra)
Current Store : [0x8000cb38] : sw t6, 488(ra) -- Store: [0x800138c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cb2c]:fsgnj.d t5, t3, s10
	-[0x8000cb30]:csrrs gp, fcsr, zero
	-[0x8000cb34]:sw t5, 480(ra)
Current Store : [0x8000cb3c] : sw t5, 496(ra) -- Store: [0x800138c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cb2c]:fsgnj.d t5, t3, s10
	-[0x8000cb30]:csrrs gp, fcsr, zero
	-[0x8000cb34]:sw t5, 480(ra)
Current Store : [0x8000cb40] : sw gp, 504(ra) -- Store: [0x800138d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cb70]:fsgnj.d t5, t3, s10
	-[0x8000cb74]:csrrs gp, fcsr, zero
	-[0x8000cb78]:sw t5, 512(ra)
Current Store : [0x8000cb7c] : sw t6, 520(ra) -- Store: [0x800138e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cb70]:fsgnj.d t5, t3, s10
	-[0x8000cb74]:csrrs gp, fcsr, zero
	-[0x8000cb78]:sw t5, 512(ra)
Current Store : [0x8000cb80] : sw t5, 528(ra) -- Store: [0x800138e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cb70]:fsgnj.d t5, t3, s10
	-[0x8000cb74]:csrrs gp, fcsr, zero
	-[0x8000cb78]:sw t5, 512(ra)
Current Store : [0x8000cb84] : sw gp, 536(ra) -- Store: [0x800138f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cbb4]:fsgnj.d t5, t3, s10
	-[0x8000cbb8]:csrrs gp, fcsr, zero
	-[0x8000cbbc]:sw t5, 544(ra)
Current Store : [0x8000cbc0] : sw t6, 552(ra) -- Store: [0x80013900]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cbb4]:fsgnj.d t5, t3, s10
	-[0x8000cbb8]:csrrs gp, fcsr, zero
	-[0x8000cbbc]:sw t5, 544(ra)
Current Store : [0x8000cbc4] : sw t5, 560(ra) -- Store: [0x80013908]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cbb4]:fsgnj.d t5, t3, s10
	-[0x8000cbb8]:csrrs gp, fcsr, zero
	-[0x8000cbbc]:sw t5, 544(ra)
Current Store : [0x8000cbc8] : sw gp, 568(ra) -- Store: [0x80013910]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cbf4]:fsgnj.d t5, t3, s10
	-[0x8000cbf8]:csrrs gp, fcsr, zero
	-[0x8000cbfc]:sw t5, 576(ra)
Current Store : [0x8000cc00] : sw t6, 584(ra) -- Store: [0x80013920]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cbf4]:fsgnj.d t5, t3, s10
	-[0x8000cbf8]:csrrs gp, fcsr, zero
	-[0x8000cbfc]:sw t5, 576(ra)
Current Store : [0x8000cc04] : sw t5, 592(ra) -- Store: [0x80013928]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cbf4]:fsgnj.d t5, t3, s10
	-[0x8000cbf8]:csrrs gp, fcsr, zero
	-[0x8000cbfc]:sw t5, 576(ra)
Current Store : [0x8000cc08] : sw gp, 600(ra) -- Store: [0x80013930]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cc34]:fsgnj.d t5, t3, s10
	-[0x8000cc38]:csrrs gp, fcsr, zero
	-[0x8000cc3c]:sw t5, 608(ra)
Current Store : [0x8000cc40] : sw t6, 616(ra) -- Store: [0x80013940]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cc34]:fsgnj.d t5, t3, s10
	-[0x8000cc38]:csrrs gp, fcsr, zero
	-[0x8000cc3c]:sw t5, 608(ra)
Current Store : [0x8000cc44] : sw t5, 624(ra) -- Store: [0x80013948]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cc34]:fsgnj.d t5, t3, s10
	-[0x8000cc38]:csrrs gp, fcsr, zero
	-[0x8000cc3c]:sw t5, 608(ra)
Current Store : [0x8000cc48] : sw gp, 632(ra) -- Store: [0x80013950]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cc74]:fsgnj.d t5, t3, s10
	-[0x8000cc78]:csrrs gp, fcsr, zero
	-[0x8000cc7c]:sw t5, 640(ra)
Current Store : [0x8000cc80] : sw t6, 648(ra) -- Store: [0x80013960]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cc74]:fsgnj.d t5, t3, s10
	-[0x8000cc78]:csrrs gp, fcsr, zero
	-[0x8000cc7c]:sw t5, 640(ra)
Current Store : [0x8000cc84] : sw t5, 656(ra) -- Store: [0x80013968]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cc74]:fsgnj.d t5, t3, s10
	-[0x8000cc78]:csrrs gp, fcsr, zero
	-[0x8000cc7c]:sw t5, 640(ra)
Current Store : [0x8000cc88] : sw gp, 664(ra) -- Store: [0x80013970]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000ccb4]:fsgnj.d t5, t3, s10
	-[0x8000ccb8]:csrrs gp, fcsr, zero
	-[0x8000ccbc]:sw t5, 672(ra)
Current Store : [0x8000ccc0] : sw t6, 680(ra) -- Store: [0x80013980]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000ccb4]:fsgnj.d t5, t3, s10
	-[0x8000ccb8]:csrrs gp, fcsr, zero
	-[0x8000ccbc]:sw t5, 672(ra)
Current Store : [0x8000ccc4] : sw t5, 688(ra) -- Store: [0x80013988]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000ccb4]:fsgnj.d t5, t3, s10
	-[0x8000ccb8]:csrrs gp, fcsr, zero
	-[0x8000ccbc]:sw t5, 672(ra)
Current Store : [0x8000ccc8] : sw gp, 696(ra) -- Store: [0x80013990]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000ccf4]:fsgnj.d t5, t3, s10
	-[0x8000ccf8]:csrrs gp, fcsr, zero
	-[0x8000ccfc]:sw t5, 704(ra)
Current Store : [0x8000cd00] : sw t6, 712(ra) -- Store: [0x800139a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000ccf4]:fsgnj.d t5, t3, s10
	-[0x8000ccf8]:csrrs gp, fcsr, zero
	-[0x8000ccfc]:sw t5, 704(ra)
Current Store : [0x8000cd04] : sw t5, 720(ra) -- Store: [0x800139a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000ccf4]:fsgnj.d t5, t3, s10
	-[0x8000ccf8]:csrrs gp, fcsr, zero
	-[0x8000ccfc]:sw t5, 704(ra)
Current Store : [0x8000cd08] : sw gp, 728(ra) -- Store: [0x800139b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cd34]:fsgnj.d t5, t3, s10
	-[0x8000cd38]:csrrs gp, fcsr, zero
	-[0x8000cd3c]:sw t5, 736(ra)
Current Store : [0x8000cd40] : sw t6, 744(ra) -- Store: [0x800139c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cd34]:fsgnj.d t5, t3, s10
	-[0x8000cd38]:csrrs gp, fcsr, zero
	-[0x8000cd3c]:sw t5, 736(ra)
Current Store : [0x8000cd44] : sw t5, 752(ra) -- Store: [0x800139c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cd34]:fsgnj.d t5, t3, s10
	-[0x8000cd38]:csrrs gp, fcsr, zero
	-[0x8000cd3c]:sw t5, 736(ra)
Current Store : [0x8000cd48] : sw gp, 760(ra) -- Store: [0x800139d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cd74]:fsgnj.d t5, t3, s10
	-[0x8000cd78]:csrrs gp, fcsr, zero
	-[0x8000cd7c]:sw t5, 768(ra)
Current Store : [0x8000cd80] : sw t6, 776(ra) -- Store: [0x800139e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cd74]:fsgnj.d t5, t3, s10
	-[0x8000cd78]:csrrs gp, fcsr, zero
	-[0x8000cd7c]:sw t5, 768(ra)
Current Store : [0x8000cd84] : sw t5, 784(ra) -- Store: [0x800139e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cd74]:fsgnj.d t5, t3, s10
	-[0x8000cd78]:csrrs gp, fcsr, zero
	-[0x8000cd7c]:sw t5, 768(ra)
Current Store : [0x8000cd88] : sw gp, 792(ra) -- Store: [0x800139f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cdb4]:fsgnj.d t5, t3, s10
	-[0x8000cdb8]:csrrs gp, fcsr, zero
	-[0x8000cdbc]:sw t5, 800(ra)
Current Store : [0x8000cdc0] : sw t6, 808(ra) -- Store: [0x80013a00]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cdb4]:fsgnj.d t5, t3, s10
	-[0x8000cdb8]:csrrs gp, fcsr, zero
	-[0x8000cdbc]:sw t5, 800(ra)
Current Store : [0x8000cdc4] : sw t5, 816(ra) -- Store: [0x80013a08]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cdb4]:fsgnj.d t5, t3, s10
	-[0x8000cdb8]:csrrs gp, fcsr, zero
	-[0x8000cdbc]:sw t5, 800(ra)
Current Store : [0x8000cdc8] : sw gp, 824(ra) -- Store: [0x80013a10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cdf4]:fsgnj.d t5, t3, s10
	-[0x8000cdf8]:csrrs gp, fcsr, zero
	-[0x8000cdfc]:sw t5, 832(ra)
Current Store : [0x8000ce00] : sw t6, 840(ra) -- Store: [0x80013a20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cdf4]:fsgnj.d t5, t3, s10
	-[0x8000cdf8]:csrrs gp, fcsr, zero
	-[0x8000cdfc]:sw t5, 832(ra)
Current Store : [0x8000ce04] : sw t5, 848(ra) -- Store: [0x80013a28]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000cdf4]:fsgnj.d t5, t3, s10
	-[0x8000cdf8]:csrrs gp, fcsr, zero
	-[0x8000cdfc]:sw t5, 832(ra)
Current Store : [0x8000ce08] : sw gp, 856(ra) -- Store: [0x80013a30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000ce34]:fsgnj.d t5, t3, s10
	-[0x8000ce38]:csrrs gp, fcsr, zero
	-[0x8000ce3c]:sw t5, 864(ra)
Current Store : [0x8000ce40] : sw t6, 872(ra) -- Store: [0x80013a40]:0x00000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|           signature           |                                                                                                               coverpoints                                                                                                                |                                                                    code                                                                    |
|---:|-------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80011618]<br>0x00000000<br> |- mnemonic : fsgnj.d<br> - rs1 : x28<br> - rs2 : x30<br> - rd : x30<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br> |[0x8000013c]:fsgnj.d t5, t3, t5<br> [0x80000140]:csrrs tp, fcsr, zero<br> [0x80000144]:sw t5, 0(ra)<br>                                     |
|   2|[0x80011638]<br>0x00000000<br> |- rs1 : x26<br> - rs2 : x28<br> - rd : x26<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                          |[0x8000017c]:fsgnj.d s10, s10, t3<br> [0x80000180]:csrrs tp, fcsr, zero<br> [0x80000184]:sw s10, 32(ra)<br>                                 |
|   3|[0x80011658]<br>0x00000000<br> |- rs1 : x24<br> - rs2 : x24<br> - rd : x28<br> - rs1 == rs2 != rd<br>                                                                                                                                                                     |[0x800001bc]:fsgnj.d t3, s8, s8<br> [0x800001c0]:csrrs tp, fcsr, zero<br> [0x800001c4]:sw t3, 64(ra)<br>                                    |
|   4|[0x80011678]<br>0x00000000<br> |- rs1 : x30<br> - rs2 : x26<br> - rd : x24<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>   |[0x800001fc]:fsgnj.d s8, t5, s10<br> [0x80000200]:csrrs tp, fcsr, zero<br> [0x80000204]:sw s8, 96(ra)<br>                                   |
|   5|[0x80011698]<br>0x00000000<br> |- rs1 : x22<br> - rs2 : x22<br> - rd : x22<br> - rs1 == rs2 == rd<br>                                                                                                                                                                     |[0x8000023c]:fsgnj.d s6, s6, s6<br> [0x80000240]:csrrs tp, fcsr, zero<br> [0x80000244]:sw s6, 128(ra)<br>                                   |
|   6|[0x800116b8]<br>0x00000000<br> |- rs1 : x18<br> - rs2 : x16<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                 |[0x8000027c]:fsgnj.d s4, s2, a6<br> [0x80000280]:csrrs tp, fcsr, zero<br> [0x80000284]:sw s4, 160(ra)<br>                                   |
|   7|[0x800116d8]<br>0x00000000<br> |- rs1 : x16<br> - rs2 : x20<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                 |[0x800002c0]:fsgnj.d s2, a6, s4<br> [0x800002c4]:csrrs tp, fcsr, zero<br> [0x800002c8]:sw s2, 192(ra)<br>                                   |
|   8|[0x800116f8]<br>0x00000000<br> |- rs1 : x20<br> - rs2 : x18<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                 |[0x80000304]:fsgnj.d a6, s4, s2<br> [0x80000308]:csrrs tp, fcsr, zero<br> [0x8000030c]:sw a6, 224(ra)<br>                                   |
|   9|[0x80011718]<br>0x00000000<br> |- rs1 : x12<br> - rs2 : x10<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                 |[0x80000344]:fsgnj.d a4, a2, a0<br> [0x80000348]:csrrs tp, fcsr, zero<br> [0x8000034c]:sw a4, 256(ra)<br>                                   |
|  10|[0x80011738]<br>0x00000000<br> |- rs1 : x10<br> - rs2 : x14<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                 |[0x8000038c]:fsgnj.d a2, a0, a4<br> [0x80000390]:csrrs s2, fcsr, zero<br> [0x80000394]:sw a2, 288(ra)<br>                                   |
|  11|[0x80011758]<br>0x00000000<br> |- rs1 : x14<br> - rs2 : x12<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                 |[0x800003cc]:fsgnj.d a0, a4, a2<br> [0x800003d0]:csrrs s2, fcsr, zero<br> [0x800003d4]:sw a0, 320(ra)<br>                                   |
|  12|[0x80011778]<br>0x00000000<br> |- rs1 : x6<br> - rs2 : x4<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                    |[0x8000040c]:fsgnj.d fp, t1, tp<br> [0x80000410]:csrrs s2, fcsr, zero<br> [0x80000414]:sw fp, 352(ra)<br>                                   |
|  13|[0x800116d8]<br>0x00000000<br> |- rs1 : x4<br> - rs2 : x8<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                    |[0x80000458]:fsgnj.d t1, tp, fp<br> [0x8000045c]:csrrs s2, fcsr, zero<br> [0x80000460]:sw t1, 0(ra)<br>                                     |
|  14|[0x800116f8]<br>0x00000000<br> |- rs1 : x8<br> - rs2 : x6<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                    |[0x8000049c]:fsgnj.d tp, fp, t1<br> [0x800004a0]:csrrs s2, fcsr, zero<br> [0x800004a4]:sw tp, 32(ra)<br>                                    |
|  15|[0x80011718]<br>0x00000000<br> |- rs1 : x2<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                 |[0x800004dc]:fsgnj.d t5, sp, t3<br> [0x800004e0]:csrrs s2, fcsr, zero<br> [0x800004e4]:sw t5, 64(ra)<br>                                    |
|  16|[0x80011738]<br>0x00000000<br> |- rs2 : x2<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                 |[0x8000051c]:fsgnj.d t5, t3, sp<br> [0x80000520]:csrrs s2, fcsr, zero<br> [0x80000524]:sw t5, 96(ra)<br>                                    |
|  17|[0x80011758]<br>0x00000000<br> |- rd : x2<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                  |[0x8000055c]:fsgnj.d sp, t5, t3<br> [0x80000560]:csrrs s2, fcsr, zero<br> [0x80000564]:sw sp, 128(ra)<br>                                   |
|  18|[0x80011778]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000059c]:fsgnj.d t5, t3, s10<br> [0x800005a0]:csrrs s2, fcsr, zero<br> [0x800005a4]:sw t5, 160(ra)<br>                                  |
|  19|[0x80011798]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x800005e4]:fsgnj.d t5, t3, s10<br> [0x800005e8]:csrrs gp, fcsr, zero<br> [0x800005ec]:sw t5, 192(ra)<br>                                  |
|  20|[0x800117b8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80000624]:fsgnj.d t5, t3, s10<br> [0x80000628]:csrrs gp, fcsr, zero<br> [0x8000062c]:sw t5, 224(ra)<br>                                  |
|  21|[0x800117d8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80000664]:fsgnj.d t5, t3, s10<br> [0x80000668]:csrrs gp, fcsr, zero<br> [0x8000066c]:sw t5, 256(ra)<br>                                  |
|  22|[0x800117f8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x800006a4]:fsgnj.d t5, t3, s10<br> [0x800006a8]:csrrs gp, fcsr, zero<br> [0x800006ac]:sw t5, 288(ra)<br>                                  |
|  23|[0x80011818]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800006e4]:fsgnj.d t5, t3, s10<br> [0x800006e8]:csrrs gp, fcsr, zero<br> [0x800006ec]:sw t5, 320(ra)<br>                                  |
|  24|[0x80011838]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80000724]:fsgnj.d t5, t3, s10<br> [0x80000728]:csrrs gp, fcsr, zero<br> [0x8000072c]:sw t5, 352(ra)<br>                                  |
|  25|[0x80011858]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80000764]:fsgnj.d t5, t3, s10<br> [0x80000768]:csrrs gp, fcsr, zero<br> [0x8000076c]:sw t5, 384(ra)<br>                                  |
|  26|[0x80011878]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800007a4]:fsgnj.d t5, t3, s10<br> [0x800007a8]:csrrs gp, fcsr, zero<br> [0x800007ac]:sw t5, 416(ra)<br>                                  |
|  27|[0x80011898]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x800007e4]:fsgnj.d t5, t3, s10<br> [0x800007e8]:csrrs gp, fcsr, zero<br> [0x800007ec]:sw t5, 448(ra)<br>                                  |
|  28|[0x800118b8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80000824]:fsgnj.d t5, t3, s10<br> [0x80000828]:csrrs gp, fcsr, zero<br> [0x8000082c]:sw t5, 480(ra)<br>                                  |
|  29|[0x800118d8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80000864]:fsgnj.d t5, t3, s10<br> [0x80000868]:csrrs gp, fcsr, zero<br> [0x8000086c]:sw t5, 512(ra)<br>                                  |
|  30|[0x800118f8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x800008a4]:fsgnj.d t5, t3, s10<br> [0x800008a8]:csrrs gp, fcsr, zero<br> [0x800008ac]:sw t5, 544(ra)<br>                                  |
|  31|[0x80011918]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x800008e8]:fsgnj.d t5, t3, s10<br> [0x800008ec]:csrrs gp, fcsr, zero<br> [0x800008f0]:sw t5, 576(ra)<br>                                  |
|  32|[0x80011938]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x8000092c]:fsgnj.d t5, t3, s10<br> [0x80000930]:csrrs gp, fcsr, zero<br> [0x80000934]:sw t5, 608(ra)<br>                                  |
|  33|[0x80011958]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000096c]:fsgnj.d t5, t3, s10<br> [0x80000970]:csrrs gp, fcsr, zero<br> [0x80000974]:sw t5, 640(ra)<br>                                  |
|  34|[0x80011978]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800009ac]:fsgnj.d t5, t3, s10<br> [0x800009b0]:csrrs gp, fcsr, zero<br> [0x800009b4]:sw t5, 672(ra)<br>                                  |
|  35|[0x80011998]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x800009ec]:fsgnj.d t5, t3, s10<br> [0x800009f0]:csrrs gp, fcsr, zero<br> [0x800009f4]:sw t5, 704(ra)<br>                                  |
|  36|[0x800119b8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80000a2c]:fsgnj.d t5, t3, s10<br> [0x80000a30]:csrrs gp, fcsr, zero<br> [0x80000a34]:sw t5, 736(ra)<br>                                  |
|  37|[0x800119d8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80000a70]:fsgnj.d t5, t3, s10<br> [0x80000a74]:csrrs gp, fcsr, zero<br> [0x80000a78]:sw t5, 768(ra)<br>                                  |
|  38|[0x800119f8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80000ab4]:fsgnj.d t5, t3, s10<br> [0x80000ab8]:csrrs gp, fcsr, zero<br> [0x80000abc]:sw t5, 800(ra)<br>                                  |
|  39|[0x80011a18]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80000af4]:fsgnj.d t5, t3, s10<br> [0x80000af8]:csrrs gp, fcsr, zero<br> [0x80000afc]:sw t5, 832(ra)<br>                                  |
|  40|[0x80011a38]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80000b34]:fsgnj.d t5, t3, s10<br> [0x80000b38]:csrrs gp, fcsr, zero<br> [0x80000b3c]:sw t5, 864(ra)<br>                                  |
|  41|[0x80011a58]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80000b74]:fsgnj.d t5, t3, s10<br> [0x80000b78]:csrrs gp, fcsr, zero<br> [0x80000b7c]:sw t5, 896(ra)<br>                                  |
|  42|[0x80011a78]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80000bb4]:fsgnj.d t5, t3, s10<br> [0x80000bb8]:csrrs gp, fcsr, zero<br> [0x80000bbc]:sw t5, 928(ra)<br>                                  |
|  43|[0x80011a98]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80000bf4]:fsgnj.d t5, t3, s10<br> [0x80000bf8]:csrrs gp, fcsr, zero<br> [0x80000bfc]:sw t5, 960(ra)<br>                                  |
|  44|[0x80011ab8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80000c34]:fsgnj.d t5, t3, s10<br> [0x80000c38]:csrrs gp, fcsr, zero<br> [0x80000c3c]:sw t5, 992(ra)<br>                                  |
|  45|[0x80011ad8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80000c74]:fsgnj.d t5, t3, s10<br> [0x80000c78]:csrrs gp, fcsr, zero<br> [0x80000c7c]:sw t5, 1024(ra)<br>                                 |
|  46|[0x80011af8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80000cb4]:fsgnj.d t5, t3, s10<br> [0x80000cb8]:csrrs gp, fcsr, zero<br> [0x80000cbc]:sw t5, 1056(ra)<br>                                 |
|  47|[0x80011b18]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80000cf4]:fsgnj.d t5, t3, s10<br> [0x80000cf8]:csrrs gp, fcsr, zero<br> [0x80000cfc]:sw t5, 1088(ra)<br>                                 |
|  48|[0x80011b38]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80000d34]:fsgnj.d t5, t3, s10<br> [0x80000d38]:csrrs gp, fcsr, zero<br> [0x80000d3c]:sw t5, 1120(ra)<br>                                 |
|  49|[0x80011b58]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80000d74]:fsgnj.d t5, t3, s10<br> [0x80000d78]:csrrs gp, fcsr, zero<br> [0x80000d7c]:sw t5, 1152(ra)<br>                                 |
|  50|[0x80011b78]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80000db4]:fsgnj.d t5, t3, s10<br> [0x80000db8]:csrrs gp, fcsr, zero<br> [0x80000dbc]:sw t5, 1184(ra)<br>                                 |
|  51|[0x80011b98]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80000df4]:fsgnj.d t5, t3, s10<br> [0x80000df8]:csrrs gp, fcsr, zero<br> [0x80000dfc]:sw t5, 1216(ra)<br>                                 |
|  52|[0x80011bb8]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80000e34]:fsgnj.d t5, t3, s10<br> [0x80000e38]:csrrs gp, fcsr, zero<br> [0x80000e3c]:sw t5, 1248(ra)<br>                                 |
|  53|[0x80011bd8]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80000e74]:fsgnj.d t5, t3, s10<br> [0x80000e78]:csrrs gp, fcsr, zero<br> [0x80000e7c]:sw t5, 1280(ra)<br>                                 |
|  54|[0x80011bf8]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80000eb4]:fsgnj.d t5, t3, s10<br> [0x80000eb8]:csrrs gp, fcsr, zero<br> [0x80000ebc]:sw t5, 1312(ra)<br>                                 |
|  55|[0x80011c18]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80000ef8]:fsgnj.d t5, t3, s10<br> [0x80000efc]:csrrs gp, fcsr, zero<br> [0x80000f00]:sw t5, 1344(ra)<br>                                 |
|  56|[0x80011c38]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80000f3c]:fsgnj.d t5, t3, s10<br> [0x80000f40]:csrrs gp, fcsr, zero<br> [0x80000f44]:sw t5, 1376(ra)<br>                                 |
|  57|[0x80011c58]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80000f7c]:fsgnj.d t5, t3, s10<br> [0x80000f80]:csrrs gp, fcsr, zero<br> [0x80000f84]:sw t5, 1408(ra)<br>                                 |
|  58|[0x80011c78]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80000fbc]:fsgnj.d t5, t3, s10<br> [0x80000fc0]:csrrs gp, fcsr, zero<br> [0x80000fc4]:sw t5, 1440(ra)<br>                                 |
|  59|[0x80011c98]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80000ffc]:fsgnj.d t5, t3, s10<br> [0x80001000]:csrrs gp, fcsr, zero<br> [0x80001004]:sw t5, 1472(ra)<br>                                 |
|  60|[0x80011cb8]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000103c]:fsgnj.d t5, t3, s10<br> [0x80001040]:csrrs gp, fcsr, zero<br> [0x80001044]:sw t5, 1504(ra)<br>                                 |
|  61|[0x80011cd8]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80001080]:fsgnj.d t5, t3, s10<br> [0x80001084]:csrrs gp, fcsr, zero<br> [0x80001088]:sw t5, 1536(ra)<br>                                 |
|  62|[0x80011cf8]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x800010c4]:fsgnj.d t5, t3, s10<br> [0x800010c8]:csrrs gp, fcsr, zero<br> [0x800010cc]:sw t5, 1568(ra)<br>                                 |
|  63|[0x80011d18]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001104]:fsgnj.d t5, t3, s10<br> [0x80001108]:csrrs gp, fcsr, zero<br> [0x8000110c]:sw t5, 1600(ra)<br>                                 |
|  64|[0x80011d38]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001144]:fsgnj.d t5, t3, s10<br> [0x80001148]:csrrs gp, fcsr, zero<br> [0x8000114c]:sw t5, 1632(ra)<br>                                 |
|  65|[0x80011d58]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001184]:fsgnj.d t5, t3, s10<br> [0x80001188]:csrrs gp, fcsr, zero<br> [0x8000118c]:sw t5, 1664(ra)<br>                                 |
|  66|[0x80011d78]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800011c4]:fsgnj.d t5, t3, s10<br> [0x800011c8]:csrrs gp, fcsr, zero<br> [0x800011cc]:sw t5, 1696(ra)<br>                                 |
|  67|[0x80011d98]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001204]:fsgnj.d t5, t3, s10<br> [0x80001208]:csrrs gp, fcsr, zero<br> [0x8000120c]:sw t5, 1728(ra)<br>                                 |
|  68|[0x80011db8]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001244]:fsgnj.d t5, t3, s10<br> [0x80001248]:csrrs gp, fcsr, zero<br> [0x8000124c]:sw t5, 1760(ra)<br>                                 |
|  69|[0x80011dd8]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001284]:fsgnj.d t5, t3, s10<br> [0x80001288]:csrrs gp, fcsr, zero<br> [0x8000128c]:sw t5, 1792(ra)<br>                                 |
|  70|[0x80011df8]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x800012c4]:fsgnj.d t5, t3, s10<br> [0x800012c8]:csrrs gp, fcsr, zero<br> [0x800012cc]:sw t5, 1824(ra)<br>                                 |
|  71|[0x80011e18]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001304]:fsgnj.d t5, t3, s10<br> [0x80001308]:csrrs gp, fcsr, zero<br> [0x8000130c]:sw t5, 1856(ra)<br>                                 |
|  72|[0x80011e38]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001344]:fsgnj.d t5, t3, s10<br> [0x80001348]:csrrs gp, fcsr, zero<br> [0x8000134c]:sw t5, 1888(ra)<br>                                 |
|  73|[0x80011e58]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001384]:fsgnj.d t5, t3, s10<br> [0x80001388]:csrrs gp, fcsr, zero<br> [0x8000138c]:sw t5, 1920(ra)<br>                                 |
|  74|[0x80011e78]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800013c4]:fsgnj.d t5, t3, s10<br> [0x800013c8]:csrrs gp, fcsr, zero<br> [0x800013cc]:sw t5, 1952(ra)<br>                                 |
|  75|[0x80011e98]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001404]:fsgnj.d t5, t3, s10<br> [0x80001408]:csrrs gp, fcsr, zero<br> [0x8000140c]:sw t5, 1984(ra)<br>                                 |
|  76|[0x80011eb8]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001444]:fsgnj.d t5, t3, s10<br> [0x80001448]:csrrs gp, fcsr, zero<br> [0x8000144c]:sw t5, 2016(ra)<br>                                 |
|  77|[0x80011ed8]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001484]:fsgnj.d t5, t3, s10<br> [0x80001488]:csrrs gp, fcsr, zero<br> [0x8000148c]:addi ra, ra, 2040<br> [0x80001490]:sw t5, 8(ra)<br> |
|  78|[0x80011ef8]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x800014c8]:fsgnj.d t5, t3, s10<br> [0x800014cc]:csrrs gp, fcsr, zero<br> [0x800014d0]:sw t5, 40(ra)<br>                                   |
|  79|[0x80011f18]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x8000150c]:fsgnj.d t5, t3, s10<br> [0x80001510]:csrrs gp, fcsr, zero<br> [0x80001514]:sw t5, 72(ra)<br>                                   |
|  80|[0x80011f38]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80001550]:fsgnj.d t5, t3, s10<br> [0x80001554]:csrrs gp, fcsr, zero<br> [0x80001558]:sw t5, 104(ra)<br>                                  |
|  81|[0x80011f58]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001590]:fsgnj.d t5, t3, s10<br> [0x80001594]:csrrs gp, fcsr, zero<br> [0x80001598]:sw t5, 136(ra)<br>                                  |
|  82|[0x80011f78]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800015d0]:fsgnj.d t5, t3, s10<br> [0x800015d4]:csrrs gp, fcsr, zero<br> [0x800015d8]:sw t5, 168(ra)<br>                                  |
|  83|[0x80011f98]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001610]:fsgnj.d t5, t3, s10<br> [0x80001614]:csrrs gp, fcsr, zero<br> [0x80001618]:sw t5, 200(ra)<br>                                  |
|  84|[0x80011fb8]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001650]:fsgnj.d t5, t3, s10<br> [0x80001654]:csrrs gp, fcsr, zero<br> [0x80001658]:sw t5, 232(ra)<br>                                  |
|  85|[0x80011fd8]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80001694]:fsgnj.d t5, t3, s10<br> [0x80001698]:csrrs gp, fcsr, zero<br> [0x8000169c]:sw t5, 264(ra)<br>                                  |
|  86|[0x80011ff8]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x800016d8]:fsgnj.d t5, t3, s10<br> [0x800016dc]:csrrs gp, fcsr, zero<br> [0x800016e0]:sw t5, 296(ra)<br>                                  |
|  87|[0x80012018]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001718]:fsgnj.d t5, t3, s10<br> [0x8000171c]:csrrs gp, fcsr, zero<br> [0x80001720]:sw t5, 328(ra)<br>                                  |
|  88|[0x80012038]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001758]:fsgnj.d t5, t3, s10<br> [0x8000175c]:csrrs gp, fcsr, zero<br> [0x80001760]:sw t5, 360(ra)<br>                                  |
|  89|[0x80012058]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001798]:fsgnj.d t5, t3, s10<br> [0x8000179c]:csrrs gp, fcsr, zero<br> [0x800017a0]:sw t5, 392(ra)<br>                                  |
|  90|[0x80012078]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800017d8]:fsgnj.d t5, t3, s10<br> [0x800017dc]:csrrs gp, fcsr, zero<br> [0x800017e0]:sw t5, 424(ra)<br>                                  |
|  91|[0x80012098]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001818]:fsgnj.d t5, t3, s10<br> [0x8000181c]:csrrs gp, fcsr, zero<br> [0x80001820]:sw t5, 456(ra)<br>                                  |
|  92|[0x800120b8]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001858]:fsgnj.d t5, t3, s10<br> [0x8000185c]:csrrs gp, fcsr, zero<br> [0x80001860]:sw t5, 488(ra)<br>                                  |
|  93|[0x800120d8]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001898]:fsgnj.d t5, t3, s10<br> [0x8000189c]:csrrs gp, fcsr, zero<br> [0x800018a0]:sw t5, 520(ra)<br>                                  |
|  94|[0x800120f8]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x800018d8]:fsgnj.d t5, t3, s10<br> [0x800018dc]:csrrs gp, fcsr, zero<br> [0x800018e0]:sw t5, 552(ra)<br>                                  |
|  95|[0x80012118]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001918]:fsgnj.d t5, t3, s10<br> [0x8000191c]:csrrs gp, fcsr, zero<br> [0x80001920]:sw t5, 584(ra)<br>                                  |
|  96|[0x80012138]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001958]:fsgnj.d t5, t3, s10<br> [0x8000195c]:csrrs gp, fcsr, zero<br> [0x80001960]:sw t5, 616(ra)<br>                                  |
|  97|[0x80012158]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001998]:fsgnj.d t5, t3, s10<br> [0x8000199c]:csrrs gp, fcsr, zero<br> [0x800019a0]:sw t5, 648(ra)<br>                                  |
|  98|[0x80012178]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800019d8]:fsgnj.d t5, t3, s10<br> [0x800019dc]:csrrs gp, fcsr, zero<br> [0x800019e0]:sw t5, 680(ra)<br>                                  |
|  99|[0x80012198]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001a18]:fsgnj.d t5, t3, s10<br> [0x80001a1c]:csrrs gp, fcsr, zero<br> [0x80001a20]:sw t5, 712(ra)<br>                                  |
| 100|[0x800121b8]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001a58]:fsgnj.d t5, t3, s10<br> [0x80001a5c]:csrrs gp, fcsr, zero<br> [0x80001a60]:sw t5, 744(ra)<br>                                  |
| 101|[0x800121d8]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001a98]:fsgnj.d t5, t3, s10<br> [0x80001a9c]:csrrs gp, fcsr, zero<br> [0x80001aa0]:sw t5, 776(ra)<br>                                  |
| 102|[0x800121f8]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001ad8]:fsgnj.d t5, t3, s10<br> [0x80001adc]:csrrs gp, fcsr, zero<br> [0x80001ae0]:sw t5, 808(ra)<br>                                  |
| 103|[0x80012218]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80001b1c]:fsgnj.d t5, t3, s10<br> [0x80001b20]:csrrs gp, fcsr, zero<br> [0x80001b24]:sw t5, 840(ra)<br>                                  |
| 104|[0x80012238]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80001b60]:fsgnj.d t5, t3, s10<br> [0x80001b64]:csrrs gp, fcsr, zero<br> [0x80001b68]:sw t5, 872(ra)<br>                                  |
| 105|[0x80012258]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001ba0]:fsgnj.d t5, t3, s10<br> [0x80001ba4]:csrrs gp, fcsr, zero<br> [0x80001ba8]:sw t5, 904(ra)<br>                                  |
| 106|[0x80012278]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001be0]:fsgnj.d t5, t3, s10<br> [0x80001be4]:csrrs gp, fcsr, zero<br> [0x80001be8]:sw t5, 936(ra)<br>                                  |
| 107|[0x80012298]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001c20]:fsgnj.d t5, t3, s10<br> [0x80001c24]:csrrs gp, fcsr, zero<br> [0x80001c28]:sw t5, 968(ra)<br>                                  |
| 108|[0x800122b8]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001c60]:fsgnj.d t5, t3, s10<br> [0x80001c64]:csrrs gp, fcsr, zero<br> [0x80001c68]:sw t5, 1000(ra)<br>                                 |
| 109|[0x800122d8]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80001ca4]:fsgnj.d t5, t3, s10<br> [0x80001ca8]:csrrs gp, fcsr, zero<br> [0x80001cac]:sw t5, 1032(ra)<br>                                 |
| 110|[0x800122f8]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80001ce8]:fsgnj.d t5, t3, s10<br> [0x80001cec]:csrrs gp, fcsr, zero<br> [0x80001cf0]:sw t5, 1064(ra)<br>                                 |
| 111|[0x80012318]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001d28]:fsgnj.d t5, t3, s10<br> [0x80001d2c]:csrrs gp, fcsr, zero<br> [0x80001d30]:sw t5, 1096(ra)<br>                                 |
| 112|[0x80012338]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001d68]:fsgnj.d t5, t3, s10<br> [0x80001d6c]:csrrs gp, fcsr, zero<br> [0x80001d70]:sw t5, 1128(ra)<br>                                 |
| 113|[0x80012358]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001da8]:fsgnj.d t5, t3, s10<br> [0x80001dac]:csrrs gp, fcsr, zero<br> [0x80001db0]:sw t5, 1160(ra)<br>                                 |
| 114|[0x80012378]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001de8]:fsgnj.d t5, t3, s10<br> [0x80001dec]:csrrs gp, fcsr, zero<br> [0x80001df0]:sw t5, 1192(ra)<br>                                 |
| 115|[0x80012398]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001e28]:fsgnj.d t5, t3, s10<br> [0x80001e2c]:csrrs gp, fcsr, zero<br> [0x80001e30]:sw t5, 1224(ra)<br>                                 |
| 116|[0x800123b8]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001e68]:fsgnj.d t5, t3, s10<br> [0x80001e6c]:csrrs gp, fcsr, zero<br> [0x80001e70]:sw t5, 1256(ra)<br>                                 |
| 117|[0x800123d8]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001ea8]:fsgnj.d t5, t3, s10<br> [0x80001eac]:csrrs gp, fcsr, zero<br> [0x80001eb0]:sw t5, 1288(ra)<br>                                 |
| 118|[0x800123f8]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001ee8]:fsgnj.d t5, t3, s10<br> [0x80001eec]:csrrs gp, fcsr, zero<br> [0x80001ef0]:sw t5, 1320(ra)<br>                                 |
| 119|[0x80012418]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001f28]:fsgnj.d t5, t3, s10<br> [0x80001f2c]:csrrs gp, fcsr, zero<br> [0x80001f30]:sw t5, 1352(ra)<br>                                 |
| 120|[0x80012438]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001f68]:fsgnj.d t5, t3, s10<br> [0x80001f6c]:csrrs gp, fcsr, zero<br> [0x80001f70]:sw t5, 1384(ra)<br>                                 |
| 121|[0x80012458]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001fa8]:fsgnj.d t5, t3, s10<br> [0x80001fac]:csrrs gp, fcsr, zero<br> [0x80001fb0]:sw t5, 1416(ra)<br>                                 |
| 122|[0x80012478]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80001fe8]:fsgnj.d t5, t3, s10<br> [0x80001fec]:csrrs gp, fcsr, zero<br> [0x80001ff0]:sw t5, 1448(ra)<br>                                 |
| 123|[0x80012498]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80002028]:fsgnj.d t5, t3, s10<br> [0x8000202c]:csrrs gp, fcsr, zero<br> [0x80002030]:sw t5, 1480(ra)<br>                                 |
| 124|[0x800124b8]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80002068]:fsgnj.d t5, t3, s10<br> [0x8000206c]:csrrs gp, fcsr, zero<br> [0x80002070]:sw t5, 1512(ra)<br>                                 |
| 125|[0x800124d8]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x800020a8]:fsgnj.d t5, t3, s10<br> [0x800020ac]:csrrs gp, fcsr, zero<br> [0x800020b0]:sw t5, 1544(ra)<br>                                 |
| 126|[0x800124f8]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x800020e8]:fsgnj.d t5, t3, s10<br> [0x800020ec]:csrrs gp, fcsr, zero<br> [0x800020f0]:sw t5, 1576(ra)<br>                                 |
| 127|[0x80012518]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x8000212c]:fsgnj.d t5, t3, s10<br> [0x80002130]:csrrs gp, fcsr, zero<br> [0x80002134]:sw t5, 1608(ra)<br>                                 |
| 128|[0x80012538]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80002170]:fsgnj.d t5, t3, s10<br> [0x80002174]:csrrs gp, fcsr, zero<br> [0x80002178]:sw t5, 1640(ra)<br>                                 |
| 129|[0x80012558]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800021b0]:fsgnj.d t5, t3, s10<br> [0x800021b4]:csrrs gp, fcsr, zero<br> [0x800021b8]:sw t5, 1672(ra)<br>                                 |
| 130|[0x80012578]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800021f0]:fsgnj.d t5, t3, s10<br> [0x800021f4]:csrrs gp, fcsr, zero<br> [0x800021f8]:sw t5, 1704(ra)<br>                                 |
| 131|[0x80012598]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80002230]:fsgnj.d t5, t3, s10<br> [0x80002234]:csrrs gp, fcsr, zero<br> [0x80002238]:sw t5, 1736(ra)<br>                                 |
| 132|[0x800125b8]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80002270]:fsgnj.d t5, t3, s10<br> [0x80002274]:csrrs gp, fcsr, zero<br> [0x80002278]:sw t5, 1768(ra)<br>                                 |
| 133|[0x800125d8]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x800022b4]:fsgnj.d t5, t3, s10<br> [0x800022b8]:csrrs gp, fcsr, zero<br> [0x800022bc]:sw t5, 1800(ra)<br>                                 |
| 134|[0x800125f8]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x800022f8]:fsgnj.d t5, t3, s10<br> [0x800022fc]:csrrs gp, fcsr, zero<br> [0x80002300]:sw t5, 1832(ra)<br>                                 |
| 135|[0x80012618]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80002338]:fsgnj.d t5, t3, s10<br> [0x8000233c]:csrrs gp, fcsr, zero<br> [0x80002340]:sw t5, 1864(ra)<br>                                 |
| 136|[0x80012638]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80002378]:fsgnj.d t5, t3, s10<br> [0x8000237c]:csrrs gp, fcsr, zero<br> [0x80002380]:sw t5, 1896(ra)<br>                                 |
| 137|[0x80012658]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800023b8]:fsgnj.d t5, t3, s10<br> [0x800023bc]:csrrs gp, fcsr, zero<br> [0x800023c0]:sw t5, 1928(ra)<br>                                 |
| 138|[0x80012678]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800023f8]:fsgnj.d t5, t3, s10<br> [0x800023fc]:csrrs gp, fcsr, zero<br> [0x80002400]:sw t5, 1960(ra)<br>                                 |
| 139|[0x80012698]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80002438]:fsgnj.d t5, t3, s10<br> [0x8000243c]:csrrs gp, fcsr, zero<br> [0x80002440]:sw t5, 1992(ra)<br>                                 |
| 140|[0x800126b8]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80002478]:fsgnj.d t5, t3, s10<br> [0x8000247c]:csrrs gp, fcsr, zero<br> [0x80002480]:sw t5, 2024(ra)<br>                                 |
| 141|[0x800126d8]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x800024fc]:fsgnj.d t5, t3, s10<br> [0x80002500]:csrrs gp, fcsr, zero<br> [0x80002504]:sw t5, 16(ra)<br>                                   |
| 142|[0x800126f8]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000257c]:fsgnj.d t5, t3, s10<br> [0x80002580]:csrrs gp, fcsr, zero<br> [0x80002584]:sw t5, 48(ra)<br>                                   |
| 143|[0x80012718]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800025fc]:fsgnj.d t5, t3, s10<br> [0x80002600]:csrrs gp, fcsr, zero<br> [0x80002604]:sw t5, 80(ra)<br>                                   |
| 144|[0x80012738]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000267c]:fsgnj.d t5, t3, s10<br> [0x80002680]:csrrs gp, fcsr, zero<br> [0x80002684]:sw t5, 112(ra)<br>                                  |
| 145|[0x80012758]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80002700]:fsgnj.d t5, t3, s10<br> [0x80002704]:csrrs gp, fcsr, zero<br> [0x80002708]:sw t5, 144(ra)<br>                                  |
| 146|[0x80012778]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80002784]:fsgnj.d t5, t3, s10<br> [0x80002788]:csrrs gp, fcsr, zero<br> [0x8000278c]:sw t5, 176(ra)<br>                                  |
| 147|[0x80012798]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80002808]:fsgnj.d t5, t3, s10<br> [0x8000280c]:csrrs gp, fcsr, zero<br> [0x80002810]:sw t5, 208(ra)<br>                                  |
| 148|[0x800127b8]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000288c]:fsgnj.d t5, t3, s10<br> [0x80002890]:csrrs gp, fcsr, zero<br> [0x80002894]:sw t5, 240(ra)<br>                                  |
| 149|[0x800127d8]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80002910]:fsgnj.d t5, t3, s10<br> [0x80002914]:csrrs gp, fcsr, zero<br> [0x80002918]:sw t5, 272(ra)<br>                                  |
| 150|[0x800127f8]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80002994]:fsgnj.d t5, t3, s10<br> [0x80002998]:csrrs gp, fcsr, zero<br> [0x8000299c]:sw t5, 304(ra)<br>                                  |
| 151|[0x80012818]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80002a1c]:fsgnj.d t5, t3, s10<br> [0x80002a20]:csrrs gp, fcsr, zero<br> [0x80002a24]:sw t5, 336(ra)<br>                                  |
| 152|[0x80012838]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80002aa4]:fsgnj.d t5, t3, s10<br> [0x80002aa8]:csrrs gp, fcsr, zero<br> [0x80002aac]:sw t5, 368(ra)<br>                                  |
| 153|[0x80012858]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80002b28]:fsgnj.d t5, t3, s10<br> [0x80002b2c]:csrrs gp, fcsr, zero<br> [0x80002b30]:sw t5, 400(ra)<br>                                  |
| 154|[0x80012878]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80002bac]:fsgnj.d t5, t3, s10<br> [0x80002bb0]:csrrs gp, fcsr, zero<br> [0x80002bb4]:sw t5, 432(ra)<br>                                  |
| 155|[0x80012898]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80002c30]:fsgnj.d t5, t3, s10<br> [0x80002c34]:csrrs gp, fcsr, zero<br> [0x80002c38]:sw t5, 464(ra)<br>                                  |
| 156|[0x800128b8]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80002cb4]:fsgnj.d t5, t3, s10<br> [0x80002cb8]:csrrs gp, fcsr, zero<br> [0x80002cbc]:sw t5, 496(ra)<br>                                  |
| 157|[0x800128d8]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80002d3c]:fsgnj.d t5, t3, s10<br> [0x80002d40]:csrrs gp, fcsr, zero<br> [0x80002d44]:sw t5, 528(ra)<br>                                  |
| 158|[0x800128f8]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80002dc4]:fsgnj.d t5, t3, s10<br> [0x80002dc8]:csrrs gp, fcsr, zero<br> [0x80002dcc]:sw t5, 560(ra)<br>                                  |
| 159|[0x80012918]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80002e48]:fsgnj.d t5, t3, s10<br> [0x80002e4c]:csrrs gp, fcsr, zero<br> [0x80002e50]:sw t5, 592(ra)<br>                                  |
| 160|[0x80012938]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80002ecc]:fsgnj.d t5, t3, s10<br> [0x80002ed0]:csrrs gp, fcsr, zero<br> [0x80002ed4]:sw t5, 624(ra)<br>                                  |
| 161|[0x80012958]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80002f50]:fsgnj.d t5, t3, s10<br> [0x80002f54]:csrrs gp, fcsr, zero<br> [0x80002f58]:sw t5, 656(ra)<br>                                  |
| 162|[0x80012978]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80002fd4]:fsgnj.d t5, t3, s10<br> [0x80002fd8]:csrrs gp, fcsr, zero<br> [0x80002fdc]:sw t5, 688(ra)<br>                                  |
| 163|[0x80012998]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80003058]:fsgnj.d t5, t3, s10<br> [0x8000305c]:csrrs gp, fcsr, zero<br> [0x80003060]:sw t5, 720(ra)<br>                                  |
| 164|[0x800129b8]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x800030dc]:fsgnj.d t5, t3, s10<br> [0x800030e0]:csrrs gp, fcsr, zero<br> [0x800030e4]:sw t5, 752(ra)<br>                                  |
| 165|[0x800129d8]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80003160]:fsgnj.d t5, t3, s10<br> [0x80003164]:csrrs gp, fcsr, zero<br> [0x80003168]:sw t5, 784(ra)<br>                                  |
| 166|[0x800129f8]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x800031e4]:fsgnj.d t5, t3, s10<br> [0x800031e8]:csrrs gp, fcsr, zero<br> [0x800031ec]:sw t5, 816(ra)<br>                                  |
| 167|[0x80012a18]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80003268]:fsgnj.d t5, t3, s10<br> [0x8000326c]:csrrs gp, fcsr, zero<br> [0x80003270]:sw t5, 848(ra)<br>                                  |
| 168|[0x80012a38]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800032ec]:fsgnj.d t5, t3, s10<br> [0x800032f0]:csrrs gp, fcsr, zero<br> [0x800032f4]:sw t5, 880(ra)<br>                                  |
| 169|[0x80012a58]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80003370]:fsgnj.d t5, t3, s10<br> [0x80003374]:csrrs gp, fcsr, zero<br> [0x80003378]:sw t5, 912(ra)<br>                                  |
| 170|[0x80012a78]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800033f4]:fsgnj.d t5, t3, s10<br> [0x800033f8]:csrrs gp, fcsr, zero<br> [0x800033fc]:sw t5, 944(ra)<br>                                  |
| 171|[0x80012a98]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80003478]:fsgnj.d t5, t3, s10<br> [0x8000347c]:csrrs gp, fcsr, zero<br> [0x80003480]:sw t5, 976(ra)<br>                                  |
| 172|[0x80012ab8]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x800034fc]:fsgnj.d t5, t3, s10<br> [0x80003500]:csrrs gp, fcsr, zero<br> [0x80003504]:sw t5, 1008(ra)<br>                                 |
| 173|[0x80012ad8]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80003580]:fsgnj.d t5, t3, s10<br> [0x80003584]:csrrs gp, fcsr, zero<br> [0x80003588]:sw t5, 1040(ra)<br>                                 |
| 174|[0x80012af8]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80003604]:fsgnj.d t5, t3, s10<br> [0x80003608]:csrrs gp, fcsr, zero<br> [0x8000360c]:sw t5, 1072(ra)<br>                                 |
| 175|[0x80012b18]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x8000368c]:fsgnj.d t5, t3, s10<br> [0x80003690]:csrrs gp, fcsr, zero<br> [0x80003694]:sw t5, 1104(ra)<br>                                 |
| 176|[0x80012b38]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80003714]:fsgnj.d t5, t3, s10<br> [0x80003718]:csrrs gp, fcsr, zero<br> [0x8000371c]:sw t5, 1136(ra)<br>                                 |
| 177|[0x80012b58]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80003798]:fsgnj.d t5, t3, s10<br> [0x8000379c]:csrrs gp, fcsr, zero<br> [0x800037a0]:sw t5, 1168(ra)<br>                                 |
| 178|[0x80012b78]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000381c]:fsgnj.d t5, t3, s10<br> [0x80003820]:csrrs gp, fcsr, zero<br> [0x80003824]:sw t5, 1200(ra)<br>                                 |
| 179|[0x80012b98]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x800038a0]:fsgnj.d t5, t3, s10<br> [0x800038a4]:csrrs gp, fcsr, zero<br> [0x800038a8]:sw t5, 1232(ra)<br>                                 |
| 180|[0x80012bb8]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80003924]:fsgnj.d t5, t3, s10<br> [0x80003928]:csrrs gp, fcsr, zero<br> [0x8000392c]:sw t5, 1264(ra)<br>                                 |
| 181|[0x80012bd8]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x800039ac]:fsgnj.d t5, t3, s10<br> [0x800039b0]:csrrs gp, fcsr, zero<br> [0x800039b4]:sw t5, 1296(ra)<br>                                 |
| 182|[0x80012bf8]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80003a34]:fsgnj.d t5, t3, s10<br> [0x80003a38]:csrrs gp, fcsr, zero<br> [0x80003a3c]:sw t5, 1328(ra)<br>                                 |
| 183|[0x80012c18]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80003ab8]:fsgnj.d t5, t3, s10<br> [0x80003abc]:csrrs gp, fcsr, zero<br> [0x80003ac0]:sw t5, 1360(ra)<br>                                 |
| 184|[0x80012c38]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80003b3c]:fsgnj.d t5, t3, s10<br> [0x80003b40]:csrrs gp, fcsr, zero<br> [0x80003b44]:sw t5, 1392(ra)<br>                                 |
| 185|[0x80012c58]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80003bc0]:fsgnj.d t5, t3, s10<br> [0x80003bc4]:csrrs gp, fcsr, zero<br> [0x80003bc8]:sw t5, 1424(ra)<br>                                 |
| 186|[0x80012c78]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80003c44]:fsgnj.d t5, t3, s10<br> [0x80003c48]:csrrs gp, fcsr, zero<br> [0x80003c4c]:sw t5, 1456(ra)<br>                                 |
| 187|[0x80012c98]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80003cc8]:fsgnj.d t5, t3, s10<br> [0x80003ccc]:csrrs gp, fcsr, zero<br> [0x80003cd0]:sw t5, 1488(ra)<br>                                 |
| 188|[0x80012cb8]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80003d4c]:fsgnj.d t5, t3, s10<br> [0x80003d50]:csrrs gp, fcsr, zero<br> [0x80003d54]:sw t5, 1520(ra)<br>                                 |
| 189|[0x80012cd8]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80003dd0]:fsgnj.d t5, t3, s10<br> [0x80003dd4]:csrrs gp, fcsr, zero<br> [0x80003dd8]:sw t5, 1552(ra)<br>                                 |
| 190|[0x80012cf8]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80003e54]:fsgnj.d t5, t3, s10<br> [0x80003e58]:csrrs gp, fcsr, zero<br> [0x80003e5c]:sw t5, 1584(ra)<br>                                 |
| 191|[0x80012d18]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80003ed8]:fsgnj.d t5, t3, s10<br> [0x80003edc]:csrrs gp, fcsr, zero<br> [0x80003ee0]:sw t5, 1616(ra)<br>                                 |
| 192|[0x80012d38]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80003f5c]:fsgnj.d t5, t3, s10<br> [0x80003f60]:csrrs gp, fcsr, zero<br> [0x80003f64]:sw t5, 1648(ra)<br>                                 |
| 193|[0x80012d58]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80003fdc]:fsgnj.d t5, t3, s10<br> [0x80003fe0]:csrrs gp, fcsr, zero<br> [0x80003fe4]:sw t5, 1680(ra)<br>                                 |
| 194|[0x80012d78]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000405c]:fsgnj.d t5, t3, s10<br> [0x80004060]:csrrs gp, fcsr, zero<br> [0x80004064]:sw t5, 1712(ra)<br>                                 |
| 195|[0x80012d98]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x800040dc]:fsgnj.d t5, t3, s10<br> [0x800040e0]:csrrs gp, fcsr, zero<br> [0x800040e4]:sw t5, 1744(ra)<br>                                 |
| 196|[0x80012db8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000415c]:fsgnj.d t5, t3, s10<br> [0x80004160]:csrrs gp, fcsr, zero<br> [0x80004164]:sw t5, 1776(ra)<br>                                 |
| 197|[0x80012dd8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x800041dc]:fsgnj.d t5, t3, s10<br> [0x800041e0]:csrrs gp, fcsr, zero<br> [0x800041e4]:sw t5, 1808(ra)<br>                                 |
| 198|[0x80012df8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000425c]:fsgnj.d t5, t3, s10<br> [0x80004260]:csrrs gp, fcsr, zero<br> [0x80004264]:sw t5, 1840(ra)<br>                                 |
| 199|[0x80012e18]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x800042e0]:fsgnj.d t5, t3, s10<br> [0x800042e4]:csrrs gp, fcsr, zero<br> [0x800042e8]:sw t5, 1872(ra)<br>                                 |
| 200|[0x80012e38]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80004364]:fsgnj.d t5, t3, s10<br> [0x80004368]:csrrs gp, fcsr, zero<br> [0x8000436c]:sw t5, 1904(ra)<br>                                 |
| 201|[0x80012e58]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800043e4]:fsgnj.d t5, t3, s10<br> [0x800043e8]:csrrs gp, fcsr, zero<br> [0x800043ec]:sw t5, 1936(ra)<br>                                 |
| 202|[0x80012e78]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80004464]:fsgnj.d t5, t3, s10<br> [0x80004468]:csrrs gp, fcsr, zero<br> [0x8000446c]:sw t5, 1968(ra)<br>                                 |
| 203|[0x80012e98]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x800044e4]:fsgnj.d t5, t3, s10<br> [0x800044e8]:csrrs gp, fcsr, zero<br> [0x800044ec]:sw t5, 2000(ra)<br>                                 |
| 204|[0x80012eb8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80004564]:fsgnj.d t5, t3, s10<br> [0x80004568]:csrrs gp, fcsr, zero<br> [0x8000456c]:sw t5, 2032(ra)<br>                                 |
| 205|[0x80012ed8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x800045ec]:fsgnj.d t5, t3, s10<br> [0x800045f0]:csrrs gp, fcsr, zero<br> [0x800045f4]:sw t5, 24(ra)<br>                                   |
| 206|[0x80012ef8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80004670]:fsgnj.d t5, t3, s10<br> [0x80004674]:csrrs gp, fcsr, zero<br> [0x80004678]:sw t5, 56(ra)<br>                                   |
| 207|[0x80012f18]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800046f0]:fsgnj.d t5, t3, s10<br> [0x800046f4]:csrrs gp, fcsr, zero<br> [0x800046f8]:sw t5, 88(ra)<br>                                   |
| 208|[0x80012f38]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80004770]:fsgnj.d t5, t3, s10<br> [0x80004774]:csrrs gp, fcsr, zero<br> [0x80004778]:sw t5, 120(ra)<br>                                  |
| 209|[0x80012f58]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800047f0]:fsgnj.d t5, t3, s10<br> [0x800047f4]:csrrs gp, fcsr, zero<br> [0x800047f8]:sw t5, 152(ra)<br>                                  |
| 210|[0x80012f78]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80004870]:fsgnj.d t5, t3, s10<br> [0x80004874]:csrrs gp, fcsr, zero<br> [0x80004878]:sw t5, 184(ra)<br>                                  |
| 211|[0x80012f98]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x800048f0]:fsgnj.d t5, t3, s10<br> [0x800048f4]:csrrs gp, fcsr, zero<br> [0x800048f8]:sw t5, 216(ra)<br>                                  |
| 212|[0x80012fb8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80004970]:fsgnj.d t5, t3, s10<br> [0x80004974]:csrrs gp, fcsr, zero<br> [0x80004978]:sw t5, 248(ra)<br>                                  |
| 213|[0x80012fd8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x800049f0]:fsgnj.d t5, t3, s10<br> [0x800049f4]:csrrs gp, fcsr, zero<br> [0x800049f8]:sw t5, 280(ra)<br>                                  |
| 214|[0x80012ff8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80004a70]:fsgnj.d t5, t3, s10<br> [0x80004a74]:csrrs gp, fcsr, zero<br> [0x80004a78]:sw t5, 312(ra)<br>                                  |
| 215|[0x80013018]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80004af0]:fsgnj.d t5, t3, s10<br> [0x80004af4]:csrrs gp, fcsr, zero<br> [0x80004af8]:sw t5, 344(ra)<br>                                  |
| 216|[0x80013038]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80004b70]:fsgnj.d t5, t3, s10<br> [0x80004b74]:csrrs gp, fcsr, zero<br> [0x80004b78]:sw t5, 376(ra)<br>                                  |
| 217|[0x80013058]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80004bf0]:fsgnj.d t5, t3, s10<br> [0x80004bf4]:csrrs gp, fcsr, zero<br> [0x80004bf8]:sw t5, 408(ra)<br>                                  |
| 218|[0x80013078]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80004c70]:fsgnj.d t5, t3, s10<br> [0x80004c74]:csrrs gp, fcsr, zero<br> [0x80004c78]:sw t5, 440(ra)<br>                                  |
| 219|[0x80013098]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80004cf0]:fsgnj.d t5, t3, s10<br> [0x80004cf4]:csrrs gp, fcsr, zero<br> [0x80004cf8]:sw t5, 472(ra)<br>                                  |
| 220|[0x800130b8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80004d70]:fsgnj.d t5, t3, s10<br> [0x80004d74]:csrrs gp, fcsr, zero<br> [0x80004d78]:sw t5, 504(ra)<br>                                  |
| 221|[0x800130d8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80004df0]:fsgnj.d t5, t3, s10<br> [0x80004df4]:csrrs gp, fcsr, zero<br> [0x80004df8]:sw t5, 536(ra)<br>                                  |
| 222|[0x800130f8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80004e70]:fsgnj.d t5, t3, s10<br> [0x80004e74]:csrrs gp, fcsr, zero<br> [0x80004e78]:sw t5, 568(ra)<br>                                  |
| 223|[0x80013118]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80004ef4]:fsgnj.d t5, t3, s10<br> [0x80004ef8]:csrrs gp, fcsr, zero<br> [0x80004efc]:sw t5, 600(ra)<br>                                  |
| 224|[0x80013138]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80004f78]:fsgnj.d t5, t3, s10<br> [0x80004f7c]:csrrs gp, fcsr, zero<br> [0x80004f80]:sw t5, 632(ra)<br>                                  |
| 225|[0x80013158]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80004ff8]:fsgnj.d t5, t3, s10<br> [0x80004ffc]:csrrs gp, fcsr, zero<br> [0x80005000]:sw t5, 664(ra)<br>                                  |
| 226|[0x80013178]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80005078]:fsgnj.d t5, t3, s10<br> [0x8000507c]:csrrs gp, fcsr, zero<br> [0x80005080]:sw t5, 696(ra)<br>                                  |
| 227|[0x80013198]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x800050f8]:fsgnj.d t5, t3, s10<br> [0x800050fc]:csrrs gp, fcsr, zero<br> [0x80005100]:sw t5, 728(ra)<br>                                  |
| 228|[0x800131b8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80005178]:fsgnj.d t5, t3, s10<br> [0x8000517c]:csrrs gp, fcsr, zero<br> [0x80005180]:sw t5, 760(ra)<br>                                  |
| 229|[0x800131d8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x800051fc]:fsgnj.d t5, t3, s10<br> [0x80005200]:csrrs gp, fcsr, zero<br> [0x80005204]:sw t5, 792(ra)<br>                                  |
| 230|[0x800131f8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80005280]:fsgnj.d t5, t3, s10<br> [0x80005284]:csrrs gp, fcsr, zero<br> [0x80005288]:sw t5, 824(ra)<br>                                  |
| 231|[0x80013218]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80005300]:fsgnj.d t5, t3, s10<br> [0x80005304]:csrrs gp, fcsr, zero<br> [0x80005308]:sw t5, 856(ra)<br>                                  |
| 232|[0x80013238]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80005380]:fsgnj.d t5, t3, s10<br> [0x80005384]:csrrs gp, fcsr, zero<br> [0x80005388]:sw t5, 888(ra)<br>                                  |
| 233|[0x80013258]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80005400]:fsgnj.d t5, t3, s10<br> [0x80005404]:csrrs gp, fcsr, zero<br> [0x80005408]:sw t5, 920(ra)<br>                                  |
| 234|[0x80013278]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80005480]:fsgnj.d t5, t3, s10<br> [0x80005484]:csrrs gp, fcsr, zero<br> [0x80005488]:sw t5, 952(ra)<br>                                  |
| 235|[0x80013298]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80005500]:fsgnj.d t5, t3, s10<br> [0x80005504]:csrrs gp, fcsr, zero<br> [0x80005508]:sw t5, 984(ra)<br>                                  |
| 236|[0x800132b8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80005580]:fsgnj.d t5, t3, s10<br> [0x80005584]:csrrs gp, fcsr, zero<br> [0x80005588]:sw t5, 1016(ra)<br>                                 |
| 237|[0x800132d8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80005600]:fsgnj.d t5, t3, s10<br> [0x80005604]:csrrs gp, fcsr, zero<br> [0x80005608]:sw t5, 1048(ra)<br>                                 |
| 238|[0x800132f8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80005680]:fsgnj.d t5, t3, s10<br> [0x80005684]:csrrs gp, fcsr, zero<br> [0x80005688]:sw t5, 1080(ra)<br>                                 |
| 239|[0x80013318]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80005700]:fsgnj.d t5, t3, s10<br> [0x80005704]:csrrs gp, fcsr, zero<br> [0x80005708]:sw t5, 1112(ra)<br>                                 |
| 240|[0x80013338]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80005780]:fsgnj.d t5, t3, s10<br> [0x80005784]:csrrs gp, fcsr, zero<br> [0x80005788]:sw t5, 1144(ra)<br>                                 |
| 241|[0x80013358]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80005800]:fsgnj.d t5, t3, s10<br> [0x80005804]:csrrs gp, fcsr, zero<br> [0x80005808]:sw t5, 1176(ra)<br>                                 |
| 242|[0x80013378]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80005880]:fsgnj.d t5, t3, s10<br> [0x80005884]:csrrs gp, fcsr, zero<br> [0x80005888]:sw t5, 1208(ra)<br>                                 |
| 243|[0x80013398]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80005900]:fsgnj.d t5, t3, s10<br> [0x80005904]:csrrs gp, fcsr, zero<br> [0x80005908]:sw t5, 1240(ra)<br>                                 |
| 244|[0x800133b8]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80005980]:fsgnj.d t5, t3, s10<br> [0x80005984]:csrrs gp, fcsr, zero<br> [0x80005988]:sw t5, 1272(ra)<br>                                 |
| 245|[0x800133d8]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80005a00]:fsgnj.d t5, t3, s10<br> [0x80005a04]:csrrs gp, fcsr, zero<br> [0x80005a08]:sw t5, 1304(ra)<br>                                 |
| 246|[0x800133f8]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80005a80]:fsgnj.d t5, t3, s10<br> [0x80005a84]:csrrs gp, fcsr, zero<br> [0x80005a88]:sw t5, 1336(ra)<br>                                 |
| 247|[0x80013418]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80005b04]:fsgnj.d t5, t3, s10<br> [0x80005b08]:csrrs gp, fcsr, zero<br> [0x80005b0c]:sw t5, 1368(ra)<br>                                 |
| 248|[0x80013438]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80005b88]:fsgnj.d t5, t3, s10<br> [0x80005b8c]:csrrs gp, fcsr, zero<br> [0x80005b90]:sw t5, 1400(ra)<br>                                 |
| 249|[0x80013458]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80005c08]:fsgnj.d t5, t3, s10<br> [0x80005c0c]:csrrs gp, fcsr, zero<br> [0x80005c10]:sw t5, 1432(ra)<br>                                 |
| 250|[0x80013478]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80005c88]:fsgnj.d t5, t3, s10<br> [0x80005c8c]:csrrs gp, fcsr, zero<br> [0x80005c90]:sw t5, 1464(ra)<br>                                 |
| 251|[0x80013498]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80005d08]:fsgnj.d t5, t3, s10<br> [0x80005d0c]:csrrs gp, fcsr, zero<br> [0x80005d10]:sw t5, 1496(ra)<br>                                 |
| 252|[0x800134b8]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80005d88]:fsgnj.d t5, t3, s10<br> [0x80005d8c]:csrrs gp, fcsr, zero<br> [0x80005d90]:sw t5, 1528(ra)<br>                                 |
| 253|[0x800134d8]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80005e0c]:fsgnj.d t5, t3, s10<br> [0x80005e10]:csrrs gp, fcsr, zero<br> [0x80005e14]:sw t5, 1560(ra)<br>                                 |
| 254|[0x800134f8]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80005e90]:fsgnj.d t5, t3, s10<br> [0x80005e94]:csrrs gp, fcsr, zero<br> [0x80005e98]:sw t5, 1592(ra)<br>                                 |
| 255|[0x80013518]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80005f10]:fsgnj.d t5, t3, s10<br> [0x80005f14]:csrrs gp, fcsr, zero<br> [0x80005f18]:sw t5, 1624(ra)<br>                                 |
| 256|[0x80013538]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80005f90]:fsgnj.d t5, t3, s10<br> [0x80005f94]:csrrs gp, fcsr, zero<br> [0x80005f98]:sw t5, 1656(ra)<br>                                 |
| 257|[0x80013558]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006010]:fsgnj.d t5, t3, s10<br> [0x80006014]:csrrs gp, fcsr, zero<br> [0x80006018]:sw t5, 1688(ra)<br>                                 |
| 258|[0x80013578]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006090]:fsgnj.d t5, t3, s10<br> [0x80006094]:csrrs gp, fcsr, zero<br> [0x80006098]:sw t5, 1720(ra)<br>                                 |
| 259|[0x80013598]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006110]:fsgnj.d t5, t3, s10<br> [0x80006114]:csrrs gp, fcsr, zero<br> [0x80006118]:sw t5, 1752(ra)<br>                                 |
| 260|[0x800135b8]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006190]:fsgnj.d t5, t3, s10<br> [0x80006194]:csrrs gp, fcsr, zero<br> [0x80006198]:sw t5, 1784(ra)<br>                                 |
| 261|[0x800135d8]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006210]:fsgnj.d t5, t3, s10<br> [0x80006214]:csrrs gp, fcsr, zero<br> [0x80006218]:sw t5, 1816(ra)<br>                                 |
| 262|[0x800135f8]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006290]:fsgnj.d t5, t3, s10<br> [0x80006294]:csrrs gp, fcsr, zero<br> [0x80006298]:sw t5, 1848(ra)<br>                                 |
| 263|[0x80013618]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006310]:fsgnj.d t5, t3, s10<br> [0x80006314]:csrrs gp, fcsr, zero<br> [0x80006318]:sw t5, 1880(ra)<br>                                 |
| 264|[0x80013638]<br>0x00000002<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006390]:fsgnj.d t5, t3, s10<br> [0x80006394]:csrrs gp, fcsr, zero<br> [0x80006398]:sw t5, 1912(ra)<br>                                 |
| 265|[0x80013658]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006410]:fsgnj.d t5, t3, s10<br> [0x80006414]:csrrs gp, fcsr, zero<br> [0x80006418]:sw t5, 1944(ra)<br>                                 |
| 266|[0x80013678]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006490]:fsgnj.d t5, t3, s10<br> [0x80006494]:csrrs gp, fcsr, zero<br> [0x80006498]:sw t5, 1976(ra)<br>                                 |
| 267|[0x80013698]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006510]:fsgnj.d t5, t3, s10<br> [0x80006514]:csrrs gp, fcsr, zero<br> [0x80006518]:sw t5, 2008(ra)<br>                                 |
| 268|[0x800136b8]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006590]:fsgnj.d t5, t3, s10<br> [0x80006594]:csrrs gp, fcsr, zero<br> [0x80006598]:sw t5, 2040(ra)<br>                                 |
| 269|[0x800126d8]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x800065dc]:fsgnj.d t5, t3, s10<br> [0x800065e0]:csrrs gp, fcsr, zero<br> [0x800065e4]:sw t5, 0(ra)<br>                                    |
| 270|[0x800126f8]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000661c]:fsgnj.d t5, t3, s10<br> [0x80006620]:csrrs gp, fcsr, zero<br> [0x80006624]:sw t5, 32(ra)<br>                                   |
| 271|[0x80012718]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80006660]:fsgnj.d t5, t3, s10<br> [0x80006664]:csrrs gp, fcsr, zero<br> [0x80006668]:sw t5, 64(ra)<br>                                   |
| 272|[0x80012738]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x800066a4]:fsgnj.d t5, t3, s10<br> [0x800066a8]:csrrs gp, fcsr, zero<br> [0x800066ac]:sw t5, 96(ra)<br>                                   |
| 273|[0x80012758]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800066e4]:fsgnj.d t5, t3, s10<br> [0x800066e8]:csrrs gp, fcsr, zero<br> [0x800066ec]:sw t5, 128(ra)<br>                                  |
| 274|[0x80012778]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006724]:fsgnj.d t5, t3, s10<br> [0x80006728]:csrrs gp, fcsr, zero<br> [0x8000672c]:sw t5, 160(ra)<br>                                  |
| 275|[0x80012798]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006764]:fsgnj.d t5, t3, s10<br> [0x80006768]:csrrs gp, fcsr, zero<br> [0x8000676c]:sw t5, 192(ra)<br>                                  |
| 276|[0x800127b8]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x800067a4]:fsgnj.d t5, t3, s10<br> [0x800067a8]:csrrs gp, fcsr, zero<br> [0x800067ac]:sw t5, 224(ra)<br>                                  |
| 277|[0x800127d8]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x800067e8]:fsgnj.d t5, t3, s10<br> [0x800067ec]:csrrs gp, fcsr, zero<br> [0x800067f0]:sw t5, 256(ra)<br>                                  |
| 278|[0x800127f8]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x8000682c]:fsgnj.d t5, t3, s10<br> [0x80006830]:csrrs gp, fcsr, zero<br> [0x80006834]:sw t5, 288(ra)<br>                                  |
| 279|[0x80012818]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000686c]:fsgnj.d t5, t3, s10<br> [0x80006870]:csrrs gp, fcsr, zero<br> [0x80006874]:sw t5, 320(ra)<br>                                  |
| 280|[0x80012838]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800068ac]:fsgnj.d t5, t3, s10<br> [0x800068b0]:csrrs gp, fcsr, zero<br> [0x800068b4]:sw t5, 352(ra)<br>                                  |
| 281|[0x80012858]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800068ec]:fsgnj.d t5, t3, s10<br> [0x800068f0]:csrrs gp, fcsr, zero<br> [0x800068f4]:sw t5, 384(ra)<br>                                  |
| 282|[0x80012878]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000692c]:fsgnj.d t5, t3, s10<br> [0x80006930]:csrrs gp, fcsr, zero<br> [0x80006934]:sw t5, 416(ra)<br>                                  |
| 283|[0x80012898]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000696c]:fsgnj.d t5, t3, s10<br> [0x80006970]:csrrs gp, fcsr, zero<br> [0x80006974]:sw t5, 448(ra)<br>                                  |
| 284|[0x800128b8]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x800069ac]:fsgnj.d t5, t3, s10<br> [0x800069b0]:csrrs gp, fcsr, zero<br> [0x800069b4]:sw t5, 480(ra)<br>                                  |
| 285|[0x800128d8]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x800069ec]:fsgnj.d t5, t3, s10<br> [0x800069f0]:csrrs gp, fcsr, zero<br> [0x800069f4]:sw t5, 512(ra)<br>                                  |
| 286|[0x800128f8]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006a2c]:fsgnj.d t5, t3, s10<br> [0x80006a30]:csrrs gp, fcsr, zero<br> [0x80006a34]:sw t5, 544(ra)<br>                                  |
| 287|[0x80012918]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006a6c]:fsgnj.d t5, t3, s10<br> [0x80006a70]:csrrs gp, fcsr, zero<br> [0x80006a74]:sw t5, 576(ra)<br>                                  |
| 288|[0x80012938]<br>0x00000002<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006aac]:fsgnj.d t5, t3, s10<br> [0x80006ab0]:csrrs gp, fcsr, zero<br> [0x80006ab4]:sw t5, 608(ra)<br>                                  |
| 289|[0x80012958]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006af0]:fsgnj.d t5, t3, s10<br> [0x80006af4]:csrrs gp, fcsr, zero<br> [0x80006af8]:sw t5, 640(ra)<br>                                  |
| 290|[0x80012978]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006b34]:fsgnj.d t5, t3, s10<br> [0x80006b38]:csrrs gp, fcsr, zero<br> [0x80006b3c]:sw t5, 672(ra)<br>                                  |
| 291|[0x80012998]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006b78]:fsgnj.d t5, t3, s10<br> [0x80006b7c]:csrrs gp, fcsr, zero<br> [0x80006b80]:sw t5, 704(ra)<br>                                  |
| 292|[0x800129b8]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006bbc]:fsgnj.d t5, t3, s10<br> [0x80006bc0]:csrrs gp, fcsr, zero<br> [0x80006bc4]:sw t5, 736(ra)<br>                                  |
| 293|[0x800129d8]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006c00]:fsgnj.d t5, t3, s10<br> [0x80006c04]:csrrs gp, fcsr, zero<br> [0x80006c08]:sw t5, 768(ra)<br>                                  |
| 294|[0x800129f8]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006c44]:fsgnj.d t5, t3, s10<br> [0x80006c48]:csrrs gp, fcsr, zero<br> [0x80006c4c]:sw t5, 800(ra)<br>                                  |
| 295|[0x80012a18]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80006c8c]:fsgnj.d t5, t3, s10<br> [0x80006c90]:csrrs gp, fcsr, zero<br> [0x80006c94]:sw t5, 832(ra)<br>                                  |
| 296|[0x80012a38]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80006cd4]:fsgnj.d t5, t3, s10<br> [0x80006cd8]:csrrs gp, fcsr, zero<br> [0x80006cdc]:sw t5, 864(ra)<br>                                  |
| 297|[0x80012a58]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006d18]:fsgnj.d t5, t3, s10<br> [0x80006d1c]:csrrs gp, fcsr, zero<br> [0x80006d20]:sw t5, 896(ra)<br>                                  |
| 298|[0x80012a78]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006d5c]:fsgnj.d t5, t3, s10<br> [0x80006d60]:csrrs gp, fcsr, zero<br> [0x80006d64]:sw t5, 928(ra)<br>                                  |
| 299|[0x80012a98]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006da0]:fsgnj.d t5, t3, s10<br> [0x80006da4]:csrrs gp, fcsr, zero<br> [0x80006da8]:sw t5, 960(ra)<br>                                  |
| 300|[0x80012ab8]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006de4]:fsgnj.d t5, t3, s10<br> [0x80006de8]:csrrs gp, fcsr, zero<br> [0x80006dec]:sw t5, 992(ra)<br>                                  |
| 301|[0x80012ad8]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80006e2c]:fsgnj.d t5, t3, s10<br> [0x80006e30]:csrrs gp, fcsr, zero<br> [0x80006e34]:sw t5, 1024(ra)<br>                                 |
| 302|[0x80012af8]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80006e74]:fsgnj.d t5, t3, s10<br> [0x80006e78]:csrrs gp, fcsr, zero<br> [0x80006e7c]:sw t5, 1056(ra)<br>                                 |
| 303|[0x80012b18]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006eb8]:fsgnj.d t5, t3, s10<br> [0x80006ebc]:csrrs gp, fcsr, zero<br> [0x80006ec0]:sw t5, 1088(ra)<br>                                 |
| 304|[0x80012b38]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006efc]:fsgnj.d t5, t3, s10<br> [0x80006f00]:csrrs gp, fcsr, zero<br> [0x80006f04]:sw t5, 1120(ra)<br>                                 |
| 305|[0x80012b58]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006f40]:fsgnj.d t5, t3, s10<br> [0x80006f44]:csrrs gp, fcsr, zero<br> [0x80006f48]:sw t5, 1152(ra)<br>                                 |
| 306|[0x80012b78]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006f84]:fsgnj.d t5, t3, s10<br> [0x80006f88]:csrrs gp, fcsr, zero<br> [0x80006f8c]:sw t5, 1184(ra)<br>                                 |
| 307|[0x80012b98]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80006fc8]:fsgnj.d t5, t3, s10<br> [0x80006fcc]:csrrs gp, fcsr, zero<br> [0x80006fd0]:sw t5, 1216(ra)<br>                                 |
| 308|[0x80012bb8]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000700c]:fsgnj.d t5, t3, s10<br> [0x80007010]:csrrs gp, fcsr, zero<br> [0x80007014]:sw t5, 1248(ra)<br>                                 |
| 309|[0x80012bd8]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007050]:fsgnj.d t5, t3, s10<br> [0x80007054]:csrrs gp, fcsr, zero<br> [0x80007058]:sw t5, 1280(ra)<br>                                 |
| 310|[0x80012bf8]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007094]:fsgnj.d t5, t3, s10<br> [0x80007098]:csrrs gp, fcsr, zero<br> [0x8000709c]:sw t5, 1312(ra)<br>                                 |
| 311|[0x80012c18]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800070d8]:fsgnj.d t5, t3, s10<br> [0x800070dc]:csrrs gp, fcsr, zero<br> [0x800070e0]:sw t5, 1344(ra)<br>                                 |
| 312|[0x80012c38]<br>0xFFFFFFFF<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000711c]:fsgnj.d t5, t3, s10<br> [0x80007120]:csrrs gp, fcsr, zero<br> [0x80007124]:sw t5, 1376(ra)<br>                                 |
| 313|[0x80012c58]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007160]:fsgnj.d t5, t3, s10<br> [0x80007164]:csrrs gp, fcsr, zero<br> [0x80007168]:sw t5, 1408(ra)<br>                                 |
| 314|[0x80012c78]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800071a4]:fsgnj.d t5, t3, s10<br> [0x800071a8]:csrrs gp, fcsr, zero<br> [0x800071ac]:sw t5, 1440(ra)<br>                                 |
| 315|[0x80012c98]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x800071e8]:fsgnj.d t5, t3, s10<br> [0x800071ec]:csrrs gp, fcsr, zero<br> [0x800071f0]:sw t5, 1472(ra)<br>                                 |
| 316|[0x80012cb8]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000722c]:fsgnj.d t5, t3, s10<br> [0x80007230]:csrrs gp, fcsr, zero<br> [0x80007234]:sw t5, 1504(ra)<br>                                 |
| 317|[0x80012cd8]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007270]:fsgnj.d t5, t3, s10<br> [0x80007274]:csrrs gp, fcsr, zero<br> [0x80007278]:sw t5, 1536(ra)<br>                                 |
| 318|[0x80012cf8]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x800072b4]:fsgnj.d t5, t3, s10<br> [0x800072b8]:csrrs gp, fcsr, zero<br> [0x800072bc]:sw t5, 1568(ra)<br>                                 |
| 319|[0x80012d18]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x800072fc]:fsgnj.d t5, t3, s10<br> [0x80007300]:csrrs gp, fcsr, zero<br> [0x80007304]:sw t5, 1600(ra)<br>                                 |
| 320|[0x80012d38]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80007344]:fsgnj.d t5, t3, s10<br> [0x80007348]:csrrs gp, fcsr, zero<br> [0x8000734c]:sw t5, 1632(ra)<br>                                 |
| 321|[0x80012d58]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007388]:fsgnj.d t5, t3, s10<br> [0x8000738c]:csrrs gp, fcsr, zero<br> [0x80007390]:sw t5, 1664(ra)<br>                                 |
| 322|[0x80012d78]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800073cc]:fsgnj.d t5, t3, s10<br> [0x800073d0]:csrrs gp, fcsr, zero<br> [0x800073d4]:sw t5, 1696(ra)<br>                                 |
| 323|[0x80012d98]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007410]:fsgnj.d t5, t3, s10<br> [0x80007414]:csrrs gp, fcsr, zero<br> [0x80007418]:sw t5, 1728(ra)<br>                                 |
| 324|[0x80012db8]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007454]:fsgnj.d t5, t3, s10<br> [0x80007458]:csrrs gp, fcsr, zero<br> [0x8000745c]:sw t5, 1760(ra)<br>                                 |
| 325|[0x80012dd8]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x8000749c]:fsgnj.d t5, t3, s10<br> [0x800074a0]:csrrs gp, fcsr, zero<br> [0x800074a4]:sw t5, 1792(ra)<br>                                 |
| 326|[0x80012df8]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x800074e4]:fsgnj.d t5, t3, s10<br> [0x800074e8]:csrrs gp, fcsr, zero<br> [0x800074ec]:sw t5, 1824(ra)<br>                                 |
| 327|[0x80012e18]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007528]:fsgnj.d t5, t3, s10<br> [0x8000752c]:csrrs gp, fcsr, zero<br> [0x80007530]:sw t5, 1856(ra)<br>                                 |
| 328|[0x80012e38]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000756c]:fsgnj.d t5, t3, s10<br> [0x80007570]:csrrs gp, fcsr, zero<br> [0x80007574]:sw t5, 1888(ra)<br>                                 |
| 329|[0x80012e58]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800075b0]:fsgnj.d t5, t3, s10<br> [0x800075b4]:csrrs gp, fcsr, zero<br> [0x800075b8]:sw t5, 1920(ra)<br>                                 |
| 330|[0x80012e78]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800075f4]:fsgnj.d t5, t3, s10<br> [0x800075f8]:csrrs gp, fcsr, zero<br> [0x800075fc]:sw t5, 1952(ra)<br>                                 |
| 331|[0x80012e98]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007638]:fsgnj.d t5, t3, s10<br> [0x8000763c]:csrrs gp, fcsr, zero<br> [0x80007640]:sw t5, 1984(ra)<br>                                 |
| 332|[0x80012eb8]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000767c]:fsgnj.d t5, t3, s10<br> [0x80007680]:csrrs gp, fcsr, zero<br> [0x80007684]:sw t5, 2016(ra)<br>                                 |
| 333|[0x80012ed8]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x800076c0]:fsgnj.d t5, t3, s10<br> [0x800076c4]:csrrs gp, fcsr, zero<br> [0x800076c8]:addi ra, ra, 2040<br> [0x800076cc]:sw t5, 8(ra)<br> |
| 334|[0x80012ef8]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007708]:fsgnj.d t5, t3, s10<br> [0x8000770c]:csrrs gp, fcsr, zero<br> [0x80007710]:sw t5, 40(ra)<br>                                   |
| 335|[0x80012f18]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000774c]:fsgnj.d t5, t3, s10<br> [0x80007750]:csrrs gp, fcsr, zero<br> [0x80007754]:sw t5, 72(ra)<br>                                   |
| 336|[0x80012f38]<br>0xFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007790]:fsgnj.d t5, t3, s10<br> [0x80007794]:csrrs gp, fcsr, zero<br> [0x80007798]:sw t5, 104(ra)<br>                                  |
| 337|[0x80012f58]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800077d0]:fsgnj.d t5, t3, s10<br> [0x800077d4]:csrrs gp, fcsr, zero<br> [0x800077d8]:sw t5, 136(ra)<br>                                  |
| 338|[0x80012f78]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007810]:fsgnj.d t5, t3, s10<br> [0x80007814]:csrrs gp, fcsr, zero<br> [0x80007818]:sw t5, 168(ra)<br>                                  |
| 339|[0x80012f98]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007850]:fsgnj.d t5, t3, s10<br> [0x80007854]:csrrs gp, fcsr, zero<br> [0x80007858]:sw t5, 200(ra)<br>                                  |
| 340|[0x80012fb8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007890]:fsgnj.d t5, t3, s10<br> [0x80007894]:csrrs gp, fcsr, zero<br> [0x80007898]:sw t5, 232(ra)<br>                                  |
| 341|[0x80012fd8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x800078d0]:fsgnj.d t5, t3, s10<br> [0x800078d4]:csrrs gp, fcsr, zero<br> [0x800078d8]:sw t5, 264(ra)<br>                                  |
| 342|[0x80012ff8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007910]:fsgnj.d t5, t3, s10<br> [0x80007914]:csrrs gp, fcsr, zero<br> [0x80007918]:sw t5, 296(ra)<br>                                  |
| 343|[0x80013018]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80007954]:fsgnj.d t5, t3, s10<br> [0x80007958]:csrrs gp, fcsr, zero<br> [0x8000795c]:sw t5, 328(ra)<br>                                  |
| 344|[0x80013038]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80007998]:fsgnj.d t5, t3, s10<br> [0x8000799c]:csrrs gp, fcsr, zero<br> [0x800079a0]:sw t5, 360(ra)<br>                                  |
| 345|[0x80013058]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800079d8]:fsgnj.d t5, t3, s10<br> [0x800079dc]:csrrs gp, fcsr, zero<br> [0x800079e0]:sw t5, 392(ra)<br>                                  |
| 346|[0x80013078]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007a18]:fsgnj.d t5, t3, s10<br> [0x80007a1c]:csrrs gp, fcsr, zero<br> [0x80007a20]:sw t5, 424(ra)<br>                                  |
| 347|[0x80013098]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007a58]:fsgnj.d t5, t3, s10<br> [0x80007a5c]:csrrs gp, fcsr, zero<br> [0x80007a60]:sw t5, 456(ra)<br>                                  |
| 348|[0x800130b8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007a98]:fsgnj.d t5, t3, s10<br> [0x80007a9c]:csrrs gp, fcsr, zero<br> [0x80007aa0]:sw t5, 488(ra)<br>                                  |
| 349|[0x800130d8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80007adc]:fsgnj.d t5, t3, s10<br> [0x80007ae0]:csrrs gp, fcsr, zero<br> [0x80007ae4]:sw t5, 520(ra)<br>                                  |
| 350|[0x800130f8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80007b20]:fsgnj.d t5, t3, s10<br> [0x80007b24]:csrrs gp, fcsr, zero<br> [0x80007b28]:sw t5, 552(ra)<br>                                  |
| 351|[0x80013118]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007b60]:fsgnj.d t5, t3, s10<br> [0x80007b64]:csrrs gp, fcsr, zero<br> [0x80007b68]:sw t5, 584(ra)<br>                                  |
| 352|[0x80013138]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007ba0]:fsgnj.d t5, t3, s10<br> [0x80007ba4]:csrrs gp, fcsr, zero<br> [0x80007ba8]:sw t5, 616(ra)<br>                                  |
| 353|[0x80013158]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007be0]:fsgnj.d t5, t3, s10<br> [0x80007be4]:csrrs gp, fcsr, zero<br> [0x80007be8]:sw t5, 648(ra)<br>                                  |
| 354|[0x80013178]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007c20]:fsgnj.d t5, t3, s10<br> [0x80007c24]:csrrs gp, fcsr, zero<br> [0x80007c28]:sw t5, 680(ra)<br>                                  |
| 355|[0x80013198]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007c60]:fsgnj.d t5, t3, s10<br> [0x80007c64]:csrrs gp, fcsr, zero<br> [0x80007c68]:sw t5, 712(ra)<br>                                  |
| 356|[0x800131b8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007ca0]:fsgnj.d t5, t3, s10<br> [0x80007ca4]:csrrs gp, fcsr, zero<br> [0x80007ca8]:sw t5, 744(ra)<br>                                  |
| 357|[0x800131d8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007ce0]:fsgnj.d t5, t3, s10<br> [0x80007ce4]:csrrs gp, fcsr, zero<br> [0x80007ce8]:sw t5, 776(ra)<br>                                  |
| 358|[0x800131f8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007d20]:fsgnj.d t5, t3, s10<br> [0x80007d24]:csrrs gp, fcsr, zero<br> [0x80007d28]:sw t5, 808(ra)<br>                                  |
| 359|[0x80013218]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007d60]:fsgnj.d t5, t3, s10<br> [0x80007d64]:csrrs gp, fcsr, zero<br> [0x80007d68]:sw t5, 840(ra)<br>                                  |
| 360|[0x80013238]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007da0]:fsgnj.d t5, t3, s10<br> [0x80007da4]:csrrs gp, fcsr, zero<br> [0x80007da8]:sw t5, 872(ra)<br>                                  |
| 361|[0x80013258]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007de0]:fsgnj.d t5, t3, s10<br> [0x80007de4]:csrrs gp, fcsr, zero<br> [0x80007de8]:sw t5, 904(ra)<br>                                  |
| 362|[0x80013278]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007e20]:fsgnj.d t5, t3, s10<br> [0x80007e24]:csrrs gp, fcsr, zero<br> [0x80007e28]:sw t5, 936(ra)<br>                                  |
| 363|[0x80013298]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007e60]:fsgnj.d t5, t3, s10<br> [0x80007e64]:csrrs gp, fcsr, zero<br> [0x80007e68]:sw t5, 968(ra)<br>                                  |
| 364|[0x800132b8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007ea0]:fsgnj.d t5, t3, s10<br> [0x80007ea4]:csrrs gp, fcsr, zero<br> [0x80007ea8]:sw t5, 1000(ra)<br>                                 |
| 365|[0x800132d8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007ee0]:fsgnj.d t5, t3, s10<br> [0x80007ee4]:csrrs gp, fcsr, zero<br> [0x80007ee8]:sw t5, 1032(ra)<br>                                 |
| 366|[0x800132f8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007f20]:fsgnj.d t5, t3, s10<br> [0x80007f24]:csrrs gp, fcsr, zero<br> [0x80007f28]:sw t5, 1064(ra)<br>                                 |
| 367|[0x80013318]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80007f64]:fsgnj.d t5, t3, s10<br> [0x80007f68]:csrrs gp, fcsr, zero<br> [0x80007f6c]:sw t5, 1096(ra)<br>                                 |
| 368|[0x80013338]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80007fa8]:fsgnj.d t5, t3, s10<br> [0x80007fac]:csrrs gp, fcsr, zero<br> [0x80007fb0]:sw t5, 1128(ra)<br>                                 |
| 369|[0x80013358]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80007fe8]:fsgnj.d t5, t3, s10<br> [0x80007fec]:csrrs gp, fcsr, zero<br> [0x80007ff0]:sw t5, 1160(ra)<br>                                 |
| 370|[0x80013378]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80008028]:fsgnj.d t5, t3, s10<br> [0x8000802c]:csrrs gp, fcsr, zero<br> [0x80008030]:sw t5, 1192(ra)<br>                                 |
| 371|[0x80013398]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80008068]:fsgnj.d t5, t3, s10<br> [0x8000806c]:csrrs gp, fcsr, zero<br> [0x80008070]:sw t5, 1224(ra)<br>                                 |
| 372|[0x800133b8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x800080a8]:fsgnj.d t5, t3, s10<br> [0x800080ac]:csrrs gp, fcsr, zero<br> [0x800080b0]:sw t5, 1256(ra)<br>                                 |
| 373|[0x800133d8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x800080ec]:fsgnj.d t5, t3, s10<br> [0x800080f0]:csrrs gp, fcsr, zero<br> [0x800080f4]:sw t5, 1288(ra)<br>                                 |
| 374|[0x800133f8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80008130]:fsgnj.d t5, t3, s10<br> [0x80008134]:csrrs gp, fcsr, zero<br> [0x80008138]:sw t5, 1320(ra)<br>                                 |
| 375|[0x80013418]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80008170]:fsgnj.d t5, t3, s10<br> [0x80008174]:csrrs gp, fcsr, zero<br> [0x80008178]:sw t5, 1352(ra)<br>                                 |
| 376|[0x80013438]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800081b0]:fsgnj.d t5, t3, s10<br> [0x800081b4]:csrrs gp, fcsr, zero<br> [0x800081b8]:sw t5, 1384(ra)<br>                                 |
| 377|[0x80013458]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800081f0]:fsgnj.d t5, t3, s10<br> [0x800081f4]:csrrs gp, fcsr, zero<br> [0x800081f8]:sw t5, 1416(ra)<br>                                 |
| 378|[0x80013478]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80008230]:fsgnj.d t5, t3, s10<br> [0x80008234]:csrrs gp, fcsr, zero<br> [0x80008238]:sw t5, 1448(ra)<br>                                 |
| 379|[0x80013498]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80008270]:fsgnj.d t5, t3, s10<br> [0x80008274]:csrrs gp, fcsr, zero<br> [0x80008278]:sw t5, 1480(ra)<br>                                 |
| 380|[0x800134b8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x800082b0]:fsgnj.d t5, t3, s10<br> [0x800082b4]:csrrs gp, fcsr, zero<br> [0x800082b8]:sw t5, 1512(ra)<br>                                 |
| 381|[0x800134d8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x800082f0]:fsgnj.d t5, t3, s10<br> [0x800082f4]:csrrs gp, fcsr, zero<br> [0x800082f8]:sw t5, 1544(ra)<br>                                 |
| 382|[0x800134f8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80008330]:fsgnj.d t5, t3, s10<br> [0x80008334]:csrrs gp, fcsr, zero<br> [0x80008338]:sw t5, 1576(ra)<br>                                 |
| 383|[0x80013518]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80008370]:fsgnj.d t5, t3, s10<br> [0x80008374]:csrrs gp, fcsr, zero<br> [0x80008378]:sw t5, 1608(ra)<br>                                 |
| 384|[0x80013538]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800083b0]:fsgnj.d t5, t3, s10<br> [0x800083b4]:csrrs gp, fcsr, zero<br> [0x800083b8]:sw t5, 1640(ra)<br>                                 |
| 385|[0x80013558]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800083f0]:fsgnj.d t5, t3, s10<br> [0x800083f4]:csrrs gp, fcsr, zero<br> [0x800083f8]:sw t5, 1672(ra)<br>                                 |
| 386|[0x80013578]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80008430]:fsgnj.d t5, t3, s10<br> [0x80008434]:csrrs gp, fcsr, zero<br> [0x80008438]:sw t5, 1704(ra)<br>                                 |
| 387|[0x80013598]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80008470]:fsgnj.d t5, t3, s10<br> [0x80008474]:csrrs gp, fcsr, zero<br> [0x80008478]:sw t5, 1736(ra)<br>                                 |
| 388|[0x800135b8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x800084b0]:fsgnj.d t5, t3, s10<br> [0x800084b4]:csrrs gp, fcsr, zero<br> [0x800084b8]:sw t5, 1768(ra)<br>                                 |
| 389|[0x800135d8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x800084f0]:fsgnj.d t5, t3, s10<br> [0x800084f4]:csrrs gp, fcsr, zero<br> [0x800084f8]:sw t5, 1800(ra)<br>                                 |
| 390|[0x800135f8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80008530]:fsgnj.d t5, t3, s10<br> [0x80008534]:csrrs gp, fcsr, zero<br> [0x80008538]:sw t5, 1832(ra)<br>                                 |
| 391|[0x80013618]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80008574]:fsgnj.d t5, t3, s10<br> [0x80008578]:csrrs gp, fcsr, zero<br> [0x8000857c]:sw t5, 1864(ra)<br>                                 |
| 392|[0x80013638]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x800085b8]:fsgnj.d t5, t3, s10<br> [0x800085bc]:csrrs gp, fcsr, zero<br> [0x800085c0]:sw t5, 1896(ra)<br>                                 |
| 393|[0x80013658]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800085f8]:fsgnj.d t5, t3, s10<br> [0x800085fc]:csrrs gp, fcsr, zero<br> [0x80008600]:sw t5, 1928(ra)<br>                                 |
| 394|[0x80013678]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80008638]:fsgnj.d t5, t3, s10<br> [0x8000863c]:csrrs gp, fcsr, zero<br> [0x80008640]:sw t5, 1960(ra)<br>                                 |
| 395|[0x80013698]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80008678]:fsgnj.d t5, t3, s10<br> [0x8000867c]:csrrs gp, fcsr, zero<br> [0x80008680]:sw t5, 1992(ra)<br>                                 |
| 396|[0x800136b8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x800086b8]:fsgnj.d t5, t3, s10<br> [0x800086bc]:csrrs gp, fcsr, zero<br> [0x800086c0]:sw t5, 2024(ra)<br>                                 |
| 397|[0x800136d8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80008740]:fsgnj.d t5, t3, s10<br> [0x80008744]:csrrs gp, fcsr, zero<br> [0x80008748]:sw t5, 16(ra)<br>                                   |
| 398|[0x800136f8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x800087c4]:fsgnj.d t5, t3, s10<br> [0x800087c8]:csrrs gp, fcsr, zero<br> [0x800087cc]:sw t5, 48(ra)<br>                                   |
| 399|[0x80013718]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80008844]:fsgnj.d t5, t3, s10<br> [0x80008848]:csrrs gp, fcsr, zero<br> [0x8000884c]:sw t5, 80(ra)<br>                                   |
| 400|[0x80013738]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800088c4]:fsgnj.d t5, t3, s10<br> [0x800088c8]:csrrs gp, fcsr, zero<br> [0x800088cc]:sw t5, 112(ra)<br>                                  |
| 401|[0x80013758]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80008944]:fsgnj.d t5, t3, s10<br> [0x80008948]:csrrs gp, fcsr, zero<br> [0x8000894c]:sw t5, 144(ra)<br>                                  |
| 402|[0x80013778]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800089c4]:fsgnj.d t5, t3, s10<br> [0x800089c8]:csrrs gp, fcsr, zero<br> [0x800089cc]:sw t5, 176(ra)<br>                                  |
| 403|[0x80013798]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80008a44]:fsgnj.d t5, t3, s10<br> [0x80008a48]:csrrs gp, fcsr, zero<br> [0x80008a4c]:sw t5, 208(ra)<br>                                  |
| 404|[0x800137b8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80008ac4]:fsgnj.d t5, t3, s10<br> [0x80008ac8]:csrrs gp, fcsr, zero<br> [0x80008acc]:sw t5, 240(ra)<br>                                  |
| 405|[0x800137d8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80008b44]:fsgnj.d t5, t3, s10<br> [0x80008b48]:csrrs gp, fcsr, zero<br> [0x80008b4c]:sw t5, 272(ra)<br>                                  |
| 406|[0x800137f8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80008bc4]:fsgnj.d t5, t3, s10<br> [0x80008bc8]:csrrs gp, fcsr, zero<br> [0x80008bcc]:sw t5, 304(ra)<br>                                  |
| 407|[0x80013818]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80008c44]:fsgnj.d t5, t3, s10<br> [0x80008c48]:csrrs gp, fcsr, zero<br> [0x80008c4c]:sw t5, 336(ra)<br>                                  |
| 408|[0x80013838]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80008cc4]:fsgnj.d t5, t3, s10<br> [0x80008cc8]:csrrs gp, fcsr, zero<br> [0x80008ccc]:sw t5, 368(ra)<br>                                  |
| 409|[0x80013858]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80008d44]:fsgnj.d t5, t3, s10<br> [0x80008d48]:csrrs gp, fcsr, zero<br> [0x80008d4c]:sw t5, 400(ra)<br>                                  |
| 410|[0x80013878]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80008dc4]:fsgnj.d t5, t3, s10<br> [0x80008dc8]:csrrs gp, fcsr, zero<br> [0x80008dcc]:sw t5, 432(ra)<br>                                  |
| 411|[0x80013898]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80008e44]:fsgnj.d t5, t3, s10<br> [0x80008e48]:csrrs gp, fcsr, zero<br> [0x80008e4c]:sw t5, 464(ra)<br>                                  |
| 412|[0x800138b8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x80008ec4]:fsgnj.d t5, t3, s10<br> [0x80008ec8]:csrrs gp, fcsr, zero<br> [0x80008ecc]:sw t5, 496(ra)<br>                                  |
| 413|[0x800138d8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80008f44]:fsgnj.d t5, t3, s10<br> [0x80008f48]:csrrs gp, fcsr, zero<br> [0x80008f4c]:sw t5, 528(ra)<br>                                  |
| 414|[0x800138f8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x80008fc4]:fsgnj.d t5, t3, s10<br> [0x80008fc8]:csrrs gp, fcsr, zero<br> [0x80008fcc]:sw t5, 560(ra)<br>                                  |
| 415|[0x80013918]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80009048]:fsgnj.d t5, t3, s10<br> [0x8000904c]:csrrs gp, fcsr, zero<br> [0x80009050]:sw t5, 592(ra)<br>                                  |
| 416|[0x80013938]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x800090cc]:fsgnj.d t5, t3, s10<br> [0x800090d0]:csrrs gp, fcsr, zero<br> [0x800090d4]:sw t5, 624(ra)<br>                                  |
| 417|[0x80013958]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000914c]:fsgnj.d t5, t3, s10<br> [0x80009150]:csrrs gp, fcsr, zero<br> [0x80009154]:sw t5, 656(ra)<br>                                  |
| 418|[0x80013978]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800091cc]:fsgnj.d t5, t3, s10<br> [0x800091d0]:csrrs gp, fcsr, zero<br> [0x800091d4]:sw t5, 688(ra)<br>                                  |
| 419|[0x80013998]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000924c]:fsgnj.d t5, t3, s10<br> [0x80009250]:csrrs gp, fcsr, zero<br> [0x80009254]:sw t5, 720(ra)<br>                                  |
| 420|[0x800139b8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x800092cc]:fsgnj.d t5, t3, s10<br> [0x800092d0]:csrrs gp, fcsr, zero<br> [0x800092d4]:sw t5, 752(ra)<br>                                  |
| 421|[0x800139d8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x80009350]:fsgnj.d t5, t3, s10<br> [0x80009354]:csrrs gp, fcsr, zero<br> [0x80009358]:sw t5, 784(ra)<br>                                  |
| 422|[0x800139f8]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x800093d4]:fsgnj.d t5, t3, s10<br> [0x800093d8]:csrrs gp, fcsr, zero<br> [0x800093dc]:sw t5, 816(ra)<br>                                  |
| 423|[0x80013a18]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x80009454]:fsgnj.d t5, t3, s10<br> [0x80009458]:csrrs gp, fcsr, zero<br> [0x8000945c]:sw t5, 848(ra)<br>                                  |
| 424|[0x80013a38]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x800094d4]:fsgnj.d t5, t3, s10<br> [0x800094d8]:csrrs gp, fcsr, zero<br> [0x800094dc]:sw t5, 880(ra)<br>                                  |
| 425|[0x800136d8]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000c764]:fsgnj.d t5, t3, s10<br> [0x8000c768]:csrrs gp, fcsr, zero<br> [0x8000c76c]:sw t5, 0(ra)<br>                                    |
| 426|[0x800136f8]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000c7a4]:fsgnj.d t5, t3, s10<br> [0x8000c7a8]:csrrs gp, fcsr, zero<br> [0x8000c7ac]:sw t5, 32(ra)<br>                                   |
| 427|[0x80013718]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000c7e4]:fsgnj.d t5, t3, s10<br> [0x8000c7e8]:csrrs gp, fcsr, zero<br> [0x8000c7ec]:sw t5, 64(ra)<br>                                   |
| 428|[0x80013738]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000c824]:fsgnj.d t5, t3, s10<br> [0x8000c828]:csrrs gp, fcsr, zero<br> [0x8000c82c]:sw t5, 96(ra)<br>                                   |
| 429|[0x80013758]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000c864]:fsgnj.d t5, t3, s10<br> [0x8000c868]:csrrs gp, fcsr, zero<br> [0x8000c86c]:sw t5, 128(ra)<br>                                  |
| 430|[0x80013778]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000c8a4]:fsgnj.d t5, t3, s10<br> [0x8000c8a8]:csrrs gp, fcsr, zero<br> [0x8000c8ac]:sw t5, 160(ra)<br>                                  |
| 431|[0x80013798]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000c8e4]:fsgnj.d t5, t3, s10<br> [0x8000c8e8]:csrrs gp, fcsr, zero<br> [0x8000c8ec]:sw t5, 192(ra)<br>                                  |
| 432|[0x800137b8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000c924]:fsgnj.d t5, t3, s10<br> [0x8000c928]:csrrs gp, fcsr, zero<br> [0x8000c92c]:sw t5, 224(ra)<br>                                  |
| 433|[0x800137d8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000c964]:fsgnj.d t5, t3, s10<br> [0x8000c968]:csrrs gp, fcsr, zero<br> [0x8000c96c]:sw t5, 256(ra)<br>                                  |
| 434|[0x800137f8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000c9a4]:fsgnj.d t5, t3, s10<br> [0x8000c9a8]:csrrs gp, fcsr, zero<br> [0x8000c9ac]:sw t5, 288(ra)<br>                                  |
| 435|[0x80013818]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x8000c9e8]:fsgnj.d t5, t3, s10<br> [0x8000c9ec]:csrrs gp, fcsr, zero<br> [0x8000c9f0]:sw t5, 320(ra)<br>                                  |
| 436|[0x80013838]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x8000ca2c]:fsgnj.d t5, t3, s10<br> [0x8000ca30]:csrrs gp, fcsr, zero<br> [0x8000ca34]:sw t5, 352(ra)<br>                                  |
| 437|[0x80013858]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000ca6c]:fsgnj.d t5, t3, s10<br> [0x8000ca70]:csrrs gp, fcsr, zero<br> [0x8000ca74]:sw t5, 384(ra)<br>                                  |
| 438|[0x80013878]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000caac]:fsgnj.d t5, t3, s10<br> [0x8000cab0]:csrrs gp, fcsr, zero<br> [0x8000cab4]:sw t5, 416(ra)<br>                                  |
| 439|[0x80013898]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000caec]:fsgnj.d t5, t3, s10<br> [0x8000caf0]:csrrs gp, fcsr, zero<br> [0x8000caf4]:sw t5, 448(ra)<br>                                  |
| 440|[0x800138b8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000cb2c]:fsgnj.d t5, t3, s10<br> [0x8000cb30]:csrrs gp, fcsr, zero<br> [0x8000cb34]:sw t5, 480(ra)<br>                                  |
| 441|[0x800138d8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x8000cb70]:fsgnj.d t5, t3, s10<br> [0x8000cb74]:csrrs gp, fcsr, zero<br> [0x8000cb78]:sw t5, 512(ra)<br>                                  |
| 442|[0x800138f8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                                                                                                |[0x8000cbb4]:fsgnj.d t5, t3, s10<br> [0x8000cbb8]:csrrs gp, fcsr, zero<br> [0x8000cbbc]:sw t5, 544(ra)<br>                                  |
| 443|[0x80013918]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000cbf4]:fsgnj.d t5, t3, s10<br> [0x8000cbf8]:csrrs gp, fcsr, zero<br> [0x8000cbfc]:sw t5, 576(ra)<br>                                  |
| 444|[0x80013938]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000cc34]:fsgnj.d t5, t3, s10<br> [0x8000cc38]:csrrs gp, fcsr, zero<br> [0x8000cc3c]:sw t5, 608(ra)<br>                                  |
| 445|[0x80013958]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000cc74]:fsgnj.d t5, t3, s10<br> [0x8000cc78]:csrrs gp, fcsr, zero<br> [0x8000cc7c]:sw t5, 640(ra)<br>                                  |
| 446|[0x80013978]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000ccb4]:fsgnj.d t5, t3, s10<br> [0x8000ccb8]:csrrs gp, fcsr, zero<br> [0x8000ccbc]:sw t5, 672(ra)<br>                                  |
| 447|[0x80013998]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000ccf4]:fsgnj.d t5, t3, s10<br> [0x8000ccf8]:csrrs gp, fcsr, zero<br> [0x8000ccfc]:sw t5, 704(ra)<br>                                  |
| 448|[0x800139b8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000cd34]:fsgnj.d t5, t3, s10<br> [0x8000cd38]:csrrs gp, fcsr, zero<br> [0x8000cd3c]:sw t5, 736(ra)<br>                                  |
| 449|[0x800139d8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000cd74]:fsgnj.d t5, t3, s10<br> [0x8000cd78]:csrrs gp, fcsr, zero<br> [0x8000cd7c]:sw t5, 768(ra)<br>                                  |
| 450|[0x800139f8]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000cdb4]:fsgnj.d t5, t3, s10<br> [0x8000cdb8]:csrrs gp, fcsr, zero<br> [0x8000cdbc]:sw t5, 800(ra)<br>                                  |
| 451|[0x80013a18]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000cdf4]:fsgnj.d t5, t3, s10<br> [0x8000cdf8]:csrrs gp, fcsr, zero<br> [0x8000cdfc]:sw t5, 832(ra)<br>                                  |
| 452|[0x80013a38]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                                                                                |[0x8000ce34]:fsgnj.d t5, t3, s10<br> [0x8000ce38]:csrrs gp, fcsr, zero<br> [0x8000ce3c]:sw t5, 864(ra)<br>                                  |
