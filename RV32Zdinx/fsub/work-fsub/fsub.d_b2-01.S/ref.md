
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80006940')]      |
| SIG_REGION                | [('0x80009310', '0x8000a460', '1108 words')]      |
| COV_LABELS                | fsub.d_b2      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-fsub/fsub.d_b2-01.S/ref.S    |
| Total Number of coverpoints| 325     |
| Total Coverpoints Hit     | 325      |
| Total Signature Updates   | 596      |
| STAT1                     | 149      |
| STAT2                     | 0      |
| STAT3                     | 127     |
| STAT4                     | 298     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80002808]:fsub.d t5, t3, s10, dyn
[0x8000280c]:csrrs a7, fcsr, zero
[0x80002810]:sw t5, 176(ra)
[0x80002814]:sw t6, 184(ra)
[0x80002818]:sw t5, 192(ra)
[0x8000281c]:sw a7, 200(ra)
[0x80002820]:lui a4, 1
[0x80002824]:addi a4, a4, 2048
[0x80002828]:add a6, a6, a4
[0x8000282c]:lw t3, 96(a6)
[0x80002830]:sub a6, a6, a4
[0x80002834]:lui a4, 1
[0x80002838]:addi a4, a4, 2048
[0x8000283c]:add a6, a6, a4
[0x80002840]:lw t4, 100(a6)
[0x80002844]:sub a6, a6, a4
[0x80002848]:lui a4, 1
[0x8000284c]:addi a4, a4, 2048
[0x80002850]:add a6, a6, a4
[0x80002854]:lw s10, 104(a6)
[0x80002858]:sub a6, a6, a4
[0x8000285c]:lui a4, 1
[0x80002860]:addi a4, a4, 2048
[0x80002864]:add a6, a6, a4
[0x80002868]:lw s11, 108(a6)
[0x8000286c]:sub a6, a6, a4
[0x80002870]:addi t3, zero, 87
[0x80002874]:addi t4, zero, 0
[0x80002878]:addi s10, zero, 3880
[0x8000287c]:lui s11, 524544
[0x80002880]:addi s11, s11, 4095
[0x80002884]:addi a4, zero, 0
[0x80002888]:csrrw zero, fcsr, a4
[0x8000288c]:fsub.d t5, t3, s10, dyn
[0x80002890]:csrrs a7, fcsr, zero

[0x8000288c]:fsub.d t5, t3, s10, dyn
[0x80002890]:csrrs a7, fcsr, zero
[0x80002894]:sw t5, 208(ra)
[0x80002898]:sw t6, 216(ra)
[0x8000289c]:sw t5, 224(ra)
[0x800028a0]:sw a7, 232(ra)
[0x800028a4]:lui a4, 1
[0x800028a8]:addi a4, a4, 2048
[0x800028ac]:add a6, a6, a4
[0x800028b0]:lw t3, 112(a6)
[0x800028b4]:sub a6, a6, a4
[0x800028b8]:lui a4, 1
[0x800028bc]:addi a4, a4, 2048
[0x800028c0]:add a6, a6, a4
[0x800028c4]:lw t4, 116(a6)
[0x800028c8]:sub a6, a6, a4
[0x800028cc]:lui a4, 1
[0x800028d0]:addi a4, a4, 2048
[0x800028d4]:add a6, a6, a4
[0x800028d8]:lw s10, 120(a6)
[0x800028dc]:sub a6, a6, a4
[0x800028e0]:lui a4, 1
[0x800028e4]:addi a4, a4, 2048
[0x800028e8]:add a6, a6, a4
[0x800028ec]:lw s11, 124(a6)
[0x800028f0]:sub a6, a6, a4
[0x800028f4]:addi t3, zero, 76
[0x800028f8]:addi t4, zero, 0
[0x800028fc]:addi s10, zero, 3763
[0x80002900]:lui s11, 524544
[0x80002904]:addi s11, s11, 4095
[0x80002908]:addi a4, zero, 0
[0x8000290c]:csrrw zero, fcsr, a4
[0x80002910]:fsub.d t5, t3, s10, dyn
[0x80002914]:csrrs a7, fcsr, zero

[0x80002910]:fsub.d t5, t3, s10, dyn
[0x80002914]:csrrs a7, fcsr, zero
[0x80002918]:sw t5, 240(ra)
[0x8000291c]:sw t6, 248(ra)
[0x80002920]:sw t5, 256(ra)
[0x80002924]:sw a7, 264(ra)
[0x80002928]:lui a4, 1
[0x8000292c]:addi a4, a4, 2048
[0x80002930]:add a6, a6, a4
[0x80002934]:lw t3, 128(a6)
[0x80002938]:sub a6, a6, a4
[0x8000293c]:lui a4, 1
[0x80002940]:addi a4, a4, 2048
[0x80002944]:add a6, a6, a4
[0x80002948]:lw t4, 132(a6)
[0x8000294c]:sub a6, a6, a4
[0x80002950]:lui a4, 1
[0x80002954]:addi a4, a4, 2048
[0x80002958]:add a6, a6, a4
[0x8000295c]:lw s10, 136(a6)
[0x80002960]:sub a6, a6, a4
[0x80002964]:lui a4, 1
[0x80002968]:addi a4, a4, 2048
[0x8000296c]:add a6, a6, a4
[0x80002970]:lw s11, 140(a6)
[0x80002974]:sub a6, a6, a4
[0x80002978]:addi t3, zero, 64
[0x8000297c]:addi t4, zero, 0
[0x80002980]:addi s10, zero, 3519
[0x80002984]:lui s11, 524544
[0x80002988]:addi s11, s11, 4095
[0x8000298c]:addi a4, zero, 0
[0x80002990]:csrrw zero, fcsr, a4
[0x80002994]:fsub.d t5, t3, s10, dyn
[0x80002998]:csrrs a7, fcsr, zero

[0x80002994]:fsub.d t5, t3, s10, dyn
[0x80002998]:csrrs a7, fcsr, zero
[0x8000299c]:sw t5, 272(ra)
[0x800029a0]:sw t6, 280(ra)
[0x800029a4]:sw t5, 288(ra)
[0x800029a8]:sw a7, 296(ra)
[0x800029ac]:lui a4, 1
[0x800029b0]:addi a4, a4, 2048
[0x800029b4]:add a6, a6, a4
[0x800029b8]:lw t3, 144(a6)
[0x800029bc]:sub a6, a6, a4
[0x800029c0]:lui a4, 1
[0x800029c4]:addi a4, a4, 2048
[0x800029c8]:add a6, a6, a4
[0x800029cc]:lw t4, 148(a6)
[0x800029d0]:sub a6, a6, a4
[0x800029d4]:lui a4, 1
[0x800029d8]:addi a4, a4, 2048
[0x800029dc]:add a6, a6, a4
[0x800029e0]:lw s10, 152(a6)
[0x800029e4]:sub a6, a6, a4
[0x800029e8]:lui a4, 1
[0x800029ec]:addi a4, a4, 2048
[0x800029f0]:add a6, a6, a4
[0x800029f4]:lw s11, 156(a6)
[0x800029f8]:sub a6, a6, a4
[0x800029fc]:addi t3, zero, 86
[0x80002a00]:addi t4, zero, 0
[0x80002a04]:addi s10, zero, 2985
[0x80002a08]:lui s11, 524544
[0x80002a0c]:addi s11, s11, 4095
[0x80002a10]:addi a4, zero, 0
[0x80002a14]:csrrw zero, fcsr, a4
[0x80002a18]:fsub.d t5, t3, s10, dyn
[0x80002a1c]:csrrs a7, fcsr, zero

[0x80002a18]:fsub.d t5, t3, s10, dyn
[0x80002a1c]:csrrs a7, fcsr, zero
[0x80002a20]:sw t5, 304(ra)
[0x80002a24]:sw t6, 312(ra)
[0x80002a28]:sw t5, 320(ra)
[0x80002a2c]:sw a7, 328(ra)
[0x80002a30]:lui a4, 1
[0x80002a34]:addi a4, a4, 2048
[0x80002a38]:add a6, a6, a4
[0x80002a3c]:lw t3, 160(a6)
[0x80002a40]:sub a6, a6, a4
[0x80002a44]:lui a4, 1
[0x80002a48]:addi a4, a4, 2048
[0x80002a4c]:add a6, a6, a4
[0x80002a50]:lw t4, 164(a6)
[0x80002a54]:sub a6, a6, a4
[0x80002a58]:lui a4, 1
[0x80002a5c]:addi a4, a4, 2048
[0x80002a60]:add a6, a6, a4
[0x80002a64]:lw s10, 168(a6)
[0x80002a68]:sub a6, a6, a4
[0x80002a6c]:lui a4, 1
[0x80002a70]:addi a4, a4, 2048
[0x80002a74]:add a6, a6, a4
[0x80002a78]:lw s11, 172(a6)
[0x80002a7c]:sub a6, a6, a4
[0x80002a80]:addi t3, zero, 38
[0x80002a84]:addi t4, zero, 0
[0x80002a88]:lui s10, 1048575
[0x80002a8c]:addi s10, s10, 2009
[0x80002a90]:lui s11, 524544
[0x80002a94]:addi s11, s11, 4095
[0x80002a98]:addi a4, zero, 0
[0x80002a9c]:csrrw zero, fcsr, a4
[0x80002aa0]:fsub.d t5, t3, s10, dyn
[0x80002aa4]:csrrs a7, fcsr, zero

[0x80002aa0]:fsub.d t5, t3, s10, dyn
[0x80002aa4]:csrrs a7, fcsr, zero
[0x80002aa8]:sw t5, 336(ra)
[0x80002aac]:sw t6, 344(ra)
[0x80002ab0]:sw t5, 352(ra)
[0x80002ab4]:sw a7, 360(ra)
[0x80002ab8]:lui a4, 1
[0x80002abc]:addi a4, a4, 2048
[0x80002ac0]:add a6, a6, a4
[0x80002ac4]:lw t3, 176(a6)
[0x80002ac8]:sub a6, a6, a4
[0x80002acc]:lui a4, 1
[0x80002ad0]:addi a4, a4, 2048
[0x80002ad4]:add a6, a6, a4
[0x80002ad8]:lw t4, 180(a6)
[0x80002adc]:sub a6, a6, a4
[0x80002ae0]:lui a4, 1
[0x80002ae4]:addi a4, a4, 2048
[0x80002ae8]:add a6, a6, a4
[0x80002aec]:lw s10, 184(a6)
[0x80002af0]:sub a6, a6, a4
[0x80002af4]:lui a4, 1
[0x80002af8]:addi a4, a4, 2048
[0x80002afc]:add a6, a6, a4
[0x80002b00]:lw s11, 188(a6)
[0x80002b04]:sub a6, a6, a4
[0x80002b08]:addi t3, zero, 64
[0x80002b0c]:addi t4, zero, 0
[0x80002b10]:lui s10, 1048575
[0x80002b14]:addi s10, s10, 4031
[0x80002b18]:lui s11, 524544
[0x80002b1c]:addi s11, s11, 4095
[0x80002b20]:addi a4, zero, 0
[0x80002b24]:csrrw zero, fcsr, a4
[0x80002b28]:fsub.d t5, t3, s10, dyn
[0x80002b2c]:csrrs a7, fcsr, zero

[0x80002b28]:fsub.d t5, t3, s10, dyn
[0x80002b2c]:csrrs a7, fcsr, zero
[0x80002b30]:sw t5, 368(ra)
[0x80002b34]:sw t6, 376(ra)
[0x80002b38]:sw t5, 384(ra)
[0x80002b3c]:sw a7, 392(ra)
[0x80002b40]:lui a4, 1
[0x80002b44]:addi a4, a4, 2048
[0x80002b48]:add a6, a6, a4
[0x80002b4c]:lw t3, 192(a6)
[0x80002b50]:sub a6, a6, a4
[0x80002b54]:lui a4, 1
[0x80002b58]:addi a4, a4, 2048
[0x80002b5c]:add a6, a6, a4
[0x80002b60]:lw t4, 196(a6)
[0x80002b64]:sub a6, a6, a4
[0x80002b68]:lui a4, 1
[0x80002b6c]:addi a4, a4, 2048
[0x80002b70]:add a6, a6, a4
[0x80002b74]:lw s10, 200(a6)
[0x80002b78]:sub a6, a6, a4
[0x80002b7c]:lui a4, 1
[0x80002b80]:addi a4, a4, 2048
[0x80002b84]:add a6, a6, a4
[0x80002b88]:lw s11, 204(a6)
[0x80002b8c]:sub a6, a6, a4
[0x80002b90]:addi t3, zero, 42
[0x80002b94]:addi t4, zero, 0
[0x80002b98]:lui s10, 1048574
[0x80002b9c]:addi s10, s10, 4053
[0x80002ba0]:lui s11, 524544
[0x80002ba4]:addi s11, s11, 4095
[0x80002ba8]:addi a4, zero, 0
[0x80002bac]:csrrw zero, fcsr, a4
[0x80002bb0]:fsub.d t5, t3, s10, dyn
[0x80002bb4]:csrrs a7, fcsr, zero

[0x80002bb0]:fsub.d t5, t3, s10, dyn
[0x80002bb4]:csrrs a7, fcsr, zero
[0x80002bb8]:sw t5, 400(ra)
[0x80002bbc]:sw t6, 408(ra)
[0x80002bc0]:sw t5, 416(ra)
[0x80002bc4]:sw a7, 424(ra)
[0x80002bc8]:lui a4, 1
[0x80002bcc]:addi a4, a4, 2048
[0x80002bd0]:add a6, a6, a4
[0x80002bd4]:lw t3, 208(a6)
[0x80002bd8]:sub a6, a6, a4
[0x80002bdc]:lui a4, 1
[0x80002be0]:addi a4, a4, 2048
[0x80002be4]:add a6, a6, a4
[0x80002be8]:lw t4, 212(a6)
[0x80002bec]:sub a6, a6, a4
[0x80002bf0]:lui a4, 1
[0x80002bf4]:addi a4, a4, 2048
[0x80002bf8]:add a6, a6, a4
[0x80002bfc]:lw s10, 216(a6)
[0x80002c00]:sub a6, a6, a4
[0x80002c04]:lui a4, 1
[0x80002c08]:addi a4, a4, 2048
[0x80002c0c]:add a6, a6, a4
[0x80002c10]:lw s11, 220(a6)
[0x80002c14]:sub a6, a6, a4
[0x80002c18]:addi t3, zero, 52
[0x80002c1c]:addi t4, zero, 0
[0x80002c20]:lui s10, 1048572
[0x80002c24]:addi s10, s10, 4043
[0x80002c28]:lui s11, 524544
[0x80002c2c]:addi s11, s11, 4095
[0x80002c30]:addi a4, zero, 0
[0x80002c34]:csrrw zero, fcsr, a4
[0x80002c38]:fsub.d t5, t3, s10, dyn
[0x80002c3c]:csrrs a7, fcsr, zero

[0x80002c38]:fsub.d t5, t3, s10, dyn
[0x80002c3c]:csrrs a7, fcsr, zero
[0x80002c40]:sw t5, 432(ra)
[0x80002c44]:sw t6, 440(ra)
[0x80002c48]:sw t5, 448(ra)
[0x80002c4c]:sw a7, 456(ra)
[0x80002c50]:lui a4, 1
[0x80002c54]:addi a4, a4, 2048
[0x80002c58]:add a6, a6, a4
[0x80002c5c]:lw t3, 224(a6)
[0x80002c60]:sub a6, a6, a4
[0x80002c64]:lui a4, 1
[0x80002c68]:addi a4, a4, 2048
[0x80002c6c]:add a6, a6, a4
[0x80002c70]:lw t4, 228(a6)
[0x80002c74]:sub a6, a6, a4
[0x80002c78]:lui a4, 1
[0x80002c7c]:addi a4, a4, 2048
[0x80002c80]:add a6, a6, a4
[0x80002c84]:lw s10, 232(a6)
[0x80002c88]:sub a6, a6, a4
[0x80002c8c]:lui a4, 1
[0x80002c90]:addi a4, a4, 2048
[0x80002c94]:add a6, a6, a4
[0x80002c98]:lw s11, 236(a6)
[0x80002c9c]:sub a6, a6, a4
[0x80002ca0]:addi t3, zero, 3
[0x80002ca4]:addi t4, zero, 0
[0x80002ca8]:lui s10, 1048568
[0x80002cac]:addi s10, s10, 4092
[0x80002cb0]:lui s11, 524544
[0x80002cb4]:addi s11, s11, 4095
[0x80002cb8]:addi a4, zero, 0
[0x80002cbc]:csrrw zero, fcsr, a4
[0x80002cc0]:fsub.d t5, t3, s10, dyn
[0x80002cc4]:csrrs a7, fcsr, zero

[0x80002cc0]:fsub.d t5, t3, s10, dyn
[0x80002cc4]:csrrs a7, fcsr, zero
[0x80002cc8]:sw t5, 464(ra)
[0x80002ccc]:sw t6, 472(ra)
[0x80002cd0]:sw t5, 480(ra)
[0x80002cd4]:sw a7, 488(ra)
[0x80002cd8]:lui a4, 1
[0x80002cdc]:addi a4, a4, 2048
[0x80002ce0]:add a6, a6, a4
[0x80002ce4]:lw t3, 240(a6)
[0x80002ce8]:sub a6, a6, a4
[0x80002cec]:lui a4, 1
[0x80002cf0]:addi a4, a4, 2048
[0x80002cf4]:add a6, a6, a4
[0x80002cf8]:lw t4, 244(a6)
[0x80002cfc]:sub a6, a6, a4
[0x80002d00]:lui a4, 1
[0x80002d04]:addi a4, a4, 2048
[0x80002d08]:add a6, a6, a4
[0x80002d0c]:lw s10, 248(a6)
[0x80002d10]:sub a6, a6, a4
[0x80002d14]:lui a4, 1
[0x80002d18]:addi a4, a4, 2048
[0x80002d1c]:add a6, a6, a4
[0x80002d20]:lw s11, 252(a6)
[0x80002d24]:sub a6, a6, a4
[0x80002d28]:addi t3, zero, 26
[0x80002d2c]:addi t4, zero, 0
[0x80002d30]:lui s10, 1048560
[0x80002d34]:addi s10, s10, 4069
[0x80002d38]:lui s11, 524544
[0x80002d3c]:addi s11, s11, 4095
[0x80002d40]:addi a4, zero, 0
[0x80002d44]:csrrw zero, fcsr, a4
[0x80002d48]:fsub.d t5, t3, s10, dyn
[0x80002d4c]:csrrs a7, fcsr, zero

[0x80002d48]:fsub.d t5, t3, s10, dyn
[0x80002d4c]:csrrs a7, fcsr, zero
[0x80002d50]:sw t5, 496(ra)
[0x80002d54]:sw t6, 504(ra)
[0x80002d58]:sw t5, 512(ra)
[0x80002d5c]:sw a7, 520(ra)
[0x80002d60]:lui a4, 1
[0x80002d64]:addi a4, a4, 2048
[0x80002d68]:add a6, a6, a4
[0x80002d6c]:lw t3, 256(a6)
[0x80002d70]:sub a6, a6, a4
[0x80002d74]:lui a4, 1
[0x80002d78]:addi a4, a4, 2048
[0x80002d7c]:add a6, a6, a4
[0x80002d80]:lw t4, 260(a6)
[0x80002d84]:sub a6, a6, a4
[0x80002d88]:lui a4, 1
[0x80002d8c]:addi a4, a4, 2048
[0x80002d90]:add a6, a6, a4
[0x80002d94]:lw s10, 264(a6)
[0x80002d98]:sub a6, a6, a4
[0x80002d9c]:lui a4, 1
[0x80002da0]:addi a4, a4, 2048
[0x80002da4]:add a6, a6, a4
[0x80002da8]:lw s11, 268(a6)
[0x80002dac]:sub a6, a6, a4
[0x80002db0]:addi t3, zero, 73
[0x80002db4]:addi t4, zero, 0
[0x80002db8]:lui s10, 1048544
[0x80002dbc]:addi s10, s10, 4022
[0x80002dc0]:lui s11, 524544
[0x80002dc4]:addi s11, s11, 4095
[0x80002dc8]:addi a4, zero, 0
[0x80002dcc]:csrrw zero, fcsr, a4
[0x80002dd0]:fsub.d t5, t3, s10, dyn
[0x80002dd4]:csrrs a7, fcsr, zero

[0x80002dd0]:fsub.d t5, t3, s10, dyn
[0x80002dd4]:csrrs a7, fcsr, zero
[0x80002dd8]:sw t5, 528(ra)
[0x80002ddc]:sw t6, 536(ra)
[0x80002de0]:sw t5, 544(ra)
[0x80002de4]:sw a7, 552(ra)
[0x80002de8]:lui a4, 1
[0x80002dec]:addi a4, a4, 2048
[0x80002df0]:add a6, a6, a4
[0x80002df4]:lw t3, 272(a6)
[0x80002df8]:sub a6, a6, a4
[0x80002dfc]:lui a4, 1
[0x80002e00]:addi a4, a4, 2048
[0x80002e04]:add a6, a6, a4
[0x80002e08]:lw t4, 276(a6)
[0x80002e0c]:sub a6, a6, a4
[0x80002e10]:lui a4, 1
[0x80002e14]:addi a4, a4, 2048
[0x80002e18]:add a6, a6, a4
[0x80002e1c]:lw s10, 280(a6)
[0x80002e20]:sub a6, a6, a4
[0x80002e24]:lui a4, 1
[0x80002e28]:addi a4, a4, 2048
[0x80002e2c]:add a6, a6, a4
[0x80002e30]:lw s11, 284(a6)
[0x80002e34]:sub a6, a6, a4
[0x80002e38]:addi t3, zero, 44
[0x80002e3c]:addi t4, zero, 0
[0x80002e40]:lui s10, 1048512
[0x80002e44]:addi s10, s10, 4051
[0x80002e48]:lui s11, 524544
[0x80002e4c]:addi s11, s11, 4095
[0x80002e50]:addi a4, zero, 0
[0x80002e54]:csrrw zero, fcsr, a4
[0x80002e58]:fsub.d t5, t3, s10, dyn
[0x80002e5c]:csrrs a7, fcsr, zero

[0x80002e58]:fsub.d t5, t3, s10, dyn
[0x80002e5c]:csrrs a7, fcsr, zero
[0x80002e60]:sw t5, 560(ra)
[0x80002e64]:sw t6, 568(ra)
[0x80002e68]:sw t5, 576(ra)
[0x80002e6c]:sw a7, 584(ra)
[0x80002e70]:lui a4, 1
[0x80002e74]:addi a4, a4, 2048
[0x80002e78]:add a6, a6, a4
[0x80002e7c]:lw t3, 288(a6)
[0x80002e80]:sub a6, a6, a4
[0x80002e84]:lui a4, 1
[0x80002e88]:addi a4, a4, 2048
[0x80002e8c]:add a6, a6, a4
[0x80002e90]:lw t4, 292(a6)
[0x80002e94]:sub a6, a6, a4
[0x80002e98]:lui a4, 1
[0x80002e9c]:addi a4, a4, 2048
[0x80002ea0]:add a6, a6, a4
[0x80002ea4]:lw s10, 296(a6)
[0x80002ea8]:sub a6, a6, a4
[0x80002eac]:lui a4, 1
[0x80002eb0]:addi a4, a4, 2048
[0x80002eb4]:add a6, a6, a4
[0x80002eb8]:lw s11, 300(a6)
[0x80002ebc]:sub a6, a6, a4
[0x80002ec0]:addi t3, zero, 28
[0x80002ec4]:addi t4, zero, 0
[0x80002ec8]:lui s10, 1048448
[0x80002ecc]:addi s10, s10, 4067
[0x80002ed0]:lui s11, 524544
[0x80002ed4]:addi s11, s11, 4095
[0x80002ed8]:addi a4, zero, 0
[0x80002edc]:csrrw zero, fcsr, a4
[0x80002ee0]:fsub.d t5, t3, s10, dyn
[0x80002ee4]:csrrs a7, fcsr, zero

[0x80002ee0]:fsub.d t5, t3, s10, dyn
[0x80002ee4]:csrrs a7, fcsr, zero
[0x80002ee8]:sw t5, 592(ra)
[0x80002eec]:sw t6, 600(ra)
[0x80002ef0]:sw t5, 608(ra)
[0x80002ef4]:sw a7, 616(ra)
[0x80002ef8]:lui a4, 1
[0x80002efc]:addi a4, a4, 2048
[0x80002f00]:add a6, a6, a4
[0x80002f04]:lw t3, 304(a6)
[0x80002f08]:sub a6, a6, a4
[0x80002f0c]:lui a4, 1
[0x80002f10]:addi a4, a4, 2048
[0x80002f14]:add a6, a6, a4
[0x80002f18]:lw t4, 308(a6)
[0x80002f1c]:sub a6, a6, a4
[0x80002f20]:lui a4, 1
[0x80002f24]:addi a4, a4, 2048
[0x80002f28]:add a6, a6, a4
[0x80002f2c]:lw s10, 312(a6)
[0x80002f30]:sub a6, a6, a4
[0x80002f34]:lui a4, 1
[0x80002f38]:addi a4, a4, 2048
[0x80002f3c]:add a6, a6, a4
[0x80002f40]:lw s11, 316(a6)
[0x80002f44]:sub a6, a6, a4
[0x80002f48]:addi t3, zero, 87
[0x80002f4c]:addi t4, zero, 0
[0x80002f50]:lui s10, 1048320
[0x80002f54]:addi s10, s10, 4008
[0x80002f58]:lui s11, 524544
[0x80002f5c]:addi s11, s11, 4095
[0x80002f60]:addi a4, zero, 0
[0x80002f64]:csrrw zero, fcsr, a4
[0x80002f68]:fsub.d t5, t3, s10, dyn
[0x80002f6c]:csrrs a7, fcsr, zero

[0x80002f68]:fsub.d t5, t3, s10, dyn
[0x80002f6c]:csrrs a7, fcsr, zero
[0x80002f70]:sw t5, 624(ra)
[0x80002f74]:sw t6, 632(ra)
[0x80002f78]:sw t5, 640(ra)
[0x80002f7c]:sw a7, 648(ra)
[0x80002f80]:lui a4, 1
[0x80002f84]:addi a4, a4, 2048
[0x80002f88]:add a6, a6, a4
[0x80002f8c]:lw t3, 320(a6)
[0x80002f90]:sub a6, a6, a4
[0x80002f94]:lui a4, 1
[0x80002f98]:addi a4, a4, 2048
[0x80002f9c]:add a6, a6, a4
[0x80002fa0]:lw t4, 324(a6)
[0x80002fa4]:sub a6, a6, a4
[0x80002fa8]:lui a4, 1
[0x80002fac]:addi a4, a4, 2048
[0x80002fb0]:add a6, a6, a4
[0x80002fb4]:lw s10, 328(a6)
[0x80002fb8]:sub a6, a6, a4
[0x80002fbc]:lui a4, 1
[0x80002fc0]:addi a4, a4, 2048
[0x80002fc4]:add a6, a6, a4
[0x80002fc8]:lw s11, 332(a6)
[0x80002fcc]:sub a6, a6, a4
[0x80002fd0]:addi t3, zero, 49
[0x80002fd4]:addi t4, zero, 0
[0x80002fd8]:lui s10, 1048064
[0x80002fdc]:addi s10, s10, 4046
[0x80002fe0]:lui s11, 524544
[0x80002fe4]:addi s11, s11, 4095
[0x80002fe8]:addi a4, zero, 0
[0x80002fec]:csrrw zero, fcsr, a4
[0x80002ff0]:fsub.d t5, t3, s10, dyn
[0x80002ff4]:csrrs a7, fcsr, zero

[0x80002ff0]:fsub.d t5, t3, s10, dyn
[0x80002ff4]:csrrs a7, fcsr, zero
[0x80002ff8]:sw t5, 656(ra)
[0x80002ffc]:sw t6, 664(ra)
[0x80003000]:sw t5, 672(ra)
[0x80003004]:sw a7, 680(ra)
[0x80003008]:lui a4, 1
[0x8000300c]:addi a4, a4, 2048
[0x80003010]:add a6, a6, a4
[0x80003014]:lw t3, 336(a6)
[0x80003018]:sub a6, a6, a4
[0x8000301c]:lui a4, 1
[0x80003020]:addi a4, a4, 2048
[0x80003024]:add a6, a6, a4
[0x80003028]:lw t4, 340(a6)
[0x8000302c]:sub a6, a6, a4
[0x80003030]:lui a4, 1
[0x80003034]:addi a4, a4, 2048
[0x80003038]:add a6, a6, a4
[0x8000303c]:lw s10, 344(a6)
[0x80003040]:sub a6, a6, a4
[0x80003044]:lui a4, 1
[0x80003048]:addi a4, a4, 2048
[0x8000304c]:add a6, a6, a4
[0x80003050]:lw s11, 348(a6)
[0x80003054]:sub a6, a6, a4
[0x80003058]:addi t3, zero, 45
[0x8000305c]:addi t4, zero, 0
[0x80003060]:lui s10, 1047552
[0x80003064]:addi s10, s10, 4050
[0x80003068]:lui s11, 524544
[0x8000306c]:addi s11, s11, 4095
[0x80003070]:addi a4, zero, 0
[0x80003074]:csrrw zero, fcsr, a4
[0x80003078]:fsub.d t5, t3, s10, dyn
[0x8000307c]:csrrs a7, fcsr, zero

[0x80003078]:fsub.d t5, t3, s10, dyn
[0x8000307c]:csrrs a7, fcsr, zero
[0x80003080]:sw t5, 688(ra)
[0x80003084]:sw t6, 696(ra)
[0x80003088]:sw t5, 704(ra)
[0x8000308c]:sw a7, 712(ra)
[0x80003090]:lui a4, 1
[0x80003094]:addi a4, a4, 2048
[0x80003098]:add a6, a6, a4
[0x8000309c]:lw t3, 352(a6)
[0x800030a0]:sub a6, a6, a4
[0x800030a4]:lui a4, 1
[0x800030a8]:addi a4, a4, 2048
[0x800030ac]:add a6, a6, a4
[0x800030b0]:lw t4, 356(a6)
[0x800030b4]:sub a6, a6, a4
[0x800030b8]:lui a4, 1
[0x800030bc]:addi a4, a4, 2048
[0x800030c0]:add a6, a6, a4
[0x800030c4]:lw s10, 360(a6)
[0x800030c8]:sub a6, a6, a4
[0x800030cc]:lui a4, 1
[0x800030d0]:addi a4, a4, 2048
[0x800030d4]:add a6, a6, a4
[0x800030d8]:lw s11, 364(a6)
[0x800030dc]:sub a6, a6, a4
[0x800030e0]:addi t3, zero, 69
[0x800030e4]:addi t4, zero, 0
[0x800030e8]:addi s10, zero, 67
[0x800030ec]:lui s11, 256
[0x800030f0]:addi a4, zero, 0
[0x800030f4]:csrrw zero, fcsr, a4
[0x800030f8]:fsub.d t5, t3, s10, dyn
[0x800030fc]:csrrs a7, fcsr, zero

[0x800030f8]:fsub.d t5, t3, s10, dyn
[0x800030fc]:csrrs a7, fcsr, zero
[0x80003100]:sw t5, 720(ra)
[0x80003104]:sw t6, 728(ra)
[0x80003108]:sw t5, 736(ra)
[0x8000310c]:sw a7, 744(ra)
[0x80003110]:lui a4, 1
[0x80003114]:addi a4, a4, 2048
[0x80003118]:add a6, a6, a4
[0x8000311c]:lw t3, 368(a6)
[0x80003120]:sub a6, a6, a4
[0x80003124]:lui a4, 1
[0x80003128]:addi a4, a4, 2048
[0x8000312c]:add a6, a6, a4
[0x80003130]:lw t4, 372(a6)
[0x80003134]:sub a6, a6, a4
[0x80003138]:lui a4, 1
[0x8000313c]:addi a4, a4, 2048
[0x80003140]:add a6, a6, a4
[0x80003144]:lw s10, 376(a6)
[0x80003148]:sub a6, a6, a4
[0x8000314c]:lui a4, 1
[0x80003150]:addi a4, a4, 2048
[0x80003154]:add a6, a6, a4
[0x80003158]:lw s11, 380(a6)
[0x8000315c]:sub a6, a6, a4
[0x80003160]:addi t3, zero, 99
[0x80003164]:addi t4, zero, 0
[0x80003168]:addi s10, zero, 96
[0x8000316c]:lui s11, 256
[0x80003170]:addi a4, zero, 0
[0x80003174]:csrrw zero, fcsr, a4
[0x80003178]:fsub.d t5, t3, s10, dyn
[0x8000317c]:csrrs a7, fcsr, zero

[0x80003178]:fsub.d t5, t3, s10, dyn
[0x8000317c]:csrrs a7, fcsr, zero
[0x80003180]:sw t5, 752(ra)
[0x80003184]:sw t6, 760(ra)
[0x80003188]:sw t5, 768(ra)
[0x8000318c]:sw a7, 776(ra)
[0x80003190]:lui a4, 1
[0x80003194]:addi a4, a4, 2048
[0x80003198]:add a6, a6, a4
[0x8000319c]:lw t3, 384(a6)
[0x800031a0]:sub a6, a6, a4
[0x800031a4]:lui a4, 1
[0x800031a8]:addi a4, a4, 2048
[0x800031ac]:add a6, a6, a4
[0x800031b0]:lw t4, 388(a6)
[0x800031b4]:sub a6, a6, a4
[0x800031b8]:lui a4, 1
[0x800031bc]:addi a4, a4, 2048
[0x800031c0]:add a6, a6, a4
[0x800031c4]:lw s10, 392(a6)
[0x800031c8]:sub a6, a6, a4
[0x800031cc]:lui a4, 1
[0x800031d0]:addi a4, a4, 2048
[0x800031d4]:add a6, a6, a4
[0x800031d8]:lw s11, 396(a6)
[0x800031dc]:sub a6, a6, a4
[0x800031e0]:addi t3, zero, 31
[0x800031e4]:addi t4, zero, 0
[0x800031e8]:addi s10, zero, 26
[0x800031ec]:lui s11, 256
[0x800031f0]:addi a4, zero, 0
[0x800031f4]:csrrw zero, fcsr, a4
[0x800031f8]:fsub.d t5, t3, s10, dyn
[0x800031fc]:csrrs a7, fcsr, zero

[0x800031f8]:fsub.d t5, t3, s10, dyn
[0x800031fc]:csrrs a7, fcsr, zero
[0x80003200]:sw t5, 784(ra)
[0x80003204]:sw t6, 792(ra)
[0x80003208]:sw t5, 800(ra)
[0x8000320c]:sw a7, 808(ra)
[0x80003210]:lui a4, 1
[0x80003214]:addi a4, a4, 2048
[0x80003218]:add a6, a6, a4
[0x8000321c]:lw t3, 400(a6)
[0x80003220]:sub a6, a6, a4
[0x80003224]:lui a4, 1
[0x80003228]:addi a4, a4, 2048
[0x8000322c]:add a6, a6, a4
[0x80003230]:lw t4, 404(a6)
[0x80003234]:sub a6, a6, a4
[0x80003238]:lui a4, 1
[0x8000323c]:addi a4, a4, 2048
[0x80003240]:add a6, a6, a4
[0x80003244]:lw s10, 408(a6)
[0x80003248]:sub a6, a6, a4
[0x8000324c]:lui a4, 1
[0x80003250]:addi a4, a4, 2048
[0x80003254]:add a6, a6, a4
[0x80003258]:lw s11, 412(a6)
[0x8000325c]:sub a6, a6, a4
[0x80003260]:addi t3, zero, 93
[0x80003264]:addi t4, zero, 0
[0x80003268]:addi s10, zero, 84
[0x8000326c]:lui s11, 256
[0x80003270]:addi a4, zero, 0
[0x80003274]:csrrw zero, fcsr, a4
[0x80003278]:fsub.d t5, t3, s10, dyn
[0x8000327c]:csrrs a7, fcsr, zero

[0x80003278]:fsub.d t5, t3, s10, dyn
[0x8000327c]:csrrs a7, fcsr, zero
[0x80003280]:sw t5, 816(ra)
[0x80003284]:sw t6, 824(ra)
[0x80003288]:sw t5, 832(ra)
[0x8000328c]:sw a7, 840(ra)
[0x80003290]:lui a4, 1
[0x80003294]:addi a4, a4, 2048
[0x80003298]:add a6, a6, a4
[0x8000329c]:lw t3, 416(a6)
[0x800032a0]:sub a6, a6, a4
[0x800032a4]:lui a4, 1
[0x800032a8]:addi a4, a4, 2048
[0x800032ac]:add a6, a6, a4
[0x800032b0]:lw t4, 420(a6)
[0x800032b4]:sub a6, a6, a4
[0x800032b8]:lui a4, 1
[0x800032bc]:addi a4, a4, 2048
[0x800032c0]:add a6, a6, a4
[0x800032c4]:lw s10, 424(a6)
[0x800032c8]:sub a6, a6, a4
[0x800032cc]:lui a4, 1
[0x800032d0]:addi a4, a4, 2048
[0x800032d4]:add a6, a6, a4
[0x800032d8]:lw s11, 428(a6)
[0x800032dc]:sub a6, a6, a4
[0x800032e0]:addi t3, zero, 11
[0x800032e4]:addi t4, zero, 0
[0x800032e8]:addi s10, zero, 4090
[0x800032ec]:lui s11, 256
[0x800032f0]:addi s11, s11, 4095
[0x800032f4]:addi a4, zero, 0
[0x800032f8]:csrrw zero, fcsr, a4
[0x800032fc]:fsub.d t5, t3, s10, dyn
[0x80003300]:csrrs a7, fcsr, zero

[0x800032fc]:fsub.d t5, t3, s10, dyn
[0x80003300]:csrrs a7, fcsr, zero
[0x80003304]:sw t5, 848(ra)
[0x80003308]:sw t6, 856(ra)
[0x8000330c]:sw t5, 864(ra)
[0x80003310]:sw a7, 872(ra)
[0x80003314]:lui a4, 1
[0x80003318]:addi a4, a4, 2048
[0x8000331c]:add a6, a6, a4
[0x80003320]:lw t3, 432(a6)
[0x80003324]:sub a6, a6, a4
[0x80003328]:lui a4, 1
[0x8000332c]:addi a4, a4, 2048
[0x80003330]:add a6, a6, a4
[0x80003334]:lw t4, 436(a6)
[0x80003338]:sub a6, a6, a4
[0x8000333c]:lui a4, 1
[0x80003340]:addi a4, a4, 2048
[0x80003344]:add a6, a6, a4
[0x80003348]:lw s10, 440(a6)
[0x8000334c]:sub a6, a6, a4
[0x80003350]:lui a4, 1
[0x80003354]:addi a4, a4, 2048
[0x80003358]:add a6, a6, a4
[0x8000335c]:lw s11, 444(a6)
[0x80003360]:sub a6, a6, a4
[0x80003364]:addi t3, zero, 22
[0x80003368]:addi t4, zero, 0
[0x8000336c]:addi s10, zero, 4085
[0x80003370]:lui s11, 256
[0x80003374]:addi s11, s11, 4095
[0x80003378]:addi a4, zero, 0
[0x8000337c]:csrrw zero, fcsr, a4
[0x80003380]:fsub.d t5, t3, s10, dyn
[0x80003384]:csrrs a7, fcsr, zero

[0x80003380]:fsub.d t5, t3, s10, dyn
[0x80003384]:csrrs a7, fcsr, zero
[0x80003388]:sw t5, 880(ra)
[0x8000338c]:sw t6, 888(ra)
[0x80003390]:sw t5, 896(ra)
[0x80003394]:sw a7, 904(ra)
[0x80003398]:lui a4, 1
[0x8000339c]:addi a4, a4, 2048
[0x800033a0]:add a6, a6, a4
[0x800033a4]:lw t3, 448(a6)
[0x800033a8]:sub a6, a6, a4
[0x800033ac]:lui a4, 1
[0x800033b0]:addi a4, a4, 2048
[0x800033b4]:add a6, a6, a4
[0x800033b8]:lw t4, 452(a6)
[0x800033bc]:sub a6, a6, a4
[0x800033c0]:lui a4, 1
[0x800033c4]:addi a4, a4, 2048
[0x800033c8]:add a6, a6, a4
[0x800033cc]:lw s10, 456(a6)
[0x800033d0]:sub a6, a6, a4
[0x800033d4]:lui a4, 1
[0x800033d8]:addi a4, a4, 2048
[0x800033dc]:add a6, a6, a4
[0x800033e0]:lw s11, 460(a6)
[0x800033e4]:sub a6, a6, a4
[0x800033e8]:addi t3, zero, 69
[0x800033ec]:addi t4, zero, 0
[0x800033f0]:addi s10, zero, 4
[0x800033f4]:lui s11, 256
[0x800033f8]:addi a4, zero, 0
[0x800033fc]:csrrw zero, fcsr, a4
[0x80003400]:fsub.d t5, t3, s10, dyn
[0x80003404]:csrrs a7, fcsr, zero

[0x80003400]:fsub.d t5, t3, s10, dyn
[0x80003404]:csrrs a7, fcsr, zero
[0x80003408]:sw t5, 912(ra)
[0x8000340c]:sw t6, 920(ra)
[0x80003410]:sw t5, 928(ra)
[0x80003414]:sw a7, 936(ra)
[0x80003418]:lui a4, 1
[0x8000341c]:addi a4, a4, 2048
[0x80003420]:add a6, a6, a4
[0x80003424]:lw t3, 464(a6)
[0x80003428]:sub a6, a6, a4
[0x8000342c]:lui a4, 1
[0x80003430]:addi a4, a4, 2048
[0x80003434]:add a6, a6, a4
[0x80003438]:lw t4, 468(a6)
[0x8000343c]:sub a6, a6, a4
[0x80003440]:lui a4, 1
[0x80003444]:addi a4, a4, 2048
[0x80003448]:add a6, a6, a4
[0x8000344c]:lw s10, 472(a6)
[0x80003450]:sub a6, a6, a4
[0x80003454]:lui a4, 1
[0x80003458]:addi a4, a4, 2048
[0x8000345c]:add a6, a6, a4
[0x80003460]:lw s11, 476(a6)
[0x80003464]:sub a6, a6, a4
[0x80003468]:addi t3, zero, 35
[0x8000346c]:addi t4, zero, 0
[0x80003470]:addi s10, zero, 4002
[0x80003474]:lui s11, 256
[0x80003478]:addi s11, s11, 4095
[0x8000347c]:addi a4, zero, 0
[0x80003480]:csrrw zero, fcsr, a4
[0x80003484]:fsub.d t5, t3, s10, dyn
[0x80003488]:csrrs a7, fcsr, zero

[0x80003484]:fsub.d t5, t3, s10, dyn
[0x80003488]:csrrs a7, fcsr, zero
[0x8000348c]:sw t5, 944(ra)
[0x80003490]:sw t6, 952(ra)
[0x80003494]:sw t5, 960(ra)
[0x80003498]:sw a7, 968(ra)
[0x8000349c]:lui a4, 1
[0x800034a0]:addi a4, a4, 2048
[0x800034a4]:add a6, a6, a4
[0x800034a8]:lw t3, 480(a6)
[0x800034ac]:sub a6, a6, a4
[0x800034b0]:lui a4, 1
[0x800034b4]:addi a4, a4, 2048
[0x800034b8]:add a6, a6, a4
[0x800034bc]:lw t4, 484(a6)
[0x800034c0]:sub a6, a6, a4
[0x800034c4]:lui a4, 1
[0x800034c8]:addi a4, a4, 2048
[0x800034cc]:add a6, a6, a4
[0x800034d0]:lw s10, 488(a6)
[0x800034d4]:sub a6, a6, a4
[0x800034d8]:lui a4, 1
[0x800034dc]:addi a4, a4, 2048
[0x800034e0]:add a6, a6, a4
[0x800034e4]:lw s11, 492(a6)
[0x800034e8]:sub a6, a6, a4
[0x800034ec]:addi t3, zero, 43
[0x800034f0]:addi t4, zero, 0
[0x800034f4]:addi s10, zero, 3882
[0x800034f8]:lui s11, 256
[0x800034fc]:addi s11, s11, 4095
[0x80003500]:addi a4, zero, 0
[0x80003504]:csrrw zero, fcsr, a4
[0x80003508]:fsub.d t5, t3, s10, dyn
[0x8000350c]:csrrs a7, fcsr, zero

[0x80003508]:fsub.d t5, t3, s10, dyn
[0x8000350c]:csrrs a7, fcsr, zero
[0x80003510]:sw t5, 976(ra)
[0x80003514]:sw t6, 984(ra)
[0x80003518]:sw t5, 992(ra)
[0x8000351c]:sw a7, 1000(ra)
[0x80003520]:lui a4, 1
[0x80003524]:addi a4, a4, 2048
[0x80003528]:add a6, a6, a4
[0x8000352c]:lw t3, 496(a6)
[0x80003530]:sub a6, a6, a4
[0x80003534]:lui a4, 1
[0x80003538]:addi a4, a4, 2048
[0x8000353c]:add a6, a6, a4
[0x80003540]:lw t4, 500(a6)
[0x80003544]:sub a6, a6, a4
[0x80003548]:lui a4, 1
[0x8000354c]:addi a4, a4, 2048
[0x80003550]:add a6, a6, a4
[0x80003554]:lw s10, 504(a6)
[0x80003558]:sub a6, a6, a4
[0x8000355c]:lui a4, 1
[0x80003560]:addi a4, a4, 2048
[0x80003564]:add a6, a6, a4
[0x80003568]:lw s11, 508(a6)
[0x8000356c]:sub a6, a6, a4
[0x80003570]:addi t3, zero, 65
[0x80003574]:addi t4, zero, 0
[0x80003578]:addi s10, zero, 3648
[0x8000357c]:lui s11, 256
[0x80003580]:addi s11, s11, 4095
[0x80003584]:addi a4, zero, 0
[0x80003588]:csrrw zero, fcsr, a4
[0x8000358c]:fsub.d t5, t3, s10, dyn
[0x80003590]:csrrs a7, fcsr, zero

[0x8000358c]:fsub.d t5, t3, s10, dyn
[0x80003590]:csrrs a7, fcsr, zero
[0x80003594]:sw t5, 1008(ra)
[0x80003598]:sw t6, 1016(ra)
[0x8000359c]:sw t5, 1024(ra)
[0x800035a0]:sw a7, 1032(ra)
[0x800035a4]:lui a4, 1
[0x800035a8]:addi a4, a4, 2048
[0x800035ac]:add a6, a6, a4
[0x800035b0]:lw t3, 512(a6)
[0x800035b4]:sub a6, a6, a4
[0x800035b8]:lui a4, 1
[0x800035bc]:addi a4, a4, 2048
[0x800035c0]:add a6, a6, a4
[0x800035c4]:lw t4, 516(a6)
[0x800035c8]:sub a6, a6, a4
[0x800035cc]:lui a4, 1
[0x800035d0]:addi a4, a4, 2048
[0x800035d4]:add a6, a6, a4
[0x800035d8]:lw s10, 520(a6)
[0x800035dc]:sub a6, a6, a4
[0x800035e0]:lui a4, 1
[0x800035e4]:addi a4, a4, 2048
[0x800035e8]:add a6, a6, a4
[0x800035ec]:lw s11, 524(a6)
[0x800035f0]:sub a6, a6, a4
[0x800035f4]:addi t3, zero, 48
[0x800035f8]:addi t4, zero, 0
[0x800035fc]:addi s10, zero, 3119
[0x80003600]:lui s11, 256
[0x80003604]:addi s11, s11, 4095
[0x80003608]:addi a4, zero, 0
[0x8000360c]:csrrw zero, fcsr, a4
[0x80003610]:fsub.d t5, t3, s10, dyn
[0x80003614]:csrrs a7, fcsr, zero

[0x80003610]:fsub.d t5, t3, s10, dyn
[0x80003614]:csrrs a7, fcsr, zero
[0x80003618]:sw t5, 1040(ra)
[0x8000361c]:sw t6, 1048(ra)
[0x80003620]:sw t5, 1056(ra)
[0x80003624]:sw a7, 1064(ra)
[0x80003628]:lui a4, 1
[0x8000362c]:addi a4, a4, 2048
[0x80003630]:add a6, a6, a4
[0x80003634]:lw t3, 528(a6)
[0x80003638]:sub a6, a6, a4
[0x8000363c]:lui a4, 1
[0x80003640]:addi a4, a4, 2048
[0x80003644]:add a6, a6, a4
[0x80003648]:lw t4, 532(a6)
[0x8000364c]:sub a6, a6, a4
[0x80003650]:lui a4, 1
[0x80003654]:addi a4, a4, 2048
[0x80003658]:add a6, a6, a4
[0x8000365c]:lw s10, 536(a6)
[0x80003660]:sub a6, a6, a4
[0x80003664]:lui a4, 1
[0x80003668]:addi a4, a4, 2048
[0x8000366c]:add a6, a6, a4
[0x80003670]:lw s11, 540(a6)
[0x80003674]:sub a6, a6, a4
[0x80003678]:addi t3, zero, 44
[0x8000367c]:addi t4, zero, 0
[0x80003680]:addi s10, zero, 2091
[0x80003684]:lui s11, 256
[0x80003688]:addi s11, s11, 4095
[0x8000368c]:addi a4, zero, 0
[0x80003690]:csrrw zero, fcsr, a4
[0x80003694]:fsub.d t5, t3, s10, dyn
[0x80003698]:csrrs a7, fcsr, zero

[0x80003694]:fsub.d t5, t3, s10, dyn
[0x80003698]:csrrs a7, fcsr, zero
[0x8000369c]:sw t5, 1072(ra)
[0x800036a0]:sw t6, 1080(ra)
[0x800036a4]:sw t5, 1088(ra)
[0x800036a8]:sw a7, 1096(ra)
[0x800036ac]:lui a4, 1
[0x800036b0]:addi a4, a4, 2048
[0x800036b4]:add a6, a6, a4
[0x800036b8]:lw t3, 544(a6)
[0x800036bc]:sub a6, a6, a4
[0x800036c0]:lui a4, 1
[0x800036c4]:addi a4, a4, 2048
[0x800036c8]:add a6, a6, a4
[0x800036cc]:lw t4, 548(a6)
[0x800036d0]:sub a6, a6, a4
[0x800036d4]:lui a4, 1
[0x800036d8]:addi a4, a4, 2048
[0x800036dc]:add a6, a6, a4
[0x800036e0]:lw s10, 552(a6)
[0x800036e4]:sub a6, a6, a4
[0x800036e8]:lui a4, 1
[0x800036ec]:addi a4, a4, 2048
[0x800036f0]:add a6, a6, a4
[0x800036f4]:lw s11, 556(a6)
[0x800036f8]:sub a6, a6, a4
[0x800036fc]:addi t3, zero, 38
[0x80003700]:addi t4, zero, 0
[0x80003704]:lui s10, 1048575
[0x80003708]:addi s10, s10, 37
[0x8000370c]:lui s11, 256
[0x80003710]:addi s11, s11, 4095
[0x80003714]:addi a4, zero, 0
[0x80003718]:csrrw zero, fcsr, a4
[0x8000371c]:fsub.d t5, t3, s10, dyn
[0x80003720]:csrrs a7, fcsr, zero

[0x8000371c]:fsub.d t5, t3, s10, dyn
[0x80003720]:csrrs a7, fcsr, zero
[0x80003724]:sw t5, 1104(ra)
[0x80003728]:sw t6, 1112(ra)
[0x8000372c]:sw t5, 1120(ra)
[0x80003730]:sw a7, 1128(ra)
[0x80003734]:lui a4, 1
[0x80003738]:addi a4, a4, 2048
[0x8000373c]:add a6, a6, a4
[0x80003740]:lw t3, 560(a6)
[0x80003744]:sub a6, a6, a4
[0x80003748]:lui a4, 1
[0x8000374c]:addi a4, a4, 2048
[0x80003750]:add a6, a6, a4
[0x80003754]:lw t4, 564(a6)
[0x80003758]:sub a6, a6, a4
[0x8000375c]:lui a4, 1
[0x80003760]:addi a4, a4, 2048
[0x80003764]:add a6, a6, a4
[0x80003768]:lw s10, 568(a6)
[0x8000376c]:sub a6, a6, a4
[0x80003770]:lui a4, 1
[0x80003774]:addi a4, a4, 2048
[0x80003778]:add a6, a6, a4
[0x8000377c]:lw s11, 572(a6)
[0x80003780]:sub a6, a6, a4
[0x80003784]:addi t3, zero, 78
[0x80003788]:addi t4, zero, 0
[0x8000378c]:lui s10, 1048574
[0x80003790]:addi s10, s10, 77
[0x80003794]:lui s11, 256
[0x80003798]:addi s11, s11, 4095
[0x8000379c]:addi a4, zero, 0
[0x800037a0]:csrrw zero, fcsr, a4
[0x800037a4]:fsub.d t5, t3, s10, dyn
[0x800037a8]:csrrs a7, fcsr, zero

[0x800037a4]:fsub.d t5, t3, s10, dyn
[0x800037a8]:csrrs a7, fcsr, zero
[0x800037ac]:sw t5, 1136(ra)
[0x800037b0]:sw t6, 1144(ra)
[0x800037b4]:sw t5, 1152(ra)
[0x800037b8]:sw a7, 1160(ra)
[0x800037bc]:lui a4, 1
[0x800037c0]:addi a4, a4, 2048
[0x800037c4]:add a6, a6, a4
[0x800037c8]:lw t3, 576(a6)
[0x800037cc]:sub a6, a6, a4
[0x800037d0]:lui a4, 1
[0x800037d4]:addi a4, a4, 2048
[0x800037d8]:add a6, a6, a4
[0x800037dc]:lw t4, 580(a6)
[0x800037e0]:sub a6, a6, a4
[0x800037e4]:lui a4, 1
[0x800037e8]:addi a4, a4, 2048
[0x800037ec]:add a6, a6, a4
[0x800037f0]:lw s10, 584(a6)
[0x800037f4]:sub a6, a6, a4
[0x800037f8]:lui a4, 1
[0x800037fc]:addi a4, a4, 2048
[0x80003800]:add a6, a6, a4
[0x80003804]:lw s11, 588(a6)
[0x80003808]:sub a6, a6, a4
[0x8000380c]:addi t3, zero, 63
[0x80003810]:addi t4, zero, 0
[0x80003814]:lui s10, 1048572
[0x80003818]:addi s10, s10, 62
[0x8000381c]:lui s11, 256
[0x80003820]:addi s11, s11, 4095
[0x80003824]:addi a4, zero, 0
[0x80003828]:csrrw zero, fcsr, a4
[0x8000382c]:fsub.d t5, t3, s10, dyn
[0x80003830]:csrrs a7, fcsr, zero

[0x8000382c]:fsub.d t5, t3, s10, dyn
[0x80003830]:csrrs a7, fcsr, zero
[0x80003834]:sw t5, 1168(ra)
[0x80003838]:sw t6, 1176(ra)
[0x8000383c]:sw t5, 1184(ra)
[0x80003840]:sw a7, 1192(ra)
[0x80003844]:lui a4, 1
[0x80003848]:addi a4, a4, 2048
[0x8000384c]:add a6, a6, a4
[0x80003850]:lw t3, 592(a6)
[0x80003854]:sub a6, a6, a4
[0x80003858]:lui a4, 1
[0x8000385c]:addi a4, a4, 2048
[0x80003860]:add a6, a6, a4
[0x80003864]:lw t4, 596(a6)
[0x80003868]:sub a6, a6, a4
[0x8000386c]:lui a4, 1
[0x80003870]:addi a4, a4, 2048
[0x80003874]:add a6, a6, a4
[0x80003878]:lw s10, 600(a6)
[0x8000387c]:sub a6, a6, a4
[0x80003880]:lui a4, 1
[0x80003884]:addi a4, a4, 2048
[0x80003888]:add a6, a6, a4
[0x8000388c]:lw s11, 604(a6)
[0x80003890]:sub a6, a6, a4
[0x80003894]:addi t3, zero, 75
[0x80003898]:addi t4, zero, 0
[0x8000389c]:lui s10, 1048568
[0x800038a0]:addi s10, s10, 74
[0x800038a4]:lui s11, 256
[0x800038a8]:addi s11, s11, 4095
[0x800038ac]:addi a4, zero, 0
[0x800038b0]:csrrw zero, fcsr, a4
[0x800038b4]:fsub.d t5, t3, s10, dyn
[0x800038b8]:csrrs a7, fcsr, zero

[0x800038b4]:fsub.d t5, t3, s10, dyn
[0x800038b8]:csrrs a7, fcsr, zero
[0x800038bc]:sw t5, 1200(ra)
[0x800038c0]:sw t6, 1208(ra)
[0x800038c4]:sw t5, 1216(ra)
[0x800038c8]:sw a7, 1224(ra)
[0x800038cc]:lui a4, 1
[0x800038d0]:addi a4, a4, 2048
[0x800038d4]:add a6, a6, a4
[0x800038d8]:lw t3, 608(a6)
[0x800038dc]:sub a6, a6, a4
[0x800038e0]:lui a4, 1
[0x800038e4]:addi a4, a4, 2048
[0x800038e8]:add a6, a6, a4
[0x800038ec]:lw t4, 612(a6)
[0x800038f0]:sub a6, a6, a4
[0x800038f4]:lui a4, 1
[0x800038f8]:addi a4, a4, 2048
[0x800038fc]:add a6, a6, a4
[0x80003900]:lw s10, 616(a6)
[0x80003904]:sub a6, a6, a4
[0x80003908]:lui a4, 1
[0x8000390c]:addi a4, a4, 2048
[0x80003910]:add a6, a6, a4
[0x80003914]:lw s11, 620(a6)
[0x80003918]:sub a6, a6, a4
[0x8000391c]:addi t3, zero, 99
[0x80003920]:addi t4, zero, 0
[0x80003924]:lui s10, 1048560
[0x80003928]:addi s10, s10, 98
[0x8000392c]:lui s11, 256
[0x80003930]:addi s11, s11, 4095
[0x80003934]:addi a4, zero, 0
[0x80003938]:csrrw zero, fcsr, a4
[0x8000393c]:fsub.d t5, t3, s10, dyn
[0x80003940]:csrrs a7, fcsr, zero

[0x8000393c]:fsub.d t5, t3, s10, dyn
[0x80003940]:csrrs a7, fcsr, zero
[0x80003944]:sw t5, 1232(ra)
[0x80003948]:sw t6, 1240(ra)
[0x8000394c]:sw t5, 1248(ra)
[0x80003950]:sw a7, 1256(ra)
[0x80003954]:lui a4, 1
[0x80003958]:addi a4, a4, 2048
[0x8000395c]:add a6, a6, a4
[0x80003960]:lw t3, 624(a6)
[0x80003964]:sub a6, a6, a4
[0x80003968]:lui a4, 1
[0x8000396c]:addi a4, a4, 2048
[0x80003970]:add a6, a6, a4
[0x80003974]:lw t4, 628(a6)
[0x80003978]:sub a6, a6, a4
[0x8000397c]:lui a4, 1
[0x80003980]:addi a4, a4, 2048
[0x80003984]:add a6, a6, a4
[0x80003988]:lw s10, 632(a6)
[0x8000398c]:sub a6, a6, a4
[0x80003990]:lui a4, 1
[0x80003994]:addi a4, a4, 2048
[0x80003998]:add a6, a6, a4
[0x8000399c]:lw s11, 636(a6)
[0x800039a0]:sub a6, a6, a4
[0x800039a4]:addi t3, zero, 42
[0x800039a8]:addi t4, zero, 0
[0x800039ac]:lui s10, 1048544
[0x800039b0]:addi s10, s10, 41
[0x800039b4]:lui s11, 256
[0x800039b8]:addi s11, s11, 4095
[0x800039bc]:addi a4, zero, 0
[0x800039c0]:csrrw zero, fcsr, a4
[0x800039c4]:fsub.d t5, t3, s10, dyn
[0x800039c8]:csrrs a7, fcsr, zero

[0x800039c4]:fsub.d t5, t3, s10, dyn
[0x800039c8]:csrrs a7, fcsr, zero
[0x800039cc]:sw t5, 1264(ra)
[0x800039d0]:sw t6, 1272(ra)
[0x800039d4]:sw t5, 1280(ra)
[0x800039d8]:sw a7, 1288(ra)
[0x800039dc]:lui a4, 1
[0x800039e0]:addi a4, a4, 2048
[0x800039e4]:add a6, a6, a4
[0x800039e8]:lw t3, 640(a6)
[0x800039ec]:sub a6, a6, a4
[0x800039f0]:lui a4, 1
[0x800039f4]:addi a4, a4, 2048
[0x800039f8]:add a6, a6, a4
[0x800039fc]:lw t4, 644(a6)
[0x80003a00]:sub a6, a6, a4
[0x80003a04]:lui a4, 1
[0x80003a08]:addi a4, a4, 2048
[0x80003a0c]:add a6, a6, a4
[0x80003a10]:lw s10, 648(a6)
[0x80003a14]:sub a6, a6, a4
[0x80003a18]:lui a4, 1
[0x80003a1c]:addi a4, a4, 2048
[0x80003a20]:add a6, a6, a4
[0x80003a24]:lw s11, 652(a6)
[0x80003a28]:sub a6, a6, a4
[0x80003a2c]:addi t3, zero, 53
[0x80003a30]:addi t4, zero, 0
[0x80003a34]:lui s10, 1048512
[0x80003a38]:addi s10, s10, 52
[0x80003a3c]:lui s11, 256
[0x80003a40]:addi s11, s11, 4095
[0x80003a44]:addi a4, zero, 0
[0x80003a48]:csrrw zero, fcsr, a4
[0x80003a4c]:fsub.d t5, t3, s10, dyn
[0x80003a50]:csrrs a7, fcsr, zero

[0x80003a4c]:fsub.d t5, t3, s10, dyn
[0x80003a50]:csrrs a7, fcsr, zero
[0x80003a54]:sw t5, 1296(ra)
[0x80003a58]:sw t6, 1304(ra)
[0x80003a5c]:sw t5, 1312(ra)
[0x80003a60]:sw a7, 1320(ra)
[0x80003a64]:lui a4, 1
[0x80003a68]:addi a4, a4, 2048
[0x80003a6c]:add a6, a6, a4
[0x80003a70]:lw t3, 656(a6)
[0x80003a74]:sub a6, a6, a4
[0x80003a78]:lui a4, 1
[0x80003a7c]:addi a4, a4, 2048
[0x80003a80]:add a6, a6, a4
[0x80003a84]:lw t4, 660(a6)
[0x80003a88]:sub a6, a6, a4
[0x80003a8c]:lui a4, 1
[0x80003a90]:addi a4, a4, 2048
[0x80003a94]:add a6, a6, a4
[0x80003a98]:lw s10, 664(a6)
[0x80003a9c]:sub a6, a6, a4
[0x80003aa0]:lui a4, 1
[0x80003aa4]:addi a4, a4, 2048
[0x80003aa8]:add a6, a6, a4
[0x80003aac]:lw s11, 668(a6)
[0x80003ab0]:sub a6, a6, a4
[0x80003ab4]:addi t3, zero, 49
[0x80003ab8]:addi t4, zero, 0
[0x80003abc]:lui s10, 1048448
[0x80003ac0]:addi s10, s10, 48
[0x80003ac4]:lui s11, 256
[0x80003ac8]:addi s11, s11, 4095
[0x80003acc]:addi a4, zero, 0
[0x80003ad0]:csrrw zero, fcsr, a4
[0x80003ad4]:fsub.d t5, t3, s10, dyn
[0x80003ad8]:csrrs a7, fcsr, zero

[0x80003ad4]:fsub.d t5, t3, s10, dyn
[0x80003ad8]:csrrs a7, fcsr, zero
[0x80003adc]:sw t5, 1328(ra)
[0x80003ae0]:sw t6, 1336(ra)
[0x80003ae4]:sw t5, 1344(ra)
[0x80003ae8]:sw a7, 1352(ra)
[0x80003aec]:lui a4, 1
[0x80003af0]:addi a4, a4, 2048
[0x80003af4]:add a6, a6, a4
[0x80003af8]:lw t3, 672(a6)
[0x80003afc]:sub a6, a6, a4
[0x80003b00]:lui a4, 1
[0x80003b04]:addi a4, a4, 2048
[0x80003b08]:add a6, a6, a4
[0x80003b0c]:lw t4, 676(a6)
[0x80003b10]:sub a6, a6, a4
[0x80003b14]:lui a4, 1
[0x80003b18]:addi a4, a4, 2048
[0x80003b1c]:add a6, a6, a4
[0x80003b20]:lw s10, 680(a6)
[0x80003b24]:sub a6, a6, a4
[0x80003b28]:lui a4, 1
[0x80003b2c]:addi a4, a4, 2048
[0x80003b30]:add a6, a6, a4
[0x80003b34]:lw s11, 684(a6)
[0x80003b38]:sub a6, a6, a4
[0x80003b3c]:addi t3, zero, 17
[0x80003b40]:addi t4, zero, 0
[0x80003b44]:lui s10, 1048320
[0x80003b48]:addi s10, s10, 16
[0x80003b4c]:lui s11, 256
[0x80003b50]:addi s11, s11, 4095
[0x80003b54]:addi a4, zero, 0
[0x80003b58]:csrrw zero, fcsr, a4
[0x80003b5c]:fsub.d t5, t3, s10, dyn
[0x80003b60]:csrrs a7, fcsr, zero

[0x80003b5c]:fsub.d t5, t3, s10, dyn
[0x80003b60]:csrrs a7, fcsr, zero
[0x80003b64]:sw t5, 1360(ra)
[0x80003b68]:sw t6, 1368(ra)
[0x80003b6c]:sw t5, 1376(ra)
[0x80003b70]:sw a7, 1384(ra)
[0x80003b74]:lui a4, 1
[0x80003b78]:addi a4, a4, 2048
[0x80003b7c]:add a6, a6, a4
[0x80003b80]:lw t3, 688(a6)
[0x80003b84]:sub a6, a6, a4
[0x80003b88]:lui a4, 1
[0x80003b8c]:addi a4, a4, 2048
[0x80003b90]:add a6, a6, a4
[0x80003b94]:lw t4, 692(a6)
[0x80003b98]:sub a6, a6, a4
[0x80003b9c]:lui a4, 1
[0x80003ba0]:addi a4, a4, 2048
[0x80003ba4]:add a6, a6, a4
[0x80003ba8]:lw s10, 696(a6)
[0x80003bac]:sub a6, a6, a4
[0x80003bb0]:lui a4, 1
[0x80003bb4]:addi a4, a4, 2048
[0x80003bb8]:add a6, a6, a4
[0x80003bbc]:lw s11, 700(a6)
[0x80003bc0]:sub a6, a6, a4
[0x80003bc4]:addi t3, zero, 15
[0x80003bc8]:addi t4, zero, 0
[0x80003bcc]:lui s10, 1048064
[0x80003bd0]:addi s10, s10, 14
[0x80003bd4]:lui s11, 256
[0x80003bd8]:addi s11, s11, 4095
[0x80003bdc]:addi a4, zero, 0
[0x80003be0]:csrrw zero, fcsr, a4
[0x80003be4]:fsub.d t5, t3, s10, dyn
[0x80003be8]:csrrs a7, fcsr, zero

[0x80003be4]:fsub.d t5, t3, s10, dyn
[0x80003be8]:csrrs a7, fcsr, zero
[0x80003bec]:sw t5, 1392(ra)
[0x80003bf0]:sw t6, 1400(ra)
[0x80003bf4]:sw t5, 1408(ra)
[0x80003bf8]:sw a7, 1416(ra)
[0x80003bfc]:lui a4, 1
[0x80003c00]:addi a4, a4, 2048
[0x80003c04]:add a6, a6, a4
[0x80003c08]:lw t3, 704(a6)
[0x80003c0c]:sub a6, a6, a4
[0x80003c10]:lui a4, 1
[0x80003c14]:addi a4, a4, 2048
[0x80003c18]:add a6, a6, a4
[0x80003c1c]:lw t4, 708(a6)
[0x80003c20]:sub a6, a6, a4
[0x80003c24]:lui a4, 1
[0x80003c28]:addi a4, a4, 2048
[0x80003c2c]:add a6, a6, a4
[0x80003c30]:lw s10, 712(a6)
[0x80003c34]:sub a6, a6, a4
[0x80003c38]:lui a4, 1
[0x80003c3c]:addi a4, a4, 2048
[0x80003c40]:add a6, a6, a4
[0x80003c44]:lw s11, 716(a6)
[0x80003c48]:sub a6, a6, a4
[0x80003c4c]:addi t3, zero, 76
[0x80003c50]:addi t4, zero, 0
[0x80003c54]:lui s10, 1047552
[0x80003c58]:addi s10, s10, 75
[0x80003c5c]:lui s11, 256
[0x80003c60]:addi s11, s11, 4095
[0x80003c64]:addi a4, zero, 0
[0x80003c68]:csrrw zero, fcsr, a4
[0x80003c6c]:fsub.d t5, t3, s10, dyn
[0x80003c70]:csrrs a7, fcsr, zero

[0x80003c6c]:fsub.d t5, t3, s10, dyn
[0x80003c70]:csrrs a7, fcsr, zero
[0x80003c74]:sw t5, 1424(ra)
[0x80003c78]:sw t6, 1432(ra)
[0x80003c7c]:sw t5, 1440(ra)
[0x80003c80]:sw a7, 1448(ra)
[0x80003c84]:lui a4, 1
[0x80003c88]:addi a4, a4, 2048
[0x80003c8c]:add a6, a6, a4
[0x80003c90]:lw t3, 720(a6)
[0x80003c94]:sub a6, a6, a4
[0x80003c98]:lui a4, 1
[0x80003c9c]:addi a4, a4, 2048
[0x80003ca0]:add a6, a6, a4
[0x80003ca4]:lw t4, 724(a6)
[0x80003ca8]:sub a6, a6, a4
[0x80003cac]:lui a4, 1
[0x80003cb0]:addi a4, a4, 2048
[0x80003cb4]:add a6, a6, a4
[0x80003cb8]:lw s10, 728(a6)
[0x80003cbc]:sub a6, a6, a4
[0x80003cc0]:lui a4, 1
[0x80003cc4]:addi a4, a4, 2048
[0x80003cc8]:add a6, a6, a4
[0x80003ccc]:lw s11, 732(a6)
[0x80003cd0]:sub a6, a6, a4
[0x80003cd4]:addi t3, zero, 10
[0x80003cd8]:lui t4, 256
[0x80003cdc]:addi s10, zero, 9
[0x80003ce0]:addi s11, zero, 0
[0x80003ce4]:addi a4, zero, 0
[0x80003ce8]:csrrw zero, fcsr, a4
[0x80003cec]:fsub.d t5, t3, s10, dyn
[0x80003cf0]:csrrs a7, fcsr, zero

[0x80003cec]:fsub.d t5, t3, s10, dyn
[0x80003cf0]:csrrs a7, fcsr, zero
[0x80003cf4]:sw t5, 1456(ra)
[0x80003cf8]:sw t6, 1464(ra)
[0x80003cfc]:sw t5, 1472(ra)
[0x80003d00]:sw a7, 1480(ra)
[0x80003d04]:lui a4, 1
[0x80003d08]:addi a4, a4, 2048
[0x80003d0c]:add a6, a6, a4
[0x80003d10]:lw t3, 736(a6)
[0x80003d14]:sub a6, a6, a4
[0x80003d18]:lui a4, 1
[0x80003d1c]:addi a4, a4, 2048
[0x80003d20]:add a6, a6, a4
[0x80003d24]:lw t4, 740(a6)
[0x80003d28]:sub a6, a6, a4
[0x80003d2c]:lui a4, 1
[0x80003d30]:addi a4, a4, 2048
[0x80003d34]:add a6, a6, a4
[0x80003d38]:lw s10, 744(a6)
[0x80003d3c]:sub a6, a6, a4
[0x80003d40]:lui a4, 1
[0x80003d44]:addi a4, a4, 2048
[0x80003d48]:add a6, a6, a4
[0x80003d4c]:lw s11, 748(a6)
[0x80003d50]:sub a6, a6, a4
[0x80003d54]:addi t3, zero, 71
[0x80003d58]:lui t4, 256
[0x80003d5c]:addi s10, zero, 69
[0x80003d60]:addi s11, zero, 0
[0x80003d64]:addi a4, zero, 0
[0x80003d68]:csrrw zero, fcsr, a4
[0x80003d6c]:fsub.d t5, t3, s10, dyn
[0x80003d70]:csrrs a7, fcsr, zero

[0x80003d6c]:fsub.d t5, t3, s10, dyn
[0x80003d70]:csrrs a7, fcsr, zero
[0x80003d74]:sw t5, 1488(ra)
[0x80003d78]:sw t6, 1496(ra)
[0x80003d7c]:sw t5, 1504(ra)
[0x80003d80]:sw a7, 1512(ra)
[0x80003d84]:lui a4, 1
[0x80003d88]:addi a4, a4, 2048
[0x80003d8c]:add a6, a6, a4
[0x80003d90]:lw t3, 752(a6)
[0x80003d94]:sub a6, a6, a4
[0x80003d98]:lui a4, 1
[0x80003d9c]:addi a4, a4, 2048
[0x80003da0]:add a6, a6, a4
[0x80003da4]:lw t4, 756(a6)
[0x80003da8]:sub a6, a6, a4
[0x80003dac]:lui a4, 1
[0x80003db0]:addi a4, a4, 2048
[0x80003db4]:add a6, a6, a4
[0x80003db8]:lw s10, 760(a6)
[0x80003dbc]:sub a6, a6, a4
[0x80003dc0]:lui a4, 1
[0x80003dc4]:addi a4, a4, 2048
[0x80003dc8]:add a6, a6, a4
[0x80003dcc]:lw s11, 764(a6)
[0x80003dd0]:sub a6, a6, a4
[0x80003dd4]:addi t3, zero, 73
[0x80003dd8]:lui t4, 256
[0x80003ddc]:addi s10, zero, 69
[0x80003de0]:addi s11, zero, 0
[0x80003de4]:addi a4, zero, 0
[0x80003de8]:csrrw zero, fcsr, a4
[0x80003dec]:fsub.d t5, t3, s10, dyn
[0x80003df0]:csrrs a7, fcsr, zero

[0x80003dec]:fsub.d t5, t3, s10, dyn
[0x80003df0]:csrrs a7, fcsr, zero
[0x80003df4]:sw t5, 1520(ra)
[0x80003df8]:sw t6, 1528(ra)
[0x80003dfc]:sw t5, 1536(ra)
[0x80003e00]:sw a7, 1544(ra)
[0x80003e04]:lui a4, 1
[0x80003e08]:addi a4, a4, 2048
[0x80003e0c]:add a6, a6, a4
[0x80003e10]:lw t3, 768(a6)
[0x80003e14]:sub a6, a6, a4
[0x80003e18]:lui a4, 1
[0x80003e1c]:addi a4, a4, 2048
[0x80003e20]:add a6, a6, a4
[0x80003e24]:lw t4, 772(a6)
[0x80003e28]:sub a6, a6, a4
[0x80003e2c]:lui a4, 1
[0x80003e30]:addi a4, a4, 2048
[0x80003e34]:add a6, a6, a4
[0x80003e38]:lw s10, 776(a6)
[0x80003e3c]:sub a6, a6, a4
[0x80003e40]:lui a4, 1
[0x80003e44]:addi a4, a4, 2048
[0x80003e48]:add a6, a6, a4
[0x80003e4c]:lw s11, 780(a6)
[0x80003e50]:sub a6, a6, a4
[0x80003e54]:addi t3, zero, 35
[0x80003e58]:lui t4, 256
[0x80003e5c]:addi s10, zero, 27
[0x80003e60]:addi s11, zero, 0
[0x80003e64]:addi a4, zero, 0
[0x80003e68]:csrrw zero, fcsr, a4
[0x80003e6c]:fsub.d t5, t3, s10, dyn
[0x80003e70]:csrrs a7, fcsr, zero

[0x80003e6c]:fsub.d t5, t3, s10, dyn
[0x80003e70]:csrrs a7, fcsr, zero
[0x80003e74]:sw t5, 1552(ra)
[0x80003e78]:sw t6, 1560(ra)
[0x80003e7c]:sw t5, 1568(ra)
[0x80003e80]:sw a7, 1576(ra)
[0x80003e84]:lui a4, 1
[0x80003e88]:addi a4, a4, 2048
[0x80003e8c]:add a6, a6, a4
[0x80003e90]:lw t3, 784(a6)
[0x80003e94]:sub a6, a6, a4
[0x80003e98]:lui a4, 1
[0x80003e9c]:addi a4, a4, 2048
[0x80003ea0]:add a6, a6, a4
[0x80003ea4]:lw t4, 788(a6)
[0x80003ea8]:sub a6, a6, a4
[0x80003eac]:lui a4, 1
[0x80003eb0]:addi a4, a4, 2048
[0x80003eb4]:add a6, a6, a4
[0x80003eb8]:lw s10, 792(a6)
[0x80003ebc]:sub a6, a6, a4
[0x80003ec0]:lui a4, 1
[0x80003ec4]:addi a4, a4, 2048
[0x80003ec8]:add a6, a6, a4
[0x80003ecc]:lw s11, 796(a6)
[0x80003ed0]:sub a6, a6, a4
[0x80003ed4]:addi t3, zero, 38
[0x80003ed8]:lui t4, 256
[0x80003edc]:addi s10, zero, 22
[0x80003ee0]:addi s11, zero, 0
[0x80003ee4]:addi a4, zero, 0
[0x80003ee8]:csrrw zero, fcsr, a4
[0x80003eec]:fsub.d t5, t3, s10, dyn
[0x80003ef0]:csrrs a7, fcsr, zero

[0x80003eec]:fsub.d t5, t3, s10, dyn
[0x80003ef0]:csrrs a7, fcsr, zero
[0x80003ef4]:sw t5, 1584(ra)
[0x80003ef8]:sw t6, 1592(ra)
[0x80003efc]:sw t5, 1600(ra)
[0x80003f00]:sw a7, 1608(ra)
[0x80003f04]:lui a4, 1
[0x80003f08]:addi a4, a4, 2048
[0x80003f0c]:add a6, a6, a4
[0x80003f10]:lw t3, 800(a6)
[0x80003f14]:sub a6, a6, a4
[0x80003f18]:lui a4, 1
[0x80003f1c]:addi a4, a4, 2048
[0x80003f20]:add a6, a6, a4
[0x80003f24]:lw t4, 804(a6)
[0x80003f28]:sub a6, a6, a4
[0x80003f2c]:lui a4, 1
[0x80003f30]:addi a4, a4, 2048
[0x80003f34]:add a6, a6, a4
[0x80003f38]:lw s10, 808(a6)
[0x80003f3c]:sub a6, a6, a4
[0x80003f40]:lui a4, 1
[0x80003f44]:addi a4, a4, 2048
[0x80003f48]:add a6, a6, a4
[0x80003f4c]:lw s11, 812(a6)
[0x80003f50]:sub a6, a6, a4
[0x80003f54]:addi t3, zero, 69
[0x80003f58]:lui t4, 256
[0x80003f5c]:addi s10, zero, 37
[0x80003f60]:addi s11, zero, 0
[0x80003f64]:addi a4, zero, 0
[0x80003f68]:csrrw zero, fcsr, a4
[0x80003f6c]:fsub.d t5, t3, s10, dyn
[0x80003f70]:csrrs a7, fcsr, zero

[0x80003f6c]:fsub.d t5, t3, s10, dyn
[0x80003f70]:csrrs a7, fcsr, zero
[0x80003f74]:sw t5, 1616(ra)
[0x80003f78]:sw t6, 1624(ra)
[0x80003f7c]:sw t5, 1632(ra)
[0x80003f80]:sw a7, 1640(ra)
[0x80003f84]:lui a4, 1
[0x80003f88]:addi a4, a4, 2048
[0x80003f8c]:add a6, a6, a4
[0x80003f90]:lw t3, 816(a6)
[0x80003f94]:sub a6, a6, a4
[0x80003f98]:lui a4, 1
[0x80003f9c]:addi a4, a4, 2048
[0x80003fa0]:add a6, a6, a4
[0x80003fa4]:lw t4, 820(a6)
[0x80003fa8]:sub a6, a6, a4
[0x80003fac]:lui a4, 1
[0x80003fb0]:addi a4, a4, 2048
[0x80003fb4]:add a6, a6, a4
[0x80003fb8]:lw s10, 824(a6)
[0x80003fbc]:sub a6, a6, a4
[0x80003fc0]:lui a4, 1
[0x80003fc4]:addi a4, a4, 2048
[0x80003fc8]:add a6, a6, a4
[0x80003fcc]:lw s11, 828(a6)
[0x80003fd0]:sub a6, a6, a4
[0x80003fd4]:addi t3, zero, 59
[0x80003fd8]:lui t4, 256
[0x80003fdc]:addi s10, zero, 5
[0x80003fe0]:lui s11, 524288
[0x80003fe4]:addi a4, zero, 0
[0x80003fe8]:csrrw zero, fcsr, a4
[0x80003fec]:fsub.d t5, t3, s10, dyn
[0x80003ff0]:csrrs a7, fcsr, zero

[0x80003fec]:fsub.d t5, t3, s10, dyn
[0x80003ff0]:csrrs a7, fcsr, zero
[0x80003ff4]:sw t5, 1648(ra)
[0x80003ff8]:sw t6, 1656(ra)
[0x80003ffc]:sw t5, 1664(ra)
[0x80004000]:sw a7, 1672(ra)
[0x80004004]:lui a4, 1
[0x80004008]:addi a4, a4, 2048
[0x8000400c]:add a6, a6, a4
[0x80004010]:lw t3, 832(a6)
[0x80004014]:sub a6, a6, a4
[0x80004018]:lui a4, 1
[0x8000401c]:addi a4, a4, 2048
[0x80004020]:add a6, a6, a4
[0x80004024]:lw t4, 836(a6)
[0x80004028]:sub a6, a6, a4
[0x8000402c]:lui a4, 1
[0x80004030]:addi a4, a4, 2048
[0x80004034]:add a6, a6, a4
[0x80004038]:lw s10, 840(a6)
[0x8000403c]:sub a6, a6, a4
[0x80004040]:lui a4, 1
[0x80004044]:addi a4, a4, 2048
[0x80004048]:add a6, a6, a4
[0x8000404c]:lw s11, 844(a6)
[0x80004050]:sub a6, a6, a4
[0x80004054]:addi t3, zero, 14
[0x80004058]:lui t4, 256
[0x8000405c]:addi s10, zero, 114
[0x80004060]:lui s11, 524288
[0x80004064]:addi a4, zero, 0
[0x80004068]:csrrw zero, fcsr, a4
[0x8000406c]:fsub.d t5, t3, s10, dyn
[0x80004070]:csrrs a7, fcsr, zero

[0x8000406c]:fsub.d t5, t3, s10, dyn
[0x80004070]:csrrs a7, fcsr, zero
[0x80004074]:sw t5, 1680(ra)
[0x80004078]:sw t6, 1688(ra)
[0x8000407c]:sw t5, 1696(ra)
[0x80004080]:sw a7, 1704(ra)
[0x80004084]:lui a4, 1
[0x80004088]:addi a4, a4, 2048
[0x8000408c]:add a6, a6, a4
[0x80004090]:lw t3, 848(a6)
[0x80004094]:sub a6, a6, a4
[0x80004098]:lui a4, 1
[0x8000409c]:addi a4, a4, 2048
[0x800040a0]:add a6, a6, a4
[0x800040a4]:lw t4, 852(a6)
[0x800040a8]:sub a6, a6, a4
[0x800040ac]:lui a4, 1
[0x800040b0]:addi a4, a4, 2048
[0x800040b4]:add a6, a6, a4
[0x800040b8]:lw s10, 856(a6)
[0x800040bc]:sub a6, a6, a4
[0x800040c0]:lui a4, 1
[0x800040c4]:addi a4, a4, 2048
[0x800040c8]:add a6, a6, a4
[0x800040cc]:lw s11, 860(a6)
[0x800040d0]:sub a6, a6, a4
[0x800040d4]:addi t3, zero, 38
[0x800040d8]:lui t4, 256
[0x800040dc]:addi s10, zero, 218
[0x800040e0]:lui s11, 524288
[0x800040e4]:addi a4, zero, 0
[0x800040e8]:csrrw zero, fcsr, a4
[0x800040ec]:fsub.d t5, t3, s10, dyn
[0x800040f0]:csrrs a7, fcsr, zero

[0x800040ec]:fsub.d t5, t3, s10, dyn
[0x800040f0]:csrrs a7, fcsr, zero
[0x800040f4]:sw t5, 1712(ra)
[0x800040f8]:sw t6, 1720(ra)
[0x800040fc]:sw t5, 1728(ra)
[0x80004100]:sw a7, 1736(ra)
[0x80004104]:lui a4, 1
[0x80004108]:addi a4, a4, 2048
[0x8000410c]:add a6, a6, a4
[0x80004110]:lw t3, 864(a6)
[0x80004114]:sub a6, a6, a4
[0x80004118]:lui a4, 1
[0x8000411c]:addi a4, a4, 2048
[0x80004120]:add a6, a6, a4
[0x80004124]:lw t4, 868(a6)
[0x80004128]:sub a6, a6, a4
[0x8000412c]:lui a4, 1
[0x80004130]:addi a4, a4, 2048
[0x80004134]:add a6, a6, a4
[0x80004138]:lw s10, 872(a6)
[0x8000413c]:sub a6, a6, a4
[0x80004140]:lui a4, 1
[0x80004144]:addi a4, a4, 2048
[0x80004148]:add a6, a6, a4
[0x8000414c]:lw s11, 876(a6)
[0x80004150]:sub a6, a6, a4
[0x80004154]:addi t3, zero, 79
[0x80004158]:lui t4, 256
[0x8000415c]:addi s10, zero, 433
[0x80004160]:lui s11, 524288
[0x80004164]:addi a4, zero, 0
[0x80004168]:csrrw zero, fcsr, a4
[0x8000416c]:fsub.d t5, t3, s10, dyn
[0x80004170]:csrrs a7, fcsr, zero

[0x8000416c]:fsub.d t5, t3, s10, dyn
[0x80004170]:csrrs a7, fcsr, zero
[0x80004174]:sw t5, 1744(ra)
[0x80004178]:sw t6, 1752(ra)
[0x8000417c]:sw t5, 1760(ra)
[0x80004180]:sw a7, 1768(ra)
[0x80004184]:lui a4, 1
[0x80004188]:addi a4, a4, 2048
[0x8000418c]:add a6, a6, a4
[0x80004190]:lw t3, 880(a6)
[0x80004194]:sub a6, a6, a4
[0x80004198]:lui a4, 1
[0x8000419c]:addi a4, a4, 2048
[0x800041a0]:add a6, a6, a4
[0x800041a4]:lw t4, 884(a6)
[0x800041a8]:sub a6, a6, a4
[0x800041ac]:lui a4, 1
[0x800041b0]:addi a4, a4, 2048
[0x800041b4]:add a6, a6, a4
[0x800041b8]:lw s10, 888(a6)
[0x800041bc]:sub a6, a6, a4
[0x800041c0]:lui a4, 1
[0x800041c4]:addi a4, a4, 2048
[0x800041c8]:add a6, a6, a4
[0x800041cc]:lw s11, 892(a6)
[0x800041d0]:sub a6, a6, a4
[0x800041d4]:addi t3, zero, 2
[0x800041d8]:lui t4, 256
[0x800041dc]:addi s10, zero, 1022
[0x800041e0]:lui s11, 524288
[0x800041e4]:addi a4, zero, 0
[0x800041e8]:csrrw zero, fcsr, a4
[0x800041ec]:fsub.d t5, t3, s10, dyn
[0x800041f0]:csrrs a7, fcsr, zero

[0x800041ec]:fsub.d t5, t3, s10, dyn
[0x800041f0]:csrrs a7, fcsr, zero
[0x800041f4]:sw t5, 1776(ra)
[0x800041f8]:sw t6, 1784(ra)
[0x800041fc]:sw t5, 1792(ra)
[0x80004200]:sw a7, 1800(ra)
[0x80004204]:lui a4, 1
[0x80004208]:addi a4, a4, 2048
[0x8000420c]:add a6, a6, a4
[0x80004210]:lw t3, 896(a6)
[0x80004214]:sub a6, a6, a4
[0x80004218]:lui a4, 1
[0x8000421c]:addi a4, a4, 2048
[0x80004220]:add a6, a6, a4
[0x80004224]:lw t4, 900(a6)
[0x80004228]:sub a6, a6, a4
[0x8000422c]:lui a4, 1
[0x80004230]:addi a4, a4, 2048
[0x80004234]:add a6, a6, a4
[0x80004238]:lw s10, 904(a6)
[0x8000423c]:sub a6, a6, a4
[0x80004240]:lui a4, 1
[0x80004244]:addi a4, a4, 2048
[0x80004248]:add a6, a6, a4
[0x8000424c]:lw s11, 908(a6)
[0x80004250]:sub a6, a6, a4
[0x80004254]:addi t3, zero, 53
[0x80004258]:lui t4, 256
[0x8000425c]:addi s10, zero, 1995
[0x80004260]:lui s11, 524288
[0x80004264]:addi a4, zero, 0
[0x80004268]:csrrw zero, fcsr, a4
[0x8000426c]:fsub.d t5, t3, s10, dyn
[0x80004270]:csrrs a7, fcsr, zero

[0x8000426c]:fsub.d t5, t3, s10, dyn
[0x80004270]:csrrs a7, fcsr, zero
[0x80004274]:sw t5, 1808(ra)
[0x80004278]:sw t6, 1816(ra)
[0x8000427c]:sw t5, 1824(ra)
[0x80004280]:sw a7, 1832(ra)
[0x80004284]:lui a4, 1
[0x80004288]:addi a4, a4, 2048
[0x8000428c]:add a6, a6, a4
[0x80004290]:lw t3, 912(a6)
[0x80004294]:sub a6, a6, a4
[0x80004298]:lui a4, 1
[0x8000429c]:addi a4, a4, 2048
[0x800042a0]:add a6, a6, a4
[0x800042a4]:lw t4, 916(a6)
[0x800042a8]:sub a6, a6, a4
[0x800042ac]:lui a4, 1
[0x800042b0]:addi a4, a4, 2048
[0x800042b4]:add a6, a6, a4
[0x800042b8]:lw s10, 920(a6)
[0x800042bc]:sub a6, a6, a4
[0x800042c0]:lui a4, 1
[0x800042c4]:addi a4, a4, 2048
[0x800042c8]:add a6, a6, a4
[0x800042cc]:lw s11, 924(a6)
[0x800042d0]:sub a6, a6, a4
[0x800042d4]:addi t3, zero, 6
[0x800042d8]:lui t4, 256
[0x800042dc]:lui s10, 1
[0x800042e0]:addi s10, s10, 4090
[0x800042e4]:lui s11, 524288
[0x800042e8]:addi a4, zero, 0
[0x800042ec]:csrrw zero, fcsr, a4
[0x800042f0]:fsub.d t5, t3, s10, dyn
[0x800042f4]:csrrs a7, fcsr, zero

[0x800042f0]:fsub.d t5, t3, s10, dyn
[0x800042f4]:csrrs a7, fcsr, zero
[0x800042f8]:sw t5, 1840(ra)
[0x800042fc]:sw t6, 1848(ra)
[0x80004300]:sw t5, 1856(ra)
[0x80004304]:sw a7, 1864(ra)
[0x80004308]:lui a4, 1
[0x8000430c]:addi a4, a4, 2048
[0x80004310]:add a6, a6, a4
[0x80004314]:lw t3, 928(a6)
[0x80004318]:sub a6, a6, a4
[0x8000431c]:lui a4, 1
[0x80004320]:addi a4, a4, 2048
[0x80004324]:add a6, a6, a4
[0x80004328]:lw t4, 932(a6)
[0x8000432c]:sub a6, a6, a4
[0x80004330]:lui a4, 1
[0x80004334]:addi a4, a4, 2048
[0x80004338]:add a6, a6, a4
[0x8000433c]:lw s10, 936(a6)
[0x80004340]:sub a6, a6, a4
[0x80004344]:lui a4, 1
[0x80004348]:addi a4, a4, 2048
[0x8000434c]:add a6, a6, a4
[0x80004350]:lw s11, 940(a6)
[0x80004354]:sub a6, a6, a4
[0x80004358]:addi t3, zero, 31
[0x8000435c]:lui t4, 256
[0x80004360]:lui s10, 2
[0x80004364]:addi s10, s10, 4065
[0x80004368]:lui s11, 524288
[0x8000436c]:addi a4, zero, 0
[0x80004370]:csrrw zero, fcsr, a4
[0x80004374]:fsub.d t5, t3, s10, dyn
[0x80004378]:csrrs a7, fcsr, zero

[0x80004374]:fsub.d t5, t3, s10, dyn
[0x80004378]:csrrs a7, fcsr, zero
[0x8000437c]:sw t5, 1872(ra)
[0x80004380]:sw t6, 1880(ra)
[0x80004384]:sw t5, 1888(ra)
[0x80004388]:sw a7, 1896(ra)
[0x8000438c]:lui a4, 1
[0x80004390]:addi a4, a4, 2048
[0x80004394]:add a6, a6, a4
[0x80004398]:lw t3, 944(a6)
[0x8000439c]:sub a6, a6, a4
[0x800043a0]:lui a4, 1
[0x800043a4]:addi a4, a4, 2048
[0x800043a8]:add a6, a6, a4
[0x800043ac]:lw t4, 948(a6)
[0x800043b0]:sub a6, a6, a4
[0x800043b4]:lui a4, 1
[0x800043b8]:addi a4, a4, 2048
[0x800043bc]:add a6, a6, a4
[0x800043c0]:lw s10, 952(a6)
[0x800043c4]:sub a6, a6, a4
[0x800043c8]:lui a4, 1
[0x800043cc]:addi a4, a4, 2048
[0x800043d0]:add a6, a6, a4
[0x800043d4]:lw s11, 956(a6)
[0x800043d8]:sub a6, a6, a4
[0x800043dc]:addi t3, zero, 54
[0x800043e0]:lui t4, 256
[0x800043e4]:lui s10, 4
[0x800043e8]:addi s10, s10, 4042
[0x800043ec]:lui s11, 524288
[0x800043f0]:addi a4, zero, 0
[0x800043f4]:csrrw zero, fcsr, a4
[0x800043f8]:fsub.d t5, t3, s10, dyn
[0x800043fc]:csrrs a7, fcsr, zero

[0x800043f8]:fsub.d t5, t3, s10, dyn
[0x800043fc]:csrrs a7, fcsr, zero
[0x80004400]:sw t5, 1904(ra)
[0x80004404]:sw t6, 1912(ra)
[0x80004408]:sw t5, 1920(ra)
[0x8000440c]:sw a7, 1928(ra)
[0x80004410]:lui a4, 1
[0x80004414]:addi a4, a4, 2048
[0x80004418]:add a6, a6, a4
[0x8000441c]:lw t3, 960(a6)
[0x80004420]:sub a6, a6, a4
[0x80004424]:lui a4, 1
[0x80004428]:addi a4, a4, 2048
[0x8000442c]:add a6, a6, a4
[0x80004430]:lw t4, 964(a6)
[0x80004434]:sub a6, a6, a4
[0x80004438]:lui a4, 1
[0x8000443c]:addi a4, a4, 2048
[0x80004440]:add a6, a6, a4
[0x80004444]:lw s10, 968(a6)
[0x80004448]:sub a6, a6, a4
[0x8000444c]:lui a4, 1
[0x80004450]:addi a4, a4, 2048
[0x80004454]:add a6, a6, a4
[0x80004458]:lw s11, 972(a6)
[0x8000445c]:sub a6, a6, a4
[0x80004460]:addi t3, zero, 15
[0x80004464]:lui t4, 256
[0x80004468]:lui s10, 8
[0x8000446c]:addi s10, s10, 4081
[0x80004470]:lui s11, 524288
[0x80004474]:addi a4, zero, 0
[0x80004478]:csrrw zero, fcsr, a4
[0x8000447c]:fsub.d t5, t3, s10, dyn
[0x80004480]:csrrs a7, fcsr, zero

[0x8000447c]:fsub.d t5, t3, s10, dyn
[0x80004480]:csrrs a7, fcsr, zero
[0x80004484]:sw t5, 1936(ra)
[0x80004488]:sw t6, 1944(ra)
[0x8000448c]:sw t5, 1952(ra)
[0x80004490]:sw a7, 1960(ra)
[0x80004494]:lui a4, 1
[0x80004498]:addi a4, a4, 2048
[0x8000449c]:add a6, a6, a4
[0x800044a0]:lw t3, 976(a6)
[0x800044a4]:sub a6, a6, a4
[0x800044a8]:lui a4, 1
[0x800044ac]:addi a4, a4, 2048
[0x800044b0]:add a6, a6, a4
[0x800044b4]:lw t4, 980(a6)
[0x800044b8]:sub a6, a6, a4
[0x800044bc]:lui a4, 1
[0x800044c0]:addi a4, a4, 2048
[0x800044c4]:add a6, a6, a4
[0x800044c8]:lw s10, 984(a6)
[0x800044cc]:sub a6, a6, a4
[0x800044d0]:lui a4, 1
[0x800044d4]:addi a4, a4, 2048
[0x800044d8]:add a6, a6, a4
[0x800044dc]:lw s11, 988(a6)
[0x800044e0]:sub a6, a6, a4
[0x800044e4]:addi t3, zero, 22
[0x800044e8]:lui t4, 256
[0x800044ec]:lui s10, 16
[0x800044f0]:addi s10, s10, 4074
[0x800044f4]:lui s11, 524288
[0x800044f8]:addi a4, zero, 0
[0x800044fc]:csrrw zero, fcsr, a4
[0x80004500]:fsub.d t5, t3, s10, dyn
[0x80004504]:csrrs a7, fcsr, zero

[0x80004500]:fsub.d t5, t3, s10, dyn
[0x80004504]:csrrs a7, fcsr, zero
[0x80004508]:sw t5, 1968(ra)
[0x8000450c]:sw t6, 1976(ra)
[0x80004510]:sw t5, 1984(ra)
[0x80004514]:sw a7, 1992(ra)
[0x80004518]:lui a4, 1
[0x8000451c]:addi a4, a4, 2048
[0x80004520]:add a6, a6, a4
[0x80004524]:lw t3, 992(a6)
[0x80004528]:sub a6, a6, a4
[0x8000452c]:lui a4, 1
[0x80004530]:addi a4, a4, 2048
[0x80004534]:add a6, a6, a4
[0x80004538]:lw t4, 996(a6)
[0x8000453c]:sub a6, a6, a4
[0x80004540]:lui a4, 1
[0x80004544]:addi a4, a4, 2048
[0x80004548]:add a6, a6, a4
[0x8000454c]:lw s10, 1000(a6)
[0x80004550]:sub a6, a6, a4
[0x80004554]:lui a4, 1
[0x80004558]:addi a4, a4, 2048
[0x8000455c]:add a6, a6, a4
[0x80004560]:lw s11, 1004(a6)
[0x80004564]:sub a6, a6, a4
[0x80004568]:addi t3, zero, 31
[0x8000456c]:lui t4, 256
[0x80004570]:lui s10, 32
[0x80004574]:addi s10, s10, 4065
[0x80004578]:lui s11, 524288
[0x8000457c]:addi a4, zero, 0
[0x80004580]:csrrw zero, fcsr, a4
[0x80004584]:fsub.d t5, t3, s10, dyn
[0x80004588]:csrrs a7, fcsr, zero

[0x80004584]:fsub.d t5, t3, s10, dyn
[0x80004588]:csrrs a7, fcsr, zero
[0x8000458c]:sw t5, 2000(ra)
[0x80004590]:sw t6, 2008(ra)
[0x80004594]:sw t5, 2016(ra)
[0x80004598]:sw a7, 2024(ra)
[0x8000459c]:lui a4, 1
[0x800045a0]:addi a4, a4, 2048
[0x800045a4]:add a6, a6, a4
[0x800045a8]:lw t3, 1008(a6)
[0x800045ac]:sub a6, a6, a4
[0x800045b0]:lui a4, 1
[0x800045b4]:addi a4, a4, 2048
[0x800045b8]:add a6, a6, a4
[0x800045bc]:lw t4, 1012(a6)
[0x800045c0]:sub a6, a6, a4
[0x800045c4]:lui a4, 1
[0x800045c8]:addi a4, a4, 2048
[0x800045cc]:add a6, a6, a4
[0x800045d0]:lw s10, 1016(a6)
[0x800045d4]:sub a6, a6, a4
[0x800045d8]:lui a4, 1
[0x800045dc]:addi a4, a4, 2048
[0x800045e0]:add a6, a6, a4
[0x800045e4]:lw s11, 1020(a6)
[0x800045e8]:sub a6, a6, a4
[0x800045ec]:addi t3, zero, 96
[0x800045f0]:lui t4, 256
[0x800045f4]:lui s10, 64
[0x800045f8]:addi s10, s10, 4000
[0x800045fc]:lui s11, 524288
[0x80004600]:addi a4, zero, 0
[0x80004604]:csrrw zero, fcsr, a4
[0x80004608]:fsub.d t5, t3, s10, dyn
[0x8000460c]:csrrs a7, fcsr, zero

[0x80004608]:fsub.d t5, t3, s10, dyn
[0x8000460c]:csrrs a7, fcsr, zero
[0x80004610]:sw t5, 2032(ra)
[0x80004614]:sw t6, 2040(ra)
[0x80004618]:addi ra, ra, 2040
[0x8000461c]:sw t5, 8(ra)
[0x80004620]:sw a7, 16(ra)
[0x80004624]:lui a4, 1
[0x80004628]:addi a4, a4, 2048
[0x8000462c]:add a6, a6, a4
[0x80004630]:lw t3, 1024(a6)
[0x80004634]:sub a6, a6, a4
[0x80004638]:lui a4, 1
[0x8000463c]:addi a4, a4, 2048
[0x80004640]:add a6, a6, a4
[0x80004644]:lw t4, 1028(a6)
[0x80004648]:sub a6, a6, a4
[0x8000464c]:lui a4, 1
[0x80004650]:addi a4, a4, 2048
[0x80004654]:add a6, a6, a4
[0x80004658]:lw s10, 1032(a6)
[0x8000465c]:sub a6, a6, a4
[0x80004660]:lui a4, 1
[0x80004664]:addi a4, a4, 2048
[0x80004668]:add a6, a6, a4
[0x8000466c]:lw s11, 1036(a6)
[0x80004670]:sub a6, a6, a4
[0x80004674]:addi t3, zero, 56
[0x80004678]:lui t4, 256
[0x8000467c]:lui s10, 128
[0x80004680]:addi s10, s10, 4040
[0x80004684]:lui s11, 524288
[0x80004688]:addi a4, zero, 0
[0x8000468c]:csrrw zero, fcsr, a4
[0x80004690]:fsub.d t5, t3, s10, dyn
[0x80004694]:csrrs a7, fcsr, zero

[0x80004690]:fsub.d t5, t3, s10, dyn
[0x80004694]:csrrs a7, fcsr, zero
[0x80004698]:sw t5, 24(ra)
[0x8000469c]:sw t6, 32(ra)
[0x800046a0]:sw t5, 40(ra)
[0x800046a4]:sw a7, 48(ra)
[0x800046a8]:lui a4, 1
[0x800046ac]:addi a4, a4, 2048
[0x800046b0]:add a6, a6, a4
[0x800046b4]:lw t3, 1040(a6)
[0x800046b8]:sub a6, a6, a4
[0x800046bc]:lui a4, 1
[0x800046c0]:addi a4, a4, 2048
[0x800046c4]:add a6, a6, a4
[0x800046c8]:lw t4, 1044(a6)
[0x800046cc]:sub a6, a6, a4
[0x800046d0]:lui a4, 1
[0x800046d4]:addi a4, a4, 2048
[0x800046d8]:add a6, a6, a4
[0x800046dc]:lw s10, 1048(a6)
[0x800046e0]:sub a6, a6, a4
[0x800046e4]:lui a4, 1
[0x800046e8]:addi a4, a4, 2048
[0x800046ec]:add a6, a6, a4
[0x800046f0]:lw s11, 1052(a6)
[0x800046f4]:sub a6, a6, a4
[0x800046f8]:addi t3, zero, 70
[0x800046fc]:lui t4, 256
[0x80004700]:lui s10, 256
[0x80004704]:addi s10, s10, 4026
[0x80004708]:lui s11, 524288
[0x8000470c]:addi a4, zero, 0
[0x80004710]:csrrw zero, fcsr, a4
[0x80004714]:fsub.d t5, t3, s10, dyn
[0x80004718]:csrrs a7, fcsr, zero

[0x80004714]:fsub.d t5, t3, s10, dyn
[0x80004718]:csrrs a7, fcsr, zero
[0x8000471c]:sw t5, 56(ra)
[0x80004720]:sw t6, 64(ra)
[0x80004724]:sw t5, 72(ra)
[0x80004728]:sw a7, 80(ra)
[0x8000472c]:lui a4, 1
[0x80004730]:addi a4, a4, 2048
[0x80004734]:add a6, a6, a4
[0x80004738]:lw t3, 1056(a6)
[0x8000473c]:sub a6, a6, a4
[0x80004740]:lui a4, 1
[0x80004744]:addi a4, a4, 2048
[0x80004748]:add a6, a6, a4
[0x8000474c]:lw t4, 1060(a6)
[0x80004750]:sub a6, a6, a4
[0x80004754]:lui a4, 1
[0x80004758]:addi a4, a4, 2048
[0x8000475c]:add a6, a6, a4
[0x80004760]:lw s10, 1064(a6)
[0x80004764]:sub a6, a6, a4
[0x80004768]:lui a4, 1
[0x8000476c]:addi a4, a4, 2048
[0x80004770]:add a6, a6, a4
[0x80004774]:lw s11, 1068(a6)
[0x80004778]:sub a6, a6, a4
[0x8000477c]:addi t3, zero, 71
[0x80004780]:lui t4, 256
[0x80004784]:lui s10, 512
[0x80004788]:addi s10, s10, 4025
[0x8000478c]:lui s11, 524288
[0x80004790]:addi a4, zero, 0
[0x80004794]:csrrw zero, fcsr, a4
[0x80004798]:fsub.d t5, t3, s10, dyn
[0x8000479c]:csrrs a7, fcsr, zero

[0x80004798]:fsub.d t5, t3, s10, dyn
[0x8000479c]:csrrs a7, fcsr, zero
[0x800047a0]:sw t5, 88(ra)
[0x800047a4]:sw t6, 96(ra)
[0x800047a8]:sw t5, 104(ra)
[0x800047ac]:sw a7, 112(ra)
[0x800047b0]:lui a4, 1
[0x800047b4]:addi a4, a4, 2048
[0x800047b8]:add a6, a6, a4
[0x800047bc]:lw t3, 1072(a6)
[0x800047c0]:sub a6, a6, a4
[0x800047c4]:lui a4, 1
[0x800047c8]:addi a4, a4, 2048
[0x800047cc]:add a6, a6, a4
[0x800047d0]:lw t4, 1076(a6)
[0x800047d4]:sub a6, a6, a4
[0x800047d8]:lui a4, 1
[0x800047dc]:addi a4, a4, 2048
[0x800047e0]:add a6, a6, a4
[0x800047e4]:lw s10, 1080(a6)
[0x800047e8]:sub a6, a6, a4
[0x800047ec]:lui a4, 1
[0x800047f0]:addi a4, a4, 2048
[0x800047f4]:add a6, a6, a4
[0x800047f8]:lw s11, 1084(a6)
[0x800047fc]:sub a6, a6, a4
[0x80004800]:addi t3, zero, 92
[0x80004804]:lui t4, 256
[0x80004808]:lui s10, 1024
[0x8000480c]:addi s10, s10, 4004
[0x80004810]:lui s11, 524288
[0x80004814]:addi a4, zero, 0
[0x80004818]:csrrw zero, fcsr, a4
[0x8000481c]:fsub.d t5, t3, s10, dyn
[0x80004820]:csrrs a7, fcsr, zero

[0x8000481c]:fsub.d t5, t3, s10, dyn
[0x80004820]:csrrs a7, fcsr, zero
[0x80004824]:sw t5, 120(ra)
[0x80004828]:sw t6, 128(ra)
[0x8000482c]:sw t5, 136(ra)
[0x80004830]:sw a7, 144(ra)
[0x80004834]:lui a4, 1
[0x80004838]:addi a4, a4, 2048
[0x8000483c]:add a6, a6, a4
[0x80004840]:lw t3, 1088(a6)
[0x80004844]:sub a6, a6, a4
[0x80004848]:lui a4, 1
[0x8000484c]:addi a4, a4, 2048
[0x80004850]:add a6, a6, a4
[0x80004854]:lw t4, 1092(a6)
[0x80004858]:sub a6, a6, a4
[0x8000485c]:lui a4, 1
[0x80004860]:addi a4, a4, 2048
[0x80004864]:add a6, a6, a4
[0x80004868]:lw s10, 1096(a6)
[0x8000486c]:sub a6, a6, a4
[0x80004870]:lui a4, 1
[0x80004874]:addi a4, a4, 2048
[0x80004878]:add a6, a6, a4
[0x8000487c]:lw s11, 1100(a6)
[0x80004880]:sub a6, a6, a4
[0x80004884]:addi t3, zero, 41
[0x80004888]:lui t4, 256
[0x8000488c]:addi s10, zero, 21
[0x80004890]:lui s11, 512
[0x80004894]:addi a4, zero, 0
[0x80004898]:csrrw zero, fcsr, a4
[0x8000489c]:fsub.d t5, t3, s10, dyn
[0x800048a0]:csrrs a7, fcsr, zero

[0x8000489c]:fsub.d t5, t3, s10, dyn
[0x800048a0]:csrrs a7, fcsr, zero
[0x800048a4]:sw t5, 152(ra)
[0x800048a8]:sw t6, 160(ra)
[0x800048ac]:sw t5, 168(ra)
[0x800048b0]:sw a7, 176(ra)
[0x800048b4]:lui a4, 1
[0x800048b8]:addi a4, a4, 2048
[0x800048bc]:add a6, a6, a4
[0x800048c0]:lw t3, 1104(a6)
[0x800048c4]:sub a6, a6, a4
[0x800048c8]:lui a4, 1
[0x800048cc]:addi a4, a4, 2048
[0x800048d0]:add a6, a6, a4
[0x800048d4]:lw t4, 1108(a6)
[0x800048d8]:sub a6, a6, a4
[0x800048dc]:lui a4, 1
[0x800048e0]:addi a4, a4, 2048
[0x800048e4]:add a6, a6, a4
[0x800048e8]:lw s10, 1112(a6)
[0x800048ec]:sub a6, a6, a4
[0x800048f0]:lui a4, 1
[0x800048f4]:addi a4, a4, 2048
[0x800048f8]:add a6, a6, a4
[0x800048fc]:lw s11, 1116(a6)
[0x80004900]:sub a6, a6, a4
[0x80004904]:addi t3, zero, 27
[0x80004908]:lui t4, 256
[0x8000490c]:addi s10, zero, 14
[0x80004910]:lui s11, 512
[0x80004914]:addi a4, zero, 0
[0x80004918]:csrrw zero, fcsr, a4
[0x8000491c]:fsub.d t5, t3, s10, dyn
[0x80004920]:csrrs a7, fcsr, zero

[0x8000491c]:fsub.d t5, t3, s10, dyn
[0x80004920]:csrrs a7, fcsr, zero
[0x80004924]:sw t5, 184(ra)
[0x80004928]:sw t6, 192(ra)
[0x8000492c]:sw t5, 200(ra)
[0x80004930]:sw a7, 208(ra)
[0x80004934]:lui a4, 1
[0x80004938]:addi a4, a4, 2048
[0x8000493c]:add a6, a6, a4
[0x80004940]:lw t3, 1120(a6)
[0x80004944]:sub a6, a6, a4
[0x80004948]:lui a4, 1
[0x8000494c]:addi a4, a4, 2048
[0x80004950]:add a6, a6, a4
[0x80004954]:lw t4, 1124(a6)
[0x80004958]:sub a6, a6, a4
[0x8000495c]:lui a4, 1
[0x80004960]:addi a4, a4, 2048
[0x80004964]:add a6, a6, a4
[0x80004968]:lw s10, 1128(a6)
[0x8000496c]:sub a6, a6, a4
[0x80004970]:lui a4, 1
[0x80004974]:addi a4, a4, 2048
[0x80004978]:add a6, a6, a4
[0x8000497c]:lw s11, 1132(a6)
[0x80004980]:sub a6, a6, a4
[0x80004984]:addi t3, zero, 41
[0x80004988]:lui t4, 256
[0x8000498c]:addi s10, zero, 22
[0x80004990]:lui s11, 512
[0x80004994]:addi a4, zero, 0
[0x80004998]:csrrw zero, fcsr, a4
[0x8000499c]:fsub.d t5, t3, s10, dyn
[0x800049a0]:csrrs a7, fcsr, zero

[0x8000499c]:fsub.d t5, t3, s10, dyn
[0x800049a0]:csrrs a7, fcsr, zero
[0x800049a4]:sw t5, 216(ra)
[0x800049a8]:sw t6, 224(ra)
[0x800049ac]:sw t5, 232(ra)
[0x800049b0]:sw a7, 240(ra)
[0x800049b4]:lui a4, 1
[0x800049b8]:addi a4, a4, 2048
[0x800049bc]:add a6, a6, a4
[0x800049c0]:lw t3, 1136(a6)
[0x800049c4]:sub a6, a6, a4
[0x800049c8]:lui a4, 1
[0x800049cc]:addi a4, a4, 2048
[0x800049d0]:add a6, a6, a4
[0x800049d4]:lw t4, 1140(a6)
[0x800049d8]:sub a6, a6, a4
[0x800049dc]:lui a4, 1
[0x800049e0]:addi a4, a4, 2048
[0x800049e4]:add a6, a6, a4
[0x800049e8]:lw s10, 1144(a6)
[0x800049ec]:sub a6, a6, a4
[0x800049f0]:lui a4, 1
[0x800049f4]:addi a4, a4, 2048
[0x800049f8]:add a6, a6, a4
[0x800049fc]:lw s11, 1148(a6)
[0x80004a00]:sub a6, a6, a4
[0x80004a04]:addi t3, zero, 4
[0x80004a08]:lui t4, 256
[0x80004a0c]:addi s10, zero, 6
[0x80004a10]:lui s11, 512
[0x80004a14]:addi a4, zero, 0
[0x80004a18]:csrrw zero, fcsr, a4
[0x80004a1c]:fsub.d t5, t3, s10, dyn
[0x80004a20]:csrrs a7, fcsr, zero

[0x80004a1c]:fsub.d t5, t3, s10, dyn
[0x80004a20]:csrrs a7, fcsr, zero
[0x80004a24]:sw t5, 248(ra)
[0x80004a28]:sw t6, 256(ra)
[0x80004a2c]:sw t5, 264(ra)
[0x80004a30]:sw a7, 272(ra)
[0x80004a34]:lui a4, 1
[0x80004a38]:addi a4, a4, 2048
[0x80004a3c]:add a6, a6, a4
[0x80004a40]:lw t3, 1152(a6)
[0x80004a44]:sub a6, a6, a4
[0x80004a48]:lui a4, 1
[0x80004a4c]:addi a4, a4, 2048
[0x80004a50]:add a6, a6, a4
[0x80004a54]:lw t4, 1156(a6)
[0x80004a58]:sub a6, a6, a4
[0x80004a5c]:lui a4, 1
[0x80004a60]:addi a4, a4, 2048
[0x80004a64]:add a6, a6, a4
[0x80004a68]:lw s10, 1160(a6)
[0x80004a6c]:sub a6, a6, a4
[0x80004a70]:lui a4, 1
[0x80004a74]:addi a4, a4, 2048
[0x80004a78]:add a6, a6, a4
[0x80004a7c]:lw s11, 1164(a6)
[0x80004a80]:sub a6, a6, a4
[0x80004a84]:addi t3, zero, 38
[0x80004a88]:lui t4, 256
[0x80004a8c]:addi s10, zero, 27
[0x80004a90]:lui s11, 512
[0x80004a94]:addi a4, zero, 0
[0x80004a98]:csrrw zero, fcsr, a4
[0x80004a9c]:fsub.d t5, t3, s10, dyn
[0x80004aa0]:csrrs a7, fcsr, zero

[0x80004a9c]:fsub.d t5, t3, s10, dyn
[0x80004aa0]:csrrs a7, fcsr, zero
[0x80004aa4]:sw t5, 280(ra)
[0x80004aa8]:sw t6, 288(ra)
[0x80004aac]:sw t5, 296(ra)
[0x80004ab0]:sw a7, 304(ra)
[0x80004ab4]:lui a4, 1
[0x80004ab8]:addi a4, a4, 2048
[0x80004abc]:add a6, a6, a4
[0x80004ac0]:lw t3, 1168(a6)
[0x80004ac4]:sub a6, a6, a4
[0x80004ac8]:lui a4, 1
[0x80004acc]:addi a4, a4, 2048
[0x80004ad0]:add a6, a6, a4
[0x80004ad4]:lw t4, 1172(a6)
[0x80004ad8]:sub a6, a6, a4
[0x80004adc]:lui a4, 1
[0x80004ae0]:addi a4, a4, 2048
[0x80004ae4]:add a6, a6, a4
[0x80004ae8]:lw s10, 1176(a6)
[0x80004aec]:sub a6, a6, a4
[0x80004af0]:lui a4, 1
[0x80004af4]:addi a4, a4, 2048
[0x80004af8]:add a6, a6, a4
[0x80004afc]:lw s11, 1180(a6)
[0x80004b00]:sub a6, a6, a4
[0x80004b04]:addi t3, zero, 77
[0x80004b08]:lui t4, 256
[0x80004b0c]:addi s10, zero, 54
[0x80004b10]:lui s11, 512
[0x80004b14]:addi a4, zero, 0
[0x80004b18]:csrrw zero, fcsr, a4
[0x80004b1c]:fsub.d t5, t3, s10, dyn
[0x80004b20]:csrrs a7, fcsr, zero

[0x80004b1c]:fsub.d t5, t3, s10, dyn
[0x80004b20]:csrrs a7, fcsr, zero
[0x80004b24]:sw t5, 312(ra)
[0x80004b28]:sw t6, 320(ra)
[0x80004b2c]:sw t5, 328(ra)
[0x80004b30]:sw a7, 336(ra)
[0x80004b34]:lui a4, 1
[0x80004b38]:addi a4, a4, 2048
[0x80004b3c]:add a6, a6, a4
[0x80004b40]:lw t3, 1184(a6)
[0x80004b44]:sub a6, a6, a4
[0x80004b48]:lui a4, 1
[0x80004b4c]:addi a4, a4, 2048
[0x80004b50]:add a6, a6, a4
[0x80004b54]:lw t4, 1188(a6)
[0x80004b58]:sub a6, a6, a4
[0x80004b5c]:lui a4, 1
[0x80004b60]:addi a4, a4, 2048
[0x80004b64]:add a6, a6, a4
[0x80004b68]:lw s10, 1192(a6)
[0x80004b6c]:sub a6, a6, a4
[0x80004b70]:lui a4, 1
[0x80004b74]:addi a4, a4, 2048
[0x80004b78]:add a6, a6, a4
[0x80004b7c]:lw s11, 1196(a6)
[0x80004b80]:sub a6, a6, a4
[0x80004b84]:addi t3, zero, 58
[0x80004b88]:lui t4, 256
[0x80004b8c]:addi s10, zero, 61
[0x80004b90]:lui s11, 512
[0x80004b94]:addi a4, zero, 0
[0x80004b98]:csrrw zero, fcsr, a4
[0x80004b9c]:fsub.d t5, t3, s10, dyn
[0x80004ba0]:csrrs a7, fcsr, zero

[0x80004b9c]:fsub.d t5, t3, s10, dyn
[0x80004ba0]:csrrs a7, fcsr, zero
[0x80004ba4]:sw t5, 344(ra)
[0x80004ba8]:sw t6, 352(ra)
[0x80004bac]:sw t5, 360(ra)
[0x80004bb0]:sw a7, 368(ra)
[0x80004bb4]:lui a4, 1
[0x80004bb8]:addi a4, a4, 2048
[0x80004bbc]:add a6, a6, a4
[0x80004bc0]:lw t3, 1200(a6)
[0x80004bc4]:sub a6, a6, a4
[0x80004bc8]:lui a4, 1
[0x80004bcc]:addi a4, a4, 2048
[0x80004bd0]:add a6, a6, a4
[0x80004bd4]:lw t4, 1204(a6)
[0x80004bd8]:sub a6, a6, a4
[0x80004bdc]:lui a4, 1
[0x80004be0]:addi a4, a4, 2048
[0x80004be4]:add a6, a6, a4
[0x80004be8]:lw s10, 1208(a6)
[0x80004bec]:sub a6, a6, a4
[0x80004bf0]:lui a4, 1
[0x80004bf4]:addi a4, a4, 2048
[0x80004bf8]:add a6, a6, a4
[0x80004bfc]:lw s11, 1212(a6)
[0x80004c00]:sub a6, a6, a4
[0x80004c04]:addi t3, zero, 41
[0x80004c08]:lui t4, 256
[0x80004c0c]:addi s10, zero, 84
[0x80004c10]:lui s11, 512
[0x80004c14]:addi a4, zero, 0
[0x80004c18]:csrrw zero, fcsr, a4
[0x80004c1c]:fsub.d t5, t3, s10, dyn
[0x80004c20]:csrrs a7, fcsr, zero

[0x80004c1c]:fsub.d t5, t3, s10, dyn
[0x80004c20]:csrrs a7, fcsr, zero
[0x80004c24]:sw t5, 376(ra)
[0x80004c28]:sw t6, 384(ra)
[0x80004c2c]:sw t5, 392(ra)
[0x80004c30]:sw a7, 400(ra)
[0x80004c34]:lui a4, 1
[0x80004c38]:addi a4, a4, 2048
[0x80004c3c]:add a6, a6, a4
[0x80004c40]:lw t3, 1216(a6)
[0x80004c44]:sub a6, a6, a4
[0x80004c48]:lui a4, 1
[0x80004c4c]:addi a4, a4, 2048
[0x80004c50]:add a6, a6, a4
[0x80004c54]:lw t4, 1220(a6)
[0x80004c58]:sub a6, a6, a4
[0x80004c5c]:lui a4, 1
[0x80004c60]:addi a4, a4, 2048
[0x80004c64]:add a6, a6, a4
[0x80004c68]:lw s10, 1224(a6)
[0x80004c6c]:sub a6, a6, a4
[0x80004c70]:lui a4, 1
[0x80004c74]:addi a4, a4, 2048
[0x80004c78]:add a6, a6, a4
[0x80004c7c]:lw s11, 1228(a6)
[0x80004c80]:sub a6, a6, a4
[0x80004c84]:addi t3, zero, 9
[0x80004c88]:lui t4, 256
[0x80004c8c]:addi s10, zero, 132
[0x80004c90]:lui s11, 512
[0x80004c94]:addi a4, zero, 0
[0x80004c98]:csrrw zero, fcsr, a4
[0x80004c9c]:fsub.d t5, t3, s10, dyn
[0x80004ca0]:csrrs a7, fcsr, zero

[0x80004c9c]:fsub.d t5, t3, s10, dyn
[0x80004ca0]:csrrs a7, fcsr, zero
[0x80004ca4]:sw t5, 408(ra)
[0x80004ca8]:sw t6, 416(ra)
[0x80004cac]:sw t5, 424(ra)
[0x80004cb0]:sw a7, 432(ra)
[0x80004cb4]:lui a4, 1
[0x80004cb8]:addi a4, a4, 2048
[0x80004cbc]:add a6, a6, a4
[0x80004cc0]:lw t3, 1232(a6)
[0x80004cc4]:sub a6, a6, a4
[0x80004cc8]:lui a4, 1
[0x80004ccc]:addi a4, a4, 2048
[0x80004cd0]:add a6, a6, a4
[0x80004cd4]:lw t4, 1236(a6)
[0x80004cd8]:sub a6, a6, a4
[0x80004cdc]:lui a4, 1
[0x80004ce0]:addi a4, a4, 2048
[0x80004ce4]:add a6, a6, a4
[0x80004ce8]:lw s10, 1240(a6)
[0x80004cec]:sub a6, a6, a4
[0x80004cf0]:lui a4, 1
[0x80004cf4]:addi a4, a4, 2048
[0x80004cf8]:add a6, a6, a4
[0x80004cfc]:lw s11, 1244(a6)
[0x80004d00]:sub a6, a6, a4
[0x80004d04]:addi t3, zero, 41
[0x80004d08]:lui t4, 256
[0x80004d0c]:addi s10, zero, 276
[0x80004d10]:lui s11, 512
[0x80004d14]:addi a4, zero, 0
[0x80004d18]:csrrw zero, fcsr, a4
[0x80004d1c]:fsub.d t5, t3, s10, dyn
[0x80004d20]:csrrs a7, fcsr, zero

[0x80004d1c]:fsub.d t5, t3, s10, dyn
[0x80004d20]:csrrs a7, fcsr, zero
[0x80004d24]:sw t5, 440(ra)
[0x80004d28]:sw t6, 448(ra)
[0x80004d2c]:sw t5, 456(ra)
[0x80004d30]:sw a7, 464(ra)
[0x80004d34]:lui a4, 1
[0x80004d38]:addi a4, a4, 2048
[0x80004d3c]:add a6, a6, a4
[0x80004d40]:lw t3, 1248(a6)
[0x80004d44]:sub a6, a6, a4
[0x80004d48]:lui a4, 1
[0x80004d4c]:addi a4, a4, 2048
[0x80004d50]:add a6, a6, a4
[0x80004d54]:lw t4, 1252(a6)
[0x80004d58]:sub a6, a6, a4
[0x80004d5c]:lui a4, 1
[0x80004d60]:addi a4, a4, 2048
[0x80004d64]:add a6, a6, a4
[0x80004d68]:lw s10, 1256(a6)
[0x80004d6c]:sub a6, a6, a4
[0x80004d70]:lui a4, 1
[0x80004d74]:addi a4, a4, 2048
[0x80004d78]:add a6, a6, a4
[0x80004d7c]:lw s11, 1260(a6)
[0x80004d80]:sub a6, a6, a4
[0x80004d84]:addi t3, zero, 59
[0x80004d88]:lui t4, 256
[0x80004d8c]:addi s10, zero, 542
[0x80004d90]:lui s11, 512
[0x80004d94]:addi a4, zero, 0
[0x80004d98]:csrrw zero, fcsr, a4
[0x80004d9c]:fsub.d t5, t3, s10, dyn
[0x80004da0]:csrrs a7, fcsr, zero

[0x80004d9c]:fsub.d t5, t3, s10, dyn
[0x80004da0]:csrrs a7, fcsr, zero
[0x80004da4]:sw t5, 472(ra)
[0x80004da8]:sw t6, 480(ra)
[0x80004dac]:sw t5, 488(ra)
[0x80004db0]:sw a7, 496(ra)
[0x80004db4]:lui a4, 1
[0x80004db8]:addi a4, a4, 2048
[0x80004dbc]:add a6, a6, a4
[0x80004dc0]:lw t3, 1264(a6)
[0x80004dc4]:sub a6, a6, a4
[0x80004dc8]:lui a4, 1
[0x80004dcc]:addi a4, a4, 2048
[0x80004dd0]:add a6, a6, a4
[0x80004dd4]:lw t4, 1268(a6)
[0x80004dd8]:sub a6, a6, a4
[0x80004ddc]:lui a4, 1
[0x80004de0]:addi a4, a4, 2048
[0x80004de4]:add a6, a6, a4
[0x80004de8]:lw s10, 1272(a6)
[0x80004dec]:sub a6, a6, a4
[0x80004df0]:lui a4, 1
[0x80004df4]:addi a4, a4, 2048
[0x80004df8]:add a6, a6, a4
[0x80004dfc]:lw s11, 1276(a6)
[0x80004e00]:sub a6, a6, a4
[0x80004e04]:addi t3, zero, 33
[0x80004e08]:lui t4, 256
[0x80004e0c]:addi s10, zero, 1040
[0x80004e10]:lui s11, 512
[0x80004e14]:addi a4, zero, 0
[0x80004e18]:csrrw zero, fcsr, a4
[0x80004e1c]:fsub.d t5, t3, s10, dyn
[0x80004e20]:csrrs a7, fcsr, zero

[0x80004e1c]:fsub.d t5, t3, s10, dyn
[0x80004e20]:csrrs a7, fcsr, zero
[0x80004e24]:sw t5, 504(ra)
[0x80004e28]:sw t6, 512(ra)
[0x80004e2c]:sw t5, 520(ra)
[0x80004e30]:sw a7, 528(ra)
[0x80004e34]:lui a4, 1
[0x80004e38]:addi a4, a4, 2048
[0x80004e3c]:add a6, a6, a4
[0x80004e40]:lw t3, 1280(a6)
[0x80004e44]:sub a6, a6, a4
[0x80004e48]:lui a4, 1
[0x80004e4c]:addi a4, a4, 2048
[0x80004e50]:add a6, a6, a4
[0x80004e54]:lw t4, 1284(a6)
[0x80004e58]:sub a6, a6, a4
[0x80004e5c]:lui a4, 1
[0x80004e60]:addi a4, a4, 2048
[0x80004e64]:add a6, a6, a4
[0x80004e68]:lw s10, 1288(a6)
[0x80004e6c]:sub a6, a6, a4
[0x80004e70]:lui a4, 1
[0x80004e74]:addi a4, a4, 2048
[0x80004e78]:add a6, a6, a4
[0x80004e7c]:lw s11, 1292(a6)
[0x80004e80]:sub a6, a6, a4
[0x80004e84]:addi t3, zero, 80
[0x80004e88]:lui t4, 256
[0x80004e8c]:lui s10, 1
[0x80004e90]:addi s10, s10, 2088
[0x80004e94]:lui s11, 512
[0x80004e98]:addi a4, zero, 0
[0x80004e9c]:csrrw zero, fcsr, a4
[0x80004ea0]:fsub.d t5, t3, s10, dyn
[0x80004ea4]:csrrs a7, fcsr, zero

[0x80004ea0]:fsub.d t5, t3, s10, dyn
[0x80004ea4]:csrrs a7, fcsr, zero
[0x80004ea8]:sw t5, 536(ra)
[0x80004eac]:sw t6, 544(ra)
[0x80004eb0]:sw t5, 552(ra)
[0x80004eb4]:sw a7, 560(ra)
[0x80004eb8]:lui a4, 1
[0x80004ebc]:addi a4, a4, 2048
[0x80004ec0]:add a6, a6, a4
[0x80004ec4]:lw t3, 1296(a6)
[0x80004ec8]:sub a6, a6, a4
[0x80004ecc]:lui a4, 1
[0x80004ed0]:addi a4, a4, 2048
[0x80004ed4]:add a6, a6, a4
[0x80004ed8]:lw t4, 1300(a6)
[0x80004edc]:sub a6, a6, a4
[0x80004ee0]:lui a4, 1
[0x80004ee4]:addi a4, a4, 2048
[0x80004ee8]:add a6, a6, a4
[0x80004eec]:lw s10, 1304(a6)
[0x80004ef0]:sub a6, a6, a4
[0x80004ef4]:lui a4, 1
[0x80004ef8]:addi a4, a4, 2048
[0x80004efc]:add a6, a6, a4
[0x80004f00]:lw s11, 1308(a6)
[0x80004f04]:sub a6, a6, a4
[0x80004f08]:addi t3, zero, 89
[0x80004f0c]:lui t4, 256
[0x80004f10]:lui s10, 1
[0x80004f14]:addi s10, s10, 44
[0x80004f18]:lui s11, 512
[0x80004f1c]:addi a4, zero, 0
[0x80004f20]:csrrw zero, fcsr, a4
[0x80004f24]:fsub.d t5, t3, s10, dyn
[0x80004f28]:csrrs a7, fcsr, zero

[0x80004f24]:fsub.d t5, t3, s10, dyn
[0x80004f28]:csrrs a7, fcsr, zero
[0x80004f2c]:sw t5, 568(ra)
[0x80004f30]:sw t6, 576(ra)
[0x80004f34]:sw t5, 584(ra)
[0x80004f38]:sw a7, 592(ra)
[0x80004f3c]:lui a4, 1
[0x80004f40]:addi a4, a4, 2048
[0x80004f44]:add a6, a6, a4
[0x80004f48]:lw t3, 1312(a6)
[0x80004f4c]:sub a6, a6, a4
[0x80004f50]:lui a4, 1
[0x80004f54]:addi a4, a4, 2048
[0x80004f58]:add a6, a6, a4
[0x80004f5c]:lw t4, 1316(a6)
[0x80004f60]:sub a6, a6, a4
[0x80004f64]:lui a4, 1
[0x80004f68]:addi a4, a4, 2048
[0x80004f6c]:add a6, a6, a4
[0x80004f70]:lw s10, 1320(a6)
[0x80004f74]:sub a6, a6, a4
[0x80004f78]:lui a4, 1
[0x80004f7c]:addi a4, a4, 2048
[0x80004f80]:add a6, a6, a4
[0x80004f84]:lw s11, 1324(a6)
[0x80004f88]:sub a6, a6, a4
[0x80004f8c]:addi t3, zero, 85
[0x80004f90]:lui t4, 256
[0x80004f94]:lui s10, 2
[0x80004f98]:addi s10, s10, 42
[0x80004f9c]:lui s11, 512
[0x80004fa0]:addi a4, zero, 0
[0x80004fa4]:csrrw zero, fcsr, a4
[0x80004fa8]:fsub.d t5, t3, s10, dyn
[0x80004fac]:csrrs a7, fcsr, zero

[0x80004fa8]:fsub.d t5, t3, s10, dyn
[0x80004fac]:csrrs a7, fcsr, zero
[0x80004fb0]:sw t5, 600(ra)
[0x80004fb4]:sw t6, 608(ra)
[0x80004fb8]:sw t5, 616(ra)
[0x80004fbc]:sw a7, 624(ra)
[0x80004fc0]:lui a4, 1
[0x80004fc4]:addi a4, a4, 2048
[0x80004fc8]:add a6, a6, a4
[0x80004fcc]:lw t3, 1328(a6)
[0x80004fd0]:sub a6, a6, a4
[0x80004fd4]:lui a4, 1
[0x80004fd8]:addi a4, a4, 2048
[0x80004fdc]:add a6, a6, a4
[0x80004fe0]:lw t4, 1332(a6)
[0x80004fe4]:sub a6, a6, a4
[0x80004fe8]:lui a4, 1
[0x80004fec]:addi a4, a4, 2048
[0x80004ff0]:add a6, a6, a4
[0x80004ff4]:lw s10, 1336(a6)
[0x80004ff8]:sub a6, a6, a4
[0x80004ffc]:lui a4, 1
[0x80005000]:addi a4, a4, 2048
[0x80005004]:add a6, a6, a4
[0x80005008]:lw s11, 1340(a6)
[0x8000500c]:sub a6, a6, a4
[0x80005010]:addi t3, zero, 34
[0x80005014]:lui t4, 256
[0x80005018]:lui s10, 4
[0x8000501c]:addi s10, s10, 17
[0x80005020]:lui s11, 512
[0x80005024]:addi a4, zero, 0
[0x80005028]:csrrw zero, fcsr, a4
[0x8000502c]:fsub.d t5, t3, s10, dyn
[0x80005030]:csrrs a7, fcsr, zero

[0x8000502c]:fsub.d t5, t3, s10, dyn
[0x80005030]:csrrs a7, fcsr, zero
[0x80005034]:sw t5, 632(ra)
[0x80005038]:sw t6, 640(ra)
[0x8000503c]:sw t5, 648(ra)
[0x80005040]:sw a7, 656(ra)
[0x80005044]:lui a4, 1
[0x80005048]:addi a4, a4, 2048
[0x8000504c]:add a6, a6, a4
[0x80005050]:lw t3, 1344(a6)
[0x80005054]:sub a6, a6, a4
[0x80005058]:lui a4, 1
[0x8000505c]:addi a4, a4, 2048
[0x80005060]:add a6, a6, a4
[0x80005064]:lw t4, 1348(a6)
[0x80005068]:sub a6, a6, a4
[0x8000506c]:lui a4, 1
[0x80005070]:addi a4, a4, 2048
[0x80005074]:add a6, a6, a4
[0x80005078]:lw s10, 1352(a6)
[0x8000507c]:sub a6, a6, a4
[0x80005080]:lui a4, 1
[0x80005084]:addi a4, a4, 2048
[0x80005088]:add a6, a6, a4
[0x8000508c]:lw s11, 1356(a6)
[0x80005090]:sub a6, a6, a4
[0x80005094]:addi t3, zero, 70
[0x80005098]:lui t4, 256
[0x8000509c]:lui s10, 8
[0x800050a0]:addi s10, s10, 35
[0x800050a4]:lui s11, 512
[0x800050a8]:addi a4, zero, 0
[0x800050ac]:csrrw zero, fcsr, a4
[0x800050b0]:fsub.d t5, t3, s10, dyn
[0x800050b4]:csrrs a7, fcsr, zero

[0x800050b0]:fsub.d t5, t3, s10, dyn
[0x800050b4]:csrrs a7, fcsr, zero
[0x800050b8]:sw t5, 664(ra)
[0x800050bc]:sw t6, 672(ra)
[0x800050c0]:sw t5, 680(ra)
[0x800050c4]:sw a7, 688(ra)
[0x800050c8]:lui a4, 1
[0x800050cc]:addi a4, a4, 2048
[0x800050d0]:add a6, a6, a4
[0x800050d4]:lw t3, 1360(a6)
[0x800050d8]:sub a6, a6, a4
[0x800050dc]:lui a4, 1
[0x800050e0]:addi a4, a4, 2048
[0x800050e4]:add a6, a6, a4
[0x800050e8]:lw t4, 1364(a6)
[0x800050ec]:sub a6, a6, a4
[0x800050f0]:lui a4, 1
[0x800050f4]:addi a4, a4, 2048
[0x800050f8]:add a6, a6, a4
[0x800050fc]:lw s10, 1368(a6)
[0x80005100]:sub a6, a6, a4
[0x80005104]:lui a4, 1
[0x80005108]:addi a4, a4, 2048
[0x8000510c]:add a6, a6, a4
[0x80005110]:lw s11, 1372(a6)
[0x80005114]:sub a6, a6, a4
[0x80005118]:addi t3, zero, 40
[0x8000511c]:lui t4, 256
[0x80005120]:lui s10, 16
[0x80005124]:addi s10, s10, 20
[0x80005128]:lui s11, 512
[0x8000512c]:addi a4, zero, 0
[0x80005130]:csrrw zero, fcsr, a4
[0x80005134]:fsub.d t5, t3, s10, dyn
[0x80005138]:csrrs a7, fcsr, zero

[0x80005134]:fsub.d t5, t3, s10, dyn
[0x80005138]:csrrs a7, fcsr, zero
[0x8000513c]:sw t5, 696(ra)
[0x80005140]:sw t6, 704(ra)
[0x80005144]:sw t5, 712(ra)
[0x80005148]:sw a7, 720(ra)
[0x8000514c]:lui a4, 1
[0x80005150]:addi a4, a4, 2048
[0x80005154]:add a6, a6, a4
[0x80005158]:lw t3, 1376(a6)
[0x8000515c]:sub a6, a6, a4
[0x80005160]:lui a4, 1
[0x80005164]:addi a4, a4, 2048
[0x80005168]:add a6, a6, a4
[0x8000516c]:lw t4, 1380(a6)
[0x80005170]:sub a6, a6, a4
[0x80005174]:lui a4, 1
[0x80005178]:addi a4, a4, 2048
[0x8000517c]:add a6, a6, a4
[0x80005180]:lw s10, 1384(a6)
[0x80005184]:sub a6, a6, a4
[0x80005188]:lui a4, 1
[0x8000518c]:addi a4, a4, 2048
[0x80005190]:add a6, a6, a4
[0x80005194]:lw s11, 1388(a6)
[0x80005198]:sub a6, a6, a4
[0x8000519c]:addi t3, zero, 32
[0x800051a0]:lui t4, 256
[0x800051a4]:lui s10, 32
[0x800051a8]:addi s10, s10, 16
[0x800051ac]:lui s11, 512
[0x800051b0]:addi a4, zero, 0
[0x800051b4]:csrrw zero, fcsr, a4
[0x800051b8]:fsub.d t5, t3, s10, dyn
[0x800051bc]:csrrs a7, fcsr, zero

[0x800051b8]:fsub.d t5, t3, s10, dyn
[0x800051bc]:csrrs a7, fcsr, zero
[0x800051c0]:sw t5, 728(ra)
[0x800051c4]:sw t6, 736(ra)
[0x800051c8]:sw t5, 744(ra)
[0x800051cc]:sw a7, 752(ra)
[0x800051d0]:lui a4, 1
[0x800051d4]:addi a4, a4, 2048
[0x800051d8]:add a6, a6, a4
[0x800051dc]:lw t3, 1392(a6)
[0x800051e0]:sub a6, a6, a4
[0x800051e4]:lui a4, 1
[0x800051e8]:addi a4, a4, 2048
[0x800051ec]:add a6, a6, a4
[0x800051f0]:lw t4, 1396(a6)
[0x800051f4]:sub a6, a6, a4
[0x800051f8]:lui a4, 1
[0x800051fc]:addi a4, a4, 2048
[0x80005200]:add a6, a6, a4
[0x80005204]:lw s10, 1400(a6)
[0x80005208]:sub a6, a6, a4
[0x8000520c]:lui a4, 1
[0x80005210]:addi a4, a4, 2048
[0x80005214]:add a6, a6, a4
[0x80005218]:lw s11, 1404(a6)
[0x8000521c]:sub a6, a6, a4
[0x80005220]:addi t3, zero, 11
[0x80005224]:lui t4, 256
[0x80005228]:lui s10, 64
[0x8000522c]:addi s10, s10, 6
[0x80005230]:lui s11, 512
[0x80005234]:addi a4, zero, 0
[0x80005238]:csrrw zero, fcsr, a4
[0x8000523c]:fsub.d t5, t3, s10, dyn
[0x80005240]:csrrs a7, fcsr, zero

[0x8000523c]:fsub.d t5, t3, s10, dyn
[0x80005240]:csrrs a7, fcsr, zero
[0x80005244]:sw t5, 760(ra)
[0x80005248]:sw t6, 768(ra)
[0x8000524c]:sw t5, 776(ra)
[0x80005250]:sw a7, 784(ra)
[0x80005254]:lui a4, 1
[0x80005258]:addi a4, a4, 2048
[0x8000525c]:add a6, a6, a4
[0x80005260]:lw t3, 1408(a6)
[0x80005264]:sub a6, a6, a4
[0x80005268]:lui a4, 1
[0x8000526c]:addi a4, a4, 2048
[0x80005270]:add a6, a6, a4
[0x80005274]:lw t4, 1412(a6)
[0x80005278]:sub a6, a6, a4
[0x8000527c]:lui a4, 1
[0x80005280]:addi a4, a4, 2048
[0x80005284]:add a6, a6, a4
[0x80005288]:lw s10, 1416(a6)
[0x8000528c]:sub a6, a6, a4
[0x80005290]:lui a4, 1
[0x80005294]:addi a4, a4, 2048
[0x80005298]:add a6, a6, a4
[0x8000529c]:lw s11, 1420(a6)
[0x800052a0]:sub a6, a6, a4
[0x800052a4]:addi t3, zero, 12
[0x800052a8]:lui t4, 256
[0x800052ac]:lui s10, 128
[0x800052b0]:addi s10, s10, 6
[0x800052b4]:lui s11, 512
[0x800052b8]:addi a4, zero, 0
[0x800052bc]:csrrw zero, fcsr, a4
[0x800052c0]:fsub.d t5, t3, s10, dyn
[0x800052c4]:csrrs a7, fcsr, zero

[0x800052c0]:fsub.d t5, t3, s10, dyn
[0x800052c4]:csrrs a7, fcsr, zero
[0x800052c8]:sw t5, 792(ra)
[0x800052cc]:sw t6, 800(ra)
[0x800052d0]:sw t5, 808(ra)
[0x800052d4]:sw a7, 816(ra)
[0x800052d8]:lui a4, 1
[0x800052dc]:addi a4, a4, 2048
[0x800052e0]:add a6, a6, a4
[0x800052e4]:lw t3, 1424(a6)
[0x800052e8]:sub a6, a6, a4
[0x800052ec]:lui a4, 1
[0x800052f0]:addi a4, a4, 2048
[0x800052f4]:add a6, a6, a4
[0x800052f8]:lw t4, 1428(a6)
[0x800052fc]:sub a6, a6, a4
[0x80005300]:lui a4, 1
[0x80005304]:addi a4, a4, 2048
[0x80005308]:add a6, a6, a4
[0x8000530c]:lw s10, 1432(a6)
[0x80005310]:sub a6, a6, a4
[0x80005314]:lui a4, 1
[0x80005318]:addi a4, a4, 2048
[0x8000531c]:add a6, a6, a4
[0x80005320]:lw s11, 1436(a6)
[0x80005324]:sub a6, a6, a4
[0x80005328]:addi t3, zero, 58
[0x8000532c]:lui t4, 256
[0x80005330]:lui s10, 256
[0x80005334]:addi s10, s10, 29
[0x80005338]:lui s11, 512
[0x8000533c]:addi a4, zero, 0
[0x80005340]:csrrw zero, fcsr, a4
[0x80005344]:fsub.d t5, t3, s10, dyn
[0x80005348]:csrrs a7, fcsr, zero

[0x80005344]:fsub.d t5, t3, s10, dyn
[0x80005348]:csrrs a7, fcsr, zero
[0x8000534c]:sw t5, 824(ra)
[0x80005350]:sw t6, 832(ra)
[0x80005354]:sw t5, 840(ra)
[0x80005358]:sw a7, 848(ra)
[0x8000535c]:lui a4, 1
[0x80005360]:addi a4, a4, 2048
[0x80005364]:add a6, a6, a4
[0x80005368]:lw t3, 1440(a6)
[0x8000536c]:sub a6, a6, a4
[0x80005370]:lui a4, 1
[0x80005374]:addi a4, a4, 2048
[0x80005378]:add a6, a6, a4
[0x8000537c]:lw t4, 1444(a6)
[0x80005380]:sub a6, a6, a4
[0x80005384]:lui a4, 1
[0x80005388]:addi a4, a4, 2048
[0x8000538c]:add a6, a6, a4
[0x80005390]:lw s10, 1448(a6)
[0x80005394]:sub a6, a6, a4
[0x80005398]:lui a4, 1
[0x8000539c]:addi a4, a4, 2048
[0x800053a0]:add a6, a6, a4
[0x800053a4]:lw s11, 1452(a6)
[0x800053a8]:sub a6, a6, a4
[0x800053ac]:addi t3, zero, 84
[0x800053b0]:lui t4, 256
[0x800053b4]:lui s10, 512
[0x800053b8]:addi s10, s10, 42
[0x800053bc]:lui s11, 512
[0x800053c0]:addi a4, zero, 0
[0x800053c4]:csrrw zero, fcsr, a4
[0x800053c8]:fsub.d t5, t3, s10, dyn
[0x800053cc]:csrrs a7, fcsr, zero

[0x800053c8]:fsub.d t5, t3, s10, dyn
[0x800053cc]:csrrs a7, fcsr, zero
[0x800053d0]:sw t5, 856(ra)
[0x800053d4]:sw t6, 864(ra)
[0x800053d8]:sw t5, 872(ra)
[0x800053dc]:sw a7, 880(ra)
[0x800053e0]:lui a4, 1
[0x800053e4]:addi a4, a4, 2048
[0x800053e8]:add a6, a6, a4
[0x800053ec]:lw t3, 1456(a6)
[0x800053f0]:sub a6, a6, a4
[0x800053f4]:lui a4, 1
[0x800053f8]:addi a4, a4, 2048
[0x800053fc]:add a6, a6, a4
[0x80005400]:lw t4, 1460(a6)
[0x80005404]:sub a6, a6, a4
[0x80005408]:lui a4, 1
[0x8000540c]:addi a4, a4, 2048
[0x80005410]:add a6, a6, a4
[0x80005414]:lw s10, 1464(a6)
[0x80005418]:sub a6, a6, a4
[0x8000541c]:lui a4, 1
[0x80005420]:addi a4, a4, 2048
[0x80005424]:add a6, a6, a4
[0x80005428]:lw s11, 1468(a6)
[0x8000542c]:sub a6, a6, a4
[0x80005430]:addi t3, zero, 83
[0x80005434]:lui t4, 523776
[0x80005438]:addi s10, zero, 3926
[0x8000543c]:lui s11, 1048064
[0x80005440]:addi s11, s11, 4095
[0x80005444]:addi a4, zero, 0
[0x80005448]:csrrw zero, fcsr, a4
[0x8000544c]:fsub.d t5, t3, s10, dyn
[0x80005450]:csrrs a7, fcsr, zero

[0x8000544c]:fsub.d t5, t3, s10, dyn
[0x80005450]:csrrs a7, fcsr, zero
[0x80005454]:sw t5, 888(ra)
[0x80005458]:sw t6, 896(ra)
[0x8000545c]:sw t5, 904(ra)
[0x80005460]:sw a7, 912(ra)
[0x80005464]:lui a4, 1
[0x80005468]:addi a4, a4, 2048
[0x8000546c]:add a6, a6, a4
[0x80005470]:lw t3, 1472(a6)
[0x80005474]:sub a6, a6, a4
[0x80005478]:lui a4, 1
[0x8000547c]:addi a4, a4, 2048
[0x80005480]:add a6, a6, a4
[0x80005484]:lw t4, 1476(a6)
[0x80005488]:sub a6, a6, a4
[0x8000548c]:lui a4, 1
[0x80005490]:addi a4, a4, 2048
[0x80005494]:add a6, a6, a4
[0x80005498]:lw s10, 1480(a6)
[0x8000549c]:sub a6, a6, a4
[0x800054a0]:lui a4, 1
[0x800054a4]:addi a4, a4, 2048
[0x800054a8]:add a6, a6, a4
[0x800054ac]:lw s11, 1484(a6)
[0x800054b0]:sub a6, a6, a4
[0x800054b4]:addi t3, zero, 30
[0x800054b8]:lui t4, 523776
[0x800054bc]:addi s10, zero, 4030
[0x800054c0]:lui s11, 1048064
[0x800054c4]:addi s11, s11, 4095
[0x800054c8]:addi a4, zero, 0
[0x800054cc]:csrrw zero, fcsr, a4
[0x800054d0]:fsub.d t5, t3, s10, dyn
[0x800054d4]:csrrs a7, fcsr, zero

[0x800054d0]:fsub.d t5, t3, s10, dyn
[0x800054d4]:csrrs a7, fcsr, zero
[0x800054d8]:sw t5, 920(ra)
[0x800054dc]:sw t6, 928(ra)
[0x800054e0]:sw t5, 936(ra)
[0x800054e4]:sw a7, 944(ra)
[0x800054e8]:lui a4, 1
[0x800054ec]:addi a4, a4, 2048
[0x800054f0]:add a6, a6, a4
[0x800054f4]:lw t3, 1488(a6)
[0x800054f8]:sub a6, a6, a4
[0x800054fc]:lui a4, 1
[0x80005500]:addi a4, a4, 2048
[0x80005504]:add a6, a6, a4
[0x80005508]:lw t4, 1492(a6)
[0x8000550c]:sub a6, a6, a4
[0x80005510]:lui a4, 1
[0x80005514]:addi a4, a4, 2048
[0x80005518]:add a6, a6, a4
[0x8000551c]:lw s10, 1496(a6)
[0x80005520]:sub a6, a6, a4
[0x80005524]:lui a4, 1
[0x80005528]:addi a4, a4, 2048
[0x8000552c]:add a6, a6, a4
[0x80005530]:lw s11, 1500(a6)
[0x80005534]:sub a6, a6, a4
[0x80005538]:addi t3, zero, 40
[0x8000553c]:lui t4, 523776
[0x80005540]:addi s10, zero, 4006
[0x80005544]:lui s11, 1048064
[0x80005548]:addi s11, s11, 4095
[0x8000554c]:addi a4, zero, 0
[0x80005550]:csrrw zero, fcsr, a4
[0x80005554]:fsub.d t5, t3, s10, dyn
[0x80005558]:csrrs a7, fcsr, zero

[0x80005554]:fsub.d t5, t3, s10, dyn
[0x80005558]:csrrs a7, fcsr, zero
[0x8000555c]:sw t5, 952(ra)
[0x80005560]:sw t6, 960(ra)
[0x80005564]:sw t5, 968(ra)
[0x80005568]:sw a7, 976(ra)
[0x8000556c]:lui a4, 1
[0x80005570]:addi a4, a4, 2048
[0x80005574]:add a6, a6, a4
[0x80005578]:lw t3, 1504(a6)
[0x8000557c]:sub a6, a6, a4
[0x80005580]:lui a4, 1
[0x80005584]:addi a4, a4, 2048
[0x80005588]:add a6, a6, a4
[0x8000558c]:lw t4, 1508(a6)
[0x80005590]:sub a6, a6, a4
[0x80005594]:lui a4, 1
[0x80005598]:addi a4, a4, 2048
[0x8000559c]:add a6, a6, a4
[0x800055a0]:lw s10, 1512(a6)
[0x800055a4]:sub a6, a6, a4
[0x800055a8]:lui a4, 1
[0x800055ac]:addi a4, a4, 2048
[0x800055b0]:add a6, a6, a4
[0x800055b4]:lw s11, 1516(a6)
[0x800055b8]:sub a6, a6, a4
[0x800055bc]:addi t3, zero, 42
[0x800055c0]:lui t4, 523776
[0x800055c4]:addi s10, zero, 3994
[0x800055c8]:lui s11, 1048064
[0x800055cc]:addi s11, s11, 4095
[0x800055d0]:addi a4, zero, 0
[0x800055d4]:csrrw zero, fcsr, a4
[0x800055d8]:fsub.d t5, t3, s10, dyn
[0x800055dc]:csrrs a7, fcsr, zero

[0x800055d8]:fsub.d t5, t3, s10, dyn
[0x800055dc]:csrrs a7, fcsr, zero
[0x800055e0]:sw t5, 984(ra)
[0x800055e4]:sw t6, 992(ra)
[0x800055e8]:sw t5, 1000(ra)
[0x800055ec]:sw a7, 1008(ra)
[0x800055f0]:lui a4, 1
[0x800055f4]:addi a4, a4, 2048
[0x800055f8]:add a6, a6, a4
[0x800055fc]:lw t3, 1520(a6)
[0x80005600]:sub a6, a6, a4
[0x80005604]:lui a4, 1
[0x80005608]:addi a4, a4, 2048
[0x8000560c]:add a6, a6, a4
[0x80005610]:lw t4, 1524(a6)
[0x80005614]:sub a6, a6, a4
[0x80005618]:lui a4, 1
[0x8000561c]:addi a4, a4, 2048
[0x80005620]:add a6, a6, a4
[0x80005624]:lw s10, 1528(a6)
[0x80005628]:sub a6, a6, a4
[0x8000562c]:lui a4, 1
[0x80005630]:addi a4, a4, 2048
[0x80005634]:add a6, a6, a4
[0x80005638]:lw s11, 1532(a6)
[0x8000563c]:sub a6, a6, a4
[0x80005640]:addi t3, zero, 41
[0x80005644]:lui t4, 523776
[0x80005648]:addi s10, zero, 3980
[0x8000564c]:lui s11, 1048064
[0x80005650]:addi s11, s11, 4095
[0x80005654]:addi a4, zero, 0
[0x80005658]:csrrw zero, fcsr, a4
[0x8000565c]:fsub.d t5, t3, s10, dyn
[0x80005660]:csrrs a7, fcsr, zero

[0x8000565c]:fsub.d t5, t3, s10, dyn
[0x80005660]:csrrs a7, fcsr, zero
[0x80005664]:sw t5, 1016(ra)
[0x80005668]:sw t6, 1024(ra)
[0x8000566c]:sw t5, 1032(ra)
[0x80005670]:sw a7, 1040(ra)
[0x80005674]:lui a4, 1
[0x80005678]:addi a4, a4, 2048
[0x8000567c]:add a6, a6, a4
[0x80005680]:lw t3, 1536(a6)
[0x80005684]:sub a6, a6, a4
[0x80005688]:lui a4, 1
[0x8000568c]:addi a4, a4, 2048
[0x80005690]:add a6, a6, a4
[0x80005694]:lw t4, 1540(a6)
[0x80005698]:sub a6, a6, a4
[0x8000569c]:lui a4, 1
[0x800056a0]:addi a4, a4, 2048
[0x800056a4]:add a6, a6, a4
[0x800056a8]:lw s10, 1544(a6)
[0x800056ac]:sub a6, a6, a4
[0x800056b0]:lui a4, 1
[0x800056b4]:addi a4, a4, 2048
[0x800056b8]:add a6, a6, a4
[0x800056bc]:lw s11, 1548(a6)
[0x800056c0]:sub a6, a6, a4
[0x800056c4]:addi t3, zero, 39
[0x800056c8]:lui t4, 523776
[0x800056cc]:addi s10, zero, 3952
[0x800056d0]:lui s11, 1048064
[0x800056d4]:addi s11, s11, 4095
[0x800056d8]:addi a4, zero, 0
[0x800056dc]:csrrw zero, fcsr, a4
[0x800056e0]:fsub.d t5, t3, s10, dyn
[0x800056e4]:csrrs a7, fcsr, zero

[0x800056e0]:fsub.d t5, t3, s10, dyn
[0x800056e4]:csrrs a7, fcsr, zero
[0x800056e8]:sw t5, 1048(ra)
[0x800056ec]:sw t6, 1056(ra)
[0x800056f0]:sw t5, 1064(ra)
[0x800056f4]:sw a7, 1072(ra)
[0x800056f8]:lui a4, 1
[0x800056fc]:addi a4, a4, 2048
[0x80005700]:add a6, a6, a4
[0x80005704]:lw t3, 1552(a6)
[0x80005708]:sub a6, a6, a4
[0x8000570c]:lui a4, 1
[0x80005710]:addi a4, a4, 2048
[0x80005714]:add a6, a6, a4
[0x80005718]:lw t4, 1556(a6)
[0x8000571c]:sub a6, a6, a4
[0x80005720]:lui a4, 1
[0x80005724]:addi a4, a4, 2048
[0x80005728]:add a6, a6, a4
[0x8000572c]:lw s10, 1560(a6)
[0x80005730]:sub a6, a6, a4
[0x80005734]:lui a4, 1
[0x80005738]:addi a4, a4, 2048
[0x8000573c]:add a6, a6, a4
[0x80005740]:lw s11, 1564(a6)
[0x80005744]:sub a6, a6, a4
[0x80005748]:addi t3, zero, 43
[0x8000574c]:lui t4, 523776
[0x80005750]:addi s10, zero, 3880
[0x80005754]:lui s11, 1048064
[0x80005758]:addi s11, s11, 4095
[0x8000575c]:addi a4, zero, 0
[0x80005760]:csrrw zero, fcsr, a4
[0x80005764]:fsub.d t5, t3, s10, dyn
[0x80005768]:csrrs a7, fcsr, zero

[0x80005764]:fsub.d t5, t3, s10, dyn
[0x80005768]:csrrs a7, fcsr, zero
[0x8000576c]:sw t5, 1080(ra)
[0x80005770]:sw t6, 1088(ra)
[0x80005774]:sw t5, 1096(ra)
[0x80005778]:sw a7, 1104(ra)
[0x8000577c]:lui a4, 1
[0x80005780]:addi a4, a4, 2048
[0x80005784]:add a6, a6, a4
[0x80005788]:lw t3, 1568(a6)
[0x8000578c]:sub a6, a6, a4
[0x80005790]:lui a4, 1
[0x80005794]:addi a4, a4, 2048
[0x80005798]:add a6, a6, a4
[0x8000579c]:lw t4, 1572(a6)
[0x800057a0]:sub a6, a6, a4
[0x800057a4]:lui a4, 1
[0x800057a8]:addi a4, a4, 2048
[0x800057ac]:add a6, a6, a4
[0x800057b0]:lw s10, 1576(a6)
[0x800057b4]:sub a6, a6, a4
[0x800057b8]:lui a4, 1
[0x800057bc]:addi a4, a4, 2048
[0x800057c0]:add a6, a6, a4
[0x800057c4]:lw s11, 1580(a6)
[0x800057c8]:sub a6, a6, a4
[0x800057cc]:addi t3, zero, 70
[0x800057d0]:lui t4, 523776
[0x800057d4]:addi s10, zero, 3698
[0x800057d8]:lui s11, 1048064
[0x800057dc]:addi s11, s11, 4095
[0x800057e0]:addi a4, zero, 0
[0x800057e4]:csrrw zero, fcsr, a4
[0x800057e8]:fsub.d t5, t3, s10, dyn
[0x800057ec]:csrrs a7, fcsr, zero

[0x800057e8]:fsub.d t5, t3, s10, dyn
[0x800057ec]:csrrs a7, fcsr, zero
[0x800057f0]:sw t5, 1112(ra)
[0x800057f4]:sw t6, 1120(ra)
[0x800057f8]:sw t5, 1128(ra)
[0x800057fc]:sw a7, 1136(ra)
[0x80005800]:lui a4, 1
[0x80005804]:addi a4, a4, 2048
[0x80005808]:add a6, a6, a4
[0x8000580c]:lw t3, 1584(a6)
[0x80005810]:sub a6, a6, a4
[0x80005814]:lui a4, 1
[0x80005818]:addi a4, a4, 2048
[0x8000581c]:add a6, a6, a4
[0x80005820]:lw t4, 1588(a6)
[0x80005824]:sub a6, a6, a4
[0x80005828]:lui a4, 1
[0x8000582c]:addi a4, a4, 2048
[0x80005830]:add a6, a6, a4
[0x80005834]:lw s10, 1592(a6)
[0x80005838]:sub a6, a6, a4
[0x8000583c]:lui a4, 1
[0x80005840]:addi a4, a4, 2048
[0x80005844]:add a6, a6, a4
[0x80005848]:lw s11, 1596(a6)
[0x8000584c]:sub a6, a6, a4
[0x80005850]:addi t3, zero, 75
[0x80005854]:lui t4, 523776
[0x80005858]:addi s10, zero, 3432
[0x8000585c]:lui s11, 1048064
[0x80005860]:addi s11, s11, 4095
[0x80005864]:addi a4, zero, 0
[0x80005868]:csrrw zero, fcsr, a4
[0x8000586c]:fsub.d t5, t3, s10, dyn
[0x80005870]:csrrs a7, fcsr, zero

[0x8000586c]:fsub.d t5, t3, s10, dyn
[0x80005870]:csrrs a7, fcsr, zero
[0x80005874]:sw t5, 1144(ra)
[0x80005878]:sw t6, 1152(ra)
[0x8000587c]:sw t5, 1160(ra)
[0x80005880]:sw a7, 1168(ra)
[0x80005884]:lui a4, 1
[0x80005888]:addi a4, a4, 2048
[0x8000588c]:add a6, a6, a4
[0x80005890]:lw t3, 1600(a6)
[0x80005894]:sub a6, a6, a4
[0x80005898]:lui a4, 1
[0x8000589c]:addi a4, a4, 2048
[0x800058a0]:add a6, a6, a4
[0x800058a4]:lw t4, 1604(a6)
[0x800058a8]:sub a6, a6, a4
[0x800058ac]:lui a4, 1
[0x800058b0]:addi a4, a4, 2048
[0x800058b4]:add a6, a6, a4
[0x800058b8]:lw s10, 1608(a6)
[0x800058bc]:sub a6, a6, a4
[0x800058c0]:lui a4, 1
[0x800058c4]:addi a4, a4, 2048
[0x800058c8]:add a6, a6, a4
[0x800058cc]:lw s11, 1612(a6)
[0x800058d0]:sub a6, a6, a4
[0x800058d4]:addi t3, zero, 12
[0x800058d8]:lui t4, 523776
[0x800058dc]:addi s10, zero, 3046
[0x800058e0]:lui s11, 1048064
[0x800058e4]:addi s11, s11, 4095
[0x800058e8]:addi a4, zero, 0
[0x800058ec]:csrrw zero, fcsr, a4
[0x800058f0]:fsub.d t5, t3, s10, dyn
[0x800058f4]:csrrs a7, fcsr, zero

[0x800058f0]:fsub.d t5, t3, s10, dyn
[0x800058f4]:csrrs a7, fcsr, zero
[0x800058f8]:sw t5, 1176(ra)
[0x800058fc]:sw t6, 1184(ra)
[0x80005900]:sw t5, 1192(ra)
[0x80005904]:sw a7, 1200(ra)
[0x80005908]:lui a4, 1
[0x8000590c]:addi a4, a4, 2048
[0x80005910]:add a6, a6, a4
[0x80005914]:lw t3, 1616(a6)
[0x80005918]:sub a6, a6, a4
[0x8000591c]:lui a4, 1
[0x80005920]:addi a4, a4, 2048
[0x80005924]:add a6, a6, a4
[0x80005928]:lw t4, 1620(a6)
[0x8000592c]:sub a6, a6, a4
[0x80005930]:lui a4, 1
[0x80005934]:addi a4, a4, 2048
[0x80005938]:add a6, a6, a4
[0x8000593c]:lw s10, 1624(a6)
[0x80005940]:sub a6, a6, a4
[0x80005944]:lui a4, 1
[0x80005948]:addi a4, a4, 2048
[0x8000594c]:add a6, a6, a4
[0x80005950]:lw s11, 1628(a6)
[0x80005954]:sub a6, a6, a4
[0x80005958]:addi t3, zero, 29
[0x8000595c]:lui t4, 523776
[0x80005960]:lui s10, 1048575
[0x80005964]:addi s10, s10, 1988
[0x80005968]:lui s11, 1048064
[0x8000596c]:addi s11, s11, 4095
[0x80005970]:addi a4, zero, 0
[0x80005974]:csrrw zero, fcsr, a4
[0x80005978]:fsub.d t5, t3, s10, dyn
[0x8000597c]:csrrs a7, fcsr, zero

[0x80005978]:fsub.d t5, t3, s10, dyn
[0x8000597c]:csrrs a7, fcsr, zero
[0x80005980]:sw t5, 1208(ra)
[0x80005984]:sw t6, 1216(ra)
[0x80005988]:sw t5, 1224(ra)
[0x8000598c]:sw a7, 1232(ra)
[0x80005990]:lui a4, 1
[0x80005994]:addi a4, a4, 2048
[0x80005998]:add a6, a6, a4
[0x8000599c]:lw t3, 1632(a6)
[0x800059a0]:sub a6, a6, a4
[0x800059a4]:lui a4, 1
[0x800059a8]:addi a4, a4, 2048
[0x800059ac]:add a6, a6, a4
[0x800059b0]:lw t4, 1636(a6)
[0x800059b4]:sub a6, a6, a4
[0x800059b8]:lui a4, 1
[0x800059bc]:addi a4, a4, 2048
[0x800059c0]:add a6, a6, a4
[0x800059c4]:lw s10, 1640(a6)
[0x800059c8]:sub a6, a6, a4
[0x800059cc]:lui a4, 1
[0x800059d0]:addi a4, a4, 2048
[0x800059d4]:add a6, a6, a4
[0x800059d8]:lw s11, 1644(a6)
[0x800059dc]:sub a6, a6, a4
[0x800059e0]:addi t3, zero, 32
[0x800059e4]:lui t4, 523776
[0x800059e8]:lui s10, 1048575
[0x800059ec]:addi s10, s10, 4030
[0x800059f0]:lui s11, 1048064
[0x800059f4]:addi s11, s11, 4095
[0x800059f8]:addi a4, zero, 0
[0x800059fc]:csrrw zero, fcsr, a4
[0x80005a00]:fsub.d t5, t3, s10, dyn
[0x80005a04]:csrrs a7, fcsr, zero

[0x80005a00]:fsub.d t5, t3, s10, dyn
[0x80005a04]:csrrs a7, fcsr, zero
[0x80005a08]:sw t5, 1240(ra)
[0x80005a0c]:sw t6, 1248(ra)
[0x80005a10]:sw t5, 1256(ra)
[0x80005a14]:sw a7, 1264(ra)
[0x80005a18]:lui a4, 1
[0x80005a1c]:addi a4, a4, 2048
[0x80005a20]:add a6, a6, a4
[0x80005a24]:lw t3, 1648(a6)
[0x80005a28]:sub a6, a6, a4
[0x80005a2c]:lui a4, 1
[0x80005a30]:addi a4, a4, 2048
[0x80005a34]:add a6, a6, a4
[0x80005a38]:lw t4, 1652(a6)
[0x80005a3c]:sub a6, a6, a4
[0x80005a40]:lui a4, 1
[0x80005a44]:addi a4, a4, 2048
[0x80005a48]:add a6, a6, a4
[0x80005a4c]:lw s10, 1656(a6)
[0x80005a50]:sub a6, a6, a4
[0x80005a54]:lui a4, 1
[0x80005a58]:addi a4, a4, 2048
[0x80005a5c]:add a6, a6, a4
[0x80005a60]:lw s11, 1660(a6)
[0x80005a64]:sub a6, a6, a4
[0x80005a68]:addi t3, zero, 10
[0x80005a6c]:lui t4, 523776
[0x80005a70]:lui s10, 1048574
[0x80005a74]:addi s10, s10, 4074
[0x80005a78]:lui s11, 1048064
[0x80005a7c]:addi s11, s11, 4095
[0x80005a80]:addi a4, zero, 0
[0x80005a84]:csrrw zero, fcsr, a4
[0x80005a88]:fsub.d t5, t3, s10, dyn
[0x80005a8c]:csrrs a7, fcsr, zero

[0x80005a88]:fsub.d t5, t3, s10, dyn
[0x80005a8c]:csrrs a7, fcsr, zero
[0x80005a90]:sw t5, 1272(ra)
[0x80005a94]:sw t6, 1280(ra)
[0x80005a98]:sw t5, 1288(ra)
[0x80005a9c]:sw a7, 1296(ra)
[0x80005aa0]:lui a4, 1
[0x80005aa4]:addi a4, a4, 2048
[0x80005aa8]:add a6, a6, a4
[0x80005aac]:lw t3, 1664(a6)
[0x80005ab0]:sub a6, a6, a4
[0x80005ab4]:lui a4, 1
[0x80005ab8]:addi a4, a4, 2048
[0x80005abc]:add a6, a6, a4
[0x80005ac0]:lw t4, 1668(a6)
[0x80005ac4]:sub a6, a6, a4
[0x80005ac8]:lui a4, 1
[0x80005acc]:addi a4, a4, 2048
[0x80005ad0]:add a6, a6, a4
[0x80005ad4]:lw s10, 1672(a6)
[0x80005ad8]:sub a6, a6, a4
[0x80005adc]:lui a4, 1
[0x80005ae0]:addi a4, a4, 2048
[0x80005ae4]:add a6, a6, a4
[0x80005ae8]:lw s11, 1676(a6)
[0x80005aec]:sub a6, a6, a4
[0x80005af0]:addi t3, zero, 71
[0x80005af4]:lui t4, 523776
[0x80005af8]:lui s10, 1048572
[0x80005afc]:addi s10, s10, 3952
[0x80005b00]:lui s11, 1048064
[0x80005b04]:addi s11, s11, 4095
[0x80005b08]:addi a4, zero, 0
[0x80005b0c]:csrrw zero, fcsr, a4
[0x80005b10]:fsub.d t5, t3, s10, dyn
[0x80005b14]:csrrs a7, fcsr, zero

[0x80005b10]:fsub.d t5, t3, s10, dyn
[0x80005b14]:csrrs a7, fcsr, zero
[0x80005b18]:sw t5, 1304(ra)
[0x80005b1c]:sw t6, 1312(ra)
[0x80005b20]:sw t5, 1320(ra)
[0x80005b24]:sw a7, 1328(ra)
[0x80005b28]:lui a4, 1
[0x80005b2c]:addi a4, a4, 2048
[0x80005b30]:add a6, a6, a4
[0x80005b34]:lw t3, 1680(a6)
[0x80005b38]:sub a6, a6, a4
[0x80005b3c]:lui a4, 1
[0x80005b40]:addi a4, a4, 2048
[0x80005b44]:add a6, a6, a4
[0x80005b48]:lw t4, 1684(a6)
[0x80005b4c]:sub a6, a6, a4
[0x80005b50]:lui a4, 1
[0x80005b54]:addi a4, a4, 2048
[0x80005b58]:add a6, a6, a4
[0x80005b5c]:lw s10, 1688(a6)
[0x80005b60]:sub a6, a6, a4
[0x80005b64]:lui a4, 1
[0x80005b68]:addi a4, a4, 2048
[0x80005b6c]:add a6, a6, a4
[0x80005b70]:lw s11, 1692(a6)
[0x80005b74]:sub a6, a6, a4
[0x80005b78]:addi t3, zero, 94
[0x80005b7c]:lui t4, 523776
[0x80005b80]:lui s10, 1048568
[0x80005b84]:addi s10, s10, 3906
[0x80005b88]:lui s11, 1048064
[0x80005b8c]:addi s11, s11, 4095
[0x80005b90]:addi a4, zero, 0
[0x80005b94]:csrrw zero, fcsr, a4
[0x80005b98]:fsub.d t5, t3, s10, dyn
[0x80005b9c]:csrrs a7, fcsr, zero

[0x80005b98]:fsub.d t5, t3, s10, dyn
[0x80005b9c]:csrrs a7, fcsr, zero
[0x80005ba0]:sw t5, 1336(ra)
[0x80005ba4]:sw t6, 1344(ra)
[0x80005ba8]:sw t5, 1352(ra)
[0x80005bac]:sw a7, 1360(ra)
[0x80005bb0]:lui a4, 1
[0x80005bb4]:addi a4, a4, 2048
[0x80005bb8]:add a6, a6, a4
[0x80005bbc]:lw t3, 1696(a6)
[0x80005bc0]:sub a6, a6, a4
[0x80005bc4]:lui a4, 1
[0x80005bc8]:addi a4, a4, 2048
[0x80005bcc]:add a6, a6, a4
[0x80005bd0]:lw t4, 1700(a6)
[0x80005bd4]:sub a6, a6, a4
[0x80005bd8]:lui a4, 1
[0x80005bdc]:addi a4, a4, 2048
[0x80005be0]:add a6, a6, a4
[0x80005be4]:lw s10, 1704(a6)
[0x80005be8]:sub a6, a6, a4
[0x80005bec]:lui a4, 1
[0x80005bf0]:addi a4, a4, 2048
[0x80005bf4]:add a6, a6, a4
[0x80005bf8]:lw s11, 1708(a6)
[0x80005bfc]:sub a6, a6, a4
[0x80005c00]:addi t3, zero, 3
[0x80005c04]:lui t4, 523776
[0x80005c08]:lui s10, 1048560
[0x80005c0c]:addi s10, s10, 4088
[0x80005c10]:lui s11, 1048064
[0x80005c14]:addi s11, s11, 4095
[0x80005c18]:addi a4, zero, 0
[0x80005c1c]:csrrw zero, fcsr, a4
[0x80005c20]:fsub.d t5, t3, s10, dyn
[0x80005c24]:csrrs a7, fcsr, zero

[0x80005c20]:fsub.d t5, t3, s10, dyn
[0x80005c24]:csrrs a7, fcsr, zero
[0x80005c28]:sw t5, 1368(ra)
[0x80005c2c]:sw t6, 1376(ra)
[0x80005c30]:sw t5, 1384(ra)
[0x80005c34]:sw a7, 1392(ra)
[0x80005c38]:lui a4, 1
[0x80005c3c]:addi a4, a4, 2048
[0x80005c40]:add a6, a6, a4
[0x80005c44]:lw t3, 1712(a6)
[0x80005c48]:sub a6, a6, a4
[0x80005c4c]:lui a4, 1
[0x80005c50]:addi a4, a4, 2048
[0x80005c54]:add a6, a6, a4
[0x80005c58]:lw t4, 1716(a6)
[0x80005c5c]:sub a6, a6, a4
[0x80005c60]:lui a4, 1
[0x80005c64]:addi a4, a4, 2048
[0x80005c68]:add a6, a6, a4
[0x80005c6c]:lw s10, 1720(a6)
[0x80005c70]:sub a6, a6, a4
[0x80005c74]:lui a4, 1
[0x80005c78]:addi a4, a4, 2048
[0x80005c7c]:add a6, a6, a4
[0x80005c80]:lw s11, 1724(a6)
[0x80005c84]:sub a6, a6, a4
[0x80005c88]:addi t3, zero, 2
[0x80005c8c]:lui t4, 523776
[0x80005c90]:lui s10, 1048544
[0x80005c94]:addi s10, s10, 4090
[0x80005c98]:lui s11, 1048064
[0x80005c9c]:addi s11, s11, 4095
[0x80005ca0]:addi a4, zero, 0
[0x80005ca4]:csrrw zero, fcsr, a4
[0x80005ca8]:fsub.d t5, t3, s10, dyn
[0x80005cac]:csrrs a7, fcsr, zero

[0x80005ca8]:fsub.d t5, t3, s10, dyn
[0x80005cac]:csrrs a7, fcsr, zero
[0x80005cb0]:sw t5, 1400(ra)
[0x80005cb4]:sw t6, 1408(ra)
[0x80005cb8]:sw t5, 1416(ra)
[0x80005cbc]:sw a7, 1424(ra)
[0x80005cc0]:lui a4, 1
[0x80005cc4]:addi a4, a4, 2048
[0x80005cc8]:add a6, a6, a4
[0x80005ccc]:lw t3, 1728(a6)
[0x80005cd0]:sub a6, a6, a4
[0x80005cd4]:lui a4, 1
[0x80005cd8]:addi a4, a4, 2048
[0x80005cdc]:add a6, a6, a4
[0x80005ce0]:lw t4, 1732(a6)
[0x80005ce4]:sub a6, a6, a4
[0x80005ce8]:lui a4, 1
[0x80005cec]:addi a4, a4, 2048
[0x80005cf0]:add a6, a6, a4
[0x80005cf4]:lw s10, 1736(a6)
[0x80005cf8]:sub a6, a6, a4
[0x80005cfc]:lui a4, 1
[0x80005d00]:addi a4, a4, 2048
[0x80005d04]:add a6, a6, a4
[0x80005d08]:lw s11, 1740(a6)
[0x80005d0c]:sub a6, a6, a4
[0x80005d10]:addi t3, zero, 97
[0x80005d14]:lui t4, 523776
[0x80005d18]:lui s10, 1048512
[0x80005d1c]:addi s10, s10, 3900
[0x80005d20]:lui s11, 1048064
[0x80005d24]:addi s11, s11, 4095
[0x80005d28]:addi a4, zero, 0
[0x80005d2c]:csrrw zero, fcsr, a4
[0x80005d30]:fsub.d t5, t3, s10, dyn
[0x80005d34]:csrrs a7, fcsr, zero

[0x80005d30]:fsub.d t5, t3, s10, dyn
[0x80005d34]:csrrs a7, fcsr, zero
[0x80005d38]:sw t5, 1432(ra)
[0x80005d3c]:sw t6, 1440(ra)
[0x80005d40]:sw t5, 1448(ra)
[0x80005d44]:sw a7, 1456(ra)
[0x80005d48]:lui a4, 1
[0x80005d4c]:addi a4, a4, 2048
[0x80005d50]:add a6, a6, a4
[0x80005d54]:lw t3, 1744(a6)
[0x80005d58]:sub a6, a6, a4
[0x80005d5c]:lui a4, 1
[0x80005d60]:addi a4, a4, 2048
[0x80005d64]:add a6, a6, a4
[0x80005d68]:lw t4, 1748(a6)
[0x80005d6c]:sub a6, a6, a4
[0x80005d70]:lui a4, 1
[0x80005d74]:addi a4, a4, 2048
[0x80005d78]:add a6, a6, a4
[0x80005d7c]:lw s10, 1752(a6)
[0x80005d80]:sub a6, a6, a4
[0x80005d84]:lui a4, 1
[0x80005d88]:addi a4, a4, 2048
[0x80005d8c]:add a6, a6, a4
[0x80005d90]:lw s11, 1756(a6)
[0x80005d94]:sub a6, a6, a4
[0x80005d98]:addi t3, zero, 64
[0x80005d9c]:lui t4, 523776
[0x80005da0]:lui s10, 1048448
[0x80005da4]:addi s10, s10, 3966
[0x80005da8]:lui s11, 1048064
[0x80005dac]:addi s11, s11, 4095
[0x80005db0]:addi a4, zero, 0
[0x80005db4]:csrrw zero, fcsr, a4
[0x80005db8]:fsub.d t5, t3, s10, dyn
[0x80005dbc]:csrrs a7, fcsr, zero

[0x80005db8]:fsub.d t5, t3, s10, dyn
[0x80005dbc]:csrrs a7, fcsr, zero
[0x80005dc0]:sw t5, 1464(ra)
[0x80005dc4]:sw t6, 1472(ra)
[0x80005dc8]:sw t5, 1480(ra)
[0x80005dcc]:sw a7, 1488(ra)
[0x80005dd0]:lui a4, 1
[0x80005dd4]:addi a4, a4, 2048
[0x80005dd8]:add a6, a6, a4
[0x80005ddc]:lw t3, 1760(a6)
[0x80005de0]:sub a6, a6, a4
[0x80005de4]:lui a4, 1
[0x80005de8]:addi a4, a4, 2048
[0x80005dec]:add a6, a6, a4
[0x80005df0]:lw t4, 1764(a6)
[0x80005df4]:sub a6, a6, a4
[0x80005df8]:lui a4, 1
[0x80005dfc]:addi a4, a4, 2048
[0x80005e00]:add a6, a6, a4
[0x80005e04]:lw s10, 1768(a6)
[0x80005e08]:sub a6, a6, a4
[0x80005e0c]:lui a4, 1
[0x80005e10]:addi a4, a4, 2048
[0x80005e14]:add a6, a6, a4
[0x80005e18]:lw s11, 1772(a6)
[0x80005e1c]:sub a6, a6, a4
[0x80005e20]:addi t3, zero, 20
[0x80005e24]:lui t4, 523776
[0x80005e28]:lui s10, 1048320
[0x80005e2c]:addi s10, s10, 4054
[0x80005e30]:lui s11, 1048064
[0x80005e34]:addi s11, s11, 4095
[0x80005e38]:addi a4, zero, 0
[0x80005e3c]:csrrw zero, fcsr, a4
[0x80005e40]:fsub.d t5, t3, s10, dyn
[0x80005e44]:csrrs a7, fcsr, zero

[0x80005e40]:fsub.d t5, t3, s10, dyn
[0x80005e44]:csrrs a7, fcsr, zero
[0x80005e48]:sw t5, 1496(ra)
[0x80005e4c]:sw t6, 1504(ra)
[0x80005e50]:sw t5, 1512(ra)
[0x80005e54]:sw a7, 1520(ra)
[0x80005e58]:lui a4, 1
[0x80005e5c]:addi a4, a4, 2048
[0x80005e60]:add a6, a6, a4
[0x80005e64]:lw t3, 1776(a6)
[0x80005e68]:sub a6, a6, a4
[0x80005e6c]:lui a4, 1
[0x80005e70]:addi a4, a4, 2048
[0x80005e74]:add a6, a6, a4
[0x80005e78]:lw t4, 1780(a6)
[0x80005e7c]:sub a6, a6, a4
[0x80005e80]:lui a4, 1
[0x80005e84]:addi a4, a4, 2048
[0x80005e88]:add a6, a6, a4
[0x80005e8c]:lw s10, 1784(a6)
[0x80005e90]:sub a6, a6, a4
[0x80005e94]:lui a4, 1
[0x80005e98]:addi a4, a4, 2048
[0x80005e9c]:add a6, a6, a4
[0x80005ea0]:lw s11, 1788(a6)
[0x80005ea4]:sub a6, a6, a4
[0x80005ea8]:addi t3, zero, 65
[0x80005eac]:lui t4, 523776
[0x80005eb0]:lui s10, 1048064
[0x80005eb4]:addi s10, s10, 3964
[0x80005eb8]:lui s11, 1048064
[0x80005ebc]:addi s11, s11, 4095
[0x80005ec0]:addi a4, zero, 0
[0x80005ec4]:csrrw zero, fcsr, a4
[0x80005ec8]:fsub.d t5, t3, s10, dyn
[0x80005ecc]:csrrs a7, fcsr, zero

[0x80005ec8]:fsub.d t5, t3, s10, dyn
[0x80005ecc]:csrrs a7, fcsr, zero
[0x80005ed0]:sw t5, 1528(ra)
[0x80005ed4]:sw t6, 1536(ra)
[0x80005ed8]:sw t5, 1544(ra)
[0x80005edc]:sw a7, 1552(ra)
[0x80005ee0]:lui a4, 1
[0x80005ee4]:addi a4, a4, 2048
[0x80005ee8]:add a6, a6, a4
[0x80005eec]:lw t3, 1792(a6)
[0x80005ef0]:sub a6, a6, a4
[0x80005ef4]:lui a4, 1
[0x80005ef8]:addi a4, a4, 2048
[0x80005efc]:add a6, a6, a4
[0x80005f00]:lw t4, 1796(a6)
[0x80005f04]:sub a6, a6, a4
[0x80005f08]:lui a4, 1
[0x80005f0c]:addi a4, a4, 2048
[0x80005f10]:add a6, a6, a4
[0x80005f14]:lw s10, 1800(a6)
[0x80005f18]:sub a6, a6, a4
[0x80005f1c]:lui a4, 1
[0x80005f20]:addi a4, a4, 2048
[0x80005f24]:add a6, a6, a4
[0x80005f28]:lw s11, 1804(a6)
[0x80005f2c]:sub a6, a6, a4
[0x80005f30]:addi t3, zero, 10
[0x80005f34]:lui t4, 523776
[0x80005f38]:lui s10, 1047552
[0x80005f3c]:addi s10, s10, 4074
[0x80005f40]:lui s11, 1048064
[0x80005f44]:addi s11, s11, 4095
[0x80005f48]:addi a4, zero, 0
[0x80005f4c]:csrrw zero, fcsr, a4
[0x80005f50]:fsub.d t5, t3, s10, dyn
[0x80005f54]:csrrs a7, fcsr, zero

[0x80005f50]:fsub.d t5, t3, s10, dyn
[0x80005f54]:csrrs a7, fcsr, zero
[0x80005f58]:sw t5, 1560(ra)
[0x80005f5c]:sw t6, 1568(ra)
[0x80005f60]:sw t5, 1576(ra)
[0x80005f64]:sw a7, 1584(ra)
[0x80005f68]:lui a4, 1
[0x80005f6c]:addi a4, a4, 2048
[0x80005f70]:add a6, a6, a4
[0x80005f74]:lw t3, 1808(a6)
[0x80005f78]:sub a6, a6, a4
[0x80005f7c]:lui a4, 1
[0x80005f80]:addi a4, a4, 2048
[0x80005f84]:add a6, a6, a4
[0x80005f88]:lw t4, 1812(a6)
[0x80005f8c]:sub a6, a6, a4
[0x80005f90]:lui a4, 1
[0x80005f94]:addi a4, a4, 2048
[0x80005f98]:add a6, a6, a4
[0x80005f9c]:lw s10, 1816(a6)
[0x80005fa0]:sub a6, a6, a4
[0x80005fa4]:lui a4, 1
[0x80005fa8]:addi a4, a4, 2048
[0x80005fac]:add a6, a6, a4
[0x80005fb0]:lw s11, 1820(a6)
[0x80005fb4]:sub a6, a6, a4
[0x80005fb8]:addi t3, zero, 86
[0x80005fbc]:lui t4, 523776
[0x80005fc0]:lui s10, 1046528
[0x80005fc4]:addi s10, s10, 3922
[0x80005fc8]:lui s11, 1048064
[0x80005fcc]:addi s11, s11, 4095
[0x80005fd0]:addi a4, zero, 0
[0x80005fd4]:csrrw zero, fcsr, a4
[0x80005fd8]:fsub.d t5, t3, s10, dyn
[0x80005fdc]:csrrs a7, fcsr, zero

[0x80005fd8]:fsub.d t5, t3, s10, dyn
[0x80005fdc]:csrrs a7, fcsr, zero
[0x80005fe0]:sw t5, 1592(ra)
[0x80005fe4]:sw t6, 1600(ra)
[0x80005fe8]:sw t5, 1608(ra)
[0x80005fec]:sw a7, 1616(ra)
[0x80005ff0]:lui a4, 1
[0x80005ff4]:addi a4, a4, 2048
[0x80005ff8]:add a6, a6, a4
[0x80005ffc]:lw t3, 1824(a6)
[0x80006000]:sub a6, a6, a4
[0x80006004]:lui a4, 1
[0x80006008]:addi a4, a4, 2048
[0x8000600c]:add a6, a6, a4
[0x80006010]:lw t4, 1828(a6)
[0x80006014]:sub a6, a6, a4
[0x80006018]:lui a4, 1
[0x8000601c]:addi a4, a4, 2048
[0x80006020]:add a6, a6, a4
[0x80006024]:lw s10, 1832(a6)
[0x80006028]:sub a6, a6, a4
[0x8000602c]:lui a4, 1
[0x80006030]:addi a4, a4, 2048
[0x80006034]:add a6, a6, a4
[0x80006038]:lw s11, 1836(a6)
[0x8000603c]:sub a6, a6, a4
[0x80006040]:addi t3, zero, 23
[0x80006044]:lui t4, 523776
[0x80006048]:addi s10, zero, 0
[0x8000604c]:lui s11, 524032
[0x80006050]:addi a4, zero, 0
[0x80006054]:csrrw zero, fcsr, a4
[0x80006058]:fsub.d t5, t3, s10, dyn
[0x8000605c]:csrrs a7, fcsr, zero

[0x80006058]:fsub.d t5, t3, s10, dyn
[0x8000605c]:csrrs a7, fcsr, zero
[0x80006060]:sw t5, 1624(ra)
[0x80006064]:sw t6, 1632(ra)
[0x80006068]:sw t5, 1640(ra)
[0x8000606c]:sw a7, 1648(ra)
[0x80006070]:lui a4, 1
[0x80006074]:addi a4, a4, 2048
[0x80006078]:add a6, a6, a4
[0x8000607c]:lw t3, 1840(a6)
[0x80006080]:sub a6, a6, a4
[0x80006084]:lui a4, 1
[0x80006088]:addi a4, a4, 2048
[0x8000608c]:add a6, a6, a4
[0x80006090]:lw t4, 1844(a6)
[0x80006094]:sub a6, a6, a4
[0x80006098]:lui a4, 1
[0x8000609c]:addi a4, a4, 2048
[0x800060a0]:add a6, a6, a4
[0x800060a4]:lw s10, 1848(a6)
[0x800060a8]:sub a6, a6, a4
[0x800060ac]:lui a4, 1
[0x800060b0]:addi a4, a4, 2048
[0x800060b4]:add a6, a6, a4
[0x800060b8]:lw s11, 1852(a6)
[0x800060bc]:sub a6, a6, a4
[0x800060c0]:addi t3, zero, 19
[0x800060c4]:lui t4, 523776
[0x800060c8]:addi s10, zero, 0
[0x800060cc]:lui s11, 524032
[0x800060d0]:addi a4, zero, 0
[0x800060d4]:csrrw zero, fcsr, a4
[0x800060d8]:fsub.d t5, t3, s10, dyn
[0x800060dc]:csrrs a7, fcsr, zero

[0x800060d8]:fsub.d t5, t3, s10, dyn
[0x800060dc]:csrrs a7, fcsr, zero
[0x800060e0]:sw t5, 1656(ra)
[0x800060e4]:sw t6, 1664(ra)
[0x800060e8]:sw t5, 1672(ra)
[0x800060ec]:sw a7, 1680(ra)
[0x800060f0]:lui a4, 1
[0x800060f4]:addi a4, a4, 2048
[0x800060f8]:add a6, a6, a4
[0x800060fc]:lw t3, 1856(a6)
[0x80006100]:sub a6, a6, a4
[0x80006104]:lui a4, 1
[0x80006108]:addi a4, a4, 2048
[0x8000610c]:add a6, a6, a4
[0x80006110]:lw t4, 1860(a6)
[0x80006114]:sub a6, a6, a4
[0x80006118]:lui a4, 1
[0x8000611c]:addi a4, a4, 2048
[0x80006120]:add a6, a6, a4
[0x80006124]:lw s10, 1864(a6)
[0x80006128]:sub a6, a6, a4
[0x8000612c]:lui a4, 1
[0x80006130]:addi a4, a4, 2048
[0x80006134]:add a6, a6, a4
[0x80006138]:lw s11, 1868(a6)
[0x8000613c]:sub a6, a6, a4
[0x80006140]:addi t3, zero, 40
[0x80006144]:lui t4, 523776
[0x80006148]:addi s10, zero, 0
[0x8000614c]:lui s11, 524032
[0x80006150]:addi a4, zero, 0
[0x80006154]:csrrw zero, fcsr, a4
[0x80006158]:fsub.d t5, t3, s10, dyn
[0x8000615c]:csrrs a7, fcsr, zero

[0x80006158]:fsub.d t5, t3, s10, dyn
[0x8000615c]:csrrs a7, fcsr, zero
[0x80006160]:sw t5, 1688(ra)
[0x80006164]:sw t6, 1696(ra)
[0x80006168]:sw t5, 1704(ra)
[0x8000616c]:sw a7, 1712(ra)
[0x80006170]:lui a4, 1
[0x80006174]:addi a4, a4, 2048
[0x80006178]:add a6, a6, a4
[0x8000617c]:lw t3, 1872(a6)
[0x80006180]:sub a6, a6, a4
[0x80006184]:lui a4, 1
[0x80006188]:addi a4, a4, 2048
[0x8000618c]:add a6, a6, a4
[0x80006190]:lw t4, 1876(a6)
[0x80006194]:sub a6, a6, a4
[0x80006198]:lui a4, 1
[0x8000619c]:addi a4, a4, 2048
[0x800061a0]:add a6, a6, a4
[0x800061a4]:lw s10, 1880(a6)
[0x800061a8]:sub a6, a6, a4
[0x800061ac]:lui a4, 1
[0x800061b0]:addi a4, a4, 2048
[0x800061b4]:add a6, a6, a4
[0x800061b8]:lw s11, 1884(a6)
[0x800061bc]:sub a6, a6, a4
[0x800061c0]:addi t3, zero, 91
[0x800061c4]:lui t4, 523776
[0x800061c8]:addi s10, zero, 0
[0x800061cc]:lui s11, 524032
[0x800061d0]:addi a4, zero, 0
[0x800061d4]:csrrw zero, fcsr, a4
[0x800061d8]:fsub.d t5, t3, s10, dyn
[0x800061dc]:csrrs a7, fcsr, zero

[0x800061d8]:fsub.d t5, t3, s10, dyn
[0x800061dc]:csrrs a7, fcsr, zero
[0x800061e0]:sw t5, 1720(ra)
[0x800061e4]:sw t6, 1728(ra)
[0x800061e8]:sw t5, 1736(ra)
[0x800061ec]:sw a7, 1744(ra)
[0x800061f0]:lui a4, 1
[0x800061f4]:addi a4, a4, 2048
[0x800061f8]:add a6, a6, a4
[0x800061fc]:lw t3, 1888(a6)
[0x80006200]:sub a6, a6, a4
[0x80006204]:lui a4, 1
[0x80006208]:addi a4, a4, 2048
[0x8000620c]:add a6, a6, a4
[0x80006210]:lw t4, 1892(a6)
[0x80006214]:sub a6, a6, a4
[0x80006218]:lui a4, 1
[0x8000621c]:addi a4, a4, 2048
[0x80006220]:add a6, a6, a4
[0x80006224]:lw s10, 1896(a6)
[0x80006228]:sub a6, a6, a4
[0x8000622c]:lui a4, 1
[0x80006230]:addi a4, a4, 2048
[0x80006234]:add a6, a6, a4
[0x80006238]:lw s11, 1900(a6)
[0x8000623c]:sub a6, a6, a4
[0x80006240]:addi t3, zero, 78
[0x80006244]:lui t4, 523776
[0x80006248]:addi s10, zero, 0
[0x8000624c]:lui s11, 524032
[0x80006250]:addi a4, zero, 0
[0x80006254]:csrrw zero, fcsr, a4
[0x80006258]:fsub.d t5, t3, s10, dyn
[0x8000625c]:csrrs a7, fcsr, zero

[0x80006258]:fsub.d t5, t3, s10, dyn
[0x8000625c]:csrrs a7, fcsr, zero
[0x80006260]:sw t5, 1752(ra)
[0x80006264]:sw t6, 1760(ra)
[0x80006268]:sw t5, 1768(ra)
[0x8000626c]:sw a7, 1776(ra)
[0x80006270]:lui a4, 1
[0x80006274]:addi a4, a4, 2048
[0x80006278]:add a6, a6, a4
[0x8000627c]:lw t3, 1904(a6)
[0x80006280]:sub a6, a6, a4
[0x80006284]:lui a4, 1
[0x80006288]:addi a4, a4, 2048
[0x8000628c]:add a6, a6, a4
[0x80006290]:lw t4, 1908(a6)
[0x80006294]:sub a6, a6, a4
[0x80006298]:lui a4, 1
[0x8000629c]:addi a4, a4, 2048
[0x800062a0]:add a6, a6, a4
[0x800062a4]:lw s10, 1912(a6)
[0x800062a8]:sub a6, a6, a4
[0x800062ac]:lui a4, 1
[0x800062b0]:addi a4, a4, 2048
[0x800062b4]:add a6, a6, a4
[0x800062b8]:lw s11, 1916(a6)
[0x800062bc]:sub a6, a6, a4
[0x800062c0]:addi t3, zero, 17
[0x800062c4]:lui t4, 523776
[0x800062c8]:addi s10, zero, 0
[0x800062cc]:lui s11, 524032
[0x800062d0]:addi a4, zero, 0
[0x800062d4]:csrrw zero, fcsr, a4
[0x800062d8]:fsub.d t5, t3, s10, dyn
[0x800062dc]:csrrs a7, fcsr, zero

[0x800062d8]:fsub.d t5, t3, s10, dyn
[0x800062dc]:csrrs a7, fcsr, zero
[0x800062e0]:sw t5, 1784(ra)
[0x800062e4]:sw t6, 1792(ra)
[0x800062e8]:sw t5, 1800(ra)
[0x800062ec]:sw a7, 1808(ra)
[0x800062f0]:lui a4, 1
[0x800062f4]:addi a4, a4, 2048
[0x800062f8]:add a6, a6, a4
[0x800062fc]:lw t3, 1920(a6)
[0x80006300]:sub a6, a6, a4
[0x80006304]:lui a4, 1
[0x80006308]:addi a4, a4, 2048
[0x8000630c]:add a6, a6, a4
[0x80006310]:lw t4, 1924(a6)
[0x80006314]:sub a6, a6, a4
[0x80006318]:lui a4, 1
[0x8000631c]:addi a4, a4, 2048
[0x80006320]:add a6, a6, a4
[0x80006324]:lw s10, 1928(a6)
[0x80006328]:sub a6, a6, a4
[0x8000632c]:lui a4, 1
[0x80006330]:addi a4, a4, 2048
[0x80006334]:add a6, a6, a4
[0x80006338]:lw s11, 1932(a6)
[0x8000633c]:sub a6, a6, a4
[0x80006340]:addi t3, zero, 93
[0x80006344]:lui t4, 523776
[0x80006348]:addi s10, zero, 0
[0x8000634c]:lui s11, 524032
[0x80006350]:addi a4, zero, 0
[0x80006354]:csrrw zero, fcsr, a4
[0x80006358]:fsub.d t5, t3, s10, dyn
[0x8000635c]:csrrs a7, fcsr, zero

[0x80006358]:fsub.d t5, t3, s10, dyn
[0x8000635c]:csrrs a7, fcsr, zero
[0x80006360]:sw t5, 1816(ra)
[0x80006364]:sw t6, 1824(ra)
[0x80006368]:sw t5, 1832(ra)
[0x8000636c]:sw a7, 1840(ra)
[0x80006370]:lui a4, 1
[0x80006374]:addi a4, a4, 2048
[0x80006378]:add a6, a6, a4
[0x8000637c]:lw t3, 1936(a6)
[0x80006380]:sub a6, a6, a4
[0x80006384]:lui a4, 1
[0x80006388]:addi a4, a4, 2048
[0x8000638c]:add a6, a6, a4
[0x80006390]:lw t4, 1940(a6)
[0x80006394]:sub a6, a6, a4
[0x80006398]:lui a4, 1
[0x8000639c]:addi a4, a4, 2048
[0x800063a0]:add a6, a6, a4
[0x800063a4]:lw s10, 1944(a6)
[0x800063a8]:sub a6, a6, a4
[0x800063ac]:lui a4, 1
[0x800063b0]:addi a4, a4, 2048
[0x800063b4]:add a6, a6, a4
[0x800063b8]:lw s11, 1948(a6)
[0x800063bc]:sub a6, a6, a4
[0x800063c0]:addi t3, zero, 41
[0x800063c4]:lui t4, 523776
[0x800063c8]:addi s10, zero, 0
[0x800063cc]:lui s11, 524032
[0x800063d0]:addi a4, zero, 0
[0x800063d4]:csrrw zero, fcsr, a4
[0x800063d8]:fsub.d t5, t3, s10, dyn
[0x800063dc]:csrrs a7, fcsr, zero

[0x800063d8]:fsub.d t5, t3, s10, dyn
[0x800063dc]:csrrs a7, fcsr, zero
[0x800063e0]:sw t5, 1848(ra)
[0x800063e4]:sw t6, 1856(ra)
[0x800063e8]:sw t5, 1864(ra)
[0x800063ec]:sw a7, 1872(ra)
[0x800063f0]:lui a4, 1
[0x800063f4]:addi a4, a4, 2048
[0x800063f8]:add a6, a6, a4
[0x800063fc]:lw t3, 1952(a6)
[0x80006400]:sub a6, a6, a4
[0x80006404]:lui a4, 1
[0x80006408]:addi a4, a4, 2048
[0x8000640c]:add a6, a6, a4
[0x80006410]:lw t4, 1956(a6)
[0x80006414]:sub a6, a6, a4
[0x80006418]:lui a4, 1
[0x8000641c]:addi a4, a4, 2048
[0x80006420]:add a6, a6, a4
[0x80006424]:lw s10, 1960(a6)
[0x80006428]:sub a6, a6, a4
[0x8000642c]:lui a4, 1
[0x80006430]:addi a4, a4, 2048
[0x80006434]:add a6, a6, a4
[0x80006438]:lw s11, 1964(a6)
[0x8000643c]:sub a6, a6, a4
[0x80006440]:addi t3, zero, 87
[0x80006444]:lui t4, 523776
[0x80006448]:addi s10, zero, 0
[0x8000644c]:lui s11, 524032
[0x80006450]:addi a4, zero, 0
[0x80006454]:csrrw zero, fcsr, a4
[0x80006458]:fsub.d t5, t3, s10, dyn
[0x8000645c]:csrrs a7, fcsr, zero

[0x80006458]:fsub.d t5, t3, s10, dyn
[0x8000645c]:csrrs a7, fcsr, zero
[0x80006460]:sw t5, 1880(ra)
[0x80006464]:sw t6, 1888(ra)
[0x80006468]:sw t5, 1896(ra)
[0x8000646c]:sw a7, 1904(ra)
[0x80006470]:lui a4, 1
[0x80006474]:addi a4, a4, 2048
[0x80006478]:add a6, a6, a4
[0x8000647c]:lw t3, 1968(a6)
[0x80006480]:sub a6, a6, a4
[0x80006484]:lui a4, 1
[0x80006488]:addi a4, a4, 2048
[0x8000648c]:add a6, a6, a4
[0x80006490]:lw t4, 1972(a6)
[0x80006494]:sub a6, a6, a4
[0x80006498]:lui a4, 1
[0x8000649c]:addi a4, a4, 2048
[0x800064a0]:add a6, a6, a4
[0x800064a4]:lw s10, 1976(a6)
[0x800064a8]:sub a6, a6, a4
[0x800064ac]:lui a4, 1
[0x800064b0]:addi a4, a4, 2048
[0x800064b4]:add a6, a6, a4
[0x800064b8]:lw s11, 1980(a6)
[0x800064bc]:sub a6, a6, a4
[0x800064c0]:addi t3, zero, 96
[0x800064c4]:lui t4, 523776
[0x800064c8]:addi s10, zero, 0
[0x800064cc]:lui s11, 524032
[0x800064d0]:addi a4, zero, 0
[0x800064d4]:csrrw zero, fcsr, a4
[0x800064d8]:fsub.d t5, t3, s10, dyn
[0x800064dc]:csrrs a7, fcsr, zero

[0x800064d8]:fsub.d t5, t3, s10, dyn
[0x800064dc]:csrrs a7, fcsr, zero
[0x800064e0]:sw t5, 1912(ra)
[0x800064e4]:sw t6, 1920(ra)
[0x800064e8]:sw t5, 1928(ra)
[0x800064ec]:sw a7, 1936(ra)
[0x800064f0]:lui a4, 1
[0x800064f4]:addi a4, a4, 2048
[0x800064f8]:add a6, a6, a4
[0x800064fc]:lw t3, 1984(a6)
[0x80006500]:sub a6, a6, a4
[0x80006504]:lui a4, 1
[0x80006508]:addi a4, a4, 2048
[0x8000650c]:add a6, a6, a4
[0x80006510]:lw t4, 1988(a6)
[0x80006514]:sub a6, a6, a4
[0x80006518]:lui a4, 1
[0x8000651c]:addi a4, a4, 2048
[0x80006520]:add a6, a6, a4
[0x80006524]:lw s10, 1992(a6)
[0x80006528]:sub a6, a6, a4
[0x8000652c]:lui a4, 1
[0x80006530]:addi a4, a4, 2048
[0x80006534]:add a6, a6, a4
[0x80006538]:lw s11, 1996(a6)
[0x8000653c]:sub a6, a6, a4
[0x80006540]:addi t3, zero, 27
[0x80006544]:lui t4, 523776
[0x80006548]:addi s10, zero, 0
[0x8000654c]:lui s11, 524032
[0x80006550]:addi a4, zero, 0
[0x80006554]:csrrw zero, fcsr, a4
[0x80006558]:fsub.d t5, t3, s10, dyn
[0x8000655c]:csrrs a7, fcsr, zero

[0x80006558]:fsub.d t5, t3, s10, dyn
[0x8000655c]:csrrs a7, fcsr, zero
[0x80006560]:sw t5, 1944(ra)
[0x80006564]:sw t6, 1952(ra)
[0x80006568]:sw t5, 1960(ra)
[0x8000656c]:sw a7, 1968(ra)
[0x80006570]:lui a4, 1
[0x80006574]:addi a4, a4, 2048
[0x80006578]:add a6, a6, a4
[0x8000657c]:lw t3, 2000(a6)
[0x80006580]:sub a6, a6, a4
[0x80006584]:lui a4, 1
[0x80006588]:addi a4, a4, 2048
[0x8000658c]:add a6, a6, a4
[0x80006590]:lw t4, 2004(a6)
[0x80006594]:sub a6, a6, a4
[0x80006598]:lui a4, 1
[0x8000659c]:addi a4, a4, 2048
[0x800065a0]:add a6, a6, a4
[0x800065a4]:lw s10, 2008(a6)
[0x800065a8]:sub a6, a6, a4
[0x800065ac]:lui a4, 1
[0x800065b0]:addi a4, a4, 2048
[0x800065b4]:add a6, a6, a4
[0x800065b8]:lw s11, 2012(a6)
[0x800065bc]:sub a6, a6, a4
[0x800065c0]:addi t3, zero, 39
[0x800065c4]:lui t4, 523776
[0x800065c8]:addi s10, zero, 0
[0x800065cc]:lui s11, 524032
[0x800065d0]:addi a4, zero, 0
[0x800065d4]:csrrw zero, fcsr, a4
[0x800065d8]:fsub.d t5, t3, s10, dyn
[0x800065dc]:csrrs a7, fcsr, zero

[0x800065d8]:fsub.d t5, t3, s10, dyn
[0x800065dc]:csrrs a7, fcsr, zero
[0x800065e0]:sw t5, 1976(ra)
[0x800065e4]:sw t6, 1984(ra)
[0x800065e8]:sw t5, 1992(ra)
[0x800065ec]:sw a7, 2000(ra)
[0x800065f0]:lui a4, 1
[0x800065f4]:addi a4, a4, 2048
[0x800065f8]:add a6, a6, a4
[0x800065fc]:lw t3, 2016(a6)
[0x80006600]:sub a6, a6, a4
[0x80006604]:lui a4, 1
[0x80006608]:addi a4, a4, 2048
[0x8000660c]:add a6, a6, a4
[0x80006610]:lw t4, 2020(a6)
[0x80006614]:sub a6, a6, a4
[0x80006618]:lui a4, 1
[0x8000661c]:addi a4, a4, 2048
[0x80006620]:add a6, a6, a4
[0x80006624]:lw s10, 2024(a6)
[0x80006628]:sub a6, a6, a4
[0x8000662c]:lui a4, 1
[0x80006630]:addi a4, a4, 2048
[0x80006634]:add a6, a6, a4
[0x80006638]:lw s11, 2028(a6)
[0x8000663c]:sub a6, a6, a4
[0x80006640]:addi t3, zero, 69
[0x80006644]:lui t4, 523776
[0x80006648]:addi s10, zero, 0
[0x8000664c]:lui s11, 524032
[0x80006650]:addi a4, zero, 0
[0x80006654]:csrrw zero, fcsr, a4
[0x80006658]:fsub.d t5, t3, s10, dyn
[0x8000665c]:csrrs a7, fcsr, zero

[0x80006658]:fsub.d t5, t3, s10, dyn
[0x8000665c]:csrrs a7, fcsr, zero
[0x80006660]:sw t5, 2008(ra)
[0x80006664]:sw t6, 2016(ra)
[0x80006668]:sw t5, 2024(ra)
[0x8000666c]:sw a7, 2032(ra)
[0x80006670]:lui a4, 1
[0x80006674]:addi a4, a4, 2048
[0x80006678]:add a6, a6, a4
[0x8000667c]:lw t3, 2032(a6)
[0x80006680]:sub a6, a6, a4
[0x80006684]:lui a4, 1
[0x80006688]:addi a4, a4, 2048
[0x8000668c]:add a6, a6, a4
[0x80006690]:lw t4, 2036(a6)
[0x80006694]:sub a6, a6, a4
[0x80006698]:lui a4, 1
[0x8000669c]:addi a4, a4, 2048
[0x800066a0]:add a6, a6, a4
[0x800066a4]:lw s10, 2040(a6)
[0x800066a8]:sub a6, a6, a4
[0x800066ac]:lui a4, 1
[0x800066b0]:addi a4, a4, 2048
[0x800066b4]:add a6, a6, a4
[0x800066b8]:lw s11, 2044(a6)
[0x800066bc]:sub a6, a6, a4
[0x800066c0]:addi t3, zero, 7
[0x800066c4]:lui t4, 523776
[0x800066c8]:addi s10, zero, 0
[0x800066cc]:lui s11, 524032
[0x800066d0]:addi a4, zero, 0
[0x800066d4]:csrrw zero, fcsr, a4
[0x800066d8]:fsub.d t5, t3, s10, dyn
[0x800066dc]:csrrs a7, fcsr, zero

[0x800066d8]:fsub.d t5, t3, s10, dyn
[0x800066dc]:csrrs a7, fcsr, zero
[0x800066e0]:sw t5, 2040(ra)
[0x800066e4]:addi ra, ra, 2040
[0x800066e8]:sw t6, 8(ra)
[0x800066ec]:sw t5, 16(ra)
[0x800066f0]:sw a7, 24(ra)
[0x800066f4]:auipc ra, 4
[0x800066f8]:addi ra, ra, 3284
[0x800066fc]:lw t3, 0(a6)
[0x80006700]:lw t4, 4(a6)
[0x80006704]:lw s10, 8(a6)
[0x80006708]:lw s11, 12(a6)
[0x8000670c]:addi t3, zero, 86
[0x80006710]:lui t4, 523776
[0x80006714]:addi s10, zero, 0
[0x80006718]:lui s11, 524032
[0x8000671c]:addi a4, zero, 0
[0x80006720]:csrrw zero, fcsr, a4
[0x80006724]:fsub.d t5, t3, s10, dyn
[0x80006728]:csrrs a7, fcsr, zero

[0x80006864]:fsub.d t5, t3, s10, dyn
[0x80006868]:csrrs a7, fcsr, zero
[0x8000686c]:sw t5, 160(ra)
[0x80006870]:sw t6, 168(ra)
[0x80006874]:sw t5, 176(ra)
[0x80006878]:sw a7, 184(ra)
[0x8000687c]:lw t3, 96(a6)
[0x80006880]:lw t4, 100(a6)
[0x80006884]:lw s10, 104(a6)
[0x80006888]:lw s11, 108(a6)
[0x8000688c]:addi t3, zero, 51
[0x80006890]:lui t4, 523776
[0x80006894]:addi s10, zero, 0
[0x80006898]:lui s11, 524032
[0x8000689c]:addi a4, zero, 0
[0x800068a0]:csrrw zero, fcsr, a4
[0x800068a4]:fsub.d t5, t3, s10, dyn
[0x800068a8]:csrrs a7, fcsr, zero

[0x800068a4]:fsub.d t5, t3, s10, dyn
[0x800068a8]:csrrs a7, fcsr, zero
[0x800068ac]:sw t5, 192(ra)
[0x800068b0]:sw t6, 200(ra)
[0x800068b4]:sw t5, 208(ra)
[0x800068b8]:sw a7, 216(ra)
[0x800068bc]:lw t3, 112(a6)
[0x800068c0]:lw t4, 116(a6)
[0x800068c4]:lw s10, 120(a6)
[0x800068c8]:lw s11, 124(a6)
[0x800068cc]:addi t3, zero, 18
[0x800068d0]:addi t4, zero, 0
[0x800068d4]:addi s10, zero, 17
[0x800068d8]:addi s11, zero, 0
[0x800068dc]:addi a4, zero, 0
[0x800068e0]:csrrw zero, fcsr, a4
[0x800068e4]:fsub.d t5, t3, s10, dyn
[0x800068e8]:csrrs a7, fcsr, zero

[0x800068e4]:fsub.d t5, t3, s10, dyn
[0x800068e8]:csrrs a7, fcsr, zero
[0x800068ec]:sw t5, 224(ra)
[0x800068f0]:sw t6, 232(ra)
[0x800068f4]:sw t5, 240(ra)
[0x800068f8]:sw a7, 248(ra)
[0x800068fc]:lw t3, 128(a6)
[0x80006900]:lw t4, 132(a6)
[0x80006904]:lw s10, 136(a6)
[0x80006908]:lw s11, 140(a6)
[0x8000690c]:addi t3, zero, 58
[0x80006910]:addi t4, zero, 0
[0x80006914]:addi s10, zero, 42
[0x80006918]:addi s11, zero, 0
[0x8000691c]:addi a4, zero, 0
[0x80006920]:csrrw zero, fcsr, a4
[0x80006924]:fsub.d t5, t3, s10, dyn
[0x80006928]:csrrs a7, fcsr, zero

[0x80006924]:fsub.d t5, t3, s10, dyn
[0x80006928]:csrrs a7, fcsr, zero
[0x8000692c]:sw t5, 256(ra)
[0x80006930]:sw t6, 264(ra)
[0x80006934]:sw t5, 272(ra)
[0x80006938]:sw a7, 280(ra)
[0x8000693c]:addi zero, zero, 0



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fsub.d', 'rs1 : x30', 'rs2 : x30', 'rd : x30', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x8000013c]:fsub.d t5, t5, t5, dyn
	-[0x80000140]:csrrs tp, fcsr, zero
	-[0x80000144]:sw t5, 0(ra)
	-[0x80000148]:sw t6, 8(ra)
Current Store : [0x80000148] : sw t6, 8(ra) -- Store: [0x80009320]:0x00000000




Last Coverpoint : ['mnemonic : fsub.d', 'rs1 : x30', 'rs2 : x30', 'rd : x30', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x8000013c]:fsub.d t5, t5, t5, dyn
	-[0x80000140]:csrrs tp, fcsr, zero
	-[0x80000144]:sw t5, 0(ra)
	-[0x80000148]:sw t6, 8(ra)
	-[0x8000014c]:sw t5, 16(ra)
Current Store : [0x8000014c] : sw t5, 16(ra) -- Store: [0x80009328]:0x00000000




Last Coverpoint : ['rs1 : x26', 'rs2 : x24', 'rd : x28', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000062 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000060 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000017c]:fsub.d t3, s10, s8, dyn
	-[0x80000180]:csrrs tp, fcsr, zero
	-[0x80000184]:sw t3, 32(ra)
	-[0x80000188]:sw t4, 40(ra)
Current Store : [0x80000188] : sw t4, 40(ra) -- Store: [0x80009340]:0xEEDBEADF




Last Coverpoint : ['rs1 : x26', 'rs2 : x24', 'rd : x28', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000062 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000060 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000017c]:fsub.d t3, s10, s8, dyn
	-[0x80000180]:csrrs tp, fcsr, zero
	-[0x80000184]:sw t3, 32(ra)
	-[0x80000188]:sw t4, 40(ra)
	-[0x8000018c]:sw t3, 48(ra)
Current Store : [0x8000018c] : sw t3, 48(ra) -- Store: [0x80009348]:0x00000002




Last Coverpoint : ['rs1 : x24', 'rs2 : x28', 'rd : x24', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000021 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000000001d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001bc]:fsub.d s8, s8, t3, dyn
	-[0x800001c0]:csrrs tp, fcsr, zero
	-[0x800001c4]:sw s8, 64(ra)
	-[0x800001c8]:sw s9, 72(ra)
Current Store : [0x800001c8] : sw s9, 72(ra) -- Store: [0x80009360]:0x00000000




Last Coverpoint : ['rs1 : x24', 'rs2 : x28', 'rd : x24', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000021 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000000001d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001bc]:fsub.d s8, s8, t3, dyn
	-[0x800001c0]:csrrs tp, fcsr, zero
	-[0x800001c4]:sw s8, 64(ra)
	-[0x800001c8]:sw s9, 72(ra)
	-[0x800001cc]:sw s8, 80(ra)
Current Store : [0x800001cc] : sw s8, 80(ra) -- Store: [0x80009368]:0x00000004




Last Coverpoint : ['rs1 : x28', 'rs2 : x26', 'rd : x26', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000040 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000038 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001fc]:fsub.d s10, t3, s10, dyn
	-[0x80000200]:csrrs tp, fcsr, zero
	-[0x80000204]:sw s10, 96(ra)
	-[0x80000208]:sw s11, 104(ra)
Current Store : [0x80000208] : sw s11, 104(ra) -- Store: [0x80009380]:0x00000000




Last Coverpoint : ['rs1 : x28', 'rs2 : x26', 'rd : x26', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000040 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000038 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001fc]:fsub.d s10, t3, s10, dyn
	-[0x80000200]:csrrs tp, fcsr, zero
	-[0x80000204]:sw s10, 96(ra)
	-[0x80000208]:sw s11, 104(ra)
	-[0x8000020c]:sw s10, 112(ra)
Current Store : [0x8000020c] : sw s10, 112(ra) -- Store: [0x80009388]:0x00000008




Last Coverpoint : ['rs1 : x20', 'rs2 : x20', 'rd : x22', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x8000023c]:fsub.d s6, s4, s4, dyn
	-[0x80000240]:csrrs tp, fcsr, zero
	-[0x80000244]:sw s6, 128(ra)
	-[0x80000248]:sw s7, 136(ra)
Current Store : [0x80000248] : sw s7, 136(ra) -- Store: [0x800093a0]:0xB6FAB7FB




Last Coverpoint : ['rs1 : x20', 'rs2 : x20', 'rd : x22', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x8000023c]:fsub.d s6, s4, s4, dyn
	-[0x80000240]:csrrs tp, fcsr, zero
	-[0x80000244]:sw s6, 128(ra)
	-[0x80000248]:sw s7, 136(ra)
	-[0x8000024c]:sw s6, 144(ra)
Current Store : [0x8000024c] : sw s6, 144(ra) -- Store: [0x800093a8]:0x00000000




Last Coverpoint : ['rs1 : x22', 'rs2 : x18', 'rd : x20', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000054 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000034 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000027c]:fsub.d s4, s6, s2, dyn
	-[0x80000280]:csrrs tp, fcsr, zero
	-[0x80000284]:sw s4, 160(ra)
	-[0x80000288]:sw s5, 168(ra)
Current Store : [0x80000288] : sw s5, 168(ra) -- Store: [0x800093c0]:0x00000000




Last Coverpoint : ['rs1 : x22', 'rs2 : x18', 'rd : x20', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000054 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000034 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000027c]:fsub.d s4, s6, s2, dyn
	-[0x80000280]:csrrs tp, fcsr, zero
	-[0x80000284]:sw s4, 160(ra)
	-[0x80000288]:sw s5, 168(ra)
	-[0x8000028c]:sw s4, 176(ra)
Current Store : [0x8000028c] : sw s4, 176(ra) -- Store: [0x800093c8]:0x00000020




Last Coverpoint : ['rs1 : x16', 'rs2 : x22', 'rd : x18', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001b and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000025 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002bc]:fsub.d s2, a6, s6, dyn
	-[0x800002c0]:csrrs tp, fcsr, zero
	-[0x800002c4]:sw s2, 192(ra)
	-[0x800002c8]:sw s3, 200(ra)
Current Store : [0x800002c8] : sw s3, 200(ra) -- Store: [0x800093e0]:0x00000000




Last Coverpoint : ['rs1 : x16', 'rs2 : x22', 'rd : x18', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001b and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000025 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002bc]:fsub.d s2, a6, s6, dyn
	-[0x800002c0]:csrrs tp, fcsr, zero
	-[0x800002c4]:sw s2, 192(ra)
	-[0x800002c8]:sw s3, 200(ra)
	-[0x800002cc]:sw s2, 208(ra)
Current Store : [0x800002cc] : sw s2, 208(ra) -- Store: [0x800093e8]:0x00000040




Last Coverpoint : ['rs1 : x18', 'rs2 : x14', 'rd : x16', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003f and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000041 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002fc]:fsub.d a6, s2, a4, dyn
	-[0x80000300]:csrrs tp, fcsr, zero
	-[0x80000304]:sw a6, 224(ra)
	-[0x80000308]:sw a7, 232(ra)
Current Store : [0x80000308] : sw a7, 232(ra) -- Store: [0x80009400]:0x00000000




Last Coverpoint : ['rs1 : x18', 'rs2 : x14', 'rd : x16', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003f and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000041 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002fc]:fsub.d a6, s2, a4, dyn
	-[0x80000300]:csrrs tp, fcsr, zero
	-[0x80000304]:sw a6, 224(ra)
	-[0x80000308]:sw a7, 232(ra)
	-[0x8000030c]:sw a6, 240(ra)
Current Store : [0x8000030c] : sw a6, 240(ra) -- Store: [0x80009408]:0x00000080




Last Coverpoint : ['rs1 : x12', 'rs2 : x16', 'rd : x14', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000032 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000000000ce and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000033c]:fsub.d a4, a2, a6, dyn
	-[0x80000340]:csrrs tp, fcsr, zero
	-[0x80000344]:sw a4, 256(ra)
	-[0x80000348]:sw a5, 264(ra)
Current Store : [0x80000348] : sw a5, 264(ra) -- Store: [0x80009420]:0x80000000




Last Coverpoint : ['rs1 : x12', 'rs2 : x16', 'rd : x14', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000032 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000000000ce and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000033c]:fsub.d a4, a2, a6, dyn
	-[0x80000340]:csrrs tp, fcsr, zero
	-[0x80000344]:sw a4, 256(ra)
	-[0x80000348]:sw a5, 264(ra)
	-[0x8000034c]:sw a4, 272(ra)
Current Store : [0x8000034c] : sw a4, 272(ra) -- Store: [0x80009428]:0x00000100




Last Coverpoint : ['rs1 : x14', 'rs2 : x10', 'rd : x12', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000004e and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000000001b2 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000384]:fsub.d a2, a4, a0, dyn
	-[0x80000388]:csrrs a7, fcsr, zero
	-[0x8000038c]:sw a2, 288(ra)
	-[0x80000390]:sw a3, 296(ra)
Current Store : [0x80000390] : sw a3, 296(ra) -- Store: [0x80009440]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rs2 : x10', 'rd : x12', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000004e and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000000001b2 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000384]:fsub.d a2, a4, a0, dyn
	-[0x80000388]:csrrs a7, fcsr, zero
	-[0x8000038c]:sw a2, 288(ra)
	-[0x80000390]:sw a3, 296(ra)
	-[0x80000394]:sw a2, 304(ra)
Current Store : [0x80000394] : sw a2, 304(ra) -- Store: [0x80009448]:0x00000200




Last Coverpoint : ['rs1 : x8', 'rs2 : x12', 'rd : x10', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000063 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000000039d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003c4]:fsub.d a0, fp, a2, dyn
	-[0x800003c8]:csrrs a7, fcsr, zero
	-[0x800003cc]:sw a0, 320(ra)
	-[0x800003d0]:sw a1, 328(ra)
Current Store : [0x800003d0] : sw a1, 328(ra) -- Store: [0x80009460]:0x80000000




Last Coverpoint : ['rs1 : x8', 'rs2 : x12', 'rd : x10', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000063 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000000039d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003c4]:fsub.d a0, fp, a2, dyn
	-[0x800003c8]:csrrs a7, fcsr, zero
	-[0x800003cc]:sw a0, 320(ra)
	-[0x800003d0]:sw a1, 328(ra)
	-[0x800003d4]:sw a0, 336(ra)
Current Store : [0x800003d4] : sw a0, 336(ra) -- Store: [0x80009468]:0x00000400




Last Coverpoint : ['rs1 : x10', 'rs2 : x6', 'rd : x8', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005a and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000000007a6 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000040c]:fsub.d fp, a0, t1, dyn
	-[0x80000410]:csrrs a7, fcsr, zero
	-[0x80000414]:sw fp, 0(ra)
	-[0x80000418]:sw s1, 8(ra)
Current Store : [0x80000418] : sw s1, 8(ra) -- Store: [0x800093d0]:0x00000000




Last Coverpoint : ['rs1 : x10', 'rs2 : x6', 'rd : x8', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005a and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000000007a6 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000040c]:fsub.d fp, a0, t1, dyn
	-[0x80000410]:csrrs a7, fcsr, zero
	-[0x80000414]:sw fp, 0(ra)
	-[0x80000418]:sw s1, 8(ra)
	-[0x8000041c]:sw fp, 16(ra)
Current Store : [0x8000041c] : sw fp, 16(ra) -- Store: [0x800093d8]:0x00000800




Last Coverpoint : ['rs1 : x4', 'rs2 : x8', 'rd : x6', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000023 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000fdd and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000450]:fsub.d t1, tp, fp, dyn
	-[0x80000454]:csrrs a7, fcsr, zero
	-[0x80000458]:sw t1, 32(ra)
	-[0x8000045c]:sw t2, 40(ra)
Current Store : [0x8000045c] : sw t2, 40(ra) -- Store: [0x800093f0]:0x80000000




Last Coverpoint : ['rs1 : x4', 'rs2 : x8', 'rd : x6', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000023 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000fdd and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000450]:fsub.d t1, tp, fp, dyn
	-[0x80000454]:csrrs a7, fcsr, zero
	-[0x80000458]:sw t1, 32(ra)
	-[0x8000045c]:sw t2, 40(ra)
	-[0x80000460]:sw t1, 48(ra)
Current Store : [0x80000460] : sw t1, 48(ra) -- Store: [0x800093f8]:0x00001000




Last Coverpoint : ['rs1 : x6', 'rs2 : x2', 'rd : x4', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001e and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000001fe2 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000494]:fsub.d tp, t1, sp, dyn
	-[0x80000498]:csrrs a7, fcsr, zero
	-[0x8000049c]:sw tp, 64(ra)
	-[0x800004a0]:sw t0, 72(ra)
Current Store : [0x800004a0] : sw t0, 72(ra) -- Store: [0x80009410]:0x00000000




Last Coverpoint : ['rs1 : x6', 'rs2 : x2', 'rd : x4', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001e and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000001fe2 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000494]:fsub.d tp, t1, sp, dyn
	-[0x80000498]:csrrs a7, fcsr, zero
	-[0x8000049c]:sw tp, 64(ra)
	-[0x800004a0]:sw t0, 72(ra)
	-[0x800004a4]:sw tp, 80(ra)
Current Store : [0x800004a4] : sw tp, 80(ra) -- Store: [0x80009418]:0x00002000




Last Coverpoint : ['rs1 : x2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000e and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000003ff2 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004d8]:fsub.d t5, sp, t3, dyn
	-[0x800004dc]:csrrs a7, fcsr, zero
	-[0x800004e0]:sw t5, 96(ra)
	-[0x800004e4]:sw t6, 104(ra)
Current Store : [0x800004e4] : sw t6, 104(ra) -- Store: [0x80009430]:0x00000000




Last Coverpoint : ['rs1 : x2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000e and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000003ff2 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004d8]:fsub.d t5, sp, t3, dyn
	-[0x800004dc]:csrrs a7, fcsr, zero
	-[0x800004e0]:sw t5, 96(ra)
	-[0x800004e4]:sw t6, 104(ra)
	-[0x800004e8]:sw t5, 112(ra)
Current Store : [0x800004e8] : sw t5, 112(ra) -- Store: [0x80009438]:0x00004000




Last Coverpoint : ['rs2 : x4', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000004 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000007ffc and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000051c]:fsub.d t5, t3, tp, dyn
	-[0x80000520]:csrrs a7, fcsr, zero
	-[0x80000524]:sw t5, 128(ra)
	-[0x80000528]:sw t6, 136(ra)
Current Store : [0x80000528] : sw t6, 136(ra) -- Store: [0x80009450]:0x00000000




Last Coverpoint : ['rs2 : x4', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000004 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000007ffc and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000051c]:fsub.d t5, t3, tp, dyn
	-[0x80000520]:csrrs a7, fcsr, zero
	-[0x80000524]:sw t5, 128(ra)
	-[0x80000528]:sw t6, 136(ra)
	-[0x8000052c]:sw t5, 144(ra)
Current Store : [0x8000052c] : sw t5, 144(ra) -- Store: [0x80009458]:0x00008000




Last Coverpoint : ['rd : x2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000004 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000000fffc and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000560]:fsub.d sp, t5, t3, dyn
	-[0x80000564]:csrrs a7, fcsr, zero
	-[0x80000568]:sw sp, 160(ra)
	-[0x8000056c]:sw gp, 168(ra)
Current Store : [0x8000056c] : sw gp, 168(ra) -- Store: [0x80009470]:0x00000000




Last Coverpoint : ['rd : x2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000004 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000000fffc and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000560]:fsub.d sp, t5, t3, dyn
	-[0x80000564]:csrrs a7, fcsr, zero
	-[0x80000568]:sw sp, 160(ra)
	-[0x8000056c]:sw gp, 168(ra)
	-[0x80000570]:sw sp, 176(ra)
Current Store : [0x80000570] : sw sp, 176(ra) -- Store: [0x80009478]:0x00010000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000046 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000001ffba and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005a4]:fsub.d t5, t3, s10, dyn
	-[0x800005a8]:csrrs a7, fcsr, zero
	-[0x800005ac]:sw t5, 192(ra)
	-[0x800005b0]:sw t6, 200(ra)
Current Store : [0x800005b0] : sw t6, 200(ra) -- Store: [0x80009490]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000046 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000001ffba and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005a4]:fsub.d t5, t3, s10, dyn
	-[0x800005a8]:csrrs a7, fcsr, zero
	-[0x800005ac]:sw t5, 192(ra)
	-[0x800005b0]:sw t6, 200(ra)
	-[0x800005b4]:sw t5, 208(ra)
Current Store : [0x800005b4] : sw t5, 208(ra) -- Store: [0x80009498]:0x00020000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000031 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000003ffcf and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005e8]:fsub.d t5, t3, s10, dyn
	-[0x800005ec]:csrrs a7, fcsr, zero
	-[0x800005f0]:sw t5, 224(ra)
	-[0x800005f4]:sw t6, 232(ra)
Current Store : [0x800005f4] : sw t6, 232(ra) -- Store: [0x800094b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000031 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000003ffcf and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005e8]:fsub.d t5, t3, s10, dyn
	-[0x800005ec]:csrrs a7, fcsr, zero
	-[0x800005f0]:sw t5, 224(ra)
	-[0x800005f4]:sw t6, 232(ra)
	-[0x800005f8]:sw t5, 240(ra)
Current Store : [0x800005f8] : sw t5, 240(ra) -- Store: [0x800094b8]:0x00040000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001c and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000007ffe4 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000062c]:fsub.d t5, t3, s10, dyn
	-[0x80000630]:csrrs a7, fcsr, zero
	-[0x80000634]:sw t5, 256(ra)
	-[0x80000638]:sw t6, 264(ra)
Current Store : [0x80000638] : sw t6, 264(ra) -- Store: [0x800094d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001c and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000007ffe4 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000062c]:fsub.d t5, t3, s10, dyn
	-[0x80000630]:csrrs a7, fcsr, zero
	-[0x80000634]:sw t5, 256(ra)
	-[0x80000638]:sw t6, 264(ra)
	-[0x8000063c]:sw t5, 272(ra)
Current Store : [0x8000063c] : sw t5, 272(ra) -- Store: [0x800094d8]:0x00080000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005d and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000000fffa3 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000670]:fsub.d t5, t3, s10, dyn
	-[0x80000674]:csrrs a7, fcsr, zero
	-[0x80000678]:sw t5, 288(ra)
	-[0x8000067c]:sw t6, 296(ra)
Current Store : [0x8000067c] : sw t6, 296(ra) -- Store: [0x800094f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005d and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000000fffa3 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000670]:fsub.d t5, t3, s10, dyn
	-[0x80000674]:csrrs a7, fcsr, zero
	-[0x80000678]:sw t5, 288(ra)
	-[0x8000067c]:sw t6, 296(ra)
	-[0x80000680]:sw t5, 304(ra)
Current Store : [0x80000680] : sw t5, 304(ra) -- Store: [0x800094f8]:0x00100000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000044 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000001fffbc and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006b4]:fsub.d t5, t3, s10, dyn
	-[0x800006b8]:csrrs a7, fcsr, zero
	-[0x800006bc]:sw t5, 320(ra)
	-[0x800006c0]:sw t6, 328(ra)
Current Store : [0x800006c0] : sw t6, 328(ra) -- Store: [0x80009510]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000044 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000001fffbc and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006b4]:fsub.d t5, t3, s10, dyn
	-[0x800006b8]:csrrs a7, fcsr, zero
	-[0x800006bc]:sw t5, 320(ra)
	-[0x800006c0]:sw t6, 328(ra)
	-[0x800006c4]:sw t5, 336(ra)
Current Store : [0x800006c4] : sw t5, 336(ra) -- Store: [0x80009518]:0x00200000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000062 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000003fff9e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006f8]:fsub.d t5, t3, s10, dyn
	-[0x800006fc]:csrrs a7, fcsr, zero
	-[0x80000700]:sw t5, 352(ra)
	-[0x80000704]:sw t6, 360(ra)
Current Store : [0x80000704] : sw t6, 360(ra) -- Store: [0x80009530]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000062 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000003fff9e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006f8]:fsub.d t5, t3, s10, dyn
	-[0x800006fc]:csrrs a7, fcsr, zero
	-[0x80000700]:sw t5, 352(ra)
	-[0x80000704]:sw t6, 360(ra)
	-[0x80000708]:sw t5, 368(ra)
Current Store : [0x80000708] : sw t5, 368(ra) -- Store: [0x80009538]:0x00400000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000040 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000041 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000738]:fsub.d t5, t3, s10, dyn
	-[0x8000073c]:csrrs a7, fcsr, zero
	-[0x80000740]:sw t5, 384(ra)
	-[0x80000744]:sw t6, 392(ra)
Current Store : [0x80000744] : sw t6, 392(ra) -- Store: [0x80009550]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000040 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000041 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000738]:fsub.d t5, t3, s10, dyn
	-[0x8000073c]:csrrs a7, fcsr, zero
	-[0x80000740]:sw t5, 384(ra)
	-[0x80000744]:sw t6, 392(ra)
	-[0x80000748]:sw t5, 400(ra)
Current Store : [0x80000748] : sw t5, 400(ra) -- Store: [0x80009558]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000020 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000778]:fsub.d t5, t3, s10, dyn
	-[0x8000077c]:csrrs a7, fcsr, zero
	-[0x80000780]:sw t5, 416(ra)
	-[0x80000784]:sw t6, 424(ra)
Current Store : [0x80000784] : sw t6, 424(ra) -- Store: [0x80009570]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000020 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000778]:fsub.d t5, t3, s10, dyn
	-[0x8000077c]:csrrs a7, fcsr, zero
	-[0x80000780]:sw t5, 416(ra)
	-[0x80000784]:sw t6, 424(ra)
	-[0x80000788]:sw t5, 432(ra)
Current Store : [0x80000788] : sw t5, 432(ra) -- Store: [0x80009578]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000022 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007b8]:fsub.d t5, t3, s10, dyn
	-[0x800007bc]:csrrs a7, fcsr, zero
	-[0x800007c0]:sw t5, 448(ra)
	-[0x800007c4]:sw t6, 456(ra)
Current Store : [0x800007c4] : sw t6, 456(ra) -- Store: [0x80009590]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000022 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007b8]:fsub.d t5, t3, s10, dyn
	-[0x800007bc]:csrrs a7, fcsr, zero
	-[0x800007c0]:sw t5, 448(ra)
	-[0x800007c4]:sw t6, 456(ra)
	-[0x800007c8]:sw t5, 464(ra)
Current Store : [0x800007c8] : sw t5, 464(ra) -- Store: [0x80009598]:0x00000004




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001d and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000025 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007f8]:fsub.d t5, t3, s10, dyn
	-[0x800007fc]:csrrs a7, fcsr, zero
	-[0x80000800]:sw t5, 480(ra)
	-[0x80000804]:sw t6, 488(ra)
Current Store : [0x80000804] : sw t6, 488(ra) -- Store: [0x800095b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001d and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000025 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007f8]:fsub.d t5, t3, s10, dyn
	-[0x800007fc]:csrrs a7, fcsr, zero
	-[0x80000800]:sw t5, 480(ra)
	-[0x80000804]:sw t6, 488(ra)
	-[0x80000808]:sw t5, 496(ra)
Current Store : [0x80000808] : sw t5, 496(ra) -- Store: [0x800095b8]:0x00000008




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003b and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000000004b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000838]:fsub.d t5, t3, s10, dyn
	-[0x8000083c]:csrrs a7, fcsr, zero
	-[0x80000840]:sw t5, 512(ra)
	-[0x80000844]:sw t6, 520(ra)
Current Store : [0x80000844] : sw t6, 520(ra) -- Store: [0x800095d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003b and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000000004b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000838]:fsub.d t5, t3, s10, dyn
	-[0x8000083c]:csrrs a7, fcsr, zero
	-[0x80000840]:sw t5, 512(ra)
	-[0x80000844]:sw t6, 520(ra)
	-[0x80000848]:sw t5, 528(ra)
Current Store : [0x80000848] : sw t5, 528(ra) -- Store: [0x800095d8]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000003 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000023 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000878]:fsub.d t5, t3, s10, dyn
	-[0x8000087c]:csrrs a7, fcsr, zero
	-[0x80000880]:sw t5, 544(ra)
	-[0x80000884]:sw t6, 552(ra)
Current Store : [0x80000884] : sw t6, 552(ra) -- Store: [0x800095f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000003 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000023 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000878]:fsub.d t5, t3, s10, dyn
	-[0x8000087c]:csrrs a7, fcsr, zero
	-[0x80000880]:sw t5, 544(ra)
	-[0x80000884]:sw t6, 552(ra)
	-[0x80000888]:sw t5, 560(ra)
Current Store : [0x80000888] : sw t5, 560(ra) -- Store: [0x800095f8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000048 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000088 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008b8]:fsub.d t5, t3, s10, dyn
	-[0x800008bc]:csrrs a7, fcsr, zero
	-[0x800008c0]:sw t5, 576(ra)
	-[0x800008c4]:sw t6, 584(ra)
Current Store : [0x800008c4] : sw t6, 584(ra) -- Store: [0x80009610]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000048 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000088 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008b8]:fsub.d t5, t3, s10, dyn
	-[0x800008bc]:csrrs a7, fcsr, zero
	-[0x800008c0]:sw t5, 576(ra)
	-[0x800008c4]:sw t6, 584(ra)
	-[0x800008c8]:sw t5, 592(ra)
Current Store : [0x800008c8] : sw t5, 592(ra) -- Store: [0x80009618]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000d and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000000008d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008f8]:fsub.d t5, t3, s10, dyn
	-[0x800008fc]:csrrs a7, fcsr, zero
	-[0x80000900]:sw t5, 608(ra)
	-[0x80000904]:sw t6, 616(ra)
Current Store : [0x80000904] : sw t6, 616(ra) -- Store: [0x80009630]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000d and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000000008d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008f8]:fsub.d t5, t3, s10, dyn
	-[0x800008fc]:csrrs a7, fcsr, zero
	-[0x80000900]:sw t5, 608(ra)
	-[0x80000904]:sw t6, 616(ra)
	-[0x80000908]:sw t5, 624(ra)
Current Store : [0x80000908] : sw t5, 624(ra) -- Store: [0x80009638]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000051 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000151 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000938]:fsub.d t5, t3, s10, dyn
	-[0x8000093c]:csrrs a7, fcsr, zero
	-[0x80000940]:sw t5, 640(ra)
	-[0x80000944]:sw t6, 648(ra)
Current Store : [0x80000944] : sw t6, 648(ra) -- Store: [0x80009650]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000051 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000151 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000938]:fsub.d t5, t3, s10, dyn
	-[0x8000093c]:csrrs a7, fcsr, zero
	-[0x80000940]:sw t5, 640(ra)
	-[0x80000944]:sw t6, 648(ra)
	-[0x80000948]:sw t5, 656(ra)
Current Store : [0x80000948] : sw t5, 656(ra) -- Store: [0x80009658]:0x00000100




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000026 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000226 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000978]:fsub.d t5, t3, s10, dyn
	-[0x8000097c]:csrrs a7, fcsr, zero
	-[0x80000980]:sw t5, 672(ra)
	-[0x80000984]:sw t6, 680(ra)
Current Store : [0x80000984] : sw t6, 680(ra) -- Store: [0x80009670]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000026 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000226 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000978]:fsub.d t5, t3, s10, dyn
	-[0x8000097c]:csrrs a7, fcsr, zero
	-[0x80000980]:sw t5, 672(ra)
	-[0x80000984]:sw t6, 680(ra)
	-[0x80000988]:sw t5, 688(ra)
Current Store : [0x80000988] : sw t5, 688(ra) -- Store: [0x80009678]:0x00000200




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000060 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000460 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009b8]:fsub.d t5, t3, s10, dyn
	-[0x800009bc]:csrrs a7, fcsr, zero
	-[0x800009c0]:sw t5, 704(ra)
	-[0x800009c4]:sw t6, 712(ra)
Current Store : [0x800009c4] : sw t6, 712(ra) -- Store: [0x80009690]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000060 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000460 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009b8]:fsub.d t5, t3, s10, dyn
	-[0x800009bc]:csrrs a7, fcsr, zero
	-[0x800009c0]:sw t5, 704(ra)
	-[0x800009c4]:sw t6, 712(ra)
	-[0x800009c8]:sw t5, 720(ra)
Current Store : [0x800009c8] : sw t5, 720(ra) -- Store: [0x80009698]:0x00000400




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005d and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000000085d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009fc]:fsub.d t5, t3, s10, dyn
	-[0x80000a00]:csrrs a7, fcsr, zero
	-[0x80000a04]:sw t5, 736(ra)
	-[0x80000a08]:sw t6, 744(ra)
Current Store : [0x80000a08] : sw t6, 744(ra) -- Store: [0x800096b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005d and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000000085d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009fc]:fsub.d t5, t3, s10, dyn
	-[0x80000a00]:csrrs a7, fcsr, zero
	-[0x80000a04]:sw t5, 736(ra)
	-[0x80000a08]:sw t6, 744(ra)
	-[0x80000a0c]:sw t5, 752(ra)
Current Store : [0x80000a0c] : sw t5, 752(ra) -- Store: [0x800096b8]:0x00000800




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000041 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000001041 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a40]:fsub.d t5, t3, s10, dyn
	-[0x80000a44]:csrrs a7, fcsr, zero
	-[0x80000a48]:sw t5, 768(ra)
	-[0x80000a4c]:sw t6, 776(ra)
Current Store : [0x80000a4c] : sw t6, 776(ra) -- Store: [0x800096d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000041 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000001041 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a40]:fsub.d t5, t3, s10, dyn
	-[0x80000a44]:csrrs a7, fcsr, zero
	-[0x80000a48]:sw t5, 768(ra)
	-[0x80000a4c]:sw t6, 776(ra)
	-[0x80000a50]:sw t5, 784(ra)
Current Store : [0x80000a50] : sw t5, 784(ra) -- Store: [0x800096d8]:0x00001000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000041 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000002041 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a84]:fsub.d t5, t3, s10, dyn
	-[0x80000a88]:csrrs a7, fcsr, zero
	-[0x80000a8c]:sw t5, 800(ra)
	-[0x80000a90]:sw t6, 808(ra)
Current Store : [0x80000a90] : sw t6, 808(ra) -- Store: [0x800096f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000041 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000002041 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a84]:fsub.d t5, t3, s10, dyn
	-[0x80000a88]:csrrs a7, fcsr, zero
	-[0x80000a8c]:sw t5, 800(ra)
	-[0x80000a90]:sw t6, 808(ra)
	-[0x80000a94]:sw t5, 816(ra)
Current Store : [0x80000a94] : sw t5, 816(ra) -- Store: [0x800096f8]:0x00002000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000019 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000004019 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ac8]:fsub.d t5, t3, s10, dyn
	-[0x80000acc]:csrrs a7, fcsr, zero
	-[0x80000ad0]:sw t5, 832(ra)
	-[0x80000ad4]:sw t6, 840(ra)
Current Store : [0x80000ad4] : sw t6, 840(ra) -- Store: [0x80009710]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000019 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000004019 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ac8]:fsub.d t5, t3, s10, dyn
	-[0x80000acc]:csrrs a7, fcsr, zero
	-[0x80000ad0]:sw t5, 832(ra)
	-[0x80000ad4]:sw t6, 840(ra)
	-[0x80000ad8]:sw t5, 848(ra)
Current Store : [0x80000ad8] : sw t5, 848(ra) -- Store: [0x80009718]:0x00004000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000025 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000008025 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b0c]:fsub.d t5, t3, s10, dyn
	-[0x80000b10]:csrrs a7, fcsr, zero
	-[0x80000b14]:sw t5, 864(ra)
	-[0x80000b18]:sw t6, 872(ra)
Current Store : [0x80000b18] : sw t6, 872(ra) -- Store: [0x80009730]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000025 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000008025 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b0c]:fsub.d t5, t3, s10, dyn
	-[0x80000b10]:csrrs a7, fcsr, zero
	-[0x80000b14]:sw t5, 864(ra)
	-[0x80000b18]:sw t6, 872(ra)
	-[0x80000b1c]:sw t5, 880(ra)
Current Store : [0x80000b1c] : sw t5, 880(ra) -- Store: [0x80009738]:0x00008000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000040 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000010040 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b50]:fsub.d t5, t3, s10, dyn
	-[0x80000b54]:csrrs a7, fcsr, zero
	-[0x80000b58]:sw t5, 896(ra)
	-[0x80000b5c]:sw t6, 904(ra)
Current Store : [0x80000b5c] : sw t6, 904(ra) -- Store: [0x80009750]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000040 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000010040 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b50]:fsub.d t5, t3, s10, dyn
	-[0x80000b54]:csrrs a7, fcsr, zero
	-[0x80000b58]:sw t5, 896(ra)
	-[0x80000b5c]:sw t6, 904(ra)
	-[0x80000b60]:sw t5, 912(ra)
Current Store : [0x80000b60] : sw t5, 912(ra) -- Store: [0x80009758]:0x00010000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000033 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000020033 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b94]:fsub.d t5, t3, s10, dyn
	-[0x80000b98]:csrrs a7, fcsr, zero
	-[0x80000b9c]:sw t5, 928(ra)
	-[0x80000ba0]:sw t6, 936(ra)
Current Store : [0x80000ba0] : sw t6, 936(ra) -- Store: [0x80009770]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000033 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000020033 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b94]:fsub.d t5, t3, s10, dyn
	-[0x80000b98]:csrrs a7, fcsr, zero
	-[0x80000b9c]:sw t5, 928(ra)
	-[0x80000ba0]:sw t6, 936(ra)
	-[0x80000ba4]:sw t5, 944(ra)
Current Store : [0x80000ba4] : sw t5, 944(ra) -- Store: [0x80009778]:0x00020000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000005 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000040005 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bd8]:fsub.d t5, t3, s10, dyn
	-[0x80000bdc]:csrrs a7, fcsr, zero
	-[0x80000be0]:sw t5, 960(ra)
	-[0x80000be4]:sw t6, 968(ra)
Current Store : [0x80000be4] : sw t6, 968(ra) -- Store: [0x80009790]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000005 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000040005 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bd8]:fsub.d t5, t3, s10, dyn
	-[0x80000bdc]:csrrs a7, fcsr, zero
	-[0x80000be0]:sw t5, 960(ra)
	-[0x80000be4]:sw t6, 968(ra)
	-[0x80000be8]:sw t5, 976(ra)
Current Store : [0x80000be8] : sw t5, 976(ra) -- Store: [0x80009798]:0x00040000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000020 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000080020 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c1c]:fsub.d t5, t3, s10, dyn
	-[0x80000c20]:csrrs a7, fcsr, zero
	-[0x80000c24]:sw t5, 992(ra)
	-[0x80000c28]:sw t6, 1000(ra)
Current Store : [0x80000c28] : sw t6, 1000(ra) -- Store: [0x800097b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000020 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000080020 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c1c]:fsub.d t5, t3, s10, dyn
	-[0x80000c20]:csrrs a7, fcsr, zero
	-[0x80000c24]:sw t5, 992(ra)
	-[0x80000c28]:sw t6, 1000(ra)
	-[0x80000c2c]:sw t5, 1008(ra)
Current Store : [0x80000c2c] : sw t5, 1008(ra) -- Store: [0x800097b8]:0x00080000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000034 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000100034 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c60]:fsub.d t5, t3, s10, dyn
	-[0x80000c64]:csrrs a7, fcsr, zero
	-[0x80000c68]:sw t5, 1024(ra)
	-[0x80000c6c]:sw t6, 1032(ra)
Current Store : [0x80000c6c] : sw t6, 1032(ra) -- Store: [0x800097d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000034 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000100034 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c60]:fsub.d t5, t3, s10, dyn
	-[0x80000c64]:csrrs a7, fcsr, zero
	-[0x80000c68]:sw t5, 1024(ra)
	-[0x80000c6c]:sw t6, 1032(ra)
	-[0x80000c70]:sw t5, 1040(ra)
Current Store : [0x80000c70] : sw t5, 1040(ra) -- Store: [0x800097d8]:0x00100000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000056 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000200056 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ca4]:fsub.d t5, t3, s10, dyn
	-[0x80000ca8]:csrrs a7, fcsr, zero
	-[0x80000cac]:sw t5, 1056(ra)
	-[0x80000cb0]:sw t6, 1064(ra)
Current Store : [0x80000cb0] : sw t6, 1064(ra) -- Store: [0x800097f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000056 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000200056 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ca4]:fsub.d t5, t3, s10, dyn
	-[0x80000ca8]:csrrs a7, fcsr, zero
	-[0x80000cac]:sw t5, 1056(ra)
	-[0x80000cb0]:sw t6, 1064(ra)
	-[0x80000cb4]:sw t5, 1072(ra)
Current Store : [0x80000cb4] : sw t5, 1072(ra) -- Store: [0x800097f8]:0x00200000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000002f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000040002f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ce8]:fsub.d t5, t3, s10, dyn
	-[0x80000cec]:csrrs a7, fcsr, zero
	-[0x80000cf0]:sw t5, 1088(ra)
	-[0x80000cf4]:sw t6, 1096(ra)
Current Store : [0x80000cf4] : sw t6, 1096(ra) -- Store: [0x80009810]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000002f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000040002f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ce8]:fsub.d t5, t3, s10, dyn
	-[0x80000cec]:csrrs a7, fcsr, zero
	-[0x80000cf0]:sw t5, 1088(ra)
	-[0x80000cf4]:sw t6, 1096(ra)
	-[0x80000cf8]:sw t5, 1104(ra)
Current Store : [0x80000cf8] : sw t5, 1104(ra) -- Store: [0x80009818]:0x00400000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000005a and fs2 == 0 and fe2 == 0x3d1 and fm2 == 0x6400000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d28]:fsub.d t5, t3, s10, dyn
	-[0x80000d2c]:csrrs a7, fcsr, zero
	-[0x80000d30]:sw t5, 1120(ra)
	-[0x80000d34]:sw t6, 1128(ra)
Current Store : [0x80000d34] : sw t6, 1128(ra) -- Store: [0x80009830]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000005a and fs2 == 0 and fe2 == 0x3d1 and fm2 == 0x6400000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d28]:fsub.d t5, t3, s10, dyn
	-[0x80000d2c]:csrrs a7, fcsr, zero
	-[0x80000d30]:sw t5, 1120(ra)
	-[0x80000d34]:sw t6, 1128(ra)
	-[0x80000d38]:sw t5, 1136(ra)
Current Store : [0x80000d38] : sw t5, 1136(ra) -- Store: [0x80009838]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000005f and fs2 == 0 and fe2 == 0x3d1 and fm2 == 0x7400000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d68]:fsub.d t5, t3, s10, dyn
	-[0x80000d6c]:csrrs a7, fcsr, zero
	-[0x80000d70]:sw t5, 1152(ra)
	-[0x80000d74]:sw t6, 1160(ra)
Current Store : [0x80000d74] : sw t6, 1160(ra) -- Store: [0x80009850]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000005f and fs2 == 0 and fe2 == 0x3d1 and fm2 == 0x7400000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d68]:fsub.d t5, t3, s10, dyn
	-[0x80000d6c]:csrrs a7, fcsr, zero
	-[0x80000d70]:sw t5, 1152(ra)
	-[0x80000d74]:sw t6, 1160(ra)
	-[0x80000d78]:sw t5, 1168(ra)
Current Store : [0x80000d78] : sw t5, 1168(ra) -- Store: [0x80009858]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000000c and fs2 == 0 and fe2 == 0x3ce and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000da8]:fsub.d t5, t3, s10, dyn
	-[0x80000dac]:csrrs a7, fcsr, zero
	-[0x80000db0]:sw t5, 1184(ra)
	-[0x80000db4]:sw t6, 1192(ra)
Current Store : [0x80000db4] : sw t6, 1192(ra) -- Store: [0x80009870]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000000c and fs2 == 0 and fe2 == 0x3ce and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000da8]:fsub.d t5, t3, s10, dyn
	-[0x80000dac]:csrrs a7, fcsr, zero
	-[0x80000db0]:sw t5, 1184(ra)
	-[0x80000db4]:sw t6, 1192(ra)
	-[0x80000db8]:sw t5, 1200(ra)
Current Store : [0x80000db8] : sw t5, 1200(ra) -- Store: [0x80009878]:0x00000004




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000055 and fs2 == 0 and fe2 == 0x3d1 and fm2 == 0x3400000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000de8]:fsub.d t5, t3, s10, dyn
	-[0x80000dec]:csrrs a7, fcsr, zero
	-[0x80000df0]:sw t5, 1216(ra)
	-[0x80000df4]:sw t6, 1224(ra)
Current Store : [0x80000df4] : sw t6, 1224(ra) -- Store: [0x80009890]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000055 and fs2 == 0 and fe2 == 0x3d1 and fm2 == 0x3400000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000de8]:fsub.d t5, t3, s10, dyn
	-[0x80000dec]:csrrs a7, fcsr, zero
	-[0x80000df0]:sw t5, 1216(ra)
	-[0x80000df4]:sw t6, 1224(ra)
	-[0x80000df8]:sw t5, 1232(ra)
Current Store : [0x80000df8] : sw t5, 1232(ra) -- Store: [0x80009898]:0x00000008




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000000e and fs2 == 1 and fe2 == 0x3cc and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e28]:fsub.d t5, t3, s10, dyn
	-[0x80000e2c]:csrrs a7, fcsr, zero
	-[0x80000e30]:sw t5, 1248(ra)
	-[0x80000e34]:sw t6, 1256(ra)
Current Store : [0x80000e34] : sw t6, 1256(ra) -- Store: [0x800098b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000000e and fs2 == 1 and fe2 == 0x3cc and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e28]:fsub.d t5, t3, s10, dyn
	-[0x80000e2c]:csrrs a7, fcsr, zero
	-[0x80000e30]:sw t5, 1248(ra)
	-[0x80000e34]:sw t6, 1256(ra)
	-[0x80000e38]:sw t5, 1264(ra)
Current Store : [0x80000e38] : sw t5, 1264(ra) -- Store: [0x800098b8]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000043 and fs2 == 0 and fe2 == 0x3d0 and fm2 == 0x1800000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e68]:fsub.d t5, t3, s10, dyn
	-[0x80000e6c]:csrrs a7, fcsr, zero
	-[0x80000e70]:sw t5, 1280(ra)
	-[0x80000e74]:sw t6, 1288(ra)
Current Store : [0x80000e74] : sw t6, 1288(ra) -- Store: [0x800098d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000043 and fs2 == 0 and fe2 == 0x3d0 and fm2 == 0x1800000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e68]:fsub.d t5, t3, s10, dyn
	-[0x80000e6c]:csrrs a7, fcsr, zero
	-[0x80000e70]:sw t5, 1280(ra)
	-[0x80000e74]:sw t6, 1288(ra)
	-[0x80000e78]:sw t5, 1296(ra)
Current Store : [0x80000e78] : sw t5, 1296(ra) -- Store: [0x800098d8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000030 and fs2 == 1 and fe2 == 0x3cf and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ea8]:fsub.d t5, t3, s10, dyn
	-[0x80000eac]:csrrs a7, fcsr, zero
	-[0x80000eb0]:sw t5, 1312(ra)
	-[0x80000eb4]:sw t6, 1320(ra)
Current Store : [0x80000eb4] : sw t6, 1320(ra) -- Store: [0x800098f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000030 and fs2 == 1 and fe2 == 0x3cf and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ea8]:fsub.d t5, t3, s10, dyn
	-[0x80000eac]:csrrs a7, fcsr, zero
	-[0x80000eb0]:sw t5, 1312(ra)
	-[0x80000eb4]:sw t6, 1320(ra)
	-[0x80000eb8]:sw t5, 1328(ra)
Current Store : [0x80000eb8] : sw t5, 1328(ra) -- Store: [0x800098f8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000005e and fs2 == 1 and fe2 == 0x3d0 and fm2 == 0x1000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ee8]:fsub.d t5, t3, s10, dyn
	-[0x80000eec]:csrrs a7, fcsr, zero
	-[0x80000ef0]:sw t5, 1344(ra)
	-[0x80000ef4]:sw t6, 1352(ra)
Current Store : [0x80000ef4] : sw t6, 1352(ra) -- Store: [0x80009910]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000005e and fs2 == 1 and fe2 == 0x3d0 and fm2 == 0x1000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ee8]:fsub.d t5, t3, s10, dyn
	-[0x80000eec]:csrrs a7, fcsr, zero
	-[0x80000ef0]:sw t5, 1344(ra)
	-[0x80000ef4]:sw t6, 1352(ra)
	-[0x80000ef8]:sw t5, 1360(ra)
Current Store : [0x80000ef8] : sw t5, 1360(ra) -- Store: [0x80009918]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000003d and fs2 == 1 and fe2 == 0x3d2 and fm2 == 0x8600000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f28]:fsub.d t5, t3, s10, dyn
	-[0x80000f2c]:csrrs a7, fcsr, zero
	-[0x80000f30]:sw t5, 1376(ra)
	-[0x80000f34]:sw t6, 1384(ra)
Current Store : [0x80000f34] : sw t6, 1384(ra) -- Store: [0x80009930]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000003d and fs2 == 1 and fe2 == 0x3d2 and fm2 == 0x8600000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f28]:fsub.d t5, t3, s10, dyn
	-[0x80000f2c]:csrrs a7, fcsr, zero
	-[0x80000f30]:sw t5, 1376(ra)
	-[0x80000f34]:sw t6, 1384(ra)
	-[0x80000f38]:sw t5, 1392(ra)
Current Store : [0x80000f38] : sw t5, 1392(ra) -- Store: [0x80009938]:0x00000100




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000028 and fs2 == 1 and fe2 == 0x3d3 and fm2 == 0xd800000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f68]:fsub.d t5, t3, s10, dyn
	-[0x80000f6c]:csrrs a7, fcsr, zero
	-[0x80000f70]:sw t5, 1408(ra)
	-[0x80000f74]:sw t6, 1416(ra)
Current Store : [0x80000f74] : sw t6, 1416(ra) -- Store: [0x80009950]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000028 and fs2 == 1 and fe2 == 0x3d3 and fm2 == 0xd800000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f68]:fsub.d t5, t3, s10, dyn
	-[0x80000f6c]:csrrs a7, fcsr, zero
	-[0x80000f70]:sw t5, 1408(ra)
	-[0x80000f74]:sw t6, 1416(ra)
	-[0x80000f78]:sw t5, 1424(ra)
Current Store : [0x80000f78] : sw t5, 1424(ra) -- Store: [0x80009958]:0x00000200




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000004f and fs2 == 1 and fe2 == 0x3d4 and fm2 == 0xd880000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fac]:fsub.d t5, t3, s10, dyn
	-[0x80000fb0]:csrrs a7, fcsr, zero
	-[0x80000fb4]:sw t5, 1440(ra)
	-[0x80000fb8]:sw t6, 1448(ra)
Current Store : [0x80000fb8] : sw t6, 1448(ra) -- Store: [0x80009970]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000004f and fs2 == 1 and fe2 == 0x3d4 and fm2 == 0xd880000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fac]:fsub.d t5, t3, s10, dyn
	-[0x80000fb0]:csrrs a7, fcsr, zero
	-[0x80000fb4]:sw t5, 1440(ra)
	-[0x80000fb8]:sw t6, 1448(ra)
	-[0x80000fbc]:sw t5, 1456(ra)
Current Store : [0x80000fbc] : sw t5, 1456(ra) -- Store: [0x80009978]:0x00000400




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000004b and fs2 == 1 and fe2 == 0x3d5 and fm2 == 0xed40000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ff0]:fsub.d t5, t3, s10, dyn
	-[0x80000ff4]:csrrs a7, fcsr, zero
	-[0x80000ff8]:sw t5, 1472(ra)
	-[0x80000ffc]:sw t6, 1480(ra)
Current Store : [0x80000ffc] : sw t6, 1480(ra) -- Store: [0x80009990]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000004b and fs2 == 1 and fe2 == 0x3d5 and fm2 == 0xed40000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ff0]:fsub.d t5, t3, s10, dyn
	-[0x80000ff4]:csrrs a7, fcsr, zero
	-[0x80000ff8]:sw t5, 1472(ra)
	-[0x80000ffc]:sw t6, 1480(ra)
	-[0x80001000]:sw t5, 1488(ra)
Current Store : [0x80001000] : sw t5, 1488(ra) -- Store: [0x80009998]:0x00000800




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000053 and fs2 == 1 and fe2 == 0x3d6 and fm2 == 0xf5a0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001034]:fsub.d t5, t3, s10, dyn
	-[0x80001038]:csrrs a7, fcsr, zero
	-[0x8000103c]:sw t5, 1504(ra)
	-[0x80001040]:sw t6, 1512(ra)
Current Store : [0x80001040] : sw t6, 1512(ra) -- Store: [0x800099b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000053 and fs2 == 1 and fe2 == 0x3d6 and fm2 == 0xf5a0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001034]:fsub.d t5, t3, s10, dyn
	-[0x80001038]:csrrs a7, fcsr, zero
	-[0x8000103c]:sw t5, 1504(ra)
	-[0x80001040]:sw t6, 1512(ra)
	-[0x80001044]:sw t5, 1520(ra)
Current Store : [0x80001044] : sw t5, 1520(ra) -- Store: [0x800099b8]:0x00001000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000016 and fs2 == 1 and fe2 == 0x3d7 and fm2 == 0xfea0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001078]:fsub.d t5, t3, s10, dyn
	-[0x8000107c]:csrrs a7, fcsr, zero
	-[0x80001080]:sw t5, 1536(ra)
	-[0x80001084]:sw t6, 1544(ra)
Current Store : [0x80001084] : sw t6, 1544(ra) -- Store: [0x800099d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000016 and fs2 == 1 and fe2 == 0x3d7 and fm2 == 0xfea0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001078]:fsub.d t5, t3, s10, dyn
	-[0x8000107c]:csrrs a7, fcsr, zero
	-[0x80001080]:sw t5, 1536(ra)
	-[0x80001084]:sw t6, 1544(ra)
	-[0x80001088]:sw t5, 1552(ra)
Current Store : [0x80001088] : sw t5, 1552(ra) -- Store: [0x800099d8]:0x00002000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000001e and fs2 == 1 and fe2 == 0x3d8 and fm2 == 0xff10000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010bc]:fsub.d t5, t3, s10, dyn
	-[0x800010c0]:csrrs a7, fcsr, zero
	-[0x800010c4]:sw t5, 1568(ra)
	-[0x800010c8]:sw t6, 1576(ra)
Current Store : [0x800010c8] : sw t6, 1576(ra) -- Store: [0x800099f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000001e and fs2 == 1 and fe2 == 0x3d8 and fm2 == 0xff10000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010bc]:fsub.d t5, t3, s10, dyn
	-[0x800010c0]:csrrs a7, fcsr, zero
	-[0x800010c4]:sw t5, 1568(ra)
	-[0x800010c8]:sw t6, 1576(ra)
	-[0x800010cc]:sw t5, 1584(ra)
Current Store : [0x800010cc] : sw t5, 1584(ra) -- Store: [0x800099f8]:0x00004000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000063 and fs2 == 1 and fe2 == 0x3d9 and fm2 == 0xfe74000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001100]:fsub.d t5, t3, s10, dyn
	-[0x80001104]:csrrs a7, fcsr, zero
	-[0x80001108]:sw t5, 1600(ra)
	-[0x8000110c]:sw t6, 1608(ra)
Current Store : [0x8000110c] : sw t6, 1608(ra) -- Store: [0x80009a10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000063 and fs2 == 1 and fe2 == 0x3d9 and fm2 == 0xfe74000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001100]:fsub.d t5, t3, s10, dyn
	-[0x80001104]:csrrs a7, fcsr, zero
	-[0x80001108]:sw t5, 1600(ra)
	-[0x8000110c]:sw t6, 1608(ra)
	-[0x80001110]:sw t5, 1616(ra)
Current Store : [0x80001110] : sw t5, 1616(ra) -- Store: [0x80009a18]:0x00008000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000046 and fs2 == 1 and fe2 == 0x3da and fm2 == 0xff74000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001144]:fsub.d t5, t3, s10, dyn
	-[0x80001148]:csrrs a7, fcsr, zero
	-[0x8000114c]:sw t5, 1632(ra)
	-[0x80001150]:sw t6, 1640(ra)
Current Store : [0x80001150] : sw t6, 1640(ra) -- Store: [0x80009a30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000046 and fs2 == 1 and fe2 == 0x3da and fm2 == 0xff74000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001144]:fsub.d t5, t3, s10, dyn
	-[0x80001148]:csrrs a7, fcsr, zero
	-[0x8000114c]:sw t5, 1632(ra)
	-[0x80001150]:sw t6, 1640(ra)
	-[0x80001154]:sw t5, 1648(ra)
Current Store : [0x80001154] : sw t5, 1648(ra) -- Store: [0x80009a38]:0x00010000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000001e and fs2 == 1 and fe2 == 0x3db and fm2 == 0xffe2000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001188]:fsub.d t5, t3, s10, dyn
	-[0x8000118c]:csrrs a7, fcsr, zero
	-[0x80001190]:sw t5, 1664(ra)
	-[0x80001194]:sw t6, 1672(ra)
Current Store : [0x80001194] : sw t6, 1672(ra) -- Store: [0x80009a50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000001e and fs2 == 1 and fe2 == 0x3db and fm2 == 0xffe2000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001188]:fsub.d t5, t3, s10, dyn
	-[0x8000118c]:csrrs a7, fcsr, zero
	-[0x80001190]:sw t5, 1664(ra)
	-[0x80001194]:sw t6, 1672(ra)
	-[0x80001198]:sw t5, 1680(ra)
Current Store : [0x80001198] : sw t5, 1680(ra) -- Store: [0x80009a58]:0x00020000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000042 and fs2 == 1 and fe2 == 0x3dc and fm2 == 0xffdf000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011cc]:fsub.d t5, t3, s10, dyn
	-[0x800011d0]:csrrs a7, fcsr, zero
	-[0x800011d4]:sw t5, 1696(ra)
	-[0x800011d8]:sw t6, 1704(ra)
Current Store : [0x800011d8] : sw t6, 1704(ra) -- Store: [0x80009a70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000042 and fs2 == 1 and fe2 == 0x3dc and fm2 == 0xffdf000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011cc]:fsub.d t5, t3, s10, dyn
	-[0x800011d0]:csrrs a7, fcsr, zero
	-[0x800011d4]:sw t5, 1696(ra)
	-[0x800011d8]:sw t6, 1704(ra)
	-[0x800011dc]:sw t5, 1712(ra)
Current Store : [0x800011dc] : sw t5, 1712(ra) -- Store: [0x80009a78]:0x00040000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000004a and fs2 == 1 and fe2 == 0x3dd and fm2 == 0xffed800000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001210]:fsub.d t5, t3, s10, dyn
	-[0x80001214]:csrrs a7, fcsr, zero
	-[0x80001218]:sw t5, 1728(ra)
	-[0x8000121c]:sw t6, 1736(ra)
Current Store : [0x8000121c] : sw t6, 1736(ra) -- Store: [0x80009a90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000004a and fs2 == 1 and fe2 == 0x3dd and fm2 == 0xffed800000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001210]:fsub.d t5, t3, s10, dyn
	-[0x80001214]:csrrs a7, fcsr, zero
	-[0x80001218]:sw t5, 1728(ra)
	-[0x8000121c]:sw t6, 1736(ra)
	-[0x80001220]:sw t5, 1744(ra)
Current Store : [0x80001220] : sw t5, 1744(ra) -- Store: [0x80009a98]:0x00080000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000003b and fs2 == 1 and fe2 == 0x3de and fm2 == 0xfff8a00000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001254]:fsub.d t5, t3, s10, dyn
	-[0x80001258]:csrrs a7, fcsr, zero
	-[0x8000125c]:sw t5, 1760(ra)
	-[0x80001260]:sw t6, 1768(ra)
Current Store : [0x80001260] : sw t6, 1768(ra) -- Store: [0x80009ab0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000003b and fs2 == 1 and fe2 == 0x3de and fm2 == 0xfff8a00000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001254]:fsub.d t5, t3, s10, dyn
	-[0x80001258]:csrrs a7, fcsr, zero
	-[0x8000125c]:sw t5, 1760(ra)
	-[0x80001260]:sw t6, 1768(ra)
	-[0x80001264]:sw t5, 1776(ra)
Current Store : [0x80001264] : sw t5, 1776(ra) -- Store: [0x80009ab8]:0x00100000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000055 and fs2 == 1 and fe2 == 0x3df and fm2 == 0xfffab00000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001298]:fsub.d t5, t3, s10, dyn
	-[0x8000129c]:csrrs a7, fcsr, zero
	-[0x800012a0]:sw t5, 1792(ra)
	-[0x800012a4]:sw t6, 1800(ra)
Current Store : [0x800012a4] : sw t6, 1800(ra) -- Store: [0x80009ad0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000055 and fs2 == 1 and fe2 == 0x3df and fm2 == 0xfffab00000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001298]:fsub.d t5, t3, s10, dyn
	-[0x8000129c]:csrrs a7, fcsr, zero
	-[0x800012a0]:sw t5, 1792(ra)
	-[0x800012a4]:sw t6, 1800(ra)
	-[0x800012a8]:sw t5, 1808(ra)
Current Store : [0x800012a8] : sw t5, 1808(ra) -- Store: [0x80009ad8]:0x00200000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000004e and fs2 == 1 and fe2 == 0x3e0 and fm2 == 0xfffd900000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012dc]:fsub.d t5, t3, s10, dyn
	-[0x800012e0]:csrrs a7, fcsr, zero
	-[0x800012e4]:sw t5, 1824(ra)
	-[0x800012e8]:sw t6, 1832(ra)
Current Store : [0x800012e8] : sw t6, 1832(ra) -- Store: [0x80009af0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000004e and fs2 == 1 and fe2 == 0x3e0 and fm2 == 0xfffd900000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012dc]:fsub.d t5, t3, s10, dyn
	-[0x800012e0]:csrrs a7, fcsr, zero
	-[0x800012e4]:sw t5, 1824(ra)
	-[0x800012e8]:sw t6, 1832(ra)
	-[0x800012ec]:sw t5, 1840(ra)
Current Store : [0x800012ec] : sw t5, 1840(ra) -- Store: [0x80009af8]:0x00400000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000131c]:fsub.d t5, t3, s10, dyn
	-[0x80001320]:csrrs a7, fcsr, zero
	-[0x80001324]:sw t5, 1856(ra)
	-[0x80001328]:sw t6, 1864(ra)
Current Store : [0x80001328] : sw t6, 1864(ra) -- Store: [0x80009b10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000131c]:fsub.d t5, t3, s10, dyn
	-[0x80001320]:csrrs a7, fcsr, zero
	-[0x80001324]:sw t5, 1856(ra)
	-[0x80001328]:sw t6, 1864(ra)
	-[0x8000132c]:sw t5, 1872(ra)
Current Store : [0x8000132c] : sw t5, 1872(ra) -- Store: [0x80009b18]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000005f and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000000030 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000135c]:fsub.d t5, t3, s10, dyn
	-[0x80001360]:csrrs a7, fcsr, zero
	-[0x80001364]:sw t5, 1888(ra)
	-[0x80001368]:sw t6, 1896(ra)
Current Store : [0x80001368] : sw t6, 1896(ra) -- Store: [0x80009b30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000005f and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000000030 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000135c]:fsub.d t5, t3, s10, dyn
	-[0x80001360]:csrrs a7, fcsr, zero
	-[0x80001364]:sw t5, 1888(ra)
	-[0x80001368]:sw t6, 1896(ra)
	-[0x8000136c]:sw t5, 1904(ra)
Current Store : [0x8000136c] : sw t5, 1904(ra) -- Store: [0x80009b38]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000011 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x000000000000a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000139c]:fsub.d t5, t3, s10, dyn
	-[0x800013a0]:csrrs a7, fcsr, zero
	-[0x800013a4]:sw t5, 1920(ra)
	-[0x800013a8]:sw t6, 1928(ra)
Current Store : [0x800013a8] : sw t6, 1928(ra) -- Store: [0x80009b50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000011 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x000000000000a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000139c]:fsub.d t5, t3, s10, dyn
	-[0x800013a0]:csrrs a7, fcsr, zero
	-[0x800013a4]:sw t5, 1920(ra)
	-[0x800013a8]:sw t6, 1928(ra)
	-[0x800013ac]:sw t5, 1936(ra)
Current Store : [0x800013ac] : sw t5, 1936(ra) -- Store: [0x80009b58]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000048 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000000028 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013dc]:fsub.d t5, t3, s10, dyn
	-[0x800013e0]:csrrs a7, fcsr, zero
	-[0x800013e4]:sw t5, 1952(ra)
	-[0x800013e8]:sw t6, 1960(ra)
Current Store : [0x800013e8] : sw t6, 1960(ra) -- Store: [0x80009b70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000048 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000000028 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013dc]:fsub.d t5, t3, s10, dyn
	-[0x800013e0]:csrrs a7, fcsr, zero
	-[0x800013e4]:sw t5, 1952(ra)
	-[0x800013e8]:sw t6, 1960(ra)
	-[0x800013ec]:sw t5, 1968(ra)
Current Store : [0x800013ec] : sw t5, 1968(ra) -- Store: [0x80009b78]:0x00000008




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000037 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000000024 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000141c]:fsub.d t5, t3, s10, dyn
	-[0x80001420]:csrrs a7, fcsr, zero
	-[0x80001424]:sw t5, 1984(ra)
	-[0x80001428]:sw t6, 1992(ra)
Current Store : [0x80001428] : sw t6, 1992(ra) -- Store: [0x80009b90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000037 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000000024 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000141c]:fsub.d t5, t3, s10, dyn
	-[0x80001420]:csrrs a7, fcsr, zero
	-[0x80001424]:sw t5, 1984(ra)
	-[0x80001428]:sw t6, 1992(ra)
	-[0x8000142c]:sw t5, 2000(ra)
Current Store : [0x8000142c] : sw t5, 2000(ra) -- Store: [0x80009b98]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000003e and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x000000000002f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000145c]:fsub.d t5, t3, s10, dyn
	-[0x80001460]:csrrs a7, fcsr, zero
	-[0x80001464]:sw t5, 2016(ra)
	-[0x80001468]:sw t6, 2024(ra)
Current Store : [0x80001468] : sw t6, 2024(ra) -- Store: [0x80009bb0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000003e and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x000000000002f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000145c]:fsub.d t5, t3, s10, dyn
	-[0x80001460]:csrrs a7, fcsr, zero
	-[0x80001464]:sw t5, 2016(ra)
	-[0x80001468]:sw t6, 2024(ra)
	-[0x8000146c]:sw t5, 2032(ra)
Current Store : [0x8000146c] : sw t5, 2032(ra) -- Store: [0x80009bb8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000049 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000000044 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000149c]:fsub.d t5, t3, s10, dyn
	-[0x800014a0]:csrrs a7, fcsr, zero
	-[0x800014a4]:addi ra, ra, 2040
	-[0x800014a8]:sw t5, 8(ra)
	-[0x800014ac]:sw t6, 16(ra)
Current Store : [0x800014ac] : sw t6, 16(ra) -- Store: [0x80009bd0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000049 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000000044 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000149c]:fsub.d t5, t3, s10, dyn
	-[0x800014a0]:csrrs a7, fcsr, zero
	-[0x800014a4]:addi ra, ra, 2040
	-[0x800014a8]:sw t5, 8(ra)
	-[0x800014ac]:sw t6, 16(ra)
	-[0x800014b0]:sw t5, 24(ra)
Current Store : [0x800014b0] : sw t5, 24(ra) -- Store: [0x80009bd8]:0x0000003F




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000001a and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x000000000004d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014e0]:fsub.d t5, t3, s10, dyn
	-[0x800014e4]:csrrs a7, fcsr, zero
	-[0x800014e8]:sw t5, 40(ra)
	-[0x800014ec]:sw t6, 48(ra)
Current Store : [0x800014ec] : sw t6, 48(ra) -- Store: [0x80009bf0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000001a and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x000000000004d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014e0]:fsub.d t5, t3, s10, dyn
	-[0x800014e4]:csrrs a7, fcsr, zero
	-[0x800014e8]:sw t5, 40(ra)
	-[0x800014ec]:sw t6, 48(ra)
	-[0x800014f0]:sw t5, 56(ra)
Current Store : [0x800014f0] : sw t5, 56(ra) -- Store: [0x80009bf8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000035 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x000000000009a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001520]:fsub.d t5, t3, s10, dyn
	-[0x80001524]:csrrs a7, fcsr, zero
	-[0x80001528]:sw t5, 72(ra)
	-[0x8000152c]:sw t6, 80(ra)
Current Store : [0x8000152c] : sw t6, 80(ra) -- Store: [0x80009c10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000035 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x000000000009a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001520]:fsub.d t5, t3, s10, dyn
	-[0x80001524]:csrrs a7, fcsr, zero
	-[0x80001528]:sw t5, 72(ra)
	-[0x8000152c]:sw t6, 80(ra)
	-[0x80001530]:sw t5, 88(ra)
Current Store : [0x80001530] : sw t5, 88(ra) -- Store: [0x80009c18]:0x000000FF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000002e and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000000117 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001560]:fsub.d t5, t3, s10, dyn
	-[0x80001564]:csrrs a7, fcsr, zero
	-[0x80001568]:sw t5, 104(ra)
	-[0x8000156c]:sw t6, 112(ra)
Current Store : [0x8000156c] : sw t6, 112(ra) -- Store: [0x80009c30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000002e and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000000117 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001560]:fsub.d t5, t3, s10, dyn
	-[0x80001564]:csrrs a7, fcsr, zero
	-[0x80001568]:sw t5, 104(ra)
	-[0x8000156c]:sw t6, 112(ra)
	-[0x80001570]:sw t5, 120(ra)
Current Store : [0x80001570] : sw t5, 120(ra) -- Store: [0x80009c38]:0x00000200




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000002d and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000000216 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015a0]:fsub.d t5, t3, s10, dyn
	-[0x800015a4]:csrrs a7, fcsr, zero
	-[0x800015a8]:sw t5, 136(ra)
	-[0x800015ac]:sw t6, 144(ra)
Current Store : [0x800015ac] : sw t6, 144(ra) -- Store: [0x80009c50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000002d and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000000216 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015a0]:fsub.d t5, t3, s10, dyn
	-[0x800015a4]:csrrs a7, fcsr, zero
	-[0x800015a8]:sw t5, 136(ra)
	-[0x800015ac]:sw t6, 144(ra)
	-[0x800015b0]:sw t5, 152(ra)
Current Store : [0x800015b0] : sw t5, 152(ra) -- Store: [0x80009c58]:0x000003FF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000045 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000000422 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015e0]:fsub.d t5, t3, s10, dyn
	-[0x800015e4]:csrrs a7, fcsr, zero
	-[0x800015e8]:sw t5, 168(ra)
	-[0x800015ec]:sw t6, 176(ra)
Current Store : [0x800015ec] : sw t6, 176(ra) -- Store: [0x80009c70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000045 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000000422 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015e0]:fsub.d t5, t3, s10, dyn
	-[0x800015e4]:csrrs a7, fcsr, zero
	-[0x800015e8]:sw t5, 168(ra)
	-[0x800015ec]:sw t6, 176(ra)
	-[0x800015f0]:sw t5, 184(ra)
Current Store : [0x800015f0] : sw t5, 184(ra) -- Store: [0x80009c78]:0x000007FF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000050 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000000828 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001624]:fsub.d t5, t3, s10, dyn
	-[0x80001628]:csrrs a7, fcsr, zero
	-[0x8000162c]:sw t5, 200(ra)
	-[0x80001630]:sw t6, 208(ra)
Current Store : [0x80001630] : sw t6, 208(ra) -- Store: [0x80009c90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000050 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000000828 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001624]:fsub.d t5, t3, s10, dyn
	-[0x80001628]:csrrs a7, fcsr, zero
	-[0x8000162c]:sw t5, 200(ra)
	-[0x80001630]:sw t6, 208(ra)
	-[0x80001634]:sw t5, 216(ra)
Current Store : [0x80001634] : sw t5, 216(ra) -- Store: [0x80009c98]:0x00001000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000002b and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000001016 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001668]:fsub.d t5, t3, s10, dyn
	-[0x8000166c]:csrrs a7, fcsr, zero
	-[0x80001670]:sw t5, 232(ra)
	-[0x80001674]:sw t6, 240(ra)
Current Store : [0x80001674] : sw t6, 240(ra) -- Store: [0x80009cb0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000002b and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000001016 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001668]:fsub.d t5, t3, s10, dyn
	-[0x8000166c]:csrrs a7, fcsr, zero
	-[0x80001670]:sw t5, 232(ra)
	-[0x80001674]:sw t6, 240(ra)
	-[0x80001678]:sw t5, 248(ra)
Current Store : [0x80001678] : sw t5, 248(ra) -- Store: [0x80009cb8]:0x00002001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000004d and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000002026 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016ac]:fsub.d t5, t3, s10, dyn
	-[0x800016b0]:csrrs a7, fcsr, zero
	-[0x800016b4]:sw t5, 264(ra)
	-[0x800016b8]:sw t6, 272(ra)
Current Store : [0x800016b8] : sw t6, 272(ra) -- Store: [0x80009cd0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000004d and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000002026 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016ac]:fsub.d t5, t3, s10, dyn
	-[0x800016b0]:csrrs a7, fcsr, zero
	-[0x800016b4]:sw t5, 264(ra)
	-[0x800016b8]:sw t6, 272(ra)
	-[0x800016bc]:sw t5, 280(ra)
Current Store : [0x800016bc] : sw t5, 280(ra) -- Store: [0x80009cd8]:0x00003FFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000001e and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x000000000400f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016f0]:fsub.d t5, t3, s10, dyn
	-[0x800016f4]:csrrs a7, fcsr, zero
	-[0x800016f8]:sw t5, 296(ra)
	-[0x800016fc]:sw t6, 304(ra)
Current Store : [0x800016fc] : sw t6, 304(ra) -- Store: [0x80009cf0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000001e and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x000000000400f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016f0]:fsub.d t5, t3, s10, dyn
	-[0x800016f4]:csrrs a7, fcsr, zero
	-[0x800016f8]:sw t5, 296(ra)
	-[0x800016fc]:sw t6, 304(ra)
	-[0x80001700]:sw t5, 312(ra)
Current Store : [0x80001700] : sw t5, 312(ra) -- Store: [0x80009cf8]:0x00008000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000017 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x000000000800c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001734]:fsub.d t5, t3, s10, dyn
	-[0x80001738]:csrrs a7, fcsr, zero
	-[0x8000173c]:sw t5, 328(ra)
	-[0x80001740]:sw t6, 336(ra)
Current Store : [0x80001740] : sw t6, 336(ra) -- Store: [0x80009d10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000017 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x000000000800c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001734]:fsub.d t5, t3, s10, dyn
	-[0x80001738]:csrrs a7, fcsr, zero
	-[0x8000173c]:sw t5, 328(ra)
	-[0x80001740]:sw t6, 336(ra)
	-[0x80001744]:sw t5, 344(ra)
Current Store : [0x80001744] : sw t5, 344(ra) -- Store: [0x80009d18]:0x00010001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000004b and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000010026 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001778]:fsub.d t5, t3, s10, dyn
	-[0x8000177c]:csrrs a7, fcsr, zero
	-[0x80001780]:sw t5, 360(ra)
	-[0x80001784]:sw t6, 368(ra)
Current Store : [0x80001784] : sw t6, 368(ra) -- Store: [0x80009d30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000004b and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000010026 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001778]:fsub.d t5, t3, s10, dyn
	-[0x8000177c]:csrrs a7, fcsr, zero
	-[0x80001780]:sw t5, 360(ra)
	-[0x80001784]:sw t6, 368(ra)
	-[0x80001788]:sw t5, 376(ra)
Current Store : [0x80001788] : sw t5, 376(ra) -- Store: [0x80009d38]:0x00020001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000000c and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000020006 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017bc]:fsub.d t5, t3, s10, dyn
	-[0x800017c0]:csrrs a7, fcsr, zero
	-[0x800017c4]:sw t5, 392(ra)
	-[0x800017c8]:sw t6, 400(ra)
Current Store : [0x800017c8] : sw t6, 400(ra) -- Store: [0x80009d50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000000c and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000020006 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017bc]:fsub.d t5, t3, s10, dyn
	-[0x800017c0]:csrrs a7, fcsr, zero
	-[0x800017c4]:sw t5, 392(ra)
	-[0x800017c8]:sw t6, 400(ra)
	-[0x800017cc]:sw t5, 408(ra)
Current Store : [0x800017cc] : sw t5, 408(ra) -- Store: [0x80009d58]:0x00040000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000021 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000040010 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001800]:fsub.d t5, t3, s10, dyn
	-[0x80001804]:csrrs a7, fcsr, zero
	-[0x80001808]:sw t5, 424(ra)
	-[0x8000180c]:sw t6, 432(ra)
Current Store : [0x8000180c] : sw t6, 432(ra) -- Store: [0x80009d70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000021 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000040010 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001800]:fsub.d t5, t3, s10, dyn
	-[0x80001804]:csrrs a7, fcsr, zero
	-[0x80001808]:sw t5, 424(ra)
	-[0x8000180c]:sw t6, 432(ra)
	-[0x80001810]:sw t5, 440(ra)
Current Store : [0x80001810] : sw t5, 440(ra) -- Store: [0x80009d78]:0x0007FFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000057 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x000000008002c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001844]:fsub.d t5, t3, s10, dyn
	-[0x80001848]:csrrs a7, fcsr, zero
	-[0x8000184c]:sw t5, 456(ra)
	-[0x80001850]:sw t6, 464(ra)
Current Store : [0x80001850] : sw t6, 464(ra) -- Store: [0x80009d90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000057 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x000000008002c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001844]:fsub.d t5, t3, s10, dyn
	-[0x80001848]:csrrs a7, fcsr, zero
	-[0x8000184c]:sw t5, 456(ra)
	-[0x80001850]:sw t6, 464(ra)
	-[0x80001854]:sw t5, 472(ra)
Current Store : [0x80001854] : sw t5, 472(ra) -- Store: [0x80009d98]:0x00100001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000000b and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000100006 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001888]:fsub.d t5, t3, s10, dyn
	-[0x8000188c]:csrrs a7, fcsr, zero
	-[0x80001890]:sw t5, 488(ra)
	-[0x80001894]:sw t6, 496(ra)
Current Store : [0x80001894] : sw t6, 496(ra) -- Store: [0x80009db0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000000b and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000100006 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001888]:fsub.d t5, t3, s10, dyn
	-[0x8000188c]:csrrs a7, fcsr, zero
	-[0x80001890]:sw t5, 488(ra)
	-[0x80001894]:sw t6, 496(ra)
	-[0x80001898]:sw t5, 504(ra)
Current Store : [0x80001898] : sw t5, 504(ra) -- Store: [0x80009db8]:0x00200001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000003a and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x000000020001d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018cc]:fsub.d t5, t3, s10, dyn
	-[0x800018d0]:csrrs a7, fcsr, zero
	-[0x800018d4]:sw t5, 520(ra)
	-[0x800018d8]:sw t6, 528(ra)
Current Store : [0x800018d8] : sw t6, 528(ra) -- Store: [0x80009dd0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000003a and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x000000020001d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018cc]:fsub.d t5, t3, s10, dyn
	-[0x800018d0]:csrrs a7, fcsr, zero
	-[0x800018d4]:sw t5, 520(ra)
	-[0x800018d8]:sw t6, 528(ra)
	-[0x800018dc]:sw t5, 536(ra)
Current Store : [0x800018dc] : sw t5, 536(ra) -- Store: [0x80009dd8]:0x00400000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000061 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000061 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000190c]:fsub.d t5, t3, s10, dyn
	-[0x80001910]:csrrs a7, fcsr, zero
	-[0x80001914]:sw t5, 552(ra)
	-[0x80001918]:sw t6, 560(ra)
Current Store : [0x80001918] : sw t6, 560(ra) -- Store: [0x80009df0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000061 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000061 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000190c]:fsub.d t5, t3, s10, dyn
	-[0x80001910]:csrrs a7, fcsr, zero
	-[0x80001914]:sw t5, 552(ra)
	-[0x80001918]:sw t6, 560(ra)
	-[0x8000191c]:sw t5, 568(ra)
Current Store : [0x8000191c] : sw t5, 568(ra) -- Store: [0x80009df8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000024 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000021 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000194c]:fsub.d t5, t3, s10, dyn
	-[0x80001950]:csrrs a7, fcsr, zero
	-[0x80001954]:sw t5, 584(ra)
	-[0x80001958]:sw t6, 592(ra)
Current Store : [0x80001958] : sw t6, 592(ra) -- Store: [0x80009e10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000024 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000021 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000194c]:fsub.d t5, t3, s10, dyn
	-[0x80001950]:csrrs a7, fcsr, zero
	-[0x80001954]:sw t5, 584(ra)
	-[0x80001958]:sw t6, 592(ra)
	-[0x8000195c]:sw t5, 600(ra)
Current Store : [0x8000195c] : sw t5, 600(ra) -- Store: [0x80009e18]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000023 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000000001e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000198c]:fsub.d t5, t3, s10, dyn
	-[0x80001990]:csrrs a7, fcsr, zero
	-[0x80001994]:sw t5, 616(ra)
	-[0x80001998]:sw t6, 624(ra)
Current Store : [0x80001998] : sw t6, 624(ra) -- Store: [0x80009e30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000023 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000000001e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000198c]:fsub.d t5, t3, s10, dyn
	-[0x80001990]:csrrs a7, fcsr, zero
	-[0x80001994]:sw t5, 616(ra)
	-[0x80001998]:sw t6, 624(ra)
	-[0x8000199c]:sw t5, 632(ra)
Current Store : [0x8000199c] : sw t5, 632(ra) -- Store: [0x80009e38]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000050 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000047 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800019cc]:fsub.d t5, t3, s10, dyn
	-[0x800019d0]:csrrs a7, fcsr, zero
	-[0x800019d4]:sw t5, 648(ra)
	-[0x800019d8]:sw t6, 656(ra)
Current Store : [0x800019d8] : sw t6, 656(ra) -- Store: [0x80009e50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000050 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000047 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800019cc]:fsub.d t5, t3, s10, dyn
	-[0x800019d0]:csrrs a7, fcsr, zero
	-[0x800019d4]:sw t5, 648(ra)
	-[0x800019d8]:sw t6, 656(ra)
	-[0x800019dc]:sw t5, 664(ra)
Current Store : [0x800019dc] : sw t5, 664(ra) -- Store: [0x80009e58]:0x00000009




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000002d and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000000001c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a0c]:fsub.d t5, t3, s10, dyn
	-[0x80001a10]:csrrs a7, fcsr, zero
	-[0x80001a14]:sw t5, 680(ra)
	-[0x80001a18]:sw t6, 688(ra)
Current Store : [0x80001a18] : sw t6, 688(ra) -- Store: [0x80009e70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000002d and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000000001c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a0c]:fsub.d t5, t3, s10, dyn
	-[0x80001a10]:csrrs a7, fcsr, zero
	-[0x80001a14]:sw t5, 680(ra)
	-[0x80001a18]:sw t6, 688(ra)
	-[0x80001a1c]:sw t5, 696(ra)
Current Store : [0x80001a1c] : sw t5, 696(ra) -- Store: [0x80009e78]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000009 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000018 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a4c]:fsub.d t5, t3, s10, dyn
	-[0x80001a50]:csrrs a7, fcsr, zero
	-[0x80001a54]:sw t5, 712(ra)
	-[0x80001a58]:sw t6, 720(ra)
Current Store : [0x80001a58] : sw t6, 720(ra) -- Store: [0x80009e90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000009 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000018 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a4c]:fsub.d t5, t3, s10, dyn
	-[0x80001a50]:csrrs a7, fcsr, zero
	-[0x80001a54]:sw t5, 712(ra)
	-[0x80001a58]:sw t6, 720(ra)
	-[0x80001a5c]:sw t5, 728(ra)
Current Store : [0x80001a5c] : sw t5, 728(ra) -- Store: [0x80009e98]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000015 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000000002c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a8c]:fsub.d t5, t3, s10, dyn
	-[0x80001a90]:csrrs a7, fcsr, zero
	-[0x80001a94]:sw t5, 744(ra)
	-[0x80001a98]:sw t6, 752(ra)
Current Store : [0x80001a98] : sw t6, 752(ra) -- Store: [0x80009eb0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000015 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000000002c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a8c]:fsub.d t5, t3, s10, dyn
	-[0x80001a90]:csrrs a7, fcsr, zero
	-[0x80001a94]:sw t5, 744(ra)
	-[0x80001a98]:sw t6, 752(ra)
	-[0x80001a9c]:sw t5, 760(ra)
Current Store : [0x80001a9c] : sw t5, 760(ra) -- Store: [0x80009eb8]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000044 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000000003d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001acc]:fsub.d t5, t3, s10, dyn
	-[0x80001ad0]:csrrs a7, fcsr, zero
	-[0x80001ad4]:sw t5, 776(ra)
	-[0x80001ad8]:sw t6, 784(ra)
Current Store : [0x80001ad8] : sw t6, 784(ra) -- Store: [0x80009ed0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000044 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000000003d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001acc]:fsub.d t5, t3, s10, dyn
	-[0x80001ad0]:csrrs a7, fcsr, zero
	-[0x80001ad4]:sw t5, 776(ra)
	-[0x80001ad8]:sw t6, 784(ra)
	-[0x80001adc]:sw t5, 792(ra)
Current Store : [0x80001adc] : sw t5, 792(ra) -- Store: [0x80009ed8]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000055 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000000000ac and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b0c]:fsub.d t5, t3, s10, dyn
	-[0x80001b10]:csrrs a7, fcsr, zero
	-[0x80001b14]:sw t5, 808(ra)
	-[0x80001b18]:sw t6, 816(ra)
Current Store : [0x80001b18] : sw t6, 816(ra) -- Store: [0x80009ef0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000055 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000000000ac and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b0c]:fsub.d t5, t3, s10, dyn
	-[0x80001b10]:csrrs a7, fcsr, zero
	-[0x80001b14]:sw t5, 808(ra)
	-[0x80001b18]:sw t6, 816(ra)
	-[0x80001b1c]:sw t5, 824(ra)
Current Store : [0x80001b1c] : sw t5, 824(ra) -- Store: [0x80009ef8]:0x00000101




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000053 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000000001ae and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b4c]:fsub.d t5, t3, s10, dyn
	-[0x80001b50]:csrrs a7, fcsr, zero
	-[0x80001b54]:sw t5, 840(ra)
	-[0x80001b58]:sw t6, 848(ra)
Current Store : [0x80001b58] : sw t6, 848(ra) -- Store: [0x80009f10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000053 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000000001ae and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b4c]:fsub.d t5, t3, s10, dyn
	-[0x80001b50]:csrrs a7, fcsr, zero
	-[0x80001b54]:sw t5, 840(ra)
	-[0x80001b58]:sw t6, 848(ra)
	-[0x80001b5c]:sw t5, 856(ra)
Current Store : [0x80001b5c] : sw t5, 856(ra) -- Store: [0x80009f18]:0x00000201




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000026 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000000003db and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b8c]:fsub.d t5, t3, s10, dyn
	-[0x80001b90]:csrrs a7, fcsr, zero
	-[0x80001b94]:sw t5, 872(ra)
	-[0x80001b98]:sw t6, 880(ra)
Current Store : [0x80001b98] : sw t6, 880(ra) -- Store: [0x80009f30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000026 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000000003db and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b8c]:fsub.d t5, t3, s10, dyn
	-[0x80001b90]:csrrs a7, fcsr, zero
	-[0x80001b94]:sw t5, 872(ra)
	-[0x80001b98]:sw t6, 880(ra)
	-[0x80001b9c]:sw t5, 888(ra)
Current Store : [0x80001b9c] : sw t5, 888(ra) -- Store: [0x80009f38]:0x00000401




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005a and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000000007a7 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001bcc]:fsub.d t5, t3, s10, dyn
	-[0x80001bd0]:csrrs a7, fcsr, zero
	-[0x80001bd4]:sw t5, 904(ra)
	-[0x80001bd8]:sw t6, 912(ra)
Current Store : [0x80001bd8] : sw t6, 912(ra) -- Store: [0x80009f50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005a and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000000007a7 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001bcc]:fsub.d t5, t3, s10, dyn
	-[0x80001bd0]:csrrs a7, fcsr, zero
	-[0x80001bd4]:sw t5, 904(ra)
	-[0x80001bd8]:sw t6, 912(ra)
	-[0x80001bdc]:sw t5, 920(ra)
Current Store : [0x80001bdc] : sw t5, 920(ra) -- Store: [0x80009f58]:0x00000801




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000040 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000fc1 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c10]:fsub.d t5, t3, s10, dyn
	-[0x80001c14]:csrrs a7, fcsr, zero
	-[0x80001c18]:sw t5, 936(ra)
	-[0x80001c1c]:sw t6, 944(ra)
Current Store : [0x80001c1c] : sw t6, 944(ra) -- Store: [0x80009f70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000040 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000fc1 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c10]:fsub.d t5, t3, s10, dyn
	-[0x80001c14]:csrrs a7, fcsr, zero
	-[0x80001c18]:sw t5, 936(ra)
	-[0x80001c1c]:sw t6, 944(ra)
	-[0x80001c20]:sw t5, 952(ra)
Current Store : [0x80001c20] : sw t5, 952(ra) -- Store: [0x80009f78]:0x00001001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000f and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000001ff2 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c54]:fsub.d t5, t3, s10, dyn
	-[0x80001c58]:csrrs a7, fcsr, zero
	-[0x80001c5c]:sw t5, 968(ra)
	-[0x80001c60]:sw t6, 976(ra)
Current Store : [0x80001c60] : sw t6, 976(ra) -- Store: [0x80009f90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000f and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000001ff2 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c54]:fsub.d t5, t3, s10, dyn
	-[0x80001c58]:csrrs a7, fcsr, zero
	-[0x80001c5c]:sw t5, 968(ra)
	-[0x80001c60]:sw t6, 976(ra)
	-[0x80001c64]:sw t5, 984(ra)
Current Store : [0x80001c64] : sw t5, 984(ra) -- Store: [0x80009f98]:0x00002001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000028 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000003fd9 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c98]:fsub.d t5, t3, s10, dyn
	-[0x80001c9c]:csrrs a7, fcsr, zero
	-[0x80001ca0]:sw t5, 1000(ra)
	-[0x80001ca4]:sw t6, 1008(ra)
Current Store : [0x80001ca4] : sw t6, 1008(ra) -- Store: [0x80009fb0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000028 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000003fd9 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c98]:fsub.d t5, t3, s10, dyn
	-[0x80001c9c]:csrrs a7, fcsr, zero
	-[0x80001ca0]:sw t5, 1000(ra)
	-[0x80001ca4]:sw t6, 1008(ra)
	-[0x80001ca8]:sw t5, 1016(ra)
Current Store : [0x80001ca8] : sw t5, 1016(ra) -- Store: [0x80009fb8]:0x00004001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000002c and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000007fd5 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001cdc]:fsub.d t5, t3, s10, dyn
	-[0x80001ce0]:csrrs a7, fcsr, zero
	-[0x80001ce4]:sw t5, 1032(ra)
	-[0x80001ce8]:sw t6, 1040(ra)
Current Store : [0x80001ce8] : sw t6, 1040(ra) -- Store: [0x80009fd0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000002c and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000007fd5 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001cdc]:fsub.d t5, t3, s10, dyn
	-[0x80001ce0]:csrrs a7, fcsr, zero
	-[0x80001ce4]:sw t5, 1032(ra)
	-[0x80001ce8]:sw t6, 1040(ra)
	-[0x80001cec]:sw t5, 1048(ra)
Current Store : [0x80001cec] : sw t5, 1048(ra) -- Store: [0x80009fd8]:0x00008001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000019 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000000ffe8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d20]:fsub.d t5, t3, s10, dyn
	-[0x80001d24]:csrrs a7, fcsr, zero
	-[0x80001d28]:sw t5, 1064(ra)
	-[0x80001d2c]:sw t6, 1072(ra)
Current Store : [0x80001d2c] : sw t6, 1072(ra) -- Store: [0x80009ff0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000019 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000000ffe8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d20]:fsub.d t5, t3, s10, dyn
	-[0x80001d24]:csrrs a7, fcsr, zero
	-[0x80001d28]:sw t5, 1064(ra)
	-[0x80001d2c]:sw t6, 1072(ra)
	-[0x80001d30]:sw t5, 1080(ra)
Current Store : [0x80001d30] : sw t5, 1080(ra) -- Store: [0x80009ff8]:0x00010001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000e and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000001fff3 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d64]:fsub.d t5, t3, s10, dyn
	-[0x80001d68]:csrrs a7, fcsr, zero
	-[0x80001d6c]:sw t5, 1096(ra)
	-[0x80001d70]:sw t6, 1104(ra)
Current Store : [0x80001d70] : sw t6, 1104(ra) -- Store: [0x8000a010]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000e and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000001fff3 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d64]:fsub.d t5, t3, s10, dyn
	-[0x80001d68]:csrrs a7, fcsr, zero
	-[0x80001d6c]:sw t5, 1096(ra)
	-[0x80001d70]:sw t6, 1104(ra)
	-[0x80001d74]:sw t5, 1112(ra)
Current Store : [0x80001d74] : sw t5, 1112(ra) -- Store: [0x8000a018]:0x00020001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005e and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000003ffa3 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001da8]:fsub.d t5, t3, s10, dyn
	-[0x80001dac]:csrrs a7, fcsr, zero
	-[0x80001db0]:sw t5, 1128(ra)
	-[0x80001db4]:sw t6, 1136(ra)
Current Store : [0x80001db4] : sw t6, 1136(ra) -- Store: [0x8000a030]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005e and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000003ffa3 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001da8]:fsub.d t5, t3, s10, dyn
	-[0x80001dac]:csrrs a7, fcsr, zero
	-[0x80001db0]:sw t5, 1128(ra)
	-[0x80001db4]:sw t6, 1136(ra)
	-[0x80001db8]:sw t5, 1144(ra)
Current Store : [0x80001db8] : sw t5, 1144(ra) -- Store: [0x8000a038]:0x00040001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001b and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000007ffe6 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001dec]:fsub.d t5, t3, s10, dyn
	-[0x80001df0]:csrrs a7, fcsr, zero
	-[0x80001df4]:sw t5, 1160(ra)
	-[0x80001df8]:sw t6, 1168(ra)
Current Store : [0x80001df8] : sw t6, 1168(ra) -- Store: [0x8000a050]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001b and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000007ffe6 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001dec]:fsub.d t5, t3, s10, dyn
	-[0x80001df0]:csrrs a7, fcsr, zero
	-[0x80001df4]:sw t5, 1160(ra)
	-[0x80001df8]:sw t6, 1168(ra)
	-[0x80001dfc]:sw t5, 1176(ra)
Current Store : [0x80001dfc] : sw t5, 1176(ra) -- Store: [0x8000a058]:0x00080001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000038 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000000fffc9 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e30]:fsub.d t5, t3, s10, dyn
	-[0x80001e34]:csrrs a7, fcsr, zero
	-[0x80001e38]:sw t5, 1192(ra)
	-[0x80001e3c]:sw t6, 1200(ra)
Current Store : [0x80001e3c] : sw t6, 1200(ra) -- Store: [0x8000a070]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000038 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000000fffc9 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e30]:fsub.d t5, t3, s10, dyn
	-[0x80001e34]:csrrs a7, fcsr, zero
	-[0x80001e38]:sw t5, 1192(ra)
	-[0x80001e3c]:sw t6, 1200(ra)
	-[0x80001e40]:sw t5, 1208(ra)
Current Store : [0x80001e40] : sw t5, 1208(ra) -- Store: [0x8000a078]:0x00100001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001d and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000001fffe4 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e74]:fsub.d t5, t3, s10, dyn
	-[0x80001e78]:csrrs a7, fcsr, zero
	-[0x80001e7c]:sw t5, 1224(ra)
	-[0x80001e80]:sw t6, 1232(ra)
Current Store : [0x80001e80] : sw t6, 1232(ra) -- Store: [0x8000a090]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001d and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000001fffe4 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e74]:fsub.d t5, t3, s10, dyn
	-[0x80001e78]:csrrs a7, fcsr, zero
	-[0x80001e7c]:sw t5, 1224(ra)
	-[0x80001e80]:sw t6, 1232(ra)
	-[0x80001e84]:sw t5, 1240(ra)
Current Store : [0x80001e84] : sw t5, 1240(ra) -- Store: [0x8000a098]:0x00200001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000033 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000003fffce and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001eb8]:fsub.d t5, t3, s10, dyn
	-[0x80001ebc]:csrrs a7, fcsr, zero
	-[0x80001ec0]:sw t5, 1256(ra)
	-[0x80001ec4]:sw t6, 1264(ra)
Current Store : [0x80001ec4] : sw t6, 1264(ra) -- Store: [0x8000a0b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000033 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000003fffce and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001eb8]:fsub.d t5, t3, s10, dyn
	-[0x80001ebc]:csrrs a7, fcsr, zero
	-[0x80001ec0]:sw t5, 1256(ra)
	-[0x80001ec4]:sw t6, 1264(ra)
	-[0x80001ec8]:sw t5, 1272(ra)
Current Store : [0x80001ec8] : sw t5, 1272(ra) -- Store: [0x8000a0b8]:0x00400001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000005 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000005 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ef8]:fsub.d t5, t3, s10, dyn
	-[0x80001efc]:csrrs a7, fcsr, zero
	-[0x80001f00]:sw t5, 1288(ra)
	-[0x80001f04]:sw t6, 1296(ra)
Current Store : [0x80001f04] : sw t6, 1296(ra) -- Store: [0x8000a0d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000005 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000005 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ef8]:fsub.d t5, t3, s10, dyn
	-[0x80001efc]:csrrs a7, fcsr, zero
	-[0x80001f00]:sw t5, 1288(ra)
	-[0x80001f04]:sw t6, 1296(ra)
	-[0x80001f08]:sw t5, 1304(ra)
Current Store : [0x80001f08] : sw t5, 1304(ra) -- Store: [0x8000a0d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000015 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000018 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f38]:fsub.d t5, t3, s10, dyn
	-[0x80001f3c]:csrrs a7, fcsr, zero
	-[0x80001f40]:sw t5, 1320(ra)
	-[0x80001f44]:sw t6, 1328(ra)
Current Store : [0x80001f44] : sw t6, 1328(ra) -- Store: [0x8000a0f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000015 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000018 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f38]:fsub.d t5, t3, s10, dyn
	-[0x80001f3c]:csrrs a7, fcsr, zero
	-[0x80001f40]:sw t5, 1320(ra)
	-[0x80001f44]:sw t6, 1328(ra)
	-[0x80001f48]:sw t5, 1336(ra)
Current Store : [0x80001f48] : sw t5, 1336(ra) -- Store: [0x8000a0f8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005b and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000060 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f78]:fsub.d t5, t3, s10, dyn
	-[0x80001f7c]:csrrs a7, fcsr, zero
	-[0x80001f80]:sw t5, 1352(ra)
	-[0x80001f84]:sw t6, 1360(ra)
Current Store : [0x80001f84] : sw t6, 1360(ra) -- Store: [0x8000a110]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005b and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000060 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f78]:fsub.d t5, t3, s10, dyn
	-[0x80001f7c]:csrrs a7, fcsr, zero
	-[0x80001f80]:sw t5, 1352(ra)
	-[0x80001f84]:sw t6, 1360(ra)
	-[0x80001f88]:sw t5, 1368(ra)
Current Store : [0x80001f88] : sw t5, 1368(ra) -- Store: [0x8000a118]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000057 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000060 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001fb8]:fsub.d t5, t3, s10, dyn
	-[0x80001fbc]:csrrs a7, fcsr, zero
	-[0x80001fc0]:sw t5, 1384(ra)
	-[0x80001fc4]:sw t6, 1392(ra)
Current Store : [0x80001fc4] : sw t6, 1392(ra) -- Store: [0x8000a130]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000057 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000060 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001fb8]:fsub.d t5, t3, s10, dyn
	-[0x80001fbc]:csrrs a7, fcsr, zero
	-[0x80001fc0]:sw t5, 1384(ra)
	-[0x80001fc4]:sw t6, 1392(ra)
	-[0x80001fc8]:sw t5, 1400(ra)
Current Store : [0x80001fc8] : sw t5, 1400(ra) -- Store: [0x8000a138]:0x00000009




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000046 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000057 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ff8]:fsub.d t5, t3, s10, dyn
	-[0x80001ffc]:csrrs a7, fcsr, zero
	-[0x80002000]:sw t5, 1416(ra)
	-[0x80002004]:sw t6, 1424(ra)
Current Store : [0x80002004] : sw t6, 1424(ra) -- Store: [0x8000a150]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000046 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000057 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ff8]:fsub.d t5, t3, s10, dyn
	-[0x80001ffc]:csrrs a7, fcsr, zero
	-[0x80002000]:sw t5, 1416(ra)
	-[0x80002004]:sw t6, 1424(ra)
	-[0x80002008]:sw t5, 1432(ra)
Current Store : [0x80002008] : sw t5, 1432(ra) -- Store: [0x8000a158]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000051 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000072 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002038]:fsub.d t5, t3, s10, dyn
	-[0x8000203c]:csrrs a7, fcsr, zero
	-[0x80002040]:sw t5, 1448(ra)
	-[0x80002044]:sw t6, 1456(ra)
Current Store : [0x80002044] : sw t6, 1456(ra) -- Store: [0x8000a170]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000051 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000072 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002038]:fsub.d t5, t3, s10, dyn
	-[0x8000203c]:csrrs a7, fcsr, zero
	-[0x80002040]:sw t5, 1448(ra)
	-[0x80002044]:sw t6, 1456(ra)
	-[0x80002048]:sw t5, 1464(ra)
Current Store : [0x80002048] : sw t5, 1464(ra) -- Store: [0x8000a178]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000043 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000084 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002078]:fsub.d t5, t3, s10, dyn
	-[0x8000207c]:csrrs a7, fcsr, zero
	-[0x80002080]:sw t5, 1480(ra)
	-[0x80002084]:sw t6, 1488(ra)
Current Store : [0x80002084] : sw t6, 1488(ra) -- Store: [0x8000a190]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000043 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000084 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002078]:fsub.d t5, t3, s10, dyn
	-[0x8000207c]:csrrs a7, fcsr, zero
	-[0x80002080]:sw t5, 1480(ra)
	-[0x80002084]:sw t6, 1488(ra)
	-[0x80002088]:sw t5, 1496(ra)
Current Store : [0x80002088] : sw t5, 1496(ra) -- Store: [0x8000a198]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001d and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000000009e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800020b8]:fsub.d t5, t3, s10, dyn
	-[0x800020bc]:csrrs a7, fcsr, zero
	-[0x800020c0]:sw t5, 1512(ra)
	-[0x800020c4]:sw t6, 1520(ra)
Current Store : [0x800020c4] : sw t6, 1520(ra) -- Store: [0x8000a1b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001d and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000000009e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800020b8]:fsub.d t5, t3, s10, dyn
	-[0x800020bc]:csrrs a7, fcsr, zero
	-[0x800020c0]:sw t5, 1512(ra)
	-[0x800020c4]:sw t6, 1520(ra)
	-[0x800020c8]:sw t5, 1528(ra)
Current Store : [0x800020c8] : sw t5, 1528(ra) -- Store: [0x8000a1b8]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000054 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000155 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800020f8]:fsub.d t5, t3, s10, dyn
	-[0x800020fc]:csrrs a7, fcsr, zero
	-[0x80002100]:sw t5, 1544(ra)
	-[0x80002104]:sw t6, 1552(ra)
Current Store : [0x80002104] : sw t6, 1552(ra) -- Store: [0x8000a1d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000054 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000155 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800020f8]:fsub.d t5, t3, s10, dyn
	-[0x800020fc]:csrrs a7, fcsr, zero
	-[0x80002100]:sw t5, 1544(ra)
	-[0x80002104]:sw t6, 1552(ra)
	-[0x80002108]:sw t5, 1560(ra)
Current Store : [0x80002108] : sw t5, 1560(ra) -- Store: [0x8000a1d8]:0x00000101




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000033 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000234 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002138]:fsub.d t5, t3, s10, dyn
	-[0x8000213c]:csrrs a7, fcsr, zero
	-[0x80002140]:sw t5, 1576(ra)
	-[0x80002144]:sw t6, 1584(ra)
Current Store : [0x80002144] : sw t6, 1584(ra) -- Store: [0x8000a1f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000033 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000234 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002138]:fsub.d t5, t3, s10, dyn
	-[0x8000213c]:csrrs a7, fcsr, zero
	-[0x80002140]:sw t5, 1576(ra)
	-[0x80002144]:sw t6, 1584(ra)
	-[0x80002148]:sw t5, 1592(ra)
Current Store : [0x80002148] : sw t5, 1592(ra) -- Store: [0x8000a1f8]:0x00000201




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000004a and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000000044b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002178]:fsub.d t5, t3, s10, dyn
	-[0x8000217c]:csrrs a7, fcsr, zero
	-[0x80002180]:sw t5, 1608(ra)
	-[0x80002184]:sw t6, 1616(ra)
Current Store : [0x80002184] : sw t6, 1616(ra) -- Store: [0x8000a210]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000004a and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000000044b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002178]:fsub.d t5, t3, s10, dyn
	-[0x8000217c]:csrrs a7, fcsr, zero
	-[0x80002180]:sw t5, 1608(ra)
	-[0x80002184]:sw t6, 1616(ra)
	-[0x80002188]:sw t5, 1624(ra)
Current Store : [0x80002188] : sw t5, 1624(ra) -- Store: [0x8000a218]:0x00000401




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000055 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000856 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800021bc]:fsub.d t5, t3, s10, dyn
	-[0x800021c0]:csrrs a7, fcsr, zero
	-[0x800021c4]:sw t5, 1640(ra)
	-[0x800021c8]:sw t6, 1648(ra)
Current Store : [0x800021c8] : sw t6, 1648(ra) -- Store: [0x8000a230]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000055 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000856 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800021bc]:fsub.d t5, t3, s10, dyn
	-[0x800021c0]:csrrs a7, fcsr, zero
	-[0x800021c4]:sw t5, 1640(ra)
	-[0x800021c8]:sw t6, 1648(ra)
	-[0x800021cc]:sw t5, 1656(ra)
Current Store : [0x800021cc] : sw t5, 1656(ra) -- Store: [0x8000a238]:0x00000801




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000037 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000001038 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002200]:fsub.d t5, t3, s10, dyn
	-[0x80002204]:csrrs a7, fcsr, zero
	-[0x80002208]:sw t5, 1672(ra)
	-[0x8000220c]:sw t6, 1680(ra)
Current Store : [0x8000220c] : sw t6, 1680(ra) -- Store: [0x8000a250]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000037 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000001038 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002200]:fsub.d t5, t3, s10, dyn
	-[0x80002204]:csrrs a7, fcsr, zero
	-[0x80002208]:sw t5, 1672(ra)
	-[0x8000220c]:sw t6, 1680(ra)
	-[0x80002210]:sw t5, 1688(ra)
Current Store : [0x80002210] : sw t5, 1688(ra) -- Store: [0x8000a258]:0x00001001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000002060 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002244]:fsub.d t5, t3, s10, dyn
	-[0x80002248]:csrrs a7, fcsr, zero
	-[0x8000224c]:sw t5, 1704(ra)
	-[0x80002250]:sw t6, 1712(ra)
Current Store : [0x80002250] : sw t6, 1712(ra) -- Store: [0x8000a270]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000002060 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002244]:fsub.d t5, t3, s10, dyn
	-[0x80002248]:csrrs a7, fcsr, zero
	-[0x8000224c]:sw t5, 1704(ra)
	-[0x80002250]:sw t6, 1712(ra)
	-[0x80002254]:sw t5, 1720(ra)
Current Store : [0x80002254] : sw t5, 1720(ra) -- Store: [0x8000a278]:0x00002001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000011 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000004012 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002288]:fsub.d t5, t3, s10, dyn
	-[0x8000228c]:csrrs a7, fcsr, zero
	-[0x80002290]:sw t5, 1736(ra)
	-[0x80002294]:sw t6, 1744(ra)
Current Store : [0x80002294] : sw t6, 1744(ra) -- Store: [0x8000a290]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000011 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000004012 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002288]:fsub.d t5, t3, s10, dyn
	-[0x8000228c]:csrrs a7, fcsr, zero
	-[0x80002290]:sw t5, 1736(ra)
	-[0x80002294]:sw t6, 1744(ra)
	-[0x80002298]:sw t5, 1752(ra)
Current Store : [0x80002298] : sw t5, 1752(ra) -- Store: [0x8000a298]:0x00004001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000007 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000008008 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800022cc]:fsub.d t5, t3, s10, dyn
	-[0x800022d0]:csrrs a7, fcsr, zero
	-[0x800022d4]:sw t5, 1768(ra)
	-[0x800022d8]:sw t6, 1776(ra)
Current Store : [0x800022d8] : sw t6, 1776(ra) -- Store: [0x8000a2b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000007 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000008008 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800022cc]:fsub.d t5, t3, s10, dyn
	-[0x800022d0]:csrrs a7, fcsr, zero
	-[0x800022d4]:sw t5, 1768(ra)
	-[0x800022d8]:sw t6, 1776(ra)
	-[0x800022dc]:sw t5, 1784(ra)
Current Store : [0x800022dc] : sw t5, 1784(ra) -- Store: [0x8000a2b8]:0x00008001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000a and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000001000b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002310]:fsub.d t5, t3, s10, dyn
	-[0x80002314]:csrrs a7, fcsr, zero
	-[0x80002318]:sw t5, 1800(ra)
	-[0x8000231c]:sw t6, 1808(ra)
Current Store : [0x8000231c] : sw t6, 1808(ra) -- Store: [0x8000a2d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000a and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000001000b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002310]:fsub.d t5, t3, s10, dyn
	-[0x80002314]:csrrs a7, fcsr, zero
	-[0x80002318]:sw t5, 1800(ra)
	-[0x8000231c]:sw t6, 1808(ra)
	-[0x80002320]:sw t5, 1816(ra)
Current Store : [0x80002320] : sw t5, 1816(ra) -- Store: [0x8000a2d8]:0x00010001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000028 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000020029 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002354]:fsub.d t5, t3, s10, dyn
	-[0x80002358]:csrrs a7, fcsr, zero
	-[0x8000235c]:sw t5, 1832(ra)
	-[0x80002360]:sw t6, 1840(ra)
Current Store : [0x80002360] : sw t6, 1840(ra) -- Store: [0x8000a2f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000028 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000020029 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002354]:fsub.d t5, t3, s10, dyn
	-[0x80002358]:csrrs a7, fcsr, zero
	-[0x8000235c]:sw t5, 1832(ra)
	-[0x80002360]:sw t6, 1840(ra)
	-[0x80002364]:sw t5, 1848(ra)
Current Store : [0x80002364] : sw t5, 1848(ra) -- Store: [0x8000a2f8]:0x00020001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000060 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000040061 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002398]:fsub.d t5, t3, s10, dyn
	-[0x8000239c]:csrrs a7, fcsr, zero
	-[0x800023a0]:sw t5, 1864(ra)
	-[0x800023a4]:sw t6, 1872(ra)
Current Store : [0x800023a4] : sw t6, 1872(ra) -- Store: [0x8000a310]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000060 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000040061 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002398]:fsub.d t5, t3, s10, dyn
	-[0x8000239c]:csrrs a7, fcsr, zero
	-[0x800023a0]:sw t5, 1864(ra)
	-[0x800023a4]:sw t6, 1872(ra)
	-[0x800023a8]:sw t5, 1880(ra)
Current Store : [0x800023a8] : sw t5, 1880(ra) -- Store: [0x8000a318]:0x00040001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000036 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000080037 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800023dc]:fsub.d t5, t3, s10, dyn
	-[0x800023e0]:csrrs a7, fcsr, zero
	-[0x800023e4]:sw t5, 1896(ra)
	-[0x800023e8]:sw t6, 1904(ra)
Current Store : [0x800023e8] : sw t6, 1904(ra) -- Store: [0x8000a330]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000036 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000080037 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800023dc]:fsub.d t5, t3, s10, dyn
	-[0x800023e0]:csrrs a7, fcsr, zero
	-[0x800023e4]:sw t5, 1896(ra)
	-[0x800023e8]:sw t6, 1904(ra)
	-[0x800023ec]:sw t5, 1912(ra)
Current Store : [0x800023ec] : sw t5, 1912(ra) -- Store: [0x8000a338]:0x00080001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000021 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000100022 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002420]:fsub.d t5, t3, s10, dyn
	-[0x80002424]:csrrs a7, fcsr, zero
	-[0x80002428]:sw t5, 1928(ra)
	-[0x8000242c]:sw t6, 1936(ra)
Current Store : [0x8000242c] : sw t6, 1936(ra) -- Store: [0x8000a350]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000021 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000100022 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002420]:fsub.d t5, t3, s10, dyn
	-[0x80002424]:csrrs a7, fcsr, zero
	-[0x80002428]:sw t5, 1928(ra)
	-[0x8000242c]:sw t6, 1936(ra)
	-[0x80002430]:sw t5, 1944(ra)
Current Store : [0x80002430] : sw t5, 1944(ra) -- Store: [0x8000a358]:0x00100001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000200003 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002464]:fsub.d t5, t3, s10, dyn
	-[0x80002468]:csrrs a7, fcsr, zero
	-[0x8000246c]:sw t5, 1960(ra)
	-[0x80002470]:sw t6, 1968(ra)
Current Store : [0x80002470] : sw t6, 1968(ra) -- Store: [0x8000a370]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000200003 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002464]:fsub.d t5, t3, s10, dyn
	-[0x80002468]:csrrs a7, fcsr, zero
	-[0x8000246c]:sw t5, 1960(ra)
	-[0x80002470]:sw t6, 1968(ra)
	-[0x80002474]:sw t5, 1976(ra)
Current Store : [0x80002474] : sw t5, 1976(ra) -- Store: [0x8000a378]:0x00200001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000005 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000400006 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800024a8]:fsub.d t5, t3, s10, dyn
	-[0x800024ac]:csrrs a7, fcsr, zero
	-[0x800024b0]:sw t5, 1992(ra)
	-[0x800024b4]:sw t6, 2000(ra)
Current Store : [0x800024b4] : sw t6, 2000(ra) -- Store: [0x8000a390]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000005 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000400006 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800024a8]:fsub.d t5, t3, s10, dyn
	-[0x800024ac]:csrrs a7, fcsr, zero
	-[0x800024b0]:sw t5, 1992(ra)
	-[0x800024b4]:sw t6, 2000(ra)
	-[0x800024b8]:sw t5, 2008(ra)
Current Store : [0x800024b8] : sw t5, 2008(ra) -- Store: [0x8000a398]:0x00400001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001c and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffe2 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800024ec]:fsub.d t5, t3, s10, dyn
	-[0x800024f0]:csrrs a7, fcsr, zero
	-[0x800024f4]:sw t5, 2024(ra)
	-[0x800024f8]:sw t6, 2032(ra)
Current Store : [0x800024f8] : sw t6, 2032(ra) -- Store: [0x8000a3b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001c and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffe2 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800024ec]:fsub.d t5, t3, s10, dyn
	-[0x800024f0]:csrrs a7, fcsr, zero
	-[0x800024f4]:sw t5, 2024(ra)
	-[0x800024f8]:sw t6, 2032(ra)
	-[0x800024fc]:sw t5, 2040(ra)
Current Store : [0x800024fc] : sw t5, 2040(ra) -- Store: [0x8000a3b8]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003b and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffc2 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002574]:fsub.d t5, t3, s10, dyn
	-[0x80002578]:csrrs a7, fcsr, zero
	-[0x8000257c]:sw t5, 16(ra)
	-[0x80002580]:sw t6, 24(ra)
Current Store : [0x80002580] : sw t6, 24(ra) -- Store: [0x8000a3d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003b and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffc2 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002574]:fsub.d t5, t3, s10, dyn
	-[0x80002578]:csrrs a7, fcsr, zero
	-[0x8000257c]:sw t5, 16(ra)
	-[0x80002580]:sw t6, 24(ra)
	-[0x80002584]:sw t5, 32(ra)
Current Store : [0x80002584] : sw t5, 32(ra) -- Store: [0x8000a3d8]:0xFFFFFFFD




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005b and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffa0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800025f8]:fsub.d t5, t3, s10, dyn
	-[0x800025fc]:csrrs a7, fcsr, zero
	-[0x80002600]:sw t5, 48(ra)
	-[0x80002604]:sw t6, 56(ra)
Current Store : [0x80002604] : sw t6, 56(ra) -- Store: [0x8000a3f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005b and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffa0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800025f8]:fsub.d t5, t3, s10, dyn
	-[0x800025fc]:csrrs a7, fcsr, zero
	-[0x80002600]:sw t5, 48(ra)
	-[0x80002604]:sw t6, 56(ra)
	-[0x80002608]:sw t5, 64(ra)
Current Store : [0x80002608] : sw t5, 64(ra) -- Store: [0x8000a3f8]:0xFFFFFFFB




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000042 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffb5 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000267c]:fsub.d t5, t3, s10, dyn
	-[0x80002680]:csrrs a7, fcsr, zero
	-[0x80002684]:sw t5, 80(ra)
	-[0x80002688]:sw t6, 88(ra)
Current Store : [0x80002688] : sw t6, 88(ra) -- Store: [0x8000a410]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000042 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffb5 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000267c]:fsub.d t5, t3, s10, dyn
	-[0x80002680]:csrrs a7, fcsr, zero
	-[0x80002684]:sw t5, 80(ra)
	-[0x80002688]:sw t6, 88(ra)
	-[0x8000268c]:sw t5, 96(ra)
Current Store : [0x8000268c] : sw t5, 96(ra) -- Store: [0x8000a418]:0xFFFFFFF7




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000031 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffbe and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002700]:fsub.d t5, t3, s10, dyn
	-[0x80002704]:csrrs a7, fcsr, zero
	-[0x80002708]:sw t5, 112(ra)
	-[0x8000270c]:sw t6, 120(ra)
Current Store : [0x8000270c] : sw t6, 120(ra) -- Store: [0x8000a430]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000031 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffbe and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002700]:fsub.d t5, t3, s10, dyn
	-[0x80002704]:csrrs a7, fcsr, zero
	-[0x80002708]:sw t5, 112(ra)
	-[0x8000270c]:sw t6, 120(ra)
	-[0x80002710]:sw t5, 128(ra)
Current Store : [0x80002710] : sw t5, 128(ra) -- Store: [0x8000a438]:0xFFFFFFEF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000002d and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffb2 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002784]:fsub.d t5, t3, s10, dyn
	-[0x80002788]:csrrs a7, fcsr, zero
	-[0x8000278c]:sw t5, 144(ra)
	-[0x80002790]:sw t6, 152(ra)
Current Store : [0x80002790] : sw t6, 152(ra) -- Store: [0x8000a450]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000002d and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffb2 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002784]:fsub.d t5, t3, s10, dyn
	-[0x80002788]:csrrs a7, fcsr, zero
	-[0x8000278c]:sw t5, 144(ra)
	-[0x80002790]:sw t6, 152(ra)
	-[0x80002794]:sw t5, 160(ra)
Current Store : [0x80002794] : sw t5, 160(ra) -- Store: [0x8000a458]:0xFFFFFFDF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000056 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006724]:fsub.d t5, t3, s10, dyn
	-[0x80006728]:csrrs a7, fcsr, zero
	-[0x8000672c]:sw t5, 0(ra)
	-[0x80006730]:sw t6, 8(ra)
Current Store : [0x80006730] : sw t6, 8(ra) -- Store: [0x8000a3d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000056 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006724]:fsub.d t5, t3, s10, dyn
	-[0x80006728]:csrrs a7, fcsr, zero
	-[0x8000672c]:sw t5, 0(ra)
	-[0x80006730]:sw t6, 8(ra)
	-[0x80006734]:sw t5, 16(ra)
Current Store : [0x80006734] : sw t5, 16(ra) -- Store: [0x8000a3d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000021 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006764]:fsub.d t5, t3, s10, dyn
	-[0x80006768]:csrrs a7, fcsr, zero
	-[0x8000676c]:sw t5, 32(ra)
	-[0x80006770]:sw t6, 40(ra)
Current Store : [0x80006770] : sw t6, 40(ra) -- Store: [0x8000a3f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000021 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006764]:fsub.d t5, t3, s10, dyn
	-[0x80006768]:csrrs a7, fcsr, zero
	-[0x8000676c]:sw t5, 32(ra)
	-[0x80006770]:sw t6, 40(ra)
	-[0x80006774]:sw t5, 48(ra)
Current Store : [0x80006774] : sw t5, 48(ra) -- Store: [0x8000a3f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000058 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800067a4]:fsub.d t5, t3, s10, dyn
	-[0x800067a8]:csrrs a7, fcsr, zero
	-[0x800067ac]:sw t5, 64(ra)
	-[0x800067b0]:sw t6, 72(ra)
Current Store : [0x800067b0] : sw t6, 72(ra) -- Store: [0x8000a410]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000058 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800067a4]:fsub.d t5, t3, s10, dyn
	-[0x800067a8]:csrrs a7, fcsr, zero
	-[0x800067ac]:sw t5, 64(ra)
	-[0x800067b0]:sw t6, 72(ra)
	-[0x800067b4]:sw t5, 80(ra)
Current Store : [0x800067b4] : sw t5, 80(ra) -- Store: [0x8000a418]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000038 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800067e4]:fsub.d t5, t3, s10, dyn
	-[0x800067e8]:csrrs a7, fcsr, zero
	-[0x800067ec]:sw t5, 96(ra)
	-[0x800067f0]:sw t6, 104(ra)
Current Store : [0x800067f0] : sw t6, 104(ra) -- Store: [0x8000a430]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000038 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800067e4]:fsub.d t5, t3, s10, dyn
	-[0x800067e8]:csrrs a7, fcsr, zero
	-[0x800067ec]:sw t5, 96(ra)
	-[0x800067f0]:sw t6, 104(ra)
	-[0x800067f4]:sw t5, 112(ra)
Current Store : [0x800067f4] : sw t5, 112(ra) -- Store: [0x8000a438]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000039 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006824]:fsub.d t5, t3, s10, dyn
	-[0x80006828]:csrrs a7, fcsr, zero
	-[0x8000682c]:sw t5, 128(ra)
	-[0x80006830]:sw t6, 136(ra)
Current Store : [0x80006830] : sw t6, 136(ra) -- Store: [0x8000a450]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000039 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006824]:fsub.d t5, t3, s10, dyn
	-[0x80006828]:csrrs a7, fcsr, zero
	-[0x8000682c]:sw t5, 128(ra)
	-[0x80006830]:sw t6, 136(ra)
	-[0x80006834]:sw t5, 144(ra)
Current Store : [0x80006834] : sw t5, 144(ra) -- Store: [0x8000a458]:0x00000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                          signature                           |                                                                                                                        coverpoints                                                                                                                        |                                                                                                                       code                                                                                                                        |
|---:|--------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80009318]<br>0x00000000<br> [0x80009330]<br>0x00000000<br> |- mnemonic : fsub.d<br> - rs1 : x30<br> - rs2 : x30<br> - rd : x30<br> - rs1 == rs2 == rd<br>                                                                                                                                                              |[0x8000013c]:fsub.d t5, t5, t5, dyn<br> [0x80000140]:csrrs tp, fcsr, zero<br> [0x80000144]:sw t5, 0(ra)<br> [0x80000148]:sw t6, 8(ra)<br> [0x8000014c]:sw t5, 16(ra)<br> [0x80000150]:sw tp, 24(ra)<br>                                            |
|   2|[0x80009338]<br>0x00000002<br> [0x80009350]<br>0x00000000<br> |- rs1 : x26<br> - rs2 : x24<br> - rd : x28<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000062 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000060 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x8000017c]:fsub.d t3, s10, s8, dyn<br> [0x80000180]:csrrs tp, fcsr, zero<br> [0x80000184]:sw t3, 32(ra)<br> [0x80000188]:sw t4, 40(ra)<br> [0x8000018c]:sw t3, 48(ra)<br> [0x80000190]:sw tp, 56(ra)<br>                                         |
|   3|[0x80009358]<br>0x00000004<br> [0x80009370]<br>0x00000000<br> |- rs1 : x24<br> - rs2 : x28<br> - rd : x24<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000021 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000000001d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                        |[0x800001bc]:fsub.d s8, s8, t3, dyn<br> [0x800001c0]:csrrs tp, fcsr, zero<br> [0x800001c4]:sw s8, 64(ra)<br> [0x800001c8]:sw s9, 72(ra)<br> [0x800001cc]:sw s8, 80(ra)<br> [0x800001d0]:sw tp, 88(ra)<br>                                          |
|   4|[0x80009378]<br>0x00000008<br> [0x80009390]<br>0x00000000<br> |- rs1 : x28<br> - rs2 : x26<br> - rd : x26<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000040 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000038 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                        |[0x800001fc]:fsub.d s10, t3, s10, dyn<br> [0x80000200]:csrrs tp, fcsr, zero<br> [0x80000204]:sw s10, 96(ra)<br> [0x80000208]:sw s11, 104(ra)<br> [0x8000020c]:sw s10, 112(ra)<br> [0x80000210]:sw tp, 120(ra)<br>                                  |
|   5|[0x80009398]<br>0x00000000<br> [0x800093b0]<br>0x00000000<br> |- rs1 : x20<br> - rs2 : x20<br> - rd : x22<br> - rs1 == rs2 != rd<br>                                                                                                                                                                                      |[0x8000023c]:fsub.d s6, s4, s4, dyn<br> [0x80000240]:csrrs tp, fcsr, zero<br> [0x80000244]:sw s6, 128(ra)<br> [0x80000248]:sw s7, 136(ra)<br> [0x8000024c]:sw s6, 144(ra)<br> [0x80000250]:sw tp, 152(ra)<br>                                      |
|   6|[0x800093b8]<br>0x00000020<br> [0x800093d0]<br>0x00000000<br> |- rs1 : x22<br> - rs2 : x18<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000054 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000034 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x8000027c]:fsub.d s4, s6, s2, dyn<br> [0x80000280]:csrrs tp, fcsr, zero<br> [0x80000284]:sw s4, 160(ra)<br> [0x80000288]:sw s5, 168(ra)<br> [0x8000028c]:sw s4, 176(ra)<br> [0x80000290]:sw tp, 184(ra)<br>                                      |
|   7|[0x800093d8]<br>0x00000040<br> [0x800093f0]<br>0x00000000<br> |- rs1 : x16<br> - rs2 : x22<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001b and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000025 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x800002bc]:fsub.d s2, a6, s6, dyn<br> [0x800002c0]:csrrs tp, fcsr, zero<br> [0x800002c4]:sw s2, 192(ra)<br> [0x800002c8]:sw s3, 200(ra)<br> [0x800002cc]:sw s2, 208(ra)<br> [0x800002d0]:sw tp, 216(ra)<br>                                      |
|   8|[0x800093f8]<br>0x00000080<br> [0x80009410]<br>0x00000000<br> |- rs1 : x18<br> - rs2 : x14<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003f and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000041 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x800002fc]:fsub.d a6, s2, a4, dyn<br> [0x80000300]:csrrs tp, fcsr, zero<br> [0x80000304]:sw a6, 224(ra)<br> [0x80000308]:sw a7, 232(ra)<br> [0x8000030c]:sw a6, 240(ra)<br> [0x80000310]:sw tp, 248(ra)<br>                                      |
|   9|[0x80009418]<br>0x00000100<br> [0x80009430]<br>0x00000000<br> |- rs1 : x12<br> - rs2 : x16<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000032 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000000000ce and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x8000033c]:fsub.d a4, a2, a6, dyn<br> [0x80000340]:csrrs tp, fcsr, zero<br> [0x80000344]:sw a4, 256(ra)<br> [0x80000348]:sw a5, 264(ra)<br> [0x8000034c]:sw a4, 272(ra)<br> [0x80000350]:sw tp, 280(ra)<br>                                      |
|  10|[0x80009438]<br>0x00000200<br> [0x80009450]<br>0x00000000<br> |- rs1 : x14<br> - rs2 : x10<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000004e and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000000001b2 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x80000384]:fsub.d a2, a4, a0, dyn<br> [0x80000388]:csrrs a7, fcsr, zero<br> [0x8000038c]:sw a2, 288(ra)<br> [0x80000390]:sw a3, 296(ra)<br> [0x80000394]:sw a2, 304(ra)<br> [0x80000398]:sw a7, 312(ra)<br>                                      |
|  11|[0x80009458]<br>0x00000400<br> [0x80009470]<br>0x00000000<br> |- rs1 : x8<br> - rs2 : x12<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000063 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000000039d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x800003c4]:fsub.d a0, fp, a2, dyn<br> [0x800003c8]:csrrs a7, fcsr, zero<br> [0x800003cc]:sw a0, 320(ra)<br> [0x800003d0]:sw a1, 328(ra)<br> [0x800003d4]:sw a0, 336(ra)<br> [0x800003d8]:sw a7, 344(ra)<br>                                      |
|  12|[0x800093c8]<br>0x00000800<br> [0x800093e0]<br>0x00000000<br> |- rs1 : x10<br> - rs2 : x6<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005a and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000000007a6 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                 |[0x8000040c]:fsub.d fp, a0, t1, dyn<br> [0x80000410]:csrrs a7, fcsr, zero<br> [0x80000414]:sw fp, 0(ra)<br> [0x80000418]:sw s1, 8(ra)<br> [0x8000041c]:sw fp, 16(ra)<br> [0x80000420]:sw a7, 24(ra)<br>                                            |
|  13|[0x800093e8]<br>0x00001000<br> [0x80009400]<br>0x00000000<br> |- rs1 : x4<br> - rs2 : x8<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000023 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000fdd and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                  |[0x80000450]:fsub.d t1, tp, fp, dyn<br> [0x80000454]:csrrs a7, fcsr, zero<br> [0x80000458]:sw t1, 32(ra)<br> [0x8000045c]:sw t2, 40(ra)<br> [0x80000460]:sw t1, 48(ra)<br> [0x80000464]:sw a7, 56(ra)<br>                                          |
|  14|[0x80009408]<br>0x00002000<br> [0x80009420]<br>0x00000000<br> |- rs1 : x6<br> - rs2 : x2<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001e and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000001fe2 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                  |[0x80000494]:fsub.d tp, t1, sp, dyn<br> [0x80000498]:csrrs a7, fcsr, zero<br> [0x8000049c]:sw tp, 64(ra)<br> [0x800004a0]:sw t0, 72(ra)<br> [0x800004a4]:sw tp, 80(ra)<br> [0x800004a8]:sw a7, 88(ra)<br>                                          |
|  15|[0x80009428]<br>0x00004000<br> [0x80009440]<br>0x00000000<br> |- rs1 : x2<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000e and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000003ff2 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                               |[0x800004d8]:fsub.d t5, sp, t3, dyn<br> [0x800004dc]:csrrs a7, fcsr, zero<br> [0x800004e0]:sw t5, 96(ra)<br> [0x800004e4]:sw t6, 104(ra)<br> [0x800004e8]:sw t5, 112(ra)<br> [0x800004ec]:sw a7, 120(ra)<br>                                       |
|  16|[0x80009448]<br>0x00008000<br> [0x80009460]<br>0x00000000<br> |- rs2 : x4<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000004 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000007ffc and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                               |[0x8000051c]:fsub.d t5, t3, tp, dyn<br> [0x80000520]:csrrs a7, fcsr, zero<br> [0x80000524]:sw t5, 128(ra)<br> [0x80000528]:sw t6, 136(ra)<br> [0x8000052c]:sw t5, 144(ra)<br> [0x80000530]:sw a7, 152(ra)<br>                                      |
|  17|[0x80009468]<br>0x00010000<br> [0x80009480]<br>0x00000000<br> |- rd : x2<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000004 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000000fffc and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                |[0x80000560]:fsub.d sp, t5, t3, dyn<br> [0x80000564]:csrrs a7, fcsr, zero<br> [0x80000568]:sw sp, 160(ra)<br> [0x8000056c]:sw gp, 168(ra)<br> [0x80000570]:sw sp, 176(ra)<br> [0x80000574]:sw a7, 184(ra)<br>                                      |
|  18|[0x80009488]<br>0x00020000<br> [0x800094a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000046 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000001ffba and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800005a4]:fsub.d t5, t3, s10, dyn<br> [0x800005a8]:csrrs a7, fcsr, zero<br> [0x800005ac]:sw t5, 192(ra)<br> [0x800005b0]:sw t6, 200(ra)<br> [0x800005b4]:sw t5, 208(ra)<br> [0x800005b8]:sw a7, 216(ra)<br>                                     |
|  19|[0x800094a8]<br>0x00040000<br> [0x800094c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000031 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000003ffcf and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800005e8]:fsub.d t5, t3, s10, dyn<br> [0x800005ec]:csrrs a7, fcsr, zero<br> [0x800005f0]:sw t5, 224(ra)<br> [0x800005f4]:sw t6, 232(ra)<br> [0x800005f8]:sw t5, 240(ra)<br> [0x800005fc]:sw a7, 248(ra)<br>                                     |
|  20|[0x800094c8]<br>0x00080000<br> [0x800094e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001c and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000007ffe4 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x8000062c]:fsub.d t5, t3, s10, dyn<br> [0x80000630]:csrrs a7, fcsr, zero<br> [0x80000634]:sw t5, 256(ra)<br> [0x80000638]:sw t6, 264(ra)<br> [0x8000063c]:sw t5, 272(ra)<br> [0x80000640]:sw a7, 280(ra)<br>                                     |
|  21|[0x800094e8]<br>0x00100000<br> [0x80009500]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005d and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000000fffa3 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000670]:fsub.d t5, t3, s10, dyn<br> [0x80000674]:csrrs a7, fcsr, zero<br> [0x80000678]:sw t5, 288(ra)<br> [0x8000067c]:sw t6, 296(ra)<br> [0x80000680]:sw t5, 304(ra)<br> [0x80000684]:sw a7, 312(ra)<br>                                     |
|  22|[0x80009508]<br>0x00200000<br> [0x80009520]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000044 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000001fffbc and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800006b4]:fsub.d t5, t3, s10, dyn<br> [0x800006b8]:csrrs a7, fcsr, zero<br> [0x800006bc]:sw t5, 320(ra)<br> [0x800006c0]:sw t6, 328(ra)<br> [0x800006c4]:sw t5, 336(ra)<br> [0x800006c8]:sw a7, 344(ra)<br>                                     |
|  23|[0x80009528]<br>0x00400000<br> [0x80009540]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000062 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000003fff9e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800006f8]:fsub.d t5, t3, s10, dyn<br> [0x800006fc]:csrrs a7, fcsr, zero<br> [0x80000700]:sw t5, 352(ra)<br> [0x80000704]:sw t6, 360(ra)<br> [0x80000708]:sw t5, 368(ra)<br> [0x8000070c]:sw a7, 376(ra)<br>                                     |
|  24|[0x80009548]<br>0x00000001<br> [0x80009560]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000040 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000041 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000738]:fsub.d t5, t3, s10, dyn<br> [0x8000073c]:csrrs a7, fcsr, zero<br> [0x80000740]:sw t5, 384(ra)<br> [0x80000744]:sw t6, 392(ra)<br> [0x80000748]:sw t5, 400(ra)<br> [0x8000074c]:sw a7, 408(ra)<br>                                     |
|  25|[0x80009568]<br>0x00000002<br> [0x80009580]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000020 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000778]:fsub.d t5, t3, s10, dyn<br> [0x8000077c]:csrrs a7, fcsr, zero<br> [0x80000780]:sw t5, 416(ra)<br> [0x80000784]:sw t6, 424(ra)<br> [0x80000788]:sw t5, 432(ra)<br> [0x8000078c]:sw a7, 440(ra)<br>                                     |
|  26|[0x80009588]<br>0x00000004<br> [0x800095a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001e and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000022 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800007b8]:fsub.d t5, t3, s10, dyn<br> [0x800007bc]:csrrs a7, fcsr, zero<br> [0x800007c0]:sw t5, 448(ra)<br> [0x800007c4]:sw t6, 456(ra)<br> [0x800007c8]:sw t5, 464(ra)<br> [0x800007cc]:sw a7, 472(ra)<br>                                     |
|  27|[0x800095a8]<br>0x00000008<br> [0x800095c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001d and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000025 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800007f8]:fsub.d t5, t3, s10, dyn<br> [0x800007fc]:csrrs a7, fcsr, zero<br> [0x80000800]:sw t5, 480(ra)<br> [0x80000804]:sw t6, 488(ra)<br> [0x80000808]:sw t5, 496(ra)<br> [0x8000080c]:sw a7, 504(ra)<br>                                     |
|  28|[0x800095c8]<br>0x00000010<br> [0x800095e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003b and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000000004b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000838]:fsub.d t5, t3, s10, dyn<br> [0x8000083c]:csrrs a7, fcsr, zero<br> [0x80000840]:sw t5, 512(ra)<br> [0x80000844]:sw t6, 520(ra)<br> [0x80000848]:sw t5, 528(ra)<br> [0x8000084c]:sw a7, 536(ra)<br>                                     |
|  29|[0x800095e8]<br>0x00000020<br> [0x80009600]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000003 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000023 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000878]:fsub.d t5, t3, s10, dyn<br> [0x8000087c]:csrrs a7, fcsr, zero<br> [0x80000880]:sw t5, 544(ra)<br> [0x80000884]:sw t6, 552(ra)<br> [0x80000888]:sw t5, 560(ra)<br> [0x8000088c]:sw a7, 568(ra)<br>                                     |
|  30|[0x80009608]<br>0x00000040<br> [0x80009620]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000048 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000088 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800008b8]:fsub.d t5, t3, s10, dyn<br> [0x800008bc]:csrrs a7, fcsr, zero<br> [0x800008c0]:sw t5, 576(ra)<br> [0x800008c4]:sw t6, 584(ra)<br> [0x800008c8]:sw t5, 592(ra)<br> [0x800008cc]:sw a7, 600(ra)<br>                                     |
|  31|[0x80009628]<br>0x00000080<br> [0x80009640]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000d and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000000008d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800008f8]:fsub.d t5, t3, s10, dyn<br> [0x800008fc]:csrrs a7, fcsr, zero<br> [0x80000900]:sw t5, 608(ra)<br> [0x80000904]:sw t6, 616(ra)<br> [0x80000908]:sw t5, 624(ra)<br> [0x8000090c]:sw a7, 632(ra)<br>                                     |
|  32|[0x80009648]<br>0x00000100<br> [0x80009660]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000051 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000151 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000938]:fsub.d t5, t3, s10, dyn<br> [0x8000093c]:csrrs a7, fcsr, zero<br> [0x80000940]:sw t5, 640(ra)<br> [0x80000944]:sw t6, 648(ra)<br> [0x80000948]:sw t5, 656(ra)<br> [0x8000094c]:sw a7, 664(ra)<br>                                     |
|  33|[0x80009668]<br>0x00000200<br> [0x80009680]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000026 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000226 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000978]:fsub.d t5, t3, s10, dyn<br> [0x8000097c]:csrrs a7, fcsr, zero<br> [0x80000980]:sw t5, 672(ra)<br> [0x80000984]:sw t6, 680(ra)<br> [0x80000988]:sw t5, 688(ra)<br> [0x8000098c]:sw a7, 696(ra)<br>                                     |
|  34|[0x80009688]<br>0x00000400<br> [0x800096a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000060 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000460 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800009b8]:fsub.d t5, t3, s10, dyn<br> [0x800009bc]:csrrs a7, fcsr, zero<br> [0x800009c0]:sw t5, 704(ra)<br> [0x800009c4]:sw t6, 712(ra)<br> [0x800009c8]:sw t5, 720(ra)<br> [0x800009cc]:sw a7, 728(ra)<br>                                     |
|  35|[0x800096a8]<br>0x00000800<br> [0x800096c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005d and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000000085d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800009fc]:fsub.d t5, t3, s10, dyn<br> [0x80000a00]:csrrs a7, fcsr, zero<br> [0x80000a04]:sw t5, 736(ra)<br> [0x80000a08]:sw t6, 744(ra)<br> [0x80000a0c]:sw t5, 752(ra)<br> [0x80000a10]:sw a7, 760(ra)<br>                                     |
|  36|[0x800096c8]<br>0x00001000<br> [0x800096e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000041 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000001041 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000a40]:fsub.d t5, t3, s10, dyn<br> [0x80000a44]:csrrs a7, fcsr, zero<br> [0x80000a48]:sw t5, 768(ra)<br> [0x80000a4c]:sw t6, 776(ra)<br> [0x80000a50]:sw t5, 784(ra)<br> [0x80000a54]:sw a7, 792(ra)<br>                                     |
|  37|[0x800096e8]<br>0x00002000<br> [0x80009700]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000041 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000002041 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000a84]:fsub.d t5, t3, s10, dyn<br> [0x80000a88]:csrrs a7, fcsr, zero<br> [0x80000a8c]:sw t5, 800(ra)<br> [0x80000a90]:sw t6, 808(ra)<br> [0x80000a94]:sw t5, 816(ra)<br> [0x80000a98]:sw a7, 824(ra)<br>                                     |
|  38|[0x80009708]<br>0x00004000<br> [0x80009720]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000019 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000004019 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000ac8]:fsub.d t5, t3, s10, dyn<br> [0x80000acc]:csrrs a7, fcsr, zero<br> [0x80000ad0]:sw t5, 832(ra)<br> [0x80000ad4]:sw t6, 840(ra)<br> [0x80000ad8]:sw t5, 848(ra)<br> [0x80000adc]:sw a7, 856(ra)<br>                                     |
|  39|[0x80009728]<br>0x00008000<br> [0x80009740]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000025 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000008025 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000b0c]:fsub.d t5, t3, s10, dyn<br> [0x80000b10]:csrrs a7, fcsr, zero<br> [0x80000b14]:sw t5, 864(ra)<br> [0x80000b18]:sw t6, 872(ra)<br> [0x80000b1c]:sw t5, 880(ra)<br> [0x80000b20]:sw a7, 888(ra)<br>                                     |
|  40|[0x80009748]<br>0x00010000<br> [0x80009760]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000040 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000010040 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000b50]:fsub.d t5, t3, s10, dyn<br> [0x80000b54]:csrrs a7, fcsr, zero<br> [0x80000b58]:sw t5, 896(ra)<br> [0x80000b5c]:sw t6, 904(ra)<br> [0x80000b60]:sw t5, 912(ra)<br> [0x80000b64]:sw a7, 920(ra)<br>                                     |
|  41|[0x80009768]<br>0x00020000<br> [0x80009780]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000033 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000020033 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000b94]:fsub.d t5, t3, s10, dyn<br> [0x80000b98]:csrrs a7, fcsr, zero<br> [0x80000b9c]:sw t5, 928(ra)<br> [0x80000ba0]:sw t6, 936(ra)<br> [0x80000ba4]:sw t5, 944(ra)<br> [0x80000ba8]:sw a7, 952(ra)<br>                                     |
|  42|[0x80009788]<br>0x00040000<br> [0x800097a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000005 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000040005 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000bd8]:fsub.d t5, t3, s10, dyn<br> [0x80000bdc]:csrrs a7, fcsr, zero<br> [0x80000be0]:sw t5, 960(ra)<br> [0x80000be4]:sw t6, 968(ra)<br> [0x80000be8]:sw t5, 976(ra)<br> [0x80000bec]:sw a7, 984(ra)<br>                                     |
|  43|[0x800097a8]<br>0x00080000<br> [0x800097c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000020 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000080020 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000c1c]:fsub.d t5, t3, s10, dyn<br> [0x80000c20]:csrrs a7, fcsr, zero<br> [0x80000c24]:sw t5, 992(ra)<br> [0x80000c28]:sw t6, 1000(ra)<br> [0x80000c2c]:sw t5, 1008(ra)<br> [0x80000c30]:sw a7, 1016(ra)<br>                                  |
|  44|[0x800097c8]<br>0x00100000<br> [0x800097e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000034 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000100034 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000c60]:fsub.d t5, t3, s10, dyn<br> [0x80000c64]:csrrs a7, fcsr, zero<br> [0x80000c68]:sw t5, 1024(ra)<br> [0x80000c6c]:sw t6, 1032(ra)<br> [0x80000c70]:sw t5, 1040(ra)<br> [0x80000c74]:sw a7, 1048(ra)<br>                                 |
|  45|[0x800097e8]<br>0x00200000<br> [0x80009800]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000056 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000200056 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000ca4]:fsub.d t5, t3, s10, dyn<br> [0x80000ca8]:csrrs a7, fcsr, zero<br> [0x80000cac]:sw t5, 1056(ra)<br> [0x80000cb0]:sw t6, 1064(ra)<br> [0x80000cb4]:sw t5, 1072(ra)<br> [0x80000cb8]:sw a7, 1080(ra)<br>                                 |
|  46|[0x80009808]<br>0x00400000<br> [0x80009820]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000002f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000040002f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000ce8]:fsub.d t5, t3, s10, dyn<br> [0x80000cec]:csrrs a7, fcsr, zero<br> [0x80000cf0]:sw t5, 1088(ra)<br> [0x80000cf4]:sw t6, 1096(ra)<br> [0x80000cf8]:sw t5, 1104(ra)<br> [0x80000cfc]:sw a7, 1112(ra)<br>                                 |
|  47|[0x80009828]<br>0x00000001<br> [0x80009840]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000005a and fs2 == 0 and fe2 == 0x3d1 and fm2 == 0x6400000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000d28]:fsub.d t5, t3, s10, dyn<br> [0x80000d2c]:csrrs a7, fcsr, zero<br> [0x80000d30]:sw t5, 1120(ra)<br> [0x80000d34]:sw t6, 1128(ra)<br> [0x80000d38]:sw t5, 1136(ra)<br> [0x80000d3c]:sw a7, 1144(ra)<br>                                 |
|  48|[0x80009848]<br>0x00000002<br> [0x80009860]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000005f and fs2 == 0 and fe2 == 0x3d1 and fm2 == 0x7400000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000d68]:fsub.d t5, t3, s10, dyn<br> [0x80000d6c]:csrrs a7, fcsr, zero<br> [0x80000d70]:sw t5, 1152(ra)<br> [0x80000d74]:sw t6, 1160(ra)<br> [0x80000d78]:sw t5, 1168(ra)<br> [0x80000d7c]:sw a7, 1176(ra)<br>                                 |
|  49|[0x80009868]<br>0x00000004<br> [0x80009880]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000000c and fs2 == 0 and fe2 == 0x3ce and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000da8]:fsub.d t5, t3, s10, dyn<br> [0x80000dac]:csrrs a7, fcsr, zero<br> [0x80000db0]:sw t5, 1184(ra)<br> [0x80000db4]:sw t6, 1192(ra)<br> [0x80000db8]:sw t5, 1200(ra)<br> [0x80000dbc]:sw a7, 1208(ra)<br>                                 |
|  50|[0x80009888]<br>0x00000008<br> [0x800098a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000055 and fs2 == 0 and fe2 == 0x3d1 and fm2 == 0x3400000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000de8]:fsub.d t5, t3, s10, dyn<br> [0x80000dec]:csrrs a7, fcsr, zero<br> [0x80000df0]:sw t5, 1216(ra)<br> [0x80000df4]:sw t6, 1224(ra)<br> [0x80000df8]:sw t5, 1232(ra)<br> [0x80000dfc]:sw a7, 1240(ra)<br>                                 |
|  51|[0x800098a8]<br>0x00000010<br> [0x800098c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000000e and fs2 == 1 and fe2 == 0x3cc and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000e28]:fsub.d t5, t3, s10, dyn<br> [0x80000e2c]:csrrs a7, fcsr, zero<br> [0x80000e30]:sw t5, 1248(ra)<br> [0x80000e34]:sw t6, 1256(ra)<br> [0x80000e38]:sw t5, 1264(ra)<br> [0x80000e3c]:sw a7, 1272(ra)<br>                                 |
|  52|[0x800098c8]<br>0x00000020<br> [0x800098e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000043 and fs2 == 0 and fe2 == 0x3d0 and fm2 == 0x1800000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000e68]:fsub.d t5, t3, s10, dyn<br> [0x80000e6c]:csrrs a7, fcsr, zero<br> [0x80000e70]:sw t5, 1280(ra)<br> [0x80000e74]:sw t6, 1288(ra)<br> [0x80000e78]:sw t5, 1296(ra)<br> [0x80000e7c]:sw a7, 1304(ra)<br>                                 |
|  53|[0x800098e8]<br>0x00000040<br> [0x80009900]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000030 and fs2 == 1 and fe2 == 0x3cf and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000ea8]:fsub.d t5, t3, s10, dyn<br> [0x80000eac]:csrrs a7, fcsr, zero<br> [0x80000eb0]:sw t5, 1312(ra)<br> [0x80000eb4]:sw t6, 1320(ra)<br> [0x80000eb8]:sw t5, 1328(ra)<br> [0x80000ebc]:sw a7, 1336(ra)<br>                                 |
|  54|[0x80009908]<br>0x00000080<br> [0x80009920]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000005e and fs2 == 1 and fe2 == 0x3d0 and fm2 == 0x1000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000ee8]:fsub.d t5, t3, s10, dyn<br> [0x80000eec]:csrrs a7, fcsr, zero<br> [0x80000ef0]:sw t5, 1344(ra)<br> [0x80000ef4]:sw t6, 1352(ra)<br> [0x80000ef8]:sw t5, 1360(ra)<br> [0x80000efc]:sw a7, 1368(ra)<br>                                 |
|  55|[0x80009928]<br>0x00000100<br> [0x80009940]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000003d and fs2 == 1 and fe2 == 0x3d2 and fm2 == 0x8600000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000f28]:fsub.d t5, t3, s10, dyn<br> [0x80000f2c]:csrrs a7, fcsr, zero<br> [0x80000f30]:sw t5, 1376(ra)<br> [0x80000f34]:sw t6, 1384(ra)<br> [0x80000f38]:sw t5, 1392(ra)<br> [0x80000f3c]:sw a7, 1400(ra)<br>                                 |
|  56|[0x80009948]<br>0x00000200<br> [0x80009960]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000028 and fs2 == 1 and fe2 == 0x3d3 and fm2 == 0xd800000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000f68]:fsub.d t5, t3, s10, dyn<br> [0x80000f6c]:csrrs a7, fcsr, zero<br> [0x80000f70]:sw t5, 1408(ra)<br> [0x80000f74]:sw t6, 1416(ra)<br> [0x80000f78]:sw t5, 1424(ra)<br> [0x80000f7c]:sw a7, 1432(ra)<br>                                 |
|  57|[0x80009968]<br>0x00000400<br> [0x80009980]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000004f and fs2 == 1 and fe2 == 0x3d4 and fm2 == 0xd880000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000fac]:fsub.d t5, t3, s10, dyn<br> [0x80000fb0]:csrrs a7, fcsr, zero<br> [0x80000fb4]:sw t5, 1440(ra)<br> [0x80000fb8]:sw t6, 1448(ra)<br> [0x80000fbc]:sw t5, 1456(ra)<br> [0x80000fc0]:sw a7, 1464(ra)<br>                                 |
|  58|[0x80009988]<br>0x00000800<br> [0x800099a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000004b and fs2 == 1 and fe2 == 0x3d5 and fm2 == 0xed40000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000ff0]:fsub.d t5, t3, s10, dyn<br> [0x80000ff4]:csrrs a7, fcsr, zero<br> [0x80000ff8]:sw t5, 1472(ra)<br> [0x80000ffc]:sw t6, 1480(ra)<br> [0x80001000]:sw t5, 1488(ra)<br> [0x80001004]:sw a7, 1496(ra)<br>                                 |
|  59|[0x800099a8]<br>0x00001000<br> [0x800099c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000053 and fs2 == 1 and fe2 == 0x3d6 and fm2 == 0xf5a0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001034]:fsub.d t5, t3, s10, dyn<br> [0x80001038]:csrrs a7, fcsr, zero<br> [0x8000103c]:sw t5, 1504(ra)<br> [0x80001040]:sw t6, 1512(ra)<br> [0x80001044]:sw t5, 1520(ra)<br> [0x80001048]:sw a7, 1528(ra)<br>                                 |
|  60|[0x800099c8]<br>0x00002000<br> [0x800099e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000016 and fs2 == 1 and fe2 == 0x3d7 and fm2 == 0xfea0000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001078]:fsub.d t5, t3, s10, dyn<br> [0x8000107c]:csrrs a7, fcsr, zero<br> [0x80001080]:sw t5, 1536(ra)<br> [0x80001084]:sw t6, 1544(ra)<br> [0x80001088]:sw t5, 1552(ra)<br> [0x8000108c]:sw a7, 1560(ra)<br>                                 |
|  61|[0x800099e8]<br>0x00004000<br> [0x80009a00]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000001e and fs2 == 1 and fe2 == 0x3d8 and fm2 == 0xff10000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800010bc]:fsub.d t5, t3, s10, dyn<br> [0x800010c0]:csrrs a7, fcsr, zero<br> [0x800010c4]:sw t5, 1568(ra)<br> [0x800010c8]:sw t6, 1576(ra)<br> [0x800010cc]:sw t5, 1584(ra)<br> [0x800010d0]:sw a7, 1592(ra)<br>                                 |
|  62|[0x80009a08]<br>0x00008000<br> [0x80009a20]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000063 and fs2 == 1 and fe2 == 0x3d9 and fm2 == 0xfe74000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001100]:fsub.d t5, t3, s10, dyn<br> [0x80001104]:csrrs a7, fcsr, zero<br> [0x80001108]:sw t5, 1600(ra)<br> [0x8000110c]:sw t6, 1608(ra)<br> [0x80001110]:sw t5, 1616(ra)<br> [0x80001114]:sw a7, 1624(ra)<br>                                 |
|  63|[0x80009a28]<br>0x00010000<br> [0x80009a40]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000046 and fs2 == 1 and fe2 == 0x3da and fm2 == 0xff74000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001144]:fsub.d t5, t3, s10, dyn<br> [0x80001148]:csrrs a7, fcsr, zero<br> [0x8000114c]:sw t5, 1632(ra)<br> [0x80001150]:sw t6, 1640(ra)<br> [0x80001154]:sw t5, 1648(ra)<br> [0x80001158]:sw a7, 1656(ra)<br>                                 |
|  64|[0x80009a48]<br>0x00020000<br> [0x80009a60]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000001e and fs2 == 1 and fe2 == 0x3db and fm2 == 0xffe2000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001188]:fsub.d t5, t3, s10, dyn<br> [0x8000118c]:csrrs a7, fcsr, zero<br> [0x80001190]:sw t5, 1664(ra)<br> [0x80001194]:sw t6, 1672(ra)<br> [0x80001198]:sw t5, 1680(ra)<br> [0x8000119c]:sw a7, 1688(ra)<br>                                 |
|  65|[0x80009a68]<br>0x00040000<br> [0x80009a80]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000042 and fs2 == 1 and fe2 == 0x3dc and fm2 == 0xffdf000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800011cc]:fsub.d t5, t3, s10, dyn<br> [0x800011d0]:csrrs a7, fcsr, zero<br> [0x800011d4]:sw t5, 1696(ra)<br> [0x800011d8]:sw t6, 1704(ra)<br> [0x800011dc]:sw t5, 1712(ra)<br> [0x800011e0]:sw a7, 1720(ra)<br>                                 |
|  66|[0x80009a88]<br>0x00080000<br> [0x80009aa0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000004a and fs2 == 1 and fe2 == 0x3dd and fm2 == 0xffed800000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001210]:fsub.d t5, t3, s10, dyn<br> [0x80001214]:csrrs a7, fcsr, zero<br> [0x80001218]:sw t5, 1728(ra)<br> [0x8000121c]:sw t6, 1736(ra)<br> [0x80001220]:sw t5, 1744(ra)<br> [0x80001224]:sw a7, 1752(ra)<br>                                 |
|  67|[0x80009aa8]<br>0x00100000<br> [0x80009ac0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000003b and fs2 == 1 and fe2 == 0x3de and fm2 == 0xfff8a00000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001254]:fsub.d t5, t3, s10, dyn<br> [0x80001258]:csrrs a7, fcsr, zero<br> [0x8000125c]:sw t5, 1760(ra)<br> [0x80001260]:sw t6, 1768(ra)<br> [0x80001264]:sw t5, 1776(ra)<br> [0x80001268]:sw a7, 1784(ra)<br>                                 |
|  68|[0x80009ac8]<br>0x00200000<br> [0x80009ae0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000055 and fs2 == 1 and fe2 == 0x3df and fm2 == 0xfffab00000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001298]:fsub.d t5, t3, s10, dyn<br> [0x8000129c]:csrrs a7, fcsr, zero<br> [0x800012a0]:sw t5, 1792(ra)<br> [0x800012a4]:sw t6, 1800(ra)<br> [0x800012a8]:sw t5, 1808(ra)<br> [0x800012ac]:sw a7, 1816(ra)<br>                                 |
|  69|[0x80009ae8]<br>0x00400000<br> [0x80009b00]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x000000000004e and fs2 == 1 and fe2 == 0x3e0 and fm2 == 0xfffd900000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800012dc]:fsub.d t5, t3, s10, dyn<br> [0x800012e0]:csrrs a7, fcsr, zero<br> [0x800012e4]:sw t5, 1824(ra)<br> [0x800012e8]:sw t6, 1832(ra)<br> [0x800012ec]:sw t5, 1840(ra)<br> [0x800012f0]:sw a7, 1848(ra)<br>                                 |
|  70|[0x80009b08]<br>0x00000001<br> [0x80009b20]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x8000131c]:fsub.d t5, t3, s10, dyn<br> [0x80001320]:csrrs a7, fcsr, zero<br> [0x80001324]:sw t5, 1856(ra)<br> [0x80001328]:sw t6, 1864(ra)<br> [0x8000132c]:sw t5, 1872(ra)<br> [0x80001330]:sw a7, 1880(ra)<br>                                 |
|  71|[0x80009b28]<br>0x00000001<br> [0x80009b40]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000005f and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000000030 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x8000135c]:fsub.d t5, t3, s10, dyn<br> [0x80001360]:csrrs a7, fcsr, zero<br> [0x80001364]:sw t5, 1888(ra)<br> [0x80001368]:sw t6, 1896(ra)<br> [0x8000136c]:sw t5, 1904(ra)<br> [0x80001370]:sw a7, 1912(ra)<br>                                 |
|  72|[0x80009b48]<br>0x00000003<br> [0x80009b60]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000011 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x000000000000a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x8000139c]:fsub.d t5, t3, s10, dyn<br> [0x800013a0]:csrrs a7, fcsr, zero<br> [0x800013a4]:sw t5, 1920(ra)<br> [0x800013a8]:sw t6, 1928(ra)<br> [0x800013ac]:sw t5, 1936(ra)<br> [0x800013b0]:sw a7, 1944(ra)<br>                                 |
|  73|[0x80009b68]<br>0x00000008<br> [0x80009b80]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000048 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000000028 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800013dc]:fsub.d t5, t3, s10, dyn<br> [0x800013e0]:csrrs a7, fcsr, zero<br> [0x800013e4]:sw t5, 1952(ra)<br> [0x800013e8]:sw t6, 1960(ra)<br> [0x800013ec]:sw t5, 1968(ra)<br> [0x800013f0]:sw a7, 1976(ra)<br>                                 |
|  74|[0x80009b88]<br>0x00000011<br> [0x80009ba0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000037 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000000024 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x8000141c]:fsub.d t5, t3, s10, dyn<br> [0x80001420]:csrrs a7, fcsr, zero<br> [0x80001424]:sw t5, 1984(ra)<br> [0x80001428]:sw t6, 1992(ra)<br> [0x8000142c]:sw t5, 2000(ra)<br> [0x80001430]:sw a7, 2008(ra)<br>                                 |
|  75|[0x80009ba8]<br>0x00000020<br> [0x80009bc0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000003e and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x000000000002f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x8000145c]:fsub.d t5, t3, s10, dyn<br> [0x80001460]:csrrs a7, fcsr, zero<br> [0x80001464]:sw t5, 2016(ra)<br> [0x80001468]:sw t6, 2024(ra)<br> [0x8000146c]:sw t5, 2032(ra)<br> [0x80001470]:sw a7, 2040(ra)<br>                                 |
|  76|[0x80009bc8]<br>0x0000003F<br> [0x80009be0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000049 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000000044 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x8000149c]:fsub.d t5, t3, s10, dyn<br> [0x800014a0]:csrrs a7, fcsr, zero<br> [0x800014a4]:addi ra, ra, 2040<br> [0x800014a8]:sw t5, 8(ra)<br> [0x800014ac]:sw t6, 16(ra)<br> [0x800014b0]:sw t5, 24(ra)<br> [0x800014b4]:sw a7, 32(ra)<br>       |
|  77|[0x80009be8]<br>0x00000080<br> [0x80009c00]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000001a and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x000000000004d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800014e0]:fsub.d t5, t3, s10, dyn<br> [0x800014e4]:csrrs a7, fcsr, zero<br> [0x800014e8]:sw t5, 40(ra)<br> [0x800014ec]:sw t6, 48(ra)<br> [0x800014f0]:sw t5, 56(ra)<br> [0x800014f4]:sw a7, 64(ra)<br>                                         |
|  78|[0x80009c08]<br>0x000000FF<br> [0x80009c20]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000035 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x000000000009a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001520]:fsub.d t5, t3, s10, dyn<br> [0x80001524]:csrrs a7, fcsr, zero<br> [0x80001528]:sw t5, 72(ra)<br> [0x8000152c]:sw t6, 80(ra)<br> [0x80001530]:sw t5, 88(ra)<br> [0x80001534]:sw a7, 96(ra)<br>                                         |
|  79|[0x80009c28]<br>0x00000200<br> [0x80009c40]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000002e and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000000117 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001560]:fsub.d t5, t3, s10, dyn<br> [0x80001564]:csrrs a7, fcsr, zero<br> [0x80001568]:sw t5, 104(ra)<br> [0x8000156c]:sw t6, 112(ra)<br> [0x80001570]:sw t5, 120(ra)<br> [0x80001574]:sw a7, 128(ra)<br>                                     |
|  80|[0x80009c48]<br>0x000003FF<br> [0x80009c60]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000002d and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000000216 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800015a0]:fsub.d t5, t3, s10, dyn<br> [0x800015a4]:csrrs a7, fcsr, zero<br> [0x800015a8]:sw t5, 136(ra)<br> [0x800015ac]:sw t6, 144(ra)<br> [0x800015b0]:sw t5, 152(ra)<br> [0x800015b4]:sw a7, 160(ra)<br>                                     |
|  81|[0x80009c68]<br>0x000007FF<br> [0x80009c80]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000045 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000000422 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800015e0]:fsub.d t5, t3, s10, dyn<br> [0x800015e4]:csrrs a7, fcsr, zero<br> [0x800015e8]:sw t5, 168(ra)<br> [0x800015ec]:sw t6, 176(ra)<br> [0x800015f0]:sw t5, 184(ra)<br> [0x800015f4]:sw a7, 192(ra)<br>                                     |
|  82|[0x80009c88]<br>0x00001000<br> [0x80009ca0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000050 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000000828 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001624]:fsub.d t5, t3, s10, dyn<br> [0x80001628]:csrrs a7, fcsr, zero<br> [0x8000162c]:sw t5, 200(ra)<br> [0x80001630]:sw t6, 208(ra)<br> [0x80001634]:sw t5, 216(ra)<br> [0x80001638]:sw a7, 224(ra)<br>                                     |
|  83|[0x80009ca8]<br>0x00002001<br> [0x80009cc0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000002b and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000001016 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001668]:fsub.d t5, t3, s10, dyn<br> [0x8000166c]:csrrs a7, fcsr, zero<br> [0x80001670]:sw t5, 232(ra)<br> [0x80001674]:sw t6, 240(ra)<br> [0x80001678]:sw t5, 248(ra)<br> [0x8000167c]:sw a7, 256(ra)<br>                                     |
|  84|[0x80009cc8]<br>0x00003FFF<br> [0x80009ce0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000004d and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000002026 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800016ac]:fsub.d t5, t3, s10, dyn<br> [0x800016b0]:csrrs a7, fcsr, zero<br> [0x800016b4]:sw t5, 264(ra)<br> [0x800016b8]:sw t6, 272(ra)<br> [0x800016bc]:sw t5, 280(ra)<br> [0x800016c0]:sw a7, 288(ra)<br>                                     |
|  85|[0x80009ce8]<br>0x00008000<br> [0x80009d00]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000001e and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x000000000400f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800016f0]:fsub.d t5, t3, s10, dyn<br> [0x800016f4]:csrrs a7, fcsr, zero<br> [0x800016f8]:sw t5, 296(ra)<br> [0x800016fc]:sw t6, 304(ra)<br> [0x80001700]:sw t5, 312(ra)<br> [0x80001704]:sw a7, 320(ra)<br>                                     |
|  86|[0x80009d08]<br>0x00010001<br> [0x80009d20]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000017 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x000000000800c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001734]:fsub.d t5, t3, s10, dyn<br> [0x80001738]:csrrs a7, fcsr, zero<br> [0x8000173c]:sw t5, 328(ra)<br> [0x80001740]:sw t6, 336(ra)<br> [0x80001744]:sw t5, 344(ra)<br> [0x80001748]:sw a7, 352(ra)<br>                                     |
|  87|[0x80009d28]<br>0x00020001<br> [0x80009d40]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000004b and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000010026 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001778]:fsub.d t5, t3, s10, dyn<br> [0x8000177c]:csrrs a7, fcsr, zero<br> [0x80001780]:sw t5, 360(ra)<br> [0x80001784]:sw t6, 368(ra)<br> [0x80001788]:sw t5, 376(ra)<br> [0x8000178c]:sw a7, 384(ra)<br>                                     |
|  88|[0x80009d48]<br>0x00040000<br> [0x80009d60]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000000c and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000020006 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800017bc]:fsub.d t5, t3, s10, dyn<br> [0x800017c0]:csrrs a7, fcsr, zero<br> [0x800017c4]:sw t5, 392(ra)<br> [0x800017c8]:sw t6, 400(ra)<br> [0x800017cc]:sw t5, 408(ra)<br> [0x800017d0]:sw a7, 416(ra)<br>                                     |
|  89|[0x80009d68]<br>0x0007FFFF<br> [0x80009d80]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000021 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000040010 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001800]:fsub.d t5, t3, s10, dyn<br> [0x80001804]:csrrs a7, fcsr, zero<br> [0x80001808]:sw t5, 424(ra)<br> [0x8000180c]:sw t6, 432(ra)<br> [0x80001810]:sw t5, 440(ra)<br> [0x80001814]:sw a7, 448(ra)<br>                                     |
|  90|[0x80009d88]<br>0x00100001<br> [0x80009da0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000057 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x000000008002c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001844]:fsub.d t5, t3, s10, dyn<br> [0x80001848]:csrrs a7, fcsr, zero<br> [0x8000184c]:sw t5, 456(ra)<br> [0x80001850]:sw t6, 464(ra)<br> [0x80001854]:sw t5, 472(ra)<br> [0x80001858]:sw a7, 480(ra)<br>                                     |
|  91|[0x80009da8]<br>0x00200001<br> [0x80009dc0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000000b and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x0000000100006 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001888]:fsub.d t5, t3, s10, dyn<br> [0x8000188c]:csrrs a7, fcsr, zero<br> [0x80001890]:sw t5, 488(ra)<br> [0x80001894]:sw t6, 496(ra)<br> [0x80001898]:sw t5, 504(ra)<br> [0x8000189c]:sw a7, 512(ra)<br>                                     |
|  92|[0x80009dc8]<br>0x00400000<br> [0x80009de0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000003a and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x000000020001d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800018cc]:fsub.d t5, t3, s10, dyn<br> [0x800018d0]:csrrs a7, fcsr, zero<br> [0x800018d4]:sw t5, 520(ra)<br> [0x800018d8]:sw t6, 528(ra)<br> [0x800018dc]:sw t5, 536(ra)<br> [0x800018e0]:sw a7, 544(ra)<br>                                     |
|  93|[0x80009de8]<br>0x00000000<br> [0x80009e00]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000061 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000061 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x8000190c]:fsub.d t5, t3, s10, dyn<br> [0x80001910]:csrrs a7, fcsr, zero<br> [0x80001914]:sw t5, 552(ra)<br> [0x80001918]:sw t6, 560(ra)<br> [0x8000191c]:sw t5, 568(ra)<br> [0x80001920]:sw a7, 576(ra)<br>                                     |
|  94|[0x80009e08]<br>0x00000003<br> [0x80009e20]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000024 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000021 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x8000194c]:fsub.d t5, t3, s10, dyn<br> [0x80001950]:csrrs a7, fcsr, zero<br> [0x80001954]:sw t5, 584(ra)<br> [0x80001958]:sw t6, 592(ra)<br> [0x8000195c]:sw t5, 600(ra)<br> [0x80001960]:sw a7, 608(ra)<br>                                     |
|  95|[0x80009e28]<br>0x00000005<br> [0x80009e40]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000023 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000000001e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x8000198c]:fsub.d t5, t3, s10, dyn<br> [0x80001990]:csrrs a7, fcsr, zero<br> [0x80001994]:sw t5, 616(ra)<br> [0x80001998]:sw t6, 624(ra)<br> [0x8000199c]:sw t5, 632(ra)<br> [0x800019a0]:sw a7, 640(ra)<br>                                     |
|  96|[0x80009e48]<br>0x00000009<br> [0x80009e60]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000050 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000047 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800019cc]:fsub.d t5, t3, s10, dyn<br> [0x800019d0]:csrrs a7, fcsr, zero<br> [0x800019d4]:sw t5, 648(ra)<br> [0x800019d8]:sw t6, 656(ra)<br> [0x800019dc]:sw t5, 664(ra)<br> [0x800019e0]:sw a7, 672(ra)<br>                                     |
|  97|[0x80009e68]<br>0x00000011<br> [0x80009e80]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000002d and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000000001c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001a0c]:fsub.d t5, t3, s10, dyn<br> [0x80001a10]:csrrs a7, fcsr, zero<br> [0x80001a14]:sw t5, 680(ra)<br> [0x80001a18]:sw t6, 688(ra)<br> [0x80001a1c]:sw t5, 696(ra)<br> [0x80001a20]:sw a7, 704(ra)<br>                                     |
|  98|[0x80009e88]<br>0x00000021<br> [0x80009ea0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000009 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000018 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001a4c]:fsub.d t5, t3, s10, dyn<br> [0x80001a50]:csrrs a7, fcsr, zero<br> [0x80001a54]:sw t5, 712(ra)<br> [0x80001a58]:sw t6, 720(ra)<br> [0x80001a5c]:sw t5, 728(ra)<br> [0x80001a60]:sw a7, 736(ra)<br>                                     |
|  99|[0x80009ea8]<br>0x00000041<br> [0x80009ec0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000015 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000000002c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001a8c]:fsub.d t5, t3, s10, dyn<br> [0x80001a90]:csrrs a7, fcsr, zero<br> [0x80001a94]:sw t5, 744(ra)<br> [0x80001a98]:sw t6, 752(ra)<br> [0x80001a9c]:sw t5, 760(ra)<br> [0x80001aa0]:sw a7, 768(ra)<br>                                     |
| 100|[0x80009ec8]<br>0x00000081<br> [0x80009ee0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000044 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000000003d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001acc]:fsub.d t5, t3, s10, dyn<br> [0x80001ad0]:csrrs a7, fcsr, zero<br> [0x80001ad4]:sw t5, 776(ra)<br> [0x80001ad8]:sw t6, 784(ra)<br> [0x80001adc]:sw t5, 792(ra)<br> [0x80001ae0]:sw a7, 800(ra)<br>                                     |
| 101|[0x80009ee8]<br>0x00000101<br> [0x80009f00]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000055 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000000000ac and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001b0c]:fsub.d t5, t3, s10, dyn<br> [0x80001b10]:csrrs a7, fcsr, zero<br> [0x80001b14]:sw t5, 808(ra)<br> [0x80001b18]:sw t6, 816(ra)<br> [0x80001b1c]:sw t5, 824(ra)<br> [0x80001b20]:sw a7, 832(ra)<br>                                     |
| 102|[0x80009f08]<br>0x00000201<br> [0x80009f20]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000053 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000000001ae and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001b4c]:fsub.d t5, t3, s10, dyn<br> [0x80001b50]:csrrs a7, fcsr, zero<br> [0x80001b54]:sw t5, 840(ra)<br> [0x80001b58]:sw t6, 848(ra)<br> [0x80001b5c]:sw t5, 856(ra)<br> [0x80001b60]:sw a7, 864(ra)<br>                                     |
| 103|[0x80009f28]<br>0x00000401<br> [0x80009f40]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000026 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000000003db and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001b8c]:fsub.d t5, t3, s10, dyn<br> [0x80001b90]:csrrs a7, fcsr, zero<br> [0x80001b94]:sw t5, 872(ra)<br> [0x80001b98]:sw t6, 880(ra)<br> [0x80001b9c]:sw t5, 888(ra)<br> [0x80001ba0]:sw a7, 896(ra)<br>                                     |
| 104|[0x80009f48]<br>0x00000801<br> [0x80009f60]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005a and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000000007a7 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001bcc]:fsub.d t5, t3, s10, dyn<br> [0x80001bd0]:csrrs a7, fcsr, zero<br> [0x80001bd4]:sw t5, 904(ra)<br> [0x80001bd8]:sw t6, 912(ra)<br> [0x80001bdc]:sw t5, 920(ra)<br> [0x80001be0]:sw a7, 928(ra)<br>                                     |
| 105|[0x80009f68]<br>0x00001001<br> [0x80009f80]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000040 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000fc1 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001c10]:fsub.d t5, t3, s10, dyn<br> [0x80001c14]:csrrs a7, fcsr, zero<br> [0x80001c18]:sw t5, 936(ra)<br> [0x80001c1c]:sw t6, 944(ra)<br> [0x80001c20]:sw t5, 952(ra)<br> [0x80001c24]:sw a7, 960(ra)<br>                                     |
| 106|[0x80009f88]<br>0x00002001<br> [0x80009fa0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000f and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000001ff2 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001c54]:fsub.d t5, t3, s10, dyn<br> [0x80001c58]:csrrs a7, fcsr, zero<br> [0x80001c5c]:sw t5, 968(ra)<br> [0x80001c60]:sw t6, 976(ra)<br> [0x80001c64]:sw t5, 984(ra)<br> [0x80001c68]:sw a7, 992(ra)<br>                                     |
| 107|[0x80009fa8]<br>0x00004001<br> [0x80009fc0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000028 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000003fd9 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001c98]:fsub.d t5, t3, s10, dyn<br> [0x80001c9c]:csrrs a7, fcsr, zero<br> [0x80001ca0]:sw t5, 1000(ra)<br> [0x80001ca4]:sw t6, 1008(ra)<br> [0x80001ca8]:sw t5, 1016(ra)<br> [0x80001cac]:sw a7, 1024(ra)<br>                                 |
| 108|[0x80009fc8]<br>0x00008001<br> [0x80009fe0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000002c and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000007fd5 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001cdc]:fsub.d t5, t3, s10, dyn<br> [0x80001ce0]:csrrs a7, fcsr, zero<br> [0x80001ce4]:sw t5, 1032(ra)<br> [0x80001ce8]:sw t6, 1040(ra)<br> [0x80001cec]:sw t5, 1048(ra)<br> [0x80001cf0]:sw a7, 1056(ra)<br>                                 |
| 109|[0x80009fe8]<br>0x00010001<br> [0x8000a000]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000019 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000000ffe8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001d20]:fsub.d t5, t3, s10, dyn<br> [0x80001d24]:csrrs a7, fcsr, zero<br> [0x80001d28]:sw t5, 1064(ra)<br> [0x80001d2c]:sw t6, 1072(ra)<br> [0x80001d30]:sw t5, 1080(ra)<br> [0x80001d34]:sw a7, 1088(ra)<br>                                 |
| 110|[0x8000a008]<br>0x00020001<br> [0x8000a020]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000e and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000001fff3 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001d64]:fsub.d t5, t3, s10, dyn<br> [0x80001d68]:csrrs a7, fcsr, zero<br> [0x80001d6c]:sw t5, 1096(ra)<br> [0x80001d70]:sw t6, 1104(ra)<br> [0x80001d74]:sw t5, 1112(ra)<br> [0x80001d78]:sw a7, 1120(ra)<br>                                 |
| 111|[0x8000a028]<br>0x00040001<br> [0x8000a040]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005e and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000003ffa3 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001da8]:fsub.d t5, t3, s10, dyn<br> [0x80001dac]:csrrs a7, fcsr, zero<br> [0x80001db0]:sw t5, 1128(ra)<br> [0x80001db4]:sw t6, 1136(ra)<br> [0x80001db8]:sw t5, 1144(ra)<br> [0x80001dbc]:sw a7, 1152(ra)<br>                                 |
| 112|[0x8000a048]<br>0x00080001<br> [0x8000a060]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001b and fs2 == 1 and fe2 == 0x000 and fm2 == 0x000000007ffe6 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001dec]:fsub.d t5, t3, s10, dyn<br> [0x80001df0]:csrrs a7, fcsr, zero<br> [0x80001df4]:sw t5, 1160(ra)<br> [0x80001df8]:sw t6, 1168(ra)<br> [0x80001dfc]:sw t5, 1176(ra)<br> [0x80001e00]:sw a7, 1184(ra)<br>                                 |
| 113|[0x8000a068]<br>0x00100001<br> [0x8000a080]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000038 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000000fffc9 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001e30]:fsub.d t5, t3, s10, dyn<br> [0x80001e34]:csrrs a7, fcsr, zero<br> [0x80001e38]:sw t5, 1192(ra)<br> [0x80001e3c]:sw t6, 1200(ra)<br> [0x80001e40]:sw t5, 1208(ra)<br> [0x80001e44]:sw a7, 1216(ra)<br>                                 |
| 114|[0x8000a088]<br>0x00200001<br> [0x8000a0a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001d and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000001fffe4 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001e74]:fsub.d t5, t3, s10, dyn<br> [0x80001e78]:csrrs a7, fcsr, zero<br> [0x80001e7c]:sw t5, 1224(ra)<br> [0x80001e80]:sw t6, 1232(ra)<br> [0x80001e84]:sw t5, 1240(ra)<br> [0x80001e88]:sw a7, 1248(ra)<br>                                 |
| 115|[0x8000a0a8]<br>0x00400001<br> [0x8000a0c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000033 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x00000003fffce and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001eb8]:fsub.d t5, t3, s10, dyn<br> [0x80001ebc]:csrrs a7, fcsr, zero<br> [0x80001ec0]:sw t5, 1256(ra)<br> [0x80001ec4]:sw t6, 1264(ra)<br> [0x80001ec8]:sw t5, 1272(ra)<br> [0x80001ecc]:sw a7, 1280(ra)<br>                                 |
| 116|[0x8000a0c8]<br>0x00000000<br> [0x8000a0e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000005 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000005 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001ef8]:fsub.d t5, t3, s10, dyn<br> [0x80001efc]:csrrs a7, fcsr, zero<br> [0x80001f00]:sw t5, 1288(ra)<br> [0x80001f04]:sw t6, 1296(ra)<br> [0x80001f08]:sw t5, 1304(ra)<br> [0x80001f0c]:sw a7, 1312(ra)<br>                                 |
| 117|[0x8000a0e8]<br>0x00000003<br> [0x8000a100]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000015 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000018 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001f38]:fsub.d t5, t3, s10, dyn<br> [0x80001f3c]:csrrs a7, fcsr, zero<br> [0x80001f40]:sw t5, 1320(ra)<br> [0x80001f44]:sw t6, 1328(ra)<br> [0x80001f48]:sw t5, 1336(ra)<br> [0x80001f4c]:sw a7, 1344(ra)<br>                                 |
| 118|[0x8000a108]<br>0x00000005<br> [0x8000a120]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005b and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000060 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001f78]:fsub.d t5, t3, s10, dyn<br> [0x80001f7c]:csrrs a7, fcsr, zero<br> [0x80001f80]:sw t5, 1352(ra)<br> [0x80001f84]:sw t6, 1360(ra)<br> [0x80001f88]:sw t5, 1368(ra)<br> [0x80001f8c]:sw a7, 1376(ra)<br>                                 |
| 119|[0x8000a128]<br>0x00000009<br> [0x8000a140]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000057 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000060 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001fb8]:fsub.d t5, t3, s10, dyn<br> [0x80001fbc]:csrrs a7, fcsr, zero<br> [0x80001fc0]:sw t5, 1384(ra)<br> [0x80001fc4]:sw t6, 1392(ra)<br> [0x80001fc8]:sw t5, 1400(ra)<br> [0x80001fcc]:sw a7, 1408(ra)<br>                                 |
| 120|[0x8000a148]<br>0x00000011<br> [0x8000a160]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000046 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000057 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80001ff8]:fsub.d t5, t3, s10, dyn<br> [0x80001ffc]:csrrs a7, fcsr, zero<br> [0x80002000]:sw t5, 1416(ra)<br> [0x80002004]:sw t6, 1424(ra)<br> [0x80002008]:sw t5, 1432(ra)<br> [0x8000200c]:sw a7, 1440(ra)<br>                                 |
| 121|[0x8000a168]<br>0x00000021<br> [0x8000a180]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000051 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000072 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80002038]:fsub.d t5, t3, s10, dyn<br> [0x8000203c]:csrrs a7, fcsr, zero<br> [0x80002040]:sw t5, 1448(ra)<br> [0x80002044]:sw t6, 1456(ra)<br> [0x80002048]:sw t5, 1464(ra)<br> [0x8000204c]:sw a7, 1472(ra)<br>                                 |
| 122|[0x8000a188]<br>0x00000041<br> [0x8000a1a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000043 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000084 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80002078]:fsub.d t5, t3, s10, dyn<br> [0x8000207c]:csrrs a7, fcsr, zero<br> [0x80002080]:sw t5, 1480(ra)<br> [0x80002084]:sw t6, 1488(ra)<br> [0x80002088]:sw t5, 1496(ra)<br> [0x8000208c]:sw a7, 1504(ra)<br>                                 |
| 123|[0x8000a1a8]<br>0x00000081<br> [0x8000a1c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001d and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000000009e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800020b8]:fsub.d t5, t3, s10, dyn<br> [0x800020bc]:csrrs a7, fcsr, zero<br> [0x800020c0]:sw t5, 1512(ra)<br> [0x800020c4]:sw t6, 1520(ra)<br> [0x800020c8]:sw t5, 1528(ra)<br> [0x800020cc]:sw a7, 1536(ra)<br>                                 |
| 124|[0x8000a1c8]<br>0x00000101<br> [0x8000a1e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000054 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000155 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800020f8]:fsub.d t5, t3, s10, dyn<br> [0x800020fc]:csrrs a7, fcsr, zero<br> [0x80002100]:sw t5, 1544(ra)<br> [0x80002104]:sw t6, 1552(ra)<br> [0x80002108]:sw t5, 1560(ra)<br> [0x8000210c]:sw a7, 1568(ra)<br>                                 |
| 125|[0x8000a1e8]<br>0x00000201<br> [0x8000a200]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000033 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000234 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80002138]:fsub.d t5, t3, s10, dyn<br> [0x8000213c]:csrrs a7, fcsr, zero<br> [0x80002140]:sw t5, 1576(ra)<br> [0x80002144]:sw t6, 1584(ra)<br> [0x80002148]:sw t5, 1592(ra)<br> [0x8000214c]:sw a7, 1600(ra)<br>                                 |
| 126|[0x8000a208]<br>0x00000401<br> [0x8000a220]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000004a and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000000044b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80002178]:fsub.d t5, t3, s10, dyn<br> [0x8000217c]:csrrs a7, fcsr, zero<br> [0x80002180]:sw t5, 1608(ra)<br> [0x80002184]:sw t6, 1616(ra)<br> [0x80002188]:sw t5, 1624(ra)<br> [0x8000218c]:sw a7, 1632(ra)<br>                                 |
| 127|[0x8000a228]<br>0x00000801<br> [0x8000a240]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000055 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000856 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800021bc]:fsub.d t5, t3, s10, dyn<br> [0x800021c0]:csrrs a7, fcsr, zero<br> [0x800021c4]:sw t5, 1640(ra)<br> [0x800021c8]:sw t6, 1648(ra)<br> [0x800021cc]:sw t5, 1656(ra)<br> [0x800021d0]:sw a7, 1664(ra)<br>                                 |
| 128|[0x8000a248]<br>0x00001001<br> [0x8000a260]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000037 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000001038 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80002200]:fsub.d t5, t3, s10, dyn<br> [0x80002204]:csrrs a7, fcsr, zero<br> [0x80002208]:sw t5, 1672(ra)<br> [0x8000220c]:sw t6, 1680(ra)<br> [0x80002210]:sw t5, 1688(ra)<br> [0x80002214]:sw a7, 1696(ra)<br>                                 |
| 129|[0x8000a268]<br>0x00002001<br> [0x8000a280]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005f and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000002060 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80002244]:fsub.d t5, t3, s10, dyn<br> [0x80002248]:csrrs a7, fcsr, zero<br> [0x8000224c]:sw t5, 1704(ra)<br> [0x80002250]:sw t6, 1712(ra)<br> [0x80002254]:sw t5, 1720(ra)<br> [0x80002258]:sw a7, 1728(ra)<br>                                 |
| 130|[0x8000a288]<br>0x00004001<br> [0x8000a2a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000011 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000004012 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80002288]:fsub.d t5, t3, s10, dyn<br> [0x8000228c]:csrrs a7, fcsr, zero<br> [0x80002290]:sw t5, 1736(ra)<br> [0x80002294]:sw t6, 1744(ra)<br> [0x80002298]:sw t5, 1752(ra)<br> [0x8000229c]:sw a7, 1760(ra)<br>                                 |
| 131|[0x8000a2a8]<br>0x00008001<br> [0x8000a2c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000007 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000008008 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800022cc]:fsub.d t5, t3, s10, dyn<br> [0x800022d0]:csrrs a7, fcsr, zero<br> [0x800022d4]:sw t5, 1768(ra)<br> [0x800022d8]:sw t6, 1776(ra)<br> [0x800022dc]:sw t5, 1784(ra)<br> [0x800022e0]:sw a7, 1792(ra)<br>                                 |
| 132|[0x8000a2c8]<br>0x00010001<br> [0x8000a2e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000a and fs2 == 0 and fe2 == 0x000 and fm2 == 0x000000001000b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80002310]:fsub.d t5, t3, s10, dyn<br> [0x80002314]:csrrs a7, fcsr, zero<br> [0x80002318]:sw t5, 1800(ra)<br> [0x8000231c]:sw t6, 1808(ra)<br> [0x80002320]:sw t5, 1816(ra)<br> [0x80002324]:sw a7, 1824(ra)<br>                                 |
| 133|[0x8000a2e8]<br>0x00020001<br> [0x8000a300]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000028 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000020029 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80002354]:fsub.d t5, t3, s10, dyn<br> [0x80002358]:csrrs a7, fcsr, zero<br> [0x8000235c]:sw t5, 1832(ra)<br> [0x80002360]:sw t6, 1840(ra)<br> [0x80002364]:sw t5, 1848(ra)<br> [0x80002368]:sw a7, 1856(ra)<br>                                 |
| 134|[0x8000a308]<br>0x00040001<br> [0x8000a320]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000060 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000040061 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80002398]:fsub.d t5, t3, s10, dyn<br> [0x8000239c]:csrrs a7, fcsr, zero<br> [0x800023a0]:sw t5, 1864(ra)<br> [0x800023a4]:sw t6, 1872(ra)<br> [0x800023a8]:sw t5, 1880(ra)<br> [0x800023ac]:sw a7, 1888(ra)<br>                                 |
| 135|[0x8000a328]<br>0x00080001<br> [0x8000a340]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000036 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000080037 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800023dc]:fsub.d t5, t3, s10, dyn<br> [0x800023e0]:csrrs a7, fcsr, zero<br> [0x800023e4]:sw t5, 1896(ra)<br> [0x800023e8]:sw t6, 1904(ra)<br> [0x800023ec]:sw t5, 1912(ra)<br> [0x800023f0]:sw a7, 1920(ra)<br>                                 |
| 136|[0x8000a348]<br>0x00100001<br> [0x8000a360]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000021 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000100022 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80002420]:fsub.d t5, t3, s10, dyn<br> [0x80002424]:csrrs a7, fcsr, zero<br> [0x80002428]:sw t5, 1928(ra)<br> [0x8000242c]:sw t6, 1936(ra)<br> [0x80002430]:sw t5, 1944(ra)<br> [0x80002434]:sw a7, 1952(ra)<br>                                 |
| 137|[0x8000a368]<br>0x00200001<br> [0x8000a380]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000200003 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80002464]:fsub.d t5, t3, s10, dyn<br> [0x80002468]:csrrs a7, fcsr, zero<br> [0x8000246c]:sw t5, 1960(ra)<br> [0x80002470]:sw t6, 1968(ra)<br> [0x80002474]:sw t5, 1976(ra)<br> [0x80002478]:sw a7, 1984(ra)<br>                                 |
| 138|[0x8000a388]<br>0x00400001<br> [0x8000a3a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000005 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000400006 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800024a8]:fsub.d t5, t3, s10, dyn<br> [0x800024ac]:csrrs a7, fcsr, zero<br> [0x800024b0]:sw t5, 1992(ra)<br> [0x800024b4]:sw t6, 2000(ra)<br> [0x800024b8]:sw t5, 2008(ra)<br> [0x800024bc]:sw a7, 2016(ra)<br>                                 |
| 139|[0x8000a3a8]<br>0xFFFFFFFE<br> [0x8000a3c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001c and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffe2 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800024ec]:fsub.d t5, t3, s10, dyn<br> [0x800024f0]:csrrs a7, fcsr, zero<br> [0x800024f4]:sw t5, 2024(ra)<br> [0x800024f8]:sw t6, 2032(ra)<br> [0x800024fc]:sw t5, 2040(ra)<br> [0x80002500]:addi ra, ra, 2040<br> [0x80002504]:sw a7, 8(ra)<br> |
| 140|[0x8000a3c8]<br>0xFFFFFFFD<br> [0x8000a3e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003b and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffc2 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80002574]:fsub.d t5, t3, s10, dyn<br> [0x80002578]:csrrs a7, fcsr, zero<br> [0x8000257c]:sw t5, 16(ra)<br> [0x80002580]:sw t6, 24(ra)<br> [0x80002584]:sw t5, 32(ra)<br> [0x80002588]:sw a7, 40(ra)<br>                                         |
| 141|[0x8000a3e8]<br>0xFFFFFFFB<br> [0x8000a400]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005b and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffa0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800025f8]:fsub.d t5, t3, s10, dyn<br> [0x800025fc]:csrrs a7, fcsr, zero<br> [0x80002600]:sw t5, 48(ra)<br> [0x80002604]:sw t6, 56(ra)<br> [0x80002608]:sw t5, 64(ra)<br> [0x8000260c]:sw a7, 72(ra)<br>                                         |
| 142|[0x8000a408]<br>0xFFFFFFF7<br> [0x8000a420]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000042 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffb5 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x8000267c]:fsub.d t5, t3, s10, dyn<br> [0x80002680]:csrrs a7, fcsr, zero<br> [0x80002684]:sw t5, 80(ra)<br> [0x80002688]:sw t6, 88(ra)<br> [0x8000268c]:sw t5, 96(ra)<br> [0x80002690]:sw a7, 104(ra)<br>                                        |
| 143|[0x8000a428]<br>0xFFFFFFEF<br> [0x8000a440]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000031 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffbe and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80002700]:fsub.d t5, t3, s10, dyn<br> [0x80002704]:csrrs a7, fcsr, zero<br> [0x80002708]:sw t5, 112(ra)<br> [0x8000270c]:sw t6, 120(ra)<br> [0x80002710]:sw t5, 128(ra)<br> [0x80002714]:sw a7, 136(ra)<br>                                     |
| 144|[0x8000a448]<br>0xFFFFFFDF<br> [0x8000a460]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000002d and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffb2 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80002784]:fsub.d t5, t3, s10, dyn<br> [0x80002788]:csrrs a7, fcsr, zero<br> [0x8000278c]:sw t5, 144(ra)<br> [0x80002790]:sw t6, 152(ra)<br> [0x80002794]:sw t5, 160(ra)<br> [0x80002798]:sw a7, 168(ra)<br>                                     |
| 145|[0x8000a3c8]<br>0x00000000<br> [0x8000a3e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000056 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80006724]:fsub.d t5, t3, s10, dyn<br> [0x80006728]:csrrs a7, fcsr, zero<br> [0x8000672c]:sw t5, 0(ra)<br> [0x80006730]:sw t6, 8(ra)<br> [0x80006734]:sw t5, 16(ra)<br> [0x80006738]:sw a7, 24(ra)<br>                                           |
| 146|[0x8000a3e8]<br>0x00000000<br> [0x8000a400]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000021 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80006764]:fsub.d t5, t3, s10, dyn<br> [0x80006768]:csrrs a7, fcsr, zero<br> [0x8000676c]:sw t5, 32(ra)<br> [0x80006770]:sw t6, 40(ra)<br> [0x80006774]:sw t5, 48(ra)<br> [0x80006778]:sw a7, 56(ra)<br>                                         |
| 147|[0x8000a408]<br>0x00000000<br> [0x8000a420]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000058 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800067a4]:fsub.d t5, t3, s10, dyn<br> [0x800067a8]:csrrs a7, fcsr, zero<br> [0x800067ac]:sw t5, 64(ra)<br> [0x800067b0]:sw t6, 72(ra)<br> [0x800067b4]:sw t5, 80(ra)<br> [0x800067b8]:sw a7, 88(ra)<br>                                         |
| 148|[0x8000a428]<br>0x00000000<br> [0x8000a440]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000038 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800067e4]:fsub.d t5, t3, s10, dyn<br> [0x800067e8]:csrrs a7, fcsr, zero<br> [0x800067ec]:sw t5, 96(ra)<br> [0x800067f0]:sw t6, 104(ra)<br> [0x800067f4]:sw t5, 112(ra)<br> [0x800067f8]:sw a7, 120(ra)<br>                                      |
| 149|[0x8000a448]<br>0x00000000<br> [0x8000a460]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000039 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80006824]:fsub.d t5, t3, s10, dyn<br> [0x80006828]:csrrs a7, fcsr, zero<br> [0x8000682c]:sw t5, 128(ra)<br> [0x80006830]:sw t6, 136(ra)<br> [0x80006834]:sw t5, 144(ra)<br> [0x80006838]:sw a7, 152(ra)<br>                                     |
