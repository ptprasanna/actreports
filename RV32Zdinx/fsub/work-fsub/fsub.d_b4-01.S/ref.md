
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80002d90')]      |
| SIG_REGION                | [('0x80004a10', '0x80005300', '572 words')]      |
| COV_LABELS                | fsub.d_b4      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-fsub/fsub.d_b4-01.S/ref.S    |
| Total Number of coverpoints| 191     |
| Total Coverpoints Hit     | 191      |
| Total Signature Updates   | 308      |
| STAT1                     | 77      |
| STAT2                     | 0      |
| STAT3                     | 65     |
| STAT4                     | 154     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80001970]:fsub.d t5, t3, s10, dyn
[0x80001974]:csrrs a7, fcsr, zero
[0x80001978]:sw t5, 72(ra)
[0x8000197c]:sw t6, 80(ra)
[0x80001980]:sw t5, 88(ra)
[0x80001984]:sw a7, 96(ra)
[0x80001988]:lw t3, 1072(a6)
[0x8000198c]:lw t4, 1076(a6)
[0x80001990]:lw s10, 1080(a6)
[0x80001994]:lw s11, 1084(a6)
[0x80001998]:lui t3, 491082
[0x8000199c]:addi t3, t3, 3132
[0x800019a0]:lui t4, 523889
[0x800019a4]:addi t4, t4, 1638
[0x800019a8]:lui s10, 213868
[0x800019ac]:addi s10, s10, 802
[0x800019b0]:lui s11, 523933
[0x800019b4]:addi s11, s11, 3230
[0x800019b8]:addi a4, zero, 96
[0x800019bc]:csrrw zero, fcsr, a4
[0x800019c0]:fsub.d t5, t3, s10, dyn
[0x800019c4]:csrrs a7, fcsr, zero

[0x800019c0]:fsub.d t5, t3, s10, dyn
[0x800019c4]:csrrs a7, fcsr, zero
[0x800019c8]:sw t5, 104(ra)
[0x800019cc]:sw t6, 112(ra)
[0x800019d0]:sw t5, 120(ra)
[0x800019d4]:sw a7, 128(ra)
[0x800019d8]:lw t3, 1088(a6)
[0x800019dc]:lw t4, 1092(a6)
[0x800019e0]:lw s10, 1096(a6)
[0x800019e4]:lw s11, 1100(a6)
[0x800019e8]:lui t3, 491082
[0x800019ec]:addi t3, t3, 3132
[0x800019f0]:lui t4, 523889
[0x800019f4]:addi t4, t4, 1638
[0x800019f8]:lui s10, 213868
[0x800019fc]:addi s10, s10, 802
[0x80001a00]:lui s11, 523933
[0x80001a04]:addi s11, s11, 3230
[0x80001a08]:addi a4, zero, 128
[0x80001a0c]:csrrw zero, fcsr, a4
[0x80001a10]:fsub.d t5, t3, s10, dyn
[0x80001a14]:csrrs a7, fcsr, zero

[0x80001a10]:fsub.d t5, t3, s10, dyn
[0x80001a14]:csrrs a7, fcsr, zero
[0x80001a18]:sw t5, 136(ra)
[0x80001a1c]:sw t6, 144(ra)
[0x80001a20]:sw t5, 152(ra)
[0x80001a24]:sw a7, 160(ra)
[0x80001a28]:lw t3, 1104(a6)
[0x80001a2c]:lw t4, 1108(a6)
[0x80001a30]:lw s10, 1112(a6)
[0x80001a34]:lw s11, 1116(a6)
[0x80001a38]:lui t3, 261607
[0x80001a3c]:addi t3, t3, 3165
[0x80001a40]:lui t4, 523999
[0x80001a44]:addi t4, t4, 1874
[0x80001a48]:lui s10, 107494
[0x80001a4c]:addi s10, s10, 3189
[0x80001a50]:lui s11, 523960
[0x80001a54]:addi s11, s11, 1443
[0x80001a58]:addi a4, zero, 0
[0x80001a5c]:csrrw zero, fcsr, a4
[0x80001a60]:fsub.d t5, t3, s10, dyn
[0x80001a64]:csrrs a7, fcsr, zero

[0x80001a60]:fsub.d t5, t3, s10, dyn
[0x80001a64]:csrrs a7, fcsr, zero
[0x80001a68]:sw t5, 168(ra)
[0x80001a6c]:sw t6, 176(ra)
[0x80001a70]:sw t5, 184(ra)
[0x80001a74]:sw a7, 192(ra)
[0x80001a78]:lw t3, 1120(a6)
[0x80001a7c]:lw t4, 1124(a6)
[0x80001a80]:lw s10, 1128(a6)
[0x80001a84]:lw s11, 1132(a6)
[0x80001a88]:lui t3, 261607
[0x80001a8c]:addi t3, t3, 3165
[0x80001a90]:lui t4, 523999
[0x80001a94]:addi t4, t4, 1874
[0x80001a98]:lui s10, 107494
[0x80001a9c]:addi s10, s10, 3189
[0x80001aa0]:lui s11, 523960
[0x80001aa4]:addi s11, s11, 1443
[0x80001aa8]:addi a4, zero, 32
[0x80001aac]:csrrw zero, fcsr, a4
[0x80001ab0]:fsub.d t5, t3, s10, dyn
[0x80001ab4]:csrrs a7, fcsr, zero

[0x80001ab0]:fsub.d t5, t3, s10, dyn
[0x80001ab4]:csrrs a7, fcsr, zero
[0x80001ab8]:sw t5, 200(ra)
[0x80001abc]:sw t6, 208(ra)
[0x80001ac0]:sw t5, 216(ra)
[0x80001ac4]:sw a7, 224(ra)
[0x80001ac8]:lw t3, 1136(a6)
[0x80001acc]:lw t4, 1140(a6)
[0x80001ad0]:lw s10, 1144(a6)
[0x80001ad4]:lw s11, 1148(a6)
[0x80001ad8]:lui t3, 261607
[0x80001adc]:addi t3, t3, 3165
[0x80001ae0]:lui t4, 523999
[0x80001ae4]:addi t4, t4, 1874
[0x80001ae8]:lui s10, 107494
[0x80001aec]:addi s10, s10, 3189
[0x80001af0]:lui s11, 523960
[0x80001af4]:addi s11, s11, 1443
[0x80001af8]:addi a4, zero, 64
[0x80001afc]:csrrw zero, fcsr, a4
[0x80001b00]:fsub.d t5, t3, s10, dyn
[0x80001b04]:csrrs a7, fcsr, zero

[0x80001b00]:fsub.d t5, t3, s10, dyn
[0x80001b04]:csrrs a7, fcsr, zero
[0x80001b08]:sw t5, 232(ra)
[0x80001b0c]:sw t6, 240(ra)
[0x80001b10]:sw t5, 248(ra)
[0x80001b14]:sw a7, 256(ra)
[0x80001b18]:lw t3, 1152(a6)
[0x80001b1c]:lw t4, 1156(a6)
[0x80001b20]:lw s10, 1160(a6)
[0x80001b24]:lw s11, 1164(a6)
[0x80001b28]:lui t3, 261607
[0x80001b2c]:addi t3, t3, 3165
[0x80001b30]:lui t4, 523999
[0x80001b34]:addi t4, t4, 1874
[0x80001b38]:lui s10, 107494
[0x80001b3c]:addi s10, s10, 3189
[0x80001b40]:lui s11, 523960
[0x80001b44]:addi s11, s11, 1443
[0x80001b48]:addi a4, zero, 96
[0x80001b4c]:csrrw zero, fcsr, a4
[0x80001b50]:fsub.d t5, t3, s10, dyn
[0x80001b54]:csrrs a7, fcsr, zero

[0x80001b50]:fsub.d t5, t3, s10, dyn
[0x80001b54]:csrrs a7, fcsr, zero
[0x80001b58]:sw t5, 264(ra)
[0x80001b5c]:sw t6, 272(ra)
[0x80001b60]:sw t5, 280(ra)
[0x80001b64]:sw a7, 288(ra)
[0x80001b68]:lw t3, 1168(a6)
[0x80001b6c]:lw t4, 1172(a6)
[0x80001b70]:lw s10, 1176(a6)
[0x80001b74]:lw s11, 1180(a6)
[0x80001b78]:lui t3, 261607
[0x80001b7c]:addi t3, t3, 3165
[0x80001b80]:lui t4, 523999
[0x80001b84]:addi t4, t4, 1874
[0x80001b88]:lui s10, 107494
[0x80001b8c]:addi s10, s10, 3189
[0x80001b90]:lui s11, 523960
[0x80001b94]:addi s11, s11, 1443
[0x80001b98]:addi a4, zero, 128
[0x80001b9c]:csrrw zero, fcsr, a4
[0x80001ba0]:fsub.d t5, t3, s10, dyn
[0x80001ba4]:csrrs a7, fcsr, zero

[0x80001ba0]:fsub.d t5, t3, s10, dyn
[0x80001ba4]:csrrs a7, fcsr, zero
[0x80001ba8]:sw t5, 296(ra)
[0x80001bac]:sw t6, 304(ra)
[0x80001bb0]:sw t5, 312(ra)
[0x80001bb4]:sw a7, 320(ra)
[0x80001bb8]:lw t3, 1184(a6)
[0x80001bbc]:lw t4, 1188(a6)
[0x80001bc0]:lw s10, 1192(a6)
[0x80001bc4]:lw s11, 1196(a6)
[0x80001bc8]:lui t3, 14912
[0x80001bcc]:addi t3, t3, 1932
[0x80001bd0]:lui t4, 523945
[0x80001bd4]:addi t4, t4, 4007
[0x80001bd8]:addi s10, zero, 0
[0x80001bdc]:lui s11, 524032
[0x80001be0]:addi a4, zero, 0
[0x80001be4]:csrrw zero, fcsr, a4
[0x80001be8]:fsub.d t5, t3, s10, dyn
[0x80001bec]:csrrs a7, fcsr, zero

[0x80001be8]:fsub.d t5, t3, s10, dyn
[0x80001bec]:csrrs a7, fcsr, zero
[0x80001bf0]:sw t5, 328(ra)
[0x80001bf4]:sw t6, 336(ra)
[0x80001bf8]:sw t5, 344(ra)
[0x80001bfc]:sw a7, 352(ra)
[0x80001c00]:lw t3, 1200(a6)
[0x80001c04]:lw t4, 1204(a6)
[0x80001c08]:lw s10, 1208(a6)
[0x80001c0c]:lw s11, 1212(a6)
[0x80001c10]:lui t3, 14912
[0x80001c14]:addi t3, t3, 1932
[0x80001c18]:lui t4, 523945
[0x80001c1c]:addi t4, t4, 4007
[0x80001c20]:addi s10, zero, 0
[0x80001c24]:lui s11, 524032
[0x80001c28]:addi a4, zero, 32
[0x80001c2c]:csrrw zero, fcsr, a4
[0x80001c30]:fsub.d t5, t3, s10, dyn
[0x80001c34]:csrrs a7, fcsr, zero

[0x80001c30]:fsub.d t5, t3, s10, dyn
[0x80001c34]:csrrs a7, fcsr, zero
[0x80001c38]:sw t5, 360(ra)
[0x80001c3c]:sw t6, 368(ra)
[0x80001c40]:sw t5, 376(ra)
[0x80001c44]:sw a7, 384(ra)
[0x80001c48]:lw t3, 1216(a6)
[0x80001c4c]:lw t4, 1220(a6)
[0x80001c50]:lw s10, 1224(a6)
[0x80001c54]:lw s11, 1228(a6)
[0x80001c58]:lui t3, 14912
[0x80001c5c]:addi t3, t3, 1932
[0x80001c60]:lui t4, 523945
[0x80001c64]:addi t4, t4, 4007
[0x80001c68]:addi s10, zero, 0
[0x80001c6c]:lui s11, 524032
[0x80001c70]:addi a4, zero, 64
[0x80001c74]:csrrw zero, fcsr, a4
[0x80001c78]:fsub.d t5, t3, s10, dyn
[0x80001c7c]:csrrs a7, fcsr, zero

[0x80001c78]:fsub.d t5, t3, s10, dyn
[0x80001c7c]:csrrs a7, fcsr, zero
[0x80001c80]:sw t5, 392(ra)
[0x80001c84]:sw t6, 400(ra)
[0x80001c88]:sw t5, 408(ra)
[0x80001c8c]:sw a7, 416(ra)
[0x80001c90]:lw t3, 1232(a6)
[0x80001c94]:lw t4, 1236(a6)
[0x80001c98]:lw s10, 1240(a6)
[0x80001c9c]:lw s11, 1244(a6)
[0x80001ca0]:lui t3, 14912
[0x80001ca4]:addi t3, t3, 1932
[0x80001ca8]:lui t4, 523945
[0x80001cac]:addi t4, t4, 4007
[0x80001cb0]:addi s10, zero, 0
[0x80001cb4]:lui s11, 524032
[0x80001cb8]:addi a4, zero, 96
[0x80001cbc]:csrrw zero, fcsr, a4
[0x80001cc0]:fsub.d t5, t3, s10, dyn
[0x80001cc4]:csrrs a7, fcsr, zero

[0x80001cc0]:fsub.d t5, t3, s10, dyn
[0x80001cc4]:csrrs a7, fcsr, zero
[0x80001cc8]:sw t5, 424(ra)
[0x80001ccc]:sw t6, 432(ra)
[0x80001cd0]:sw t5, 440(ra)
[0x80001cd4]:sw a7, 448(ra)
[0x80001cd8]:lw t3, 1248(a6)
[0x80001cdc]:lw t4, 1252(a6)
[0x80001ce0]:lw s10, 1256(a6)
[0x80001ce4]:lw s11, 1260(a6)
[0x80001ce8]:lui t3, 14912
[0x80001cec]:addi t3, t3, 1932
[0x80001cf0]:lui t4, 523945
[0x80001cf4]:addi t4, t4, 4007
[0x80001cf8]:addi s10, zero, 0
[0x80001cfc]:lui s11, 524032
[0x80001d00]:addi a4, zero, 128
[0x80001d04]:csrrw zero, fcsr, a4
[0x80001d08]:fsub.d t5, t3, s10, dyn
[0x80001d0c]:csrrs a7, fcsr, zero

[0x80001d08]:fsub.d t5, t3, s10, dyn
[0x80001d0c]:csrrs a7, fcsr, zero
[0x80001d10]:sw t5, 456(ra)
[0x80001d14]:sw t6, 464(ra)
[0x80001d18]:sw t5, 472(ra)
[0x80001d1c]:sw a7, 480(ra)
[0x80001d20]:lw t3, 1264(a6)
[0x80001d24]:lw t4, 1268(a6)
[0x80001d28]:lw s10, 1272(a6)
[0x80001d2c]:lw s11, 1276(a6)
[0x80001d30]:lui t3, 795743
[0x80001d34]:addi t3, t3, 1367
[0x80001d38]:lui t4, 523575
[0x80001d3c]:addi t4, t4, 2659
[0x80001d40]:lui s10, 227856
[0x80001d44]:addi s10, s10, 3139
[0x80001d48]:lui s11, 1047689
[0x80001d4c]:addi s11, s11, 1443
[0x80001d50]:addi a4, zero, 0
[0x80001d54]:csrrw zero, fcsr, a4
[0x80001d58]:fsub.d t5, t3, s10, dyn
[0x80001d5c]:csrrs a7, fcsr, zero

[0x80001d58]:fsub.d t5, t3, s10, dyn
[0x80001d5c]:csrrs a7, fcsr, zero
[0x80001d60]:sw t5, 488(ra)
[0x80001d64]:sw t6, 496(ra)
[0x80001d68]:sw t5, 504(ra)
[0x80001d6c]:sw a7, 512(ra)
[0x80001d70]:lw t3, 1280(a6)
[0x80001d74]:lw t4, 1284(a6)
[0x80001d78]:lw s10, 1288(a6)
[0x80001d7c]:lw s11, 1292(a6)
[0x80001d80]:lui t3, 795743
[0x80001d84]:addi t3, t3, 1367
[0x80001d88]:lui t4, 523575
[0x80001d8c]:addi t4, t4, 2659
[0x80001d90]:lui s10, 227856
[0x80001d94]:addi s10, s10, 3139
[0x80001d98]:lui s11, 1047689
[0x80001d9c]:addi s11, s11, 1443
[0x80001da0]:addi a4, zero, 32
[0x80001da4]:csrrw zero, fcsr, a4
[0x80001da8]:fsub.d t5, t3, s10, dyn
[0x80001dac]:csrrs a7, fcsr, zero

[0x80001da8]:fsub.d t5, t3, s10, dyn
[0x80001dac]:csrrs a7, fcsr, zero
[0x80001db0]:sw t5, 520(ra)
[0x80001db4]:sw t6, 528(ra)
[0x80001db8]:sw t5, 536(ra)
[0x80001dbc]:sw a7, 544(ra)
[0x80001dc0]:lw t3, 1296(a6)
[0x80001dc4]:lw t4, 1300(a6)
[0x80001dc8]:lw s10, 1304(a6)
[0x80001dcc]:lw s11, 1308(a6)
[0x80001dd0]:lui t3, 795743
[0x80001dd4]:addi t3, t3, 1367
[0x80001dd8]:lui t4, 523575
[0x80001ddc]:addi t4, t4, 2659
[0x80001de0]:lui s10, 227856
[0x80001de4]:addi s10, s10, 3139
[0x80001de8]:lui s11, 1047689
[0x80001dec]:addi s11, s11, 1443
[0x80001df0]:addi a4, zero, 64
[0x80001df4]:csrrw zero, fcsr, a4
[0x80001df8]:fsub.d t5, t3, s10, dyn
[0x80001dfc]:csrrs a7, fcsr, zero

[0x80001df8]:fsub.d t5, t3, s10, dyn
[0x80001dfc]:csrrs a7, fcsr, zero
[0x80001e00]:sw t5, 552(ra)
[0x80001e04]:sw t6, 560(ra)
[0x80001e08]:sw t5, 568(ra)
[0x80001e0c]:sw a7, 576(ra)
[0x80001e10]:lw t3, 1312(a6)
[0x80001e14]:lw t4, 1316(a6)
[0x80001e18]:lw s10, 1320(a6)
[0x80001e1c]:lw s11, 1324(a6)
[0x80001e20]:lui t3, 795743
[0x80001e24]:addi t3, t3, 1367
[0x80001e28]:lui t4, 523575
[0x80001e2c]:addi t4, t4, 2659
[0x80001e30]:lui s10, 227856
[0x80001e34]:addi s10, s10, 3139
[0x80001e38]:lui s11, 1047689
[0x80001e3c]:addi s11, s11, 1443
[0x80001e40]:addi a4, zero, 96
[0x80001e44]:csrrw zero, fcsr, a4
[0x80001e48]:fsub.d t5, t3, s10, dyn
[0x80001e4c]:csrrs a7, fcsr, zero

[0x80001e48]:fsub.d t5, t3, s10, dyn
[0x80001e4c]:csrrs a7, fcsr, zero
[0x80001e50]:sw t5, 584(ra)
[0x80001e54]:sw t6, 592(ra)
[0x80001e58]:sw t5, 600(ra)
[0x80001e5c]:sw a7, 608(ra)
[0x80001e60]:lw t3, 1328(a6)
[0x80001e64]:lw t4, 1332(a6)
[0x80001e68]:lw s10, 1336(a6)
[0x80001e6c]:lw s11, 1340(a6)
[0x80001e70]:lui t3, 795743
[0x80001e74]:addi t3, t3, 1367
[0x80001e78]:lui t4, 523575
[0x80001e7c]:addi t4, t4, 2659
[0x80001e80]:lui s10, 227856
[0x80001e84]:addi s10, s10, 3139
[0x80001e88]:lui s11, 1047689
[0x80001e8c]:addi s11, s11, 1443
[0x80001e90]:addi a4, zero, 128
[0x80001e94]:csrrw zero, fcsr, a4
[0x80001e98]:fsub.d t5, t3, s10, dyn
[0x80001e9c]:csrrs a7, fcsr, zero

[0x80001e98]:fsub.d t5, t3, s10, dyn
[0x80001e9c]:csrrs a7, fcsr, zero
[0x80001ea0]:sw t5, 616(ra)
[0x80001ea4]:sw t6, 624(ra)
[0x80001ea8]:sw t5, 632(ra)
[0x80001eac]:sw a7, 640(ra)
[0x80001eb0]:lw t3, 1344(a6)
[0x80001eb4]:lw t4, 1348(a6)
[0x80001eb8]:lw s10, 1352(a6)
[0x80001ebc]:lw s11, 1356(a6)
[0x80001ec0]:lui t3, 630774
[0x80001ec4]:addi t3, t3, 2243
[0x80001ec8]:lui t4, 523972
[0x80001ecc]:addi t4, t4, 3396
[0x80001ed0]:addi s10, zero, 0
[0x80001ed4]:lui s11, 524032
[0x80001ed8]:addi a4, zero, 0
[0x80001edc]:csrrw zero, fcsr, a4
[0x80001ee0]:fsub.d t5, t3, s10, dyn
[0x80001ee4]:csrrs a7, fcsr, zero

[0x80001ee0]:fsub.d t5, t3, s10, dyn
[0x80001ee4]:csrrs a7, fcsr, zero
[0x80001ee8]:sw t5, 648(ra)
[0x80001eec]:sw t6, 656(ra)
[0x80001ef0]:sw t5, 664(ra)
[0x80001ef4]:sw a7, 672(ra)
[0x80001ef8]:lw t3, 1360(a6)
[0x80001efc]:lw t4, 1364(a6)
[0x80001f00]:lw s10, 1368(a6)
[0x80001f04]:lw s11, 1372(a6)
[0x80001f08]:lui t3, 630774
[0x80001f0c]:addi t3, t3, 2243
[0x80001f10]:lui t4, 523972
[0x80001f14]:addi t4, t4, 3396
[0x80001f18]:addi s10, zero, 0
[0x80001f1c]:lui s11, 524032
[0x80001f20]:addi a4, zero, 32
[0x80001f24]:csrrw zero, fcsr, a4
[0x80001f28]:fsub.d t5, t3, s10, dyn
[0x80001f2c]:csrrs a7, fcsr, zero

[0x80001f28]:fsub.d t5, t3, s10, dyn
[0x80001f2c]:csrrs a7, fcsr, zero
[0x80001f30]:sw t5, 680(ra)
[0x80001f34]:sw t6, 688(ra)
[0x80001f38]:sw t5, 696(ra)
[0x80001f3c]:sw a7, 704(ra)
[0x80001f40]:lw t3, 1376(a6)
[0x80001f44]:lw t4, 1380(a6)
[0x80001f48]:lw s10, 1384(a6)
[0x80001f4c]:lw s11, 1388(a6)
[0x80001f50]:lui t3, 630774
[0x80001f54]:addi t3, t3, 2243
[0x80001f58]:lui t4, 523972
[0x80001f5c]:addi t4, t4, 3396
[0x80001f60]:addi s10, zero, 0
[0x80001f64]:lui s11, 524032
[0x80001f68]:addi a4, zero, 64
[0x80001f6c]:csrrw zero, fcsr, a4
[0x80001f70]:fsub.d t5, t3, s10, dyn
[0x80001f74]:csrrs a7, fcsr, zero

[0x80001f70]:fsub.d t5, t3, s10, dyn
[0x80001f74]:csrrs a7, fcsr, zero
[0x80001f78]:sw t5, 712(ra)
[0x80001f7c]:sw t6, 720(ra)
[0x80001f80]:sw t5, 728(ra)
[0x80001f84]:sw a7, 736(ra)
[0x80001f88]:lw t3, 1392(a6)
[0x80001f8c]:lw t4, 1396(a6)
[0x80001f90]:lw s10, 1400(a6)
[0x80001f94]:lw s11, 1404(a6)
[0x80001f98]:lui t3, 630774
[0x80001f9c]:addi t3, t3, 2243
[0x80001fa0]:lui t4, 523972
[0x80001fa4]:addi t4, t4, 3396
[0x80001fa8]:addi s10, zero, 0
[0x80001fac]:lui s11, 524032
[0x80001fb0]:addi a4, zero, 96
[0x80001fb4]:csrrw zero, fcsr, a4
[0x80001fb8]:fsub.d t5, t3, s10, dyn
[0x80001fbc]:csrrs a7, fcsr, zero

[0x80001fb8]:fsub.d t5, t3, s10, dyn
[0x80001fbc]:csrrs a7, fcsr, zero
[0x80001fc0]:sw t5, 744(ra)
[0x80001fc4]:sw t6, 752(ra)
[0x80001fc8]:sw t5, 760(ra)
[0x80001fcc]:sw a7, 768(ra)
[0x80001fd0]:lw t3, 1408(a6)
[0x80001fd4]:lw t4, 1412(a6)
[0x80001fd8]:lw s10, 1416(a6)
[0x80001fdc]:lw s11, 1420(a6)
[0x80001fe0]:lui t3, 630774
[0x80001fe4]:addi t3, t3, 2243
[0x80001fe8]:lui t4, 523972
[0x80001fec]:addi t4, t4, 3396
[0x80001ff0]:addi s10, zero, 0
[0x80001ff4]:lui s11, 524032
[0x80001ff8]:addi a4, zero, 128
[0x80001ffc]:csrrw zero, fcsr, a4
[0x80002000]:fsub.d t5, t3, s10, dyn
[0x80002004]:csrrs a7, fcsr, zero

[0x80002000]:fsub.d t5, t3, s10, dyn
[0x80002004]:csrrs a7, fcsr, zero
[0x80002008]:sw t5, 776(ra)
[0x8000200c]:sw t6, 784(ra)
[0x80002010]:sw t5, 792(ra)
[0x80002014]:sw a7, 800(ra)
[0x80002018]:lw t3, 1424(a6)
[0x8000201c]:lw t4, 1428(a6)
[0x80002020]:lw s10, 1432(a6)
[0x80002024]:lw s11, 1436(a6)
[0x80002028]:lui t3, 1022253
[0x8000202c]:addi t3, t3, 2889
[0x80002030]:lui t4, 523779
[0x80002034]:addi t4, t4, 2888
[0x80002038]:lui s10, 50967
[0x8000203c]:addi s10, s10, 359
[0x80002040]:lui s11, 523715
[0x80002044]:addi s11, s11, 2816
[0x80002048]:addi a4, zero, 0
[0x8000204c]:csrrw zero, fcsr, a4
[0x80002050]:fsub.d t5, t3, s10, dyn
[0x80002054]:csrrs a7, fcsr, zero

[0x80002050]:fsub.d t5, t3, s10, dyn
[0x80002054]:csrrs a7, fcsr, zero
[0x80002058]:sw t5, 808(ra)
[0x8000205c]:sw t6, 816(ra)
[0x80002060]:sw t5, 824(ra)
[0x80002064]:sw a7, 832(ra)
[0x80002068]:lw t3, 1440(a6)
[0x8000206c]:lw t4, 1444(a6)
[0x80002070]:lw s10, 1448(a6)
[0x80002074]:lw s11, 1452(a6)
[0x80002078]:lui t3, 1022253
[0x8000207c]:addi t3, t3, 2889
[0x80002080]:lui t4, 523779
[0x80002084]:addi t4, t4, 2888
[0x80002088]:lui s10, 50967
[0x8000208c]:addi s10, s10, 359
[0x80002090]:lui s11, 523715
[0x80002094]:addi s11, s11, 2816
[0x80002098]:addi a4, zero, 32
[0x8000209c]:csrrw zero, fcsr, a4
[0x800020a0]:fsub.d t5, t3, s10, dyn
[0x800020a4]:csrrs a7, fcsr, zero

[0x800020a0]:fsub.d t5, t3, s10, dyn
[0x800020a4]:csrrs a7, fcsr, zero
[0x800020a8]:sw t5, 840(ra)
[0x800020ac]:sw t6, 848(ra)
[0x800020b0]:sw t5, 856(ra)
[0x800020b4]:sw a7, 864(ra)
[0x800020b8]:lw t3, 1456(a6)
[0x800020bc]:lw t4, 1460(a6)
[0x800020c0]:lw s10, 1464(a6)
[0x800020c4]:lw s11, 1468(a6)
[0x800020c8]:lui t3, 1022253
[0x800020cc]:addi t3, t3, 2889
[0x800020d0]:lui t4, 523779
[0x800020d4]:addi t4, t4, 2888
[0x800020d8]:lui s10, 50967
[0x800020dc]:addi s10, s10, 359
[0x800020e0]:lui s11, 523715
[0x800020e4]:addi s11, s11, 2816
[0x800020e8]:addi a4, zero, 64
[0x800020ec]:csrrw zero, fcsr, a4
[0x800020f0]:fsub.d t5, t3, s10, dyn
[0x800020f4]:csrrs a7, fcsr, zero

[0x800020f0]:fsub.d t5, t3, s10, dyn
[0x800020f4]:csrrs a7, fcsr, zero
[0x800020f8]:sw t5, 872(ra)
[0x800020fc]:sw t6, 880(ra)
[0x80002100]:sw t5, 888(ra)
[0x80002104]:sw a7, 896(ra)
[0x80002108]:lw t3, 1472(a6)
[0x8000210c]:lw t4, 1476(a6)
[0x80002110]:lw s10, 1480(a6)
[0x80002114]:lw s11, 1484(a6)
[0x80002118]:lui t3, 1022253
[0x8000211c]:addi t3, t3, 2889
[0x80002120]:lui t4, 523779
[0x80002124]:addi t4, t4, 2888
[0x80002128]:lui s10, 50967
[0x8000212c]:addi s10, s10, 359
[0x80002130]:lui s11, 523715
[0x80002134]:addi s11, s11, 2816
[0x80002138]:addi a4, zero, 96
[0x8000213c]:csrrw zero, fcsr, a4
[0x80002140]:fsub.d t5, t3, s10, dyn
[0x80002144]:csrrs a7, fcsr, zero

[0x80002140]:fsub.d t5, t3, s10, dyn
[0x80002144]:csrrs a7, fcsr, zero
[0x80002148]:sw t5, 904(ra)
[0x8000214c]:sw t6, 912(ra)
[0x80002150]:sw t5, 920(ra)
[0x80002154]:sw a7, 928(ra)
[0x80002158]:lw t3, 1488(a6)
[0x8000215c]:lw t4, 1492(a6)
[0x80002160]:lw s10, 1496(a6)
[0x80002164]:lw s11, 1500(a6)
[0x80002168]:lui t3, 1022253
[0x8000216c]:addi t3, t3, 2889
[0x80002170]:lui t4, 523779
[0x80002174]:addi t4, t4, 2888
[0x80002178]:lui s10, 50967
[0x8000217c]:addi s10, s10, 359
[0x80002180]:lui s11, 523715
[0x80002184]:addi s11, s11, 2816
[0x80002188]:addi a4, zero, 128
[0x8000218c]:csrrw zero, fcsr, a4
[0x80002190]:fsub.d t5, t3, s10, dyn
[0x80002194]:csrrs a7, fcsr, zero

[0x80002190]:fsub.d t5, t3, s10, dyn
[0x80002194]:csrrs a7, fcsr, zero
[0x80002198]:sw t5, 936(ra)
[0x8000219c]:sw t6, 944(ra)
[0x800021a0]:sw t5, 952(ra)
[0x800021a4]:sw a7, 960(ra)
[0x800021a8]:lw t3, 1504(a6)
[0x800021ac]:lw t4, 1508(a6)
[0x800021b0]:lw s10, 1512(a6)
[0x800021b4]:lw s11, 1516(a6)
[0x800021b8]:lui t3, 650177
[0x800021bc]:addi t3, t3, 2527
[0x800021c0]:lui t4, 522779
[0x800021c4]:addi t4, t4, 3422
[0x800021c8]:lui s10, 537555
[0x800021cc]:addi s10, s10, 1752
[0x800021d0]:lui s11, 523216
[0x800021d4]:addi s11, s11, 1961
[0x800021d8]:addi a4, zero, 0
[0x800021dc]:csrrw zero, fcsr, a4
[0x800021e0]:fsub.d t5, t3, s10, dyn
[0x800021e4]:csrrs a7, fcsr, zero

[0x800021e0]:fsub.d t5, t3, s10, dyn
[0x800021e4]:csrrs a7, fcsr, zero
[0x800021e8]:sw t5, 968(ra)
[0x800021ec]:sw t6, 976(ra)
[0x800021f0]:sw t5, 984(ra)
[0x800021f4]:sw a7, 992(ra)
[0x800021f8]:lw t3, 1520(a6)
[0x800021fc]:lw t4, 1524(a6)
[0x80002200]:lw s10, 1528(a6)
[0x80002204]:lw s11, 1532(a6)
[0x80002208]:lui t3, 650177
[0x8000220c]:addi t3, t3, 2527
[0x80002210]:lui t4, 522779
[0x80002214]:addi t4, t4, 3422
[0x80002218]:lui s10, 537555
[0x8000221c]:addi s10, s10, 1752
[0x80002220]:lui s11, 523216
[0x80002224]:addi s11, s11, 1961
[0x80002228]:addi a4, zero, 32
[0x8000222c]:csrrw zero, fcsr, a4
[0x80002230]:fsub.d t5, t3, s10, dyn
[0x80002234]:csrrs a7, fcsr, zero

[0x80002230]:fsub.d t5, t3, s10, dyn
[0x80002234]:csrrs a7, fcsr, zero
[0x80002238]:sw t5, 1000(ra)
[0x8000223c]:sw t6, 1008(ra)
[0x80002240]:sw t5, 1016(ra)
[0x80002244]:sw a7, 1024(ra)
[0x80002248]:lw t3, 1536(a6)
[0x8000224c]:lw t4, 1540(a6)
[0x80002250]:lw s10, 1544(a6)
[0x80002254]:lw s11, 1548(a6)
[0x80002258]:lui t3, 650177
[0x8000225c]:addi t3, t3, 2527
[0x80002260]:lui t4, 522779
[0x80002264]:addi t4, t4, 3422
[0x80002268]:lui s10, 537555
[0x8000226c]:addi s10, s10, 1752
[0x80002270]:lui s11, 523216
[0x80002274]:addi s11, s11, 1961
[0x80002278]:addi a4, zero, 64
[0x8000227c]:csrrw zero, fcsr, a4
[0x80002280]:fsub.d t5, t3, s10, dyn
[0x80002284]:csrrs a7, fcsr, zero

[0x80002280]:fsub.d t5, t3, s10, dyn
[0x80002284]:csrrs a7, fcsr, zero
[0x80002288]:sw t5, 1032(ra)
[0x8000228c]:sw t6, 1040(ra)
[0x80002290]:sw t5, 1048(ra)
[0x80002294]:sw a7, 1056(ra)
[0x80002298]:lw t3, 1552(a6)
[0x8000229c]:lw t4, 1556(a6)
[0x800022a0]:lw s10, 1560(a6)
[0x800022a4]:lw s11, 1564(a6)
[0x800022a8]:lui t3, 650177
[0x800022ac]:addi t3, t3, 2527
[0x800022b0]:lui t4, 522779
[0x800022b4]:addi t4, t4, 3422
[0x800022b8]:lui s10, 537555
[0x800022bc]:addi s10, s10, 1752
[0x800022c0]:lui s11, 523216
[0x800022c4]:addi s11, s11, 1961
[0x800022c8]:addi a4, zero, 96
[0x800022cc]:csrrw zero, fcsr, a4
[0x800022d0]:fsub.d t5, t3, s10, dyn
[0x800022d4]:csrrs a7, fcsr, zero

[0x800022d0]:fsub.d t5, t3, s10, dyn
[0x800022d4]:csrrs a7, fcsr, zero
[0x800022d8]:sw t5, 1064(ra)
[0x800022dc]:sw t6, 1072(ra)
[0x800022e0]:sw t5, 1080(ra)
[0x800022e4]:sw a7, 1088(ra)
[0x800022e8]:lw t3, 1568(a6)
[0x800022ec]:lw t4, 1572(a6)
[0x800022f0]:lw s10, 1576(a6)
[0x800022f4]:lw s11, 1580(a6)
[0x800022f8]:lui t3, 650177
[0x800022fc]:addi t3, t3, 2527
[0x80002300]:lui t4, 522779
[0x80002304]:addi t4, t4, 3422
[0x80002308]:lui s10, 537555
[0x8000230c]:addi s10, s10, 1752
[0x80002310]:lui s11, 523216
[0x80002314]:addi s11, s11, 1961
[0x80002318]:addi a4, zero, 128
[0x8000231c]:csrrw zero, fcsr, a4
[0x80002320]:fsub.d t5, t3, s10, dyn
[0x80002324]:csrrs a7, fcsr, zero

[0x80002320]:fsub.d t5, t3, s10, dyn
[0x80002324]:csrrs a7, fcsr, zero
[0x80002328]:sw t5, 1096(ra)
[0x8000232c]:sw t6, 1104(ra)
[0x80002330]:sw t5, 1112(ra)
[0x80002334]:sw a7, 1120(ra)
[0x80002338]:lw t3, 1584(a6)
[0x8000233c]:lw t4, 1588(a6)
[0x80002340]:lw s10, 1592(a6)
[0x80002344]:lw s11, 1596(a6)
[0x80002348]:lui t3, 956896
[0x8000234c]:addi t3, t3, 3505
[0x80002350]:lui t4, 523928
[0x80002354]:addi t4, t4, 1109
[0x80002358]:lui s10, 162154
[0x8000235c]:addi s10, s10, 1288
[0x80002360]:lui s11, 1047585
[0x80002364]:addi s11, s11, 657
[0x80002368]:addi a4, zero, 0
[0x8000236c]:csrrw zero, fcsr, a4
[0x80002370]:fsub.d t5, t3, s10, dyn
[0x80002374]:csrrs a7, fcsr, zero

[0x80002370]:fsub.d t5, t3, s10, dyn
[0x80002374]:csrrs a7, fcsr, zero
[0x80002378]:sw t5, 1128(ra)
[0x8000237c]:sw t6, 1136(ra)
[0x80002380]:sw t5, 1144(ra)
[0x80002384]:sw a7, 1152(ra)
[0x80002388]:lw t3, 1600(a6)
[0x8000238c]:lw t4, 1604(a6)
[0x80002390]:lw s10, 1608(a6)
[0x80002394]:lw s11, 1612(a6)
[0x80002398]:lui t3, 956896
[0x8000239c]:addi t3, t3, 3505
[0x800023a0]:lui t4, 523928
[0x800023a4]:addi t4, t4, 1109
[0x800023a8]:lui s10, 162154
[0x800023ac]:addi s10, s10, 1288
[0x800023b0]:lui s11, 1047585
[0x800023b4]:addi s11, s11, 657
[0x800023b8]:addi a4, zero, 32
[0x800023bc]:csrrw zero, fcsr, a4
[0x800023c0]:fsub.d t5, t3, s10, dyn
[0x800023c4]:csrrs a7, fcsr, zero

[0x800023c0]:fsub.d t5, t3, s10, dyn
[0x800023c4]:csrrs a7, fcsr, zero
[0x800023c8]:sw t5, 1160(ra)
[0x800023cc]:sw t6, 1168(ra)
[0x800023d0]:sw t5, 1176(ra)
[0x800023d4]:sw a7, 1184(ra)
[0x800023d8]:lw t3, 1616(a6)
[0x800023dc]:lw t4, 1620(a6)
[0x800023e0]:lw s10, 1624(a6)
[0x800023e4]:lw s11, 1628(a6)
[0x800023e8]:lui t3, 956896
[0x800023ec]:addi t3, t3, 3505
[0x800023f0]:lui t4, 523928
[0x800023f4]:addi t4, t4, 1109
[0x800023f8]:lui s10, 162154
[0x800023fc]:addi s10, s10, 1288
[0x80002400]:lui s11, 1047585
[0x80002404]:addi s11, s11, 657
[0x80002408]:addi a4, zero, 64
[0x8000240c]:csrrw zero, fcsr, a4
[0x80002410]:fsub.d t5, t3, s10, dyn
[0x80002414]:csrrs a7, fcsr, zero

[0x80002410]:fsub.d t5, t3, s10, dyn
[0x80002414]:csrrs a7, fcsr, zero
[0x80002418]:sw t5, 1192(ra)
[0x8000241c]:sw t6, 1200(ra)
[0x80002420]:sw t5, 1208(ra)
[0x80002424]:sw a7, 1216(ra)
[0x80002428]:lw t3, 1632(a6)
[0x8000242c]:lw t4, 1636(a6)
[0x80002430]:lw s10, 1640(a6)
[0x80002434]:lw s11, 1644(a6)
[0x80002438]:lui t3, 956896
[0x8000243c]:addi t3, t3, 3505
[0x80002440]:lui t4, 523928
[0x80002444]:addi t4, t4, 1109
[0x80002448]:lui s10, 162154
[0x8000244c]:addi s10, s10, 1288
[0x80002450]:lui s11, 1047585
[0x80002454]:addi s11, s11, 657
[0x80002458]:addi a4, zero, 96
[0x8000245c]:csrrw zero, fcsr, a4
[0x80002460]:fsub.d t5, t3, s10, dyn
[0x80002464]:csrrs a7, fcsr, zero

[0x80002460]:fsub.d t5, t3, s10, dyn
[0x80002464]:csrrs a7, fcsr, zero
[0x80002468]:sw t5, 1224(ra)
[0x8000246c]:sw t6, 1232(ra)
[0x80002470]:sw t5, 1240(ra)
[0x80002474]:sw a7, 1248(ra)
[0x80002478]:lw t3, 1648(a6)
[0x8000247c]:lw t4, 1652(a6)
[0x80002480]:lw s10, 1656(a6)
[0x80002484]:lw s11, 1660(a6)
[0x80002488]:lui t3, 956896
[0x8000248c]:addi t3, t3, 3505
[0x80002490]:lui t4, 523928
[0x80002494]:addi t4, t4, 1109
[0x80002498]:lui s10, 162154
[0x8000249c]:addi s10, s10, 1288
[0x800024a0]:lui s11, 1047585
[0x800024a4]:addi s11, s11, 657
[0x800024a8]:addi a4, zero, 128
[0x800024ac]:csrrw zero, fcsr, a4
[0x800024b0]:fsub.d t5, t3, s10, dyn
[0x800024b4]:csrrs a7, fcsr, zero

[0x800024b0]:fsub.d t5, t3, s10, dyn
[0x800024b4]:csrrs a7, fcsr, zero
[0x800024b8]:sw t5, 1256(ra)
[0x800024bc]:sw t6, 1264(ra)
[0x800024c0]:sw t5, 1272(ra)
[0x800024c4]:sw a7, 1280(ra)
[0x800024c8]:lw t3, 1664(a6)
[0x800024cc]:lw t4, 1668(a6)
[0x800024d0]:lw s10, 1672(a6)
[0x800024d4]:lw s11, 1676(a6)
[0x800024d8]:lui t3, 286204
[0x800024dc]:addi t3, t3, 3387
[0x800024e0]:lui t4, 523362
[0x800024e4]:addi t4, t4, 1307
[0x800024e8]:addi s10, zero, 0
[0x800024ec]:lui s11, 524032
[0x800024f0]:addi a4, zero, 0
[0x800024f4]:csrrw zero, fcsr, a4
[0x800024f8]:fsub.d t5, t3, s10, dyn
[0x800024fc]:csrrs a7, fcsr, zero

[0x800024f8]:fsub.d t5, t3, s10, dyn
[0x800024fc]:csrrs a7, fcsr, zero
[0x80002500]:sw t5, 1288(ra)
[0x80002504]:sw t6, 1296(ra)
[0x80002508]:sw t5, 1304(ra)
[0x8000250c]:sw a7, 1312(ra)
[0x80002510]:lw t3, 1680(a6)
[0x80002514]:lw t4, 1684(a6)
[0x80002518]:lw s10, 1688(a6)
[0x8000251c]:lw s11, 1692(a6)
[0x80002520]:lui t3, 286204
[0x80002524]:addi t3, t3, 3387
[0x80002528]:lui t4, 523362
[0x8000252c]:addi t4, t4, 1307
[0x80002530]:addi s10, zero, 0
[0x80002534]:lui s11, 524032
[0x80002538]:addi a4, zero, 32
[0x8000253c]:csrrw zero, fcsr, a4
[0x80002540]:fsub.d t5, t3, s10, dyn
[0x80002544]:csrrs a7, fcsr, zero

[0x80002540]:fsub.d t5, t3, s10, dyn
[0x80002544]:csrrs a7, fcsr, zero
[0x80002548]:sw t5, 1320(ra)
[0x8000254c]:sw t6, 1328(ra)
[0x80002550]:sw t5, 1336(ra)
[0x80002554]:sw a7, 1344(ra)
[0x80002558]:lw t3, 1696(a6)
[0x8000255c]:lw t4, 1700(a6)
[0x80002560]:lw s10, 1704(a6)
[0x80002564]:lw s11, 1708(a6)
[0x80002568]:lui t3, 286204
[0x8000256c]:addi t3, t3, 3387
[0x80002570]:lui t4, 523362
[0x80002574]:addi t4, t4, 1307
[0x80002578]:addi s10, zero, 0
[0x8000257c]:lui s11, 524032
[0x80002580]:addi a4, zero, 64
[0x80002584]:csrrw zero, fcsr, a4
[0x80002588]:fsub.d t5, t3, s10, dyn
[0x8000258c]:csrrs a7, fcsr, zero

[0x80002588]:fsub.d t5, t3, s10, dyn
[0x8000258c]:csrrs a7, fcsr, zero
[0x80002590]:sw t5, 1352(ra)
[0x80002594]:sw t6, 1360(ra)
[0x80002598]:sw t5, 1368(ra)
[0x8000259c]:sw a7, 1376(ra)
[0x800025a0]:lw t3, 1712(a6)
[0x800025a4]:lw t4, 1716(a6)
[0x800025a8]:lw s10, 1720(a6)
[0x800025ac]:lw s11, 1724(a6)
[0x800025b0]:lui t3, 286204
[0x800025b4]:addi t3, t3, 3387
[0x800025b8]:lui t4, 523362
[0x800025bc]:addi t4, t4, 1307
[0x800025c0]:addi s10, zero, 0
[0x800025c4]:lui s11, 524032
[0x800025c8]:addi a4, zero, 96
[0x800025cc]:csrrw zero, fcsr, a4
[0x800025d0]:fsub.d t5, t3, s10, dyn
[0x800025d4]:csrrs a7, fcsr, zero

[0x800025d0]:fsub.d t5, t3, s10, dyn
[0x800025d4]:csrrs a7, fcsr, zero
[0x800025d8]:sw t5, 1384(ra)
[0x800025dc]:sw t6, 1392(ra)
[0x800025e0]:sw t5, 1400(ra)
[0x800025e4]:sw a7, 1408(ra)
[0x800025e8]:lw t3, 1728(a6)
[0x800025ec]:lw t4, 1732(a6)
[0x800025f0]:lw s10, 1736(a6)
[0x800025f4]:lw s11, 1740(a6)
[0x800025f8]:lui t3, 286204
[0x800025fc]:addi t3, t3, 3387
[0x80002600]:lui t4, 523362
[0x80002604]:addi t4, t4, 1307
[0x80002608]:addi s10, zero, 0
[0x8000260c]:lui s11, 524032
[0x80002610]:addi a4, zero, 128
[0x80002614]:csrrw zero, fcsr, a4
[0x80002618]:fsub.d t5, t3, s10, dyn
[0x8000261c]:csrrs a7, fcsr, zero

[0x80002618]:fsub.d t5, t3, s10, dyn
[0x8000261c]:csrrs a7, fcsr, zero
[0x80002620]:sw t5, 1416(ra)
[0x80002624]:sw t6, 1424(ra)
[0x80002628]:sw t5, 1432(ra)
[0x8000262c]:sw a7, 1440(ra)
[0x80002630]:lw t3, 1744(a6)
[0x80002634]:lw t4, 1748(a6)
[0x80002638]:lw s10, 1752(a6)
[0x8000263c]:lw s11, 1756(a6)
[0x80002640]:lui t3, 356775
[0x80002644]:addi t3, t3, 1326
[0x80002648]:lui t4, 523880
[0x8000264c]:addi t4, t4, 3919
[0x80002650]:lui s10, 498989
[0x80002654]:addi s10, s10, 877
[0x80002658]:lui s11, 1047568
[0x8000265c]:addi s11, s11, 3356
[0x80002660]:addi a4, zero, 0
[0x80002664]:csrrw zero, fcsr, a4
[0x80002668]:fsub.d t5, t3, s10, dyn
[0x8000266c]:csrrs a7, fcsr, zero

[0x80002668]:fsub.d t5, t3, s10, dyn
[0x8000266c]:csrrs a7, fcsr, zero
[0x80002670]:sw t5, 1448(ra)
[0x80002674]:sw t6, 1456(ra)
[0x80002678]:sw t5, 1464(ra)
[0x8000267c]:sw a7, 1472(ra)
[0x80002680]:lw t3, 1760(a6)
[0x80002684]:lw t4, 1764(a6)
[0x80002688]:lw s10, 1768(a6)
[0x8000268c]:lw s11, 1772(a6)
[0x80002690]:lui t3, 356775
[0x80002694]:addi t3, t3, 1326
[0x80002698]:lui t4, 523880
[0x8000269c]:addi t4, t4, 3919
[0x800026a0]:lui s10, 498989
[0x800026a4]:addi s10, s10, 877
[0x800026a8]:lui s11, 1047568
[0x800026ac]:addi s11, s11, 3356
[0x800026b0]:addi a4, zero, 32
[0x800026b4]:csrrw zero, fcsr, a4
[0x800026b8]:fsub.d t5, t3, s10, dyn
[0x800026bc]:csrrs a7, fcsr, zero

[0x800026b8]:fsub.d t5, t3, s10, dyn
[0x800026bc]:csrrs a7, fcsr, zero
[0x800026c0]:sw t5, 1480(ra)
[0x800026c4]:sw t6, 1488(ra)
[0x800026c8]:sw t5, 1496(ra)
[0x800026cc]:sw a7, 1504(ra)
[0x800026d0]:lw t3, 1776(a6)
[0x800026d4]:lw t4, 1780(a6)
[0x800026d8]:lw s10, 1784(a6)
[0x800026dc]:lw s11, 1788(a6)
[0x800026e0]:lui t3, 356775
[0x800026e4]:addi t3, t3, 1326
[0x800026e8]:lui t4, 523880
[0x800026ec]:addi t4, t4, 3919
[0x800026f0]:lui s10, 498989
[0x800026f4]:addi s10, s10, 877
[0x800026f8]:lui s11, 1047568
[0x800026fc]:addi s11, s11, 3356
[0x80002700]:addi a4, zero, 64
[0x80002704]:csrrw zero, fcsr, a4
[0x80002708]:fsub.d t5, t3, s10, dyn
[0x8000270c]:csrrs a7, fcsr, zero

[0x80002708]:fsub.d t5, t3, s10, dyn
[0x8000270c]:csrrs a7, fcsr, zero
[0x80002710]:sw t5, 1512(ra)
[0x80002714]:sw t6, 1520(ra)
[0x80002718]:sw t5, 1528(ra)
[0x8000271c]:sw a7, 1536(ra)
[0x80002720]:lw t3, 1792(a6)
[0x80002724]:lw t4, 1796(a6)
[0x80002728]:lw s10, 1800(a6)
[0x8000272c]:lw s11, 1804(a6)
[0x80002730]:lui t3, 356775
[0x80002734]:addi t3, t3, 1326
[0x80002738]:lui t4, 523880
[0x8000273c]:addi t4, t4, 3919
[0x80002740]:lui s10, 498989
[0x80002744]:addi s10, s10, 877
[0x80002748]:lui s11, 1047568
[0x8000274c]:addi s11, s11, 3356
[0x80002750]:addi a4, zero, 96
[0x80002754]:csrrw zero, fcsr, a4
[0x80002758]:fsub.d t5, t3, s10, dyn
[0x8000275c]:csrrs a7, fcsr, zero

[0x80002758]:fsub.d t5, t3, s10, dyn
[0x8000275c]:csrrs a7, fcsr, zero
[0x80002760]:sw t5, 1544(ra)
[0x80002764]:sw t6, 1552(ra)
[0x80002768]:sw t5, 1560(ra)
[0x8000276c]:sw a7, 1568(ra)
[0x80002770]:lw t3, 1808(a6)
[0x80002774]:lw t4, 1812(a6)
[0x80002778]:lw s10, 1816(a6)
[0x8000277c]:lw s11, 1820(a6)
[0x80002780]:lui t3, 356775
[0x80002784]:addi t3, t3, 1326
[0x80002788]:lui t4, 523880
[0x8000278c]:addi t4, t4, 3919
[0x80002790]:lui s10, 498989
[0x80002794]:addi s10, s10, 877
[0x80002798]:lui s11, 1047568
[0x8000279c]:addi s11, s11, 3356
[0x800027a0]:addi a4, zero, 128
[0x800027a4]:csrrw zero, fcsr, a4
[0x800027a8]:fsub.d t5, t3, s10, dyn
[0x800027ac]:csrrs a7, fcsr, zero

[0x800027a8]:fsub.d t5, t3, s10, dyn
[0x800027ac]:csrrs a7, fcsr, zero
[0x800027b0]:sw t5, 1576(ra)
[0x800027b4]:sw t6, 1584(ra)
[0x800027b8]:sw t5, 1592(ra)
[0x800027bc]:sw a7, 1600(ra)
[0x800027c0]:lw t3, 1824(a6)
[0x800027c4]:lw t4, 1828(a6)
[0x800027c8]:lw s10, 1832(a6)
[0x800027cc]:lw s11, 1836(a6)
[0x800027d0]:lui t3, 396049
[0x800027d4]:addi t3, t3, 1513
[0x800027d8]:lui t4, 523648
[0x800027dc]:addi t4, t4, 2850
[0x800027e0]:addi s10, zero, 0
[0x800027e4]:lui s11, 524032
[0x800027e8]:addi a4, zero, 0
[0x800027ec]:csrrw zero, fcsr, a4
[0x800027f0]:fsub.d t5, t3, s10, dyn
[0x800027f4]:csrrs a7, fcsr, zero

[0x800027f0]:fsub.d t5, t3, s10, dyn
[0x800027f4]:csrrs a7, fcsr, zero
[0x800027f8]:sw t5, 1608(ra)
[0x800027fc]:sw t6, 1616(ra)
[0x80002800]:sw t5, 1624(ra)
[0x80002804]:sw a7, 1632(ra)
[0x80002808]:lw t3, 1840(a6)
[0x8000280c]:lw t4, 1844(a6)
[0x80002810]:lw s10, 1848(a6)
[0x80002814]:lw s11, 1852(a6)
[0x80002818]:lui t3, 396049
[0x8000281c]:addi t3, t3, 1513
[0x80002820]:lui t4, 523648
[0x80002824]:addi t4, t4, 2850
[0x80002828]:addi s10, zero, 0
[0x8000282c]:lui s11, 524032
[0x80002830]:addi a4, zero, 32
[0x80002834]:csrrw zero, fcsr, a4
[0x80002838]:fsub.d t5, t3, s10, dyn
[0x8000283c]:csrrs a7, fcsr, zero

[0x80002838]:fsub.d t5, t3, s10, dyn
[0x8000283c]:csrrs a7, fcsr, zero
[0x80002840]:sw t5, 1640(ra)
[0x80002844]:sw t6, 1648(ra)
[0x80002848]:sw t5, 1656(ra)
[0x8000284c]:sw a7, 1664(ra)
[0x80002850]:lw t3, 1856(a6)
[0x80002854]:lw t4, 1860(a6)
[0x80002858]:lw s10, 1864(a6)
[0x8000285c]:lw s11, 1868(a6)
[0x80002860]:lui t3, 396049
[0x80002864]:addi t3, t3, 1513
[0x80002868]:lui t4, 523648
[0x8000286c]:addi t4, t4, 2850
[0x80002870]:addi s10, zero, 0
[0x80002874]:lui s11, 524032
[0x80002878]:addi a4, zero, 64
[0x8000287c]:csrrw zero, fcsr, a4
[0x80002880]:fsub.d t5, t3, s10, dyn
[0x80002884]:csrrs a7, fcsr, zero

[0x80002880]:fsub.d t5, t3, s10, dyn
[0x80002884]:csrrs a7, fcsr, zero
[0x80002888]:sw t5, 1672(ra)
[0x8000288c]:sw t6, 1680(ra)
[0x80002890]:sw t5, 1688(ra)
[0x80002894]:sw a7, 1696(ra)
[0x80002898]:lw t3, 1872(a6)
[0x8000289c]:lw t4, 1876(a6)
[0x800028a0]:lw s10, 1880(a6)
[0x800028a4]:lw s11, 1884(a6)
[0x800028a8]:lui t3, 396049
[0x800028ac]:addi t3, t3, 1513
[0x800028b0]:lui t4, 523648
[0x800028b4]:addi t4, t4, 2850
[0x800028b8]:addi s10, zero, 0
[0x800028bc]:lui s11, 524032
[0x800028c0]:addi a4, zero, 96
[0x800028c4]:csrrw zero, fcsr, a4
[0x800028c8]:fsub.d t5, t3, s10, dyn
[0x800028cc]:csrrs a7, fcsr, zero

[0x800028c8]:fsub.d t5, t3, s10, dyn
[0x800028cc]:csrrs a7, fcsr, zero
[0x800028d0]:sw t5, 1704(ra)
[0x800028d4]:sw t6, 1712(ra)
[0x800028d8]:sw t5, 1720(ra)
[0x800028dc]:sw a7, 1728(ra)
[0x800028e0]:lw t3, 1888(a6)
[0x800028e4]:lw t4, 1892(a6)
[0x800028e8]:lw s10, 1896(a6)
[0x800028ec]:lw s11, 1900(a6)
[0x800028f0]:lui t3, 396049
[0x800028f4]:addi t3, t3, 1513
[0x800028f8]:lui t4, 523648
[0x800028fc]:addi t4, t4, 2850
[0x80002900]:addi s10, zero, 0
[0x80002904]:lui s11, 524032
[0x80002908]:addi a4, zero, 128
[0x8000290c]:csrrw zero, fcsr, a4
[0x80002910]:fsub.d t5, t3, s10, dyn
[0x80002914]:csrrs a7, fcsr, zero

[0x80002910]:fsub.d t5, t3, s10, dyn
[0x80002914]:csrrs a7, fcsr, zero
[0x80002918]:sw t5, 1736(ra)
[0x8000291c]:sw t6, 1744(ra)
[0x80002920]:sw t5, 1752(ra)
[0x80002924]:sw a7, 1760(ra)
[0x80002928]:lw t3, 1904(a6)
[0x8000292c]:lw t4, 1908(a6)
[0x80002930]:lw s10, 1912(a6)
[0x80002934]:lw s11, 1916(a6)
[0x80002938]:lui t3, 657128
[0x8000293c]:addi t3, t3, 1415
[0x80002940]:lui t4, 523780
[0x80002944]:addi t4, t4, 1287
[0x80002948]:lui s10, 152674
[0x8000294c]:addi s10, s10, 2170
[0x80002950]:lui s11, 1047812
[0x80002954]:addi s11, s11, 3875
[0x80002958]:addi a4, zero, 0
[0x8000295c]:csrrw zero, fcsr, a4
[0x80002960]:fsub.d t5, t3, s10, dyn
[0x80002964]:csrrs a7, fcsr, zero

[0x80002960]:fsub.d t5, t3, s10, dyn
[0x80002964]:csrrs a7, fcsr, zero
[0x80002968]:sw t5, 1768(ra)
[0x8000296c]:sw t6, 1776(ra)
[0x80002970]:sw t5, 1784(ra)
[0x80002974]:sw a7, 1792(ra)
[0x80002978]:lw t3, 1920(a6)
[0x8000297c]:lw t4, 1924(a6)
[0x80002980]:lw s10, 1928(a6)
[0x80002984]:lw s11, 1932(a6)
[0x80002988]:lui t3, 657128
[0x8000298c]:addi t3, t3, 1415
[0x80002990]:lui t4, 523780
[0x80002994]:addi t4, t4, 1287
[0x80002998]:lui s10, 152674
[0x8000299c]:addi s10, s10, 2170
[0x800029a0]:lui s11, 1047812
[0x800029a4]:addi s11, s11, 3875
[0x800029a8]:addi a4, zero, 32
[0x800029ac]:csrrw zero, fcsr, a4
[0x800029b0]:fsub.d t5, t3, s10, dyn
[0x800029b4]:csrrs a7, fcsr, zero

[0x800029b0]:fsub.d t5, t3, s10, dyn
[0x800029b4]:csrrs a7, fcsr, zero
[0x800029b8]:sw t5, 1800(ra)
[0x800029bc]:sw t6, 1808(ra)
[0x800029c0]:sw t5, 1816(ra)
[0x800029c4]:sw a7, 1824(ra)
[0x800029c8]:lw t3, 1936(a6)
[0x800029cc]:lw t4, 1940(a6)
[0x800029d0]:lw s10, 1944(a6)
[0x800029d4]:lw s11, 1948(a6)
[0x800029d8]:lui t3, 657128
[0x800029dc]:addi t3, t3, 1415
[0x800029e0]:lui t4, 523780
[0x800029e4]:addi t4, t4, 1287
[0x800029e8]:lui s10, 152674
[0x800029ec]:addi s10, s10, 2170
[0x800029f0]:lui s11, 1047812
[0x800029f4]:addi s11, s11, 3875
[0x800029f8]:addi a4, zero, 64
[0x800029fc]:csrrw zero, fcsr, a4
[0x80002a00]:fsub.d t5, t3, s10, dyn
[0x80002a04]:csrrs a7, fcsr, zero

[0x80002a00]:fsub.d t5, t3, s10, dyn
[0x80002a04]:csrrs a7, fcsr, zero
[0x80002a08]:sw t5, 1832(ra)
[0x80002a0c]:sw t6, 1840(ra)
[0x80002a10]:sw t5, 1848(ra)
[0x80002a14]:sw a7, 1856(ra)
[0x80002a18]:lw t3, 1952(a6)
[0x80002a1c]:lw t4, 1956(a6)
[0x80002a20]:lw s10, 1960(a6)
[0x80002a24]:lw s11, 1964(a6)
[0x80002a28]:lui t3, 657128
[0x80002a2c]:addi t3, t3, 1415
[0x80002a30]:lui t4, 523780
[0x80002a34]:addi t4, t4, 1287
[0x80002a38]:lui s10, 152674
[0x80002a3c]:addi s10, s10, 2170
[0x80002a40]:lui s11, 1047812
[0x80002a44]:addi s11, s11, 3875
[0x80002a48]:addi a4, zero, 96
[0x80002a4c]:csrrw zero, fcsr, a4
[0x80002a50]:fsub.d t5, t3, s10, dyn
[0x80002a54]:csrrs a7, fcsr, zero

[0x80002a50]:fsub.d t5, t3, s10, dyn
[0x80002a54]:csrrs a7, fcsr, zero
[0x80002a58]:sw t5, 1864(ra)
[0x80002a5c]:sw t6, 1872(ra)
[0x80002a60]:sw t5, 1880(ra)
[0x80002a64]:sw a7, 1888(ra)
[0x80002a68]:lw t3, 1968(a6)
[0x80002a6c]:lw t4, 1972(a6)
[0x80002a70]:lw s10, 1976(a6)
[0x80002a74]:lw s11, 1980(a6)
[0x80002a78]:lui t3, 657128
[0x80002a7c]:addi t3, t3, 1415
[0x80002a80]:lui t4, 523780
[0x80002a84]:addi t4, t4, 1287
[0x80002a88]:lui s10, 152674
[0x80002a8c]:addi s10, s10, 2170
[0x80002a90]:lui s11, 1047812
[0x80002a94]:addi s11, s11, 3875
[0x80002a98]:addi a4, zero, 128
[0x80002a9c]:csrrw zero, fcsr, a4
[0x80002aa0]:fsub.d t5, t3, s10, dyn
[0x80002aa4]:csrrs a7, fcsr, zero

[0x80002aa0]:fsub.d t5, t3, s10, dyn
[0x80002aa4]:csrrs a7, fcsr, zero
[0x80002aa8]:sw t5, 1896(ra)
[0x80002aac]:sw t6, 1904(ra)
[0x80002ab0]:sw t5, 1912(ra)
[0x80002ab4]:sw a7, 1920(ra)
[0x80002ab8]:lw t3, 1984(a6)
[0x80002abc]:lw t4, 1988(a6)
[0x80002ac0]:lw s10, 1992(a6)
[0x80002ac4]:lw s11, 1996(a6)
[0x80002ac8]:lui t3, 46530
[0x80002acc]:addi t3, t3, 3136
[0x80002ad0]:lui t4, 523787
[0x80002ad4]:addi t4, t4, 2951
[0x80002ad8]:addi s10, zero, 0
[0x80002adc]:lui s11, 524032
[0x80002ae0]:addi a4, zero, 0
[0x80002ae4]:csrrw zero, fcsr, a4
[0x80002ae8]:fsub.d t5, t3, s10, dyn
[0x80002aec]:csrrs a7, fcsr, zero

[0x80002ae8]:fsub.d t5, t3, s10, dyn
[0x80002aec]:csrrs a7, fcsr, zero
[0x80002af0]:sw t5, 1928(ra)
[0x80002af4]:sw t6, 1936(ra)
[0x80002af8]:sw t5, 1944(ra)
[0x80002afc]:sw a7, 1952(ra)
[0x80002b00]:lw t3, 2000(a6)
[0x80002b04]:lw t4, 2004(a6)
[0x80002b08]:lw s10, 2008(a6)
[0x80002b0c]:lw s11, 2012(a6)
[0x80002b10]:lui t3, 46530
[0x80002b14]:addi t3, t3, 3136
[0x80002b18]:lui t4, 523787
[0x80002b1c]:addi t4, t4, 2951
[0x80002b20]:addi s10, zero, 0
[0x80002b24]:lui s11, 524032
[0x80002b28]:addi a4, zero, 32
[0x80002b2c]:csrrw zero, fcsr, a4
[0x80002b30]:fsub.d t5, t3, s10, dyn
[0x80002b34]:csrrs a7, fcsr, zero

[0x80002b30]:fsub.d t5, t3, s10, dyn
[0x80002b34]:csrrs a7, fcsr, zero
[0x80002b38]:sw t5, 1960(ra)
[0x80002b3c]:sw t6, 1968(ra)
[0x80002b40]:sw t5, 1976(ra)
[0x80002b44]:sw a7, 1984(ra)
[0x80002b48]:lw t3, 2016(a6)
[0x80002b4c]:lw t4, 2020(a6)
[0x80002b50]:lw s10, 2024(a6)
[0x80002b54]:lw s11, 2028(a6)
[0x80002b58]:lui t3, 46530
[0x80002b5c]:addi t3, t3, 3136
[0x80002b60]:lui t4, 523787
[0x80002b64]:addi t4, t4, 2951
[0x80002b68]:addi s10, zero, 0
[0x80002b6c]:lui s11, 524032
[0x80002b70]:addi a4, zero, 64
[0x80002b74]:csrrw zero, fcsr, a4
[0x80002b78]:fsub.d t5, t3, s10, dyn
[0x80002b7c]:csrrs a7, fcsr, zero

[0x80002b78]:fsub.d t5, t3, s10, dyn
[0x80002b7c]:csrrs a7, fcsr, zero
[0x80002b80]:sw t5, 1992(ra)
[0x80002b84]:sw t6, 2000(ra)
[0x80002b88]:sw t5, 2008(ra)
[0x80002b8c]:sw a7, 2016(ra)
[0x80002b90]:lw t3, 2032(a6)
[0x80002b94]:lw t4, 2036(a6)
[0x80002b98]:lw s10, 2040(a6)
[0x80002b9c]:lw s11, 2044(a6)
[0x80002ba0]:lui t3, 46530
[0x80002ba4]:addi t3, t3, 3136
[0x80002ba8]:lui t4, 523787
[0x80002bac]:addi t4, t4, 2951
[0x80002bb0]:addi s10, zero, 0
[0x80002bb4]:lui s11, 524032
[0x80002bb8]:addi a4, zero, 96
[0x80002bbc]:csrrw zero, fcsr, a4
[0x80002bc0]:fsub.d t5, t3, s10, dyn
[0x80002bc4]:csrrs a7, fcsr, zero

[0x80002bc0]:fsub.d t5, t3, s10, dyn
[0x80002bc4]:csrrs a7, fcsr, zero
[0x80002bc8]:sw t5, 2024(ra)
[0x80002bcc]:sw t6, 2032(ra)
[0x80002bd0]:sw t5, 2040(ra)
[0x80002bd4]:addi ra, ra, 2040
[0x80002bd8]:sw a7, 8(ra)
[0x80002bdc]:lui a4, 1
[0x80002be0]:addi a4, a4, 2048
[0x80002be4]:add a6, a6, a4
[0x80002be8]:lw t3, 0(a6)
[0x80002bec]:sub a6, a6, a4
[0x80002bf0]:lui a4, 1
[0x80002bf4]:addi a4, a4, 2048
[0x80002bf8]:add a6, a6, a4
[0x80002bfc]:lw t4, 4(a6)
[0x80002c00]:sub a6, a6, a4
[0x80002c04]:lui a4, 1
[0x80002c08]:addi a4, a4, 2048
[0x80002c0c]:add a6, a6, a4
[0x80002c10]:lw s10, 8(a6)
[0x80002c14]:sub a6, a6, a4
[0x80002c18]:lui a4, 1
[0x80002c1c]:addi a4, a4, 2048
[0x80002c20]:add a6, a6, a4
[0x80002c24]:lw s11, 12(a6)
[0x80002c28]:sub a6, a6, a4
[0x80002c2c]:lui t3, 46530
[0x80002c30]:addi t3, t3, 3136
[0x80002c34]:lui t4, 523787
[0x80002c38]:addi t4, t4, 2951
[0x80002c3c]:addi s10, zero, 0
[0x80002c40]:lui s11, 524032
[0x80002c44]:addi a4, zero, 128
[0x80002c48]:csrrw zero, fcsr, a4
[0x80002c4c]:fsub.d t5, t3, s10, dyn
[0x80002c50]:csrrs a7, fcsr, zero

[0x80002c4c]:fsub.d t5, t3, s10, dyn
[0x80002c50]:csrrs a7, fcsr, zero
[0x80002c54]:sw t5, 16(ra)
[0x80002c58]:sw t6, 24(ra)
[0x80002c5c]:sw t5, 32(ra)
[0x80002c60]:sw a7, 40(ra)
[0x80002c64]:lui a4, 1
[0x80002c68]:addi a4, a4, 2048
[0x80002c6c]:add a6, a6, a4
[0x80002c70]:lw t3, 16(a6)
[0x80002c74]:sub a6, a6, a4
[0x80002c78]:lui a4, 1
[0x80002c7c]:addi a4, a4, 2048
[0x80002c80]:add a6, a6, a4
[0x80002c84]:lw t4, 20(a6)
[0x80002c88]:sub a6, a6, a4
[0x80002c8c]:lui a4, 1
[0x80002c90]:addi a4, a4, 2048
[0x80002c94]:add a6, a6, a4
[0x80002c98]:lw s10, 24(a6)
[0x80002c9c]:sub a6, a6, a4
[0x80002ca0]:lui a4, 1
[0x80002ca4]:addi a4, a4, 2048
[0x80002ca8]:add a6, a6, a4
[0x80002cac]:lw s11, 28(a6)
[0x80002cb0]:sub a6, a6, a4
[0x80002cb4]:lui t3, 303639
[0x80002cb8]:addi t3, t3, 489
[0x80002cbc]:lui t4, 523720
[0x80002cc0]:addi t4, t4, 313
[0x80002cc4]:lui s10, 372468
[0x80002cc8]:addi s10, s10, 1802
[0x80002ccc]:lui s11, 1048092
[0x80002cd0]:addi s11, s11, 3939
[0x80002cd4]:addi a4, zero, 0
[0x80002cd8]:csrrw zero, fcsr, a4
[0x80002cdc]:fsub.d t5, t3, s10, dyn
[0x80002ce0]:csrrs a7, fcsr, zero

[0x80002cdc]:fsub.d t5, t3, s10, dyn
[0x80002ce0]:csrrs a7, fcsr, zero
[0x80002ce4]:sw t5, 48(ra)
[0x80002ce8]:sw t6, 56(ra)
[0x80002cec]:sw t5, 64(ra)
[0x80002cf0]:sw a7, 72(ra)
[0x80002cf4]:lui a4, 1
[0x80002cf8]:addi a4, a4, 2048
[0x80002cfc]:add a6, a6, a4
[0x80002d00]:lw t3, 32(a6)
[0x80002d04]:sub a6, a6, a4
[0x80002d08]:lui a4, 1
[0x80002d0c]:addi a4, a4, 2048
[0x80002d10]:add a6, a6, a4
[0x80002d14]:lw t4, 36(a6)
[0x80002d18]:sub a6, a6, a4
[0x80002d1c]:lui a4, 1
[0x80002d20]:addi a4, a4, 2048
[0x80002d24]:add a6, a6, a4
[0x80002d28]:lw s10, 40(a6)
[0x80002d2c]:sub a6, a6, a4
[0x80002d30]:lui a4, 1
[0x80002d34]:addi a4, a4, 2048
[0x80002d38]:add a6, a6, a4
[0x80002d3c]:lw s11, 44(a6)
[0x80002d40]:sub a6, a6, a4
[0x80002d44]:lui t3, 303639
[0x80002d48]:addi t3, t3, 489
[0x80002d4c]:lui t4, 523720
[0x80002d50]:addi t4, t4, 313
[0x80002d54]:lui s10, 372468
[0x80002d58]:addi s10, s10, 1802
[0x80002d5c]:lui s11, 1048092
[0x80002d60]:addi s11, s11, 3939
[0x80002d64]:addi a4, zero, 128
[0x80002d68]:csrrw zero, fcsr, a4
[0x80002d6c]:fsub.d t5, t3, s10, dyn
[0x80002d70]:csrrs a7, fcsr, zero

[0x80002d6c]:fsub.d t5, t3, s10, dyn
[0x80002d70]:csrrs a7, fcsr, zero
[0x80002d74]:sw t5, 80(ra)
[0x80002d78]:sw t6, 88(ra)
[0x80002d7c]:sw t5, 96(ra)
[0x80002d80]:sw a7, 104(ra)
[0x80002d84]:addi zero, zero, 0
[0x80002d88]:addi zero, zero, 0
[0x80002d8c]:addi zero, zero, 0



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fsub.d', 'rs1 : x30', 'rs2 : x30', 'rd : x30', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x8000014c]:fsub.d t5, t5, t5, dyn
	-[0x80000150]:csrrs tp, fcsr, zero
	-[0x80000154]:sw t5, 0(ra)
	-[0x80000158]:sw t6, 8(ra)
Current Store : [0x80000158] : sw t6, 8(ra) -- Store: [0x80004a20]:0x7FDC8139




Last Coverpoint : ['mnemonic : fsub.d', 'rs1 : x30', 'rs2 : x30', 'rd : x30', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x8000014c]:fsub.d t5, t5, t5, dyn
	-[0x80000150]:csrrs tp, fcsr, zero
	-[0x80000154]:sw t5, 0(ra)
	-[0x80000158]:sw t6, 8(ra)
	-[0x8000015c]:sw t5, 16(ra)
Current Store : [0x8000015c] : sw t5, 16(ra) -- Store: [0x80004a28]:0x00000000




Last Coverpoint : ['rs1 : x26', 'rs2 : x24', 'rd : x28', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc81394a2171e9 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x1bf635aef470a and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000019c]:fsub.d t3, s10, s8, dyn
	-[0x800001a0]:csrrs tp, fcsr, zero
	-[0x800001a4]:sw t3, 32(ra)
	-[0x800001a8]:sw t4, 40(ra)
Current Store : [0x800001a8] : sw t4, 40(ra) -- Store: [0x80004a40]:0xEEDBEADF




Last Coverpoint : ['rs1 : x26', 'rs2 : x24', 'rd : x28', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc81394a2171e9 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x1bf635aef470a and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000019c]:fsub.d t3, s10, s8, dyn
	-[0x800001a0]:csrrs tp, fcsr, zero
	-[0x800001a4]:sw t3, 32(ra)
	-[0x800001a8]:sw t4, 40(ra)
	-[0x800001ac]:sw t3, 48(ra)
Current Store : [0x800001ac] : sw t3, 48(ra) -- Store: [0x80004a48]:0xFFFFFFFE




Last Coverpoint : ['rs1 : x24', 'rs2 : x28', 'rd : x24', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc81394a2171e9 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x1bf635aef470a and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001ec]:fsub.d s8, s8, t3, dyn
	-[0x800001f0]:csrrs tp, fcsr, zero
	-[0x800001f4]:sw s8, 64(ra)
	-[0x800001f8]:sw s9, 72(ra)
Current Store : [0x800001f8] : sw s9, 72(ra) -- Store: [0x80004a60]:0x7FDC8139




Last Coverpoint : ['rs1 : x24', 'rs2 : x28', 'rd : x24', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc81394a2171e9 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x1bf635aef470a and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001ec]:fsub.d s8, s8, t3, dyn
	-[0x800001f0]:csrrs tp, fcsr, zero
	-[0x800001f4]:sw s8, 64(ra)
	-[0x800001f8]:sw s9, 72(ra)
	-[0x800001fc]:sw s8, 80(ra)
Current Store : [0x800001fc] : sw s8, 80(ra) -- Store: [0x80004a68]:0xFFFFFFFE




Last Coverpoint : ['rs1 : x28', 'rs2 : x26', 'rd : x26', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc81394a2171e9 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x1bf635aef470a and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000023c]:fsub.d s10, t3, s10, dyn
	-[0x80000240]:csrrs tp, fcsr, zero
	-[0x80000244]:sw s10, 96(ra)
	-[0x80000248]:sw s11, 104(ra)
Current Store : [0x80000248] : sw s11, 104(ra) -- Store: [0x80004a80]:0xFFE1BF63




Last Coverpoint : ['rs1 : x28', 'rs2 : x26', 'rd : x26', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc81394a2171e9 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x1bf635aef470a and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000023c]:fsub.d s10, t3, s10, dyn
	-[0x80000240]:csrrs tp, fcsr, zero
	-[0x80000244]:sw s10, 96(ra)
	-[0x80000248]:sw s11, 104(ra)
	-[0x8000024c]:sw s10, 112(ra)
Current Store : [0x8000024c] : sw s10, 112(ra) -- Store: [0x80004a88]:0xFFFFFFFF




Last Coverpoint : ['rs1 : x20', 'rs2 : x20', 'rd : x22', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x8000028c]:fsub.d s6, s4, s4, dyn
	-[0x80000290]:csrrs tp, fcsr, zero
	-[0x80000294]:sw s6, 128(ra)
	-[0x80000298]:sw s7, 136(ra)
Current Store : [0x80000298] : sw s7, 136(ra) -- Store: [0x80004aa0]:0xB6FAB7FB




Last Coverpoint : ['rs1 : x20', 'rs2 : x20', 'rd : x22', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x8000028c]:fsub.d s6, s4, s4, dyn
	-[0x80000290]:csrrs tp, fcsr, zero
	-[0x80000294]:sw s6, 128(ra)
	-[0x80000298]:sw s7, 136(ra)
	-[0x8000029c]:sw s6, 144(ra)
Current Store : [0x8000029c] : sw s6, 144(ra) -- Store: [0x80004aa8]:0x00000000




Last Coverpoint : ['rs1 : x22', 'rs2 : x18', 'rd : x20', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0xd481499755d4b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8adfad9a2a8ab and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002dc]:fsub.d s4, s6, s2, dyn
	-[0x800002e0]:csrrs tp, fcsr, zero
	-[0x800002e4]:sw s4, 160(ra)
	-[0x800002e8]:sw s5, 168(ra)
Current Store : [0x800002e8] : sw s5, 168(ra) -- Store: [0x80004ac0]:0x7FDC8139




Last Coverpoint : ['rs1 : x22', 'rs2 : x18', 'rd : x20', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0xd481499755d4b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8adfad9a2a8ab and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002dc]:fsub.d s4, s6, s2, dyn
	-[0x800002e0]:csrrs tp, fcsr, zero
	-[0x800002e4]:sw s4, 160(ra)
	-[0x800002e8]:sw s5, 168(ra)
	-[0x800002ec]:sw s4, 176(ra)
Current Store : [0x800002ec] : sw s4, 176(ra) -- Store: [0x80004ac8]:0xFFFFFFFE




Last Coverpoint : ['rs1 : x16', 'rs2 : x22', 'rd : x18', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0xd481499755d4b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8adfad9a2a8ab and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000032c]:fsub.d s2, a6, s6, dyn
	-[0x80000330]:csrrs tp, fcsr, zero
	-[0x80000334]:sw s2, 192(ra)
	-[0x80000338]:sw s3, 200(ra)
Current Store : [0x80000338] : sw s3, 200(ra) -- Store: [0x80004ae0]:0xFFE8ADFA




Last Coverpoint : ['rs1 : x16', 'rs2 : x22', 'rd : x18', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0xd481499755d4b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8adfad9a2a8ab and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000032c]:fsub.d s2, a6, s6, dyn
	-[0x80000330]:csrrs tp, fcsr, zero
	-[0x80000334]:sw s2, 192(ra)
	-[0x80000338]:sw s3, 200(ra)
	-[0x8000033c]:sw s2, 208(ra)
Current Store : [0x8000033c] : sw s2, 208(ra) -- Store: [0x80004ae8]:0xFFFFFFFD




Last Coverpoint : ['rs1 : x18', 'rs2 : x14', 'rd : x16', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0xd481499755d4b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8adfad9a2a8ab and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000037c]:fsub.d a6, s2, a4, dyn
	-[0x80000380]:csrrs tp, fcsr, zero
	-[0x80000384]:sw a6, 224(ra)
	-[0x80000388]:sw a7, 232(ra)
Current Store : [0x80000388] : sw a7, 232(ra) -- Store: [0x80004b00]:0x7FCD4814




Last Coverpoint : ['rs1 : x18', 'rs2 : x14', 'rd : x16', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0xd481499755d4b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8adfad9a2a8ab and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000037c]:fsub.d a6, s2, a4, dyn
	-[0x80000380]:csrrs tp, fcsr, zero
	-[0x80000384]:sw a6, 224(ra)
	-[0x80000388]:sw a7, 232(ra)
	-[0x8000038c]:sw a6, 240(ra)
Current Store : [0x8000038c] : sw a6, 240(ra) -- Store: [0x80004b08]:0xFFFFFFFD




Last Coverpoint : ['rs1 : x12', 'rs2 : x16', 'rd : x14', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0xd481499755d4b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8adfad9a2a8ab and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003cc]:fsub.d a4, a2, a6, dyn
	-[0x800003d0]:csrrs tp, fcsr, zero
	-[0x800003d4]:sw a4, 256(ra)
	-[0x800003d8]:sw a5, 264(ra)
Current Store : [0x800003d8] : sw a5, 264(ra) -- Store: [0x80004b20]:0xFFE8ADFA




Last Coverpoint : ['rs1 : x12', 'rs2 : x16', 'rd : x14', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0xd481499755d4b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8adfad9a2a8ab and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003cc]:fsub.d a4, a2, a6, dyn
	-[0x800003d0]:csrrs tp, fcsr, zero
	-[0x800003d4]:sw a4, 256(ra)
	-[0x800003d8]:sw a5, 264(ra)
	-[0x800003dc]:sw a4, 272(ra)
Current Store : [0x800003dc] : sw a4, 272(ra) -- Store: [0x80004b28]:0xFFFFFFFE




Last Coverpoint : ['rs1 : x14', 'rs2 : x10', 'rd : x12', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0xd481499755d4b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8adfad9a2a8ab and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000424]:fsub.d a2, a4, a0, dyn
	-[0x80000428]:csrrs a7, fcsr, zero
	-[0x8000042c]:sw a2, 288(ra)
	-[0x80000430]:sw a3, 296(ra)
Current Store : [0x80000430] : sw a3, 296(ra) -- Store: [0x80004b40]:0x7FCD4814




Last Coverpoint : ['rs1 : x14', 'rs2 : x10', 'rd : x12', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0xd481499755d4b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8adfad9a2a8ab and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000424]:fsub.d a2, a4, a0, dyn
	-[0x80000428]:csrrs a7, fcsr, zero
	-[0x8000042c]:sw a2, 288(ra)
	-[0x80000430]:sw a3, 296(ra)
	-[0x80000434]:sw a2, 304(ra)
Current Store : [0x80000434] : sw a2, 304(ra) -- Store: [0x80004b48]:0xFFFFFFFE




Last Coverpoint : ['rs1 : x8', 'rs2 : x12', 'rd : x10', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd87e65450c45 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x93c0cd5d79dd0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000474]:fsub.d a0, fp, a2, dyn
	-[0x80000478]:csrrs a7, fcsr, zero
	-[0x8000047c]:sw a0, 320(ra)
	-[0x80000480]:sw a1, 328(ra)
Current Store : [0x80000480] : sw a1, 328(ra) -- Store: [0x80004b60]:0xFFE8ADFA




Last Coverpoint : ['rs1 : x8', 'rs2 : x12', 'rd : x10', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd87e65450c45 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x93c0cd5d79dd0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000474]:fsub.d a0, fp, a2, dyn
	-[0x80000478]:csrrs a7, fcsr, zero
	-[0x8000047c]:sw a0, 320(ra)
	-[0x80000480]:sw a1, 328(ra)
	-[0x80000484]:sw a0, 336(ra)
Current Store : [0x80000484] : sw a0, 336(ra) -- Store: [0x80004b68]:0xFFFFFFFF




Last Coverpoint : ['rs1 : x10', 'rs2 : x6', 'rd : x8', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd87e65450c45 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x93c0cd5d79dd0 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004cc]:fsub.d fp, a0, t1, dyn
	-[0x800004d0]:csrrs a7, fcsr, zero
	-[0x800004d4]:sw fp, 0(ra)
	-[0x800004d8]:sw s1, 8(ra)
Current Store : [0x800004d8] : sw s1, 8(ra) -- Store: [0x80004ad0]:0x7FECD87E




Last Coverpoint : ['rs1 : x10', 'rs2 : x6', 'rd : x8', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd87e65450c45 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x93c0cd5d79dd0 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004cc]:fsub.d fp, a0, t1, dyn
	-[0x800004d0]:csrrs a7, fcsr, zero
	-[0x800004d4]:sw fp, 0(ra)
	-[0x800004d8]:sw s1, 8(ra)
	-[0x800004dc]:sw fp, 16(ra)
Current Store : [0x800004dc] : sw fp, 16(ra) -- Store: [0x80004ad8]:0xFFFFFFFF




Last Coverpoint : ['rs1 : x4', 'rs2 : x8', 'rd : x6', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd87e65450c45 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x93c0cd5d79dd0 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000051c]:fsub.d t1, tp, fp, dyn
	-[0x80000520]:csrrs a7, fcsr, zero
	-[0x80000524]:sw t1, 32(ra)
	-[0x80000528]:sw t2, 40(ra)
Current Store : [0x80000528] : sw t2, 40(ra) -- Store: [0x80004af0]:0xFFB93C0C




Last Coverpoint : ['rs1 : x4', 'rs2 : x8', 'rd : x6', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd87e65450c45 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x93c0cd5d79dd0 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000051c]:fsub.d t1, tp, fp, dyn
	-[0x80000520]:csrrs a7, fcsr, zero
	-[0x80000524]:sw t1, 32(ra)
	-[0x80000528]:sw t2, 40(ra)
	-[0x8000052c]:sw t1, 48(ra)
Current Store : [0x8000052c] : sw t1, 48(ra) -- Store: [0x80004af8]:0xFFFFFFFF




Last Coverpoint : ['rs1 : x6', 'rs2 : x2', 'rd : x4', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd87e65450c45 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x93c0cd5d79dd0 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000056c]:fsub.d tp, t1, sp, dyn
	-[0x80000570]:csrrs a7, fcsr, zero
	-[0x80000574]:sw tp, 64(ra)
	-[0x80000578]:sw t0, 72(ra)
Current Store : [0x80000578] : sw t0, 72(ra) -- Store: [0x80004b10]:0x7FECD87E




Last Coverpoint : ['rs1 : x6', 'rs2 : x2', 'rd : x4', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd87e65450c45 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x93c0cd5d79dd0 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000056c]:fsub.d tp, t1, sp, dyn
	-[0x80000570]:csrrs a7, fcsr, zero
	-[0x80000574]:sw tp, 64(ra)
	-[0x80000578]:sw t0, 72(ra)
	-[0x8000057c]:sw tp, 80(ra)
Current Store : [0x8000057c] : sw tp, 80(ra) -- Store: [0x80004b18]:0xFFFFFFFF




Last Coverpoint : ['rs1 : x2', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd87e65450c45 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x93c0cd5d79dd0 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005bc]:fsub.d t5, sp, t3, dyn
	-[0x800005c0]:csrrs a7, fcsr, zero
	-[0x800005c4]:sw t5, 96(ra)
	-[0x800005c8]:sw t6, 104(ra)
Current Store : [0x800005c8] : sw t6, 104(ra) -- Store: [0x80004b30]:0x7FDC8139




Last Coverpoint : ['rs1 : x2', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd87e65450c45 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x93c0cd5d79dd0 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005bc]:fsub.d t5, sp, t3, dyn
	-[0x800005c0]:csrrs a7, fcsr, zero
	-[0x800005c4]:sw t5, 96(ra)
	-[0x800005c8]:sw t6, 104(ra)
	-[0x800005cc]:sw t5, 112(ra)
Current Store : [0x800005cc] : sw t5, 112(ra) -- Store: [0x80004b38]:0xFFFFFFFF




Last Coverpoint : ['rs2 : x4', 'fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xa0e7ad32453df and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf2f8c2966dd5f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000060c]:fsub.d t5, t3, tp, dyn
	-[0x80000610]:csrrs a7, fcsr, zero
	-[0x80000614]:sw t5, 128(ra)
	-[0x80000618]:sw t6, 136(ra)
Current Store : [0x80000618] : sw t6, 136(ra) -- Store: [0x80004b50]:0x7FDC8139




Last Coverpoint : ['rs2 : x4', 'fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xa0e7ad32453df and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf2f8c2966dd5f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000060c]:fsub.d t5, t3, tp, dyn
	-[0x80000610]:csrrs a7, fcsr, zero
	-[0x80000614]:sw t5, 128(ra)
	-[0x80000618]:sw t6, 136(ra)
	-[0x8000061c]:sw t5, 144(ra)
Current Store : [0x8000061c] : sw t5, 144(ra) -- Store: [0x80004b58]:0xFFFFFFFE




Last Coverpoint : ['rd : x2', 'fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xa0e7ad32453df and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf2f8c2966dd5f and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000065c]:fsub.d sp, t5, t3, dyn
	-[0x80000660]:csrrs a7, fcsr, zero
	-[0x80000664]:sw sp, 160(ra)
	-[0x80000668]:sw gp, 168(ra)
Current Store : [0x80000668] : sw gp, 168(ra) -- Store: [0x80004b70]:0x7FECD87E




Last Coverpoint : ['rd : x2', 'fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xa0e7ad32453df and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf2f8c2966dd5f and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000065c]:fsub.d sp, t5, t3, dyn
	-[0x80000660]:csrrs a7, fcsr, zero
	-[0x80000664]:sw sp, 160(ra)
	-[0x80000668]:sw gp, 168(ra)
	-[0x8000066c]:sw sp, 176(ra)
Current Store : [0x8000066c] : sw sp, 176(ra) -- Store: [0x80004b78]:0xFFFFFFFD




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xa0e7ad32453df and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf2f8c2966dd5f and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006ac]:fsub.d t5, t3, s10, dyn
	-[0x800006b0]:csrrs a7, fcsr, zero
	-[0x800006b4]:sw t5, 192(ra)
	-[0x800006b8]:sw t6, 200(ra)
Current Store : [0x800006b8] : sw t6, 200(ra) -- Store: [0x80004b90]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xa0e7ad32453df and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf2f8c2966dd5f and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006ac]:fsub.d t5, t3, s10, dyn
	-[0x800006b0]:csrrs a7, fcsr, zero
	-[0x800006b4]:sw t5, 192(ra)
	-[0x800006b8]:sw t6, 200(ra)
	-[0x800006bc]:sw t5, 208(ra)
Current Store : [0x800006bc] : sw t5, 208(ra) -- Store: [0x80004b98]:0xFFFFFFFD




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xa0e7ad32453df and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf2f8c2966dd5f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006fc]:fsub.d t5, t3, s10, dyn
	-[0x80000700]:csrrs a7, fcsr, zero
	-[0x80000704]:sw t5, 224(ra)
	-[0x80000708]:sw t6, 232(ra)
Current Store : [0x80000708] : sw t6, 232(ra) -- Store: [0x80004bb0]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xa0e7ad32453df and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf2f8c2966dd5f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006fc]:fsub.d t5, t3, s10, dyn
	-[0x80000700]:csrrs a7, fcsr, zero
	-[0x80000704]:sw t5, 224(ra)
	-[0x80000708]:sw t6, 232(ra)
	-[0x8000070c]:sw t5, 240(ra)
Current Store : [0x8000070c] : sw t5, 240(ra) -- Store: [0x80004bb8]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xa0e7ad32453df and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf2f8c2966dd5f and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000074c]:fsub.d t5, t3, s10, dyn
	-[0x80000750]:csrrs a7, fcsr, zero
	-[0x80000754]:sw t5, 256(ra)
	-[0x80000758]:sw t6, 264(ra)
Current Store : [0x80000758] : sw t6, 264(ra) -- Store: [0x80004bd0]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xa0e7ad32453df and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf2f8c2966dd5f and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000074c]:fsub.d t5, t3, s10, dyn
	-[0x80000750]:csrrs a7, fcsr, zero
	-[0x80000754]:sw t5, 256(ra)
	-[0x80000758]:sw t6, 264(ra)
	-[0x8000075c]:sw t5, 272(ra)
Current Store : [0x8000075c] : sw t5, 272(ra) -- Store: [0x80004bd8]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe0d828b86622a and fs2 == 1 and fe2 == 0x7fa and fm2 == 0xf27d74799dd4f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000079c]:fsub.d t5, t3, s10, dyn
	-[0x800007a0]:csrrs a7, fcsr, zero
	-[0x800007a4]:sw t5, 288(ra)
	-[0x800007a8]:sw t6, 296(ra)
Current Store : [0x800007a8] : sw t6, 296(ra) -- Store: [0x80004bf0]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe0d828b86622a and fs2 == 1 and fe2 == 0x7fa and fm2 == 0xf27d74799dd4f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000079c]:fsub.d t5, t3, s10, dyn
	-[0x800007a0]:csrrs a7, fcsr, zero
	-[0x800007a4]:sw t5, 288(ra)
	-[0x800007a8]:sw t6, 296(ra)
	-[0x800007ac]:sw t5, 304(ra)
Current Store : [0x800007ac] : sw t5, 304(ra) -- Store: [0x80004bf8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe0d828b86622a and fs2 == 1 and fe2 == 0x7fa and fm2 == 0xf27d74799dd4f and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007ec]:fsub.d t5, t3, s10, dyn
	-[0x800007f0]:csrrs a7, fcsr, zero
	-[0x800007f4]:sw t5, 320(ra)
	-[0x800007f8]:sw t6, 328(ra)
Current Store : [0x800007f8] : sw t6, 328(ra) -- Store: [0x80004c10]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe0d828b86622a and fs2 == 1 and fe2 == 0x7fa and fm2 == 0xf27d74799dd4f and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007ec]:fsub.d t5, t3, s10, dyn
	-[0x800007f0]:csrrs a7, fcsr, zero
	-[0x800007f4]:sw t5, 320(ra)
	-[0x800007f8]:sw t6, 328(ra)
	-[0x800007fc]:sw t5, 336(ra)
Current Store : [0x800007fc] : sw t5, 336(ra) -- Store: [0x80004c18]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe0d828b86622a and fs2 == 1 and fe2 == 0x7fa and fm2 == 0xf27d74799dd4f and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000083c]:fsub.d t5, t3, s10, dyn
	-[0x80000840]:csrrs a7, fcsr, zero
	-[0x80000844]:sw t5, 352(ra)
	-[0x80000848]:sw t6, 360(ra)
Current Store : [0x80000848] : sw t6, 360(ra) -- Store: [0x80004c30]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe0d828b86622a and fs2 == 1 and fe2 == 0x7fa and fm2 == 0xf27d74799dd4f and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000083c]:fsub.d t5, t3, s10, dyn
	-[0x80000840]:csrrs a7, fcsr, zero
	-[0x80000844]:sw t5, 352(ra)
	-[0x80000848]:sw t6, 360(ra)
	-[0x8000084c]:sw t5, 368(ra)
Current Store : [0x8000084c] : sw t5, 368(ra) -- Store: [0x80004c38]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe0d828b86622a and fs2 == 1 and fe2 == 0x7fa and fm2 == 0xf27d74799dd4f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000088c]:fsub.d t5, t3, s10, dyn
	-[0x80000890]:csrrs a7, fcsr, zero
	-[0x80000894]:sw t5, 384(ra)
	-[0x80000898]:sw t6, 392(ra)
Current Store : [0x80000898] : sw t6, 392(ra) -- Store: [0x80004c50]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe0d828b86622a and fs2 == 1 and fe2 == 0x7fa and fm2 == 0xf27d74799dd4f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000088c]:fsub.d t5, t3, s10, dyn
	-[0x80000890]:csrrs a7, fcsr, zero
	-[0x80000894]:sw t5, 384(ra)
	-[0x80000898]:sw t6, 392(ra)
	-[0x8000089c]:sw t5, 400(ra)
Current Store : [0x8000089c] : sw t5, 400(ra) -- Store: [0x80004c58]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe0d828b86622a and fs2 == 1 and fe2 == 0x7fa and fm2 == 0xf27d74799dd4f and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008dc]:fsub.d t5, t3, s10, dyn
	-[0x800008e0]:csrrs a7, fcsr, zero
	-[0x800008e4]:sw t5, 416(ra)
	-[0x800008e8]:sw t6, 424(ra)
Current Store : [0x800008e8] : sw t6, 424(ra) -- Store: [0x80004c70]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe0d828b86622a and fs2 == 1 and fe2 == 0x7fa and fm2 == 0xf27d74799dd4f and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008dc]:fsub.d t5, t3, s10, dyn
	-[0x800008e0]:csrrs a7, fcsr, zero
	-[0x800008e4]:sw t5, 416(ra)
	-[0x800008e8]:sw t6, 424(ra)
	-[0x800008ec]:sw t5, 432(ra)
Current Store : [0x800008ec] : sw t5, 432(ra) -- Store: [0x80004c78]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb9876f8130c3 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9119e241fb3cd and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000092c]:fsub.d t5, t3, s10, dyn
	-[0x80000930]:csrrs a7, fcsr, zero
	-[0x80000934]:sw t5, 448(ra)
	-[0x80000938]:sw t6, 456(ra)
Current Store : [0x80000938] : sw t6, 456(ra) -- Store: [0x80004c90]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb9876f8130c3 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9119e241fb3cd and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000092c]:fsub.d t5, t3, s10, dyn
	-[0x80000930]:csrrs a7, fcsr, zero
	-[0x80000934]:sw t5, 448(ra)
	-[0x80000938]:sw t6, 456(ra)
	-[0x8000093c]:sw t5, 464(ra)
Current Store : [0x8000093c] : sw t5, 464(ra) -- Store: [0x80004c98]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb9876f8130c3 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9119e241fb3cd and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000097c]:fsub.d t5, t3, s10, dyn
	-[0x80000980]:csrrs a7, fcsr, zero
	-[0x80000984]:sw t5, 480(ra)
	-[0x80000988]:sw t6, 488(ra)
Current Store : [0x80000988] : sw t6, 488(ra) -- Store: [0x80004cb0]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb9876f8130c3 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9119e241fb3cd and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000097c]:fsub.d t5, t3, s10, dyn
	-[0x80000980]:csrrs a7, fcsr, zero
	-[0x80000984]:sw t5, 480(ra)
	-[0x80000988]:sw t6, 488(ra)
	-[0x8000098c]:sw t5, 496(ra)
Current Store : [0x8000098c] : sw t5, 496(ra) -- Store: [0x80004cb8]:0xFFFFFFFD




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb9876f8130c3 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9119e241fb3cd and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009cc]:fsub.d t5, t3, s10, dyn
	-[0x800009d0]:csrrs a7, fcsr, zero
	-[0x800009d4]:sw t5, 512(ra)
	-[0x800009d8]:sw t6, 520(ra)
Current Store : [0x800009d8] : sw t6, 520(ra) -- Store: [0x80004cd0]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb9876f8130c3 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9119e241fb3cd and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009cc]:fsub.d t5, t3, s10, dyn
	-[0x800009d0]:csrrs a7, fcsr, zero
	-[0x800009d4]:sw t5, 512(ra)
	-[0x800009d8]:sw t6, 520(ra)
	-[0x800009dc]:sw t5, 528(ra)
Current Store : [0x800009dc] : sw t5, 528(ra) -- Store: [0x80004cd8]:0xFFFFFFFD




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb9876f8130c3 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9119e241fb3cd and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a1c]:fsub.d t5, t3, s10, dyn
	-[0x80000a20]:csrrs a7, fcsr, zero
	-[0x80000a24]:sw t5, 544(ra)
	-[0x80000a28]:sw t6, 552(ra)
Current Store : [0x80000a28] : sw t6, 552(ra) -- Store: [0x80004cf0]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb9876f8130c3 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9119e241fb3cd and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a1c]:fsub.d t5, t3, s10, dyn
	-[0x80000a20]:csrrs a7, fcsr, zero
	-[0x80000a24]:sw t5, 544(ra)
	-[0x80000a28]:sw t6, 552(ra)
	-[0x80000a2c]:sw t5, 560(ra)
Current Store : [0x80000a2c] : sw t5, 560(ra) -- Store: [0x80004cf8]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb9876f8130c3 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9119e241fb3cd and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a6c]:fsub.d t5, t3, s10, dyn
	-[0x80000a70]:csrrs a7, fcsr, zero
	-[0x80000a74]:sw t5, 576(ra)
	-[0x80000a78]:sw t6, 584(ra)
Current Store : [0x80000a78] : sw t6, 584(ra) -- Store: [0x80004d10]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb9876f8130c3 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9119e241fb3cd and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a6c]:fsub.d t5, t3, s10, dyn
	-[0x80000a70]:csrrs a7, fcsr, zero
	-[0x80000a74]:sw t5, 576(ra)
	-[0x80000a78]:sw t6, 584(ra)
	-[0x80000a7c]:sw t5, 592(ra)
Current Store : [0x80000a7c] : sw t5, 592(ra) -- Store: [0x80004d18]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xdbcde43895c3f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf12190de3b51d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000abc]:fsub.d t5, t3, s10, dyn
	-[0x80000ac0]:csrrs a7, fcsr, zero
	-[0x80000ac4]:sw t5, 608(ra)
	-[0x80000ac8]:sw t6, 616(ra)
Current Store : [0x80000ac8] : sw t6, 616(ra) -- Store: [0x80004d30]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xdbcde43895c3f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf12190de3b51d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000abc]:fsub.d t5, t3, s10, dyn
	-[0x80000ac0]:csrrs a7, fcsr, zero
	-[0x80000ac4]:sw t5, 608(ra)
	-[0x80000ac8]:sw t6, 616(ra)
	-[0x80000acc]:sw t5, 624(ra)
Current Store : [0x80000acc] : sw t5, 624(ra) -- Store: [0x80004d38]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xdbcde43895c3f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf12190de3b51d and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b0c]:fsub.d t5, t3, s10, dyn
	-[0x80000b10]:csrrs a7, fcsr, zero
	-[0x80000b14]:sw t5, 640(ra)
	-[0x80000b18]:sw t6, 648(ra)
Current Store : [0x80000b18] : sw t6, 648(ra) -- Store: [0x80004d50]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xdbcde43895c3f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf12190de3b51d and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b0c]:fsub.d t5, t3, s10, dyn
	-[0x80000b10]:csrrs a7, fcsr, zero
	-[0x80000b14]:sw t5, 640(ra)
	-[0x80000b18]:sw t6, 648(ra)
	-[0x80000b1c]:sw t5, 656(ra)
Current Store : [0x80000b1c] : sw t5, 656(ra) -- Store: [0x80004d58]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xdbcde43895c3f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf12190de3b51d and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b5c]:fsub.d t5, t3, s10, dyn
	-[0x80000b60]:csrrs a7, fcsr, zero
	-[0x80000b64]:sw t5, 672(ra)
	-[0x80000b68]:sw t6, 680(ra)
Current Store : [0x80000b68] : sw t6, 680(ra) -- Store: [0x80004d70]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xdbcde43895c3f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf12190de3b51d and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b5c]:fsub.d t5, t3, s10, dyn
	-[0x80000b60]:csrrs a7, fcsr, zero
	-[0x80000b64]:sw t5, 672(ra)
	-[0x80000b68]:sw t6, 680(ra)
	-[0x80000b6c]:sw t5, 688(ra)
Current Store : [0x80000b6c] : sw t5, 688(ra) -- Store: [0x80004d78]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xdbcde43895c3f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf12190de3b51d and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bac]:fsub.d t5, t3, s10, dyn
	-[0x80000bb0]:csrrs a7, fcsr, zero
	-[0x80000bb4]:sw t5, 704(ra)
	-[0x80000bb8]:sw t6, 712(ra)
Current Store : [0x80000bb8] : sw t6, 712(ra) -- Store: [0x80004d90]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xdbcde43895c3f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf12190de3b51d and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bac]:fsub.d t5, t3, s10, dyn
	-[0x80000bb0]:csrrs a7, fcsr, zero
	-[0x80000bb4]:sw t5, 704(ra)
	-[0x80000bb8]:sw t6, 712(ra)
	-[0x80000bbc]:sw t5, 720(ra)
Current Store : [0x80000bbc] : sw t5, 720(ra) -- Store: [0x80004d98]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xdbcde43895c3f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf12190de3b51d and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bfc]:fsub.d t5, t3, s10, dyn
	-[0x80000c00]:csrrs a7, fcsr, zero
	-[0x80000c04]:sw t5, 736(ra)
	-[0x80000c08]:sw t6, 744(ra)
Current Store : [0x80000c08] : sw t6, 744(ra) -- Store: [0x80004db0]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xdbcde43895c3f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf12190de3b51d and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bfc]:fsub.d t5, t3, s10, dyn
	-[0x80000c00]:csrrs a7, fcsr, zero
	-[0x80000c04]:sw t5, 736(ra)
	-[0x80000c08]:sw t6, 744(ra)
	-[0x80000c0c]:sw t5, 752(ra)
Current Store : [0x80000c0c] : sw t5, 752(ra) -- Store: [0x80004db8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc0659af8369fd and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x1fcd3283e4aff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c4c]:fsub.d t5, t3, s10, dyn
	-[0x80000c50]:csrrs a7, fcsr, zero
	-[0x80000c54]:sw t5, 768(ra)
	-[0x80000c58]:sw t6, 776(ra)
Current Store : [0x80000c58] : sw t6, 776(ra) -- Store: [0x80004dd0]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc0659af8369fd and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x1fcd3283e4aff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c4c]:fsub.d t5, t3, s10, dyn
	-[0x80000c50]:csrrs a7, fcsr, zero
	-[0x80000c54]:sw t5, 768(ra)
	-[0x80000c58]:sw t6, 776(ra)
	-[0x80000c5c]:sw t5, 784(ra)
Current Store : [0x80000c5c] : sw t5, 784(ra) -- Store: [0x80004dd8]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc0659af8369fd and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x1fcd3283e4aff and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c9c]:fsub.d t5, t3, s10, dyn
	-[0x80000ca0]:csrrs a7, fcsr, zero
	-[0x80000ca4]:sw t5, 800(ra)
	-[0x80000ca8]:sw t6, 808(ra)
Current Store : [0x80000ca8] : sw t6, 808(ra) -- Store: [0x80004df0]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc0659af8369fd and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x1fcd3283e4aff and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c9c]:fsub.d t5, t3, s10, dyn
	-[0x80000ca0]:csrrs a7, fcsr, zero
	-[0x80000ca4]:sw t5, 800(ra)
	-[0x80000ca8]:sw t6, 808(ra)
	-[0x80000cac]:sw t5, 816(ra)
Current Store : [0x80000cac] : sw t5, 816(ra) -- Store: [0x80004df8]:0xFFFFFFFD




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc0659af8369fd and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x1fcd3283e4aff and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cec]:fsub.d t5, t3, s10, dyn
	-[0x80000cf0]:csrrs a7, fcsr, zero
	-[0x80000cf4]:sw t5, 832(ra)
	-[0x80000cf8]:sw t6, 840(ra)
Current Store : [0x80000cf8] : sw t6, 840(ra) -- Store: [0x80004e10]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc0659af8369fd and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x1fcd3283e4aff and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cec]:fsub.d t5, t3, s10, dyn
	-[0x80000cf0]:csrrs a7, fcsr, zero
	-[0x80000cf4]:sw t5, 832(ra)
	-[0x80000cf8]:sw t6, 840(ra)
	-[0x80000cfc]:sw t5, 848(ra)
Current Store : [0x80000cfc] : sw t5, 848(ra) -- Store: [0x80004e18]:0xFFFFFFFD




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc0659af8369fd and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x1fcd3283e4aff and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d3c]:fsub.d t5, t3, s10, dyn
	-[0x80000d40]:csrrs a7, fcsr, zero
	-[0x80000d44]:sw t5, 864(ra)
	-[0x80000d48]:sw t6, 872(ra)
Current Store : [0x80000d48] : sw t6, 872(ra) -- Store: [0x80004e30]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc0659af8369fd and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x1fcd3283e4aff and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d3c]:fsub.d t5, t3, s10, dyn
	-[0x80000d40]:csrrs a7, fcsr, zero
	-[0x80000d44]:sw t5, 864(ra)
	-[0x80000d48]:sw t6, 872(ra)
	-[0x80000d4c]:sw t5, 880(ra)
Current Store : [0x80000d4c] : sw t5, 880(ra) -- Store: [0x80004e38]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc0659af8369fd and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x1fcd3283e4aff and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d8c]:fsub.d t5, t3, s10, dyn
	-[0x80000d90]:csrrs a7, fcsr, zero
	-[0x80000d94]:sw t5, 896(ra)
	-[0x80000d98]:sw t6, 904(ra)
Current Store : [0x80000d98] : sw t6, 904(ra) -- Store: [0x80004e50]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc0659af8369fd and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x1fcd3283e4aff and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d8c]:fsub.d t5, t3, s10, dyn
	-[0x80000d90]:csrrs a7, fcsr, zero
	-[0x80000d94]:sw t5, 896(ra)
	-[0x80000d98]:sw t6, 904(ra)
	-[0x80000d9c]:sw t5, 912(ra)
Current Store : [0x80000d9c] : sw t5, 912(ra) -- Store: [0x80004e58]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdd5b61587fd27 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x88a927a9e00b5 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ddc]:fsub.d t5, t3, s10, dyn
	-[0x80000de0]:csrrs a7, fcsr, zero
	-[0x80000de4]:sw t5, 928(ra)
	-[0x80000de8]:sw t6, 936(ra)
Current Store : [0x80000de8] : sw t6, 936(ra) -- Store: [0x80004e70]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdd5b61587fd27 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x88a927a9e00b5 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ddc]:fsub.d t5, t3, s10, dyn
	-[0x80000de0]:csrrs a7, fcsr, zero
	-[0x80000de4]:sw t5, 928(ra)
	-[0x80000de8]:sw t6, 936(ra)
	-[0x80000dec]:sw t5, 944(ra)
Current Store : [0x80000dec] : sw t5, 944(ra) -- Store: [0x80004e78]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdd5b61587fd27 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x88a927a9e00b5 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e2c]:fsub.d t5, t3, s10, dyn
	-[0x80000e30]:csrrs a7, fcsr, zero
	-[0x80000e34]:sw t5, 960(ra)
	-[0x80000e38]:sw t6, 968(ra)
Current Store : [0x80000e38] : sw t6, 968(ra) -- Store: [0x80004e90]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdd5b61587fd27 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x88a927a9e00b5 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e2c]:fsub.d t5, t3, s10, dyn
	-[0x80000e30]:csrrs a7, fcsr, zero
	-[0x80000e34]:sw t5, 960(ra)
	-[0x80000e38]:sw t6, 968(ra)
	-[0x80000e3c]:sw t5, 976(ra)
Current Store : [0x80000e3c] : sw t5, 976(ra) -- Store: [0x80004e98]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdd5b61587fd27 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x88a927a9e00b5 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e7c]:fsub.d t5, t3, s10, dyn
	-[0x80000e80]:csrrs a7, fcsr, zero
	-[0x80000e84]:sw t5, 992(ra)
	-[0x80000e88]:sw t6, 1000(ra)
Current Store : [0x80000e88] : sw t6, 1000(ra) -- Store: [0x80004eb0]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdd5b61587fd27 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x88a927a9e00b5 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e7c]:fsub.d t5, t3, s10, dyn
	-[0x80000e80]:csrrs a7, fcsr, zero
	-[0x80000e84]:sw t5, 992(ra)
	-[0x80000e88]:sw t6, 1000(ra)
	-[0x80000e8c]:sw t5, 1008(ra)
Current Store : [0x80000e8c] : sw t5, 1008(ra) -- Store: [0x80004eb8]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdd5b61587fd27 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x88a927a9e00b5 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ecc]:fsub.d t5, t3, s10, dyn
	-[0x80000ed0]:csrrs a7, fcsr, zero
	-[0x80000ed4]:sw t5, 1024(ra)
	-[0x80000ed8]:sw t6, 1032(ra)
Current Store : [0x80000ed8] : sw t6, 1032(ra) -- Store: [0x80004ed0]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdd5b61587fd27 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x88a927a9e00b5 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ecc]:fsub.d t5, t3, s10, dyn
	-[0x80000ed0]:csrrs a7, fcsr, zero
	-[0x80000ed4]:sw t5, 1024(ra)
	-[0x80000ed8]:sw t6, 1032(ra)
	-[0x80000edc]:sw t5, 1040(ra)
Current Store : [0x80000edc] : sw t5, 1040(ra) -- Store: [0x80004ed8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdd5b61587fd27 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x88a927a9e00b5 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f1c]:fsub.d t5, t3, s10, dyn
	-[0x80000f20]:csrrs a7, fcsr, zero
	-[0x80000f24]:sw t5, 1056(ra)
	-[0x80000f28]:sw t6, 1064(ra)
Current Store : [0x80000f28] : sw t6, 1064(ra) -- Store: [0x80004ef0]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdd5b61587fd27 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x88a927a9e00b5 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f1c]:fsub.d t5, t3, s10, dyn
	-[0x80000f20]:csrrs a7, fcsr, zero
	-[0x80000f24]:sw t5, 1056(ra)
	-[0x80000f28]:sw t6, 1064(ra)
	-[0x80000f2c]:sw t5, 1072(ra)
Current Store : [0x80000f2c] : sw t5, 1072(ra) -- Store: [0x80004ef8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc01045c2cd787 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8ffbee8f4ca1c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f6c]:fsub.d t5, t3, s10, dyn
	-[0x80000f70]:csrrs a7, fcsr, zero
	-[0x80000f74]:sw t5, 1088(ra)
	-[0x80000f78]:sw t6, 1096(ra)
Current Store : [0x80000f78] : sw t6, 1096(ra) -- Store: [0x80004f10]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc01045c2cd787 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8ffbee8f4ca1c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f6c]:fsub.d t5, t3, s10, dyn
	-[0x80000f70]:csrrs a7, fcsr, zero
	-[0x80000f74]:sw t5, 1088(ra)
	-[0x80000f78]:sw t6, 1096(ra)
	-[0x80000f7c]:sw t5, 1104(ra)
Current Store : [0x80000f7c] : sw t5, 1104(ra) -- Store: [0x80004f18]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc01045c2cd787 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8ffbee8f4ca1c and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fbc]:fsub.d t5, t3, s10, dyn
	-[0x80000fc0]:csrrs a7, fcsr, zero
	-[0x80000fc4]:sw t5, 1120(ra)
	-[0x80000fc8]:sw t6, 1128(ra)
Current Store : [0x80000fc8] : sw t6, 1128(ra) -- Store: [0x80004f30]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc01045c2cd787 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8ffbee8f4ca1c and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fbc]:fsub.d t5, t3, s10, dyn
	-[0x80000fc0]:csrrs a7, fcsr, zero
	-[0x80000fc4]:sw t5, 1120(ra)
	-[0x80000fc8]:sw t6, 1128(ra)
	-[0x80000fcc]:sw t5, 1136(ra)
Current Store : [0x80000fcc] : sw t5, 1136(ra) -- Store: [0x80004f38]:0xFFFFFFFD




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc01045c2cd787 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8ffbee8f4ca1c and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000100c]:fsub.d t5, t3, s10, dyn
	-[0x80001010]:csrrs a7, fcsr, zero
	-[0x80001014]:sw t5, 1152(ra)
	-[0x80001018]:sw t6, 1160(ra)
Current Store : [0x80001018] : sw t6, 1160(ra) -- Store: [0x80004f50]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc01045c2cd787 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8ffbee8f4ca1c and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000100c]:fsub.d t5, t3, s10, dyn
	-[0x80001010]:csrrs a7, fcsr, zero
	-[0x80001014]:sw t5, 1152(ra)
	-[0x80001018]:sw t6, 1160(ra)
	-[0x8000101c]:sw t5, 1168(ra)
Current Store : [0x8000101c] : sw t5, 1168(ra) -- Store: [0x80004f58]:0xFFFFFFFD




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc01045c2cd787 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8ffbee8f4ca1c and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000105c]:fsub.d t5, t3, s10, dyn
	-[0x80001060]:csrrs a7, fcsr, zero
	-[0x80001064]:sw t5, 1184(ra)
	-[0x80001068]:sw t6, 1192(ra)
Current Store : [0x80001068] : sw t6, 1192(ra) -- Store: [0x80004f70]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc01045c2cd787 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8ffbee8f4ca1c and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000105c]:fsub.d t5, t3, s10, dyn
	-[0x80001060]:csrrs a7, fcsr, zero
	-[0x80001064]:sw t5, 1184(ra)
	-[0x80001068]:sw t6, 1192(ra)
	-[0x8000106c]:sw t5, 1200(ra)
Current Store : [0x8000106c] : sw t5, 1200(ra) -- Store: [0x80004f78]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc01045c2cd787 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8ffbee8f4ca1c and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010ac]:fsub.d t5, t3, s10, dyn
	-[0x800010b0]:csrrs a7, fcsr, zero
	-[0x800010b4]:sw t5, 1216(ra)
	-[0x800010b8]:sw t6, 1224(ra)
Current Store : [0x800010b8] : sw t6, 1224(ra) -- Store: [0x80004f90]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc01045c2cd787 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8ffbee8f4ca1c and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010ac]:fsub.d t5, t3, s10, dyn
	-[0x800010b0]:csrrs a7, fcsr, zero
	-[0x800010b4]:sw t5, 1216(ra)
	-[0x800010b8]:sw t6, 1224(ra)
	-[0x800010bc]:sw t5, 1232(ra)
Current Store : [0x800010bc] : sw t5, 1232(ra) -- Store: [0x80004f98]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x28bc82f697c4d and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x6ba1be84b41d8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010fc]:fsub.d t5, t3, s10, dyn
	-[0x80001100]:csrrs a7, fcsr, zero
	-[0x80001104]:sw t5, 1248(ra)
	-[0x80001108]:sw t6, 1256(ra)
Current Store : [0x80001108] : sw t6, 1256(ra) -- Store: [0x80004fb0]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x28bc82f697c4d and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x6ba1be84b41d8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010fc]:fsub.d t5, t3, s10, dyn
	-[0x80001100]:csrrs a7, fcsr, zero
	-[0x80001104]:sw t5, 1248(ra)
	-[0x80001108]:sw t6, 1256(ra)
	-[0x8000110c]:sw t5, 1264(ra)
Current Store : [0x8000110c] : sw t5, 1264(ra) -- Store: [0x80004fb8]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x28bc82f697c4d and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x6ba1be84b41d8 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000114c]:fsub.d t5, t3, s10, dyn
	-[0x80001150]:csrrs a7, fcsr, zero
	-[0x80001154]:sw t5, 1280(ra)
	-[0x80001158]:sw t6, 1288(ra)
Current Store : [0x80001158] : sw t6, 1288(ra) -- Store: [0x80004fd0]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x28bc82f697c4d and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x6ba1be84b41d8 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000114c]:fsub.d t5, t3, s10, dyn
	-[0x80001150]:csrrs a7, fcsr, zero
	-[0x80001154]:sw t5, 1280(ra)
	-[0x80001158]:sw t6, 1288(ra)
	-[0x8000115c]:sw t5, 1296(ra)
Current Store : [0x8000115c] : sw t5, 1296(ra) -- Store: [0x80004fd8]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x28bc82f697c4d and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x6ba1be84b41d8 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000119c]:fsub.d t5, t3, s10, dyn
	-[0x800011a0]:csrrs a7, fcsr, zero
	-[0x800011a4]:sw t5, 1312(ra)
	-[0x800011a8]:sw t6, 1320(ra)
Current Store : [0x800011a8] : sw t6, 1320(ra) -- Store: [0x80004ff0]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x28bc82f697c4d and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x6ba1be84b41d8 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000119c]:fsub.d t5, t3, s10, dyn
	-[0x800011a0]:csrrs a7, fcsr, zero
	-[0x800011a4]:sw t5, 1312(ra)
	-[0x800011a8]:sw t6, 1320(ra)
	-[0x800011ac]:sw t5, 1328(ra)
Current Store : [0x800011ac] : sw t5, 1328(ra) -- Store: [0x80004ff8]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x28bc82f697c4d and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x6ba1be84b41d8 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011ec]:fsub.d t5, t3, s10, dyn
	-[0x800011f0]:csrrs a7, fcsr, zero
	-[0x800011f4]:sw t5, 1344(ra)
	-[0x800011f8]:sw t6, 1352(ra)
Current Store : [0x800011f8] : sw t6, 1352(ra) -- Store: [0x80005010]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x28bc82f697c4d and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x6ba1be84b41d8 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011ec]:fsub.d t5, t3, s10, dyn
	-[0x800011f0]:csrrs a7, fcsr, zero
	-[0x800011f4]:sw t5, 1344(ra)
	-[0x800011f8]:sw t6, 1352(ra)
	-[0x800011fc]:sw t5, 1360(ra)
Current Store : [0x800011fc] : sw t5, 1360(ra) -- Store: [0x80005018]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x28bc82f697c4d and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x6ba1be84b41d8 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000123c]:fsub.d t5, t3, s10, dyn
	-[0x80001240]:csrrs a7, fcsr, zero
	-[0x80001244]:sw t5, 1376(ra)
	-[0x80001248]:sw t6, 1384(ra)
Current Store : [0x80001248] : sw t6, 1384(ra) -- Store: [0x80005030]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x28bc82f697c4d and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x6ba1be84b41d8 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000123c]:fsub.d t5, t3, s10, dyn
	-[0x80001240]:csrrs a7, fcsr, zero
	-[0x80001244]:sw t5, 1376(ra)
	-[0x80001248]:sw t6, 1384(ra)
	-[0x8000124c]:sw t5, 1392(ra)
Current Store : [0x8000124c] : sw t5, 1392(ra) -- Store: [0x80005038]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xacd7053aa42a2 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x4ca3eb156f570 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000128c]:fsub.d t5, t3, s10, dyn
	-[0x80001290]:csrrs a7, fcsr, zero
	-[0x80001294]:sw t5, 1408(ra)
	-[0x80001298]:sw t6, 1416(ra)
Current Store : [0x80001298] : sw t6, 1416(ra) -- Store: [0x80005050]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xacd7053aa42a2 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x4ca3eb156f570 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000128c]:fsub.d t5, t3, s10, dyn
	-[0x80001290]:csrrs a7, fcsr, zero
	-[0x80001294]:sw t5, 1408(ra)
	-[0x80001298]:sw t6, 1416(ra)
	-[0x8000129c]:sw t5, 1424(ra)
Current Store : [0x8000129c] : sw t5, 1424(ra) -- Store: [0x80005058]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xacd7053aa42a2 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x4ca3eb156f570 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012dc]:fsub.d t5, t3, s10, dyn
	-[0x800012e0]:csrrs a7, fcsr, zero
	-[0x800012e4]:sw t5, 1440(ra)
	-[0x800012e8]:sw t6, 1448(ra)
Current Store : [0x800012e8] : sw t6, 1448(ra) -- Store: [0x80005070]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xacd7053aa42a2 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x4ca3eb156f570 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012dc]:fsub.d t5, t3, s10, dyn
	-[0x800012e0]:csrrs a7, fcsr, zero
	-[0x800012e4]:sw t5, 1440(ra)
	-[0x800012e8]:sw t6, 1448(ra)
	-[0x800012ec]:sw t5, 1456(ra)
Current Store : [0x800012ec] : sw t5, 1456(ra) -- Store: [0x80005078]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xacd7053aa42a2 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x4ca3eb156f570 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000132c]:fsub.d t5, t3, s10, dyn
	-[0x80001330]:csrrs a7, fcsr, zero
	-[0x80001334]:sw t5, 1472(ra)
	-[0x80001338]:sw t6, 1480(ra)
Current Store : [0x80001338] : sw t6, 1480(ra) -- Store: [0x80005090]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xacd7053aa42a2 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x4ca3eb156f570 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000132c]:fsub.d t5, t3, s10, dyn
	-[0x80001330]:csrrs a7, fcsr, zero
	-[0x80001334]:sw t5, 1472(ra)
	-[0x80001338]:sw t6, 1480(ra)
	-[0x8000133c]:sw t5, 1488(ra)
Current Store : [0x8000133c] : sw t5, 1488(ra) -- Store: [0x80005098]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xacd7053aa42a2 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x4ca3eb156f570 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000137c]:fsub.d t5, t3, s10, dyn
	-[0x80001380]:csrrs a7, fcsr, zero
	-[0x80001384]:sw t5, 1504(ra)
	-[0x80001388]:sw t6, 1512(ra)
Current Store : [0x80001388] : sw t6, 1512(ra) -- Store: [0x800050b0]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xacd7053aa42a2 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x4ca3eb156f570 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000137c]:fsub.d t5, t3, s10, dyn
	-[0x80001380]:csrrs a7, fcsr, zero
	-[0x80001384]:sw t5, 1504(ra)
	-[0x80001388]:sw t6, 1512(ra)
	-[0x8000138c]:sw t5, 1520(ra)
Current Store : [0x8000138c] : sw t5, 1520(ra) -- Store: [0x800050b8]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xacd7053aa42a2 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x4ca3eb156f570 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013cc]:fsub.d t5, t3, s10, dyn
	-[0x800013d0]:csrrs a7, fcsr, zero
	-[0x800013d4]:sw t5, 1536(ra)
	-[0x800013d8]:sw t6, 1544(ra)
Current Store : [0x800013d8] : sw t6, 1544(ra) -- Store: [0x800050d0]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xacd7053aa42a2 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x4ca3eb156f570 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013cc]:fsub.d t5, t3, s10, dyn
	-[0x800013d0]:csrrs a7, fcsr, zero
	-[0x800013d4]:sw t5, 1536(ra)
	-[0x800013d8]:sw t6, 1544(ra)
	-[0x800013dc]:sw t5, 1552(ra)
Current Store : [0x800013dc] : sw t5, 1552(ra) -- Store: [0x800050d8]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x48dace8666677 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x6e4a62f333310 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000141c]:fsub.d t5, t3, s10, dyn
	-[0x80001420]:csrrs a7, fcsr, zero
	-[0x80001424]:sw t5, 1568(ra)
	-[0x80001428]:sw t6, 1576(ra)
Current Store : [0x80001428] : sw t6, 1576(ra) -- Store: [0x800050f0]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x48dace8666677 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x6e4a62f333310 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000141c]:fsub.d t5, t3, s10, dyn
	-[0x80001420]:csrrs a7, fcsr, zero
	-[0x80001424]:sw t5, 1568(ra)
	-[0x80001428]:sw t6, 1576(ra)
	-[0x8000142c]:sw t5, 1584(ra)
Current Store : [0x8000142c] : sw t5, 1584(ra) -- Store: [0x800050f8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x48dace8666677 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x6e4a62f333310 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000146c]:fsub.d t5, t3, s10, dyn
	-[0x80001470]:csrrs a7, fcsr, zero
	-[0x80001474]:sw t5, 1600(ra)
	-[0x80001478]:sw t6, 1608(ra)
Current Store : [0x80001478] : sw t6, 1608(ra) -- Store: [0x80005110]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x48dace8666677 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x6e4a62f333310 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000146c]:fsub.d t5, t3, s10, dyn
	-[0x80001470]:csrrs a7, fcsr, zero
	-[0x80001474]:sw t5, 1600(ra)
	-[0x80001478]:sw t6, 1608(ra)
	-[0x8000147c]:sw t5, 1616(ra)
Current Store : [0x8000147c] : sw t5, 1616(ra) -- Store: [0x80005118]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x48dace8666677 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x6e4a62f333310 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014bc]:fsub.d t5, t3, s10, dyn
	-[0x800014c0]:csrrs a7, fcsr, zero
	-[0x800014c4]:sw t5, 1632(ra)
	-[0x800014c8]:sw t6, 1640(ra)
Current Store : [0x800014c8] : sw t6, 1640(ra) -- Store: [0x80005130]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x48dace8666677 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x6e4a62f333310 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014bc]:fsub.d t5, t3, s10, dyn
	-[0x800014c0]:csrrs a7, fcsr, zero
	-[0x800014c4]:sw t5, 1632(ra)
	-[0x800014c8]:sw t6, 1640(ra)
	-[0x800014cc]:sw t5, 1648(ra)
Current Store : [0x800014cc] : sw t5, 1648(ra) -- Store: [0x80005138]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x48dace8666677 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x6e4a62f333310 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000150c]:fsub.d t5, t3, s10, dyn
	-[0x80001510]:csrrs a7, fcsr, zero
	-[0x80001514]:sw t5, 1664(ra)
	-[0x80001518]:sw t6, 1672(ra)
Current Store : [0x80001518] : sw t6, 1672(ra) -- Store: [0x80005150]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x48dace8666677 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x6e4a62f333310 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000150c]:fsub.d t5, t3, s10, dyn
	-[0x80001510]:csrrs a7, fcsr, zero
	-[0x80001514]:sw t5, 1664(ra)
	-[0x80001518]:sw t6, 1672(ra)
	-[0x8000151c]:sw t5, 1680(ra)
Current Store : [0x8000151c] : sw t5, 1680(ra) -- Store: [0x80005158]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x48dace8666677 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x6e4a62f333310 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000155c]:fsub.d t5, t3, s10, dyn
	-[0x80001560]:csrrs a7, fcsr, zero
	-[0x80001564]:sw t5, 1696(ra)
	-[0x80001568]:sw t6, 1704(ra)
Current Store : [0x80001568] : sw t6, 1704(ra) -- Store: [0x80005170]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x48dace8666677 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x6e4a62f333310 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000155c]:fsub.d t5, t3, s10, dyn
	-[0x80001560]:csrrs a7, fcsr, zero
	-[0x80001564]:sw t5, 1696(ra)
	-[0x80001568]:sw t6, 1704(ra)
	-[0x8000156c]:sw t5, 1712(ra)
Current Store : [0x8000156c] : sw t5, 1712(ra) -- Store: [0x80005178]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfc2ea66e5019e and fs2 == 1 and fe2 == 0x7f7 and fm2 == 0xe8acc8d7f2ffb and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015ac]:fsub.d t5, t3, s10, dyn
	-[0x800015b0]:csrrs a7, fcsr, zero
	-[0x800015b4]:sw t5, 1728(ra)
	-[0x800015b8]:sw t6, 1736(ra)
Current Store : [0x800015b8] : sw t6, 1736(ra) -- Store: [0x80005190]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfc2ea66e5019e and fs2 == 1 and fe2 == 0x7f7 and fm2 == 0xe8acc8d7f2ffb and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015ac]:fsub.d t5, t3, s10, dyn
	-[0x800015b0]:csrrs a7, fcsr, zero
	-[0x800015b4]:sw t5, 1728(ra)
	-[0x800015b8]:sw t6, 1736(ra)
	-[0x800015bc]:sw t5, 1744(ra)
Current Store : [0x800015bc] : sw t5, 1744(ra) -- Store: [0x80005198]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfc2ea66e5019e and fs2 == 1 and fe2 == 0x7f7 and fm2 == 0xe8acc8d7f2ffb and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015fc]:fsub.d t5, t3, s10, dyn
	-[0x80001600]:csrrs a7, fcsr, zero
	-[0x80001604]:sw t5, 1760(ra)
	-[0x80001608]:sw t6, 1768(ra)
Current Store : [0x80001608] : sw t6, 1768(ra) -- Store: [0x800051b0]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfc2ea66e5019e and fs2 == 1 and fe2 == 0x7f7 and fm2 == 0xe8acc8d7f2ffb and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015fc]:fsub.d t5, t3, s10, dyn
	-[0x80001600]:csrrs a7, fcsr, zero
	-[0x80001604]:sw t5, 1760(ra)
	-[0x80001608]:sw t6, 1768(ra)
	-[0x8000160c]:sw t5, 1776(ra)
Current Store : [0x8000160c] : sw t5, 1776(ra) -- Store: [0x800051b8]:0xFFFFFFFD




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfc2ea66e5019e and fs2 == 1 and fe2 == 0x7f7 and fm2 == 0xe8acc8d7f2ffb and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000164c]:fsub.d t5, t3, s10, dyn
	-[0x80001650]:csrrs a7, fcsr, zero
	-[0x80001654]:sw t5, 1792(ra)
	-[0x80001658]:sw t6, 1800(ra)
Current Store : [0x80001658] : sw t6, 1800(ra) -- Store: [0x800051d0]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfc2ea66e5019e and fs2 == 1 and fe2 == 0x7f7 and fm2 == 0xe8acc8d7f2ffb and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000164c]:fsub.d t5, t3, s10, dyn
	-[0x80001650]:csrrs a7, fcsr, zero
	-[0x80001654]:sw t5, 1792(ra)
	-[0x80001658]:sw t6, 1800(ra)
	-[0x8000165c]:sw t5, 1808(ra)
Current Store : [0x8000165c] : sw t5, 1808(ra) -- Store: [0x800051d8]:0xFFFFFFFD




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfc2ea66e5019e and fs2 == 1 and fe2 == 0x7f7 and fm2 == 0xe8acc8d7f2ffb and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000169c]:fsub.d t5, t3, s10, dyn
	-[0x800016a0]:csrrs a7, fcsr, zero
	-[0x800016a4]:sw t5, 1824(ra)
	-[0x800016a8]:sw t6, 1832(ra)
Current Store : [0x800016a8] : sw t6, 1832(ra) -- Store: [0x800051f0]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfc2ea66e5019e and fs2 == 1 and fe2 == 0x7f7 and fm2 == 0xe8acc8d7f2ffb and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000169c]:fsub.d t5, t3, s10, dyn
	-[0x800016a0]:csrrs a7, fcsr, zero
	-[0x800016a4]:sw t5, 1824(ra)
	-[0x800016a8]:sw t6, 1832(ra)
	-[0x800016ac]:sw t5, 1840(ra)
Current Store : [0x800016ac] : sw t5, 1840(ra) -- Store: [0x800051f8]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfc2ea66e5019e and fs2 == 1 and fe2 == 0x7f7 and fm2 == 0xe8acc8d7f2ffb and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016ec]:fsub.d t5, t3, s10, dyn
	-[0x800016f0]:csrrs a7, fcsr, zero
	-[0x800016f4]:sw t5, 1856(ra)
	-[0x800016f8]:sw t6, 1864(ra)
Current Store : [0x800016f8] : sw t6, 1864(ra) -- Store: [0x80005210]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfc2ea66e5019e and fs2 == 1 and fe2 == 0x7f7 and fm2 == 0xe8acc8d7f2ffb and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016ec]:fsub.d t5, t3, s10, dyn
	-[0x800016f0]:csrrs a7, fcsr, zero
	-[0x800016f4]:sw t5, 1856(ra)
	-[0x800016f8]:sw t6, 1864(ra)
	-[0x800016fc]:sw t5, 1872(ra)
Current Store : [0x800016fc] : sw t5, 1872(ra) -- Store: [0x80005218]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xef2a4f7c7db7f and fs2 == 1 and fe2 == 0x7f8 and fm2 == 0xb9867d3787c37 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000173c]:fsub.d t5, t3, s10, dyn
	-[0x80001740]:csrrs a7, fcsr, zero
	-[0x80001744]:sw t5, 1888(ra)
	-[0x80001748]:sw t6, 1896(ra)
Current Store : [0x80001748] : sw t6, 1896(ra) -- Store: [0x80005230]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xef2a4f7c7db7f and fs2 == 1 and fe2 == 0x7f8 and fm2 == 0xb9867d3787c37 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000173c]:fsub.d t5, t3, s10, dyn
	-[0x80001740]:csrrs a7, fcsr, zero
	-[0x80001744]:sw t5, 1888(ra)
	-[0x80001748]:sw t6, 1896(ra)
	-[0x8000174c]:sw t5, 1904(ra)
Current Store : [0x8000174c] : sw t5, 1904(ra) -- Store: [0x80005238]:0xF91B7583




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xef2a4f7c7db7f and fs2 == 1 and fe2 == 0x7f8 and fm2 == 0xb9867d3787c37 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000178c]:fsub.d t5, t3, s10, dyn
	-[0x80001790]:csrrs a7, fcsr, zero
	-[0x80001794]:sw t5, 1920(ra)
	-[0x80001798]:sw t6, 1928(ra)
Current Store : [0x80001798] : sw t6, 1928(ra) -- Store: [0x80005250]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xef2a4f7c7db7f and fs2 == 1 and fe2 == 0x7f8 and fm2 == 0xb9867d3787c37 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000178c]:fsub.d t5, t3, s10, dyn
	-[0x80001790]:csrrs a7, fcsr, zero
	-[0x80001794]:sw t5, 1920(ra)
	-[0x80001798]:sw t6, 1928(ra)
	-[0x8000179c]:sw t5, 1936(ra)
Current Store : [0x8000179c] : sw t5, 1936(ra) -- Store: [0x80005258]:0xF91B7582




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xef2a4f7c7db7f and fs2 == 1 and fe2 == 0x7f8 and fm2 == 0xb9867d3787c37 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017dc]:fsub.d t5, t3, s10, dyn
	-[0x800017e0]:csrrs a7, fcsr, zero
	-[0x800017e4]:sw t5, 1952(ra)
	-[0x800017e8]:sw t6, 1960(ra)
Current Store : [0x800017e8] : sw t6, 1960(ra) -- Store: [0x80005270]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xef2a4f7c7db7f and fs2 == 1 and fe2 == 0x7f8 and fm2 == 0xb9867d3787c37 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017dc]:fsub.d t5, t3, s10, dyn
	-[0x800017e0]:csrrs a7, fcsr, zero
	-[0x800017e4]:sw t5, 1952(ra)
	-[0x800017e8]:sw t6, 1960(ra)
	-[0x800017ec]:sw t5, 1968(ra)
Current Store : [0x800017ec] : sw t5, 1968(ra) -- Store: [0x80005278]:0xF91B7582




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xef2a4f7c7db7f and fs2 == 1 and fe2 == 0x7f8 and fm2 == 0xb9867d3787c37 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000182c]:fsub.d t5, t3, s10, dyn
	-[0x80001830]:csrrs a7, fcsr, zero
	-[0x80001834]:sw t5, 1984(ra)
	-[0x80001838]:sw t6, 1992(ra)
Current Store : [0x80001838] : sw t6, 1992(ra) -- Store: [0x80005290]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xef2a4f7c7db7f and fs2 == 1 and fe2 == 0x7f8 and fm2 == 0xb9867d3787c37 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000182c]:fsub.d t5, t3, s10, dyn
	-[0x80001830]:csrrs a7, fcsr, zero
	-[0x80001834]:sw t5, 1984(ra)
	-[0x80001838]:sw t6, 1992(ra)
	-[0x8000183c]:sw t5, 2000(ra)
Current Store : [0x8000183c] : sw t5, 2000(ra) -- Store: [0x80005298]:0xF91B7583




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xef2a4f7c7db7f and fs2 == 1 and fe2 == 0x7f8 and fm2 == 0xb9867d3787c37 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000187c]:fsub.d t5, t3, s10, dyn
	-[0x80001880]:csrrs a7, fcsr, zero
	-[0x80001884]:sw t5, 2016(ra)
	-[0x80001888]:sw t6, 2024(ra)
Current Store : [0x80001888] : sw t6, 2024(ra) -- Store: [0x800052b0]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xef2a4f7c7db7f and fs2 == 1 and fe2 == 0x7f8 and fm2 == 0xb9867d3787c37 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000187c]:fsub.d t5, t3, s10, dyn
	-[0x80001880]:csrrs a7, fcsr, zero
	-[0x80001884]:sw t5, 2016(ra)
	-[0x80001888]:sw t6, 2024(ra)
	-[0x8000188c]:sw t5, 2032(ra)
Current Store : [0x8000188c] : sw t5, 2032(ra) -- Store: [0x800052b8]:0xF91B7583




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7166677e49c3c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x9cc9e3436c322 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018cc]:fsub.d t5, t3, s10, dyn
	-[0x800018d0]:csrrs a7, fcsr, zero
	-[0x800018d4]:addi ra, ra, 2040
	-[0x800018d8]:sw t5, 8(ra)
	-[0x800018dc]:sw t6, 16(ra)
Current Store : [0x800018dc] : sw t6, 16(ra) -- Store: [0x800052d0]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7166677e49c3c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x9cc9e3436c322 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018cc]:fsub.d t5, t3, s10, dyn
	-[0x800018d0]:csrrs a7, fcsr, zero
	-[0x800018d4]:addi ra, ra, 2040
	-[0x800018d8]:sw t5, 8(ra)
	-[0x800018dc]:sw t6, 16(ra)
	-[0x800018e0]:sw t5, 24(ra)
Current Store : [0x800018e0] : sw t5, 24(ra) -- Store: [0x800052d8]:0xE2913730




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7166677e49c3c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x9cc9e3436c322 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001920]:fsub.d t5, t3, s10, dyn
	-[0x80001924]:csrrs a7, fcsr, zero
	-[0x80001928]:sw t5, 40(ra)
	-[0x8000192c]:sw t6, 48(ra)
Current Store : [0x8000192c] : sw t6, 48(ra) -- Store: [0x800052f0]:0x7F9A0E7A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7166677e49c3c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x9cc9e3436c322 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001920]:fsub.d t5, t3, s10, dyn
	-[0x80001924]:csrrs a7, fcsr, zero
	-[0x80001928]:sw t5, 40(ra)
	-[0x8000192c]:sw t6, 48(ra)
	-[0x80001930]:sw t5, 56(ra)
Current Store : [0x80001930] : sw t5, 56(ra) -- Store: [0x800052f8]:0xE2913730





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                          signature                           |                                                                                                                        coverpoints                                                                                                                         |                                                                                                                    code                                                                                                                     |
|---:|--------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80004a18]<br>0x00000000<br> [0x80004a30]<br>0x00000000<br> |- mnemonic : fsub.d<br> - rs1 : x30<br> - rs2 : x30<br> - rd : x30<br> - rs1 == rs2 == rd<br>                                                                                                                                                               |[0x8000014c]:fsub.d t5, t5, t5, dyn<br> [0x80000150]:csrrs tp, fcsr, zero<br> [0x80000154]:sw t5, 0(ra)<br> [0x80000158]:sw t6, 8(ra)<br> [0x8000015c]:sw t5, 16(ra)<br> [0x80000160]:sw tp, 24(ra)<br>                                      |
|   2|[0x80004a38]<br>0xFFFFFFFE<br> [0x80004a50]<br>0x00000021<br> |- rs1 : x26<br> - rs2 : x24<br> - rd : x28<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc81394a2171e9 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x1bf635aef470a and  fcsr == 0x20 and rm_val == 7   #nosat<br> |[0x8000019c]:fsub.d t3, s10, s8, dyn<br> [0x800001a0]:csrrs tp, fcsr, zero<br> [0x800001a4]:sw t3, 32(ra)<br> [0x800001a8]:sw t4, 40(ra)<br> [0x800001ac]:sw t3, 48(ra)<br> [0x800001b0]:sw tp, 56(ra)<br>                                   |
|   3|[0x80004a58]<br>0xFFFFFFFE<br> [0x80004a70]<br>0x00000041<br> |- rs1 : x24<br> - rs2 : x28<br> - rd : x24<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc81394a2171e9 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x1bf635aef470a and  fcsr == 0x40 and rm_val == 7   #nosat<br>                        |[0x800001ec]:fsub.d s8, s8, t3, dyn<br> [0x800001f0]:csrrs tp, fcsr, zero<br> [0x800001f4]:sw s8, 64(ra)<br> [0x800001f8]:sw s9, 72(ra)<br> [0x800001fc]:sw s8, 80(ra)<br> [0x80000200]:sw tp, 88(ra)<br>                                    |
|   4|[0x80004a78]<br>0xFFFFFFFF<br> [0x80004a90]<br>0x00000061<br> |- rs1 : x28<br> - rs2 : x26<br> - rd : x26<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc81394a2171e9 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x1bf635aef470a and  fcsr == 0x60 and rm_val == 7   #nosat<br>                        |[0x8000023c]:fsub.d s10, t3, s10, dyn<br> [0x80000240]:csrrs tp, fcsr, zero<br> [0x80000244]:sw s10, 96(ra)<br> [0x80000248]:sw s11, 104(ra)<br> [0x8000024c]:sw s10, 112(ra)<br> [0x80000250]:sw tp, 120(ra)<br>                            |
|   5|[0x80004a98]<br>0x00000000<br> [0x80004ab0]<br>0x00000080<br> |- rs1 : x20<br> - rs2 : x20<br> - rd : x22<br> - rs1 == rs2 != rd<br>                                                                                                                                                                                       |[0x8000028c]:fsub.d s6, s4, s4, dyn<br> [0x80000290]:csrrs tp, fcsr, zero<br> [0x80000294]:sw s6, 128(ra)<br> [0x80000298]:sw s7, 136(ra)<br> [0x8000029c]:sw s6, 144(ra)<br> [0x800002a0]:sw tp, 152(ra)<br>                                |
|   6|[0x80004ab8]<br>0xFFFFFFFE<br> [0x80004ad0]<br>0x00000001<br> |- rs1 : x22<br> - rs2 : x18<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0xd481499755d4b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8adfad9a2a8ab and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x800002dc]:fsub.d s4, s6, s2, dyn<br> [0x800002e0]:csrrs tp, fcsr, zero<br> [0x800002e4]:sw s4, 160(ra)<br> [0x800002e8]:sw s5, 168(ra)<br> [0x800002ec]:sw s4, 176(ra)<br> [0x800002f0]:sw tp, 184(ra)<br>                                |
|   7|[0x80004ad8]<br>0xFFFFFFFD<br> [0x80004af0]<br>0x00000021<br> |- rs1 : x16<br> - rs2 : x22<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0xd481499755d4b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8adfad9a2a8ab and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                               |[0x8000032c]:fsub.d s2, a6, s6, dyn<br> [0x80000330]:csrrs tp, fcsr, zero<br> [0x80000334]:sw s2, 192(ra)<br> [0x80000338]:sw s3, 200(ra)<br> [0x8000033c]:sw s2, 208(ra)<br> [0x80000340]:sw tp, 216(ra)<br>                                |
|   8|[0x80004af8]<br>0xFFFFFFFD<br> [0x80004b10]<br>0x00000041<br> |- rs1 : x18<br> - rs2 : x14<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0xd481499755d4b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8adfad9a2a8ab and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                               |[0x8000037c]:fsub.d a6, s2, a4, dyn<br> [0x80000380]:csrrs tp, fcsr, zero<br> [0x80000384]:sw a6, 224(ra)<br> [0x80000388]:sw a7, 232(ra)<br> [0x8000038c]:sw a6, 240(ra)<br> [0x80000390]:sw tp, 248(ra)<br>                                |
|   9|[0x80004b18]<br>0xFFFFFFFE<br> [0x80004b30]<br>0x00000061<br> |- rs1 : x12<br> - rs2 : x16<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0xd481499755d4b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8adfad9a2a8ab and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                               |[0x800003cc]:fsub.d a4, a2, a6, dyn<br> [0x800003d0]:csrrs tp, fcsr, zero<br> [0x800003d4]:sw a4, 256(ra)<br> [0x800003d8]:sw a5, 264(ra)<br> [0x800003dc]:sw a4, 272(ra)<br> [0x800003e0]:sw tp, 280(ra)<br>                                |
|  10|[0x80004b38]<br>0xFFFFFFFE<br> [0x80004b50]<br>0x00000081<br> |- rs1 : x14<br> - rs2 : x10<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0xd481499755d4b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8adfad9a2a8ab and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                               |[0x80000424]:fsub.d a2, a4, a0, dyn<br> [0x80000428]:csrrs a7, fcsr, zero<br> [0x8000042c]:sw a2, 288(ra)<br> [0x80000430]:sw a3, 296(ra)<br> [0x80000434]:sw a2, 304(ra)<br> [0x80000438]:sw a7, 312(ra)<br>                                |
|  11|[0x80004b58]<br>0xFFFFFFFF<br> [0x80004b70]<br>0x00000000<br> |- rs1 : x8<br> - rs2 : x12<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd87e65450c45 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x93c0cd5d79dd0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                 |[0x80000474]:fsub.d a0, fp, a2, dyn<br> [0x80000478]:csrrs a7, fcsr, zero<br> [0x8000047c]:sw a0, 320(ra)<br> [0x80000480]:sw a1, 328(ra)<br> [0x80000484]:sw a0, 336(ra)<br> [0x80000488]:sw a7, 344(ra)<br>                                |
|  12|[0x80004ac8]<br>0xFFFFFFFF<br> [0x80004ae0]<br>0x00000020<br> |- rs1 : x10<br> - rs2 : x6<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd87e65450c45 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x93c0cd5d79dd0 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                 |[0x800004cc]:fsub.d fp, a0, t1, dyn<br> [0x800004d0]:csrrs a7, fcsr, zero<br> [0x800004d4]:sw fp, 0(ra)<br> [0x800004d8]:sw s1, 8(ra)<br> [0x800004dc]:sw fp, 16(ra)<br> [0x800004e0]:sw a7, 24(ra)<br>                                      |
|  13|[0x80004ae8]<br>0xFFFFFFFF<br> [0x80004b00]<br>0x00000040<br> |- rs1 : x4<br> - rs2 : x8<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd87e65450c45 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x93c0cd5d79dd0 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                  |[0x8000051c]:fsub.d t1, tp, fp, dyn<br> [0x80000520]:csrrs a7, fcsr, zero<br> [0x80000524]:sw t1, 32(ra)<br> [0x80000528]:sw t2, 40(ra)<br> [0x8000052c]:sw t1, 48(ra)<br> [0x80000530]:sw a7, 56(ra)<br>                                    |
|  14|[0x80004b08]<br>0xFFFFFFFF<br> [0x80004b20]<br>0x00000060<br> |- rs1 : x6<br> - rs2 : x2<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd87e65450c45 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x93c0cd5d79dd0 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                  |[0x8000056c]:fsub.d tp, t1, sp, dyn<br> [0x80000570]:csrrs a7, fcsr, zero<br> [0x80000574]:sw tp, 64(ra)<br> [0x80000578]:sw t0, 72(ra)<br> [0x8000057c]:sw tp, 80(ra)<br> [0x80000580]:sw a7, 88(ra)<br>                                    |
|  15|[0x80004b28]<br>0xFFFFFFFF<br> [0x80004b40]<br>0x00000080<br> |- rs1 : x2<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd87e65450c45 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x93c0cd5d79dd0 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                               |[0x800005bc]:fsub.d t5, sp, t3, dyn<br> [0x800005c0]:csrrs a7, fcsr, zero<br> [0x800005c4]:sw t5, 96(ra)<br> [0x800005c8]:sw t6, 104(ra)<br> [0x800005cc]:sw t5, 112(ra)<br> [0x800005d0]:sw a7, 120(ra)<br>                                 |
|  16|[0x80004b48]<br>0xFFFFFFFE<br> [0x80004b60]<br>0x00000001<br> |- rs2 : x4<br> - fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xa0e7ad32453df and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf2f8c2966dd5f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                |[0x8000060c]:fsub.d t5, t3, tp, dyn<br> [0x80000610]:csrrs a7, fcsr, zero<br> [0x80000614]:sw t5, 128(ra)<br> [0x80000618]:sw t6, 136(ra)<br> [0x8000061c]:sw t5, 144(ra)<br> [0x80000620]:sw a7, 152(ra)<br>                                |
|  17|[0x80004b68]<br>0xFFFFFFFD<br> [0x80004b80]<br>0x00000021<br> |- rd : x2<br> - fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xa0e7ad32453df and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf2f8c2966dd5f and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                |[0x8000065c]:fsub.d sp, t5, t3, dyn<br> [0x80000660]:csrrs a7, fcsr, zero<br> [0x80000664]:sw sp, 160(ra)<br> [0x80000668]:sw gp, 168(ra)<br> [0x8000066c]:sw sp, 176(ra)<br> [0x80000670]:sw a7, 184(ra)<br>                                |
|  18|[0x80004b88]<br>0xFFFFFFFD<br> [0x80004ba0]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xa0e7ad32453df and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf2f8c2966dd5f and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800006ac]:fsub.d t5, t3, s10, dyn<br> [0x800006b0]:csrrs a7, fcsr, zero<br> [0x800006b4]:sw t5, 192(ra)<br> [0x800006b8]:sw t6, 200(ra)<br> [0x800006bc]:sw t5, 208(ra)<br> [0x800006c0]:sw a7, 216(ra)<br>                               |
|  19|[0x80004ba8]<br>0xFFFFFFFE<br> [0x80004bc0]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xa0e7ad32453df and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf2f8c2966dd5f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800006fc]:fsub.d t5, t3, s10, dyn<br> [0x80000700]:csrrs a7, fcsr, zero<br> [0x80000704]:sw t5, 224(ra)<br> [0x80000708]:sw t6, 232(ra)<br> [0x8000070c]:sw t5, 240(ra)<br> [0x80000710]:sw a7, 248(ra)<br>                               |
|  20|[0x80004bc8]<br>0xFFFFFFFE<br> [0x80004be0]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xa0e7ad32453df and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf2f8c2966dd5f and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000074c]:fsub.d t5, t3, s10, dyn<br> [0x80000750]:csrrs a7, fcsr, zero<br> [0x80000754]:sw t5, 256(ra)<br> [0x80000758]:sw t6, 264(ra)<br> [0x8000075c]:sw t5, 272(ra)<br> [0x80000760]:sw a7, 280(ra)<br>                               |
|  21|[0x80004be8]<br>0xFFFFFFFF<br> [0x80004c00]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe0d828b86622a and fs2 == 1 and fe2 == 0x7fa and fm2 == 0xf27d74799dd4f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000079c]:fsub.d t5, t3, s10, dyn<br> [0x800007a0]:csrrs a7, fcsr, zero<br> [0x800007a4]:sw t5, 288(ra)<br> [0x800007a8]:sw t6, 296(ra)<br> [0x800007ac]:sw t5, 304(ra)<br> [0x800007b0]:sw a7, 312(ra)<br>                               |
|  22|[0x80004c08]<br>0xFFFFFFFE<br> [0x80004c20]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe0d828b86622a and fs2 == 1 and fe2 == 0x7fa and fm2 == 0xf27d74799dd4f and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800007ec]:fsub.d t5, t3, s10, dyn<br> [0x800007f0]:csrrs a7, fcsr, zero<br> [0x800007f4]:sw t5, 320(ra)<br> [0x800007f8]:sw t6, 328(ra)<br> [0x800007fc]:sw t5, 336(ra)<br> [0x80000800]:sw a7, 344(ra)<br>                               |
|  23|[0x80004c28]<br>0xFFFFFFFE<br> [0x80004c40]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe0d828b86622a and fs2 == 1 and fe2 == 0x7fa and fm2 == 0xf27d74799dd4f and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000083c]:fsub.d t5, t3, s10, dyn<br> [0x80000840]:csrrs a7, fcsr, zero<br> [0x80000844]:sw t5, 352(ra)<br> [0x80000848]:sw t6, 360(ra)<br> [0x8000084c]:sw t5, 368(ra)<br> [0x80000850]:sw a7, 376(ra)<br>                               |
|  24|[0x80004c48]<br>0xFFFFFFFF<br> [0x80004c60]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe0d828b86622a and fs2 == 1 and fe2 == 0x7fa and fm2 == 0xf27d74799dd4f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000088c]:fsub.d t5, t3, s10, dyn<br> [0x80000890]:csrrs a7, fcsr, zero<br> [0x80000894]:sw t5, 384(ra)<br> [0x80000898]:sw t6, 392(ra)<br> [0x8000089c]:sw t5, 400(ra)<br> [0x800008a0]:sw a7, 408(ra)<br>                               |
|  25|[0x80004c68]<br>0xFFFFFFFF<br> [0x80004c80]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe0d828b86622a and fs2 == 1 and fe2 == 0x7fa and fm2 == 0xf27d74799dd4f and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800008dc]:fsub.d t5, t3, s10, dyn<br> [0x800008e0]:csrrs a7, fcsr, zero<br> [0x800008e4]:sw t5, 416(ra)<br> [0x800008e8]:sw t6, 424(ra)<br> [0x800008ec]:sw t5, 432(ra)<br> [0x800008f0]:sw a7, 440(ra)<br>                               |
|  26|[0x80004c88]<br>0xFFFFFFFE<br> [0x80004ca0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb9876f8130c3 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9119e241fb3cd and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000092c]:fsub.d t5, t3, s10, dyn<br> [0x80000930]:csrrs a7, fcsr, zero<br> [0x80000934]:sw t5, 448(ra)<br> [0x80000938]:sw t6, 456(ra)<br> [0x8000093c]:sw t5, 464(ra)<br> [0x80000940]:sw a7, 472(ra)<br>                               |
|  27|[0x80004ca8]<br>0xFFFFFFFD<br> [0x80004cc0]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb9876f8130c3 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9119e241fb3cd and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000097c]:fsub.d t5, t3, s10, dyn<br> [0x80000980]:csrrs a7, fcsr, zero<br> [0x80000984]:sw t5, 480(ra)<br> [0x80000988]:sw t6, 488(ra)<br> [0x8000098c]:sw t5, 496(ra)<br> [0x80000990]:sw a7, 504(ra)<br>                               |
|  28|[0x80004cc8]<br>0xFFFFFFFD<br> [0x80004ce0]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb9876f8130c3 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9119e241fb3cd and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800009cc]:fsub.d t5, t3, s10, dyn<br> [0x800009d0]:csrrs a7, fcsr, zero<br> [0x800009d4]:sw t5, 512(ra)<br> [0x800009d8]:sw t6, 520(ra)<br> [0x800009dc]:sw t5, 528(ra)<br> [0x800009e0]:sw a7, 536(ra)<br>                               |
|  29|[0x80004ce8]<br>0xFFFFFFFE<br> [0x80004d00]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb9876f8130c3 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9119e241fb3cd and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000a1c]:fsub.d t5, t3, s10, dyn<br> [0x80000a20]:csrrs a7, fcsr, zero<br> [0x80000a24]:sw t5, 544(ra)<br> [0x80000a28]:sw t6, 552(ra)<br> [0x80000a2c]:sw t5, 560(ra)<br> [0x80000a30]:sw a7, 568(ra)<br>                               |
|  30|[0x80004d08]<br>0xFFFFFFFE<br> [0x80004d20]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb9876f8130c3 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9119e241fb3cd and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000a6c]:fsub.d t5, t3, s10, dyn<br> [0x80000a70]:csrrs a7, fcsr, zero<br> [0x80000a74]:sw t5, 576(ra)<br> [0x80000a78]:sw t6, 584(ra)<br> [0x80000a7c]:sw t5, 592(ra)<br> [0x80000a80]:sw a7, 600(ra)<br>                               |
|  31|[0x80004d28]<br>0xFFFFFFFF<br> [0x80004d40]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xdbcde43895c3f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf12190de3b51d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000abc]:fsub.d t5, t3, s10, dyn<br> [0x80000ac0]:csrrs a7, fcsr, zero<br> [0x80000ac4]:sw t5, 608(ra)<br> [0x80000ac8]:sw t6, 616(ra)<br> [0x80000acc]:sw t5, 624(ra)<br> [0x80000ad0]:sw a7, 632(ra)<br>                               |
|  32|[0x80004d48]<br>0xFFFFFFFE<br> [0x80004d60]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xdbcde43895c3f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf12190de3b51d and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000b0c]:fsub.d t5, t3, s10, dyn<br> [0x80000b10]:csrrs a7, fcsr, zero<br> [0x80000b14]:sw t5, 640(ra)<br> [0x80000b18]:sw t6, 648(ra)<br> [0x80000b1c]:sw t5, 656(ra)<br> [0x80000b20]:sw a7, 664(ra)<br>                               |
|  33|[0x80004d68]<br>0xFFFFFFFE<br> [0x80004d80]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xdbcde43895c3f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf12190de3b51d and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000b5c]:fsub.d t5, t3, s10, dyn<br> [0x80000b60]:csrrs a7, fcsr, zero<br> [0x80000b64]:sw t5, 672(ra)<br> [0x80000b68]:sw t6, 680(ra)<br> [0x80000b6c]:sw t5, 688(ra)<br> [0x80000b70]:sw a7, 696(ra)<br>                               |
|  34|[0x80004d88]<br>0xFFFFFFFF<br> [0x80004da0]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xdbcde43895c3f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf12190de3b51d and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000bac]:fsub.d t5, t3, s10, dyn<br> [0x80000bb0]:csrrs a7, fcsr, zero<br> [0x80000bb4]:sw t5, 704(ra)<br> [0x80000bb8]:sw t6, 712(ra)<br> [0x80000bbc]:sw t5, 720(ra)<br> [0x80000bc0]:sw a7, 728(ra)<br>                               |
|  35|[0x80004da8]<br>0xFFFFFFFF<br> [0x80004dc0]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xdbcde43895c3f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf12190de3b51d and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000bfc]:fsub.d t5, t3, s10, dyn<br> [0x80000c00]:csrrs a7, fcsr, zero<br> [0x80000c04]:sw t5, 736(ra)<br> [0x80000c08]:sw t6, 744(ra)<br> [0x80000c0c]:sw t5, 752(ra)<br> [0x80000c10]:sw a7, 760(ra)<br>                               |
|  36|[0x80004dc8]<br>0xFFFFFFFE<br> [0x80004de0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc0659af8369fd and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x1fcd3283e4aff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000c4c]:fsub.d t5, t3, s10, dyn<br> [0x80000c50]:csrrs a7, fcsr, zero<br> [0x80000c54]:sw t5, 768(ra)<br> [0x80000c58]:sw t6, 776(ra)<br> [0x80000c5c]:sw t5, 784(ra)<br> [0x80000c60]:sw a7, 792(ra)<br>                               |
|  37|[0x80004de8]<br>0xFFFFFFFD<br> [0x80004e00]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc0659af8369fd and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x1fcd3283e4aff and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000c9c]:fsub.d t5, t3, s10, dyn<br> [0x80000ca0]:csrrs a7, fcsr, zero<br> [0x80000ca4]:sw t5, 800(ra)<br> [0x80000ca8]:sw t6, 808(ra)<br> [0x80000cac]:sw t5, 816(ra)<br> [0x80000cb0]:sw a7, 824(ra)<br>                               |
|  38|[0x80004e08]<br>0xFFFFFFFD<br> [0x80004e20]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc0659af8369fd and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x1fcd3283e4aff and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000cec]:fsub.d t5, t3, s10, dyn<br> [0x80000cf0]:csrrs a7, fcsr, zero<br> [0x80000cf4]:sw t5, 832(ra)<br> [0x80000cf8]:sw t6, 840(ra)<br> [0x80000cfc]:sw t5, 848(ra)<br> [0x80000d00]:sw a7, 856(ra)<br>                               |
|  39|[0x80004e28]<br>0xFFFFFFFE<br> [0x80004e40]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc0659af8369fd and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x1fcd3283e4aff and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000d3c]:fsub.d t5, t3, s10, dyn<br> [0x80000d40]:csrrs a7, fcsr, zero<br> [0x80000d44]:sw t5, 864(ra)<br> [0x80000d48]:sw t6, 872(ra)<br> [0x80000d4c]:sw t5, 880(ra)<br> [0x80000d50]:sw a7, 888(ra)<br>                               |
|  40|[0x80004e48]<br>0xFFFFFFFE<br> [0x80004e60]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc0659af8369fd and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x1fcd3283e4aff and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000d8c]:fsub.d t5, t3, s10, dyn<br> [0x80000d90]:csrrs a7, fcsr, zero<br> [0x80000d94]:sw t5, 896(ra)<br> [0x80000d98]:sw t6, 904(ra)<br> [0x80000d9c]:sw t5, 912(ra)<br> [0x80000da0]:sw a7, 920(ra)<br>                               |
|  41|[0x80004e68]<br>0xFFFFFFFF<br> [0x80004e80]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdd5b61587fd27 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x88a927a9e00b5 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000ddc]:fsub.d t5, t3, s10, dyn<br> [0x80000de0]:csrrs a7, fcsr, zero<br> [0x80000de4]:sw t5, 928(ra)<br> [0x80000de8]:sw t6, 936(ra)<br> [0x80000dec]:sw t5, 944(ra)<br> [0x80000df0]:sw a7, 952(ra)<br>                               |
|  42|[0x80004e88]<br>0xFFFFFFFE<br> [0x80004ea0]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdd5b61587fd27 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x88a927a9e00b5 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000e2c]:fsub.d t5, t3, s10, dyn<br> [0x80000e30]:csrrs a7, fcsr, zero<br> [0x80000e34]:sw t5, 960(ra)<br> [0x80000e38]:sw t6, 968(ra)<br> [0x80000e3c]:sw t5, 976(ra)<br> [0x80000e40]:sw a7, 984(ra)<br>                               |
|  43|[0x80004ea8]<br>0xFFFFFFFE<br> [0x80004ec0]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdd5b61587fd27 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x88a927a9e00b5 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000e7c]:fsub.d t5, t3, s10, dyn<br> [0x80000e80]:csrrs a7, fcsr, zero<br> [0x80000e84]:sw t5, 992(ra)<br> [0x80000e88]:sw t6, 1000(ra)<br> [0x80000e8c]:sw t5, 1008(ra)<br> [0x80000e90]:sw a7, 1016(ra)<br>                            |
|  44|[0x80004ec8]<br>0xFFFFFFFF<br> [0x80004ee0]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdd5b61587fd27 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x88a927a9e00b5 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000ecc]:fsub.d t5, t3, s10, dyn<br> [0x80000ed0]:csrrs a7, fcsr, zero<br> [0x80000ed4]:sw t5, 1024(ra)<br> [0x80000ed8]:sw t6, 1032(ra)<br> [0x80000edc]:sw t5, 1040(ra)<br> [0x80000ee0]:sw a7, 1048(ra)<br>                           |
|  45|[0x80004ee8]<br>0xFFFFFFFF<br> [0x80004f00]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdd5b61587fd27 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x88a927a9e00b5 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000f1c]:fsub.d t5, t3, s10, dyn<br> [0x80000f20]:csrrs a7, fcsr, zero<br> [0x80000f24]:sw t5, 1056(ra)<br> [0x80000f28]:sw t6, 1064(ra)<br> [0x80000f2c]:sw t5, 1072(ra)<br> [0x80000f30]:sw a7, 1080(ra)<br>                           |
|  46|[0x80004f08]<br>0xFFFFFFFE<br> [0x80004f20]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc01045c2cd787 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8ffbee8f4ca1c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000f6c]:fsub.d t5, t3, s10, dyn<br> [0x80000f70]:csrrs a7, fcsr, zero<br> [0x80000f74]:sw t5, 1088(ra)<br> [0x80000f78]:sw t6, 1096(ra)<br> [0x80000f7c]:sw t5, 1104(ra)<br> [0x80000f80]:sw a7, 1112(ra)<br>                           |
|  47|[0x80004f28]<br>0xFFFFFFFD<br> [0x80004f40]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc01045c2cd787 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8ffbee8f4ca1c and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000fbc]:fsub.d t5, t3, s10, dyn<br> [0x80000fc0]:csrrs a7, fcsr, zero<br> [0x80000fc4]:sw t5, 1120(ra)<br> [0x80000fc8]:sw t6, 1128(ra)<br> [0x80000fcc]:sw t5, 1136(ra)<br> [0x80000fd0]:sw a7, 1144(ra)<br>                           |
|  48|[0x80004f48]<br>0xFFFFFFFD<br> [0x80004f60]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc01045c2cd787 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8ffbee8f4ca1c and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000100c]:fsub.d t5, t3, s10, dyn<br> [0x80001010]:csrrs a7, fcsr, zero<br> [0x80001014]:sw t5, 1152(ra)<br> [0x80001018]:sw t6, 1160(ra)<br> [0x8000101c]:sw t5, 1168(ra)<br> [0x80001020]:sw a7, 1176(ra)<br>                           |
|  49|[0x80004f68]<br>0xFFFFFFFE<br> [0x80004f80]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc01045c2cd787 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8ffbee8f4ca1c and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000105c]:fsub.d t5, t3, s10, dyn<br> [0x80001060]:csrrs a7, fcsr, zero<br> [0x80001064]:sw t5, 1184(ra)<br> [0x80001068]:sw t6, 1192(ra)<br> [0x8000106c]:sw t5, 1200(ra)<br> [0x80001070]:sw a7, 1208(ra)<br>                           |
|  50|[0x80004f88]<br>0xFFFFFFFE<br> [0x80004fa0]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc01045c2cd787 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8ffbee8f4ca1c and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800010ac]:fsub.d t5, t3, s10, dyn<br> [0x800010b0]:csrrs a7, fcsr, zero<br> [0x800010b4]:sw t5, 1216(ra)<br> [0x800010b8]:sw t6, 1224(ra)<br> [0x800010bc]:sw t5, 1232(ra)<br> [0x800010c0]:sw a7, 1240(ra)<br>                           |
|  51|[0x80004fa8]<br>0xFFFFFFFE<br> [0x80004fc0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x28bc82f697c4d and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x6ba1be84b41d8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800010fc]:fsub.d t5, t3, s10, dyn<br> [0x80001100]:csrrs a7, fcsr, zero<br> [0x80001104]:sw t5, 1248(ra)<br> [0x80001108]:sw t6, 1256(ra)<br> [0x8000110c]:sw t5, 1264(ra)<br> [0x80001110]:sw a7, 1272(ra)<br>                           |
|  52|[0x80004fc8]<br>0xFFFFFFFE<br> [0x80004fe0]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x28bc82f697c4d and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x6ba1be84b41d8 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000114c]:fsub.d t5, t3, s10, dyn<br> [0x80001150]:csrrs a7, fcsr, zero<br> [0x80001154]:sw t5, 1280(ra)<br> [0x80001158]:sw t6, 1288(ra)<br> [0x8000115c]:sw t5, 1296(ra)<br> [0x80001160]:sw a7, 1304(ra)<br>                           |
|  53|[0x80004fe8]<br>0xFFFFFFFE<br> [0x80005000]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x28bc82f697c4d and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x6ba1be84b41d8 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000119c]:fsub.d t5, t3, s10, dyn<br> [0x800011a0]:csrrs a7, fcsr, zero<br> [0x800011a4]:sw t5, 1312(ra)<br> [0x800011a8]:sw t6, 1320(ra)<br> [0x800011ac]:sw t5, 1328(ra)<br> [0x800011b0]:sw a7, 1336(ra)<br>                           |
|  54|[0x80005008]<br>0xFFFFFFFF<br> [0x80005020]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x28bc82f697c4d and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x6ba1be84b41d8 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800011ec]:fsub.d t5, t3, s10, dyn<br> [0x800011f0]:csrrs a7, fcsr, zero<br> [0x800011f4]:sw t5, 1344(ra)<br> [0x800011f8]:sw t6, 1352(ra)<br> [0x800011fc]:sw t5, 1360(ra)<br> [0x80001200]:sw a7, 1368(ra)<br>                           |
|  55|[0x80005028]<br>0xFFFFFFFF<br> [0x80005040]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x28bc82f697c4d and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x6ba1be84b41d8 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000123c]:fsub.d t5, t3, s10, dyn<br> [0x80001240]:csrrs a7, fcsr, zero<br> [0x80001244]:sw t5, 1376(ra)<br> [0x80001248]:sw t6, 1384(ra)<br> [0x8000124c]:sw t5, 1392(ra)<br> [0x80001250]:sw a7, 1400(ra)<br>                           |
|  56|[0x80005048]<br>0xFFFFFFFE<br> [0x80005060]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xacd7053aa42a2 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x4ca3eb156f570 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000128c]:fsub.d t5, t3, s10, dyn<br> [0x80001290]:csrrs a7, fcsr, zero<br> [0x80001294]:sw t5, 1408(ra)<br> [0x80001298]:sw t6, 1416(ra)<br> [0x8000129c]:sw t5, 1424(ra)<br> [0x800012a0]:sw a7, 1432(ra)<br>                           |
|  57|[0x80005068]<br>0xFFFFFFFE<br> [0x80005080]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xacd7053aa42a2 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x4ca3eb156f570 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800012dc]:fsub.d t5, t3, s10, dyn<br> [0x800012e0]:csrrs a7, fcsr, zero<br> [0x800012e4]:sw t5, 1440(ra)<br> [0x800012e8]:sw t6, 1448(ra)<br> [0x800012ec]:sw t5, 1456(ra)<br> [0x800012f0]:sw a7, 1464(ra)<br>                           |
|  58|[0x80005088]<br>0xFFFFFFFE<br> [0x800050a0]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xacd7053aa42a2 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x4ca3eb156f570 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000132c]:fsub.d t5, t3, s10, dyn<br> [0x80001330]:csrrs a7, fcsr, zero<br> [0x80001334]:sw t5, 1472(ra)<br> [0x80001338]:sw t6, 1480(ra)<br> [0x8000133c]:sw t5, 1488(ra)<br> [0x80001340]:sw a7, 1496(ra)<br>                           |
|  59|[0x800050a8]<br>0xFFFFFFFE<br> [0x800050c0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xacd7053aa42a2 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x4ca3eb156f570 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000137c]:fsub.d t5, t3, s10, dyn<br> [0x80001380]:csrrs a7, fcsr, zero<br> [0x80001384]:sw t5, 1504(ra)<br> [0x80001388]:sw t6, 1512(ra)<br> [0x8000138c]:sw t5, 1520(ra)<br> [0x80001390]:sw a7, 1528(ra)<br>                           |
|  60|[0x800050c8]<br>0xFFFFFFFE<br> [0x800050e0]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xacd7053aa42a2 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x4ca3eb156f570 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800013cc]:fsub.d t5, t3, s10, dyn<br> [0x800013d0]:csrrs a7, fcsr, zero<br> [0x800013d4]:sw t5, 1536(ra)<br> [0x800013d8]:sw t6, 1544(ra)<br> [0x800013dc]:sw t5, 1552(ra)<br> [0x800013e0]:sw a7, 1560(ra)<br>                           |
|  61|[0x800050e8]<br>0xFFFFFFFF<br> [0x80005100]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x48dace8666677 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x6e4a62f333310 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000141c]:fsub.d t5, t3, s10, dyn<br> [0x80001420]:csrrs a7, fcsr, zero<br> [0x80001424]:sw t5, 1568(ra)<br> [0x80001428]:sw t6, 1576(ra)<br> [0x8000142c]:sw t5, 1584(ra)<br> [0x80001430]:sw a7, 1592(ra)<br>                           |
|  62|[0x80005108]<br>0xFFFFFFFF<br> [0x80005120]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x48dace8666677 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x6e4a62f333310 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000146c]:fsub.d t5, t3, s10, dyn<br> [0x80001470]:csrrs a7, fcsr, zero<br> [0x80001474]:sw t5, 1600(ra)<br> [0x80001478]:sw t6, 1608(ra)<br> [0x8000147c]:sw t5, 1616(ra)<br> [0x80001480]:sw a7, 1624(ra)<br>                           |
|  63|[0x80005128]<br>0xFFFFFFFF<br> [0x80005140]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x48dace8666677 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x6e4a62f333310 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800014bc]:fsub.d t5, t3, s10, dyn<br> [0x800014c0]:csrrs a7, fcsr, zero<br> [0x800014c4]:sw t5, 1632(ra)<br> [0x800014c8]:sw t6, 1640(ra)<br> [0x800014cc]:sw t5, 1648(ra)<br> [0x800014d0]:sw a7, 1656(ra)<br>                           |
|  64|[0x80005148]<br>0xFFFFFFFF<br> [0x80005160]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x48dace8666677 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x6e4a62f333310 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000150c]:fsub.d t5, t3, s10, dyn<br> [0x80001510]:csrrs a7, fcsr, zero<br> [0x80001514]:sw t5, 1664(ra)<br> [0x80001518]:sw t6, 1672(ra)<br> [0x8000151c]:sw t5, 1680(ra)<br> [0x80001520]:sw a7, 1688(ra)<br>                           |
|  65|[0x80005168]<br>0xFFFFFFFF<br> [0x80005180]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x48dace8666677 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x6e4a62f333310 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000155c]:fsub.d t5, t3, s10, dyn<br> [0x80001560]:csrrs a7, fcsr, zero<br> [0x80001564]:sw t5, 1696(ra)<br> [0x80001568]:sw t6, 1704(ra)<br> [0x8000156c]:sw t5, 1712(ra)<br> [0x80001570]:sw a7, 1720(ra)<br>                           |
|  66|[0x80005188]<br>0xFFFFFFFE<br> [0x800051a0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfc2ea66e5019e and fs2 == 1 and fe2 == 0x7f7 and fm2 == 0xe8acc8d7f2ffb and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800015ac]:fsub.d t5, t3, s10, dyn<br> [0x800015b0]:csrrs a7, fcsr, zero<br> [0x800015b4]:sw t5, 1728(ra)<br> [0x800015b8]:sw t6, 1736(ra)<br> [0x800015bc]:sw t5, 1744(ra)<br> [0x800015c0]:sw a7, 1752(ra)<br>                           |
|  67|[0x800051a8]<br>0xFFFFFFFD<br> [0x800051c0]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfc2ea66e5019e and fs2 == 1 and fe2 == 0x7f7 and fm2 == 0xe8acc8d7f2ffb and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800015fc]:fsub.d t5, t3, s10, dyn<br> [0x80001600]:csrrs a7, fcsr, zero<br> [0x80001604]:sw t5, 1760(ra)<br> [0x80001608]:sw t6, 1768(ra)<br> [0x8000160c]:sw t5, 1776(ra)<br> [0x80001610]:sw a7, 1784(ra)<br>                           |
|  68|[0x800051c8]<br>0xFFFFFFFD<br> [0x800051e0]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfc2ea66e5019e and fs2 == 1 and fe2 == 0x7f7 and fm2 == 0xe8acc8d7f2ffb and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000164c]:fsub.d t5, t3, s10, dyn<br> [0x80001650]:csrrs a7, fcsr, zero<br> [0x80001654]:sw t5, 1792(ra)<br> [0x80001658]:sw t6, 1800(ra)<br> [0x8000165c]:sw t5, 1808(ra)<br> [0x80001660]:sw a7, 1816(ra)<br>                           |
|  69|[0x800051e8]<br>0xFFFFFFFE<br> [0x80005200]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfc2ea66e5019e and fs2 == 1 and fe2 == 0x7f7 and fm2 == 0xe8acc8d7f2ffb and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000169c]:fsub.d t5, t3, s10, dyn<br> [0x800016a0]:csrrs a7, fcsr, zero<br> [0x800016a4]:sw t5, 1824(ra)<br> [0x800016a8]:sw t6, 1832(ra)<br> [0x800016ac]:sw t5, 1840(ra)<br> [0x800016b0]:sw a7, 1848(ra)<br>                           |
|  70|[0x80005208]<br>0xFFFFFFFE<br> [0x80005220]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfc2ea66e5019e and fs2 == 1 and fe2 == 0x7f7 and fm2 == 0xe8acc8d7f2ffb and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800016ec]:fsub.d t5, t3, s10, dyn<br> [0x800016f0]:csrrs a7, fcsr, zero<br> [0x800016f4]:sw t5, 1856(ra)<br> [0x800016f8]:sw t6, 1864(ra)<br> [0x800016fc]:sw t5, 1872(ra)<br> [0x80001700]:sw a7, 1880(ra)<br>                           |
|  71|[0x80005228]<br>0xF91B7583<br> [0x80005240]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xef2a4f7c7db7f and fs2 == 1 and fe2 == 0x7f8 and fm2 == 0xb9867d3787c37 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000173c]:fsub.d t5, t3, s10, dyn<br> [0x80001740]:csrrs a7, fcsr, zero<br> [0x80001744]:sw t5, 1888(ra)<br> [0x80001748]:sw t6, 1896(ra)<br> [0x8000174c]:sw t5, 1904(ra)<br> [0x80001750]:sw a7, 1912(ra)<br>                           |
|  72|[0x80005248]<br>0xF91B7582<br> [0x80005260]<br>0x00000021<br> |- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xef2a4f7c7db7f and fs2 == 1 and fe2 == 0x7f8 and fm2 == 0xb9867d3787c37 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000178c]:fsub.d t5, t3, s10, dyn<br> [0x80001790]:csrrs a7, fcsr, zero<br> [0x80001794]:sw t5, 1920(ra)<br> [0x80001798]:sw t6, 1928(ra)<br> [0x8000179c]:sw t5, 1936(ra)<br> [0x800017a0]:sw a7, 1944(ra)<br>                           |
|  73|[0x80005268]<br>0xF91B7582<br> [0x80005280]<br>0x00000041<br> |- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xef2a4f7c7db7f and fs2 == 1 and fe2 == 0x7f8 and fm2 == 0xb9867d3787c37 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800017dc]:fsub.d t5, t3, s10, dyn<br> [0x800017e0]:csrrs a7, fcsr, zero<br> [0x800017e4]:sw t5, 1952(ra)<br> [0x800017e8]:sw t6, 1960(ra)<br> [0x800017ec]:sw t5, 1968(ra)<br> [0x800017f0]:sw a7, 1976(ra)<br>                           |
|  74|[0x80005288]<br>0xF91B7583<br> [0x800052a0]<br>0x00000061<br> |- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xef2a4f7c7db7f and fs2 == 1 and fe2 == 0x7f8 and fm2 == 0xb9867d3787c37 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000182c]:fsub.d t5, t3, s10, dyn<br> [0x80001830]:csrrs a7, fcsr, zero<br> [0x80001834]:sw t5, 1984(ra)<br> [0x80001838]:sw t6, 1992(ra)<br> [0x8000183c]:sw t5, 2000(ra)<br> [0x80001840]:sw a7, 2008(ra)<br>                           |
|  75|[0x800052a8]<br>0xF91B7583<br> [0x800052c0]<br>0x00000081<br> |- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xef2a4f7c7db7f and fs2 == 1 and fe2 == 0x7f8 and fm2 == 0xb9867d3787c37 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000187c]:fsub.d t5, t3, s10, dyn<br> [0x80001880]:csrrs a7, fcsr, zero<br> [0x80001884]:sw t5, 2016(ra)<br> [0x80001888]:sw t6, 2024(ra)<br> [0x8000188c]:sw t5, 2032(ra)<br> [0x80001890]:sw a7, 2040(ra)<br>                           |
|  76|[0x800052c8]<br>0xE2913730<br> [0x800052e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7166677e49c3c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x9cc9e3436c322 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800018cc]:fsub.d t5, t3, s10, dyn<br> [0x800018d0]:csrrs a7, fcsr, zero<br> [0x800018d4]:addi ra, ra, 2040<br> [0x800018d8]:sw t5, 8(ra)<br> [0x800018dc]:sw t6, 16(ra)<br> [0x800018e0]:sw t5, 24(ra)<br> [0x800018e4]:sw a7, 32(ra)<br> |
|  77|[0x800052e8]<br>0xE2913730<br> [0x80005300]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7166677e49c3c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x9cc9e3436c322 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80001920]:fsub.d t5, t3, s10, dyn<br> [0x80001924]:csrrs a7, fcsr, zero<br> [0x80001928]:sw t5, 40(ra)<br> [0x8000192c]:sw t6, 48(ra)<br> [0x80001930]:sw t5, 56(ra)<br> [0x80001934]:sw a7, 64(ra)<br>                                   |
