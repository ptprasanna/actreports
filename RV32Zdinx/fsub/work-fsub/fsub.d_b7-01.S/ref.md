
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80008ae0')]      |
| SIG_REGION                | [('0x8000b710', '0x8000cc40', '1356 words')]      |
| COV_LABELS                | fsub.d_b7      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-fsub/fsub.d_b7-01.S/ref.S    |
| Total Number of coverpoints| 387     |
| Total Coverpoints Hit     | 387      |
| Total Signature Updates   | 844      |
| STAT1                     | 211      |
| STAT2                     | 0      |
| STAT3                     | 125     |
| STAT4                     | 422     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80004154]:fsub.d t5, t3, s10, dyn
[0x80004158]:csrrs a7, fcsr, zero
[0x8000415c]:sw t5, 1168(ra)
[0x80004160]:sw t6, 1176(ra)
[0x80004164]:sw t5, 1184(ra)
[0x80004168]:sw a7, 1192(ra)
[0x8000416c]:lui a4, 1
[0x80004170]:addi a4, a4, 2048
[0x80004174]:add a6, a6, a4
[0x80004178]:lw t3, 592(a6)
[0x8000417c]:sub a6, a6, a4
[0x80004180]:lui a4, 1
[0x80004184]:addi a4, a4, 2048
[0x80004188]:add a6, a6, a4
[0x8000418c]:lw t4, 596(a6)
[0x80004190]:sub a6, a6, a4
[0x80004194]:lui a4, 1
[0x80004198]:addi a4, a4, 2048
[0x8000419c]:add a6, a6, a4
[0x800041a0]:lw s10, 600(a6)
[0x800041a4]:sub a6, a6, a4
[0x800041a8]:lui a4, 1
[0x800041ac]:addi a4, a4, 2048
[0x800041b0]:add a6, a6, a4
[0x800041b4]:lw s11, 604(a6)
[0x800041b8]:sub a6, a6, a4
[0x800041bc]:lui t3, 846597
[0x800041c0]:addi t3, t3, 1216
[0x800041c4]:lui t4, 523932
[0x800041c8]:addi t4, t4, 2352
[0x800041cc]:lui s10, 846597
[0x800041d0]:addi s10, s10, 1216
[0x800041d4]:lui s11, 523932
[0x800041d8]:addi s11, s11, 2352
[0x800041dc]:addi a4, zero, 96
[0x800041e0]:csrrw zero, fcsr, a4
[0x800041e4]:fsub.d t5, t3, s10, dyn
[0x800041e8]:csrrs a7, fcsr, zero

[0x800041e4]:fsub.d t5, t3, s10, dyn
[0x800041e8]:csrrs a7, fcsr, zero
[0x800041ec]:sw t5, 1200(ra)
[0x800041f0]:sw t6, 1208(ra)
[0x800041f4]:sw t5, 1216(ra)
[0x800041f8]:sw a7, 1224(ra)
[0x800041fc]:lui a4, 1
[0x80004200]:addi a4, a4, 2048
[0x80004204]:add a6, a6, a4
[0x80004208]:lw t3, 608(a6)
[0x8000420c]:sub a6, a6, a4
[0x80004210]:lui a4, 1
[0x80004214]:addi a4, a4, 2048
[0x80004218]:add a6, a6, a4
[0x8000421c]:lw t4, 612(a6)
[0x80004220]:sub a6, a6, a4
[0x80004224]:lui a4, 1
[0x80004228]:addi a4, a4, 2048
[0x8000422c]:add a6, a6, a4
[0x80004230]:lw s10, 616(a6)
[0x80004234]:sub a6, a6, a4
[0x80004238]:lui a4, 1
[0x8000423c]:addi a4, a4, 2048
[0x80004240]:add a6, a6, a4
[0x80004244]:lw s11, 620(a6)
[0x80004248]:sub a6, a6, a4
[0x8000424c]:lui t3, 556130
[0x80004250]:addi t3, t3, 3636
[0x80004254]:lui t4, 523950
[0x80004258]:addi t4, t4, 1834
[0x8000425c]:lui s10, 556130
[0x80004260]:addi s10, s10, 3636
[0x80004264]:lui s11, 523950
[0x80004268]:addi s11, s11, 1834
[0x8000426c]:addi a4, zero, 96
[0x80004270]:csrrw zero, fcsr, a4
[0x80004274]:fsub.d t5, t3, s10, dyn
[0x80004278]:csrrs a7, fcsr, zero

[0x80004274]:fsub.d t5, t3, s10, dyn
[0x80004278]:csrrs a7, fcsr, zero
[0x8000427c]:sw t5, 1232(ra)
[0x80004280]:sw t6, 1240(ra)
[0x80004284]:sw t5, 1248(ra)
[0x80004288]:sw a7, 1256(ra)
[0x8000428c]:lui a4, 1
[0x80004290]:addi a4, a4, 2048
[0x80004294]:add a6, a6, a4
[0x80004298]:lw t3, 624(a6)
[0x8000429c]:sub a6, a6, a4
[0x800042a0]:lui a4, 1
[0x800042a4]:addi a4, a4, 2048
[0x800042a8]:add a6, a6, a4
[0x800042ac]:lw t4, 628(a6)
[0x800042b0]:sub a6, a6, a4
[0x800042b4]:lui a4, 1
[0x800042b8]:addi a4, a4, 2048
[0x800042bc]:add a6, a6, a4
[0x800042c0]:lw s10, 632(a6)
[0x800042c4]:sub a6, a6, a4
[0x800042c8]:lui a4, 1
[0x800042cc]:addi a4, a4, 2048
[0x800042d0]:add a6, a6, a4
[0x800042d4]:lw s11, 636(a6)
[0x800042d8]:sub a6, a6, a4
[0x800042dc]:lui t3, 953638
[0x800042e0]:addi t3, t3, 3411
[0x800042e4]:lui t4, 523873
[0x800042e8]:addi t4, t4, 297
[0x800042ec]:lui s10, 953638
[0x800042f0]:addi s10, s10, 3411
[0x800042f4]:lui s11, 523873
[0x800042f8]:addi s11, s11, 297
[0x800042fc]:addi a4, zero, 96
[0x80004300]:csrrw zero, fcsr, a4
[0x80004304]:fsub.d t5, t3, s10, dyn
[0x80004308]:csrrs a7, fcsr, zero

[0x80004304]:fsub.d t5, t3, s10, dyn
[0x80004308]:csrrs a7, fcsr, zero
[0x8000430c]:sw t5, 1264(ra)
[0x80004310]:sw t6, 1272(ra)
[0x80004314]:sw t5, 1280(ra)
[0x80004318]:sw a7, 1288(ra)
[0x8000431c]:lui a4, 1
[0x80004320]:addi a4, a4, 2048
[0x80004324]:add a6, a6, a4
[0x80004328]:lw t3, 640(a6)
[0x8000432c]:sub a6, a6, a4
[0x80004330]:lui a4, 1
[0x80004334]:addi a4, a4, 2048
[0x80004338]:add a6, a6, a4
[0x8000433c]:lw t4, 644(a6)
[0x80004340]:sub a6, a6, a4
[0x80004344]:lui a4, 1
[0x80004348]:addi a4, a4, 2048
[0x8000434c]:add a6, a6, a4
[0x80004350]:lw s10, 648(a6)
[0x80004354]:sub a6, a6, a4
[0x80004358]:lui a4, 1
[0x8000435c]:addi a4, a4, 2048
[0x80004360]:add a6, a6, a4
[0x80004364]:lw s11, 652(a6)
[0x80004368]:sub a6, a6, a4
[0x8000436c]:lui t3, 312525
[0x80004370]:addi t3, t3, 2243
[0x80004374]:lui t4, 523707
[0x80004378]:addi t4, t4, 87
[0x8000437c]:lui s10, 312525
[0x80004380]:addi s10, s10, 2243
[0x80004384]:lui s11, 523707
[0x80004388]:addi s11, s11, 87
[0x8000438c]:addi a4, zero, 96
[0x80004390]:csrrw zero, fcsr, a4
[0x80004394]:fsub.d t5, t3, s10, dyn
[0x80004398]:csrrs a7, fcsr, zero

[0x80004394]:fsub.d t5, t3, s10, dyn
[0x80004398]:csrrs a7, fcsr, zero
[0x8000439c]:sw t5, 1296(ra)
[0x800043a0]:sw t6, 1304(ra)
[0x800043a4]:sw t5, 1312(ra)
[0x800043a8]:sw a7, 1320(ra)
[0x800043ac]:lui a4, 1
[0x800043b0]:addi a4, a4, 2048
[0x800043b4]:add a6, a6, a4
[0x800043b8]:lw t3, 656(a6)
[0x800043bc]:sub a6, a6, a4
[0x800043c0]:lui a4, 1
[0x800043c4]:addi a4, a4, 2048
[0x800043c8]:add a6, a6, a4
[0x800043cc]:lw t4, 660(a6)
[0x800043d0]:sub a6, a6, a4
[0x800043d4]:lui a4, 1
[0x800043d8]:addi a4, a4, 2048
[0x800043dc]:add a6, a6, a4
[0x800043e0]:lw s10, 664(a6)
[0x800043e4]:sub a6, a6, a4
[0x800043e8]:lui a4, 1
[0x800043ec]:addi a4, a4, 2048
[0x800043f0]:add a6, a6, a4
[0x800043f4]:lw s11, 668(a6)
[0x800043f8]:sub a6, a6, a4
[0x800043fc]:lui t3, 554797
[0x80004400]:addi t3, t3, 2578
[0x80004404]:lui t4, 523886
[0x80004408]:addi t4, t4, 4055
[0x8000440c]:lui s10, 554797
[0x80004410]:addi s10, s10, 2578
[0x80004414]:lui s11, 523886
[0x80004418]:addi s11, s11, 4055
[0x8000441c]:addi a4, zero, 96
[0x80004420]:csrrw zero, fcsr, a4
[0x80004424]:fsub.d t5, t3, s10, dyn
[0x80004428]:csrrs a7, fcsr, zero

[0x80004424]:fsub.d t5, t3, s10, dyn
[0x80004428]:csrrs a7, fcsr, zero
[0x8000442c]:sw t5, 1328(ra)
[0x80004430]:sw t6, 1336(ra)
[0x80004434]:sw t5, 1344(ra)
[0x80004438]:sw a7, 1352(ra)
[0x8000443c]:lui a4, 1
[0x80004440]:addi a4, a4, 2048
[0x80004444]:add a6, a6, a4
[0x80004448]:lw t3, 672(a6)
[0x8000444c]:sub a6, a6, a4
[0x80004450]:lui a4, 1
[0x80004454]:addi a4, a4, 2048
[0x80004458]:add a6, a6, a4
[0x8000445c]:lw t4, 676(a6)
[0x80004460]:sub a6, a6, a4
[0x80004464]:lui a4, 1
[0x80004468]:addi a4, a4, 2048
[0x8000446c]:add a6, a6, a4
[0x80004470]:lw s10, 680(a6)
[0x80004474]:sub a6, a6, a4
[0x80004478]:lui a4, 1
[0x8000447c]:addi a4, a4, 2048
[0x80004480]:add a6, a6, a4
[0x80004484]:lw s11, 684(a6)
[0x80004488]:sub a6, a6, a4
[0x8000448c]:lui t3, 189326
[0x80004490]:addi t3, t3, 741
[0x80004494]:lui t4, 523523
[0x80004498]:addi t4, t4, 2656
[0x8000449c]:lui s10, 189326
[0x800044a0]:addi s10, s10, 741
[0x800044a4]:lui s11, 523523
[0x800044a8]:addi s11, s11, 2656
[0x800044ac]:addi a4, zero, 96
[0x800044b0]:csrrw zero, fcsr, a4
[0x800044b4]:fsub.d t5, t3, s10, dyn
[0x800044b8]:csrrs a7, fcsr, zero

[0x800044b4]:fsub.d t5, t3, s10, dyn
[0x800044b8]:csrrs a7, fcsr, zero
[0x800044bc]:sw t5, 1360(ra)
[0x800044c0]:sw t6, 1368(ra)
[0x800044c4]:sw t5, 1376(ra)
[0x800044c8]:sw a7, 1384(ra)
[0x800044cc]:lui a4, 1
[0x800044d0]:addi a4, a4, 2048
[0x800044d4]:add a6, a6, a4
[0x800044d8]:lw t3, 688(a6)
[0x800044dc]:sub a6, a6, a4
[0x800044e0]:lui a4, 1
[0x800044e4]:addi a4, a4, 2048
[0x800044e8]:add a6, a6, a4
[0x800044ec]:lw t4, 692(a6)
[0x800044f0]:sub a6, a6, a4
[0x800044f4]:lui a4, 1
[0x800044f8]:addi a4, a4, 2048
[0x800044fc]:add a6, a6, a4
[0x80004500]:lw s10, 696(a6)
[0x80004504]:sub a6, a6, a4
[0x80004508]:lui a4, 1
[0x8000450c]:addi a4, a4, 2048
[0x80004510]:add a6, a6, a4
[0x80004514]:lw s11, 700(a6)
[0x80004518]:sub a6, a6, a4
[0x8000451c]:lui t3, 662679
[0x80004520]:addi t3, t3, 2664
[0x80004524]:lui t4, 524013
[0x80004528]:addi t4, t4, 1038
[0x8000452c]:lui s10, 662679
[0x80004530]:addi s10, s10, 2664
[0x80004534]:lui s11, 524013
[0x80004538]:addi s11, s11, 1038
[0x8000453c]:addi a4, zero, 96
[0x80004540]:csrrw zero, fcsr, a4
[0x80004544]:fsub.d t5, t3, s10, dyn
[0x80004548]:csrrs a7, fcsr, zero

[0x80004544]:fsub.d t5, t3, s10, dyn
[0x80004548]:csrrs a7, fcsr, zero
[0x8000454c]:sw t5, 1392(ra)
[0x80004550]:sw t6, 1400(ra)
[0x80004554]:sw t5, 1408(ra)
[0x80004558]:sw a7, 1416(ra)
[0x8000455c]:lui a4, 1
[0x80004560]:addi a4, a4, 2048
[0x80004564]:add a6, a6, a4
[0x80004568]:lw t3, 704(a6)
[0x8000456c]:sub a6, a6, a4
[0x80004570]:lui a4, 1
[0x80004574]:addi a4, a4, 2048
[0x80004578]:add a6, a6, a4
[0x8000457c]:lw t4, 708(a6)
[0x80004580]:sub a6, a6, a4
[0x80004584]:lui a4, 1
[0x80004588]:addi a4, a4, 2048
[0x8000458c]:add a6, a6, a4
[0x80004590]:lw s10, 712(a6)
[0x80004594]:sub a6, a6, a4
[0x80004598]:lui a4, 1
[0x8000459c]:addi a4, a4, 2048
[0x800045a0]:add a6, a6, a4
[0x800045a4]:lw s11, 716(a6)
[0x800045a8]:sub a6, a6, a4
[0x800045ac]:lui t3, 561156
[0x800045b0]:addi t3, t3, 2462
[0x800045b4]:lui t4, 523801
[0x800045b8]:addi t4, t4, 926
[0x800045bc]:lui s10, 561156
[0x800045c0]:addi s10, s10, 2462
[0x800045c4]:lui s11, 523801
[0x800045c8]:addi s11, s11, 926
[0x800045cc]:addi a4, zero, 96
[0x800045d0]:csrrw zero, fcsr, a4
[0x800045d4]:fsub.d t5, t3, s10, dyn
[0x800045d8]:csrrs a7, fcsr, zero

[0x800045d4]:fsub.d t5, t3, s10, dyn
[0x800045d8]:csrrs a7, fcsr, zero
[0x800045dc]:sw t5, 1424(ra)
[0x800045e0]:sw t6, 1432(ra)
[0x800045e4]:sw t5, 1440(ra)
[0x800045e8]:sw a7, 1448(ra)
[0x800045ec]:lui a4, 1
[0x800045f0]:addi a4, a4, 2048
[0x800045f4]:add a6, a6, a4
[0x800045f8]:lw t3, 720(a6)
[0x800045fc]:sub a6, a6, a4
[0x80004600]:lui a4, 1
[0x80004604]:addi a4, a4, 2048
[0x80004608]:add a6, a6, a4
[0x8000460c]:lw t4, 724(a6)
[0x80004610]:sub a6, a6, a4
[0x80004614]:lui a4, 1
[0x80004618]:addi a4, a4, 2048
[0x8000461c]:add a6, a6, a4
[0x80004620]:lw s10, 728(a6)
[0x80004624]:sub a6, a6, a4
[0x80004628]:lui a4, 1
[0x8000462c]:addi a4, a4, 2048
[0x80004630]:add a6, a6, a4
[0x80004634]:lw s11, 732(a6)
[0x80004638]:sub a6, a6, a4
[0x8000463c]:lui t3, 123504
[0x80004640]:addi t3, t3, 2348
[0x80004644]:lui t4, 523956
[0x80004648]:addi t4, t4, 3530
[0x8000464c]:lui s10, 123504
[0x80004650]:addi s10, s10, 2348
[0x80004654]:lui s11, 523956
[0x80004658]:addi s11, s11, 3530
[0x8000465c]:addi a4, zero, 96
[0x80004660]:csrrw zero, fcsr, a4
[0x80004664]:fsub.d t5, t3, s10, dyn
[0x80004668]:csrrs a7, fcsr, zero

[0x80004664]:fsub.d t5, t3, s10, dyn
[0x80004668]:csrrs a7, fcsr, zero
[0x8000466c]:sw t5, 1456(ra)
[0x80004670]:sw t6, 1464(ra)
[0x80004674]:sw t5, 1472(ra)
[0x80004678]:sw a7, 1480(ra)
[0x8000467c]:lui a4, 1
[0x80004680]:addi a4, a4, 2048
[0x80004684]:add a6, a6, a4
[0x80004688]:lw t3, 736(a6)
[0x8000468c]:sub a6, a6, a4
[0x80004690]:lui a4, 1
[0x80004694]:addi a4, a4, 2048
[0x80004698]:add a6, a6, a4
[0x8000469c]:lw t4, 740(a6)
[0x800046a0]:sub a6, a6, a4
[0x800046a4]:lui a4, 1
[0x800046a8]:addi a4, a4, 2048
[0x800046ac]:add a6, a6, a4
[0x800046b0]:lw s10, 744(a6)
[0x800046b4]:sub a6, a6, a4
[0x800046b8]:lui a4, 1
[0x800046bc]:addi a4, a4, 2048
[0x800046c0]:add a6, a6, a4
[0x800046c4]:lw s11, 748(a6)
[0x800046c8]:sub a6, a6, a4
[0x800046cc]:lui t3, 944908
[0x800046d0]:addi t3, t3, 2389
[0x800046d4]:lui t4, 523669
[0x800046d8]:addi t4, t4, 849
[0x800046dc]:lui s10, 944908
[0x800046e0]:addi s10, s10, 2389
[0x800046e4]:lui s11, 523669
[0x800046e8]:addi s11, s11, 849
[0x800046ec]:addi a4, zero, 96
[0x800046f0]:csrrw zero, fcsr, a4
[0x800046f4]:fsub.d t5, t3, s10, dyn
[0x800046f8]:csrrs a7, fcsr, zero

[0x800046f4]:fsub.d t5, t3, s10, dyn
[0x800046f8]:csrrs a7, fcsr, zero
[0x800046fc]:sw t5, 1488(ra)
[0x80004700]:sw t6, 1496(ra)
[0x80004704]:sw t5, 1504(ra)
[0x80004708]:sw a7, 1512(ra)
[0x8000470c]:lui a4, 1
[0x80004710]:addi a4, a4, 2048
[0x80004714]:add a6, a6, a4
[0x80004718]:lw t3, 752(a6)
[0x8000471c]:sub a6, a6, a4
[0x80004720]:lui a4, 1
[0x80004724]:addi a4, a4, 2048
[0x80004728]:add a6, a6, a4
[0x8000472c]:lw t4, 756(a6)
[0x80004730]:sub a6, a6, a4
[0x80004734]:lui a4, 1
[0x80004738]:addi a4, a4, 2048
[0x8000473c]:add a6, a6, a4
[0x80004740]:lw s10, 760(a6)
[0x80004744]:sub a6, a6, a4
[0x80004748]:lui a4, 1
[0x8000474c]:addi a4, a4, 2048
[0x80004750]:add a6, a6, a4
[0x80004754]:lw s11, 764(a6)
[0x80004758]:sub a6, a6, a4
[0x8000475c]:lui t3, 835118
[0x80004760]:addi t3, t3, 575
[0x80004764]:lui t4, 523528
[0x80004768]:addi t4, t4, 656
[0x8000476c]:lui s10, 835118
[0x80004770]:addi s10, s10, 575
[0x80004774]:lui s11, 523528
[0x80004778]:addi s11, s11, 656
[0x8000477c]:addi a4, zero, 96
[0x80004780]:csrrw zero, fcsr, a4
[0x80004784]:fsub.d t5, t3, s10, dyn
[0x80004788]:csrrs a7, fcsr, zero

[0x80004784]:fsub.d t5, t3, s10, dyn
[0x80004788]:csrrs a7, fcsr, zero
[0x8000478c]:sw t5, 1520(ra)
[0x80004790]:sw t6, 1528(ra)
[0x80004794]:sw t5, 1536(ra)
[0x80004798]:sw a7, 1544(ra)
[0x8000479c]:lui a4, 1
[0x800047a0]:addi a4, a4, 2048
[0x800047a4]:add a6, a6, a4
[0x800047a8]:lw t3, 768(a6)
[0x800047ac]:sub a6, a6, a4
[0x800047b0]:lui a4, 1
[0x800047b4]:addi a4, a4, 2048
[0x800047b8]:add a6, a6, a4
[0x800047bc]:lw t4, 772(a6)
[0x800047c0]:sub a6, a6, a4
[0x800047c4]:lui a4, 1
[0x800047c8]:addi a4, a4, 2048
[0x800047cc]:add a6, a6, a4
[0x800047d0]:lw s10, 776(a6)
[0x800047d4]:sub a6, a6, a4
[0x800047d8]:lui a4, 1
[0x800047dc]:addi a4, a4, 2048
[0x800047e0]:add a6, a6, a4
[0x800047e4]:lw s11, 780(a6)
[0x800047e8]:sub a6, a6, a4
[0x800047ec]:lui t3, 539568
[0x800047f0]:addi t3, t3, 1492
[0x800047f4]:lui t4, 523851
[0x800047f8]:addi t4, t4, 4000
[0x800047fc]:lui s10, 539568
[0x80004800]:addi s10, s10, 1492
[0x80004804]:lui s11, 523851
[0x80004808]:addi s11, s11, 4000
[0x8000480c]:addi a4, zero, 96
[0x80004810]:csrrw zero, fcsr, a4
[0x80004814]:fsub.d t5, t3, s10, dyn
[0x80004818]:csrrs a7, fcsr, zero

[0x80004814]:fsub.d t5, t3, s10, dyn
[0x80004818]:csrrs a7, fcsr, zero
[0x8000481c]:sw t5, 1552(ra)
[0x80004820]:sw t6, 1560(ra)
[0x80004824]:sw t5, 1568(ra)
[0x80004828]:sw a7, 1576(ra)
[0x8000482c]:lui a4, 1
[0x80004830]:addi a4, a4, 2048
[0x80004834]:add a6, a6, a4
[0x80004838]:lw t3, 784(a6)
[0x8000483c]:sub a6, a6, a4
[0x80004840]:lui a4, 1
[0x80004844]:addi a4, a4, 2048
[0x80004848]:add a6, a6, a4
[0x8000484c]:lw t4, 788(a6)
[0x80004850]:sub a6, a6, a4
[0x80004854]:lui a4, 1
[0x80004858]:addi a4, a4, 2048
[0x8000485c]:add a6, a6, a4
[0x80004860]:lw s10, 792(a6)
[0x80004864]:sub a6, a6, a4
[0x80004868]:lui a4, 1
[0x8000486c]:addi a4, a4, 2048
[0x80004870]:add a6, a6, a4
[0x80004874]:lw s11, 796(a6)
[0x80004878]:sub a6, a6, a4
[0x8000487c]:lui t3, 573750
[0x80004880]:addi t3, t3, 3429
[0x80004884]:lui t4, 523812
[0x80004888]:addi t4, t4, 610
[0x8000488c]:lui s10, 573750
[0x80004890]:addi s10, s10, 3429
[0x80004894]:lui s11, 523812
[0x80004898]:addi s11, s11, 610
[0x8000489c]:addi a4, zero, 96
[0x800048a0]:csrrw zero, fcsr, a4
[0x800048a4]:fsub.d t5, t3, s10, dyn
[0x800048a8]:csrrs a7, fcsr, zero

[0x800048a4]:fsub.d t5, t3, s10, dyn
[0x800048a8]:csrrs a7, fcsr, zero
[0x800048ac]:sw t5, 1584(ra)
[0x800048b0]:sw t6, 1592(ra)
[0x800048b4]:sw t5, 1600(ra)
[0x800048b8]:sw a7, 1608(ra)
[0x800048bc]:lui a4, 1
[0x800048c0]:addi a4, a4, 2048
[0x800048c4]:add a6, a6, a4
[0x800048c8]:lw t3, 800(a6)
[0x800048cc]:sub a6, a6, a4
[0x800048d0]:lui a4, 1
[0x800048d4]:addi a4, a4, 2048
[0x800048d8]:add a6, a6, a4
[0x800048dc]:lw t4, 804(a6)
[0x800048e0]:sub a6, a6, a4
[0x800048e4]:lui a4, 1
[0x800048e8]:addi a4, a4, 2048
[0x800048ec]:add a6, a6, a4
[0x800048f0]:lw s10, 808(a6)
[0x800048f4]:sub a6, a6, a4
[0x800048f8]:lui a4, 1
[0x800048fc]:addi a4, a4, 2048
[0x80004900]:add a6, a6, a4
[0x80004904]:lw s11, 812(a6)
[0x80004908]:sub a6, a6, a4
[0x8000490c]:lui t3, 739904
[0x80004910]:addi t3, t3, 3081
[0x80004914]:lui t4, 523627
[0x80004918]:addi t4, t4, 1892
[0x8000491c]:lui s10, 739904
[0x80004920]:addi s10, s10, 3081
[0x80004924]:lui s11, 523627
[0x80004928]:addi s11, s11, 1892
[0x8000492c]:addi a4, zero, 96
[0x80004930]:csrrw zero, fcsr, a4
[0x80004934]:fsub.d t5, t3, s10, dyn
[0x80004938]:csrrs a7, fcsr, zero

[0x80004934]:fsub.d t5, t3, s10, dyn
[0x80004938]:csrrs a7, fcsr, zero
[0x8000493c]:sw t5, 1616(ra)
[0x80004940]:sw t6, 1624(ra)
[0x80004944]:sw t5, 1632(ra)
[0x80004948]:sw a7, 1640(ra)
[0x8000494c]:lui a4, 1
[0x80004950]:addi a4, a4, 2048
[0x80004954]:add a6, a6, a4
[0x80004958]:lw t3, 816(a6)
[0x8000495c]:sub a6, a6, a4
[0x80004960]:lui a4, 1
[0x80004964]:addi a4, a4, 2048
[0x80004968]:add a6, a6, a4
[0x8000496c]:lw t4, 820(a6)
[0x80004970]:sub a6, a6, a4
[0x80004974]:lui a4, 1
[0x80004978]:addi a4, a4, 2048
[0x8000497c]:add a6, a6, a4
[0x80004980]:lw s10, 824(a6)
[0x80004984]:sub a6, a6, a4
[0x80004988]:lui a4, 1
[0x8000498c]:addi a4, a4, 2048
[0x80004990]:add a6, a6, a4
[0x80004994]:lw s11, 828(a6)
[0x80004998]:sub a6, a6, a4
[0x8000499c]:lui t3, 271220
[0x800049a0]:addi t3, t3, 1883
[0x800049a4]:lui t4, 523264
[0x800049a8]:addi t4, t4, 1082
[0x800049ac]:lui s10, 271220
[0x800049b0]:addi s10, s10, 1883
[0x800049b4]:lui s11, 523264
[0x800049b8]:addi s11, s11, 1082
[0x800049bc]:addi a4, zero, 96
[0x800049c0]:csrrw zero, fcsr, a4
[0x800049c4]:fsub.d t5, t3, s10, dyn
[0x800049c8]:csrrs a7, fcsr, zero

[0x800049c4]:fsub.d t5, t3, s10, dyn
[0x800049c8]:csrrs a7, fcsr, zero
[0x800049cc]:sw t5, 1648(ra)
[0x800049d0]:sw t6, 1656(ra)
[0x800049d4]:sw t5, 1664(ra)
[0x800049d8]:sw a7, 1672(ra)
[0x800049dc]:lui a4, 1
[0x800049e0]:addi a4, a4, 2048
[0x800049e4]:add a6, a6, a4
[0x800049e8]:lw t3, 832(a6)
[0x800049ec]:sub a6, a6, a4
[0x800049f0]:lui a4, 1
[0x800049f4]:addi a4, a4, 2048
[0x800049f8]:add a6, a6, a4
[0x800049fc]:lw t4, 836(a6)
[0x80004a00]:sub a6, a6, a4
[0x80004a04]:lui a4, 1
[0x80004a08]:addi a4, a4, 2048
[0x80004a0c]:add a6, a6, a4
[0x80004a10]:lw s10, 840(a6)
[0x80004a14]:sub a6, a6, a4
[0x80004a18]:lui a4, 1
[0x80004a1c]:addi a4, a4, 2048
[0x80004a20]:add a6, a6, a4
[0x80004a24]:lw s11, 844(a6)
[0x80004a28]:sub a6, a6, a4
[0x80004a2c]:lui t3, 414137
[0x80004a30]:addi t3, t3, 3949
[0x80004a34]:lui t4, 523944
[0x80004a38]:addi t4, t4, 1697
[0x80004a3c]:lui s10, 414137
[0x80004a40]:addi s10, s10, 3949
[0x80004a44]:lui s11, 523944
[0x80004a48]:addi s11, s11, 1697
[0x80004a4c]:addi a4, zero, 96
[0x80004a50]:csrrw zero, fcsr, a4
[0x80004a54]:fsub.d t5, t3, s10, dyn
[0x80004a58]:csrrs a7, fcsr, zero

[0x80004a54]:fsub.d t5, t3, s10, dyn
[0x80004a58]:csrrs a7, fcsr, zero
[0x80004a5c]:sw t5, 1680(ra)
[0x80004a60]:sw t6, 1688(ra)
[0x80004a64]:sw t5, 1696(ra)
[0x80004a68]:sw a7, 1704(ra)
[0x80004a6c]:lui a4, 1
[0x80004a70]:addi a4, a4, 2048
[0x80004a74]:add a6, a6, a4
[0x80004a78]:lw t3, 848(a6)
[0x80004a7c]:sub a6, a6, a4
[0x80004a80]:lui a4, 1
[0x80004a84]:addi a4, a4, 2048
[0x80004a88]:add a6, a6, a4
[0x80004a8c]:lw t4, 852(a6)
[0x80004a90]:sub a6, a6, a4
[0x80004a94]:lui a4, 1
[0x80004a98]:addi a4, a4, 2048
[0x80004a9c]:add a6, a6, a4
[0x80004aa0]:lw s10, 856(a6)
[0x80004aa4]:sub a6, a6, a4
[0x80004aa8]:lui a4, 1
[0x80004aac]:addi a4, a4, 2048
[0x80004ab0]:add a6, a6, a4
[0x80004ab4]:lw s11, 860(a6)
[0x80004ab8]:sub a6, a6, a4
[0x80004abc]:lui t3, 497162
[0x80004ac0]:addi t3, t3, 3295
[0x80004ac4]:lui t4, 523675
[0x80004ac8]:addi t4, t4, 2900
[0x80004acc]:lui s10, 497162
[0x80004ad0]:addi s10, s10, 3295
[0x80004ad4]:lui s11, 523675
[0x80004ad8]:addi s11, s11, 2900
[0x80004adc]:addi a4, zero, 96
[0x80004ae0]:csrrw zero, fcsr, a4
[0x80004ae4]:fsub.d t5, t3, s10, dyn
[0x80004ae8]:csrrs a7, fcsr, zero

[0x80004ae4]:fsub.d t5, t3, s10, dyn
[0x80004ae8]:csrrs a7, fcsr, zero
[0x80004aec]:sw t5, 1712(ra)
[0x80004af0]:sw t6, 1720(ra)
[0x80004af4]:sw t5, 1728(ra)
[0x80004af8]:sw a7, 1736(ra)
[0x80004afc]:lui a4, 1
[0x80004b00]:addi a4, a4, 2048
[0x80004b04]:add a6, a6, a4
[0x80004b08]:lw t3, 864(a6)
[0x80004b0c]:sub a6, a6, a4
[0x80004b10]:lui a4, 1
[0x80004b14]:addi a4, a4, 2048
[0x80004b18]:add a6, a6, a4
[0x80004b1c]:lw t4, 868(a6)
[0x80004b20]:sub a6, a6, a4
[0x80004b24]:lui a4, 1
[0x80004b28]:addi a4, a4, 2048
[0x80004b2c]:add a6, a6, a4
[0x80004b30]:lw s10, 872(a6)
[0x80004b34]:sub a6, a6, a4
[0x80004b38]:lui a4, 1
[0x80004b3c]:addi a4, a4, 2048
[0x80004b40]:add a6, a6, a4
[0x80004b44]:lw s11, 876(a6)
[0x80004b48]:sub a6, a6, a4
[0x80004b4c]:lui t3, 752746
[0x80004b50]:addi t3, t3, 1143
[0x80004b54]:lui t4, 523486
[0x80004b58]:addi t4, t4, 1100
[0x80004b5c]:lui s10, 752746
[0x80004b60]:addi s10, s10, 1143
[0x80004b64]:lui s11, 523486
[0x80004b68]:addi s11, s11, 1100
[0x80004b6c]:addi a4, zero, 96
[0x80004b70]:csrrw zero, fcsr, a4
[0x80004b74]:fsub.d t5, t3, s10, dyn
[0x80004b78]:csrrs a7, fcsr, zero

[0x80004b74]:fsub.d t5, t3, s10, dyn
[0x80004b78]:csrrs a7, fcsr, zero
[0x80004b7c]:sw t5, 1744(ra)
[0x80004b80]:sw t6, 1752(ra)
[0x80004b84]:sw t5, 1760(ra)
[0x80004b88]:sw a7, 1768(ra)
[0x80004b8c]:lui a4, 1
[0x80004b90]:addi a4, a4, 2048
[0x80004b94]:add a6, a6, a4
[0x80004b98]:lw t3, 880(a6)
[0x80004b9c]:sub a6, a6, a4
[0x80004ba0]:lui a4, 1
[0x80004ba4]:addi a4, a4, 2048
[0x80004ba8]:add a6, a6, a4
[0x80004bac]:lw t4, 884(a6)
[0x80004bb0]:sub a6, a6, a4
[0x80004bb4]:lui a4, 1
[0x80004bb8]:addi a4, a4, 2048
[0x80004bbc]:add a6, a6, a4
[0x80004bc0]:lw s10, 888(a6)
[0x80004bc4]:sub a6, a6, a4
[0x80004bc8]:lui a4, 1
[0x80004bcc]:addi a4, a4, 2048
[0x80004bd0]:add a6, a6, a4
[0x80004bd4]:lw s11, 892(a6)
[0x80004bd8]:sub a6, a6, a4
[0x80004bdc]:lui t3, 955470
[0x80004be0]:addi t3, t3, 2812
[0x80004be4]:lui t4, 523791
[0x80004be8]:addi t4, t4, 2847
[0x80004bec]:lui s10, 955470
[0x80004bf0]:addi s10, s10, 2812
[0x80004bf4]:lui s11, 523791
[0x80004bf8]:addi s11, s11, 2847
[0x80004bfc]:addi a4, zero, 96
[0x80004c00]:csrrw zero, fcsr, a4
[0x80004c04]:fsub.d t5, t3, s10, dyn
[0x80004c08]:csrrs a7, fcsr, zero

[0x80004c04]:fsub.d t5, t3, s10, dyn
[0x80004c08]:csrrs a7, fcsr, zero
[0x80004c0c]:sw t5, 1776(ra)
[0x80004c10]:sw t6, 1784(ra)
[0x80004c14]:sw t5, 1792(ra)
[0x80004c18]:sw a7, 1800(ra)
[0x80004c1c]:lui a4, 1
[0x80004c20]:addi a4, a4, 2048
[0x80004c24]:add a6, a6, a4
[0x80004c28]:lw t3, 896(a6)
[0x80004c2c]:sub a6, a6, a4
[0x80004c30]:lui a4, 1
[0x80004c34]:addi a4, a4, 2048
[0x80004c38]:add a6, a6, a4
[0x80004c3c]:lw t4, 900(a6)
[0x80004c40]:sub a6, a6, a4
[0x80004c44]:lui a4, 1
[0x80004c48]:addi a4, a4, 2048
[0x80004c4c]:add a6, a6, a4
[0x80004c50]:lw s10, 904(a6)
[0x80004c54]:sub a6, a6, a4
[0x80004c58]:lui a4, 1
[0x80004c5c]:addi a4, a4, 2048
[0x80004c60]:add a6, a6, a4
[0x80004c64]:lw s11, 908(a6)
[0x80004c68]:sub a6, a6, a4
[0x80004c6c]:lui t3, 260840
[0x80004c70]:addi t3, t3, 1702
[0x80004c74]:lui t4, 523852
[0x80004c78]:addi t4, t4, 843
[0x80004c7c]:lui s10, 260840
[0x80004c80]:addi s10, s10, 1702
[0x80004c84]:lui s11, 523852
[0x80004c88]:addi s11, s11, 843
[0x80004c8c]:addi a4, zero, 96
[0x80004c90]:csrrw zero, fcsr, a4
[0x80004c94]:fsub.d t5, t3, s10, dyn
[0x80004c98]:csrrs a7, fcsr, zero

[0x80004c94]:fsub.d t5, t3, s10, dyn
[0x80004c98]:csrrs a7, fcsr, zero
[0x80004c9c]:sw t5, 1808(ra)
[0x80004ca0]:sw t6, 1816(ra)
[0x80004ca4]:sw t5, 1824(ra)
[0x80004ca8]:sw a7, 1832(ra)
[0x80004cac]:lui a4, 1
[0x80004cb0]:addi a4, a4, 2048
[0x80004cb4]:add a6, a6, a4
[0x80004cb8]:lw t3, 912(a6)
[0x80004cbc]:sub a6, a6, a4
[0x80004cc0]:lui a4, 1
[0x80004cc4]:addi a4, a4, 2048
[0x80004cc8]:add a6, a6, a4
[0x80004ccc]:lw t4, 916(a6)
[0x80004cd0]:sub a6, a6, a4
[0x80004cd4]:lui a4, 1
[0x80004cd8]:addi a4, a4, 2048
[0x80004cdc]:add a6, a6, a4
[0x80004ce0]:lw s10, 920(a6)
[0x80004ce4]:sub a6, a6, a4
[0x80004ce8]:lui a4, 1
[0x80004cec]:addi a4, a4, 2048
[0x80004cf0]:add a6, a6, a4
[0x80004cf4]:lw s11, 924(a6)
[0x80004cf8]:sub a6, a6, a4
[0x80004cfc]:lui t3, 403687
[0x80004d00]:addi t3, t3, 785
[0x80004d04]:lui t4, 523871
[0x80004d08]:addi t4, t4, 2026
[0x80004d0c]:lui s10, 403687
[0x80004d10]:addi s10, s10, 785
[0x80004d14]:lui s11, 523871
[0x80004d18]:addi s11, s11, 2026
[0x80004d1c]:addi a4, zero, 96
[0x80004d20]:csrrw zero, fcsr, a4
[0x80004d24]:fsub.d t5, t3, s10, dyn
[0x80004d28]:csrrs a7, fcsr, zero

[0x80004d24]:fsub.d t5, t3, s10, dyn
[0x80004d28]:csrrs a7, fcsr, zero
[0x80004d2c]:sw t5, 1840(ra)
[0x80004d30]:sw t6, 1848(ra)
[0x80004d34]:sw t5, 1856(ra)
[0x80004d38]:sw a7, 1864(ra)
[0x80004d3c]:lui a4, 1
[0x80004d40]:addi a4, a4, 2048
[0x80004d44]:add a6, a6, a4
[0x80004d48]:lw t3, 928(a6)
[0x80004d4c]:sub a6, a6, a4
[0x80004d50]:lui a4, 1
[0x80004d54]:addi a4, a4, 2048
[0x80004d58]:add a6, a6, a4
[0x80004d5c]:lw t4, 932(a6)
[0x80004d60]:sub a6, a6, a4
[0x80004d64]:lui a4, 1
[0x80004d68]:addi a4, a4, 2048
[0x80004d6c]:add a6, a6, a4
[0x80004d70]:lw s10, 936(a6)
[0x80004d74]:sub a6, a6, a4
[0x80004d78]:lui a4, 1
[0x80004d7c]:addi a4, a4, 2048
[0x80004d80]:add a6, a6, a4
[0x80004d84]:lw s11, 940(a6)
[0x80004d88]:sub a6, a6, a4
[0x80004d8c]:lui t3, 741408
[0x80004d90]:addi t3, t3, 3179
[0x80004d94]:lui t4, 523496
[0x80004d98]:addi t4, t4, 783
[0x80004d9c]:lui s10, 741408
[0x80004da0]:addi s10, s10, 3179
[0x80004da4]:lui s11, 523496
[0x80004da8]:addi s11, s11, 783
[0x80004dac]:addi a4, zero, 96
[0x80004db0]:csrrw zero, fcsr, a4
[0x80004db4]:fsub.d t5, t3, s10, dyn
[0x80004db8]:csrrs a7, fcsr, zero

[0x80004db4]:fsub.d t5, t3, s10, dyn
[0x80004db8]:csrrs a7, fcsr, zero
[0x80004dbc]:sw t5, 1872(ra)
[0x80004dc0]:sw t6, 1880(ra)
[0x80004dc4]:sw t5, 1888(ra)
[0x80004dc8]:sw a7, 1896(ra)
[0x80004dcc]:lui a4, 1
[0x80004dd0]:addi a4, a4, 2048
[0x80004dd4]:add a6, a6, a4
[0x80004dd8]:lw t3, 944(a6)
[0x80004ddc]:sub a6, a6, a4
[0x80004de0]:lui a4, 1
[0x80004de4]:addi a4, a4, 2048
[0x80004de8]:add a6, a6, a4
[0x80004dec]:lw t4, 948(a6)
[0x80004df0]:sub a6, a6, a4
[0x80004df4]:lui a4, 1
[0x80004df8]:addi a4, a4, 2048
[0x80004dfc]:add a6, a6, a4
[0x80004e00]:lw s10, 952(a6)
[0x80004e04]:sub a6, a6, a4
[0x80004e08]:lui a4, 1
[0x80004e0c]:addi a4, a4, 2048
[0x80004e10]:add a6, a6, a4
[0x80004e14]:lw s11, 956(a6)
[0x80004e18]:sub a6, a6, a4
[0x80004e1c]:lui t3, 555632
[0x80004e20]:addi t3, t3, 3659
[0x80004e24]:lui t4, 523754
[0x80004e28]:addi t4, t4, 1305
[0x80004e2c]:lui s10, 555632
[0x80004e30]:addi s10, s10, 3659
[0x80004e34]:lui s11, 523754
[0x80004e38]:addi s11, s11, 1305
[0x80004e3c]:addi a4, zero, 96
[0x80004e40]:csrrw zero, fcsr, a4
[0x80004e44]:fsub.d t5, t3, s10, dyn
[0x80004e48]:csrrs a7, fcsr, zero

[0x80004e44]:fsub.d t5, t3, s10, dyn
[0x80004e48]:csrrs a7, fcsr, zero
[0x80004e4c]:sw t5, 1904(ra)
[0x80004e50]:sw t6, 1912(ra)
[0x80004e54]:sw t5, 1920(ra)
[0x80004e58]:sw a7, 1928(ra)
[0x80004e5c]:lui a4, 1
[0x80004e60]:addi a4, a4, 2048
[0x80004e64]:add a6, a6, a4
[0x80004e68]:lw t3, 960(a6)
[0x80004e6c]:sub a6, a6, a4
[0x80004e70]:lui a4, 1
[0x80004e74]:addi a4, a4, 2048
[0x80004e78]:add a6, a6, a4
[0x80004e7c]:lw t4, 964(a6)
[0x80004e80]:sub a6, a6, a4
[0x80004e84]:lui a4, 1
[0x80004e88]:addi a4, a4, 2048
[0x80004e8c]:add a6, a6, a4
[0x80004e90]:lw s10, 968(a6)
[0x80004e94]:sub a6, a6, a4
[0x80004e98]:lui a4, 1
[0x80004e9c]:addi a4, a4, 2048
[0x80004ea0]:add a6, a6, a4
[0x80004ea4]:lw s11, 972(a6)
[0x80004ea8]:sub a6, a6, a4
[0x80004eac]:lui t3, 633633
[0x80004eb0]:addi t3, t3, 927
[0x80004eb4]:lui t4, 523686
[0x80004eb8]:addi t4, t4, 573
[0x80004ebc]:lui s10, 633633
[0x80004ec0]:addi s10, s10, 927
[0x80004ec4]:lui s11, 523686
[0x80004ec8]:addi s11, s11, 573
[0x80004ecc]:addi a4, zero, 96
[0x80004ed0]:csrrw zero, fcsr, a4
[0x80004ed4]:fsub.d t5, t3, s10, dyn
[0x80004ed8]:csrrs a7, fcsr, zero

[0x80004ed4]:fsub.d t5, t3, s10, dyn
[0x80004ed8]:csrrs a7, fcsr, zero
[0x80004edc]:sw t5, 1936(ra)
[0x80004ee0]:sw t6, 1944(ra)
[0x80004ee4]:sw t5, 1952(ra)
[0x80004ee8]:sw a7, 1960(ra)
[0x80004eec]:lui a4, 1
[0x80004ef0]:addi a4, a4, 2048
[0x80004ef4]:add a6, a6, a4
[0x80004ef8]:lw t3, 976(a6)
[0x80004efc]:sub a6, a6, a4
[0x80004f00]:lui a4, 1
[0x80004f04]:addi a4, a4, 2048
[0x80004f08]:add a6, a6, a4
[0x80004f0c]:lw t4, 980(a6)
[0x80004f10]:sub a6, a6, a4
[0x80004f14]:lui a4, 1
[0x80004f18]:addi a4, a4, 2048
[0x80004f1c]:add a6, a6, a4
[0x80004f20]:lw s10, 984(a6)
[0x80004f24]:sub a6, a6, a4
[0x80004f28]:lui a4, 1
[0x80004f2c]:addi a4, a4, 2048
[0x80004f30]:add a6, a6, a4
[0x80004f34]:lw s11, 988(a6)
[0x80004f38]:sub a6, a6, a4
[0x80004f3c]:lui t3, 1044123
[0x80004f40]:addi t3, t3, 688
[0x80004f44]:lui t4, 523984
[0x80004f48]:addi t4, t4, 1445
[0x80004f4c]:lui s10, 1044123
[0x80004f50]:addi s10, s10, 688
[0x80004f54]:lui s11, 523984
[0x80004f58]:addi s11, s11, 1445
[0x80004f5c]:addi a4, zero, 96
[0x80004f60]:csrrw zero, fcsr, a4
[0x80004f64]:fsub.d t5, t3, s10, dyn
[0x80004f68]:csrrs a7, fcsr, zero

[0x80004f64]:fsub.d t5, t3, s10, dyn
[0x80004f68]:csrrs a7, fcsr, zero
[0x80004f6c]:sw t5, 1968(ra)
[0x80004f70]:sw t6, 1976(ra)
[0x80004f74]:sw t5, 1984(ra)
[0x80004f78]:sw a7, 1992(ra)
[0x80004f7c]:lui a4, 1
[0x80004f80]:addi a4, a4, 2048
[0x80004f84]:add a6, a6, a4
[0x80004f88]:lw t3, 992(a6)
[0x80004f8c]:sub a6, a6, a4
[0x80004f90]:lui a4, 1
[0x80004f94]:addi a4, a4, 2048
[0x80004f98]:add a6, a6, a4
[0x80004f9c]:lw t4, 996(a6)
[0x80004fa0]:sub a6, a6, a4
[0x80004fa4]:lui a4, 1
[0x80004fa8]:addi a4, a4, 2048
[0x80004fac]:add a6, a6, a4
[0x80004fb0]:lw s10, 1000(a6)
[0x80004fb4]:sub a6, a6, a4
[0x80004fb8]:lui a4, 1
[0x80004fbc]:addi a4, a4, 2048
[0x80004fc0]:add a6, a6, a4
[0x80004fc4]:lw s11, 1004(a6)
[0x80004fc8]:sub a6, a6, a4
[0x80004fcc]:lui t3, 883830
[0x80004fd0]:addi t3, t3, 359
[0x80004fd4]:lui t4, 523546
[0x80004fd8]:addi t4, t4, 3402
[0x80004fdc]:lui s10, 883830
[0x80004fe0]:addi s10, s10, 359
[0x80004fe4]:lui s11, 523546
[0x80004fe8]:addi s11, s11, 3402
[0x80004fec]:addi a4, zero, 96
[0x80004ff0]:csrrw zero, fcsr, a4
[0x80004ff4]:fsub.d t5, t3, s10, dyn
[0x80004ff8]:csrrs a7, fcsr, zero

[0x80004ff4]:fsub.d t5, t3, s10, dyn
[0x80004ff8]:csrrs a7, fcsr, zero
[0x80004ffc]:sw t5, 2000(ra)
[0x80005000]:sw t6, 2008(ra)
[0x80005004]:sw t5, 2016(ra)
[0x80005008]:sw a7, 2024(ra)
[0x8000500c]:lui a4, 1
[0x80005010]:addi a4, a4, 2048
[0x80005014]:add a6, a6, a4
[0x80005018]:lw t3, 1008(a6)
[0x8000501c]:sub a6, a6, a4
[0x80005020]:lui a4, 1
[0x80005024]:addi a4, a4, 2048
[0x80005028]:add a6, a6, a4
[0x8000502c]:lw t4, 1012(a6)
[0x80005030]:sub a6, a6, a4
[0x80005034]:lui a4, 1
[0x80005038]:addi a4, a4, 2048
[0x8000503c]:add a6, a6, a4
[0x80005040]:lw s10, 1016(a6)
[0x80005044]:sub a6, a6, a4
[0x80005048]:lui a4, 1
[0x8000504c]:addi a4, a4, 2048
[0x80005050]:add a6, a6, a4
[0x80005054]:lw s11, 1020(a6)
[0x80005058]:sub a6, a6, a4
[0x8000505c]:lui t3, 503737
[0x80005060]:addi t3, t3, 3391
[0x80005064]:lui t4, 522891
[0x80005068]:addi t4, t4, 3410
[0x8000506c]:lui s10, 503737
[0x80005070]:addi s10, s10, 3391
[0x80005074]:lui s11, 522891
[0x80005078]:addi s11, s11, 3410
[0x8000507c]:addi a4, zero, 96
[0x80005080]:csrrw zero, fcsr, a4
[0x80005084]:fsub.d t5, t3, s10, dyn
[0x80005088]:csrrs a7, fcsr, zero

[0x80005084]:fsub.d t5, t3, s10, dyn
[0x80005088]:csrrs a7, fcsr, zero
[0x8000508c]:sw t5, 2032(ra)
[0x80005090]:sw t6, 2040(ra)
[0x80005094]:addi ra, ra, 2040
[0x80005098]:sw t5, 8(ra)
[0x8000509c]:sw a7, 16(ra)
[0x800050a0]:lui a4, 1
[0x800050a4]:addi a4, a4, 2048
[0x800050a8]:add a6, a6, a4
[0x800050ac]:lw t3, 1024(a6)
[0x800050b0]:sub a6, a6, a4
[0x800050b4]:lui a4, 1
[0x800050b8]:addi a4, a4, 2048
[0x800050bc]:add a6, a6, a4
[0x800050c0]:lw t4, 1028(a6)
[0x800050c4]:sub a6, a6, a4
[0x800050c8]:lui a4, 1
[0x800050cc]:addi a4, a4, 2048
[0x800050d0]:add a6, a6, a4
[0x800050d4]:lw s10, 1032(a6)
[0x800050d8]:sub a6, a6, a4
[0x800050dc]:lui a4, 1
[0x800050e0]:addi a4, a4, 2048
[0x800050e4]:add a6, a6, a4
[0x800050e8]:lw s11, 1036(a6)
[0x800050ec]:sub a6, a6, a4
[0x800050f0]:lui t3, 305484
[0x800050f4]:addi t3, t3, 1660
[0x800050f8]:lui t4, 523782
[0x800050fc]:addi t4, t4, 4039
[0x80005100]:lui s10, 305484
[0x80005104]:addi s10, s10, 1660
[0x80005108]:lui s11, 523782
[0x8000510c]:addi s11, s11, 4039
[0x80005110]:addi a4, zero, 96
[0x80005114]:csrrw zero, fcsr, a4
[0x80005118]:fsub.d t5, t3, s10, dyn
[0x8000511c]:csrrs a7, fcsr, zero

[0x80005118]:fsub.d t5, t3, s10, dyn
[0x8000511c]:csrrs a7, fcsr, zero
[0x80005120]:sw t5, 24(ra)
[0x80005124]:sw t6, 32(ra)
[0x80005128]:sw t5, 40(ra)
[0x8000512c]:sw a7, 48(ra)
[0x80005130]:lui a4, 1
[0x80005134]:addi a4, a4, 2048
[0x80005138]:add a6, a6, a4
[0x8000513c]:lw t3, 1040(a6)
[0x80005140]:sub a6, a6, a4
[0x80005144]:lui a4, 1
[0x80005148]:addi a4, a4, 2048
[0x8000514c]:add a6, a6, a4
[0x80005150]:lw t4, 1044(a6)
[0x80005154]:sub a6, a6, a4
[0x80005158]:lui a4, 1
[0x8000515c]:addi a4, a4, 2048
[0x80005160]:add a6, a6, a4
[0x80005164]:lw s10, 1048(a6)
[0x80005168]:sub a6, a6, a4
[0x8000516c]:lui a4, 1
[0x80005170]:addi a4, a4, 2048
[0x80005174]:add a6, a6, a4
[0x80005178]:lw s11, 1052(a6)
[0x8000517c]:sub a6, a6, a4
[0x80005180]:lui t3, 295674
[0x80005184]:addi t3, t3, 1235
[0x80005188]:lui t4, 523335
[0x8000518c]:addi t4, t4, 2416
[0x80005190]:lui s10, 295674
[0x80005194]:addi s10, s10, 1235
[0x80005198]:lui s11, 523335
[0x8000519c]:addi s11, s11, 2416
[0x800051a0]:addi a4, zero, 96
[0x800051a4]:csrrw zero, fcsr, a4
[0x800051a8]:fsub.d t5, t3, s10, dyn
[0x800051ac]:csrrs a7, fcsr, zero

[0x800051a8]:fsub.d t5, t3, s10, dyn
[0x800051ac]:csrrs a7, fcsr, zero
[0x800051b0]:sw t5, 56(ra)
[0x800051b4]:sw t6, 64(ra)
[0x800051b8]:sw t5, 72(ra)
[0x800051bc]:sw a7, 80(ra)
[0x800051c0]:lui a4, 1
[0x800051c4]:addi a4, a4, 2048
[0x800051c8]:add a6, a6, a4
[0x800051cc]:lw t3, 1056(a6)
[0x800051d0]:sub a6, a6, a4
[0x800051d4]:lui a4, 1
[0x800051d8]:addi a4, a4, 2048
[0x800051dc]:add a6, a6, a4
[0x800051e0]:lw t4, 1060(a6)
[0x800051e4]:sub a6, a6, a4
[0x800051e8]:lui a4, 1
[0x800051ec]:addi a4, a4, 2048
[0x800051f0]:add a6, a6, a4
[0x800051f4]:lw s10, 1064(a6)
[0x800051f8]:sub a6, a6, a4
[0x800051fc]:lui a4, 1
[0x80005200]:addi a4, a4, 2048
[0x80005204]:add a6, a6, a4
[0x80005208]:lw s11, 1068(a6)
[0x8000520c]:sub a6, a6, a4
[0x80005210]:lui t3, 298252
[0x80005214]:addi t3, t3, 3209
[0x80005218]:lui t4, 523972
[0x8000521c]:addi t4, t4, 413
[0x80005220]:lui s10, 298252
[0x80005224]:addi s10, s10, 3209
[0x80005228]:lui s11, 523972
[0x8000522c]:addi s11, s11, 413
[0x80005230]:addi a4, zero, 96
[0x80005234]:csrrw zero, fcsr, a4
[0x80005238]:fsub.d t5, t3, s10, dyn
[0x8000523c]:csrrs a7, fcsr, zero

[0x80005238]:fsub.d t5, t3, s10, dyn
[0x8000523c]:csrrs a7, fcsr, zero
[0x80005240]:sw t5, 88(ra)
[0x80005244]:sw t6, 96(ra)
[0x80005248]:sw t5, 104(ra)
[0x8000524c]:sw a7, 112(ra)
[0x80005250]:lui a4, 1
[0x80005254]:addi a4, a4, 2048
[0x80005258]:add a6, a6, a4
[0x8000525c]:lw t3, 1072(a6)
[0x80005260]:sub a6, a6, a4
[0x80005264]:lui a4, 1
[0x80005268]:addi a4, a4, 2048
[0x8000526c]:add a6, a6, a4
[0x80005270]:lw t4, 1076(a6)
[0x80005274]:sub a6, a6, a4
[0x80005278]:lui a4, 1
[0x8000527c]:addi a4, a4, 2048
[0x80005280]:add a6, a6, a4
[0x80005284]:lw s10, 1080(a6)
[0x80005288]:sub a6, a6, a4
[0x8000528c]:lui a4, 1
[0x80005290]:addi a4, a4, 2048
[0x80005294]:add a6, a6, a4
[0x80005298]:lw s11, 1084(a6)
[0x8000529c]:sub a6, a6, a4
[0x800052a0]:lui t3, 234284
[0x800052a4]:addi t3, t3, 378
[0x800052a8]:lui t4, 523875
[0x800052ac]:addi t4, t4, 2413
[0x800052b0]:lui s10, 234284
[0x800052b4]:addi s10, s10, 378
[0x800052b8]:lui s11, 523875
[0x800052bc]:addi s11, s11, 2413
[0x800052c0]:addi a4, zero, 96
[0x800052c4]:csrrw zero, fcsr, a4
[0x800052c8]:fsub.d t5, t3, s10, dyn
[0x800052cc]:csrrs a7, fcsr, zero

[0x800052c8]:fsub.d t5, t3, s10, dyn
[0x800052cc]:csrrs a7, fcsr, zero
[0x800052d0]:sw t5, 120(ra)
[0x800052d4]:sw t6, 128(ra)
[0x800052d8]:sw t5, 136(ra)
[0x800052dc]:sw a7, 144(ra)
[0x800052e0]:lui a4, 1
[0x800052e4]:addi a4, a4, 2048
[0x800052e8]:add a6, a6, a4
[0x800052ec]:lw t3, 1088(a6)
[0x800052f0]:sub a6, a6, a4
[0x800052f4]:lui a4, 1
[0x800052f8]:addi a4, a4, 2048
[0x800052fc]:add a6, a6, a4
[0x80005300]:lw t4, 1092(a6)
[0x80005304]:sub a6, a6, a4
[0x80005308]:lui a4, 1
[0x8000530c]:addi a4, a4, 2048
[0x80005310]:add a6, a6, a4
[0x80005314]:lw s10, 1096(a6)
[0x80005318]:sub a6, a6, a4
[0x8000531c]:lui a4, 1
[0x80005320]:addi a4, a4, 2048
[0x80005324]:add a6, a6, a4
[0x80005328]:lw s11, 1100(a6)
[0x8000532c]:sub a6, a6, a4
[0x80005330]:lui t3, 496933
[0x80005334]:addi t3, t3, 2357
[0x80005338]:lui t4, 523645
[0x8000533c]:addi t4, t4, 2183
[0x80005340]:lui s10, 496933
[0x80005344]:addi s10, s10, 2357
[0x80005348]:lui s11, 523645
[0x8000534c]:addi s11, s11, 2183
[0x80005350]:addi a4, zero, 96
[0x80005354]:csrrw zero, fcsr, a4
[0x80005358]:fsub.d t5, t3, s10, dyn
[0x8000535c]:csrrs a7, fcsr, zero

[0x80005358]:fsub.d t5, t3, s10, dyn
[0x8000535c]:csrrs a7, fcsr, zero
[0x80005360]:sw t5, 152(ra)
[0x80005364]:sw t6, 160(ra)
[0x80005368]:sw t5, 168(ra)
[0x8000536c]:sw a7, 176(ra)
[0x80005370]:lui a4, 1
[0x80005374]:addi a4, a4, 2048
[0x80005378]:add a6, a6, a4
[0x8000537c]:lw t3, 1104(a6)
[0x80005380]:sub a6, a6, a4
[0x80005384]:lui a4, 1
[0x80005388]:addi a4, a4, 2048
[0x8000538c]:add a6, a6, a4
[0x80005390]:lw t4, 1108(a6)
[0x80005394]:sub a6, a6, a4
[0x80005398]:lui a4, 1
[0x8000539c]:addi a4, a4, 2048
[0x800053a0]:add a6, a6, a4
[0x800053a4]:lw s10, 1112(a6)
[0x800053a8]:sub a6, a6, a4
[0x800053ac]:lui a4, 1
[0x800053b0]:addi a4, a4, 2048
[0x800053b4]:add a6, a6, a4
[0x800053b8]:lw s11, 1116(a6)
[0x800053bc]:sub a6, a6, a4
[0x800053c0]:lui t3, 761279
[0x800053c4]:addi t3, t3, 3802
[0x800053c8]:lui t4, 523897
[0x800053cc]:addi t4, t4, 188
[0x800053d0]:lui s10, 761279
[0x800053d4]:addi s10, s10, 3802
[0x800053d8]:lui s11, 523897
[0x800053dc]:addi s11, s11, 188
[0x800053e0]:addi a4, zero, 96
[0x800053e4]:csrrw zero, fcsr, a4
[0x800053e8]:fsub.d t5, t3, s10, dyn
[0x800053ec]:csrrs a7, fcsr, zero

[0x800053e8]:fsub.d t5, t3, s10, dyn
[0x800053ec]:csrrs a7, fcsr, zero
[0x800053f0]:sw t5, 184(ra)
[0x800053f4]:sw t6, 192(ra)
[0x800053f8]:sw t5, 200(ra)
[0x800053fc]:sw a7, 208(ra)
[0x80005400]:lui a4, 1
[0x80005404]:addi a4, a4, 2048
[0x80005408]:add a6, a6, a4
[0x8000540c]:lw t3, 1120(a6)
[0x80005410]:sub a6, a6, a4
[0x80005414]:lui a4, 1
[0x80005418]:addi a4, a4, 2048
[0x8000541c]:add a6, a6, a4
[0x80005420]:lw t4, 1124(a6)
[0x80005424]:sub a6, a6, a4
[0x80005428]:lui a4, 1
[0x8000542c]:addi a4, a4, 2048
[0x80005430]:add a6, a6, a4
[0x80005434]:lw s10, 1128(a6)
[0x80005438]:sub a6, a6, a4
[0x8000543c]:lui a4, 1
[0x80005440]:addi a4, a4, 2048
[0x80005444]:add a6, a6, a4
[0x80005448]:lw s11, 1132(a6)
[0x8000544c]:sub a6, a6, a4
[0x80005450]:lui t3, 329699
[0x80005454]:addi t3, t3, 932
[0x80005458]:lui t4, 523958
[0x8000545c]:addi t4, t4, 1695
[0x80005460]:lui s10, 329699
[0x80005464]:addi s10, s10, 932
[0x80005468]:lui s11, 523958
[0x8000546c]:addi s11, s11, 1695
[0x80005470]:addi a4, zero, 96
[0x80005474]:csrrw zero, fcsr, a4
[0x80005478]:fsub.d t5, t3, s10, dyn
[0x8000547c]:csrrs a7, fcsr, zero

[0x80005478]:fsub.d t5, t3, s10, dyn
[0x8000547c]:csrrs a7, fcsr, zero
[0x80005480]:sw t5, 216(ra)
[0x80005484]:sw t6, 224(ra)
[0x80005488]:sw t5, 232(ra)
[0x8000548c]:sw a7, 240(ra)
[0x80005490]:lui a4, 1
[0x80005494]:addi a4, a4, 2048
[0x80005498]:add a6, a6, a4
[0x8000549c]:lw t3, 1136(a6)
[0x800054a0]:sub a6, a6, a4
[0x800054a4]:lui a4, 1
[0x800054a8]:addi a4, a4, 2048
[0x800054ac]:add a6, a6, a4
[0x800054b0]:lw t4, 1140(a6)
[0x800054b4]:sub a6, a6, a4
[0x800054b8]:lui a4, 1
[0x800054bc]:addi a4, a4, 2048
[0x800054c0]:add a6, a6, a4
[0x800054c4]:lw s10, 1144(a6)
[0x800054c8]:sub a6, a6, a4
[0x800054cc]:lui a4, 1
[0x800054d0]:addi a4, a4, 2048
[0x800054d4]:add a6, a6, a4
[0x800054d8]:lw s11, 1148(a6)
[0x800054dc]:sub a6, a6, a4
[0x800054e0]:lui t3, 221413
[0x800054e4]:addi t3, t3, 2363
[0x800054e8]:lui t4, 524012
[0x800054ec]:addi t4, t4, 2295
[0x800054f0]:lui s10, 221413
[0x800054f4]:addi s10, s10, 2363
[0x800054f8]:lui s11, 524012
[0x800054fc]:addi s11, s11, 2295
[0x80005500]:addi a4, zero, 96
[0x80005504]:csrrw zero, fcsr, a4
[0x80005508]:fsub.d t5, t3, s10, dyn
[0x8000550c]:csrrs a7, fcsr, zero

[0x80005508]:fsub.d t5, t3, s10, dyn
[0x8000550c]:csrrs a7, fcsr, zero
[0x80005510]:sw t5, 248(ra)
[0x80005514]:sw t6, 256(ra)
[0x80005518]:sw t5, 264(ra)
[0x8000551c]:sw a7, 272(ra)
[0x80005520]:lui a4, 1
[0x80005524]:addi a4, a4, 2048
[0x80005528]:add a6, a6, a4
[0x8000552c]:lw t3, 1152(a6)
[0x80005530]:sub a6, a6, a4
[0x80005534]:lui a4, 1
[0x80005538]:addi a4, a4, 2048
[0x8000553c]:add a6, a6, a4
[0x80005540]:lw t4, 1156(a6)
[0x80005544]:sub a6, a6, a4
[0x80005548]:lui a4, 1
[0x8000554c]:addi a4, a4, 2048
[0x80005550]:add a6, a6, a4
[0x80005554]:lw s10, 1160(a6)
[0x80005558]:sub a6, a6, a4
[0x8000555c]:lui a4, 1
[0x80005560]:addi a4, a4, 2048
[0x80005564]:add a6, a6, a4
[0x80005568]:lw s11, 1164(a6)
[0x8000556c]:sub a6, a6, a4
[0x80005570]:lui t3, 403425
[0x80005574]:addi t3, t3, 599
[0x80005578]:lui t4, 523369
[0x8000557c]:addi t4, t4, 53
[0x80005580]:lui s10, 403425
[0x80005584]:addi s10, s10, 599
[0x80005588]:lui s11, 523369
[0x8000558c]:addi s11, s11, 53
[0x80005590]:addi a4, zero, 96
[0x80005594]:csrrw zero, fcsr, a4
[0x80005598]:fsub.d t5, t3, s10, dyn
[0x8000559c]:csrrs a7, fcsr, zero

[0x80005598]:fsub.d t5, t3, s10, dyn
[0x8000559c]:csrrs a7, fcsr, zero
[0x800055a0]:sw t5, 280(ra)
[0x800055a4]:sw t6, 288(ra)
[0x800055a8]:sw t5, 296(ra)
[0x800055ac]:sw a7, 304(ra)
[0x800055b0]:lui a4, 1
[0x800055b4]:addi a4, a4, 2048
[0x800055b8]:add a6, a6, a4
[0x800055bc]:lw t3, 1168(a6)
[0x800055c0]:sub a6, a6, a4
[0x800055c4]:lui a4, 1
[0x800055c8]:addi a4, a4, 2048
[0x800055cc]:add a6, a6, a4
[0x800055d0]:lw t4, 1172(a6)
[0x800055d4]:sub a6, a6, a4
[0x800055d8]:lui a4, 1
[0x800055dc]:addi a4, a4, 2048
[0x800055e0]:add a6, a6, a4
[0x800055e4]:lw s10, 1176(a6)
[0x800055e8]:sub a6, a6, a4
[0x800055ec]:lui a4, 1
[0x800055f0]:addi a4, a4, 2048
[0x800055f4]:add a6, a6, a4
[0x800055f8]:lw s11, 1180(a6)
[0x800055fc]:sub a6, a6, a4
[0x80005600]:lui t3, 913683
[0x80005604]:addi t3, t3, 3323
[0x80005608]:lui t4, 523454
[0x8000560c]:addi t4, t4, 2797
[0x80005610]:lui s10, 913683
[0x80005614]:addi s10, s10, 3323
[0x80005618]:lui s11, 523454
[0x8000561c]:addi s11, s11, 2797
[0x80005620]:addi a4, zero, 96
[0x80005624]:csrrw zero, fcsr, a4
[0x80005628]:fsub.d t5, t3, s10, dyn
[0x8000562c]:csrrs a7, fcsr, zero

[0x80005628]:fsub.d t5, t3, s10, dyn
[0x8000562c]:csrrs a7, fcsr, zero
[0x80005630]:sw t5, 312(ra)
[0x80005634]:sw t6, 320(ra)
[0x80005638]:sw t5, 328(ra)
[0x8000563c]:sw a7, 336(ra)
[0x80005640]:lui a4, 1
[0x80005644]:addi a4, a4, 2048
[0x80005648]:add a6, a6, a4
[0x8000564c]:lw t3, 1184(a6)
[0x80005650]:sub a6, a6, a4
[0x80005654]:lui a4, 1
[0x80005658]:addi a4, a4, 2048
[0x8000565c]:add a6, a6, a4
[0x80005660]:lw t4, 1188(a6)
[0x80005664]:sub a6, a6, a4
[0x80005668]:lui a4, 1
[0x8000566c]:addi a4, a4, 2048
[0x80005670]:add a6, a6, a4
[0x80005674]:lw s10, 1192(a6)
[0x80005678]:sub a6, a6, a4
[0x8000567c]:lui a4, 1
[0x80005680]:addi a4, a4, 2048
[0x80005684]:add a6, a6, a4
[0x80005688]:lw s11, 1196(a6)
[0x8000568c]:sub a6, a6, a4
[0x80005690]:lui t3, 176657
[0x80005694]:addi t3, t3, 2821
[0x80005698]:lui t4, 523908
[0x8000569c]:addi t4, t4, 3965
[0x800056a0]:lui s10, 176657
[0x800056a4]:addi s10, s10, 2821
[0x800056a8]:lui s11, 523908
[0x800056ac]:addi s11, s11, 3965
[0x800056b0]:addi a4, zero, 96
[0x800056b4]:csrrw zero, fcsr, a4
[0x800056b8]:fsub.d t5, t3, s10, dyn
[0x800056bc]:csrrs a7, fcsr, zero

[0x800056b8]:fsub.d t5, t3, s10, dyn
[0x800056bc]:csrrs a7, fcsr, zero
[0x800056c0]:sw t5, 344(ra)
[0x800056c4]:sw t6, 352(ra)
[0x800056c8]:sw t5, 360(ra)
[0x800056cc]:sw a7, 368(ra)
[0x800056d0]:lui a4, 1
[0x800056d4]:addi a4, a4, 2048
[0x800056d8]:add a6, a6, a4
[0x800056dc]:lw t3, 1200(a6)
[0x800056e0]:sub a6, a6, a4
[0x800056e4]:lui a4, 1
[0x800056e8]:addi a4, a4, 2048
[0x800056ec]:add a6, a6, a4
[0x800056f0]:lw t4, 1204(a6)
[0x800056f4]:sub a6, a6, a4
[0x800056f8]:lui a4, 1
[0x800056fc]:addi a4, a4, 2048
[0x80005700]:add a6, a6, a4
[0x80005704]:lw s10, 1208(a6)
[0x80005708]:sub a6, a6, a4
[0x8000570c]:lui a4, 1
[0x80005710]:addi a4, a4, 2048
[0x80005714]:add a6, a6, a4
[0x80005718]:lw s11, 1212(a6)
[0x8000571c]:sub a6, a6, a4
[0x80005720]:lui t3, 268593
[0x80005724]:addi t3, t3, 3526
[0x80005728]:lui t4, 523869
[0x8000572c]:addi t4, t4, 4087
[0x80005730]:lui s10, 268593
[0x80005734]:addi s10, s10, 3526
[0x80005738]:lui s11, 523869
[0x8000573c]:addi s11, s11, 4087
[0x80005740]:addi a4, zero, 96
[0x80005744]:csrrw zero, fcsr, a4
[0x80005748]:fsub.d t5, t3, s10, dyn
[0x8000574c]:csrrs a7, fcsr, zero

[0x80005748]:fsub.d t5, t3, s10, dyn
[0x8000574c]:csrrs a7, fcsr, zero
[0x80005750]:sw t5, 376(ra)
[0x80005754]:sw t6, 384(ra)
[0x80005758]:sw t5, 392(ra)
[0x8000575c]:sw a7, 400(ra)
[0x80005760]:lui a4, 1
[0x80005764]:addi a4, a4, 2048
[0x80005768]:add a6, a6, a4
[0x8000576c]:lw t3, 1216(a6)
[0x80005770]:sub a6, a6, a4
[0x80005774]:lui a4, 1
[0x80005778]:addi a4, a4, 2048
[0x8000577c]:add a6, a6, a4
[0x80005780]:lw t4, 1220(a6)
[0x80005784]:sub a6, a6, a4
[0x80005788]:lui a4, 1
[0x8000578c]:addi a4, a4, 2048
[0x80005790]:add a6, a6, a4
[0x80005794]:lw s10, 1224(a6)
[0x80005798]:sub a6, a6, a4
[0x8000579c]:lui a4, 1
[0x800057a0]:addi a4, a4, 2048
[0x800057a4]:add a6, a6, a4
[0x800057a8]:lw s11, 1228(a6)
[0x800057ac]:sub a6, a6, a4
[0x800057b0]:lui t3, 241958
[0x800057b4]:addi t3, t3, 601
[0x800057b8]:lui t4, 523620
[0x800057bc]:addi t4, t4, 1364
[0x800057c0]:lui s10, 241958
[0x800057c4]:addi s10, s10, 601
[0x800057c8]:lui s11, 523620
[0x800057cc]:addi s11, s11, 1364
[0x800057d0]:addi a4, zero, 96
[0x800057d4]:csrrw zero, fcsr, a4
[0x800057d8]:fsub.d t5, t3, s10, dyn
[0x800057dc]:csrrs a7, fcsr, zero

[0x800057d8]:fsub.d t5, t3, s10, dyn
[0x800057dc]:csrrs a7, fcsr, zero
[0x800057e0]:sw t5, 408(ra)
[0x800057e4]:sw t6, 416(ra)
[0x800057e8]:sw t5, 424(ra)
[0x800057ec]:sw a7, 432(ra)
[0x800057f0]:lui a4, 1
[0x800057f4]:addi a4, a4, 2048
[0x800057f8]:add a6, a6, a4
[0x800057fc]:lw t3, 1232(a6)
[0x80005800]:sub a6, a6, a4
[0x80005804]:lui a4, 1
[0x80005808]:addi a4, a4, 2048
[0x8000580c]:add a6, a6, a4
[0x80005810]:lw t4, 1236(a6)
[0x80005814]:sub a6, a6, a4
[0x80005818]:lui a4, 1
[0x8000581c]:addi a4, a4, 2048
[0x80005820]:add a6, a6, a4
[0x80005824]:lw s10, 1240(a6)
[0x80005828]:sub a6, a6, a4
[0x8000582c]:lui a4, 1
[0x80005830]:addi a4, a4, 2048
[0x80005834]:add a6, a6, a4
[0x80005838]:lw s11, 1244(a6)
[0x8000583c]:sub a6, a6, a4
[0x80005840]:lui t3, 101202
[0x80005844]:addi t3, t3, 779
[0x80005848]:lui t4, 523346
[0x8000584c]:addi t4, t4, 2075
[0x80005850]:lui s10, 101202
[0x80005854]:addi s10, s10, 779
[0x80005858]:lui s11, 523346
[0x8000585c]:addi s11, s11, 2075
[0x80005860]:addi a4, zero, 96
[0x80005864]:csrrw zero, fcsr, a4
[0x80005868]:fsub.d t5, t3, s10, dyn
[0x8000586c]:csrrs a7, fcsr, zero

[0x80005868]:fsub.d t5, t3, s10, dyn
[0x8000586c]:csrrs a7, fcsr, zero
[0x80005870]:sw t5, 440(ra)
[0x80005874]:sw t6, 448(ra)
[0x80005878]:sw t5, 456(ra)
[0x8000587c]:sw a7, 464(ra)
[0x80005880]:lui a4, 1
[0x80005884]:addi a4, a4, 2048
[0x80005888]:add a6, a6, a4
[0x8000588c]:lw t3, 1248(a6)
[0x80005890]:sub a6, a6, a4
[0x80005894]:lui a4, 1
[0x80005898]:addi a4, a4, 2048
[0x8000589c]:add a6, a6, a4
[0x800058a0]:lw t4, 1252(a6)
[0x800058a4]:sub a6, a6, a4
[0x800058a8]:lui a4, 1
[0x800058ac]:addi a4, a4, 2048
[0x800058b0]:add a6, a6, a4
[0x800058b4]:lw s10, 1256(a6)
[0x800058b8]:sub a6, a6, a4
[0x800058bc]:lui a4, 1
[0x800058c0]:addi a4, a4, 2048
[0x800058c4]:add a6, a6, a4
[0x800058c8]:lw s11, 1260(a6)
[0x800058cc]:sub a6, a6, a4
[0x800058d0]:lui t3, 923841
[0x800058d4]:addi t3, t3, 239
[0x800058d8]:lui t4, 522829
[0x800058dc]:addi t4, t4, 1988
[0x800058e0]:lui s10, 923841
[0x800058e4]:addi s10, s10, 239
[0x800058e8]:lui s11, 522829
[0x800058ec]:addi s11, s11, 1988
[0x800058f0]:addi a4, zero, 96
[0x800058f4]:csrrw zero, fcsr, a4
[0x800058f8]:fsub.d t5, t3, s10, dyn
[0x800058fc]:csrrs a7, fcsr, zero

[0x800058f8]:fsub.d t5, t3, s10, dyn
[0x800058fc]:csrrs a7, fcsr, zero
[0x80005900]:sw t5, 472(ra)
[0x80005904]:sw t6, 480(ra)
[0x80005908]:sw t5, 488(ra)
[0x8000590c]:sw a7, 496(ra)
[0x80005910]:lui a4, 1
[0x80005914]:addi a4, a4, 2048
[0x80005918]:add a6, a6, a4
[0x8000591c]:lw t3, 1264(a6)
[0x80005920]:sub a6, a6, a4
[0x80005924]:lui a4, 1
[0x80005928]:addi a4, a4, 2048
[0x8000592c]:add a6, a6, a4
[0x80005930]:lw t4, 1268(a6)
[0x80005934]:sub a6, a6, a4
[0x80005938]:lui a4, 1
[0x8000593c]:addi a4, a4, 2048
[0x80005940]:add a6, a6, a4
[0x80005944]:lw s10, 1272(a6)
[0x80005948]:sub a6, a6, a4
[0x8000594c]:lui a4, 1
[0x80005950]:addi a4, a4, 2048
[0x80005954]:add a6, a6, a4
[0x80005958]:lw s11, 1276(a6)
[0x8000595c]:sub a6, a6, a4
[0x80005960]:lui t3, 856149
[0x80005964]:addi t3, t3, 3822
[0x80005968]:lui t4, 523934
[0x8000596c]:addi t4, t4, 2732
[0x80005970]:lui s10, 856149
[0x80005974]:addi s10, s10, 3822
[0x80005978]:lui s11, 523934
[0x8000597c]:addi s11, s11, 2732
[0x80005980]:addi a4, zero, 96
[0x80005984]:csrrw zero, fcsr, a4
[0x80005988]:fsub.d t5, t3, s10, dyn
[0x8000598c]:csrrs a7, fcsr, zero

[0x80005988]:fsub.d t5, t3, s10, dyn
[0x8000598c]:csrrs a7, fcsr, zero
[0x80005990]:sw t5, 504(ra)
[0x80005994]:sw t6, 512(ra)
[0x80005998]:sw t5, 520(ra)
[0x8000599c]:sw a7, 528(ra)
[0x800059a0]:lui a4, 1
[0x800059a4]:addi a4, a4, 2048
[0x800059a8]:add a6, a6, a4
[0x800059ac]:lw t3, 1280(a6)
[0x800059b0]:sub a6, a6, a4
[0x800059b4]:lui a4, 1
[0x800059b8]:addi a4, a4, 2048
[0x800059bc]:add a6, a6, a4
[0x800059c0]:lw t4, 1284(a6)
[0x800059c4]:sub a6, a6, a4
[0x800059c8]:lui a4, 1
[0x800059cc]:addi a4, a4, 2048
[0x800059d0]:add a6, a6, a4
[0x800059d4]:lw s10, 1288(a6)
[0x800059d8]:sub a6, a6, a4
[0x800059dc]:lui a4, 1
[0x800059e0]:addi a4, a4, 2048
[0x800059e4]:add a6, a6, a4
[0x800059e8]:lw s11, 1292(a6)
[0x800059ec]:sub a6, a6, a4
[0x800059f0]:lui t3, 417363
[0x800059f4]:addi t3, t3, 4033
[0x800059f8]:lui t4, 523538
[0x800059fc]:addi t4, t4, 3878
[0x80005a00]:lui s10, 417363
[0x80005a04]:addi s10, s10, 4033
[0x80005a08]:lui s11, 523538
[0x80005a0c]:addi s11, s11, 3878
[0x80005a10]:addi a4, zero, 96
[0x80005a14]:csrrw zero, fcsr, a4
[0x80005a18]:fsub.d t5, t3, s10, dyn
[0x80005a1c]:csrrs a7, fcsr, zero

[0x80005a18]:fsub.d t5, t3, s10, dyn
[0x80005a1c]:csrrs a7, fcsr, zero
[0x80005a20]:sw t5, 536(ra)
[0x80005a24]:sw t6, 544(ra)
[0x80005a28]:sw t5, 552(ra)
[0x80005a2c]:sw a7, 560(ra)
[0x80005a30]:lui a4, 1
[0x80005a34]:addi a4, a4, 2048
[0x80005a38]:add a6, a6, a4
[0x80005a3c]:lw t3, 1296(a6)
[0x80005a40]:sub a6, a6, a4
[0x80005a44]:lui a4, 1
[0x80005a48]:addi a4, a4, 2048
[0x80005a4c]:add a6, a6, a4
[0x80005a50]:lw t4, 1300(a6)
[0x80005a54]:sub a6, a6, a4
[0x80005a58]:lui a4, 1
[0x80005a5c]:addi a4, a4, 2048
[0x80005a60]:add a6, a6, a4
[0x80005a64]:lw s10, 1304(a6)
[0x80005a68]:sub a6, a6, a4
[0x80005a6c]:lui a4, 1
[0x80005a70]:addi a4, a4, 2048
[0x80005a74]:add a6, a6, a4
[0x80005a78]:lw s11, 1308(a6)
[0x80005a7c]:sub a6, a6, a4
[0x80005a80]:lui t3, 81507
[0x80005a84]:addi t3, t3, 1905
[0x80005a88]:lui t4, 524011
[0x80005a8c]:addi t4, t4, 953
[0x80005a90]:lui s10, 81507
[0x80005a94]:addi s10, s10, 1905
[0x80005a98]:lui s11, 524011
[0x80005a9c]:addi s11, s11, 953
[0x80005aa0]:addi a4, zero, 96
[0x80005aa4]:csrrw zero, fcsr, a4
[0x80005aa8]:fsub.d t5, t3, s10, dyn
[0x80005aac]:csrrs a7, fcsr, zero

[0x80005aa8]:fsub.d t5, t3, s10, dyn
[0x80005aac]:csrrs a7, fcsr, zero
[0x80005ab0]:sw t5, 568(ra)
[0x80005ab4]:sw t6, 576(ra)
[0x80005ab8]:sw t5, 584(ra)
[0x80005abc]:sw a7, 592(ra)
[0x80005ac0]:lui a4, 1
[0x80005ac4]:addi a4, a4, 2048
[0x80005ac8]:add a6, a6, a4
[0x80005acc]:lw t3, 1312(a6)
[0x80005ad0]:sub a6, a6, a4
[0x80005ad4]:lui a4, 1
[0x80005ad8]:addi a4, a4, 2048
[0x80005adc]:add a6, a6, a4
[0x80005ae0]:lw t4, 1316(a6)
[0x80005ae4]:sub a6, a6, a4
[0x80005ae8]:lui a4, 1
[0x80005aec]:addi a4, a4, 2048
[0x80005af0]:add a6, a6, a4
[0x80005af4]:lw s10, 1320(a6)
[0x80005af8]:sub a6, a6, a4
[0x80005afc]:lui a4, 1
[0x80005b00]:addi a4, a4, 2048
[0x80005b04]:add a6, a6, a4
[0x80005b08]:lw s11, 1324(a6)
[0x80005b0c]:sub a6, a6, a4
[0x80005b10]:lui t3, 911227
[0x80005b14]:addi t3, t3, 1362
[0x80005b18]:lui t4, 523917
[0x80005b1c]:addi t4, t4, 768
[0x80005b20]:lui s10, 911227
[0x80005b24]:addi s10, s10, 1362
[0x80005b28]:lui s11, 523917
[0x80005b2c]:addi s11, s11, 768
[0x80005b30]:addi a4, zero, 96
[0x80005b34]:csrrw zero, fcsr, a4
[0x80005b38]:fsub.d t5, t3, s10, dyn
[0x80005b3c]:csrrs a7, fcsr, zero

[0x80005b38]:fsub.d t5, t3, s10, dyn
[0x80005b3c]:csrrs a7, fcsr, zero
[0x80005b40]:sw t5, 600(ra)
[0x80005b44]:sw t6, 608(ra)
[0x80005b48]:sw t5, 616(ra)
[0x80005b4c]:sw a7, 624(ra)
[0x80005b50]:lui a4, 1
[0x80005b54]:addi a4, a4, 2048
[0x80005b58]:add a6, a6, a4
[0x80005b5c]:lw t3, 1328(a6)
[0x80005b60]:sub a6, a6, a4
[0x80005b64]:lui a4, 1
[0x80005b68]:addi a4, a4, 2048
[0x80005b6c]:add a6, a6, a4
[0x80005b70]:lw t4, 1332(a6)
[0x80005b74]:sub a6, a6, a4
[0x80005b78]:lui a4, 1
[0x80005b7c]:addi a4, a4, 2048
[0x80005b80]:add a6, a6, a4
[0x80005b84]:lw s10, 1336(a6)
[0x80005b88]:sub a6, a6, a4
[0x80005b8c]:lui a4, 1
[0x80005b90]:addi a4, a4, 2048
[0x80005b94]:add a6, a6, a4
[0x80005b98]:lw s11, 1340(a6)
[0x80005b9c]:sub a6, a6, a4
[0x80005ba0]:lui t3, 601074
[0x80005ba4]:addi t3, t3, 3000
[0x80005ba8]:lui t4, 523858
[0x80005bac]:addi t4, t4, 3175
[0x80005bb0]:lui s10, 601074
[0x80005bb4]:addi s10, s10, 3000
[0x80005bb8]:lui s11, 523858
[0x80005bbc]:addi s11, s11, 3175
[0x80005bc0]:addi a4, zero, 96
[0x80005bc4]:csrrw zero, fcsr, a4
[0x80005bc8]:fsub.d t5, t3, s10, dyn
[0x80005bcc]:csrrs a7, fcsr, zero

[0x80005bc8]:fsub.d t5, t3, s10, dyn
[0x80005bcc]:csrrs a7, fcsr, zero
[0x80005bd0]:sw t5, 632(ra)
[0x80005bd4]:sw t6, 640(ra)
[0x80005bd8]:sw t5, 648(ra)
[0x80005bdc]:sw a7, 656(ra)
[0x80005be0]:lui a4, 1
[0x80005be4]:addi a4, a4, 2048
[0x80005be8]:add a6, a6, a4
[0x80005bec]:lw t3, 1344(a6)
[0x80005bf0]:sub a6, a6, a4
[0x80005bf4]:lui a4, 1
[0x80005bf8]:addi a4, a4, 2048
[0x80005bfc]:add a6, a6, a4
[0x80005c00]:lw t4, 1348(a6)
[0x80005c04]:sub a6, a6, a4
[0x80005c08]:lui a4, 1
[0x80005c0c]:addi a4, a4, 2048
[0x80005c10]:add a6, a6, a4
[0x80005c14]:lw s10, 1352(a6)
[0x80005c18]:sub a6, a6, a4
[0x80005c1c]:lui a4, 1
[0x80005c20]:addi a4, a4, 2048
[0x80005c24]:add a6, a6, a4
[0x80005c28]:lw s11, 1356(a6)
[0x80005c2c]:sub a6, a6, a4
[0x80005c30]:lui t3, 650477
[0x80005c34]:addi t3, t3, 2447
[0x80005c38]:lui t4, 523720
[0x80005c3c]:addi t4, t4, 3031
[0x80005c40]:lui s10, 650477
[0x80005c44]:addi s10, s10, 2447
[0x80005c48]:lui s11, 523720
[0x80005c4c]:addi s11, s11, 3031
[0x80005c50]:addi a4, zero, 96
[0x80005c54]:csrrw zero, fcsr, a4
[0x80005c58]:fsub.d t5, t3, s10, dyn
[0x80005c5c]:csrrs a7, fcsr, zero

[0x80005c58]:fsub.d t5, t3, s10, dyn
[0x80005c5c]:csrrs a7, fcsr, zero
[0x80005c60]:sw t5, 664(ra)
[0x80005c64]:sw t6, 672(ra)
[0x80005c68]:sw t5, 680(ra)
[0x80005c6c]:sw a7, 688(ra)
[0x80005c70]:lui a4, 1
[0x80005c74]:addi a4, a4, 2048
[0x80005c78]:add a6, a6, a4
[0x80005c7c]:lw t3, 1360(a6)
[0x80005c80]:sub a6, a6, a4
[0x80005c84]:lui a4, 1
[0x80005c88]:addi a4, a4, 2048
[0x80005c8c]:add a6, a6, a4
[0x80005c90]:lw t4, 1364(a6)
[0x80005c94]:sub a6, a6, a4
[0x80005c98]:lui a4, 1
[0x80005c9c]:addi a4, a4, 2048
[0x80005ca0]:add a6, a6, a4
[0x80005ca4]:lw s10, 1368(a6)
[0x80005ca8]:sub a6, a6, a4
[0x80005cac]:lui a4, 1
[0x80005cb0]:addi a4, a4, 2048
[0x80005cb4]:add a6, a6, a4
[0x80005cb8]:lw s11, 1372(a6)
[0x80005cbc]:sub a6, a6, a4
[0x80005cc0]:lui t3, 849527
[0x80005cc4]:addi t3, t3, 3265
[0x80005cc8]:lui t4, 524017
[0x80005ccc]:addi t4, t4, 1057
[0x80005cd0]:lui s10, 849527
[0x80005cd4]:addi s10, s10, 3265
[0x80005cd8]:lui s11, 524017
[0x80005cdc]:addi s11, s11, 1057
[0x80005ce0]:addi a4, zero, 96
[0x80005ce4]:csrrw zero, fcsr, a4
[0x80005ce8]:fsub.d t5, t3, s10, dyn
[0x80005cec]:csrrs a7, fcsr, zero

[0x80005ce8]:fsub.d t5, t3, s10, dyn
[0x80005cec]:csrrs a7, fcsr, zero
[0x80005cf0]:sw t5, 696(ra)
[0x80005cf4]:sw t6, 704(ra)
[0x80005cf8]:sw t5, 712(ra)
[0x80005cfc]:sw a7, 720(ra)
[0x80005d00]:lui a4, 1
[0x80005d04]:addi a4, a4, 2048
[0x80005d08]:add a6, a6, a4
[0x80005d0c]:lw t3, 1376(a6)
[0x80005d10]:sub a6, a6, a4
[0x80005d14]:lui a4, 1
[0x80005d18]:addi a4, a4, 2048
[0x80005d1c]:add a6, a6, a4
[0x80005d20]:lw t4, 1380(a6)
[0x80005d24]:sub a6, a6, a4
[0x80005d28]:lui a4, 1
[0x80005d2c]:addi a4, a4, 2048
[0x80005d30]:add a6, a6, a4
[0x80005d34]:lw s10, 1384(a6)
[0x80005d38]:sub a6, a6, a4
[0x80005d3c]:lui a4, 1
[0x80005d40]:addi a4, a4, 2048
[0x80005d44]:add a6, a6, a4
[0x80005d48]:lw s11, 1388(a6)
[0x80005d4c]:sub a6, a6, a4
[0x80005d50]:lui t3, 96722
[0x80005d54]:addi t3, t3, 2990
[0x80005d58]:lui t4, 523931
[0x80005d5c]:addi t4, t4, 4048
[0x80005d60]:lui s10, 96722
[0x80005d64]:addi s10, s10, 2990
[0x80005d68]:lui s11, 523931
[0x80005d6c]:addi s11, s11, 4048
[0x80005d70]:addi a4, zero, 96
[0x80005d74]:csrrw zero, fcsr, a4
[0x80005d78]:fsub.d t5, t3, s10, dyn
[0x80005d7c]:csrrs a7, fcsr, zero

[0x80005d78]:fsub.d t5, t3, s10, dyn
[0x80005d7c]:csrrs a7, fcsr, zero
[0x80005d80]:sw t5, 728(ra)
[0x80005d84]:sw t6, 736(ra)
[0x80005d88]:sw t5, 744(ra)
[0x80005d8c]:sw a7, 752(ra)
[0x80005d90]:lui a4, 1
[0x80005d94]:addi a4, a4, 2048
[0x80005d98]:add a6, a6, a4
[0x80005d9c]:lw t3, 1392(a6)
[0x80005da0]:sub a6, a6, a4
[0x80005da4]:lui a4, 1
[0x80005da8]:addi a4, a4, 2048
[0x80005dac]:add a6, a6, a4
[0x80005db0]:lw t4, 1396(a6)
[0x80005db4]:sub a6, a6, a4
[0x80005db8]:lui a4, 1
[0x80005dbc]:addi a4, a4, 2048
[0x80005dc0]:add a6, a6, a4
[0x80005dc4]:lw s10, 1400(a6)
[0x80005dc8]:sub a6, a6, a4
[0x80005dcc]:lui a4, 1
[0x80005dd0]:addi a4, a4, 2048
[0x80005dd4]:add a6, a6, a4
[0x80005dd8]:lw s11, 1404(a6)
[0x80005ddc]:sub a6, a6, a4
[0x80005de0]:lui t3, 583931
[0x80005de4]:addi t3, t3, 1127
[0x80005de8]:lui t4, 523345
[0x80005dec]:addi t4, t4, 1706
[0x80005df0]:lui s10, 583931
[0x80005df4]:addi s10, s10, 1127
[0x80005df8]:lui s11, 523345
[0x80005dfc]:addi s11, s11, 1706
[0x80005e00]:addi a4, zero, 96
[0x80005e04]:csrrw zero, fcsr, a4
[0x80005e08]:fsub.d t5, t3, s10, dyn
[0x80005e0c]:csrrs a7, fcsr, zero

[0x80005e08]:fsub.d t5, t3, s10, dyn
[0x80005e0c]:csrrs a7, fcsr, zero
[0x80005e10]:sw t5, 760(ra)
[0x80005e14]:sw t6, 768(ra)
[0x80005e18]:sw t5, 776(ra)
[0x80005e1c]:sw a7, 784(ra)
[0x80005e20]:lui a4, 1
[0x80005e24]:addi a4, a4, 2048
[0x80005e28]:add a6, a6, a4
[0x80005e2c]:lw t3, 1408(a6)
[0x80005e30]:sub a6, a6, a4
[0x80005e34]:lui a4, 1
[0x80005e38]:addi a4, a4, 2048
[0x80005e3c]:add a6, a6, a4
[0x80005e40]:lw t4, 1412(a6)
[0x80005e44]:sub a6, a6, a4
[0x80005e48]:lui a4, 1
[0x80005e4c]:addi a4, a4, 2048
[0x80005e50]:add a6, a6, a4
[0x80005e54]:lw s10, 1416(a6)
[0x80005e58]:sub a6, a6, a4
[0x80005e5c]:lui a4, 1
[0x80005e60]:addi a4, a4, 2048
[0x80005e64]:add a6, a6, a4
[0x80005e68]:lw s11, 1420(a6)
[0x80005e6c]:sub a6, a6, a4
[0x80005e70]:lui t3, 499542
[0x80005e74]:addi t3, t3, 3163
[0x80005e78]:lui t4, 523267
[0x80005e7c]:addi t4, t4, 2965
[0x80005e80]:lui s10, 499542
[0x80005e84]:addi s10, s10, 3163
[0x80005e88]:lui s11, 523267
[0x80005e8c]:addi s11, s11, 2965
[0x80005e90]:addi a4, zero, 96
[0x80005e94]:csrrw zero, fcsr, a4
[0x80005e98]:fsub.d t5, t3, s10, dyn
[0x80005e9c]:csrrs a7, fcsr, zero

[0x80005e98]:fsub.d t5, t3, s10, dyn
[0x80005e9c]:csrrs a7, fcsr, zero
[0x80005ea0]:sw t5, 792(ra)
[0x80005ea4]:sw t6, 800(ra)
[0x80005ea8]:sw t5, 808(ra)
[0x80005eac]:sw a7, 816(ra)
[0x80005eb0]:lui a4, 1
[0x80005eb4]:addi a4, a4, 2048
[0x80005eb8]:add a6, a6, a4
[0x80005ebc]:lw t3, 1424(a6)
[0x80005ec0]:sub a6, a6, a4
[0x80005ec4]:lui a4, 1
[0x80005ec8]:addi a4, a4, 2048
[0x80005ecc]:add a6, a6, a4
[0x80005ed0]:lw t4, 1428(a6)
[0x80005ed4]:sub a6, a6, a4
[0x80005ed8]:lui a4, 1
[0x80005edc]:addi a4, a4, 2048
[0x80005ee0]:add a6, a6, a4
[0x80005ee4]:lw s10, 1432(a6)
[0x80005ee8]:sub a6, a6, a4
[0x80005eec]:lui a4, 1
[0x80005ef0]:addi a4, a4, 2048
[0x80005ef4]:add a6, a6, a4
[0x80005ef8]:lw s11, 1436(a6)
[0x80005efc]:sub a6, a6, a4
[0x80005f00]:lui t3, 134546
[0x80005f04]:addi t3, t3, 2685
[0x80005f08]:lui t4, 524011
[0x80005f0c]:addi t4, t4, 922
[0x80005f10]:lui s10, 134546
[0x80005f14]:addi s10, s10, 2685
[0x80005f18]:lui s11, 524011
[0x80005f1c]:addi s11, s11, 922
[0x80005f20]:addi a4, zero, 96
[0x80005f24]:csrrw zero, fcsr, a4
[0x80005f28]:fsub.d t5, t3, s10, dyn
[0x80005f2c]:csrrs a7, fcsr, zero

[0x80005f28]:fsub.d t5, t3, s10, dyn
[0x80005f2c]:csrrs a7, fcsr, zero
[0x80005f30]:sw t5, 824(ra)
[0x80005f34]:sw t6, 832(ra)
[0x80005f38]:sw t5, 840(ra)
[0x80005f3c]:sw a7, 848(ra)
[0x80005f40]:lui a4, 1
[0x80005f44]:addi a4, a4, 2048
[0x80005f48]:add a6, a6, a4
[0x80005f4c]:lw t3, 1440(a6)
[0x80005f50]:sub a6, a6, a4
[0x80005f54]:lui a4, 1
[0x80005f58]:addi a4, a4, 2048
[0x80005f5c]:add a6, a6, a4
[0x80005f60]:lw t4, 1444(a6)
[0x80005f64]:sub a6, a6, a4
[0x80005f68]:lui a4, 1
[0x80005f6c]:addi a4, a4, 2048
[0x80005f70]:add a6, a6, a4
[0x80005f74]:lw s10, 1448(a6)
[0x80005f78]:sub a6, a6, a4
[0x80005f7c]:lui a4, 1
[0x80005f80]:addi a4, a4, 2048
[0x80005f84]:add a6, a6, a4
[0x80005f88]:lw s11, 1452(a6)
[0x80005f8c]:sub a6, a6, a4
[0x80005f90]:lui t3, 352814
[0x80005f94]:addi t3, t3, 3888
[0x80005f98]:lui t4, 523828
[0x80005f9c]:addi t4, t4, 2243
[0x80005fa0]:lui s10, 352814
[0x80005fa4]:addi s10, s10, 3888
[0x80005fa8]:lui s11, 523828
[0x80005fac]:addi s11, s11, 2243
[0x80005fb0]:addi a4, zero, 96
[0x80005fb4]:csrrw zero, fcsr, a4
[0x80005fb8]:fsub.d t5, t3, s10, dyn
[0x80005fbc]:csrrs a7, fcsr, zero

[0x80005fb8]:fsub.d t5, t3, s10, dyn
[0x80005fbc]:csrrs a7, fcsr, zero
[0x80005fc0]:sw t5, 856(ra)
[0x80005fc4]:sw t6, 864(ra)
[0x80005fc8]:sw t5, 872(ra)
[0x80005fcc]:sw a7, 880(ra)
[0x80005fd0]:lui a4, 1
[0x80005fd4]:addi a4, a4, 2048
[0x80005fd8]:add a6, a6, a4
[0x80005fdc]:lw t3, 1456(a6)
[0x80005fe0]:sub a6, a6, a4
[0x80005fe4]:lui a4, 1
[0x80005fe8]:addi a4, a4, 2048
[0x80005fec]:add a6, a6, a4
[0x80005ff0]:lw t4, 1460(a6)
[0x80005ff4]:sub a6, a6, a4
[0x80005ff8]:lui a4, 1
[0x80005ffc]:addi a4, a4, 2048
[0x80006000]:add a6, a6, a4
[0x80006004]:lw s10, 1464(a6)
[0x80006008]:sub a6, a6, a4
[0x8000600c]:lui a4, 1
[0x80006010]:addi a4, a4, 2048
[0x80006014]:add a6, a6, a4
[0x80006018]:lw s11, 1468(a6)
[0x8000601c]:sub a6, a6, a4
[0x80006020]:lui t3, 1004078
[0x80006024]:addi t3, t3, 1343
[0x80006028]:lui t4, 523236
[0x8000602c]:addi t4, t4, 2853
[0x80006030]:lui s10, 1004078
[0x80006034]:addi s10, s10, 1343
[0x80006038]:lui s11, 523236
[0x8000603c]:addi s11, s11, 2853
[0x80006040]:addi a4, zero, 96
[0x80006044]:csrrw zero, fcsr, a4
[0x80006048]:fsub.d t5, t3, s10, dyn
[0x8000604c]:csrrs a7, fcsr, zero

[0x80006048]:fsub.d t5, t3, s10, dyn
[0x8000604c]:csrrs a7, fcsr, zero
[0x80006050]:sw t5, 888(ra)
[0x80006054]:sw t6, 896(ra)
[0x80006058]:sw t5, 904(ra)
[0x8000605c]:sw a7, 912(ra)
[0x80006060]:lui a4, 1
[0x80006064]:addi a4, a4, 2048
[0x80006068]:add a6, a6, a4
[0x8000606c]:lw t3, 1472(a6)
[0x80006070]:sub a6, a6, a4
[0x80006074]:lui a4, 1
[0x80006078]:addi a4, a4, 2048
[0x8000607c]:add a6, a6, a4
[0x80006080]:lw t4, 1476(a6)
[0x80006084]:sub a6, a6, a4
[0x80006088]:lui a4, 1
[0x8000608c]:addi a4, a4, 2048
[0x80006090]:add a6, a6, a4
[0x80006094]:lw s10, 1480(a6)
[0x80006098]:sub a6, a6, a4
[0x8000609c]:lui a4, 1
[0x800060a0]:addi a4, a4, 2048
[0x800060a4]:add a6, a6, a4
[0x800060a8]:lw s11, 1484(a6)
[0x800060ac]:sub a6, a6, a4
[0x800060b0]:lui t3, 876796
[0x800060b4]:addi t3, t3, 1147
[0x800060b8]:lui t4, 523516
[0x800060bc]:addi t4, t4, 1421
[0x800060c0]:lui s10, 876796
[0x800060c4]:addi s10, s10, 1147
[0x800060c8]:lui s11, 523516
[0x800060cc]:addi s11, s11, 1421
[0x800060d0]:addi a4, zero, 96
[0x800060d4]:csrrw zero, fcsr, a4
[0x800060d8]:fsub.d t5, t3, s10, dyn
[0x800060dc]:csrrs a7, fcsr, zero

[0x800060d8]:fsub.d t5, t3, s10, dyn
[0x800060dc]:csrrs a7, fcsr, zero
[0x800060e0]:sw t5, 920(ra)
[0x800060e4]:sw t6, 928(ra)
[0x800060e8]:sw t5, 936(ra)
[0x800060ec]:sw a7, 944(ra)
[0x800060f0]:lui a4, 1
[0x800060f4]:addi a4, a4, 2048
[0x800060f8]:add a6, a6, a4
[0x800060fc]:lw t3, 1488(a6)
[0x80006100]:sub a6, a6, a4
[0x80006104]:lui a4, 1
[0x80006108]:addi a4, a4, 2048
[0x8000610c]:add a6, a6, a4
[0x80006110]:lw t4, 1492(a6)
[0x80006114]:sub a6, a6, a4
[0x80006118]:lui a4, 1
[0x8000611c]:addi a4, a4, 2048
[0x80006120]:add a6, a6, a4
[0x80006124]:lw s10, 1496(a6)
[0x80006128]:sub a6, a6, a4
[0x8000612c]:lui a4, 1
[0x80006130]:addi a4, a4, 2048
[0x80006134]:add a6, a6, a4
[0x80006138]:lw s11, 1500(a6)
[0x8000613c]:sub a6, a6, a4
[0x80006140]:lui t3, 964279
[0x80006144]:addi t3, t3, 511
[0x80006148]:lui t4, 521991
[0x8000614c]:addi t4, t4, 2993
[0x80006150]:lui s10, 964279
[0x80006154]:addi s10, s10, 511
[0x80006158]:lui s11, 521991
[0x8000615c]:addi s11, s11, 2993
[0x80006160]:addi a4, zero, 96
[0x80006164]:csrrw zero, fcsr, a4
[0x80006168]:fsub.d t5, t3, s10, dyn
[0x8000616c]:csrrs a7, fcsr, zero

[0x80006168]:fsub.d t5, t3, s10, dyn
[0x8000616c]:csrrs a7, fcsr, zero
[0x80006170]:sw t5, 952(ra)
[0x80006174]:sw t6, 960(ra)
[0x80006178]:sw t5, 968(ra)
[0x8000617c]:sw a7, 976(ra)
[0x80006180]:lui a4, 1
[0x80006184]:addi a4, a4, 2048
[0x80006188]:add a6, a6, a4
[0x8000618c]:lw t3, 1504(a6)
[0x80006190]:sub a6, a6, a4
[0x80006194]:lui a4, 1
[0x80006198]:addi a4, a4, 2048
[0x8000619c]:add a6, a6, a4
[0x800061a0]:lw t4, 1508(a6)
[0x800061a4]:sub a6, a6, a4
[0x800061a8]:lui a4, 1
[0x800061ac]:addi a4, a4, 2048
[0x800061b0]:add a6, a6, a4
[0x800061b4]:lw s10, 1512(a6)
[0x800061b8]:sub a6, a6, a4
[0x800061bc]:lui a4, 1
[0x800061c0]:addi a4, a4, 2048
[0x800061c4]:add a6, a6, a4
[0x800061c8]:lw s11, 1516(a6)
[0x800061cc]:sub a6, a6, a4
[0x800061d0]:lui t3, 674335
[0x800061d4]:addi t3, t3, 1553
[0x800061d8]:lui t4, 523713
[0x800061dc]:addi t4, t4, 1243
[0x800061e0]:lui s10, 674335
[0x800061e4]:addi s10, s10, 1553
[0x800061e8]:lui s11, 523713
[0x800061ec]:addi s11, s11, 1243
[0x800061f0]:addi a4, zero, 96
[0x800061f4]:csrrw zero, fcsr, a4
[0x800061f8]:fsub.d t5, t3, s10, dyn
[0x800061fc]:csrrs a7, fcsr, zero

[0x800061f8]:fsub.d t5, t3, s10, dyn
[0x800061fc]:csrrs a7, fcsr, zero
[0x80006200]:sw t5, 984(ra)
[0x80006204]:sw t6, 992(ra)
[0x80006208]:sw t5, 1000(ra)
[0x8000620c]:sw a7, 1008(ra)
[0x80006210]:lui a4, 1
[0x80006214]:addi a4, a4, 2048
[0x80006218]:add a6, a6, a4
[0x8000621c]:lw t3, 1520(a6)
[0x80006220]:sub a6, a6, a4
[0x80006224]:lui a4, 1
[0x80006228]:addi a4, a4, 2048
[0x8000622c]:add a6, a6, a4
[0x80006230]:lw t4, 1524(a6)
[0x80006234]:sub a6, a6, a4
[0x80006238]:lui a4, 1
[0x8000623c]:addi a4, a4, 2048
[0x80006240]:add a6, a6, a4
[0x80006244]:lw s10, 1528(a6)
[0x80006248]:sub a6, a6, a4
[0x8000624c]:lui a4, 1
[0x80006250]:addi a4, a4, 2048
[0x80006254]:add a6, a6, a4
[0x80006258]:lw s11, 1532(a6)
[0x8000625c]:sub a6, a6, a4
[0x80006260]:lui t3, 671303
[0x80006264]:addi t3, t3, 534
[0x80006268]:lui t4, 523841
[0x8000626c]:addi t4, t4, 1202
[0x80006270]:lui s10, 671303
[0x80006274]:addi s10, s10, 534
[0x80006278]:lui s11, 523841
[0x8000627c]:addi s11, s11, 1202
[0x80006280]:addi a4, zero, 96
[0x80006284]:csrrw zero, fcsr, a4
[0x80006288]:fsub.d t5, t3, s10, dyn
[0x8000628c]:csrrs a7, fcsr, zero

[0x80006288]:fsub.d t5, t3, s10, dyn
[0x8000628c]:csrrs a7, fcsr, zero
[0x80006290]:sw t5, 1016(ra)
[0x80006294]:sw t6, 1024(ra)
[0x80006298]:sw t5, 1032(ra)
[0x8000629c]:sw a7, 1040(ra)
[0x800062a0]:lui a4, 1
[0x800062a4]:addi a4, a4, 2048
[0x800062a8]:add a6, a6, a4
[0x800062ac]:lw t3, 1536(a6)
[0x800062b0]:sub a6, a6, a4
[0x800062b4]:lui a4, 1
[0x800062b8]:addi a4, a4, 2048
[0x800062bc]:add a6, a6, a4
[0x800062c0]:lw t4, 1540(a6)
[0x800062c4]:sub a6, a6, a4
[0x800062c8]:lui a4, 1
[0x800062cc]:addi a4, a4, 2048
[0x800062d0]:add a6, a6, a4
[0x800062d4]:lw s10, 1544(a6)
[0x800062d8]:sub a6, a6, a4
[0x800062dc]:lui a4, 1
[0x800062e0]:addi a4, a4, 2048
[0x800062e4]:add a6, a6, a4
[0x800062e8]:lw s11, 1548(a6)
[0x800062ec]:sub a6, a6, a4
[0x800062f0]:lui t3, 954538
[0x800062f4]:addi t3, t3, 1395
[0x800062f8]:lui t4, 523948
[0x800062fc]:addi t4, t4, 2826
[0x80006300]:lui s10, 954538
[0x80006304]:addi s10, s10, 1395
[0x80006308]:lui s11, 523948
[0x8000630c]:addi s11, s11, 2826
[0x80006310]:addi a4, zero, 96
[0x80006314]:csrrw zero, fcsr, a4
[0x80006318]:fsub.d t5, t3, s10, dyn
[0x8000631c]:csrrs a7, fcsr, zero

[0x80006318]:fsub.d t5, t3, s10, dyn
[0x8000631c]:csrrs a7, fcsr, zero
[0x80006320]:sw t5, 1048(ra)
[0x80006324]:sw t6, 1056(ra)
[0x80006328]:sw t5, 1064(ra)
[0x8000632c]:sw a7, 1072(ra)
[0x80006330]:lui a4, 1
[0x80006334]:addi a4, a4, 2048
[0x80006338]:add a6, a6, a4
[0x8000633c]:lw t3, 1552(a6)
[0x80006340]:sub a6, a6, a4
[0x80006344]:lui a4, 1
[0x80006348]:addi a4, a4, 2048
[0x8000634c]:add a6, a6, a4
[0x80006350]:lw t4, 1556(a6)
[0x80006354]:sub a6, a6, a4
[0x80006358]:lui a4, 1
[0x8000635c]:addi a4, a4, 2048
[0x80006360]:add a6, a6, a4
[0x80006364]:lw s10, 1560(a6)
[0x80006368]:sub a6, a6, a4
[0x8000636c]:lui a4, 1
[0x80006370]:addi a4, a4, 2048
[0x80006374]:add a6, a6, a4
[0x80006378]:lw s11, 1564(a6)
[0x8000637c]:sub a6, a6, a4
[0x80006380]:lui t3, 692991
[0x80006384]:addi t3, t3, 463
[0x80006388]:lui t4, 523556
[0x8000638c]:addi t4, t4, 2524
[0x80006390]:lui s10, 692991
[0x80006394]:addi s10, s10, 463
[0x80006398]:lui s11, 523556
[0x8000639c]:addi s11, s11, 2524
[0x800063a0]:addi a4, zero, 96
[0x800063a4]:csrrw zero, fcsr, a4
[0x800063a8]:fsub.d t5, t3, s10, dyn
[0x800063ac]:csrrs a7, fcsr, zero

[0x800063a8]:fsub.d t5, t3, s10, dyn
[0x800063ac]:csrrs a7, fcsr, zero
[0x800063b0]:sw t5, 1080(ra)
[0x800063b4]:sw t6, 1088(ra)
[0x800063b8]:sw t5, 1096(ra)
[0x800063bc]:sw a7, 1104(ra)
[0x800063c0]:lui a4, 1
[0x800063c4]:addi a4, a4, 2048
[0x800063c8]:add a6, a6, a4
[0x800063cc]:lw t3, 1568(a6)
[0x800063d0]:sub a6, a6, a4
[0x800063d4]:lui a4, 1
[0x800063d8]:addi a4, a4, 2048
[0x800063dc]:add a6, a6, a4
[0x800063e0]:lw t4, 1572(a6)
[0x800063e4]:sub a6, a6, a4
[0x800063e8]:lui a4, 1
[0x800063ec]:addi a4, a4, 2048
[0x800063f0]:add a6, a6, a4
[0x800063f4]:lw s10, 1576(a6)
[0x800063f8]:sub a6, a6, a4
[0x800063fc]:lui a4, 1
[0x80006400]:addi a4, a4, 2048
[0x80006404]:add a6, a6, a4
[0x80006408]:lw s11, 1580(a6)
[0x8000640c]:sub a6, a6, a4
[0x80006410]:lui t3, 665555
[0x80006414]:addi t3, t3, 1199
[0x80006418]:lui t4, 523544
[0x8000641c]:addi t4, t4, 3207
[0x80006420]:lui s10, 665555
[0x80006424]:addi s10, s10, 1199
[0x80006428]:lui s11, 523544
[0x8000642c]:addi s11, s11, 3207
[0x80006430]:addi a4, zero, 96
[0x80006434]:csrrw zero, fcsr, a4
[0x80006438]:fsub.d t5, t3, s10, dyn
[0x8000643c]:csrrs a7, fcsr, zero

[0x80006438]:fsub.d t5, t3, s10, dyn
[0x8000643c]:csrrs a7, fcsr, zero
[0x80006440]:sw t5, 1112(ra)
[0x80006444]:sw t6, 1120(ra)
[0x80006448]:sw t5, 1128(ra)
[0x8000644c]:sw a7, 1136(ra)
[0x80006450]:lui a4, 1
[0x80006454]:addi a4, a4, 2048
[0x80006458]:add a6, a6, a4
[0x8000645c]:lw t3, 1584(a6)
[0x80006460]:sub a6, a6, a4
[0x80006464]:lui a4, 1
[0x80006468]:addi a4, a4, 2048
[0x8000646c]:add a6, a6, a4
[0x80006470]:lw t4, 1588(a6)
[0x80006474]:sub a6, a6, a4
[0x80006478]:lui a4, 1
[0x8000647c]:addi a4, a4, 2048
[0x80006480]:add a6, a6, a4
[0x80006484]:lw s10, 1592(a6)
[0x80006488]:sub a6, a6, a4
[0x8000648c]:lui a4, 1
[0x80006490]:addi a4, a4, 2048
[0x80006494]:add a6, a6, a4
[0x80006498]:lw s11, 1596(a6)
[0x8000649c]:sub a6, a6, a4
[0x800064a0]:lui t3, 354955
[0x800064a4]:addi t3, t3, 2481
[0x800064a8]:lui t4, 523521
[0x800064ac]:addi t4, t4, 3700
[0x800064b0]:lui s10, 354955
[0x800064b4]:addi s10, s10, 2481
[0x800064b8]:lui s11, 523521
[0x800064bc]:addi s11, s11, 3700
[0x800064c0]:addi a4, zero, 96
[0x800064c4]:csrrw zero, fcsr, a4
[0x800064c8]:fsub.d t5, t3, s10, dyn
[0x800064cc]:csrrs a7, fcsr, zero

[0x800064c8]:fsub.d t5, t3, s10, dyn
[0x800064cc]:csrrs a7, fcsr, zero
[0x800064d0]:sw t5, 1144(ra)
[0x800064d4]:sw t6, 1152(ra)
[0x800064d8]:sw t5, 1160(ra)
[0x800064dc]:sw a7, 1168(ra)
[0x800064e0]:lui a4, 1
[0x800064e4]:addi a4, a4, 2048
[0x800064e8]:add a6, a6, a4
[0x800064ec]:lw t3, 1600(a6)
[0x800064f0]:sub a6, a6, a4
[0x800064f4]:lui a4, 1
[0x800064f8]:addi a4, a4, 2048
[0x800064fc]:add a6, a6, a4
[0x80006500]:lw t4, 1604(a6)
[0x80006504]:sub a6, a6, a4
[0x80006508]:lui a4, 1
[0x8000650c]:addi a4, a4, 2048
[0x80006510]:add a6, a6, a4
[0x80006514]:lw s10, 1608(a6)
[0x80006518]:sub a6, a6, a4
[0x8000651c]:lui a4, 1
[0x80006520]:addi a4, a4, 2048
[0x80006524]:add a6, a6, a4
[0x80006528]:lw s11, 1612(a6)
[0x8000652c]:sub a6, a6, a4
[0x80006530]:lui t3, 505631
[0x80006534]:addi t3, t3, 2350
[0x80006538]:lui t4, 523925
[0x8000653c]:addi t4, t4, 157
[0x80006540]:lui s10, 505631
[0x80006544]:addi s10, s10, 2350
[0x80006548]:lui s11, 523925
[0x8000654c]:addi s11, s11, 157
[0x80006550]:addi a4, zero, 96
[0x80006554]:csrrw zero, fcsr, a4
[0x80006558]:fsub.d t5, t3, s10, dyn
[0x8000655c]:csrrs a7, fcsr, zero

[0x80006558]:fsub.d t5, t3, s10, dyn
[0x8000655c]:csrrs a7, fcsr, zero
[0x80006560]:sw t5, 1176(ra)
[0x80006564]:sw t6, 1184(ra)
[0x80006568]:sw t5, 1192(ra)
[0x8000656c]:sw a7, 1200(ra)
[0x80006570]:lui a4, 1
[0x80006574]:addi a4, a4, 2048
[0x80006578]:add a6, a6, a4
[0x8000657c]:lw t3, 1616(a6)
[0x80006580]:sub a6, a6, a4
[0x80006584]:lui a4, 1
[0x80006588]:addi a4, a4, 2048
[0x8000658c]:add a6, a6, a4
[0x80006590]:lw t4, 1620(a6)
[0x80006594]:sub a6, a6, a4
[0x80006598]:lui a4, 1
[0x8000659c]:addi a4, a4, 2048
[0x800065a0]:add a6, a6, a4
[0x800065a4]:lw s10, 1624(a6)
[0x800065a8]:sub a6, a6, a4
[0x800065ac]:lui a4, 1
[0x800065b0]:addi a4, a4, 2048
[0x800065b4]:add a6, a6, a4
[0x800065b8]:lw s11, 1628(a6)
[0x800065bc]:sub a6, a6, a4
[0x800065c0]:lui t3, 1018560
[0x800065c4]:addi t3, t3, 724
[0x800065c8]:lui t4, 524018
[0x800065cc]:addi t4, t4, 2012
[0x800065d0]:lui s10, 1018560
[0x800065d4]:addi s10, s10, 724
[0x800065d8]:lui s11, 524018
[0x800065dc]:addi s11, s11, 2012
[0x800065e0]:addi a4, zero, 96
[0x800065e4]:csrrw zero, fcsr, a4
[0x800065e8]:fsub.d t5, t3, s10, dyn
[0x800065ec]:csrrs a7, fcsr, zero

[0x800065e8]:fsub.d t5, t3, s10, dyn
[0x800065ec]:csrrs a7, fcsr, zero
[0x800065f0]:sw t5, 1208(ra)
[0x800065f4]:sw t6, 1216(ra)
[0x800065f8]:sw t5, 1224(ra)
[0x800065fc]:sw a7, 1232(ra)
[0x80006600]:lui a4, 1
[0x80006604]:addi a4, a4, 2048
[0x80006608]:add a6, a6, a4
[0x8000660c]:lw t3, 1632(a6)
[0x80006610]:sub a6, a6, a4
[0x80006614]:lui a4, 1
[0x80006618]:addi a4, a4, 2048
[0x8000661c]:add a6, a6, a4
[0x80006620]:lw t4, 1636(a6)
[0x80006624]:sub a6, a6, a4
[0x80006628]:lui a4, 1
[0x8000662c]:addi a4, a4, 2048
[0x80006630]:add a6, a6, a4
[0x80006634]:lw s10, 1640(a6)
[0x80006638]:sub a6, a6, a4
[0x8000663c]:lui a4, 1
[0x80006640]:addi a4, a4, 2048
[0x80006644]:add a6, a6, a4
[0x80006648]:lw s11, 1644(a6)
[0x8000664c]:sub a6, a6, a4
[0x80006650]:lui t3, 341441
[0x80006654]:addi t3, t3, 1897
[0x80006658]:lui t4, 523767
[0x8000665c]:addi t4, t4, 2411
[0x80006660]:lui s10, 341441
[0x80006664]:addi s10, s10, 1897
[0x80006668]:lui s11, 523767
[0x8000666c]:addi s11, s11, 2411
[0x80006670]:addi a4, zero, 96
[0x80006674]:csrrw zero, fcsr, a4
[0x80006678]:fsub.d t5, t3, s10, dyn
[0x8000667c]:csrrs a7, fcsr, zero

[0x80006678]:fsub.d t5, t3, s10, dyn
[0x8000667c]:csrrs a7, fcsr, zero
[0x80006680]:sw t5, 1240(ra)
[0x80006684]:sw t6, 1248(ra)
[0x80006688]:sw t5, 1256(ra)
[0x8000668c]:sw a7, 1264(ra)
[0x80006690]:lui a4, 1
[0x80006694]:addi a4, a4, 2048
[0x80006698]:add a6, a6, a4
[0x8000669c]:lw t3, 1648(a6)
[0x800066a0]:sub a6, a6, a4
[0x800066a4]:lui a4, 1
[0x800066a8]:addi a4, a4, 2048
[0x800066ac]:add a6, a6, a4
[0x800066b0]:lw t4, 1652(a6)
[0x800066b4]:sub a6, a6, a4
[0x800066b8]:lui a4, 1
[0x800066bc]:addi a4, a4, 2048
[0x800066c0]:add a6, a6, a4
[0x800066c4]:lw s10, 1656(a6)
[0x800066c8]:sub a6, a6, a4
[0x800066cc]:lui a4, 1
[0x800066d0]:addi a4, a4, 2048
[0x800066d4]:add a6, a6, a4
[0x800066d8]:lw s11, 1660(a6)
[0x800066dc]:sub a6, a6, a4
[0x800066e0]:lui t3, 216415
[0x800066e4]:addi t3, t3, 1467
[0x800066e8]:lui t4, 523914
[0x800066ec]:addi t4, t4, 3123
[0x800066f0]:lui s10, 216415
[0x800066f4]:addi s10, s10, 1467
[0x800066f8]:lui s11, 523914
[0x800066fc]:addi s11, s11, 3123
[0x80006700]:addi a4, zero, 96
[0x80006704]:csrrw zero, fcsr, a4
[0x80006708]:fsub.d t5, t3, s10, dyn
[0x8000670c]:csrrs a7, fcsr, zero

[0x80006708]:fsub.d t5, t3, s10, dyn
[0x8000670c]:csrrs a7, fcsr, zero
[0x80006710]:sw t5, 1272(ra)
[0x80006714]:sw t6, 1280(ra)
[0x80006718]:sw t5, 1288(ra)
[0x8000671c]:sw a7, 1296(ra)
[0x80006720]:lui a4, 1
[0x80006724]:addi a4, a4, 2048
[0x80006728]:add a6, a6, a4
[0x8000672c]:lw t3, 1664(a6)
[0x80006730]:sub a6, a6, a4
[0x80006734]:lui a4, 1
[0x80006738]:addi a4, a4, 2048
[0x8000673c]:add a6, a6, a4
[0x80006740]:lw t4, 1668(a6)
[0x80006744]:sub a6, a6, a4
[0x80006748]:lui a4, 1
[0x8000674c]:addi a4, a4, 2048
[0x80006750]:add a6, a6, a4
[0x80006754]:lw s10, 1672(a6)
[0x80006758]:sub a6, a6, a4
[0x8000675c]:lui a4, 1
[0x80006760]:addi a4, a4, 2048
[0x80006764]:add a6, a6, a4
[0x80006768]:lw s11, 1676(a6)
[0x8000676c]:sub a6, a6, a4
[0x80006770]:lui t3, 825912
[0x80006774]:addi t3, t3, 2451
[0x80006778]:lui t4, 523656
[0x8000677c]:addi t4, t4, 1861
[0x80006780]:lui s10, 825912
[0x80006784]:addi s10, s10, 2451
[0x80006788]:lui s11, 523656
[0x8000678c]:addi s11, s11, 1861
[0x80006790]:addi a4, zero, 96
[0x80006794]:csrrw zero, fcsr, a4
[0x80006798]:fsub.d t5, t3, s10, dyn
[0x8000679c]:csrrs a7, fcsr, zero

[0x80006798]:fsub.d t5, t3, s10, dyn
[0x8000679c]:csrrs a7, fcsr, zero
[0x800067a0]:sw t5, 1304(ra)
[0x800067a4]:sw t6, 1312(ra)
[0x800067a8]:sw t5, 1320(ra)
[0x800067ac]:sw a7, 1328(ra)
[0x800067b0]:lui a4, 1
[0x800067b4]:addi a4, a4, 2048
[0x800067b8]:add a6, a6, a4
[0x800067bc]:lw t3, 1680(a6)
[0x800067c0]:sub a6, a6, a4
[0x800067c4]:lui a4, 1
[0x800067c8]:addi a4, a4, 2048
[0x800067cc]:add a6, a6, a4
[0x800067d0]:lw t4, 1684(a6)
[0x800067d4]:sub a6, a6, a4
[0x800067d8]:lui a4, 1
[0x800067dc]:addi a4, a4, 2048
[0x800067e0]:add a6, a6, a4
[0x800067e4]:lw s10, 1688(a6)
[0x800067e8]:sub a6, a6, a4
[0x800067ec]:lui a4, 1
[0x800067f0]:addi a4, a4, 2048
[0x800067f4]:add a6, a6, a4
[0x800067f8]:lw s11, 1692(a6)
[0x800067fc]:sub a6, a6, a4
[0x80006800]:lui t3, 904923
[0x80006804]:addi t3, t3, 1719
[0x80006808]:lui t4, 523195
[0x8000680c]:addi t4, t4, 3842
[0x80006810]:lui s10, 904923
[0x80006814]:addi s10, s10, 1719
[0x80006818]:lui s11, 523195
[0x8000681c]:addi s11, s11, 3842
[0x80006820]:addi a4, zero, 96
[0x80006824]:csrrw zero, fcsr, a4
[0x80006828]:fsub.d t5, t3, s10, dyn
[0x8000682c]:csrrs a7, fcsr, zero

[0x80006828]:fsub.d t5, t3, s10, dyn
[0x8000682c]:csrrs a7, fcsr, zero
[0x80006830]:sw t5, 1336(ra)
[0x80006834]:sw t6, 1344(ra)
[0x80006838]:sw t5, 1352(ra)
[0x8000683c]:sw a7, 1360(ra)
[0x80006840]:lui a4, 1
[0x80006844]:addi a4, a4, 2048
[0x80006848]:add a6, a6, a4
[0x8000684c]:lw t3, 1696(a6)
[0x80006850]:sub a6, a6, a4
[0x80006854]:lui a4, 1
[0x80006858]:addi a4, a4, 2048
[0x8000685c]:add a6, a6, a4
[0x80006860]:lw t4, 1700(a6)
[0x80006864]:sub a6, a6, a4
[0x80006868]:lui a4, 1
[0x8000686c]:addi a4, a4, 2048
[0x80006870]:add a6, a6, a4
[0x80006874]:lw s10, 1704(a6)
[0x80006878]:sub a6, a6, a4
[0x8000687c]:lui a4, 1
[0x80006880]:addi a4, a4, 2048
[0x80006884]:add a6, a6, a4
[0x80006888]:lw s11, 1708(a6)
[0x8000688c]:sub a6, a6, a4
[0x80006890]:lui t3, 598908
[0x80006894]:addi t3, t3, 1687
[0x80006898]:lui t4, 523236
[0x8000689c]:addi t4, t4, 2543
[0x800068a0]:lui s10, 598908
[0x800068a4]:addi s10, s10, 1687
[0x800068a8]:lui s11, 523236
[0x800068ac]:addi s11, s11, 2543
[0x800068b0]:addi a4, zero, 96
[0x800068b4]:csrrw zero, fcsr, a4
[0x800068b8]:fsub.d t5, t3, s10, dyn
[0x800068bc]:csrrs a7, fcsr, zero

[0x800068b8]:fsub.d t5, t3, s10, dyn
[0x800068bc]:csrrs a7, fcsr, zero
[0x800068c0]:sw t5, 1368(ra)
[0x800068c4]:sw t6, 1376(ra)
[0x800068c8]:sw t5, 1384(ra)
[0x800068cc]:sw a7, 1392(ra)
[0x800068d0]:lui a4, 1
[0x800068d4]:addi a4, a4, 2048
[0x800068d8]:add a6, a6, a4
[0x800068dc]:lw t3, 1712(a6)
[0x800068e0]:sub a6, a6, a4
[0x800068e4]:lui a4, 1
[0x800068e8]:addi a4, a4, 2048
[0x800068ec]:add a6, a6, a4
[0x800068f0]:lw t4, 1716(a6)
[0x800068f4]:sub a6, a6, a4
[0x800068f8]:lui a4, 1
[0x800068fc]:addi a4, a4, 2048
[0x80006900]:add a6, a6, a4
[0x80006904]:lw s10, 1720(a6)
[0x80006908]:sub a6, a6, a4
[0x8000690c]:lui a4, 1
[0x80006910]:addi a4, a4, 2048
[0x80006914]:add a6, a6, a4
[0x80006918]:lw s11, 1724(a6)
[0x8000691c]:sub a6, a6, a4
[0x80006920]:lui t3, 955128
[0x80006924]:addi t3, t3, 1736
[0x80006928]:lui t4, 523799
[0x8000692c]:addi t4, t4, 765
[0x80006930]:lui s10, 955128
[0x80006934]:addi s10, s10, 1736
[0x80006938]:lui s11, 523799
[0x8000693c]:addi s11, s11, 765
[0x80006940]:addi a4, zero, 96
[0x80006944]:csrrw zero, fcsr, a4
[0x80006948]:fsub.d t5, t3, s10, dyn
[0x8000694c]:csrrs a7, fcsr, zero

[0x80006948]:fsub.d t5, t3, s10, dyn
[0x8000694c]:csrrs a7, fcsr, zero
[0x80006950]:sw t5, 1400(ra)
[0x80006954]:sw t6, 1408(ra)
[0x80006958]:sw t5, 1416(ra)
[0x8000695c]:sw a7, 1424(ra)
[0x80006960]:lui a4, 1
[0x80006964]:addi a4, a4, 2048
[0x80006968]:add a6, a6, a4
[0x8000696c]:lw t3, 1728(a6)
[0x80006970]:sub a6, a6, a4
[0x80006974]:lui a4, 1
[0x80006978]:addi a4, a4, 2048
[0x8000697c]:add a6, a6, a4
[0x80006980]:lw t4, 1732(a6)
[0x80006984]:sub a6, a6, a4
[0x80006988]:lui a4, 1
[0x8000698c]:addi a4, a4, 2048
[0x80006990]:add a6, a6, a4
[0x80006994]:lw s10, 1736(a6)
[0x80006998]:sub a6, a6, a4
[0x8000699c]:lui a4, 1
[0x800069a0]:addi a4, a4, 2048
[0x800069a4]:add a6, a6, a4
[0x800069a8]:lw s11, 1740(a6)
[0x800069ac]:sub a6, a6, a4
[0x800069b0]:lui t3, 388699
[0x800069b4]:addi t3, t3, 776
[0x800069b8]:lui t4, 523910
[0x800069bc]:addi t4, t4, 2726
[0x800069c0]:lui s10, 388699
[0x800069c4]:addi s10, s10, 776
[0x800069c8]:lui s11, 523910
[0x800069cc]:addi s11, s11, 2726
[0x800069d0]:addi a4, zero, 96
[0x800069d4]:csrrw zero, fcsr, a4
[0x800069d8]:fsub.d t5, t3, s10, dyn
[0x800069dc]:csrrs a7, fcsr, zero

[0x800069d8]:fsub.d t5, t3, s10, dyn
[0x800069dc]:csrrs a7, fcsr, zero
[0x800069e0]:sw t5, 1432(ra)
[0x800069e4]:sw t6, 1440(ra)
[0x800069e8]:sw t5, 1448(ra)
[0x800069ec]:sw a7, 1456(ra)
[0x800069f0]:lui a4, 1
[0x800069f4]:addi a4, a4, 2048
[0x800069f8]:add a6, a6, a4
[0x800069fc]:lw t3, 1744(a6)
[0x80006a00]:sub a6, a6, a4
[0x80006a04]:lui a4, 1
[0x80006a08]:addi a4, a4, 2048
[0x80006a0c]:add a6, a6, a4
[0x80006a10]:lw t4, 1748(a6)
[0x80006a14]:sub a6, a6, a4
[0x80006a18]:lui a4, 1
[0x80006a1c]:addi a4, a4, 2048
[0x80006a20]:add a6, a6, a4
[0x80006a24]:lw s10, 1752(a6)
[0x80006a28]:sub a6, a6, a4
[0x80006a2c]:lui a4, 1
[0x80006a30]:addi a4, a4, 2048
[0x80006a34]:add a6, a6, a4
[0x80006a38]:lw s11, 1756(a6)
[0x80006a3c]:sub a6, a6, a4
[0x80006a40]:lui t3, 660287
[0x80006a44]:addi t3, t3, 943
[0x80006a48]:lui t4, 523288
[0x80006a4c]:addi t4, t4, 3049
[0x80006a50]:lui s10, 660287
[0x80006a54]:addi s10, s10, 943
[0x80006a58]:lui s11, 523288
[0x80006a5c]:addi s11, s11, 3049
[0x80006a60]:addi a4, zero, 96
[0x80006a64]:csrrw zero, fcsr, a4
[0x80006a68]:fsub.d t5, t3, s10, dyn
[0x80006a6c]:csrrs a7, fcsr, zero

[0x80006a68]:fsub.d t5, t3, s10, dyn
[0x80006a6c]:csrrs a7, fcsr, zero
[0x80006a70]:sw t5, 1464(ra)
[0x80006a74]:sw t6, 1472(ra)
[0x80006a78]:sw t5, 1480(ra)
[0x80006a7c]:sw a7, 1488(ra)
[0x80006a80]:lui a4, 1
[0x80006a84]:addi a4, a4, 2048
[0x80006a88]:add a6, a6, a4
[0x80006a8c]:lw t3, 1760(a6)
[0x80006a90]:sub a6, a6, a4
[0x80006a94]:lui a4, 1
[0x80006a98]:addi a4, a4, 2048
[0x80006a9c]:add a6, a6, a4
[0x80006aa0]:lw t4, 1764(a6)
[0x80006aa4]:sub a6, a6, a4
[0x80006aa8]:lui a4, 1
[0x80006aac]:addi a4, a4, 2048
[0x80006ab0]:add a6, a6, a4
[0x80006ab4]:lw s10, 1768(a6)
[0x80006ab8]:sub a6, a6, a4
[0x80006abc]:lui a4, 1
[0x80006ac0]:addi a4, a4, 2048
[0x80006ac4]:add a6, a6, a4
[0x80006ac8]:lw s11, 1772(a6)
[0x80006acc]:sub a6, a6, a4
[0x80006ad0]:lui t3, 734093
[0x80006ad4]:addi t3, t3, 3956
[0x80006ad8]:lui t4, 523813
[0x80006adc]:addi t4, t4, 655
[0x80006ae0]:lui s10, 734093
[0x80006ae4]:addi s10, s10, 3956
[0x80006ae8]:lui s11, 523813
[0x80006aec]:addi s11, s11, 655
[0x80006af0]:addi a4, zero, 96
[0x80006af4]:csrrw zero, fcsr, a4
[0x80006af8]:fsub.d t5, t3, s10, dyn
[0x80006afc]:csrrs a7, fcsr, zero

[0x80006af8]:fsub.d t5, t3, s10, dyn
[0x80006afc]:csrrs a7, fcsr, zero
[0x80006b00]:sw t5, 1496(ra)
[0x80006b04]:sw t6, 1504(ra)
[0x80006b08]:sw t5, 1512(ra)
[0x80006b0c]:sw a7, 1520(ra)
[0x80006b10]:lui a4, 1
[0x80006b14]:addi a4, a4, 2048
[0x80006b18]:add a6, a6, a4
[0x80006b1c]:lw t3, 1776(a6)
[0x80006b20]:sub a6, a6, a4
[0x80006b24]:lui a4, 1
[0x80006b28]:addi a4, a4, 2048
[0x80006b2c]:add a6, a6, a4
[0x80006b30]:lw t4, 1780(a6)
[0x80006b34]:sub a6, a6, a4
[0x80006b38]:lui a4, 1
[0x80006b3c]:addi a4, a4, 2048
[0x80006b40]:add a6, a6, a4
[0x80006b44]:lw s10, 1784(a6)
[0x80006b48]:sub a6, a6, a4
[0x80006b4c]:lui a4, 1
[0x80006b50]:addi a4, a4, 2048
[0x80006b54]:add a6, a6, a4
[0x80006b58]:lw s11, 1788(a6)
[0x80006b5c]:sub a6, a6, a4
[0x80006b60]:lui t3, 617304
[0x80006b64]:addi t3, t3, 1127
[0x80006b68]:lui t4, 523778
[0x80006b6c]:addi t4, t4, 2263
[0x80006b70]:lui s10, 617304
[0x80006b74]:addi s10, s10, 1127
[0x80006b78]:lui s11, 523778
[0x80006b7c]:addi s11, s11, 2263
[0x80006b80]:addi a4, zero, 96
[0x80006b84]:csrrw zero, fcsr, a4
[0x80006b88]:fsub.d t5, t3, s10, dyn
[0x80006b8c]:csrrs a7, fcsr, zero

[0x80006b88]:fsub.d t5, t3, s10, dyn
[0x80006b8c]:csrrs a7, fcsr, zero
[0x80006b90]:sw t5, 1528(ra)
[0x80006b94]:sw t6, 1536(ra)
[0x80006b98]:sw t5, 1544(ra)
[0x80006b9c]:sw a7, 1552(ra)
[0x80006ba0]:lui a4, 1
[0x80006ba4]:addi a4, a4, 2048
[0x80006ba8]:add a6, a6, a4
[0x80006bac]:lw t3, 1792(a6)
[0x80006bb0]:sub a6, a6, a4
[0x80006bb4]:lui a4, 1
[0x80006bb8]:addi a4, a4, 2048
[0x80006bbc]:add a6, a6, a4
[0x80006bc0]:lw t4, 1796(a6)
[0x80006bc4]:sub a6, a6, a4
[0x80006bc8]:lui a4, 1
[0x80006bcc]:addi a4, a4, 2048
[0x80006bd0]:add a6, a6, a4
[0x80006bd4]:lw s10, 1800(a6)
[0x80006bd8]:sub a6, a6, a4
[0x80006bdc]:lui a4, 1
[0x80006be0]:addi a4, a4, 2048
[0x80006be4]:add a6, a6, a4
[0x80006be8]:lw s11, 1804(a6)
[0x80006bec]:sub a6, a6, a4
[0x80006bf0]:lui t3, 496823
[0x80006bf4]:addi t3, t3, 1234
[0x80006bf8]:lui t4, 523791
[0x80006bfc]:addi t4, t4, 2202
[0x80006c00]:lui s10, 496823
[0x80006c04]:addi s10, s10, 1234
[0x80006c08]:lui s11, 523791
[0x80006c0c]:addi s11, s11, 2202
[0x80006c10]:addi a4, zero, 96
[0x80006c14]:csrrw zero, fcsr, a4
[0x80006c18]:fsub.d t5, t3, s10, dyn
[0x80006c1c]:csrrs a7, fcsr, zero

[0x80006c18]:fsub.d t5, t3, s10, dyn
[0x80006c1c]:csrrs a7, fcsr, zero
[0x80006c20]:sw t5, 1560(ra)
[0x80006c24]:sw t6, 1568(ra)
[0x80006c28]:sw t5, 1576(ra)
[0x80006c2c]:sw a7, 1584(ra)
[0x80006c30]:lui a4, 1
[0x80006c34]:addi a4, a4, 2048
[0x80006c38]:add a6, a6, a4
[0x80006c3c]:lw t3, 1808(a6)
[0x80006c40]:sub a6, a6, a4
[0x80006c44]:lui a4, 1
[0x80006c48]:addi a4, a4, 2048
[0x80006c4c]:add a6, a6, a4
[0x80006c50]:lw t4, 1812(a6)
[0x80006c54]:sub a6, a6, a4
[0x80006c58]:lui a4, 1
[0x80006c5c]:addi a4, a4, 2048
[0x80006c60]:add a6, a6, a4
[0x80006c64]:lw s10, 1816(a6)
[0x80006c68]:sub a6, a6, a4
[0x80006c6c]:lui a4, 1
[0x80006c70]:addi a4, a4, 2048
[0x80006c74]:add a6, a6, a4
[0x80006c78]:lw s11, 1820(a6)
[0x80006c7c]:sub a6, a6, a4
[0x80006c80]:lui t3, 809958
[0x80006c84]:addi t3, t3, 1489
[0x80006c88]:lui t4, 523717
[0x80006c8c]:addi t4, t4, 3808
[0x80006c90]:lui s10, 809958
[0x80006c94]:addi s10, s10, 1489
[0x80006c98]:lui s11, 523717
[0x80006c9c]:addi s11, s11, 3808
[0x80006ca0]:addi a4, zero, 96
[0x80006ca4]:csrrw zero, fcsr, a4
[0x80006ca8]:fsub.d t5, t3, s10, dyn
[0x80006cac]:csrrs a7, fcsr, zero

[0x80006ca8]:fsub.d t5, t3, s10, dyn
[0x80006cac]:csrrs a7, fcsr, zero
[0x80006cb0]:sw t5, 1592(ra)
[0x80006cb4]:sw t6, 1600(ra)
[0x80006cb8]:sw t5, 1608(ra)
[0x80006cbc]:sw a7, 1616(ra)
[0x80006cc0]:lui a4, 1
[0x80006cc4]:addi a4, a4, 2048
[0x80006cc8]:add a6, a6, a4
[0x80006ccc]:lw t3, 1824(a6)
[0x80006cd0]:sub a6, a6, a4
[0x80006cd4]:lui a4, 1
[0x80006cd8]:addi a4, a4, 2048
[0x80006cdc]:add a6, a6, a4
[0x80006ce0]:lw t4, 1828(a6)
[0x80006ce4]:sub a6, a6, a4
[0x80006ce8]:lui a4, 1
[0x80006cec]:addi a4, a4, 2048
[0x80006cf0]:add a6, a6, a4
[0x80006cf4]:lw s10, 1832(a6)
[0x80006cf8]:sub a6, a6, a4
[0x80006cfc]:lui a4, 1
[0x80006d00]:addi a4, a4, 2048
[0x80006d04]:add a6, a6, a4
[0x80006d08]:lw s11, 1836(a6)
[0x80006d0c]:sub a6, a6, a4
[0x80006d10]:lui t3, 476424
[0x80006d14]:addi t3, t3, 2579
[0x80006d18]:lui t4, 523576
[0x80006d1c]:addi t4, t4, 468
[0x80006d20]:lui s10, 476424
[0x80006d24]:addi s10, s10, 2579
[0x80006d28]:lui s11, 523576
[0x80006d2c]:addi s11, s11, 468
[0x80006d30]:addi a4, zero, 96
[0x80006d34]:csrrw zero, fcsr, a4
[0x80006d38]:fsub.d t5, t3, s10, dyn
[0x80006d3c]:csrrs a7, fcsr, zero

[0x80006d38]:fsub.d t5, t3, s10, dyn
[0x80006d3c]:csrrs a7, fcsr, zero
[0x80006d40]:sw t5, 1624(ra)
[0x80006d44]:sw t6, 1632(ra)
[0x80006d48]:sw t5, 1640(ra)
[0x80006d4c]:sw a7, 1648(ra)
[0x80006d50]:lui a4, 1
[0x80006d54]:addi a4, a4, 2048
[0x80006d58]:add a6, a6, a4
[0x80006d5c]:lw t3, 1840(a6)
[0x80006d60]:sub a6, a6, a4
[0x80006d64]:lui a4, 1
[0x80006d68]:addi a4, a4, 2048
[0x80006d6c]:add a6, a6, a4
[0x80006d70]:lw t4, 1844(a6)
[0x80006d74]:sub a6, a6, a4
[0x80006d78]:lui a4, 1
[0x80006d7c]:addi a4, a4, 2048
[0x80006d80]:add a6, a6, a4
[0x80006d84]:lw s10, 1848(a6)
[0x80006d88]:sub a6, a6, a4
[0x80006d8c]:lui a4, 1
[0x80006d90]:addi a4, a4, 2048
[0x80006d94]:add a6, a6, a4
[0x80006d98]:lw s11, 1852(a6)
[0x80006d9c]:sub a6, a6, a4
[0x80006da0]:lui t3, 126667
[0x80006da4]:addi t3, t3, 3658
[0x80006da8]:lui t4, 523921
[0x80006dac]:addi t4, t4, 3853
[0x80006db0]:lui s10, 126667
[0x80006db4]:addi s10, s10, 3658
[0x80006db8]:lui s11, 523921
[0x80006dbc]:addi s11, s11, 3853
[0x80006dc0]:addi a4, zero, 96
[0x80006dc4]:csrrw zero, fcsr, a4
[0x80006dc8]:fsub.d t5, t3, s10, dyn
[0x80006dcc]:csrrs a7, fcsr, zero

[0x80006dc8]:fsub.d t5, t3, s10, dyn
[0x80006dcc]:csrrs a7, fcsr, zero
[0x80006dd0]:sw t5, 1656(ra)
[0x80006dd4]:sw t6, 1664(ra)
[0x80006dd8]:sw t5, 1672(ra)
[0x80006ddc]:sw a7, 1680(ra)
[0x80006de0]:lui a4, 1
[0x80006de4]:addi a4, a4, 2048
[0x80006de8]:add a6, a6, a4
[0x80006dec]:lw t3, 1856(a6)
[0x80006df0]:sub a6, a6, a4
[0x80006df4]:lui a4, 1
[0x80006df8]:addi a4, a4, 2048
[0x80006dfc]:add a6, a6, a4
[0x80006e00]:lw t4, 1860(a6)
[0x80006e04]:sub a6, a6, a4
[0x80006e08]:lui a4, 1
[0x80006e0c]:addi a4, a4, 2048
[0x80006e10]:add a6, a6, a4
[0x80006e14]:lw s10, 1864(a6)
[0x80006e18]:sub a6, a6, a4
[0x80006e1c]:lui a4, 1
[0x80006e20]:addi a4, a4, 2048
[0x80006e24]:add a6, a6, a4
[0x80006e28]:lw s11, 1868(a6)
[0x80006e2c]:sub a6, a6, a4
[0x80006e30]:lui t3, 233082
[0x80006e34]:addi t3, t3, 3721
[0x80006e38]:lui t4, 523768
[0x80006e3c]:addi t4, t4, 468
[0x80006e40]:lui s10, 233082
[0x80006e44]:addi s10, s10, 3721
[0x80006e48]:lui s11, 523768
[0x80006e4c]:addi s11, s11, 468
[0x80006e50]:addi a4, zero, 96
[0x80006e54]:csrrw zero, fcsr, a4
[0x80006e58]:fsub.d t5, t3, s10, dyn
[0x80006e5c]:csrrs a7, fcsr, zero

[0x80006e58]:fsub.d t5, t3, s10, dyn
[0x80006e5c]:csrrs a7, fcsr, zero
[0x80006e60]:sw t5, 1688(ra)
[0x80006e64]:sw t6, 1696(ra)
[0x80006e68]:sw t5, 1704(ra)
[0x80006e6c]:sw a7, 1712(ra)
[0x80006e70]:lui a4, 1
[0x80006e74]:addi a4, a4, 2048
[0x80006e78]:add a6, a6, a4
[0x80006e7c]:lw t3, 1872(a6)
[0x80006e80]:sub a6, a6, a4
[0x80006e84]:lui a4, 1
[0x80006e88]:addi a4, a4, 2048
[0x80006e8c]:add a6, a6, a4
[0x80006e90]:lw t4, 1876(a6)
[0x80006e94]:sub a6, a6, a4
[0x80006e98]:lui a4, 1
[0x80006e9c]:addi a4, a4, 2048
[0x80006ea0]:add a6, a6, a4
[0x80006ea4]:lw s10, 1880(a6)
[0x80006ea8]:sub a6, a6, a4
[0x80006eac]:lui a4, 1
[0x80006eb0]:addi a4, a4, 2048
[0x80006eb4]:add a6, a6, a4
[0x80006eb8]:lw s11, 1884(a6)
[0x80006ebc]:sub a6, a6, a4
[0x80006ec0]:lui t3, 962802
[0x80006ec4]:addi t3, t3, 51
[0x80006ec8]:lui t4, 523651
[0x80006ecc]:addi t4, t4, 2537
[0x80006ed0]:lui s10, 962802
[0x80006ed4]:addi s10, s10, 51
[0x80006ed8]:lui s11, 523651
[0x80006edc]:addi s11, s11, 2537
[0x80006ee0]:addi a4, zero, 96
[0x80006ee4]:csrrw zero, fcsr, a4
[0x80006ee8]:fsub.d t5, t3, s10, dyn
[0x80006eec]:csrrs a7, fcsr, zero

[0x80006ee8]:fsub.d t5, t3, s10, dyn
[0x80006eec]:csrrs a7, fcsr, zero
[0x80006ef0]:sw t5, 1720(ra)
[0x80006ef4]:sw t6, 1728(ra)
[0x80006ef8]:sw t5, 1736(ra)
[0x80006efc]:sw a7, 1744(ra)
[0x80006f00]:lui a4, 1
[0x80006f04]:addi a4, a4, 2048
[0x80006f08]:add a6, a6, a4
[0x80006f0c]:lw t3, 1888(a6)
[0x80006f10]:sub a6, a6, a4
[0x80006f14]:lui a4, 1
[0x80006f18]:addi a4, a4, 2048
[0x80006f1c]:add a6, a6, a4
[0x80006f20]:lw t4, 1892(a6)
[0x80006f24]:sub a6, a6, a4
[0x80006f28]:lui a4, 1
[0x80006f2c]:addi a4, a4, 2048
[0x80006f30]:add a6, a6, a4
[0x80006f34]:lw s10, 1896(a6)
[0x80006f38]:sub a6, a6, a4
[0x80006f3c]:lui a4, 1
[0x80006f40]:addi a4, a4, 2048
[0x80006f44]:add a6, a6, a4
[0x80006f48]:lw s11, 1900(a6)
[0x80006f4c]:sub a6, a6, a4
[0x80006f50]:lui t3, 1026895
[0x80006f54]:addi t3, t3, 3839
[0x80006f58]:lui t4, 521980
[0x80006f5c]:addi t4, t4, 4055
[0x80006f60]:lui s10, 1026895
[0x80006f64]:addi s10, s10, 3839
[0x80006f68]:lui s11, 521980
[0x80006f6c]:addi s11, s11, 4055
[0x80006f70]:addi a4, zero, 96
[0x80006f74]:csrrw zero, fcsr, a4
[0x80006f78]:fsub.d t5, t3, s10, dyn
[0x80006f7c]:csrrs a7, fcsr, zero

[0x80006f78]:fsub.d t5, t3, s10, dyn
[0x80006f7c]:csrrs a7, fcsr, zero
[0x80006f80]:sw t5, 1752(ra)
[0x80006f84]:sw t6, 1760(ra)
[0x80006f88]:sw t5, 1768(ra)
[0x80006f8c]:sw a7, 1776(ra)
[0x80006f90]:lui a4, 1
[0x80006f94]:addi a4, a4, 2048
[0x80006f98]:add a6, a6, a4
[0x80006f9c]:lw t3, 1904(a6)
[0x80006fa0]:sub a6, a6, a4
[0x80006fa4]:lui a4, 1
[0x80006fa8]:addi a4, a4, 2048
[0x80006fac]:add a6, a6, a4
[0x80006fb0]:lw t4, 1908(a6)
[0x80006fb4]:sub a6, a6, a4
[0x80006fb8]:lui a4, 1
[0x80006fbc]:addi a4, a4, 2048
[0x80006fc0]:add a6, a6, a4
[0x80006fc4]:lw s10, 1912(a6)
[0x80006fc8]:sub a6, a6, a4
[0x80006fcc]:lui a4, 1
[0x80006fd0]:addi a4, a4, 2048
[0x80006fd4]:add a6, a6, a4
[0x80006fd8]:lw s11, 1916(a6)
[0x80006fdc]:sub a6, a6, a4
[0x80006fe0]:lui t3, 257594
[0x80006fe4]:addi t3, t3, 3965
[0x80006fe8]:lui t4, 523826
[0x80006fec]:addi t4, t4, 1065
[0x80006ff0]:lui s10, 257594
[0x80006ff4]:addi s10, s10, 3965
[0x80006ff8]:lui s11, 523826
[0x80006ffc]:addi s11, s11, 1065
[0x80007000]:addi a4, zero, 96
[0x80007004]:csrrw zero, fcsr, a4
[0x80007008]:fsub.d t5, t3, s10, dyn
[0x8000700c]:csrrs a7, fcsr, zero

[0x80007008]:fsub.d t5, t3, s10, dyn
[0x8000700c]:csrrs a7, fcsr, zero
[0x80007010]:sw t5, 1784(ra)
[0x80007014]:sw t6, 1792(ra)
[0x80007018]:sw t5, 1800(ra)
[0x8000701c]:sw a7, 1808(ra)
[0x80007020]:lui a4, 1
[0x80007024]:addi a4, a4, 2048
[0x80007028]:add a6, a6, a4
[0x8000702c]:lw t3, 1920(a6)
[0x80007030]:sub a6, a6, a4
[0x80007034]:lui a4, 1
[0x80007038]:addi a4, a4, 2048
[0x8000703c]:add a6, a6, a4
[0x80007040]:lw t4, 1924(a6)
[0x80007044]:sub a6, a6, a4
[0x80007048]:lui a4, 1
[0x8000704c]:addi a4, a4, 2048
[0x80007050]:add a6, a6, a4
[0x80007054]:lw s10, 1928(a6)
[0x80007058]:sub a6, a6, a4
[0x8000705c]:lui a4, 1
[0x80007060]:addi a4, a4, 2048
[0x80007064]:add a6, a6, a4
[0x80007068]:lw s11, 1932(a6)
[0x8000706c]:sub a6, a6, a4
[0x80007070]:lui t3, 454302
[0x80007074]:addi t3, t3, 3221
[0x80007078]:lui t4, 523945
[0x8000707c]:addi t4, t4, 2718
[0x80007080]:lui s10, 454302
[0x80007084]:addi s10, s10, 3221
[0x80007088]:lui s11, 523945
[0x8000708c]:addi s11, s11, 2718
[0x80007090]:addi a4, zero, 96
[0x80007094]:csrrw zero, fcsr, a4
[0x80007098]:fsub.d t5, t3, s10, dyn
[0x8000709c]:csrrs a7, fcsr, zero

[0x80007098]:fsub.d t5, t3, s10, dyn
[0x8000709c]:csrrs a7, fcsr, zero
[0x800070a0]:sw t5, 1816(ra)
[0x800070a4]:sw t6, 1824(ra)
[0x800070a8]:sw t5, 1832(ra)
[0x800070ac]:sw a7, 1840(ra)
[0x800070b0]:lui a4, 1
[0x800070b4]:addi a4, a4, 2048
[0x800070b8]:add a6, a6, a4
[0x800070bc]:lw t3, 1936(a6)
[0x800070c0]:sub a6, a6, a4
[0x800070c4]:lui a4, 1
[0x800070c8]:addi a4, a4, 2048
[0x800070cc]:add a6, a6, a4
[0x800070d0]:lw t4, 1940(a6)
[0x800070d4]:sub a6, a6, a4
[0x800070d8]:lui a4, 1
[0x800070dc]:addi a4, a4, 2048
[0x800070e0]:add a6, a6, a4
[0x800070e4]:lw s10, 1944(a6)
[0x800070e8]:sub a6, a6, a4
[0x800070ec]:lui a4, 1
[0x800070f0]:addi a4, a4, 2048
[0x800070f4]:add a6, a6, a4
[0x800070f8]:lw s11, 1948(a6)
[0x800070fc]:sub a6, a6, a4
[0x80007100]:lui t3, 80663
[0x80007104]:addi t3, t3, 1994
[0x80007108]:lui t4, 524025
[0x8000710c]:addi t4, t4, 1447
[0x80007110]:lui s10, 80663
[0x80007114]:addi s10, s10, 1994
[0x80007118]:lui s11, 524025
[0x8000711c]:addi s11, s11, 1447
[0x80007120]:addi a4, zero, 96
[0x80007124]:csrrw zero, fcsr, a4
[0x80007128]:fsub.d t5, t3, s10, dyn
[0x8000712c]:csrrs a7, fcsr, zero

[0x80007128]:fsub.d t5, t3, s10, dyn
[0x8000712c]:csrrs a7, fcsr, zero
[0x80007130]:sw t5, 1848(ra)
[0x80007134]:sw t6, 1856(ra)
[0x80007138]:sw t5, 1864(ra)
[0x8000713c]:sw a7, 1872(ra)
[0x80007140]:lui a4, 1
[0x80007144]:addi a4, a4, 2048
[0x80007148]:add a6, a6, a4
[0x8000714c]:lw t3, 1952(a6)
[0x80007150]:sub a6, a6, a4
[0x80007154]:lui a4, 1
[0x80007158]:addi a4, a4, 2048
[0x8000715c]:add a6, a6, a4
[0x80007160]:lw t4, 1956(a6)
[0x80007164]:sub a6, a6, a4
[0x80007168]:lui a4, 1
[0x8000716c]:addi a4, a4, 2048
[0x80007170]:add a6, a6, a4
[0x80007174]:lw s10, 1960(a6)
[0x80007178]:sub a6, a6, a4
[0x8000717c]:lui a4, 1
[0x80007180]:addi a4, a4, 2048
[0x80007184]:add a6, a6, a4
[0x80007188]:lw s11, 1964(a6)
[0x8000718c]:sub a6, a6, a4
[0x80007190]:lui t3, 418069
[0x80007194]:addi t3, t3, 383
[0x80007198]:lui t4, 523947
[0x8000719c]:addi t4, t4, 1277
[0x800071a0]:lui s10, 418069
[0x800071a4]:addi s10, s10, 383
[0x800071a8]:lui s11, 523947
[0x800071ac]:addi s11, s11, 1277
[0x800071b0]:addi a4, zero, 96
[0x800071b4]:csrrw zero, fcsr, a4
[0x800071b8]:fsub.d t5, t3, s10, dyn
[0x800071bc]:csrrs a7, fcsr, zero

[0x800071b8]:fsub.d t5, t3, s10, dyn
[0x800071bc]:csrrs a7, fcsr, zero
[0x800071c0]:sw t5, 1880(ra)
[0x800071c4]:sw t6, 1888(ra)
[0x800071c8]:sw t5, 1896(ra)
[0x800071cc]:sw a7, 1904(ra)
[0x800071d0]:lui a4, 1
[0x800071d4]:addi a4, a4, 2048
[0x800071d8]:add a6, a6, a4
[0x800071dc]:lw t3, 1968(a6)
[0x800071e0]:sub a6, a6, a4
[0x800071e4]:lui a4, 1
[0x800071e8]:addi a4, a4, 2048
[0x800071ec]:add a6, a6, a4
[0x800071f0]:lw t4, 1972(a6)
[0x800071f4]:sub a6, a6, a4
[0x800071f8]:lui a4, 1
[0x800071fc]:addi a4, a4, 2048
[0x80007200]:add a6, a6, a4
[0x80007204]:lw s10, 1976(a6)
[0x80007208]:sub a6, a6, a4
[0x8000720c]:lui a4, 1
[0x80007210]:addi a4, a4, 2048
[0x80007214]:add a6, a6, a4
[0x80007218]:lw s11, 1980(a6)
[0x8000721c]:sub a6, a6, a4
[0x80007220]:lui t3, 617876
[0x80007224]:addi t3, t3, 2139
[0x80007228]:lui t4, 523901
[0x8000722c]:addi t4, t4, 1027
[0x80007230]:lui s10, 617876
[0x80007234]:addi s10, s10, 2139
[0x80007238]:lui s11, 523901
[0x8000723c]:addi s11, s11, 1027
[0x80007240]:addi a4, zero, 96
[0x80007244]:csrrw zero, fcsr, a4
[0x80007248]:fsub.d t5, t3, s10, dyn
[0x8000724c]:csrrs a7, fcsr, zero

[0x80007248]:fsub.d t5, t3, s10, dyn
[0x8000724c]:csrrs a7, fcsr, zero
[0x80007250]:sw t5, 1912(ra)
[0x80007254]:sw t6, 1920(ra)
[0x80007258]:sw t5, 1928(ra)
[0x8000725c]:sw a7, 1936(ra)
[0x80007260]:lui a4, 1
[0x80007264]:addi a4, a4, 2048
[0x80007268]:add a6, a6, a4
[0x8000726c]:lw t3, 1984(a6)
[0x80007270]:sub a6, a6, a4
[0x80007274]:lui a4, 1
[0x80007278]:addi a4, a4, 2048
[0x8000727c]:add a6, a6, a4
[0x80007280]:lw t4, 1988(a6)
[0x80007284]:sub a6, a6, a4
[0x80007288]:lui a4, 1
[0x8000728c]:addi a4, a4, 2048
[0x80007290]:add a6, a6, a4
[0x80007294]:lw s10, 1992(a6)
[0x80007298]:sub a6, a6, a4
[0x8000729c]:lui a4, 1
[0x800072a0]:addi a4, a4, 2048
[0x800072a4]:add a6, a6, a4
[0x800072a8]:lw s11, 1996(a6)
[0x800072ac]:sub a6, a6, a4
[0x800072b0]:lui t3, 514552
[0x800072b4]:addi t3, t3, 1481
[0x800072b8]:lui t4, 523577
[0x800072bc]:addi t4, t4, 2722
[0x800072c0]:lui s10, 514552
[0x800072c4]:addi s10, s10, 1481
[0x800072c8]:lui s11, 523577
[0x800072cc]:addi s11, s11, 2722
[0x800072d0]:addi a4, zero, 96
[0x800072d4]:csrrw zero, fcsr, a4
[0x800072d8]:fsub.d t5, t3, s10, dyn
[0x800072dc]:csrrs a7, fcsr, zero

[0x800072d8]:fsub.d t5, t3, s10, dyn
[0x800072dc]:csrrs a7, fcsr, zero
[0x800072e0]:sw t5, 1944(ra)
[0x800072e4]:sw t6, 1952(ra)
[0x800072e8]:sw t5, 1960(ra)
[0x800072ec]:sw a7, 1968(ra)
[0x800072f0]:lui a4, 1
[0x800072f4]:addi a4, a4, 2048
[0x800072f8]:add a6, a6, a4
[0x800072fc]:lw t3, 2000(a6)
[0x80007300]:sub a6, a6, a4
[0x80007304]:lui a4, 1
[0x80007308]:addi a4, a4, 2048
[0x8000730c]:add a6, a6, a4
[0x80007310]:lw t4, 2004(a6)
[0x80007314]:sub a6, a6, a4
[0x80007318]:lui a4, 1
[0x8000731c]:addi a4, a4, 2048
[0x80007320]:add a6, a6, a4
[0x80007324]:lw s10, 2008(a6)
[0x80007328]:sub a6, a6, a4
[0x8000732c]:lui a4, 1
[0x80007330]:addi a4, a4, 2048
[0x80007334]:add a6, a6, a4
[0x80007338]:lw s11, 2012(a6)
[0x8000733c]:sub a6, a6, a4
[0x80007340]:lui t3, 977614
[0x80007344]:addi t3, t3, 1151
[0x80007348]:lui t4, 523837
[0x8000734c]:addi t4, t4, 1872
[0x80007350]:lui s10, 977614
[0x80007354]:addi s10, s10, 1151
[0x80007358]:lui s11, 523837
[0x8000735c]:addi s11, s11, 1872
[0x80007360]:addi a4, zero, 96
[0x80007364]:csrrw zero, fcsr, a4
[0x80007368]:fsub.d t5, t3, s10, dyn
[0x8000736c]:csrrs a7, fcsr, zero

[0x80007368]:fsub.d t5, t3, s10, dyn
[0x8000736c]:csrrs a7, fcsr, zero
[0x80007370]:sw t5, 1976(ra)
[0x80007374]:sw t6, 1984(ra)
[0x80007378]:sw t5, 1992(ra)
[0x8000737c]:sw a7, 2000(ra)
[0x80007380]:lui a4, 1
[0x80007384]:addi a4, a4, 2048
[0x80007388]:add a6, a6, a4
[0x8000738c]:lw t3, 2016(a6)
[0x80007390]:sub a6, a6, a4
[0x80007394]:lui a4, 1
[0x80007398]:addi a4, a4, 2048
[0x8000739c]:add a6, a6, a4
[0x800073a0]:lw t4, 2020(a6)
[0x800073a4]:sub a6, a6, a4
[0x800073a8]:lui a4, 1
[0x800073ac]:addi a4, a4, 2048
[0x800073b0]:add a6, a6, a4
[0x800073b4]:lw s10, 2024(a6)
[0x800073b8]:sub a6, a6, a4
[0x800073bc]:lui a4, 1
[0x800073c0]:addi a4, a4, 2048
[0x800073c4]:add a6, a6, a4
[0x800073c8]:lw s11, 2028(a6)
[0x800073cc]:sub a6, a6, a4
[0x800073d0]:lui t3, 279386
[0x800073d4]:addi t3, t3, 887
[0x800073d8]:lui t4, 523632
[0x800073dc]:addi t4, t4, 197
[0x800073e0]:lui s10, 279386
[0x800073e4]:addi s10, s10, 887
[0x800073e8]:lui s11, 523632
[0x800073ec]:addi s11, s11, 197
[0x800073f0]:addi a4, zero, 96
[0x800073f4]:csrrw zero, fcsr, a4
[0x800073f8]:fsub.d t5, t3, s10, dyn
[0x800073fc]:csrrs a7, fcsr, zero

[0x800073f8]:fsub.d t5, t3, s10, dyn
[0x800073fc]:csrrs a7, fcsr, zero
[0x80007400]:sw t5, 2008(ra)
[0x80007404]:sw t6, 2016(ra)
[0x80007408]:sw t5, 2024(ra)
[0x8000740c]:sw a7, 2032(ra)
[0x80007410]:lui a4, 1
[0x80007414]:addi a4, a4, 2048
[0x80007418]:add a6, a6, a4
[0x8000741c]:lw t3, 2032(a6)
[0x80007420]:sub a6, a6, a4
[0x80007424]:lui a4, 1
[0x80007428]:addi a4, a4, 2048
[0x8000742c]:add a6, a6, a4
[0x80007430]:lw t4, 2036(a6)
[0x80007434]:sub a6, a6, a4
[0x80007438]:lui a4, 1
[0x8000743c]:addi a4, a4, 2048
[0x80007440]:add a6, a6, a4
[0x80007444]:lw s10, 2040(a6)
[0x80007448]:sub a6, a6, a4
[0x8000744c]:lui a4, 1
[0x80007450]:addi a4, a4, 2048
[0x80007454]:add a6, a6, a4
[0x80007458]:lw s11, 2044(a6)
[0x8000745c]:sub a6, a6, a4
[0x80007460]:lui t3, 444968
[0x80007464]:addi t3, t3, 65
[0x80007468]:lui t4, 523663
[0x8000746c]:addi t4, t4, 2058
[0x80007470]:lui s10, 444968
[0x80007474]:addi s10, s10, 65
[0x80007478]:lui s11, 523663
[0x8000747c]:addi s11, s11, 2058
[0x80007480]:addi a4, zero, 96
[0x80007484]:csrrw zero, fcsr, a4
[0x80007488]:fsub.d t5, t3, s10, dyn
[0x8000748c]:csrrs a7, fcsr, zero

[0x80007488]:fsub.d t5, t3, s10, dyn
[0x8000748c]:csrrs a7, fcsr, zero
[0x80007490]:sw t5, 2040(ra)
[0x80007494]:addi ra, ra, 2040
[0x80007498]:sw t6, 8(ra)
[0x8000749c]:sw t5, 16(ra)
[0x800074a0]:sw a7, 24(ra)
[0x800074a4]:auipc ra, 5
[0x800074a8]:addi ra, ra, 804
[0x800074ac]:lw t3, 0(a6)
[0x800074b0]:lw t4, 4(a6)
[0x800074b4]:lw s10, 8(a6)
[0x800074b8]:lw s11, 12(a6)
[0x800074bc]:lui t3, 404226
[0x800074c0]:addi t3, t3, 1563
[0x800074c4]:lui t4, 523679
[0x800074c8]:addi t4, t4, 4006
[0x800074cc]:lui s10, 404226
[0x800074d0]:addi s10, s10, 1563
[0x800074d4]:lui s11, 523679
[0x800074d8]:addi s11, s11, 4006
[0x800074dc]:addi a4, zero, 96
[0x800074e0]:csrrw zero, fcsr, a4
[0x800074e4]:fsub.d t5, t3, s10, dyn
[0x800074e8]:csrrs a7, fcsr, zero

[0x80008024]:fsub.d t5, t3, s10, dyn
[0x80008028]:csrrs a7, fcsr, zero
[0x8000802c]:sw t5, 1152(ra)
[0x80008030]:sw t6, 1160(ra)
[0x80008034]:sw t5, 1168(ra)
[0x80008038]:sw a7, 1176(ra)
[0x8000803c]:lw t3, 592(a6)
[0x80008040]:lw t4, 596(a6)
[0x80008044]:lw s10, 600(a6)
[0x80008048]:lw s11, 604(a6)
[0x8000804c]:lui t3, 266553
[0x80008050]:addi t3, t3, 3253
[0x80008054]:lui t4, 523922
[0x80008058]:addi t4, t4, 536
[0x8000805c]:lui s10, 266553
[0x80008060]:addi s10, s10, 3253
[0x80008064]:lui s11, 523922
[0x80008068]:addi s11, s11, 536
[0x8000806c]:addi a4, zero, 96
[0x80008070]:csrrw zero, fcsr, a4
[0x80008074]:fsub.d t5, t3, s10, dyn
[0x80008078]:csrrs a7, fcsr, zero

[0x80008074]:fsub.d t5, t3, s10, dyn
[0x80008078]:csrrs a7, fcsr, zero
[0x8000807c]:sw t5, 1184(ra)
[0x80008080]:sw t6, 1192(ra)
[0x80008084]:sw t5, 1200(ra)
[0x80008088]:sw a7, 1208(ra)
[0x8000808c]:lw t3, 608(a6)
[0x80008090]:lw t4, 612(a6)
[0x80008094]:lw s10, 616(a6)
[0x80008098]:lw s11, 620(a6)
[0x8000809c]:lui t3, 143070
[0x800080a0]:addi t3, t3, 1593
[0x800080a4]:lui t4, 523840
[0x800080a8]:addi t4, t4, 1859
[0x800080ac]:lui s10, 143070
[0x800080b0]:addi s10, s10, 1593
[0x800080b4]:lui s11, 523840
[0x800080b8]:addi s11, s11, 1859
[0x800080bc]:addi a4, zero, 96
[0x800080c0]:csrrw zero, fcsr, a4
[0x800080c4]:fsub.d t5, t3, s10, dyn
[0x800080c8]:csrrs a7, fcsr, zero

[0x800080c4]:fsub.d t5, t3, s10, dyn
[0x800080c8]:csrrs a7, fcsr, zero
[0x800080cc]:sw t5, 1216(ra)
[0x800080d0]:sw t6, 1224(ra)
[0x800080d4]:sw t5, 1232(ra)
[0x800080d8]:sw a7, 1240(ra)
[0x800080dc]:lw t3, 624(a6)
[0x800080e0]:lw t4, 628(a6)
[0x800080e4]:lw s10, 632(a6)
[0x800080e8]:lw s11, 636(a6)
[0x800080ec]:lui t3, 419089
[0x800080f0]:addi t3, t3, 3020
[0x800080f4]:lui t4, 524018
[0x800080f8]:addi t4, t4, 825
[0x800080fc]:lui s10, 419089
[0x80008100]:addi s10, s10, 3020
[0x80008104]:lui s11, 524018
[0x80008108]:addi s11, s11, 825
[0x8000810c]:addi a4, zero, 96
[0x80008110]:csrrw zero, fcsr, a4
[0x80008114]:fsub.d t5, t3, s10, dyn
[0x80008118]:csrrs a7, fcsr, zero

[0x80008114]:fsub.d t5, t3, s10, dyn
[0x80008118]:csrrs a7, fcsr, zero
[0x8000811c]:sw t5, 1248(ra)
[0x80008120]:sw t6, 1256(ra)
[0x80008124]:sw t5, 1264(ra)
[0x80008128]:sw a7, 1272(ra)
[0x8000812c]:lw t3, 640(a6)
[0x80008130]:lw t4, 644(a6)
[0x80008134]:lw s10, 648(a6)
[0x80008138]:lw s11, 652(a6)
[0x8000813c]:lui t3, 916405
[0x80008140]:addi t3, t3, 710
[0x80008144]:lui t4, 523988
[0x80008148]:addi t4, t4, 2171
[0x8000814c]:lui s10, 916405
[0x80008150]:addi s10, s10, 710
[0x80008154]:lui s11, 523988
[0x80008158]:addi s11, s11, 2171
[0x8000815c]:addi a4, zero, 96
[0x80008160]:csrrw zero, fcsr, a4
[0x80008164]:fsub.d t5, t3, s10, dyn
[0x80008168]:csrrs a7, fcsr, zero

[0x80008164]:fsub.d t5, t3, s10, dyn
[0x80008168]:csrrs a7, fcsr, zero
[0x8000816c]:sw t5, 1280(ra)
[0x80008170]:sw t6, 1288(ra)
[0x80008174]:sw t5, 1296(ra)
[0x80008178]:sw a7, 1304(ra)
[0x8000817c]:lw t3, 656(a6)
[0x80008180]:lw t4, 660(a6)
[0x80008184]:lw s10, 664(a6)
[0x80008188]:lw s11, 668(a6)
[0x8000818c]:lui t3, 179331
[0x80008190]:addi t3, t3, 2763
[0x80008194]:lui t4, 523830
[0x80008198]:addi t4, t4, 1659
[0x8000819c]:lui s10, 179331
[0x800081a0]:addi s10, s10, 2763
[0x800081a4]:lui s11, 523830
[0x800081a8]:addi s11, s11, 1659
[0x800081ac]:addi a4, zero, 96
[0x800081b0]:csrrw zero, fcsr, a4
[0x800081b4]:fsub.d t5, t3, s10, dyn
[0x800081b8]:csrrs a7, fcsr, zero

[0x800081b4]:fsub.d t5, t3, s10, dyn
[0x800081b8]:csrrs a7, fcsr, zero
[0x800081bc]:sw t5, 1312(ra)
[0x800081c0]:sw t6, 1320(ra)
[0x800081c4]:sw t5, 1328(ra)
[0x800081c8]:sw a7, 1336(ra)
[0x800081cc]:lw t3, 672(a6)
[0x800081d0]:lw t4, 676(a6)
[0x800081d4]:lw s10, 680(a6)
[0x800081d8]:lw s11, 684(a6)
[0x800081dc]:lui t3, 150119
[0x800081e0]:addi t3, t3, 4039
[0x800081e4]:lui t4, 523790
[0x800081e8]:addi t4, t4, 2520
[0x800081ec]:lui s10, 150119
[0x800081f0]:addi s10, s10, 4039
[0x800081f4]:lui s11, 523790
[0x800081f8]:addi s11, s11, 2520
[0x800081fc]:addi a4, zero, 96
[0x80008200]:csrrw zero, fcsr, a4
[0x80008204]:fsub.d t5, t3, s10, dyn
[0x80008208]:csrrs a7, fcsr, zero

[0x80008204]:fsub.d t5, t3, s10, dyn
[0x80008208]:csrrs a7, fcsr, zero
[0x8000820c]:sw t5, 1344(ra)
[0x80008210]:sw t6, 1352(ra)
[0x80008214]:sw t5, 1360(ra)
[0x80008218]:sw a7, 1368(ra)
[0x8000821c]:lw t3, 688(a6)
[0x80008220]:lw t4, 692(a6)
[0x80008224]:lw s10, 696(a6)
[0x80008228]:lw s11, 700(a6)
[0x8000822c]:lui t3, 976085
[0x80008230]:addi t3, t3, 2775
[0x80008234]:lui t4, 523291
[0x80008238]:addi t4, t4, 3377
[0x8000823c]:lui s10, 976085
[0x80008240]:addi s10, s10, 2775
[0x80008244]:lui s11, 523291
[0x80008248]:addi s11, s11, 3377
[0x8000824c]:addi a4, zero, 96
[0x80008250]:csrrw zero, fcsr, a4
[0x80008254]:fsub.d t5, t3, s10, dyn
[0x80008258]:csrrs a7, fcsr, zero

[0x80008254]:fsub.d t5, t3, s10, dyn
[0x80008258]:csrrs a7, fcsr, zero
[0x8000825c]:sw t5, 1376(ra)
[0x80008260]:sw t6, 1384(ra)
[0x80008264]:sw t5, 1392(ra)
[0x80008268]:sw a7, 1400(ra)
[0x8000826c]:lw t3, 704(a6)
[0x80008270]:lw t4, 708(a6)
[0x80008274]:lw s10, 712(a6)
[0x80008278]:lw s11, 716(a6)
[0x8000827c]:lui t3, 272294
[0x80008280]:addi t3, t3, 1131
[0x80008284]:lui t4, 523634
[0x80008288]:addi t4, t4, 3096
[0x8000828c]:lui s10, 272294
[0x80008290]:addi s10, s10, 1131
[0x80008294]:lui s11, 523634
[0x80008298]:addi s11, s11, 3096
[0x8000829c]:addi a4, zero, 96
[0x800082a0]:csrrw zero, fcsr, a4
[0x800082a4]:fsub.d t5, t3, s10, dyn
[0x800082a8]:csrrs a7, fcsr, zero

[0x800082a4]:fsub.d t5, t3, s10, dyn
[0x800082a8]:csrrs a7, fcsr, zero
[0x800082ac]:sw t5, 1408(ra)
[0x800082b0]:sw t6, 1416(ra)
[0x800082b4]:sw t5, 1424(ra)
[0x800082b8]:sw a7, 1432(ra)
[0x800082bc]:lw t3, 720(a6)
[0x800082c0]:lw t4, 724(a6)
[0x800082c4]:lw s10, 728(a6)
[0x800082c8]:lw s11, 732(a6)
[0x800082cc]:lui t3, 597415
[0x800082d0]:addi t3, t3, 2007
[0x800082d4]:lui t4, 523501
[0x800082d8]:addi t4, t4, 2174
[0x800082dc]:lui s10, 597415
[0x800082e0]:addi s10, s10, 2007
[0x800082e4]:lui s11, 523501
[0x800082e8]:addi s11, s11, 2174
[0x800082ec]:addi a4, zero, 96
[0x800082f0]:csrrw zero, fcsr, a4
[0x800082f4]:fsub.d t5, t3, s10, dyn
[0x800082f8]:csrrs a7, fcsr, zero

[0x800082f4]:fsub.d t5, t3, s10, dyn
[0x800082f8]:csrrs a7, fcsr, zero
[0x800082fc]:sw t5, 1440(ra)
[0x80008300]:sw t6, 1448(ra)
[0x80008304]:sw t5, 1456(ra)
[0x80008308]:sw a7, 1464(ra)
[0x8000830c]:lw t3, 736(a6)
[0x80008310]:lw t4, 740(a6)
[0x80008314]:lw s10, 744(a6)
[0x80008318]:lw s11, 748(a6)
[0x8000831c]:lui t3, 338844
[0x80008320]:addi t3, t3, 146
[0x80008324]:lui t4, 523888
[0x80008328]:addi t4, t4, 3523
[0x8000832c]:lui s10, 338844
[0x80008330]:addi s10, s10, 146
[0x80008334]:lui s11, 523888
[0x80008338]:addi s11, s11, 3523
[0x8000833c]:addi a4, zero, 96
[0x80008340]:csrrw zero, fcsr, a4
[0x80008344]:fsub.d t5, t3, s10, dyn
[0x80008348]:csrrs a7, fcsr, zero

[0x80008344]:fsub.d t5, t3, s10, dyn
[0x80008348]:csrrs a7, fcsr, zero
[0x8000834c]:sw t5, 1472(ra)
[0x80008350]:sw t6, 1480(ra)
[0x80008354]:sw t5, 1488(ra)
[0x80008358]:sw a7, 1496(ra)
[0x8000835c]:lw t3, 752(a6)
[0x80008360]:lw t4, 756(a6)
[0x80008364]:lw s10, 760(a6)
[0x80008368]:lw s11, 764(a6)
[0x8000836c]:lui t3, 423051
[0x80008370]:addi t3, t3, 2327
[0x80008374]:lui t4, 523188
[0x80008378]:addi t4, t4, 3091
[0x8000837c]:lui s10, 423051
[0x80008380]:addi s10, s10, 2327
[0x80008384]:lui s11, 523188
[0x80008388]:addi s11, s11, 3091
[0x8000838c]:addi a4, zero, 96
[0x80008390]:csrrw zero, fcsr, a4
[0x80008394]:fsub.d t5, t3, s10, dyn
[0x80008398]:csrrs a7, fcsr, zero

[0x80008394]:fsub.d t5, t3, s10, dyn
[0x80008398]:csrrs a7, fcsr, zero
[0x8000839c]:sw t5, 1504(ra)
[0x800083a0]:sw t6, 1512(ra)
[0x800083a4]:sw t5, 1520(ra)
[0x800083a8]:sw a7, 1528(ra)
[0x800083ac]:lw t3, 768(a6)
[0x800083b0]:lw t4, 772(a6)
[0x800083b4]:lw s10, 776(a6)
[0x800083b8]:lw s11, 780(a6)
[0x800083bc]:lui t3, 849629
[0x800083c0]:addi t3, t3, 2341
[0x800083c4]:lui t4, 523768
[0x800083c8]:addi t4, t4, 729
[0x800083cc]:lui s10, 849629
[0x800083d0]:addi s10, s10, 2341
[0x800083d4]:lui s11, 523768
[0x800083d8]:addi s11, s11, 729
[0x800083dc]:addi a4, zero, 96
[0x800083e0]:csrrw zero, fcsr, a4
[0x800083e4]:fsub.d t5, t3, s10, dyn
[0x800083e8]:csrrs a7, fcsr, zero

[0x800083e4]:fsub.d t5, t3, s10, dyn
[0x800083e8]:csrrs a7, fcsr, zero
[0x800083ec]:sw t5, 1536(ra)
[0x800083f0]:sw t6, 1544(ra)
[0x800083f4]:sw t5, 1552(ra)
[0x800083f8]:sw a7, 1560(ra)
[0x800083fc]:lw t3, 784(a6)
[0x80008400]:lw t4, 788(a6)
[0x80008404]:lw s10, 792(a6)
[0x80008408]:lw s11, 796(a6)
[0x8000840c]:lui t3, 2948
[0x80008410]:addi t3, t3, 3491
[0x80008414]:lui t4, 523390
[0x80008418]:addi t4, t4, 1995
[0x8000841c]:lui s10, 2948
[0x80008420]:addi s10, s10, 3491
[0x80008424]:lui s11, 523390
[0x80008428]:addi s11, s11, 1995
[0x8000842c]:addi a4, zero, 96
[0x80008430]:csrrw zero, fcsr, a4
[0x80008434]:fsub.d t5, t3, s10, dyn
[0x80008438]:csrrs a7, fcsr, zero

[0x80008434]:fsub.d t5, t3, s10, dyn
[0x80008438]:csrrs a7, fcsr, zero
[0x8000843c]:sw t5, 1568(ra)
[0x80008440]:sw t6, 1576(ra)
[0x80008444]:sw t5, 1584(ra)
[0x80008448]:sw a7, 1592(ra)
[0x8000844c]:lw t3, 800(a6)
[0x80008450]:lw t4, 804(a6)
[0x80008454]:lw s10, 808(a6)
[0x80008458]:lw s11, 812(a6)
[0x8000845c]:lui t3, 819581
[0x80008460]:addi t3, t3, 1947
[0x80008464]:lui t4, 523826
[0x80008468]:addi t4, t4, 3796
[0x8000846c]:lui s10, 819581
[0x80008470]:addi s10, s10, 1947
[0x80008474]:lui s11, 523826
[0x80008478]:addi s11, s11, 3796
[0x8000847c]:addi a4, zero, 96
[0x80008480]:csrrw zero, fcsr, a4
[0x80008484]:fsub.d t5, t3, s10, dyn
[0x80008488]:csrrs a7, fcsr, zero

[0x80008484]:fsub.d t5, t3, s10, dyn
[0x80008488]:csrrs a7, fcsr, zero
[0x8000848c]:sw t5, 1600(ra)
[0x80008490]:sw t6, 1608(ra)
[0x80008494]:sw t5, 1616(ra)
[0x80008498]:sw a7, 1624(ra)
[0x8000849c]:lw t3, 816(a6)
[0x800084a0]:lw t4, 820(a6)
[0x800084a4]:lw s10, 824(a6)
[0x800084a8]:lw s11, 828(a6)
[0x800084ac]:lui t3, 437997
[0x800084b0]:addi t3, t3, 2899
[0x800084b4]:lui t4, 523452
[0x800084b8]:addi t4, t4, 2090
[0x800084bc]:lui s10, 437997
[0x800084c0]:addi s10, s10, 2899
[0x800084c4]:lui s11, 523452
[0x800084c8]:addi s11, s11, 2090
[0x800084cc]:addi a4, zero, 96
[0x800084d0]:csrrw zero, fcsr, a4
[0x800084d4]:fsub.d t5, t3, s10, dyn
[0x800084d8]:csrrs a7, fcsr, zero

[0x800084d4]:fsub.d t5, t3, s10, dyn
[0x800084d8]:csrrs a7, fcsr, zero
[0x800084dc]:sw t5, 1632(ra)
[0x800084e0]:sw t6, 1640(ra)
[0x800084e4]:sw t5, 1648(ra)
[0x800084e8]:sw a7, 1656(ra)
[0x800084ec]:lw t3, 832(a6)
[0x800084f0]:lw t4, 836(a6)
[0x800084f4]:lw s10, 840(a6)
[0x800084f8]:lw s11, 844(a6)
[0x800084fc]:lui t3, 811476
[0x80008500]:addi t3, t3, 2199
[0x80008504]:lui t4, 523880
[0x80008508]:addi t4, t4, 1674
[0x8000850c]:lui s10, 811476
[0x80008510]:addi s10, s10, 2199
[0x80008514]:lui s11, 523880
[0x80008518]:addi s11, s11, 1674
[0x8000851c]:addi a4, zero, 96
[0x80008520]:csrrw zero, fcsr, a4
[0x80008524]:fsub.d t5, t3, s10, dyn
[0x80008528]:csrrs a7, fcsr, zero

[0x80008524]:fsub.d t5, t3, s10, dyn
[0x80008528]:csrrs a7, fcsr, zero
[0x8000852c]:sw t5, 1664(ra)
[0x80008530]:sw t6, 1672(ra)
[0x80008534]:sw t5, 1680(ra)
[0x80008538]:sw a7, 1688(ra)
[0x8000853c]:lw t3, 848(a6)
[0x80008540]:lw t4, 852(a6)
[0x80008544]:lw s10, 856(a6)
[0x80008548]:lw s11, 860(a6)
[0x8000854c]:lui t3, 538824
[0x80008550]:addi t3, t3, 2121
[0x80008554]:lui t4, 524014
[0x80008558]:addi t4, t4, 2581
[0x8000855c]:lui s10, 538824
[0x80008560]:addi s10, s10, 2121
[0x80008564]:lui s11, 524014
[0x80008568]:addi s11, s11, 2581
[0x8000856c]:addi a4, zero, 96
[0x80008570]:csrrw zero, fcsr, a4
[0x80008574]:fsub.d t5, t3, s10, dyn
[0x80008578]:csrrs a7, fcsr, zero

[0x80008574]:fsub.d t5, t3, s10, dyn
[0x80008578]:csrrs a7, fcsr, zero
[0x8000857c]:sw t5, 1696(ra)
[0x80008580]:sw t6, 1704(ra)
[0x80008584]:sw t5, 1712(ra)
[0x80008588]:sw a7, 1720(ra)
[0x8000858c]:lw t3, 864(a6)
[0x80008590]:lw t4, 868(a6)
[0x80008594]:lw s10, 872(a6)
[0x80008598]:lw s11, 876(a6)
[0x8000859c]:lui t3, 775387
[0x800085a0]:addi t3, t3, 2997
[0x800085a4]:lui t4, 523615
[0x800085a8]:addi t4, t4, 2710
[0x800085ac]:lui s10, 775387
[0x800085b0]:addi s10, s10, 2997
[0x800085b4]:lui s11, 523615
[0x800085b8]:addi s11, s11, 2710
[0x800085bc]:addi a4, zero, 96
[0x800085c0]:csrrw zero, fcsr, a4
[0x800085c4]:fsub.d t5, t3, s10, dyn
[0x800085c8]:csrrs a7, fcsr, zero

[0x800085c4]:fsub.d t5, t3, s10, dyn
[0x800085c8]:csrrs a7, fcsr, zero
[0x800085cc]:sw t5, 1728(ra)
[0x800085d0]:sw t6, 1736(ra)
[0x800085d4]:sw t5, 1744(ra)
[0x800085d8]:sw a7, 1752(ra)
[0x800085dc]:lw t3, 880(a6)
[0x800085e0]:lw t4, 884(a6)
[0x800085e4]:lw s10, 888(a6)
[0x800085e8]:lw s11, 892(a6)
[0x800085ec]:lui t3, 342523
[0x800085f0]:addi t3, t3, 3623
[0x800085f4]:lui t4, 523236
[0x800085f8]:addi t4, t4, 2469
[0x800085fc]:lui s10, 342523
[0x80008600]:addi s10, s10, 3623
[0x80008604]:lui s11, 523236
[0x80008608]:addi s11, s11, 2469
[0x8000860c]:addi a4, zero, 96
[0x80008610]:csrrw zero, fcsr, a4
[0x80008614]:fsub.d t5, t3, s10, dyn
[0x80008618]:csrrs a7, fcsr, zero

[0x80008614]:fsub.d t5, t3, s10, dyn
[0x80008618]:csrrs a7, fcsr, zero
[0x8000861c]:sw t5, 1760(ra)
[0x80008620]:sw t6, 1768(ra)
[0x80008624]:sw t5, 1776(ra)
[0x80008628]:sw a7, 1784(ra)
[0x8000862c]:lw t3, 896(a6)
[0x80008630]:lw t4, 900(a6)
[0x80008634]:lw s10, 904(a6)
[0x80008638]:lw s11, 908(a6)
[0x8000863c]:lui t3, 553395
[0x80008640]:addi t3, t3, 2143
[0x80008644]:lui t4, 523142
[0x80008648]:addi t4, t4, 268
[0x8000864c]:lui s10, 553395
[0x80008650]:addi s10, s10, 2143
[0x80008654]:lui s11, 523142
[0x80008658]:addi s11, s11, 268
[0x8000865c]:addi a4, zero, 96
[0x80008660]:csrrw zero, fcsr, a4
[0x80008664]:fsub.d t5, t3, s10, dyn
[0x80008668]:csrrs a7, fcsr, zero

[0x80008664]:fsub.d t5, t3, s10, dyn
[0x80008668]:csrrs a7, fcsr, zero
[0x8000866c]:sw t5, 1792(ra)
[0x80008670]:sw t6, 1800(ra)
[0x80008674]:sw t5, 1808(ra)
[0x80008678]:sw a7, 1816(ra)
[0x8000867c]:lw t3, 912(a6)
[0x80008680]:lw t4, 916(a6)
[0x80008684]:lw s10, 920(a6)
[0x80008688]:lw s11, 924(a6)
[0x8000868c]:lui t3, 473492
[0x80008690]:addi t3, t3, 2055
[0x80008694]:lui t4, 523771
[0x80008698]:addi t4, t4, 3700
[0x8000869c]:lui s10, 473492
[0x800086a0]:addi s10, s10, 2055
[0x800086a4]:lui s11, 523771
[0x800086a8]:addi s11, s11, 3700
[0x800086ac]:addi a4, zero, 96
[0x800086b0]:csrrw zero, fcsr, a4
[0x800086b4]:fsub.d t5, t3, s10, dyn
[0x800086b8]:csrrs a7, fcsr, zero

[0x800086b4]:fsub.d t5, t3, s10, dyn
[0x800086b8]:csrrs a7, fcsr, zero
[0x800086bc]:sw t5, 1824(ra)
[0x800086c0]:sw t6, 1832(ra)
[0x800086c4]:sw t5, 1840(ra)
[0x800086c8]:sw a7, 1848(ra)
[0x800086cc]:lw t3, 928(a6)
[0x800086d0]:lw t4, 932(a6)
[0x800086d4]:lw s10, 936(a6)
[0x800086d8]:lw s11, 940(a6)
[0x800086dc]:lui t3, 1046835
[0x800086e0]:addi t3, t3, 1011
[0x800086e4]:lui t4, 523353
[0x800086e8]:addi t4, t4, 1178
[0x800086ec]:lui s10, 1046835
[0x800086f0]:addi s10, s10, 1011
[0x800086f4]:lui s11, 523353
[0x800086f8]:addi s11, s11, 1178
[0x800086fc]:addi a4, zero, 96
[0x80008700]:csrrw zero, fcsr, a4
[0x80008704]:fsub.d t5, t3, s10, dyn
[0x80008708]:csrrs a7, fcsr, zero

[0x80008704]:fsub.d t5, t3, s10, dyn
[0x80008708]:csrrs a7, fcsr, zero
[0x8000870c]:sw t5, 1856(ra)
[0x80008710]:sw t6, 1864(ra)
[0x80008714]:sw t5, 1872(ra)
[0x80008718]:sw a7, 1880(ra)
[0x8000871c]:lw t3, 944(a6)
[0x80008720]:lw t4, 948(a6)
[0x80008724]:lw s10, 952(a6)
[0x80008728]:lw s11, 956(a6)
[0x8000872c]:lui t3, 217639
[0x80008730]:addi t3, t3, 746
[0x80008734]:lui t4, 523940
[0x80008738]:addi t4, t4, 3755
[0x8000873c]:lui s10, 217639
[0x80008740]:addi s10, s10, 746
[0x80008744]:lui s11, 523940
[0x80008748]:addi s11, s11, 3755
[0x8000874c]:addi a4, zero, 96
[0x80008750]:csrrw zero, fcsr, a4
[0x80008754]:fsub.d t5, t3, s10, dyn
[0x80008758]:csrrs a7, fcsr, zero

[0x80008754]:fsub.d t5, t3, s10, dyn
[0x80008758]:csrrs a7, fcsr, zero
[0x8000875c]:sw t5, 1888(ra)
[0x80008760]:sw t6, 1896(ra)
[0x80008764]:sw t5, 1904(ra)
[0x80008768]:sw a7, 1912(ra)
[0x8000876c]:lw t3, 960(a6)
[0x80008770]:lw t4, 964(a6)
[0x80008774]:lw s10, 968(a6)
[0x80008778]:lw s11, 972(a6)
[0x8000877c]:lui t3, 121071
[0x80008780]:addi t3, t3, 407
[0x80008784]:lui t4, 523817
[0x80008788]:addi t4, t4, 3791
[0x8000878c]:lui s10, 121071
[0x80008790]:addi s10, s10, 407
[0x80008794]:lui s11, 523817
[0x80008798]:addi s11, s11, 3791
[0x8000879c]:addi a4, zero, 96
[0x800087a0]:csrrw zero, fcsr, a4
[0x800087a4]:fsub.d t5, t3, s10, dyn
[0x800087a8]:csrrs a7, fcsr, zero

[0x800087a4]:fsub.d t5, t3, s10, dyn
[0x800087a8]:csrrs a7, fcsr, zero
[0x800087ac]:sw t5, 1920(ra)
[0x800087b0]:sw t6, 1928(ra)
[0x800087b4]:sw t5, 1936(ra)
[0x800087b8]:sw a7, 1944(ra)
[0x800087bc]:lw t3, 976(a6)
[0x800087c0]:lw t4, 980(a6)
[0x800087c4]:lw s10, 984(a6)
[0x800087c8]:lw s11, 988(a6)
[0x800087cc]:lui t3, 512129
[0x800087d0]:addi t3, t3, 92
[0x800087d4]:lui t4, 523886
[0x800087d8]:addi t4, t4, 191
[0x800087dc]:lui s10, 512129
[0x800087e0]:addi s10, s10, 92
[0x800087e4]:lui s11, 523886
[0x800087e8]:addi s11, s11, 191
[0x800087ec]:addi a4, zero, 96
[0x800087f0]:csrrw zero, fcsr, a4
[0x800087f4]:fsub.d t5, t3, s10, dyn
[0x800087f8]:csrrs a7, fcsr, zero

[0x800087f4]:fsub.d t5, t3, s10, dyn
[0x800087f8]:csrrs a7, fcsr, zero
[0x800087fc]:sw t5, 1952(ra)
[0x80008800]:sw t6, 1960(ra)
[0x80008804]:sw t5, 1968(ra)
[0x80008808]:sw a7, 1976(ra)
[0x8000880c]:lw t3, 992(a6)
[0x80008810]:lw t4, 996(a6)
[0x80008814]:lw s10, 1000(a6)
[0x80008818]:lw s11, 1004(a6)
[0x8000881c]:lui t3, 427802
[0x80008820]:addi t3, t3, 3002
[0x80008824]:lui t4, 523824
[0x80008828]:addi t4, t4, 3820
[0x8000882c]:lui s10, 427802
[0x80008830]:addi s10, s10, 3002
[0x80008834]:lui s11, 523824
[0x80008838]:addi s11, s11, 3820
[0x8000883c]:addi a4, zero, 96
[0x80008840]:csrrw zero, fcsr, a4
[0x80008844]:fsub.d t5, t3, s10, dyn
[0x80008848]:csrrs a7, fcsr, zero

[0x80008844]:fsub.d t5, t3, s10, dyn
[0x80008848]:csrrs a7, fcsr, zero
[0x8000884c]:sw t5, 1984(ra)
[0x80008850]:sw t6, 1992(ra)
[0x80008854]:sw t5, 2000(ra)
[0x80008858]:sw a7, 2008(ra)
[0x8000885c]:lw t3, 1008(a6)
[0x80008860]:lw t4, 1012(a6)
[0x80008864]:lw s10, 1016(a6)
[0x80008868]:lw s11, 1020(a6)
[0x8000886c]:lui t3, 132364
[0x80008870]:addi t3, t3, 3756
[0x80008874]:lui t4, 524029
[0x80008878]:addi t4, t4, 530
[0x8000887c]:lui s10, 132364
[0x80008880]:addi s10, s10, 3756
[0x80008884]:lui s11, 524029
[0x80008888]:addi s11, s11, 530
[0x8000888c]:addi a4, zero, 96
[0x80008890]:csrrw zero, fcsr, a4
[0x80008894]:fsub.d t5, t3, s10, dyn
[0x80008898]:csrrs a7, fcsr, zero

[0x80008894]:fsub.d t5, t3, s10, dyn
[0x80008898]:csrrs a7, fcsr, zero
[0x8000889c]:sw t5, 2016(ra)
[0x800088a0]:sw t6, 2024(ra)
[0x800088a4]:sw t5, 2032(ra)
[0x800088a8]:sw a7, 2040(ra)
[0x800088ac]:lw t3, 1024(a6)
[0x800088b0]:lw t4, 1028(a6)
[0x800088b4]:lw s10, 1032(a6)
[0x800088b8]:lw s11, 1036(a6)
[0x800088bc]:lui t3, 606294
[0x800088c0]:addi t3, t3, 2644
[0x800088c4]:lui t4, 523928
[0x800088c8]:addi t4, t4, 1190
[0x800088cc]:lui s10, 606294
[0x800088d0]:addi s10, s10, 2644
[0x800088d4]:lui s11, 523928
[0x800088d8]:addi s11, s11, 1190
[0x800088dc]:addi a4, zero, 96
[0x800088e0]:csrrw zero, fcsr, a4
[0x800088e4]:fsub.d t5, t3, s10, dyn
[0x800088e8]:csrrs a7, fcsr, zero

[0x800088e4]:fsub.d t5, t3, s10, dyn
[0x800088e8]:csrrs a7, fcsr, zero
[0x800088ec]:addi ra, ra, 2040
[0x800088f0]:sw t5, 8(ra)
[0x800088f4]:sw t6, 16(ra)
[0x800088f8]:sw t5, 24(ra)
[0x800088fc]:sw a7, 32(ra)
[0x80008900]:lw t3, 1040(a6)
[0x80008904]:lw t4, 1044(a6)
[0x80008908]:lw s10, 1048(a6)
[0x8000890c]:lw s11, 1052(a6)
[0x80008910]:lui t3, 356805
[0x80008914]:addi t3, t3, 715
[0x80008918]:lui t4, 523591
[0x8000891c]:addi t4, t4, 1000
[0x80008920]:lui s10, 356805
[0x80008924]:addi s10, s10, 715
[0x80008928]:lui s11, 523591
[0x8000892c]:addi s11, s11, 1000
[0x80008930]:addi a4, zero, 96
[0x80008934]:csrrw zero, fcsr, a4
[0x80008938]:fsub.d t5, t3, s10, dyn
[0x8000893c]:csrrs a7, fcsr, zero

[0x80008938]:fsub.d t5, t3, s10, dyn
[0x8000893c]:csrrs a7, fcsr, zero
[0x80008940]:sw t5, 40(ra)
[0x80008944]:sw t6, 48(ra)
[0x80008948]:sw t5, 56(ra)
[0x8000894c]:sw a7, 64(ra)
[0x80008950]:lw t3, 1056(a6)
[0x80008954]:lw t4, 1060(a6)
[0x80008958]:lw s10, 1064(a6)
[0x8000895c]:lw s11, 1068(a6)
[0x80008960]:lui t3, 348770
[0x80008964]:addi t3, t3, 3021
[0x80008968]:lui t4, 523817
[0x8000896c]:addi t4, t4, 368
[0x80008970]:lui s10, 348770
[0x80008974]:addi s10, s10, 3021
[0x80008978]:lui s11, 523817
[0x8000897c]:addi s11, s11, 368
[0x80008980]:addi a4, zero, 96
[0x80008984]:csrrw zero, fcsr, a4
[0x80008988]:fsub.d t5, t3, s10, dyn
[0x8000898c]:csrrs a7, fcsr, zero

[0x80008988]:fsub.d t5, t3, s10, dyn
[0x8000898c]:csrrs a7, fcsr, zero
[0x80008990]:sw t5, 72(ra)
[0x80008994]:sw t6, 80(ra)
[0x80008998]:sw t5, 88(ra)
[0x8000899c]:sw a7, 96(ra)
[0x800089a0]:lw t3, 1072(a6)
[0x800089a4]:lw t4, 1076(a6)
[0x800089a8]:lw s10, 1080(a6)
[0x800089ac]:lw s11, 1084(a6)
[0x800089b0]:lui t3, 103549
[0x800089b4]:addi t3, t3, 431
[0x800089b8]:lui t4, 523674
[0x800089bc]:addi t4, t4, 2158
[0x800089c0]:lui s10, 103549
[0x800089c4]:addi s10, s10, 431
[0x800089c8]:lui s11, 523674
[0x800089cc]:addi s11, s11, 2158
[0x800089d0]:addi a4, zero, 96
[0x800089d4]:csrrw zero, fcsr, a4
[0x800089d8]:fsub.d t5, t3, s10, dyn
[0x800089dc]:csrrs a7, fcsr, zero

[0x800089d8]:fsub.d t5, t3, s10, dyn
[0x800089dc]:csrrs a7, fcsr, zero
[0x800089e0]:sw t5, 104(ra)
[0x800089e4]:sw t6, 112(ra)
[0x800089e8]:sw t5, 120(ra)
[0x800089ec]:sw a7, 128(ra)
[0x800089f0]:lw t3, 1088(a6)
[0x800089f4]:lw t4, 1092(a6)
[0x800089f8]:lw s10, 1096(a6)
[0x800089fc]:lw s11, 1100(a6)
[0x80008a00]:lui t3, 764249
[0x80008a04]:addi t3, t3, 3239
[0x80008a08]:lui t4, 523908
[0x80008a0c]:addi t4, t4, 1568
[0x80008a10]:lui s10, 764249
[0x80008a14]:addi s10, s10, 3239
[0x80008a18]:lui s11, 523908
[0x80008a1c]:addi s11, s11, 1568
[0x80008a20]:addi a4, zero, 96
[0x80008a24]:csrrw zero, fcsr, a4
[0x80008a28]:fsub.d t5, t3, s10, dyn
[0x80008a2c]:csrrs a7, fcsr, zero

[0x80008a28]:fsub.d t5, t3, s10, dyn
[0x80008a2c]:csrrs a7, fcsr, zero
[0x80008a30]:sw t5, 136(ra)
[0x80008a34]:sw t6, 144(ra)
[0x80008a38]:sw t5, 152(ra)
[0x80008a3c]:sw a7, 160(ra)
[0x80008a40]:lw t3, 1104(a6)
[0x80008a44]:lw t4, 1108(a6)
[0x80008a48]:lw s10, 1112(a6)
[0x80008a4c]:lw s11, 1116(a6)
[0x80008a50]:lui t3, 1020343
[0x80008a54]:addi t3, t3, 1411
[0x80008a58]:lui t4, 523283
[0x80008a5c]:addi t4, t4, 728
[0x80008a60]:lui s10, 1020343
[0x80008a64]:addi s10, s10, 1411
[0x80008a68]:lui s11, 523283
[0x80008a6c]:addi s11, s11, 728
[0x80008a70]:addi a4, zero, 96
[0x80008a74]:csrrw zero, fcsr, a4
[0x80008a78]:fsub.d t5, t3, s10, dyn
[0x80008a7c]:csrrs a7, fcsr, zero



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fsub.d', 'rs1 : x30', 'rs2 : x30', 'rd : x30', 'rs1 == rs2 == rd', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0x132d8f91b7583 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x132d8f91b7583 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000014c]:fsub.d t5, t5, t5, dyn
	-[0x80000150]:csrrs tp, fcsr, zero
	-[0x80000154]:sw t5, 0(ra)
	-[0x80000158]:sw t6, 8(ra)
Current Store : [0x80000158] : sw t6, 8(ra) -- Store: [0x8000b720]:0x7FC132D8




Last Coverpoint : ['mnemonic : fsub.d', 'rs1 : x30', 'rs2 : x30', 'rd : x30', 'rs1 == rs2 == rd', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0x132d8f91b7583 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x132d8f91b7583 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000014c]:fsub.d t5, t5, t5, dyn
	-[0x80000150]:csrrs tp, fcsr, zero
	-[0x80000154]:sw t5, 0(ra)
	-[0x80000158]:sw t6, 8(ra)
	-[0x8000015c]:sw t5, 16(ra)
Current Store : [0x8000015c] : sw t5, 16(ra) -- Store: [0x8000b728]:0x00000000




Last Coverpoint : ['rs1 : x26', 'rs2 : x24', 'rd : x28', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0x870d778409f12 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x870d778409f12 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000019c]:fsub.d t3, s10, s8, dyn
	-[0x800001a0]:csrrs tp, fcsr, zero
	-[0x800001a4]:sw t3, 32(ra)
	-[0x800001a8]:sw t4, 40(ra)
Current Store : [0x800001a8] : sw t4, 40(ra) -- Store: [0x8000b740]:0xEEDBEADF




Last Coverpoint : ['rs1 : x26', 'rs2 : x24', 'rd : x28', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0x870d778409f12 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x870d778409f12 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000019c]:fsub.d t3, s10, s8, dyn
	-[0x800001a0]:csrrs tp, fcsr, zero
	-[0x800001a4]:sw t3, 32(ra)
	-[0x800001a8]:sw t4, 40(ra)
	-[0x800001ac]:sw t3, 48(ra)
Current Store : [0x800001ac] : sw t3, 48(ra) -- Store: [0x8000b748]:0x00000000




Last Coverpoint : ['rs1 : x24', 'rs2 : x28', 'rd : x24', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xfb5355e167379 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xfb5355e167379 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001ec]:fsub.d s8, s8, t3, dyn
	-[0x800001f0]:csrrs tp, fcsr, zero
	-[0x800001f4]:sw s8, 64(ra)
	-[0x800001f8]:sw s9, 72(ra)
Current Store : [0x800001f8] : sw s9, 72(ra) -- Store: [0x8000b760]:0x7FDFB535




Last Coverpoint : ['rs1 : x24', 'rs2 : x28', 'rd : x24', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xfb5355e167379 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xfb5355e167379 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001ec]:fsub.d s8, s8, t3, dyn
	-[0x800001f0]:csrrs tp, fcsr, zero
	-[0x800001f4]:sw s8, 64(ra)
	-[0x800001f8]:sw s9, 72(ra)
	-[0x800001fc]:sw s8, 80(ra)
Current Store : [0x800001fc] : sw s8, 80(ra) -- Store: [0x8000b768]:0x00000000




Last Coverpoint : ['rs1 : x28', 'rs2 : x26', 'rd : x26', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4d9d98184b9d9 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x4d9d98184b9d9 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000023c]:fsub.d s10, t3, s10, dyn
	-[0x80000240]:csrrs tp, fcsr, zero
	-[0x80000244]:sw s10, 96(ra)
	-[0x80000248]:sw s11, 104(ra)
Current Store : [0x80000248] : sw s11, 104(ra) -- Store: [0x8000b780]:0x7FE4D9D9




Last Coverpoint : ['rs1 : x28', 'rs2 : x26', 'rd : x26', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4d9d98184b9d9 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x4d9d98184b9d9 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000023c]:fsub.d s10, t3, s10, dyn
	-[0x80000240]:csrrs tp, fcsr, zero
	-[0x80000244]:sw s10, 96(ra)
	-[0x80000248]:sw s11, 104(ra)
	-[0x8000024c]:sw s10, 112(ra)
Current Store : [0x8000024c] : sw s10, 112(ra) -- Store: [0x8000b788]:0x00000000




Last Coverpoint : ['rs1 : x20', 'rs2 : x20', 'rd : x22', 'rs1 == rs2 != rd', 'fs1 == 0 and fe1 == 0x7fb and fm1 == 0x8072e8f9c858f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x8072e8f9c858f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000028c]:fsub.d s6, s4, s4, dyn
	-[0x80000290]:csrrs tp, fcsr, zero
	-[0x80000294]:sw s6, 128(ra)
	-[0x80000298]:sw s7, 136(ra)
Current Store : [0x80000298] : sw s7, 136(ra) -- Store: [0x8000b7a0]:0xB6FAB7FB




Last Coverpoint : ['rs1 : x20', 'rs2 : x20', 'rd : x22', 'rs1 == rs2 != rd', 'fs1 == 0 and fe1 == 0x7fb and fm1 == 0x8072e8f9c858f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x8072e8f9c858f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000028c]:fsub.d s6, s4, s4, dyn
	-[0x80000290]:csrrs tp, fcsr, zero
	-[0x80000294]:sw s6, 128(ra)
	-[0x80000298]:sw s7, 136(ra)
	-[0x8000029c]:sw s6, 144(ra)
Current Store : [0x8000029c] : sw s6, 144(ra) -- Store: [0x8000b7a8]:0x00000000




Last Coverpoint : ['rs1 : x22', 'rs2 : x18', 'rd : x20', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xabe96758f2a09 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xabe96758f2a09 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002dc]:fsub.d s4, s6, s2, dyn
	-[0x800002e0]:csrrs tp, fcsr, zero
	-[0x800002e4]:sw s4, 160(ra)
	-[0x800002e8]:sw s5, 168(ra)
Current Store : [0x800002e8] : sw s5, 168(ra) -- Store: [0x8000b7c0]:0x7FB8072E




Last Coverpoint : ['rs1 : x22', 'rs2 : x18', 'rd : x20', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xabe96758f2a09 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xabe96758f2a09 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002dc]:fsub.d s4, s6, s2, dyn
	-[0x800002e0]:csrrs tp, fcsr, zero
	-[0x800002e4]:sw s4, 160(ra)
	-[0x800002e8]:sw s5, 168(ra)
	-[0x800002ec]:sw s4, 176(ra)
Current Store : [0x800002ec] : sw s4, 176(ra) -- Store: [0x8000b7c8]:0x00000000




Last Coverpoint : ['rs1 : x16', 'rs2 : x22', 'rd : x18', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0x86499331191c4 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x86499331191c4 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000032c]:fsub.d s2, a6, s6, dyn
	-[0x80000330]:csrrs tp, fcsr, zero
	-[0x80000334]:sw s2, 192(ra)
	-[0x80000338]:sw s3, 200(ra)
Current Store : [0x80000338] : sw s3, 200(ra) -- Store: [0x8000b7e0]:0x7FEABE96




Last Coverpoint : ['rs1 : x16', 'rs2 : x22', 'rd : x18', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0x86499331191c4 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x86499331191c4 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000032c]:fsub.d s2, a6, s6, dyn
	-[0x80000330]:csrrs tp, fcsr, zero
	-[0x80000334]:sw s2, 192(ra)
	-[0x80000338]:sw s3, 200(ra)
	-[0x8000033c]:sw s2, 208(ra)
Current Store : [0x8000033c] : sw s2, 208(ra) -- Store: [0x8000b7e8]:0x00000000




Last Coverpoint : ['rs1 : x18', 'rs2 : x14', 'rd : x16', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc81394a2171e9 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xc81394a2171e9 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000037c]:fsub.d a6, s2, a4, dyn
	-[0x80000380]:csrrs tp, fcsr, zero
	-[0x80000384]:sw a6, 224(ra)
	-[0x80000388]:sw a7, 232(ra)
Current Store : [0x80000388] : sw a7, 232(ra) -- Store: [0x8000b800]:0x7FE86499




Last Coverpoint : ['rs1 : x18', 'rs2 : x14', 'rd : x16', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc81394a2171e9 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xc81394a2171e9 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000037c]:fsub.d a6, s2, a4, dyn
	-[0x80000380]:csrrs tp, fcsr, zero
	-[0x80000384]:sw a6, 224(ra)
	-[0x80000388]:sw a7, 232(ra)
	-[0x8000038c]:sw a6, 240(ra)
Current Store : [0x8000038c] : sw a6, 240(ra) -- Store: [0x8000b808]:0x00000000




Last Coverpoint : ['rs1 : x12', 'rs2 : x16', 'rd : x14', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0xd481499755d4b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xd481499755d4b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003cc]:fsub.d a4, a2, a6, dyn
	-[0x800003d0]:csrrs tp, fcsr, zero
	-[0x800003d4]:sw a4, 256(ra)
	-[0x800003d8]:sw a5, 264(ra)
Current Store : [0x800003d8] : sw a5, 264(ra) -- Store: [0x8000b820]:0x7FDC8139




Last Coverpoint : ['rs1 : x12', 'rs2 : x16', 'rd : x14', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0xd481499755d4b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xd481499755d4b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003cc]:fsub.d a4, a2, a6, dyn
	-[0x800003d0]:csrrs tp, fcsr, zero
	-[0x800003d4]:sw a4, 256(ra)
	-[0x800003d8]:sw a5, 264(ra)
	-[0x800003dc]:sw a4, 272(ra)
Current Store : [0x800003dc] : sw a4, 272(ra) -- Store: [0x8000b828]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rs2 : x10', 'rd : x12', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd87e65450c45 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xcd87e65450c45 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000424]:fsub.d a2, a4, a0, dyn
	-[0x80000428]:csrrs a7, fcsr, zero
	-[0x8000042c]:sw a2, 288(ra)
	-[0x80000430]:sw a3, 296(ra)
Current Store : [0x80000430] : sw a3, 296(ra) -- Store: [0x8000b840]:0x7FCD4814




Last Coverpoint : ['rs1 : x14', 'rs2 : x10', 'rd : x12', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd87e65450c45 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xcd87e65450c45 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000424]:fsub.d a2, a4, a0, dyn
	-[0x80000428]:csrrs a7, fcsr, zero
	-[0x8000042c]:sw a2, 288(ra)
	-[0x80000430]:sw a3, 296(ra)
	-[0x80000434]:sw a2, 304(ra)
Current Store : [0x80000434] : sw a2, 304(ra) -- Store: [0x8000b848]:0x00000000




Last Coverpoint : ['rs1 : x8', 'rs2 : x12', 'rd : x10', 'fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xa0e7ad32453df and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xa0e7ad32453df and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000474]:fsub.d a0, fp, a2, dyn
	-[0x80000478]:csrrs a7, fcsr, zero
	-[0x8000047c]:sw a0, 320(ra)
	-[0x80000480]:sw a1, 328(ra)
Current Store : [0x80000480] : sw a1, 328(ra) -- Store: [0x8000b860]:0x7FECD87E




Last Coverpoint : ['rs1 : x8', 'rs2 : x12', 'rd : x10', 'fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xa0e7ad32453df and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xa0e7ad32453df and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000474]:fsub.d a0, fp, a2, dyn
	-[0x80000478]:csrrs a7, fcsr, zero
	-[0x8000047c]:sw a0, 320(ra)
	-[0x80000480]:sw a1, 328(ra)
	-[0x80000484]:sw a0, 336(ra)
Current Store : [0x80000484] : sw a0, 336(ra) -- Store: [0x8000b868]:0x00000000




Last Coverpoint : ['rs1 : x10', 'rs2 : x6', 'rd : x8', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe0d828b86622a and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xe0d828b86622a and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004cc]:fsub.d fp, a0, t1, dyn
	-[0x800004d0]:csrrs a7, fcsr, zero
	-[0x800004d4]:sw fp, 0(ra)
	-[0x800004d8]:sw s1, 8(ra)
Current Store : [0x800004d8] : sw s1, 8(ra) -- Store: [0x8000b7d0]:0x7F9A0E7A




Last Coverpoint : ['rs1 : x10', 'rs2 : x6', 'rd : x8', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe0d828b86622a and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xe0d828b86622a and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004cc]:fsub.d fp, a0, t1, dyn
	-[0x800004d0]:csrrs a7, fcsr, zero
	-[0x800004d4]:sw fp, 0(ra)
	-[0x800004d8]:sw s1, 8(ra)
	-[0x800004dc]:sw fp, 16(ra)
Current Store : [0x800004dc] : sw fp, 16(ra) -- Store: [0x8000b7d8]:0x00000000




Last Coverpoint : ['rs1 : x4', 'rs2 : x8', 'rd : x6', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb9876f8130c3 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xbb9876f8130c3 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000051c]:fsub.d t1, tp, fp, dyn
	-[0x80000520]:csrrs a7, fcsr, zero
	-[0x80000524]:sw t1, 32(ra)
	-[0x80000528]:sw t2, 40(ra)
Current Store : [0x80000528] : sw t2, 40(ra) -- Store: [0x8000b7f0]:0x7FEE0D82




Last Coverpoint : ['rs1 : x4', 'rs2 : x8', 'rd : x6', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb9876f8130c3 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xbb9876f8130c3 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000051c]:fsub.d t1, tp, fp, dyn
	-[0x80000520]:csrrs a7, fcsr, zero
	-[0x80000524]:sw t1, 32(ra)
	-[0x80000528]:sw t2, 40(ra)
	-[0x8000052c]:sw t1, 48(ra)
Current Store : [0x8000052c] : sw t1, 48(ra) -- Store: [0x8000b7f8]:0x00000000




Last Coverpoint : ['rs1 : x6', 'rs2 : x2', 'rd : x4', 'fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xdbcde43895c3f and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xdbcde43895c3f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000056c]:fsub.d tp, t1, sp, dyn
	-[0x80000570]:csrrs a7, fcsr, zero
	-[0x80000574]:sw tp, 64(ra)
	-[0x80000578]:sw t0, 72(ra)
Current Store : [0x80000578] : sw t0, 72(ra) -- Store: [0x8000b810]:0x7FCBB987




Last Coverpoint : ['rs1 : x6', 'rs2 : x2', 'rd : x4', 'fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xdbcde43895c3f and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xdbcde43895c3f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000056c]:fsub.d tp, t1, sp, dyn
	-[0x80000570]:csrrs a7, fcsr, zero
	-[0x80000574]:sw tp, 64(ra)
	-[0x80000578]:sw t0, 72(ra)
	-[0x8000057c]:sw tp, 80(ra)
Current Store : [0x8000057c] : sw tp, 80(ra) -- Store: [0x8000b818]:0x00000000




Last Coverpoint : ['rs1 : x2', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc0659af8369fd and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xc0659af8369fd and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005bc]:fsub.d t5, sp, t3, dyn
	-[0x800005c0]:csrrs a7, fcsr, zero
	-[0x800005c4]:sw t5, 96(ra)
	-[0x800005c8]:sw t6, 104(ra)
Current Store : [0x800005c8] : sw t6, 104(ra) -- Store: [0x8000b830]:0x7FC132D8




Last Coverpoint : ['rs1 : x2', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc0659af8369fd and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xc0659af8369fd and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005bc]:fsub.d t5, sp, t3, dyn
	-[0x800005c0]:csrrs a7, fcsr, zero
	-[0x800005c4]:sw t5, 96(ra)
	-[0x800005c8]:sw t6, 104(ra)
	-[0x800005cc]:sw t5, 112(ra)
Current Store : [0x800005cc] : sw t5, 112(ra) -- Store: [0x8000b838]:0x00000000




Last Coverpoint : ['rs2 : x4', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdd5b61587fd27 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xdd5b61587fd27 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000060c]:fsub.d t5, t3, tp, dyn
	-[0x80000610]:csrrs a7, fcsr, zero
	-[0x80000614]:sw t5, 128(ra)
	-[0x80000618]:sw t6, 136(ra)
Current Store : [0x80000618] : sw t6, 136(ra) -- Store: [0x8000b850]:0x7FC132D8




Last Coverpoint : ['rs2 : x4', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdd5b61587fd27 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xdd5b61587fd27 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000060c]:fsub.d t5, t3, tp, dyn
	-[0x80000610]:csrrs a7, fcsr, zero
	-[0x80000614]:sw t5, 128(ra)
	-[0x80000618]:sw t6, 136(ra)
	-[0x8000061c]:sw t5, 144(ra)
Current Store : [0x8000061c] : sw t5, 144(ra) -- Store: [0x8000b858]:0x00000000




Last Coverpoint : ['rd : x2', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc01045c2cd787 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xc01045c2cd787 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000065c]:fsub.d sp, t5, t3, dyn
	-[0x80000660]:csrrs a7, fcsr, zero
	-[0x80000664]:sw sp, 160(ra)
	-[0x80000668]:sw gp, 168(ra)
Current Store : [0x80000668] : sw gp, 168(ra) -- Store: [0x8000b870]:0x7FDC0659




Last Coverpoint : ['rd : x2', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc01045c2cd787 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xc01045c2cd787 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000065c]:fsub.d sp, t5, t3, dyn
	-[0x80000660]:csrrs a7, fcsr, zero
	-[0x80000664]:sw sp, 160(ra)
	-[0x80000668]:sw gp, 168(ra)
	-[0x8000066c]:sw sp, 176(ra)
Current Store : [0x8000066c] : sw sp, 176(ra) -- Store: [0x8000b878]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x28bc82f697c4d and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x28bc82f697c4d and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006ac]:fsub.d t5, t3, s10, dyn
	-[0x800006b0]:csrrs a7, fcsr, zero
	-[0x800006b4]:sw t5, 192(ra)
	-[0x800006b8]:sw t6, 200(ra)
Current Store : [0x800006b8] : sw t6, 200(ra) -- Store: [0x8000b890]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x28bc82f697c4d and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x28bc82f697c4d and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006ac]:fsub.d t5, t3, s10, dyn
	-[0x800006b0]:csrrs a7, fcsr, zero
	-[0x800006b4]:sw t5, 192(ra)
	-[0x800006b8]:sw t6, 200(ra)
	-[0x800006bc]:sw t5, 208(ra)
Current Store : [0x800006bc] : sw t5, 208(ra) -- Store: [0x8000b898]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xacd7053aa42a2 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xacd7053aa42a2 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006fc]:fsub.d t5, t3, s10, dyn
	-[0x80000700]:csrrs a7, fcsr, zero
	-[0x80000704]:sw t5, 224(ra)
	-[0x80000708]:sw t6, 232(ra)
Current Store : [0x80000708] : sw t6, 232(ra) -- Store: [0x8000b8b0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xacd7053aa42a2 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xacd7053aa42a2 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006fc]:fsub.d t5, t3, s10, dyn
	-[0x80000700]:csrrs a7, fcsr, zero
	-[0x80000704]:sw t5, 224(ra)
	-[0x80000708]:sw t6, 232(ra)
	-[0x8000070c]:sw t5, 240(ra)
Current Store : [0x8000070c] : sw t5, 240(ra) -- Store: [0x8000b8b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x48dace8666677 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x48dace8666677 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000074c]:fsub.d t5, t3, s10, dyn
	-[0x80000750]:csrrs a7, fcsr, zero
	-[0x80000754]:sw t5, 256(ra)
	-[0x80000758]:sw t6, 264(ra)
Current Store : [0x80000758] : sw t6, 264(ra) -- Store: [0x8000b8d0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x48dace8666677 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x48dace8666677 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000074c]:fsub.d t5, t3, s10, dyn
	-[0x80000750]:csrrs a7, fcsr, zero
	-[0x80000754]:sw t5, 256(ra)
	-[0x80000758]:sw t6, 264(ra)
	-[0x8000075c]:sw t5, 272(ra)
Current Store : [0x8000075c] : sw t5, 272(ra) -- Store: [0x8000b8d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfc2ea66e5019e and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfc2ea66e5019e and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000079c]:fsub.d t5, t3, s10, dyn
	-[0x800007a0]:csrrs a7, fcsr, zero
	-[0x800007a4]:sw t5, 288(ra)
	-[0x800007a8]:sw t6, 296(ra)
Current Store : [0x800007a8] : sw t6, 296(ra) -- Store: [0x8000b8f0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfc2ea66e5019e and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfc2ea66e5019e and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000079c]:fsub.d t5, t3, s10, dyn
	-[0x800007a0]:csrrs a7, fcsr, zero
	-[0x800007a4]:sw t5, 288(ra)
	-[0x800007a8]:sw t6, 296(ra)
	-[0x800007ac]:sw t5, 304(ra)
Current Store : [0x800007ac] : sw t5, 304(ra) -- Store: [0x8000b8f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xef2a4f7c7db7f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xef2a4f7c7db7f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007ec]:fsub.d t5, t3, s10, dyn
	-[0x800007f0]:csrrs a7, fcsr, zero
	-[0x800007f4]:sw t5, 320(ra)
	-[0x800007f8]:sw t6, 328(ra)
Current Store : [0x800007f8] : sw t6, 328(ra) -- Store: [0x8000b910]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xef2a4f7c7db7f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xef2a4f7c7db7f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007ec]:fsub.d t5, t3, s10, dyn
	-[0x800007f0]:csrrs a7, fcsr, zero
	-[0x800007f4]:sw t5, 320(ra)
	-[0x800007f8]:sw t6, 328(ra)
	-[0x800007fc]:sw t5, 336(ra)
Current Store : [0x800007fc] : sw t5, 336(ra) -- Store: [0x8000b918]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7166677e49c3c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x7166677e49c3c and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000083c]:fsub.d t5, t3, s10, dyn
	-[0x80000840]:csrrs a7, fcsr, zero
	-[0x80000844]:sw t5, 352(ra)
	-[0x80000848]:sw t6, 360(ra)
Current Store : [0x80000848] : sw t6, 360(ra) -- Store: [0x8000b930]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7166677e49c3c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x7166677e49c3c and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000083c]:fsub.d t5, t3, s10, dyn
	-[0x80000840]:csrrs a7, fcsr, zero
	-[0x80000844]:sw t5, 352(ra)
	-[0x80000848]:sw t6, 360(ra)
	-[0x8000084c]:sw t5, 368(ra)
Current Store : [0x8000084c] : sw t5, 368(ra) -- Store: [0x8000b938]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xdf7523fde6c5d and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xdf7523fde6c5d and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000088c]:fsub.d t5, t3, s10, dyn
	-[0x80000890]:csrrs a7, fcsr, zero
	-[0x80000894]:sw t5, 384(ra)
	-[0x80000898]:sw t6, 392(ra)
Current Store : [0x80000898] : sw t6, 392(ra) -- Store: [0x8000b950]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xdf7523fde6c5d and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xdf7523fde6c5d and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000088c]:fsub.d t5, t3, s10, dyn
	-[0x80000890]:csrrs a7, fcsr, zero
	-[0x80000894]:sw t5, 384(ra)
	-[0x80000898]:sw t6, 392(ra)
	-[0x8000089c]:sw t5, 400(ra)
Current Store : [0x8000089c] : sw t5, 400(ra) -- Store: [0x8000b958]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa8fa703a4078c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xa8fa703a4078c and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008dc]:fsub.d t5, t3, s10, dyn
	-[0x800008e0]:csrrs a7, fcsr, zero
	-[0x800008e4]:sw t5, 416(ra)
	-[0x800008e8]:sw t6, 424(ra)
Current Store : [0x800008e8] : sw t6, 424(ra) -- Store: [0x8000b970]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa8fa703a4078c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xa8fa703a4078c and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008dc]:fsub.d t5, t3, s10, dyn
	-[0x800008e0]:csrrs a7, fcsr, zero
	-[0x800008e4]:sw t5, 416(ra)
	-[0x800008e8]:sw t6, 424(ra)
	-[0x800008ec]:sw t5, 432(ra)
Current Store : [0x800008ec] : sw t5, 432(ra) -- Store: [0x8000b978]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x36a63c245f557 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x36a63c245f557 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000092c]:fsub.d t5, t3, s10, dyn
	-[0x80000930]:csrrs a7, fcsr, zero
	-[0x80000934]:sw t5, 448(ra)
	-[0x80000938]:sw t6, 456(ra)
Current Store : [0x80000938] : sw t6, 456(ra) -- Store: [0x8000b990]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x36a63c245f557 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x36a63c245f557 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000092c]:fsub.d t5, t3, s10, dyn
	-[0x80000930]:csrrs a7, fcsr, zero
	-[0x80000934]:sw t5, 448(ra)
	-[0x80000938]:sw t6, 456(ra)
	-[0x8000093c]:sw t5, 464(ra)
Current Store : [0x8000093c] : sw t5, 464(ra) -- Store: [0x8000b998]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc3d4499ff58c3 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc3d4499ff58c3 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000097c]:fsub.d t5, t3, s10, dyn
	-[0x80000980]:csrrs a7, fcsr, zero
	-[0x80000984]:sw t5, 480(ra)
	-[0x80000988]:sw t6, 488(ra)
Current Store : [0x80000988] : sw t6, 488(ra) -- Store: [0x8000b9b0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc3d4499ff58c3 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc3d4499ff58c3 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000097c]:fsub.d t5, t3, s10, dyn
	-[0x80000980]:csrrs a7, fcsr, zero
	-[0x80000984]:sw t5, 480(ra)
	-[0x80000988]:sw t6, 488(ra)
	-[0x8000098c]:sw t5, 496(ra)
Current Store : [0x8000098c] : sw t5, 496(ra) -- Store: [0x8000b9b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x02b48f992cb49 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x02b48f992cb49 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009cc]:fsub.d t5, t3, s10, dyn
	-[0x800009d0]:csrrs a7, fcsr, zero
	-[0x800009d4]:sw t5, 512(ra)
	-[0x800009d8]:sw t6, 520(ra)
Current Store : [0x800009d8] : sw t6, 520(ra) -- Store: [0x8000b9d0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x02b48f992cb49 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x02b48f992cb49 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009cc]:fsub.d t5, t3, s10, dyn
	-[0x800009d0]:csrrs a7, fcsr, zero
	-[0x800009d4]:sw t5, 512(ra)
	-[0x800009d8]:sw t6, 520(ra)
	-[0x800009dc]:sw t5, 528(ra)
Current Store : [0x800009dc] : sw t5, 528(ra) -- Store: [0x8000b9d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fa and fm1 == 0x1ad5e9ebc09df and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x1ad5e9ebc09df and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a1c]:fsub.d t5, t3, s10, dyn
	-[0x80000a20]:csrrs a7, fcsr, zero
	-[0x80000a24]:sw t5, 544(ra)
	-[0x80000a28]:sw t6, 552(ra)
Current Store : [0x80000a28] : sw t6, 552(ra) -- Store: [0x8000b9f0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fa and fm1 == 0x1ad5e9ebc09df and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x1ad5e9ebc09df and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a1c]:fsub.d t5, t3, s10, dyn
	-[0x80000a20]:csrrs a7, fcsr, zero
	-[0x80000a24]:sw t5, 544(ra)
	-[0x80000a28]:sw t6, 552(ra)
	-[0x80000a2c]:sw t5, 560(ra)
Current Store : [0x80000a2c] : sw t5, 560(ra) -- Store: [0x8000b9f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x98455e99dfdb1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x98455e99dfdb1 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a6c]:fsub.d t5, t3, s10, dyn
	-[0x80000a70]:csrrs a7, fcsr, zero
	-[0x80000a74]:sw t5, 576(ra)
	-[0x80000a78]:sw t6, 584(ra)
Current Store : [0x80000a78] : sw t6, 584(ra) -- Store: [0x8000ba10]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x98455e99dfdb1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x98455e99dfdb1 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a6c]:fsub.d t5, t3, s10, dyn
	-[0x80000a70]:csrrs a7, fcsr, zero
	-[0x80000a74]:sw t5, 576(ra)
	-[0x80000a78]:sw t6, 584(ra)
	-[0x80000a7c]:sw t5, 592(ra)
Current Store : [0x80000a7c] : sw t5, 592(ra) -- Store: [0x8000ba18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0x6251b45dfbd3b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x6251b45dfbd3b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000abc]:fsub.d t5, t3, s10, dyn
	-[0x80000ac0]:csrrs a7, fcsr, zero
	-[0x80000ac4]:sw t5, 608(ra)
	-[0x80000ac8]:sw t6, 616(ra)
Current Store : [0x80000ac8] : sw t6, 616(ra) -- Store: [0x8000ba30]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0x6251b45dfbd3b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x6251b45dfbd3b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000abc]:fsub.d t5, t3, s10, dyn
	-[0x80000ac0]:csrrs a7, fcsr, zero
	-[0x80000ac4]:sw t5, 608(ra)
	-[0x80000ac8]:sw t6, 616(ra)
	-[0x80000acc]:sw t5, 624(ra)
Current Store : [0x80000acc] : sw t5, 624(ra) -- Store: [0x8000ba38]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x67f4f571a752e and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x67f4f571a752e and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b0c]:fsub.d t5, t3, s10, dyn
	-[0x80000b10]:csrrs a7, fcsr, zero
	-[0x80000b14]:sw t5, 640(ra)
	-[0x80000b18]:sw t6, 648(ra)
Current Store : [0x80000b18] : sw t6, 648(ra) -- Store: [0x8000ba50]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x67f4f571a752e and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x67f4f571a752e and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b0c]:fsub.d t5, t3, s10, dyn
	-[0x80000b10]:csrrs a7, fcsr, zero
	-[0x80000b14]:sw t5, 640(ra)
	-[0x80000b18]:sw t6, 648(ra)
	-[0x80000b1c]:sw t5, 656(ra)
Current Store : [0x80000b1c] : sw t5, 656(ra) -- Store: [0x8000ba58]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x7fb2260b115e9 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x7fb2260b115e9 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b5c]:fsub.d t5, t3, s10, dyn
	-[0x80000b60]:csrrs a7, fcsr, zero
	-[0x80000b64]:sw t5, 672(ra)
	-[0x80000b68]:sw t6, 680(ra)
Current Store : [0x80000b68] : sw t6, 680(ra) -- Store: [0x8000ba70]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x7fb2260b115e9 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x7fb2260b115e9 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b5c]:fsub.d t5, t3, s10, dyn
	-[0x80000b60]:csrrs a7, fcsr, zero
	-[0x80000b64]:sw t5, 672(ra)
	-[0x80000b68]:sw t6, 680(ra)
	-[0x80000b6c]:sw t5, 688(ra)
Current Store : [0x80000b6c] : sw t5, 688(ra) -- Store: [0x8000ba78]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x04507a06e8587 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x04507a06e8587 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bac]:fsub.d t5, t3, s10, dyn
	-[0x80000bb0]:csrrs a7, fcsr, zero
	-[0x80000bb4]:sw t5, 704(ra)
	-[0x80000bb8]:sw t6, 712(ra)
Current Store : [0x80000bb8] : sw t6, 712(ra) -- Store: [0x8000ba90]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x04507a06e8587 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x04507a06e8587 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bac]:fsub.d t5, t3, s10, dyn
	-[0x80000bb0]:csrrs a7, fcsr, zero
	-[0x80000bb4]:sw t5, 704(ra)
	-[0x80000bb8]:sw t6, 712(ra)
	-[0x80000bbc]:sw t5, 720(ra)
Current Store : [0x80000bbc] : sw t5, 720(ra) -- Store: [0x8000ba98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0ab870b5c1c40 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x0ab870b5c1c40 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bfc]:fsub.d t5, t3, s10, dyn
	-[0x80000c00]:csrrs a7, fcsr, zero
	-[0x80000c04]:sw t5, 736(ra)
	-[0x80000c08]:sw t6, 744(ra)
Current Store : [0x80000c08] : sw t6, 744(ra) -- Store: [0x8000bab0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0ab870b5c1c40 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x0ab870b5c1c40 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bfc]:fsub.d t5, t3, s10, dyn
	-[0x80000c00]:csrrs a7, fcsr, zero
	-[0x80000c04]:sw t5, 736(ra)
	-[0x80000c08]:sw t6, 744(ra)
	-[0x80000c0c]:sw t5, 752(ra)
Current Store : [0x80000c0c] : sw t5, 752(ra) -- Store: [0x8000bab8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf57237ddcb451 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xf57237ddcb451 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c4c]:fsub.d t5, t3, s10, dyn
	-[0x80000c50]:csrrs a7, fcsr, zero
	-[0x80000c54]:sw t5, 768(ra)
	-[0x80000c58]:sw t6, 776(ra)
Current Store : [0x80000c58] : sw t6, 776(ra) -- Store: [0x8000bad0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf57237ddcb451 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xf57237ddcb451 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c4c]:fsub.d t5, t3, s10, dyn
	-[0x80000c50]:csrrs a7, fcsr, zero
	-[0x80000c54]:sw t5, 768(ra)
	-[0x80000c58]:sw t6, 776(ra)
	-[0x80000c5c]:sw t5, 784(ra)
Current Store : [0x80000c5c] : sw t5, 784(ra) -- Store: [0x8000bad8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fa and fm1 == 0x643f753bef22f and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x643f753bef22f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c9c]:fsub.d t5, t3, s10, dyn
	-[0x80000ca0]:csrrs a7, fcsr, zero
	-[0x80000ca4]:sw t5, 800(ra)
	-[0x80000ca8]:sw t6, 808(ra)
Current Store : [0x80000ca8] : sw t6, 808(ra) -- Store: [0x8000baf0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fa and fm1 == 0x643f753bef22f and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x643f753bef22f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c9c]:fsub.d t5, t3, s10, dyn
	-[0x80000ca0]:csrrs a7, fcsr, zero
	-[0x80000ca4]:sw t5, 800(ra)
	-[0x80000ca8]:sw t6, 808(ra)
	-[0x80000cac]:sw t5, 816(ra)
Current Store : [0x80000cac] : sw t5, 816(ra) -- Store: [0x8000baf8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf7646167590ef and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf7646167590ef and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cec]:fsub.d t5, t3, s10, dyn
	-[0x80000cf0]:csrrs a7, fcsr, zero
	-[0x80000cf4]:sw t5, 832(ra)
	-[0x80000cf8]:sw t6, 840(ra)
Current Store : [0x80000cf8] : sw t6, 840(ra) -- Store: [0x8000bb10]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf7646167590ef and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf7646167590ef and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cec]:fsub.d t5, t3, s10, dyn
	-[0x80000cf0]:csrrs a7, fcsr, zero
	-[0x80000cf4]:sw t5, 832(ra)
	-[0x80000cf8]:sw t6, 840(ra)
	-[0x80000cfc]:sw t5, 848(ra)
Current Store : [0x80000cfc] : sw t5, 848(ra) -- Store: [0x8000bb18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x930bcbd2d6035 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x930bcbd2d6035 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d3c]:fsub.d t5, t3, s10, dyn
	-[0x80000d40]:csrrs a7, fcsr, zero
	-[0x80000d44]:sw t5, 864(ra)
	-[0x80000d48]:sw t6, 872(ra)
Current Store : [0x80000d48] : sw t6, 872(ra) -- Store: [0x8000bb30]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x930bcbd2d6035 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x930bcbd2d6035 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d3c]:fsub.d t5, t3, s10, dyn
	-[0x80000d40]:csrrs a7, fcsr, zero
	-[0x80000d44]:sw t5, 864(ra)
	-[0x80000d48]:sw t6, 872(ra)
	-[0x80000d4c]:sw t5, 880(ra)
Current Store : [0x80000d4c] : sw t5, 880(ra) -- Store: [0x8000bb38]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0125698e86242 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x0125698e86242 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d8c]:fsub.d t5, t3, s10, dyn
	-[0x80000d90]:csrrs a7, fcsr, zero
	-[0x80000d94]:sw t5, 896(ra)
	-[0x80000d98]:sw t6, 904(ra)
Current Store : [0x80000d98] : sw t6, 904(ra) -- Store: [0x8000bb50]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0125698e86242 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x0125698e86242 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d8c]:fsub.d t5, t3, s10, dyn
	-[0x80000d90]:csrrs a7, fcsr, zero
	-[0x80000d94]:sw t5, 896(ra)
	-[0x80000d98]:sw t6, 904(ra)
	-[0x80000d9c]:sw t5, 912(ra)
Current Store : [0x80000d9c] : sw t5, 912(ra) -- Store: [0x8000bb58]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8a82024cc4e03 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x8a82024cc4e03 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ddc]:fsub.d t5, t3, s10, dyn
	-[0x80000de0]:csrrs a7, fcsr, zero
	-[0x80000de4]:sw t5, 928(ra)
	-[0x80000de8]:sw t6, 936(ra)
Current Store : [0x80000de8] : sw t6, 936(ra) -- Store: [0x8000bb70]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8a82024cc4e03 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x8a82024cc4e03 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ddc]:fsub.d t5, t3, s10, dyn
	-[0x80000de0]:csrrs a7, fcsr, zero
	-[0x80000de4]:sw t5, 928(ra)
	-[0x80000de8]:sw t6, 936(ra)
	-[0x80000dec]:sw t5, 944(ra)
Current Store : [0x80000dec] : sw t5, 944(ra) -- Store: [0x8000bb78]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb877e6e317fa2 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xb877e6e317fa2 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e2c]:fsub.d t5, t3, s10, dyn
	-[0x80000e30]:csrrs a7, fcsr, zero
	-[0x80000e34]:sw t5, 960(ra)
	-[0x80000e38]:sw t6, 968(ra)
Current Store : [0x80000e38] : sw t6, 968(ra) -- Store: [0x8000bb90]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb877e6e317fa2 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xb877e6e317fa2 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e2c]:fsub.d t5, t3, s10, dyn
	-[0x80000e30]:csrrs a7, fcsr, zero
	-[0x80000e34]:sw t5, 960(ra)
	-[0x80000e38]:sw t6, 968(ra)
	-[0x80000e3c]:sw t5, 976(ra)
Current Store : [0x80000e3c] : sw t5, 976(ra) -- Store: [0x8000bb98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x070d1456013e3 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x070d1456013e3 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e7c]:fsub.d t5, t3, s10, dyn
	-[0x80000e80]:csrrs a7, fcsr, zero
	-[0x80000e84]:sw t5, 992(ra)
	-[0x80000e88]:sw t6, 1000(ra)
Current Store : [0x80000e88] : sw t6, 1000(ra) -- Store: [0x8000bbb0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x070d1456013e3 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x070d1456013e3 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e7c]:fsub.d t5, t3, s10, dyn
	-[0x80000e80]:csrrs a7, fcsr, zero
	-[0x80000e84]:sw t5, 992(ra)
	-[0x80000e88]:sw t6, 1000(ra)
	-[0x80000e8c]:sw t5, 1008(ra)
Current Store : [0x80000e8c] : sw t5, 1008(ra) -- Store: [0x8000bbb8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x27d4b8969c0b2 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x27d4b8969c0b2 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ecc]:fsub.d t5, t3, s10, dyn
	-[0x80000ed0]:csrrs a7, fcsr, zero
	-[0x80000ed4]:sw t5, 1024(ra)
	-[0x80000ed8]:sw t6, 1032(ra)
Current Store : [0x80000ed8] : sw t6, 1032(ra) -- Store: [0x8000bbd0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x27d4b8969c0b2 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x27d4b8969c0b2 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ecc]:fsub.d t5, t3, s10, dyn
	-[0x80000ed0]:csrrs a7, fcsr, zero
	-[0x80000ed4]:sw t5, 1024(ra)
	-[0x80000ed8]:sw t6, 1032(ra)
	-[0x80000edc]:sw t5, 1040(ra)
Current Store : [0x80000edc] : sw t5, 1040(ra) -- Store: [0x8000bbd8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x13bdffd461269 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x13bdffd461269 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f1c]:fsub.d t5, t3, s10, dyn
	-[0x80000f20]:csrrs a7, fcsr, zero
	-[0x80000f24]:sw t5, 1056(ra)
	-[0x80000f28]:sw t6, 1064(ra)
Current Store : [0x80000f28] : sw t6, 1064(ra) -- Store: [0x8000bbf0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x13bdffd461269 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x13bdffd461269 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f1c]:fsub.d t5, t3, s10, dyn
	-[0x80000f20]:csrrs a7, fcsr, zero
	-[0x80000f24]:sw t5, 1056(ra)
	-[0x80000f28]:sw t6, 1064(ra)
	-[0x80000f2c]:sw t5, 1072(ra)
Current Store : [0x80000f2c] : sw t5, 1072(ra) -- Store: [0x8000bbf8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xea0b252eae7e0 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xea0b252eae7e0 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f6c]:fsub.d t5, t3, s10, dyn
	-[0x80000f70]:csrrs a7, fcsr, zero
	-[0x80000f74]:sw t5, 1088(ra)
	-[0x80000f78]:sw t6, 1096(ra)
Current Store : [0x80000f78] : sw t6, 1096(ra) -- Store: [0x8000bc10]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xea0b252eae7e0 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xea0b252eae7e0 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f6c]:fsub.d t5, t3, s10, dyn
	-[0x80000f70]:csrrs a7, fcsr, zero
	-[0x80000f74]:sw t5, 1088(ra)
	-[0x80000f78]:sw t6, 1096(ra)
	-[0x80000f7c]:sw t5, 1104(ra)
Current Store : [0x80000f7c] : sw t5, 1104(ra) -- Store: [0x8000bc18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x913b4236d8411 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x913b4236d8411 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fbc]:fsub.d t5, t3, s10, dyn
	-[0x80000fc0]:csrrs a7, fcsr, zero
	-[0x80000fc4]:sw t5, 1120(ra)
	-[0x80000fc8]:sw t6, 1128(ra)
Current Store : [0x80000fc8] : sw t6, 1128(ra) -- Store: [0x8000bc30]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x913b4236d8411 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x913b4236d8411 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fbc]:fsub.d t5, t3, s10, dyn
	-[0x80000fc0]:csrrs a7, fcsr, zero
	-[0x80000fc4]:sw t5, 1120(ra)
	-[0x80000fc8]:sw t6, 1128(ra)
	-[0x80000fcc]:sw t5, 1136(ra)
Current Store : [0x80000fcc] : sw t5, 1136(ra) -- Store: [0x8000bc38]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc5b9547c0fb71 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc5b9547c0fb71 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000100c]:fsub.d t5, t3, s10, dyn
	-[0x80001010]:csrrs a7, fcsr, zero
	-[0x80001014]:sw t5, 1152(ra)
	-[0x80001018]:sw t6, 1160(ra)
Current Store : [0x80001018] : sw t6, 1160(ra) -- Store: [0x8000bc50]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc5b9547c0fb71 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc5b9547c0fb71 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000100c]:fsub.d t5, t3, s10, dyn
	-[0x80001010]:csrrs a7, fcsr, zero
	-[0x80001014]:sw t5, 1152(ra)
	-[0x80001018]:sw t6, 1160(ra)
	-[0x8000101c]:sw t5, 1168(ra)
Current Store : [0x8000101c] : sw t5, 1168(ra) -- Store: [0x8000bc58]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9e4795c8459f5 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x9e4795c8459f5 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000105c]:fsub.d t5, t3, s10, dyn
	-[0x80001060]:csrrs a7, fcsr, zero
	-[0x80001064]:sw t5, 1184(ra)
	-[0x80001068]:sw t6, 1192(ra)
Current Store : [0x80001068] : sw t6, 1192(ra) -- Store: [0x8000bc70]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9e4795c8459f5 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x9e4795c8459f5 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000105c]:fsub.d t5, t3, s10, dyn
	-[0x80001060]:csrrs a7, fcsr, zero
	-[0x80001064]:sw t5, 1184(ra)
	-[0x80001068]:sw t6, 1192(ra)
	-[0x8000106c]:sw t5, 1200(ra)
Current Store : [0x8000106c] : sw t5, 1200(ra) -- Store: [0x8000bc78]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x1f6a4c4d26ab9 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x1f6a4c4d26ab9 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010ac]:fsub.d t5, t3, s10, dyn
	-[0x800010b0]:csrrs a7, fcsr, zero
	-[0x800010b4]:sw t5, 1216(ra)
	-[0x800010b8]:sw t6, 1224(ra)
Current Store : [0x800010b8] : sw t6, 1224(ra) -- Store: [0x8000bc90]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x1f6a4c4d26ab9 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x1f6a4c4d26ab9 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010ac]:fsub.d t5, t3, s10, dyn
	-[0x800010b0]:csrrs a7, fcsr, zero
	-[0x800010b4]:sw t5, 1216(ra)
	-[0x800010b8]:sw t6, 1224(ra)
	-[0x800010bc]:sw t5, 1232(ra)
Current Store : [0x800010bc] : sw t5, 1232(ra) -- Store: [0x8000bc98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fa and fm1 == 0xcbc315eca5f3f and fs2 == 0 and fe2 == 0x7fa and fm2 == 0xcbc315eca5f3f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010fc]:fsub.d t5, t3, s10, dyn
	-[0x80001100]:csrrs a7, fcsr, zero
	-[0x80001104]:sw t5, 1248(ra)
	-[0x80001108]:sw t6, 1256(ra)
Current Store : [0x80001108] : sw t6, 1256(ra) -- Store: [0x8000bcb0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fa and fm1 == 0xcbc315eca5f3f and fs2 == 0 and fe2 == 0x7fa and fm2 == 0xcbc315eca5f3f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010fc]:fsub.d t5, t3, s10, dyn
	-[0x80001100]:csrrs a7, fcsr, zero
	-[0x80001104]:sw t5, 1248(ra)
	-[0x80001108]:sw t6, 1256(ra)
	-[0x8000110c]:sw t5, 1264(ra)
Current Store : [0x8000110c] : sw t5, 1264(ra) -- Store: [0x8000bcb8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x23d6f3e37b4f1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x23d6f3e37b4f1 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000114c]:fsub.d t5, t3, s10, dyn
	-[0x80001150]:csrrs a7, fcsr, zero
	-[0x80001154]:sw t5, 1280(ra)
	-[0x80001158]:sw t6, 1288(ra)
Current Store : [0x80001158] : sw t6, 1288(ra) -- Store: [0x8000bcd0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x23d6f3e37b4f1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x23d6f3e37b4f1 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000114c]:fsub.d t5, t3, s10, dyn
	-[0x80001150]:csrrs a7, fcsr, zero
	-[0x80001154]:sw t5, 1280(ra)
	-[0x80001158]:sw t6, 1288(ra)
	-[0x8000115c]:sw t5, 1296(ra)
Current Store : [0x8000115c] : sw t5, 1296(ra) -- Store: [0x8000bcd8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x026ab89a75256 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x026ab89a75256 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000119c]:fsub.d t5, t3, s10, dyn
	-[0x800011a0]:csrrs a7, fcsr, zero
	-[0x800011a4]:sw t5, 1312(ra)
	-[0x800011a8]:sw t6, 1320(ra)
Current Store : [0x800011a8] : sw t6, 1320(ra) -- Store: [0x8000bcf0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x026ab89a75256 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x026ab89a75256 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000119c]:fsub.d t5, t3, s10, dyn
	-[0x800011a0]:csrrs a7, fcsr, zero
	-[0x800011a4]:sw t5, 1312(ra)
	-[0x800011a8]:sw t6, 1320(ra)
	-[0x800011ac]:sw t5, 1328(ra)
Current Store : [0x800011ac] : sw t5, 1328(ra) -- Store: [0x8000bcf8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x6d5a59350bdcb and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x6d5a59350bdcb and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011ec]:fsub.d t5, t3, s10, dyn
	-[0x800011f0]:csrrs a7, fcsr, zero
	-[0x800011f4]:sw t5, 1344(ra)
	-[0x800011f8]:sw t6, 1352(ra)
Current Store : [0x800011f8] : sw t6, 1352(ra) -- Store: [0x8000bd10]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x6d5a59350bdcb and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x6d5a59350bdcb and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011ec]:fsub.d t5, t3, s10, dyn
	-[0x800011f0]:csrrs a7, fcsr, zero
	-[0x800011f4]:sw t5, 1344(ra)
	-[0x800011f8]:sw t6, 1352(ra)
	-[0x800011fc]:sw t5, 1360(ra)
Current Store : [0x800011fc] : sw t5, 1360(ra) -- Store: [0x8000bd18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x13b37e2291279 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x13b37e2291279 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000123c]:fsub.d t5, t3, s10, dyn
	-[0x80001240]:csrrs a7, fcsr, zero
	-[0x80001244]:sw t5, 1376(ra)
	-[0x80001248]:sw t6, 1384(ra)
Current Store : [0x80001248] : sw t6, 1384(ra) -- Store: [0x8000bd30]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x13b37e2291279 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x13b37e2291279 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000123c]:fsub.d t5, t3, s10, dyn
	-[0x80001240]:csrrs a7, fcsr, zero
	-[0x80001244]:sw t5, 1376(ra)
	-[0x80001248]:sw t6, 1384(ra)
	-[0x8000124c]:sw t5, 1392(ra)
Current Store : [0x8000124c] : sw t5, 1392(ra) -- Store: [0x8000bd38]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x39935e95315b1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x39935e95315b1 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000128c]:fsub.d t5, t3, s10, dyn
	-[0x80001290]:csrrs a7, fcsr, zero
	-[0x80001294]:sw t5, 1408(ra)
	-[0x80001298]:sw t6, 1416(ra)
Current Store : [0x80001298] : sw t6, 1416(ra) -- Store: [0x8000bd50]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x39935e95315b1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x39935e95315b1 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000128c]:fsub.d t5, t3, s10, dyn
	-[0x80001290]:csrrs a7, fcsr, zero
	-[0x80001294]:sw t5, 1408(ra)
	-[0x80001298]:sw t6, 1416(ra)
	-[0x8000129c]:sw t5, 1424(ra)
Current Store : [0x8000129c] : sw t5, 1424(ra) -- Store: [0x8000bd58]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xca57966fc21ff and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xca57966fc21ff and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012dc]:fsub.d t5, t3, s10, dyn
	-[0x800012e0]:csrrs a7, fcsr, zero
	-[0x800012e4]:sw t5, 1440(ra)
	-[0x800012e8]:sw t6, 1448(ra)
Current Store : [0x800012e8] : sw t6, 1448(ra) -- Store: [0x8000bd70]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xca57966fc21ff and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xca57966fc21ff and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012dc]:fsub.d t5, t3, s10, dyn
	-[0x800012e0]:csrrs a7, fcsr, zero
	-[0x800012e4]:sw t5, 1440(ra)
	-[0x800012e8]:sw t6, 1448(ra)
	-[0x800012ec]:sw t5, 1456(ra)
Current Store : [0x800012ec] : sw t5, 1456(ra) -- Store: [0x8000bd78]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0x6aedbc8cfe5cb and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x6aedbc8cfe5cb and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000132c]:fsub.d t5, t3, s10, dyn
	-[0x80001330]:csrrs a7, fcsr, zero
	-[0x80001334]:sw t5, 1472(ra)
	-[0x80001338]:sw t6, 1480(ra)
Current Store : [0x80001338] : sw t6, 1480(ra) -- Store: [0x8000bd90]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0x6aedbc8cfe5cb and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x6aedbc8cfe5cb and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000132c]:fsub.d t5, t3, s10, dyn
	-[0x80001330]:csrrs a7, fcsr, zero
	-[0x80001334]:sw t5, 1472(ra)
	-[0x80001338]:sw t6, 1480(ra)
	-[0x8000133c]:sw t5, 1488(ra)
Current Store : [0x8000133c] : sw t5, 1488(ra) -- Store: [0x8000bd98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb8d62745dccc1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xb8d62745dccc1 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000137c]:fsub.d t5, t3, s10, dyn
	-[0x80001380]:csrrs a7, fcsr, zero
	-[0x80001384]:sw t5, 1504(ra)
	-[0x80001388]:sw t6, 1512(ra)
Current Store : [0x80001388] : sw t6, 1512(ra) -- Store: [0x8000bdb0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb8d62745dccc1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xb8d62745dccc1 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000137c]:fsub.d t5, t3, s10, dyn
	-[0x80001380]:csrrs a7, fcsr, zero
	-[0x80001384]:sw t5, 1504(ra)
	-[0x80001388]:sw t6, 1512(ra)
	-[0x8000138c]:sw t5, 1520(ra)
Current Store : [0x8000138c] : sw t5, 1520(ra) -- Store: [0x8000bdb8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x981d2bf67b45e and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x981d2bf67b45e and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013cc]:fsub.d t5, t3, s10, dyn
	-[0x800013d0]:csrrs a7, fcsr, zero
	-[0x800013d4]:sw t5, 1536(ra)
	-[0x800013d8]:sw t6, 1544(ra)
Current Store : [0x800013d8] : sw t6, 1544(ra) -- Store: [0x8000bdd0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x981d2bf67b45e and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x981d2bf67b45e and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013cc]:fsub.d t5, t3, s10, dyn
	-[0x800013d0]:csrrs a7, fcsr, zero
	-[0x800013d4]:sw t5, 1536(ra)
	-[0x800013d8]:sw t6, 1544(ra)
	-[0x800013dc]:sw t5, 1552(ra)
Current Store : [0x800013dc] : sw t5, 1552(ra) -- Store: [0x8000bdd8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x056bcd04279ed and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x056bcd04279ed and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000141c]:fsub.d t5, t3, s10, dyn
	-[0x80001420]:csrrs a7, fcsr, zero
	-[0x80001424]:sw t5, 1568(ra)
	-[0x80001428]:sw t6, 1576(ra)
Current Store : [0x80001428] : sw t6, 1576(ra) -- Store: [0x8000bdf0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x056bcd04279ed and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x056bcd04279ed and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000141c]:fsub.d t5, t3, s10, dyn
	-[0x80001420]:csrrs a7, fcsr, zero
	-[0x80001424]:sw t5, 1568(ra)
	-[0x80001428]:sw t6, 1576(ra)
	-[0x8000142c]:sw t5, 1584(ra)
Current Store : [0x8000142c] : sw t5, 1584(ra) -- Store: [0x8000bdf8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x58a25604824f3 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x58a25604824f3 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000146c]:fsub.d t5, t3, s10, dyn
	-[0x80001470]:csrrs a7, fcsr, zero
	-[0x80001474]:sw t5, 1600(ra)
	-[0x80001478]:sw t6, 1608(ra)
Current Store : [0x80001478] : sw t6, 1608(ra) -- Store: [0x8000be10]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x58a25604824f3 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x58a25604824f3 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000146c]:fsub.d t5, t3, s10, dyn
	-[0x80001470]:csrrs a7, fcsr, zero
	-[0x80001474]:sw t5, 1600(ra)
	-[0x80001478]:sw t6, 1608(ra)
	-[0x8000147c]:sw t5, 1616(ra)
Current Store : [0x8000147c] : sw t5, 1616(ra) -- Store: [0x8000be18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f9 and fm1 == 0x1175939fbdd3f and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0x1175939fbdd3f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014bc]:fsub.d t5, t3, s10, dyn
	-[0x800014c0]:csrrs a7, fcsr, zero
	-[0x800014c4]:sw t5, 1632(ra)
	-[0x800014c8]:sw t6, 1640(ra)
Current Store : [0x800014c8] : sw t6, 1640(ra) -- Store: [0x8000be30]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f9 and fm1 == 0x1175939fbdd3f and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0x1175939fbdd3f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014bc]:fsub.d t5, t3, s10, dyn
	-[0x800014c0]:csrrs a7, fcsr, zero
	-[0x800014c4]:sw t5, 1632(ra)
	-[0x800014c8]:sw t6, 1640(ra)
	-[0x800014cc]:sw t5, 1648(ra)
Current Store : [0x800014cc] : sw t5, 1648(ra) -- Store: [0x8000be38]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x82dc4511ff204 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x82dc4511ff204 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000150c]:fsub.d t5, t3, s10, dyn
	-[0x80001510]:csrrs a7, fcsr, zero
	-[0x80001514]:sw t5, 1664(ra)
	-[0x80001518]:sw t6, 1672(ra)
Current Store : [0x80001518] : sw t6, 1672(ra) -- Store: [0x8000be50]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x82dc4511ff204 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x82dc4511ff204 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000150c]:fsub.d t5, t3, s10, dyn
	-[0x80001510]:csrrs a7, fcsr, zero
	-[0x80001514]:sw t5, 1664(ra)
	-[0x80001518]:sw t6, 1672(ra)
	-[0x8000151c]:sw t5, 1680(ra)
Current Store : [0x8000151c] : sw t5, 1680(ra) -- Store: [0x8000be58]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xc07725983617f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xc07725983617f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000155c]:fsub.d t5, t3, s10, dyn
	-[0x80001560]:csrrs a7, fcsr, zero
	-[0x80001564]:sw t5, 1696(ra)
	-[0x80001568]:sw t6, 1704(ra)
Current Store : [0x80001568] : sw t6, 1704(ra) -- Store: [0x8000be70]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xc07725983617f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xc07725983617f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000155c]:fsub.d t5, t3, s10, dyn
	-[0x80001560]:csrrs a7, fcsr, zero
	-[0x80001564]:sw t5, 1696(ra)
	-[0x80001568]:sw t6, 1704(ra)
	-[0x8000156c]:sw t5, 1712(ra)
Current Store : [0x8000156c] : sw t5, 1712(ra) -- Store: [0x8000be78]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x60b0632528095 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x60b0632528095 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015ac]:fsub.d t5, t3, s10, dyn
	-[0x800015b0]:csrrs a7, fcsr, zero
	-[0x800015b4]:sw t5, 1728(ra)
	-[0x800015b8]:sw t6, 1736(ra)
Current Store : [0x800015b8] : sw t6, 1736(ra) -- Store: [0x8000be90]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x60b0632528095 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x60b0632528095 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015ac]:fsub.d t5, t3, s10, dyn
	-[0x800015b0]:csrrs a7, fcsr, zero
	-[0x800015b4]:sw t5, 1728(ra)
	-[0x800015b8]:sw t6, 1736(ra)
	-[0x800015bc]:sw t5, 1744(ra)
Current Store : [0x800015bc] : sw t5, 1744(ra) -- Store: [0x8000be98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0x46e9bf4155d7b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x46e9bf4155d7b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015fc]:fsub.d t5, t3, s10, dyn
	-[0x80001600]:csrrs a7, fcsr, zero
	-[0x80001604]:sw t5, 1760(ra)
	-[0x80001608]:sw t6, 1768(ra)
Current Store : [0x80001608] : sw t6, 1768(ra) -- Store: [0x8000beb0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0x46e9bf4155d7b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x46e9bf4155d7b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015fc]:fsub.d t5, t3, s10, dyn
	-[0x80001600]:csrrs a7, fcsr, zero
	-[0x80001604]:sw t5, 1760(ra)
	-[0x80001608]:sw t6, 1768(ra)
	-[0x8000160c]:sw t5, 1776(ra)
Current Store : [0x8000160c] : sw t5, 1776(ra) -- Store: [0x8000beb8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0x585c60a81aa3f and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x585c60a81aa3f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000164c]:fsub.d t5, t3, s10, dyn
	-[0x80001650]:csrrs a7, fcsr, zero
	-[0x80001654]:sw t5, 1792(ra)
	-[0x80001658]:sw t6, 1800(ra)
Current Store : [0x80001658] : sw t6, 1800(ra) -- Store: [0x8000bed0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0x585c60a81aa3f and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x585c60a81aa3f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000164c]:fsub.d t5, t3, s10, dyn
	-[0x80001650]:csrrs a7, fcsr, zero
	-[0x80001654]:sw t5, 1792(ra)
	-[0x80001658]:sw t6, 1800(ra)
	-[0x8000165c]:sw t5, 1808(ra)
Current Store : [0x8000165c] : sw t5, 1808(ra) -- Store: [0x8000bed8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x6c5583d2d8f82 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x6c5583d2d8f82 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000169c]:fsub.d t5, t3, s10, dyn
	-[0x800016a0]:csrrs a7, fcsr, zero
	-[0x800016a4]:sw t5, 1824(ra)
	-[0x800016a8]:sw t6, 1832(ra)
Current Store : [0x800016a8] : sw t6, 1832(ra) -- Store: [0x8000bef0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x6c5583d2d8f82 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x6c5583d2d8f82 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000169c]:fsub.d t5, t3, s10, dyn
	-[0x800016a0]:csrrs a7, fcsr, zero
	-[0x800016a4]:sw t5, 1824(ra)
	-[0x800016a8]:sw t6, 1832(ra)
	-[0x800016ac]:sw t5, 1840(ra)
Current Store : [0x800016ac] : sw t5, 1840(ra) -- Store: [0x8000bef8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x49bad4bf8d1a9 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x49bad4bf8d1a9 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016ec]:fsub.d t5, t3, s10, dyn
	-[0x800016f0]:csrrs a7, fcsr, zero
	-[0x800016f4]:sw t5, 1856(ra)
	-[0x800016f8]:sw t6, 1864(ra)
Current Store : [0x800016f8] : sw t6, 1864(ra) -- Store: [0x8000bf10]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x49bad4bf8d1a9 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x49bad4bf8d1a9 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016ec]:fsub.d t5, t3, s10, dyn
	-[0x800016f0]:csrrs a7, fcsr, zero
	-[0x800016f4]:sw t5, 1856(ra)
	-[0x800016f8]:sw t6, 1864(ra)
	-[0x800016fc]:sw t5, 1872(ra)
Current Store : [0x800016fc] : sw t5, 1872(ra) -- Store: [0x8000bf18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f9 and fm1 == 0x833a9a7efc6ff and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0x833a9a7efc6ff and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000173c]:fsub.d t5, t3, s10, dyn
	-[0x80001740]:csrrs a7, fcsr, zero
	-[0x80001744]:sw t5, 1888(ra)
	-[0x80001748]:sw t6, 1896(ra)
Current Store : [0x80001748] : sw t6, 1896(ra) -- Store: [0x8000bf30]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f9 and fm1 == 0x833a9a7efc6ff and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0x833a9a7efc6ff and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000173c]:fsub.d t5, t3, s10, dyn
	-[0x80001740]:csrrs a7, fcsr, zero
	-[0x80001744]:sw t5, 1888(ra)
	-[0x80001748]:sw t6, 1896(ra)
	-[0x8000174c]:sw t5, 1904(ra)
Current Store : [0x8000174c] : sw t5, 1904(ra) -- Store: [0x8000bf38]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xaf054e65e9fad and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xaf054e65e9fad and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000178c]:fsub.d t5, t3, s10, dyn
	-[0x80001790]:csrrs a7, fcsr, zero
	-[0x80001794]:sw t5, 1920(ra)
	-[0x80001798]:sw t6, 1928(ra)
Current Store : [0x80001798] : sw t6, 1928(ra) -- Store: [0x8000bf50]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xaf054e65e9fad and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xaf054e65e9fad and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000178c]:fsub.d t5, t3, s10, dyn
	-[0x80001790]:csrrs a7, fcsr, zero
	-[0x80001794]:sw t5, 1920(ra)
	-[0x80001798]:sw t6, 1928(ra)
	-[0x8000179c]:sw t5, 1936(ra)
Current Store : [0x8000179c] : sw t5, 1936(ra) -- Store: [0x8000bf58]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xbd7ce681c543f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xbd7ce681c543f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017dc]:fsub.d t5, t3, s10, dyn
	-[0x800017e0]:csrrs a7, fcsr, zero
	-[0x800017e4]:sw t5, 1952(ra)
	-[0x800017e8]:sw t6, 1960(ra)
Current Store : [0x800017e8] : sw t6, 1960(ra) -- Store: [0x8000bf70]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xbd7ce681c543f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xbd7ce681c543f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017dc]:fsub.d t5, t3, s10, dyn
	-[0x800017e0]:csrrs a7, fcsr, zero
	-[0x800017e4]:sw t5, 1952(ra)
	-[0x800017e8]:sw t6, 1960(ra)
	-[0x800017ec]:sw t5, 1968(ra)
Current Store : [0x800017ec] : sw t5, 1968(ra) -- Store: [0x8000bf78]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x052debfe82e13 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x052debfe82e13 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000182c]:fsub.d t5, t3, s10, dyn
	-[0x80001830]:csrrs a7, fcsr, zero
	-[0x80001834]:sw t5, 1984(ra)
	-[0x80001838]:sw t6, 1992(ra)
Current Store : [0x80001838] : sw t6, 1992(ra) -- Store: [0x8000bf90]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x052debfe82e13 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x052debfe82e13 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000182c]:fsub.d t5, t3, s10, dyn
	-[0x80001830]:csrrs a7, fcsr, zero
	-[0x80001834]:sw t5, 1984(ra)
	-[0x80001838]:sw t6, 1992(ra)
	-[0x8000183c]:sw t5, 2000(ra)
Current Store : [0x8000183c] : sw t5, 2000(ra) -- Store: [0x8000bf98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3617941ba03e8 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x3617941ba03e8 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000187c]:fsub.d t5, t3, s10, dyn
	-[0x80001880]:csrrs a7, fcsr, zero
	-[0x80001884]:sw t5, 2016(ra)
	-[0x80001888]:sw t6, 2024(ra)
Current Store : [0x80001888] : sw t6, 2024(ra) -- Store: [0x8000bfb0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3617941ba03e8 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x3617941ba03e8 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000187c]:fsub.d t5, t3, s10, dyn
	-[0x80001880]:csrrs a7, fcsr, zero
	-[0x80001884]:sw t5, 2016(ra)
	-[0x80001888]:sw t6, 2024(ra)
	-[0x8000188c]:sw t5, 2032(ra)
Current Store : [0x8000188c] : sw t5, 2032(ra) -- Store: [0x8000bfb8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f9 and fm1 == 0x551579cd90e3f and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0x551579cd90e3f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018cc]:fsub.d t5, t3, s10, dyn
	-[0x800018d0]:csrrs a7, fcsr, zero
	-[0x800018d4]:addi ra, ra, 2040
	-[0x800018d8]:sw t5, 8(ra)
	-[0x800018dc]:sw t6, 16(ra)
Current Store : [0x800018dc] : sw t6, 16(ra) -- Store: [0x8000bfd0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f9 and fm1 == 0x551579cd90e3f and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0x551579cd90e3f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018cc]:fsub.d t5, t3, s10, dyn
	-[0x800018d0]:csrrs a7, fcsr, zero
	-[0x800018d4]:addi ra, ra, 2040
	-[0x800018d8]:sw t5, 8(ra)
	-[0x800018dc]:sw t6, 16(ra)
	-[0x800018e0]:sw t5, 24(ra)
Current Store : [0x800018e0] : sw t5, 24(ra) -- Store: [0x8000bfd8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0x2bf422090b207 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x2bf422090b207 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001920]:fsub.d t5, t3, s10, dyn
	-[0x80001924]:csrrs a7, fcsr, zero
	-[0x80001928]:sw t5, 40(ra)
	-[0x8000192c]:sw t6, 48(ra)
Current Store : [0x8000192c] : sw t6, 48(ra) -- Store: [0x8000bff0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0x2bf422090b207 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x2bf422090b207 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001920]:fsub.d t5, t3, s10, dyn
	-[0x80001924]:csrrs a7, fcsr, zero
	-[0x80001928]:sw t5, 40(ra)
	-[0x8000192c]:sw t6, 48(ra)
	-[0x80001930]:sw t5, 56(ra)
Current Store : [0x80001930] : sw t5, 56(ra) -- Store: [0x8000bff8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0x482567721754b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x482567721754b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001970]:fsub.d t5, t3, s10, dyn
	-[0x80001974]:csrrs a7, fcsr, zero
	-[0x80001978]:sw t5, 72(ra)
	-[0x8000197c]:sw t6, 80(ra)
Current Store : [0x8000197c] : sw t6, 80(ra) -- Store: [0x8000c010]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0x482567721754b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x482567721754b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001970]:fsub.d t5, t3, s10, dyn
	-[0x80001974]:csrrs a7, fcsr, zero
	-[0x80001978]:sw t5, 72(ra)
	-[0x8000197c]:sw t6, 80(ra)
	-[0x80001980]:sw t5, 88(ra)
Current Store : [0x80001980] : sw t5, 88(ra) -- Store: [0x8000c018]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x5b39db9b4e7ac and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x5b39db9b4e7ac and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800019c0]:fsub.d t5, t3, s10, dyn
	-[0x800019c4]:csrrs a7, fcsr, zero
	-[0x800019c8]:sw t5, 104(ra)
	-[0x800019cc]:sw t6, 112(ra)
Current Store : [0x800019cc] : sw t6, 112(ra) -- Store: [0x8000c030]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x5b39db9b4e7ac and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x5b39db9b4e7ac and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800019c0]:fsub.d t5, t3, s10, dyn
	-[0x800019c4]:csrrs a7, fcsr, zero
	-[0x800019c8]:sw t5, 104(ra)
	-[0x800019cc]:sw t6, 112(ra)
	-[0x800019d0]:sw t5, 120(ra)
Current Store : [0x800019d0] : sw t5, 120(ra) -- Store: [0x8000c038]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc3c9ccfa1b1bb and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xc3c9ccfa1b1bb and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a10]:fsub.d t5, t3, s10, dyn
	-[0x80001a14]:csrrs a7, fcsr, zero
	-[0x80001a18]:sw t5, 136(ra)
	-[0x80001a1c]:sw t6, 144(ra)
Current Store : [0x80001a1c] : sw t6, 144(ra) -- Store: [0x8000c050]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc3c9ccfa1b1bb and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xc3c9ccfa1b1bb and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a10]:fsub.d t5, t3, s10, dyn
	-[0x80001a14]:csrrs a7, fcsr, zero
	-[0x80001a18]:sw t5, 136(ra)
	-[0x80001a1c]:sw t6, 144(ra)
	-[0x80001a20]:sw t5, 152(ra)
Current Store : [0x80001a20] : sw t5, 152(ra) -- Store: [0x8000c058]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x987aaa2c7bb6a and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x987aaa2c7bb6a and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a60]:fsub.d t5, t3, s10, dyn
	-[0x80001a64]:csrrs a7, fcsr, zero
	-[0x80001a68]:sw t5, 168(ra)
	-[0x80001a6c]:sw t6, 176(ra)
Current Store : [0x80001a6c] : sw t6, 176(ra) -- Store: [0x8000c070]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x987aaa2c7bb6a and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x987aaa2c7bb6a and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a60]:fsub.d t5, t3, s10, dyn
	-[0x80001a64]:csrrs a7, fcsr, zero
	-[0x80001a68]:sw t5, 168(ra)
	-[0x80001a6c]:sw t6, 176(ra)
	-[0x80001a70]:sw t5, 184(ra)
Current Store : [0x80001a70] : sw t5, 184(ra) -- Store: [0x8000c078]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc91ade861e02b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xc91ade861e02b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ab0]:fsub.d t5, t3, s10, dyn
	-[0x80001ab4]:csrrs a7, fcsr, zero
	-[0x80001ab8]:sw t5, 200(ra)
	-[0x80001abc]:sw t6, 208(ra)
Current Store : [0x80001abc] : sw t6, 208(ra) -- Store: [0x8000c090]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc91ade861e02b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xc91ade861e02b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ab0]:fsub.d t5, t3, s10, dyn
	-[0x80001ab4]:csrrs a7, fcsr, zero
	-[0x80001ab8]:sw t5, 200(ra)
	-[0x80001abc]:sw t6, 208(ra)
	-[0x80001ac0]:sw t5, 216(ra)
Current Store : [0x80001ac0] : sw t5, 216(ra) -- Store: [0x8000c098]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x946024d663351 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x946024d663351 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b00]:fsub.d t5, t3, s10, dyn
	-[0x80001b04]:csrrs a7, fcsr, zero
	-[0x80001b08]:sw t5, 232(ra)
	-[0x80001b0c]:sw t6, 240(ra)
Current Store : [0x80001b0c] : sw t6, 240(ra) -- Store: [0x8000c0b0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x946024d663351 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x946024d663351 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b00]:fsub.d t5, t3, s10, dyn
	-[0x80001b04]:csrrs a7, fcsr, zero
	-[0x80001b08]:sw t5, 232(ra)
	-[0x80001b0c]:sw t6, 240(ra)
	-[0x80001b10]:sw t5, 248(ra)
Current Store : [0x80001b10] : sw t5, 248(ra) -- Store: [0x8000c0b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x48f4a954751bd and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x48f4a954751bd and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b50]:fsub.d t5, t3, s10, dyn
	-[0x80001b54]:csrrs a7, fcsr, zero
	-[0x80001b58]:sw t5, 264(ra)
	-[0x80001b5c]:sw t6, 272(ra)
Current Store : [0x80001b5c] : sw t6, 272(ra) -- Store: [0x8000c0d0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x48f4a954751bd and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x48f4a954751bd and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b50]:fsub.d t5, t3, s10, dyn
	-[0x80001b54]:csrrs a7, fcsr, zero
	-[0x80001b58]:sw t5, 264(ra)
	-[0x80001b5c]:sw t6, 272(ra)
	-[0x80001b60]:sw t5, 280(ra)
Current Store : [0x80001b60] : sw t5, 280(ra) -- Store: [0x8000c0d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fa and fm1 == 0xe19152f3266af and fs2 == 0 and fe2 == 0x7fa and fm2 == 0xe19152f3266af and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ba0]:fsub.d t5, t3, s10, dyn
	-[0x80001ba4]:csrrs a7, fcsr, zero
	-[0x80001ba8]:sw t5, 296(ra)
	-[0x80001bac]:sw t6, 304(ra)
Current Store : [0x80001bac] : sw t6, 304(ra) -- Store: [0x8000c0f0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fa and fm1 == 0xe19152f3266af and fs2 == 0 and fe2 == 0x7fa and fm2 == 0xe19152f3266af and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ba0]:fsub.d t5, t3, s10, dyn
	-[0x80001ba4]:csrrs a7, fcsr, zero
	-[0x80001ba8]:sw t5, 296(ra)
	-[0x80001bac]:sw t6, 304(ra)
	-[0x80001bb0]:sw t5, 312(ra)
Current Store : [0x80001bb0] : sw t5, 312(ra) -- Store: [0x8000c0f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xef9105cd9390b and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xef9105cd9390b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001bf0]:fsub.d t5, t3, s10, dyn
	-[0x80001bf4]:csrrs a7, fcsr, zero
	-[0x80001bf8]:sw t5, 328(ra)
	-[0x80001bfc]:sw t6, 336(ra)
Current Store : [0x80001bfc] : sw t6, 336(ra) -- Store: [0x8000c110]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xef9105cd9390b and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xef9105cd9390b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001bf0]:fsub.d t5, t3, s10, dyn
	-[0x80001bf4]:csrrs a7, fcsr, zero
	-[0x80001bf8]:sw t5, 328(ra)
	-[0x80001bfc]:sw t6, 336(ra)
	-[0x80001c00]:sw t5, 344(ra)
Current Store : [0x80001c00] : sw t5, 344(ra) -- Store: [0x8000c118]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x39bd6a090d93f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x39bd6a090d93f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c40]:fsub.d t5, t3, s10, dyn
	-[0x80001c44]:csrrs a7, fcsr, zero
	-[0x80001c48]:sw t5, 360(ra)
	-[0x80001c4c]:sw t6, 368(ra)
Current Store : [0x80001c4c] : sw t6, 368(ra) -- Store: [0x8000c130]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x39bd6a090d93f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x39bd6a090d93f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c40]:fsub.d t5, t3, s10, dyn
	-[0x80001c44]:csrrs a7, fcsr, zero
	-[0x80001c48]:sw t5, 360(ra)
	-[0x80001c4c]:sw t6, 368(ra)
	-[0x80001c50]:sw t5, 376(ra)
Current Store : [0x80001c50] : sw t5, 376(ra) -- Store: [0x8000c138]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3dcff67566087 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x3dcff67566087 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c90]:fsub.d t5, t3, s10, dyn
	-[0x80001c94]:csrrs a7, fcsr, zero
	-[0x80001c98]:sw t5, 392(ra)
	-[0x80001c9c]:sw t6, 400(ra)
Current Store : [0x80001c9c] : sw t6, 400(ra) -- Store: [0x8000c150]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3dcff67566087 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x3dcff67566087 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c90]:fsub.d t5, t3, s10, dyn
	-[0x80001c94]:csrrs a7, fcsr, zero
	-[0x80001c98]:sw t5, 392(ra)
	-[0x80001c9c]:sw t6, 400(ra)
	-[0x80001ca0]:sw t5, 408(ra)
Current Store : [0x80001ca0] : sw t5, 408(ra) -- Store: [0x8000c158]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7cd8dfca2011d and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x7cd8dfca2011d and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ce0]:fsub.d t5, t3, s10, dyn
	-[0x80001ce4]:csrrs a7, fcsr, zero
	-[0x80001ce8]:sw t5, 424(ra)
	-[0x80001cec]:sw t6, 432(ra)
Current Store : [0x80001cec] : sw t6, 432(ra) -- Store: [0x8000c170]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7cd8dfca2011d and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x7cd8dfca2011d and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ce0]:fsub.d t5, t3, s10, dyn
	-[0x80001ce4]:csrrs a7, fcsr, zero
	-[0x80001ce8]:sw t5, 424(ra)
	-[0x80001cec]:sw t6, 432(ra)
	-[0x80001cf0]:sw t5, 440(ra)
Current Store : [0x80001cf0] : sw t5, 440(ra) -- Store: [0x8000c178]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x026a2990b0a7f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x026a2990b0a7f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d30]:fsub.d t5, t3, s10, dyn
	-[0x80001d34]:csrrs a7, fcsr, zero
	-[0x80001d38]:sw t5, 456(ra)
	-[0x80001d3c]:sw t6, 464(ra)
Current Store : [0x80001d3c] : sw t6, 464(ra) -- Store: [0x8000c190]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x026a2990b0a7f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x026a2990b0a7f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d30]:fsub.d t5, t3, s10, dyn
	-[0x80001d34]:csrrs a7, fcsr, zero
	-[0x80001d38]:sw t5, 456(ra)
	-[0x80001d3c]:sw t6, 464(ra)
	-[0x80001d40]:sw t5, 472(ra)
Current Store : [0x80001d40] : sw t5, 472(ra) -- Store: [0x8000c198]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc1e74ff66f075 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc1e74ff66f075 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d80]:fsub.d t5, t3, s10, dyn
	-[0x80001d84]:csrrs a7, fcsr, zero
	-[0x80001d88]:sw t5, 488(ra)
	-[0x80001d8c]:sw t6, 496(ra)
Current Store : [0x80001d8c] : sw t6, 496(ra) -- Store: [0x8000c1b0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc1e74ff66f075 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc1e74ff66f075 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d80]:fsub.d t5, t3, s10, dyn
	-[0x80001d84]:csrrs a7, fcsr, zero
	-[0x80001d88]:sw t5, 488(ra)
	-[0x80001d8c]:sw t6, 496(ra)
	-[0x80001d90]:sw t5, 504(ra)
Current Store : [0x80001d90] : sw t5, 504(ra) -- Store: [0x8000c1b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa38a3f0decfff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xa38a3f0decfff and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001dd0]:fsub.d t5, t3, s10, dyn
	-[0x80001dd4]:csrrs a7, fcsr, zero
	-[0x80001dd8]:sw t5, 520(ra)
	-[0x80001ddc]:sw t6, 528(ra)
Current Store : [0x80001ddc] : sw t6, 528(ra) -- Store: [0x8000c1d0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa38a3f0decfff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xa38a3f0decfff and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001dd0]:fsub.d t5, t3, s10, dyn
	-[0x80001dd4]:csrrs a7, fcsr, zero
	-[0x80001dd8]:sw t5, 520(ra)
	-[0x80001ddc]:sw t6, 528(ra)
	-[0x80001de0]:sw t5, 536(ra)
Current Store : [0x80001de0] : sw t5, 536(ra) -- Store: [0x8000c1d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x23fbd09d7e9b6 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x23fbd09d7e9b6 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e20]:fsub.d t5, t3, s10, dyn
	-[0x80001e24]:csrrs a7, fcsr, zero
	-[0x80001e28]:sw t5, 552(ra)
	-[0x80001e2c]:sw t6, 560(ra)
Current Store : [0x80001e2c] : sw t6, 560(ra) -- Store: [0x8000c1f0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x23fbd09d7e9b6 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x23fbd09d7e9b6 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e20]:fsub.d t5, t3, s10, dyn
	-[0x80001e24]:csrrs a7, fcsr, zero
	-[0x80001e28]:sw t5, 552(ra)
	-[0x80001e2c]:sw t6, 560(ra)
	-[0x80001e30]:sw t5, 568(ra)
Current Store : [0x80001e30] : sw t5, 568(ra) -- Store: [0x8000c1f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbc4dccb7ac380 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xbc4dccb7ac380 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e70]:fsub.d t5, t3, s10, dyn
	-[0x80001e74]:csrrs a7, fcsr, zero
	-[0x80001e78]:sw t5, 584(ra)
	-[0x80001e7c]:sw t6, 592(ra)
Current Store : [0x80001e7c] : sw t6, 592(ra) -- Store: [0x8000c210]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbc4dccb7ac380 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xbc4dccb7ac380 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e70]:fsub.d t5, t3, s10, dyn
	-[0x80001e74]:csrrs a7, fcsr, zero
	-[0x80001e78]:sw t5, 584(ra)
	-[0x80001e7c]:sw t6, 592(ra)
	-[0x80001e80]:sw t5, 600(ra)
Current Store : [0x80001e80] : sw t5, 600(ra) -- Store: [0x8000c218]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x6875b8a7de9f5 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x6875b8a7de9f5 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ec0]:fsub.d t5, t3, s10, dyn
	-[0x80001ec4]:csrrs a7, fcsr, zero
	-[0x80001ec8]:sw t5, 616(ra)
	-[0x80001ecc]:sw t6, 624(ra)
Current Store : [0x80001ecc] : sw t6, 624(ra) -- Store: [0x8000c230]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x6875b8a7de9f5 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x6875b8a7de9f5 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ec0]:fsub.d t5, t3, s10, dyn
	-[0x80001ec4]:csrrs a7, fcsr, zero
	-[0x80001ec8]:sw t5, 616(ra)
	-[0x80001ecc]:sw t6, 624(ra)
	-[0x80001ed0]:sw t5, 632(ra)
Current Store : [0x80001ed0] : sw t5, 632(ra) -- Store: [0x8000c238]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x830a4319a6f37 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x830a4319a6f37 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f10]:fsub.d t5, t3, s10, dyn
	-[0x80001f14]:csrrs a7, fcsr, zero
	-[0x80001f18]:sw t5, 648(ra)
	-[0x80001f1c]:sw t6, 656(ra)
Current Store : [0x80001f1c] : sw t6, 656(ra) -- Store: [0x8000c250]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x830a4319a6f37 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x830a4319a6f37 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f10]:fsub.d t5, t3, s10, dyn
	-[0x80001f14]:csrrs a7, fcsr, zero
	-[0x80001f18]:sw t5, 648(ra)
	-[0x80001f1c]:sw t6, 656(ra)
	-[0x80001f20]:sw t5, 664(ra)
Current Store : [0x80001f20] : sw t5, 664(ra) -- Store: [0x8000c258]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xa5666b92c9353 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xa5666b92c9353 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f60]:fsub.d t5, t3, s10, dyn
	-[0x80001f64]:csrrs a7, fcsr, zero
	-[0x80001f68]:sw t5, 680(ra)
	-[0x80001f6c]:sw t6, 688(ra)
Current Store : [0x80001f6c] : sw t6, 688(ra) -- Store: [0x8000c270]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xa5666b92c9353 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xa5666b92c9353 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f60]:fsub.d t5, t3, s10, dyn
	-[0x80001f64]:csrrs a7, fcsr, zero
	-[0x80001f68]:sw t5, 680(ra)
	-[0x80001f6c]:sw t6, 688(ra)
	-[0x80001f70]:sw t5, 696(ra)
Current Store : [0x80001f70] : sw t5, 696(ra) -- Store: [0x8000c278]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xbb5746cbb34cd and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xbb5746cbb34cd and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001fb0]:fsub.d t5, t3, s10, dyn
	-[0x80001fb4]:csrrs a7, fcsr, zero
	-[0x80001fb8]:sw t5, 712(ra)
	-[0x80001fbc]:sw t6, 720(ra)
Current Store : [0x80001fbc] : sw t6, 720(ra) -- Store: [0x8000c290]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xbb5746cbb34cd and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xbb5746cbb34cd and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001fb0]:fsub.d t5, t3, s10, dyn
	-[0x80001fb4]:csrrs a7, fcsr, zero
	-[0x80001fb8]:sw t5, 712(ra)
	-[0x80001fbc]:sw t6, 720(ra)
	-[0x80001fc0]:sw t5, 728(ra)
Current Store : [0x80001fc0] : sw t5, 728(ra) -- Store: [0x8000c298]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xabb8bbe03b7df and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xabb8bbe03b7df and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002000]:fsub.d t5, t3, s10, dyn
	-[0x80002004]:csrrs a7, fcsr, zero
	-[0x80002008]:sw t5, 744(ra)
	-[0x8000200c]:sw t6, 752(ra)
Current Store : [0x8000200c] : sw t6, 752(ra) -- Store: [0x8000c2b0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xabb8bbe03b7df and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xabb8bbe03b7df and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002000]:fsub.d t5, t3, s10, dyn
	-[0x80002004]:csrrs a7, fcsr, zero
	-[0x80002008]:sw t5, 744(ra)
	-[0x8000200c]:sw t6, 752(ra)
	-[0x80002010]:sw t5, 760(ra)
Current Store : [0x80002010] : sw t5, 760(ra) -- Store: [0x8000c2b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x2f2dacc08696f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x2f2dacc08696f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002050]:fsub.d t5, t3, s10, dyn
	-[0x80002054]:csrrs a7, fcsr, zero
	-[0x80002058]:sw t5, 776(ra)
	-[0x8000205c]:sw t6, 784(ra)
Current Store : [0x8000205c] : sw t6, 784(ra) -- Store: [0x8000c2d0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x2f2dacc08696f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x2f2dacc08696f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002050]:fsub.d t5, t3, s10, dyn
	-[0x80002054]:csrrs a7, fcsr, zero
	-[0x80002058]:sw t5, 776(ra)
	-[0x8000205c]:sw t6, 784(ra)
	-[0x80002060]:sw t5, 792(ra)
Current Store : [0x80002060] : sw t5, 792(ra) -- Store: [0x8000c2d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x4d27694e5a38b and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x4d27694e5a38b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800020a0]:fsub.d t5, t3, s10, dyn
	-[0x800020a4]:csrrs a7, fcsr, zero
	-[0x800020a8]:sw t5, 808(ra)
	-[0x800020ac]:sw t6, 816(ra)
Current Store : [0x800020ac] : sw t6, 816(ra) -- Store: [0x8000c2f0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x4d27694e5a38b and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x4d27694e5a38b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800020a0]:fsub.d t5, t3, s10, dyn
	-[0x800020a4]:csrrs a7, fcsr, zero
	-[0x800020a8]:sw t5, 808(ra)
	-[0x800020ac]:sw t6, 816(ra)
	-[0x800020b0]:sw t5, 824(ra)
Current Store : [0x800020b0] : sw t5, 824(ra) -- Store: [0x8000c2f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcca2a15201aa9 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xcca2a15201aa9 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800020f0]:fsub.d t5, t3, s10, dyn
	-[0x800020f4]:csrrs a7, fcsr, zero
	-[0x800020f8]:sw t5, 840(ra)
	-[0x800020fc]:sw t6, 848(ra)
Current Store : [0x800020fc] : sw t6, 848(ra) -- Store: [0x8000c310]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcca2a15201aa9 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xcca2a15201aa9 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800020f0]:fsub.d t5, t3, s10, dyn
	-[0x800020f4]:csrrs a7, fcsr, zero
	-[0x800020f8]:sw t5, 840(ra)
	-[0x800020fc]:sw t6, 848(ra)
	-[0x80002100]:sw t5, 856(ra)
Current Store : [0x80002100] : sw t5, 856(ra) -- Store: [0x8000c318]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0x9b58d2db8786f and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x9b58d2db8786f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002140]:fsub.d t5, t3, s10, dyn
	-[0x80002144]:csrrs a7, fcsr, zero
	-[0x80002148]:sw t5, 872(ra)
	-[0x8000214c]:sw t6, 880(ra)
Current Store : [0x8000214c] : sw t6, 880(ra) -- Store: [0x8000c330]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0x9b58d2db8786f and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x9b58d2db8786f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002140]:fsub.d t5, t3, s10, dyn
	-[0x80002144]:csrrs a7, fcsr, zero
	-[0x80002148]:sw t5, 872(ra)
	-[0x8000214c]:sw t6, 880(ra)
	-[0x80002150]:sw t5, 888(ra)
Current Store : [0x80002150] : sw t5, 888(ra) -- Store: [0x8000c338]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf95e94a40dc56 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf95e94a40dc56 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002190]:fsub.d t5, t3, s10, dyn
	-[0x80002194]:csrrs a7, fcsr, zero
	-[0x80002198]:sw t5, 904(ra)
	-[0x8000219c]:sw t6, 912(ra)
Current Store : [0x8000219c] : sw t6, 912(ra) -- Store: [0x8000c350]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf95e94a40dc56 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf95e94a40dc56 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002190]:fsub.d t5, t3, s10, dyn
	-[0x80002194]:csrrs a7, fcsr, zero
	-[0x80002198]:sw t5, 904(ra)
	-[0x8000219c]:sw t6, 912(ra)
	-[0x800021a0]:sw t5, 920(ra)
Current Store : [0x800021a0] : sw t5, 920(ra) -- Store: [0x8000c358]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x5b3be3b6f1597 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x5b3be3b6f1597 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800021e0]:fsub.d t5, t3, s10, dyn
	-[0x800021e4]:csrrs a7, fcsr, zero
	-[0x800021e8]:sw t5, 936(ra)
	-[0x800021ec]:sw t6, 944(ra)
Current Store : [0x800021ec] : sw t6, 944(ra) -- Store: [0x8000c370]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x5b3be3b6f1597 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x5b3be3b6f1597 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800021e0]:fsub.d t5, t3, s10, dyn
	-[0x800021e4]:csrrs a7, fcsr, zero
	-[0x800021e8]:sw t5, 936(ra)
	-[0x800021ec]:sw t6, 944(ra)
	-[0x800021f0]:sw t5, 952(ra)
Current Store : [0x800021f0] : sw t5, 952(ra) -- Store: [0x8000c378]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x59522cc62b803 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x59522cc62b803 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002230]:fsub.d t5, t3, s10, dyn
	-[0x80002234]:csrrs a7, fcsr, zero
	-[0x80002238]:sw t5, 968(ra)
	-[0x8000223c]:sw t6, 976(ra)
Current Store : [0x8000223c] : sw t6, 976(ra) -- Store: [0x8000c390]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x59522cc62b803 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x59522cc62b803 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002230]:fsub.d t5, t3, s10, dyn
	-[0x80002234]:csrrs a7, fcsr, zero
	-[0x80002238]:sw t5, 968(ra)
	-[0x8000223c]:sw t6, 976(ra)
	-[0x80002240]:sw t5, 984(ra)
Current Store : [0x80002240] : sw t5, 984(ra) -- Store: [0x8000c398]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xdd47ad230c500 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xdd47ad230c500 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002280]:fsub.d t5, t3, s10, dyn
	-[0x80002284]:csrrs a7, fcsr, zero
	-[0x80002288]:sw t5, 1000(ra)
	-[0x8000228c]:sw t6, 1008(ra)
Current Store : [0x8000228c] : sw t6, 1008(ra) -- Store: [0x8000c3b0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xdd47ad230c500 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xdd47ad230c500 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002280]:fsub.d t5, t3, s10, dyn
	-[0x80002284]:csrrs a7, fcsr, zero
	-[0x80002288]:sw t5, 1000(ra)
	-[0x8000228c]:sw t6, 1008(ra)
	-[0x80002290]:sw t5, 1016(ra)
Current Store : [0x80002290] : sw t5, 1016(ra) -- Store: [0x8000c3b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc3c90ab59cc1f and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc3c90ab59cc1f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800022d0]:fsub.d t5, t3, s10, dyn
	-[0x800022d4]:csrrs a7, fcsr, zero
	-[0x800022d8]:sw t5, 1032(ra)
	-[0x800022dc]:sw t6, 1040(ra)
Current Store : [0x800022dc] : sw t6, 1040(ra) -- Store: [0x8000c3d0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc3c90ab59cc1f and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc3c90ab59cc1f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800022d0]:fsub.d t5, t3, s10, dyn
	-[0x800022d4]:csrrs a7, fcsr, zero
	-[0x800022d8]:sw t5, 1032(ra)
	-[0x800022dc]:sw t6, 1040(ra)
	-[0x800022e0]:sw t5, 1048(ra)
Current Store : [0x800022e0] : sw t5, 1048(ra) -- Store: [0x8000c3d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf0206ee24c395 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xf0206ee24c395 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002320]:fsub.d t5, t3, s10, dyn
	-[0x80002324]:csrrs a7, fcsr, zero
	-[0x80002328]:sw t5, 1064(ra)
	-[0x8000232c]:sw t6, 1072(ra)
Current Store : [0x8000232c] : sw t6, 1072(ra) -- Store: [0x8000c3f0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf0206ee24c395 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xf0206ee24c395 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002320]:fsub.d t5, t3, s10, dyn
	-[0x80002324]:csrrs a7, fcsr, zero
	-[0x80002328]:sw t5, 1064(ra)
	-[0x8000232c]:sw t6, 1072(ra)
	-[0x80002330]:sw t5, 1080(ra)
Current Store : [0x80002330] : sw t5, 1080(ra) -- Store: [0x8000c3f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xe08b2a10b8fdf and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xe08b2a10b8fdf and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002370]:fsub.d t5, t3, s10, dyn
	-[0x80002374]:csrrs a7, fcsr, zero
	-[0x80002378]:sw t5, 1096(ra)
	-[0x8000237c]:sw t6, 1104(ra)
Current Store : [0x8000237c] : sw t6, 1104(ra) -- Store: [0x8000c410]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xe08b2a10b8fdf and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xe08b2a10b8fdf and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002370]:fsub.d t5, t3, s10, dyn
	-[0x80002374]:csrrs a7, fcsr, zero
	-[0x80002378]:sw t5, 1096(ra)
	-[0x8000237c]:sw t6, 1104(ra)
	-[0x80002380]:sw t5, 1112(ra)
Current Store : [0x80002380] : sw t5, 1112(ra) -- Store: [0x8000c418]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0x5ad9a8441acdf and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x5ad9a8441acdf and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800023c0]:fsub.d t5, t3, s10, dyn
	-[0x800023c4]:csrrs a7, fcsr, zero
	-[0x800023c8]:sw t5, 1128(ra)
	-[0x800023cc]:sw t6, 1136(ra)
Current Store : [0x800023cc] : sw t6, 1136(ra) -- Store: [0x8000c430]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0x5ad9a8441acdf and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x5ad9a8441acdf and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800023c0]:fsub.d t5, t3, s10, dyn
	-[0x800023c4]:csrrs a7, fcsr, zero
	-[0x800023c8]:sw t5, 1128(ra)
	-[0x800023cc]:sw t6, 1136(ra)
	-[0x800023d0]:sw t5, 1144(ra)
Current Store : [0x800023d0] : sw t5, 1144(ra) -- Store: [0x8000c438]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd26cfda272030 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd26cfda272030 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002410]:fsub.d t5, t3, s10, dyn
	-[0x80002414]:csrrs a7, fcsr, zero
	-[0x80002418]:sw t5, 1160(ra)
	-[0x8000241c]:sw t6, 1168(ra)
Current Store : [0x8000241c] : sw t6, 1168(ra) -- Store: [0x8000c450]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd26cfda272030 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd26cfda272030 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002410]:fsub.d t5, t3, s10, dyn
	-[0x80002414]:csrrs a7, fcsr, zero
	-[0x80002418]:sw t5, 1160(ra)
	-[0x8000241c]:sw t6, 1168(ra)
	-[0x80002420]:sw t5, 1176(ra)
Current Store : [0x80002420] : sw t5, 1176(ra) -- Store: [0x8000c458]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x84aae05543502 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x84aae05543502 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002460]:fsub.d t5, t3, s10, dyn
	-[0x80002464]:csrrs a7, fcsr, zero
	-[0x80002468]:sw t5, 1192(ra)
	-[0x8000246c]:sw t6, 1200(ra)
Current Store : [0x8000246c] : sw t6, 1200(ra) -- Store: [0x8000c470]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x84aae05543502 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x84aae05543502 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002460]:fsub.d t5, t3, s10, dyn
	-[0x80002464]:csrrs a7, fcsr, zero
	-[0x80002468]:sw t5, 1192(ra)
	-[0x8000246c]:sw t6, 1200(ra)
	-[0x80002470]:sw t5, 1208(ra)
Current Store : [0x80002470] : sw t5, 1208(ra) -- Store: [0x8000c478]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xaea8e11056b0f and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xaea8e11056b0f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800024b0]:fsub.d t5, t3, s10, dyn
	-[0x800024b4]:csrrs a7, fcsr, zero
	-[0x800024b8]:sw t5, 1224(ra)
	-[0x800024bc]:sw t6, 1232(ra)
Current Store : [0x800024bc] : sw t6, 1232(ra) -- Store: [0x8000c490]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xaea8e11056b0f and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xaea8e11056b0f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800024b0]:fsub.d t5, t3, s10, dyn
	-[0x800024b4]:csrrs a7, fcsr, zero
	-[0x800024b8]:sw t5, 1224(ra)
	-[0x800024bc]:sw t6, 1232(ra)
	-[0x800024c0]:sw t5, 1240(ra)
Current Store : [0x800024c0] : sw t5, 1240(ra) -- Store: [0x8000c498]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x5c73bb8e94b2b and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x5c73bb8e94b2b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002500]:fsub.d t5, t3, s10, dyn
	-[0x80002504]:csrrs a7, fcsr, zero
	-[0x80002508]:sw t5, 1256(ra)
	-[0x8000250c]:sw t6, 1264(ra)
Current Store : [0x8000250c] : sw t6, 1264(ra) -- Store: [0x8000c4b0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x5c73bb8e94b2b and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x5c73bb8e94b2b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002500]:fsub.d t5, t3, s10, dyn
	-[0x80002504]:csrrs a7, fcsr, zero
	-[0x80002508]:sw t5, 1256(ra)
	-[0x8000250c]:sw t6, 1264(ra)
	-[0x80002510]:sw t5, 1272(ra)
Current Store : [0x80002510] : sw t5, 1272(ra) -- Store: [0x8000c4b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbc1e737c6a698 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xbc1e737c6a698 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002550]:fsub.d t5, t3, s10, dyn
	-[0x80002554]:csrrs a7, fcsr, zero
	-[0x80002558]:sw t5, 1288(ra)
	-[0x8000255c]:sw t6, 1296(ra)
Current Store : [0x8000255c] : sw t6, 1296(ra) -- Store: [0x8000c4d0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbc1e737c6a698 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xbc1e737c6a698 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002550]:fsub.d t5, t3, s10, dyn
	-[0x80002554]:csrrs a7, fcsr, zero
	-[0x80002558]:sw t5, 1288(ra)
	-[0x8000255c]:sw t6, 1296(ra)
	-[0x80002560]:sw t5, 1304(ra)
Current Store : [0x80002560] : sw t5, 1304(ra) -- Store: [0x8000c4d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe89afcadc456f and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xe89afcadc456f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800025a0]:fsub.d t5, t3, s10, dyn
	-[0x800025a4]:csrrs a7, fcsr, zero
	-[0x800025a8]:sw t5, 1320(ra)
	-[0x800025ac]:sw t6, 1328(ra)
Current Store : [0x800025ac] : sw t6, 1328(ra) -- Store: [0x8000c4f0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe89afcadc456f and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xe89afcadc456f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800025a0]:fsub.d t5, t3, s10, dyn
	-[0x800025a4]:csrrs a7, fcsr, zero
	-[0x800025a8]:sw t5, 1320(ra)
	-[0x800025ac]:sw t6, 1328(ra)
	-[0x800025b0]:sw t5, 1336(ra)
Current Store : [0x800025b0] : sw t5, 1336(ra) -- Store: [0x8000c4f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0x153045947810b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x153045947810b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800025f0]:fsub.d t5, t3, s10, dyn
	-[0x800025f4]:csrrs a7, fcsr, zero
	-[0x800025f8]:sw t5, 1352(ra)
	-[0x800025fc]:sw t6, 1360(ra)
Current Store : [0x800025fc] : sw t6, 1360(ra) -- Store: [0x8000c510]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0x153045947810b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x153045947810b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800025f0]:fsub.d t5, t3, s10, dyn
	-[0x800025f4]:csrrs a7, fcsr, zero
	-[0x800025f8]:sw t5, 1352(ra)
	-[0x800025fc]:sw t6, 1360(ra)
	-[0x80002600]:sw t5, 1368(ra)
Current Store : [0x80002600] : sw t5, 1368(ra) -- Store: [0x8000c518]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xab1c42a43630f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xab1c42a43630f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002640]:fsub.d t5, t3, s10, dyn
	-[0x80002644]:csrrs a7, fcsr, zero
	-[0x80002648]:sw t5, 1384(ra)
	-[0x8000264c]:sw t6, 1392(ra)
Current Store : [0x8000264c] : sw t6, 1392(ra) -- Store: [0x8000c530]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xab1c42a43630f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xab1c42a43630f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002640]:fsub.d t5, t3, s10, dyn
	-[0x80002644]:csrrs a7, fcsr, zero
	-[0x80002648]:sw t5, 1384(ra)
	-[0x8000264c]:sw t6, 1392(ra)
	-[0x80002650]:sw t5, 1400(ra)
Current Store : [0x80002650] : sw t5, 1400(ra) -- Store: [0x8000c538]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0x2bccdcc2ad897 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x2bccdcc2ad897 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002690]:fsub.d t5, t3, s10, dyn
	-[0x80002694]:csrrs a7, fcsr, zero
	-[0x80002698]:sw t5, 1416(ra)
	-[0x8000269c]:sw t6, 1424(ra)
Current Store : [0x8000269c] : sw t6, 1424(ra) -- Store: [0x8000c550]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0x2bccdcc2ad897 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x2bccdcc2ad897 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002690]:fsub.d t5, t3, s10, dyn
	-[0x80002694]:csrrs a7, fcsr, zero
	-[0x80002698]:sw t5, 1416(ra)
	-[0x8000269c]:sw t6, 1424(ra)
	-[0x800026a0]:sw t5, 1432(ra)
Current Store : [0x800026a0] : sw t5, 1432(ra) -- Store: [0x8000c558]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9383ffc96dd3f and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x9383ffc96dd3f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800026e0]:fsub.d t5, t3, s10, dyn
	-[0x800026e4]:csrrs a7, fcsr, zero
	-[0x800026e8]:sw t5, 1448(ra)
	-[0x800026ec]:sw t6, 1456(ra)
Current Store : [0x800026ec] : sw t6, 1456(ra) -- Store: [0x8000c570]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9383ffc96dd3f and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x9383ffc96dd3f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800026e0]:fsub.d t5, t3, s10, dyn
	-[0x800026e4]:csrrs a7, fcsr, zero
	-[0x800026e8]:sw t5, 1448(ra)
	-[0x800026ec]:sw t6, 1456(ra)
	-[0x800026f0]:sw t5, 1464(ra)
Current Store : [0x800026f0] : sw t5, 1464(ra) -- Store: [0x8000c578]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x5d14398eae23f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x5d14398eae23f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002730]:fsub.d t5, t3, s10, dyn
	-[0x80002734]:csrrs a7, fcsr, zero
	-[0x80002738]:sw t5, 1480(ra)
	-[0x8000273c]:sw t6, 1488(ra)
Current Store : [0x8000273c] : sw t6, 1488(ra) -- Store: [0x8000c590]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x5d14398eae23f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x5d14398eae23f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002730]:fsub.d t5, t3, s10, dyn
	-[0x80002734]:csrrs a7, fcsr, zero
	-[0x80002738]:sw t5, 1480(ra)
	-[0x8000273c]:sw t6, 1488(ra)
	-[0x80002740]:sw t5, 1496(ra)
Current Store : [0x80002740] : sw t5, 1496(ra) -- Store: [0x8000c598]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9055ab3b464b5 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x9055ab3b464b5 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002780]:fsub.d t5, t3, s10, dyn
	-[0x80002784]:csrrs a7, fcsr, zero
	-[0x80002788]:sw t5, 1512(ra)
	-[0x8000278c]:sw t6, 1520(ra)
Current Store : [0x8000278c] : sw t6, 1520(ra) -- Store: [0x8000c5b0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9055ab3b464b5 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x9055ab3b464b5 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002780]:fsub.d t5, t3, s10, dyn
	-[0x80002784]:csrrs a7, fcsr, zero
	-[0x80002788]:sw t5, 1512(ra)
	-[0x8000278c]:sw t6, 1520(ra)
	-[0x80002790]:sw t5, 1528(ra)
Current Store : [0x80002790] : sw t5, 1528(ra) -- Store: [0x8000c5b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x243d79e337b38 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x243d79e337b38 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800027d0]:fsub.d t5, t3, s10, dyn
	-[0x800027d4]:csrrs a7, fcsr, zero
	-[0x800027d8]:sw t5, 1544(ra)
	-[0x800027dc]:sw t6, 1552(ra)
Current Store : [0x800027dc] : sw t6, 1552(ra) -- Store: [0x8000c5d0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x243d79e337b38 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x243d79e337b38 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800027d0]:fsub.d t5, t3, s10, dyn
	-[0x800027d4]:csrrs a7, fcsr, zero
	-[0x800027d8]:sw t5, 1544(ra)
	-[0x800027dc]:sw t6, 1552(ra)
	-[0x800027e0]:sw t5, 1560(ra)
Current Store : [0x800027e0] : sw t5, 1560(ra) -- Store: [0x8000c5d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0x4ed205e78cd0f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x4ed205e78cd0f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002820]:fsub.d t5, t3, s10, dyn
	-[0x80002824]:csrrs a7, fcsr, zero
	-[0x80002828]:sw t5, 1576(ra)
	-[0x8000282c]:sw t6, 1584(ra)
Current Store : [0x8000282c] : sw t6, 1584(ra) -- Store: [0x8000c5f0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0x4ed205e78cd0f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x4ed205e78cd0f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002820]:fsub.d t5, t3, s10, dyn
	-[0x80002824]:csrrs a7, fcsr, zero
	-[0x80002828]:sw t5, 1576(ra)
	-[0x8000282c]:sw t6, 1584(ra)
	-[0x80002830]:sw t5, 1592(ra)
Current Store : [0x80002830] : sw t5, 1592(ra) -- Store: [0x8000c5f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc812c292ea556 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc812c292ea556 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002870]:fsub.d t5, t3, s10, dyn
	-[0x80002874]:csrrs a7, fcsr, zero
	-[0x80002878]:sw t5, 1608(ra)
	-[0x8000287c]:sw t6, 1616(ra)
Current Store : [0x8000287c] : sw t6, 1616(ra) -- Store: [0x8000c610]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc812c292ea556 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc812c292ea556 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002870]:fsub.d t5, t3, s10, dyn
	-[0x80002874]:csrrs a7, fcsr, zero
	-[0x80002878]:sw t5, 1608(ra)
	-[0x8000287c]:sw t6, 1616(ra)
	-[0x80002880]:sw t5, 1624(ra)
Current Store : [0x80002880] : sw t5, 1624(ra) -- Store: [0x8000c618]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd9a2688750f46 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd9a2688750f46 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800028c0]:fsub.d t5, t3, s10, dyn
	-[0x800028c4]:csrrs a7, fcsr, zero
	-[0x800028c8]:sw t5, 1640(ra)
	-[0x800028cc]:sw t6, 1648(ra)
Current Store : [0x800028cc] : sw t6, 1648(ra) -- Store: [0x8000c630]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd9a2688750f46 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd9a2688750f46 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800028c0]:fsub.d t5, t3, s10, dyn
	-[0x800028c4]:csrrs a7, fcsr, zero
	-[0x800028c8]:sw t5, 1640(ra)
	-[0x800028cc]:sw t6, 1648(ra)
	-[0x800028d0]:sw t5, 1656(ra)
Current Store : [0x800028d0] : sw t5, 1656(ra) -- Store: [0x8000c638]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x1bd5cc8dca1e5 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x1bd5cc8dca1e5 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002910]:fsub.d t5, t3, s10, dyn
	-[0x80002914]:csrrs a7, fcsr, zero
	-[0x80002918]:sw t5, 1672(ra)
	-[0x8000291c]:sw t6, 1680(ra)
Current Store : [0x8000291c] : sw t6, 1680(ra) -- Store: [0x8000c650]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x1bd5cc8dca1e5 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x1bd5cc8dca1e5 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002910]:fsub.d t5, t3, s10, dyn
	-[0x80002914]:csrrs a7, fcsr, zero
	-[0x80002918]:sw t5, 1672(ra)
	-[0x8000291c]:sw t6, 1680(ra)
	-[0x80002920]:sw t5, 1688(ra)
Current Store : [0x80002920] : sw t5, 1688(ra) -- Store: [0x8000c658]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa7d13a52ed5ec and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xa7d13a52ed5ec and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002960]:fsub.d t5, t3, s10, dyn
	-[0x80002964]:csrrs a7, fcsr, zero
	-[0x80002968]:sw t5, 1704(ra)
	-[0x8000296c]:sw t6, 1712(ra)
Current Store : [0x8000296c] : sw t6, 1712(ra) -- Store: [0x8000c670]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa7d13a52ed5ec and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xa7d13a52ed5ec and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002960]:fsub.d t5, t3, s10, dyn
	-[0x80002964]:csrrs a7, fcsr, zero
	-[0x80002968]:sw t5, 1704(ra)
	-[0x8000296c]:sw t6, 1712(ra)
	-[0x80002970]:sw t5, 1720(ra)
Current Store : [0x80002970] : sw t5, 1720(ra) -- Store: [0x8000c678]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x574031c0ee5b5 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x574031c0ee5b5 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800029b0]:fsub.d t5, t3, s10, dyn
	-[0x800029b4]:csrrs a7, fcsr, zero
	-[0x800029b8]:sw t5, 1736(ra)
	-[0x800029bc]:sw t6, 1744(ra)
Current Store : [0x800029bc] : sw t6, 1744(ra) -- Store: [0x8000c690]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x574031c0ee5b5 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x574031c0ee5b5 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800029b0]:fsub.d t5, t3, s10, dyn
	-[0x800029b4]:csrrs a7, fcsr, zero
	-[0x800029b8]:sw t5, 1736(ra)
	-[0x800029bc]:sw t6, 1744(ra)
	-[0x800029c0]:sw t5, 1752(ra)
Current Store : [0x800029c0] : sw t5, 1752(ra) -- Store: [0x8000c698]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xd775b7a6f9327 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xd775b7a6f9327 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002a00]:fsub.d t5, t3, s10, dyn
	-[0x80002a04]:csrrs a7, fcsr, zero
	-[0x80002a08]:sw t5, 1768(ra)
	-[0x80002a0c]:sw t6, 1776(ra)
Current Store : [0x80002a0c] : sw t6, 1776(ra) -- Store: [0x8000c6b0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xd775b7a6f9327 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xd775b7a6f9327 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002a00]:fsub.d t5, t3, s10, dyn
	-[0x80002a04]:csrrs a7, fcsr, zero
	-[0x80002a08]:sw t5, 1768(ra)
	-[0x80002a0c]:sw t6, 1776(ra)
	-[0x80002a10]:sw t5, 1784(ra)
Current Store : [0x80002a10] : sw t5, 1784(ra) -- Store: [0x8000c6b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fa and fm1 == 0x47df70c06ea5f and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x47df70c06ea5f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002a50]:fsub.d t5, t3, s10, dyn
	-[0x80002a54]:csrrs a7, fcsr, zero
	-[0x80002a58]:sw t5, 1800(ra)
	-[0x80002a5c]:sw t6, 1808(ra)
Current Store : [0x80002a5c] : sw t6, 1808(ra) -- Store: [0x8000c6d0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fa and fm1 == 0x47df70c06ea5f and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x47df70c06ea5f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002a50]:fsub.d t5, t3, s10, dyn
	-[0x80002a54]:csrrs a7, fcsr, zero
	-[0x80002a58]:sw t5, 1800(ra)
	-[0x80002a5c]:sw t6, 1808(ra)
	-[0x80002a60]:sw t5, 1816(ra)
Current Store : [0x80002a60] : sw t5, 1816(ra) -- Store: [0x8000c6d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf9efe9258e03a and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf9efe9258e03a and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002aa0]:fsub.d t5, t3, s10, dyn
	-[0x80002aa4]:csrrs a7, fcsr, zero
	-[0x80002aa8]:sw t5, 1832(ra)
	-[0x80002aac]:sw t6, 1840(ra)
Current Store : [0x80002aac] : sw t6, 1840(ra) -- Store: [0x8000c6f0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf9efe9258e03a and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf9efe9258e03a and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002aa0]:fsub.d t5, t3, s10, dyn
	-[0x80002aa4]:csrrs a7, fcsr, zero
	-[0x80002aa8]:sw t5, 1832(ra)
	-[0x80002aac]:sw t6, 1840(ra)
	-[0x80002ab0]:sw t5, 1848(ra)
Current Store : [0x80002ab0] : sw t5, 1848(ra) -- Store: [0x8000c6f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xd95388e6dd7e7 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xd95388e6dd7e7 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002af0]:fsub.d t5, t3, s10, dyn
	-[0x80002af4]:csrrs a7, fcsr, zero
	-[0x80002af8]:sw t5, 1864(ra)
	-[0x80002afc]:sw t6, 1872(ra)
Current Store : [0x80002afc] : sw t6, 1872(ra) -- Store: [0x8000c710]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xd95388e6dd7e7 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xd95388e6dd7e7 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002af0]:fsub.d t5, t3, s10, dyn
	-[0x80002af4]:csrrs a7, fcsr, zero
	-[0x80002af8]:sw t5, 1864(ra)
	-[0x80002afc]:sw t6, 1872(ra)
	-[0x80002b00]:sw t5, 1880(ra)
Current Store : [0x80002b00] : sw t5, 1880(ra) -- Store: [0x8000c718]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xee6dc228b09a7 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xee6dc228b09a7 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002b40]:fsub.d t5, t3, s10, dyn
	-[0x80002b44]:csrrs a7, fcsr, zero
	-[0x80002b48]:sw t5, 1896(ra)
	-[0x80002b4c]:sw t6, 1904(ra)
Current Store : [0x80002b4c] : sw t6, 1904(ra) -- Store: [0x8000c730]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xee6dc228b09a7 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xee6dc228b09a7 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002b40]:fsub.d t5, t3, s10, dyn
	-[0x80002b44]:csrrs a7, fcsr, zero
	-[0x80002b48]:sw t5, 1896(ra)
	-[0x80002b4c]:sw t6, 1904(ra)
	-[0x80002b50]:sw t5, 1912(ra)
Current Store : [0x80002b50] : sw t5, 1912(ra) -- Store: [0x8000c738]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xa5356adec5cbf and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xa5356adec5cbf and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002b90]:fsub.d t5, t3, s10, dyn
	-[0x80002b94]:csrrs a7, fcsr, zero
	-[0x80002b98]:sw t5, 1928(ra)
	-[0x80002b9c]:sw t6, 1936(ra)
Current Store : [0x80002b9c] : sw t6, 1936(ra) -- Store: [0x8000c750]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xa5356adec5cbf and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xa5356adec5cbf and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002b90]:fsub.d t5, t3, s10, dyn
	-[0x80002b94]:csrrs a7, fcsr, zero
	-[0x80002b98]:sw t5, 1928(ra)
	-[0x80002b9c]:sw t6, 1936(ra)
	-[0x80002ba0]:sw t5, 1944(ra)
Current Store : [0x80002ba0] : sw t5, 1944(ra) -- Store: [0x8000c758]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x835b1de73afa3 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x835b1de73afa3 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002be0]:fsub.d t5, t3, s10, dyn
	-[0x80002be4]:csrrs a7, fcsr, zero
	-[0x80002be8]:sw t5, 1960(ra)
	-[0x80002bec]:sw t6, 1968(ra)
Current Store : [0x80002bec] : sw t6, 1968(ra) -- Store: [0x8000c770]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x835b1de73afa3 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x835b1de73afa3 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002be0]:fsub.d t5, t3, s10, dyn
	-[0x80002be4]:csrrs a7, fcsr, zero
	-[0x80002be8]:sw t5, 1960(ra)
	-[0x80002bec]:sw t6, 1968(ra)
	-[0x80002bf0]:sw t5, 1976(ra)
Current Store : [0x80002bf0] : sw t5, 1976(ra) -- Store: [0x8000c778]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd185a4345fd91 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd185a4345fd91 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002c30]:fsub.d t5, t3, s10, dyn
	-[0x80002c34]:csrrs a7, fcsr, zero
	-[0x80002c38]:sw t5, 1992(ra)
	-[0x80002c3c]:sw t6, 2000(ra)
Current Store : [0x80002c3c] : sw t6, 2000(ra) -- Store: [0x8000c790]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd185a4345fd91 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd185a4345fd91 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002c30]:fsub.d t5, t3, s10, dyn
	-[0x80002c34]:csrrs a7, fcsr, zero
	-[0x80002c38]:sw t5, 1992(ra)
	-[0x80002c3c]:sw t6, 2000(ra)
	-[0x80002c40]:sw t5, 2008(ra)
Current Store : [0x80002c40] : sw t5, 2008(ra) -- Store: [0x8000c798]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x037df25b16113 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x037df25b16113 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002c80]:fsub.d t5, t3, s10, dyn
	-[0x80002c84]:csrrs a7, fcsr, zero
	-[0x80002c88]:sw t5, 2024(ra)
	-[0x80002c8c]:sw t6, 2032(ra)
Current Store : [0x80002c8c] : sw t6, 2032(ra) -- Store: [0x8000c7b0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x037df25b16113 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x037df25b16113 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002c80]:fsub.d t5, t3, s10, dyn
	-[0x80002c84]:csrrs a7, fcsr, zero
	-[0x80002c88]:sw t5, 2024(ra)
	-[0x80002c8c]:sw t6, 2032(ra)
	-[0x80002c90]:sw t5, 2040(ra)
Current Store : [0x80002c90] : sw t5, 2040(ra) -- Store: [0x8000c7b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0x9a21046a4c767 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x9a21046a4c767 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002d14]:fsub.d t5, t3, s10, dyn
	-[0x80002d18]:csrrs a7, fcsr, zero
	-[0x80002d1c]:sw t5, 16(ra)
	-[0x80002d20]:sw t6, 24(ra)
Current Store : [0x80002d20] : sw t6, 24(ra) -- Store: [0x8000c7d0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0x9a21046a4c767 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x9a21046a4c767 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002d14]:fsub.d t5, t3, s10, dyn
	-[0x80002d18]:csrrs a7, fcsr, zero
	-[0x80002d1c]:sw t5, 16(ra)
	-[0x80002d20]:sw t6, 24(ra)
	-[0x80002d24]:sw t5, 32(ra)
Current Store : [0x80002d24] : sw t5, 32(ra) -- Store: [0x8000c7d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fa and fm1 == 0x44919c1beab5f and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x44919c1beab5f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002da4]:fsub.d t5, t3, s10, dyn
	-[0x80002da8]:csrrs a7, fcsr, zero
	-[0x80002dac]:sw t5, 48(ra)
	-[0x80002db0]:sw t6, 56(ra)
Current Store : [0x80002db0] : sw t6, 56(ra) -- Store: [0x8000c7f0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fa and fm1 == 0x44919c1beab5f and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x44919c1beab5f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002da4]:fsub.d t5, t3, s10, dyn
	-[0x80002da8]:csrrs a7, fcsr, zero
	-[0x80002dac]:sw t5, 48(ra)
	-[0x80002db0]:sw t6, 56(ra)
	-[0x80002db4]:sw t5, 64(ra)
Current Store : [0x80002db4] : sw t5, 64(ra) -- Store: [0x8000c7f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf715337b3d172 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf715337b3d172 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002e34]:fsub.d t5, t3, s10, dyn
	-[0x80002e38]:csrrs a7, fcsr, zero
	-[0x80002e3c]:sw t5, 80(ra)
	-[0x80002e40]:sw t6, 88(ra)
Current Store : [0x80002e40] : sw t6, 88(ra) -- Store: [0x8000c810]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf715337b3d172 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf715337b3d172 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002e34]:fsub.d t5, t3, s10, dyn
	-[0x80002e38]:csrrs a7, fcsr, zero
	-[0x80002e3c]:sw t5, 80(ra)
	-[0x80002e40]:sw t6, 88(ra)
	-[0x80002e44]:sw t5, 96(ra)
Current Store : [0x80002e44] : sw t5, 96(ra) -- Store: [0x8000c818]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3171b5147eff2 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x3171b5147eff2 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002ec4]:fsub.d t5, t3, s10, dyn
	-[0x80002ec8]:csrrs a7, fcsr, zero
	-[0x80002ecc]:sw t5, 112(ra)
	-[0x80002ed0]:sw t6, 120(ra)
Current Store : [0x80002ed0] : sw t6, 120(ra) -- Store: [0x8000c830]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3171b5147eff2 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x3171b5147eff2 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002ec4]:fsub.d t5, t3, s10, dyn
	-[0x80002ec8]:csrrs a7, fcsr, zero
	-[0x80002ecc]:sw t5, 112(ra)
	-[0x80002ed0]:sw t6, 120(ra)
	-[0x80002ed4]:sw t5, 128(ra)
Current Store : [0x80002ed4] : sw t5, 128(ra) -- Store: [0x8000c838]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x40ccb2b303daf and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x40ccb2b303daf and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002f54]:fsub.d t5, t3, s10, dyn
	-[0x80002f58]:csrrs a7, fcsr, zero
	-[0x80002f5c]:sw t5, 144(ra)
	-[0x80002f60]:sw t6, 152(ra)
Current Store : [0x80002f60] : sw t6, 152(ra) -- Store: [0x8000c850]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x40ccb2b303daf and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x40ccb2b303daf and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002f54]:fsub.d t5, t3, s10, dyn
	-[0x80002f58]:csrrs a7, fcsr, zero
	-[0x80002f5c]:sw t5, 144(ra)
	-[0x80002f60]:sw t6, 152(ra)
	-[0x80002f64]:sw t5, 160(ra)
Current Store : [0x80002f64] : sw t5, 160(ra) -- Store: [0x8000c858]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd3a81e544f745 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd3a81e544f745 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002fe4]:fsub.d t5, t3, s10, dyn
	-[0x80002fe8]:csrrs a7, fcsr, zero
	-[0x80002fec]:sw t5, 176(ra)
	-[0x80002ff0]:sw t6, 184(ra)
Current Store : [0x80002ff0] : sw t6, 184(ra) -- Store: [0x8000c870]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd3a81e544f745 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd3a81e544f745 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002fe4]:fsub.d t5, t3, s10, dyn
	-[0x80002fe8]:csrrs a7, fcsr, zero
	-[0x80002fec]:sw t5, 176(ra)
	-[0x80002ff0]:sw t6, 184(ra)
	-[0x80002ff4]:sw t5, 192(ra)
Current Store : [0x80002ff4] : sw t5, 192(ra) -- Store: [0x8000c878]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf0892add2cc6e and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf0892add2cc6e and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003074]:fsub.d t5, t3, s10, dyn
	-[0x80003078]:csrrs a7, fcsr, zero
	-[0x8000307c]:sw t5, 208(ra)
	-[0x80003080]:sw t6, 216(ra)
Current Store : [0x80003080] : sw t6, 216(ra) -- Store: [0x8000c890]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf0892add2cc6e and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf0892add2cc6e and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003074]:fsub.d t5, t3, s10, dyn
	-[0x80003078]:csrrs a7, fcsr, zero
	-[0x8000307c]:sw t5, 208(ra)
	-[0x80003080]:sw t6, 216(ra)
	-[0x80003084]:sw t5, 224(ra)
Current Store : [0x80003084] : sw t5, 224(ra) -- Store: [0x8000c898]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xb8b73fc8fea5b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xb8b73fc8fea5b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003104]:fsub.d t5, t3, s10, dyn
	-[0x80003108]:csrrs a7, fcsr, zero
	-[0x8000310c]:sw t5, 240(ra)
	-[0x80003110]:sw t6, 248(ra)
Current Store : [0x80003110] : sw t6, 248(ra) -- Store: [0x8000c8b0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xb8b73fc8fea5b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xb8b73fc8fea5b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003104]:fsub.d t5, t3, s10, dyn
	-[0x80003108]:csrrs a7, fcsr, zero
	-[0x8000310c]:sw t5, 240(ra)
	-[0x80003110]:sw t6, 248(ra)
	-[0x80003114]:sw t5, 256(ra)
Current Store : [0x80003114] : sw t5, 256(ra) -- Store: [0x8000c8b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf5bc627909931 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf5bc627909931 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003194]:fsub.d t5, t3, s10, dyn
	-[0x80003198]:csrrs a7, fcsr, zero
	-[0x8000319c]:sw t5, 272(ra)
	-[0x800031a0]:sw t6, 280(ra)
Current Store : [0x800031a0] : sw t6, 280(ra) -- Store: [0x8000c8d0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf5bc627909931 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf5bc627909931 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003194]:fsub.d t5, t3, s10, dyn
	-[0x80003198]:csrrs a7, fcsr, zero
	-[0x8000319c]:sw t5, 272(ra)
	-[0x800031a0]:sw t6, 280(ra)
	-[0x800031a4]:sw t5, 288(ra)
Current Store : [0x800031a4] : sw t5, 288(ra) -- Store: [0x8000c8d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x605a6a1e02c96 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x605a6a1e02c96 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003224]:fsub.d t5, t3, s10, dyn
	-[0x80003228]:csrrs a7, fcsr, zero
	-[0x8000322c]:sw t5, 304(ra)
	-[0x80003230]:sw t6, 312(ra)
Current Store : [0x80003230] : sw t6, 312(ra) -- Store: [0x8000c8f0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x605a6a1e02c96 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x605a6a1e02c96 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003224]:fsub.d t5, t3, s10, dyn
	-[0x80003228]:csrrs a7, fcsr, zero
	-[0x8000322c]:sw t5, 304(ra)
	-[0x80003230]:sw t6, 312(ra)
	-[0x80003234]:sw t5, 320(ra)
Current Store : [0x80003234] : sw t5, 320(ra) -- Store: [0x8000c8f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x094dd69773d7b and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x094dd69773d7b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800032b4]:fsub.d t5, t3, s10, dyn
	-[0x800032b8]:csrrs a7, fcsr, zero
	-[0x800032bc]:sw t5, 336(ra)
	-[0x800032c0]:sw t6, 344(ra)
Current Store : [0x800032c0] : sw t6, 344(ra) -- Store: [0x8000c910]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x094dd69773d7b and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x094dd69773d7b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800032b4]:fsub.d t5, t3, s10, dyn
	-[0x800032b8]:csrrs a7, fcsr, zero
	-[0x800032bc]:sw t5, 336(ra)
	-[0x800032c0]:sw t6, 344(ra)
	-[0x800032c4]:sw t5, 352(ra)
Current Store : [0x800032c4] : sw t5, 352(ra) -- Store: [0x8000c918]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3ab263197fe7f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x3ab263197fe7f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003344]:fsub.d t5, t3, s10, dyn
	-[0x80003348]:csrrs a7, fcsr, zero
	-[0x8000334c]:sw t5, 368(ra)
	-[0x80003350]:sw t6, 376(ra)
Current Store : [0x80003350] : sw t6, 376(ra) -- Store: [0x8000c930]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3ab263197fe7f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x3ab263197fe7f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003344]:fsub.d t5, t3, s10, dyn
	-[0x80003348]:csrrs a7, fcsr, zero
	-[0x8000334c]:sw t5, 368(ra)
	-[0x80003350]:sw t6, 376(ra)
	-[0x80003354]:sw t5, 384(ra)
Current Store : [0x80003354] : sw t5, 384(ra) -- Store: [0x8000c938]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0x4d4955a3d407f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x4d4955a3d407f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800033d4]:fsub.d t5, t3, s10, dyn
	-[0x800033d8]:csrrs a7, fcsr, zero
	-[0x800033dc]:sw t5, 400(ra)
	-[0x800033e0]:sw t6, 408(ra)
Current Store : [0x800033e0] : sw t6, 408(ra) -- Store: [0x8000c950]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0x4d4955a3d407f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x4d4955a3d407f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800033d4]:fsub.d t5, t3, s10, dyn
	-[0x800033d8]:csrrs a7, fcsr, zero
	-[0x800033dc]:sw t5, 400(ra)
	-[0x800033e0]:sw t6, 408(ra)
	-[0x800033e4]:sw t5, 416(ra)
Current Store : [0x800033e4] : sw t5, 416(ra) -- Store: [0x8000c958]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf77d273035d94 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf77d273035d94 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003464]:fsub.d t5, t3, s10, dyn
	-[0x80003468]:csrrs a7, fcsr, zero
	-[0x8000346c]:sw t5, 432(ra)
	-[0x80003470]:sw t6, 440(ra)
Current Store : [0x80003470] : sw t6, 440(ra) -- Store: [0x8000c970]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf77d273035d94 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf77d273035d94 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003464]:fsub.d t5, t3, s10, dyn
	-[0x80003468]:csrrs a7, fcsr, zero
	-[0x8000346c]:sw t5, 432(ra)
	-[0x80003470]:sw t6, 440(ra)
	-[0x80003474]:sw t5, 448(ra)
Current Store : [0x80003474] : sw t5, 448(ra) -- Store: [0x8000c978]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4dd45324c2409 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x4dd45324c2409 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800034f4]:fsub.d t5, t3, s10, dyn
	-[0x800034f8]:csrrs a7, fcsr, zero
	-[0x800034fc]:sw t5, 464(ra)
	-[0x80003500]:sw t6, 472(ra)
Current Store : [0x80003500] : sw t6, 472(ra) -- Store: [0x8000c990]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4dd45324c2409 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x4dd45324c2409 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800034f4]:fsub.d t5, t3, s10, dyn
	-[0x800034f8]:csrrs a7, fcsr, zero
	-[0x800034fc]:sw t5, 464(ra)
	-[0x80003500]:sw t6, 472(ra)
	-[0x80003504]:sw t5, 480(ra)
Current Store : [0x80003504] : sw t5, 480(ra) -- Store: [0x8000c998]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe1a7f48e8e26b and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xe1a7f48e8e26b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003584]:fsub.d t5, t3, s10, dyn
	-[0x80003588]:csrrs a7, fcsr, zero
	-[0x8000358c]:sw t5, 496(ra)
	-[0x80003590]:sw t6, 504(ra)
Current Store : [0x80003590] : sw t6, 504(ra) -- Store: [0x8000c9b0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe1a7f48e8e26b and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xe1a7f48e8e26b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003584]:fsub.d t5, t3, s10, dyn
	-[0x80003588]:csrrs a7, fcsr, zero
	-[0x8000358c]:sw t5, 496(ra)
	-[0x80003590]:sw t6, 504(ra)
	-[0x80003594]:sw t5, 512(ra)
Current Store : [0x80003594] : sw t5, 512(ra) -- Store: [0x8000c9b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3a25a98541333 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x3a25a98541333 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003614]:fsub.d t5, t3, s10, dyn
	-[0x80003618]:csrrs a7, fcsr, zero
	-[0x8000361c]:sw t5, 528(ra)
	-[0x80003620]:sw t6, 536(ra)
Current Store : [0x80003620] : sw t6, 536(ra) -- Store: [0x8000c9d0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3a25a98541333 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x3a25a98541333 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003614]:fsub.d t5, t3, s10, dyn
	-[0x80003618]:csrrs a7, fcsr, zero
	-[0x8000361c]:sw t5, 528(ra)
	-[0x80003620]:sw t6, 536(ra)
	-[0x80003624]:sw t5, 544(ra)
Current Store : [0x80003624] : sw t5, 544(ra) -- Store: [0x8000c9d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x445637e5783c3 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x445637e5783c3 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800036a4]:fsub.d t5, t3, s10, dyn
	-[0x800036a8]:csrrs a7, fcsr, zero
	-[0x800036ac]:sw t5, 560(ra)
	-[0x800036b0]:sw t6, 568(ra)
Current Store : [0x800036b0] : sw t6, 568(ra) -- Store: [0x8000c9f0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x445637e5783c3 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x445637e5783c3 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800036a4]:fsub.d t5, t3, s10, dyn
	-[0x800036a8]:csrrs a7, fcsr, zero
	-[0x800036ac]:sw t5, 560(ra)
	-[0x800036b0]:sw t6, 568(ra)
	-[0x800036b4]:sw t5, 576(ra)
Current Store : [0x800036b4] : sw t5, 576(ra) -- Store: [0x8000c9f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc978dd3af76c1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc978dd3af76c1 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003734]:fsub.d t5, t3, s10, dyn
	-[0x80003738]:csrrs a7, fcsr, zero
	-[0x8000373c]:sw t5, 592(ra)
	-[0x80003740]:sw t6, 600(ra)
Current Store : [0x80003740] : sw t6, 600(ra) -- Store: [0x8000ca10]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc978dd3af76c1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc978dd3af76c1 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003734]:fsub.d t5, t3, s10, dyn
	-[0x80003738]:csrrs a7, fcsr, zero
	-[0x8000373c]:sw t5, 592(ra)
	-[0x80003740]:sw t6, 600(ra)
	-[0x80003744]:sw t5, 608(ra)
Current Store : [0x80003744] : sw t5, 608(ra) -- Store: [0x8000ca18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x565b7f0cebd9f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x565b7f0cebd9f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800037c4]:fsub.d t5, t3, s10, dyn
	-[0x800037c8]:csrrs a7, fcsr, zero
	-[0x800037cc]:sw t5, 624(ra)
	-[0x800037d0]:sw t6, 632(ra)
Current Store : [0x800037d0] : sw t6, 632(ra) -- Store: [0x8000ca30]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x565b7f0cebd9f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x565b7f0cebd9f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800037c4]:fsub.d t5, t3, s10, dyn
	-[0x800037c8]:csrrs a7, fcsr, zero
	-[0x800037cc]:sw t5, 624(ra)
	-[0x800037d0]:sw t6, 632(ra)
	-[0x800037d4]:sw t5, 640(ra)
Current Store : [0x800037d4] : sw t5, 640(ra) -- Store: [0x8000ca38]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x2870c773af305 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x2870c773af305 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003854]:fsub.d t5, t3, s10, dyn
	-[0x80003858]:csrrs a7, fcsr, zero
	-[0x8000385c]:sw t5, 656(ra)
	-[0x80003860]:sw t6, 664(ra)
Current Store : [0x80003860] : sw t6, 664(ra) -- Store: [0x8000ca50]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x2870c773af305 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x2870c773af305 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003854]:fsub.d t5, t3, s10, dyn
	-[0x80003858]:csrrs a7, fcsr, zero
	-[0x8000385c]:sw t5, 656(ra)
	-[0x80003860]:sw t6, 664(ra)
	-[0x80003864]:sw t5, 672(ra)
Current Store : [0x80003864] : sw t5, 672(ra) -- Store: [0x8000ca58]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xf5f5f1385c1af and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xf5f5f1385c1af and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800038e4]:fsub.d t5, t3, s10, dyn
	-[0x800038e8]:csrrs a7, fcsr, zero
	-[0x800038ec]:sw t5, 688(ra)
	-[0x800038f0]:sw t6, 696(ra)
Current Store : [0x800038f0] : sw t6, 696(ra) -- Store: [0x8000ca70]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xf5f5f1385c1af and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xf5f5f1385c1af and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800038e4]:fsub.d t5, t3, s10, dyn
	-[0x800038e8]:csrrs a7, fcsr, zero
	-[0x800038ec]:sw t5, 688(ra)
	-[0x800038f0]:sw t6, 696(ra)
	-[0x800038f4]:sw t5, 704(ra)
Current Store : [0x800038f4] : sw t5, 704(ra) -- Store: [0x8000ca78]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xf3381366daa33 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xf3381366daa33 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003974]:fsub.d t5, t3, s10, dyn
	-[0x80003978]:csrrs a7, fcsr, zero
	-[0x8000397c]:sw t5, 720(ra)
	-[0x80003980]:sw t6, 728(ra)
Current Store : [0x80003980] : sw t6, 728(ra) -- Store: [0x8000ca90]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xf3381366daa33 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xf3381366daa33 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003974]:fsub.d t5, t3, s10, dyn
	-[0x80003978]:csrrs a7, fcsr, zero
	-[0x8000397c]:sw t5, 720(ra)
	-[0x80003980]:sw t6, 728(ra)
	-[0x80003984]:sw t5, 736(ra)
Current Store : [0x80003984] : sw t5, 736(ra) -- Store: [0x8000ca98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x1a3782778609c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x1a3782778609c and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003a04]:fsub.d t5, t3, s10, dyn
	-[0x80003a08]:csrrs a7, fcsr, zero
	-[0x80003a0c]:sw t5, 752(ra)
	-[0x80003a10]:sw t6, 760(ra)
Current Store : [0x80003a10] : sw t6, 760(ra) -- Store: [0x8000cab0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x1a3782778609c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x1a3782778609c and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003a04]:fsub.d t5, t3, s10, dyn
	-[0x80003a08]:csrrs a7, fcsr, zero
	-[0x80003a0c]:sw t5, 752(ra)
	-[0x80003a10]:sw t6, 760(ra)
	-[0x80003a14]:sw t5, 768(ra)
Current Store : [0x80003a14] : sw t5, 768(ra) -- Store: [0x8000cab8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0x33bb4c0b03e47 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x33bb4c0b03e47 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003a94]:fsub.d t5, t3, s10, dyn
	-[0x80003a98]:csrrs a7, fcsr, zero
	-[0x80003a9c]:sw t5, 784(ra)
	-[0x80003aa0]:sw t6, 792(ra)
Current Store : [0x80003aa0] : sw t6, 792(ra) -- Store: [0x8000cad0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0x33bb4c0b03e47 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x33bb4c0b03e47 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003a94]:fsub.d t5, t3, s10, dyn
	-[0x80003a98]:csrrs a7, fcsr, zero
	-[0x80003a9c]:sw t5, 784(ra)
	-[0x80003aa0]:sw t6, 792(ra)
	-[0x80003aa4]:sw t5, 800(ra)
Current Store : [0x80003aa4] : sw t5, 800(ra) -- Store: [0x8000cad8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x29cd1fe017e0f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x29cd1fe017e0f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003b24]:fsub.d t5, t3, s10, dyn
	-[0x80003b28]:csrrs a7, fcsr, zero
	-[0x80003b2c]:sw t5, 816(ra)
	-[0x80003b30]:sw t6, 824(ra)
Current Store : [0x80003b30] : sw t6, 824(ra) -- Store: [0x8000caf0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x29cd1fe017e0f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x29cd1fe017e0f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003b24]:fsub.d t5, t3, s10, dyn
	-[0x80003b28]:csrrs a7, fcsr, zero
	-[0x80003b2c]:sw t5, 816(ra)
	-[0x80003b30]:sw t6, 824(ra)
	-[0x80003b34]:sw t5, 832(ra)
Current Store : [0x80003b34] : sw t5, 832(ra) -- Store: [0x8000caf8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf9196c3c02c3d and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xf9196c3c02c3d and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003bb4]:fsub.d t5, t3, s10, dyn
	-[0x80003bb8]:csrrs a7, fcsr, zero
	-[0x80003bbc]:sw t5, 848(ra)
	-[0x80003bc0]:sw t6, 856(ra)
Current Store : [0x80003bc0] : sw t6, 856(ra) -- Store: [0x8000cb10]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf9196c3c02c3d and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xf9196c3c02c3d and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003bb4]:fsub.d t5, t3, s10, dyn
	-[0x80003bb8]:csrrs a7, fcsr, zero
	-[0x80003bbc]:sw t5, 848(ra)
	-[0x80003bc0]:sw t6, 856(ra)
	-[0x80003bc4]:sw t5, 864(ra)
Current Store : [0x80003bc4] : sw t5, 864(ra) -- Store: [0x8000cb18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0x3bc28319d6d6f and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x3bc28319d6d6f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003c44]:fsub.d t5, t3, s10, dyn
	-[0x80003c48]:csrrs a7, fcsr, zero
	-[0x80003c4c]:sw t5, 880(ra)
	-[0x80003c50]:sw t6, 888(ra)
Current Store : [0x80003c50] : sw t6, 888(ra) -- Store: [0x8000cb30]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0x3bc28319d6d6f and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x3bc28319d6d6f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003c44]:fsub.d t5, t3, s10, dyn
	-[0x80003c48]:csrrs a7, fcsr, zero
	-[0x80003c4c]:sw t5, 880(ra)
	-[0x80003c50]:sw t6, 888(ra)
	-[0x80003c54]:sw t5, 896(ra)
Current Store : [0x80003c54] : sw t5, 896(ra) -- Store: [0x8000cb38]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x97081394ff7c0 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x97081394ff7c0 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003cd4]:fsub.d t5, t3, s10, dyn
	-[0x80003cd8]:csrrs a7, fcsr, zero
	-[0x80003cdc]:sw t5, 912(ra)
	-[0x80003ce0]:sw t6, 920(ra)
Current Store : [0x80003ce0] : sw t6, 920(ra) -- Store: [0x8000cb50]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x97081394ff7c0 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x97081394ff7c0 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003cd4]:fsub.d t5, t3, s10, dyn
	-[0x80003cd8]:csrrs a7, fcsr, zero
	-[0x80003cdc]:sw t5, 912(ra)
	-[0x80003ce0]:sw t6, 920(ra)
	-[0x80003ce4]:sw t5, 928(ra)
Current Store : [0x80003ce4] : sw t5, 928(ra) -- Store: [0x8000cb58]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe60134aa9369f and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xe60134aa9369f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003d64]:fsub.d t5, t3, s10, dyn
	-[0x80003d68]:csrrs a7, fcsr, zero
	-[0x80003d6c]:sw t5, 944(ra)
	-[0x80003d70]:sw t6, 952(ra)
Current Store : [0x80003d70] : sw t6, 952(ra) -- Store: [0x8000cb70]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe60134aa9369f and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xe60134aa9369f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003d64]:fsub.d t5, t3, s10, dyn
	-[0x80003d68]:csrrs a7, fcsr, zero
	-[0x80003d6c]:sw t5, 944(ra)
	-[0x80003d70]:sw t6, 952(ra)
	-[0x80003d74]:sw t5, 960(ra)
Current Store : [0x80003d74] : sw t5, 960(ra) -- Store: [0x8000cb78]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8d6b438992705 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x8d6b438992705 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003df4]:fsub.d t5, t3, s10, dyn
	-[0x80003df8]:csrrs a7, fcsr, zero
	-[0x80003dfc]:sw t5, 976(ra)
	-[0x80003e00]:sw t6, 984(ra)
Current Store : [0x80003e00] : sw t6, 984(ra) -- Store: [0x8000cb90]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8d6b438992705 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x8d6b438992705 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003df4]:fsub.d t5, t3, s10, dyn
	-[0x80003df8]:csrrs a7, fcsr, zero
	-[0x80003dfc]:sw t5, 976(ra)
	-[0x80003e00]:sw t6, 984(ra)
	-[0x80003e04]:sw t5, 992(ra)
Current Store : [0x80003e04] : sw t5, 992(ra) -- Store: [0x8000cb98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa4a2387765198 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xa4a2387765198 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003e84]:fsub.d t5, t3, s10, dyn
	-[0x80003e88]:csrrs a7, fcsr, zero
	-[0x80003e8c]:sw t5, 1008(ra)
	-[0x80003e90]:sw t6, 1016(ra)
Current Store : [0x80003e90] : sw t6, 1016(ra) -- Store: [0x8000cbb0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa4a2387765198 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xa4a2387765198 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003e84]:fsub.d t5, t3, s10, dyn
	-[0x80003e88]:csrrs a7, fcsr, zero
	-[0x80003e8c]:sw t5, 1008(ra)
	-[0x80003e90]:sw t6, 1016(ra)
	-[0x80003e94]:sw t5, 1024(ra)
Current Store : [0x80003e94] : sw t5, 1024(ra) -- Store: [0x8000cbb8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xb5c56d6b2c837 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xb5c56d6b2c837 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003f14]:fsub.d t5, t3, s10, dyn
	-[0x80003f18]:csrrs a7, fcsr, zero
	-[0x80003f1c]:sw t5, 1040(ra)
	-[0x80003f20]:sw t6, 1048(ra)
Current Store : [0x80003f20] : sw t6, 1048(ra) -- Store: [0x8000cbd0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xb5c56d6b2c837 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xb5c56d6b2c837 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003f14]:fsub.d t5, t3, s10, dyn
	-[0x80003f18]:csrrs a7, fcsr, zero
	-[0x80003f1c]:sw t5, 1040(ra)
	-[0x80003f20]:sw t6, 1048(ra)
	-[0x80003f24]:sw t5, 1056(ra)
Current Store : [0x80003f24] : sw t5, 1056(ra) -- Store: [0x8000cbd8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd6b5f3e68568b and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd6b5f3e68568b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003fa4]:fsub.d t5, t3, s10, dyn
	-[0x80003fa8]:csrrs a7, fcsr, zero
	-[0x80003fac]:sw t5, 1072(ra)
	-[0x80003fb0]:sw t6, 1080(ra)
Current Store : [0x80003fb0] : sw t6, 1080(ra) -- Store: [0x8000cbf0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd6b5f3e68568b and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd6b5f3e68568b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003fa4]:fsub.d t5, t3, s10, dyn
	-[0x80003fa8]:csrrs a7, fcsr, zero
	-[0x80003fac]:sw t5, 1072(ra)
	-[0x80003fb0]:sw t6, 1080(ra)
	-[0x80003fb4]:sw t5, 1088(ra)
Current Store : [0x80003fb4] : sw t5, 1088(ra) -- Store: [0x8000cbf8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc99ac0cd3b3ca and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc99ac0cd3b3ca and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004034]:fsub.d t5, t3, s10, dyn
	-[0x80004038]:csrrs a7, fcsr, zero
	-[0x8000403c]:sw t5, 1104(ra)
	-[0x80004040]:sw t6, 1112(ra)
Current Store : [0x80004040] : sw t6, 1112(ra) -- Store: [0x8000cc10]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc99ac0cd3b3ca and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc99ac0cd3b3ca and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004034]:fsub.d t5, t3, s10, dyn
	-[0x80004038]:csrrs a7, fcsr, zero
	-[0x8000403c]:sw t5, 1104(ra)
	-[0x80004040]:sw t6, 1112(ra)
	-[0x80004044]:sw t5, 1120(ra)
Current Store : [0x80004044] : sw t5, 1120(ra) -- Store: [0x8000cc18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd22aa76e3f8bc and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd22aa76e3f8bc and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800040c4]:fsub.d t5, t3, s10, dyn
	-[0x800040c8]:csrrs a7, fcsr, zero
	-[0x800040cc]:sw t5, 1136(ra)
	-[0x800040d0]:sw t6, 1144(ra)
Current Store : [0x800040d0] : sw t6, 1144(ra) -- Store: [0x8000cc30]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd22aa76e3f8bc and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd22aa76e3f8bc and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800040c4]:fsub.d t5, t3, s10, dyn
	-[0x800040c8]:csrrs a7, fcsr, zero
	-[0x800040cc]:sw t5, 1136(ra)
	-[0x800040d0]:sw t6, 1144(ra)
	-[0x800040d4]:sw t5, 1152(ra)
Current Store : [0x800040d4] : sw t5, 1152(ra) -- Store: [0x8000cc38]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x9efa662b0261b and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x9efa662b0261b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800074e4]:fsub.d t5, t3, s10, dyn
	-[0x800074e8]:csrrs a7, fcsr, zero
	-[0x800074ec]:sw t5, 0(ra)
	-[0x800074f0]:sw t6, 8(ra)
Current Store : [0x800074f0] : sw t6, 8(ra) -- Store: [0x8000c7d0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x9efa662b0261b and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x9efa662b0261b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800074e4]:fsub.d t5, t3, s10, dyn
	-[0x800074e8]:csrrs a7, fcsr, zero
	-[0x800074ec]:sw t5, 0(ra)
	-[0x800074f0]:sw t6, 8(ra)
	-[0x800074f4]:sw t5, 16(ra)
Current Store : [0x800074f4] : sw t5, 16(ra) -- Store: [0x8000c7d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x2b3a267e5dfb6 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x2b3a267e5dfb6 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007534]:fsub.d t5, t3, s10, dyn
	-[0x80007538]:csrrs a7, fcsr, zero
	-[0x8000753c]:sw t5, 32(ra)
	-[0x80007540]:sw t6, 40(ra)
Current Store : [0x80007540] : sw t6, 40(ra) -- Store: [0x8000c7f0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x2b3a267e5dfb6 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x2b3a267e5dfb6 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007534]:fsub.d t5, t3, s10, dyn
	-[0x80007538]:csrrs a7, fcsr, zero
	-[0x8000753c]:sw t5, 32(ra)
	-[0x80007540]:sw t6, 40(ra)
	-[0x80007544]:sw t5, 48(ra)
Current Store : [0x80007544] : sw t5, 48(ra) -- Store: [0x8000c7f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcbbac03deb701 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xcbbac03deb701 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007584]:fsub.d t5, t3, s10, dyn
	-[0x80007588]:csrrs a7, fcsr, zero
	-[0x8000758c]:sw t5, 64(ra)
	-[0x80007590]:sw t6, 72(ra)
Current Store : [0x80007590] : sw t6, 72(ra) -- Store: [0x8000c810]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcbbac03deb701 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xcbbac03deb701 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007584]:fsub.d t5, t3, s10, dyn
	-[0x80007588]:csrrs a7, fcsr, zero
	-[0x8000758c]:sw t5, 64(ra)
	-[0x80007590]:sw t6, 72(ra)
	-[0x80007594]:sw t5, 80(ra)
Current Store : [0x80007594] : sw t5, 80(ra) -- Store: [0x8000c818]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf886e2fe6ac5f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xf886e2fe6ac5f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800075d4]:fsub.d t5, t3, s10, dyn
	-[0x800075d8]:csrrs a7, fcsr, zero
	-[0x800075dc]:sw t5, 96(ra)
	-[0x800075e0]:sw t6, 104(ra)
Current Store : [0x800075e0] : sw t6, 104(ra) -- Store: [0x8000c830]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf886e2fe6ac5f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xf886e2fe6ac5f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800075d4]:fsub.d t5, t3, s10, dyn
	-[0x800075d8]:csrrs a7, fcsr, zero
	-[0x800075dc]:sw t5, 96(ra)
	-[0x800075e0]:sw t6, 104(ra)
	-[0x800075e4]:sw t5, 112(ra)
Current Store : [0x800075e4] : sw t5, 112(ra) -- Store: [0x8000c838]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x47dca9bde3664 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x47dca9bde3664 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007624]:fsub.d t5, t3, s10, dyn
	-[0x80007628]:csrrs a7, fcsr, zero
	-[0x8000762c]:sw t5, 128(ra)
	-[0x80007630]:sw t6, 136(ra)
Current Store : [0x80007630] : sw t6, 136(ra) -- Store: [0x8000c850]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x47dca9bde3664 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x47dca9bde3664 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007624]:fsub.d t5, t3, s10, dyn
	-[0x80007628]:csrrs a7, fcsr, zero
	-[0x8000762c]:sw t5, 128(ra)
	-[0x80007630]:sw t6, 136(ra)
	-[0x80007634]:sw t5, 144(ra)
Current Store : [0x80007634] : sw t5, 144(ra) -- Store: [0x8000c858]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4264cf0154662 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x4264cf0154662 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007674]:fsub.d t5, t3, s10, dyn
	-[0x80007678]:csrrs a7, fcsr, zero
	-[0x8000767c]:sw t5, 160(ra)
	-[0x80007680]:sw t6, 168(ra)
Current Store : [0x80007680] : sw t6, 168(ra) -- Store: [0x8000c870]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4264cf0154662 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x4264cf0154662 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007674]:fsub.d t5, t3, s10, dyn
	-[0x80007678]:csrrs a7, fcsr, zero
	-[0x8000767c]:sw t5, 160(ra)
	-[0x80007680]:sw t6, 168(ra)
	-[0x80007684]:sw t5, 176(ra)
Current Store : [0x80007684] : sw t5, 176(ra) -- Store: [0x8000c878]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x422ea209fd4bd and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x422ea209fd4bd and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800076c4]:fsub.d t5, t3, s10, dyn
	-[0x800076c8]:csrrs a7, fcsr, zero
	-[0x800076cc]:sw t5, 192(ra)
	-[0x800076d0]:sw t6, 200(ra)
Current Store : [0x800076d0] : sw t6, 200(ra) -- Store: [0x8000c890]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x422ea209fd4bd and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x422ea209fd4bd and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800076c4]:fsub.d t5, t3, s10, dyn
	-[0x800076c8]:csrrs a7, fcsr, zero
	-[0x800076cc]:sw t5, 192(ra)
	-[0x800076d0]:sw t6, 200(ra)
	-[0x800076d4]:sw t5, 208(ra)
Current Store : [0x800076d4] : sw t5, 208(ra) -- Store: [0x8000c898]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xdfcdecd96da66 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xdfcdecd96da66 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007714]:fsub.d t5, t3, s10, dyn
	-[0x80007718]:csrrs a7, fcsr, zero
	-[0x8000771c]:sw t5, 224(ra)
	-[0x80007720]:sw t6, 232(ra)
Current Store : [0x80007720] : sw t6, 232(ra) -- Store: [0x8000c8b0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xdfcdecd96da66 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xdfcdecd96da66 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007714]:fsub.d t5, t3, s10, dyn
	-[0x80007718]:csrrs a7, fcsr, zero
	-[0x8000771c]:sw t5, 224(ra)
	-[0x80007720]:sw t6, 232(ra)
	-[0x80007724]:sw t5, 240(ra)
Current Store : [0x80007724] : sw t5, 240(ra) -- Store: [0x8000c8b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb14a0c4b66d3b and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xb14a0c4b66d3b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007764]:fsub.d t5, t3, s10, dyn
	-[0x80007768]:csrrs a7, fcsr, zero
	-[0x8000776c]:sw t5, 256(ra)
	-[0x80007770]:sw t6, 264(ra)
Current Store : [0x80007770] : sw t6, 264(ra) -- Store: [0x8000c8d0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb14a0c4b66d3b and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xb14a0c4b66d3b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007764]:fsub.d t5, t3, s10, dyn
	-[0x80007768]:csrrs a7, fcsr, zero
	-[0x8000776c]:sw t5, 256(ra)
	-[0x80007770]:sw t6, 264(ra)
	-[0x80007774]:sw t5, 272(ra)
Current Store : [0x80007774] : sw t5, 272(ra) -- Store: [0x8000c8d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa1726431ab40b and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xa1726431ab40b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800077b4]:fsub.d t5, t3, s10, dyn
	-[0x800077b8]:csrrs a7, fcsr, zero
	-[0x800077bc]:sw t5, 288(ra)
	-[0x800077c0]:sw t6, 296(ra)
Current Store : [0x800077c0] : sw t6, 296(ra) -- Store: [0x8000c8f0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa1726431ab40b and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xa1726431ab40b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800077b4]:fsub.d t5, t3, s10, dyn
	-[0x800077b8]:csrrs a7, fcsr, zero
	-[0x800077bc]:sw t5, 288(ra)
	-[0x800077c0]:sw t6, 296(ra)
	-[0x800077c4]:sw t5, 304(ra)
Current Store : [0x800077c4] : sw t5, 304(ra) -- Store: [0x8000c8f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x65d63e74d209d and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x65d63e74d209d and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007804]:fsub.d t5, t3, s10, dyn
	-[0x80007808]:csrrs a7, fcsr, zero
	-[0x8000780c]:sw t5, 320(ra)
	-[0x80007810]:sw t6, 328(ra)
Current Store : [0x80007810] : sw t6, 328(ra) -- Store: [0x8000c910]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x65d63e74d209d and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x65d63e74d209d and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007804]:fsub.d t5, t3, s10, dyn
	-[0x80007808]:csrrs a7, fcsr, zero
	-[0x8000780c]:sw t5, 320(ra)
	-[0x80007810]:sw t6, 328(ra)
	-[0x80007814]:sw t5, 336(ra)
Current Store : [0x80007814] : sw t5, 336(ra) -- Store: [0x8000c918]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x6a8199da501dc and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x6a8199da501dc and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007854]:fsub.d t5, t3, s10, dyn
	-[0x80007858]:csrrs a7, fcsr, zero
	-[0x8000785c]:sw t5, 352(ra)
	-[0x80007860]:sw t6, 360(ra)
Current Store : [0x80007860] : sw t6, 360(ra) -- Store: [0x8000c930]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x6a8199da501dc and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x6a8199da501dc and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007854]:fsub.d t5, t3, s10, dyn
	-[0x80007858]:csrrs a7, fcsr, zero
	-[0x8000785c]:sw t5, 352(ra)
	-[0x80007860]:sw t6, 360(ra)
	-[0x80007864]:sw t5, 368(ra)
Current Store : [0x80007864] : sw t5, 368(ra) -- Store: [0x8000c938]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x16a782d36f4f6 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x16a782d36f4f6 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800078a4]:fsub.d t5, t3, s10, dyn
	-[0x800078a8]:csrrs a7, fcsr, zero
	-[0x800078ac]:sw t5, 384(ra)
	-[0x800078b0]:sw t6, 392(ra)
Current Store : [0x800078b0] : sw t6, 392(ra) -- Store: [0x8000c950]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x16a782d36f4f6 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x16a782d36f4f6 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800078a4]:fsub.d t5, t3, s10, dyn
	-[0x800078a8]:csrrs a7, fcsr, zero
	-[0x800078ac]:sw t5, 384(ra)
	-[0x800078b0]:sw t6, 392(ra)
	-[0x800078b4]:sw t5, 400(ra)
Current Store : [0x800078b4] : sw t5, 400(ra) -- Store: [0x8000c958]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xaa7bbc9099344 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xaa7bbc9099344 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800078f4]:fsub.d t5, t3, s10, dyn
	-[0x800078f8]:csrrs a7, fcsr, zero
	-[0x800078fc]:sw t5, 416(ra)
	-[0x80007900]:sw t6, 424(ra)
Current Store : [0x80007900] : sw t6, 424(ra) -- Store: [0x8000c970]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xaa7bbc9099344 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xaa7bbc9099344 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800078f4]:fsub.d t5, t3, s10, dyn
	-[0x800078f8]:csrrs a7, fcsr, zero
	-[0x800078fc]:sw t5, 416(ra)
	-[0x80007900]:sw t6, 424(ra)
	-[0x80007904]:sw t5, 432(ra)
Current Store : [0x80007904] : sw t5, 432(ra) -- Store: [0x8000c978]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xde5026c152607 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xde5026c152607 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007944]:fsub.d t5, t3, s10, dyn
	-[0x80007948]:csrrs a7, fcsr, zero
	-[0x8000794c]:sw t5, 448(ra)
	-[0x80007950]:sw t6, 456(ra)
Current Store : [0x80007950] : sw t6, 456(ra) -- Store: [0x8000c990]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xde5026c152607 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xde5026c152607 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007944]:fsub.d t5, t3, s10, dyn
	-[0x80007948]:csrrs a7, fcsr, zero
	-[0x8000794c]:sw t5, 448(ra)
	-[0x80007950]:sw t6, 456(ra)
	-[0x80007954]:sw t5, 464(ra)
Current Store : [0x80007954] : sw t5, 464(ra) -- Store: [0x8000c998]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x05438a864ff48 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x05438a864ff48 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007994]:fsub.d t5, t3, s10, dyn
	-[0x80007998]:csrrs a7, fcsr, zero
	-[0x8000799c]:sw t5, 480(ra)
	-[0x800079a0]:sw t6, 488(ra)
Current Store : [0x800079a0] : sw t6, 488(ra) -- Store: [0x8000c9b0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x05438a864ff48 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x05438a864ff48 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007994]:fsub.d t5, t3, s10, dyn
	-[0x80007998]:csrrs a7, fcsr, zero
	-[0x8000799c]:sw t5, 480(ra)
	-[0x800079a0]:sw t6, 488(ra)
	-[0x800079a4]:sw t5, 496(ra)
Current Store : [0x800079a4] : sw t5, 496(ra) -- Store: [0x8000c9b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xb0bd7b08edb55 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xb0bd7b08edb55 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800079e4]:fsub.d t5, t3, s10, dyn
	-[0x800079e8]:csrrs a7, fcsr, zero
	-[0x800079ec]:sw t5, 512(ra)
	-[0x800079f0]:sw t6, 520(ra)
Current Store : [0x800079f0] : sw t6, 520(ra) -- Store: [0x8000c9d0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xb0bd7b08edb55 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xb0bd7b08edb55 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800079e4]:fsub.d t5, t3, s10, dyn
	-[0x800079e8]:csrrs a7, fcsr, zero
	-[0x800079ec]:sw t5, 512(ra)
	-[0x800079f0]:sw t6, 520(ra)
	-[0x800079f4]:sw t5, 528(ra)
Current Store : [0x800079f4] : sw t5, 528(ra) -- Store: [0x8000c9d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x504dcbdc51a65 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x504dcbdc51a65 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007a34]:fsub.d t5, t3, s10, dyn
	-[0x80007a38]:csrrs a7, fcsr, zero
	-[0x80007a3c]:sw t5, 544(ra)
	-[0x80007a40]:sw t6, 552(ra)
Current Store : [0x80007a40] : sw t6, 552(ra) -- Store: [0x8000c9f0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x504dcbdc51a65 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x504dcbdc51a65 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007a34]:fsub.d t5, t3, s10, dyn
	-[0x80007a38]:csrrs a7, fcsr, zero
	-[0x80007a3c]:sw t5, 544(ra)
	-[0x80007a40]:sw t6, 552(ra)
	-[0x80007a44]:sw t5, 560(ra)
Current Store : [0x80007a44] : sw t5, 560(ra) -- Store: [0x8000c9f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x03aaf26d74a36 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x03aaf26d74a36 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007a84]:fsub.d t5, t3, s10, dyn
	-[0x80007a88]:csrrs a7, fcsr, zero
	-[0x80007a8c]:sw t5, 576(ra)
	-[0x80007a90]:sw t6, 584(ra)
Current Store : [0x80007a90] : sw t6, 584(ra) -- Store: [0x8000ca10]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x03aaf26d74a36 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x03aaf26d74a36 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007a84]:fsub.d t5, t3, s10, dyn
	-[0x80007a88]:csrrs a7, fcsr, zero
	-[0x80007a8c]:sw t5, 576(ra)
	-[0x80007a90]:sw t6, 584(ra)
	-[0x80007a94]:sw t5, 592(ra)
Current Store : [0x80007a94] : sw t5, 592(ra) -- Store: [0x8000ca18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x618258c5f4965 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x618258c5f4965 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007ad4]:fsub.d t5, t3, s10, dyn
	-[0x80007ad8]:csrrs a7, fcsr, zero
	-[0x80007adc]:sw t5, 608(ra)
	-[0x80007ae0]:sw t6, 616(ra)
Current Store : [0x80007ae0] : sw t6, 616(ra) -- Store: [0x8000ca30]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x618258c5f4965 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x618258c5f4965 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007ad4]:fsub.d t5, t3, s10, dyn
	-[0x80007ad8]:csrrs a7, fcsr, zero
	-[0x80007adc]:sw t5, 608(ra)
	-[0x80007ae0]:sw t6, 616(ra)
	-[0x80007ae4]:sw t5, 624(ra)
Current Store : [0x80007ae4] : sw t5, 624(ra) -- Store: [0x8000ca38]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x60b89491a6a27 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x60b89491a6a27 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007b24]:fsub.d t5, t3, s10, dyn
	-[0x80007b28]:csrrs a7, fcsr, zero
	-[0x80007b2c]:sw t5, 640(ra)
	-[0x80007b30]:sw t6, 648(ra)
Current Store : [0x80007b30] : sw t6, 648(ra) -- Store: [0x8000ca50]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x60b89491a6a27 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x60b89491a6a27 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007b24]:fsub.d t5, t3, s10, dyn
	-[0x80007b28]:csrrs a7, fcsr, zero
	-[0x80007b2c]:sw t5, 640(ra)
	-[0x80007b30]:sw t6, 648(ra)
	-[0x80007b34]:sw t5, 656(ra)
Current Store : [0x80007b34] : sw t5, 656(ra) -- Store: [0x8000ca58]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xabce33873116b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xabce33873116b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007b74]:fsub.d t5, t3, s10, dyn
	-[0x80007b78]:csrrs a7, fcsr, zero
	-[0x80007b7c]:sw t5, 672(ra)
	-[0x80007b80]:sw t6, 680(ra)
Current Store : [0x80007b80] : sw t6, 680(ra) -- Store: [0x8000ca70]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xabce33873116b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xabce33873116b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007b74]:fsub.d t5, t3, s10, dyn
	-[0x80007b78]:csrrs a7, fcsr, zero
	-[0x80007b7c]:sw t5, 672(ra)
	-[0x80007b80]:sw t6, 680(ra)
	-[0x80007b84]:sw t5, 688(ra)
Current Store : [0x80007b84] : sw t5, 688(ra) -- Store: [0x8000ca78]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc5a51b555f5c9 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc5a51b555f5c9 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007bc4]:fsub.d t5, t3, s10, dyn
	-[0x80007bc8]:csrrs a7, fcsr, zero
	-[0x80007bcc]:sw t5, 704(ra)
	-[0x80007bd0]:sw t6, 712(ra)
Current Store : [0x80007bd0] : sw t6, 712(ra) -- Store: [0x8000ca90]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc5a51b555f5c9 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc5a51b555f5c9 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007bc4]:fsub.d t5, t3, s10, dyn
	-[0x80007bc8]:csrrs a7, fcsr, zero
	-[0x80007bcc]:sw t5, 704(ra)
	-[0x80007bd0]:sw t6, 712(ra)
	-[0x80007bd4]:sw t5, 720(ra)
Current Store : [0x80007bd4] : sw t5, 720(ra) -- Store: [0x8000ca98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0x32ba6165fce3f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x32ba6165fce3f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007c14]:fsub.d t5, t3, s10, dyn
	-[0x80007c18]:csrrs a7, fcsr, zero
	-[0x80007c1c]:sw t5, 736(ra)
	-[0x80007c20]:sw t6, 744(ra)
Current Store : [0x80007c20] : sw t6, 744(ra) -- Store: [0x8000cab0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0x32ba6165fce3f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x32ba6165fce3f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007c14]:fsub.d t5, t3, s10, dyn
	-[0x80007c18]:csrrs a7, fcsr, zero
	-[0x80007c1c]:sw t5, 736(ra)
	-[0x80007c20]:sw t6, 744(ra)
	-[0x80007c24]:sw t5, 752(ra)
Current Store : [0x80007c24] : sw t5, 752(ra) -- Store: [0x8000cab8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0be093ea29884 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x0be093ea29884 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007c64]:fsub.d t5, t3, s10, dyn
	-[0x80007c68]:csrrs a7, fcsr, zero
	-[0x80007c6c]:sw t5, 768(ra)
	-[0x80007c70]:sw t6, 776(ra)
Current Store : [0x80007c70] : sw t6, 776(ra) -- Store: [0x8000cad0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0be093ea29884 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x0be093ea29884 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007c64]:fsub.d t5, t3, s10, dyn
	-[0x80007c68]:csrrs a7, fcsr, zero
	-[0x80007c6c]:sw t5, 768(ra)
	-[0x80007c70]:sw t6, 776(ra)
	-[0x80007c74]:sw t5, 784(ra)
Current Store : [0x80007c74] : sw t5, 784(ra) -- Store: [0x8000cad8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x05e5cee3b08d7 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x05e5cee3b08d7 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007cb4]:fsub.d t5, t3, s10, dyn
	-[0x80007cb8]:csrrs a7, fcsr, zero
	-[0x80007cbc]:sw t5, 800(ra)
	-[0x80007cc0]:sw t6, 808(ra)
Current Store : [0x80007cc0] : sw t6, 808(ra) -- Store: [0x8000caf0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x05e5cee3b08d7 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x05e5cee3b08d7 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007cb4]:fsub.d t5, t3, s10, dyn
	-[0x80007cb8]:csrrs a7, fcsr, zero
	-[0x80007cbc]:sw t5, 800(ra)
	-[0x80007cc0]:sw t6, 808(ra)
	-[0x80007cc4]:sw t5, 816(ra)
Current Store : [0x80007cc4] : sw t5, 816(ra) -- Store: [0x8000caf8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0x5932a24c0014f and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x5932a24c0014f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007d04]:fsub.d t5, t3, s10, dyn
	-[0x80007d08]:csrrs a7, fcsr, zero
	-[0x80007d0c]:sw t5, 832(ra)
	-[0x80007d10]:sw t6, 840(ra)
Current Store : [0x80007d10] : sw t6, 840(ra) -- Store: [0x8000cb10]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0x5932a24c0014f and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x5932a24c0014f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007d04]:fsub.d t5, t3, s10, dyn
	-[0x80007d08]:csrrs a7, fcsr, zero
	-[0x80007d0c]:sw t5, 832(ra)
	-[0x80007d10]:sw t6, 840(ra)
	-[0x80007d14]:sw t5, 848(ra)
Current Store : [0x80007d14] : sw t5, 848(ra) -- Store: [0x8000cb18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x6d479d3fc4771 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x6d479d3fc4771 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007d54]:fsub.d t5, t3, s10, dyn
	-[0x80007d58]:csrrs a7, fcsr, zero
	-[0x80007d5c]:sw t5, 864(ra)
	-[0x80007d60]:sw t6, 872(ra)
Current Store : [0x80007d60] : sw t6, 872(ra) -- Store: [0x8000cb30]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x6d479d3fc4771 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x6d479d3fc4771 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007d54]:fsub.d t5, t3, s10, dyn
	-[0x80007d58]:csrrs a7, fcsr, zero
	-[0x80007d5c]:sw t5, 864(ra)
	-[0x80007d60]:sw t6, 872(ra)
	-[0x80007d64]:sw t5, 880(ra)
Current Store : [0x80007d64] : sw t5, 880(ra) -- Store: [0x8000cb38]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x143c21ad8c8b5 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x143c21ad8c8b5 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007da4]:fsub.d t5, t3, s10, dyn
	-[0x80007da8]:csrrs a7, fcsr, zero
	-[0x80007dac]:sw t5, 896(ra)
	-[0x80007db0]:sw t6, 904(ra)
Current Store : [0x80007db0] : sw t6, 904(ra) -- Store: [0x8000cb50]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x143c21ad8c8b5 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x143c21ad8c8b5 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007da4]:fsub.d t5, t3, s10, dyn
	-[0x80007da8]:csrrs a7, fcsr, zero
	-[0x80007dac]:sw t5, 896(ra)
	-[0x80007db0]:sw t6, 904(ra)
	-[0x80007db4]:sw t5, 912(ra)
Current Store : [0x80007db4] : sw t5, 912(ra) -- Store: [0x8000cb58]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdb5e85647ec13 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xdb5e85647ec13 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007df4]:fsub.d t5, t3, s10, dyn
	-[0x80007df8]:csrrs a7, fcsr, zero
	-[0x80007dfc]:sw t5, 928(ra)
	-[0x80007e00]:sw t6, 936(ra)
Current Store : [0x80007e00] : sw t6, 936(ra) -- Store: [0x8000cb70]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdb5e85647ec13 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xdb5e85647ec13 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007df4]:fsub.d t5, t3, s10, dyn
	-[0x80007df8]:csrrs a7, fcsr, zero
	-[0x80007dfc]:sw t5, 928(ra)
	-[0x80007e00]:sw t6, 936(ra)
	-[0x80007e04]:sw t5, 944(ra)
Current Store : [0x80007e04] : sw t5, 944(ra) -- Store: [0x8000cb78]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0x60ffd67bcec83 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x60ffd67bcec83 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007e44]:fsub.d t5, t3, s10, dyn
	-[0x80007e48]:csrrs a7, fcsr, zero
	-[0x80007e4c]:sw t5, 960(ra)
	-[0x80007e50]:sw t6, 968(ra)
Current Store : [0x80007e50] : sw t6, 968(ra) -- Store: [0x8000cb90]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0x60ffd67bcec83 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x60ffd67bcec83 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007e44]:fsub.d t5, t3, s10, dyn
	-[0x80007e48]:csrrs a7, fcsr, zero
	-[0x80007e4c]:sw t5, 960(ra)
	-[0x80007e50]:sw t6, 968(ra)
	-[0x80007e54]:sw t5, 976(ra)
Current Store : [0x80007e54] : sw t5, 976(ra) -- Store: [0x8000cb98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbbc271a710d1b and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xbbc271a710d1b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007e94]:fsub.d t5, t3, s10, dyn
	-[0x80007e98]:csrrs a7, fcsr, zero
	-[0x80007e9c]:sw t5, 992(ra)
	-[0x80007ea0]:sw t6, 1000(ra)
Current Store : [0x80007ea0] : sw t6, 1000(ra) -- Store: [0x8000cbb0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbbc271a710d1b and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xbbc271a710d1b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007e94]:fsub.d t5, t3, s10, dyn
	-[0x80007e98]:csrrs a7, fcsr, zero
	-[0x80007e9c]:sw t5, 992(ra)
	-[0x80007ea0]:sw t6, 1000(ra)
	-[0x80007ea4]:sw t5, 1008(ra)
Current Store : [0x80007ea4] : sw t5, 1008(ra) -- Store: [0x8000cbb8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc74ef4423e96b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xc74ef4423e96b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007ee4]:fsub.d t5, t3, s10, dyn
	-[0x80007ee8]:csrrs a7, fcsr, zero
	-[0x80007eec]:sw t5, 1024(ra)
	-[0x80007ef0]:sw t6, 1032(ra)
Current Store : [0x80007ef0] : sw t6, 1032(ra) -- Store: [0x8000cbd0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc74ef4423e96b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xc74ef4423e96b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007ee4]:fsub.d t5, t3, s10, dyn
	-[0x80007ee8]:csrrs a7, fcsr, zero
	-[0x80007eec]:sw t5, 1024(ra)
	-[0x80007ef0]:sw t6, 1032(ra)
	-[0x80007ef4]:sw t5, 1040(ra)
Current Store : [0x80007ef4] : sw t5, 1040(ra) -- Store: [0x8000cbd8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x69d3500fa16c1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x69d3500fa16c1 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007f34]:fsub.d t5, t3, s10, dyn
	-[0x80007f38]:csrrs a7, fcsr, zero
	-[0x80007f3c]:sw t5, 1056(ra)
	-[0x80007f40]:sw t6, 1064(ra)
Current Store : [0x80007f40] : sw t6, 1064(ra) -- Store: [0x8000cbf0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x69d3500fa16c1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x69d3500fa16c1 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007f34]:fsub.d t5, t3, s10, dyn
	-[0x80007f38]:csrrs a7, fcsr, zero
	-[0x80007f3c]:sw t5, 1056(ra)
	-[0x80007f40]:sw t6, 1064(ra)
	-[0x80007f44]:sw t5, 1072(ra)
Current Store : [0x80007f44] : sw t5, 1072(ra) -- Store: [0x8000cbf8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xf446ded06de1f and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xf446ded06de1f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007f84]:fsub.d t5, t3, s10, dyn
	-[0x80007f88]:csrrs a7, fcsr, zero
	-[0x80007f8c]:sw t5, 1088(ra)
	-[0x80007f90]:sw t6, 1096(ra)
Current Store : [0x80007f90] : sw t6, 1096(ra) -- Store: [0x8000cc10]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xf446ded06de1f and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xf446ded06de1f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007f84]:fsub.d t5, t3, s10, dyn
	-[0x80007f88]:csrrs a7, fcsr, zero
	-[0x80007f8c]:sw t5, 1088(ra)
	-[0x80007f90]:sw t6, 1096(ra)
	-[0x80007f94]:sw t5, 1104(ra)
Current Store : [0x80007f94] : sw t5, 1104(ra) -- Store: [0x8000cc18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3eb20959c42c2 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x3eb20959c42c2 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007fd4]:fsub.d t5, t3, s10, dyn
	-[0x80007fd8]:csrrs a7, fcsr, zero
	-[0x80007fdc]:sw t5, 1120(ra)
	-[0x80007fe0]:sw t6, 1128(ra)
Current Store : [0x80007fe0] : sw t6, 1128(ra) -- Store: [0x8000cc30]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3eb20959c42c2 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x3eb20959c42c2 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007fd4]:fsub.d t5, t3, s10, dyn
	-[0x80007fd8]:csrrs a7, fcsr, zero
	-[0x80007fdc]:sw t5, 1120(ra)
	-[0x80007fe0]:sw t6, 1128(ra)
	-[0x80007fe4]:sw t5, 1136(ra)
Current Store : [0x80007fe4] : sw t5, 1136(ra) -- Store: [0x8000cc38]:0x00000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                          signature                           |                                                                                                                         coverpoints                                                                                                                         |                                                                                                                       code                                                                                                                        |
|---:|--------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x8000b718]<br>0x00000000<br> [0x8000b730]<br>0x00000060<br> |- mnemonic : fsub.d<br> - rs1 : x30<br> - rs2 : x30<br> - rd : x30<br> - rs1 == rs2 == rd<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0x132d8f91b7583 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x132d8f91b7583 and  fcsr == 0x60 and rm_val == 7   #nosat<br> |[0x8000014c]:fsub.d t5, t5, t5, dyn<br> [0x80000150]:csrrs tp, fcsr, zero<br> [0x80000154]:sw t5, 0(ra)<br> [0x80000158]:sw t6, 8(ra)<br> [0x8000015c]:sw t5, 16(ra)<br> [0x80000160]:sw tp, 24(ra)<br>                                            |
|   2|[0x8000b738]<br>0x00000000<br> [0x8000b750]<br>0x00000060<br> |- rs1 : x26<br> - rs2 : x24<br> - rd : x28<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x870d778409f12 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x870d778409f12 and  fcsr == 0x60 and rm_val == 7   #nosat<br>  |[0x8000019c]:fsub.d t3, s10, s8, dyn<br> [0x800001a0]:csrrs tp, fcsr, zero<br> [0x800001a4]:sw t3, 32(ra)<br> [0x800001a8]:sw t4, 40(ra)<br> [0x800001ac]:sw t3, 48(ra)<br> [0x800001b0]:sw tp, 56(ra)<br>                                         |
|   3|[0x8000b758]<br>0x00000000<br> [0x8000b770]<br>0x00000060<br> |- rs1 : x24<br> - rs2 : x28<br> - rd : x24<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0xfb5355e167379 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xfb5355e167379 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                         |[0x800001ec]:fsub.d s8, s8, t3, dyn<br> [0x800001f0]:csrrs tp, fcsr, zero<br> [0x800001f4]:sw s8, 64(ra)<br> [0x800001f8]:sw s9, 72(ra)<br> [0x800001fc]:sw s8, 80(ra)<br> [0x80000200]:sw tp, 88(ra)<br>                                          |
|   4|[0x8000b778]<br>0x00000000<br> [0x8000b790]<br>0x00000060<br> |- rs1 : x28<br> - rs2 : x26<br> - rd : x26<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4d9d98184b9d9 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x4d9d98184b9d9 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                         |[0x8000023c]:fsub.d s10, t3, s10, dyn<br> [0x80000240]:csrrs tp, fcsr, zero<br> [0x80000244]:sw s10, 96(ra)<br> [0x80000248]:sw s11, 104(ra)<br> [0x8000024c]:sw s10, 112(ra)<br> [0x80000250]:sw tp, 120(ra)<br>                                  |
|   5|[0x8000b798]<br>0x00000000<br> [0x8000b7b0]<br>0x00000060<br> |- rs1 : x20<br> - rs2 : x20<br> - rd : x22<br> - rs1 == rs2 != rd<br> - fs1 == 0 and fe1 == 0x7fb and fm1 == 0x8072e8f9c858f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x8072e8f9c858f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                         |[0x8000028c]:fsub.d s6, s4, s4, dyn<br> [0x80000290]:csrrs tp, fcsr, zero<br> [0x80000294]:sw s6, 128(ra)<br> [0x80000298]:sw s7, 136(ra)<br> [0x8000029c]:sw s6, 144(ra)<br> [0x800002a0]:sw tp, 152(ra)<br>                                      |
|   6|[0x8000b7b8]<br>0x00000000<br> [0x8000b7d0]<br>0x00000060<br> |- rs1 : x22<br> - rs2 : x18<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xabe96758f2a09 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xabe96758f2a09 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                |[0x800002dc]:fsub.d s4, s6, s2, dyn<br> [0x800002e0]:csrrs tp, fcsr, zero<br> [0x800002e4]:sw s4, 160(ra)<br> [0x800002e8]:sw s5, 168(ra)<br> [0x800002ec]:sw s4, 176(ra)<br> [0x800002f0]:sw tp, 184(ra)<br>                                      |
|   7|[0x8000b7d8]<br>0x00000000<br> [0x8000b7f0]<br>0x00000060<br> |- rs1 : x16<br> - rs2 : x22<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x86499331191c4 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x86499331191c4 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                |[0x8000032c]:fsub.d s2, a6, s6, dyn<br> [0x80000330]:csrrs tp, fcsr, zero<br> [0x80000334]:sw s2, 192(ra)<br> [0x80000338]:sw s3, 200(ra)<br> [0x8000033c]:sw s2, 208(ra)<br> [0x80000340]:sw tp, 216(ra)<br>                                      |
|   8|[0x8000b7f8]<br>0x00000000<br> [0x8000b810]<br>0x00000060<br> |- rs1 : x18<br> - rs2 : x14<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc81394a2171e9 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xc81394a2171e9 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                |[0x8000037c]:fsub.d a6, s2, a4, dyn<br> [0x80000380]:csrrs tp, fcsr, zero<br> [0x80000384]:sw a6, 224(ra)<br> [0x80000388]:sw a7, 232(ra)<br> [0x8000038c]:sw a6, 240(ra)<br> [0x80000390]:sw tp, 248(ra)<br>                                      |
|   9|[0x8000b818]<br>0x00000000<br> [0x8000b830]<br>0x00000060<br> |- rs1 : x12<br> - rs2 : x16<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0xd481499755d4b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xd481499755d4b and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                |[0x800003cc]:fsub.d a4, a2, a6, dyn<br> [0x800003d0]:csrrs tp, fcsr, zero<br> [0x800003d4]:sw a4, 256(ra)<br> [0x800003d8]:sw a5, 264(ra)<br> [0x800003dc]:sw a4, 272(ra)<br> [0x800003e0]:sw tp, 280(ra)<br>                                      |
|  10|[0x8000b838]<br>0x00000000<br> [0x8000b850]<br>0x00000060<br> |- rs1 : x14<br> - rs2 : x10<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd87e65450c45 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xcd87e65450c45 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                |[0x80000424]:fsub.d a2, a4, a0, dyn<br> [0x80000428]:csrrs a7, fcsr, zero<br> [0x8000042c]:sw a2, 288(ra)<br> [0x80000430]:sw a3, 296(ra)<br> [0x80000434]:sw a2, 304(ra)<br> [0x80000438]:sw a7, 312(ra)<br>                                      |
|  11|[0x8000b858]<br>0x00000000<br> [0x8000b870]<br>0x00000060<br> |- rs1 : x8<br> - rs2 : x12<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xa0e7ad32453df and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xa0e7ad32453df and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                 |[0x80000474]:fsub.d a0, fp, a2, dyn<br> [0x80000478]:csrrs a7, fcsr, zero<br> [0x8000047c]:sw a0, 320(ra)<br> [0x80000480]:sw a1, 328(ra)<br> [0x80000484]:sw a0, 336(ra)<br> [0x80000488]:sw a7, 344(ra)<br>                                      |
|  12|[0x8000b7c8]<br>0x00000000<br> [0x8000b7e0]<br>0x00000060<br> |- rs1 : x10<br> - rs2 : x6<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe0d828b86622a and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xe0d828b86622a and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                  |[0x800004cc]:fsub.d fp, a0, t1, dyn<br> [0x800004d0]:csrrs a7, fcsr, zero<br> [0x800004d4]:sw fp, 0(ra)<br> [0x800004d8]:sw s1, 8(ra)<br> [0x800004dc]:sw fp, 16(ra)<br> [0x800004e0]:sw a7, 24(ra)<br>                                            |
|  13|[0x8000b7e8]<br>0x00000000<br> [0x8000b800]<br>0x00000060<br> |- rs1 : x4<br> - rs2 : x8<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb9876f8130c3 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xbb9876f8130c3 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                   |[0x8000051c]:fsub.d t1, tp, fp, dyn<br> [0x80000520]:csrrs a7, fcsr, zero<br> [0x80000524]:sw t1, 32(ra)<br> [0x80000528]:sw t2, 40(ra)<br> [0x8000052c]:sw t1, 48(ra)<br> [0x80000530]:sw a7, 56(ra)<br>                                          |
|  14|[0x8000b808]<br>0x00000000<br> [0x8000b820]<br>0x00000060<br> |- rs1 : x6<br> - rs2 : x2<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xdbcde43895c3f and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xdbcde43895c3f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                   |[0x8000056c]:fsub.d tp, t1, sp, dyn<br> [0x80000570]:csrrs a7, fcsr, zero<br> [0x80000574]:sw tp, 64(ra)<br> [0x80000578]:sw t0, 72(ra)<br> [0x8000057c]:sw tp, 80(ra)<br> [0x80000580]:sw a7, 88(ra)<br>                                          |
|  15|[0x8000b828]<br>0x00000000<br> [0x8000b840]<br>0x00000060<br> |- rs1 : x2<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc0659af8369fd and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xc0659af8369fd and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                |[0x800005bc]:fsub.d t5, sp, t3, dyn<br> [0x800005c0]:csrrs a7, fcsr, zero<br> [0x800005c4]:sw t5, 96(ra)<br> [0x800005c8]:sw t6, 104(ra)<br> [0x800005cc]:sw t5, 112(ra)<br> [0x800005d0]:sw a7, 120(ra)<br>                                       |
|  16|[0x8000b848]<br>0x00000000<br> [0x8000b860]<br>0x00000060<br> |- rs2 : x4<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdd5b61587fd27 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xdd5b61587fd27 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                |[0x8000060c]:fsub.d t5, t3, tp, dyn<br> [0x80000610]:csrrs a7, fcsr, zero<br> [0x80000614]:sw t5, 128(ra)<br> [0x80000618]:sw t6, 136(ra)<br> [0x8000061c]:sw t5, 144(ra)<br> [0x80000620]:sw a7, 152(ra)<br>                                      |
|  17|[0x8000b868]<br>0x00000000<br> [0x8000b880]<br>0x00000060<br> |- rd : x2<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc01045c2cd787 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xc01045c2cd787 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                 |[0x8000065c]:fsub.d sp, t5, t3, dyn<br> [0x80000660]:csrrs a7, fcsr, zero<br> [0x80000664]:sw sp, 160(ra)<br> [0x80000668]:sw gp, 168(ra)<br> [0x8000066c]:sw sp, 176(ra)<br> [0x80000670]:sw a7, 184(ra)<br>                                      |
|  18|[0x8000b888]<br>0x00000000<br> [0x8000b8a0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x28bc82f697c4d and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x28bc82f697c4d and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800006ac]:fsub.d t5, t3, s10, dyn<br> [0x800006b0]:csrrs a7, fcsr, zero<br> [0x800006b4]:sw t5, 192(ra)<br> [0x800006b8]:sw t6, 200(ra)<br> [0x800006bc]:sw t5, 208(ra)<br> [0x800006c0]:sw a7, 216(ra)<br>                                     |
|  19|[0x8000b8a8]<br>0x00000000<br> [0x8000b8c0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xacd7053aa42a2 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xacd7053aa42a2 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800006fc]:fsub.d t5, t3, s10, dyn<br> [0x80000700]:csrrs a7, fcsr, zero<br> [0x80000704]:sw t5, 224(ra)<br> [0x80000708]:sw t6, 232(ra)<br> [0x8000070c]:sw t5, 240(ra)<br> [0x80000710]:sw a7, 248(ra)<br>                                     |
|  20|[0x8000b8c8]<br>0x00000000<br> [0x8000b8e0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x48dace8666677 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x48dace8666677 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x8000074c]:fsub.d t5, t3, s10, dyn<br> [0x80000750]:csrrs a7, fcsr, zero<br> [0x80000754]:sw t5, 256(ra)<br> [0x80000758]:sw t6, 264(ra)<br> [0x8000075c]:sw t5, 272(ra)<br> [0x80000760]:sw a7, 280(ra)<br>                                     |
|  21|[0x8000b8e8]<br>0x00000000<br> [0x8000b900]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfc2ea66e5019e and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfc2ea66e5019e and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x8000079c]:fsub.d t5, t3, s10, dyn<br> [0x800007a0]:csrrs a7, fcsr, zero<br> [0x800007a4]:sw t5, 288(ra)<br> [0x800007a8]:sw t6, 296(ra)<br> [0x800007ac]:sw t5, 304(ra)<br> [0x800007b0]:sw a7, 312(ra)<br>                                     |
|  22|[0x8000b908]<br>0x00000000<br> [0x8000b920]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xef2a4f7c7db7f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xef2a4f7c7db7f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800007ec]:fsub.d t5, t3, s10, dyn<br> [0x800007f0]:csrrs a7, fcsr, zero<br> [0x800007f4]:sw t5, 320(ra)<br> [0x800007f8]:sw t6, 328(ra)<br> [0x800007fc]:sw t5, 336(ra)<br> [0x80000800]:sw a7, 344(ra)<br>                                     |
|  23|[0x8000b928]<br>0x00000000<br> [0x8000b940]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7166677e49c3c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x7166677e49c3c and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x8000083c]:fsub.d t5, t3, s10, dyn<br> [0x80000840]:csrrs a7, fcsr, zero<br> [0x80000844]:sw t5, 352(ra)<br> [0x80000848]:sw t6, 360(ra)<br> [0x8000084c]:sw t5, 368(ra)<br> [0x80000850]:sw a7, 376(ra)<br>                                     |
|  24|[0x8000b948]<br>0x00000000<br> [0x8000b960]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xdf7523fde6c5d and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xdf7523fde6c5d and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x8000088c]:fsub.d t5, t3, s10, dyn<br> [0x80000890]:csrrs a7, fcsr, zero<br> [0x80000894]:sw t5, 384(ra)<br> [0x80000898]:sw t6, 392(ra)<br> [0x8000089c]:sw t5, 400(ra)<br> [0x800008a0]:sw a7, 408(ra)<br>                                     |
|  25|[0x8000b968]<br>0x00000000<br> [0x8000b980]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa8fa703a4078c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xa8fa703a4078c and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800008dc]:fsub.d t5, t3, s10, dyn<br> [0x800008e0]:csrrs a7, fcsr, zero<br> [0x800008e4]:sw t5, 416(ra)<br> [0x800008e8]:sw t6, 424(ra)<br> [0x800008ec]:sw t5, 432(ra)<br> [0x800008f0]:sw a7, 440(ra)<br>                                     |
|  26|[0x8000b988]<br>0x00000000<br> [0x8000b9a0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x36a63c245f557 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x36a63c245f557 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x8000092c]:fsub.d t5, t3, s10, dyn<br> [0x80000930]:csrrs a7, fcsr, zero<br> [0x80000934]:sw t5, 448(ra)<br> [0x80000938]:sw t6, 456(ra)<br> [0x8000093c]:sw t5, 464(ra)<br> [0x80000940]:sw a7, 472(ra)<br>                                     |
|  27|[0x8000b9a8]<br>0x00000000<br> [0x8000b9c0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc3d4499ff58c3 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc3d4499ff58c3 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x8000097c]:fsub.d t5, t3, s10, dyn<br> [0x80000980]:csrrs a7, fcsr, zero<br> [0x80000984]:sw t5, 480(ra)<br> [0x80000988]:sw t6, 488(ra)<br> [0x8000098c]:sw t5, 496(ra)<br> [0x80000990]:sw a7, 504(ra)<br>                                     |
|  28|[0x8000b9c8]<br>0x00000000<br> [0x8000b9e0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x02b48f992cb49 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x02b48f992cb49 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800009cc]:fsub.d t5, t3, s10, dyn<br> [0x800009d0]:csrrs a7, fcsr, zero<br> [0x800009d4]:sw t5, 512(ra)<br> [0x800009d8]:sw t6, 520(ra)<br> [0x800009dc]:sw t5, 528(ra)<br> [0x800009e0]:sw a7, 536(ra)<br>                                     |
|  29|[0x8000b9e8]<br>0x00000000<br> [0x8000ba00]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x1ad5e9ebc09df and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x1ad5e9ebc09df and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80000a1c]:fsub.d t5, t3, s10, dyn<br> [0x80000a20]:csrrs a7, fcsr, zero<br> [0x80000a24]:sw t5, 544(ra)<br> [0x80000a28]:sw t6, 552(ra)<br> [0x80000a2c]:sw t5, 560(ra)<br> [0x80000a30]:sw a7, 568(ra)<br>                                     |
|  30|[0x8000ba08]<br>0x00000000<br> [0x8000ba20]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x98455e99dfdb1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x98455e99dfdb1 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80000a6c]:fsub.d t5, t3, s10, dyn<br> [0x80000a70]:csrrs a7, fcsr, zero<br> [0x80000a74]:sw t5, 576(ra)<br> [0x80000a78]:sw t6, 584(ra)<br> [0x80000a7c]:sw t5, 592(ra)<br> [0x80000a80]:sw a7, 600(ra)<br>                                     |
|  31|[0x8000ba28]<br>0x00000000<br> [0x8000ba40]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x6251b45dfbd3b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x6251b45dfbd3b and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80000abc]:fsub.d t5, t3, s10, dyn<br> [0x80000ac0]:csrrs a7, fcsr, zero<br> [0x80000ac4]:sw t5, 608(ra)<br> [0x80000ac8]:sw t6, 616(ra)<br> [0x80000acc]:sw t5, 624(ra)<br> [0x80000ad0]:sw a7, 632(ra)<br>                                     |
|  32|[0x8000ba48]<br>0x00000000<br> [0x8000ba60]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x67f4f571a752e and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x67f4f571a752e and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80000b0c]:fsub.d t5, t3, s10, dyn<br> [0x80000b10]:csrrs a7, fcsr, zero<br> [0x80000b14]:sw t5, 640(ra)<br> [0x80000b18]:sw t6, 648(ra)<br> [0x80000b1c]:sw t5, 656(ra)<br> [0x80000b20]:sw a7, 664(ra)<br>                                     |
|  33|[0x8000ba68]<br>0x00000000<br> [0x8000ba80]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x7fb2260b115e9 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x7fb2260b115e9 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80000b5c]:fsub.d t5, t3, s10, dyn<br> [0x80000b60]:csrrs a7, fcsr, zero<br> [0x80000b64]:sw t5, 672(ra)<br> [0x80000b68]:sw t6, 680(ra)<br> [0x80000b6c]:sw t5, 688(ra)<br> [0x80000b70]:sw a7, 696(ra)<br>                                     |
|  34|[0x8000ba88]<br>0x00000000<br> [0x8000baa0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x04507a06e8587 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x04507a06e8587 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80000bac]:fsub.d t5, t3, s10, dyn<br> [0x80000bb0]:csrrs a7, fcsr, zero<br> [0x80000bb4]:sw t5, 704(ra)<br> [0x80000bb8]:sw t6, 712(ra)<br> [0x80000bbc]:sw t5, 720(ra)<br> [0x80000bc0]:sw a7, 728(ra)<br>                                     |
|  35|[0x8000baa8]<br>0x00000000<br> [0x8000bac0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0ab870b5c1c40 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x0ab870b5c1c40 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80000bfc]:fsub.d t5, t3, s10, dyn<br> [0x80000c00]:csrrs a7, fcsr, zero<br> [0x80000c04]:sw t5, 736(ra)<br> [0x80000c08]:sw t6, 744(ra)<br> [0x80000c0c]:sw t5, 752(ra)<br> [0x80000c10]:sw a7, 760(ra)<br>                                     |
|  36|[0x8000bac8]<br>0x00000000<br> [0x8000bae0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf57237ddcb451 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xf57237ddcb451 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80000c4c]:fsub.d t5, t3, s10, dyn<br> [0x80000c50]:csrrs a7, fcsr, zero<br> [0x80000c54]:sw t5, 768(ra)<br> [0x80000c58]:sw t6, 776(ra)<br> [0x80000c5c]:sw t5, 784(ra)<br> [0x80000c60]:sw a7, 792(ra)<br>                                     |
|  37|[0x8000bae8]<br>0x00000000<br> [0x8000bb00]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x643f753bef22f and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x643f753bef22f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80000c9c]:fsub.d t5, t3, s10, dyn<br> [0x80000ca0]:csrrs a7, fcsr, zero<br> [0x80000ca4]:sw t5, 800(ra)<br> [0x80000ca8]:sw t6, 808(ra)<br> [0x80000cac]:sw t5, 816(ra)<br> [0x80000cb0]:sw a7, 824(ra)<br>                                     |
|  38|[0x8000bb08]<br>0x00000000<br> [0x8000bb20]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf7646167590ef and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf7646167590ef and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80000cec]:fsub.d t5, t3, s10, dyn<br> [0x80000cf0]:csrrs a7, fcsr, zero<br> [0x80000cf4]:sw t5, 832(ra)<br> [0x80000cf8]:sw t6, 840(ra)<br> [0x80000cfc]:sw t5, 848(ra)<br> [0x80000d00]:sw a7, 856(ra)<br>                                     |
|  39|[0x8000bb28]<br>0x00000000<br> [0x8000bb40]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x930bcbd2d6035 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x930bcbd2d6035 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80000d3c]:fsub.d t5, t3, s10, dyn<br> [0x80000d40]:csrrs a7, fcsr, zero<br> [0x80000d44]:sw t5, 864(ra)<br> [0x80000d48]:sw t6, 872(ra)<br> [0x80000d4c]:sw t5, 880(ra)<br> [0x80000d50]:sw a7, 888(ra)<br>                                     |
|  40|[0x8000bb48]<br>0x00000000<br> [0x8000bb60]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0125698e86242 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x0125698e86242 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80000d8c]:fsub.d t5, t3, s10, dyn<br> [0x80000d90]:csrrs a7, fcsr, zero<br> [0x80000d94]:sw t5, 896(ra)<br> [0x80000d98]:sw t6, 904(ra)<br> [0x80000d9c]:sw t5, 912(ra)<br> [0x80000da0]:sw a7, 920(ra)<br>                                     |
|  41|[0x8000bb68]<br>0x00000000<br> [0x8000bb80]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8a82024cc4e03 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x8a82024cc4e03 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80000ddc]:fsub.d t5, t3, s10, dyn<br> [0x80000de0]:csrrs a7, fcsr, zero<br> [0x80000de4]:sw t5, 928(ra)<br> [0x80000de8]:sw t6, 936(ra)<br> [0x80000dec]:sw t5, 944(ra)<br> [0x80000df0]:sw a7, 952(ra)<br>                                     |
|  42|[0x8000bb88]<br>0x00000000<br> [0x8000bba0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb877e6e317fa2 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xb877e6e317fa2 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80000e2c]:fsub.d t5, t3, s10, dyn<br> [0x80000e30]:csrrs a7, fcsr, zero<br> [0x80000e34]:sw t5, 960(ra)<br> [0x80000e38]:sw t6, 968(ra)<br> [0x80000e3c]:sw t5, 976(ra)<br> [0x80000e40]:sw a7, 984(ra)<br>                                     |
|  43|[0x8000bba8]<br>0x00000000<br> [0x8000bbc0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x070d1456013e3 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x070d1456013e3 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80000e7c]:fsub.d t5, t3, s10, dyn<br> [0x80000e80]:csrrs a7, fcsr, zero<br> [0x80000e84]:sw t5, 992(ra)<br> [0x80000e88]:sw t6, 1000(ra)<br> [0x80000e8c]:sw t5, 1008(ra)<br> [0x80000e90]:sw a7, 1016(ra)<br>                                  |
|  44|[0x8000bbc8]<br>0x00000000<br> [0x8000bbe0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x27d4b8969c0b2 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x27d4b8969c0b2 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80000ecc]:fsub.d t5, t3, s10, dyn<br> [0x80000ed0]:csrrs a7, fcsr, zero<br> [0x80000ed4]:sw t5, 1024(ra)<br> [0x80000ed8]:sw t6, 1032(ra)<br> [0x80000edc]:sw t5, 1040(ra)<br> [0x80000ee0]:sw a7, 1048(ra)<br>                                 |
|  45|[0x8000bbe8]<br>0x00000000<br> [0x8000bc00]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x13bdffd461269 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x13bdffd461269 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80000f1c]:fsub.d t5, t3, s10, dyn<br> [0x80000f20]:csrrs a7, fcsr, zero<br> [0x80000f24]:sw t5, 1056(ra)<br> [0x80000f28]:sw t6, 1064(ra)<br> [0x80000f2c]:sw t5, 1072(ra)<br> [0x80000f30]:sw a7, 1080(ra)<br>                                 |
|  46|[0x8000bc08]<br>0x00000000<br> [0x8000bc20]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xea0b252eae7e0 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xea0b252eae7e0 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80000f6c]:fsub.d t5, t3, s10, dyn<br> [0x80000f70]:csrrs a7, fcsr, zero<br> [0x80000f74]:sw t5, 1088(ra)<br> [0x80000f78]:sw t6, 1096(ra)<br> [0x80000f7c]:sw t5, 1104(ra)<br> [0x80000f80]:sw a7, 1112(ra)<br>                                 |
|  47|[0x8000bc28]<br>0x00000000<br> [0x8000bc40]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x913b4236d8411 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x913b4236d8411 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80000fbc]:fsub.d t5, t3, s10, dyn<br> [0x80000fc0]:csrrs a7, fcsr, zero<br> [0x80000fc4]:sw t5, 1120(ra)<br> [0x80000fc8]:sw t6, 1128(ra)<br> [0x80000fcc]:sw t5, 1136(ra)<br> [0x80000fd0]:sw a7, 1144(ra)<br>                                 |
|  48|[0x8000bc48]<br>0x00000000<br> [0x8000bc60]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc5b9547c0fb71 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc5b9547c0fb71 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x8000100c]:fsub.d t5, t3, s10, dyn<br> [0x80001010]:csrrs a7, fcsr, zero<br> [0x80001014]:sw t5, 1152(ra)<br> [0x80001018]:sw t6, 1160(ra)<br> [0x8000101c]:sw t5, 1168(ra)<br> [0x80001020]:sw a7, 1176(ra)<br>                                 |
|  49|[0x8000bc68]<br>0x00000000<br> [0x8000bc80]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9e4795c8459f5 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x9e4795c8459f5 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x8000105c]:fsub.d t5, t3, s10, dyn<br> [0x80001060]:csrrs a7, fcsr, zero<br> [0x80001064]:sw t5, 1184(ra)<br> [0x80001068]:sw t6, 1192(ra)<br> [0x8000106c]:sw t5, 1200(ra)<br> [0x80001070]:sw a7, 1208(ra)<br>                                 |
|  50|[0x8000bc88]<br>0x00000000<br> [0x8000bca0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x1f6a4c4d26ab9 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x1f6a4c4d26ab9 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800010ac]:fsub.d t5, t3, s10, dyn<br> [0x800010b0]:csrrs a7, fcsr, zero<br> [0x800010b4]:sw t5, 1216(ra)<br> [0x800010b8]:sw t6, 1224(ra)<br> [0x800010bc]:sw t5, 1232(ra)<br> [0x800010c0]:sw a7, 1240(ra)<br>                                 |
|  51|[0x8000bca8]<br>0x00000000<br> [0x8000bcc0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fa and fm1 == 0xcbc315eca5f3f and fs2 == 0 and fe2 == 0x7fa and fm2 == 0xcbc315eca5f3f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800010fc]:fsub.d t5, t3, s10, dyn<br> [0x80001100]:csrrs a7, fcsr, zero<br> [0x80001104]:sw t5, 1248(ra)<br> [0x80001108]:sw t6, 1256(ra)<br> [0x8000110c]:sw t5, 1264(ra)<br> [0x80001110]:sw a7, 1272(ra)<br>                                 |
|  52|[0x8000bcc8]<br>0x00000000<br> [0x8000bce0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x23d6f3e37b4f1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x23d6f3e37b4f1 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x8000114c]:fsub.d t5, t3, s10, dyn<br> [0x80001150]:csrrs a7, fcsr, zero<br> [0x80001154]:sw t5, 1280(ra)<br> [0x80001158]:sw t6, 1288(ra)<br> [0x8000115c]:sw t5, 1296(ra)<br> [0x80001160]:sw a7, 1304(ra)<br>                                 |
|  53|[0x8000bce8]<br>0x00000000<br> [0x8000bd00]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x026ab89a75256 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x026ab89a75256 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x8000119c]:fsub.d t5, t3, s10, dyn<br> [0x800011a0]:csrrs a7, fcsr, zero<br> [0x800011a4]:sw t5, 1312(ra)<br> [0x800011a8]:sw t6, 1320(ra)<br> [0x800011ac]:sw t5, 1328(ra)<br> [0x800011b0]:sw a7, 1336(ra)<br>                                 |
|  54|[0x8000bd08]<br>0x00000000<br> [0x8000bd20]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x6d5a59350bdcb and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x6d5a59350bdcb and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800011ec]:fsub.d t5, t3, s10, dyn<br> [0x800011f0]:csrrs a7, fcsr, zero<br> [0x800011f4]:sw t5, 1344(ra)<br> [0x800011f8]:sw t6, 1352(ra)<br> [0x800011fc]:sw t5, 1360(ra)<br> [0x80001200]:sw a7, 1368(ra)<br>                                 |
|  55|[0x8000bd28]<br>0x00000000<br> [0x8000bd40]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x13b37e2291279 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x13b37e2291279 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x8000123c]:fsub.d t5, t3, s10, dyn<br> [0x80001240]:csrrs a7, fcsr, zero<br> [0x80001244]:sw t5, 1376(ra)<br> [0x80001248]:sw t6, 1384(ra)<br> [0x8000124c]:sw t5, 1392(ra)<br> [0x80001250]:sw a7, 1400(ra)<br>                                 |
|  56|[0x8000bd48]<br>0x00000000<br> [0x8000bd60]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x39935e95315b1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x39935e95315b1 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x8000128c]:fsub.d t5, t3, s10, dyn<br> [0x80001290]:csrrs a7, fcsr, zero<br> [0x80001294]:sw t5, 1408(ra)<br> [0x80001298]:sw t6, 1416(ra)<br> [0x8000129c]:sw t5, 1424(ra)<br> [0x800012a0]:sw a7, 1432(ra)<br>                                 |
|  57|[0x8000bd68]<br>0x00000000<br> [0x8000bd80]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xca57966fc21ff and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xca57966fc21ff and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800012dc]:fsub.d t5, t3, s10, dyn<br> [0x800012e0]:csrrs a7, fcsr, zero<br> [0x800012e4]:sw t5, 1440(ra)<br> [0x800012e8]:sw t6, 1448(ra)<br> [0x800012ec]:sw t5, 1456(ra)<br> [0x800012f0]:sw a7, 1464(ra)<br>                                 |
|  58|[0x8000bd88]<br>0x00000000<br> [0x8000bda0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x6aedbc8cfe5cb and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x6aedbc8cfe5cb and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x8000132c]:fsub.d t5, t3, s10, dyn<br> [0x80001330]:csrrs a7, fcsr, zero<br> [0x80001334]:sw t5, 1472(ra)<br> [0x80001338]:sw t6, 1480(ra)<br> [0x8000133c]:sw t5, 1488(ra)<br> [0x80001340]:sw a7, 1496(ra)<br>                                 |
|  59|[0x8000bda8]<br>0x00000000<br> [0x8000bdc0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb8d62745dccc1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xb8d62745dccc1 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x8000137c]:fsub.d t5, t3, s10, dyn<br> [0x80001380]:csrrs a7, fcsr, zero<br> [0x80001384]:sw t5, 1504(ra)<br> [0x80001388]:sw t6, 1512(ra)<br> [0x8000138c]:sw t5, 1520(ra)<br> [0x80001390]:sw a7, 1528(ra)<br>                                 |
|  60|[0x8000bdc8]<br>0x00000000<br> [0x8000bde0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x981d2bf67b45e and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x981d2bf67b45e and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800013cc]:fsub.d t5, t3, s10, dyn<br> [0x800013d0]:csrrs a7, fcsr, zero<br> [0x800013d4]:sw t5, 1536(ra)<br> [0x800013d8]:sw t6, 1544(ra)<br> [0x800013dc]:sw t5, 1552(ra)<br> [0x800013e0]:sw a7, 1560(ra)<br>                                 |
|  61|[0x8000bde8]<br>0x00000000<br> [0x8000be00]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x056bcd04279ed and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x056bcd04279ed and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x8000141c]:fsub.d t5, t3, s10, dyn<br> [0x80001420]:csrrs a7, fcsr, zero<br> [0x80001424]:sw t5, 1568(ra)<br> [0x80001428]:sw t6, 1576(ra)<br> [0x8000142c]:sw t5, 1584(ra)<br> [0x80001430]:sw a7, 1592(ra)<br>                                 |
|  62|[0x8000be08]<br>0x00000000<br> [0x8000be20]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x58a25604824f3 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x58a25604824f3 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x8000146c]:fsub.d t5, t3, s10, dyn<br> [0x80001470]:csrrs a7, fcsr, zero<br> [0x80001474]:sw t5, 1600(ra)<br> [0x80001478]:sw t6, 1608(ra)<br> [0x8000147c]:sw t5, 1616(ra)<br> [0x80001480]:sw a7, 1624(ra)<br>                                 |
|  63|[0x8000be28]<br>0x00000000<br> [0x8000be40]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0x1175939fbdd3f and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0x1175939fbdd3f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800014bc]:fsub.d t5, t3, s10, dyn<br> [0x800014c0]:csrrs a7, fcsr, zero<br> [0x800014c4]:sw t5, 1632(ra)<br> [0x800014c8]:sw t6, 1640(ra)<br> [0x800014cc]:sw t5, 1648(ra)<br> [0x800014d0]:sw a7, 1656(ra)<br>                                 |
|  64|[0x8000be48]<br>0x00000000<br> [0x8000be60]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x82dc4511ff204 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x82dc4511ff204 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x8000150c]:fsub.d t5, t3, s10, dyn<br> [0x80001510]:csrrs a7, fcsr, zero<br> [0x80001514]:sw t5, 1664(ra)<br> [0x80001518]:sw t6, 1672(ra)<br> [0x8000151c]:sw t5, 1680(ra)<br> [0x80001520]:sw a7, 1688(ra)<br>                                 |
|  65|[0x8000be68]<br>0x00000000<br> [0x8000be80]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xc07725983617f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xc07725983617f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x8000155c]:fsub.d t5, t3, s10, dyn<br> [0x80001560]:csrrs a7, fcsr, zero<br> [0x80001564]:sw t5, 1696(ra)<br> [0x80001568]:sw t6, 1704(ra)<br> [0x8000156c]:sw t5, 1712(ra)<br> [0x80001570]:sw a7, 1720(ra)<br>                                 |
|  66|[0x8000be88]<br>0x00000000<br> [0x8000bea0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x60b0632528095 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x60b0632528095 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800015ac]:fsub.d t5, t3, s10, dyn<br> [0x800015b0]:csrrs a7, fcsr, zero<br> [0x800015b4]:sw t5, 1728(ra)<br> [0x800015b8]:sw t6, 1736(ra)<br> [0x800015bc]:sw t5, 1744(ra)<br> [0x800015c0]:sw a7, 1752(ra)<br>                                 |
|  67|[0x8000bea8]<br>0x00000000<br> [0x8000bec0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x46e9bf4155d7b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x46e9bf4155d7b and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800015fc]:fsub.d t5, t3, s10, dyn<br> [0x80001600]:csrrs a7, fcsr, zero<br> [0x80001604]:sw t5, 1760(ra)<br> [0x80001608]:sw t6, 1768(ra)<br> [0x8000160c]:sw t5, 1776(ra)<br> [0x80001610]:sw a7, 1784(ra)<br>                                 |
|  68|[0x8000bec8]<br>0x00000000<br> [0x8000bee0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x585c60a81aa3f and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x585c60a81aa3f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x8000164c]:fsub.d t5, t3, s10, dyn<br> [0x80001650]:csrrs a7, fcsr, zero<br> [0x80001654]:sw t5, 1792(ra)<br> [0x80001658]:sw t6, 1800(ra)<br> [0x8000165c]:sw t5, 1808(ra)<br> [0x80001660]:sw a7, 1816(ra)<br>                                 |
|  69|[0x8000bee8]<br>0x00000000<br> [0x8000bf00]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x6c5583d2d8f82 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x6c5583d2d8f82 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x8000169c]:fsub.d t5, t3, s10, dyn<br> [0x800016a0]:csrrs a7, fcsr, zero<br> [0x800016a4]:sw t5, 1824(ra)<br> [0x800016a8]:sw t6, 1832(ra)<br> [0x800016ac]:sw t5, 1840(ra)<br> [0x800016b0]:sw a7, 1848(ra)<br>                                 |
|  70|[0x8000bf08]<br>0x00000000<br> [0x8000bf20]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x49bad4bf8d1a9 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x49bad4bf8d1a9 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800016ec]:fsub.d t5, t3, s10, dyn<br> [0x800016f0]:csrrs a7, fcsr, zero<br> [0x800016f4]:sw t5, 1856(ra)<br> [0x800016f8]:sw t6, 1864(ra)<br> [0x800016fc]:sw t5, 1872(ra)<br> [0x80001700]:sw a7, 1880(ra)<br>                                 |
|  71|[0x8000bf28]<br>0x00000000<br> [0x8000bf40]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0x833a9a7efc6ff and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0x833a9a7efc6ff and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x8000173c]:fsub.d t5, t3, s10, dyn<br> [0x80001740]:csrrs a7, fcsr, zero<br> [0x80001744]:sw t5, 1888(ra)<br> [0x80001748]:sw t6, 1896(ra)<br> [0x8000174c]:sw t5, 1904(ra)<br> [0x80001750]:sw a7, 1912(ra)<br>                                 |
|  72|[0x8000bf48]<br>0x00000000<br> [0x8000bf60]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xaf054e65e9fad and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xaf054e65e9fad and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x8000178c]:fsub.d t5, t3, s10, dyn<br> [0x80001790]:csrrs a7, fcsr, zero<br> [0x80001794]:sw t5, 1920(ra)<br> [0x80001798]:sw t6, 1928(ra)<br> [0x8000179c]:sw t5, 1936(ra)<br> [0x800017a0]:sw a7, 1944(ra)<br>                                 |
|  73|[0x8000bf68]<br>0x00000000<br> [0x8000bf80]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xbd7ce681c543f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xbd7ce681c543f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800017dc]:fsub.d t5, t3, s10, dyn<br> [0x800017e0]:csrrs a7, fcsr, zero<br> [0x800017e4]:sw t5, 1952(ra)<br> [0x800017e8]:sw t6, 1960(ra)<br> [0x800017ec]:sw t5, 1968(ra)<br> [0x800017f0]:sw a7, 1976(ra)<br>                                 |
|  74|[0x8000bf88]<br>0x00000000<br> [0x8000bfa0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x052debfe82e13 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x052debfe82e13 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x8000182c]:fsub.d t5, t3, s10, dyn<br> [0x80001830]:csrrs a7, fcsr, zero<br> [0x80001834]:sw t5, 1984(ra)<br> [0x80001838]:sw t6, 1992(ra)<br> [0x8000183c]:sw t5, 2000(ra)<br> [0x80001840]:sw a7, 2008(ra)<br>                                 |
|  75|[0x8000bfa8]<br>0x00000000<br> [0x8000bfc0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3617941ba03e8 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x3617941ba03e8 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x8000187c]:fsub.d t5, t3, s10, dyn<br> [0x80001880]:csrrs a7, fcsr, zero<br> [0x80001884]:sw t5, 2016(ra)<br> [0x80001888]:sw t6, 2024(ra)<br> [0x8000188c]:sw t5, 2032(ra)<br> [0x80001890]:sw a7, 2040(ra)<br>                                 |
|  76|[0x8000bfc8]<br>0x00000000<br> [0x8000bfe0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0x551579cd90e3f and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0x551579cd90e3f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800018cc]:fsub.d t5, t3, s10, dyn<br> [0x800018d0]:csrrs a7, fcsr, zero<br> [0x800018d4]:addi ra, ra, 2040<br> [0x800018d8]:sw t5, 8(ra)<br> [0x800018dc]:sw t6, 16(ra)<br> [0x800018e0]:sw t5, 24(ra)<br> [0x800018e4]:sw a7, 32(ra)<br>       |
|  77|[0x8000bfe8]<br>0x00000000<br> [0x8000c000]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x2bf422090b207 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x2bf422090b207 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80001920]:fsub.d t5, t3, s10, dyn<br> [0x80001924]:csrrs a7, fcsr, zero<br> [0x80001928]:sw t5, 40(ra)<br> [0x8000192c]:sw t6, 48(ra)<br> [0x80001930]:sw t5, 56(ra)<br> [0x80001934]:sw a7, 64(ra)<br>                                         |
|  78|[0x8000c008]<br>0x00000000<br> [0x8000c020]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x482567721754b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x482567721754b and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80001970]:fsub.d t5, t3, s10, dyn<br> [0x80001974]:csrrs a7, fcsr, zero<br> [0x80001978]:sw t5, 72(ra)<br> [0x8000197c]:sw t6, 80(ra)<br> [0x80001980]:sw t5, 88(ra)<br> [0x80001984]:sw a7, 96(ra)<br>                                         |
|  79|[0x8000c028]<br>0x00000000<br> [0x8000c040]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x5b39db9b4e7ac and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x5b39db9b4e7ac and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800019c0]:fsub.d t5, t3, s10, dyn<br> [0x800019c4]:csrrs a7, fcsr, zero<br> [0x800019c8]:sw t5, 104(ra)<br> [0x800019cc]:sw t6, 112(ra)<br> [0x800019d0]:sw t5, 120(ra)<br> [0x800019d4]:sw a7, 128(ra)<br>                                     |
|  80|[0x8000c048]<br>0x00000000<br> [0x8000c060]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc3c9ccfa1b1bb and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xc3c9ccfa1b1bb and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80001a10]:fsub.d t5, t3, s10, dyn<br> [0x80001a14]:csrrs a7, fcsr, zero<br> [0x80001a18]:sw t5, 136(ra)<br> [0x80001a1c]:sw t6, 144(ra)<br> [0x80001a20]:sw t5, 152(ra)<br> [0x80001a24]:sw a7, 160(ra)<br>                                     |
|  81|[0x8000c068]<br>0x00000000<br> [0x8000c080]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x987aaa2c7bb6a and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x987aaa2c7bb6a and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80001a60]:fsub.d t5, t3, s10, dyn<br> [0x80001a64]:csrrs a7, fcsr, zero<br> [0x80001a68]:sw t5, 168(ra)<br> [0x80001a6c]:sw t6, 176(ra)<br> [0x80001a70]:sw t5, 184(ra)<br> [0x80001a74]:sw a7, 192(ra)<br>                                     |
|  82|[0x8000c088]<br>0x00000000<br> [0x8000c0a0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc91ade861e02b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xc91ade861e02b and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80001ab0]:fsub.d t5, t3, s10, dyn<br> [0x80001ab4]:csrrs a7, fcsr, zero<br> [0x80001ab8]:sw t5, 200(ra)<br> [0x80001abc]:sw t6, 208(ra)<br> [0x80001ac0]:sw t5, 216(ra)<br> [0x80001ac4]:sw a7, 224(ra)<br>                                     |
|  83|[0x8000c0a8]<br>0x00000000<br> [0x8000c0c0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x946024d663351 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x946024d663351 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80001b00]:fsub.d t5, t3, s10, dyn<br> [0x80001b04]:csrrs a7, fcsr, zero<br> [0x80001b08]:sw t5, 232(ra)<br> [0x80001b0c]:sw t6, 240(ra)<br> [0x80001b10]:sw t5, 248(ra)<br> [0x80001b14]:sw a7, 256(ra)<br>                                     |
|  84|[0x8000c0c8]<br>0x00000000<br> [0x8000c0e0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x48f4a954751bd and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x48f4a954751bd and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80001b50]:fsub.d t5, t3, s10, dyn<br> [0x80001b54]:csrrs a7, fcsr, zero<br> [0x80001b58]:sw t5, 264(ra)<br> [0x80001b5c]:sw t6, 272(ra)<br> [0x80001b60]:sw t5, 280(ra)<br> [0x80001b64]:sw a7, 288(ra)<br>                                     |
|  85|[0x8000c0e8]<br>0x00000000<br> [0x8000c100]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fa and fm1 == 0xe19152f3266af and fs2 == 0 and fe2 == 0x7fa and fm2 == 0xe19152f3266af and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80001ba0]:fsub.d t5, t3, s10, dyn<br> [0x80001ba4]:csrrs a7, fcsr, zero<br> [0x80001ba8]:sw t5, 296(ra)<br> [0x80001bac]:sw t6, 304(ra)<br> [0x80001bb0]:sw t5, 312(ra)<br> [0x80001bb4]:sw a7, 320(ra)<br>                                     |
|  86|[0x8000c108]<br>0x00000000<br> [0x8000c120]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xef9105cd9390b and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xef9105cd9390b and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80001bf0]:fsub.d t5, t3, s10, dyn<br> [0x80001bf4]:csrrs a7, fcsr, zero<br> [0x80001bf8]:sw t5, 328(ra)<br> [0x80001bfc]:sw t6, 336(ra)<br> [0x80001c00]:sw t5, 344(ra)<br> [0x80001c04]:sw a7, 352(ra)<br>                                     |
|  87|[0x8000c128]<br>0x00000000<br> [0x8000c140]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x39bd6a090d93f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x39bd6a090d93f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80001c40]:fsub.d t5, t3, s10, dyn<br> [0x80001c44]:csrrs a7, fcsr, zero<br> [0x80001c48]:sw t5, 360(ra)<br> [0x80001c4c]:sw t6, 368(ra)<br> [0x80001c50]:sw t5, 376(ra)<br> [0x80001c54]:sw a7, 384(ra)<br>                                     |
|  88|[0x8000c148]<br>0x00000000<br> [0x8000c160]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3dcff67566087 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x3dcff67566087 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80001c90]:fsub.d t5, t3, s10, dyn<br> [0x80001c94]:csrrs a7, fcsr, zero<br> [0x80001c98]:sw t5, 392(ra)<br> [0x80001c9c]:sw t6, 400(ra)<br> [0x80001ca0]:sw t5, 408(ra)<br> [0x80001ca4]:sw a7, 416(ra)<br>                                     |
|  89|[0x8000c168]<br>0x00000000<br> [0x8000c180]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7cd8dfca2011d and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x7cd8dfca2011d and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80001ce0]:fsub.d t5, t3, s10, dyn<br> [0x80001ce4]:csrrs a7, fcsr, zero<br> [0x80001ce8]:sw t5, 424(ra)<br> [0x80001cec]:sw t6, 432(ra)<br> [0x80001cf0]:sw t5, 440(ra)<br> [0x80001cf4]:sw a7, 448(ra)<br>                                     |
|  90|[0x8000c188]<br>0x00000000<br> [0x8000c1a0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x026a2990b0a7f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x026a2990b0a7f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80001d30]:fsub.d t5, t3, s10, dyn<br> [0x80001d34]:csrrs a7, fcsr, zero<br> [0x80001d38]:sw t5, 456(ra)<br> [0x80001d3c]:sw t6, 464(ra)<br> [0x80001d40]:sw t5, 472(ra)<br> [0x80001d44]:sw a7, 480(ra)<br>                                     |
|  91|[0x8000c1a8]<br>0x00000000<br> [0x8000c1c0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc1e74ff66f075 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc1e74ff66f075 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80001d80]:fsub.d t5, t3, s10, dyn<br> [0x80001d84]:csrrs a7, fcsr, zero<br> [0x80001d88]:sw t5, 488(ra)<br> [0x80001d8c]:sw t6, 496(ra)<br> [0x80001d90]:sw t5, 504(ra)<br> [0x80001d94]:sw a7, 512(ra)<br>                                     |
|  92|[0x8000c1c8]<br>0x00000000<br> [0x8000c1e0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa38a3f0decfff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xa38a3f0decfff and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80001dd0]:fsub.d t5, t3, s10, dyn<br> [0x80001dd4]:csrrs a7, fcsr, zero<br> [0x80001dd8]:sw t5, 520(ra)<br> [0x80001ddc]:sw t6, 528(ra)<br> [0x80001de0]:sw t5, 536(ra)<br> [0x80001de4]:sw a7, 544(ra)<br>                                     |
|  93|[0x8000c1e8]<br>0x00000000<br> [0x8000c200]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x23fbd09d7e9b6 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x23fbd09d7e9b6 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80001e20]:fsub.d t5, t3, s10, dyn<br> [0x80001e24]:csrrs a7, fcsr, zero<br> [0x80001e28]:sw t5, 552(ra)<br> [0x80001e2c]:sw t6, 560(ra)<br> [0x80001e30]:sw t5, 568(ra)<br> [0x80001e34]:sw a7, 576(ra)<br>                                     |
|  94|[0x8000c208]<br>0x00000000<br> [0x8000c220]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbc4dccb7ac380 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xbc4dccb7ac380 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80001e70]:fsub.d t5, t3, s10, dyn<br> [0x80001e74]:csrrs a7, fcsr, zero<br> [0x80001e78]:sw t5, 584(ra)<br> [0x80001e7c]:sw t6, 592(ra)<br> [0x80001e80]:sw t5, 600(ra)<br> [0x80001e84]:sw a7, 608(ra)<br>                                     |
|  95|[0x8000c228]<br>0x00000000<br> [0x8000c240]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x6875b8a7de9f5 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x6875b8a7de9f5 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80001ec0]:fsub.d t5, t3, s10, dyn<br> [0x80001ec4]:csrrs a7, fcsr, zero<br> [0x80001ec8]:sw t5, 616(ra)<br> [0x80001ecc]:sw t6, 624(ra)<br> [0x80001ed0]:sw t5, 632(ra)<br> [0x80001ed4]:sw a7, 640(ra)<br>                                     |
|  96|[0x8000c248]<br>0x00000000<br> [0x8000c260]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x830a4319a6f37 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x830a4319a6f37 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80001f10]:fsub.d t5, t3, s10, dyn<br> [0x80001f14]:csrrs a7, fcsr, zero<br> [0x80001f18]:sw t5, 648(ra)<br> [0x80001f1c]:sw t6, 656(ra)<br> [0x80001f20]:sw t5, 664(ra)<br> [0x80001f24]:sw a7, 672(ra)<br>                                     |
|  97|[0x8000c268]<br>0x00000000<br> [0x8000c280]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xa5666b92c9353 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xa5666b92c9353 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80001f60]:fsub.d t5, t3, s10, dyn<br> [0x80001f64]:csrrs a7, fcsr, zero<br> [0x80001f68]:sw t5, 680(ra)<br> [0x80001f6c]:sw t6, 688(ra)<br> [0x80001f70]:sw t5, 696(ra)<br> [0x80001f74]:sw a7, 704(ra)<br>                                     |
|  98|[0x8000c288]<br>0x00000000<br> [0x8000c2a0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xbb5746cbb34cd and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xbb5746cbb34cd and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80001fb0]:fsub.d t5, t3, s10, dyn<br> [0x80001fb4]:csrrs a7, fcsr, zero<br> [0x80001fb8]:sw t5, 712(ra)<br> [0x80001fbc]:sw t6, 720(ra)<br> [0x80001fc0]:sw t5, 728(ra)<br> [0x80001fc4]:sw a7, 736(ra)<br>                                     |
|  99|[0x8000c2a8]<br>0x00000000<br> [0x8000c2c0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xabb8bbe03b7df and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xabb8bbe03b7df and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002000]:fsub.d t5, t3, s10, dyn<br> [0x80002004]:csrrs a7, fcsr, zero<br> [0x80002008]:sw t5, 744(ra)<br> [0x8000200c]:sw t6, 752(ra)<br> [0x80002010]:sw t5, 760(ra)<br> [0x80002014]:sw a7, 768(ra)<br>                                     |
| 100|[0x8000c2c8]<br>0x00000000<br> [0x8000c2e0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x2f2dacc08696f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x2f2dacc08696f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002050]:fsub.d t5, t3, s10, dyn<br> [0x80002054]:csrrs a7, fcsr, zero<br> [0x80002058]:sw t5, 776(ra)<br> [0x8000205c]:sw t6, 784(ra)<br> [0x80002060]:sw t5, 792(ra)<br> [0x80002064]:sw a7, 800(ra)<br>                                     |
| 101|[0x8000c2e8]<br>0x00000000<br> [0x8000c300]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x4d27694e5a38b and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x4d27694e5a38b and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800020a0]:fsub.d t5, t3, s10, dyn<br> [0x800020a4]:csrrs a7, fcsr, zero<br> [0x800020a8]:sw t5, 808(ra)<br> [0x800020ac]:sw t6, 816(ra)<br> [0x800020b0]:sw t5, 824(ra)<br> [0x800020b4]:sw a7, 832(ra)<br>                                     |
| 102|[0x8000c308]<br>0x00000000<br> [0x8000c320]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcca2a15201aa9 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xcca2a15201aa9 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800020f0]:fsub.d t5, t3, s10, dyn<br> [0x800020f4]:csrrs a7, fcsr, zero<br> [0x800020f8]:sw t5, 840(ra)<br> [0x800020fc]:sw t6, 848(ra)<br> [0x80002100]:sw t5, 856(ra)<br> [0x80002104]:sw a7, 864(ra)<br>                                     |
| 103|[0x8000c328]<br>0x00000000<br> [0x8000c340]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x9b58d2db8786f and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x9b58d2db8786f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002140]:fsub.d t5, t3, s10, dyn<br> [0x80002144]:csrrs a7, fcsr, zero<br> [0x80002148]:sw t5, 872(ra)<br> [0x8000214c]:sw t6, 880(ra)<br> [0x80002150]:sw t5, 888(ra)<br> [0x80002154]:sw a7, 896(ra)<br>                                     |
| 104|[0x8000c348]<br>0x00000000<br> [0x8000c360]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf95e94a40dc56 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf95e94a40dc56 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002190]:fsub.d t5, t3, s10, dyn<br> [0x80002194]:csrrs a7, fcsr, zero<br> [0x80002198]:sw t5, 904(ra)<br> [0x8000219c]:sw t6, 912(ra)<br> [0x800021a0]:sw t5, 920(ra)<br> [0x800021a4]:sw a7, 928(ra)<br>                                     |
| 105|[0x8000c368]<br>0x00000000<br> [0x8000c380]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x5b3be3b6f1597 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x5b3be3b6f1597 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800021e0]:fsub.d t5, t3, s10, dyn<br> [0x800021e4]:csrrs a7, fcsr, zero<br> [0x800021e8]:sw t5, 936(ra)<br> [0x800021ec]:sw t6, 944(ra)<br> [0x800021f0]:sw t5, 952(ra)<br> [0x800021f4]:sw a7, 960(ra)<br>                                     |
| 106|[0x8000c388]<br>0x00000000<br> [0x8000c3a0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x59522cc62b803 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x59522cc62b803 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002230]:fsub.d t5, t3, s10, dyn<br> [0x80002234]:csrrs a7, fcsr, zero<br> [0x80002238]:sw t5, 968(ra)<br> [0x8000223c]:sw t6, 976(ra)<br> [0x80002240]:sw t5, 984(ra)<br> [0x80002244]:sw a7, 992(ra)<br>                                     |
| 107|[0x8000c3a8]<br>0x00000000<br> [0x8000c3c0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xdd47ad230c500 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xdd47ad230c500 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002280]:fsub.d t5, t3, s10, dyn<br> [0x80002284]:csrrs a7, fcsr, zero<br> [0x80002288]:sw t5, 1000(ra)<br> [0x8000228c]:sw t6, 1008(ra)<br> [0x80002290]:sw t5, 1016(ra)<br> [0x80002294]:sw a7, 1024(ra)<br>                                 |
| 108|[0x8000c3c8]<br>0x00000000<br> [0x8000c3e0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc3c90ab59cc1f and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc3c90ab59cc1f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800022d0]:fsub.d t5, t3, s10, dyn<br> [0x800022d4]:csrrs a7, fcsr, zero<br> [0x800022d8]:sw t5, 1032(ra)<br> [0x800022dc]:sw t6, 1040(ra)<br> [0x800022e0]:sw t5, 1048(ra)<br> [0x800022e4]:sw a7, 1056(ra)<br>                                 |
| 109|[0x8000c3e8]<br>0x00000000<br> [0x8000c400]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf0206ee24c395 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xf0206ee24c395 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002320]:fsub.d t5, t3, s10, dyn<br> [0x80002324]:csrrs a7, fcsr, zero<br> [0x80002328]:sw t5, 1064(ra)<br> [0x8000232c]:sw t6, 1072(ra)<br> [0x80002330]:sw t5, 1080(ra)<br> [0x80002334]:sw a7, 1088(ra)<br>                                 |
| 110|[0x8000c408]<br>0x00000000<br> [0x8000c420]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xe08b2a10b8fdf and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xe08b2a10b8fdf and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002370]:fsub.d t5, t3, s10, dyn<br> [0x80002374]:csrrs a7, fcsr, zero<br> [0x80002378]:sw t5, 1096(ra)<br> [0x8000237c]:sw t6, 1104(ra)<br> [0x80002380]:sw t5, 1112(ra)<br> [0x80002384]:sw a7, 1120(ra)<br>                                 |
| 111|[0x8000c428]<br>0x00000000<br> [0x8000c440]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x5ad9a8441acdf and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x5ad9a8441acdf and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800023c0]:fsub.d t5, t3, s10, dyn<br> [0x800023c4]:csrrs a7, fcsr, zero<br> [0x800023c8]:sw t5, 1128(ra)<br> [0x800023cc]:sw t6, 1136(ra)<br> [0x800023d0]:sw t5, 1144(ra)<br> [0x800023d4]:sw a7, 1152(ra)<br>                                 |
| 112|[0x8000c448]<br>0x00000000<br> [0x8000c460]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd26cfda272030 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd26cfda272030 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002410]:fsub.d t5, t3, s10, dyn<br> [0x80002414]:csrrs a7, fcsr, zero<br> [0x80002418]:sw t5, 1160(ra)<br> [0x8000241c]:sw t6, 1168(ra)<br> [0x80002420]:sw t5, 1176(ra)<br> [0x80002424]:sw a7, 1184(ra)<br>                                 |
| 113|[0x8000c468]<br>0x00000000<br> [0x8000c480]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x84aae05543502 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x84aae05543502 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002460]:fsub.d t5, t3, s10, dyn<br> [0x80002464]:csrrs a7, fcsr, zero<br> [0x80002468]:sw t5, 1192(ra)<br> [0x8000246c]:sw t6, 1200(ra)<br> [0x80002470]:sw t5, 1208(ra)<br> [0x80002474]:sw a7, 1216(ra)<br>                                 |
| 114|[0x8000c488]<br>0x00000000<br> [0x8000c4a0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xaea8e11056b0f and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xaea8e11056b0f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800024b0]:fsub.d t5, t3, s10, dyn<br> [0x800024b4]:csrrs a7, fcsr, zero<br> [0x800024b8]:sw t5, 1224(ra)<br> [0x800024bc]:sw t6, 1232(ra)<br> [0x800024c0]:sw t5, 1240(ra)<br> [0x800024c4]:sw a7, 1248(ra)<br>                                 |
| 115|[0x8000c4a8]<br>0x00000000<br> [0x8000c4c0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x5c73bb8e94b2b and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x5c73bb8e94b2b and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002500]:fsub.d t5, t3, s10, dyn<br> [0x80002504]:csrrs a7, fcsr, zero<br> [0x80002508]:sw t5, 1256(ra)<br> [0x8000250c]:sw t6, 1264(ra)<br> [0x80002510]:sw t5, 1272(ra)<br> [0x80002514]:sw a7, 1280(ra)<br>                                 |
| 116|[0x8000c4c8]<br>0x00000000<br> [0x8000c4e0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbc1e737c6a698 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xbc1e737c6a698 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002550]:fsub.d t5, t3, s10, dyn<br> [0x80002554]:csrrs a7, fcsr, zero<br> [0x80002558]:sw t5, 1288(ra)<br> [0x8000255c]:sw t6, 1296(ra)<br> [0x80002560]:sw t5, 1304(ra)<br> [0x80002564]:sw a7, 1312(ra)<br>                                 |
| 117|[0x8000c4e8]<br>0x00000000<br> [0x8000c500]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe89afcadc456f and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xe89afcadc456f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800025a0]:fsub.d t5, t3, s10, dyn<br> [0x800025a4]:csrrs a7, fcsr, zero<br> [0x800025a8]:sw t5, 1320(ra)<br> [0x800025ac]:sw t6, 1328(ra)<br> [0x800025b0]:sw t5, 1336(ra)<br> [0x800025b4]:sw a7, 1344(ra)<br>                                 |
| 118|[0x8000c508]<br>0x00000000<br> [0x8000c520]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x153045947810b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x153045947810b and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800025f0]:fsub.d t5, t3, s10, dyn<br> [0x800025f4]:csrrs a7, fcsr, zero<br> [0x800025f8]:sw t5, 1352(ra)<br> [0x800025fc]:sw t6, 1360(ra)<br> [0x80002600]:sw t5, 1368(ra)<br> [0x80002604]:sw a7, 1376(ra)<br>                                 |
| 119|[0x8000c528]<br>0x00000000<br> [0x8000c540]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xab1c42a43630f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xab1c42a43630f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002640]:fsub.d t5, t3, s10, dyn<br> [0x80002644]:csrrs a7, fcsr, zero<br> [0x80002648]:sw t5, 1384(ra)<br> [0x8000264c]:sw t6, 1392(ra)<br> [0x80002650]:sw t5, 1400(ra)<br> [0x80002654]:sw a7, 1408(ra)<br>                                 |
| 120|[0x8000c548]<br>0x00000000<br> [0x8000c560]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x2bccdcc2ad897 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x2bccdcc2ad897 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002690]:fsub.d t5, t3, s10, dyn<br> [0x80002694]:csrrs a7, fcsr, zero<br> [0x80002698]:sw t5, 1416(ra)<br> [0x8000269c]:sw t6, 1424(ra)<br> [0x800026a0]:sw t5, 1432(ra)<br> [0x800026a4]:sw a7, 1440(ra)<br>                                 |
| 121|[0x8000c568]<br>0x00000000<br> [0x8000c580]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9383ffc96dd3f and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x9383ffc96dd3f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800026e0]:fsub.d t5, t3, s10, dyn<br> [0x800026e4]:csrrs a7, fcsr, zero<br> [0x800026e8]:sw t5, 1448(ra)<br> [0x800026ec]:sw t6, 1456(ra)<br> [0x800026f0]:sw t5, 1464(ra)<br> [0x800026f4]:sw a7, 1472(ra)<br>                                 |
| 122|[0x8000c588]<br>0x00000000<br> [0x8000c5a0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x5d14398eae23f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x5d14398eae23f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002730]:fsub.d t5, t3, s10, dyn<br> [0x80002734]:csrrs a7, fcsr, zero<br> [0x80002738]:sw t5, 1480(ra)<br> [0x8000273c]:sw t6, 1488(ra)<br> [0x80002740]:sw t5, 1496(ra)<br> [0x80002744]:sw a7, 1504(ra)<br>                                 |
| 123|[0x8000c5a8]<br>0x00000000<br> [0x8000c5c0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9055ab3b464b5 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x9055ab3b464b5 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002780]:fsub.d t5, t3, s10, dyn<br> [0x80002784]:csrrs a7, fcsr, zero<br> [0x80002788]:sw t5, 1512(ra)<br> [0x8000278c]:sw t6, 1520(ra)<br> [0x80002790]:sw t5, 1528(ra)<br> [0x80002794]:sw a7, 1536(ra)<br>                                 |
| 124|[0x8000c5c8]<br>0x00000000<br> [0x8000c5e0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x243d79e337b38 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x243d79e337b38 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800027d0]:fsub.d t5, t3, s10, dyn<br> [0x800027d4]:csrrs a7, fcsr, zero<br> [0x800027d8]:sw t5, 1544(ra)<br> [0x800027dc]:sw t6, 1552(ra)<br> [0x800027e0]:sw t5, 1560(ra)<br> [0x800027e4]:sw a7, 1568(ra)<br>                                 |
| 125|[0x8000c5e8]<br>0x00000000<br> [0x8000c600]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x4ed205e78cd0f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x4ed205e78cd0f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002820]:fsub.d t5, t3, s10, dyn<br> [0x80002824]:csrrs a7, fcsr, zero<br> [0x80002828]:sw t5, 1576(ra)<br> [0x8000282c]:sw t6, 1584(ra)<br> [0x80002830]:sw t5, 1592(ra)<br> [0x80002834]:sw a7, 1600(ra)<br>                                 |
| 126|[0x8000c608]<br>0x00000000<br> [0x8000c620]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc812c292ea556 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc812c292ea556 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002870]:fsub.d t5, t3, s10, dyn<br> [0x80002874]:csrrs a7, fcsr, zero<br> [0x80002878]:sw t5, 1608(ra)<br> [0x8000287c]:sw t6, 1616(ra)<br> [0x80002880]:sw t5, 1624(ra)<br> [0x80002884]:sw a7, 1632(ra)<br>                                 |
| 127|[0x8000c628]<br>0x00000000<br> [0x8000c640]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd9a2688750f46 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd9a2688750f46 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800028c0]:fsub.d t5, t3, s10, dyn<br> [0x800028c4]:csrrs a7, fcsr, zero<br> [0x800028c8]:sw t5, 1640(ra)<br> [0x800028cc]:sw t6, 1648(ra)<br> [0x800028d0]:sw t5, 1656(ra)<br> [0x800028d4]:sw a7, 1664(ra)<br>                                 |
| 128|[0x8000c648]<br>0x00000000<br> [0x8000c660]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x1bd5cc8dca1e5 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x1bd5cc8dca1e5 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002910]:fsub.d t5, t3, s10, dyn<br> [0x80002914]:csrrs a7, fcsr, zero<br> [0x80002918]:sw t5, 1672(ra)<br> [0x8000291c]:sw t6, 1680(ra)<br> [0x80002920]:sw t5, 1688(ra)<br> [0x80002924]:sw a7, 1696(ra)<br>                                 |
| 129|[0x8000c668]<br>0x00000000<br> [0x8000c680]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa7d13a52ed5ec and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xa7d13a52ed5ec and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002960]:fsub.d t5, t3, s10, dyn<br> [0x80002964]:csrrs a7, fcsr, zero<br> [0x80002968]:sw t5, 1704(ra)<br> [0x8000296c]:sw t6, 1712(ra)<br> [0x80002970]:sw t5, 1720(ra)<br> [0x80002974]:sw a7, 1728(ra)<br>                                 |
| 130|[0x8000c688]<br>0x00000000<br> [0x8000c6a0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x574031c0ee5b5 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x574031c0ee5b5 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800029b0]:fsub.d t5, t3, s10, dyn<br> [0x800029b4]:csrrs a7, fcsr, zero<br> [0x800029b8]:sw t5, 1736(ra)<br> [0x800029bc]:sw t6, 1744(ra)<br> [0x800029c0]:sw t5, 1752(ra)<br> [0x800029c4]:sw a7, 1760(ra)<br>                                 |
| 131|[0x8000c6a8]<br>0x00000000<br> [0x8000c6c0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xd775b7a6f9327 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xd775b7a6f9327 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002a00]:fsub.d t5, t3, s10, dyn<br> [0x80002a04]:csrrs a7, fcsr, zero<br> [0x80002a08]:sw t5, 1768(ra)<br> [0x80002a0c]:sw t6, 1776(ra)<br> [0x80002a10]:sw t5, 1784(ra)<br> [0x80002a14]:sw a7, 1792(ra)<br>                                 |
| 132|[0x8000c6c8]<br>0x00000000<br> [0x8000c6e0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x47df70c06ea5f and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x47df70c06ea5f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002a50]:fsub.d t5, t3, s10, dyn<br> [0x80002a54]:csrrs a7, fcsr, zero<br> [0x80002a58]:sw t5, 1800(ra)<br> [0x80002a5c]:sw t6, 1808(ra)<br> [0x80002a60]:sw t5, 1816(ra)<br> [0x80002a64]:sw a7, 1824(ra)<br>                                 |
| 133|[0x8000c6e8]<br>0x00000000<br> [0x8000c700]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf9efe9258e03a and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf9efe9258e03a and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002aa0]:fsub.d t5, t3, s10, dyn<br> [0x80002aa4]:csrrs a7, fcsr, zero<br> [0x80002aa8]:sw t5, 1832(ra)<br> [0x80002aac]:sw t6, 1840(ra)<br> [0x80002ab0]:sw t5, 1848(ra)<br> [0x80002ab4]:sw a7, 1856(ra)<br>                                 |
| 134|[0x8000c708]<br>0x00000000<br> [0x8000c720]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xd95388e6dd7e7 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xd95388e6dd7e7 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002af0]:fsub.d t5, t3, s10, dyn<br> [0x80002af4]:csrrs a7, fcsr, zero<br> [0x80002af8]:sw t5, 1864(ra)<br> [0x80002afc]:sw t6, 1872(ra)<br> [0x80002b00]:sw t5, 1880(ra)<br> [0x80002b04]:sw a7, 1888(ra)<br>                                 |
| 135|[0x8000c728]<br>0x00000000<br> [0x8000c740]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xee6dc228b09a7 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xee6dc228b09a7 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002b40]:fsub.d t5, t3, s10, dyn<br> [0x80002b44]:csrrs a7, fcsr, zero<br> [0x80002b48]:sw t5, 1896(ra)<br> [0x80002b4c]:sw t6, 1904(ra)<br> [0x80002b50]:sw t5, 1912(ra)<br> [0x80002b54]:sw a7, 1920(ra)<br>                                 |
| 136|[0x8000c748]<br>0x00000000<br> [0x8000c760]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xa5356adec5cbf and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xa5356adec5cbf and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002b90]:fsub.d t5, t3, s10, dyn<br> [0x80002b94]:csrrs a7, fcsr, zero<br> [0x80002b98]:sw t5, 1928(ra)<br> [0x80002b9c]:sw t6, 1936(ra)<br> [0x80002ba0]:sw t5, 1944(ra)<br> [0x80002ba4]:sw a7, 1952(ra)<br>                                 |
| 137|[0x8000c768]<br>0x00000000<br> [0x8000c780]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x835b1de73afa3 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x835b1de73afa3 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002be0]:fsub.d t5, t3, s10, dyn<br> [0x80002be4]:csrrs a7, fcsr, zero<br> [0x80002be8]:sw t5, 1960(ra)<br> [0x80002bec]:sw t6, 1968(ra)<br> [0x80002bf0]:sw t5, 1976(ra)<br> [0x80002bf4]:sw a7, 1984(ra)<br>                                 |
| 138|[0x8000c788]<br>0x00000000<br> [0x8000c7a0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd185a4345fd91 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd185a4345fd91 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002c30]:fsub.d t5, t3, s10, dyn<br> [0x80002c34]:csrrs a7, fcsr, zero<br> [0x80002c38]:sw t5, 1992(ra)<br> [0x80002c3c]:sw t6, 2000(ra)<br> [0x80002c40]:sw t5, 2008(ra)<br> [0x80002c44]:sw a7, 2016(ra)<br>                                 |
| 139|[0x8000c7a8]<br>0x00000000<br> [0x8000c7c0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x037df25b16113 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x037df25b16113 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002c80]:fsub.d t5, t3, s10, dyn<br> [0x80002c84]:csrrs a7, fcsr, zero<br> [0x80002c88]:sw t5, 2024(ra)<br> [0x80002c8c]:sw t6, 2032(ra)<br> [0x80002c90]:sw t5, 2040(ra)<br> [0x80002c94]:addi ra, ra, 2040<br> [0x80002c98]:sw a7, 8(ra)<br> |
| 140|[0x8000c7c8]<br>0x00000000<br> [0x8000c7e0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x9a21046a4c767 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x9a21046a4c767 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002d14]:fsub.d t5, t3, s10, dyn<br> [0x80002d18]:csrrs a7, fcsr, zero<br> [0x80002d1c]:sw t5, 16(ra)<br> [0x80002d20]:sw t6, 24(ra)<br> [0x80002d24]:sw t5, 32(ra)<br> [0x80002d28]:sw a7, 40(ra)<br>                                         |
| 141|[0x8000c7e8]<br>0x00000000<br> [0x8000c800]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x44919c1beab5f and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x44919c1beab5f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002da4]:fsub.d t5, t3, s10, dyn<br> [0x80002da8]:csrrs a7, fcsr, zero<br> [0x80002dac]:sw t5, 48(ra)<br> [0x80002db0]:sw t6, 56(ra)<br> [0x80002db4]:sw t5, 64(ra)<br> [0x80002db8]:sw a7, 72(ra)<br>                                         |
| 142|[0x8000c808]<br>0x00000000<br> [0x8000c820]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf715337b3d172 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf715337b3d172 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002e34]:fsub.d t5, t3, s10, dyn<br> [0x80002e38]:csrrs a7, fcsr, zero<br> [0x80002e3c]:sw t5, 80(ra)<br> [0x80002e40]:sw t6, 88(ra)<br> [0x80002e44]:sw t5, 96(ra)<br> [0x80002e48]:sw a7, 104(ra)<br>                                        |
| 143|[0x8000c828]<br>0x00000000<br> [0x8000c840]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3171b5147eff2 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x3171b5147eff2 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002ec4]:fsub.d t5, t3, s10, dyn<br> [0x80002ec8]:csrrs a7, fcsr, zero<br> [0x80002ecc]:sw t5, 112(ra)<br> [0x80002ed0]:sw t6, 120(ra)<br> [0x80002ed4]:sw t5, 128(ra)<br> [0x80002ed8]:sw a7, 136(ra)<br>                                     |
| 144|[0x8000c848]<br>0x00000000<br> [0x8000c860]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x40ccb2b303daf and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x40ccb2b303daf and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002f54]:fsub.d t5, t3, s10, dyn<br> [0x80002f58]:csrrs a7, fcsr, zero<br> [0x80002f5c]:sw t5, 144(ra)<br> [0x80002f60]:sw t6, 152(ra)<br> [0x80002f64]:sw t5, 160(ra)<br> [0x80002f68]:sw a7, 168(ra)<br>                                     |
| 145|[0x8000c868]<br>0x00000000<br> [0x8000c880]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd3a81e544f745 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd3a81e544f745 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80002fe4]:fsub.d t5, t3, s10, dyn<br> [0x80002fe8]:csrrs a7, fcsr, zero<br> [0x80002fec]:sw t5, 176(ra)<br> [0x80002ff0]:sw t6, 184(ra)<br> [0x80002ff4]:sw t5, 192(ra)<br> [0x80002ff8]:sw a7, 200(ra)<br>                                     |
| 146|[0x8000c888]<br>0x00000000<br> [0x8000c8a0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf0892add2cc6e and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf0892add2cc6e and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80003074]:fsub.d t5, t3, s10, dyn<br> [0x80003078]:csrrs a7, fcsr, zero<br> [0x8000307c]:sw t5, 208(ra)<br> [0x80003080]:sw t6, 216(ra)<br> [0x80003084]:sw t5, 224(ra)<br> [0x80003088]:sw a7, 232(ra)<br>                                     |
| 147|[0x8000c8a8]<br>0x00000000<br> [0x8000c8c0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xb8b73fc8fea5b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xb8b73fc8fea5b and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80003104]:fsub.d t5, t3, s10, dyn<br> [0x80003108]:csrrs a7, fcsr, zero<br> [0x8000310c]:sw t5, 240(ra)<br> [0x80003110]:sw t6, 248(ra)<br> [0x80003114]:sw t5, 256(ra)<br> [0x80003118]:sw a7, 264(ra)<br>                                     |
| 148|[0x8000c8c8]<br>0x00000000<br> [0x8000c8e0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf5bc627909931 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf5bc627909931 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80003194]:fsub.d t5, t3, s10, dyn<br> [0x80003198]:csrrs a7, fcsr, zero<br> [0x8000319c]:sw t5, 272(ra)<br> [0x800031a0]:sw t6, 280(ra)<br> [0x800031a4]:sw t5, 288(ra)<br> [0x800031a8]:sw a7, 296(ra)<br>                                     |
| 149|[0x8000c8e8]<br>0x00000000<br> [0x8000c900]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x605a6a1e02c96 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x605a6a1e02c96 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80003224]:fsub.d t5, t3, s10, dyn<br> [0x80003228]:csrrs a7, fcsr, zero<br> [0x8000322c]:sw t5, 304(ra)<br> [0x80003230]:sw t6, 312(ra)<br> [0x80003234]:sw t5, 320(ra)<br> [0x80003238]:sw a7, 328(ra)<br>                                     |
| 150|[0x8000c908]<br>0x00000000<br> [0x8000c920]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x094dd69773d7b and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x094dd69773d7b and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800032b4]:fsub.d t5, t3, s10, dyn<br> [0x800032b8]:csrrs a7, fcsr, zero<br> [0x800032bc]:sw t5, 336(ra)<br> [0x800032c0]:sw t6, 344(ra)<br> [0x800032c4]:sw t5, 352(ra)<br> [0x800032c8]:sw a7, 360(ra)<br>                                     |
| 151|[0x8000c928]<br>0x00000000<br> [0x8000c940]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3ab263197fe7f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x3ab263197fe7f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80003344]:fsub.d t5, t3, s10, dyn<br> [0x80003348]:csrrs a7, fcsr, zero<br> [0x8000334c]:sw t5, 368(ra)<br> [0x80003350]:sw t6, 376(ra)<br> [0x80003354]:sw t5, 384(ra)<br> [0x80003358]:sw a7, 392(ra)<br>                                     |
| 152|[0x8000c948]<br>0x00000000<br> [0x8000c960]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x4d4955a3d407f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x4d4955a3d407f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800033d4]:fsub.d t5, t3, s10, dyn<br> [0x800033d8]:csrrs a7, fcsr, zero<br> [0x800033dc]:sw t5, 400(ra)<br> [0x800033e0]:sw t6, 408(ra)<br> [0x800033e4]:sw t5, 416(ra)<br> [0x800033e8]:sw a7, 424(ra)<br>                                     |
| 153|[0x8000c968]<br>0x00000000<br> [0x8000c980]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf77d273035d94 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf77d273035d94 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80003464]:fsub.d t5, t3, s10, dyn<br> [0x80003468]:csrrs a7, fcsr, zero<br> [0x8000346c]:sw t5, 432(ra)<br> [0x80003470]:sw t6, 440(ra)<br> [0x80003474]:sw t5, 448(ra)<br> [0x80003478]:sw a7, 456(ra)<br>                                     |
| 154|[0x8000c988]<br>0x00000000<br> [0x8000c9a0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4dd45324c2409 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x4dd45324c2409 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800034f4]:fsub.d t5, t3, s10, dyn<br> [0x800034f8]:csrrs a7, fcsr, zero<br> [0x800034fc]:sw t5, 464(ra)<br> [0x80003500]:sw t6, 472(ra)<br> [0x80003504]:sw t5, 480(ra)<br> [0x80003508]:sw a7, 488(ra)<br>                                     |
| 155|[0x8000c9a8]<br>0x00000000<br> [0x8000c9c0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe1a7f48e8e26b and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xe1a7f48e8e26b and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80003584]:fsub.d t5, t3, s10, dyn<br> [0x80003588]:csrrs a7, fcsr, zero<br> [0x8000358c]:sw t5, 496(ra)<br> [0x80003590]:sw t6, 504(ra)<br> [0x80003594]:sw t5, 512(ra)<br> [0x80003598]:sw a7, 520(ra)<br>                                     |
| 156|[0x8000c9c8]<br>0x00000000<br> [0x8000c9e0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3a25a98541333 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x3a25a98541333 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80003614]:fsub.d t5, t3, s10, dyn<br> [0x80003618]:csrrs a7, fcsr, zero<br> [0x8000361c]:sw t5, 528(ra)<br> [0x80003620]:sw t6, 536(ra)<br> [0x80003624]:sw t5, 544(ra)<br> [0x80003628]:sw a7, 552(ra)<br>                                     |
| 157|[0x8000c9e8]<br>0x00000000<br> [0x8000ca00]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x445637e5783c3 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x445637e5783c3 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800036a4]:fsub.d t5, t3, s10, dyn<br> [0x800036a8]:csrrs a7, fcsr, zero<br> [0x800036ac]:sw t5, 560(ra)<br> [0x800036b0]:sw t6, 568(ra)<br> [0x800036b4]:sw t5, 576(ra)<br> [0x800036b8]:sw a7, 584(ra)<br>                                     |
| 158|[0x8000ca08]<br>0x00000000<br> [0x8000ca20]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc978dd3af76c1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc978dd3af76c1 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80003734]:fsub.d t5, t3, s10, dyn<br> [0x80003738]:csrrs a7, fcsr, zero<br> [0x8000373c]:sw t5, 592(ra)<br> [0x80003740]:sw t6, 600(ra)<br> [0x80003744]:sw t5, 608(ra)<br> [0x80003748]:sw a7, 616(ra)<br>                                     |
| 159|[0x8000ca28]<br>0x00000000<br> [0x8000ca40]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x565b7f0cebd9f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x565b7f0cebd9f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800037c4]:fsub.d t5, t3, s10, dyn<br> [0x800037c8]:csrrs a7, fcsr, zero<br> [0x800037cc]:sw t5, 624(ra)<br> [0x800037d0]:sw t6, 632(ra)<br> [0x800037d4]:sw t5, 640(ra)<br> [0x800037d8]:sw a7, 648(ra)<br>                                     |
| 160|[0x8000ca48]<br>0x00000000<br> [0x8000ca60]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x2870c773af305 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x2870c773af305 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80003854]:fsub.d t5, t3, s10, dyn<br> [0x80003858]:csrrs a7, fcsr, zero<br> [0x8000385c]:sw t5, 656(ra)<br> [0x80003860]:sw t6, 664(ra)<br> [0x80003864]:sw t5, 672(ra)<br> [0x80003868]:sw a7, 680(ra)<br>                                     |
| 161|[0x8000ca68]<br>0x00000000<br> [0x8000ca80]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xf5f5f1385c1af and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xf5f5f1385c1af and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800038e4]:fsub.d t5, t3, s10, dyn<br> [0x800038e8]:csrrs a7, fcsr, zero<br> [0x800038ec]:sw t5, 688(ra)<br> [0x800038f0]:sw t6, 696(ra)<br> [0x800038f4]:sw t5, 704(ra)<br> [0x800038f8]:sw a7, 712(ra)<br>                                     |
| 162|[0x8000ca88]<br>0x00000000<br> [0x8000caa0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xf3381366daa33 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xf3381366daa33 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80003974]:fsub.d t5, t3, s10, dyn<br> [0x80003978]:csrrs a7, fcsr, zero<br> [0x8000397c]:sw t5, 720(ra)<br> [0x80003980]:sw t6, 728(ra)<br> [0x80003984]:sw t5, 736(ra)<br> [0x80003988]:sw a7, 744(ra)<br>                                     |
| 163|[0x8000caa8]<br>0x00000000<br> [0x8000cac0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x1a3782778609c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x1a3782778609c and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80003a04]:fsub.d t5, t3, s10, dyn<br> [0x80003a08]:csrrs a7, fcsr, zero<br> [0x80003a0c]:sw t5, 752(ra)<br> [0x80003a10]:sw t6, 760(ra)<br> [0x80003a14]:sw t5, 768(ra)<br> [0x80003a18]:sw a7, 776(ra)<br>                                     |
| 164|[0x8000cac8]<br>0x00000000<br> [0x8000cae0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x33bb4c0b03e47 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x33bb4c0b03e47 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80003a94]:fsub.d t5, t3, s10, dyn<br> [0x80003a98]:csrrs a7, fcsr, zero<br> [0x80003a9c]:sw t5, 784(ra)<br> [0x80003aa0]:sw t6, 792(ra)<br> [0x80003aa4]:sw t5, 800(ra)<br> [0x80003aa8]:sw a7, 808(ra)<br>                                     |
| 165|[0x8000cae8]<br>0x00000000<br> [0x8000cb00]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x29cd1fe017e0f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x29cd1fe017e0f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80003b24]:fsub.d t5, t3, s10, dyn<br> [0x80003b28]:csrrs a7, fcsr, zero<br> [0x80003b2c]:sw t5, 816(ra)<br> [0x80003b30]:sw t6, 824(ra)<br> [0x80003b34]:sw t5, 832(ra)<br> [0x80003b38]:sw a7, 840(ra)<br>                                     |
| 166|[0x8000cb08]<br>0x00000000<br> [0x8000cb20]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf9196c3c02c3d and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xf9196c3c02c3d and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80003bb4]:fsub.d t5, t3, s10, dyn<br> [0x80003bb8]:csrrs a7, fcsr, zero<br> [0x80003bbc]:sw t5, 848(ra)<br> [0x80003bc0]:sw t6, 856(ra)<br> [0x80003bc4]:sw t5, 864(ra)<br> [0x80003bc8]:sw a7, 872(ra)<br>                                     |
| 167|[0x8000cb28]<br>0x00000000<br> [0x8000cb40]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x3bc28319d6d6f and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x3bc28319d6d6f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80003c44]:fsub.d t5, t3, s10, dyn<br> [0x80003c48]:csrrs a7, fcsr, zero<br> [0x80003c4c]:sw t5, 880(ra)<br> [0x80003c50]:sw t6, 888(ra)<br> [0x80003c54]:sw t5, 896(ra)<br> [0x80003c58]:sw a7, 904(ra)<br>                                     |
| 168|[0x8000cb48]<br>0x00000000<br> [0x8000cb60]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x97081394ff7c0 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x97081394ff7c0 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80003cd4]:fsub.d t5, t3, s10, dyn<br> [0x80003cd8]:csrrs a7, fcsr, zero<br> [0x80003cdc]:sw t5, 912(ra)<br> [0x80003ce0]:sw t6, 920(ra)<br> [0x80003ce4]:sw t5, 928(ra)<br> [0x80003ce8]:sw a7, 936(ra)<br>                                     |
| 169|[0x8000cb68]<br>0x00000000<br> [0x8000cb80]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe60134aa9369f and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xe60134aa9369f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80003d64]:fsub.d t5, t3, s10, dyn<br> [0x80003d68]:csrrs a7, fcsr, zero<br> [0x80003d6c]:sw t5, 944(ra)<br> [0x80003d70]:sw t6, 952(ra)<br> [0x80003d74]:sw t5, 960(ra)<br> [0x80003d78]:sw a7, 968(ra)<br>                                     |
| 170|[0x8000cb88]<br>0x00000000<br> [0x8000cba0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8d6b438992705 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x8d6b438992705 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80003df4]:fsub.d t5, t3, s10, dyn<br> [0x80003df8]:csrrs a7, fcsr, zero<br> [0x80003dfc]:sw t5, 976(ra)<br> [0x80003e00]:sw t6, 984(ra)<br> [0x80003e04]:sw t5, 992(ra)<br> [0x80003e08]:sw a7, 1000(ra)<br>                                    |
| 171|[0x8000cba8]<br>0x00000000<br> [0x8000cbc0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa4a2387765198 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xa4a2387765198 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80003e84]:fsub.d t5, t3, s10, dyn<br> [0x80003e88]:csrrs a7, fcsr, zero<br> [0x80003e8c]:sw t5, 1008(ra)<br> [0x80003e90]:sw t6, 1016(ra)<br> [0x80003e94]:sw t5, 1024(ra)<br> [0x80003e98]:sw a7, 1032(ra)<br>                                 |
| 172|[0x8000cbc8]<br>0x00000000<br> [0x8000cbe0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xb5c56d6b2c837 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xb5c56d6b2c837 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80003f14]:fsub.d t5, t3, s10, dyn<br> [0x80003f18]:csrrs a7, fcsr, zero<br> [0x80003f1c]:sw t5, 1040(ra)<br> [0x80003f20]:sw t6, 1048(ra)<br> [0x80003f24]:sw t5, 1056(ra)<br> [0x80003f28]:sw a7, 1064(ra)<br>                                 |
| 173|[0x8000cbe8]<br>0x00000000<br> [0x8000cc00]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd6b5f3e68568b and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd6b5f3e68568b and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80003fa4]:fsub.d t5, t3, s10, dyn<br> [0x80003fa8]:csrrs a7, fcsr, zero<br> [0x80003fac]:sw t5, 1072(ra)<br> [0x80003fb0]:sw t6, 1080(ra)<br> [0x80003fb4]:sw t5, 1088(ra)<br> [0x80003fb8]:sw a7, 1096(ra)<br>                                 |
| 174|[0x8000cc08]<br>0x00000000<br> [0x8000cc20]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc99ac0cd3b3ca and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc99ac0cd3b3ca and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80004034]:fsub.d t5, t3, s10, dyn<br> [0x80004038]:csrrs a7, fcsr, zero<br> [0x8000403c]:sw t5, 1104(ra)<br> [0x80004040]:sw t6, 1112(ra)<br> [0x80004044]:sw t5, 1120(ra)<br> [0x80004048]:sw a7, 1128(ra)<br>                                 |
| 175|[0x8000cc28]<br>0x00000000<br> [0x8000cc40]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd22aa76e3f8bc and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd22aa76e3f8bc and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800040c4]:fsub.d t5, t3, s10, dyn<br> [0x800040c8]:csrrs a7, fcsr, zero<br> [0x800040cc]:sw t5, 1136(ra)<br> [0x800040d0]:sw t6, 1144(ra)<br> [0x800040d4]:sw t5, 1152(ra)<br> [0x800040d8]:sw a7, 1160(ra)<br>                                 |
| 176|[0x8000c7c8]<br>0x00000000<br> [0x8000c7e0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x9efa662b0261b and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x9efa662b0261b and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800074e4]:fsub.d t5, t3, s10, dyn<br> [0x800074e8]:csrrs a7, fcsr, zero<br> [0x800074ec]:sw t5, 0(ra)<br> [0x800074f0]:sw t6, 8(ra)<br> [0x800074f4]:sw t5, 16(ra)<br> [0x800074f8]:sw a7, 24(ra)<br>                                           |
| 177|[0x8000c7e8]<br>0x00000000<br> [0x8000c800]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x2b3a267e5dfb6 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x2b3a267e5dfb6 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007534]:fsub.d t5, t3, s10, dyn<br> [0x80007538]:csrrs a7, fcsr, zero<br> [0x8000753c]:sw t5, 32(ra)<br> [0x80007540]:sw t6, 40(ra)<br> [0x80007544]:sw t5, 48(ra)<br> [0x80007548]:sw a7, 56(ra)<br>                                         |
| 178|[0x8000c808]<br>0x00000000<br> [0x8000c820]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcbbac03deb701 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xcbbac03deb701 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007584]:fsub.d t5, t3, s10, dyn<br> [0x80007588]:csrrs a7, fcsr, zero<br> [0x8000758c]:sw t5, 64(ra)<br> [0x80007590]:sw t6, 72(ra)<br> [0x80007594]:sw t5, 80(ra)<br> [0x80007598]:sw a7, 88(ra)<br>                                         |
| 179|[0x8000c828]<br>0x00000000<br> [0x8000c840]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf886e2fe6ac5f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xf886e2fe6ac5f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800075d4]:fsub.d t5, t3, s10, dyn<br> [0x800075d8]:csrrs a7, fcsr, zero<br> [0x800075dc]:sw t5, 96(ra)<br> [0x800075e0]:sw t6, 104(ra)<br> [0x800075e4]:sw t5, 112(ra)<br> [0x800075e8]:sw a7, 120(ra)<br>                                      |
| 180|[0x8000c848]<br>0x00000000<br> [0x8000c860]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x47dca9bde3664 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x47dca9bde3664 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007624]:fsub.d t5, t3, s10, dyn<br> [0x80007628]:csrrs a7, fcsr, zero<br> [0x8000762c]:sw t5, 128(ra)<br> [0x80007630]:sw t6, 136(ra)<br> [0x80007634]:sw t5, 144(ra)<br> [0x80007638]:sw a7, 152(ra)<br>                                     |
| 181|[0x8000c868]<br>0x00000000<br> [0x8000c880]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4264cf0154662 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x4264cf0154662 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007674]:fsub.d t5, t3, s10, dyn<br> [0x80007678]:csrrs a7, fcsr, zero<br> [0x8000767c]:sw t5, 160(ra)<br> [0x80007680]:sw t6, 168(ra)<br> [0x80007684]:sw t5, 176(ra)<br> [0x80007688]:sw a7, 184(ra)<br>                                     |
| 182|[0x8000c888]<br>0x00000000<br> [0x8000c8a0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x422ea209fd4bd and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x422ea209fd4bd and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800076c4]:fsub.d t5, t3, s10, dyn<br> [0x800076c8]:csrrs a7, fcsr, zero<br> [0x800076cc]:sw t5, 192(ra)<br> [0x800076d0]:sw t6, 200(ra)<br> [0x800076d4]:sw t5, 208(ra)<br> [0x800076d8]:sw a7, 216(ra)<br>                                     |
| 183|[0x8000c8a8]<br>0x00000000<br> [0x8000c8c0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xdfcdecd96da66 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xdfcdecd96da66 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007714]:fsub.d t5, t3, s10, dyn<br> [0x80007718]:csrrs a7, fcsr, zero<br> [0x8000771c]:sw t5, 224(ra)<br> [0x80007720]:sw t6, 232(ra)<br> [0x80007724]:sw t5, 240(ra)<br> [0x80007728]:sw a7, 248(ra)<br>                                     |
| 184|[0x8000c8c8]<br>0x00000000<br> [0x8000c8e0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb14a0c4b66d3b and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xb14a0c4b66d3b and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007764]:fsub.d t5, t3, s10, dyn<br> [0x80007768]:csrrs a7, fcsr, zero<br> [0x8000776c]:sw t5, 256(ra)<br> [0x80007770]:sw t6, 264(ra)<br> [0x80007774]:sw t5, 272(ra)<br> [0x80007778]:sw a7, 280(ra)<br>                                     |
| 185|[0x8000c8e8]<br>0x00000000<br> [0x8000c900]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa1726431ab40b and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xa1726431ab40b and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800077b4]:fsub.d t5, t3, s10, dyn<br> [0x800077b8]:csrrs a7, fcsr, zero<br> [0x800077bc]:sw t5, 288(ra)<br> [0x800077c0]:sw t6, 296(ra)<br> [0x800077c4]:sw t5, 304(ra)<br> [0x800077c8]:sw a7, 312(ra)<br>                                     |
| 186|[0x8000c908]<br>0x00000000<br> [0x8000c920]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x65d63e74d209d and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x65d63e74d209d and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007804]:fsub.d t5, t3, s10, dyn<br> [0x80007808]:csrrs a7, fcsr, zero<br> [0x8000780c]:sw t5, 320(ra)<br> [0x80007810]:sw t6, 328(ra)<br> [0x80007814]:sw t5, 336(ra)<br> [0x80007818]:sw a7, 344(ra)<br>                                     |
| 187|[0x8000c928]<br>0x00000000<br> [0x8000c940]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x6a8199da501dc and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x6a8199da501dc and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007854]:fsub.d t5, t3, s10, dyn<br> [0x80007858]:csrrs a7, fcsr, zero<br> [0x8000785c]:sw t5, 352(ra)<br> [0x80007860]:sw t6, 360(ra)<br> [0x80007864]:sw t5, 368(ra)<br> [0x80007868]:sw a7, 376(ra)<br>                                     |
| 188|[0x8000c948]<br>0x00000000<br> [0x8000c960]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x16a782d36f4f6 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x16a782d36f4f6 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800078a4]:fsub.d t5, t3, s10, dyn<br> [0x800078a8]:csrrs a7, fcsr, zero<br> [0x800078ac]:sw t5, 384(ra)<br> [0x800078b0]:sw t6, 392(ra)<br> [0x800078b4]:sw t5, 400(ra)<br> [0x800078b8]:sw a7, 408(ra)<br>                                     |
| 189|[0x8000c968]<br>0x00000000<br> [0x8000c980]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xaa7bbc9099344 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xaa7bbc9099344 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800078f4]:fsub.d t5, t3, s10, dyn<br> [0x800078f8]:csrrs a7, fcsr, zero<br> [0x800078fc]:sw t5, 416(ra)<br> [0x80007900]:sw t6, 424(ra)<br> [0x80007904]:sw t5, 432(ra)<br> [0x80007908]:sw a7, 440(ra)<br>                                     |
| 190|[0x8000c988]<br>0x00000000<br> [0x8000c9a0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xde5026c152607 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xde5026c152607 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007944]:fsub.d t5, t3, s10, dyn<br> [0x80007948]:csrrs a7, fcsr, zero<br> [0x8000794c]:sw t5, 448(ra)<br> [0x80007950]:sw t6, 456(ra)<br> [0x80007954]:sw t5, 464(ra)<br> [0x80007958]:sw a7, 472(ra)<br>                                     |
| 191|[0x8000c9a8]<br>0x00000000<br> [0x8000c9c0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x05438a864ff48 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x05438a864ff48 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007994]:fsub.d t5, t3, s10, dyn<br> [0x80007998]:csrrs a7, fcsr, zero<br> [0x8000799c]:sw t5, 480(ra)<br> [0x800079a0]:sw t6, 488(ra)<br> [0x800079a4]:sw t5, 496(ra)<br> [0x800079a8]:sw a7, 504(ra)<br>                                     |
| 192|[0x8000c9c8]<br>0x00000000<br> [0x8000c9e0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xb0bd7b08edb55 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xb0bd7b08edb55 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x800079e4]:fsub.d t5, t3, s10, dyn<br> [0x800079e8]:csrrs a7, fcsr, zero<br> [0x800079ec]:sw t5, 512(ra)<br> [0x800079f0]:sw t6, 520(ra)<br> [0x800079f4]:sw t5, 528(ra)<br> [0x800079f8]:sw a7, 536(ra)<br>                                     |
| 193|[0x8000c9e8]<br>0x00000000<br> [0x8000ca00]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x504dcbdc51a65 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x504dcbdc51a65 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007a34]:fsub.d t5, t3, s10, dyn<br> [0x80007a38]:csrrs a7, fcsr, zero<br> [0x80007a3c]:sw t5, 544(ra)<br> [0x80007a40]:sw t6, 552(ra)<br> [0x80007a44]:sw t5, 560(ra)<br> [0x80007a48]:sw a7, 568(ra)<br>                                     |
| 194|[0x8000ca08]<br>0x00000000<br> [0x8000ca20]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x03aaf26d74a36 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x03aaf26d74a36 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007a84]:fsub.d t5, t3, s10, dyn<br> [0x80007a88]:csrrs a7, fcsr, zero<br> [0x80007a8c]:sw t5, 576(ra)<br> [0x80007a90]:sw t6, 584(ra)<br> [0x80007a94]:sw t5, 592(ra)<br> [0x80007a98]:sw a7, 600(ra)<br>                                     |
| 195|[0x8000ca28]<br>0x00000000<br> [0x8000ca40]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x618258c5f4965 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x618258c5f4965 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007ad4]:fsub.d t5, t3, s10, dyn<br> [0x80007ad8]:csrrs a7, fcsr, zero<br> [0x80007adc]:sw t5, 608(ra)<br> [0x80007ae0]:sw t6, 616(ra)<br> [0x80007ae4]:sw t5, 624(ra)<br> [0x80007ae8]:sw a7, 632(ra)<br>                                     |
| 196|[0x8000ca48]<br>0x00000000<br> [0x8000ca60]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x60b89491a6a27 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x60b89491a6a27 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007b24]:fsub.d t5, t3, s10, dyn<br> [0x80007b28]:csrrs a7, fcsr, zero<br> [0x80007b2c]:sw t5, 640(ra)<br> [0x80007b30]:sw t6, 648(ra)<br> [0x80007b34]:sw t5, 656(ra)<br> [0x80007b38]:sw a7, 664(ra)<br>                                     |
| 197|[0x8000ca68]<br>0x00000000<br> [0x8000ca80]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xabce33873116b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xabce33873116b and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007b74]:fsub.d t5, t3, s10, dyn<br> [0x80007b78]:csrrs a7, fcsr, zero<br> [0x80007b7c]:sw t5, 672(ra)<br> [0x80007b80]:sw t6, 680(ra)<br> [0x80007b84]:sw t5, 688(ra)<br> [0x80007b88]:sw a7, 696(ra)<br>                                     |
| 198|[0x8000ca88]<br>0x00000000<br> [0x8000caa0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc5a51b555f5c9 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc5a51b555f5c9 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007bc4]:fsub.d t5, t3, s10, dyn<br> [0x80007bc8]:csrrs a7, fcsr, zero<br> [0x80007bcc]:sw t5, 704(ra)<br> [0x80007bd0]:sw t6, 712(ra)<br> [0x80007bd4]:sw t5, 720(ra)<br> [0x80007bd8]:sw a7, 728(ra)<br>                                     |
| 199|[0x8000caa8]<br>0x00000000<br> [0x8000cac0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x32ba6165fce3f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x32ba6165fce3f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007c14]:fsub.d t5, t3, s10, dyn<br> [0x80007c18]:csrrs a7, fcsr, zero<br> [0x80007c1c]:sw t5, 736(ra)<br> [0x80007c20]:sw t6, 744(ra)<br> [0x80007c24]:sw t5, 752(ra)<br> [0x80007c28]:sw a7, 760(ra)<br>                                     |
| 200|[0x8000cac8]<br>0x00000000<br> [0x8000cae0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0be093ea29884 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x0be093ea29884 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007c64]:fsub.d t5, t3, s10, dyn<br> [0x80007c68]:csrrs a7, fcsr, zero<br> [0x80007c6c]:sw t5, 768(ra)<br> [0x80007c70]:sw t6, 776(ra)<br> [0x80007c74]:sw t5, 784(ra)<br> [0x80007c78]:sw a7, 792(ra)<br>                                     |
| 201|[0x8000cae8]<br>0x00000000<br> [0x8000cb00]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x05e5cee3b08d7 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x05e5cee3b08d7 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007cb4]:fsub.d t5, t3, s10, dyn<br> [0x80007cb8]:csrrs a7, fcsr, zero<br> [0x80007cbc]:sw t5, 800(ra)<br> [0x80007cc0]:sw t6, 808(ra)<br> [0x80007cc4]:sw t5, 816(ra)<br> [0x80007cc8]:sw a7, 824(ra)<br>                                     |
| 202|[0x8000cb08]<br>0x00000000<br> [0x8000cb20]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x5932a24c0014f and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x5932a24c0014f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007d04]:fsub.d t5, t3, s10, dyn<br> [0x80007d08]:csrrs a7, fcsr, zero<br> [0x80007d0c]:sw t5, 832(ra)<br> [0x80007d10]:sw t6, 840(ra)<br> [0x80007d14]:sw t5, 848(ra)<br> [0x80007d18]:sw a7, 856(ra)<br>                                     |
| 203|[0x8000cb28]<br>0x00000000<br> [0x8000cb40]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x6d479d3fc4771 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x6d479d3fc4771 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007d54]:fsub.d t5, t3, s10, dyn<br> [0x80007d58]:csrrs a7, fcsr, zero<br> [0x80007d5c]:sw t5, 864(ra)<br> [0x80007d60]:sw t6, 872(ra)<br> [0x80007d64]:sw t5, 880(ra)<br> [0x80007d68]:sw a7, 888(ra)<br>                                     |
| 204|[0x8000cb48]<br>0x00000000<br> [0x8000cb60]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x143c21ad8c8b5 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x143c21ad8c8b5 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007da4]:fsub.d t5, t3, s10, dyn<br> [0x80007da8]:csrrs a7, fcsr, zero<br> [0x80007dac]:sw t5, 896(ra)<br> [0x80007db0]:sw t6, 904(ra)<br> [0x80007db4]:sw t5, 912(ra)<br> [0x80007db8]:sw a7, 920(ra)<br>                                     |
| 205|[0x8000cb68]<br>0x00000000<br> [0x8000cb80]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdb5e85647ec13 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xdb5e85647ec13 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007df4]:fsub.d t5, t3, s10, dyn<br> [0x80007df8]:csrrs a7, fcsr, zero<br> [0x80007dfc]:sw t5, 928(ra)<br> [0x80007e00]:sw t6, 936(ra)<br> [0x80007e04]:sw t5, 944(ra)<br> [0x80007e08]:sw a7, 952(ra)<br>                                     |
| 206|[0x8000cb88]<br>0x00000000<br> [0x8000cba0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x60ffd67bcec83 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x60ffd67bcec83 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007e44]:fsub.d t5, t3, s10, dyn<br> [0x80007e48]:csrrs a7, fcsr, zero<br> [0x80007e4c]:sw t5, 960(ra)<br> [0x80007e50]:sw t6, 968(ra)<br> [0x80007e54]:sw t5, 976(ra)<br> [0x80007e58]:sw a7, 984(ra)<br>                                     |
| 207|[0x8000cba8]<br>0x00000000<br> [0x8000cbc0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbbc271a710d1b and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xbbc271a710d1b and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007e94]:fsub.d t5, t3, s10, dyn<br> [0x80007e98]:csrrs a7, fcsr, zero<br> [0x80007e9c]:sw t5, 992(ra)<br> [0x80007ea0]:sw t6, 1000(ra)<br> [0x80007ea4]:sw t5, 1008(ra)<br> [0x80007ea8]:sw a7, 1016(ra)<br>                                  |
| 208|[0x8000cbc8]<br>0x00000000<br> [0x8000cbe0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc74ef4423e96b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xc74ef4423e96b and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007ee4]:fsub.d t5, t3, s10, dyn<br> [0x80007ee8]:csrrs a7, fcsr, zero<br> [0x80007eec]:sw t5, 1024(ra)<br> [0x80007ef0]:sw t6, 1032(ra)<br> [0x80007ef4]:sw t5, 1040(ra)<br> [0x80007ef8]:sw a7, 1048(ra)<br>                                 |
| 209|[0x8000cbe8]<br>0x00000000<br> [0x8000cc00]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x69d3500fa16c1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x69d3500fa16c1 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007f34]:fsub.d t5, t3, s10, dyn<br> [0x80007f38]:csrrs a7, fcsr, zero<br> [0x80007f3c]:sw t5, 1056(ra)<br> [0x80007f40]:sw t6, 1064(ra)<br> [0x80007f44]:sw t5, 1072(ra)<br> [0x80007f48]:sw a7, 1080(ra)<br>                                 |
| 210|[0x8000cc08]<br>0x00000000<br> [0x8000cc20]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xf446ded06de1f and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xf446ded06de1f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007f84]:fsub.d t5, t3, s10, dyn<br> [0x80007f88]:csrrs a7, fcsr, zero<br> [0x80007f8c]:sw t5, 1088(ra)<br> [0x80007f90]:sw t6, 1096(ra)<br> [0x80007f94]:sw t5, 1104(ra)<br> [0x80007f98]:sw a7, 1112(ra)<br>                                 |
| 211|[0x8000cc28]<br>0x00000000<br> [0x8000cc40]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3eb20959c42c2 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x3eb20959c42c2 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                               |[0x80007fd4]:fsub.d t5, t3, s10, dyn<br> [0x80007fd8]:csrrs a7, fcsr, zero<br> [0x80007fdc]:sw t5, 1120(ra)<br> [0x80007fe0]:sw t6, 1128(ra)<br> [0x80007fe4]:sw t5, 1136(ra)<br> [0x80007fe8]:sw a7, 1144(ra)<br>                                 |
