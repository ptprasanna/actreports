
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80001170')]      |
| SIG_REGION                | [('0x80003510', '0x80003860', '212 words')]      |
| COV_LABELS                | fsub.d_b12      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-fsub/fsub.d_b12-01.S/ref.S    |
| Total Number of coverpoints| 101     |
| Total Coverpoints Hit     | 101      |
| Total Signature Updates   | 128      |
| STAT1                     | 32      |
| STAT2                     | 0      |
| STAT3                     | 20     |
| STAT4                     | 64     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000b5c]:fsub.d t5, t3, s10, dyn
[0x80000b60]:csrrs a7, fcsr, zero
[0x80000b64]:sw t5, 672(ra)
[0x80000b68]:sw t6, 680(ra)
[0x80000b6c]:sw t5, 688(ra)
[0x80000b70]:sw a7, 696(ra)
[0x80000b74]:lw t3, 352(a6)
[0x80000b78]:lw t4, 356(a6)
[0x80000b7c]:lw s10, 360(a6)
[0x80000b80]:lw s11, 364(a6)
[0x80000b84]:lui t3, 657128
[0x80000b88]:addi t3, t3, 1415
[0x80000b8c]:lui t4, 523780
[0x80000b90]:addi t4, t4, 1287
[0x80000b94]:lui s10, 755491
[0x80000b98]:addi s10, s10, 1532
[0x80000b9c]:lui s11, 1047828
[0x80000ba0]:addi s11, s11, 2033
[0x80000ba4]:addi a4, zero, 0
[0x80000ba8]:csrrw zero, fcsr, a4
[0x80000bac]:fsub.d t5, t3, s10, dyn
[0x80000bb0]:csrrs a7, fcsr, zero

[0x80000bac]:fsub.d t5, t3, s10, dyn
[0x80000bb0]:csrrs a7, fcsr, zero
[0x80000bb4]:sw t5, 704(ra)
[0x80000bb8]:sw t6, 712(ra)
[0x80000bbc]:sw t5, 720(ra)
[0x80000bc0]:sw a7, 728(ra)
[0x80000bc4]:lw t3, 368(a6)
[0x80000bc8]:lw t4, 372(a6)
[0x80000bcc]:lw s10, 376(a6)
[0x80000bd0]:lw s11, 380(a6)
[0x80000bd4]:lui t3, 46530
[0x80000bd8]:addi t3, t3, 3136
[0x80000bdc]:lui t4, 523787
[0x80000be0]:addi t4, t4, 2951
[0x80000be4]:lui s10, 358444
[0x80000be8]:addi s10, s10, 2006
[0x80000bec]:lui s11, 523269
[0x80000bf0]:addi s11, s11, 2029
[0x80000bf4]:addi a4, zero, 0
[0x80000bf8]:csrrw zero, fcsr, a4
[0x80000bfc]:fsub.d t5, t3, s10, dyn
[0x80000c00]:csrrs a7, fcsr, zero

[0x80000bfc]:fsub.d t5, t3, s10, dyn
[0x80000c00]:csrrs a7, fcsr, zero
[0x80000c04]:sw t5, 736(ra)
[0x80000c08]:sw t6, 744(ra)
[0x80000c0c]:sw t5, 752(ra)
[0x80000c10]:sw a7, 760(ra)
[0x80000c14]:lw t3, 384(a6)
[0x80000c18]:lw t4, 388(a6)
[0x80000c1c]:lw s10, 392(a6)
[0x80000c20]:lw s11, 396(a6)
[0x80000c24]:lui t3, 515531
[0x80000c28]:addi t3, t3, 1105
[0x80000c2c]:lui t4, 523765
[0x80000c30]:addi t4, t4, 1827
[0x80000c34]:lui s10, 106937
[0x80000c38]:addi s10, s10, 2407
[0x80000c3c]:lui s11, 523735
[0x80000c40]:addi s11, s11, 661
[0x80000c44]:addi a4, zero, 0
[0x80000c48]:csrrw zero, fcsr, a4
[0x80000c4c]:fsub.d t5, t3, s10, dyn
[0x80000c50]:csrrs a7, fcsr, zero

[0x80000c4c]:fsub.d t5, t3, s10, dyn
[0x80000c50]:csrrs a7, fcsr, zero
[0x80000c54]:sw t5, 768(ra)
[0x80000c58]:sw t6, 776(ra)
[0x80000c5c]:sw t5, 784(ra)
[0x80000c60]:sw a7, 792(ra)
[0x80000c64]:lw t3, 400(a6)
[0x80000c68]:lw t4, 404(a6)
[0x80000c6c]:lw s10, 408(a6)
[0x80000c70]:lw s11, 412(a6)
[0x80000c74]:lui t3, 343023
[0x80000c78]:addi t3, t3, 559
[0x80000c7c]:lui t4, 522852
[0x80000c80]:addi t4, t4, 1015
[0x80000c84]:lui s10, 729740
[0x80000c88]:addi s10, s10, 2566
[0x80000c8c]:lui s11, 1048146
[0x80000c90]:addi s11, s11, 3547
[0x80000c94]:addi a4, zero, 0
[0x80000c98]:csrrw zero, fcsr, a4
[0x80000c9c]:fsub.d t5, t3, s10, dyn
[0x80000ca0]:csrrs a7, fcsr, zero

[0x80000c9c]:fsub.d t5, t3, s10, dyn
[0x80000ca0]:csrrs a7, fcsr, zero
[0x80000ca4]:sw t5, 800(ra)
[0x80000ca8]:sw t6, 808(ra)
[0x80000cac]:sw t5, 816(ra)
[0x80000cb0]:sw a7, 824(ra)
[0x80000cb4]:lw t3, 416(a6)
[0x80000cb8]:lw t4, 420(a6)
[0x80000cbc]:lw s10, 424(a6)
[0x80000cc0]:lw s11, 428(a6)
[0x80000cc4]:lui t3, 91993
[0x80000cc8]:addi t3, t3, 239
[0x80000ccc]:lui t4, 524023
[0x80000cd0]:addi t4, t4, 1606
[0x80000cd4]:lui s10, 296112
[0x80000cd8]:addi s10, s10, 1840
[0x80000cdc]:lui s11, 523663
[0x80000ce0]:addi s11, s11, 1491
[0x80000ce4]:addi a4, zero, 0
[0x80000ce8]:csrrw zero, fcsr, a4
[0x80000cec]:fsub.d t5, t3, s10, dyn
[0x80000cf0]:csrrs a7, fcsr, zero

[0x80000cec]:fsub.d t5, t3, s10, dyn
[0x80000cf0]:csrrs a7, fcsr, zero
[0x80000cf4]:sw t5, 832(ra)
[0x80000cf8]:sw t6, 840(ra)
[0x80000cfc]:sw t5, 848(ra)
[0x80000d00]:sw a7, 856(ra)
[0x80000d04]:lw t3, 432(a6)
[0x80000d08]:lw t4, 436(a6)
[0x80000d0c]:lw s10, 440(a6)
[0x80000d10]:lw s11, 444(a6)
[0x80000d14]:lui t3, 774870
[0x80000d18]:addi t3, t3, 53
[0x80000d1c]:lui t4, 523667
[0x80000d20]:addi t4, t4, 188
[0x80000d24]:lui s10, 883331
[0x80000d28]:addi s10, s10, 127
[0x80000d2c]:lui s11, 523465
[0x80000d30]:addi s11, s11, 888
[0x80000d34]:addi a4, zero, 0
[0x80000d38]:csrrw zero, fcsr, a4
[0x80000d3c]:fsub.d t5, t3, s10, dyn
[0x80000d40]:csrrs a7, fcsr, zero

[0x80000d3c]:fsub.d t5, t3, s10, dyn
[0x80000d40]:csrrs a7, fcsr, zero
[0x80000d44]:sw t5, 864(ra)
[0x80000d48]:sw t6, 872(ra)
[0x80000d4c]:sw t5, 880(ra)
[0x80000d50]:sw a7, 888(ra)
[0x80000d54]:lw t3, 448(a6)
[0x80000d58]:lw t4, 452(a6)
[0x80000d5c]:lw s10, 456(a6)
[0x80000d60]:lw s11, 460(a6)
[0x80000d64]:lui t3, 626310
[0x80000d68]:addi t3, t3, 578
[0x80000d6c]:lui t4, 523777
[0x80000d70]:addi t4, t4, 598
[0x80000d74]:lui s10, 812167
[0x80000d78]:addi s10, s10, 3340
[0x80000d7c]:lui s11, 1048043
[0x80000d80]:addi s11, s11, 1450
[0x80000d84]:addi a4, zero, 0
[0x80000d88]:csrrw zero, fcsr, a4
[0x80000d8c]:fsub.d t5, t3, s10, dyn
[0x80000d90]:csrrs a7, fcsr, zero

[0x80000d8c]:fsub.d t5, t3, s10, dyn
[0x80000d90]:csrrs a7, fcsr, zero
[0x80000d94]:sw t5, 896(ra)
[0x80000d98]:sw t6, 904(ra)
[0x80000d9c]:sw t5, 912(ra)
[0x80000da0]:sw a7, 920(ra)
[0x80000da4]:lw t3, 464(a6)
[0x80000da8]:lw t4, 468(a6)
[0x80000dac]:lw s10, 472(a6)
[0x80000db0]:lw s11, 476(a6)
[0x80000db4]:lui t3, 150725
[0x80000db8]:addi t3, t3, 3587
[0x80000dbc]:lui t4, 523915
[0x80000dc0]:addi t4, t4, 2080
[0x80000dc4]:lui s10, 345993
[0x80000dc8]:addi s10, s10, 2948
[0x80000dcc]:lui s11, 523481
[0x80000dd0]:addi s11, s11, 3665
[0x80000dd4]:addi a4, zero, 0
[0x80000dd8]:csrrw zero, fcsr, a4
[0x80000ddc]:fsub.d t5, t3, s10, dyn
[0x80000de0]:csrrs a7, fcsr, zero

[0x80000ddc]:fsub.d t5, t3, s10, dyn
[0x80000de0]:csrrs a7, fcsr, zero
[0x80000de4]:sw t5, 928(ra)
[0x80000de8]:sw t6, 936(ra)
[0x80000dec]:sw t5, 944(ra)
[0x80000df0]:sw a7, 952(ra)
[0x80000df4]:lw t3, 480(a6)
[0x80000df8]:lw t4, 484(a6)
[0x80000dfc]:lw s10, 488(a6)
[0x80000e00]:lw s11, 492(a6)
[0x80000e04]:lui t3, 451352
[0x80000e08]:addi t3, t3, 4002
[0x80000e0c]:lui t4, 523960
[0x80000e10]:addi t4, t4, 1918
[0x80000e14]:lui s10, 837941
[0x80000e18]:addi s10, s10, 1592
[0x80000e1c]:lui s11, 523842
[0x80000e20]:addi s11, s11, 2433
[0x80000e24]:addi a4, zero, 0
[0x80000e28]:csrrw zero, fcsr, a4
[0x80000e2c]:fsub.d t5, t3, s10, dyn
[0x80000e30]:csrrs a7, fcsr, zero

[0x80000e2c]:fsub.d t5, t3, s10, dyn
[0x80000e30]:csrrs a7, fcsr, zero
[0x80000e34]:sw t5, 960(ra)
[0x80000e38]:sw t6, 968(ra)
[0x80000e3c]:sw t5, 976(ra)
[0x80000e40]:sw a7, 984(ra)
[0x80000e44]:lw t3, 496(a6)
[0x80000e48]:lw t4, 500(a6)
[0x80000e4c]:lw s10, 504(a6)
[0x80000e50]:lw s11, 508(a6)
[0x80000e54]:lui t3, 284161
[0x80000e58]:addi t3, t3, 995
[0x80000e5c]:lui t4, 523783
[0x80000e60]:addi t4, t4, 209
[0x80000e64]:lui s10, 245879
[0x80000e68]:addi s10, s10, 472
[0x80000e6c]:lui s11, 1048001
[0x80000e70]:addi s11, s11, 919
[0x80000e74]:addi a4, zero, 0
[0x80000e78]:csrrw zero, fcsr, a4
[0x80000e7c]:fsub.d t5, t3, s10, dyn
[0x80000e80]:csrrs a7, fcsr, zero

[0x80000e7c]:fsub.d t5, t3, s10, dyn
[0x80000e80]:csrrs a7, fcsr, zero
[0x80000e84]:sw t5, 992(ra)
[0x80000e88]:sw t6, 1000(ra)
[0x80000e8c]:sw t5, 1008(ra)
[0x80000e90]:sw a7, 1016(ra)
[0x80000e94]:lw t3, 512(a6)
[0x80000e98]:lw t4, 516(a6)
[0x80000e9c]:lw s10, 520(a6)
[0x80000ea0]:lw s11, 524(a6)
[0x80000ea4]:lui t3, 562844
[0x80000ea8]:addi t3, t3, 178
[0x80000eac]:lui t4, 523816
[0x80000eb0]:addi t4, t4, 3403
[0x80000eb4]:lui s10, 58133
[0x80000eb8]:addi s10, s10, 3852
[0x80000ebc]:lui s11, 523238
[0x80000ec0]:addi s11, s11, 180
[0x80000ec4]:addi a4, zero, 0
[0x80000ec8]:csrrw zero, fcsr, a4
[0x80000ecc]:fsub.d t5, t3, s10, dyn
[0x80000ed0]:csrrs a7, fcsr, zero

[0x80000ecc]:fsub.d t5, t3, s10, dyn
[0x80000ed0]:csrrs a7, fcsr, zero
[0x80000ed4]:sw t5, 1024(ra)
[0x80000ed8]:sw t6, 1032(ra)
[0x80000edc]:sw t5, 1040(ra)
[0x80000ee0]:sw a7, 1048(ra)
[0x80000ee4]:lw t3, 528(a6)
[0x80000ee8]:lw t4, 532(a6)
[0x80000eec]:lw s10, 536(a6)
[0x80000ef0]:lw s11, 540(a6)
[0x80000ef4]:lui t3, 1037409
[0x80000ef8]:addi t3, t3, 617
[0x80000efc]:lui t4, 523540
[0x80000f00]:addi t4, t4, 3039
[0x80000f04]:lui s10, 129425
[0x80000f08]:addi s10, s10, 3083
[0x80000f0c]:lui s11, 1047837
[0x80000f10]:addi s11, s11, 1663
[0x80000f14]:addi a4, zero, 0
[0x80000f18]:csrrw zero, fcsr, a4
[0x80000f1c]:fsub.d t5, t3, s10, dyn
[0x80000f20]:csrrs a7, fcsr, zero

[0x80000f1c]:fsub.d t5, t3, s10, dyn
[0x80000f20]:csrrs a7, fcsr, zero
[0x80000f24]:sw t5, 1056(ra)
[0x80000f28]:sw t6, 1064(ra)
[0x80000f2c]:sw t5, 1072(ra)
[0x80000f30]:sw a7, 1080(ra)
[0x80000f34]:lw t3, 544(a6)
[0x80000f38]:lw t4, 548(a6)
[0x80000f3c]:lw s10, 552(a6)
[0x80000f40]:lw s11, 556(a6)
[0x80000f44]:lui t3, 339630
[0x80000f48]:addi t3, t3, 2016
[0x80000f4c]:lui t4, 524010
[0x80000f50]:addi t4, t4, 178
[0x80000f54]:lui s10, 431648
[0x80000f58]:addi s10, s10, 497
[0x80000f5c]:lui s11, 524007
[0x80000f60]:addi s11, s11, 493
[0x80000f64]:addi a4, zero, 0
[0x80000f68]:csrrw zero, fcsr, a4
[0x80000f6c]:fsub.d t5, t3, s10, dyn
[0x80000f70]:csrrs a7, fcsr, zero

[0x80000f6c]:fsub.d t5, t3, s10, dyn
[0x80000f70]:csrrs a7, fcsr, zero
[0x80000f74]:sw t5, 1088(ra)
[0x80000f78]:sw t6, 1096(ra)
[0x80000f7c]:sw t5, 1104(ra)
[0x80000f80]:sw a7, 1112(ra)
[0x80000f84]:lw t3, 560(a6)
[0x80000f88]:lw t4, 564(a6)
[0x80000f8c]:lw s10, 568(a6)
[0x80000f90]:lw s11, 572(a6)
[0x80000f94]:lui t3, 145112
[0x80000f98]:addi t3, t3, 1041
[0x80000f9c]:lui t4, 523921
[0x80000fa0]:addi t4, t4, 948
[0x80000fa4]:lui s10, 549899
[0x80000fa8]:addi s10, s10, 2976
[0x80000fac]:lui s11, 1047086
[0x80000fb0]:addi s11, s11, 2936
[0x80000fb4]:addi a4, zero, 0
[0x80000fb8]:csrrw zero, fcsr, a4
[0x80000fbc]:fsub.d t5, t3, s10, dyn
[0x80000fc0]:csrrs a7, fcsr, zero

[0x80000fbc]:fsub.d t5, t3, s10, dyn
[0x80000fc0]:csrrs a7, fcsr, zero
[0x80000fc4]:sw t5, 1120(ra)
[0x80000fc8]:sw t6, 1128(ra)
[0x80000fcc]:sw t5, 1136(ra)
[0x80000fd0]:sw a7, 1144(ra)
[0x80000fd4]:lw t3, 576(a6)
[0x80000fd8]:lw t4, 580(a6)
[0x80000fdc]:lw s10, 584(a6)
[0x80000fe0]:lw s11, 588(a6)
[0x80000fe4]:lui t3, 293904
[0x80000fe8]:addi t3, t3, 2929
[0x80000fec]:lui t4, 523974
[0x80000ff0]:addi t4, t4, 2965
[0x80000ff4]:lui s10, 290060
[0x80000ff8]:addi s10, s10, 2728
[0x80000ffc]:lui s11, 523306
[0x80001000]:addi s11, s11, 1412
[0x80001004]:addi a4, zero, 0
[0x80001008]:csrrw zero, fcsr, a4
[0x8000100c]:fsub.d t5, t3, s10, dyn
[0x80001010]:csrrs a7, fcsr, zero

[0x8000100c]:fsub.d t5, t3, s10, dyn
[0x80001010]:csrrs a7, fcsr, zero
[0x80001014]:sw t5, 1152(ra)
[0x80001018]:sw t6, 1160(ra)
[0x8000101c]:sw t5, 1168(ra)
[0x80001020]:sw a7, 1176(ra)
[0x80001024]:lw t3, 592(a6)
[0x80001028]:lw t4, 596(a6)
[0x8000102c]:lw s10, 600(a6)
[0x80001030]:lw s11, 604(a6)
[0x80001034]:lui t3, 378950
[0x80001038]:addi t3, t3, 2549
[0x8000103c]:lui t4, 523934
[0x80001040]:addi t4, t4, 1145
[0x80001044]:lui s10, 339209
[0x80001048]:addi s10, s10, 2026
[0x8000104c]:lui s11, 523561
[0x80001050]:addi s11, s11, 1772
[0x80001054]:addi a4, zero, 0
[0x80001058]:csrrw zero, fcsr, a4
[0x8000105c]:fsub.d t5, t3, s10, dyn
[0x80001060]:csrrs a7, fcsr, zero

[0x8000105c]:fsub.d t5, t3, s10, dyn
[0x80001060]:csrrs a7, fcsr, zero
[0x80001064]:sw t5, 1184(ra)
[0x80001068]:sw t6, 1192(ra)
[0x8000106c]:sw t5, 1200(ra)
[0x80001070]:sw a7, 1208(ra)
[0x80001074]:lw t3, 608(a6)
[0x80001078]:lw t4, 612(a6)
[0x8000107c]:lw s10, 616(a6)
[0x80001080]:lw s11, 620(a6)
[0x80001084]:lui t3, 806183
[0x80001088]:addi t3, t3, 2745
[0x8000108c]:lui t4, 523807
[0x80001090]:addi t4, t4, 1700
[0x80001094]:lui s10, 409978
[0x80001098]:addi s10, s10, 3602
[0x8000109c]:lui s11, 523285
[0x800010a0]:addi s11, s11, 111
[0x800010a4]:addi a4, zero, 0
[0x800010a8]:csrrw zero, fcsr, a4
[0x800010ac]:fsub.d t5, t3, s10, dyn
[0x800010b0]:csrrs a7, fcsr, zero

[0x800010ac]:fsub.d t5, t3, s10, dyn
[0x800010b0]:csrrs a7, fcsr, zero
[0x800010b4]:sw t5, 1216(ra)
[0x800010b8]:sw t6, 1224(ra)
[0x800010bc]:sw t5, 1232(ra)
[0x800010c0]:sw a7, 1240(ra)
[0x800010c4]:lw t3, 624(a6)
[0x800010c8]:lw t4, 628(a6)
[0x800010cc]:lw s10, 632(a6)
[0x800010d0]:lw s11, 636(a6)
[0x800010d4]:lui t3, 1020343
[0x800010d8]:addi t3, t3, 1411
[0x800010dc]:lui t4, 523283
[0x800010e0]:addi t4, t4, 728
[0x800010e4]:lui s10, 118506
[0x800010e8]:addi s10, s10, 1942
[0x800010ec]:lui s11, 1048173
[0x800010f0]:addi s11, s11, 375
[0x800010f4]:addi a4, zero, 0
[0x800010f8]:csrrw zero, fcsr, a4
[0x800010fc]:fsub.d t5, t3, s10, dyn
[0x80001100]:csrrs a7, fcsr, zero

[0x800010fc]:fsub.d t5, t3, s10, dyn
[0x80001100]:csrrs a7, fcsr, zero
[0x80001104]:sw t5, 1248(ra)
[0x80001108]:sw t6, 1256(ra)
[0x8000110c]:sw t5, 1264(ra)
[0x80001110]:sw a7, 1272(ra)
[0x80001114]:lw t3, 640(a6)
[0x80001118]:lw t4, 644(a6)
[0x8000111c]:lw s10, 648(a6)
[0x80001120]:lw s11, 652(a6)
[0x80001124]:lui t3, 588232
[0x80001128]:addi t3, t3, 1423
[0x8000112c]:lui t4, 523136
[0x80001130]:addi t4, t4, 1838
[0x80001134]:lui s10, 864359
[0x80001138]:addi s10, s10, 2039
[0x8000113c]:lui s11, 523020
[0x80001140]:addi s11, s11, 1382
[0x80001144]:addi a4, zero, 0
[0x80001148]:csrrw zero, fcsr, a4
[0x8000114c]:fsub.d t5, t3, s10, dyn
[0x80001150]:csrrs a7, fcsr, zero

[0x8000114c]:fsub.d t5, t3, s10, dyn
[0x80001150]:csrrs a7, fcsr, zero
[0x80001154]:sw t5, 1280(ra)
[0x80001158]:sw t6, 1288(ra)
[0x8000115c]:sw t5, 1296(ra)
[0x80001160]:sw a7, 1304(ra)
[0x80001164]:addi zero, zero, 0
[0x80001168]:addi zero, zero, 0
[0x8000116c]:addi zero, zero, 0



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fsub.d', 'rs1 : x30', 'rs2 : x30', 'rd : x30', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x8000014c]:fsub.d t5, t5, t5, dyn
	-[0x80000150]:csrrs tp, fcsr, zero
	-[0x80000154]:sw t5, 0(ra)
	-[0x80000158]:sw t6, 8(ra)
Current Store : [0x80000158] : sw t6, 8(ra) -- Store: [0x80003520]:0x7FC132D8




Last Coverpoint : ['mnemonic : fsub.d', 'rs1 : x30', 'rs2 : x30', 'rd : x30', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x8000014c]:fsub.d t5, t5, t5, dyn
	-[0x80000150]:csrrs tp, fcsr, zero
	-[0x80000154]:sw t5, 0(ra)
	-[0x80000158]:sw t6, 8(ra)
	-[0x8000015c]:sw t5, 16(ra)
Current Store : [0x8000015c] : sw t5, 16(ra) -- Store: [0x80003528]:0x00000000




Last Coverpoint : ['rs1 : x26', 'rs2 : x24', 'rd : x28', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0x870d778409f12 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x04750f3c7df65 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000019c]:fsub.d t3, s10, s8, dyn
	-[0x800001a0]:csrrs tp, fcsr, zero
	-[0x800001a4]:sw t3, 32(ra)
	-[0x800001a8]:sw t4, 40(ra)
Current Store : [0x800001a8] : sw t4, 40(ra) -- Store: [0x80003540]:0xEEDBEADF




Last Coverpoint : ['rs1 : x26', 'rs2 : x24', 'rd : x28', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0x870d778409f12 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x04750f3c7df65 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000019c]:fsub.d t3, s10, s8, dyn
	-[0x800001a0]:csrrs tp, fcsr, zero
	-[0x800001a4]:sw t3, 32(ra)
	-[0x800001a8]:sw t4, 40(ra)
	-[0x800001ac]:sw t3, 48(ra)
Current Store : [0x800001ac] : sw t3, 48(ra) -- Store: [0x80003548]:0x08F17F5A




Last Coverpoint : ['rs1 : x24', 'rs2 : x28', 'rd : x24', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xfb5355e167379 and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x785f9927a57c0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001ec]:fsub.d s8, s8, t3, dyn
	-[0x800001f0]:csrrs tp, fcsr, zero
	-[0x800001f4]:sw s8, 64(ra)
	-[0x800001f8]:sw s9, 72(ra)
Current Store : [0x800001f8] : sw s9, 72(ra) -- Store: [0x80003560]:0x7FDFB535




Last Coverpoint : ['rs1 : x24', 'rs2 : x28', 'rd : x24', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xfb5355e167379 and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x785f9927a57c0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001ec]:fsub.d s8, s8, t3, dyn
	-[0x800001f0]:csrrs tp, fcsr, zero
	-[0x800001f4]:sw s8, 64(ra)
	-[0x800001f8]:sw s9, 72(ra)
	-[0x800001fc]:sw s8, 80(ra)
Current Store : [0x800001fc] : sw s8, 80(ra) -- Store: [0x80003568]:0x2BC72881




Last Coverpoint : ['rs1 : x28', 'rs2 : x26', 'rd : x26', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4d9d98184b9d9 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x18d7cfd491228 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000023c]:fsub.d s10, t3, s10, dyn
	-[0x80000240]:csrrs tp, fcsr, zero
	-[0x80000244]:sw s10, 96(ra)
	-[0x80000248]:sw s11, 104(ra)
Current Store : [0x80000248] : sw s11, 104(ra) -- Store: [0x80003580]:0xFFC18D7C




Last Coverpoint : ['rs1 : x28', 'rs2 : x26', 'rd : x26', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4d9d98184b9d9 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x18d7cfd491228 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000023c]:fsub.d s10, t3, s10, dyn
	-[0x80000240]:csrrs tp, fcsr, zero
	-[0x80000244]:sw s10, 96(ra)
	-[0x80000248]:sw s11, 104(ra)
	-[0x8000024c]:sw s10, 112(ra)
Current Store : [0x8000024c] : sw s10, 112(ra) -- Store: [0x80003588]:0xC0D6FE63




Last Coverpoint : ['rs1 : x20', 'rs2 : x20', 'rd : x22', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x8000028c]:fsub.d s6, s4, s4, dyn
	-[0x80000290]:csrrs tp, fcsr, zero
	-[0x80000294]:sw s6, 128(ra)
	-[0x80000298]:sw s7, 136(ra)
Current Store : [0x80000298] : sw s7, 136(ra) -- Store: [0x800035a0]:0xB6FAB7FB




Last Coverpoint : ['rs1 : x20', 'rs2 : x20', 'rd : x22', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x8000028c]:fsub.d s6, s4, s4, dyn
	-[0x80000290]:csrrs tp, fcsr, zero
	-[0x80000294]:sw s6, 128(ra)
	-[0x80000298]:sw s7, 136(ra)
	-[0x8000029c]:sw s6, 144(ra)
Current Store : [0x8000029c] : sw s6, 144(ra) -- Store: [0x800035a8]:0x00000000




Last Coverpoint : ['rs1 : x22', 'rs2 : x18', 'rd : x20', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xabe96758f2a09 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x9cab846424ba1 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002dc]:fsub.d s4, s6, s2, dyn
	-[0x800002e0]:csrrs tp, fcsr, zero
	-[0x800002e4]:sw s4, 160(ra)
	-[0x800002e8]:sw s5, 168(ra)
Current Store : [0x800002e8] : sw s5, 168(ra) -- Store: [0x800035c0]:0x7FB8072E




Last Coverpoint : ['rs1 : x22', 'rs2 : x18', 'rd : x20', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xabe96758f2a09 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x9cab846424ba1 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002dc]:fsub.d s4, s6, s2, dyn
	-[0x800002e0]:csrrs tp, fcsr, zero
	-[0x800002e4]:sw s4, 160(ra)
	-[0x800002e8]:sw s5, 168(ra)
	-[0x800002ec]:sw s4, 176(ra)
Current Store : [0x800002ec] : sw s4, 176(ra) -- Store: [0x800035c8]:0xA4DC0871




Last Coverpoint : ['rs1 : x16', 'rs2 : x22', 'rd : x18', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0x86499331191c4 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x853587c49095b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000032c]:fsub.d s2, a6, s6, dyn
	-[0x80000330]:csrrs tp, fcsr, zero
	-[0x80000334]:sw s2, 192(ra)
	-[0x80000338]:sw s3, 200(ra)
Current Store : [0x80000338] : sw s3, 200(ra) -- Store: [0x800035e0]:0x7FD9CAB8




Last Coverpoint : ['rs1 : x16', 'rs2 : x22', 'rd : x18', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0x86499331191c4 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x853587c49095b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000032c]:fsub.d s2, a6, s6, dyn
	-[0x80000330]:csrrs tp, fcsr, zero
	-[0x80000334]:sw s2, 192(ra)
	-[0x80000338]:sw s3, 200(ra)
	-[0x8000033c]:sw s2, 208(ra)
Current Store : [0x8000033c] : sw s2, 208(ra) -- Store: [0x800035e8]:0xC8886900




Last Coverpoint : ['rs1 : x18', 'rs2 : x14', 'rd : x16', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc81394a2171e9 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x1ac7cf448b205 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000037c]:fsub.d a6, s2, a4, dyn
	-[0x80000380]:csrrs tp, fcsr, zero
	-[0x80000384]:sw a6, 224(ra)
	-[0x80000388]:sw a7, 232(ra)
Current Store : [0x80000388] : sw a7, 232(ra) -- Store: [0x80003600]:0x7FE86499




Last Coverpoint : ['rs1 : x18', 'rs2 : x14', 'rd : x16', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc81394a2171e9 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x1ac7cf448b205 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000037c]:fsub.d a6, s2, a4, dyn
	-[0x80000380]:csrrs tp, fcsr, zero
	-[0x80000384]:sw a6, 224(ra)
	-[0x80000388]:sw a7, 232(ra)
	-[0x8000038c]:sw a6, 240(ra)
Current Store : [0x8000038c] : sw a6, 240(ra) -- Store: [0x80003608]:0x1F3511F7




Last Coverpoint : ['rs1 : x12', 'rs2 : x16', 'rd : x14', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0xd481499755d4b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x6eda32e0b56e8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003cc]:fsub.d a4, a2, a6, dyn
	-[0x800003d0]:csrrs tp, fcsr, zero
	-[0x800003d4]:sw a4, 256(ra)
	-[0x800003d8]:sw a5, 264(ra)
Current Store : [0x800003d8] : sw a5, 264(ra) -- Store: [0x80003620]:0xFFD1AC7C




Last Coverpoint : ['rs1 : x12', 'rs2 : x16', 'rd : x14', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0xd481499755d4b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x6eda32e0b56e8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003cc]:fsub.d a4, a2, a6, dyn
	-[0x800003d0]:csrrs tp, fcsr, zero
	-[0x800003d4]:sw a4, 256(ra)
	-[0x800003d8]:sw a5, 264(ra)
	-[0x800003dc]:sw a4, 272(ra)
Current Store : [0x800003dc] : sw a4, 272(ra) -- Store: [0x80003628]:0x5468AE3B




Last Coverpoint : ['rs1 : x14', 'rs2 : x10', 'rd : x12', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd87e65450c45 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xbdde68d2e30aa and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000424]:fsub.d a2, a4, a0, dyn
	-[0x80000428]:csrrs a7, fcsr, zero
	-[0x8000042c]:sw a2, 288(ra)
	-[0x80000430]:sw a3, 296(ra)
Current Store : [0x80000430] : sw a3, 296(ra) -- Store: [0x80003640]:0x7FCD4814




Last Coverpoint : ['rs1 : x14', 'rs2 : x10', 'rd : x12', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd87e65450c45 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xbdde68d2e30aa and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000424]:fsub.d a2, a4, a0, dyn
	-[0x80000428]:csrrs a7, fcsr, zero
	-[0x8000042c]:sw a2, 288(ra)
	-[0x80000430]:sw a3, 296(ra)
	-[0x80000434]:sw a2, 304(ra)
Current Store : [0x80000434] : sw a2, 304(ra) -- Store: [0x80003648]:0x02DB7360




Last Coverpoint : ['rs1 : x8', 'rs2 : x12', 'rd : x10', 'fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xa0e7ad32453df and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x082cc69704a64 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000474]:fsub.d a0, fp, a2, dyn
	-[0x80000478]:csrrs a7, fcsr, zero
	-[0x8000047c]:sw a0, 320(ra)
	-[0x80000480]:sw a1, 328(ra)
Current Store : [0x80000480] : sw a1, 328(ra) -- Store: [0x80003660]:0x7FEBDDE6




Last Coverpoint : ['rs1 : x8', 'rs2 : x12', 'rd : x10', 'fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xa0e7ad32453df and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x082cc69704a64 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000474]:fsub.d a0, fp, a2, dyn
	-[0x80000478]:csrrs a7, fcsr, zero
	-[0x8000047c]:sw a0, 320(ra)
	-[0x80000480]:sw a1, 328(ra)
	-[0x80000484]:sw a0, 336(ra)
Current Store : [0x80000484] : sw a0, 336(ra) -- Store: [0x80003668]:0x40096D03




Last Coverpoint : ['rs1 : x10', 'rs2 : x6', 'rd : x8', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe0d828b86622a and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x1daaf50c76c8b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004cc]:fsub.d fp, a0, t1, dyn
	-[0x800004d0]:csrrs a7, fcsr, zero
	-[0x800004d4]:sw fp, 0(ra)
	-[0x800004d8]:sw s1, 8(ra)
Current Store : [0x800004d8] : sw s1, 8(ra) -- Store: [0x800035d0]:0x7F9A0E7A




Last Coverpoint : ['rs1 : x10', 'rs2 : x6', 'rd : x8', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe0d828b86622a and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x1daaf50c76c8b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004cc]:fsub.d fp, a0, t1, dyn
	-[0x800004d0]:csrrs a7, fcsr, zero
	-[0x800004d4]:sw fp, 0(ra)
	-[0x800004d8]:sw s1, 8(ra)
	-[0x800004dc]:sw fp, 16(ra)
Current Store : [0x800004dc] : sw fp, 16(ra) -- Store: [0x800035d8]:0x757DEB3E




Last Coverpoint : ['rs1 : x4', 'rs2 : x8', 'rd : x6', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb9876f8130c3 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xa4e630c1be6d7 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000051c]:fsub.d t1, tp, fp, dyn
	-[0x80000520]:csrrs a7, fcsr, zero
	-[0x80000524]:sw t1, 32(ra)
	-[0x80000528]:sw t2, 40(ra)
Current Store : [0x80000528] : sw t2, 40(ra) -- Store: [0x800035f0]:0x7FE1DAAF




Last Coverpoint : ['rs1 : x4', 'rs2 : x8', 'rd : x6', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb9876f8130c3 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xa4e630c1be6d7 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000051c]:fsub.d t1, tp, fp, dyn
	-[0x80000520]:csrrs a7, fcsr, zero
	-[0x80000524]:sw t1, 32(ra)
	-[0x80000528]:sw t2, 40(ra)
	-[0x8000052c]:sw t1, 48(ra)
Current Store : [0x8000052c] : sw t1, 48(ra) -- Store: [0x800035f8]:0x3DCE8BCD




Last Coverpoint : ['rs1 : x6', 'rs2 : x2', 'rd : x4', 'fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xdbcde43895c3f and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x8a8c8b3c6f2ff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000056c]:fsub.d tp, t1, sp, dyn
	-[0x80000570]:csrrs a7, fcsr, zero
	-[0x80000574]:sw tp, 64(ra)
	-[0x80000578]:sw t0, 72(ra)
Current Store : [0x80000578] : sw t0, 72(ra) -- Store: [0x80003610]:0x7FCBB987




Last Coverpoint : ['rs1 : x6', 'rs2 : x2', 'rd : x4', 'fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xdbcde43895c3f and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x8a8c8b3c6f2ff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000056c]:fsub.d tp, t1, sp, dyn
	-[0x80000570]:csrrs a7, fcsr, zero
	-[0x80000574]:sw tp, 64(ra)
	-[0x80000578]:sw t0, 72(ra)
	-[0x8000057c]:sw tp, 80(ra)
Current Store : [0x8000057c] : sw tp, 80(ra) -- Store: [0x80003618]:0x7C381E87




Last Coverpoint : ['rs1 : x2', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc0659af8369fd and fs2 == 1 and fe2 == 0x7fa and fm2 == 0xda84ca746bd30 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005bc]:fsub.d t5, sp, t3, dyn
	-[0x800005c0]:csrrs a7, fcsr, zero
	-[0x800005c4]:sw t5, 96(ra)
	-[0x800005c8]:sw t6, 104(ra)
Current Store : [0x800005c8] : sw t6, 104(ra) -- Store: [0x80003630]:0x7FC132D8




Last Coverpoint : ['rs1 : x2', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc0659af8369fd and fs2 == 1 and fe2 == 0x7fa and fm2 == 0xda84ca746bd30 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005bc]:fsub.d t5, sp, t3, dyn
	-[0x800005c0]:csrrs a7, fcsr, zero
	-[0x800005c4]:sw t5, 96(ra)
	-[0x800005c8]:sw t6, 104(ra)
	-[0x800005cc]:sw t5, 112(ra)
Current Store : [0x800005cc] : sw t5, 112(ra) -- Store: [0x80003638]:0x446C41A3




Last Coverpoint : ['rs2 : x4', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdd5b61587fd27 and fs2 == 0 and fe2 == 0x7f6 and fm2 == 0x22b4aace78200 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000060c]:fsub.d t5, t3, tp, dyn
	-[0x80000610]:csrrs a7, fcsr, zero
	-[0x80000614]:sw t5, 128(ra)
	-[0x80000618]:sw t6, 136(ra)
Current Store : [0x80000618] : sw t6, 136(ra) -- Store: [0x80003650]:0x7FC132D8




Last Coverpoint : ['rs2 : x4', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdd5b61587fd27 and fs2 == 0 and fe2 == 0x7f6 and fm2 == 0x22b4aace78200 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000060c]:fsub.d t5, t3, tp, dyn
	-[0x80000610]:csrrs a7, fcsr, zero
	-[0x80000614]:sw t5, 128(ra)
	-[0x80000618]:sw t6, 136(ra)
	-[0x8000061c]:sw t5, 144(ra)
Current Store : [0x8000061c] : sw t5, 144(ra) -- Store: [0x80003658]:0xEAD45F1F




Last Coverpoint : ['rd : x2', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc01045c2cd787 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xed344f30f8d23 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000065c]:fsub.d sp, t5, t3, dyn
	-[0x80000660]:csrrs a7, fcsr, zero
	-[0x80000664]:sw sp, 160(ra)
	-[0x80000668]:sw gp, 168(ra)
Current Store : [0x80000668] : sw gp, 168(ra) -- Store: [0x80003670]:0x7FDC0659




Last Coverpoint : ['rd : x2', 'fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc01045c2cd787 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xed344f30f8d23 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000065c]:fsub.d sp, t5, t3, dyn
	-[0x80000660]:csrrs a7, fcsr, zero
	-[0x80000664]:sw sp, 160(ra)
	-[0x80000668]:sw gp, 168(ra)
	-[0x8000066c]:sw sp, 176(ra)
Current Store : [0x8000066c] : sw sp, 176(ra) -- Store: [0x80003678]:0xA79E3255




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x28bc82f697c4d and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x12bb1d4152629 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006ac]:fsub.d t5, t3, s10, dyn
	-[0x800006b0]:csrrs a7, fcsr, zero
	-[0x800006b4]:sw t5, 192(ra)
	-[0x800006b8]:sw t6, 200(ra)
Current Store : [0x800006b8] : sw t6, 200(ra) -- Store: [0x80003690]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x28bc82f697c4d and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x12bb1d4152629 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006ac]:fsub.d t5, t3, s10, dyn
	-[0x800006b0]:csrrs a7, fcsr, zero
	-[0x800006b4]:sw t5, 192(ra)
	-[0x800006b8]:sw t6, 200(ra)
	-[0x800006bc]:sw t5, 208(ra)
Current Store : [0x800006bc] : sw t5, 208(ra) -- Store: [0x80003698]:0xB5456240




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xacd7053aa42a2 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x1fdee0ff3e0e2 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006fc]:fsub.d t5, t3, s10, dyn
	-[0x80000700]:csrrs a7, fcsr, zero
	-[0x80000704]:sw t5, 224(ra)
	-[0x80000708]:sw t6, 232(ra)
Current Store : [0x80000708] : sw t6, 232(ra) -- Store: [0x800036b0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xacd7053aa42a2 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x1fdee0ff3e0e2 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006fc]:fsub.d t5, t3, s10, dyn
	-[0x80000700]:csrrs a7, fcsr, zero
	-[0x80000704]:sw t5, 224(ra)
	-[0x80000708]:sw t6, 232(ra)
	-[0x8000070c]:sw t5, 240(ra)
Current Store : [0x8000070c] : sw t5, 240(ra) -- Store: [0x800036b8]:0x4BB05231




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x48dace8666677 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xd35766bc3e2c3 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000074c]:fsub.d t5, t3, s10, dyn
	-[0x80000750]:csrrs a7, fcsr, zero
	-[0x80000754]:sw t5, 256(ra)
	-[0x80000758]:sw t6, 264(ra)
Current Store : [0x80000758] : sw t6, 264(ra) -- Store: [0x800036d0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x48dace8666677 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xd35766bc3e2c3 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000074c]:fsub.d t5, t3, s10, dyn
	-[0x80000750]:csrrs a7, fcsr, zero
	-[0x80000754]:sw t5, 256(ra)
	-[0x80000758]:sw t6, 264(ra)
	-[0x8000075c]:sw t5, 272(ra)
Current Store : [0x8000075c] : sw t5, 272(ra) -- Store: [0x800036d8]:0xCA11D456




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfc2ea66e5019e and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x0f8ef46d602a4 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000079c]:fsub.d t5, t3, s10, dyn
	-[0x800007a0]:csrrs a7, fcsr, zero
	-[0x800007a4]:sw t5, 288(ra)
	-[0x800007a8]:sw t6, 296(ra)
Current Store : [0x800007a8] : sw t6, 296(ra) -- Store: [0x800036f0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfc2ea66e5019e and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x0f8ef46d602a4 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000079c]:fsub.d t5, t3, s10, dyn
	-[0x800007a0]:csrrs a7, fcsr, zero
	-[0x800007a4]:sw t5, 288(ra)
	-[0x800007a8]:sw t6, 296(ra)
	-[0x800007ac]:sw t5, 304(ra)
Current Store : [0x800007ac] : sw t5, 304(ra) -- Store: [0x800036f8]:0x952F80F5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xef2a4f7c7db7f and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xb1c6f0270591a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007ec]:fsub.d t5, t3, s10, dyn
	-[0x800007f0]:csrrs a7, fcsr, zero
	-[0x800007f4]:sw t5, 320(ra)
	-[0x800007f8]:sw t6, 328(ra)
Current Store : [0x800007f8] : sw t6, 328(ra) -- Store: [0x80003710]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fb and fm1 == 0xef2a4f7c7db7f and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xb1c6f0270591a and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007ec]:fsub.d t5, t3, s10, dyn
	-[0x800007f0]:csrrs a7, fcsr, zero
	-[0x800007f4]:sw t5, 320(ra)
	-[0x800007f8]:sw t6, 328(ra)
	-[0x800007fc]:sw t5, 336(ra)
Current Store : [0x800007fc] : sw t5, 336(ra) -- Store: [0x80003718]:0xBF2A236D




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7166677e49c3c and fs2 == 0 and fe2 == 0x7f8 and fm2 == 0x5144e78f2a6c0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000083c]:fsub.d t5, t3, s10, dyn
	-[0x80000840]:csrrs a7, fcsr, zero
	-[0x80000844]:sw t5, 352(ra)
	-[0x80000848]:sw t6, 360(ra)
Current Store : [0x80000848] : sw t6, 360(ra) -- Store: [0x80003730]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7166677e49c3c and fs2 == 0 and fe2 == 0x7f8 and fm2 == 0x5144e78f2a6c0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000083c]:fsub.d t5, t3, s10, dyn
	-[0x80000840]:csrrs a7, fcsr, zero
	-[0x80000844]:sw t5, 352(ra)
	-[0x80000848]:sw t6, 360(ra)
	-[0x8000084c]:sw t5, 368(ra)
Current Store : [0x8000084c] : sw t5, 368(ra) -- Store: [0x80003738]:0x3E00D1A1




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xdf7523fde6c5d and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x0756bb5d68556 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000088c]:fsub.d t5, t3, s10, dyn
	-[0x80000890]:csrrs a7, fcsr, zero
	-[0x80000894]:sw t5, 384(ra)
	-[0x80000898]:sw t6, 392(ra)
Current Store : [0x80000898] : sw t6, 392(ra) -- Store: [0x80003750]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xdf7523fde6c5d and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x0756bb5d68556 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000088c]:fsub.d t5, t3, s10, dyn
	-[0x80000890]:csrrs a7, fcsr, zero
	-[0x80000894]:sw t5, 384(ra)
	-[0x80000898]:sw t6, 392(ra)
	-[0x8000089c]:sw t5, 400(ra)
Current Store : [0x8000089c] : sw t5, 400(ra) -- Store: [0x80003758]:0x140FCE0E




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa8fa703a4078c and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x472096b867e58 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008dc]:fsub.d t5, t3, s10, dyn
	-[0x800008e0]:csrrs a7, fcsr, zero
	-[0x800008e4]:sw t5, 416(ra)
	-[0x800008e8]:sw t6, 424(ra)
Current Store : [0x800008e8] : sw t6, 424(ra) -- Store: [0x80003770]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa8fa703a4078c and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x472096b867e58 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008dc]:fsub.d t5, t3, s10, dyn
	-[0x800008e0]:csrrs a7, fcsr, zero
	-[0x800008e4]:sw t5, 416(ra)
	-[0x800008e8]:sw t6, 424(ra)
	-[0x800008ec]:sw t5, 432(ra)
Current Store : [0x800008ec] : sw t5, 432(ra) -- Store: [0x80003778]:0xA8C267F6




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x36a63c245f557 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x23087ed83ab89 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000092c]:fsub.d t5, t3, s10, dyn
	-[0x80000930]:csrrs a7, fcsr, zero
	-[0x80000934]:sw t5, 448(ra)
	-[0x80000938]:sw t6, 456(ra)
Current Store : [0x80000938] : sw t6, 456(ra) -- Store: [0x80003790]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x36a63c245f557 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x23087ed83ab89 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000092c]:fsub.d t5, t3, s10, dyn
	-[0x80000930]:csrrs a7, fcsr, zero
	-[0x80000934]:sw t5, 448(ra)
	-[0x80000938]:sw t6, 456(ra)
	-[0x8000093c]:sw t5, 464(ra)
Current Store : [0x8000093c] : sw t5, 464(ra) -- Store: [0x80003798]:0xD7E4D070




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc3d4499ff58c3 and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x2937fe3bd9f20 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000097c]:fsub.d t5, t3, s10, dyn
	-[0x80000980]:csrrs a7, fcsr, zero
	-[0x80000984]:sw t5, 480(ra)
	-[0x80000988]:sw t6, 488(ra)
Current Store : [0x80000988] : sw t6, 488(ra) -- Store: [0x800037b0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc3d4499ff58c3 and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x2937fe3bd9f20 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000097c]:fsub.d t5, t3, s10, dyn
	-[0x80000980]:csrrs a7, fcsr, zero
	-[0x80000984]:sw t5, 480(ra)
	-[0x80000988]:sw t6, 488(ra)
	-[0x8000098c]:sw t5, 496(ra)
Current Store : [0x8000098c] : sw t5, 496(ra) -- Store: [0x800037b8]:0x9BC37ED1




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x02b48f992cb49 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x56e924eb7c838 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009cc]:fsub.d t5, t3, s10, dyn
	-[0x800009d0]:csrrs a7, fcsr, zero
	-[0x800009d4]:sw t5, 512(ra)
	-[0x800009d8]:sw t6, 520(ra)
Current Store : [0x800009d8] : sw t6, 520(ra) -- Store: [0x800037d0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x02b48f992cb49 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x56e924eb7c838 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009cc]:fsub.d t5, t3, s10, dyn
	-[0x800009d0]:csrrs a7, fcsr, zero
	-[0x800009d4]:sw t5, 512(ra)
	-[0x800009d8]:sw t6, 520(ra)
	-[0x800009dc]:sw t5, 528(ra)
Current Store : [0x800009dc] : sw t5, 528(ra) -- Store: [0x800037d8]:0x4369C450




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fa and fm1 == 0x1ad5e9ebc09df and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xaa6c2d4374fa3 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a1c]:fsub.d t5, t3, s10, dyn
	-[0x80000a20]:csrrs a7, fcsr, zero
	-[0x80000a24]:sw t5, 544(ra)
	-[0x80000a28]:sw t6, 552(ra)
Current Store : [0x80000a28] : sw t6, 552(ra) -- Store: [0x800037f0]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fa and fm1 == 0x1ad5e9ebc09df and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xaa6c2d4374fa3 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a1c]:fsub.d t5, t3, s10, dyn
	-[0x80000a20]:csrrs a7, fcsr, zero
	-[0x80000a24]:sw t5, 544(ra)
	-[0x80000a28]:sw t6, 552(ra)
	-[0x80000a2c]:sw t5, 560(ra)
Current Store : [0x80000a2c] : sw t5, 560(ra) -- Store: [0x800037f8]:0x7BE6521B




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x98455e99dfdb1 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x8848cf5ea9657 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a6c]:fsub.d t5, t3, s10, dyn
	-[0x80000a70]:csrrs a7, fcsr, zero
	-[0x80000a74]:sw t5, 576(ra)
	-[0x80000a78]:sw t6, 584(ra)
Current Store : [0x80000a78] : sw t6, 584(ra) -- Store: [0x80003810]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x98455e99dfdb1 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x8848cf5ea9657 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a6c]:fsub.d t5, t3, s10, dyn
	-[0x80000a70]:csrrs a7, fcsr, zero
	-[0x80000a74]:sw t5, 576(ra)
	-[0x80000a78]:sw t6, 584(ra)
	-[0x80000a7c]:sw t5, 592(ra)
Current Store : [0x80000a7c] : sw t5, 592(ra) -- Store: [0x80003818]:0xDD51650B




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0x6251b45dfbd3b and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x80cf7341ff72e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000abc]:fsub.d t5, t3, s10, dyn
	-[0x80000ac0]:csrrs a7, fcsr, zero
	-[0x80000ac4]:sw t5, 608(ra)
	-[0x80000ac8]:sw t6, 616(ra)
Current Store : [0x80000ac8] : sw t6, 616(ra) -- Store: [0x80003830]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fc and fm1 == 0x6251b45dfbd3b and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x80cf7341ff72e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000abc]:fsub.d t5, t3, s10, dyn
	-[0x80000ac0]:csrrs a7, fcsr, zero
	-[0x80000ac4]:sw t5, 608(ra)
	-[0x80000ac8]:sw t6, 616(ra)
	-[0x80000acc]:sw t5, 624(ra)
Current Store : [0x80000acc] : sw t5, 624(ra) -- Store: [0x80003838]:0x6B87EAE6




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x67f4f571a752e and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xd3d8104d0cdc0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b0c]:fsub.d t5, t3, s10, dyn
	-[0x80000b10]:csrrs a7, fcsr, zero
	-[0x80000b14]:sw t5, 640(ra)
	-[0x80000b18]:sw t6, 648(ra)
Current Store : [0x80000b18] : sw t6, 648(ra) -- Store: [0x80003850]:0x7FCC0104




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x67f4f571a752e and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xd3d8104d0cdc0 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b0c]:fsub.d t5, t3, s10, dyn
	-[0x80000b10]:csrrs a7, fcsr, zero
	-[0x80000b14]:sw t5, 640(ra)
	-[0x80000b18]:sw t6, 648(ra)
	-[0x80000b1c]:sw t5, 656(ra)
Current Store : [0x80000b1c] : sw t5, 656(ra) -- Store: [0x80003858]:0x4EF3EEC0





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                          signature                           |                                                                                                                        coverpoints                                                                                                                        |                                                                                                       code                                                                                                       |
|---:|--------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80003518]<br>0x00000000<br> [0x80003530]<br>0x00000000<br> |- mnemonic : fsub.d<br> - rs1 : x30<br> - rs2 : x30<br> - rd : x30<br> - rs1 == rs2 == rd<br>                                                                                                                                                              |[0x8000014c]:fsub.d t5, t5, t5, dyn<br> [0x80000150]:csrrs tp, fcsr, zero<br> [0x80000154]:sw t5, 0(ra)<br> [0x80000158]:sw t6, 8(ra)<br> [0x8000015c]:sw t5, 16(ra)<br> [0x80000160]:sw tp, 24(ra)<br>           |
|   2|[0x80003538]<br>0x08F17F5A<br> [0x80003550]<br>0x00000000<br> |- rs1 : x26<br> - rs2 : x24<br> - rd : x28<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x870d778409f12 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x04750f3c7df65 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x8000019c]:fsub.d t3, s10, s8, dyn<br> [0x800001a0]:csrrs tp, fcsr, zero<br> [0x800001a4]:sw t3, 32(ra)<br> [0x800001a8]:sw t4, 40(ra)<br> [0x800001ac]:sw t3, 48(ra)<br> [0x800001b0]:sw tp, 56(ra)<br>        |
|   3|[0x80003558]<br>0x2BC72881<br> [0x80003570]<br>0x00000000<br> |- rs1 : x24<br> - rs2 : x28<br> - rd : x24<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0xfb5355e167379 and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x785f9927a57c0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                        |[0x800001ec]:fsub.d s8, s8, t3, dyn<br> [0x800001f0]:csrrs tp, fcsr, zero<br> [0x800001f4]:sw s8, 64(ra)<br> [0x800001f8]:sw s9, 72(ra)<br> [0x800001fc]:sw s8, 80(ra)<br> [0x80000200]:sw tp, 88(ra)<br>         |
|   4|[0x80003578]<br>0xC0D6FE63<br> [0x80003590]<br>0x00000000<br> |- rs1 : x28<br> - rs2 : x26<br> - rd : x26<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4d9d98184b9d9 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x18d7cfd491228 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                        |[0x8000023c]:fsub.d s10, t3, s10, dyn<br> [0x80000240]:csrrs tp, fcsr, zero<br> [0x80000244]:sw s10, 96(ra)<br> [0x80000248]:sw s11, 104(ra)<br> [0x8000024c]:sw s10, 112(ra)<br> [0x80000250]:sw tp, 120(ra)<br> |
|   5|[0x80003598]<br>0x00000000<br> [0x800035b0]<br>0x00000000<br> |- rs1 : x20<br> - rs2 : x20<br> - rd : x22<br> - rs1 == rs2 != rd<br>                                                                                                                                                                                      |[0x8000028c]:fsub.d s6, s4, s4, dyn<br> [0x80000290]:csrrs tp, fcsr, zero<br> [0x80000294]:sw s6, 128(ra)<br> [0x80000298]:sw s7, 136(ra)<br> [0x8000029c]:sw s6, 144(ra)<br> [0x800002a0]:sw tp, 152(ra)<br>     |
|   6|[0x800035b8]<br>0xA4DC0871<br> [0x800035d0]<br>0x00000000<br> |- rs1 : x22<br> - rs2 : x18<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xabe96758f2a09 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x9cab846424ba1 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x800002dc]:fsub.d s4, s6, s2, dyn<br> [0x800002e0]:csrrs tp, fcsr, zero<br> [0x800002e4]:sw s4, 160(ra)<br> [0x800002e8]:sw s5, 168(ra)<br> [0x800002ec]:sw s4, 176(ra)<br> [0x800002f0]:sw tp, 184(ra)<br>     |
|   7|[0x800035d8]<br>0xC8886900<br> [0x800035f0]<br>0x00000000<br> |- rs1 : x16<br> - rs2 : x22<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x86499331191c4 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x853587c49095b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x8000032c]:fsub.d s2, a6, s6, dyn<br> [0x80000330]:csrrs tp, fcsr, zero<br> [0x80000334]:sw s2, 192(ra)<br> [0x80000338]:sw s3, 200(ra)<br> [0x8000033c]:sw s2, 208(ra)<br> [0x80000340]:sw tp, 216(ra)<br>     |
|   8|[0x800035f8]<br>0x1F3511F7<br> [0x80003610]<br>0x00000000<br> |- rs1 : x18<br> - rs2 : x14<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc81394a2171e9 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x1ac7cf448b205 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x8000037c]:fsub.d a6, s2, a4, dyn<br> [0x80000380]:csrrs tp, fcsr, zero<br> [0x80000384]:sw a6, 224(ra)<br> [0x80000388]:sw a7, 232(ra)<br> [0x8000038c]:sw a6, 240(ra)<br> [0x80000390]:sw tp, 248(ra)<br>     |
|   9|[0x80003618]<br>0x5468AE3B<br> [0x80003630]<br>0x00000001<br> |- rs1 : x12<br> - rs2 : x16<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0xd481499755d4b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x6eda32e0b56e8 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x800003cc]:fsub.d a4, a2, a6, dyn<br> [0x800003d0]:csrrs tp, fcsr, zero<br> [0x800003d4]:sw a4, 256(ra)<br> [0x800003d8]:sw a5, 264(ra)<br> [0x800003dc]:sw a4, 272(ra)<br> [0x800003e0]:sw tp, 280(ra)<br>     |
|  10|[0x80003638]<br>0x02DB7360<br> [0x80003650]<br>0x00000000<br> |- rs1 : x14<br> - rs2 : x10<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd87e65450c45 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xbdde68d2e30aa and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x80000424]:fsub.d a2, a4, a0, dyn<br> [0x80000428]:csrrs a7, fcsr, zero<br> [0x8000042c]:sw a2, 288(ra)<br> [0x80000430]:sw a3, 296(ra)<br> [0x80000434]:sw a2, 304(ra)<br> [0x80000438]:sw a7, 312(ra)<br>     |
|  11|[0x80003658]<br>0x40096D03<br> [0x80003670]<br>0x00000001<br> |- rs1 : x8<br> - rs2 : x12<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xa0e7ad32453df and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x082cc69704a64 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000474]:fsub.d a0, fp, a2, dyn<br> [0x80000478]:csrrs a7, fcsr, zero<br> [0x8000047c]:sw a0, 320(ra)<br> [0x80000480]:sw a1, 328(ra)<br> [0x80000484]:sw a0, 336(ra)<br> [0x80000488]:sw a7, 344(ra)<br>     |
|  12|[0x800035c8]<br>0x757DEB3E<br> [0x800035e0]<br>0x00000000<br> |- rs1 : x10<br> - rs2 : x6<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe0d828b86622a and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x1daaf50c76c8b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                 |[0x800004cc]:fsub.d fp, a0, t1, dyn<br> [0x800004d0]:csrrs a7, fcsr, zero<br> [0x800004d4]:sw fp, 0(ra)<br> [0x800004d8]:sw s1, 8(ra)<br> [0x800004dc]:sw fp, 16(ra)<br> [0x800004e0]:sw a7, 24(ra)<br>           |
|  13|[0x800035e8]<br>0x3DCE8BCD<br> [0x80003600]<br>0x00000000<br> |- rs1 : x4<br> - rs2 : x8<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb9876f8130c3 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xa4e630c1be6d7 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                  |[0x8000051c]:fsub.d t1, tp, fp, dyn<br> [0x80000520]:csrrs a7, fcsr, zero<br> [0x80000524]:sw t1, 32(ra)<br> [0x80000528]:sw t2, 40(ra)<br> [0x8000052c]:sw t1, 48(ra)<br> [0x80000530]:sw a7, 56(ra)<br>         |
|  14|[0x80003608]<br>0x7C381E87<br> [0x80003620]<br>0x00000001<br> |- rs1 : x6<br> - rs2 : x2<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xdbcde43895c3f and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x8a8c8b3c6f2ff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                  |[0x8000056c]:fsub.d tp, t1, sp, dyn<br> [0x80000570]:csrrs a7, fcsr, zero<br> [0x80000574]:sw tp, 64(ra)<br> [0x80000578]:sw t0, 72(ra)<br> [0x8000057c]:sw tp, 80(ra)<br> [0x80000580]:sw a7, 88(ra)<br>         |
|  15|[0x80003628]<br>0x446C41A3<br> [0x80003640]<br>0x00000000<br> |- rs1 : x2<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc0659af8369fd and fs2 == 1 and fe2 == 0x7fa and fm2 == 0xda84ca746bd30 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                               |[0x800005bc]:fsub.d t5, sp, t3, dyn<br> [0x800005c0]:csrrs a7, fcsr, zero<br> [0x800005c4]:sw t5, 96(ra)<br> [0x800005c8]:sw t6, 104(ra)<br> [0x800005cc]:sw t5, 112(ra)<br> [0x800005d0]:sw a7, 120(ra)<br>      |
|  16|[0x80003648]<br>0xEAD45F1F<br> [0x80003660]<br>0x00000000<br> |- rs2 : x4<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdd5b61587fd27 and fs2 == 0 and fe2 == 0x7f6 and fm2 == 0x22b4aace78200 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                               |[0x8000060c]:fsub.d t5, t3, tp, dyn<br> [0x80000610]:csrrs a7, fcsr, zero<br> [0x80000614]:sw t5, 128(ra)<br> [0x80000618]:sw t6, 136(ra)<br> [0x8000061c]:sw t5, 144(ra)<br> [0x80000620]:sw a7, 152(ra)<br>     |
|  17|[0x80003668]<br>0xA79E3255<br> [0x80003680]<br>0x00000000<br> |- rd : x2<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc01045c2cd787 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xed344f30f8d23 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                |[0x8000065c]:fsub.d sp, t5, t3, dyn<br> [0x80000660]:csrrs a7, fcsr, zero<br> [0x80000664]:sw sp, 160(ra)<br> [0x80000668]:sw gp, 168(ra)<br> [0x8000066c]:sw sp, 176(ra)<br> [0x80000670]:sw a7, 184(ra)<br>     |
|  18|[0x80003688]<br>0xB5456240<br> [0x800036a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x28bc82f697c4d and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x12bb1d4152629 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800006ac]:fsub.d t5, t3, s10, dyn<br> [0x800006b0]:csrrs a7, fcsr, zero<br> [0x800006b4]:sw t5, 192(ra)<br> [0x800006b8]:sw t6, 200(ra)<br> [0x800006bc]:sw t5, 208(ra)<br> [0x800006c0]:sw a7, 216(ra)<br>    |
|  19|[0x800036a8]<br>0x4BB05231<br> [0x800036c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xacd7053aa42a2 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x1fdee0ff3e0e2 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800006fc]:fsub.d t5, t3, s10, dyn<br> [0x80000700]:csrrs a7, fcsr, zero<br> [0x80000704]:sw t5, 224(ra)<br> [0x80000708]:sw t6, 232(ra)<br> [0x8000070c]:sw t5, 240(ra)<br> [0x80000710]:sw a7, 248(ra)<br>    |
|  20|[0x800036c8]<br>0xCA11D456<br> [0x800036e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x48dace8666677 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xd35766bc3e2c3 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x8000074c]:fsub.d t5, t3, s10, dyn<br> [0x80000750]:csrrs a7, fcsr, zero<br> [0x80000754]:sw t5, 256(ra)<br> [0x80000758]:sw t6, 264(ra)<br> [0x8000075c]:sw t5, 272(ra)<br> [0x80000760]:sw a7, 280(ra)<br>    |
|  21|[0x800036e8]<br>0x952F80F5<br> [0x80003700]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfc2ea66e5019e and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x0f8ef46d602a4 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x8000079c]:fsub.d t5, t3, s10, dyn<br> [0x800007a0]:csrrs a7, fcsr, zero<br> [0x800007a4]:sw t5, 288(ra)<br> [0x800007a8]:sw t6, 296(ra)<br> [0x800007ac]:sw t5, 304(ra)<br> [0x800007b0]:sw a7, 312(ra)<br>    |
|  22|[0x80003708]<br>0xBF2A236D<br> [0x80003720]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xef2a4f7c7db7f and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xb1c6f0270591a and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800007ec]:fsub.d t5, t3, s10, dyn<br> [0x800007f0]:csrrs a7, fcsr, zero<br> [0x800007f4]:sw t5, 320(ra)<br> [0x800007f8]:sw t6, 328(ra)<br> [0x800007fc]:sw t5, 336(ra)<br> [0x80000800]:sw a7, 344(ra)<br>    |
|  23|[0x80003728]<br>0x3E00D1A1<br> [0x80003740]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7166677e49c3c and fs2 == 0 and fe2 == 0x7f8 and fm2 == 0x5144e78f2a6c0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x8000083c]:fsub.d t5, t3, s10, dyn<br> [0x80000840]:csrrs a7, fcsr, zero<br> [0x80000844]:sw t5, 352(ra)<br> [0x80000848]:sw t6, 360(ra)<br> [0x8000084c]:sw t5, 368(ra)<br> [0x80000850]:sw a7, 376(ra)<br>    |
|  24|[0x80003748]<br>0x140FCE0E<br> [0x80003760]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xdf7523fde6c5d and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x0756bb5d68556 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x8000088c]:fsub.d t5, t3, s10, dyn<br> [0x80000890]:csrrs a7, fcsr, zero<br> [0x80000894]:sw t5, 384(ra)<br> [0x80000898]:sw t6, 392(ra)<br> [0x8000089c]:sw t5, 400(ra)<br> [0x800008a0]:sw a7, 408(ra)<br>    |
|  25|[0x80003768]<br>0xA8C267F6<br> [0x80003780]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa8fa703a4078c and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x472096b867e58 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800008dc]:fsub.d t5, t3, s10, dyn<br> [0x800008e0]:csrrs a7, fcsr, zero<br> [0x800008e4]:sw t5, 416(ra)<br> [0x800008e8]:sw t6, 424(ra)<br> [0x800008ec]:sw t5, 432(ra)<br> [0x800008f0]:sw a7, 440(ra)<br>    |
|  26|[0x80003788]<br>0xD7E4D070<br> [0x800037a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x36a63c245f557 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x23087ed83ab89 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x8000092c]:fsub.d t5, t3, s10, dyn<br> [0x80000930]:csrrs a7, fcsr, zero<br> [0x80000934]:sw t5, 448(ra)<br> [0x80000938]:sw t6, 456(ra)<br> [0x8000093c]:sw t5, 464(ra)<br> [0x80000940]:sw a7, 472(ra)<br>    |
|  27|[0x800037a8]<br>0x9BC37ED1<br> [0x800037c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc3d4499ff58c3 and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x2937fe3bd9f20 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x8000097c]:fsub.d t5, t3, s10, dyn<br> [0x80000980]:csrrs a7, fcsr, zero<br> [0x80000984]:sw t5, 480(ra)<br> [0x80000988]:sw t6, 488(ra)<br> [0x8000098c]:sw t5, 496(ra)<br> [0x80000990]:sw a7, 504(ra)<br>    |
|  28|[0x800037c8]<br>0x4369C450<br> [0x800037e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x02b48f992cb49 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x56e924eb7c838 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800009cc]:fsub.d t5, t3, s10, dyn<br> [0x800009d0]:csrrs a7, fcsr, zero<br> [0x800009d4]:sw t5, 512(ra)<br> [0x800009d8]:sw t6, 520(ra)<br> [0x800009dc]:sw t5, 528(ra)<br> [0x800009e0]:sw a7, 536(ra)<br>    |
|  29|[0x800037e8]<br>0x7BE6521B<br> [0x80003800]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x1ad5e9ebc09df and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xaa6c2d4374fa3 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000a1c]:fsub.d t5, t3, s10, dyn<br> [0x80000a20]:csrrs a7, fcsr, zero<br> [0x80000a24]:sw t5, 544(ra)<br> [0x80000a28]:sw t6, 552(ra)<br> [0x80000a2c]:sw t5, 560(ra)<br> [0x80000a30]:sw a7, 568(ra)<br>    |
|  30|[0x80003808]<br>0xDD51650B<br> [0x80003820]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x98455e99dfdb1 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x8848cf5ea9657 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000a6c]:fsub.d t5, t3, s10, dyn<br> [0x80000a70]:csrrs a7, fcsr, zero<br> [0x80000a74]:sw t5, 576(ra)<br> [0x80000a78]:sw t6, 584(ra)<br> [0x80000a7c]:sw t5, 592(ra)<br> [0x80000a80]:sw a7, 600(ra)<br>    |
|  31|[0x80003828]<br>0x6B87EAE6<br> [0x80003840]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x6251b45dfbd3b and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x80cf7341ff72e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000abc]:fsub.d t5, t3, s10, dyn<br> [0x80000ac0]:csrrs a7, fcsr, zero<br> [0x80000ac4]:sw t5, 608(ra)<br> [0x80000ac8]:sw t6, 616(ra)<br> [0x80000acc]:sw t5, 624(ra)<br> [0x80000ad0]:sw a7, 632(ra)<br>    |
|  32|[0x80003848]<br>0x4EF3EEC0<br> [0x80003860]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x67f4f571a752e and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xd3d8104d0cdc0 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000b0c]:fsub.d t5, t3, s10, dyn<br> [0x80000b10]:csrrs a7, fcsr, zero<br> [0x80000b14]:sw t5, 640(ra)<br> [0x80000b18]:sw t6, 648(ra)<br> [0x80000b1c]:sw t5, 656(ra)<br> [0x80000b20]:sw a7, 664(ra)<br>    |
