
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000500')]      |
| SIG_REGION                | [('0x80002210', '0x800022e0', '52 words')]      |
| COV_LABELS                | fclass.d_b1      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-fclass20/fclass.d_b1-01.S/ref.S    |
| Total Number of coverpoints| 55     |
| Total Coverpoints Hit     | 55      |
| Total Signature Updates   | 39      |
| STAT1                     | 20      |
| STAT2                     | 0      |
| STAT3                     | 4     |
| STAT4                     | 19     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x8000046c]:fclass.d t5, t3
[0x80000470]:csrrs a4, fcsr, zero
[0x80000474]:sw t5, 112(ra)
[0x80000478]:sw a4, 120(ra)
[0x8000047c]:lw t3, 64(a3)
[0x80000480]:lw t4, 68(a3)
[0x80000484]:addi t3, zero, 1
[0x80000488]:lui t4, 1048320
[0x8000048c]:addi a2, zero, 0
[0x80000490]:csrrw zero, fcsr, a2
[0x80000494]:fclass.d t5, t3

[0x80000494]:fclass.d t5, t3
[0x80000498]:csrrs a4, fcsr, zero
[0x8000049c]:sw t5, 128(ra)
[0x800004a0]:sw a4, 136(ra)
[0x800004a4]:lw t3, 72(a3)
[0x800004a8]:lw t4, 76(a3)
[0x800004ac]:addi t3, zero, 0
[0x800004b0]:lui t4, 261888
[0x800004b4]:addi a2, zero, 0
[0x800004b8]:csrrw zero, fcsr, a2
[0x800004bc]:fclass.d t5, t3

[0x800004bc]:fclass.d t5, t3
[0x800004c0]:csrrs a4, fcsr, zero
[0x800004c4]:sw t5, 144(ra)
[0x800004c8]:sw a4, 152(ra)
[0x800004cc]:lw t3, 80(a3)
[0x800004d0]:lw t4, 84(a3)
[0x800004d4]:addi t3, zero, 0
[0x800004d8]:lui t4, 784384
[0x800004dc]:addi a2, zero, 0
[0x800004e0]:csrrw zero, fcsr, a2
[0x800004e4]:fclass.d t5, t3

[0x800004e4]:fclass.d t5, t3
[0x800004e8]:csrrs a4, fcsr, zero
[0x800004ec]:sw t5, 160(ra)
[0x800004f0]:sw a4, 168(ra)
[0x800004f4]:addi zero, zero, 0
[0x800004f8]:addi zero, zero, 0
[0x800004fc]:addi zero, zero, 0



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fclass.d', 'rs1 : x28', 'rd : x30', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000012c]:fclass.d t5, t3
	-[0x80000130]:csrrs tp, fcsr, zero
	-[0x80000134]:sw t5, 0(ra)
Current Store : [0x80000138] : sw tp, 8(ra) -- Store: [0x80002220]:0x00000000




Last Coverpoint : ['rs1 : x30', 'rd : x28', 'fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000154]:fclass.d t3, t5
	-[0x80000158]:csrrs tp, fcsr, zero
	-[0x8000015c]:sw t3, 16(ra)
Current Store : [0x80000160] : sw tp, 24(ra) -- Store: [0x80002230]:0x00000000




Last Coverpoint : ['rs1 : x24', 'rd : x26', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000017c]:fclass.d s10, s8
	-[0x80000180]:csrrs tp, fcsr, zero
	-[0x80000184]:sw s10, 32(ra)
Current Store : [0x80000188] : sw tp, 40(ra) -- Store: [0x80002240]:0x00000000




Last Coverpoint : ['rs1 : x26', 'rd : x24', 'fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800001a4]:fclass.d s8, s10
	-[0x800001a8]:csrrs tp, fcsr, zero
	-[0x800001ac]:sw s8, 48(ra)
Current Store : [0x800001b0] : sw tp, 56(ra) -- Store: [0x80002250]:0x00000000




Last Coverpoint : ['rs1 : x20', 'rd : x22', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800001cc]:fclass.d s6, s4
	-[0x800001d0]:csrrs tp, fcsr, zero
	-[0x800001d4]:sw s6, 64(ra)
Current Store : [0x800001d8] : sw tp, 72(ra) -- Store: [0x80002260]:0x00000000




Last Coverpoint : ['rs1 : x22', 'rd : x20', 'fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800001f4]:fclass.d s4, s6
	-[0x800001f8]:csrrs tp, fcsr, zero
	-[0x800001fc]:sw s4, 80(ra)
Current Store : [0x80000200] : sw tp, 88(ra) -- Store: [0x80002270]:0x00000000




Last Coverpoint : ['rs1 : x16', 'rd : x18', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000220]:fclass.d s2, a6
	-[0x80000224]:csrrs tp, fcsr, zero
	-[0x80000228]:sw s2, 96(ra)
Current Store : [0x8000022c] : sw tp, 104(ra) -- Store: [0x80002280]:0x00000000




Last Coverpoint : ['rs1 : x18', 'rd : x16', 'fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000024c]:fclass.d a6, s2
	-[0x80000250]:csrrs tp, fcsr, zero
	-[0x80000254]:sw a6, 112(ra)
Current Store : [0x80000258] : sw tp, 120(ra) -- Store: [0x80002290]:0x00000000




Last Coverpoint : ['rs1 : x12', 'rd : x14', 'fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000274]:fclass.d a4, a2
	-[0x80000278]:csrrs tp, fcsr, zero
	-[0x8000027c]:sw a4, 128(ra)
Current Store : [0x80000280] : sw tp, 136(ra) -- Store: [0x800022a0]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rd : x12', 'fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000029c]:fclass.d a2, a4
	-[0x800002a0]:csrrs tp, fcsr, zero
	-[0x800002a4]:sw a2, 144(ra)
Current Store : [0x800002a8] : sw tp, 152(ra) -- Store: [0x800022b0]:0x00000000




Last Coverpoint : ['rs1 : x8', 'rd : x10', 'fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800002c4]:fclass.d a0, fp
	-[0x800002c8]:csrrs tp, fcsr, zero
	-[0x800002cc]:sw a0, 160(ra)
Current Store : [0x800002d0] : sw tp, 168(ra) -- Store: [0x800022c0]:0x00000000




Last Coverpoint : ['rs1 : x10', 'rd : x8', 'fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800002f4]:fclass.d fp, a0
	-[0x800002f8]:csrrs a4, fcsr, zero
	-[0x800002fc]:sw fp, 176(ra)
Current Store : [0x80000300] : sw a4, 184(ra) -- Store: [0x800022d0]:0x00000000




Last Coverpoint : ['rs1 : x4', 'rd : x6', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000320]:fclass.d t1, tp
	-[0x80000324]:csrrs a4, fcsr, zero
	-[0x80000328]:sw t1, 192(ra)
Current Store : [0x8000032c] : sw a4, 200(ra) -- Store: [0x800022e0]:0x00000000




Last Coverpoint : ['rs1 : x6', 'rd : x4', 'fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000354]:fclass.d tp, t1
	-[0x80000358]:csrrs a4, fcsr, zero
	-[0x8000035c]:sw tp, 0(ra)
Current Store : [0x80000360] : sw a4, 8(ra) -- Store: [0x80002288]:0x00000000




Last Coverpoint : ['rs1 : x2', 'fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000037c]:fclass.d t5, sp
	-[0x80000380]:csrrs a4, fcsr, zero
	-[0x80000384]:sw t5, 16(ra)
Current Store : [0x80000388] : sw a4, 24(ra) -- Store: [0x80002298]:0x00000000




Last Coverpoint : ['rd : x2', 'fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800003a4]:fclass.d sp, t5
	-[0x800003a8]:csrrs a4, fcsr, zero
	-[0x800003ac]:sw sp, 32(ra)
Current Store : [0x800003b0] : sw a4, 40(ra) -- Store: [0x800022a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800003cc]:fclass.d t5, t3
	-[0x800003d0]:csrrs a4, fcsr, zero
	-[0x800003d4]:sw t5, 48(ra)
Current Store : [0x800003d8] : sw a4, 56(ra) -- Store: [0x800022b8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800003f4]:fclass.d t5, t3
	-[0x800003f8]:csrrs a4, fcsr, zero
	-[0x800003fc]:sw t5, 64(ra)
Current Store : [0x80000400] : sw a4, 72(ra) -- Store: [0x800022c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000041c]:fclass.d t5, t3
	-[0x80000420]:csrrs a4, fcsr, zero
	-[0x80000424]:sw t5, 80(ra)
Current Store : [0x80000428] : sw a4, 88(ra) -- Store: [0x800022d8]:0x00000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|           signature           |                                                                coverpoints                                                                |                                                  code                                                  |
|---:|-------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------|
|   1|[0x80002218]<br>0x00000010<br> |- mnemonic : fclass.d<br> - rs1 : x28<br> - rd : x30<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and  fcsr == 0  #nosat<br> |[0x8000012c]:fclass.d t5, t3<br> [0x80000130]:csrrs tp, fcsr, zero<br> [0x80000134]:sw t5, 0(ra)<br>    |
|   2|[0x80002228]<br>0x00000008<br> |- rs1 : x30<br> - rd : x28<br> - fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and  fcsr == 0  #nosat<br>                           |[0x80000154]:fclass.d t3, t5<br> [0x80000158]:csrrs tp, fcsr, zero<br> [0x8000015c]:sw t3, 16(ra)<br>   |
|   3|[0x80002238]<br>0x00000020<br> |- rs1 : x24<br> - rd : x26<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and  fcsr == 0  #nosat<br>                           |[0x8000017c]:fclass.d s10, s8<br> [0x80000180]:csrrs tp, fcsr, zero<br> [0x80000184]:sw s10, 32(ra)<br> |
|   4|[0x80002248]<br>0x00000004<br> |- rs1 : x26<br> - rd : x24<br> - fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and  fcsr == 0  #nosat<br>                           |[0x800001a4]:fclass.d s8, s10<br> [0x800001a8]:csrrs tp, fcsr, zero<br> [0x800001ac]:sw s8, 48(ra)<br>  |
|   5|[0x80002258]<br>0x00000020<br> |- rs1 : x20<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and  fcsr == 0  #nosat<br>                           |[0x800001cc]:fclass.d s6, s4<br> [0x800001d0]:csrrs tp, fcsr, zero<br> [0x800001d4]:sw s6, 64(ra)<br>   |
|   6|[0x80002268]<br>0x00000004<br> |- rs1 : x22<br> - rd : x20<br> - fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and  fcsr == 0  #nosat<br>                           |[0x800001f4]:fclass.d s4, s6<br> [0x800001f8]:csrrs tp, fcsr, zero<br> [0x800001fc]:sw s4, 80(ra)<br>   |
|   7|[0x80002278]<br>0x00000020<br> |- rs1 : x16<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                           |[0x80000220]:fclass.d s2, a6<br> [0x80000224]:csrrs tp, fcsr, zero<br> [0x80000228]:sw s2, 96(ra)<br>   |
|   8|[0x80002288]<br>0x00000004<br> |- rs1 : x18<br> - rd : x16<br> - fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                           |[0x8000024c]:fclass.d a6, s2<br> [0x80000250]:csrrs tp, fcsr, zero<br> [0x80000254]:sw a6, 112(ra)<br>  |
|   9|[0x80002298]<br>0x00000040<br> |- rs1 : x12<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and  fcsr == 0  #nosat<br>                           |[0x80000274]:fclass.d a4, a2<br> [0x80000278]:csrrs tp, fcsr, zero<br> [0x8000027c]:sw a4, 128(ra)<br>  |
|  10|[0x800022a8]<br>0x00000002<br> |- rs1 : x14<br> - rd : x12<br> - fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and  fcsr == 0  #nosat<br>                           |[0x8000029c]:fclass.d a2, a4<br> [0x800002a0]:csrrs tp, fcsr, zero<br> [0x800002a4]:sw a2, 144(ra)<br>  |
|  11|[0x800022b8]<br>0x00000040<br> |- rs1 : x8<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and  fcsr == 0  #nosat<br>                            |[0x800002c4]:fclass.d a0, fp<br> [0x800002c8]:csrrs tp, fcsr, zero<br> [0x800002cc]:sw a0, 160(ra)<br>  |
|  12|[0x800022c8]<br>0x00000002<br> |- rs1 : x10<br> - rd : x8<br> - fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and  fcsr == 0  #nosat<br>                            |[0x800002f4]:fclass.d fp, a0<br> [0x800002f8]:csrrs a4, fcsr, zero<br> [0x800002fc]:sw fp, 176(ra)<br>  |
|  13|[0x800022d8]<br>0x00000040<br> |- rs1 : x4<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                             |[0x80000320]:fclass.d t1, tp<br> [0x80000324]:csrrs a4, fcsr, zero<br> [0x80000328]:sw t1, 192(ra)<br>  |
|  14|[0x80002280]<br>0x00000002<br> |- rs1 : x6<br> - rd : x4<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and  fcsr == 0  #nosat<br>                             |[0x80000354]:fclass.d tp, t1<br> [0x80000358]:csrrs a4, fcsr, zero<br> [0x8000035c]:sw tp, 0(ra)<br>    |
|  15|[0x80002290]<br>0x00000080<br> |- rs1 : x2<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                           |[0x8000037c]:fclass.d t5, sp<br> [0x80000380]:csrrs a4, fcsr, zero<br> [0x80000384]:sw t5, 16(ra)<br>   |
|  16|[0x800022a0]<br>0x00000001<br> |- rd : x2<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and  fcsr == 0  #nosat<br>                                            |[0x800003a4]:fclass.d sp, t5<br> [0x800003a8]:csrrs a4, fcsr, zero<br> [0x800003ac]:sw sp, 32(ra)<br>   |
|  17|[0x800022b0]<br>0x00000200<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                          |[0x800003cc]:fclass.d t5, t3<br> [0x800003d0]:csrrs a4, fcsr, zero<br> [0x800003d4]:sw t5, 48(ra)<br>   |
|  18|[0x800022c0]<br>0x00000200<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and  fcsr == 0  #nosat<br>                                                          |[0x800003f4]:fclass.d t5, t3<br> [0x800003f8]:csrrs a4, fcsr, zero<br> [0x800003fc]:sw t5, 64(ra)<br>   |
|  19|[0x800022d0]<br>0x00000200<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                          |[0x8000041c]:fclass.d t5, t3<br> [0x80000420]:csrrs a4, fcsr, zero<br> [0x80000424]:sw t5, 80(ra)<br>   |
|  20|[0x800022e0]<br>0x00000200<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and  fcsr == 0  #nosat<br>                                                          |[0x80000444]:fclass.d t5, t3<br> [0x80000448]:csrrs a4, fcsr, zero<br> [0x8000044c]:sw t5, 96(ra)<br>   |
