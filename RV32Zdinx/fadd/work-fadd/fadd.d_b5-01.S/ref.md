
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800055b0')]      |
| SIG_REGION                | [('0x80007f10', '0x80008c60', '852 words')]      |
| COV_LABELS                | fadd.d_b5      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-fadd/fadd.d_b5-01.S/ref.S    |
| Total Number of coverpoints| 261     |
| Total Coverpoints Hit     | 261      |
| Total Signature Updates   | 448      |
| STAT1                     | 112      |
| STAT2                     | 0      |
| STAT3                     | 100     |
| STAT4                     | 224     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80002460]:fadd.d t5, t3, s10, dyn
[0x80002464]:csrrs a7, fcsr, zero
[0x80002468]:sw t5, 1192(ra)
[0x8000246c]:sw t6, 1200(ra)
[0x80002470]:sw t5, 1208(ra)
[0x80002474]:sw a7, 1216(ra)
[0x80002478]:lw t3, 1632(a6)
[0x8000247c]:lw t4, 1636(a6)
[0x80002480]:lw s10, 1640(a6)
[0x80002484]:lw s11, 1644(a6)
[0x80002488]:lui t3, 1006097
[0x8000248c]:addi t3, t3, 3925
[0x80002490]:lui t4, 523597
[0x80002494]:addi t4, t4, 37
[0x80002498]:lui s10, 1006097
[0x8000249c]:addi s10, s10, 3925
[0x800024a0]:lui s11, 1047885
[0x800024a4]:addi s11, s11, 37
[0x800024a8]:addi a4, zero, 96
[0x800024ac]:csrrw zero, fcsr, a4
[0x800024b0]:fadd.d t5, t3, s10, dyn
[0x800024b4]:csrrs a7, fcsr, zero

[0x800024b0]:fadd.d t5, t3, s10, dyn
[0x800024b4]:csrrs a7, fcsr, zero
[0x800024b8]:sw t5, 1224(ra)
[0x800024bc]:sw t6, 1232(ra)
[0x800024c0]:sw t5, 1240(ra)
[0x800024c4]:sw a7, 1248(ra)
[0x800024c8]:lw t3, 1648(a6)
[0x800024cc]:lw t4, 1652(a6)
[0x800024d0]:lw s10, 1656(a6)
[0x800024d4]:lw s11, 1660(a6)
[0x800024d8]:lui t3, 1006097
[0x800024dc]:addi t3, t3, 3925
[0x800024e0]:lui t4, 523597
[0x800024e4]:addi t4, t4, 37
[0x800024e8]:lui s10, 1006097
[0x800024ec]:addi s10, s10, 3925
[0x800024f0]:lui s11, 1047885
[0x800024f4]:addi s11, s11, 37
[0x800024f8]:addi a4, zero, 128
[0x800024fc]:csrrw zero, fcsr, a4
[0x80002500]:fadd.d t5, t3, s10, dyn
[0x80002504]:csrrs a7, fcsr, zero

[0x80002500]:fadd.d t5, t3, s10, dyn
[0x80002504]:csrrs a7, fcsr, zero
[0x80002508]:sw t5, 1256(ra)
[0x8000250c]:sw t6, 1264(ra)
[0x80002510]:sw t5, 1272(ra)
[0x80002514]:sw a7, 1280(ra)
[0x80002518]:lw t3, 1664(a6)
[0x8000251c]:lw t4, 1668(a6)
[0x80002520]:lw s10, 1672(a6)
[0x80002524]:lw s11, 1676(a6)
[0x80002528]:lui t3, 977602
[0x8000252c]:addi t3, t3, 3091
[0x80002530]:lui t4, 523399
[0x80002534]:addi t4, t4, 1250
[0x80002538]:lui s10, 977602
[0x8000253c]:addi s10, s10, 3091
[0x80002540]:lui s11, 1047687
[0x80002544]:addi s11, s11, 1250
[0x80002548]:addi a4, zero, 0
[0x8000254c]:csrrw zero, fcsr, a4
[0x80002550]:fadd.d t5, t3, s10, dyn
[0x80002554]:csrrs a7, fcsr, zero

[0x80002550]:fadd.d t5, t3, s10, dyn
[0x80002554]:csrrs a7, fcsr, zero
[0x80002558]:sw t5, 1288(ra)
[0x8000255c]:sw t6, 1296(ra)
[0x80002560]:sw t5, 1304(ra)
[0x80002564]:sw a7, 1312(ra)
[0x80002568]:lw t3, 1680(a6)
[0x8000256c]:lw t4, 1684(a6)
[0x80002570]:lw s10, 1688(a6)
[0x80002574]:lw s11, 1692(a6)
[0x80002578]:lui t3, 977602
[0x8000257c]:addi t3, t3, 3091
[0x80002580]:lui t4, 523399
[0x80002584]:addi t4, t4, 1250
[0x80002588]:lui s10, 977602
[0x8000258c]:addi s10, s10, 3091
[0x80002590]:lui s11, 1047687
[0x80002594]:addi s11, s11, 1250
[0x80002598]:addi a4, zero, 32
[0x8000259c]:csrrw zero, fcsr, a4
[0x800025a0]:fadd.d t5, t3, s10, dyn
[0x800025a4]:csrrs a7, fcsr, zero

[0x800025a0]:fadd.d t5, t3, s10, dyn
[0x800025a4]:csrrs a7, fcsr, zero
[0x800025a8]:sw t5, 1320(ra)
[0x800025ac]:sw t6, 1328(ra)
[0x800025b0]:sw t5, 1336(ra)
[0x800025b4]:sw a7, 1344(ra)
[0x800025b8]:lw t3, 1696(a6)
[0x800025bc]:lw t4, 1700(a6)
[0x800025c0]:lw s10, 1704(a6)
[0x800025c4]:lw s11, 1708(a6)
[0x800025c8]:lui t3, 977602
[0x800025cc]:addi t3, t3, 3091
[0x800025d0]:lui t4, 523399
[0x800025d4]:addi t4, t4, 1250
[0x800025d8]:lui s10, 977602
[0x800025dc]:addi s10, s10, 3091
[0x800025e0]:lui s11, 1047687
[0x800025e4]:addi s11, s11, 1250
[0x800025e8]:addi a4, zero, 64
[0x800025ec]:csrrw zero, fcsr, a4
[0x800025f0]:fadd.d t5, t3, s10, dyn
[0x800025f4]:csrrs a7, fcsr, zero

[0x800025f0]:fadd.d t5, t3, s10, dyn
[0x800025f4]:csrrs a7, fcsr, zero
[0x800025f8]:sw t5, 1352(ra)
[0x800025fc]:sw t6, 1360(ra)
[0x80002600]:sw t5, 1368(ra)
[0x80002604]:sw a7, 1376(ra)
[0x80002608]:lw t3, 1712(a6)
[0x8000260c]:lw t4, 1716(a6)
[0x80002610]:lw s10, 1720(a6)
[0x80002614]:lw s11, 1724(a6)
[0x80002618]:lui t3, 977602
[0x8000261c]:addi t3, t3, 3091
[0x80002620]:lui t4, 523399
[0x80002624]:addi t4, t4, 1250
[0x80002628]:lui s10, 977602
[0x8000262c]:addi s10, s10, 3091
[0x80002630]:lui s11, 1047687
[0x80002634]:addi s11, s11, 1250
[0x80002638]:addi a4, zero, 96
[0x8000263c]:csrrw zero, fcsr, a4
[0x80002640]:fadd.d t5, t3, s10, dyn
[0x80002644]:csrrs a7, fcsr, zero

[0x80002640]:fadd.d t5, t3, s10, dyn
[0x80002644]:csrrs a7, fcsr, zero
[0x80002648]:sw t5, 1384(ra)
[0x8000264c]:sw t6, 1392(ra)
[0x80002650]:sw t5, 1400(ra)
[0x80002654]:sw a7, 1408(ra)
[0x80002658]:lw t3, 1728(a6)
[0x8000265c]:lw t4, 1732(a6)
[0x80002660]:lw s10, 1736(a6)
[0x80002664]:lw s11, 1740(a6)
[0x80002668]:lui t3, 977602
[0x8000266c]:addi t3, t3, 3091
[0x80002670]:lui t4, 523399
[0x80002674]:addi t4, t4, 1250
[0x80002678]:lui s10, 977602
[0x8000267c]:addi s10, s10, 3091
[0x80002680]:lui s11, 1047687
[0x80002684]:addi s11, s11, 1250
[0x80002688]:addi a4, zero, 128
[0x8000268c]:csrrw zero, fcsr, a4
[0x80002690]:fadd.d t5, t3, s10, dyn
[0x80002694]:csrrs a7, fcsr, zero

[0x80002690]:fadd.d t5, t3, s10, dyn
[0x80002694]:csrrs a7, fcsr, zero
[0x80002698]:sw t5, 1416(ra)
[0x8000269c]:sw t6, 1424(ra)
[0x800026a0]:sw t5, 1432(ra)
[0x800026a4]:sw a7, 1440(ra)
[0x800026a8]:lw t3, 1744(a6)
[0x800026ac]:lw t4, 1748(a6)
[0x800026b0]:lw s10, 1752(a6)
[0x800026b4]:lw s11, 1756(a6)
[0x800026b8]:lui t3, 511400
[0x800026bc]:addi t3, t3, 83
[0x800026c0]:lui t4, 523497
[0x800026c4]:addi t4, t4, 2807
[0x800026c8]:lui s10, 511400
[0x800026cc]:addi s10, s10, 83
[0x800026d0]:lui s11, 1047785
[0x800026d4]:addi s11, s11, 2807
[0x800026d8]:addi a4, zero, 0
[0x800026dc]:csrrw zero, fcsr, a4
[0x800026e0]:fadd.d t5, t3, s10, dyn
[0x800026e4]:csrrs a7, fcsr, zero

[0x800026e0]:fadd.d t5, t3, s10, dyn
[0x800026e4]:csrrs a7, fcsr, zero
[0x800026e8]:sw t5, 1448(ra)
[0x800026ec]:sw t6, 1456(ra)
[0x800026f0]:sw t5, 1464(ra)
[0x800026f4]:sw a7, 1472(ra)
[0x800026f8]:lw t3, 1760(a6)
[0x800026fc]:lw t4, 1764(a6)
[0x80002700]:lw s10, 1768(a6)
[0x80002704]:lw s11, 1772(a6)
[0x80002708]:lui t3, 511400
[0x8000270c]:addi t3, t3, 83
[0x80002710]:lui t4, 523497
[0x80002714]:addi t4, t4, 2807
[0x80002718]:lui s10, 511400
[0x8000271c]:addi s10, s10, 83
[0x80002720]:lui s11, 1047785
[0x80002724]:addi s11, s11, 2807
[0x80002728]:addi a4, zero, 32
[0x8000272c]:csrrw zero, fcsr, a4
[0x80002730]:fadd.d t5, t3, s10, dyn
[0x80002734]:csrrs a7, fcsr, zero

[0x80002730]:fadd.d t5, t3, s10, dyn
[0x80002734]:csrrs a7, fcsr, zero
[0x80002738]:sw t5, 1480(ra)
[0x8000273c]:sw t6, 1488(ra)
[0x80002740]:sw t5, 1496(ra)
[0x80002744]:sw a7, 1504(ra)
[0x80002748]:lw t3, 1776(a6)
[0x8000274c]:lw t4, 1780(a6)
[0x80002750]:lw s10, 1784(a6)
[0x80002754]:lw s11, 1788(a6)
[0x80002758]:lui t3, 511400
[0x8000275c]:addi t3, t3, 83
[0x80002760]:lui t4, 523497
[0x80002764]:addi t4, t4, 2807
[0x80002768]:lui s10, 511400
[0x8000276c]:addi s10, s10, 83
[0x80002770]:lui s11, 1047785
[0x80002774]:addi s11, s11, 2807
[0x80002778]:addi a4, zero, 64
[0x8000277c]:csrrw zero, fcsr, a4
[0x80002780]:fadd.d t5, t3, s10, dyn
[0x80002784]:csrrs a7, fcsr, zero

[0x80002780]:fadd.d t5, t3, s10, dyn
[0x80002784]:csrrs a7, fcsr, zero
[0x80002788]:sw t5, 1512(ra)
[0x8000278c]:sw t6, 1520(ra)
[0x80002790]:sw t5, 1528(ra)
[0x80002794]:sw a7, 1536(ra)
[0x80002798]:lw t3, 1792(a6)
[0x8000279c]:lw t4, 1796(a6)
[0x800027a0]:lw s10, 1800(a6)
[0x800027a4]:lw s11, 1804(a6)
[0x800027a8]:lui t3, 511400
[0x800027ac]:addi t3, t3, 83
[0x800027b0]:lui t4, 523497
[0x800027b4]:addi t4, t4, 2807
[0x800027b8]:lui s10, 511400
[0x800027bc]:addi s10, s10, 83
[0x800027c0]:lui s11, 1047785
[0x800027c4]:addi s11, s11, 2807
[0x800027c8]:addi a4, zero, 96
[0x800027cc]:csrrw zero, fcsr, a4
[0x800027d0]:fadd.d t5, t3, s10, dyn
[0x800027d4]:csrrs a7, fcsr, zero

[0x800027d0]:fadd.d t5, t3, s10, dyn
[0x800027d4]:csrrs a7, fcsr, zero
[0x800027d8]:sw t5, 1544(ra)
[0x800027dc]:sw t6, 1552(ra)
[0x800027e0]:sw t5, 1560(ra)
[0x800027e4]:sw a7, 1568(ra)
[0x800027e8]:lw t3, 1808(a6)
[0x800027ec]:lw t4, 1812(a6)
[0x800027f0]:lw s10, 1816(a6)
[0x800027f4]:lw s11, 1820(a6)
[0x800027f8]:lui t3, 511400
[0x800027fc]:addi t3, t3, 83
[0x80002800]:lui t4, 523497
[0x80002804]:addi t4, t4, 2807
[0x80002808]:lui s10, 511400
[0x8000280c]:addi s10, s10, 83
[0x80002810]:lui s11, 1047785
[0x80002814]:addi s11, s11, 2807
[0x80002818]:addi a4, zero, 128
[0x8000281c]:csrrw zero, fcsr, a4
[0x80002820]:fadd.d t5, t3, s10, dyn
[0x80002824]:csrrs a7, fcsr, zero

[0x80002820]:fadd.d t5, t3, s10, dyn
[0x80002824]:csrrs a7, fcsr, zero
[0x80002828]:sw t5, 1576(ra)
[0x8000282c]:sw t6, 1584(ra)
[0x80002830]:sw t5, 1592(ra)
[0x80002834]:sw a7, 1600(ra)
[0x80002838]:lw t3, 1824(a6)
[0x8000283c]:lw t4, 1828(a6)
[0x80002840]:lw s10, 1832(a6)
[0x80002844]:lw s11, 1836(a6)
[0x80002848]:lui t3, 451264
[0x8000284c]:addi t3, t3, 1422
[0x80002850]:lui t4, 523931
[0x80002854]:addi t4, t4, 933
[0x80002858]:lui s10, 451264
[0x8000285c]:addi s10, s10, 1422
[0x80002860]:lui s11, 1048219
[0x80002864]:addi s11, s11, 933
[0x80002868]:addi a4, zero, 0
[0x8000286c]:csrrw zero, fcsr, a4
[0x80002870]:fadd.d t5, t3, s10, dyn
[0x80002874]:csrrs a7, fcsr, zero

[0x80002870]:fadd.d t5, t3, s10, dyn
[0x80002874]:csrrs a7, fcsr, zero
[0x80002878]:sw t5, 1608(ra)
[0x8000287c]:sw t6, 1616(ra)
[0x80002880]:sw t5, 1624(ra)
[0x80002884]:sw a7, 1632(ra)
[0x80002888]:lw t3, 1840(a6)
[0x8000288c]:lw t4, 1844(a6)
[0x80002890]:lw s10, 1848(a6)
[0x80002894]:lw s11, 1852(a6)
[0x80002898]:lui t3, 451264
[0x8000289c]:addi t3, t3, 1422
[0x800028a0]:lui t4, 523931
[0x800028a4]:addi t4, t4, 933
[0x800028a8]:lui s10, 451264
[0x800028ac]:addi s10, s10, 1422
[0x800028b0]:lui s11, 1048219
[0x800028b4]:addi s11, s11, 933
[0x800028b8]:addi a4, zero, 32
[0x800028bc]:csrrw zero, fcsr, a4
[0x800028c0]:fadd.d t5, t3, s10, dyn
[0x800028c4]:csrrs a7, fcsr, zero

[0x800028c0]:fadd.d t5, t3, s10, dyn
[0x800028c4]:csrrs a7, fcsr, zero
[0x800028c8]:sw t5, 1640(ra)
[0x800028cc]:sw t6, 1648(ra)
[0x800028d0]:sw t5, 1656(ra)
[0x800028d4]:sw a7, 1664(ra)
[0x800028d8]:lw t3, 1856(a6)
[0x800028dc]:lw t4, 1860(a6)
[0x800028e0]:lw s10, 1864(a6)
[0x800028e4]:lw s11, 1868(a6)
[0x800028e8]:lui t3, 451264
[0x800028ec]:addi t3, t3, 1422
[0x800028f0]:lui t4, 523931
[0x800028f4]:addi t4, t4, 933
[0x800028f8]:lui s10, 451264
[0x800028fc]:addi s10, s10, 1422
[0x80002900]:lui s11, 1048219
[0x80002904]:addi s11, s11, 933
[0x80002908]:addi a4, zero, 64
[0x8000290c]:csrrw zero, fcsr, a4
[0x80002910]:fadd.d t5, t3, s10, dyn
[0x80002914]:csrrs a7, fcsr, zero

[0x80002910]:fadd.d t5, t3, s10, dyn
[0x80002914]:csrrs a7, fcsr, zero
[0x80002918]:sw t5, 1672(ra)
[0x8000291c]:sw t6, 1680(ra)
[0x80002920]:sw t5, 1688(ra)
[0x80002924]:sw a7, 1696(ra)
[0x80002928]:lw t3, 1872(a6)
[0x8000292c]:lw t4, 1876(a6)
[0x80002930]:lw s10, 1880(a6)
[0x80002934]:lw s11, 1884(a6)
[0x80002938]:lui t3, 451264
[0x8000293c]:addi t3, t3, 1422
[0x80002940]:lui t4, 523931
[0x80002944]:addi t4, t4, 933
[0x80002948]:lui s10, 451264
[0x8000294c]:addi s10, s10, 1422
[0x80002950]:lui s11, 1048219
[0x80002954]:addi s11, s11, 933
[0x80002958]:addi a4, zero, 96
[0x8000295c]:csrrw zero, fcsr, a4
[0x80002960]:fadd.d t5, t3, s10, dyn
[0x80002964]:csrrs a7, fcsr, zero

[0x80002960]:fadd.d t5, t3, s10, dyn
[0x80002964]:csrrs a7, fcsr, zero
[0x80002968]:sw t5, 1704(ra)
[0x8000296c]:sw t6, 1712(ra)
[0x80002970]:sw t5, 1720(ra)
[0x80002974]:sw a7, 1728(ra)
[0x80002978]:lw t3, 1888(a6)
[0x8000297c]:lw t4, 1892(a6)
[0x80002980]:lw s10, 1896(a6)
[0x80002984]:lw s11, 1900(a6)
[0x80002988]:lui t3, 451264
[0x8000298c]:addi t3, t3, 1422
[0x80002990]:lui t4, 523931
[0x80002994]:addi t4, t4, 933
[0x80002998]:lui s10, 451264
[0x8000299c]:addi s10, s10, 1422
[0x800029a0]:lui s11, 1048219
[0x800029a4]:addi s11, s11, 933
[0x800029a8]:addi a4, zero, 128
[0x800029ac]:csrrw zero, fcsr, a4
[0x800029b0]:fadd.d t5, t3, s10, dyn
[0x800029b4]:csrrs a7, fcsr, zero

[0x800029b0]:fadd.d t5, t3, s10, dyn
[0x800029b4]:csrrs a7, fcsr, zero
[0x800029b8]:sw t5, 1736(ra)
[0x800029bc]:sw t6, 1744(ra)
[0x800029c0]:sw t5, 1752(ra)
[0x800029c4]:sw a7, 1760(ra)
[0x800029c8]:lw t3, 1904(a6)
[0x800029cc]:lw t4, 1908(a6)
[0x800029d0]:lw s10, 1912(a6)
[0x800029d4]:lw s11, 1916(a6)
[0x800029d8]:lui t3, 916616
[0x800029dc]:addi t3, t3, 2191
[0x800029e0]:lui t4, 523082
[0x800029e4]:addi t4, t4, 2072
[0x800029e8]:lui s10, 916616
[0x800029ec]:addi s10, s10, 2191
[0x800029f0]:lui s11, 1047370
[0x800029f4]:addi s11, s11, 2072
[0x800029f8]:addi a4, zero, 0
[0x800029fc]:csrrw zero, fcsr, a4
[0x80002a00]:fadd.d t5, t3, s10, dyn
[0x80002a04]:csrrs a7, fcsr, zero

[0x80002a00]:fadd.d t5, t3, s10, dyn
[0x80002a04]:csrrs a7, fcsr, zero
[0x80002a08]:sw t5, 1768(ra)
[0x80002a0c]:sw t6, 1776(ra)
[0x80002a10]:sw t5, 1784(ra)
[0x80002a14]:sw a7, 1792(ra)
[0x80002a18]:lw t3, 1920(a6)
[0x80002a1c]:lw t4, 1924(a6)
[0x80002a20]:lw s10, 1928(a6)
[0x80002a24]:lw s11, 1932(a6)
[0x80002a28]:lui t3, 916616
[0x80002a2c]:addi t3, t3, 2191
[0x80002a30]:lui t4, 523082
[0x80002a34]:addi t4, t4, 2072
[0x80002a38]:lui s10, 916616
[0x80002a3c]:addi s10, s10, 2191
[0x80002a40]:lui s11, 1047370
[0x80002a44]:addi s11, s11, 2072
[0x80002a48]:addi a4, zero, 32
[0x80002a4c]:csrrw zero, fcsr, a4
[0x80002a50]:fadd.d t5, t3, s10, dyn
[0x80002a54]:csrrs a7, fcsr, zero

[0x80002a50]:fadd.d t5, t3, s10, dyn
[0x80002a54]:csrrs a7, fcsr, zero
[0x80002a58]:sw t5, 1800(ra)
[0x80002a5c]:sw t6, 1808(ra)
[0x80002a60]:sw t5, 1816(ra)
[0x80002a64]:sw a7, 1824(ra)
[0x80002a68]:lw t3, 1936(a6)
[0x80002a6c]:lw t4, 1940(a6)
[0x80002a70]:lw s10, 1944(a6)
[0x80002a74]:lw s11, 1948(a6)
[0x80002a78]:lui t3, 916616
[0x80002a7c]:addi t3, t3, 2191
[0x80002a80]:lui t4, 523082
[0x80002a84]:addi t4, t4, 2072
[0x80002a88]:lui s10, 916616
[0x80002a8c]:addi s10, s10, 2191
[0x80002a90]:lui s11, 1047370
[0x80002a94]:addi s11, s11, 2072
[0x80002a98]:addi a4, zero, 64
[0x80002a9c]:csrrw zero, fcsr, a4
[0x80002aa0]:fadd.d t5, t3, s10, dyn
[0x80002aa4]:csrrs a7, fcsr, zero

[0x80002aa0]:fadd.d t5, t3, s10, dyn
[0x80002aa4]:csrrs a7, fcsr, zero
[0x80002aa8]:sw t5, 1832(ra)
[0x80002aac]:sw t6, 1840(ra)
[0x80002ab0]:sw t5, 1848(ra)
[0x80002ab4]:sw a7, 1856(ra)
[0x80002ab8]:lw t3, 1952(a6)
[0x80002abc]:lw t4, 1956(a6)
[0x80002ac0]:lw s10, 1960(a6)
[0x80002ac4]:lw s11, 1964(a6)
[0x80002ac8]:lui t3, 916616
[0x80002acc]:addi t3, t3, 2191
[0x80002ad0]:lui t4, 523082
[0x80002ad4]:addi t4, t4, 2072
[0x80002ad8]:lui s10, 916616
[0x80002adc]:addi s10, s10, 2191
[0x80002ae0]:lui s11, 1047370
[0x80002ae4]:addi s11, s11, 2072
[0x80002ae8]:addi a4, zero, 96
[0x80002aec]:csrrw zero, fcsr, a4
[0x80002af0]:fadd.d t5, t3, s10, dyn
[0x80002af4]:csrrs a7, fcsr, zero

[0x80002af0]:fadd.d t5, t3, s10, dyn
[0x80002af4]:csrrs a7, fcsr, zero
[0x80002af8]:sw t5, 1864(ra)
[0x80002afc]:sw t6, 1872(ra)
[0x80002b00]:sw t5, 1880(ra)
[0x80002b04]:sw a7, 1888(ra)
[0x80002b08]:lw t3, 1968(a6)
[0x80002b0c]:lw t4, 1972(a6)
[0x80002b10]:lw s10, 1976(a6)
[0x80002b14]:lw s11, 1980(a6)
[0x80002b18]:lui t3, 916616
[0x80002b1c]:addi t3, t3, 2191
[0x80002b20]:lui t4, 523082
[0x80002b24]:addi t4, t4, 2072
[0x80002b28]:lui s10, 916616
[0x80002b2c]:addi s10, s10, 2191
[0x80002b30]:lui s11, 1047370
[0x80002b34]:addi s11, s11, 2072
[0x80002b38]:addi a4, zero, 128
[0x80002b3c]:csrrw zero, fcsr, a4
[0x80002b40]:fadd.d t5, t3, s10, dyn
[0x80002b44]:csrrs a7, fcsr, zero

[0x80002b40]:fadd.d t5, t3, s10, dyn
[0x80002b44]:csrrs a7, fcsr, zero
[0x80002b48]:sw t5, 1896(ra)
[0x80002b4c]:sw t6, 1904(ra)
[0x80002b50]:sw t5, 1912(ra)
[0x80002b54]:sw a7, 1920(ra)
[0x80002b58]:lw t3, 1984(a6)
[0x80002b5c]:lw t4, 1988(a6)
[0x80002b60]:lw s10, 1992(a6)
[0x80002b64]:lw s11, 1996(a6)
[0x80002b68]:lui t3, 770015
[0x80002b6c]:addi t3, t3, 3141
[0x80002b70]:lui t4, 523780
[0x80002b74]:addi t4, t4, 268
[0x80002b78]:lui s10, 770015
[0x80002b7c]:addi s10, s10, 3141
[0x80002b80]:lui s11, 1048068
[0x80002b84]:addi s11, s11, 268
[0x80002b88]:addi a4, zero, 0
[0x80002b8c]:csrrw zero, fcsr, a4
[0x80002b90]:fadd.d t5, t3, s10, dyn
[0x80002b94]:csrrs a7, fcsr, zero

[0x80002b90]:fadd.d t5, t3, s10, dyn
[0x80002b94]:csrrs a7, fcsr, zero
[0x80002b98]:sw t5, 1928(ra)
[0x80002b9c]:sw t6, 1936(ra)
[0x80002ba0]:sw t5, 1944(ra)
[0x80002ba4]:sw a7, 1952(ra)
[0x80002ba8]:lw t3, 2000(a6)
[0x80002bac]:lw t4, 2004(a6)
[0x80002bb0]:lw s10, 2008(a6)
[0x80002bb4]:lw s11, 2012(a6)
[0x80002bb8]:lui t3, 770015
[0x80002bbc]:addi t3, t3, 3141
[0x80002bc0]:lui t4, 523780
[0x80002bc4]:addi t4, t4, 268
[0x80002bc8]:lui s10, 770015
[0x80002bcc]:addi s10, s10, 3141
[0x80002bd0]:lui s11, 1048068
[0x80002bd4]:addi s11, s11, 268
[0x80002bd8]:addi a4, zero, 32
[0x80002bdc]:csrrw zero, fcsr, a4
[0x80002be0]:fadd.d t5, t3, s10, dyn
[0x80002be4]:csrrs a7, fcsr, zero

[0x80002be0]:fadd.d t5, t3, s10, dyn
[0x80002be4]:csrrs a7, fcsr, zero
[0x80002be8]:sw t5, 1960(ra)
[0x80002bec]:sw t6, 1968(ra)
[0x80002bf0]:sw t5, 1976(ra)
[0x80002bf4]:sw a7, 1984(ra)
[0x80002bf8]:lw t3, 2016(a6)
[0x80002bfc]:lw t4, 2020(a6)
[0x80002c00]:lw s10, 2024(a6)
[0x80002c04]:lw s11, 2028(a6)
[0x80002c08]:lui t3, 770015
[0x80002c0c]:addi t3, t3, 3141
[0x80002c10]:lui t4, 523780
[0x80002c14]:addi t4, t4, 268
[0x80002c18]:lui s10, 770015
[0x80002c1c]:addi s10, s10, 3141
[0x80002c20]:lui s11, 1048068
[0x80002c24]:addi s11, s11, 268
[0x80002c28]:addi a4, zero, 64
[0x80002c2c]:csrrw zero, fcsr, a4
[0x80002c30]:fadd.d t5, t3, s10, dyn
[0x80002c34]:csrrs a7, fcsr, zero

[0x80002c30]:fadd.d t5, t3, s10, dyn
[0x80002c34]:csrrs a7, fcsr, zero
[0x80002c38]:sw t5, 1992(ra)
[0x80002c3c]:sw t6, 2000(ra)
[0x80002c40]:sw t5, 2008(ra)
[0x80002c44]:sw a7, 2016(ra)
[0x80002c48]:lw t3, 2032(a6)
[0x80002c4c]:lw t4, 2036(a6)
[0x80002c50]:lw s10, 2040(a6)
[0x80002c54]:lw s11, 2044(a6)
[0x80002c58]:lui t3, 770015
[0x80002c5c]:addi t3, t3, 3141
[0x80002c60]:lui t4, 523780
[0x80002c64]:addi t4, t4, 268
[0x80002c68]:lui s10, 770015
[0x80002c6c]:addi s10, s10, 3141
[0x80002c70]:lui s11, 1048068
[0x80002c74]:addi s11, s11, 268
[0x80002c78]:addi a4, zero, 96
[0x80002c7c]:csrrw zero, fcsr, a4
[0x80002c80]:fadd.d t5, t3, s10, dyn
[0x80002c84]:csrrs a7, fcsr, zero

[0x80002c80]:fadd.d t5, t3, s10, dyn
[0x80002c84]:csrrs a7, fcsr, zero
[0x80002c88]:sw t5, 2024(ra)
[0x80002c8c]:sw t6, 2032(ra)
[0x80002c90]:sw t5, 2040(ra)
[0x80002c94]:addi ra, ra, 2040
[0x80002c98]:sw a7, 8(ra)
[0x80002c9c]:lui a4, 1
[0x80002ca0]:addi a4, a4, 2048
[0x80002ca4]:add a6, a6, a4
[0x80002ca8]:lw t3, 0(a6)
[0x80002cac]:sub a6, a6, a4
[0x80002cb0]:lui a4, 1
[0x80002cb4]:addi a4, a4, 2048
[0x80002cb8]:add a6, a6, a4
[0x80002cbc]:lw t4, 4(a6)
[0x80002cc0]:sub a6, a6, a4
[0x80002cc4]:lui a4, 1
[0x80002cc8]:addi a4, a4, 2048
[0x80002ccc]:add a6, a6, a4
[0x80002cd0]:lw s10, 8(a6)
[0x80002cd4]:sub a6, a6, a4
[0x80002cd8]:lui a4, 1
[0x80002cdc]:addi a4, a4, 2048
[0x80002ce0]:add a6, a6, a4
[0x80002ce4]:lw s11, 12(a6)
[0x80002ce8]:sub a6, a6, a4
[0x80002cec]:lui t3, 770015
[0x80002cf0]:addi t3, t3, 3141
[0x80002cf4]:lui t4, 523780
[0x80002cf8]:addi t4, t4, 268
[0x80002cfc]:lui s10, 770015
[0x80002d00]:addi s10, s10, 3141
[0x80002d04]:lui s11, 1048068
[0x80002d08]:addi s11, s11, 268
[0x80002d0c]:addi a4, zero, 128
[0x80002d10]:csrrw zero, fcsr, a4
[0x80002d14]:fadd.d t5, t3, s10, dyn
[0x80002d18]:csrrs a7, fcsr, zero

[0x80002d14]:fadd.d t5, t3, s10, dyn
[0x80002d18]:csrrs a7, fcsr, zero
[0x80002d1c]:sw t5, 16(ra)
[0x80002d20]:sw t6, 24(ra)
[0x80002d24]:sw t5, 32(ra)
[0x80002d28]:sw a7, 40(ra)
[0x80002d2c]:lui a4, 1
[0x80002d30]:addi a4, a4, 2048
[0x80002d34]:add a6, a6, a4
[0x80002d38]:lw t3, 16(a6)
[0x80002d3c]:sub a6, a6, a4
[0x80002d40]:lui a4, 1
[0x80002d44]:addi a4, a4, 2048
[0x80002d48]:add a6, a6, a4
[0x80002d4c]:lw t4, 20(a6)
[0x80002d50]:sub a6, a6, a4
[0x80002d54]:lui a4, 1
[0x80002d58]:addi a4, a4, 2048
[0x80002d5c]:add a6, a6, a4
[0x80002d60]:lw s10, 24(a6)
[0x80002d64]:sub a6, a6, a4
[0x80002d68]:lui a4, 1
[0x80002d6c]:addi a4, a4, 2048
[0x80002d70]:add a6, a6, a4
[0x80002d74]:lw s11, 28(a6)
[0x80002d78]:sub a6, a6, a4
[0x80002d7c]:lui t3, 39511
[0x80002d80]:addi t3, t3, 951
[0x80002d84]:lui t4, 523199
[0x80002d88]:addi t4, t4, 2871
[0x80002d8c]:lui s10, 39511
[0x80002d90]:addi s10, s10, 951
[0x80002d94]:lui s11, 1047487
[0x80002d98]:addi s11, s11, 2871
[0x80002d9c]:addi a4, zero, 0
[0x80002da0]:csrrw zero, fcsr, a4
[0x80002da4]:fadd.d t5, t3, s10, dyn
[0x80002da8]:csrrs a7, fcsr, zero

[0x80002da4]:fadd.d t5, t3, s10, dyn
[0x80002da8]:csrrs a7, fcsr, zero
[0x80002dac]:sw t5, 48(ra)
[0x80002db0]:sw t6, 56(ra)
[0x80002db4]:sw t5, 64(ra)
[0x80002db8]:sw a7, 72(ra)
[0x80002dbc]:lui a4, 1
[0x80002dc0]:addi a4, a4, 2048
[0x80002dc4]:add a6, a6, a4
[0x80002dc8]:lw t3, 32(a6)
[0x80002dcc]:sub a6, a6, a4
[0x80002dd0]:lui a4, 1
[0x80002dd4]:addi a4, a4, 2048
[0x80002dd8]:add a6, a6, a4
[0x80002ddc]:lw t4, 36(a6)
[0x80002de0]:sub a6, a6, a4
[0x80002de4]:lui a4, 1
[0x80002de8]:addi a4, a4, 2048
[0x80002dec]:add a6, a6, a4
[0x80002df0]:lw s10, 40(a6)
[0x80002df4]:sub a6, a6, a4
[0x80002df8]:lui a4, 1
[0x80002dfc]:addi a4, a4, 2048
[0x80002e00]:add a6, a6, a4
[0x80002e04]:lw s11, 44(a6)
[0x80002e08]:sub a6, a6, a4
[0x80002e0c]:lui t3, 39511
[0x80002e10]:addi t3, t3, 951
[0x80002e14]:lui t4, 523199
[0x80002e18]:addi t4, t4, 2871
[0x80002e1c]:lui s10, 39511
[0x80002e20]:addi s10, s10, 951
[0x80002e24]:lui s11, 1047487
[0x80002e28]:addi s11, s11, 2871
[0x80002e2c]:addi a4, zero, 32
[0x80002e30]:csrrw zero, fcsr, a4
[0x80002e34]:fadd.d t5, t3, s10, dyn
[0x80002e38]:csrrs a7, fcsr, zero

[0x80002e34]:fadd.d t5, t3, s10, dyn
[0x80002e38]:csrrs a7, fcsr, zero
[0x80002e3c]:sw t5, 80(ra)
[0x80002e40]:sw t6, 88(ra)
[0x80002e44]:sw t5, 96(ra)
[0x80002e48]:sw a7, 104(ra)
[0x80002e4c]:lui a4, 1
[0x80002e50]:addi a4, a4, 2048
[0x80002e54]:add a6, a6, a4
[0x80002e58]:lw t3, 48(a6)
[0x80002e5c]:sub a6, a6, a4
[0x80002e60]:lui a4, 1
[0x80002e64]:addi a4, a4, 2048
[0x80002e68]:add a6, a6, a4
[0x80002e6c]:lw t4, 52(a6)
[0x80002e70]:sub a6, a6, a4
[0x80002e74]:lui a4, 1
[0x80002e78]:addi a4, a4, 2048
[0x80002e7c]:add a6, a6, a4
[0x80002e80]:lw s10, 56(a6)
[0x80002e84]:sub a6, a6, a4
[0x80002e88]:lui a4, 1
[0x80002e8c]:addi a4, a4, 2048
[0x80002e90]:add a6, a6, a4
[0x80002e94]:lw s11, 60(a6)
[0x80002e98]:sub a6, a6, a4
[0x80002e9c]:lui t3, 39511
[0x80002ea0]:addi t3, t3, 951
[0x80002ea4]:lui t4, 523199
[0x80002ea8]:addi t4, t4, 2871
[0x80002eac]:lui s10, 39511
[0x80002eb0]:addi s10, s10, 951
[0x80002eb4]:lui s11, 1047487
[0x80002eb8]:addi s11, s11, 2871
[0x80002ebc]:addi a4, zero, 64
[0x80002ec0]:csrrw zero, fcsr, a4
[0x80002ec4]:fadd.d t5, t3, s10, dyn
[0x80002ec8]:csrrs a7, fcsr, zero

[0x80002ec4]:fadd.d t5, t3, s10, dyn
[0x80002ec8]:csrrs a7, fcsr, zero
[0x80002ecc]:sw t5, 112(ra)
[0x80002ed0]:sw t6, 120(ra)
[0x80002ed4]:sw t5, 128(ra)
[0x80002ed8]:sw a7, 136(ra)
[0x80002edc]:lui a4, 1
[0x80002ee0]:addi a4, a4, 2048
[0x80002ee4]:add a6, a6, a4
[0x80002ee8]:lw t3, 64(a6)
[0x80002eec]:sub a6, a6, a4
[0x80002ef0]:lui a4, 1
[0x80002ef4]:addi a4, a4, 2048
[0x80002ef8]:add a6, a6, a4
[0x80002efc]:lw t4, 68(a6)
[0x80002f00]:sub a6, a6, a4
[0x80002f04]:lui a4, 1
[0x80002f08]:addi a4, a4, 2048
[0x80002f0c]:add a6, a6, a4
[0x80002f10]:lw s10, 72(a6)
[0x80002f14]:sub a6, a6, a4
[0x80002f18]:lui a4, 1
[0x80002f1c]:addi a4, a4, 2048
[0x80002f20]:add a6, a6, a4
[0x80002f24]:lw s11, 76(a6)
[0x80002f28]:sub a6, a6, a4
[0x80002f2c]:lui t3, 39511
[0x80002f30]:addi t3, t3, 951
[0x80002f34]:lui t4, 523199
[0x80002f38]:addi t4, t4, 2871
[0x80002f3c]:lui s10, 39511
[0x80002f40]:addi s10, s10, 951
[0x80002f44]:lui s11, 1047487
[0x80002f48]:addi s11, s11, 2871
[0x80002f4c]:addi a4, zero, 96
[0x80002f50]:csrrw zero, fcsr, a4
[0x80002f54]:fadd.d t5, t3, s10, dyn
[0x80002f58]:csrrs a7, fcsr, zero

[0x80002f54]:fadd.d t5, t3, s10, dyn
[0x80002f58]:csrrs a7, fcsr, zero
[0x80002f5c]:sw t5, 144(ra)
[0x80002f60]:sw t6, 152(ra)
[0x80002f64]:sw t5, 160(ra)
[0x80002f68]:sw a7, 168(ra)
[0x80002f6c]:lui a4, 1
[0x80002f70]:addi a4, a4, 2048
[0x80002f74]:add a6, a6, a4
[0x80002f78]:lw t3, 80(a6)
[0x80002f7c]:sub a6, a6, a4
[0x80002f80]:lui a4, 1
[0x80002f84]:addi a4, a4, 2048
[0x80002f88]:add a6, a6, a4
[0x80002f8c]:lw t4, 84(a6)
[0x80002f90]:sub a6, a6, a4
[0x80002f94]:lui a4, 1
[0x80002f98]:addi a4, a4, 2048
[0x80002f9c]:add a6, a6, a4
[0x80002fa0]:lw s10, 88(a6)
[0x80002fa4]:sub a6, a6, a4
[0x80002fa8]:lui a4, 1
[0x80002fac]:addi a4, a4, 2048
[0x80002fb0]:add a6, a6, a4
[0x80002fb4]:lw s11, 92(a6)
[0x80002fb8]:sub a6, a6, a4
[0x80002fbc]:lui t3, 39511
[0x80002fc0]:addi t3, t3, 951
[0x80002fc4]:lui t4, 523199
[0x80002fc8]:addi t4, t4, 2871
[0x80002fcc]:lui s10, 39511
[0x80002fd0]:addi s10, s10, 951
[0x80002fd4]:lui s11, 1047487
[0x80002fd8]:addi s11, s11, 2871
[0x80002fdc]:addi a4, zero, 128
[0x80002fe0]:csrrw zero, fcsr, a4
[0x80002fe4]:fadd.d t5, t3, s10, dyn
[0x80002fe8]:csrrs a7, fcsr, zero

[0x80002fe4]:fadd.d t5, t3, s10, dyn
[0x80002fe8]:csrrs a7, fcsr, zero
[0x80002fec]:sw t5, 176(ra)
[0x80002ff0]:sw t6, 184(ra)
[0x80002ff4]:sw t5, 192(ra)
[0x80002ff8]:sw a7, 200(ra)
[0x80002ffc]:lui a4, 1
[0x80003000]:addi a4, a4, 2048
[0x80003004]:add a6, a6, a4
[0x80003008]:lw t3, 96(a6)
[0x8000300c]:sub a6, a6, a4
[0x80003010]:lui a4, 1
[0x80003014]:addi a4, a4, 2048
[0x80003018]:add a6, a6, a4
[0x8000301c]:lw t4, 100(a6)
[0x80003020]:sub a6, a6, a4
[0x80003024]:lui a4, 1
[0x80003028]:addi a4, a4, 2048
[0x8000302c]:add a6, a6, a4
[0x80003030]:lw s10, 104(a6)
[0x80003034]:sub a6, a6, a4
[0x80003038]:lui a4, 1
[0x8000303c]:addi a4, a4, 2048
[0x80003040]:add a6, a6, a4
[0x80003044]:lw s11, 108(a6)
[0x80003048]:sub a6, a6, a4
[0x8000304c]:lui t3, 706557
[0x80003050]:addi t3, t3, 3680
[0x80003054]:lui t4, 523882
[0x80003058]:addi t4, t4, 3110
[0x8000305c]:lui s10, 706557
[0x80003060]:addi s10, s10, 3680
[0x80003064]:lui s11, 1048170
[0x80003068]:addi s11, s11, 3110
[0x8000306c]:addi a4, zero, 0
[0x80003070]:csrrw zero, fcsr, a4
[0x80003074]:fadd.d t5, t3, s10, dyn
[0x80003078]:csrrs a7, fcsr, zero

[0x80003074]:fadd.d t5, t3, s10, dyn
[0x80003078]:csrrs a7, fcsr, zero
[0x8000307c]:sw t5, 208(ra)
[0x80003080]:sw t6, 216(ra)
[0x80003084]:sw t5, 224(ra)
[0x80003088]:sw a7, 232(ra)
[0x8000308c]:lui a4, 1
[0x80003090]:addi a4, a4, 2048
[0x80003094]:add a6, a6, a4
[0x80003098]:lw t3, 112(a6)
[0x8000309c]:sub a6, a6, a4
[0x800030a0]:lui a4, 1
[0x800030a4]:addi a4, a4, 2048
[0x800030a8]:add a6, a6, a4
[0x800030ac]:lw t4, 116(a6)
[0x800030b0]:sub a6, a6, a4
[0x800030b4]:lui a4, 1
[0x800030b8]:addi a4, a4, 2048
[0x800030bc]:add a6, a6, a4
[0x800030c0]:lw s10, 120(a6)
[0x800030c4]:sub a6, a6, a4
[0x800030c8]:lui a4, 1
[0x800030cc]:addi a4, a4, 2048
[0x800030d0]:add a6, a6, a4
[0x800030d4]:lw s11, 124(a6)
[0x800030d8]:sub a6, a6, a4
[0x800030dc]:lui t3, 706557
[0x800030e0]:addi t3, t3, 3680
[0x800030e4]:lui t4, 523882
[0x800030e8]:addi t4, t4, 3110
[0x800030ec]:lui s10, 706557
[0x800030f0]:addi s10, s10, 3680
[0x800030f4]:lui s11, 1048170
[0x800030f8]:addi s11, s11, 3110
[0x800030fc]:addi a4, zero, 32
[0x80003100]:csrrw zero, fcsr, a4
[0x80003104]:fadd.d t5, t3, s10, dyn
[0x80003108]:csrrs a7, fcsr, zero

[0x80003104]:fadd.d t5, t3, s10, dyn
[0x80003108]:csrrs a7, fcsr, zero
[0x8000310c]:sw t5, 240(ra)
[0x80003110]:sw t6, 248(ra)
[0x80003114]:sw t5, 256(ra)
[0x80003118]:sw a7, 264(ra)
[0x8000311c]:lui a4, 1
[0x80003120]:addi a4, a4, 2048
[0x80003124]:add a6, a6, a4
[0x80003128]:lw t3, 128(a6)
[0x8000312c]:sub a6, a6, a4
[0x80003130]:lui a4, 1
[0x80003134]:addi a4, a4, 2048
[0x80003138]:add a6, a6, a4
[0x8000313c]:lw t4, 132(a6)
[0x80003140]:sub a6, a6, a4
[0x80003144]:lui a4, 1
[0x80003148]:addi a4, a4, 2048
[0x8000314c]:add a6, a6, a4
[0x80003150]:lw s10, 136(a6)
[0x80003154]:sub a6, a6, a4
[0x80003158]:lui a4, 1
[0x8000315c]:addi a4, a4, 2048
[0x80003160]:add a6, a6, a4
[0x80003164]:lw s11, 140(a6)
[0x80003168]:sub a6, a6, a4
[0x8000316c]:lui t3, 706557
[0x80003170]:addi t3, t3, 3680
[0x80003174]:lui t4, 523882
[0x80003178]:addi t4, t4, 3110
[0x8000317c]:lui s10, 706557
[0x80003180]:addi s10, s10, 3680
[0x80003184]:lui s11, 1048170
[0x80003188]:addi s11, s11, 3110
[0x8000318c]:addi a4, zero, 64
[0x80003190]:csrrw zero, fcsr, a4
[0x80003194]:fadd.d t5, t3, s10, dyn
[0x80003198]:csrrs a7, fcsr, zero

[0x80003194]:fadd.d t5, t3, s10, dyn
[0x80003198]:csrrs a7, fcsr, zero
[0x8000319c]:sw t5, 272(ra)
[0x800031a0]:sw t6, 280(ra)
[0x800031a4]:sw t5, 288(ra)
[0x800031a8]:sw a7, 296(ra)
[0x800031ac]:lui a4, 1
[0x800031b0]:addi a4, a4, 2048
[0x800031b4]:add a6, a6, a4
[0x800031b8]:lw t3, 144(a6)
[0x800031bc]:sub a6, a6, a4
[0x800031c0]:lui a4, 1
[0x800031c4]:addi a4, a4, 2048
[0x800031c8]:add a6, a6, a4
[0x800031cc]:lw t4, 148(a6)
[0x800031d0]:sub a6, a6, a4
[0x800031d4]:lui a4, 1
[0x800031d8]:addi a4, a4, 2048
[0x800031dc]:add a6, a6, a4
[0x800031e0]:lw s10, 152(a6)
[0x800031e4]:sub a6, a6, a4
[0x800031e8]:lui a4, 1
[0x800031ec]:addi a4, a4, 2048
[0x800031f0]:add a6, a6, a4
[0x800031f4]:lw s11, 156(a6)
[0x800031f8]:sub a6, a6, a4
[0x800031fc]:lui t3, 706557
[0x80003200]:addi t3, t3, 3680
[0x80003204]:lui t4, 523882
[0x80003208]:addi t4, t4, 3110
[0x8000320c]:lui s10, 706557
[0x80003210]:addi s10, s10, 3680
[0x80003214]:lui s11, 1048170
[0x80003218]:addi s11, s11, 3110
[0x8000321c]:addi a4, zero, 96
[0x80003220]:csrrw zero, fcsr, a4
[0x80003224]:fadd.d t5, t3, s10, dyn
[0x80003228]:csrrs a7, fcsr, zero

[0x80003224]:fadd.d t5, t3, s10, dyn
[0x80003228]:csrrs a7, fcsr, zero
[0x8000322c]:sw t5, 304(ra)
[0x80003230]:sw t6, 312(ra)
[0x80003234]:sw t5, 320(ra)
[0x80003238]:sw a7, 328(ra)
[0x8000323c]:lui a4, 1
[0x80003240]:addi a4, a4, 2048
[0x80003244]:add a6, a6, a4
[0x80003248]:lw t3, 160(a6)
[0x8000324c]:sub a6, a6, a4
[0x80003250]:lui a4, 1
[0x80003254]:addi a4, a4, 2048
[0x80003258]:add a6, a6, a4
[0x8000325c]:lw t4, 164(a6)
[0x80003260]:sub a6, a6, a4
[0x80003264]:lui a4, 1
[0x80003268]:addi a4, a4, 2048
[0x8000326c]:add a6, a6, a4
[0x80003270]:lw s10, 168(a6)
[0x80003274]:sub a6, a6, a4
[0x80003278]:lui a4, 1
[0x8000327c]:addi a4, a4, 2048
[0x80003280]:add a6, a6, a4
[0x80003284]:lw s11, 172(a6)
[0x80003288]:sub a6, a6, a4
[0x8000328c]:lui t3, 706557
[0x80003290]:addi t3, t3, 3680
[0x80003294]:lui t4, 523882
[0x80003298]:addi t4, t4, 3110
[0x8000329c]:lui s10, 706557
[0x800032a0]:addi s10, s10, 3680
[0x800032a4]:lui s11, 1048170
[0x800032a8]:addi s11, s11, 3110
[0x800032ac]:addi a4, zero, 128
[0x800032b0]:csrrw zero, fcsr, a4
[0x800032b4]:fadd.d t5, t3, s10, dyn
[0x800032b8]:csrrs a7, fcsr, zero

[0x800032b4]:fadd.d t5, t3, s10, dyn
[0x800032b8]:csrrs a7, fcsr, zero
[0x800032bc]:sw t5, 336(ra)
[0x800032c0]:sw t6, 344(ra)
[0x800032c4]:sw t5, 352(ra)
[0x800032c8]:sw a7, 360(ra)
[0x800032cc]:lui a4, 1
[0x800032d0]:addi a4, a4, 2048
[0x800032d4]:add a6, a6, a4
[0x800032d8]:lw t3, 176(a6)
[0x800032dc]:sub a6, a6, a4
[0x800032e0]:lui a4, 1
[0x800032e4]:addi a4, a4, 2048
[0x800032e8]:add a6, a6, a4
[0x800032ec]:lw t4, 180(a6)
[0x800032f0]:sub a6, a6, a4
[0x800032f4]:lui a4, 1
[0x800032f8]:addi a4, a4, 2048
[0x800032fc]:add a6, a6, a4
[0x80003300]:lw s10, 184(a6)
[0x80003304]:sub a6, a6, a4
[0x80003308]:lui a4, 1
[0x8000330c]:addi a4, a4, 2048
[0x80003310]:add a6, a6, a4
[0x80003314]:lw s11, 188(a6)
[0x80003318]:sub a6, a6, a4
[0x8000331c]:lui t3, 850694
[0x80003320]:addi t3, t3, 570
[0x80003324]:lui t4, 523937
[0x80003328]:addi t4, t4, 28
[0x8000332c]:lui s10, 850694
[0x80003330]:addi s10, s10, 570
[0x80003334]:lui s11, 1048225
[0x80003338]:addi s11, s11, 28
[0x8000333c]:addi a4, zero, 0
[0x80003340]:csrrw zero, fcsr, a4
[0x80003344]:fadd.d t5, t3, s10, dyn
[0x80003348]:csrrs a7, fcsr, zero

[0x80003344]:fadd.d t5, t3, s10, dyn
[0x80003348]:csrrs a7, fcsr, zero
[0x8000334c]:sw t5, 368(ra)
[0x80003350]:sw t6, 376(ra)
[0x80003354]:sw t5, 384(ra)
[0x80003358]:sw a7, 392(ra)
[0x8000335c]:lui a4, 1
[0x80003360]:addi a4, a4, 2048
[0x80003364]:add a6, a6, a4
[0x80003368]:lw t3, 192(a6)
[0x8000336c]:sub a6, a6, a4
[0x80003370]:lui a4, 1
[0x80003374]:addi a4, a4, 2048
[0x80003378]:add a6, a6, a4
[0x8000337c]:lw t4, 196(a6)
[0x80003380]:sub a6, a6, a4
[0x80003384]:lui a4, 1
[0x80003388]:addi a4, a4, 2048
[0x8000338c]:add a6, a6, a4
[0x80003390]:lw s10, 200(a6)
[0x80003394]:sub a6, a6, a4
[0x80003398]:lui a4, 1
[0x8000339c]:addi a4, a4, 2048
[0x800033a0]:add a6, a6, a4
[0x800033a4]:lw s11, 204(a6)
[0x800033a8]:sub a6, a6, a4
[0x800033ac]:lui t3, 850694
[0x800033b0]:addi t3, t3, 570
[0x800033b4]:lui t4, 523937
[0x800033b8]:addi t4, t4, 28
[0x800033bc]:lui s10, 850694
[0x800033c0]:addi s10, s10, 570
[0x800033c4]:lui s11, 1048225
[0x800033c8]:addi s11, s11, 28
[0x800033cc]:addi a4, zero, 32
[0x800033d0]:csrrw zero, fcsr, a4
[0x800033d4]:fadd.d t5, t3, s10, dyn
[0x800033d8]:csrrs a7, fcsr, zero

[0x800033d4]:fadd.d t5, t3, s10, dyn
[0x800033d8]:csrrs a7, fcsr, zero
[0x800033dc]:sw t5, 400(ra)
[0x800033e0]:sw t6, 408(ra)
[0x800033e4]:sw t5, 416(ra)
[0x800033e8]:sw a7, 424(ra)
[0x800033ec]:lui a4, 1
[0x800033f0]:addi a4, a4, 2048
[0x800033f4]:add a6, a6, a4
[0x800033f8]:lw t3, 208(a6)
[0x800033fc]:sub a6, a6, a4
[0x80003400]:lui a4, 1
[0x80003404]:addi a4, a4, 2048
[0x80003408]:add a6, a6, a4
[0x8000340c]:lw t4, 212(a6)
[0x80003410]:sub a6, a6, a4
[0x80003414]:lui a4, 1
[0x80003418]:addi a4, a4, 2048
[0x8000341c]:add a6, a6, a4
[0x80003420]:lw s10, 216(a6)
[0x80003424]:sub a6, a6, a4
[0x80003428]:lui a4, 1
[0x8000342c]:addi a4, a4, 2048
[0x80003430]:add a6, a6, a4
[0x80003434]:lw s11, 220(a6)
[0x80003438]:sub a6, a6, a4
[0x8000343c]:lui t3, 850694
[0x80003440]:addi t3, t3, 570
[0x80003444]:lui t4, 523937
[0x80003448]:addi t4, t4, 28
[0x8000344c]:lui s10, 850694
[0x80003450]:addi s10, s10, 570
[0x80003454]:lui s11, 1048225
[0x80003458]:addi s11, s11, 28
[0x8000345c]:addi a4, zero, 64
[0x80003460]:csrrw zero, fcsr, a4
[0x80003464]:fadd.d t5, t3, s10, dyn
[0x80003468]:csrrs a7, fcsr, zero

[0x80003464]:fadd.d t5, t3, s10, dyn
[0x80003468]:csrrs a7, fcsr, zero
[0x8000346c]:sw t5, 432(ra)
[0x80003470]:sw t6, 440(ra)
[0x80003474]:sw t5, 448(ra)
[0x80003478]:sw a7, 456(ra)
[0x8000347c]:lui a4, 1
[0x80003480]:addi a4, a4, 2048
[0x80003484]:add a6, a6, a4
[0x80003488]:lw t3, 224(a6)
[0x8000348c]:sub a6, a6, a4
[0x80003490]:lui a4, 1
[0x80003494]:addi a4, a4, 2048
[0x80003498]:add a6, a6, a4
[0x8000349c]:lw t4, 228(a6)
[0x800034a0]:sub a6, a6, a4
[0x800034a4]:lui a4, 1
[0x800034a8]:addi a4, a4, 2048
[0x800034ac]:add a6, a6, a4
[0x800034b0]:lw s10, 232(a6)
[0x800034b4]:sub a6, a6, a4
[0x800034b8]:lui a4, 1
[0x800034bc]:addi a4, a4, 2048
[0x800034c0]:add a6, a6, a4
[0x800034c4]:lw s11, 236(a6)
[0x800034c8]:sub a6, a6, a4
[0x800034cc]:lui t3, 850694
[0x800034d0]:addi t3, t3, 570
[0x800034d4]:lui t4, 523937
[0x800034d8]:addi t4, t4, 28
[0x800034dc]:lui s10, 850694
[0x800034e0]:addi s10, s10, 570
[0x800034e4]:lui s11, 1048225
[0x800034e8]:addi s11, s11, 28
[0x800034ec]:addi a4, zero, 96
[0x800034f0]:csrrw zero, fcsr, a4
[0x800034f4]:fadd.d t5, t3, s10, dyn
[0x800034f8]:csrrs a7, fcsr, zero

[0x800034f4]:fadd.d t5, t3, s10, dyn
[0x800034f8]:csrrs a7, fcsr, zero
[0x800034fc]:sw t5, 464(ra)
[0x80003500]:sw t6, 472(ra)
[0x80003504]:sw t5, 480(ra)
[0x80003508]:sw a7, 488(ra)
[0x8000350c]:lui a4, 1
[0x80003510]:addi a4, a4, 2048
[0x80003514]:add a6, a6, a4
[0x80003518]:lw t3, 240(a6)
[0x8000351c]:sub a6, a6, a4
[0x80003520]:lui a4, 1
[0x80003524]:addi a4, a4, 2048
[0x80003528]:add a6, a6, a4
[0x8000352c]:lw t4, 244(a6)
[0x80003530]:sub a6, a6, a4
[0x80003534]:lui a4, 1
[0x80003538]:addi a4, a4, 2048
[0x8000353c]:add a6, a6, a4
[0x80003540]:lw s10, 248(a6)
[0x80003544]:sub a6, a6, a4
[0x80003548]:lui a4, 1
[0x8000354c]:addi a4, a4, 2048
[0x80003550]:add a6, a6, a4
[0x80003554]:lw s11, 252(a6)
[0x80003558]:sub a6, a6, a4
[0x8000355c]:lui t3, 850694
[0x80003560]:addi t3, t3, 570
[0x80003564]:lui t4, 523937
[0x80003568]:addi t4, t4, 28
[0x8000356c]:lui s10, 850694
[0x80003570]:addi s10, s10, 570
[0x80003574]:lui s11, 1048225
[0x80003578]:addi s11, s11, 28
[0x8000357c]:addi a4, zero, 128
[0x80003580]:csrrw zero, fcsr, a4
[0x80003584]:fadd.d t5, t3, s10, dyn
[0x80003588]:csrrs a7, fcsr, zero

[0x80003584]:fadd.d t5, t3, s10, dyn
[0x80003588]:csrrs a7, fcsr, zero
[0x8000358c]:sw t5, 496(ra)
[0x80003590]:sw t6, 504(ra)
[0x80003594]:sw t5, 512(ra)
[0x80003598]:sw a7, 520(ra)
[0x8000359c]:lui a4, 1
[0x800035a0]:addi a4, a4, 2048
[0x800035a4]:add a6, a6, a4
[0x800035a8]:lw t3, 256(a6)
[0x800035ac]:sub a6, a6, a4
[0x800035b0]:lui a4, 1
[0x800035b4]:addi a4, a4, 2048
[0x800035b8]:add a6, a6, a4
[0x800035bc]:lw t4, 260(a6)
[0x800035c0]:sub a6, a6, a4
[0x800035c4]:lui a4, 1
[0x800035c8]:addi a4, a4, 2048
[0x800035cc]:add a6, a6, a4
[0x800035d0]:lw s10, 264(a6)
[0x800035d4]:sub a6, a6, a4
[0x800035d8]:lui a4, 1
[0x800035dc]:addi a4, a4, 2048
[0x800035e0]:add a6, a6, a4
[0x800035e4]:lw s11, 268(a6)
[0x800035e8]:sub a6, a6, a4
[0x800035ec]:lui t3, 979754
[0x800035f0]:addi t3, t3, 3332
[0x800035f4]:lui t4, 524013
[0x800035f8]:addi t4, t4, 1987
[0x800035fc]:lui s10, 979754
[0x80003600]:addi s10, s10, 3332
[0x80003604]:lui s11, 1048301
[0x80003608]:addi s11, s11, 1987
[0x8000360c]:addi a4, zero, 0
[0x80003610]:csrrw zero, fcsr, a4
[0x80003614]:fadd.d t5, t3, s10, dyn
[0x80003618]:csrrs a7, fcsr, zero

[0x80003614]:fadd.d t5, t3, s10, dyn
[0x80003618]:csrrs a7, fcsr, zero
[0x8000361c]:sw t5, 528(ra)
[0x80003620]:sw t6, 536(ra)
[0x80003624]:sw t5, 544(ra)
[0x80003628]:sw a7, 552(ra)
[0x8000362c]:lui a4, 1
[0x80003630]:addi a4, a4, 2048
[0x80003634]:add a6, a6, a4
[0x80003638]:lw t3, 272(a6)
[0x8000363c]:sub a6, a6, a4
[0x80003640]:lui a4, 1
[0x80003644]:addi a4, a4, 2048
[0x80003648]:add a6, a6, a4
[0x8000364c]:lw t4, 276(a6)
[0x80003650]:sub a6, a6, a4
[0x80003654]:lui a4, 1
[0x80003658]:addi a4, a4, 2048
[0x8000365c]:add a6, a6, a4
[0x80003660]:lw s10, 280(a6)
[0x80003664]:sub a6, a6, a4
[0x80003668]:lui a4, 1
[0x8000366c]:addi a4, a4, 2048
[0x80003670]:add a6, a6, a4
[0x80003674]:lw s11, 284(a6)
[0x80003678]:sub a6, a6, a4
[0x8000367c]:lui t3, 979754
[0x80003680]:addi t3, t3, 3332
[0x80003684]:lui t4, 524013
[0x80003688]:addi t4, t4, 1987
[0x8000368c]:lui s10, 979754
[0x80003690]:addi s10, s10, 3332
[0x80003694]:lui s11, 1048301
[0x80003698]:addi s11, s11, 1987
[0x8000369c]:addi a4, zero, 32
[0x800036a0]:csrrw zero, fcsr, a4
[0x800036a4]:fadd.d t5, t3, s10, dyn
[0x800036a8]:csrrs a7, fcsr, zero

[0x800036a4]:fadd.d t5, t3, s10, dyn
[0x800036a8]:csrrs a7, fcsr, zero
[0x800036ac]:sw t5, 560(ra)
[0x800036b0]:sw t6, 568(ra)
[0x800036b4]:sw t5, 576(ra)
[0x800036b8]:sw a7, 584(ra)
[0x800036bc]:lui a4, 1
[0x800036c0]:addi a4, a4, 2048
[0x800036c4]:add a6, a6, a4
[0x800036c8]:lw t3, 288(a6)
[0x800036cc]:sub a6, a6, a4
[0x800036d0]:lui a4, 1
[0x800036d4]:addi a4, a4, 2048
[0x800036d8]:add a6, a6, a4
[0x800036dc]:lw t4, 292(a6)
[0x800036e0]:sub a6, a6, a4
[0x800036e4]:lui a4, 1
[0x800036e8]:addi a4, a4, 2048
[0x800036ec]:add a6, a6, a4
[0x800036f0]:lw s10, 296(a6)
[0x800036f4]:sub a6, a6, a4
[0x800036f8]:lui a4, 1
[0x800036fc]:addi a4, a4, 2048
[0x80003700]:add a6, a6, a4
[0x80003704]:lw s11, 300(a6)
[0x80003708]:sub a6, a6, a4
[0x8000370c]:lui t3, 979754
[0x80003710]:addi t3, t3, 3332
[0x80003714]:lui t4, 524013
[0x80003718]:addi t4, t4, 1987
[0x8000371c]:lui s10, 979754
[0x80003720]:addi s10, s10, 3332
[0x80003724]:lui s11, 1048301
[0x80003728]:addi s11, s11, 1987
[0x8000372c]:addi a4, zero, 64
[0x80003730]:csrrw zero, fcsr, a4
[0x80003734]:fadd.d t5, t3, s10, dyn
[0x80003738]:csrrs a7, fcsr, zero

[0x80003734]:fadd.d t5, t3, s10, dyn
[0x80003738]:csrrs a7, fcsr, zero
[0x8000373c]:sw t5, 592(ra)
[0x80003740]:sw t6, 600(ra)
[0x80003744]:sw t5, 608(ra)
[0x80003748]:sw a7, 616(ra)
[0x8000374c]:lui a4, 1
[0x80003750]:addi a4, a4, 2048
[0x80003754]:add a6, a6, a4
[0x80003758]:lw t3, 304(a6)
[0x8000375c]:sub a6, a6, a4
[0x80003760]:lui a4, 1
[0x80003764]:addi a4, a4, 2048
[0x80003768]:add a6, a6, a4
[0x8000376c]:lw t4, 308(a6)
[0x80003770]:sub a6, a6, a4
[0x80003774]:lui a4, 1
[0x80003778]:addi a4, a4, 2048
[0x8000377c]:add a6, a6, a4
[0x80003780]:lw s10, 312(a6)
[0x80003784]:sub a6, a6, a4
[0x80003788]:lui a4, 1
[0x8000378c]:addi a4, a4, 2048
[0x80003790]:add a6, a6, a4
[0x80003794]:lw s11, 316(a6)
[0x80003798]:sub a6, a6, a4
[0x8000379c]:lui t3, 979754
[0x800037a0]:addi t3, t3, 3332
[0x800037a4]:lui t4, 524013
[0x800037a8]:addi t4, t4, 1987
[0x800037ac]:lui s10, 979754
[0x800037b0]:addi s10, s10, 3332
[0x800037b4]:lui s11, 1048301
[0x800037b8]:addi s11, s11, 1987
[0x800037bc]:addi a4, zero, 96
[0x800037c0]:csrrw zero, fcsr, a4
[0x800037c4]:fadd.d t5, t3, s10, dyn
[0x800037c8]:csrrs a7, fcsr, zero

[0x800037c4]:fadd.d t5, t3, s10, dyn
[0x800037c8]:csrrs a7, fcsr, zero
[0x800037cc]:sw t5, 624(ra)
[0x800037d0]:sw t6, 632(ra)
[0x800037d4]:sw t5, 640(ra)
[0x800037d8]:sw a7, 648(ra)
[0x800037dc]:lui a4, 1
[0x800037e0]:addi a4, a4, 2048
[0x800037e4]:add a6, a6, a4
[0x800037e8]:lw t3, 320(a6)
[0x800037ec]:sub a6, a6, a4
[0x800037f0]:lui a4, 1
[0x800037f4]:addi a4, a4, 2048
[0x800037f8]:add a6, a6, a4
[0x800037fc]:lw t4, 324(a6)
[0x80003800]:sub a6, a6, a4
[0x80003804]:lui a4, 1
[0x80003808]:addi a4, a4, 2048
[0x8000380c]:add a6, a6, a4
[0x80003810]:lw s10, 328(a6)
[0x80003814]:sub a6, a6, a4
[0x80003818]:lui a4, 1
[0x8000381c]:addi a4, a4, 2048
[0x80003820]:add a6, a6, a4
[0x80003824]:lw s11, 332(a6)
[0x80003828]:sub a6, a6, a4
[0x8000382c]:lui t3, 979754
[0x80003830]:addi t3, t3, 3332
[0x80003834]:lui t4, 524013
[0x80003838]:addi t4, t4, 1987
[0x8000383c]:lui s10, 979754
[0x80003840]:addi s10, s10, 3332
[0x80003844]:lui s11, 1048301
[0x80003848]:addi s11, s11, 1987
[0x8000384c]:addi a4, zero, 128
[0x80003850]:csrrw zero, fcsr, a4
[0x80003854]:fadd.d t5, t3, s10, dyn
[0x80003858]:csrrs a7, fcsr, zero

[0x80003854]:fadd.d t5, t3, s10, dyn
[0x80003858]:csrrs a7, fcsr, zero
[0x8000385c]:sw t5, 656(ra)
[0x80003860]:sw t6, 664(ra)
[0x80003864]:sw t5, 672(ra)
[0x80003868]:sw a7, 680(ra)
[0x8000386c]:lui a4, 1
[0x80003870]:addi a4, a4, 2048
[0x80003874]:add a6, a6, a4
[0x80003878]:lw t3, 336(a6)
[0x8000387c]:sub a6, a6, a4
[0x80003880]:lui a4, 1
[0x80003884]:addi a4, a4, 2048
[0x80003888]:add a6, a6, a4
[0x8000388c]:lw t4, 340(a6)
[0x80003890]:sub a6, a6, a4
[0x80003894]:lui a4, 1
[0x80003898]:addi a4, a4, 2048
[0x8000389c]:add a6, a6, a4
[0x800038a0]:lw s10, 344(a6)
[0x800038a4]:sub a6, a6, a4
[0x800038a8]:lui a4, 1
[0x800038ac]:addi a4, a4, 2048
[0x800038b0]:add a6, a6, a4
[0x800038b4]:lw s11, 348(a6)
[0x800038b8]:sub a6, a6, a4
[0x800038bc]:lui t3, 316009
[0x800038c0]:addi t3, t3, 3999
[0x800038c4]:lui t4, 523821
[0x800038c8]:addi t4, t4, 3522
[0x800038cc]:lui s10, 316009
[0x800038d0]:addi s10, s10, 3999
[0x800038d4]:lui s11, 1048109
[0x800038d8]:addi s11, s11, 3522
[0x800038dc]:addi a4, zero, 0
[0x800038e0]:csrrw zero, fcsr, a4
[0x800038e4]:fadd.d t5, t3, s10, dyn
[0x800038e8]:csrrs a7, fcsr, zero

[0x800038e4]:fadd.d t5, t3, s10, dyn
[0x800038e8]:csrrs a7, fcsr, zero
[0x800038ec]:sw t5, 688(ra)
[0x800038f0]:sw t6, 696(ra)
[0x800038f4]:sw t5, 704(ra)
[0x800038f8]:sw a7, 712(ra)
[0x800038fc]:lui a4, 1
[0x80003900]:addi a4, a4, 2048
[0x80003904]:add a6, a6, a4
[0x80003908]:lw t3, 352(a6)
[0x8000390c]:sub a6, a6, a4
[0x80003910]:lui a4, 1
[0x80003914]:addi a4, a4, 2048
[0x80003918]:add a6, a6, a4
[0x8000391c]:lw t4, 356(a6)
[0x80003920]:sub a6, a6, a4
[0x80003924]:lui a4, 1
[0x80003928]:addi a4, a4, 2048
[0x8000392c]:add a6, a6, a4
[0x80003930]:lw s10, 360(a6)
[0x80003934]:sub a6, a6, a4
[0x80003938]:lui a4, 1
[0x8000393c]:addi a4, a4, 2048
[0x80003940]:add a6, a6, a4
[0x80003944]:lw s11, 364(a6)
[0x80003948]:sub a6, a6, a4
[0x8000394c]:lui t3, 316009
[0x80003950]:addi t3, t3, 3999
[0x80003954]:lui t4, 523821
[0x80003958]:addi t4, t4, 3522
[0x8000395c]:lui s10, 316009
[0x80003960]:addi s10, s10, 3999
[0x80003964]:lui s11, 1048109
[0x80003968]:addi s11, s11, 3522
[0x8000396c]:addi a4, zero, 32
[0x80003970]:csrrw zero, fcsr, a4
[0x80003974]:fadd.d t5, t3, s10, dyn
[0x80003978]:csrrs a7, fcsr, zero

[0x80003974]:fadd.d t5, t3, s10, dyn
[0x80003978]:csrrs a7, fcsr, zero
[0x8000397c]:sw t5, 720(ra)
[0x80003980]:sw t6, 728(ra)
[0x80003984]:sw t5, 736(ra)
[0x80003988]:sw a7, 744(ra)
[0x8000398c]:lui a4, 1
[0x80003990]:addi a4, a4, 2048
[0x80003994]:add a6, a6, a4
[0x80003998]:lw t3, 368(a6)
[0x8000399c]:sub a6, a6, a4
[0x800039a0]:lui a4, 1
[0x800039a4]:addi a4, a4, 2048
[0x800039a8]:add a6, a6, a4
[0x800039ac]:lw t4, 372(a6)
[0x800039b0]:sub a6, a6, a4
[0x800039b4]:lui a4, 1
[0x800039b8]:addi a4, a4, 2048
[0x800039bc]:add a6, a6, a4
[0x800039c0]:lw s10, 376(a6)
[0x800039c4]:sub a6, a6, a4
[0x800039c8]:lui a4, 1
[0x800039cc]:addi a4, a4, 2048
[0x800039d0]:add a6, a6, a4
[0x800039d4]:lw s11, 380(a6)
[0x800039d8]:sub a6, a6, a4
[0x800039dc]:lui t3, 316009
[0x800039e0]:addi t3, t3, 3999
[0x800039e4]:lui t4, 523821
[0x800039e8]:addi t4, t4, 3522
[0x800039ec]:lui s10, 316009
[0x800039f0]:addi s10, s10, 3999
[0x800039f4]:lui s11, 1048109
[0x800039f8]:addi s11, s11, 3522
[0x800039fc]:addi a4, zero, 64
[0x80003a00]:csrrw zero, fcsr, a4
[0x80003a04]:fadd.d t5, t3, s10, dyn
[0x80003a08]:csrrs a7, fcsr, zero

[0x80003a04]:fadd.d t5, t3, s10, dyn
[0x80003a08]:csrrs a7, fcsr, zero
[0x80003a0c]:sw t5, 752(ra)
[0x80003a10]:sw t6, 760(ra)
[0x80003a14]:sw t5, 768(ra)
[0x80003a18]:sw a7, 776(ra)
[0x80003a1c]:lui a4, 1
[0x80003a20]:addi a4, a4, 2048
[0x80003a24]:add a6, a6, a4
[0x80003a28]:lw t3, 384(a6)
[0x80003a2c]:sub a6, a6, a4
[0x80003a30]:lui a4, 1
[0x80003a34]:addi a4, a4, 2048
[0x80003a38]:add a6, a6, a4
[0x80003a3c]:lw t4, 388(a6)
[0x80003a40]:sub a6, a6, a4
[0x80003a44]:lui a4, 1
[0x80003a48]:addi a4, a4, 2048
[0x80003a4c]:add a6, a6, a4
[0x80003a50]:lw s10, 392(a6)
[0x80003a54]:sub a6, a6, a4
[0x80003a58]:lui a4, 1
[0x80003a5c]:addi a4, a4, 2048
[0x80003a60]:add a6, a6, a4
[0x80003a64]:lw s11, 396(a6)
[0x80003a68]:sub a6, a6, a4
[0x80003a6c]:lui t3, 316009
[0x80003a70]:addi t3, t3, 3999
[0x80003a74]:lui t4, 523821
[0x80003a78]:addi t4, t4, 3522
[0x80003a7c]:lui s10, 316009
[0x80003a80]:addi s10, s10, 3999
[0x80003a84]:lui s11, 1048109
[0x80003a88]:addi s11, s11, 3522
[0x80003a8c]:addi a4, zero, 96
[0x80003a90]:csrrw zero, fcsr, a4
[0x80003a94]:fadd.d t5, t3, s10, dyn
[0x80003a98]:csrrs a7, fcsr, zero

[0x80003a94]:fadd.d t5, t3, s10, dyn
[0x80003a98]:csrrs a7, fcsr, zero
[0x80003a9c]:sw t5, 784(ra)
[0x80003aa0]:sw t6, 792(ra)
[0x80003aa4]:sw t5, 800(ra)
[0x80003aa8]:sw a7, 808(ra)
[0x80003aac]:lui a4, 1
[0x80003ab0]:addi a4, a4, 2048
[0x80003ab4]:add a6, a6, a4
[0x80003ab8]:lw t3, 400(a6)
[0x80003abc]:sub a6, a6, a4
[0x80003ac0]:lui a4, 1
[0x80003ac4]:addi a4, a4, 2048
[0x80003ac8]:add a6, a6, a4
[0x80003acc]:lw t4, 404(a6)
[0x80003ad0]:sub a6, a6, a4
[0x80003ad4]:lui a4, 1
[0x80003ad8]:addi a4, a4, 2048
[0x80003adc]:add a6, a6, a4
[0x80003ae0]:lw s10, 408(a6)
[0x80003ae4]:sub a6, a6, a4
[0x80003ae8]:lui a4, 1
[0x80003aec]:addi a4, a4, 2048
[0x80003af0]:add a6, a6, a4
[0x80003af4]:lw s11, 412(a6)
[0x80003af8]:sub a6, a6, a4
[0x80003afc]:lui t3, 316009
[0x80003b00]:addi t3, t3, 3999
[0x80003b04]:lui t4, 523821
[0x80003b08]:addi t4, t4, 3522
[0x80003b0c]:lui s10, 316009
[0x80003b10]:addi s10, s10, 3999
[0x80003b14]:lui s11, 1048109
[0x80003b18]:addi s11, s11, 3522
[0x80003b1c]:addi a4, zero, 128
[0x80003b20]:csrrw zero, fcsr, a4
[0x80003b24]:fadd.d t5, t3, s10, dyn
[0x80003b28]:csrrs a7, fcsr, zero

[0x80003b24]:fadd.d t5, t3, s10, dyn
[0x80003b28]:csrrs a7, fcsr, zero
[0x80003b2c]:sw t5, 816(ra)
[0x80003b30]:sw t6, 824(ra)
[0x80003b34]:sw t5, 832(ra)
[0x80003b38]:sw a7, 840(ra)
[0x80003b3c]:lui a4, 1
[0x80003b40]:addi a4, a4, 2048
[0x80003b44]:add a6, a6, a4
[0x80003b48]:lw t3, 416(a6)
[0x80003b4c]:sub a6, a6, a4
[0x80003b50]:lui a4, 1
[0x80003b54]:addi a4, a4, 2048
[0x80003b58]:add a6, a6, a4
[0x80003b5c]:lw t4, 420(a6)
[0x80003b60]:sub a6, a6, a4
[0x80003b64]:lui a4, 1
[0x80003b68]:addi a4, a4, 2048
[0x80003b6c]:add a6, a6, a4
[0x80003b70]:lw s10, 424(a6)
[0x80003b74]:sub a6, a6, a4
[0x80003b78]:lui a4, 1
[0x80003b7c]:addi a4, a4, 2048
[0x80003b80]:add a6, a6, a4
[0x80003b84]:lw s11, 428(a6)
[0x80003b88]:sub a6, a6, a4
[0x80003b8c]:lui t3, 193297
[0x80003b90]:addi t3, t3, 1503
[0x80003b94]:lui t4, 523825
[0x80003b98]:addi t4, t4, 1224
[0x80003b9c]:lui s10, 193297
[0x80003ba0]:addi s10, s10, 1503
[0x80003ba4]:lui s11, 1048113
[0x80003ba8]:addi s11, s11, 1224
[0x80003bac]:addi a4, zero, 0
[0x80003bb0]:csrrw zero, fcsr, a4
[0x80003bb4]:fadd.d t5, t3, s10, dyn
[0x80003bb8]:csrrs a7, fcsr, zero

[0x80003bb4]:fadd.d t5, t3, s10, dyn
[0x80003bb8]:csrrs a7, fcsr, zero
[0x80003bbc]:sw t5, 848(ra)
[0x80003bc0]:sw t6, 856(ra)
[0x80003bc4]:sw t5, 864(ra)
[0x80003bc8]:sw a7, 872(ra)
[0x80003bcc]:lui a4, 1
[0x80003bd0]:addi a4, a4, 2048
[0x80003bd4]:add a6, a6, a4
[0x80003bd8]:lw t3, 432(a6)
[0x80003bdc]:sub a6, a6, a4
[0x80003be0]:lui a4, 1
[0x80003be4]:addi a4, a4, 2048
[0x80003be8]:add a6, a6, a4
[0x80003bec]:lw t4, 436(a6)
[0x80003bf0]:sub a6, a6, a4
[0x80003bf4]:lui a4, 1
[0x80003bf8]:addi a4, a4, 2048
[0x80003bfc]:add a6, a6, a4
[0x80003c00]:lw s10, 440(a6)
[0x80003c04]:sub a6, a6, a4
[0x80003c08]:lui a4, 1
[0x80003c0c]:addi a4, a4, 2048
[0x80003c10]:add a6, a6, a4
[0x80003c14]:lw s11, 444(a6)
[0x80003c18]:sub a6, a6, a4
[0x80003c1c]:lui t3, 193297
[0x80003c20]:addi t3, t3, 1503
[0x80003c24]:lui t4, 523825
[0x80003c28]:addi t4, t4, 1224
[0x80003c2c]:lui s10, 193297
[0x80003c30]:addi s10, s10, 1503
[0x80003c34]:lui s11, 1048113
[0x80003c38]:addi s11, s11, 1224
[0x80003c3c]:addi a4, zero, 32
[0x80003c40]:csrrw zero, fcsr, a4
[0x80003c44]:fadd.d t5, t3, s10, dyn
[0x80003c48]:csrrs a7, fcsr, zero

[0x80003c44]:fadd.d t5, t3, s10, dyn
[0x80003c48]:csrrs a7, fcsr, zero
[0x80003c4c]:sw t5, 880(ra)
[0x80003c50]:sw t6, 888(ra)
[0x80003c54]:sw t5, 896(ra)
[0x80003c58]:sw a7, 904(ra)
[0x80003c5c]:lui a4, 1
[0x80003c60]:addi a4, a4, 2048
[0x80003c64]:add a6, a6, a4
[0x80003c68]:lw t3, 448(a6)
[0x80003c6c]:sub a6, a6, a4
[0x80003c70]:lui a4, 1
[0x80003c74]:addi a4, a4, 2048
[0x80003c78]:add a6, a6, a4
[0x80003c7c]:lw t4, 452(a6)
[0x80003c80]:sub a6, a6, a4
[0x80003c84]:lui a4, 1
[0x80003c88]:addi a4, a4, 2048
[0x80003c8c]:add a6, a6, a4
[0x80003c90]:lw s10, 456(a6)
[0x80003c94]:sub a6, a6, a4
[0x80003c98]:lui a4, 1
[0x80003c9c]:addi a4, a4, 2048
[0x80003ca0]:add a6, a6, a4
[0x80003ca4]:lw s11, 460(a6)
[0x80003ca8]:sub a6, a6, a4
[0x80003cac]:lui t3, 193297
[0x80003cb0]:addi t3, t3, 1503
[0x80003cb4]:lui t4, 523825
[0x80003cb8]:addi t4, t4, 1224
[0x80003cbc]:lui s10, 193297
[0x80003cc0]:addi s10, s10, 1503
[0x80003cc4]:lui s11, 1048113
[0x80003cc8]:addi s11, s11, 1224
[0x80003ccc]:addi a4, zero, 64
[0x80003cd0]:csrrw zero, fcsr, a4
[0x80003cd4]:fadd.d t5, t3, s10, dyn
[0x80003cd8]:csrrs a7, fcsr, zero

[0x80003cd4]:fadd.d t5, t3, s10, dyn
[0x80003cd8]:csrrs a7, fcsr, zero
[0x80003cdc]:sw t5, 912(ra)
[0x80003ce0]:sw t6, 920(ra)
[0x80003ce4]:sw t5, 928(ra)
[0x80003ce8]:sw a7, 936(ra)
[0x80003cec]:lui a4, 1
[0x80003cf0]:addi a4, a4, 2048
[0x80003cf4]:add a6, a6, a4
[0x80003cf8]:lw t3, 464(a6)
[0x80003cfc]:sub a6, a6, a4
[0x80003d00]:lui a4, 1
[0x80003d04]:addi a4, a4, 2048
[0x80003d08]:add a6, a6, a4
[0x80003d0c]:lw t4, 468(a6)
[0x80003d10]:sub a6, a6, a4
[0x80003d14]:lui a4, 1
[0x80003d18]:addi a4, a4, 2048
[0x80003d1c]:add a6, a6, a4
[0x80003d20]:lw s10, 472(a6)
[0x80003d24]:sub a6, a6, a4
[0x80003d28]:lui a4, 1
[0x80003d2c]:addi a4, a4, 2048
[0x80003d30]:add a6, a6, a4
[0x80003d34]:lw s11, 476(a6)
[0x80003d38]:sub a6, a6, a4
[0x80003d3c]:lui t3, 193297
[0x80003d40]:addi t3, t3, 1503
[0x80003d44]:lui t4, 523825
[0x80003d48]:addi t4, t4, 1224
[0x80003d4c]:lui s10, 193297
[0x80003d50]:addi s10, s10, 1503
[0x80003d54]:lui s11, 1048113
[0x80003d58]:addi s11, s11, 1224
[0x80003d5c]:addi a4, zero, 96
[0x80003d60]:csrrw zero, fcsr, a4
[0x80003d64]:fadd.d t5, t3, s10, dyn
[0x80003d68]:csrrs a7, fcsr, zero

[0x80003d64]:fadd.d t5, t3, s10, dyn
[0x80003d68]:csrrs a7, fcsr, zero
[0x80003d6c]:sw t5, 944(ra)
[0x80003d70]:sw t6, 952(ra)
[0x80003d74]:sw t5, 960(ra)
[0x80003d78]:sw a7, 968(ra)
[0x80003d7c]:lui a4, 1
[0x80003d80]:addi a4, a4, 2048
[0x80003d84]:add a6, a6, a4
[0x80003d88]:lw t3, 480(a6)
[0x80003d8c]:sub a6, a6, a4
[0x80003d90]:lui a4, 1
[0x80003d94]:addi a4, a4, 2048
[0x80003d98]:add a6, a6, a4
[0x80003d9c]:lw t4, 484(a6)
[0x80003da0]:sub a6, a6, a4
[0x80003da4]:lui a4, 1
[0x80003da8]:addi a4, a4, 2048
[0x80003dac]:add a6, a6, a4
[0x80003db0]:lw s10, 488(a6)
[0x80003db4]:sub a6, a6, a4
[0x80003db8]:lui a4, 1
[0x80003dbc]:addi a4, a4, 2048
[0x80003dc0]:add a6, a6, a4
[0x80003dc4]:lw s11, 492(a6)
[0x80003dc8]:sub a6, a6, a4
[0x80003dcc]:lui t3, 193297
[0x80003dd0]:addi t3, t3, 1503
[0x80003dd4]:lui t4, 523825
[0x80003dd8]:addi t4, t4, 1224
[0x80003ddc]:lui s10, 193297
[0x80003de0]:addi s10, s10, 1503
[0x80003de4]:lui s11, 1048113
[0x80003de8]:addi s11, s11, 1224
[0x80003dec]:addi a4, zero, 128
[0x80003df0]:csrrw zero, fcsr, a4
[0x80003df4]:fadd.d t5, t3, s10, dyn
[0x80003df8]:csrrs a7, fcsr, zero

[0x80003df4]:fadd.d t5, t3, s10, dyn
[0x80003df8]:csrrs a7, fcsr, zero
[0x80003dfc]:sw t5, 976(ra)
[0x80003e00]:sw t6, 984(ra)
[0x80003e04]:sw t5, 992(ra)
[0x80003e08]:sw a7, 1000(ra)
[0x80003e0c]:lui a4, 1
[0x80003e10]:addi a4, a4, 2048
[0x80003e14]:add a6, a6, a4
[0x80003e18]:lw t3, 496(a6)
[0x80003e1c]:sub a6, a6, a4
[0x80003e20]:lui a4, 1
[0x80003e24]:addi a4, a4, 2048
[0x80003e28]:add a6, a6, a4
[0x80003e2c]:lw t4, 500(a6)
[0x80003e30]:sub a6, a6, a4
[0x80003e34]:lui a4, 1
[0x80003e38]:addi a4, a4, 2048
[0x80003e3c]:add a6, a6, a4
[0x80003e40]:lw s10, 504(a6)
[0x80003e44]:sub a6, a6, a4
[0x80003e48]:lui a4, 1
[0x80003e4c]:addi a4, a4, 2048
[0x80003e50]:add a6, a6, a4
[0x80003e54]:lw s11, 508(a6)
[0x80003e58]:sub a6, a6, a4
[0x80003e5c]:lui t3, 708479
[0x80003e60]:addi t3, t3, 2799
[0x80003e64]:lui t4, 523815
[0x80003e68]:addi t4, t4, 3003
[0x80003e6c]:lui s10, 708479
[0x80003e70]:addi s10, s10, 2799
[0x80003e74]:lui s11, 1048103
[0x80003e78]:addi s11, s11, 3003
[0x80003e7c]:addi a4, zero, 0
[0x80003e80]:csrrw zero, fcsr, a4
[0x80003e84]:fadd.d t5, t3, s10, dyn
[0x80003e88]:csrrs a7, fcsr, zero

[0x80003e84]:fadd.d t5, t3, s10, dyn
[0x80003e88]:csrrs a7, fcsr, zero
[0x80003e8c]:sw t5, 1008(ra)
[0x80003e90]:sw t6, 1016(ra)
[0x80003e94]:sw t5, 1024(ra)
[0x80003e98]:sw a7, 1032(ra)
[0x80003e9c]:lui a4, 1
[0x80003ea0]:addi a4, a4, 2048
[0x80003ea4]:add a6, a6, a4
[0x80003ea8]:lw t3, 512(a6)
[0x80003eac]:sub a6, a6, a4
[0x80003eb0]:lui a4, 1
[0x80003eb4]:addi a4, a4, 2048
[0x80003eb8]:add a6, a6, a4
[0x80003ebc]:lw t4, 516(a6)
[0x80003ec0]:sub a6, a6, a4
[0x80003ec4]:lui a4, 1
[0x80003ec8]:addi a4, a4, 2048
[0x80003ecc]:add a6, a6, a4
[0x80003ed0]:lw s10, 520(a6)
[0x80003ed4]:sub a6, a6, a4
[0x80003ed8]:lui a4, 1
[0x80003edc]:addi a4, a4, 2048
[0x80003ee0]:add a6, a6, a4
[0x80003ee4]:lw s11, 524(a6)
[0x80003ee8]:sub a6, a6, a4
[0x80003eec]:lui t3, 708479
[0x80003ef0]:addi t3, t3, 2799
[0x80003ef4]:lui t4, 523815
[0x80003ef8]:addi t4, t4, 3003
[0x80003efc]:lui s10, 708479
[0x80003f00]:addi s10, s10, 2799
[0x80003f04]:lui s11, 1048103
[0x80003f08]:addi s11, s11, 3003
[0x80003f0c]:addi a4, zero, 32
[0x80003f10]:csrrw zero, fcsr, a4
[0x80003f14]:fadd.d t5, t3, s10, dyn
[0x80003f18]:csrrs a7, fcsr, zero

[0x80003f14]:fadd.d t5, t3, s10, dyn
[0x80003f18]:csrrs a7, fcsr, zero
[0x80003f1c]:sw t5, 1040(ra)
[0x80003f20]:sw t6, 1048(ra)
[0x80003f24]:sw t5, 1056(ra)
[0x80003f28]:sw a7, 1064(ra)
[0x80003f2c]:lui a4, 1
[0x80003f30]:addi a4, a4, 2048
[0x80003f34]:add a6, a6, a4
[0x80003f38]:lw t3, 528(a6)
[0x80003f3c]:sub a6, a6, a4
[0x80003f40]:lui a4, 1
[0x80003f44]:addi a4, a4, 2048
[0x80003f48]:add a6, a6, a4
[0x80003f4c]:lw t4, 532(a6)
[0x80003f50]:sub a6, a6, a4
[0x80003f54]:lui a4, 1
[0x80003f58]:addi a4, a4, 2048
[0x80003f5c]:add a6, a6, a4
[0x80003f60]:lw s10, 536(a6)
[0x80003f64]:sub a6, a6, a4
[0x80003f68]:lui a4, 1
[0x80003f6c]:addi a4, a4, 2048
[0x80003f70]:add a6, a6, a4
[0x80003f74]:lw s11, 540(a6)
[0x80003f78]:sub a6, a6, a4
[0x80003f7c]:lui t3, 708479
[0x80003f80]:addi t3, t3, 2799
[0x80003f84]:lui t4, 523815
[0x80003f88]:addi t4, t4, 3003
[0x80003f8c]:lui s10, 708479
[0x80003f90]:addi s10, s10, 2799
[0x80003f94]:lui s11, 1048103
[0x80003f98]:addi s11, s11, 3003
[0x80003f9c]:addi a4, zero, 64
[0x80003fa0]:csrrw zero, fcsr, a4
[0x80003fa4]:fadd.d t5, t3, s10, dyn
[0x80003fa8]:csrrs a7, fcsr, zero

[0x80003fa4]:fadd.d t5, t3, s10, dyn
[0x80003fa8]:csrrs a7, fcsr, zero
[0x80003fac]:sw t5, 1072(ra)
[0x80003fb0]:sw t6, 1080(ra)
[0x80003fb4]:sw t5, 1088(ra)
[0x80003fb8]:sw a7, 1096(ra)
[0x80003fbc]:lui a4, 1
[0x80003fc0]:addi a4, a4, 2048
[0x80003fc4]:add a6, a6, a4
[0x80003fc8]:lw t3, 544(a6)
[0x80003fcc]:sub a6, a6, a4
[0x80003fd0]:lui a4, 1
[0x80003fd4]:addi a4, a4, 2048
[0x80003fd8]:add a6, a6, a4
[0x80003fdc]:lw t4, 548(a6)
[0x80003fe0]:sub a6, a6, a4
[0x80003fe4]:lui a4, 1
[0x80003fe8]:addi a4, a4, 2048
[0x80003fec]:add a6, a6, a4
[0x80003ff0]:lw s10, 552(a6)
[0x80003ff4]:sub a6, a6, a4
[0x80003ff8]:lui a4, 1
[0x80003ffc]:addi a4, a4, 2048
[0x80004000]:add a6, a6, a4
[0x80004004]:lw s11, 556(a6)
[0x80004008]:sub a6, a6, a4
[0x8000400c]:lui t3, 708479
[0x80004010]:addi t3, t3, 2799
[0x80004014]:lui t4, 523815
[0x80004018]:addi t4, t4, 3003
[0x8000401c]:lui s10, 708479
[0x80004020]:addi s10, s10, 2799
[0x80004024]:lui s11, 1048103
[0x80004028]:addi s11, s11, 3003
[0x8000402c]:addi a4, zero, 96
[0x80004030]:csrrw zero, fcsr, a4
[0x80004034]:fadd.d t5, t3, s10, dyn
[0x80004038]:csrrs a7, fcsr, zero

[0x80004034]:fadd.d t5, t3, s10, dyn
[0x80004038]:csrrs a7, fcsr, zero
[0x8000403c]:sw t5, 1104(ra)
[0x80004040]:sw t6, 1112(ra)
[0x80004044]:sw t5, 1120(ra)
[0x80004048]:sw a7, 1128(ra)
[0x8000404c]:lui a4, 1
[0x80004050]:addi a4, a4, 2048
[0x80004054]:add a6, a6, a4
[0x80004058]:lw t3, 560(a6)
[0x8000405c]:sub a6, a6, a4
[0x80004060]:lui a4, 1
[0x80004064]:addi a4, a4, 2048
[0x80004068]:add a6, a6, a4
[0x8000406c]:lw t4, 564(a6)
[0x80004070]:sub a6, a6, a4
[0x80004074]:lui a4, 1
[0x80004078]:addi a4, a4, 2048
[0x8000407c]:add a6, a6, a4
[0x80004080]:lw s10, 568(a6)
[0x80004084]:sub a6, a6, a4
[0x80004088]:lui a4, 1
[0x8000408c]:addi a4, a4, 2048
[0x80004090]:add a6, a6, a4
[0x80004094]:lw s11, 572(a6)
[0x80004098]:sub a6, a6, a4
[0x8000409c]:lui t3, 708479
[0x800040a0]:addi t3, t3, 2799
[0x800040a4]:lui t4, 523815
[0x800040a8]:addi t4, t4, 3003
[0x800040ac]:lui s10, 708479
[0x800040b0]:addi s10, s10, 2799
[0x800040b4]:lui s11, 1048103
[0x800040b8]:addi s11, s11, 3003
[0x800040bc]:addi a4, zero, 128
[0x800040c0]:csrrw zero, fcsr, a4
[0x800040c4]:fadd.d t5, t3, s10, dyn
[0x800040c8]:csrrs a7, fcsr, zero

[0x800040c4]:fadd.d t5, t3, s10, dyn
[0x800040c8]:csrrs a7, fcsr, zero
[0x800040cc]:sw t5, 1136(ra)
[0x800040d0]:sw t6, 1144(ra)
[0x800040d4]:sw t5, 1152(ra)
[0x800040d8]:sw a7, 1160(ra)
[0x800040dc]:lui a4, 1
[0x800040e0]:addi a4, a4, 2048
[0x800040e4]:add a6, a6, a4
[0x800040e8]:lw t3, 576(a6)
[0x800040ec]:sub a6, a6, a4
[0x800040f0]:lui a4, 1
[0x800040f4]:addi a4, a4, 2048
[0x800040f8]:add a6, a6, a4
[0x800040fc]:lw t4, 580(a6)
[0x80004100]:sub a6, a6, a4
[0x80004104]:lui a4, 1
[0x80004108]:addi a4, a4, 2048
[0x8000410c]:add a6, a6, a4
[0x80004110]:lw s10, 584(a6)
[0x80004114]:sub a6, a6, a4
[0x80004118]:lui a4, 1
[0x8000411c]:addi a4, a4, 2048
[0x80004120]:add a6, a6, a4
[0x80004124]:lw s11, 588(a6)
[0x80004128]:sub a6, a6, a4
[0x8000412c]:lui t3, 643660
[0x80004130]:addi t3, t3, 2763
[0x80004134]:lui t4, 523396
[0x80004138]:addi t4, t4, 3577
[0x8000413c]:lui s10, 643660
[0x80004140]:addi s10, s10, 2763
[0x80004144]:lui s11, 1047684
[0x80004148]:addi s11, s11, 3577
[0x8000414c]:addi a4, zero, 0
[0x80004150]:csrrw zero, fcsr, a4
[0x80004154]:fadd.d t5, t3, s10, dyn
[0x80004158]:csrrs a7, fcsr, zero

[0x80004154]:fadd.d t5, t3, s10, dyn
[0x80004158]:csrrs a7, fcsr, zero
[0x8000415c]:sw t5, 1168(ra)
[0x80004160]:sw t6, 1176(ra)
[0x80004164]:sw t5, 1184(ra)
[0x80004168]:sw a7, 1192(ra)
[0x8000416c]:lui a4, 1
[0x80004170]:addi a4, a4, 2048
[0x80004174]:add a6, a6, a4
[0x80004178]:lw t3, 592(a6)
[0x8000417c]:sub a6, a6, a4
[0x80004180]:lui a4, 1
[0x80004184]:addi a4, a4, 2048
[0x80004188]:add a6, a6, a4
[0x8000418c]:lw t4, 596(a6)
[0x80004190]:sub a6, a6, a4
[0x80004194]:lui a4, 1
[0x80004198]:addi a4, a4, 2048
[0x8000419c]:add a6, a6, a4
[0x800041a0]:lw s10, 600(a6)
[0x800041a4]:sub a6, a6, a4
[0x800041a8]:lui a4, 1
[0x800041ac]:addi a4, a4, 2048
[0x800041b0]:add a6, a6, a4
[0x800041b4]:lw s11, 604(a6)
[0x800041b8]:sub a6, a6, a4
[0x800041bc]:lui t3, 643660
[0x800041c0]:addi t3, t3, 2763
[0x800041c4]:lui t4, 523396
[0x800041c8]:addi t4, t4, 3577
[0x800041cc]:lui s10, 643660
[0x800041d0]:addi s10, s10, 2763
[0x800041d4]:lui s11, 1047684
[0x800041d8]:addi s11, s11, 3577
[0x800041dc]:addi a4, zero, 32
[0x800041e0]:csrrw zero, fcsr, a4
[0x800041e4]:fadd.d t5, t3, s10, dyn
[0x800041e8]:csrrs a7, fcsr, zero

[0x800041e4]:fadd.d t5, t3, s10, dyn
[0x800041e8]:csrrs a7, fcsr, zero
[0x800041ec]:sw t5, 1200(ra)
[0x800041f0]:sw t6, 1208(ra)
[0x800041f4]:sw t5, 1216(ra)
[0x800041f8]:sw a7, 1224(ra)
[0x800041fc]:lui a4, 1
[0x80004200]:addi a4, a4, 2048
[0x80004204]:add a6, a6, a4
[0x80004208]:lw t3, 608(a6)
[0x8000420c]:sub a6, a6, a4
[0x80004210]:lui a4, 1
[0x80004214]:addi a4, a4, 2048
[0x80004218]:add a6, a6, a4
[0x8000421c]:lw t4, 612(a6)
[0x80004220]:sub a6, a6, a4
[0x80004224]:lui a4, 1
[0x80004228]:addi a4, a4, 2048
[0x8000422c]:add a6, a6, a4
[0x80004230]:lw s10, 616(a6)
[0x80004234]:sub a6, a6, a4
[0x80004238]:lui a4, 1
[0x8000423c]:addi a4, a4, 2048
[0x80004240]:add a6, a6, a4
[0x80004244]:lw s11, 620(a6)
[0x80004248]:sub a6, a6, a4
[0x8000424c]:lui t3, 643660
[0x80004250]:addi t3, t3, 2763
[0x80004254]:lui t4, 523396
[0x80004258]:addi t4, t4, 3577
[0x8000425c]:lui s10, 643660
[0x80004260]:addi s10, s10, 2763
[0x80004264]:lui s11, 1047684
[0x80004268]:addi s11, s11, 3577
[0x8000426c]:addi a4, zero, 64
[0x80004270]:csrrw zero, fcsr, a4
[0x80004274]:fadd.d t5, t3, s10, dyn
[0x80004278]:csrrs a7, fcsr, zero

[0x80004274]:fadd.d t5, t3, s10, dyn
[0x80004278]:csrrs a7, fcsr, zero
[0x8000427c]:sw t5, 1232(ra)
[0x80004280]:sw t6, 1240(ra)
[0x80004284]:sw t5, 1248(ra)
[0x80004288]:sw a7, 1256(ra)
[0x8000428c]:lui a4, 1
[0x80004290]:addi a4, a4, 2048
[0x80004294]:add a6, a6, a4
[0x80004298]:lw t3, 624(a6)
[0x8000429c]:sub a6, a6, a4
[0x800042a0]:lui a4, 1
[0x800042a4]:addi a4, a4, 2048
[0x800042a8]:add a6, a6, a4
[0x800042ac]:lw t4, 628(a6)
[0x800042b0]:sub a6, a6, a4
[0x800042b4]:lui a4, 1
[0x800042b8]:addi a4, a4, 2048
[0x800042bc]:add a6, a6, a4
[0x800042c0]:lw s10, 632(a6)
[0x800042c4]:sub a6, a6, a4
[0x800042c8]:lui a4, 1
[0x800042cc]:addi a4, a4, 2048
[0x800042d0]:add a6, a6, a4
[0x800042d4]:lw s11, 636(a6)
[0x800042d8]:sub a6, a6, a4
[0x800042dc]:lui t3, 643660
[0x800042e0]:addi t3, t3, 2763
[0x800042e4]:lui t4, 523396
[0x800042e8]:addi t4, t4, 3577
[0x800042ec]:lui s10, 643660
[0x800042f0]:addi s10, s10, 2763
[0x800042f4]:lui s11, 1047684
[0x800042f8]:addi s11, s11, 3577
[0x800042fc]:addi a4, zero, 96
[0x80004300]:csrrw zero, fcsr, a4
[0x80004304]:fadd.d t5, t3, s10, dyn
[0x80004308]:csrrs a7, fcsr, zero

[0x80004304]:fadd.d t5, t3, s10, dyn
[0x80004308]:csrrs a7, fcsr, zero
[0x8000430c]:sw t5, 1264(ra)
[0x80004310]:sw t6, 1272(ra)
[0x80004314]:sw t5, 1280(ra)
[0x80004318]:sw a7, 1288(ra)
[0x8000431c]:lui a4, 1
[0x80004320]:addi a4, a4, 2048
[0x80004324]:add a6, a6, a4
[0x80004328]:lw t3, 640(a6)
[0x8000432c]:sub a6, a6, a4
[0x80004330]:lui a4, 1
[0x80004334]:addi a4, a4, 2048
[0x80004338]:add a6, a6, a4
[0x8000433c]:lw t4, 644(a6)
[0x80004340]:sub a6, a6, a4
[0x80004344]:lui a4, 1
[0x80004348]:addi a4, a4, 2048
[0x8000434c]:add a6, a6, a4
[0x80004350]:lw s10, 648(a6)
[0x80004354]:sub a6, a6, a4
[0x80004358]:lui a4, 1
[0x8000435c]:addi a4, a4, 2048
[0x80004360]:add a6, a6, a4
[0x80004364]:lw s11, 652(a6)
[0x80004368]:sub a6, a6, a4
[0x8000436c]:lui t3, 643660
[0x80004370]:addi t3, t3, 2763
[0x80004374]:lui t4, 523396
[0x80004378]:addi t4, t4, 3577
[0x8000437c]:lui s10, 643660
[0x80004380]:addi s10, s10, 2763
[0x80004384]:lui s11, 1047684
[0x80004388]:addi s11, s11, 3577
[0x8000438c]:addi a4, zero, 128
[0x80004390]:csrrw zero, fcsr, a4
[0x80004394]:fadd.d t5, t3, s10, dyn
[0x80004398]:csrrs a7, fcsr, zero

[0x80004394]:fadd.d t5, t3, s10, dyn
[0x80004398]:csrrs a7, fcsr, zero
[0x8000439c]:sw t5, 1296(ra)
[0x800043a0]:sw t6, 1304(ra)
[0x800043a4]:sw t5, 1312(ra)
[0x800043a8]:sw a7, 1320(ra)
[0x800043ac]:lui a4, 1
[0x800043b0]:addi a4, a4, 2048
[0x800043b4]:add a6, a6, a4
[0x800043b8]:lw t3, 656(a6)
[0x800043bc]:sub a6, a6, a4
[0x800043c0]:lui a4, 1
[0x800043c4]:addi a4, a4, 2048
[0x800043c8]:add a6, a6, a4
[0x800043cc]:lw t4, 660(a6)
[0x800043d0]:sub a6, a6, a4
[0x800043d4]:lui a4, 1
[0x800043d8]:addi a4, a4, 2048
[0x800043dc]:add a6, a6, a4
[0x800043e0]:lw s10, 664(a6)
[0x800043e4]:sub a6, a6, a4
[0x800043e8]:lui a4, 1
[0x800043ec]:addi a4, a4, 2048
[0x800043f0]:add a6, a6, a4
[0x800043f4]:lw s11, 668(a6)
[0x800043f8]:sub a6, a6, a4
[0x800043fc]:lui t3, 523982
[0x80004400]:addi t3, t3, 2517
[0x80004404]:lui t4, 523834
[0x80004408]:addi t4, t4, 3030
[0x8000440c]:lui s10, 523982
[0x80004410]:addi s10, s10, 2517
[0x80004414]:lui s11, 1048122
[0x80004418]:addi s11, s11, 3030
[0x8000441c]:addi a4, zero, 0
[0x80004420]:csrrw zero, fcsr, a4
[0x80004424]:fadd.d t5, t3, s10, dyn
[0x80004428]:csrrs a7, fcsr, zero

[0x80004424]:fadd.d t5, t3, s10, dyn
[0x80004428]:csrrs a7, fcsr, zero
[0x8000442c]:sw t5, 1328(ra)
[0x80004430]:sw t6, 1336(ra)
[0x80004434]:sw t5, 1344(ra)
[0x80004438]:sw a7, 1352(ra)
[0x8000443c]:lui a4, 1
[0x80004440]:addi a4, a4, 2048
[0x80004444]:add a6, a6, a4
[0x80004448]:lw t3, 672(a6)
[0x8000444c]:sub a6, a6, a4
[0x80004450]:lui a4, 1
[0x80004454]:addi a4, a4, 2048
[0x80004458]:add a6, a6, a4
[0x8000445c]:lw t4, 676(a6)
[0x80004460]:sub a6, a6, a4
[0x80004464]:lui a4, 1
[0x80004468]:addi a4, a4, 2048
[0x8000446c]:add a6, a6, a4
[0x80004470]:lw s10, 680(a6)
[0x80004474]:sub a6, a6, a4
[0x80004478]:lui a4, 1
[0x8000447c]:addi a4, a4, 2048
[0x80004480]:add a6, a6, a4
[0x80004484]:lw s11, 684(a6)
[0x80004488]:sub a6, a6, a4
[0x8000448c]:lui t3, 523982
[0x80004490]:addi t3, t3, 2517
[0x80004494]:lui t4, 523834
[0x80004498]:addi t4, t4, 3030
[0x8000449c]:lui s10, 523982
[0x800044a0]:addi s10, s10, 2517
[0x800044a4]:lui s11, 1048122
[0x800044a8]:addi s11, s11, 3030
[0x800044ac]:addi a4, zero, 32
[0x800044b0]:csrrw zero, fcsr, a4
[0x800044b4]:fadd.d t5, t3, s10, dyn
[0x800044b8]:csrrs a7, fcsr, zero

[0x800044b4]:fadd.d t5, t3, s10, dyn
[0x800044b8]:csrrs a7, fcsr, zero
[0x800044bc]:sw t5, 1360(ra)
[0x800044c0]:sw t6, 1368(ra)
[0x800044c4]:sw t5, 1376(ra)
[0x800044c8]:sw a7, 1384(ra)
[0x800044cc]:lui a4, 1
[0x800044d0]:addi a4, a4, 2048
[0x800044d4]:add a6, a6, a4
[0x800044d8]:lw t3, 688(a6)
[0x800044dc]:sub a6, a6, a4
[0x800044e0]:lui a4, 1
[0x800044e4]:addi a4, a4, 2048
[0x800044e8]:add a6, a6, a4
[0x800044ec]:lw t4, 692(a6)
[0x800044f0]:sub a6, a6, a4
[0x800044f4]:lui a4, 1
[0x800044f8]:addi a4, a4, 2048
[0x800044fc]:add a6, a6, a4
[0x80004500]:lw s10, 696(a6)
[0x80004504]:sub a6, a6, a4
[0x80004508]:lui a4, 1
[0x8000450c]:addi a4, a4, 2048
[0x80004510]:add a6, a6, a4
[0x80004514]:lw s11, 700(a6)
[0x80004518]:sub a6, a6, a4
[0x8000451c]:lui t3, 523982
[0x80004520]:addi t3, t3, 2517
[0x80004524]:lui t4, 523834
[0x80004528]:addi t4, t4, 3030
[0x8000452c]:lui s10, 523982
[0x80004530]:addi s10, s10, 2517
[0x80004534]:lui s11, 1048122
[0x80004538]:addi s11, s11, 3030
[0x8000453c]:addi a4, zero, 64
[0x80004540]:csrrw zero, fcsr, a4
[0x80004544]:fadd.d t5, t3, s10, dyn
[0x80004548]:csrrs a7, fcsr, zero

[0x80004544]:fadd.d t5, t3, s10, dyn
[0x80004548]:csrrs a7, fcsr, zero
[0x8000454c]:sw t5, 1392(ra)
[0x80004550]:sw t6, 1400(ra)
[0x80004554]:sw t5, 1408(ra)
[0x80004558]:sw a7, 1416(ra)
[0x8000455c]:lui a4, 1
[0x80004560]:addi a4, a4, 2048
[0x80004564]:add a6, a6, a4
[0x80004568]:lw t3, 704(a6)
[0x8000456c]:sub a6, a6, a4
[0x80004570]:lui a4, 1
[0x80004574]:addi a4, a4, 2048
[0x80004578]:add a6, a6, a4
[0x8000457c]:lw t4, 708(a6)
[0x80004580]:sub a6, a6, a4
[0x80004584]:lui a4, 1
[0x80004588]:addi a4, a4, 2048
[0x8000458c]:add a6, a6, a4
[0x80004590]:lw s10, 712(a6)
[0x80004594]:sub a6, a6, a4
[0x80004598]:lui a4, 1
[0x8000459c]:addi a4, a4, 2048
[0x800045a0]:add a6, a6, a4
[0x800045a4]:lw s11, 716(a6)
[0x800045a8]:sub a6, a6, a4
[0x800045ac]:lui t3, 523982
[0x800045b0]:addi t3, t3, 2517
[0x800045b4]:lui t4, 523834
[0x800045b8]:addi t4, t4, 3030
[0x800045bc]:lui s10, 523982
[0x800045c0]:addi s10, s10, 2517
[0x800045c4]:lui s11, 1048122
[0x800045c8]:addi s11, s11, 3030
[0x800045cc]:addi a4, zero, 96
[0x800045d0]:csrrw zero, fcsr, a4
[0x800045d4]:fadd.d t5, t3, s10, dyn
[0x800045d8]:csrrs a7, fcsr, zero

[0x800045d4]:fadd.d t5, t3, s10, dyn
[0x800045d8]:csrrs a7, fcsr, zero
[0x800045dc]:sw t5, 1424(ra)
[0x800045e0]:sw t6, 1432(ra)
[0x800045e4]:sw t5, 1440(ra)
[0x800045e8]:sw a7, 1448(ra)
[0x800045ec]:lui a4, 1
[0x800045f0]:addi a4, a4, 2048
[0x800045f4]:add a6, a6, a4
[0x800045f8]:lw t3, 720(a6)
[0x800045fc]:sub a6, a6, a4
[0x80004600]:lui a4, 1
[0x80004604]:addi a4, a4, 2048
[0x80004608]:add a6, a6, a4
[0x8000460c]:lw t4, 724(a6)
[0x80004610]:sub a6, a6, a4
[0x80004614]:lui a4, 1
[0x80004618]:addi a4, a4, 2048
[0x8000461c]:add a6, a6, a4
[0x80004620]:lw s10, 728(a6)
[0x80004624]:sub a6, a6, a4
[0x80004628]:lui a4, 1
[0x8000462c]:addi a4, a4, 2048
[0x80004630]:add a6, a6, a4
[0x80004634]:lw s11, 732(a6)
[0x80004638]:sub a6, a6, a4
[0x8000463c]:lui t3, 523982
[0x80004640]:addi t3, t3, 2517
[0x80004644]:lui t4, 523834
[0x80004648]:addi t4, t4, 3030
[0x8000464c]:lui s10, 523982
[0x80004650]:addi s10, s10, 2517
[0x80004654]:lui s11, 1048122
[0x80004658]:addi s11, s11, 3030
[0x8000465c]:addi a4, zero, 128
[0x80004660]:csrrw zero, fcsr, a4
[0x80004664]:fadd.d t5, t3, s10, dyn
[0x80004668]:csrrs a7, fcsr, zero

[0x80004664]:fadd.d t5, t3, s10, dyn
[0x80004668]:csrrs a7, fcsr, zero
[0x8000466c]:sw t5, 1456(ra)
[0x80004670]:sw t6, 1464(ra)
[0x80004674]:sw t5, 1472(ra)
[0x80004678]:sw a7, 1480(ra)
[0x8000467c]:lui a4, 1
[0x80004680]:addi a4, a4, 2048
[0x80004684]:add a6, a6, a4
[0x80004688]:lw t3, 736(a6)
[0x8000468c]:sub a6, a6, a4
[0x80004690]:lui a4, 1
[0x80004694]:addi a4, a4, 2048
[0x80004698]:add a6, a6, a4
[0x8000469c]:lw t4, 740(a6)
[0x800046a0]:sub a6, a6, a4
[0x800046a4]:lui a4, 1
[0x800046a8]:addi a4, a4, 2048
[0x800046ac]:add a6, a6, a4
[0x800046b0]:lw s10, 744(a6)
[0x800046b4]:sub a6, a6, a4
[0x800046b8]:lui a4, 1
[0x800046bc]:addi a4, a4, 2048
[0x800046c0]:add a6, a6, a4
[0x800046c4]:lw s11, 748(a6)
[0x800046c8]:sub a6, a6, a4
[0x800046cc]:lui t3, 887000
[0x800046d0]:addi t3, t3, 3215
[0x800046d4]:lui t4, 523752
[0x800046d8]:addi t4, t4, 3963
[0x800046dc]:lui s10, 887000
[0x800046e0]:addi s10, s10, 3215
[0x800046e4]:lui s11, 1048040
[0x800046e8]:addi s11, s11, 3963
[0x800046ec]:addi a4, zero, 0
[0x800046f0]:csrrw zero, fcsr, a4
[0x800046f4]:fadd.d t5, t3, s10, dyn
[0x800046f8]:csrrs a7, fcsr, zero

[0x800046f4]:fadd.d t5, t3, s10, dyn
[0x800046f8]:csrrs a7, fcsr, zero
[0x800046fc]:sw t5, 1488(ra)
[0x80004700]:sw t6, 1496(ra)
[0x80004704]:sw t5, 1504(ra)
[0x80004708]:sw a7, 1512(ra)
[0x8000470c]:lui a4, 1
[0x80004710]:addi a4, a4, 2048
[0x80004714]:add a6, a6, a4
[0x80004718]:lw t3, 752(a6)
[0x8000471c]:sub a6, a6, a4
[0x80004720]:lui a4, 1
[0x80004724]:addi a4, a4, 2048
[0x80004728]:add a6, a6, a4
[0x8000472c]:lw t4, 756(a6)
[0x80004730]:sub a6, a6, a4
[0x80004734]:lui a4, 1
[0x80004738]:addi a4, a4, 2048
[0x8000473c]:add a6, a6, a4
[0x80004740]:lw s10, 760(a6)
[0x80004744]:sub a6, a6, a4
[0x80004748]:lui a4, 1
[0x8000474c]:addi a4, a4, 2048
[0x80004750]:add a6, a6, a4
[0x80004754]:lw s11, 764(a6)
[0x80004758]:sub a6, a6, a4
[0x8000475c]:lui t3, 887000
[0x80004760]:addi t3, t3, 3215
[0x80004764]:lui t4, 523752
[0x80004768]:addi t4, t4, 3963
[0x8000476c]:lui s10, 887000
[0x80004770]:addi s10, s10, 3215
[0x80004774]:lui s11, 1048040
[0x80004778]:addi s11, s11, 3963
[0x8000477c]:addi a4, zero, 32
[0x80004780]:csrrw zero, fcsr, a4
[0x80004784]:fadd.d t5, t3, s10, dyn
[0x80004788]:csrrs a7, fcsr, zero

[0x80004784]:fadd.d t5, t3, s10, dyn
[0x80004788]:csrrs a7, fcsr, zero
[0x8000478c]:sw t5, 1520(ra)
[0x80004790]:sw t6, 1528(ra)
[0x80004794]:sw t5, 1536(ra)
[0x80004798]:sw a7, 1544(ra)
[0x8000479c]:lui a4, 1
[0x800047a0]:addi a4, a4, 2048
[0x800047a4]:add a6, a6, a4
[0x800047a8]:lw t3, 768(a6)
[0x800047ac]:sub a6, a6, a4
[0x800047b0]:lui a4, 1
[0x800047b4]:addi a4, a4, 2048
[0x800047b8]:add a6, a6, a4
[0x800047bc]:lw t4, 772(a6)
[0x800047c0]:sub a6, a6, a4
[0x800047c4]:lui a4, 1
[0x800047c8]:addi a4, a4, 2048
[0x800047cc]:add a6, a6, a4
[0x800047d0]:lw s10, 776(a6)
[0x800047d4]:sub a6, a6, a4
[0x800047d8]:lui a4, 1
[0x800047dc]:addi a4, a4, 2048
[0x800047e0]:add a6, a6, a4
[0x800047e4]:lw s11, 780(a6)
[0x800047e8]:sub a6, a6, a4
[0x800047ec]:lui t3, 887000
[0x800047f0]:addi t3, t3, 3215
[0x800047f4]:lui t4, 523752
[0x800047f8]:addi t4, t4, 3963
[0x800047fc]:lui s10, 887000
[0x80004800]:addi s10, s10, 3215
[0x80004804]:lui s11, 1048040
[0x80004808]:addi s11, s11, 3963
[0x8000480c]:addi a4, zero, 64
[0x80004810]:csrrw zero, fcsr, a4
[0x80004814]:fadd.d t5, t3, s10, dyn
[0x80004818]:csrrs a7, fcsr, zero

[0x80004814]:fadd.d t5, t3, s10, dyn
[0x80004818]:csrrs a7, fcsr, zero
[0x8000481c]:sw t5, 1552(ra)
[0x80004820]:sw t6, 1560(ra)
[0x80004824]:sw t5, 1568(ra)
[0x80004828]:sw a7, 1576(ra)
[0x8000482c]:lui a4, 1
[0x80004830]:addi a4, a4, 2048
[0x80004834]:add a6, a6, a4
[0x80004838]:lw t3, 784(a6)
[0x8000483c]:sub a6, a6, a4
[0x80004840]:lui a4, 1
[0x80004844]:addi a4, a4, 2048
[0x80004848]:add a6, a6, a4
[0x8000484c]:lw t4, 788(a6)
[0x80004850]:sub a6, a6, a4
[0x80004854]:lui a4, 1
[0x80004858]:addi a4, a4, 2048
[0x8000485c]:add a6, a6, a4
[0x80004860]:lw s10, 792(a6)
[0x80004864]:sub a6, a6, a4
[0x80004868]:lui a4, 1
[0x8000486c]:addi a4, a4, 2048
[0x80004870]:add a6, a6, a4
[0x80004874]:lw s11, 796(a6)
[0x80004878]:sub a6, a6, a4
[0x8000487c]:lui t3, 887000
[0x80004880]:addi t3, t3, 3215
[0x80004884]:lui t4, 523752
[0x80004888]:addi t4, t4, 3963
[0x8000488c]:lui s10, 887000
[0x80004890]:addi s10, s10, 3215
[0x80004894]:lui s11, 1048040
[0x80004898]:addi s11, s11, 3963
[0x8000489c]:addi a4, zero, 96
[0x800048a0]:csrrw zero, fcsr, a4
[0x800048a4]:fadd.d t5, t3, s10, dyn
[0x800048a8]:csrrs a7, fcsr, zero

[0x800048a4]:fadd.d t5, t3, s10, dyn
[0x800048a8]:csrrs a7, fcsr, zero
[0x800048ac]:sw t5, 1584(ra)
[0x800048b0]:sw t6, 1592(ra)
[0x800048b4]:sw t5, 1600(ra)
[0x800048b8]:sw a7, 1608(ra)
[0x800048bc]:lui a4, 1
[0x800048c0]:addi a4, a4, 2048
[0x800048c4]:add a6, a6, a4
[0x800048c8]:lw t3, 800(a6)
[0x800048cc]:sub a6, a6, a4
[0x800048d0]:lui a4, 1
[0x800048d4]:addi a4, a4, 2048
[0x800048d8]:add a6, a6, a4
[0x800048dc]:lw t4, 804(a6)
[0x800048e0]:sub a6, a6, a4
[0x800048e4]:lui a4, 1
[0x800048e8]:addi a4, a4, 2048
[0x800048ec]:add a6, a6, a4
[0x800048f0]:lw s10, 808(a6)
[0x800048f4]:sub a6, a6, a4
[0x800048f8]:lui a4, 1
[0x800048fc]:addi a4, a4, 2048
[0x80004900]:add a6, a6, a4
[0x80004904]:lw s11, 812(a6)
[0x80004908]:sub a6, a6, a4
[0x8000490c]:lui t3, 887000
[0x80004910]:addi t3, t3, 3215
[0x80004914]:lui t4, 523752
[0x80004918]:addi t4, t4, 3963
[0x8000491c]:lui s10, 887000
[0x80004920]:addi s10, s10, 3215
[0x80004924]:lui s11, 1048040
[0x80004928]:addi s11, s11, 3963
[0x8000492c]:addi a4, zero, 128
[0x80004930]:csrrw zero, fcsr, a4
[0x80004934]:fadd.d t5, t3, s10, dyn
[0x80004938]:csrrs a7, fcsr, zero

[0x80004934]:fadd.d t5, t3, s10, dyn
[0x80004938]:csrrs a7, fcsr, zero
[0x8000493c]:sw t5, 1616(ra)
[0x80004940]:sw t6, 1624(ra)
[0x80004944]:sw t5, 1632(ra)
[0x80004948]:sw a7, 1640(ra)
[0x8000494c]:lui a4, 1
[0x80004950]:addi a4, a4, 2048
[0x80004954]:add a6, a6, a4
[0x80004958]:lw t3, 816(a6)
[0x8000495c]:sub a6, a6, a4
[0x80004960]:lui a4, 1
[0x80004964]:addi a4, a4, 2048
[0x80004968]:add a6, a6, a4
[0x8000496c]:lw t4, 820(a6)
[0x80004970]:sub a6, a6, a4
[0x80004974]:lui a4, 1
[0x80004978]:addi a4, a4, 2048
[0x8000497c]:add a6, a6, a4
[0x80004980]:lw s10, 824(a6)
[0x80004984]:sub a6, a6, a4
[0x80004988]:lui a4, 1
[0x8000498c]:addi a4, a4, 2048
[0x80004990]:add a6, a6, a4
[0x80004994]:lw s11, 828(a6)
[0x80004998]:sub a6, a6, a4
[0x8000499c]:lui t3, 608342
[0x800049a0]:addi t3, t3, 2445
[0x800049a4]:lui t4, 523908
[0x800049a8]:addi t4, t4, 3658
[0x800049ac]:lui s10, 608342
[0x800049b0]:addi s10, s10, 2445
[0x800049b4]:lui s11, 1048196
[0x800049b8]:addi s11, s11, 3658
[0x800049bc]:addi a4, zero, 0
[0x800049c0]:csrrw zero, fcsr, a4
[0x800049c4]:fadd.d t5, t3, s10, dyn
[0x800049c8]:csrrs a7, fcsr, zero

[0x800049c4]:fadd.d t5, t3, s10, dyn
[0x800049c8]:csrrs a7, fcsr, zero
[0x800049cc]:sw t5, 1648(ra)
[0x800049d0]:sw t6, 1656(ra)
[0x800049d4]:sw t5, 1664(ra)
[0x800049d8]:sw a7, 1672(ra)
[0x800049dc]:lui a4, 1
[0x800049e0]:addi a4, a4, 2048
[0x800049e4]:add a6, a6, a4
[0x800049e8]:lw t3, 832(a6)
[0x800049ec]:sub a6, a6, a4
[0x800049f0]:lui a4, 1
[0x800049f4]:addi a4, a4, 2048
[0x800049f8]:add a6, a6, a4
[0x800049fc]:lw t4, 836(a6)
[0x80004a00]:sub a6, a6, a4
[0x80004a04]:lui a4, 1
[0x80004a08]:addi a4, a4, 2048
[0x80004a0c]:add a6, a6, a4
[0x80004a10]:lw s10, 840(a6)
[0x80004a14]:sub a6, a6, a4
[0x80004a18]:lui a4, 1
[0x80004a1c]:addi a4, a4, 2048
[0x80004a20]:add a6, a6, a4
[0x80004a24]:lw s11, 844(a6)
[0x80004a28]:sub a6, a6, a4
[0x80004a2c]:lui t3, 608342
[0x80004a30]:addi t3, t3, 2445
[0x80004a34]:lui t4, 523908
[0x80004a38]:addi t4, t4, 3658
[0x80004a3c]:lui s10, 608342
[0x80004a40]:addi s10, s10, 2445
[0x80004a44]:lui s11, 1048196
[0x80004a48]:addi s11, s11, 3658
[0x80004a4c]:addi a4, zero, 32
[0x80004a50]:csrrw zero, fcsr, a4
[0x80004a54]:fadd.d t5, t3, s10, dyn
[0x80004a58]:csrrs a7, fcsr, zero

[0x80004a54]:fadd.d t5, t3, s10, dyn
[0x80004a58]:csrrs a7, fcsr, zero
[0x80004a5c]:sw t5, 1680(ra)
[0x80004a60]:sw t6, 1688(ra)
[0x80004a64]:sw t5, 1696(ra)
[0x80004a68]:sw a7, 1704(ra)
[0x80004a6c]:lui a4, 1
[0x80004a70]:addi a4, a4, 2048
[0x80004a74]:add a6, a6, a4
[0x80004a78]:lw t3, 848(a6)
[0x80004a7c]:sub a6, a6, a4
[0x80004a80]:lui a4, 1
[0x80004a84]:addi a4, a4, 2048
[0x80004a88]:add a6, a6, a4
[0x80004a8c]:lw t4, 852(a6)
[0x80004a90]:sub a6, a6, a4
[0x80004a94]:lui a4, 1
[0x80004a98]:addi a4, a4, 2048
[0x80004a9c]:add a6, a6, a4
[0x80004aa0]:lw s10, 856(a6)
[0x80004aa4]:sub a6, a6, a4
[0x80004aa8]:lui a4, 1
[0x80004aac]:addi a4, a4, 2048
[0x80004ab0]:add a6, a6, a4
[0x80004ab4]:lw s11, 860(a6)
[0x80004ab8]:sub a6, a6, a4
[0x80004abc]:lui t3, 608342
[0x80004ac0]:addi t3, t3, 2445
[0x80004ac4]:lui t4, 523908
[0x80004ac8]:addi t4, t4, 3658
[0x80004acc]:lui s10, 608342
[0x80004ad0]:addi s10, s10, 2445
[0x80004ad4]:lui s11, 1048196
[0x80004ad8]:addi s11, s11, 3658
[0x80004adc]:addi a4, zero, 64
[0x80004ae0]:csrrw zero, fcsr, a4
[0x80004ae4]:fadd.d t5, t3, s10, dyn
[0x80004ae8]:csrrs a7, fcsr, zero

[0x80004ae4]:fadd.d t5, t3, s10, dyn
[0x80004ae8]:csrrs a7, fcsr, zero
[0x80004aec]:sw t5, 1712(ra)
[0x80004af0]:sw t6, 1720(ra)
[0x80004af4]:sw t5, 1728(ra)
[0x80004af8]:sw a7, 1736(ra)
[0x80004afc]:lui a4, 1
[0x80004b00]:addi a4, a4, 2048
[0x80004b04]:add a6, a6, a4
[0x80004b08]:lw t3, 864(a6)
[0x80004b0c]:sub a6, a6, a4
[0x80004b10]:lui a4, 1
[0x80004b14]:addi a4, a4, 2048
[0x80004b18]:add a6, a6, a4
[0x80004b1c]:lw t4, 868(a6)
[0x80004b20]:sub a6, a6, a4
[0x80004b24]:lui a4, 1
[0x80004b28]:addi a4, a4, 2048
[0x80004b2c]:add a6, a6, a4
[0x80004b30]:lw s10, 872(a6)
[0x80004b34]:sub a6, a6, a4
[0x80004b38]:lui a4, 1
[0x80004b3c]:addi a4, a4, 2048
[0x80004b40]:add a6, a6, a4
[0x80004b44]:lw s11, 876(a6)
[0x80004b48]:sub a6, a6, a4
[0x80004b4c]:lui t3, 608342
[0x80004b50]:addi t3, t3, 2445
[0x80004b54]:lui t4, 523908
[0x80004b58]:addi t4, t4, 3658
[0x80004b5c]:lui s10, 608342
[0x80004b60]:addi s10, s10, 2445
[0x80004b64]:lui s11, 1048196
[0x80004b68]:addi s11, s11, 3658
[0x80004b6c]:addi a4, zero, 96
[0x80004b70]:csrrw zero, fcsr, a4
[0x80004b74]:fadd.d t5, t3, s10, dyn
[0x80004b78]:csrrs a7, fcsr, zero

[0x80004b74]:fadd.d t5, t3, s10, dyn
[0x80004b78]:csrrs a7, fcsr, zero
[0x80004b7c]:sw t5, 1744(ra)
[0x80004b80]:sw t6, 1752(ra)
[0x80004b84]:sw t5, 1760(ra)
[0x80004b88]:sw a7, 1768(ra)
[0x80004b8c]:lui a4, 1
[0x80004b90]:addi a4, a4, 2048
[0x80004b94]:add a6, a6, a4
[0x80004b98]:lw t3, 880(a6)
[0x80004b9c]:sub a6, a6, a4
[0x80004ba0]:lui a4, 1
[0x80004ba4]:addi a4, a4, 2048
[0x80004ba8]:add a6, a6, a4
[0x80004bac]:lw t4, 884(a6)
[0x80004bb0]:sub a6, a6, a4
[0x80004bb4]:lui a4, 1
[0x80004bb8]:addi a4, a4, 2048
[0x80004bbc]:add a6, a6, a4
[0x80004bc0]:lw s10, 888(a6)
[0x80004bc4]:sub a6, a6, a4
[0x80004bc8]:lui a4, 1
[0x80004bcc]:addi a4, a4, 2048
[0x80004bd0]:add a6, a6, a4
[0x80004bd4]:lw s11, 892(a6)
[0x80004bd8]:sub a6, a6, a4
[0x80004bdc]:lui t3, 608342
[0x80004be0]:addi t3, t3, 2445
[0x80004be4]:lui t4, 523908
[0x80004be8]:addi t4, t4, 3658
[0x80004bec]:lui s10, 608342
[0x80004bf0]:addi s10, s10, 2445
[0x80004bf4]:lui s11, 1048196
[0x80004bf8]:addi s11, s11, 3658
[0x80004bfc]:addi a4, zero, 128
[0x80004c00]:csrrw zero, fcsr, a4
[0x80004c04]:fadd.d t5, t3, s10, dyn
[0x80004c08]:csrrs a7, fcsr, zero

[0x80004c04]:fadd.d t5, t3, s10, dyn
[0x80004c08]:csrrs a7, fcsr, zero
[0x80004c0c]:sw t5, 1776(ra)
[0x80004c10]:sw t6, 1784(ra)
[0x80004c14]:sw t5, 1792(ra)
[0x80004c18]:sw a7, 1800(ra)
[0x80004c1c]:lui a4, 1
[0x80004c20]:addi a4, a4, 2048
[0x80004c24]:add a6, a6, a4
[0x80004c28]:lw t3, 896(a6)
[0x80004c2c]:sub a6, a6, a4
[0x80004c30]:lui a4, 1
[0x80004c34]:addi a4, a4, 2048
[0x80004c38]:add a6, a6, a4
[0x80004c3c]:lw t4, 900(a6)
[0x80004c40]:sub a6, a6, a4
[0x80004c44]:lui a4, 1
[0x80004c48]:addi a4, a4, 2048
[0x80004c4c]:add a6, a6, a4
[0x80004c50]:lw s10, 904(a6)
[0x80004c54]:sub a6, a6, a4
[0x80004c58]:lui a4, 1
[0x80004c5c]:addi a4, a4, 2048
[0x80004c60]:add a6, a6, a4
[0x80004c64]:lw s11, 908(a6)
[0x80004c68]:sub a6, a6, a4
[0x80004c6c]:lui t3, 361081
[0x80004c70]:addi t3, t3, 422
[0x80004c74]:lui t4, 523993
[0x80004c78]:addi t4, t4, 3158
[0x80004c7c]:lui s10, 361081
[0x80004c80]:addi s10, s10, 422
[0x80004c84]:lui s11, 1048281
[0x80004c88]:addi s11, s11, 3158
[0x80004c8c]:addi a4, zero, 0
[0x80004c90]:csrrw zero, fcsr, a4
[0x80004c94]:fadd.d t5, t3, s10, dyn
[0x80004c98]:csrrs a7, fcsr, zero

[0x80004c94]:fadd.d t5, t3, s10, dyn
[0x80004c98]:csrrs a7, fcsr, zero
[0x80004c9c]:sw t5, 1808(ra)
[0x80004ca0]:sw t6, 1816(ra)
[0x80004ca4]:sw t5, 1824(ra)
[0x80004ca8]:sw a7, 1832(ra)
[0x80004cac]:lui a4, 1
[0x80004cb0]:addi a4, a4, 2048
[0x80004cb4]:add a6, a6, a4
[0x80004cb8]:lw t3, 912(a6)
[0x80004cbc]:sub a6, a6, a4
[0x80004cc0]:lui a4, 1
[0x80004cc4]:addi a4, a4, 2048
[0x80004cc8]:add a6, a6, a4
[0x80004ccc]:lw t4, 916(a6)
[0x80004cd0]:sub a6, a6, a4
[0x80004cd4]:lui a4, 1
[0x80004cd8]:addi a4, a4, 2048
[0x80004cdc]:add a6, a6, a4
[0x80004ce0]:lw s10, 920(a6)
[0x80004ce4]:sub a6, a6, a4
[0x80004ce8]:lui a4, 1
[0x80004cec]:addi a4, a4, 2048
[0x80004cf0]:add a6, a6, a4
[0x80004cf4]:lw s11, 924(a6)
[0x80004cf8]:sub a6, a6, a4
[0x80004cfc]:lui t3, 361081
[0x80004d00]:addi t3, t3, 422
[0x80004d04]:lui t4, 523993
[0x80004d08]:addi t4, t4, 3158
[0x80004d0c]:lui s10, 361081
[0x80004d10]:addi s10, s10, 422
[0x80004d14]:lui s11, 1048281
[0x80004d18]:addi s11, s11, 3158
[0x80004d1c]:addi a4, zero, 32
[0x80004d20]:csrrw zero, fcsr, a4
[0x80004d24]:fadd.d t5, t3, s10, dyn
[0x80004d28]:csrrs a7, fcsr, zero

[0x80004d24]:fadd.d t5, t3, s10, dyn
[0x80004d28]:csrrs a7, fcsr, zero
[0x80004d2c]:sw t5, 1840(ra)
[0x80004d30]:sw t6, 1848(ra)
[0x80004d34]:sw t5, 1856(ra)
[0x80004d38]:sw a7, 1864(ra)
[0x80004d3c]:lui a4, 1
[0x80004d40]:addi a4, a4, 2048
[0x80004d44]:add a6, a6, a4
[0x80004d48]:lw t3, 928(a6)
[0x80004d4c]:sub a6, a6, a4
[0x80004d50]:lui a4, 1
[0x80004d54]:addi a4, a4, 2048
[0x80004d58]:add a6, a6, a4
[0x80004d5c]:lw t4, 932(a6)
[0x80004d60]:sub a6, a6, a4
[0x80004d64]:lui a4, 1
[0x80004d68]:addi a4, a4, 2048
[0x80004d6c]:add a6, a6, a4
[0x80004d70]:lw s10, 936(a6)
[0x80004d74]:sub a6, a6, a4
[0x80004d78]:lui a4, 1
[0x80004d7c]:addi a4, a4, 2048
[0x80004d80]:add a6, a6, a4
[0x80004d84]:lw s11, 940(a6)
[0x80004d88]:sub a6, a6, a4
[0x80004d8c]:lui t3, 361081
[0x80004d90]:addi t3, t3, 422
[0x80004d94]:lui t4, 523993
[0x80004d98]:addi t4, t4, 3158
[0x80004d9c]:lui s10, 361081
[0x80004da0]:addi s10, s10, 422
[0x80004da4]:lui s11, 1048281
[0x80004da8]:addi s11, s11, 3158
[0x80004dac]:addi a4, zero, 64
[0x80004db0]:csrrw zero, fcsr, a4
[0x80004db4]:fadd.d t5, t3, s10, dyn
[0x80004db8]:csrrs a7, fcsr, zero

[0x80004db4]:fadd.d t5, t3, s10, dyn
[0x80004db8]:csrrs a7, fcsr, zero
[0x80004dbc]:sw t5, 1872(ra)
[0x80004dc0]:sw t6, 1880(ra)
[0x80004dc4]:sw t5, 1888(ra)
[0x80004dc8]:sw a7, 1896(ra)
[0x80004dcc]:lui a4, 1
[0x80004dd0]:addi a4, a4, 2048
[0x80004dd4]:add a6, a6, a4
[0x80004dd8]:lw t3, 944(a6)
[0x80004ddc]:sub a6, a6, a4
[0x80004de0]:lui a4, 1
[0x80004de4]:addi a4, a4, 2048
[0x80004de8]:add a6, a6, a4
[0x80004dec]:lw t4, 948(a6)
[0x80004df0]:sub a6, a6, a4
[0x80004df4]:lui a4, 1
[0x80004df8]:addi a4, a4, 2048
[0x80004dfc]:add a6, a6, a4
[0x80004e00]:lw s10, 952(a6)
[0x80004e04]:sub a6, a6, a4
[0x80004e08]:lui a4, 1
[0x80004e0c]:addi a4, a4, 2048
[0x80004e10]:add a6, a6, a4
[0x80004e14]:lw s11, 956(a6)
[0x80004e18]:sub a6, a6, a4
[0x80004e1c]:lui t3, 361081
[0x80004e20]:addi t3, t3, 422
[0x80004e24]:lui t4, 523993
[0x80004e28]:addi t4, t4, 3158
[0x80004e2c]:lui s10, 361081
[0x80004e30]:addi s10, s10, 422
[0x80004e34]:lui s11, 1048281
[0x80004e38]:addi s11, s11, 3158
[0x80004e3c]:addi a4, zero, 96
[0x80004e40]:csrrw zero, fcsr, a4
[0x80004e44]:fadd.d t5, t3, s10, dyn
[0x80004e48]:csrrs a7, fcsr, zero

[0x80004e44]:fadd.d t5, t3, s10, dyn
[0x80004e48]:csrrs a7, fcsr, zero
[0x80004e4c]:sw t5, 1904(ra)
[0x80004e50]:sw t6, 1912(ra)
[0x80004e54]:sw t5, 1920(ra)
[0x80004e58]:sw a7, 1928(ra)
[0x80004e5c]:lui a4, 1
[0x80004e60]:addi a4, a4, 2048
[0x80004e64]:add a6, a6, a4
[0x80004e68]:lw t3, 960(a6)
[0x80004e6c]:sub a6, a6, a4
[0x80004e70]:lui a4, 1
[0x80004e74]:addi a4, a4, 2048
[0x80004e78]:add a6, a6, a4
[0x80004e7c]:lw t4, 964(a6)
[0x80004e80]:sub a6, a6, a4
[0x80004e84]:lui a4, 1
[0x80004e88]:addi a4, a4, 2048
[0x80004e8c]:add a6, a6, a4
[0x80004e90]:lw s10, 968(a6)
[0x80004e94]:sub a6, a6, a4
[0x80004e98]:lui a4, 1
[0x80004e9c]:addi a4, a4, 2048
[0x80004ea0]:add a6, a6, a4
[0x80004ea4]:lw s11, 972(a6)
[0x80004ea8]:sub a6, a6, a4
[0x80004eac]:lui t3, 361081
[0x80004eb0]:addi t3, t3, 422
[0x80004eb4]:lui t4, 523993
[0x80004eb8]:addi t4, t4, 3158
[0x80004ebc]:lui s10, 361081
[0x80004ec0]:addi s10, s10, 422
[0x80004ec4]:lui s11, 1048281
[0x80004ec8]:addi s11, s11, 3158
[0x80004ecc]:addi a4, zero, 128
[0x80004ed0]:csrrw zero, fcsr, a4
[0x80004ed4]:fadd.d t5, t3, s10, dyn
[0x80004ed8]:csrrs a7, fcsr, zero

[0x80004ed4]:fadd.d t5, t3, s10, dyn
[0x80004ed8]:csrrs a7, fcsr, zero
[0x80004edc]:sw t5, 1936(ra)
[0x80004ee0]:sw t6, 1944(ra)
[0x80004ee4]:sw t5, 1952(ra)
[0x80004ee8]:sw a7, 1960(ra)
[0x80004eec]:lui a4, 1
[0x80004ef0]:addi a4, a4, 2048
[0x80004ef4]:add a6, a6, a4
[0x80004ef8]:lw t3, 976(a6)
[0x80004efc]:sub a6, a6, a4
[0x80004f00]:lui a4, 1
[0x80004f04]:addi a4, a4, 2048
[0x80004f08]:add a6, a6, a4
[0x80004f0c]:lw t4, 980(a6)
[0x80004f10]:sub a6, a6, a4
[0x80004f14]:lui a4, 1
[0x80004f18]:addi a4, a4, 2048
[0x80004f1c]:add a6, a6, a4
[0x80004f20]:lw s10, 984(a6)
[0x80004f24]:sub a6, a6, a4
[0x80004f28]:lui a4, 1
[0x80004f2c]:addi a4, a4, 2048
[0x80004f30]:add a6, a6, a4
[0x80004f34]:lw s11, 988(a6)
[0x80004f38]:sub a6, a6, a4
[0x80004f3c]:lui t3, 584898
[0x80004f40]:addi t3, t3, 2885
[0x80004f44]:lui t4, 523980
[0x80004f48]:addi t4, t4, 3541
[0x80004f4c]:lui s10, 584898
[0x80004f50]:addi s10, s10, 2885
[0x80004f54]:lui s11, 1048268
[0x80004f58]:addi s11, s11, 3541
[0x80004f5c]:addi a4, zero, 0
[0x80004f60]:csrrw zero, fcsr, a4
[0x80004f64]:fadd.d t5, t3, s10, dyn
[0x80004f68]:csrrs a7, fcsr, zero

[0x80004f64]:fadd.d t5, t3, s10, dyn
[0x80004f68]:csrrs a7, fcsr, zero
[0x80004f6c]:sw t5, 1968(ra)
[0x80004f70]:sw t6, 1976(ra)
[0x80004f74]:sw t5, 1984(ra)
[0x80004f78]:sw a7, 1992(ra)
[0x80004f7c]:lui a4, 1
[0x80004f80]:addi a4, a4, 2048
[0x80004f84]:add a6, a6, a4
[0x80004f88]:lw t3, 992(a6)
[0x80004f8c]:sub a6, a6, a4
[0x80004f90]:lui a4, 1
[0x80004f94]:addi a4, a4, 2048
[0x80004f98]:add a6, a6, a4
[0x80004f9c]:lw t4, 996(a6)
[0x80004fa0]:sub a6, a6, a4
[0x80004fa4]:lui a4, 1
[0x80004fa8]:addi a4, a4, 2048
[0x80004fac]:add a6, a6, a4
[0x80004fb0]:lw s10, 1000(a6)
[0x80004fb4]:sub a6, a6, a4
[0x80004fb8]:lui a4, 1
[0x80004fbc]:addi a4, a4, 2048
[0x80004fc0]:add a6, a6, a4
[0x80004fc4]:lw s11, 1004(a6)
[0x80004fc8]:sub a6, a6, a4
[0x80004fcc]:lui t3, 584898
[0x80004fd0]:addi t3, t3, 2885
[0x80004fd4]:lui t4, 523980
[0x80004fd8]:addi t4, t4, 3541
[0x80004fdc]:lui s10, 584898
[0x80004fe0]:addi s10, s10, 2885
[0x80004fe4]:lui s11, 1048268
[0x80004fe8]:addi s11, s11, 3541
[0x80004fec]:addi a4, zero, 32
[0x80004ff0]:csrrw zero, fcsr, a4
[0x80004ff4]:fadd.d t5, t3, s10, dyn
[0x80004ff8]:csrrs a7, fcsr, zero

[0x80004ff4]:fadd.d t5, t3, s10, dyn
[0x80004ff8]:csrrs a7, fcsr, zero
[0x80004ffc]:sw t5, 2000(ra)
[0x80005000]:sw t6, 2008(ra)
[0x80005004]:sw t5, 2016(ra)
[0x80005008]:sw a7, 2024(ra)
[0x8000500c]:lui a4, 1
[0x80005010]:addi a4, a4, 2048
[0x80005014]:add a6, a6, a4
[0x80005018]:lw t3, 1008(a6)
[0x8000501c]:sub a6, a6, a4
[0x80005020]:lui a4, 1
[0x80005024]:addi a4, a4, 2048
[0x80005028]:add a6, a6, a4
[0x8000502c]:lw t4, 1012(a6)
[0x80005030]:sub a6, a6, a4
[0x80005034]:lui a4, 1
[0x80005038]:addi a4, a4, 2048
[0x8000503c]:add a6, a6, a4
[0x80005040]:lw s10, 1016(a6)
[0x80005044]:sub a6, a6, a4
[0x80005048]:lui a4, 1
[0x8000504c]:addi a4, a4, 2048
[0x80005050]:add a6, a6, a4
[0x80005054]:lw s11, 1020(a6)
[0x80005058]:sub a6, a6, a4
[0x8000505c]:lui t3, 584898
[0x80005060]:addi t3, t3, 2885
[0x80005064]:lui t4, 523980
[0x80005068]:addi t4, t4, 3541
[0x8000506c]:lui s10, 584898
[0x80005070]:addi s10, s10, 2885
[0x80005074]:lui s11, 1048268
[0x80005078]:addi s11, s11, 3541
[0x8000507c]:addi a4, zero, 64
[0x80005080]:csrrw zero, fcsr, a4
[0x80005084]:fadd.d t5, t3, s10, dyn
[0x80005088]:csrrs a7, fcsr, zero

[0x80005084]:fadd.d t5, t3, s10, dyn
[0x80005088]:csrrs a7, fcsr, zero
[0x8000508c]:sw t5, 2032(ra)
[0x80005090]:sw t6, 2040(ra)
[0x80005094]:addi ra, ra, 2040
[0x80005098]:sw t5, 8(ra)
[0x8000509c]:sw a7, 16(ra)
[0x800050a0]:lui a4, 1
[0x800050a4]:addi a4, a4, 2048
[0x800050a8]:add a6, a6, a4
[0x800050ac]:lw t3, 1024(a6)
[0x800050b0]:sub a6, a6, a4
[0x800050b4]:lui a4, 1
[0x800050b8]:addi a4, a4, 2048
[0x800050bc]:add a6, a6, a4
[0x800050c0]:lw t4, 1028(a6)
[0x800050c4]:sub a6, a6, a4
[0x800050c8]:lui a4, 1
[0x800050cc]:addi a4, a4, 2048
[0x800050d0]:add a6, a6, a4
[0x800050d4]:lw s10, 1032(a6)
[0x800050d8]:sub a6, a6, a4
[0x800050dc]:lui a4, 1
[0x800050e0]:addi a4, a4, 2048
[0x800050e4]:add a6, a6, a4
[0x800050e8]:lw s11, 1036(a6)
[0x800050ec]:sub a6, a6, a4
[0x800050f0]:lui t3, 584898
[0x800050f4]:addi t3, t3, 2885
[0x800050f8]:lui t4, 523980
[0x800050fc]:addi t4, t4, 3541
[0x80005100]:lui s10, 584898
[0x80005104]:addi s10, s10, 2885
[0x80005108]:lui s11, 1048268
[0x8000510c]:addi s11, s11, 3541
[0x80005110]:addi a4, zero, 96
[0x80005114]:csrrw zero, fcsr, a4
[0x80005118]:fadd.d t5, t3, s10, dyn
[0x8000511c]:csrrs a7, fcsr, zero

[0x80005118]:fadd.d t5, t3, s10, dyn
[0x8000511c]:csrrs a7, fcsr, zero
[0x80005120]:sw t5, 24(ra)
[0x80005124]:sw t6, 32(ra)
[0x80005128]:sw t5, 40(ra)
[0x8000512c]:sw a7, 48(ra)
[0x80005130]:lui a4, 1
[0x80005134]:addi a4, a4, 2048
[0x80005138]:add a6, a6, a4
[0x8000513c]:lw t3, 1040(a6)
[0x80005140]:sub a6, a6, a4
[0x80005144]:lui a4, 1
[0x80005148]:addi a4, a4, 2048
[0x8000514c]:add a6, a6, a4
[0x80005150]:lw t4, 1044(a6)
[0x80005154]:sub a6, a6, a4
[0x80005158]:lui a4, 1
[0x8000515c]:addi a4, a4, 2048
[0x80005160]:add a6, a6, a4
[0x80005164]:lw s10, 1048(a6)
[0x80005168]:sub a6, a6, a4
[0x8000516c]:lui a4, 1
[0x80005170]:addi a4, a4, 2048
[0x80005174]:add a6, a6, a4
[0x80005178]:lw s11, 1052(a6)
[0x8000517c]:sub a6, a6, a4
[0x80005180]:lui t3, 584898
[0x80005184]:addi t3, t3, 2885
[0x80005188]:lui t4, 523980
[0x8000518c]:addi t4, t4, 3541
[0x80005190]:lui s10, 584898
[0x80005194]:addi s10, s10, 2885
[0x80005198]:lui s11, 1048268
[0x8000519c]:addi s11, s11, 3541
[0x800051a0]:addi a4, zero, 128
[0x800051a4]:csrrw zero, fcsr, a4
[0x800051a8]:fadd.d t5, t3, s10, dyn
[0x800051ac]:csrrs a7, fcsr, zero

[0x800051a8]:fadd.d t5, t3, s10, dyn
[0x800051ac]:csrrs a7, fcsr, zero
[0x800051b0]:sw t5, 56(ra)
[0x800051b4]:sw t6, 64(ra)
[0x800051b8]:sw t5, 72(ra)
[0x800051bc]:sw a7, 80(ra)
[0x800051c0]:lui a4, 1
[0x800051c4]:addi a4, a4, 2048
[0x800051c8]:add a6, a6, a4
[0x800051cc]:lw t3, 1056(a6)
[0x800051d0]:sub a6, a6, a4
[0x800051d4]:lui a4, 1
[0x800051d8]:addi a4, a4, 2048
[0x800051dc]:add a6, a6, a4
[0x800051e0]:lw t4, 1060(a6)
[0x800051e4]:sub a6, a6, a4
[0x800051e8]:lui a4, 1
[0x800051ec]:addi a4, a4, 2048
[0x800051f0]:add a6, a6, a4
[0x800051f4]:lw s10, 1064(a6)
[0x800051f8]:sub a6, a6, a4
[0x800051fc]:lui a4, 1
[0x80005200]:addi a4, a4, 2048
[0x80005204]:add a6, a6, a4
[0x80005208]:lw s11, 1068(a6)
[0x8000520c]:sub a6, a6, a4
[0x80005210]:lui t3, 224190
[0x80005214]:addi t3, t3, 1791
[0x80005218]:lui t4, 523797
[0x8000521c]:addi t4, t4, 3224
[0x80005220]:lui s10, 224190
[0x80005224]:addi s10, s10, 1791
[0x80005228]:lui s11, 1048085
[0x8000522c]:addi s11, s11, 3224
[0x80005230]:addi a4, zero, 0
[0x80005234]:csrrw zero, fcsr, a4
[0x80005238]:fadd.d t5, t3, s10, dyn
[0x8000523c]:csrrs a7, fcsr, zero

[0x80005238]:fadd.d t5, t3, s10, dyn
[0x8000523c]:csrrs a7, fcsr, zero
[0x80005240]:sw t5, 88(ra)
[0x80005244]:sw t6, 96(ra)
[0x80005248]:sw t5, 104(ra)
[0x8000524c]:sw a7, 112(ra)
[0x80005250]:lui a4, 1
[0x80005254]:addi a4, a4, 2048
[0x80005258]:add a6, a6, a4
[0x8000525c]:lw t3, 1072(a6)
[0x80005260]:sub a6, a6, a4
[0x80005264]:lui a4, 1
[0x80005268]:addi a4, a4, 2048
[0x8000526c]:add a6, a6, a4
[0x80005270]:lw t4, 1076(a6)
[0x80005274]:sub a6, a6, a4
[0x80005278]:lui a4, 1
[0x8000527c]:addi a4, a4, 2048
[0x80005280]:add a6, a6, a4
[0x80005284]:lw s10, 1080(a6)
[0x80005288]:sub a6, a6, a4
[0x8000528c]:lui a4, 1
[0x80005290]:addi a4, a4, 2048
[0x80005294]:add a6, a6, a4
[0x80005298]:lw s11, 1084(a6)
[0x8000529c]:sub a6, a6, a4
[0x800052a0]:lui t3, 224190
[0x800052a4]:addi t3, t3, 1791
[0x800052a8]:lui t4, 523797
[0x800052ac]:addi t4, t4, 3224
[0x800052b0]:lui s10, 224190
[0x800052b4]:addi s10, s10, 1791
[0x800052b8]:lui s11, 1048085
[0x800052bc]:addi s11, s11, 3224
[0x800052c0]:addi a4, zero, 32
[0x800052c4]:csrrw zero, fcsr, a4
[0x800052c8]:fadd.d t5, t3, s10, dyn
[0x800052cc]:csrrs a7, fcsr, zero

[0x800052c8]:fadd.d t5, t3, s10, dyn
[0x800052cc]:csrrs a7, fcsr, zero
[0x800052d0]:sw t5, 120(ra)
[0x800052d4]:sw t6, 128(ra)
[0x800052d8]:sw t5, 136(ra)
[0x800052dc]:sw a7, 144(ra)
[0x800052e0]:lui a4, 1
[0x800052e4]:addi a4, a4, 2048
[0x800052e8]:add a6, a6, a4
[0x800052ec]:lw t3, 1088(a6)
[0x800052f0]:sub a6, a6, a4
[0x800052f4]:lui a4, 1
[0x800052f8]:addi a4, a4, 2048
[0x800052fc]:add a6, a6, a4
[0x80005300]:lw t4, 1092(a6)
[0x80005304]:sub a6, a6, a4
[0x80005308]:lui a4, 1
[0x8000530c]:addi a4, a4, 2048
[0x80005310]:add a6, a6, a4
[0x80005314]:lw s10, 1096(a6)
[0x80005318]:sub a6, a6, a4
[0x8000531c]:lui a4, 1
[0x80005320]:addi a4, a4, 2048
[0x80005324]:add a6, a6, a4
[0x80005328]:lw s11, 1100(a6)
[0x8000532c]:sub a6, a6, a4
[0x80005330]:lui t3, 224190
[0x80005334]:addi t3, t3, 1791
[0x80005338]:lui t4, 523797
[0x8000533c]:addi t4, t4, 3224
[0x80005340]:lui s10, 224190
[0x80005344]:addi s10, s10, 1791
[0x80005348]:lui s11, 1048085
[0x8000534c]:addi s11, s11, 3224
[0x80005350]:addi a4, zero, 64
[0x80005354]:csrrw zero, fcsr, a4
[0x80005358]:fadd.d t5, t3, s10, dyn
[0x8000535c]:csrrs a7, fcsr, zero

[0x80005358]:fadd.d t5, t3, s10, dyn
[0x8000535c]:csrrs a7, fcsr, zero
[0x80005360]:sw t5, 152(ra)
[0x80005364]:sw t6, 160(ra)
[0x80005368]:sw t5, 168(ra)
[0x8000536c]:sw a7, 176(ra)
[0x80005370]:lui a4, 1
[0x80005374]:addi a4, a4, 2048
[0x80005378]:add a6, a6, a4
[0x8000537c]:lw t3, 1104(a6)
[0x80005380]:sub a6, a6, a4
[0x80005384]:lui a4, 1
[0x80005388]:addi a4, a4, 2048
[0x8000538c]:add a6, a6, a4
[0x80005390]:lw t4, 1108(a6)
[0x80005394]:sub a6, a6, a4
[0x80005398]:lui a4, 1
[0x8000539c]:addi a4, a4, 2048
[0x800053a0]:add a6, a6, a4
[0x800053a4]:lw s10, 1112(a6)
[0x800053a8]:sub a6, a6, a4
[0x800053ac]:lui a4, 1
[0x800053b0]:addi a4, a4, 2048
[0x800053b4]:add a6, a6, a4
[0x800053b8]:lw s11, 1116(a6)
[0x800053bc]:sub a6, a6, a4
[0x800053c0]:lui t3, 224190
[0x800053c4]:addi t3, t3, 1791
[0x800053c8]:lui t4, 523797
[0x800053cc]:addi t4, t4, 3224
[0x800053d0]:lui s10, 224190
[0x800053d4]:addi s10, s10, 1791
[0x800053d8]:lui s11, 1048085
[0x800053dc]:addi s11, s11, 3224
[0x800053e0]:addi a4, zero, 96
[0x800053e4]:csrrw zero, fcsr, a4
[0x800053e8]:fadd.d t5, t3, s10, dyn
[0x800053ec]:csrrs a7, fcsr, zero

[0x800053e8]:fadd.d t5, t3, s10, dyn
[0x800053ec]:csrrs a7, fcsr, zero
[0x800053f0]:sw t5, 184(ra)
[0x800053f4]:sw t6, 192(ra)
[0x800053f8]:sw t5, 200(ra)
[0x800053fc]:sw a7, 208(ra)
[0x80005400]:lui a4, 1
[0x80005404]:addi a4, a4, 2048
[0x80005408]:add a6, a6, a4
[0x8000540c]:lw t3, 1120(a6)
[0x80005410]:sub a6, a6, a4
[0x80005414]:lui a4, 1
[0x80005418]:addi a4, a4, 2048
[0x8000541c]:add a6, a6, a4
[0x80005420]:lw t4, 1124(a6)
[0x80005424]:sub a6, a6, a4
[0x80005428]:lui a4, 1
[0x8000542c]:addi a4, a4, 2048
[0x80005430]:add a6, a6, a4
[0x80005434]:lw s10, 1128(a6)
[0x80005438]:sub a6, a6, a4
[0x8000543c]:lui a4, 1
[0x80005440]:addi a4, a4, 2048
[0x80005444]:add a6, a6, a4
[0x80005448]:lw s11, 1132(a6)
[0x8000544c]:sub a6, a6, a4
[0x80005450]:lui t3, 224190
[0x80005454]:addi t3, t3, 1791
[0x80005458]:lui t4, 523797
[0x8000545c]:addi t4, t4, 3224
[0x80005460]:lui s10, 224190
[0x80005464]:addi s10, s10, 1791
[0x80005468]:lui s11, 1048085
[0x8000546c]:addi s11, s11, 3224
[0x80005470]:addi a4, zero, 128
[0x80005474]:csrrw zero, fcsr, a4
[0x80005478]:fadd.d t5, t3, s10, dyn
[0x8000547c]:csrrs a7, fcsr, zero

[0x80005478]:fadd.d t5, t3, s10, dyn
[0x8000547c]:csrrs a7, fcsr, zero
[0x80005480]:sw t5, 216(ra)
[0x80005484]:sw t6, 224(ra)
[0x80005488]:sw t5, 232(ra)
[0x8000548c]:sw a7, 240(ra)
[0x80005490]:lui a4, 1
[0x80005494]:addi a4, a4, 2048
[0x80005498]:add a6, a6, a4
[0x8000549c]:lw t3, 1136(a6)
[0x800054a0]:sub a6, a6, a4
[0x800054a4]:lui a4, 1
[0x800054a8]:addi a4, a4, 2048
[0x800054ac]:add a6, a6, a4
[0x800054b0]:lw t4, 1140(a6)
[0x800054b4]:sub a6, a6, a4
[0x800054b8]:lui a4, 1
[0x800054bc]:addi a4, a4, 2048
[0x800054c0]:add a6, a6, a4
[0x800054c4]:lw s10, 1144(a6)
[0x800054c8]:sub a6, a6, a4
[0x800054cc]:lui a4, 1
[0x800054d0]:addi a4, a4, 2048
[0x800054d4]:add a6, a6, a4
[0x800054d8]:lw s11, 1148(a6)
[0x800054dc]:sub a6, a6, a4
[0x800054e0]:lui t3, 1022120
[0x800054e4]:addi t3, t3, 3517
[0x800054e8]:lui t4, 523952
[0x800054ec]:addi t4, t4, 1408
[0x800054f0]:lui s10, 1022120
[0x800054f4]:addi s10, s10, 3517
[0x800054f8]:lui s11, 1048240
[0x800054fc]:addi s11, s11, 1408
[0x80005500]:addi a4, zero, 0
[0x80005504]:csrrw zero, fcsr, a4
[0x80005508]:fadd.d t5, t3, s10, dyn
[0x8000550c]:csrrs a7, fcsr, zero

[0x80005508]:fadd.d t5, t3, s10, dyn
[0x8000550c]:csrrs a7, fcsr, zero
[0x80005510]:sw t5, 248(ra)
[0x80005514]:sw t6, 256(ra)
[0x80005518]:sw t5, 264(ra)
[0x8000551c]:sw a7, 272(ra)
[0x80005520]:lui a4, 1
[0x80005524]:addi a4, a4, 2048
[0x80005528]:add a6, a6, a4
[0x8000552c]:lw t3, 1152(a6)
[0x80005530]:sub a6, a6, a4
[0x80005534]:lui a4, 1
[0x80005538]:addi a4, a4, 2048
[0x8000553c]:add a6, a6, a4
[0x80005540]:lw t4, 1156(a6)
[0x80005544]:sub a6, a6, a4
[0x80005548]:lui a4, 1
[0x8000554c]:addi a4, a4, 2048
[0x80005550]:add a6, a6, a4
[0x80005554]:lw s10, 1160(a6)
[0x80005558]:sub a6, a6, a4
[0x8000555c]:lui a4, 1
[0x80005560]:addi a4, a4, 2048
[0x80005564]:add a6, a6, a4
[0x80005568]:lw s11, 1164(a6)
[0x8000556c]:sub a6, a6, a4
[0x80005570]:lui t3, 1022120
[0x80005574]:addi t3, t3, 3517
[0x80005578]:lui t4, 523952
[0x8000557c]:addi t4, t4, 1408
[0x80005580]:lui s10, 1022120
[0x80005584]:addi s10, s10, 3517
[0x80005588]:lui s11, 1048240
[0x8000558c]:addi s11, s11, 1408
[0x80005590]:addi a4, zero, 32
[0x80005594]:csrrw zero, fcsr, a4
[0x80005598]:fadd.d t5, t3, s10, dyn
[0x8000559c]:csrrs a7, fcsr, zero

[0x80005598]:fadd.d t5, t3, s10, dyn
[0x8000559c]:csrrs a7, fcsr, zero
[0x800055a0]:sw t5, 280(ra)
[0x800055a4]:sw t6, 288(ra)
[0x800055a8]:sw t5, 296(ra)
[0x800055ac]:sw a7, 304(ra)



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fadd.d', 'rs1 : x30', 'rs2 : x30', 'rd : x30', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x8000014c]:fadd.d t5, t5, t5, dyn
	-[0x80000150]:csrrs tp, fcsr, zero
	-[0x80000154]:sw t5, 0(ra)
	-[0x80000158]:sw t6, 8(ra)
Current Store : [0x80000158] : sw t6, 8(ra) -- Store: [0x80007f20]:0x7FEB0580




Last Coverpoint : ['mnemonic : fadd.d', 'rs1 : x30', 'rs2 : x30', 'rd : x30', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x8000014c]:fadd.d t5, t5, t5, dyn
	-[0x80000150]:csrrs tp, fcsr, zero
	-[0x80000154]:sw t5, 0(ra)
	-[0x80000158]:sw t6, 8(ra)
	-[0x8000015c]:sw t5, 16(ra)
Current Store : [0x8000015c] : sw t5, 16(ra) -- Store: [0x80007f28]:0x00000000




Last Coverpoint : ['rs1 : x26', 'rs2 : x26', 'rd : x28', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x8000019c]:fadd.d t3, s10, s10, dyn
	-[0x800001a0]:csrrs tp, fcsr, zero
	-[0x800001a4]:sw t3, 32(ra)
	-[0x800001a8]:sw t4, 40(ra)
Current Store : [0x800001a8] : sw t4, 40(ra) -- Store: [0x80007f40]:0xEEDBEADF




Last Coverpoint : ['rs1 : x26', 'rs2 : x26', 'rd : x28', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x8000019c]:fadd.d t3, s10, s10, dyn
	-[0x800001a0]:csrrs tp, fcsr, zero
	-[0x800001a4]:sw t3, 32(ra)
	-[0x800001a8]:sw t4, 40(ra)
	-[0x800001ac]:sw t3, 48(ra)
Current Store : [0x800001ac] : sw t3, 48(ra) -- Store: [0x80007f48]:0xFFFFFFFF




Last Coverpoint : ['rs1 : x28', 'rs2 : x24', 'rd : x26', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb0580f98a7dbd and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xb0580f98a7dbd and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001ec]:fadd.d s10, t3, s8, dyn
	-[0x800001f0]:csrrs tp, fcsr, zero
	-[0x800001f4]:sw s10, 64(ra)
	-[0x800001f8]:sw s11, 72(ra)
Current Store : [0x800001f8] : sw s11, 72(ra) -- Store: [0x80007f60]:0x7FEB0580




Last Coverpoint : ['rs1 : x28', 'rs2 : x24', 'rd : x26', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb0580f98a7dbd and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xb0580f98a7dbd and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001ec]:fadd.d s10, t3, s8, dyn
	-[0x800001f0]:csrrs tp, fcsr, zero
	-[0x800001f4]:sw s10, 64(ra)
	-[0x800001f8]:sw s11, 72(ra)
	-[0x800001fc]:sw s10, 80(ra)
Current Store : [0x800001fc] : sw s10, 80(ra) -- Store: [0x80007f68]:0x00000000




Last Coverpoint : ['rs1 : x24', 'rs2 : x28', 'rd : x24', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb0580f98a7dbd and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xb0580f98a7dbd and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000023c]:fadd.d s8, s8, t3, dyn
	-[0x80000240]:csrrs tp, fcsr, zero
	-[0x80000244]:sw s8, 96(ra)
	-[0x80000248]:sw s9, 104(ra)
Current Store : [0x80000248] : sw s9, 104(ra) -- Store: [0x80007f80]:0x7FEB0580




Last Coverpoint : ['rs1 : x24', 'rs2 : x28', 'rd : x24', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb0580f98a7dbd and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xb0580f98a7dbd and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000023c]:fadd.d s8, s8, t3, dyn
	-[0x80000240]:csrrs tp, fcsr, zero
	-[0x80000244]:sw s8, 96(ra)
	-[0x80000248]:sw s9, 104(ra)
	-[0x8000024c]:sw s8, 112(ra)
Current Store : [0x8000024c] : sw s8, 112(ra) -- Store: [0x80007f88]:0x00000000




Last Coverpoint : ['rs1 : x20', 'rs2 : x22', 'rd : x22', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb0580f98a7dbd and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xb0580f98a7dbd and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000028c]:fadd.d s6, s4, s6, dyn
	-[0x80000290]:csrrs tp, fcsr, zero
	-[0x80000294]:sw s6, 128(ra)
	-[0x80000298]:sw s7, 136(ra)
Current Store : [0x80000298] : sw s7, 136(ra) -- Store: [0x80007fa0]:0xFFEB0580




Last Coverpoint : ['rs1 : x20', 'rs2 : x22', 'rd : x22', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb0580f98a7dbd and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xb0580f98a7dbd and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000028c]:fadd.d s6, s4, s6, dyn
	-[0x80000290]:csrrs tp, fcsr, zero
	-[0x80000294]:sw s6, 128(ra)
	-[0x80000298]:sw s7, 136(ra)
	-[0x8000029c]:sw s6, 144(ra)
Current Store : [0x8000029c] : sw s6, 144(ra) -- Store: [0x80007fa8]:0x00000000




Last Coverpoint : ['rs1 : x22', 'rs2 : x18', 'rd : x20', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xaeaa51052e977 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xaeaa51052e977 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002dc]:fadd.d s4, s6, s2, dyn
	-[0x800002e0]:csrrs tp, fcsr, zero
	-[0x800002e4]:sw s4, 160(ra)
	-[0x800002e8]:sw s5, 168(ra)
Current Store : [0x800002e8] : sw s5, 168(ra) -- Store: [0x80007fc0]:0x7FEB0580




Last Coverpoint : ['rs1 : x22', 'rs2 : x18', 'rd : x20', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xaeaa51052e977 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xaeaa51052e977 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002dc]:fadd.d s4, s6, s2, dyn
	-[0x800002e0]:csrrs tp, fcsr, zero
	-[0x800002e4]:sw s4, 160(ra)
	-[0x800002e8]:sw s5, 168(ra)
	-[0x800002ec]:sw s4, 176(ra)
Current Store : [0x800002ec] : sw s4, 176(ra) -- Store: [0x80007fc8]:0x00000000




Last Coverpoint : ['rs1 : x16', 'rs2 : x20', 'rd : x18', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xaeaa51052e977 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xaeaa51052e977 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000032c]:fadd.d s2, a6, s4, dyn
	-[0x80000330]:csrrs tp, fcsr, zero
	-[0x80000334]:sw s2, 192(ra)
	-[0x80000338]:sw s3, 200(ra)
Current Store : [0x80000338] : sw s3, 200(ra) -- Store: [0x80007fe0]:0xFFDAEAA5




Last Coverpoint : ['rs1 : x16', 'rs2 : x20', 'rd : x18', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xaeaa51052e977 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xaeaa51052e977 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000032c]:fadd.d s2, a6, s4, dyn
	-[0x80000330]:csrrs tp, fcsr, zero
	-[0x80000334]:sw s2, 192(ra)
	-[0x80000338]:sw s3, 200(ra)
	-[0x8000033c]:sw s2, 208(ra)
Current Store : [0x8000033c] : sw s2, 208(ra) -- Store: [0x80007fe8]:0x00000000




Last Coverpoint : ['rs1 : x18', 'rs2 : x14', 'rd : x16', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xaeaa51052e977 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xaeaa51052e977 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000037c]:fadd.d a6, s2, a4, dyn
	-[0x80000380]:csrrs tp, fcsr, zero
	-[0x80000384]:sw a6, 224(ra)
	-[0x80000388]:sw a7, 232(ra)
Current Store : [0x80000388] : sw a7, 232(ra) -- Store: [0x80008000]:0x7FDAEAA5




Last Coverpoint : ['rs1 : x18', 'rs2 : x14', 'rd : x16', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xaeaa51052e977 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xaeaa51052e977 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000037c]:fadd.d a6, s2, a4, dyn
	-[0x80000380]:csrrs tp, fcsr, zero
	-[0x80000384]:sw a6, 224(ra)
	-[0x80000388]:sw a7, 232(ra)
	-[0x8000038c]:sw a6, 240(ra)
Current Store : [0x8000038c] : sw a6, 240(ra) -- Store: [0x80008008]:0x00000000




Last Coverpoint : ['rs1 : x12', 'rs2 : x16', 'rd : x14', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xaeaa51052e977 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xaeaa51052e977 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003cc]:fadd.d a4, a2, a6, dyn
	-[0x800003d0]:csrrs tp, fcsr, zero
	-[0x800003d4]:sw a4, 256(ra)
	-[0x800003d8]:sw a5, 264(ra)
Current Store : [0x800003d8] : sw a5, 264(ra) -- Store: [0x80008020]:0xFFDAEAA5




Last Coverpoint : ['rs1 : x12', 'rs2 : x16', 'rd : x14', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xaeaa51052e977 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xaeaa51052e977 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003cc]:fadd.d a4, a2, a6, dyn
	-[0x800003d0]:csrrs tp, fcsr, zero
	-[0x800003d4]:sw a4, 256(ra)
	-[0x800003d8]:sw a5, 264(ra)
	-[0x800003dc]:sw a4, 272(ra)
Current Store : [0x800003dc] : sw a4, 272(ra) -- Store: [0x80008028]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rs2 : x10', 'rd : x12', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xaeaa51052e977 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xaeaa51052e977 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000424]:fadd.d a2, a4, a0, dyn
	-[0x80000428]:csrrs a7, fcsr, zero
	-[0x8000042c]:sw a2, 288(ra)
	-[0x80000430]:sw a3, 296(ra)
Current Store : [0x80000430] : sw a3, 296(ra) -- Store: [0x80008040]:0x7FDAEAA5




Last Coverpoint : ['rs1 : x14', 'rs2 : x10', 'rd : x12', 'fs1 == 0 and fe1 == 0x7fd and fm1 == 0xaeaa51052e977 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xaeaa51052e977 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000424]:fadd.d a2, a4, a0, dyn
	-[0x80000428]:csrrs a7, fcsr, zero
	-[0x8000042c]:sw a2, 288(ra)
	-[0x80000430]:sw a3, 296(ra)
	-[0x80000434]:sw a2, 304(ra)
Current Store : [0x80000434] : sw a2, 304(ra) -- Store: [0x80008048]:0x00000000




Last Coverpoint : ['rs1 : x8', 'rs2 : x12', 'rd : x10', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0x05c5ccdf19706 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x05c5ccdf19706 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000474]:fadd.d a0, fp, a2, dyn
	-[0x80000478]:csrrs a7, fcsr, zero
	-[0x8000047c]:sw a0, 320(ra)
	-[0x80000480]:sw a1, 328(ra)
Current Store : [0x80000480] : sw a1, 328(ra) -- Store: [0x80008060]:0xFFDAEAA5




Last Coverpoint : ['rs1 : x8', 'rs2 : x12', 'rd : x10', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0x05c5ccdf19706 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x05c5ccdf19706 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000474]:fadd.d a0, fp, a2, dyn
	-[0x80000478]:csrrs a7, fcsr, zero
	-[0x8000047c]:sw a0, 320(ra)
	-[0x80000480]:sw a1, 328(ra)
	-[0x80000484]:sw a0, 336(ra)
Current Store : [0x80000484] : sw a0, 336(ra) -- Store: [0x80008068]:0x00000000




Last Coverpoint : ['rs1 : x10', 'rs2 : x6', 'rd : x8', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0x05c5ccdf19706 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x05c5ccdf19706 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004cc]:fadd.d fp, a0, t1, dyn
	-[0x800004d0]:csrrs a7, fcsr, zero
	-[0x800004d4]:sw fp, 0(ra)
	-[0x800004d8]:sw s1, 8(ra)
Current Store : [0x800004d8] : sw s1, 8(ra) -- Store: [0x80007fd0]:0x7FE05C5C




Last Coverpoint : ['rs1 : x10', 'rs2 : x6', 'rd : x8', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0x05c5ccdf19706 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x05c5ccdf19706 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004cc]:fadd.d fp, a0, t1, dyn
	-[0x800004d0]:csrrs a7, fcsr, zero
	-[0x800004d4]:sw fp, 0(ra)
	-[0x800004d8]:sw s1, 8(ra)
	-[0x800004dc]:sw fp, 16(ra)
Current Store : [0x800004dc] : sw fp, 16(ra) -- Store: [0x80007fd8]:0x00000000




Last Coverpoint : ['rs1 : x4', 'rs2 : x8', 'rd : x6', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0x05c5ccdf19706 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x05c5ccdf19706 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000051c]:fadd.d t1, tp, fp, dyn
	-[0x80000520]:csrrs a7, fcsr, zero
	-[0x80000524]:sw t1, 32(ra)
	-[0x80000528]:sw t2, 40(ra)
Current Store : [0x80000528] : sw t2, 40(ra) -- Store: [0x80007ff0]:0xFFE05C5C




Last Coverpoint : ['rs1 : x4', 'rs2 : x8', 'rd : x6', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0x05c5ccdf19706 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x05c5ccdf19706 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000051c]:fadd.d t1, tp, fp, dyn
	-[0x80000520]:csrrs a7, fcsr, zero
	-[0x80000524]:sw t1, 32(ra)
	-[0x80000528]:sw t2, 40(ra)
	-[0x8000052c]:sw t1, 48(ra)
Current Store : [0x8000052c] : sw t1, 48(ra) -- Store: [0x80007ff8]:0x00000000




Last Coverpoint : ['rs1 : x6', 'rs2 : x2', 'rd : x4', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0x05c5ccdf19706 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x05c5ccdf19706 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000056c]:fadd.d tp, t1, sp, dyn
	-[0x80000570]:csrrs a7, fcsr, zero
	-[0x80000574]:sw tp, 64(ra)
	-[0x80000578]:sw t0, 72(ra)
Current Store : [0x80000578] : sw t0, 72(ra) -- Store: [0x80008010]:0x7FE05C5C




Last Coverpoint : ['rs1 : x6', 'rs2 : x2', 'rd : x4', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0x05c5ccdf19706 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x05c5ccdf19706 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000056c]:fadd.d tp, t1, sp, dyn
	-[0x80000570]:csrrs a7, fcsr, zero
	-[0x80000574]:sw tp, 64(ra)
	-[0x80000578]:sw t0, 72(ra)
	-[0x8000057c]:sw tp, 80(ra)
Current Store : [0x8000057c] : sw tp, 80(ra) -- Store: [0x80008018]:0x00000000




Last Coverpoint : ['rs1 : x2', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0x05c5ccdf19706 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x05c5ccdf19706 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005bc]:fadd.d t5, sp, t3, dyn
	-[0x800005c0]:csrrs a7, fcsr, zero
	-[0x800005c4]:sw t5, 96(ra)
	-[0x800005c8]:sw t6, 104(ra)
Current Store : [0x800005c8] : sw t6, 104(ra) -- Store: [0x80008030]:0x7FEB0580




Last Coverpoint : ['rs1 : x2', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0x05c5ccdf19706 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x05c5ccdf19706 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005bc]:fadd.d t5, sp, t3, dyn
	-[0x800005c0]:csrrs a7, fcsr, zero
	-[0x800005c4]:sw t5, 96(ra)
	-[0x800005c8]:sw t6, 104(ra)
	-[0x800005cc]:sw t5, 112(ra)
Current Store : [0x800005cc] : sw t5, 112(ra) -- Store: [0x80008038]:0x00000000




Last Coverpoint : ['rs2 : x4', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0x914e0c751c4f4 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x914e0c751c4f4 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000060c]:fadd.d t5, t3, tp, dyn
	-[0x80000610]:csrrs a7, fcsr, zero
	-[0x80000614]:sw t5, 128(ra)
	-[0x80000618]:sw t6, 136(ra)
Current Store : [0x80000618] : sw t6, 136(ra) -- Store: [0x80008050]:0x7FEB0580




Last Coverpoint : ['rs2 : x4', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0x914e0c751c4f4 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x914e0c751c4f4 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000060c]:fadd.d t5, t3, tp, dyn
	-[0x80000610]:csrrs a7, fcsr, zero
	-[0x80000614]:sw t5, 128(ra)
	-[0x80000618]:sw t6, 136(ra)
	-[0x8000061c]:sw t5, 144(ra)
Current Store : [0x8000061c] : sw t5, 144(ra) -- Store: [0x80008058]:0x00000000




Last Coverpoint : ['rd : x2', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0x914e0c751c4f4 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x914e0c751c4f4 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000065c]:fadd.d sp, t5, t3, dyn
	-[0x80000660]:csrrs a7, fcsr, zero
	-[0x80000664]:sw sp, 160(ra)
	-[0x80000668]:sw gp, 168(ra)
Current Store : [0x80000668] : sw gp, 168(ra) -- Store: [0x80008070]:0x7FE05C5C




Last Coverpoint : ['rd : x2', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0x914e0c751c4f4 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x914e0c751c4f4 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000065c]:fadd.d sp, t5, t3, dyn
	-[0x80000660]:csrrs a7, fcsr, zero
	-[0x80000664]:sw sp, 160(ra)
	-[0x80000668]:sw gp, 168(ra)
	-[0x8000066c]:sw sp, 176(ra)
Current Store : [0x8000066c] : sw sp, 176(ra) -- Store: [0x80008078]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x914e0c751c4f4 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x914e0c751c4f4 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006ac]:fadd.d t5, t3, s10, dyn
	-[0x800006b0]:csrrs a7, fcsr, zero
	-[0x800006b4]:sw t5, 192(ra)
	-[0x800006b8]:sw t6, 200(ra)
Current Store : [0x800006b8] : sw t6, 200(ra) -- Store: [0x80008090]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x914e0c751c4f4 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x914e0c751c4f4 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006ac]:fadd.d t5, t3, s10, dyn
	-[0x800006b0]:csrrs a7, fcsr, zero
	-[0x800006b4]:sw t5, 192(ra)
	-[0x800006b8]:sw t6, 200(ra)
	-[0x800006bc]:sw t5, 208(ra)
Current Store : [0x800006bc] : sw t5, 208(ra) -- Store: [0x80008098]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x914e0c751c4f4 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x914e0c751c4f4 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006fc]:fadd.d t5, t3, s10, dyn
	-[0x80000700]:csrrs a7, fcsr, zero
	-[0x80000704]:sw t5, 224(ra)
	-[0x80000708]:sw t6, 232(ra)
Current Store : [0x80000708] : sw t6, 232(ra) -- Store: [0x800080b0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x914e0c751c4f4 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x914e0c751c4f4 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006fc]:fadd.d t5, t3, s10, dyn
	-[0x80000700]:csrrs a7, fcsr, zero
	-[0x80000704]:sw t5, 224(ra)
	-[0x80000708]:sw t6, 232(ra)
	-[0x8000070c]:sw t5, 240(ra)
Current Store : [0x8000070c] : sw t5, 240(ra) -- Store: [0x800080b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x914e0c751c4f4 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x914e0c751c4f4 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000074c]:fadd.d t5, t3, s10, dyn
	-[0x80000750]:csrrs a7, fcsr, zero
	-[0x80000754]:sw t5, 256(ra)
	-[0x80000758]:sw t6, 264(ra)
Current Store : [0x80000758] : sw t6, 264(ra) -- Store: [0x800080d0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x914e0c751c4f4 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x914e0c751c4f4 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000074c]:fadd.d t5, t3, s10, dyn
	-[0x80000750]:csrrs a7, fcsr, zero
	-[0x80000754]:sw t5, 256(ra)
	-[0x80000758]:sw t6, 264(ra)
	-[0x8000075c]:sw t5, 272(ra)
Current Store : [0x8000075c] : sw t5, 272(ra) -- Store: [0x800080d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe809082dd48fb and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe809082dd48fb and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000079c]:fadd.d t5, t3, s10, dyn
	-[0x800007a0]:csrrs a7, fcsr, zero
	-[0x800007a4]:sw t5, 288(ra)
	-[0x800007a8]:sw t6, 296(ra)
Current Store : [0x800007a8] : sw t6, 296(ra) -- Store: [0x800080f0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe809082dd48fb and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe809082dd48fb and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000079c]:fadd.d t5, t3, s10, dyn
	-[0x800007a0]:csrrs a7, fcsr, zero
	-[0x800007a4]:sw t5, 288(ra)
	-[0x800007a8]:sw t6, 296(ra)
	-[0x800007ac]:sw t5, 304(ra)
Current Store : [0x800007ac] : sw t5, 304(ra) -- Store: [0x800080f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe809082dd48fb and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe809082dd48fb and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007ec]:fadd.d t5, t3, s10, dyn
	-[0x800007f0]:csrrs a7, fcsr, zero
	-[0x800007f4]:sw t5, 320(ra)
	-[0x800007f8]:sw t6, 328(ra)
Current Store : [0x800007f8] : sw t6, 328(ra) -- Store: [0x80008110]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe809082dd48fb and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe809082dd48fb and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007ec]:fadd.d t5, t3, s10, dyn
	-[0x800007f0]:csrrs a7, fcsr, zero
	-[0x800007f4]:sw t5, 320(ra)
	-[0x800007f8]:sw t6, 328(ra)
	-[0x800007fc]:sw t5, 336(ra)
Current Store : [0x800007fc] : sw t5, 336(ra) -- Store: [0x80008118]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe809082dd48fb and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe809082dd48fb and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000083c]:fadd.d t5, t3, s10, dyn
	-[0x80000840]:csrrs a7, fcsr, zero
	-[0x80000844]:sw t5, 352(ra)
	-[0x80000848]:sw t6, 360(ra)
Current Store : [0x80000848] : sw t6, 360(ra) -- Store: [0x80008130]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe809082dd48fb and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe809082dd48fb and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000083c]:fadd.d t5, t3, s10, dyn
	-[0x80000840]:csrrs a7, fcsr, zero
	-[0x80000844]:sw t5, 352(ra)
	-[0x80000848]:sw t6, 360(ra)
	-[0x8000084c]:sw t5, 368(ra)
Current Store : [0x8000084c] : sw t5, 368(ra) -- Store: [0x80008138]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe809082dd48fb and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe809082dd48fb and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000088c]:fadd.d t5, t3, s10, dyn
	-[0x80000890]:csrrs a7, fcsr, zero
	-[0x80000894]:sw t5, 384(ra)
	-[0x80000898]:sw t6, 392(ra)
Current Store : [0x80000898] : sw t6, 392(ra) -- Store: [0x80008150]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe809082dd48fb and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe809082dd48fb and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000088c]:fadd.d t5, t3, s10, dyn
	-[0x80000890]:csrrs a7, fcsr, zero
	-[0x80000894]:sw t5, 384(ra)
	-[0x80000898]:sw t6, 392(ra)
	-[0x8000089c]:sw t5, 400(ra)
Current Store : [0x8000089c] : sw t5, 400(ra) -- Store: [0x80008158]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe809082dd48fb and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe809082dd48fb and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008dc]:fadd.d t5, t3, s10, dyn
	-[0x800008e0]:csrrs a7, fcsr, zero
	-[0x800008e4]:sw t5, 416(ra)
	-[0x800008e8]:sw t6, 424(ra)
Current Store : [0x800008e8] : sw t6, 424(ra) -- Store: [0x80008170]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe809082dd48fb and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe809082dd48fb and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008dc]:fadd.d t5, t3, s10, dyn
	-[0x800008e0]:csrrs a7, fcsr, zero
	-[0x800008e4]:sw t5, 416(ra)
	-[0x800008e8]:sw t6, 424(ra)
	-[0x800008ec]:sw t5, 432(ra)
Current Store : [0x800008ec] : sw t5, 432(ra) -- Store: [0x80008178]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd0f42c0dfaf72 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd0f42c0dfaf72 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000092c]:fadd.d t5, t3, s10, dyn
	-[0x80000930]:csrrs a7, fcsr, zero
	-[0x80000934]:sw t5, 448(ra)
	-[0x80000938]:sw t6, 456(ra)
Current Store : [0x80000938] : sw t6, 456(ra) -- Store: [0x80008190]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd0f42c0dfaf72 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd0f42c0dfaf72 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000092c]:fadd.d t5, t3, s10, dyn
	-[0x80000930]:csrrs a7, fcsr, zero
	-[0x80000934]:sw t5, 448(ra)
	-[0x80000938]:sw t6, 456(ra)
	-[0x8000093c]:sw t5, 464(ra)
Current Store : [0x8000093c] : sw t5, 464(ra) -- Store: [0x80008198]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd0f42c0dfaf72 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd0f42c0dfaf72 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000097c]:fadd.d t5, t3, s10, dyn
	-[0x80000980]:csrrs a7, fcsr, zero
	-[0x80000984]:sw t5, 480(ra)
	-[0x80000988]:sw t6, 488(ra)
Current Store : [0x80000988] : sw t6, 488(ra) -- Store: [0x800081b0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd0f42c0dfaf72 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd0f42c0dfaf72 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000097c]:fadd.d t5, t3, s10, dyn
	-[0x80000980]:csrrs a7, fcsr, zero
	-[0x80000984]:sw t5, 480(ra)
	-[0x80000988]:sw t6, 488(ra)
	-[0x8000098c]:sw t5, 496(ra)
Current Store : [0x8000098c] : sw t5, 496(ra) -- Store: [0x800081b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd0f42c0dfaf72 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd0f42c0dfaf72 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009cc]:fadd.d t5, t3, s10, dyn
	-[0x800009d0]:csrrs a7, fcsr, zero
	-[0x800009d4]:sw t5, 512(ra)
	-[0x800009d8]:sw t6, 520(ra)
Current Store : [0x800009d8] : sw t6, 520(ra) -- Store: [0x800081d0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd0f42c0dfaf72 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd0f42c0dfaf72 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009cc]:fadd.d t5, t3, s10, dyn
	-[0x800009d0]:csrrs a7, fcsr, zero
	-[0x800009d4]:sw t5, 512(ra)
	-[0x800009d8]:sw t6, 520(ra)
	-[0x800009dc]:sw t5, 528(ra)
Current Store : [0x800009dc] : sw t5, 528(ra) -- Store: [0x800081d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd0f42c0dfaf72 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd0f42c0dfaf72 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a1c]:fadd.d t5, t3, s10, dyn
	-[0x80000a20]:csrrs a7, fcsr, zero
	-[0x80000a24]:sw t5, 544(ra)
	-[0x80000a28]:sw t6, 552(ra)
Current Store : [0x80000a28] : sw t6, 552(ra) -- Store: [0x800081f0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd0f42c0dfaf72 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd0f42c0dfaf72 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a1c]:fadd.d t5, t3, s10, dyn
	-[0x80000a20]:csrrs a7, fcsr, zero
	-[0x80000a24]:sw t5, 544(ra)
	-[0x80000a28]:sw t6, 552(ra)
	-[0x80000a2c]:sw t5, 560(ra)
Current Store : [0x80000a2c] : sw t5, 560(ra) -- Store: [0x800081f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd0f42c0dfaf72 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd0f42c0dfaf72 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a6c]:fadd.d t5, t3, s10, dyn
	-[0x80000a70]:csrrs a7, fcsr, zero
	-[0x80000a74]:sw t5, 576(ra)
	-[0x80000a78]:sw t6, 584(ra)
Current Store : [0x80000a78] : sw t6, 584(ra) -- Store: [0x80008210]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd0f42c0dfaf72 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd0f42c0dfaf72 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a6c]:fadd.d t5, t3, s10, dyn
	-[0x80000a70]:csrrs a7, fcsr, zero
	-[0x80000a74]:sw t5, 576(ra)
	-[0x80000a78]:sw t6, 584(ra)
	-[0x80000a7c]:sw t5, 592(ra)
Current Store : [0x80000a7c] : sw t5, 592(ra) -- Store: [0x80008218]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x209a1991e3307 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x209a1991e3307 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000abc]:fadd.d t5, t3, s10, dyn
	-[0x80000ac0]:csrrs a7, fcsr, zero
	-[0x80000ac4]:sw t5, 608(ra)
	-[0x80000ac8]:sw t6, 616(ra)
Current Store : [0x80000ac8] : sw t6, 616(ra) -- Store: [0x80008230]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x209a1991e3307 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x209a1991e3307 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000abc]:fadd.d t5, t3, s10, dyn
	-[0x80000ac0]:csrrs a7, fcsr, zero
	-[0x80000ac4]:sw t5, 608(ra)
	-[0x80000ac8]:sw t6, 616(ra)
	-[0x80000acc]:sw t5, 624(ra)
Current Store : [0x80000acc] : sw t5, 624(ra) -- Store: [0x80008238]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x209a1991e3307 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x209a1991e3307 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b0c]:fadd.d t5, t3, s10, dyn
	-[0x80000b10]:csrrs a7, fcsr, zero
	-[0x80000b14]:sw t5, 640(ra)
	-[0x80000b18]:sw t6, 648(ra)
Current Store : [0x80000b18] : sw t6, 648(ra) -- Store: [0x80008250]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x209a1991e3307 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x209a1991e3307 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b0c]:fadd.d t5, t3, s10, dyn
	-[0x80000b10]:csrrs a7, fcsr, zero
	-[0x80000b14]:sw t5, 640(ra)
	-[0x80000b18]:sw t6, 648(ra)
	-[0x80000b1c]:sw t5, 656(ra)
Current Store : [0x80000b1c] : sw t5, 656(ra) -- Store: [0x80008258]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x209a1991e3307 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x209a1991e3307 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b5c]:fadd.d t5, t3, s10, dyn
	-[0x80000b60]:csrrs a7, fcsr, zero
	-[0x80000b64]:sw t5, 672(ra)
	-[0x80000b68]:sw t6, 680(ra)
Current Store : [0x80000b68] : sw t6, 680(ra) -- Store: [0x80008270]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x209a1991e3307 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x209a1991e3307 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b5c]:fadd.d t5, t3, s10, dyn
	-[0x80000b60]:csrrs a7, fcsr, zero
	-[0x80000b64]:sw t5, 672(ra)
	-[0x80000b68]:sw t6, 680(ra)
	-[0x80000b6c]:sw t5, 688(ra)
Current Store : [0x80000b6c] : sw t5, 688(ra) -- Store: [0x80008278]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x209a1991e3307 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x209a1991e3307 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bac]:fadd.d t5, t3, s10, dyn
	-[0x80000bb0]:csrrs a7, fcsr, zero
	-[0x80000bb4]:sw t5, 704(ra)
	-[0x80000bb8]:sw t6, 712(ra)
Current Store : [0x80000bb8] : sw t6, 712(ra) -- Store: [0x80008290]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x209a1991e3307 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x209a1991e3307 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bac]:fadd.d t5, t3, s10, dyn
	-[0x80000bb0]:csrrs a7, fcsr, zero
	-[0x80000bb4]:sw t5, 704(ra)
	-[0x80000bb8]:sw t6, 712(ra)
	-[0x80000bbc]:sw t5, 720(ra)
Current Store : [0x80000bbc] : sw t5, 720(ra) -- Store: [0x80008298]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x209a1991e3307 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x209a1991e3307 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bfc]:fadd.d t5, t3, s10, dyn
	-[0x80000c00]:csrrs a7, fcsr, zero
	-[0x80000c04]:sw t5, 736(ra)
	-[0x80000c08]:sw t6, 744(ra)
Current Store : [0x80000c08] : sw t6, 744(ra) -- Store: [0x800082b0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x209a1991e3307 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x209a1991e3307 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bfc]:fadd.d t5, t3, s10, dyn
	-[0x80000c00]:csrrs a7, fcsr, zero
	-[0x80000c04]:sw t5, 736(ra)
	-[0x80000c08]:sw t6, 744(ra)
	-[0x80000c0c]:sw t5, 752(ra)
Current Store : [0x80000c0c] : sw t5, 752(ra) -- Store: [0x800082b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3c9adc7329695 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x3c9adc7329695 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c4c]:fadd.d t5, t3, s10, dyn
	-[0x80000c50]:csrrs a7, fcsr, zero
	-[0x80000c54]:sw t5, 768(ra)
	-[0x80000c58]:sw t6, 776(ra)
Current Store : [0x80000c58] : sw t6, 776(ra) -- Store: [0x800082d0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3c9adc7329695 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x3c9adc7329695 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c4c]:fadd.d t5, t3, s10, dyn
	-[0x80000c50]:csrrs a7, fcsr, zero
	-[0x80000c54]:sw t5, 768(ra)
	-[0x80000c58]:sw t6, 776(ra)
	-[0x80000c5c]:sw t5, 784(ra)
Current Store : [0x80000c5c] : sw t5, 784(ra) -- Store: [0x800082d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3c9adc7329695 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x3c9adc7329695 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c9c]:fadd.d t5, t3, s10, dyn
	-[0x80000ca0]:csrrs a7, fcsr, zero
	-[0x80000ca4]:sw t5, 800(ra)
	-[0x80000ca8]:sw t6, 808(ra)
Current Store : [0x80000ca8] : sw t6, 808(ra) -- Store: [0x800082f0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3c9adc7329695 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x3c9adc7329695 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c9c]:fadd.d t5, t3, s10, dyn
	-[0x80000ca0]:csrrs a7, fcsr, zero
	-[0x80000ca4]:sw t5, 800(ra)
	-[0x80000ca8]:sw t6, 808(ra)
	-[0x80000cac]:sw t5, 816(ra)
Current Store : [0x80000cac] : sw t5, 816(ra) -- Store: [0x800082f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3c9adc7329695 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x3c9adc7329695 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cec]:fadd.d t5, t3, s10, dyn
	-[0x80000cf0]:csrrs a7, fcsr, zero
	-[0x80000cf4]:sw t5, 832(ra)
	-[0x80000cf8]:sw t6, 840(ra)
Current Store : [0x80000cf8] : sw t6, 840(ra) -- Store: [0x80008310]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3c9adc7329695 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x3c9adc7329695 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cec]:fadd.d t5, t3, s10, dyn
	-[0x80000cf0]:csrrs a7, fcsr, zero
	-[0x80000cf4]:sw t5, 832(ra)
	-[0x80000cf8]:sw t6, 840(ra)
	-[0x80000cfc]:sw t5, 848(ra)
Current Store : [0x80000cfc] : sw t5, 848(ra) -- Store: [0x80008318]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3c9adc7329695 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x3c9adc7329695 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d3c]:fadd.d t5, t3, s10, dyn
	-[0x80000d40]:csrrs a7, fcsr, zero
	-[0x80000d44]:sw t5, 864(ra)
	-[0x80000d48]:sw t6, 872(ra)
Current Store : [0x80000d48] : sw t6, 872(ra) -- Store: [0x80008330]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3c9adc7329695 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x3c9adc7329695 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d3c]:fadd.d t5, t3, s10, dyn
	-[0x80000d40]:csrrs a7, fcsr, zero
	-[0x80000d44]:sw t5, 864(ra)
	-[0x80000d48]:sw t6, 872(ra)
	-[0x80000d4c]:sw t5, 880(ra)
Current Store : [0x80000d4c] : sw t5, 880(ra) -- Store: [0x80008338]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3c9adc7329695 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x3c9adc7329695 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d8c]:fadd.d t5, t3, s10, dyn
	-[0x80000d90]:csrrs a7, fcsr, zero
	-[0x80000d94]:sw t5, 896(ra)
	-[0x80000d98]:sw t6, 904(ra)
Current Store : [0x80000d98] : sw t6, 904(ra) -- Store: [0x80008350]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3c9adc7329695 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x3c9adc7329695 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d8c]:fadd.d t5, t3, s10, dyn
	-[0x80000d90]:csrrs a7, fcsr, zero
	-[0x80000d94]:sw t5, 896(ra)
	-[0x80000d98]:sw t6, 904(ra)
	-[0x80000d9c]:sw t5, 912(ra)
Current Store : [0x80000d9c] : sw t5, 912(ra) -- Store: [0x80008358]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd1ca42e21585b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd1ca42e21585b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ddc]:fadd.d t5, t3, s10, dyn
	-[0x80000de0]:csrrs a7, fcsr, zero
	-[0x80000de4]:sw t5, 928(ra)
	-[0x80000de8]:sw t6, 936(ra)
Current Store : [0x80000de8] : sw t6, 936(ra) -- Store: [0x80008370]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd1ca42e21585b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd1ca42e21585b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ddc]:fadd.d t5, t3, s10, dyn
	-[0x80000de0]:csrrs a7, fcsr, zero
	-[0x80000de4]:sw t5, 928(ra)
	-[0x80000de8]:sw t6, 936(ra)
	-[0x80000dec]:sw t5, 944(ra)
Current Store : [0x80000dec] : sw t5, 944(ra) -- Store: [0x80008378]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd1ca42e21585b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd1ca42e21585b and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e2c]:fadd.d t5, t3, s10, dyn
	-[0x80000e30]:csrrs a7, fcsr, zero
	-[0x80000e34]:sw t5, 960(ra)
	-[0x80000e38]:sw t6, 968(ra)
Current Store : [0x80000e38] : sw t6, 968(ra) -- Store: [0x80008390]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd1ca42e21585b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd1ca42e21585b and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e2c]:fadd.d t5, t3, s10, dyn
	-[0x80000e30]:csrrs a7, fcsr, zero
	-[0x80000e34]:sw t5, 960(ra)
	-[0x80000e38]:sw t6, 968(ra)
	-[0x80000e3c]:sw t5, 976(ra)
Current Store : [0x80000e3c] : sw t5, 976(ra) -- Store: [0x80008398]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd1ca42e21585b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd1ca42e21585b and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e7c]:fadd.d t5, t3, s10, dyn
	-[0x80000e80]:csrrs a7, fcsr, zero
	-[0x80000e84]:sw t5, 992(ra)
	-[0x80000e88]:sw t6, 1000(ra)
Current Store : [0x80000e88] : sw t6, 1000(ra) -- Store: [0x800083b0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd1ca42e21585b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd1ca42e21585b and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e7c]:fadd.d t5, t3, s10, dyn
	-[0x80000e80]:csrrs a7, fcsr, zero
	-[0x80000e84]:sw t5, 992(ra)
	-[0x80000e88]:sw t6, 1000(ra)
	-[0x80000e8c]:sw t5, 1008(ra)
Current Store : [0x80000e8c] : sw t5, 1008(ra) -- Store: [0x800083b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd1ca42e21585b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd1ca42e21585b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ecc]:fadd.d t5, t3, s10, dyn
	-[0x80000ed0]:csrrs a7, fcsr, zero
	-[0x80000ed4]:sw t5, 1024(ra)
	-[0x80000ed8]:sw t6, 1032(ra)
Current Store : [0x80000ed8] : sw t6, 1032(ra) -- Store: [0x800083d0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd1ca42e21585b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd1ca42e21585b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ecc]:fadd.d t5, t3, s10, dyn
	-[0x80000ed0]:csrrs a7, fcsr, zero
	-[0x80000ed4]:sw t5, 1024(ra)
	-[0x80000ed8]:sw t6, 1032(ra)
	-[0x80000edc]:sw t5, 1040(ra)
Current Store : [0x80000edc] : sw t5, 1040(ra) -- Store: [0x800083d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd1ca42e21585b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd1ca42e21585b and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f1c]:fadd.d t5, t3, s10, dyn
	-[0x80000f20]:csrrs a7, fcsr, zero
	-[0x80000f24]:sw t5, 1056(ra)
	-[0x80000f28]:sw t6, 1064(ra)
Current Store : [0x80000f28] : sw t6, 1064(ra) -- Store: [0x800083f0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd1ca42e21585b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd1ca42e21585b and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f1c]:fadd.d t5, t3, s10, dyn
	-[0x80000f20]:csrrs a7, fcsr, zero
	-[0x80000f24]:sw t5, 1056(ra)
	-[0x80000f28]:sw t6, 1064(ra)
	-[0x80000f2c]:sw t5, 1072(ra)
Current Store : [0x80000f2c] : sw t5, 1072(ra) -- Store: [0x800083f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9ed4cb2685903 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9ed4cb2685903 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f6c]:fadd.d t5, t3, s10, dyn
	-[0x80000f70]:csrrs a7, fcsr, zero
	-[0x80000f74]:sw t5, 1088(ra)
	-[0x80000f78]:sw t6, 1096(ra)
Current Store : [0x80000f78] : sw t6, 1096(ra) -- Store: [0x80008410]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9ed4cb2685903 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9ed4cb2685903 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f6c]:fadd.d t5, t3, s10, dyn
	-[0x80000f70]:csrrs a7, fcsr, zero
	-[0x80000f74]:sw t5, 1088(ra)
	-[0x80000f78]:sw t6, 1096(ra)
	-[0x80000f7c]:sw t5, 1104(ra)
Current Store : [0x80000f7c] : sw t5, 1104(ra) -- Store: [0x80008418]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9ed4cb2685903 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9ed4cb2685903 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fbc]:fadd.d t5, t3, s10, dyn
	-[0x80000fc0]:csrrs a7, fcsr, zero
	-[0x80000fc4]:sw t5, 1120(ra)
	-[0x80000fc8]:sw t6, 1128(ra)
Current Store : [0x80000fc8] : sw t6, 1128(ra) -- Store: [0x80008430]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9ed4cb2685903 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9ed4cb2685903 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fbc]:fadd.d t5, t3, s10, dyn
	-[0x80000fc0]:csrrs a7, fcsr, zero
	-[0x80000fc4]:sw t5, 1120(ra)
	-[0x80000fc8]:sw t6, 1128(ra)
	-[0x80000fcc]:sw t5, 1136(ra)
Current Store : [0x80000fcc] : sw t5, 1136(ra) -- Store: [0x80008438]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9ed4cb2685903 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9ed4cb2685903 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000100c]:fadd.d t5, t3, s10, dyn
	-[0x80001010]:csrrs a7, fcsr, zero
	-[0x80001014]:sw t5, 1152(ra)
	-[0x80001018]:sw t6, 1160(ra)
Current Store : [0x80001018] : sw t6, 1160(ra) -- Store: [0x80008450]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9ed4cb2685903 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9ed4cb2685903 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000100c]:fadd.d t5, t3, s10, dyn
	-[0x80001010]:csrrs a7, fcsr, zero
	-[0x80001014]:sw t5, 1152(ra)
	-[0x80001018]:sw t6, 1160(ra)
	-[0x8000101c]:sw t5, 1168(ra)
Current Store : [0x8000101c] : sw t5, 1168(ra) -- Store: [0x80008458]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9ed4cb2685903 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9ed4cb2685903 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000105c]:fadd.d t5, t3, s10, dyn
	-[0x80001060]:csrrs a7, fcsr, zero
	-[0x80001064]:sw t5, 1184(ra)
	-[0x80001068]:sw t6, 1192(ra)
Current Store : [0x80001068] : sw t6, 1192(ra) -- Store: [0x80008470]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9ed4cb2685903 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9ed4cb2685903 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000105c]:fadd.d t5, t3, s10, dyn
	-[0x80001060]:csrrs a7, fcsr, zero
	-[0x80001064]:sw t5, 1184(ra)
	-[0x80001068]:sw t6, 1192(ra)
	-[0x8000106c]:sw t5, 1200(ra)
Current Store : [0x8000106c] : sw t5, 1200(ra) -- Store: [0x80008478]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9ed4cb2685903 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9ed4cb2685903 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010ac]:fadd.d t5, t3, s10, dyn
	-[0x800010b0]:csrrs a7, fcsr, zero
	-[0x800010b4]:sw t5, 1216(ra)
	-[0x800010b8]:sw t6, 1224(ra)
Current Store : [0x800010b8] : sw t6, 1224(ra) -- Store: [0x80008490]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9ed4cb2685903 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9ed4cb2685903 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010ac]:fadd.d t5, t3, s10, dyn
	-[0x800010b0]:csrrs a7, fcsr, zero
	-[0x800010b4]:sw t5, 1216(ra)
	-[0x800010b8]:sw t6, 1224(ra)
	-[0x800010bc]:sw t5, 1232(ra)
Current Store : [0x800010bc] : sw t5, 1232(ra) -- Store: [0x80008498]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3d97530ca446d and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x3d97530ca446d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010fc]:fadd.d t5, t3, s10, dyn
	-[0x80001100]:csrrs a7, fcsr, zero
	-[0x80001104]:sw t5, 1248(ra)
	-[0x80001108]:sw t6, 1256(ra)
Current Store : [0x80001108] : sw t6, 1256(ra) -- Store: [0x800084b0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3d97530ca446d and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x3d97530ca446d and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010fc]:fadd.d t5, t3, s10, dyn
	-[0x80001100]:csrrs a7, fcsr, zero
	-[0x80001104]:sw t5, 1248(ra)
	-[0x80001108]:sw t6, 1256(ra)
	-[0x8000110c]:sw t5, 1264(ra)
Current Store : [0x8000110c] : sw t5, 1264(ra) -- Store: [0x800084b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3d97530ca446d and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x3d97530ca446d and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000114c]:fadd.d t5, t3, s10, dyn
	-[0x80001150]:csrrs a7, fcsr, zero
	-[0x80001154]:sw t5, 1280(ra)
	-[0x80001158]:sw t6, 1288(ra)
Current Store : [0x80001158] : sw t6, 1288(ra) -- Store: [0x800084d0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3d97530ca446d and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x3d97530ca446d and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000114c]:fadd.d t5, t3, s10, dyn
	-[0x80001150]:csrrs a7, fcsr, zero
	-[0x80001154]:sw t5, 1280(ra)
	-[0x80001158]:sw t6, 1288(ra)
	-[0x8000115c]:sw t5, 1296(ra)
Current Store : [0x8000115c] : sw t5, 1296(ra) -- Store: [0x800084d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3d97530ca446d and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x3d97530ca446d and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000119c]:fadd.d t5, t3, s10, dyn
	-[0x800011a0]:csrrs a7, fcsr, zero
	-[0x800011a4]:sw t5, 1312(ra)
	-[0x800011a8]:sw t6, 1320(ra)
Current Store : [0x800011a8] : sw t6, 1320(ra) -- Store: [0x800084f0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3d97530ca446d and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x3d97530ca446d and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000119c]:fadd.d t5, t3, s10, dyn
	-[0x800011a0]:csrrs a7, fcsr, zero
	-[0x800011a4]:sw t5, 1312(ra)
	-[0x800011a8]:sw t6, 1320(ra)
	-[0x800011ac]:sw t5, 1328(ra)
Current Store : [0x800011ac] : sw t5, 1328(ra) -- Store: [0x800084f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3d97530ca446d and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x3d97530ca446d and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011ec]:fadd.d t5, t3, s10, dyn
	-[0x800011f0]:csrrs a7, fcsr, zero
	-[0x800011f4]:sw t5, 1344(ra)
	-[0x800011f8]:sw t6, 1352(ra)
Current Store : [0x800011f8] : sw t6, 1352(ra) -- Store: [0x80008510]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3d97530ca446d and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x3d97530ca446d and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011ec]:fadd.d t5, t3, s10, dyn
	-[0x800011f0]:csrrs a7, fcsr, zero
	-[0x800011f4]:sw t5, 1344(ra)
	-[0x800011f8]:sw t6, 1352(ra)
	-[0x800011fc]:sw t5, 1360(ra)
Current Store : [0x800011fc] : sw t5, 1360(ra) -- Store: [0x80008518]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3d97530ca446d and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x3d97530ca446d and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000123c]:fadd.d t5, t3, s10, dyn
	-[0x80001240]:csrrs a7, fcsr, zero
	-[0x80001244]:sw t5, 1376(ra)
	-[0x80001248]:sw t6, 1384(ra)
Current Store : [0x80001248] : sw t6, 1384(ra) -- Store: [0x80008530]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3d97530ca446d and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x3d97530ca446d and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000123c]:fadd.d t5, t3, s10, dyn
	-[0x80001240]:csrrs a7, fcsr, zero
	-[0x80001244]:sw t5, 1376(ra)
	-[0x80001248]:sw t6, 1384(ra)
	-[0x8000124c]:sw t5, 1392(ra)
Current Store : [0x8000124c] : sw t5, 1392(ra) -- Store: [0x80008538]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcc3488366e29b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xcc3488366e29b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000128c]:fadd.d t5, t3, s10, dyn
	-[0x80001290]:csrrs a7, fcsr, zero
	-[0x80001294]:sw t5, 1408(ra)
	-[0x80001298]:sw t6, 1416(ra)
Current Store : [0x80001298] : sw t6, 1416(ra) -- Store: [0x80008550]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcc3488366e29b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xcc3488366e29b and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000128c]:fadd.d t5, t3, s10, dyn
	-[0x80001290]:csrrs a7, fcsr, zero
	-[0x80001294]:sw t5, 1408(ra)
	-[0x80001298]:sw t6, 1416(ra)
	-[0x8000129c]:sw t5, 1424(ra)
Current Store : [0x8000129c] : sw t5, 1424(ra) -- Store: [0x80008558]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcc3488366e29b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xcc3488366e29b and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012dc]:fadd.d t5, t3, s10, dyn
	-[0x800012e0]:csrrs a7, fcsr, zero
	-[0x800012e4]:sw t5, 1440(ra)
	-[0x800012e8]:sw t6, 1448(ra)
Current Store : [0x800012e8] : sw t6, 1448(ra) -- Store: [0x80008570]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcc3488366e29b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xcc3488366e29b and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012dc]:fadd.d t5, t3, s10, dyn
	-[0x800012e0]:csrrs a7, fcsr, zero
	-[0x800012e4]:sw t5, 1440(ra)
	-[0x800012e8]:sw t6, 1448(ra)
	-[0x800012ec]:sw t5, 1456(ra)
Current Store : [0x800012ec] : sw t5, 1456(ra) -- Store: [0x80008578]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcc3488366e29b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xcc3488366e29b and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000132c]:fadd.d t5, t3, s10, dyn
	-[0x80001330]:csrrs a7, fcsr, zero
	-[0x80001334]:sw t5, 1472(ra)
	-[0x80001338]:sw t6, 1480(ra)
Current Store : [0x80001338] : sw t6, 1480(ra) -- Store: [0x80008590]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcc3488366e29b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xcc3488366e29b and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000132c]:fadd.d t5, t3, s10, dyn
	-[0x80001330]:csrrs a7, fcsr, zero
	-[0x80001334]:sw t5, 1472(ra)
	-[0x80001338]:sw t6, 1480(ra)
	-[0x8000133c]:sw t5, 1488(ra)
Current Store : [0x8000133c] : sw t5, 1488(ra) -- Store: [0x80008598]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcc3488366e29b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xcc3488366e29b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000137c]:fadd.d t5, t3, s10, dyn
	-[0x80001380]:csrrs a7, fcsr, zero
	-[0x80001384]:sw t5, 1504(ra)
	-[0x80001388]:sw t6, 1512(ra)
Current Store : [0x80001388] : sw t6, 1512(ra) -- Store: [0x800085b0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcc3488366e29b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xcc3488366e29b and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000137c]:fadd.d t5, t3, s10, dyn
	-[0x80001380]:csrrs a7, fcsr, zero
	-[0x80001384]:sw t5, 1504(ra)
	-[0x80001388]:sw t6, 1512(ra)
	-[0x8000138c]:sw t5, 1520(ra)
Current Store : [0x8000138c] : sw t5, 1520(ra) -- Store: [0x800085b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcc3488366e29b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xcc3488366e29b and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013cc]:fadd.d t5, t3, s10, dyn
	-[0x800013d0]:csrrs a7, fcsr, zero
	-[0x800013d4]:sw t5, 1536(ra)
	-[0x800013d8]:sw t6, 1544(ra)
Current Store : [0x800013d8] : sw t6, 1544(ra) -- Store: [0x800085d0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcc3488366e29b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xcc3488366e29b and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013cc]:fadd.d t5, t3, s10, dyn
	-[0x800013d0]:csrrs a7, fcsr, zero
	-[0x800013d4]:sw t5, 1536(ra)
	-[0x800013d8]:sw t6, 1544(ra)
	-[0x800013dc]:sw t5, 1552(ra)
Current Store : [0x800013dc] : sw t5, 1552(ra) -- Store: [0x800085d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe3796147a7f97 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe3796147a7f97 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000141c]:fadd.d t5, t3, s10, dyn
	-[0x80001420]:csrrs a7, fcsr, zero
	-[0x80001424]:sw t5, 1568(ra)
	-[0x80001428]:sw t6, 1576(ra)
Current Store : [0x80001428] : sw t6, 1576(ra) -- Store: [0x800085f0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe3796147a7f97 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe3796147a7f97 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000141c]:fadd.d t5, t3, s10, dyn
	-[0x80001420]:csrrs a7, fcsr, zero
	-[0x80001424]:sw t5, 1568(ra)
	-[0x80001428]:sw t6, 1576(ra)
	-[0x8000142c]:sw t5, 1584(ra)
Current Store : [0x8000142c] : sw t5, 1584(ra) -- Store: [0x800085f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe3796147a7f97 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe3796147a7f97 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000146c]:fadd.d t5, t3, s10, dyn
	-[0x80001470]:csrrs a7, fcsr, zero
	-[0x80001474]:sw t5, 1600(ra)
	-[0x80001478]:sw t6, 1608(ra)
Current Store : [0x80001478] : sw t6, 1608(ra) -- Store: [0x80008610]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe3796147a7f97 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe3796147a7f97 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000146c]:fadd.d t5, t3, s10, dyn
	-[0x80001470]:csrrs a7, fcsr, zero
	-[0x80001474]:sw t5, 1600(ra)
	-[0x80001478]:sw t6, 1608(ra)
	-[0x8000147c]:sw t5, 1616(ra)
Current Store : [0x8000147c] : sw t5, 1616(ra) -- Store: [0x80008618]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe3796147a7f97 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe3796147a7f97 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014bc]:fadd.d t5, t3, s10, dyn
	-[0x800014c0]:csrrs a7, fcsr, zero
	-[0x800014c4]:sw t5, 1632(ra)
	-[0x800014c8]:sw t6, 1640(ra)
Current Store : [0x800014c8] : sw t6, 1640(ra) -- Store: [0x80008630]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe3796147a7f97 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe3796147a7f97 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014bc]:fadd.d t5, t3, s10, dyn
	-[0x800014c0]:csrrs a7, fcsr, zero
	-[0x800014c4]:sw t5, 1632(ra)
	-[0x800014c8]:sw t6, 1640(ra)
	-[0x800014cc]:sw t5, 1648(ra)
Current Store : [0x800014cc] : sw t5, 1648(ra) -- Store: [0x80008638]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe3796147a7f97 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe3796147a7f97 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000150c]:fadd.d t5, t3, s10, dyn
	-[0x80001510]:csrrs a7, fcsr, zero
	-[0x80001514]:sw t5, 1664(ra)
	-[0x80001518]:sw t6, 1672(ra)
Current Store : [0x80001518] : sw t6, 1672(ra) -- Store: [0x80008650]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe3796147a7f97 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe3796147a7f97 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000150c]:fadd.d t5, t3, s10, dyn
	-[0x80001510]:csrrs a7, fcsr, zero
	-[0x80001514]:sw t5, 1664(ra)
	-[0x80001518]:sw t6, 1672(ra)
	-[0x8000151c]:sw t5, 1680(ra)
Current Store : [0x8000151c] : sw t5, 1680(ra) -- Store: [0x80008658]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe3796147a7f97 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe3796147a7f97 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000155c]:fadd.d t5, t3, s10, dyn
	-[0x80001560]:csrrs a7, fcsr, zero
	-[0x80001564]:sw t5, 1696(ra)
	-[0x80001568]:sw t6, 1704(ra)
Current Store : [0x80001568] : sw t6, 1704(ra) -- Store: [0x80008670]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe3796147a7f97 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe3796147a7f97 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000155c]:fadd.d t5, t3, s10, dyn
	-[0x80001560]:csrrs a7, fcsr, zero
	-[0x80001564]:sw t5, 1696(ra)
	-[0x80001568]:sw t6, 1704(ra)
	-[0x8000156c]:sw t5, 1712(ra)
Current Store : [0x8000156c] : sw t5, 1712(ra) -- Store: [0x80008678]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xbc978aa879221 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xbc978aa879221 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015ac]:fadd.d t5, t3, s10, dyn
	-[0x800015b0]:csrrs a7, fcsr, zero
	-[0x800015b4]:sw t5, 1728(ra)
	-[0x800015b8]:sw t6, 1736(ra)
Current Store : [0x800015b8] : sw t6, 1736(ra) -- Store: [0x80008690]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xbc978aa879221 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xbc978aa879221 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015ac]:fadd.d t5, t3, s10, dyn
	-[0x800015b0]:csrrs a7, fcsr, zero
	-[0x800015b4]:sw t5, 1728(ra)
	-[0x800015b8]:sw t6, 1736(ra)
	-[0x800015bc]:sw t5, 1744(ra)
Current Store : [0x800015bc] : sw t5, 1744(ra) -- Store: [0x80008698]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xbc978aa879221 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xbc978aa879221 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015fc]:fadd.d t5, t3, s10, dyn
	-[0x80001600]:csrrs a7, fcsr, zero
	-[0x80001604]:sw t5, 1760(ra)
	-[0x80001608]:sw t6, 1768(ra)
Current Store : [0x80001608] : sw t6, 1768(ra) -- Store: [0x800086b0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xbc978aa879221 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xbc978aa879221 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015fc]:fadd.d t5, t3, s10, dyn
	-[0x80001600]:csrrs a7, fcsr, zero
	-[0x80001604]:sw t5, 1760(ra)
	-[0x80001608]:sw t6, 1768(ra)
	-[0x8000160c]:sw t5, 1776(ra)
Current Store : [0x8000160c] : sw t5, 1776(ra) -- Store: [0x800086b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xbc978aa879221 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xbc978aa879221 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000164c]:fadd.d t5, t3, s10, dyn
	-[0x80001650]:csrrs a7, fcsr, zero
	-[0x80001654]:sw t5, 1792(ra)
	-[0x80001658]:sw t6, 1800(ra)
Current Store : [0x80001658] : sw t6, 1800(ra) -- Store: [0x800086d0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xbc978aa879221 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xbc978aa879221 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000164c]:fadd.d t5, t3, s10, dyn
	-[0x80001650]:csrrs a7, fcsr, zero
	-[0x80001654]:sw t5, 1792(ra)
	-[0x80001658]:sw t6, 1800(ra)
	-[0x8000165c]:sw t5, 1808(ra)
Current Store : [0x8000165c] : sw t5, 1808(ra) -- Store: [0x800086d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xbc978aa879221 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xbc978aa879221 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000169c]:fadd.d t5, t3, s10, dyn
	-[0x800016a0]:csrrs a7, fcsr, zero
	-[0x800016a4]:sw t5, 1824(ra)
	-[0x800016a8]:sw t6, 1832(ra)
Current Store : [0x800016a8] : sw t6, 1832(ra) -- Store: [0x800086f0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xbc978aa879221 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xbc978aa879221 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000169c]:fadd.d t5, t3, s10, dyn
	-[0x800016a0]:csrrs a7, fcsr, zero
	-[0x800016a4]:sw t5, 1824(ra)
	-[0x800016a8]:sw t6, 1832(ra)
	-[0x800016ac]:sw t5, 1840(ra)
Current Store : [0x800016ac] : sw t5, 1840(ra) -- Store: [0x800086f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xbc978aa879221 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xbc978aa879221 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016ec]:fadd.d t5, t3, s10, dyn
	-[0x800016f0]:csrrs a7, fcsr, zero
	-[0x800016f4]:sw t5, 1856(ra)
	-[0x800016f8]:sw t6, 1864(ra)
Current Store : [0x800016f8] : sw t6, 1864(ra) -- Store: [0x80008710]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xbc978aa879221 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xbc978aa879221 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016ec]:fadd.d t5, t3, s10, dyn
	-[0x800016f0]:csrrs a7, fcsr, zero
	-[0x800016f4]:sw t5, 1856(ra)
	-[0x800016f8]:sw t6, 1864(ra)
	-[0x800016fc]:sw t5, 1872(ra)
Current Store : [0x800016fc] : sw t5, 1872(ra) -- Store: [0x80008718]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd3762f4d1629c and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd3762f4d1629c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000173c]:fadd.d t5, t3, s10, dyn
	-[0x80001740]:csrrs a7, fcsr, zero
	-[0x80001744]:sw t5, 1888(ra)
	-[0x80001748]:sw t6, 1896(ra)
Current Store : [0x80001748] : sw t6, 1896(ra) -- Store: [0x80008730]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd3762f4d1629c and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd3762f4d1629c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000173c]:fadd.d t5, t3, s10, dyn
	-[0x80001740]:csrrs a7, fcsr, zero
	-[0x80001744]:sw t5, 1888(ra)
	-[0x80001748]:sw t6, 1896(ra)
	-[0x8000174c]:sw t5, 1904(ra)
Current Store : [0x8000174c] : sw t5, 1904(ra) -- Store: [0x80008738]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd3762f4d1629c and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd3762f4d1629c and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000178c]:fadd.d t5, t3, s10, dyn
	-[0x80001790]:csrrs a7, fcsr, zero
	-[0x80001794]:sw t5, 1920(ra)
	-[0x80001798]:sw t6, 1928(ra)
Current Store : [0x80001798] : sw t6, 1928(ra) -- Store: [0x80008750]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd3762f4d1629c and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd3762f4d1629c and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000178c]:fadd.d t5, t3, s10, dyn
	-[0x80001790]:csrrs a7, fcsr, zero
	-[0x80001794]:sw t5, 1920(ra)
	-[0x80001798]:sw t6, 1928(ra)
	-[0x8000179c]:sw t5, 1936(ra)
Current Store : [0x8000179c] : sw t5, 1936(ra) -- Store: [0x80008758]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd3762f4d1629c and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd3762f4d1629c and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017dc]:fadd.d t5, t3, s10, dyn
	-[0x800017e0]:csrrs a7, fcsr, zero
	-[0x800017e4]:sw t5, 1952(ra)
	-[0x800017e8]:sw t6, 1960(ra)
Current Store : [0x800017e8] : sw t6, 1960(ra) -- Store: [0x80008770]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd3762f4d1629c and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd3762f4d1629c and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017dc]:fadd.d t5, t3, s10, dyn
	-[0x800017e0]:csrrs a7, fcsr, zero
	-[0x800017e4]:sw t5, 1952(ra)
	-[0x800017e8]:sw t6, 1960(ra)
	-[0x800017ec]:sw t5, 1968(ra)
Current Store : [0x800017ec] : sw t5, 1968(ra) -- Store: [0x80008778]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd3762f4d1629c and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd3762f4d1629c and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000182c]:fadd.d t5, t3, s10, dyn
	-[0x80001830]:csrrs a7, fcsr, zero
	-[0x80001834]:sw t5, 1984(ra)
	-[0x80001838]:sw t6, 1992(ra)
Current Store : [0x80001838] : sw t6, 1992(ra) -- Store: [0x80008790]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd3762f4d1629c and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd3762f4d1629c and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000182c]:fadd.d t5, t3, s10, dyn
	-[0x80001830]:csrrs a7, fcsr, zero
	-[0x80001834]:sw t5, 1984(ra)
	-[0x80001838]:sw t6, 1992(ra)
	-[0x8000183c]:sw t5, 2000(ra)
Current Store : [0x8000183c] : sw t5, 2000(ra) -- Store: [0x80008798]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd3762f4d1629c and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd3762f4d1629c and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000187c]:fadd.d t5, t3, s10, dyn
	-[0x80001880]:csrrs a7, fcsr, zero
	-[0x80001884]:sw t5, 2016(ra)
	-[0x80001888]:sw t6, 2024(ra)
Current Store : [0x80001888] : sw t6, 2024(ra) -- Store: [0x800087b0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd3762f4d1629c and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd3762f4d1629c and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000187c]:fadd.d t5, t3, s10, dyn
	-[0x80001880]:csrrs a7, fcsr, zero
	-[0x80001884]:sw t5, 2016(ra)
	-[0x80001888]:sw t6, 2024(ra)
	-[0x8000188c]:sw t5, 2032(ra)
Current Store : [0x8000188c] : sw t5, 2032(ra) -- Store: [0x800087b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe8754038aa2cf and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe8754038aa2cf and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018cc]:fadd.d t5, t3, s10, dyn
	-[0x800018d0]:csrrs a7, fcsr, zero
	-[0x800018d4]:addi ra, ra, 2040
	-[0x800018d8]:sw t5, 8(ra)
	-[0x800018dc]:sw t6, 16(ra)
Current Store : [0x800018dc] : sw t6, 16(ra) -- Store: [0x800087d0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe8754038aa2cf and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe8754038aa2cf and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018cc]:fadd.d t5, t3, s10, dyn
	-[0x800018d0]:csrrs a7, fcsr, zero
	-[0x800018d4]:addi ra, ra, 2040
	-[0x800018d8]:sw t5, 8(ra)
	-[0x800018dc]:sw t6, 16(ra)
	-[0x800018e0]:sw t5, 24(ra)
Current Store : [0x800018e0] : sw t5, 24(ra) -- Store: [0x800087d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe8754038aa2cf and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe8754038aa2cf and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001920]:fadd.d t5, t3, s10, dyn
	-[0x80001924]:csrrs a7, fcsr, zero
	-[0x80001928]:sw t5, 40(ra)
	-[0x8000192c]:sw t6, 48(ra)
Current Store : [0x8000192c] : sw t6, 48(ra) -- Store: [0x800087f0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe8754038aa2cf and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe8754038aa2cf and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001920]:fadd.d t5, t3, s10, dyn
	-[0x80001924]:csrrs a7, fcsr, zero
	-[0x80001928]:sw t5, 40(ra)
	-[0x8000192c]:sw t6, 48(ra)
	-[0x80001930]:sw t5, 56(ra)
Current Store : [0x80001930] : sw t5, 56(ra) -- Store: [0x800087f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe8754038aa2cf and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe8754038aa2cf and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001970]:fadd.d t5, t3, s10, dyn
	-[0x80001974]:csrrs a7, fcsr, zero
	-[0x80001978]:sw t5, 72(ra)
	-[0x8000197c]:sw t6, 80(ra)
Current Store : [0x8000197c] : sw t6, 80(ra) -- Store: [0x80008810]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe8754038aa2cf and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe8754038aa2cf and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001970]:fadd.d t5, t3, s10, dyn
	-[0x80001974]:csrrs a7, fcsr, zero
	-[0x80001978]:sw t5, 72(ra)
	-[0x8000197c]:sw t6, 80(ra)
	-[0x80001980]:sw t5, 88(ra)
Current Store : [0x80001980] : sw t5, 88(ra) -- Store: [0x80008818]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe8754038aa2cf and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe8754038aa2cf and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800019c0]:fadd.d t5, t3, s10, dyn
	-[0x800019c4]:csrrs a7, fcsr, zero
	-[0x800019c8]:sw t5, 104(ra)
	-[0x800019cc]:sw t6, 112(ra)
Current Store : [0x800019cc] : sw t6, 112(ra) -- Store: [0x80008830]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe8754038aa2cf and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe8754038aa2cf and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800019c0]:fadd.d t5, t3, s10, dyn
	-[0x800019c4]:csrrs a7, fcsr, zero
	-[0x800019c8]:sw t5, 104(ra)
	-[0x800019cc]:sw t6, 112(ra)
	-[0x800019d0]:sw t5, 120(ra)
Current Store : [0x800019d0] : sw t5, 120(ra) -- Store: [0x80008838]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe8754038aa2cf and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe8754038aa2cf and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a10]:fadd.d t5, t3, s10, dyn
	-[0x80001a14]:csrrs a7, fcsr, zero
	-[0x80001a18]:sw t5, 136(ra)
	-[0x80001a1c]:sw t6, 144(ra)
Current Store : [0x80001a1c] : sw t6, 144(ra) -- Store: [0x80008850]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe8754038aa2cf and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe8754038aa2cf and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a10]:fadd.d t5, t3, s10, dyn
	-[0x80001a14]:csrrs a7, fcsr, zero
	-[0x80001a18]:sw t5, 136(ra)
	-[0x80001a1c]:sw t6, 144(ra)
	-[0x80001a20]:sw t5, 152(ra)
Current Store : [0x80001a20] : sw t5, 152(ra) -- Store: [0x80008858]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x0abe7f07f8c6f and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x0abe7f07f8c6f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a60]:fadd.d t5, t3, s10, dyn
	-[0x80001a64]:csrrs a7, fcsr, zero
	-[0x80001a68]:sw t5, 168(ra)
	-[0x80001a6c]:sw t6, 176(ra)
Current Store : [0x80001a6c] : sw t6, 176(ra) -- Store: [0x80008870]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x0abe7f07f8c6f and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x0abe7f07f8c6f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a60]:fadd.d t5, t3, s10, dyn
	-[0x80001a64]:csrrs a7, fcsr, zero
	-[0x80001a68]:sw t5, 168(ra)
	-[0x80001a6c]:sw t6, 176(ra)
	-[0x80001a70]:sw t5, 184(ra)
Current Store : [0x80001a70] : sw t5, 184(ra) -- Store: [0x80008878]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x0abe7f07f8c6f and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x0abe7f07f8c6f and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ab0]:fadd.d t5, t3, s10, dyn
	-[0x80001ab4]:csrrs a7, fcsr, zero
	-[0x80001ab8]:sw t5, 200(ra)
	-[0x80001abc]:sw t6, 208(ra)
Current Store : [0x80001abc] : sw t6, 208(ra) -- Store: [0x80008890]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x0abe7f07f8c6f and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x0abe7f07f8c6f and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ab0]:fadd.d t5, t3, s10, dyn
	-[0x80001ab4]:csrrs a7, fcsr, zero
	-[0x80001ab8]:sw t5, 200(ra)
	-[0x80001abc]:sw t6, 208(ra)
	-[0x80001ac0]:sw t5, 216(ra)
Current Store : [0x80001ac0] : sw t5, 216(ra) -- Store: [0x80008898]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x0abe7f07f8c6f and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x0abe7f07f8c6f and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b00]:fadd.d t5, t3, s10, dyn
	-[0x80001b04]:csrrs a7, fcsr, zero
	-[0x80001b08]:sw t5, 232(ra)
	-[0x80001b0c]:sw t6, 240(ra)
Current Store : [0x80001b0c] : sw t6, 240(ra) -- Store: [0x800088b0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x0abe7f07f8c6f and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x0abe7f07f8c6f and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b00]:fadd.d t5, t3, s10, dyn
	-[0x80001b04]:csrrs a7, fcsr, zero
	-[0x80001b08]:sw t5, 232(ra)
	-[0x80001b0c]:sw t6, 240(ra)
	-[0x80001b10]:sw t5, 248(ra)
Current Store : [0x80001b10] : sw t5, 248(ra) -- Store: [0x800088b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x0abe7f07f8c6f and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x0abe7f07f8c6f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b50]:fadd.d t5, t3, s10, dyn
	-[0x80001b54]:csrrs a7, fcsr, zero
	-[0x80001b58]:sw t5, 264(ra)
	-[0x80001b5c]:sw t6, 272(ra)
Current Store : [0x80001b5c] : sw t6, 272(ra) -- Store: [0x800088d0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x0abe7f07f8c6f and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x0abe7f07f8c6f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b50]:fadd.d t5, t3, s10, dyn
	-[0x80001b54]:csrrs a7, fcsr, zero
	-[0x80001b58]:sw t5, 264(ra)
	-[0x80001b5c]:sw t6, 272(ra)
	-[0x80001b60]:sw t5, 280(ra)
Current Store : [0x80001b60] : sw t5, 280(ra) -- Store: [0x800088d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x0abe7f07f8c6f and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x0abe7f07f8c6f and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ba0]:fadd.d t5, t3, s10, dyn
	-[0x80001ba4]:csrrs a7, fcsr, zero
	-[0x80001ba8]:sw t5, 296(ra)
	-[0x80001bac]:sw t6, 304(ra)
Current Store : [0x80001bac] : sw t6, 304(ra) -- Store: [0x800088f0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x0abe7f07f8c6f and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x0abe7f07f8c6f and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ba0]:fadd.d t5, t3, s10, dyn
	-[0x80001ba4]:csrrs a7, fcsr, zero
	-[0x80001ba8]:sw t5, 296(ra)
	-[0x80001bac]:sw t6, 304(ra)
	-[0x80001bb0]:sw t5, 312(ra)
Current Store : [0x80001bb0] : sw t5, 312(ra) -- Store: [0x800088f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x18ef1d7a9fa74 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x18ef1d7a9fa74 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001bf0]:fadd.d t5, t3, s10, dyn
	-[0x80001bf4]:csrrs a7, fcsr, zero
	-[0x80001bf8]:sw t5, 328(ra)
	-[0x80001bfc]:sw t6, 336(ra)
Current Store : [0x80001bfc] : sw t6, 336(ra) -- Store: [0x80008910]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x18ef1d7a9fa74 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x18ef1d7a9fa74 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001bf0]:fadd.d t5, t3, s10, dyn
	-[0x80001bf4]:csrrs a7, fcsr, zero
	-[0x80001bf8]:sw t5, 328(ra)
	-[0x80001bfc]:sw t6, 336(ra)
	-[0x80001c00]:sw t5, 344(ra)
Current Store : [0x80001c00] : sw t5, 344(ra) -- Store: [0x80008918]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x18ef1d7a9fa74 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x18ef1d7a9fa74 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c40]:fadd.d t5, t3, s10, dyn
	-[0x80001c44]:csrrs a7, fcsr, zero
	-[0x80001c48]:sw t5, 360(ra)
	-[0x80001c4c]:sw t6, 368(ra)
Current Store : [0x80001c4c] : sw t6, 368(ra) -- Store: [0x80008930]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x18ef1d7a9fa74 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x18ef1d7a9fa74 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c40]:fadd.d t5, t3, s10, dyn
	-[0x80001c44]:csrrs a7, fcsr, zero
	-[0x80001c48]:sw t5, 360(ra)
	-[0x80001c4c]:sw t6, 368(ra)
	-[0x80001c50]:sw t5, 376(ra)
Current Store : [0x80001c50] : sw t5, 376(ra) -- Store: [0x80008938]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x18ef1d7a9fa74 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x18ef1d7a9fa74 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c90]:fadd.d t5, t3, s10, dyn
	-[0x80001c94]:csrrs a7, fcsr, zero
	-[0x80001c98]:sw t5, 392(ra)
	-[0x80001c9c]:sw t6, 400(ra)
Current Store : [0x80001c9c] : sw t6, 400(ra) -- Store: [0x80008950]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x18ef1d7a9fa74 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x18ef1d7a9fa74 and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c90]:fadd.d t5, t3, s10, dyn
	-[0x80001c94]:csrrs a7, fcsr, zero
	-[0x80001c98]:sw t5, 392(ra)
	-[0x80001c9c]:sw t6, 400(ra)
	-[0x80001ca0]:sw t5, 408(ra)
Current Store : [0x80001ca0] : sw t5, 408(ra) -- Store: [0x80008958]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x18ef1d7a9fa74 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x18ef1d7a9fa74 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ce0]:fadd.d t5, t3, s10, dyn
	-[0x80001ce4]:csrrs a7, fcsr, zero
	-[0x80001ce8]:sw t5, 424(ra)
	-[0x80001cec]:sw t6, 432(ra)
Current Store : [0x80001cec] : sw t6, 432(ra) -- Store: [0x80008970]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x18ef1d7a9fa74 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x18ef1d7a9fa74 and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ce0]:fadd.d t5, t3, s10, dyn
	-[0x80001ce4]:csrrs a7, fcsr, zero
	-[0x80001ce8]:sw t5, 424(ra)
	-[0x80001cec]:sw t6, 432(ra)
	-[0x80001cf0]:sw t5, 440(ra)
Current Store : [0x80001cf0] : sw t5, 440(ra) -- Store: [0x80008978]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x18ef1d7a9fa74 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x18ef1d7a9fa74 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d30]:fadd.d t5, t3, s10, dyn
	-[0x80001d34]:csrrs a7, fcsr, zero
	-[0x80001d38]:sw t5, 456(ra)
	-[0x80001d3c]:sw t6, 464(ra)
Current Store : [0x80001d3c] : sw t6, 464(ra) -- Store: [0x80008990]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x18ef1d7a9fa74 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x18ef1d7a9fa74 and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d30]:fadd.d t5, t3, s10, dyn
	-[0x80001d34]:csrrs a7, fcsr, zero
	-[0x80001d38]:sw t5, 456(ra)
	-[0x80001d3c]:sw t6, 464(ra)
	-[0x80001d40]:sw t5, 472(ra)
Current Store : [0x80001d40] : sw t5, 472(ra) -- Store: [0x80008998]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x707d21f5c40de and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x707d21f5c40de and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d80]:fadd.d t5, t3, s10, dyn
	-[0x80001d84]:csrrs a7, fcsr, zero
	-[0x80001d88]:sw t5, 488(ra)
	-[0x80001d8c]:sw t6, 496(ra)
Current Store : [0x80001d8c] : sw t6, 496(ra) -- Store: [0x800089b0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x707d21f5c40de and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x707d21f5c40de and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d80]:fadd.d t5, t3, s10, dyn
	-[0x80001d84]:csrrs a7, fcsr, zero
	-[0x80001d88]:sw t5, 488(ra)
	-[0x80001d8c]:sw t6, 496(ra)
	-[0x80001d90]:sw t5, 504(ra)
Current Store : [0x80001d90] : sw t5, 504(ra) -- Store: [0x800089b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x707d21f5c40de and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x707d21f5c40de and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001dd0]:fadd.d t5, t3, s10, dyn
	-[0x80001dd4]:csrrs a7, fcsr, zero
	-[0x80001dd8]:sw t5, 520(ra)
	-[0x80001ddc]:sw t6, 528(ra)
Current Store : [0x80001ddc] : sw t6, 528(ra) -- Store: [0x800089d0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x707d21f5c40de and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x707d21f5c40de and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001dd0]:fadd.d t5, t3, s10, dyn
	-[0x80001dd4]:csrrs a7, fcsr, zero
	-[0x80001dd8]:sw t5, 520(ra)
	-[0x80001ddc]:sw t6, 528(ra)
	-[0x80001de0]:sw t5, 536(ra)
Current Store : [0x80001de0] : sw t5, 536(ra) -- Store: [0x800089d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x707d21f5c40de and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x707d21f5c40de and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e20]:fadd.d t5, t3, s10, dyn
	-[0x80001e24]:csrrs a7, fcsr, zero
	-[0x80001e28]:sw t5, 552(ra)
	-[0x80001e2c]:sw t6, 560(ra)
Current Store : [0x80001e2c] : sw t6, 560(ra) -- Store: [0x800089f0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x707d21f5c40de and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x707d21f5c40de and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e20]:fadd.d t5, t3, s10, dyn
	-[0x80001e24]:csrrs a7, fcsr, zero
	-[0x80001e28]:sw t5, 552(ra)
	-[0x80001e2c]:sw t6, 560(ra)
	-[0x80001e30]:sw t5, 568(ra)
Current Store : [0x80001e30] : sw t5, 568(ra) -- Store: [0x800089f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x707d21f5c40de and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x707d21f5c40de and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e70]:fadd.d t5, t3, s10, dyn
	-[0x80001e74]:csrrs a7, fcsr, zero
	-[0x80001e78]:sw t5, 584(ra)
	-[0x80001e7c]:sw t6, 592(ra)
Current Store : [0x80001e7c] : sw t6, 592(ra) -- Store: [0x80008a10]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x707d21f5c40de and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x707d21f5c40de and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e70]:fadd.d t5, t3, s10, dyn
	-[0x80001e74]:csrrs a7, fcsr, zero
	-[0x80001e78]:sw t5, 584(ra)
	-[0x80001e7c]:sw t6, 592(ra)
	-[0x80001e80]:sw t5, 600(ra)
Current Store : [0x80001e80] : sw t5, 600(ra) -- Store: [0x80008a18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x707d21f5c40de and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x707d21f5c40de and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ec0]:fadd.d t5, t3, s10, dyn
	-[0x80001ec4]:csrrs a7, fcsr, zero
	-[0x80001ec8]:sw t5, 616(ra)
	-[0x80001ecc]:sw t6, 624(ra)
Current Store : [0x80001ecc] : sw t6, 624(ra) -- Store: [0x80008a30]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0x707d21f5c40de and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x707d21f5c40de and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ec0]:fadd.d t5, t3, s10, dyn
	-[0x80001ec4]:csrrs a7, fcsr, zero
	-[0x80001ec8]:sw t5, 616(ra)
	-[0x80001ecc]:sw t6, 624(ra)
	-[0x80001ed0]:sw t5, 632(ra)
Current Store : [0x80001ed0] : sw t5, 632(ra) -- Store: [0x80008a38]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa65214b23e38e and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xa65214b23e38e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f10]:fadd.d t5, t3, s10, dyn
	-[0x80001f14]:csrrs a7, fcsr, zero
	-[0x80001f18]:sw t5, 648(ra)
	-[0x80001f1c]:sw t6, 656(ra)
Current Store : [0x80001f1c] : sw t6, 656(ra) -- Store: [0x80008a50]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa65214b23e38e and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xa65214b23e38e and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f10]:fadd.d t5, t3, s10, dyn
	-[0x80001f14]:csrrs a7, fcsr, zero
	-[0x80001f18]:sw t5, 648(ra)
	-[0x80001f1c]:sw t6, 656(ra)
	-[0x80001f20]:sw t5, 664(ra)
Current Store : [0x80001f20] : sw t5, 664(ra) -- Store: [0x80008a58]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa65214b23e38e and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xa65214b23e38e and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f60]:fadd.d t5, t3, s10, dyn
	-[0x80001f64]:csrrs a7, fcsr, zero
	-[0x80001f68]:sw t5, 680(ra)
	-[0x80001f6c]:sw t6, 688(ra)
Current Store : [0x80001f6c] : sw t6, 688(ra) -- Store: [0x80008a70]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa65214b23e38e and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xa65214b23e38e and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f60]:fadd.d t5, t3, s10, dyn
	-[0x80001f64]:csrrs a7, fcsr, zero
	-[0x80001f68]:sw t5, 680(ra)
	-[0x80001f6c]:sw t6, 688(ra)
	-[0x80001f70]:sw t5, 696(ra)
Current Store : [0x80001f70] : sw t5, 696(ra) -- Store: [0x80008a78]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa65214b23e38e and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xa65214b23e38e and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001fb0]:fadd.d t5, t3, s10, dyn
	-[0x80001fb4]:csrrs a7, fcsr, zero
	-[0x80001fb8]:sw t5, 712(ra)
	-[0x80001fbc]:sw t6, 720(ra)
Current Store : [0x80001fbc] : sw t6, 720(ra) -- Store: [0x80008a90]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa65214b23e38e and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xa65214b23e38e and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001fb0]:fadd.d t5, t3, s10, dyn
	-[0x80001fb4]:csrrs a7, fcsr, zero
	-[0x80001fb8]:sw t5, 712(ra)
	-[0x80001fbc]:sw t6, 720(ra)
	-[0x80001fc0]:sw t5, 728(ra)
Current Store : [0x80001fc0] : sw t5, 728(ra) -- Store: [0x80008a98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa65214b23e38e and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xa65214b23e38e and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002000]:fadd.d t5, t3, s10, dyn
	-[0x80002004]:csrrs a7, fcsr, zero
	-[0x80002008]:sw t5, 744(ra)
	-[0x8000200c]:sw t6, 752(ra)
Current Store : [0x8000200c] : sw t6, 752(ra) -- Store: [0x80008ab0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa65214b23e38e and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xa65214b23e38e and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002000]:fadd.d t5, t3, s10, dyn
	-[0x80002004]:csrrs a7, fcsr, zero
	-[0x80002008]:sw t5, 744(ra)
	-[0x8000200c]:sw t6, 752(ra)
	-[0x80002010]:sw t5, 760(ra)
Current Store : [0x80002010] : sw t5, 760(ra) -- Store: [0x80008ab8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa65214b23e38e and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xa65214b23e38e and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002050]:fadd.d t5, t3, s10, dyn
	-[0x80002054]:csrrs a7, fcsr, zero
	-[0x80002058]:sw t5, 776(ra)
	-[0x8000205c]:sw t6, 784(ra)
Current Store : [0x8000205c] : sw t6, 784(ra) -- Store: [0x80008ad0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa65214b23e38e and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xa65214b23e38e and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002050]:fadd.d t5, t3, s10, dyn
	-[0x80002054]:csrrs a7, fcsr, zero
	-[0x80002058]:sw t5, 776(ra)
	-[0x8000205c]:sw t6, 784(ra)
	-[0x80002060]:sw t5, 792(ra)
Current Store : [0x80002060] : sw t5, 792(ra) -- Store: [0x80008ad8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f5 and fm1 == 0x2b954e52a4bff and fs2 == 1 and fe2 == 0x7f5 and fm2 == 0x2b954e52a4bff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800020a0]:fadd.d t5, t3, s10, dyn
	-[0x800020a4]:csrrs a7, fcsr, zero
	-[0x800020a8]:sw t5, 808(ra)
	-[0x800020ac]:sw t6, 816(ra)
Current Store : [0x800020ac] : sw t6, 816(ra) -- Store: [0x80008af0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f5 and fm1 == 0x2b954e52a4bff and fs2 == 1 and fe2 == 0x7f5 and fm2 == 0x2b954e52a4bff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800020a0]:fadd.d t5, t3, s10, dyn
	-[0x800020a4]:csrrs a7, fcsr, zero
	-[0x800020a8]:sw t5, 808(ra)
	-[0x800020ac]:sw t6, 816(ra)
	-[0x800020b0]:sw t5, 824(ra)
Current Store : [0x800020b0] : sw t5, 824(ra) -- Store: [0x80008af8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f5 and fm1 == 0x2b954e52a4bff and fs2 == 1 and fe2 == 0x7f5 and fm2 == 0x2b954e52a4bff and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800020f0]:fadd.d t5, t3, s10, dyn
	-[0x800020f4]:csrrs a7, fcsr, zero
	-[0x800020f8]:sw t5, 840(ra)
	-[0x800020fc]:sw t6, 848(ra)
Current Store : [0x800020fc] : sw t6, 848(ra) -- Store: [0x80008b10]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f5 and fm1 == 0x2b954e52a4bff and fs2 == 1 and fe2 == 0x7f5 and fm2 == 0x2b954e52a4bff and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800020f0]:fadd.d t5, t3, s10, dyn
	-[0x800020f4]:csrrs a7, fcsr, zero
	-[0x800020f8]:sw t5, 840(ra)
	-[0x800020fc]:sw t6, 848(ra)
	-[0x80002100]:sw t5, 856(ra)
Current Store : [0x80002100] : sw t5, 856(ra) -- Store: [0x80008b18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f5 and fm1 == 0x2b954e52a4bff and fs2 == 1 and fe2 == 0x7f5 and fm2 == 0x2b954e52a4bff and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002140]:fadd.d t5, t3, s10, dyn
	-[0x80002144]:csrrs a7, fcsr, zero
	-[0x80002148]:sw t5, 872(ra)
	-[0x8000214c]:sw t6, 880(ra)
Current Store : [0x8000214c] : sw t6, 880(ra) -- Store: [0x80008b30]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f5 and fm1 == 0x2b954e52a4bff and fs2 == 1 and fe2 == 0x7f5 and fm2 == 0x2b954e52a4bff and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002140]:fadd.d t5, t3, s10, dyn
	-[0x80002144]:csrrs a7, fcsr, zero
	-[0x80002148]:sw t5, 872(ra)
	-[0x8000214c]:sw t6, 880(ra)
	-[0x80002150]:sw t5, 888(ra)
Current Store : [0x80002150] : sw t5, 888(ra) -- Store: [0x80008b38]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f5 and fm1 == 0x2b954e52a4bff and fs2 == 1 and fe2 == 0x7f5 and fm2 == 0x2b954e52a4bff and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002190]:fadd.d t5, t3, s10, dyn
	-[0x80002194]:csrrs a7, fcsr, zero
	-[0x80002198]:sw t5, 904(ra)
	-[0x8000219c]:sw t6, 912(ra)
Current Store : [0x8000219c] : sw t6, 912(ra) -- Store: [0x80008b50]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f5 and fm1 == 0x2b954e52a4bff and fs2 == 1 and fe2 == 0x7f5 and fm2 == 0x2b954e52a4bff and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002190]:fadd.d t5, t3, s10, dyn
	-[0x80002194]:csrrs a7, fcsr, zero
	-[0x80002198]:sw t5, 904(ra)
	-[0x8000219c]:sw t6, 912(ra)
	-[0x800021a0]:sw t5, 920(ra)
Current Store : [0x800021a0] : sw t5, 920(ra) -- Store: [0x80008b58]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f5 and fm1 == 0x2b954e52a4bff and fs2 == 1 and fe2 == 0x7f5 and fm2 == 0x2b954e52a4bff and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800021e0]:fadd.d t5, t3, s10, dyn
	-[0x800021e4]:csrrs a7, fcsr, zero
	-[0x800021e8]:sw t5, 936(ra)
	-[0x800021ec]:sw t6, 944(ra)
Current Store : [0x800021ec] : sw t6, 944(ra) -- Store: [0x80008b70]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f5 and fm1 == 0x2b954e52a4bff and fs2 == 1 and fe2 == 0x7f5 and fm2 == 0x2b954e52a4bff and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800021e0]:fadd.d t5, t3, s10, dyn
	-[0x800021e4]:csrrs a7, fcsr, zero
	-[0x800021e8]:sw t5, 936(ra)
	-[0x800021ec]:sw t6, 944(ra)
	-[0x800021f0]:sw t5, 952(ra)
Current Store : [0x800021f0] : sw t5, 952(ra) -- Store: [0x80008b78]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbc366e555215f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xbc366e555215f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002230]:fadd.d t5, t3, s10, dyn
	-[0x80002234]:csrrs a7, fcsr, zero
	-[0x80002238]:sw t5, 968(ra)
	-[0x8000223c]:sw t6, 976(ra)
Current Store : [0x8000223c] : sw t6, 976(ra) -- Store: [0x80008b90]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbc366e555215f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xbc366e555215f and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002230]:fadd.d t5, t3, s10, dyn
	-[0x80002234]:csrrs a7, fcsr, zero
	-[0x80002238]:sw t5, 968(ra)
	-[0x8000223c]:sw t6, 976(ra)
	-[0x80002240]:sw t5, 984(ra)
Current Store : [0x80002240] : sw t5, 984(ra) -- Store: [0x80008b98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbc366e555215f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xbc366e555215f and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002280]:fadd.d t5, t3, s10, dyn
	-[0x80002284]:csrrs a7, fcsr, zero
	-[0x80002288]:sw t5, 1000(ra)
	-[0x8000228c]:sw t6, 1008(ra)
Current Store : [0x8000228c] : sw t6, 1008(ra) -- Store: [0x80008bb0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbc366e555215f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xbc366e555215f and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002280]:fadd.d t5, t3, s10, dyn
	-[0x80002284]:csrrs a7, fcsr, zero
	-[0x80002288]:sw t5, 1000(ra)
	-[0x8000228c]:sw t6, 1008(ra)
	-[0x80002290]:sw t5, 1016(ra)
Current Store : [0x80002290] : sw t5, 1016(ra) -- Store: [0x80008bb8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbc366e555215f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xbc366e555215f and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800022d0]:fadd.d t5, t3, s10, dyn
	-[0x800022d4]:csrrs a7, fcsr, zero
	-[0x800022d8]:sw t5, 1032(ra)
	-[0x800022dc]:sw t6, 1040(ra)
Current Store : [0x800022dc] : sw t6, 1040(ra) -- Store: [0x80008bd0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbc366e555215f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xbc366e555215f and  fcsr == 0x40 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800022d0]:fadd.d t5, t3, s10, dyn
	-[0x800022d4]:csrrs a7, fcsr, zero
	-[0x800022d8]:sw t5, 1032(ra)
	-[0x800022dc]:sw t6, 1040(ra)
	-[0x800022e0]:sw t5, 1048(ra)
Current Store : [0x800022e0] : sw t5, 1048(ra) -- Store: [0x80008bd8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbc366e555215f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xbc366e555215f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002320]:fadd.d t5, t3, s10, dyn
	-[0x80002324]:csrrs a7, fcsr, zero
	-[0x80002328]:sw t5, 1064(ra)
	-[0x8000232c]:sw t6, 1072(ra)
Current Store : [0x8000232c] : sw t6, 1072(ra) -- Store: [0x80008bf0]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbc366e555215f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xbc366e555215f and  fcsr == 0x60 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002320]:fadd.d t5, t3, s10, dyn
	-[0x80002324]:csrrs a7, fcsr, zero
	-[0x80002328]:sw t5, 1064(ra)
	-[0x8000232c]:sw t6, 1072(ra)
	-[0x80002330]:sw t5, 1080(ra)
Current Store : [0x80002330] : sw t5, 1080(ra) -- Store: [0x80008bf8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbc366e555215f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xbc366e555215f and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002370]:fadd.d t5, t3, s10, dyn
	-[0x80002374]:csrrs a7, fcsr, zero
	-[0x80002378]:sw t5, 1096(ra)
	-[0x8000237c]:sw t6, 1104(ra)
Current Store : [0x8000237c] : sw t6, 1104(ra) -- Store: [0x80008c10]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbc366e555215f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xbc366e555215f and  fcsr == 0x80 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002370]:fadd.d t5, t3, s10, dyn
	-[0x80002374]:csrrs a7, fcsr, zero
	-[0x80002378]:sw t5, 1096(ra)
	-[0x8000237c]:sw t6, 1104(ra)
	-[0x80002380]:sw t5, 1112(ra)
Current Store : [0x80002380] : sw t5, 1112(ra) -- Store: [0x80008c18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x4d025f5a10f55 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x4d025f5a10f55 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800023c0]:fadd.d t5, t3, s10, dyn
	-[0x800023c4]:csrrs a7, fcsr, zero
	-[0x800023c8]:sw t5, 1128(ra)
	-[0x800023cc]:sw t6, 1136(ra)
Current Store : [0x800023cc] : sw t6, 1136(ra) -- Store: [0x80008c30]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x4d025f5a10f55 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x4d025f5a10f55 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800023c0]:fadd.d t5, t3, s10, dyn
	-[0x800023c4]:csrrs a7, fcsr, zero
	-[0x800023c8]:sw t5, 1128(ra)
	-[0x800023cc]:sw t6, 1136(ra)
	-[0x800023d0]:sw t5, 1144(ra)
Current Store : [0x800023d0] : sw t5, 1144(ra) -- Store: [0x80008c38]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x4d025f5a10f55 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x4d025f5a10f55 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002410]:fadd.d t5, t3, s10, dyn
	-[0x80002414]:csrrs a7, fcsr, zero
	-[0x80002418]:sw t5, 1160(ra)
	-[0x8000241c]:sw t6, 1168(ra)
Current Store : [0x8000241c] : sw t6, 1168(ra) -- Store: [0x80008c50]:0x7FE914E0




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fd and fm1 == 0x4d025f5a10f55 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x4d025f5a10f55 and  fcsr == 0x20 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002410]:fadd.d t5, t3, s10, dyn
	-[0x80002414]:csrrs a7, fcsr, zero
	-[0x80002418]:sw t5, 1160(ra)
	-[0x8000241c]:sw t6, 1168(ra)
	-[0x80002420]:sw t5, 1176(ra)
Current Store : [0x80002420] : sw t5, 1176(ra) -- Store: [0x80008c58]:0x00000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                          signature                           |                                                                                                                        coverpoints                                                                                                                         |                                                                                                                    code                                                                                                                     |
|---:|--------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80007f18]<br>0x00000000<br> [0x80007f30]<br>0x00000005<br> |- mnemonic : fadd.d<br> - rs1 : x30<br> - rs2 : x30<br> - rd : x30<br> - rs1 == rs2 == rd<br>                                                                                                                                                               |[0x8000014c]:fadd.d t5, t5, t5, dyn<br> [0x80000150]:csrrs tp, fcsr, zero<br> [0x80000154]:sw t5, 0(ra)<br> [0x80000158]:sw t6, 8(ra)<br> [0x8000015c]:sw t5, 16(ra)<br> [0x80000160]:sw tp, 24(ra)<br>                                      |
|   2|[0x80007f38]<br>0xFFFFFFFF<br> [0x80007f50]<br>0x00000025<br> |- rs1 : x26<br> - rs2 : x26<br> - rd : x28<br> - rs1 == rs2 != rd<br>                                                                                                                                                                                       |[0x8000019c]:fadd.d t3, s10, s10, dyn<br> [0x800001a0]:csrrs tp, fcsr, zero<br> [0x800001a4]:sw t3, 32(ra)<br> [0x800001a8]:sw t4, 40(ra)<br> [0x800001ac]:sw t3, 48(ra)<br> [0x800001b0]:sw tp, 56(ra)<br>                                  |
|   3|[0x80007f58]<br>0x00000000<br> [0x80007f70]<br>0x00000040<br> |- rs1 : x28<br> - rs2 : x24<br> - rd : x26<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb0580f98a7dbd and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xb0580f98a7dbd and  fcsr == 0x40 and rm_val == 7   #nosat<br> |[0x800001ec]:fadd.d s10, t3, s8, dyn<br> [0x800001f0]:csrrs tp, fcsr, zero<br> [0x800001f4]:sw s10, 64(ra)<br> [0x800001f8]:sw s11, 72(ra)<br> [0x800001fc]:sw s10, 80(ra)<br> [0x80000200]:sw tp, 88(ra)<br>                                |
|   4|[0x80007f78]<br>0x00000000<br> [0x80007f90]<br>0x00000060<br> |- rs1 : x24<br> - rs2 : x28<br> - rd : x24<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb0580f98a7dbd and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xb0580f98a7dbd and  fcsr == 0x60 and rm_val == 7   #nosat<br>                        |[0x8000023c]:fadd.d s8, s8, t3, dyn<br> [0x80000240]:csrrs tp, fcsr, zero<br> [0x80000244]:sw s8, 96(ra)<br> [0x80000248]:sw s9, 104(ra)<br> [0x8000024c]:sw s8, 112(ra)<br> [0x80000250]:sw tp, 120(ra)<br>                                 |
|   5|[0x80007f98]<br>0x00000000<br> [0x80007fb0]<br>0x00000080<br> |- rs1 : x20<br> - rs2 : x22<br> - rd : x22<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb0580f98a7dbd and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xb0580f98a7dbd and  fcsr == 0x80 and rm_val == 7   #nosat<br>                        |[0x8000028c]:fadd.d s6, s4, s6, dyn<br> [0x80000290]:csrrs tp, fcsr, zero<br> [0x80000294]:sw s6, 128(ra)<br> [0x80000298]:sw s7, 136(ra)<br> [0x8000029c]:sw s6, 144(ra)<br> [0x800002a0]:sw tp, 152(ra)<br>                                |
|   6|[0x80007fb8]<br>0x00000000<br> [0x80007fd0]<br>0x00000000<br> |- rs1 : x22<br> - rs2 : x18<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0xaeaa51052e977 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xaeaa51052e977 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x800002dc]:fadd.d s4, s6, s2, dyn<br> [0x800002e0]:csrrs tp, fcsr, zero<br> [0x800002e4]:sw s4, 160(ra)<br> [0x800002e8]:sw s5, 168(ra)<br> [0x800002ec]:sw s4, 176(ra)<br> [0x800002f0]:sw tp, 184(ra)<br>                                |
|   7|[0x80007fd8]<br>0x00000000<br> [0x80007ff0]<br>0x00000020<br> |- rs1 : x16<br> - rs2 : x20<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0xaeaa51052e977 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xaeaa51052e977 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                               |[0x8000032c]:fadd.d s2, a6, s4, dyn<br> [0x80000330]:csrrs tp, fcsr, zero<br> [0x80000334]:sw s2, 192(ra)<br> [0x80000338]:sw s3, 200(ra)<br> [0x8000033c]:sw s2, 208(ra)<br> [0x80000340]:sw tp, 216(ra)<br>                                |
|   8|[0x80007ff8]<br>0x00000000<br> [0x80008010]<br>0x00000040<br> |- rs1 : x18<br> - rs2 : x14<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0xaeaa51052e977 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xaeaa51052e977 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                               |[0x8000037c]:fadd.d a6, s2, a4, dyn<br> [0x80000380]:csrrs tp, fcsr, zero<br> [0x80000384]:sw a6, 224(ra)<br> [0x80000388]:sw a7, 232(ra)<br> [0x8000038c]:sw a6, 240(ra)<br> [0x80000390]:sw tp, 248(ra)<br>                                |
|   9|[0x80008018]<br>0x00000000<br> [0x80008030]<br>0x00000060<br> |- rs1 : x12<br> - rs2 : x16<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0xaeaa51052e977 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xaeaa51052e977 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                               |[0x800003cc]:fadd.d a4, a2, a6, dyn<br> [0x800003d0]:csrrs tp, fcsr, zero<br> [0x800003d4]:sw a4, 256(ra)<br> [0x800003d8]:sw a5, 264(ra)<br> [0x800003dc]:sw a4, 272(ra)<br> [0x800003e0]:sw tp, 280(ra)<br>                                |
|  10|[0x80008038]<br>0x00000000<br> [0x80008050]<br>0x00000080<br> |- rs1 : x14<br> - rs2 : x10<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0xaeaa51052e977 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xaeaa51052e977 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                               |[0x80000424]:fadd.d a2, a4, a0, dyn<br> [0x80000428]:csrrs a7, fcsr, zero<br> [0x8000042c]:sw a2, 288(ra)<br> [0x80000430]:sw a3, 296(ra)<br> [0x80000434]:sw a2, 304(ra)<br> [0x80000438]:sw a7, 312(ra)<br>                                |
|  11|[0x80008058]<br>0x00000000<br> [0x80008070]<br>0x00000000<br> |- rs1 : x8<br> - rs2 : x12<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x05c5ccdf19706 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x05c5ccdf19706 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                 |[0x80000474]:fadd.d a0, fp, a2, dyn<br> [0x80000478]:csrrs a7, fcsr, zero<br> [0x8000047c]:sw a0, 320(ra)<br> [0x80000480]:sw a1, 328(ra)<br> [0x80000484]:sw a0, 336(ra)<br> [0x80000488]:sw a7, 344(ra)<br>                                |
|  12|[0x80007fc8]<br>0x00000000<br> [0x80007fe0]<br>0x00000020<br> |- rs1 : x10<br> - rs2 : x6<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x05c5ccdf19706 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x05c5ccdf19706 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                 |[0x800004cc]:fadd.d fp, a0, t1, dyn<br> [0x800004d0]:csrrs a7, fcsr, zero<br> [0x800004d4]:sw fp, 0(ra)<br> [0x800004d8]:sw s1, 8(ra)<br> [0x800004dc]:sw fp, 16(ra)<br> [0x800004e0]:sw a7, 24(ra)<br>                                      |
|  13|[0x80007fe8]<br>0x00000000<br> [0x80008000]<br>0x00000040<br> |- rs1 : x4<br> - rs2 : x8<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x05c5ccdf19706 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x05c5ccdf19706 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                  |[0x8000051c]:fadd.d t1, tp, fp, dyn<br> [0x80000520]:csrrs a7, fcsr, zero<br> [0x80000524]:sw t1, 32(ra)<br> [0x80000528]:sw t2, 40(ra)<br> [0x8000052c]:sw t1, 48(ra)<br> [0x80000530]:sw a7, 56(ra)<br>                                    |
|  14|[0x80008008]<br>0x00000000<br> [0x80008020]<br>0x00000060<br> |- rs1 : x6<br> - rs2 : x2<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x05c5ccdf19706 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x05c5ccdf19706 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                  |[0x8000056c]:fadd.d tp, t1, sp, dyn<br> [0x80000570]:csrrs a7, fcsr, zero<br> [0x80000574]:sw tp, 64(ra)<br> [0x80000578]:sw t0, 72(ra)<br> [0x8000057c]:sw tp, 80(ra)<br> [0x80000580]:sw a7, 88(ra)<br>                                    |
|  15|[0x80008028]<br>0x00000000<br> [0x80008040]<br>0x00000080<br> |- rs1 : x2<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x05c5ccdf19706 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x05c5ccdf19706 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                               |[0x800005bc]:fadd.d t5, sp, t3, dyn<br> [0x800005c0]:csrrs a7, fcsr, zero<br> [0x800005c4]:sw t5, 96(ra)<br> [0x800005c8]:sw t6, 104(ra)<br> [0x800005cc]:sw t5, 112(ra)<br> [0x800005d0]:sw a7, 120(ra)<br>                                 |
|  16|[0x80008048]<br>0x00000000<br> [0x80008060]<br>0x00000000<br> |- rs2 : x4<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x914e0c751c4f4 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x914e0c751c4f4 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                |[0x8000060c]:fadd.d t5, t3, tp, dyn<br> [0x80000610]:csrrs a7, fcsr, zero<br> [0x80000614]:sw t5, 128(ra)<br> [0x80000618]:sw t6, 136(ra)<br> [0x8000061c]:sw t5, 144(ra)<br> [0x80000620]:sw a7, 152(ra)<br>                                |
|  17|[0x80008068]<br>0x00000000<br> [0x80008080]<br>0x00000020<br> |- rd : x2<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x914e0c751c4f4 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x914e0c751c4f4 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                |[0x8000065c]:fadd.d sp, t5, t3, dyn<br> [0x80000660]:csrrs a7, fcsr, zero<br> [0x80000664]:sw sp, 160(ra)<br> [0x80000668]:sw gp, 168(ra)<br> [0x8000066c]:sw sp, 176(ra)<br> [0x80000670]:sw a7, 184(ra)<br>                                |
|  18|[0x80008088]<br>0x00000000<br> [0x800080a0]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x914e0c751c4f4 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x914e0c751c4f4 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800006ac]:fadd.d t5, t3, s10, dyn<br> [0x800006b0]:csrrs a7, fcsr, zero<br> [0x800006b4]:sw t5, 192(ra)<br> [0x800006b8]:sw t6, 200(ra)<br> [0x800006bc]:sw t5, 208(ra)<br> [0x800006c0]:sw a7, 216(ra)<br>                               |
|  19|[0x800080a8]<br>0x00000000<br> [0x800080c0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x914e0c751c4f4 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x914e0c751c4f4 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800006fc]:fadd.d t5, t3, s10, dyn<br> [0x80000700]:csrrs a7, fcsr, zero<br> [0x80000704]:sw t5, 224(ra)<br> [0x80000708]:sw t6, 232(ra)<br> [0x8000070c]:sw t5, 240(ra)<br> [0x80000710]:sw a7, 248(ra)<br>                               |
|  20|[0x800080c8]<br>0x00000000<br> [0x800080e0]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x914e0c751c4f4 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x914e0c751c4f4 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000074c]:fadd.d t5, t3, s10, dyn<br> [0x80000750]:csrrs a7, fcsr, zero<br> [0x80000754]:sw t5, 256(ra)<br> [0x80000758]:sw t6, 264(ra)<br> [0x8000075c]:sw t5, 272(ra)<br> [0x80000760]:sw a7, 280(ra)<br>                               |
|  21|[0x800080e8]<br>0x00000000<br> [0x80008100]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe809082dd48fb and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe809082dd48fb and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000079c]:fadd.d t5, t3, s10, dyn<br> [0x800007a0]:csrrs a7, fcsr, zero<br> [0x800007a4]:sw t5, 288(ra)<br> [0x800007a8]:sw t6, 296(ra)<br> [0x800007ac]:sw t5, 304(ra)<br> [0x800007b0]:sw a7, 312(ra)<br>                               |
|  22|[0x80008108]<br>0x00000000<br> [0x80008120]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe809082dd48fb and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe809082dd48fb and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800007ec]:fadd.d t5, t3, s10, dyn<br> [0x800007f0]:csrrs a7, fcsr, zero<br> [0x800007f4]:sw t5, 320(ra)<br> [0x800007f8]:sw t6, 328(ra)<br> [0x800007fc]:sw t5, 336(ra)<br> [0x80000800]:sw a7, 344(ra)<br>                               |
|  23|[0x80008128]<br>0x00000000<br> [0x80008140]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe809082dd48fb and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe809082dd48fb and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000083c]:fadd.d t5, t3, s10, dyn<br> [0x80000840]:csrrs a7, fcsr, zero<br> [0x80000844]:sw t5, 352(ra)<br> [0x80000848]:sw t6, 360(ra)<br> [0x8000084c]:sw t5, 368(ra)<br> [0x80000850]:sw a7, 376(ra)<br>                               |
|  24|[0x80008148]<br>0x00000000<br> [0x80008160]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe809082dd48fb and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe809082dd48fb and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000088c]:fadd.d t5, t3, s10, dyn<br> [0x80000890]:csrrs a7, fcsr, zero<br> [0x80000894]:sw t5, 384(ra)<br> [0x80000898]:sw t6, 392(ra)<br> [0x8000089c]:sw t5, 400(ra)<br> [0x800008a0]:sw a7, 408(ra)<br>                               |
|  25|[0x80008168]<br>0x00000000<br> [0x80008180]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe809082dd48fb and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe809082dd48fb and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800008dc]:fadd.d t5, t3, s10, dyn<br> [0x800008e0]:csrrs a7, fcsr, zero<br> [0x800008e4]:sw t5, 416(ra)<br> [0x800008e8]:sw t6, 424(ra)<br> [0x800008ec]:sw t5, 432(ra)<br> [0x800008f0]:sw a7, 440(ra)<br>                               |
|  26|[0x80008188]<br>0x00000000<br> [0x800081a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd0f42c0dfaf72 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd0f42c0dfaf72 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000092c]:fadd.d t5, t3, s10, dyn<br> [0x80000930]:csrrs a7, fcsr, zero<br> [0x80000934]:sw t5, 448(ra)<br> [0x80000938]:sw t6, 456(ra)<br> [0x8000093c]:sw t5, 464(ra)<br> [0x80000940]:sw a7, 472(ra)<br>                               |
|  27|[0x800081a8]<br>0x00000000<br> [0x800081c0]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd0f42c0dfaf72 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd0f42c0dfaf72 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000097c]:fadd.d t5, t3, s10, dyn<br> [0x80000980]:csrrs a7, fcsr, zero<br> [0x80000984]:sw t5, 480(ra)<br> [0x80000988]:sw t6, 488(ra)<br> [0x8000098c]:sw t5, 496(ra)<br> [0x80000990]:sw a7, 504(ra)<br>                               |
|  28|[0x800081c8]<br>0x00000000<br> [0x800081e0]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd0f42c0dfaf72 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd0f42c0dfaf72 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800009cc]:fadd.d t5, t3, s10, dyn<br> [0x800009d0]:csrrs a7, fcsr, zero<br> [0x800009d4]:sw t5, 512(ra)<br> [0x800009d8]:sw t6, 520(ra)<br> [0x800009dc]:sw t5, 528(ra)<br> [0x800009e0]:sw a7, 536(ra)<br>                               |
|  29|[0x800081e8]<br>0x00000000<br> [0x80008200]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd0f42c0dfaf72 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd0f42c0dfaf72 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000a1c]:fadd.d t5, t3, s10, dyn<br> [0x80000a20]:csrrs a7, fcsr, zero<br> [0x80000a24]:sw t5, 544(ra)<br> [0x80000a28]:sw t6, 552(ra)<br> [0x80000a2c]:sw t5, 560(ra)<br> [0x80000a30]:sw a7, 568(ra)<br>                               |
|  30|[0x80008208]<br>0x00000000<br> [0x80008220]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd0f42c0dfaf72 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd0f42c0dfaf72 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000a6c]:fadd.d t5, t3, s10, dyn<br> [0x80000a70]:csrrs a7, fcsr, zero<br> [0x80000a74]:sw t5, 576(ra)<br> [0x80000a78]:sw t6, 584(ra)<br> [0x80000a7c]:sw t5, 592(ra)<br> [0x80000a80]:sw a7, 600(ra)<br>                               |
|  31|[0x80008228]<br>0x00000000<br> [0x80008240]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x209a1991e3307 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x209a1991e3307 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000abc]:fadd.d t5, t3, s10, dyn<br> [0x80000ac0]:csrrs a7, fcsr, zero<br> [0x80000ac4]:sw t5, 608(ra)<br> [0x80000ac8]:sw t6, 616(ra)<br> [0x80000acc]:sw t5, 624(ra)<br> [0x80000ad0]:sw a7, 632(ra)<br>                               |
|  32|[0x80008248]<br>0x00000000<br> [0x80008260]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x209a1991e3307 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x209a1991e3307 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000b0c]:fadd.d t5, t3, s10, dyn<br> [0x80000b10]:csrrs a7, fcsr, zero<br> [0x80000b14]:sw t5, 640(ra)<br> [0x80000b18]:sw t6, 648(ra)<br> [0x80000b1c]:sw t5, 656(ra)<br> [0x80000b20]:sw a7, 664(ra)<br>                               |
|  33|[0x80008268]<br>0x00000000<br> [0x80008280]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x209a1991e3307 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x209a1991e3307 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000b5c]:fadd.d t5, t3, s10, dyn<br> [0x80000b60]:csrrs a7, fcsr, zero<br> [0x80000b64]:sw t5, 672(ra)<br> [0x80000b68]:sw t6, 680(ra)<br> [0x80000b6c]:sw t5, 688(ra)<br> [0x80000b70]:sw a7, 696(ra)<br>                               |
|  34|[0x80008288]<br>0x00000000<br> [0x800082a0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x209a1991e3307 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x209a1991e3307 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000bac]:fadd.d t5, t3, s10, dyn<br> [0x80000bb0]:csrrs a7, fcsr, zero<br> [0x80000bb4]:sw t5, 704(ra)<br> [0x80000bb8]:sw t6, 712(ra)<br> [0x80000bbc]:sw t5, 720(ra)<br> [0x80000bc0]:sw a7, 728(ra)<br>                               |
|  35|[0x800082a8]<br>0x00000000<br> [0x800082c0]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x209a1991e3307 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x209a1991e3307 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000bfc]:fadd.d t5, t3, s10, dyn<br> [0x80000c00]:csrrs a7, fcsr, zero<br> [0x80000c04]:sw t5, 736(ra)<br> [0x80000c08]:sw t6, 744(ra)<br> [0x80000c0c]:sw t5, 752(ra)<br> [0x80000c10]:sw a7, 760(ra)<br>                               |
|  36|[0x800082c8]<br>0x00000000<br> [0x800082e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3c9adc7329695 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x3c9adc7329695 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000c4c]:fadd.d t5, t3, s10, dyn<br> [0x80000c50]:csrrs a7, fcsr, zero<br> [0x80000c54]:sw t5, 768(ra)<br> [0x80000c58]:sw t6, 776(ra)<br> [0x80000c5c]:sw t5, 784(ra)<br> [0x80000c60]:sw a7, 792(ra)<br>                               |
|  37|[0x800082e8]<br>0x00000000<br> [0x80008300]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3c9adc7329695 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x3c9adc7329695 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000c9c]:fadd.d t5, t3, s10, dyn<br> [0x80000ca0]:csrrs a7, fcsr, zero<br> [0x80000ca4]:sw t5, 800(ra)<br> [0x80000ca8]:sw t6, 808(ra)<br> [0x80000cac]:sw t5, 816(ra)<br> [0x80000cb0]:sw a7, 824(ra)<br>                               |
|  38|[0x80008308]<br>0x00000000<br> [0x80008320]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3c9adc7329695 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x3c9adc7329695 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000cec]:fadd.d t5, t3, s10, dyn<br> [0x80000cf0]:csrrs a7, fcsr, zero<br> [0x80000cf4]:sw t5, 832(ra)<br> [0x80000cf8]:sw t6, 840(ra)<br> [0x80000cfc]:sw t5, 848(ra)<br> [0x80000d00]:sw a7, 856(ra)<br>                               |
|  39|[0x80008328]<br>0x00000000<br> [0x80008340]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3c9adc7329695 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x3c9adc7329695 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000d3c]:fadd.d t5, t3, s10, dyn<br> [0x80000d40]:csrrs a7, fcsr, zero<br> [0x80000d44]:sw t5, 864(ra)<br> [0x80000d48]:sw t6, 872(ra)<br> [0x80000d4c]:sw t5, 880(ra)<br> [0x80000d50]:sw a7, 888(ra)<br>                               |
|  40|[0x80008348]<br>0x00000000<br> [0x80008360]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3c9adc7329695 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x3c9adc7329695 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000d8c]:fadd.d t5, t3, s10, dyn<br> [0x80000d90]:csrrs a7, fcsr, zero<br> [0x80000d94]:sw t5, 896(ra)<br> [0x80000d98]:sw t6, 904(ra)<br> [0x80000d9c]:sw t5, 912(ra)<br> [0x80000da0]:sw a7, 920(ra)<br>                               |
|  41|[0x80008368]<br>0x00000000<br> [0x80008380]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd1ca42e21585b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd1ca42e21585b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000ddc]:fadd.d t5, t3, s10, dyn<br> [0x80000de0]:csrrs a7, fcsr, zero<br> [0x80000de4]:sw t5, 928(ra)<br> [0x80000de8]:sw t6, 936(ra)<br> [0x80000dec]:sw t5, 944(ra)<br> [0x80000df0]:sw a7, 952(ra)<br>                               |
|  42|[0x80008388]<br>0x00000000<br> [0x800083a0]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd1ca42e21585b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd1ca42e21585b and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000e2c]:fadd.d t5, t3, s10, dyn<br> [0x80000e30]:csrrs a7, fcsr, zero<br> [0x80000e34]:sw t5, 960(ra)<br> [0x80000e38]:sw t6, 968(ra)<br> [0x80000e3c]:sw t5, 976(ra)<br> [0x80000e40]:sw a7, 984(ra)<br>                               |
|  43|[0x800083a8]<br>0x00000000<br> [0x800083c0]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd1ca42e21585b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd1ca42e21585b and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80000e7c]:fadd.d t5, t3, s10, dyn<br> [0x80000e80]:csrrs a7, fcsr, zero<br> [0x80000e84]:sw t5, 992(ra)<br> [0x80000e88]:sw t6, 1000(ra)<br> [0x80000e8c]:sw t5, 1008(ra)<br> [0x80000e90]:sw a7, 1016(ra)<br>                            |
|  44|[0x800083c8]<br>0x00000000<br> [0x800083e0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd1ca42e21585b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd1ca42e21585b and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80000ecc]:fadd.d t5, t3, s10, dyn<br> [0x80000ed0]:csrrs a7, fcsr, zero<br> [0x80000ed4]:sw t5, 1024(ra)<br> [0x80000ed8]:sw t6, 1032(ra)<br> [0x80000edc]:sw t5, 1040(ra)<br> [0x80000ee0]:sw a7, 1048(ra)<br>                           |
|  45|[0x800083e8]<br>0x00000000<br> [0x80008400]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd1ca42e21585b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd1ca42e21585b and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80000f1c]:fadd.d t5, t3, s10, dyn<br> [0x80000f20]:csrrs a7, fcsr, zero<br> [0x80000f24]:sw t5, 1056(ra)<br> [0x80000f28]:sw t6, 1064(ra)<br> [0x80000f2c]:sw t5, 1072(ra)<br> [0x80000f30]:sw a7, 1080(ra)<br>                           |
|  46|[0x80008408]<br>0x00000000<br> [0x80008420]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9ed4cb2685903 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9ed4cb2685903 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000f6c]:fadd.d t5, t3, s10, dyn<br> [0x80000f70]:csrrs a7, fcsr, zero<br> [0x80000f74]:sw t5, 1088(ra)<br> [0x80000f78]:sw t6, 1096(ra)<br> [0x80000f7c]:sw t5, 1104(ra)<br> [0x80000f80]:sw a7, 1112(ra)<br>                           |
|  47|[0x80008428]<br>0x00000000<br> [0x80008440]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9ed4cb2685903 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9ed4cb2685903 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80000fbc]:fadd.d t5, t3, s10, dyn<br> [0x80000fc0]:csrrs a7, fcsr, zero<br> [0x80000fc4]:sw t5, 1120(ra)<br> [0x80000fc8]:sw t6, 1128(ra)<br> [0x80000fcc]:sw t5, 1136(ra)<br> [0x80000fd0]:sw a7, 1144(ra)<br>                           |
|  48|[0x80008448]<br>0x00000000<br> [0x80008460]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9ed4cb2685903 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9ed4cb2685903 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000100c]:fadd.d t5, t3, s10, dyn<br> [0x80001010]:csrrs a7, fcsr, zero<br> [0x80001014]:sw t5, 1152(ra)<br> [0x80001018]:sw t6, 1160(ra)<br> [0x8000101c]:sw t5, 1168(ra)<br> [0x80001020]:sw a7, 1176(ra)<br>                           |
|  49|[0x80008468]<br>0x00000000<br> [0x80008480]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9ed4cb2685903 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9ed4cb2685903 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000105c]:fadd.d t5, t3, s10, dyn<br> [0x80001060]:csrrs a7, fcsr, zero<br> [0x80001064]:sw t5, 1184(ra)<br> [0x80001068]:sw t6, 1192(ra)<br> [0x8000106c]:sw t5, 1200(ra)<br> [0x80001070]:sw a7, 1208(ra)<br>                           |
|  50|[0x80008488]<br>0x00000000<br> [0x800084a0]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9ed4cb2685903 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9ed4cb2685903 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800010ac]:fadd.d t5, t3, s10, dyn<br> [0x800010b0]:csrrs a7, fcsr, zero<br> [0x800010b4]:sw t5, 1216(ra)<br> [0x800010b8]:sw t6, 1224(ra)<br> [0x800010bc]:sw t5, 1232(ra)<br> [0x800010c0]:sw a7, 1240(ra)<br>                           |
|  51|[0x800084a8]<br>0x00000000<br> [0x800084c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3d97530ca446d and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x3d97530ca446d and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800010fc]:fadd.d t5, t3, s10, dyn<br> [0x80001100]:csrrs a7, fcsr, zero<br> [0x80001104]:sw t5, 1248(ra)<br> [0x80001108]:sw t6, 1256(ra)<br> [0x8000110c]:sw t5, 1264(ra)<br> [0x80001110]:sw a7, 1272(ra)<br>                           |
|  52|[0x800084c8]<br>0x00000000<br> [0x800084e0]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3d97530ca446d and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x3d97530ca446d and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000114c]:fadd.d t5, t3, s10, dyn<br> [0x80001150]:csrrs a7, fcsr, zero<br> [0x80001154]:sw t5, 1280(ra)<br> [0x80001158]:sw t6, 1288(ra)<br> [0x8000115c]:sw t5, 1296(ra)<br> [0x80001160]:sw a7, 1304(ra)<br>                           |
|  53|[0x800084e8]<br>0x00000000<br> [0x80008500]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3d97530ca446d and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x3d97530ca446d and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000119c]:fadd.d t5, t3, s10, dyn<br> [0x800011a0]:csrrs a7, fcsr, zero<br> [0x800011a4]:sw t5, 1312(ra)<br> [0x800011a8]:sw t6, 1320(ra)<br> [0x800011ac]:sw t5, 1328(ra)<br> [0x800011b0]:sw a7, 1336(ra)<br>                           |
|  54|[0x80008508]<br>0x00000000<br> [0x80008520]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3d97530ca446d and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x3d97530ca446d and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800011ec]:fadd.d t5, t3, s10, dyn<br> [0x800011f0]:csrrs a7, fcsr, zero<br> [0x800011f4]:sw t5, 1344(ra)<br> [0x800011f8]:sw t6, 1352(ra)<br> [0x800011fc]:sw t5, 1360(ra)<br> [0x80001200]:sw a7, 1368(ra)<br>                           |
|  55|[0x80008528]<br>0x00000000<br> [0x80008540]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3d97530ca446d and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x3d97530ca446d and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000123c]:fadd.d t5, t3, s10, dyn<br> [0x80001240]:csrrs a7, fcsr, zero<br> [0x80001244]:sw t5, 1376(ra)<br> [0x80001248]:sw t6, 1384(ra)<br> [0x8000124c]:sw t5, 1392(ra)<br> [0x80001250]:sw a7, 1400(ra)<br>                           |
|  56|[0x80008548]<br>0x00000000<br> [0x80008560]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcc3488366e29b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xcc3488366e29b and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000128c]:fadd.d t5, t3, s10, dyn<br> [0x80001290]:csrrs a7, fcsr, zero<br> [0x80001294]:sw t5, 1408(ra)<br> [0x80001298]:sw t6, 1416(ra)<br> [0x8000129c]:sw t5, 1424(ra)<br> [0x800012a0]:sw a7, 1432(ra)<br>                           |
|  57|[0x80008568]<br>0x00000000<br> [0x80008580]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcc3488366e29b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xcc3488366e29b and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800012dc]:fadd.d t5, t3, s10, dyn<br> [0x800012e0]:csrrs a7, fcsr, zero<br> [0x800012e4]:sw t5, 1440(ra)<br> [0x800012e8]:sw t6, 1448(ra)<br> [0x800012ec]:sw t5, 1456(ra)<br> [0x800012f0]:sw a7, 1464(ra)<br>                           |
|  58|[0x80008588]<br>0x00000000<br> [0x800085a0]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcc3488366e29b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xcc3488366e29b and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000132c]:fadd.d t5, t3, s10, dyn<br> [0x80001330]:csrrs a7, fcsr, zero<br> [0x80001334]:sw t5, 1472(ra)<br> [0x80001338]:sw t6, 1480(ra)<br> [0x8000133c]:sw t5, 1488(ra)<br> [0x80001340]:sw a7, 1496(ra)<br>                           |
|  59|[0x800085a8]<br>0x00000000<br> [0x800085c0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcc3488366e29b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xcc3488366e29b and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000137c]:fadd.d t5, t3, s10, dyn<br> [0x80001380]:csrrs a7, fcsr, zero<br> [0x80001384]:sw t5, 1504(ra)<br> [0x80001388]:sw t6, 1512(ra)<br> [0x8000138c]:sw t5, 1520(ra)<br> [0x80001390]:sw a7, 1528(ra)<br>                           |
|  60|[0x800085c8]<br>0x00000000<br> [0x800085e0]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcc3488366e29b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xcc3488366e29b and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800013cc]:fadd.d t5, t3, s10, dyn<br> [0x800013d0]:csrrs a7, fcsr, zero<br> [0x800013d4]:sw t5, 1536(ra)<br> [0x800013d8]:sw t6, 1544(ra)<br> [0x800013dc]:sw t5, 1552(ra)<br> [0x800013e0]:sw a7, 1560(ra)<br>                           |
|  61|[0x800085e8]<br>0x00000000<br> [0x80008600]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe3796147a7f97 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe3796147a7f97 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000141c]:fadd.d t5, t3, s10, dyn<br> [0x80001420]:csrrs a7, fcsr, zero<br> [0x80001424]:sw t5, 1568(ra)<br> [0x80001428]:sw t6, 1576(ra)<br> [0x8000142c]:sw t5, 1584(ra)<br> [0x80001430]:sw a7, 1592(ra)<br>                           |
|  62|[0x80008608]<br>0x00000000<br> [0x80008620]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe3796147a7f97 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe3796147a7f97 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000146c]:fadd.d t5, t3, s10, dyn<br> [0x80001470]:csrrs a7, fcsr, zero<br> [0x80001474]:sw t5, 1600(ra)<br> [0x80001478]:sw t6, 1608(ra)<br> [0x8000147c]:sw t5, 1616(ra)<br> [0x80001480]:sw a7, 1624(ra)<br>                           |
|  63|[0x80008628]<br>0x00000000<br> [0x80008640]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe3796147a7f97 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe3796147a7f97 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800014bc]:fadd.d t5, t3, s10, dyn<br> [0x800014c0]:csrrs a7, fcsr, zero<br> [0x800014c4]:sw t5, 1632(ra)<br> [0x800014c8]:sw t6, 1640(ra)<br> [0x800014cc]:sw t5, 1648(ra)<br> [0x800014d0]:sw a7, 1656(ra)<br>                           |
|  64|[0x80008648]<br>0x00000000<br> [0x80008660]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe3796147a7f97 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe3796147a7f97 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000150c]:fadd.d t5, t3, s10, dyn<br> [0x80001510]:csrrs a7, fcsr, zero<br> [0x80001514]:sw t5, 1664(ra)<br> [0x80001518]:sw t6, 1672(ra)<br> [0x8000151c]:sw t5, 1680(ra)<br> [0x80001520]:sw a7, 1688(ra)<br>                           |
|  65|[0x80008668]<br>0x00000000<br> [0x80008680]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe3796147a7f97 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe3796147a7f97 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000155c]:fadd.d t5, t3, s10, dyn<br> [0x80001560]:csrrs a7, fcsr, zero<br> [0x80001564]:sw t5, 1696(ra)<br> [0x80001568]:sw t6, 1704(ra)<br> [0x8000156c]:sw t5, 1712(ra)<br> [0x80001570]:sw a7, 1720(ra)<br>                           |
|  66|[0x80008688]<br>0x00000000<br> [0x800086a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xbc978aa879221 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xbc978aa879221 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800015ac]:fadd.d t5, t3, s10, dyn<br> [0x800015b0]:csrrs a7, fcsr, zero<br> [0x800015b4]:sw t5, 1728(ra)<br> [0x800015b8]:sw t6, 1736(ra)<br> [0x800015bc]:sw t5, 1744(ra)<br> [0x800015c0]:sw a7, 1752(ra)<br>                           |
|  67|[0x800086a8]<br>0x00000000<br> [0x800086c0]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xbc978aa879221 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xbc978aa879221 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800015fc]:fadd.d t5, t3, s10, dyn<br> [0x80001600]:csrrs a7, fcsr, zero<br> [0x80001604]:sw t5, 1760(ra)<br> [0x80001608]:sw t6, 1768(ra)<br> [0x8000160c]:sw t5, 1776(ra)<br> [0x80001610]:sw a7, 1784(ra)<br>                           |
|  68|[0x800086c8]<br>0x00000000<br> [0x800086e0]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xbc978aa879221 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xbc978aa879221 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x8000164c]:fadd.d t5, t3, s10, dyn<br> [0x80001650]:csrrs a7, fcsr, zero<br> [0x80001654]:sw t5, 1792(ra)<br> [0x80001658]:sw t6, 1800(ra)<br> [0x8000165c]:sw t5, 1808(ra)<br> [0x80001660]:sw a7, 1816(ra)<br>                           |
|  69|[0x800086e8]<br>0x00000000<br> [0x80008700]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xbc978aa879221 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xbc978aa879221 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000169c]:fadd.d t5, t3, s10, dyn<br> [0x800016a0]:csrrs a7, fcsr, zero<br> [0x800016a4]:sw t5, 1824(ra)<br> [0x800016a8]:sw t6, 1832(ra)<br> [0x800016ac]:sw t5, 1840(ra)<br> [0x800016b0]:sw a7, 1848(ra)<br>                           |
|  70|[0x80008708]<br>0x00000000<br> [0x80008720]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xbc978aa879221 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xbc978aa879221 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800016ec]:fadd.d t5, t3, s10, dyn<br> [0x800016f0]:csrrs a7, fcsr, zero<br> [0x800016f4]:sw t5, 1856(ra)<br> [0x800016f8]:sw t6, 1864(ra)<br> [0x800016fc]:sw t5, 1872(ra)<br> [0x80001700]:sw a7, 1880(ra)<br>                           |
|  71|[0x80008728]<br>0x00000000<br> [0x80008740]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd3762f4d1629c and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd3762f4d1629c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000173c]:fadd.d t5, t3, s10, dyn<br> [0x80001740]:csrrs a7, fcsr, zero<br> [0x80001744]:sw t5, 1888(ra)<br> [0x80001748]:sw t6, 1896(ra)<br> [0x8000174c]:sw t5, 1904(ra)<br> [0x80001750]:sw a7, 1912(ra)<br>                           |
|  72|[0x80008748]<br>0x00000000<br> [0x80008760]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd3762f4d1629c and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd3762f4d1629c and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x8000178c]:fadd.d t5, t3, s10, dyn<br> [0x80001790]:csrrs a7, fcsr, zero<br> [0x80001794]:sw t5, 1920(ra)<br> [0x80001798]:sw t6, 1928(ra)<br> [0x8000179c]:sw t5, 1936(ra)<br> [0x800017a0]:sw a7, 1944(ra)<br>                           |
|  73|[0x80008768]<br>0x00000000<br> [0x80008780]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd3762f4d1629c and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd3762f4d1629c and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800017dc]:fadd.d t5, t3, s10, dyn<br> [0x800017e0]:csrrs a7, fcsr, zero<br> [0x800017e4]:sw t5, 1952(ra)<br> [0x800017e8]:sw t6, 1960(ra)<br> [0x800017ec]:sw t5, 1968(ra)<br> [0x800017f0]:sw a7, 1976(ra)<br>                           |
|  74|[0x80008788]<br>0x00000000<br> [0x800087a0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd3762f4d1629c and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd3762f4d1629c and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x8000182c]:fadd.d t5, t3, s10, dyn<br> [0x80001830]:csrrs a7, fcsr, zero<br> [0x80001834]:sw t5, 1984(ra)<br> [0x80001838]:sw t6, 1992(ra)<br> [0x8000183c]:sw t5, 2000(ra)<br> [0x80001840]:sw a7, 2008(ra)<br>                           |
|  75|[0x800087a8]<br>0x00000000<br> [0x800087c0]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd3762f4d1629c and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd3762f4d1629c and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x8000187c]:fadd.d t5, t3, s10, dyn<br> [0x80001880]:csrrs a7, fcsr, zero<br> [0x80001884]:sw t5, 2016(ra)<br> [0x80001888]:sw t6, 2024(ra)<br> [0x8000188c]:sw t5, 2032(ra)<br> [0x80001890]:sw a7, 2040(ra)<br>                           |
|  76|[0x800087c8]<br>0x00000000<br> [0x800087e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe8754038aa2cf and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe8754038aa2cf and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800018cc]:fadd.d t5, t3, s10, dyn<br> [0x800018d0]:csrrs a7, fcsr, zero<br> [0x800018d4]:addi ra, ra, 2040<br> [0x800018d8]:sw t5, 8(ra)<br> [0x800018dc]:sw t6, 16(ra)<br> [0x800018e0]:sw t5, 24(ra)<br> [0x800018e4]:sw a7, 32(ra)<br> |
|  77|[0x800087e8]<br>0x00000000<br> [0x80008800]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe8754038aa2cf and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe8754038aa2cf and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80001920]:fadd.d t5, t3, s10, dyn<br> [0x80001924]:csrrs a7, fcsr, zero<br> [0x80001928]:sw t5, 40(ra)<br> [0x8000192c]:sw t6, 48(ra)<br> [0x80001930]:sw t5, 56(ra)<br> [0x80001934]:sw a7, 64(ra)<br>                                   |
|  78|[0x80008808]<br>0x00000000<br> [0x80008820]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe8754038aa2cf and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe8754038aa2cf and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80001970]:fadd.d t5, t3, s10, dyn<br> [0x80001974]:csrrs a7, fcsr, zero<br> [0x80001978]:sw t5, 72(ra)<br> [0x8000197c]:sw t6, 80(ra)<br> [0x80001980]:sw t5, 88(ra)<br> [0x80001984]:sw a7, 96(ra)<br>                                   |
|  79|[0x80008828]<br>0x00000000<br> [0x80008840]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe8754038aa2cf and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe8754038aa2cf and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x800019c0]:fadd.d t5, t3, s10, dyn<br> [0x800019c4]:csrrs a7, fcsr, zero<br> [0x800019c8]:sw t5, 104(ra)<br> [0x800019cc]:sw t6, 112(ra)<br> [0x800019d0]:sw t5, 120(ra)<br> [0x800019d4]:sw a7, 128(ra)<br>                               |
|  80|[0x80008848]<br>0x00000000<br> [0x80008860]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe8754038aa2cf and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe8754038aa2cf and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80001a10]:fadd.d t5, t3, s10, dyn<br> [0x80001a14]:csrrs a7, fcsr, zero<br> [0x80001a18]:sw t5, 136(ra)<br> [0x80001a1c]:sw t6, 144(ra)<br> [0x80001a20]:sw t5, 152(ra)<br> [0x80001a24]:sw a7, 160(ra)<br>                               |
|  81|[0x80008868]<br>0x00000000<br> [0x80008880]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x0abe7f07f8c6f and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x0abe7f07f8c6f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001a60]:fadd.d t5, t3, s10, dyn<br> [0x80001a64]:csrrs a7, fcsr, zero<br> [0x80001a68]:sw t5, 168(ra)<br> [0x80001a6c]:sw t6, 176(ra)<br> [0x80001a70]:sw t5, 184(ra)<br> [0x80001a74]:sw a7, 192(ra)<br>                               |
|  82|[0x80008888]<br>0x00000000<br> [0x800088a0]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x0abe7f07f8c6f and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x0abe7f07f8c6f and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80001ab0]:fadd.d t5, t3, s10, dyn<br> [0x80001ab4]:csrrs a7, fcsr, zero<br> [0x80001ab8]:sw t5, 200(ra)<br> [0x80001abc]:sw t6, 208(ra)<br> [0x80001ac0]:sw t5, 216(ra)<br> [0x80001ac4]:sw a7, 224(ra)<br>                               |
|  83|[0x800088a8]<br>0x00000000<br> [0x800088c0]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x0abe7f07f8c6f and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x0abe7f07f8c6f and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80001b00]:fadd.d t5, t3, s10, dyn<br> [0x80001b04]:csrrs a7, fcsr, zero<br> [0x80001b08]:sw t5, 232(ra)<br> [0x80001b0c]:sw t6, 240(ra)<br> [0x80001b10]:sw t5, 248(ra)<br> [0x80001b14]:sw a7, 256(ra)<br>                               |
|  84|[0x800088c8]<br>0x00000000<br> [0x800088e0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x0abe7f07f8c6f and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x0abe7f07f8c6f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80001b50]:fadd.d t5, t3, s10, dyn<br> [0x80001b54]:csrrs a7, fcsr, zero<br> [0x80001b58]:sw t5, 264(ra)<br> [0x80001b5c]:sw t6, 272(ra)<br> [0x80001b60]:sw t5, 280(ra)<br> [0x80001b64]:sw a7, 288(ra)<br>                               |
|  85|[0x800088e8]<br>0x00000000<br> [0x80008900]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x0abe7f07f8c6f and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x0abe7f07f8c6f and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80001ba0]:fadd.d t5, t3, s10, dyn<br> [0x80001ba4]:csrrs a7, fcsr, zero<br> [0x80001ba8]:sw t5, 296(ra)<br> [0x80001bac]:sw t6, 304(ra)<br> [0x80001bb0]:sw t5, 312(ra)<br> [0x80001bb4]:sw a7, 320(ra)<br>                               |
|  86|[0x80008908]<br>0x00000000<br> [0x80008920]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x18ef1d7a9fa74 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x18ef1d7a9fa74 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001bf0]:fadd.d t5, t3, s10, dyn<br> [0x80001bf4]:csrrs a7, fcsr, zero<br> [0x80001bf8]:sw t5, 328(ra)<br> [0x80001bfc]:sw t6, 336(ra)<br> [0x80001c00]:sw t5, 344(ra)<br> [0x80001c04]:sw a7, 352(ra)<br>                               |
|  87|[0x80008928]<br>0x00000000<br> [0x80008940]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x18ef1d7a9fa74 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x18ef1d7a9fa74 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80001c40]:fadd.d t5, t3, s10, dyn<br> [0x80001c44]:csrrs a7, fcsr, zero<br> [0x80001c48]:sw t5, 360(ra)<br> [0x80001c4c]:sw t6, 368(ra)<br> [0x80001c50]:sw t5, 376(ra)<br> [0x80001c54]:sw a7, 384(ra)<br>                               |
|  88|[0x80008948]<br>0x00000000<br> [0x80008960]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x18ef1d7a9fa74 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x18ef1d7a9fa74 and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80001c90]:fadd.d t5, t3, s10, dyn<br> [0x80001c94]:csrrs a7, fcsr, zero<br> [0x80001c98]:sw t5, 392(ra)<br> [0x80001c9c]:sw t6, 400(ra)<br> [0x80001ca0]:sw t5, 408(ra)<br> [0x80001ca4]:sw a7, 416(ra)<br>                               |
|  89|[0x80008968]<br>0x00000000<br> [0x80008980]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x18ef1d7a9fa74 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x18ef1d7a9fa74 and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80001ce0]:fadd.d t5, t3, s10, dyn<br> [0x80001ce4]:csrrs a7, fcsr, zero<br> [0x80001ce8]:sw t5, 424(ra)<br> [0x80001cec]:sw t6, 432(ra)<br> [0x80001cf0]:sw t5, 440(ra)<br> [0x80001cf4]:sw a7, 448(ra)<br>                               |
|  90|[0x80008988]<br>0x00000000<br> [0x800089a0]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x18ef1d7a9fa74 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x18ef1d7a9fa74 and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80001d30]:fadd.d t5, t3, s10, dyn<br> [0x80001d34]:csrrs a7, fcsr, zero<br> [0x80001d38]:sw t5, 456(ra)<br> [0x80001d3c]:sw t6, 464(ra)<br> [0x80001d40]:sw t5, 472(ra)<br> [0x80001d44]:sw a7, 480(ra)<br>                               |
|  91|[0x800089a8]<br>0x00000000<br> [0x800089c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x707d21f5c40de and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x707d21f5c40de and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001d80]:fadd.d t5, t3, s10, dyn<br> [0x80001d84]:csrrs a7, fcsr, zero<br> [0x80001d88]:sw t5, 488(ra)<br> [0x80001d8c]:sw t6, 496(ra)<br> [0x80001d90]:sw t5, 504(ra)<br> [0x80001d94]:sw a7, 512(ra)<br>                               |
|  92|[0x800089c8]<br>0x00000000<br> [0x800089e0]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x707d21f5c40de and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x707d21f5c40de and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80001dd0]:fadd.d t5, t3, s10, dyn<br> [0x80001dd4]:csrrs a7, fcsr, zero<br> [0x80001dd8]:sw t5, 520(ra)<br> [0x80001ddc]:sw t6, 528(ra)<br> [0x80001de0]:sw t5, 536(ra)<br> [0x80001de4]:sw a7, 544(ra)<br>                               |
|  93|[0x800089e8]<br>0x00000000<br> [0x80008a00]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x707d21f5c40de and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x707d21f5c40de and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80001e20]:fadd.d t5, t3, s10, dyn<br> [0x80001e24]:csrrs a7, fcsr, zero<br> [0x80001e28]:sw t5, 552(ra)<br> [0x80001e2c]:sw t6, 560(ra)<br> [0x80001e30]:sw t5, 568(ra)<br> [0x80001e34]:sw a7, 576(ra)<br>                               |
|  94|[0x80008a08]<br>0x00000000<br> [0x80008a20]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x707d21f5c40de and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x707d21f5c40de and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80001e70]:fadd.d t5, t3, s10, dyn<br> [0x80001e74]:csrrs a7, fcsr, zero<br> [0x80001e78]:sw t5, 584(ra)<br> [0x80001e7c]:sw t6, 592(ra)<br> [0x80001e80]:sw t5, 600(ra)<br> [0x80001e84]:sw a7, 608(ra)<br>                               |
|  95|[0x80008a28]<br>0x00000000<br> [0x80008a40]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x707d21f5c40de and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x707d21f5c40de and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80001ec0]:fadd.d t5, t3, s10, dyn<br> [0x80001ec4]:csrrs a7, fcsr, zero<br> [0x80001ec8]:sw t5, 616(ra)<br> [0x80001ecc]:sw t6, 624(ra)<br> [0x80001ed0]:sw t5, 632(ra)<br> [0x80001ed4]:sw a7, 640(ra)<br>                               |
|  96|[0x80008a48]<br>0x00000000<br> [0x80008a60]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa65214b23e38e and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xa65214b23e38e and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001f10]:fadd.d t5, t3, s10, dyn<br> [0x80001f14]:csrrs a7, fcsr, zero<br> [0x80001f18]:sw t5, 648(ra)<br> [0x80001f1c]:sw t6, 656(ra)<br> [0x80001f20]:sw t5, 664(ra)<br> [0x80001f24]:sw a7, 672(ra)<br>                               |
|  97|[0x80008a68]<br>0x00000000<br> [0x80008a80]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa65214b23e38e and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xa65214b23e38e and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80001f60]:fadd.d t5, t3, s10, dyn<br> [0x80001f64]:csrrs a7, fcsr, zero<br> [0x80001f68]:sw t5, 680(ra)<br> [0x80001f6c]:sw t6, 688(ra)<br> [0x80001f70]:sw t5, 696(ra)<br> [0x80001f74]:sw a7, 704(ra)<br>                               |
|  98|[0x80008a88]<br>0x00000000<br> [0x80008aa0]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa65214b23e38e and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xa65214b23e38e and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80001fb0]:fadd.d t5, t3, s10, dyn<br> [0x80001fb4]:csrrs a7, fcsr, zero<br> [0x80001fb8]:sw t5, 712(ra)<br> [0x80001fbc]:sw t6, 720(ra)<br> [0x80001fc0]:sw t5, 728(ra)<br> [0x80001fc4]:sw a7, 736(ra)<br>                               |
|  99|[0x80008aa8]<br>0x00000000<br> [0x80008ac0]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa65214b23e38e and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xa65214b23e38e and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80002000]:fadd.d t5, t3, s10, dyn<br> [0x80002004]:csrrs a7, fcsr, zero<br> [0x80002008]:sw t5, 744(ra)<br> [0x8000200c]:sw t6, 752(ra)<br> [0x80002010]:sw t5, 760(ra)<br> [0x80002014]:sw a7, 768(ra)<br>                               |
| 100|[0x80008ac8]<br>0x00000000<br> [0x80008ae0]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa65214b23e38e and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xa65214b23e38e and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80002050]:fadd.d t5, t3, s10, dyn<br> [0x80002054]:csrrs a7, fcsr, zero<br> [0x80002058]:sw t5, 776(ra)<br> [0x8000205c]:sw t6, 784(ra)<br> [0x80002060]:sw t5, 792(ra)<br> [0x80002064]:sw a7, 800(ra)<br>                               |
| 101|[0x80008ae8]<br>0x00000000<br> [0x80008b00]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7f5 and fm1 == 0x2b954e52a4bff and fs2 == 1 and fe2 == 0x7f5 and fm2 == 0x2b954e52a4bff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800020a0]:fadd.d t5, t3, s10, dyn<br> [0x800020a4]:csrrs a7, fcsr, zero<br> [0x800020a8]:sw t5, 808(ra)<br> [0x800020ac]:sw t6, 816(ra)<br> [0x800020b0]:sw t5, 824(ra)<br> [0x800020b4]:sw a7, 832(ra)<br>                               |
| 102|[0x80008b08]<br>0x00000000<br> [0x80008b20]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7f5 and fm1 == 0x2b954e52a4bff and fs2 == 1 and fe2 == 0x7f5 and fm2 == 0x2b954e52a4bff and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x800020f0]:fadd.d t5, t3, s10, dyn<br> [0x800020f4]:csrrs a7, fcsr, zero<br> [0x800020f8]:sw t5, 840(ra)<br> [0x800020fc]:sw t6, 848(ra)<br> [0x80002100]:sw t5, 856(ra)<br> [0x80002104]:sw a7, 864(ra)<br>                               |
| 103|[0x80008b28]<br>0x00000000<br> [0x80008b40]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7f5 and fm1 == 0x2b954e52a4bff and fs2 == 1 and fe2 == 0x7f5 and fm2 == 0x2b954e52a4bff and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x80002140]:fadd.d t5, t3, s10, dyn<br> [0x80002144]:csrrs a7, fcsr, zero<br> [0x80002148]:sw t5, 872(ra)<br> [0x8000214c]:sw t6, 880(ra)<br> [0x80002150]:sw t5, 888(ra)<br> [0x80002154]:sw a7, 896(ra)<br>                               |
| 104|[0x80008b48]<br>0x00000000<br> [0x80008b60]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7f5 and fm1 == 0x2b954e52a4bff and fs2 == 1 and fe2 == 0x7f5 and fm2 == 0x2b954e52a4bff and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80002190]:fadd.d t5, t3, s10, dyn<br> [0x80002194]:csrrs a7, fcsr, zero<br> [0x80002198]:sw t5, 904(ra)<br> [0x8000219c]:sw t6, 912(ra)<br> [0x800021a0]:sw t5, 920(ra)<br> [0x800021a4]:sw a7, 928(ra)<br>                               |
| 105|[0x80008b68]<br>0x00000000<br> [0x80008b80]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7f5 and fm1 == 0x2b954e52a4bff and fs2 == 1 and fe2 == 0x7f5 and fm2 == 0x2b954e52a4bff and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x800021e0]:fadd.d t5, t3, s10, dyn<br> [0x800021e4]:csrrs a7, fcsr, zero<br> [0x800021e8]:sw t5, 936(ra)<br> [0x800021ec]:sw t6, 944(ra)<br> [0x800021f0]:sw t5, 952(ra)<br> [0x800021f4]:sw a7, 960(ra)<br>                               |
| 106|[0x80008b88]<br>0x00000000<br> [0x80008ba0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbc366e555215f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xbc366e555215f and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002230]:fadd.d t5, t3, s10, dyn<br> [0x80002234]:csrrs a7, fcsr, zero<br> [0x80002238]:sw t5, 968(ra)<br> [0x8000223c]:sw t6, 976(ra)<br> [0x80002240]:sw t5, 984(ra)<br> [0x80002244]:sw a7, 992(ra)<br>                               |
| 107|[0x80008ba8]<br>0x00000000<br> [0x80008bc0]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbc366e555215f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xbc366e555215f and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80002280]:fadd.d t5, t3, s10, dyn<br> [0x80002284]:csrrs a7, fcsr, zero<br> [0x80002288]:sw t5, 1000(ra)<br> [0x8000228c]:sw t6, 1008(ra)<br> [0x80002290]:sw t5, 1016(ra)<br> [0x80002294]:sw a7, 1024(ra)<br>                           |
| 108|[0x80008bc8]<br>0x00000000<br> [0x80008be0]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbc366e555215f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xbc366e555215f and  fcsr == 0x40 and rm_val == 7   #nosat<br>                                                                                              |[0x800022d0]:fadd.d t5, t3, s10, dyn<br> [0x800022d4]:csrrs a7, fcsr, zero<br> [0x800022d8]:sw t5, 1032(ra)<br> [0x800022dc]:sw t6, 1040(ra)<br> [0x800022e0]:sw t5, 1048(ra)<br> [0x800022e4]:sw a7, 1056(ra)<br>                           |
| 109|[0x80008be8]<br>0x00000000<br> [0x80008c00]<br>0x00000060<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbc366e555215f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xbc366e555215f and  fcsr == 0x60 and rm_val == 7   #nosat<br>                                                                                              |[0x80002320]:fadd.d t5, t3, s10, dyn<br> [0x80002324]:csrrs a7, fcsr, zero<br> [0x80002328]:sw t5, 1064(ra)<br> [0x8000232c]:sw t6, 1072(ra)<br> [0x80002330]:sw t5, 1080(ra)<br> [0x80002334]:sw a7, 1088(ra)<br>                           |
| 110|[0x80008c08]<br>0x00000000<br> [0x80008c20]<br>0x00000080<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbc366e555215f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xbc366e555215f and  fcsr == 0x80 and rm_val == 7   #nosat<br>                                                                                              |[0x80002370]:fadd.d t5, t3, s10, dyn<br> [0x80002374]:csrrs a7, fcsr, zero<br> [0x80002378]:sw t5, 1096(ra)<br> [0x8000237c]:sw t6, 1104(ra)<br> [0x80002380]:sw t5, 1112(ra)<br> [0x80002384]:sw a7, 1120(ra)<br>                           |
| 111|[0x80008c28]<br>0x00000000<br> [0x80008c40]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x4d025f5a10f55 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x4d025f5a10f55 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800023c0]:fadd.d t5, t3, s10, dyn<br> [0x800023c4]:csrrs a7, fcsr, zero<br> [0x800023c8]:sw t5, 1128(ra)<br> [0x800023cc]:sw t6, 1136(ra)<br> [0x800023d0]:sw t5, 1144(ra)<br> [0x800023d4]:sw a7, 1152(ra)<br>                           |
| 112|[0x80008c48]<br>0x00000000<br> [0x80008c60]<br>0x00000020<br> |- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x4d025f5a10f55 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x4d025f5a10f55 and  fcsr == 0x20 and rm_val == 7   #nosat<br>                                                                                              |[0x80002410]:fadd.d t5, t3, s10, dyn<br> [0x80002414]:csrrs a7, fcsr, zero<br> [0x80002418]:sw t5, 1160(ra)<br> [0x8000241c]:sw t6, 1168(ra)<br> [0x80002420]:sw t5, 1176(ra)<br> [0x80002424]:sw a7, 1184(ra)<br>                           |
