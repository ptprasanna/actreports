
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80001060')]      |
| SIG_REGION                | [('0x80003510', '0x80003860', '212 words')]      |
| COV_LABELS                | fadd.d_b12      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-fadd/fadd.d_b12-01.S/ref.S    |
| Total Number of coverpoints| 101     |
| Total Coverpoints Hit     | 101      |
| Total Signature Updates   | 128      |
| STAT1                     | 32      |
| STAT2                     | 0      |
| STAT3                     | 20     |
| STAT4                     | 64     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000aa4]:fadd.d t5, t3, s10, dyn
[0x80000aa8]:csrrs a7, fcsr, zero
[0x80000aac]:sw t5, 672(ra)
[0x80000ab0]:sw t6, 680(ra)
[0x80000ab4]:sw t5, 688(ra)
[0x80000ab8]:sw a7, 696(ra)
[0x80000abc]:lw t3, 352(a6)
[0x80000ac0]:lw t4, 356(a6)
[0x80000ac4]:lw s10, 360(a6)
[0x80000ac8]:lw s11, 364(a6)
[0x80000acc]:lui t3, 193297
[0x80000ad0]:addi t3, t3, 1503
[0x80000ad4]:lui t4, 1048113
[0x80000ad8]:addi t4, t4, 1224
[0x80000adc]:lui s10, 411484
[0x80000ae0]:addi s10, s10, 3531
[0x80000ae4]:lui s11, 524022
[0x80000ae8]:addi s11, s11, 1508
[0x80000aec]:addi a4, zero, 0
[0x80000af0]:csrrw zero, fcsr, a4
[0x80000af4]:fadd.d t5, t3, s10, dyn
[0x80000af8]:csrrs a7, fcsr, zero

[0x80000af4]:fadd.d t5, t3, s10, dyn
[0x80000af8]:csrrs a7, fcsr, zero
[0x80000afc]:sw t5, 704(ra)
[0x80000b00]:sw t6, 712(ra)
[0x80000b04]:sw t5, 720(ra)
[0x80000b08]:sw a7, 728(ra)
[0x80000b0c]:lw t3, 368(a6)
[0x80000b10]:lw t4, 372(a6)
[0x80000b14]:lw s10, 376(a6)
[0x80000b18]:lw s11, 380(a6)
[0x80000b1c]:lui t3, 708479
[0x80000b20]:addi t3, t3, 2799
[0x80000b24]:lui t4, 1048103
[0x80000b28]:addi t4, t4, 3003
[0x80000b2c]:lui s10, 809795
[0x80000b30]:addi s10, s10, 772
[0x80000b34]:lui s11, 523963
[0x80000b38]:addi s11, s11, 1564
[0x80000b3c]:addi a4, zero, 0
[0x80000b40]:csrrw zero, fcsr, a4
[0x80000b44]:fadd.d t5, t3, s10, dyn
[0x80000b48]:csrrs a7, fcsr, zero

[0x80000b44]:fadd.d t5, t3, s10, dyn
[0x80000b48]:csrrs a7, fcsr, zero
[0x80000b4c]:sw t5, 736(ra)
[0x80000b50]:sw t6, 744(ra)
[0x80000b54]:sw t5, 752(ra)
[0x80000b58]:sw a7, 760(ra)
[0x80000b5c]:lw t3, 384(a6)
[0x80000b60]:lw t4, 388(a6)
[0x80000b64]:lw s10, 392(a6)
[0x80000b68]:lw s11, 396(a6)
[0x80000b6c]:lui t3, 643660
[0x80000b70]:addi t3, t3, 2763
[0x80000b74]:lui t4, 1047684
[0x80000b78]:addi t4, t4, 3577
[0x80000b7c]:lui s10, 224606
[0x80000b80]:addi s10, s10, 1135
[0x80000b84]:lui s11, 523649
[0x80000b88]:addi s11, s11, 605
[0x80000b8c]:addi a4, zero, 0
[0x80000b90]:csrrw zero, fcsr, a4
[0x80000b94]:fadd.d t5, t3, s10, dyn
[0x80000b98]:csrrs a7, fcsr, zero

[0x80000b94]:fadd.d t5, t3, s10, dyn
[0x80000b98]:csrrs a7, fcsr, zero
[0x80000b9c]:sw t5, 768(ra)
[0x80000ba0]:sw t6, 776(ra)
[0x80000ba4]:sw t5, 784(ra)
[0x80000ba8]:sw a7, 792(ra)
[0x80000bac]:lw t3, 400(a6)
[0x80000bb0]:lw t4, 404(a6)
[0x80000bb4]:lw s10, 408(a6)
[0x80000bb8]:lw s11, 412(a6)
[0x80000bbc]:lui t3, 523982
[0x80000bc0]:addi t3, t3, 2517
[0x80000bc4]:lui t4, 1048122
[0x80000bc8]:addi t4, t4, 3030
[0x80000bcc]:addi s10, zero, 0
[0x80000bd0]:lui s11, 524032
[0x80000bd4]:addi a4, zero, 0
[0x80000bd8]:csrrw zero, fcsr, a4
[0x80000bdc]:fadd.d t5, t3, s10, dyn
[0x80000be0]:csrrs a7, fcsr, zero

[0x80000bdc]:fadd.d t5, t3, s10, dyn
[0x80000be0]:csrrs a7, fcsr, zero
[0x80000be4]:sw t5, 800(ra)
[0x80000be8]:sw t6, 808(ra)
[0x80000bec]:sw t5, 816(ra)
[0x80000bf0]:sw a7, 824(ra)
[0x80000bf4]:lw t3, 416(a6)
[0x80000bf8]:lw t4, 420(a6)
[0x80000bfc]:lw s10, 424(a6)
[0x80000c00]:lw s11, 428(a6)
[0x80000c04]:lui t3, 887000
[0x80000c08]:addi t3, t3, 3215
[0x80000c0c]:lui t4, 1048040
[0x80000c10]:addi t4, t4, 3963
[0x80000c14]:lui s10, 274396
[0x80000c18]:addi s10, s10, 432
[0x80000c1c]:lui s11, 523810
[0x80000c20]:addi s11, s11, 3989
[0x80000c24]:addi a4, zero, 0
[0x80000c28]:csrrw zero, fcsr, a4
[0x80000c2c]:fadd.d t5, t3, s10, dyn
[0x80000c30]:csrrs a7, fcsr, zero

[0x80000c2c]:fadd.d t5, t3, s10, dyn
[0x80000c30]:csrrs a7, fcsr, zero
[0x80000c34]:sw t5, 832(ra)
[0x80000c38]:sw t6, 840(ra)
[0x80000c3c]:sw t5, 848(ra)
[0x80000c40]:sw a7, 856(ra)
[0x80000c44]:lw t3, 432(a6)
[0x80000c48]:lw t4, 436(a6)
[0x80000c4c]:lw s10, 440(a6)
[0x80000c50]:lw s11, 444(a6)
[0x80000c54]:lui t3, 608342
[0x80000c58]:addi t3, t3, 2445
[0x80000c5c]:lui t4, 1048196
[0x80000c60]:addi t4, t4, 3658
[0x80000c64]:addi s10, zero, 0
[0x80000c68]:lui s11, 524032
[0x80000c6c]:addi a4, zero, 0
[0x80000c70]:csrrw zero, fcsr, a4
[0x80000c74]:fadd.d t5, t3, s10, dyn
[0x80000c78]:csrrs a7, fcsr, zero

[0x80000c74]:fadd.d t5, t3, s10, dyn
[0x80000c78]:csrrs a7, fcsr, zero
[0x80000c7c]:sw t5, 864(ra)
[0x80000c80]:sw t6, 872(ra)
[0x80000c84]:sw t5, 880(ra)
[0x80000c88]:sw a7, 888(ra)
[0x80000c8c]:lw t3, 448(a6)
[0x80000c90]:lw t4, 452(a6)
[0x80000c94]:lw s10, 456(a6)
[0x80000c98]:lw s11, 460(a6)
[0x80000c9c]:lui t3, 361081
[0x80000ca0]:addi t3, t3, 422
[0x80000ca4]:lui t4, 1048281
[0x80000ca8]:addi t4, t4, 3158
[0x80000cac]:addi s10, zero, 0
[0x80000cb0]:lui s11, 524032
[0x80000cb4]:addi a4, zero, 0
[0x80000cb8]:csrrw zero, fcsr, a4
[0x80000cbc]:fadd.d t5, t3, s10, dyn
[0x80000cc0]:csrrs a7, fcsr, zero

[0x80000cbc]:fadd.d t5, t3, s10, dyn
[0x80000cc0]:csrrs a7, fcsr, zero
[0x80000cc4]:sw t5, 896(ra)
[0x80000cc8]:sw t6, 904(ra)
[0x80000ccc]:sw t5, 912(ra)
[0x80000cd0]:sw a7, 920(ra)
[0x80000cd4]:lw t3, 464(a6)
[0x80000cd8]:lw t4, 468(a6)
[0x80000cdc]:lw s10, 472(a6)
[0x80000ce0]:lw s11, 476(a6)
[0x80000ce4]:lui t3, 584898
[0x80000ce8]:addi t3, t3, 2885
[0x80000cec]:lui t4, 1048268
[0x80000cf0]:addi t4, t4, 3541
[0x80000cf4]:addi s10, zero, 0
[0x80000cf8]:lui s11, 524032
[0x80000cfc]:addi a4, zero, 0
[0x80000d00]:csrrw zero, fcsr, a4
[0x80000d04]:fadd.d t5, t3, s10, dyn
[0x80000d08]:csrrs a7, fcsr, zero

[0x80000d04]:fadd.d t5, t3, s10, dyn
[0x80000d08]:csrrs a7, fcsr, zero
[0x80000d0c]:sw t5, 928(ra)
[0x80000d10]:sw t6, 936(ra)
[0x80000d14]:sw t5, 944(ra)
[0x80000d18]:sw a7, 952(ra)
[0x80000d1c]:lw t3, 480(a6)
[0x80000d20]:lw t4, 484(a6)
[0x80000d24]:lw s10, 488(a6)
[0x80000d28]:lw s11, 492(a6)
[0x80000d2c]:lui t3, 224190
[0x80000d30]:addi t3, t3, 1791
[0x80000d34]:lui t4, 1048085
[0x80000d38]:addi t4, t4, 3224
[0x80000d3c]:lui s10, 532830
[0x80000d40]:addi s10, s10, 86
[0x80000d44]:lui s11, 523997
[0x80000d48]:addi s11, s11, 535
[0x80000d4c]:addi a4, zero, 0
[0x80000d50]:csrrw zero, fcsr, a4
[0x80000d54]:fadd.d t5, t3, s10, dyn
[0x80000d58]:csrrs a7, fcsr, zero

[0x80000d54]:fadd.d t5, t3, s10, dyn
[0x80000d58]:csrrs a7, fcsr, zero
[0x80000d5c]:sw t5, 960(ra)
[0x80000d60]:sw t6, 968(ra)
[0x80000d64]:sw t5, 976(ra)
[0x80000d68]:sw a7, 984(ra)
[0x80000d6c]:lw t3, 496(a6)
[0x80000d70]:lw t4, 500(a6)
[0x80000d74]:lw s10, 504(a6)
[0x80000d78]:lw s11, 508(a6)
[0x80000d7c]:lui t3, 516499
[0x80000d80]:addi t3, t3, 2536
[0x80000d84]:lui t4, 1048169
[0x80000d88]:addi t4, t4, 430
[0x80000d8c]:lui s10, 148216
[0x80000d90]:addi s10, s10, 2571
[0x80000d94]:lui s11, 524022
[0x80000d98]:addi s11, s11, 941
[0x80000d9c]:addi a4, zero, 0
[0x80000da0]:csrrw zero, fcsr, a4
[0x80000da4]:fadd.d t5, t3, s10, dyn
[0x80000da8]:csrrs a7, fcsr, zero

[0x80000da4]:fadd.d t5, t3, s10, dyn
[0x80000da8]:csrrs a7, fcsr, zero
[0x80000dac]:sw t5, 992(ra)
[0x80000db0]:sw t6, 1000(ra)
[0x80000db4]:sw t5, 1008(ra)
[0x80000db8]:sw a7, 1016(ra)
[0x80000dbc]:lw t3, 512(a6)
[0x80000dc0]:lw t4, 516(a6)
[0x80000dc4]:lw s10, 520(a6)
[0x80000dc8]:lw s11, 524(a6)
[0x80000dcc]:lui t3, 806674
[0x80000dd0]:addi t3, t3, 1884
[0x80000dd4]:lui t4, 1048224
[0x80000dd8]:addi t4, t4, 2268
[0x80000ddc]:addi s10, zero, 0
[0x80000de0]:lui s11, 524032
[0x80000de4]:addi a4, zero, 0
[0x80000de8]:csrrw zero, fcsr, a4
[0x80000dec]:fadd.d t5, t3, s10, dyn
[0x80000df0]:csrrs a7, fcsr, zero

[0x80000dec]:fadd.d t5, t3, s10, dyn
[0x80000df0]:csrrs a7, fcsr, zero
[0x80000df4]:sw t5, 1024(ra)
[0x80000df8]:sw t6, 1032(ra)
[0x80000dfc]:sw t5, 1040(ra)
[0x80000e00]:sw a7, 1048(ra)
[0x80000e04]:lw t3, 528(a6)
[0x80000e08]:lw t4, 532(a6)
[0x80000e0c]:lw s10, 536(a6)
[0x80000e10]:lw s11, 540(a6)
[0x80000e14]:lui t3, 797565
[0x80000e18]:addi t3, t3, 2079
[0x80000e1c]:lui t4, 1048266
[0x80000e20]:addi t4, t4, 1064
[0x80000e24]:addi s10, zero, 0
[0x80000e28]:lui s11, 524032
[0x80000e2c]:addi a4, zero, 0
[0x80000e30]:csrrw zero, fcsr, a4
[0x80000e34]:fadd.d t5, t3, s10, dyn
[0x80000e38]:csrrs a7, fcsr, zero

[0x80000e34]:fadd.d t5, t3, s10, dyn
[0x80000e38]:csrrs a7, fcsr, zero
[0x80000e3c]:sw t5, 1056(ra)
[0x80000e40]:sw t6, 1064(ra)
[0x80000e44]:sw t5, 1072(ra)
[0x80000e48]:sw a7, 1080(ra)
[0x80000e4c]:lw t3, 544(a6)
[0x80000e50]:lw t4, 548(a6)
[0x80000e54]:lw s10, 552(a6)
[0x80000e58]:lw s11, 556(a6)
[0x80000e5c]:lui t3, 318168
[0x80000e60]:addi t3, t3, 3400
[0x80000e64]:lui t4, 1048294
[0x80000e68]:addi t4, t4, 1145
[0x80000e6c]:addi s10, zero, 0
[0x80000e70]:lui s11, 524032
[0x80000e74]:addi a4, zero, 0
[0x80000e78]:csrrw zero, fcsr, a4
[0x80000e7c]:fadd.d t5, t3, s10, dyn
[0x80000e80]:csrrs a7, fcsr, zero

[0x80000e7c]:fadd.d t5, t3, s10, dyn
[0x80000e80]:csrrs a7, fcsr, zero
[0x80000e84]:sw t5, 1088(ra)
[0x80000e88]:sw t6, 1096(ra)
[0x80000e8c]:sw t5, 1104(ra)
[0x80000e90]:sw a7, 1112(ra)
[0x80000e94]:lw t3, 560(a6)
[0x80000e98]:lw t4, 564(a6)
[0x80000e9c]:lw s10, 568(a6)
[0x80000ea0]:lw s11, 572(a6)
[0x80000ea4]:lui t3, 671503
[0x80000ea8]:addi t3, t3, 1357
[0x80000eac]:lui t4, 1048013
[0x80000eb0]:addi t4, t4, 1542
[0x80000eb4]:addi s10, zero, 0
[0x80000eb8]:lui s11, 524032
[0x80000ebc]:addi a4, zero, 0
[0x80000ec0]:csrrw zero, fcsr, a4
[0x80000ec4]:fadd.d t5, t3, s10, dyn
[0x80000ec8]:csrrs a7, fcsr, zero

[0x80000ec4]:fadd.d t5, t3, s10, dyn
[0x80000ec8]:csrrs a7, fcsr, zero
[0x80000ecc]:sw t5, 1120(ra)
[0x80000ed0]:sw t6, 1128(ra)
[0x80000ed4]:sw t5, 1136(ra)
[0x80000ed8]:sw a7, 1144(ra)
[0x80000edc]:lw t3, 576(a6)
[0x80000ee0]:lw t4, 580(a6)
[0x80000ee4]:lw s10, 584(a6)
[0x80000ee8]:lw s11, 588(a6)
[0x80000eec]:lui t3, 126160
[0x80000ef0]:addi t3, t3, 2026
[0x80000ef4]:lui t4, 1048318
[0x80000ef8]:addi t4, t4, 344
[0x80000efc]:addi s10, zero, 0
[0x80000f00]:lui s11, 524032
[0x80000f04]:addi a4, zero, 0
[0x80000f08]:csrrw zero, fcsr, a4
[0x80000f0c]:fadd.d t5, t3, s10, dyn
[0x80000f10]:csrrs a7, fcsr, zero

[0x80000f0c]:fadd.d t5, t3, s10, dyn
[0x80000f10]:csrrs a7, fcsr, zero
[0x80000f14]:sw t5, 1152(ra)
[0x80000f18]:sw t6, 1160(ra)
[0x80000f1c]:sw t5, 1168(ra)
[0x80000f20]:sw a7, 1176(ra)
[0x80000f24]:lw t3, 592(a6)
[0x80000f28]:lw t4, 596(a6)
[0x80000f2c]:lw s10, 600(a6)
[0x80000f30]:lw s11, 604(a6)
[0x80000f34]:lui t3, 300767
[0x80000f38]:addi t3, t3, 449
[0x80000f3c]:lui t4, 1048214
[0x80000f40]:addi t4, t4, 747
[0x80000f44]:lui s10, 1010593
[0x80000f48]:addi s10, s10, 3472
[0x80000f4c]:lui s11, 523968
[0x80000f50]:addi s11, s11, 1463
[0x80000f54]:addi a4, zero, 0
[0x80000f58]:csrrw zero, fcsr, a4
[0x80000f5c]:fadd.d t5, t3, s10, dyn
[0x80000f60]:csrrs a7, fcsr, zero

[0x80000f5c]:fadd.d t5, t3, s10, dyn
[0x80000f60]:csrrs a7, fcsr, zero
[0x80000f64]:sw t5, 1184(ra)
[0x80000f68]:sw t6, 1192(ra)
[0x80000f6c]:sw t5, 1200(ra)
[0x80000f70]:sw a7, 1208(ra)
[0x80000f74]:lw t3, 608(a6)
[0x80000f78]:lw t4, 612(a6)
[0x80000f7c]:lw s10, 616(a6)
[0x80000f80]:lw s11, 620(a6)
[0x80000f84]:lui t3, 329570
[0x80000f88]:addi t3, t3, 3645
[0x80000f8c]:lui t4, 1048122
[0x80000f90]:addi t4, t4, 3051
[0x80000f94]:addi s10, zero, 0
[0x80000f98]:lui s11, 524032
[0x80000f9c]:addi a4, zero, 0
[0x80000fa0]:csrrw zero, fcsr, a4
[0x80000fa4]:fadd.d t5, t3, s10, dyn
[0x80000fa8]:csrrs a7, fcsr, zero

[0x80000fa4]:fadd.d t5, t3, s10, dyn
[0x80000fa8]:csrrs a7, fcsr, zero
[0x80000fac]:sw t5, 1216(ra)
[0x80000fb0]:sw t6, 1224(ra)
[0x80000fb4]:sw t5, 1232(ra)
[0x80000fb8]:sw a7, 1240(ra)
[0x80000fbc]:lw t3, 624(a6)
[0x80000fc0]:lw t4, 628(a6)
[0x80000fc4]:lw s10, 632(a6)
[0x80000fc8]:lw s11, 636(a6)
[0x80000fcc]:lui t3, 1022120
[0x80000fd0]:addi t3, t3, 3517
[0x80000fd4]:lui t4, 1048240
[0x80000fd8]:addi t4, t4, 1408
[0x80000fdc]:addi s10, zero, 0
[0x80000fe0]:lui s11, 524032
[0x80000fe4]:addi a4, zero, 0
[0x80000fe8]:csrrw zero, fcsr, a4
[0x80000fec]:fadd.d t5, t3, s10, dyn
[0x80000ff0]:csrrs a7, fcsr, zero

[0x80000fec]:fadd.d t5, t3, s10, dyn
[0x80000ff0]:csrrs a7, fcsr, zero
[0x80000ff4]:sw t5, 1248(ra)
[0x80000ff8]:sw t6, 1256(ra)
[0x80000ffc]:sw t5, 1264(ra)
[0x80001000]:sw a7, 1272(ra)
[0x80001004]:lw t3, 640(a6)
[0x80001008]:lw t4, 644(a6)
[0x8000100c]:lw s10, 648(a6)
[0x80001010]:lw s11, 652(a6)
[0x80001014]:lui t3, 66863
[0x80001018]:addi t3, t3, 2423
[0x8000101c]:lui t4, 1047983
[0x80001020]:addi t4, t4, 2725
[0x80001024]:lui s10, 327777
[0x80001028]:addi s10, s10, 1934
[0x8000102c]:lui s11, 523868
[0x80001030]:addi s11, s11, 3678
[0x80001034]:addi a4, zero, 0
[0x80001038]:csrrw zero, fcsr, a4
[0x8000103c]:fadd.d t5, t3, s10, dyn
[0x80001040]:csrrs a7, fcsr, zero

[0x8000103c]:fadd.d t5, t3, s10, dyn
[0x80001040]:csrrs a7, fcsr, zero
[0x80001044]:sw t5, 1280(ra)
[0x80001048]:sw t6, 1288(ra)
[0x8000104c]:sw t5, 1296(ra)
[0x80001050]:sw a7, 1304(ra)
[0x80001054]:addi zero, zero, 0
[0x80001058]:addi zero, zero, 0
[0x8000105c]:addi zero, zero, 0



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fadd.d', 'rs1 : x30', 'rs2 : x30', 'rd : x30', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x8000014c]:fadd.d t5, t5, t5, dyn
	-[0x80000150]:csrrs tp, fcsr, zero
	-[0x80000154]:sw t5, 0(ra)
	-[0x80000158]:sw t6, 8(ra)
Current Store : [0x80000158] : sw t6, 8(ra) -- Store: [0x80003520]:0xFFEB0580




Last Coverpoint : ['mnemonic : fadd.d', 'rs1 : x30', 'rs2 : x30', 'rd : x30', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x8000014c]:fadd.d t5, t5, t5, dyn
	-[0x80000150]:csrrs tp, fcsr, zero
	-[0x80000154]:sw t5, 0(ra)
	-[0x80000158]:sw t6, 8(ra)
	-[0x8000015c]:sw t5, 16(ra)
Current Store : [0x8000015c] : sw t5, 16(ra) -- Store: [0x80003528]:0x00000000




Last Coverpoint : ['rs1 : x26', 'rs2 : x26', 'rd : x28', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x8000019c]:fadd.d t3, s10, s10, dyn
	-[0x800001a0]:csrrs tp, fcsr, zero
	-[0x800001a4]:sw t3, 32(ra)
	-[0x800001a8]:sw t4, 40(ra)
Current Store : [0x800001a8] : sw t4, 40(ra) -- Store: [0x80003540]:0xEEDBEADF




Last Coverpoint : ['rs1 : x26', 'rs2 : x26', 'rd : x28', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x8000019c]:fadd.d t3, s10, s10, dyn
	-[0x800001a0]:csrrs tp, fcsr, zero
	-[0x800001a4]:sw t3, 32(ra)
	-[0x800001a8]:sw t4, 40(ra)
	-[0x800001ac]:sw t3, 48(ra)
Current Store : [0x800001ac] : sw t3, 48(ra) -- Store: [0x80003548]:0x1052E977




Last Coverpoint : ['rs1 : x28', 'rs2 : x24', 'rd : x26', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 1 and fe1 == 0x7fe and fm1 == 0x05c5ccdf19706 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd51953d9ddca4 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001ec]:fadd.d s10, t3, s8, dyn
	-[0x800001f0]:csrrs tp, fcsr, zero
	-[0x800001f4]:sw s10, 64(ra)
	-[0x800001f8]:sw s11, 72(ra)
Current Store : [0x800001f8] : sw s11, 72(ra) -- Store: [0x80003560]:0xFFDAEAA5




Last Coverpoint : ['rs1 : x28', 'rs2 : x24', 'rd : x26', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 1 and fe1 == 0x7fe and fm1 == 0x05c5ccdf19706 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd51953d9ddca4 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001ec]:fadd.d s10, t3, s8, dyn
	-[0x800001f0]:csrrs tp, fcsr, zero
	-[0x800001f4]:sw s10, 64(ra)
	-[0x800001f8]:sw s11, 72(ra)
	-[0x800001fc]:sw s10, 80(ra)
Current Store : [0x800001fc] : sw s10, 80(ra) -- Store: [0x80003568]:0xDF588B3C




Last Coverpoint : ['rs1 : x24', 'rs2 : x28', 'rd : x24', 'rs1 == rd != rs2', 'fs1 == 1 and fe1 == 0x7fe and fm1 == 0x914e0c751c4f4 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000234]:fadd.d s8, s8, t3, dyn
	-[0x80000238]:csrrs tp, fcsr, zero
	-[0x8000023c]:sw s8, 96(ra)
	-[0x80000240]:sw s9, 104(ra)
Current Store : [0x80000240] : sw s9, 104(ra) -- Store: [0x80003580]:0xFFE914E0




Last Coverpoint : ['rs1 : x24', 'rs2 : x28', 'rd : x24', 'rs1 == rd != rs2', 'fs1 == 1 and fe1 == 0x7fe and fm1 == 0x914e0c751c4f4 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000234]:fadd.d s8, s8, t3, dyn
	-[0x80000238]:csrrs tp, fcsr, zero
	-[0x8000023c]:sw s8, 96(ra)
	-[0x80000240]:sw s9, 104(ra)
	-[0x80000244]:sw s8, 112(ra)
Current Store : [0x80000244] : sw s8, 112(ra) -- Store: [0x80003588]:0x00000000




Last Coverpoint : ['rs1 : x20', 'rs2 : x22', 'rd : x22', 'rs2 == rd != rs1', 'fs1 == 1 and fe1 == 0x7fd and fm1 == 0xe809082dd48fb and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000027c]:fadd.d s6, s4, s6, dyn
	-[0x80000280]:csrrs tp, fcsr, zero
	-[0x80000284]:sw s6, 128(ra)
	-[0x80000288]:sw s7, 136(ra)
Current Store : [0x80000288] : sw s7, 136(ra) -- Store: [0x800035a0]:0x7FF00000




Last Coverpoint : ['rs1 : x20', 'rs2 : x22', 'rd : x22', 'rs2 == rd != rs1', 'fs1 == 1 and fe1 == 0x7fd and fm1 == 0xe809082dd48fb and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000027c]:fadd.d s6, s4, s6, dyn
	-[0x80000280]:csrrs tp, fcsr, zero
	-[0x80000284]:sw s6, 128(ra)
	-[0x80000288]:sw s7, 136(ra)
	-[0x8000028c]:sw s6, 144(ra)
Current Store : [0x8000028c] : sw s6, 144(ra) -- Store: [0x800035a8]:0x00000000




Last Coverpoint : ['rs1 : x22', 'rs2 : x18', 'rd : x20', 'fs1 == 1 and fe1 == 0x7fe and fm1 == 0xd0f42c0dfaf72 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002c4]:fadd.d s4, s6, s2, dyn
	-[0x800002c8]:csrrs tp, fcsr, zero
	-[0x800002cc]:sw s4, 160(ra)
	-[0x800002d0]:sw s5, 168(ra)
Current Store : [0x800002d0] : sw s5, 168(ra) -- Store: [0x800035c0]:0xFFDE8090




Last Coverpoint : ['rs1 : x22', 'rs2 : x18', 'rd : x20', 'fs1 == 1 and fe1 == 0x7fe and fm1 == 0xd0f42c0dfaf72 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002c4]:fadd.d s4, s6, s2, dyn
	-[0x800002c8]:csrrs tp, fcsr, zero
	-[0x800002cc]:sw s4, 160(ra)
	-[0x800002d0]:sw s5, 168(ra)
	-[0x800002d4]:sw s4, 176(ra)
Current Store : [0x800002d4] : sw s4, 176(ra) -- Store: [0x800035c8]:0x00000000




Last Coverpoint : ['rs1 : x16', 'rs2 : x20', 'rd : x18', 'fs1 == 1 and fe1 == 0x7fd and fm1 == 0x209a1991e3307 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000030c]:fadd.d s2, a6, s4, dyn
	-[0x80000310]:csrrs tp, fcsr, zero
	-[0x80000314]:sw s2, 192(ra)
	-[0x80000318]:sw s3, 200(ra)
Current Store : [0x80000318] : sw s3, 200(ra) -- Store: [0x800035e0]:0x7FF00000




Last Coverpoint : ['rs1 : x16', 'rs2 : x20', 'rd : x18', 'fs1 == 1 and fe1 == 0x7fd and fm1 == 0x209a1991e3307 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000030c]:fadd.d s2, a6, s4, dyn
	-[0x80000310]:csrrs tp, fcsr, zero
	-[0x80000314]:sw s2, 192(ra)
	-[0x80000318]:sw s3, 200(ra)
	-[0x8000031c]:sw s2, 208(ra)
Current Store : [0x8000031c] : sw s2, 208(ra) -- Store: [0x800035e8]:0x00000000




Last Coverpoint : ['rs1 : x18', 'rs2 : x14', 'rd : x16', 'fs1 == 1 and fe1 == 0x7fe and fm1 == 0x3c9adc7329695 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xbcdd3a7258aa7 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000035c]:fadd.d a6, s2, a4, dyn
	-[0x80000360]:csrrs tp, fcsr, zero
	-[0x80000364]:sw a6, 224(ra)
	-[0x80000368]:sw a7, 232(ra)
Current Store : [0x80000368] : sw a7, 232(ra) -- Store: [0x80003600]:0xFFD209A1




Last Coverpoint : ['rs1 : x18', 'rs2 : x14', 'rd : x16', 'fs1 == 1 and fe1 == 0x7fe and fm1 == 0x3c9adc7329695 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xbcdd3a7258aa7 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000035c]:fadd.d a6, s2, a4, dyn
	-[0x80000360]:csrrs tp, fcsr, zero
	-[0x80000364]:sw a6, 224(ra)
	-[0x80000368]:sw a7, 232(ra)
	-[0x8000036c]:sw a6, 240(ra)
Current Store : [0x8000036c] : sw a6, 240(ra) -- Store: [0x80003608]:0xBFE5E824




Last Coverpoint : ['rs1 : x12', 'rs2 : x16', 'rd : x14', 'fs1 == 1 and fe1 == 0x7fe and fm1 == 0xd1ca42e21585b and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003a4]:fadd.d a4, a2, a6, dyn
	-[0x800003a8]:csrrs tp, fcsr, zero
	-[0x800003ac]:sw a4, 256(ra)
	-[0x800003b0]:sw a5, 264(ra)
Current Store : [0x800003b0] : sw a5, 264(ra) -- Store: [0x80003620]:0x7FEBCDD3




Last Coverpoint : ['rs1 : x12', 'rs2 : x16', 'rd : x14', 'fs1 == 1 and fe1 == 0x7fe and fm1 == 0xd1ca42e21585b and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003a4]:fadd.d a4, a2, a6, dyn
	-[0x800003a8]:csrrs tp, fcsr, zero
	-[0x800003ac]:sw a4, 256(ra)
	-[0x800003b0]:sw a5, 264(ra)
	-[0x800003b4]:sw a4, 272(ra)
Current Store : [0x800003b4] : sw a4, 272(ra) -- Store: [0x80003628]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rs2 : x10', 'rd : x12', 'fs1 == 1 and fe1 == 0x7fe and fm1 == 0x9ed4cb2685903 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003f4]:fadd.d a2, a4, a0, dyn
	-[0x800003f8]:csrrs a7, fcsr, zero
	-[0x800003fc]:sw a2, 288(ra)
	-[0x80000400]:sw a3, 296(ra)
Current Store : [0x80000400] : sw a3, 296(ra) -- Store: [0x80003640]:0xFFED1CA4




Last Coverpoint : ['rs1 : x14', 'rs2 : x10', 'rd : x12', 'fs1 == 1 and fe1 == 0x7fe and fm1 == 0x9ed4cb2685903 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003f4]:fadd.d a2, a4, a0, dyn
	-[0x800003f8]:csrrs a7, fcsr, zero
	-[0x800003fc]:sw a2, 288(ra)
	-[0x80000400]:sw a3, 296(ra)
	-[0x80000404]:sw a2, 304(ra)
Current Store : [0x80000404] : sw a2, 304(ra) -- Store: [0x80003648]:0x00000000




Last Coverpoint : ['rs1 : x8', 'rs2 : x12', 'rd : x10', 'fs1 == 1 and fe1 == 0x7fd and fm1 == 0x3d97530ca446d and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000043c]:fadd.d a0, fp, a2, dyn
	-[0x80000440]:csrrs a7, fcsr, zero
	-[0x80000444]:sw a0, 320(ra)
	-[0x80000448]:sw a1, 328(ra)
Current Store : [0x80000448] : sw a1, 328(ra) -- Store: [0x80003660]:0x7FF00000




Last Coverpoint : ['rs1 : x8', 'rs2 : x12', 'rd : x10', 'fs1 == 1 and fe1 == 0x7fd and fm1 == 0x3d97530ca446d and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000043c]:fadd.d a0, fp, a2, dyn
	-[0x80000440]:csrrs a7, fcsr, zero
	-[0x80000444]:sw a0, 320(ra)
	-[0x80000448]:sw a1, 328(ra)
	-[0x8000044c]:sw a0, 336(ra)
Current Store : [0x8000044c] : sw a0, 336(ra) -- Store: [0x80003668]:0x00000000




Last Coverpoint : ['rs1 : x10', 'rs2 : x6', 'rd : x8', 'fs1 == 1 and fe1 == 0x7fe and fm1 == 0xcc3488366e29b and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000048c]:fadd.d fp, a0, t1, dyn
	-[0x80000490]:csrrs a7, fcsr, zero
	-[0x80000494]:sw fp, 0(ra)
	-[0x80000498]:sw s1, 8(ra)
Current Store : [0x80000498] : sw s1, 8(ra) -- Store: [0x800035d0]:0xFFD3D975




Last Coverpoint : ['rs1 : x10', 'rs2 : x6', 'rd : x8', 'fs1 == 1 and fe1 == 0x7fe and fm1 == 0xcc3488366e29b and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000048c]:fadd.d fp, a0, t1, dyn
	-[0x80000490]:csrrs a7, fcsr, zero
	-[0x80000494]:sw fp, 0(ra)
	-[0x80000498]:sw s1, 8(ra)
	-[0x8000049c]:sw fp, 16(ra)
Current Store : [0x8000049c] : sw fp, 16(ra) -- Store: [0x800035d8]:0x00000000




Last Coverpoint : ['rs1 : x4', 'rs2 : x8', 'rd : x6', 'fs1 == 1 and fe1 == 0x7fd and fm1 == 0xe3796147a7f97 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x254bcc7a78811 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004dc]:fadd.d t1, tp, fp, dyn
	-[0x800004e0]:csrrs a7, fcsr, zero
	-[0x800004e4]:sw t1, 32(ra)
	-[0x800004e8]:sw t2, 40(ra)
Current Store : [0x800004e8] : sw t2, 40(ra) -- Store: [0x800035f0]:0x7FF00000




Last Coverpoint : ['rs1 : x4', 'rs2 : x8', 'rd : x6', 'fs1 == 1 and fe1 == 0x7fd and fm1 == 0xe3796147a7f97 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x254bcc7a78811 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004dc]:fadd.d t1, tp, fp, dyn
	-[0x800004e0]:csrrs a7, fcsr, zero
	-[0x800004e4]:sw t1, 32(ra)
	-[0x800004e8]:sw t2, 40(ra)
	-[0x800004ec]:sw t1, 48(ra)
Current Store : [0x800004ec] : sw t1, 48(ra) -- Store: [0x800035f8]:0xEB52422C




Last Coverpoint : ['rs1 : x6', 'rs2 : x2', 'rd : x4', 'fs1 == 1 and fe1 == 0x7fd and fm1 == 0xbc978aa879221 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000524]:fadd.d tp, t1, sp, dyn
	-[0x80000528]:csrrs a7, fcsr, zero
	-[0x8000052c]:sw tp, 64(ra)
	-[0x80000530]:sw t0, 72(ra)
Current Store : [0x80000530] : sw t0, 72(ra) -- Store: [0x80003610]:0xFFDE3796




Last Coverpoint : ['rs1 : x6', 'rs2 : x2', 'rd : x4', 'fs1 == 1 and fe1 == 0x7fd and fm1 == 0xbc978aa879221 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000524]:fadd.d tp, t1, sp, dyn
	-[0x80000528]:csrrs a7, fcsr, zero
	-[0x8000052c]:sw tp, 64(ra)
	-[0x80000530]:sw t0, 72(ra)
	-[0x80000534]:sw tp, 80(ra)
Current Store : [0x80000534] : sw tp, 80(ra) -- Store: [0x80003618]:0x00000000




Last Coverpoint : ['rs1 : x2', 'fs1 == 1 and fe1 == 0x7fe and fm1 == 0xd3762f4d1629c and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000056c]:fadd.d t5, sp, t3, dyn
	-[0x80000570]:csrrs a7, fcsr, zero
	-[0x80000574]:sw t5, 96(ra)
	-[0x80000578]:sw t6, 104(ra)
Current Store : [0x80000578] : sw t6, 104(ra) -- Store: [0x80003630]:0xFFEB0580




Last Coverpoint : ['rs1 : x2', 'fs1 == 1 and fe1 == 0x7fe and fm1 == 0xd3762f4d1629c and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000056c]:fadd.d t5, sp, t3, dyn
	-[0x80000570]:csrrs a7, fcsr, zero
	-[0x80000574]:sw t5, 96(ra)
	-[0x80000578]:sw t6, 104(ra)
	-[0x8000057c]:sw t5, 112(ra)
Current Store : [0x8000057c] : sw t5, 112(ra) -- Store: [0x80003638]:0x00000000




Last Coverpoint : ['rs2 : x4', 'fs1 == 1 and fe1 == 0x7fd and fm1 == 0xe8754038aa2cf and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005b4]:fadd.d t5, t3, tp, dyn
	-[0x800005b8]:csrrs a7, fcsr, zero
	-[0x800005bc]:sw t5, 128(ra)
	-[0x800005c0]:sw t6, 136(ra)
Current Store : [0x800005c0] : sw t6, 136(ra) -- Store: [0x80003650]:0xFFEB0580




Last Coverpoint : ['rs2 : x4', 'fs1 == 1 and fe1 == 0x7fd and fm1 == 0xe8754038aa2cf and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005b4]:fadd.d t5, t3, tp, dyn
	-[0x800005b8]:csrrs a7, fcsr, zero
	-[0x800005bc]:sw t5, 128(ra)
	-[0x800005c0]:sw t6, 136(ra)
	-[0x800005c4]:sw t5, 144(ra)
Current Store : [0x800005c4] : sw t5, 144(ra) -- Store: [0x80003658]:0x00000000




Last Coverpoint : ['rd : x2', 'fs1 == 1 and fe1 == 0x7fd and fm1 == 0x0abe7f07f8c6f and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005fc]:fadd.d sp, t5, t3, dyn
	-[0x80000600]:csrrs a7, fcsr, zero
	-[0x80000604]:sw sp, 160(ra)
	-[0x80000608]:sw gp, 168(ra)
Current Store : [0x80000608] : sw gp, 168(ra) -- Store: [0x80003670]:0xFFED3762




Last Coverpoint : ['rd : x2', 'fs1 == 1 and fe1 == 0x7fd and fm1 == 0x0abe7f07f8c6f and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005fc]:fadd.d sp, t5, t3, dyn
	-[0x80000600]:csrrs a7, fcsr, zero
	-[0x80000604]:sw sp, 160(ra)
	-[0x80000608]:sw gp, 168(ra)
	-[0x8000060c]:sw sp, 176(ra)
Current Store : [0x8000060c] : sw sp, 176(ra) -- Store: [0x80003678]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0x18ef1d7a9fa74 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x201f96c097d1c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000064c]:fadd.d t5, t3, s10, dyn
	-[0x80000650]:csrrs a7, fcsr, zero
	-[0x80000654]:sw t5, 192(ra)
	-[0x80000658]:sw t6, 200(ra)
Current Store : [0x80000658] : sw t6, 200(ra) -- Store: [0x80003690]:0xFFD0ABE7




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0x18ef1d7a9fa74 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x201f96c097d1c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000064c]:fadd.d t5, t3, s10, dyn
	-[0x80000650]:csrrs a7, fcsr, zero
	-[0x80000654]:sw t5, 192(ra)
	-[0x80000658]:sw t6, 200(ra)
	-[0x8000065c]:sw t5, 208(ra)
Current Store : [0x8000065c] : sw t5, 208(ra) -- Store: [0x80003698]:0x17E0AA00




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0x707d21f5c40de and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000694]:fadd.d t5, t3, s10, dyn
	-[0x80000698]:csrrs a7, fcsr, zero
	-[0x8000069c]:sw t5, 224(ra)
	-[0x800006a0]:sw t6, 232(ra)
Current Store : [0x800006a0] : sw t6, 232(ra) -- Store: [0x800036b0]:0xFFD0ABE7




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0x707d21f5c40de and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000694]:fadd.d t5, t3, s10, dyn
	-[0x80000698]:csrrs a7, fcsr, zero
	-[0x8000069c]:sw t5, 224(ra)
	-[0x800006a0]:sw t6, 232(ra)
	-[0x800006a4]:sw t5, 240(ra)
Current Store : [0x800006a4] : sw t5, 240(ra) -- Store: [0x800036b8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xa65214b23e38e and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006dc]:fadd.d t5, t3, s10, dyn
	-[0x800006e0]:csrrs a7, fcsr, zero
	-[0x800006e4]:sw t5, 256(ra)
	-[0x800006e8]:sw t6, 264(ra)
Current Store : [0x800006e8] : sw t6, 264(ra) -- Store: [0x800036d0]:0xFFD0ABE7




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xa65214b23e38e and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006dc]:fadd.d t5, t3, s10, dyn
	-[0x800006e0]:csrrs a7, fcsr, zero
	-[0x800006e4]:sw t5, 256(ra)
	-[0x800006e8]:sw t6, 264(ra)
	-[0x800006ec]:sw t5, 272(ra)
Current Store : [0x800006ec] : sw t5, 272(ra) -- Store: [0x800036d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f5 and fm1 == 0x2b954e52a4bff and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xfa980f38509ed and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000072c]:fadd.d t5, t3, s10, dyn
	-[0x80000730]:csrrs a7, fcsr, zero
	-[0x80000734]:sw t5, 288(ra)
	-[0x80000738]:sw t6, 296(ra)
Current Store : [0x80000738] : sw t6, 296(ra) -- Store: [0x800036f0]:0xFFD0ABE7




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f5 and fm1 == 0x2b954e52a4bff and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xfa980f38509ed and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000072c]:fadd.d t5, t3, s10, dyn
	-[0x80000730]:csrrs a7, fcsr, zero
	-[0x80000734]:sw t5, 288(ra)
	-[0x80000738]:sw t6, 296(ra)
	-[0x8000073c]:sw t5, 304(ra)
Current Store : [0x8000073c] : sw t5, 304(ra) -- Store: [0x800036f8]:0x9E9FDFA1




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xbc366e555215f and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000774]:fadd.d t5, t3, s10, dyn
	-[0x80000778]:csrrs a7, fcsr, zero
	-[0x8000077c]:sw t5, 320(ra)
	-[0x80000780]:sw t6, 328(ra)
Current Store : [0x80000780] : sw t6, 328(ra) -- Store: [0x80003710]:0xFFD0ABE7




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xbc366e555215f and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000774]:fadd.d t5, t3, s10, dyn
	-[0x80000778]:csrrs a7, fcsr, zero
	-[0x8000077c]:sw t5, 320(ra)
	-[0x80000780]:sw t6, 328(ra)
	-[0x80000784]:sw t5, 336(ra)
Current Store : [0x80000784] : sw t5, 336(ra) -- Store: [0x80003718]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fd and fm1 == 0x4d025f5a10f55 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007bc]:fadd.d t5, t3, s10, dyn
	-[0x800007c0]:csrrs a7, fcsr, zero
	-[0x800007c4]:sw t5, 352(ra)
	-[0x800007c8]:sw t6, 360(ra)
Current Store : [0x800007c8] : sw t6, 360(ra) -- Store: [0x80003730]:0xFFD0ABE7




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fd and fm1 == 0x4d025f5a10f55 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007bc]:fadd.d t5, t3, s10, dyn
	-[0x800007c0]:csrrs a7, fcsr, zero
	-[0x800007c4]:sw t5, 352(ra)
	-[0x800007c8]:sw t6, 360(ra)
	-[0x800007cc]:sw t5, 368(ra)
Current Store : [0x800007cc] : sw t5, 368(ra) -- Store: [0x80003738]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fc and fm1 == 0x874e2eeac1c13 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x84645048e0d5c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000080c]:fadd.d t5, t3, s10, dyn
	-[0x80000810]:csrrs a7, fcsr, zero
	-[0x80000814]:sw t5, 384(ra)
	-[0x80000818]:sw t6, 392(ra)
Current Store : [0x80000818] : sw t6, 392(ra) -- Store: [0x80003750]:0xFFD0ABE7




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fc and fm1 == 0x874e2eeac1c13 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x84645048e0d5c and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000080c]:fadd.d t5, t3, s10, dyn
	-[0x80000810]:csrrs a7, fcsr, zero
	-[0x80000814]:sw t5, 384(ra)
	-[0x80000818]:sw t6, 392(ra)
	-[0x8000081c]:sw t5, 400(ra)
Current Store : [0x8000081c] : sw t5, 400(ra) -- Store: [0x80003758]:0x48E30657




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fc and fm1 == 0xe8af77cda8053 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000854]:fadd.d t5, t3, s10, dyn
	-[0x80000858]:csrrs a7, fcsr, zero
	-[0x8000085c]:sw t5, 416(ra)
	-[0x80000860]:sw t6, 424(ra)
Current Store : [0x80000860] : sw t6, 424(ra) -- Store: [0x80003770]:0xFFD0ABE7




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fc and fm1 == 0xe8af77cda8053 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000854]:fadd.d t5, t3, s10, dyn
	-[0x80000858]:csrrs a7, fcsr, zero
	-[0x8000085c]:sw t5, 416(ra)
	-[0x80000860]:sw t6, 424(ra)
	-[0x80000864]:sw t5, 432(ra)
Current Store : [0x80000864] : sw t5, 432(ra) -- Store: [0x80003778]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0x9b3a56e2c058e and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000089c]:fadd.d t5, t3, s10, dyn
	-[0x800008a0]:csrrs a7, fcsr, zero
	-[0x800008a4]:sw t5, 448(ra)
	-[0x800008a8]:sw t6, 456(ra)
Current Store : [0x800008a8] : sw t6, 456(ra) -- Store: [0x80003790]:0xFFD0ABE7




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0x9b3a56e2c058e and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000089c]:fadd.d t5, t3, s10, dyn
	-[0x800008a0]:csrrs a7, fcsr, zero
	-[0x800008a4]:sw t5, 448(ra)
	-[0x800008a8]:sw t6, 456(ra)
	-[0x800008ac]:sw t5, 464(ra)
Current Store : [0x800008ac] : sw t5, 464(ra) -- Store: [0x80003798]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fb and fm1 == 0x49818dfc8788f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x9a1cc86f24be5 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008ec]:fadd.d t5, t3, s10, dyn
	-[0x800008f0]:csrrs a7, fcsr, zero
	-[0x800008f4]:sw t5, 480(ra)
	-[0x800008f8]:sw t6, 488(ra)
Current Store : [0x800008f8] : sw t6, 488(ra) -- Store: [0x800037b0]:0xFFD0ABE7




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fb and fm1 == 0x49818dfc8788f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x9a1cc86f24be5 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008ec]:fadd.d t5, t3, s10, dyn
	-[0x800008f0]:csrrs a7, fcsr, zero
	-[0x800008f4]:sw t5, 480(ra)
	-[0x800008f8]:sw t6, 488(ra)
	-[0x800008fc]:sw t5, 496(ra)
Current Store : [0x800008fc] : sw t5, 496(ra) -- Store: [0x800037b8]:0x4F002DC1




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0x0410cbbfdec45 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000934]:fadd.d t5, t3, s10, dyn
	-[0x80000938]:csrrs a7, fcsr, zero
	-[0x8000093c]:sw t5, 512(ra)
	-[0x80000940]:sw t6, 520(ra)
Current Store : [0x80000940] : sw t6, 520(ra) -- Store: [0x800037d0]:0xFFD0ABE7




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0x0410cbbfdec45 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000934]:fadd.d t5, t3, s10, dyn
	-[0x80000938]:csrrs a7, fcsr, zero
	-[0x8000093c]:sw t5, 512(ra)
	-[0x80000940]:sw t6, 520(ra)
	-[0x80000944]:sw t5, 528(ra)
Current Store : [0x80000944] : sw t5, 528(ra) -- Store: [0x800037d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fb and fm1 == 0xbeb3709a573b7 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x52162165ec222 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000984]:fadd.d t5, t3, s10, dyn
	-[0x80000988]:csrrs a7, fcsr, zero
	-[0x8000098c]:sw t5, 544(ra)
	-[0x80000990]:sw t6, 552(ra)
Current Store : [0x80000990] : sw t6, 552(ra) -- Store: [0x800037f0]:0xFFD0ABE7




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fb and fm1 == 0xbeb3709a573b7 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x52162165ec222 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000984]:fadd.d t5, t3, s10, dyn
	-[0x80000988]:csrrs a7, fcsr, zero
	-[0x8000098c]:sw t5, 544(ra)
	-[0x80000990]:sw t6, 552(ra)
	-[0x80000994]:sw t5, 560(ra)
Current Store : [0x80000994] : sw t5, 560(ra) -- Store: [0x800037f8]:0x352A13AB




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0x69c26ac7fce60 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009cc]:fadd.d t5, t3, s10, dyn
	-[0x800009d0]:csrrs a7, fcsr, zero
	-[0x800009d4]:sw t5, 576(ra)
	-[0x800009d8]:sw t6, 584(ra)
Current Store : [0x800009d8] : sw t6, 584(ra) -- Store: [0x80003810]:0xFFD0ABE7




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0x69c26ac7fce60 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009cc]:fadd.d t5, t3, s10, dyn
	-[0x800009d0]:csrrs a7, fcsr, zero
	-[0x800009d4]:sw t5, 576(ra)
	-[0x800009d8]:sw t6, 584(ra)
	-[0x800009dc]:sw t5, 592(ra)
Current Store : [0x800009dc] : sw t5, 592(ra) -- Store: [0x80003818]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xa101ccfb0623a and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a14]:fadd.d t5, t3, s10, dyn
	-[0x80000a18]:csrrs a7, fcsr, zero
	-[0x80000a1c]:sw t5, 608(ra)
	-[0x80000a20]:sw t6, 616(ra)
Current Store : [0x80000a20] : sw t6, 616(ra) -- Store: [0x80003830]:0xFFD0ABE7




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xa101ccfb0623a and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a14]:fadd.d t5, t3, s10, dyn
	-[0x80000a18]:csrrs a7, fcsr, zero
	-[0x80000a1c]:sw t5, 608(ra)
	-[0x80000a20]:sw t6, 616(ra)
	-[0x80000a24]:sw t5, 624(ra)
Current Store : [0x80000a24] : sw t5, 624(ra) -- Store: [0x80003838]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xed7c3ef329d04 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a5c]:fadd.d t5, t3, s10, dyn
	-[0x80000a60]:csrrs a7, fcsr, zero
	-[0x80000a64]:sw t5, 640(ra)
	-[0x80000a68]:sw t6, 648(ra)
Current Store : [0x80000a68] : sw t6, 648(ra) -- Store: [0x80003850]:0xFFD0ABE7




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xed7c3ef329d04 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a5c]:fadd.d t5, t3, s10, dyn
	-[0x80000a60]:csrrs a7, fcsr, zero
	-[0x80000a64]:sw t5, 640(ra)
	-[0x80000a68]:sw t6, 648(ra)
	-[0x80000a6c]:sw t5, 656(ra)
Current Store : [0x80000a6c] : sw t5, 656(ra) -- Store: [0x80003858]:0x00000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                          signature                           |                                                                                                                        coverpoints                                                                                                                        |                                                                                                     code                                                                                                      |
|---:|--------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80003518]<br>0x00000000<br> [0x80003530]<br>0x00000005<br> |- mnemonic : fadd.d<br> - rs1 : x30<br> - rs2 : x30<br> - rd : x30<br> - rs1 == rs2 == rd<br>                                                                                                                                                              |[0x8000014c]:fadd.d t5, t5, t5, dyn<br> [0x80000150]:csrrs tp, fcsr, zero<br> [0x80000154]:sw t5, 0(ra)<br> [0x80000158]:sw t6, 8(ra)<br> [0x8000015c]:sw t5, 16(ra)<br> [0x80000160]:sw tp, 24(ra)<br>        |
|   2|[0x80003538]<br>0x1052E977<br> [0x80003550]<br>0x00000000<br> |- rs1 : x26<br> - rs2 : x26<br> - rd : x28<br> - rs1 == rs2 != rd<br>                                                                                                                                                                                      |[0x8000019c]:fadd.d t3, s10, s10, dyn<br> [0x800001a0]:csrrs tp, fcsr, zero<br> [0x800001a4]:sw t3, 32(ra)<br> [0x800001a8]:sw t4, 40(ra)<br> [0x800001ac]:sw t3, 48(ra)<br> [0x800001b0]:sw tp, 56(ra)<br>    |
|   3|[0x80003558]<br>0xDF588B3C<br> [0x80003570]<br>0x00000000<br> |- rs1 : x28<br> - rs2 : x24<br> - rd : x26<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0x05c5ccdf19706 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd51953d9ddca4 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x800001ec]:fadd.d s10, t3, s8, dyn<br> [0x800001f0]:csrrs tp, fcsr, zero<br> [0x800001f4]:sw s10, 64(ra)<br> [0x800001f8]:sw s11, 72(ra)<br> [0x800001fc]:sw s10, 80(ra)<br> [0x80000200]:sw tp, 88(ra)<br>  |
|   4|[0x80003578]<br>0x00000000<br> [0x80003590]<br>0x00000000<br> |- rs1 : x24<br> - rs2 : x28<br> - rd : x24<br> - rs1 == rd != rs2<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0x914e0c751c4f4 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                        |[0x80000234]:fadd.d s8, s8, t3, dyn<br> [0x80000238]:csrrs tp, fcsr, zero<br> [0x8000023c]:sw s8, 96(ra)<br> [0x80000240]:sw s9, 104(ra)<br> [0x80000244]:sw s8, 112(ra)<br> [0x80000248]:sw tp, 120(ra)<br>   |
|   5|[0x80003598]<br>0x00000000<br> [0x800035b0]<br>0x00000000<br> |- rs1 : x20<br> - rs2 : x22<br> - rd : x22<br> - rs2 == rd != rs1<br> - fs1 == 1 and fe1 == 0x7fd and fm1 == 0xe809082dd48fb and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                        |[0x8000027c]:fadd.d s6, s4, s6, dyn<br> [0x80000280]:csrrs tp, fcsr, zero<br> [0x80000284]:sw s6, 128(ra)<br> [0x80000288]:sw s7, 136(ra)<br> [0x8000028c]:sw s6, 144(ra)<br> [0x80000290]:sw tp, 152(ra)<br>  |
|   6|[0x800035b8]<br>0x00000000<br> [0x800035d0]<br>0x00000000<br> |- rs1 : x22<br> - rs2 : x18<br> - rd : x20<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0xd0f42c0dfaf72 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x800002c4]:fadd.d s4, s6, s2, dyn<br> [0x800002c8]:csrrs tp, fcsr, zero<br> [0x800002cc]:sw s4, 160(ra)<br> [0x800002d0]:sw s5, 168(ra)<br> [0x800002d4]:sw s4, 176(ra)<br> [0x800002d8]:sw tp, 184(ra)<br>  |
|   7|[0x800035d8]<br>0x00000000<br> [0x800035f0]<br>0x00000000<br> |- rs1 : x16<br> - rs2 : x20<br> - rd : x18<br> - fs1 == 1 and fe1 == 0x7fd and fm1 == 0x209a1991e3307 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x8000030c]:fadd.d s2, a6, s4, dyn<br> [0x80000310]:csrrs tp, fcsr, zero<br> [0x80000314]:sw s2, 192(ra)<br> [0x80000318]:sw s3, 200(ra)<br> [0x8000031c]:sw s2, 208(ra)<br> [0x80000320]:sw tp, 216(ra)<br>  |
|   8|[0x800035f8]<br>0xBFE5E824<br> [0x80003610]<br>0x00000000<br> |- rs1 : x18<br> - rs2 : x14<br> - rd : x16<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0x3c9adc7329695 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xbcdd3a7258aa7 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x8000035c]:fadd.d a6, s2, a4, dyn<br> [0x80000360]:csrrs tp, fcsr, zero<br> [0x80000364]:sw a6, 224(ra)<br> [0x80000368]:sw a7, 232(ra)<br> [0x8000036c]:sw a6, 240(ra)<br> [0x80000370]:sw tp, 248(ra)<br>  |
|   9|[0x80003618]<br>0x00000000<br> [0x80003630]<br>0x00000000<br> |- rs1 : x12<br> - rs2 : x16<br> - rd : x14<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0xd1ca42e21585b and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x800003a4]:fadd.d a4, a2, a6, dyn<br> [0x800003a8]:csrrs tp, fcsr, zero<br> [0x800003ac]:sw a4, 256(ra)<br> [0x800003b0]:sw a5, 264(ra)<br> [0x800003b4]:sw a4, 272(ra)<br> [0x800003b8]:sw tp, 280(ra)<br>  |
|  10|[0x80003638]<br>0x00000000<br> [0x80003650]<br>0x00000000<br> |- rs1 : x14<br> - rs2 : x10<br> - rd : x12<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0x9ed4cb2685903 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                               |[0x800003f4]:fadd.d a2, a4, a0, dyn<br> [0x800003f8]:csrrs a7, fcsr, zero<br> [0x800003fc]:sw a2, 288(ra)<br> [0x80000400]:sw a3, 296(ra)<br> [0x80000404]:sw a2, 304(ra)<br> [0x80000408]:sw a7, 312(ra)<br>  |
|  11|[0x80003658]<br>0x00000000<br> [0x80003670]<br>0x00000000<br> |- rs1 : x8<br> - rs2 : x12<br> - rd : x10<br> - fs1 == 1 and fe1 == 0x7fd and fm1 == 0x3d97530ca446d and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x8000043c]:fadd.d a0, fp, a2, dyn<br> [0x80000440]:csrrs a7, fcsr, zero<br> [0x80000444]:sw a0, 320(ra)<br> [0x80000448]:sw a1, 328(ra)<br> [0x8000044c]:sw a0, 336(ra)<br> [0x80000450]:sw a7, 344(ra)<br>  |
|  12|[0x800035c8]<br>0x00000000<br> [0x800035e0]<br>0x00000000<br> |- rs1 : x10<br> - rs2 : x6<br> - rd : x8<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0xcc3488366e29b and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                 |[0x8000048c]:fadd.d fp, a0, t1, dyn<br> [0x80000490]:csrrs a7, fcsr, zero<br> [0x80000494]:sw fp, 0(ra)<br> [0x80000498]:sw s1, 8(ra)<br> [0x8000049c]:sw fp, 16(ra)<br> [0x800004a0]:sw a7, 24(ra)<br>        |
|  13|[0x800035e8]<br>0xEB52422C<br> [0x80003600]<br>0x00000000<br> |- rs1 : x4<br> - rs2 : x8<br> - rd : x6<br> - fs1 == 1 and fe1 == 0x7fd and fm1 == 0xe3796147a7f97 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x254bcc7a78811 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                  |[0x800004dc]:fadd.d t1, tp, fp, dyn<br> [0x800004e0]:csrrs a7, fcsr, zero<br> [0x800004e4]:sw t1, 32(ra)<br> [0x800004e8]:sw t2, 40(ra)<br> [0x800004ec]:sw t1, 48(ra)<br> [0x800004f0]:sw a7, 56(ra)<br>      |
|  14|[0x80003608]<br>0x00000000<br> [0x80003620]<br>0x00000000<br> |- rs1 : x6<br> - rs2 : x2<br> - rd : x4<br> - fs1 == 1 and fe1 == 0x7fd and fm1 == 0xbc978aa879221 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                  |[0x80000524]:fadd.d tp, t1, sp, dyn<br> [0x80000528]:csrrs a7, fcsr, zero<br> [0x8000052c]:sw tp, 64(ra)<br> [0x80000530]:sw t0, 72(ra)<br> [0x80000534]:sw tp, 80(ra)<br> [0x80000538]:sw a7, 88(ra)<br>      |
|  15|[0x80003628]<br>0x00000000<br> [0x80003640]<br>0x00000000<br> |- rs1 : x2<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0xd3762f4d1629c and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                               |[0x8000056c]:fadd.d t5, sp, t3, dyn<br> [0x80000570]:csrrs a7, fcsr, zero<br> [0x80000574]:sw t5, 96(ra)<br> [0x80000578]:sw t6, 104(ra)<br> [0x8000057c]:sw t5, 112(ra)<br> [0x80000580]:sw a7, 120(ra)<br>   |
|  16|[0x80003648]<br>0x00000000<br> [0x80003660]<br>0x00000000<br> |- rs2 : x4<br> - fs1 == 1 and fe1 == 0x7fd and fm1 == 0xe8754038aa2cf and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                               |[0x800005b4]:fadd.d t5, t3, tp, dyn<br> [0x800005b8]:csrrs a7, fcsr, zero<br> [0x800005bc]:sw t5, 128(ra)<br> [0x800005c0]:sw t6, 136(ra)<br> [0x800005c4]:sw t5, 144(ra)<br> [0x800005c8]:sw a7, 152(ra)<br>  |
|  17|[0x80003668]<br>0x00000000<br> [0x80003680]<br>0x00000000<br> |- rd : x2<br> - fs1 == 1 and fe1 == 0x7fd and fm1 == 0x0abe7f07f8c6f and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                |[0x800005fc]:fadd.d sp, t5, t3, dyn<br> [0x80000600]:csrrs a7, fcsr, zero<br> [0x80000604]:sw sp, 160(ra)<br> [0x80000608]:sw gp, 168(ra)<br> [0x8000060c]:sw sp, 176(ra)<br> [0x80000610]:sw a7, 184(ra)<br>  |
|  18|[0x80003688]<br>0x17E0AA00<br> [0x800036a0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0x18ef1d7a9fa74 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x201f96c097d1c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x8000064c]:fadd.d t5, t3, s10, dyn<br> [0x80000650]:csrrs a7, fcsr, zero<br> [0x80000654]:sw t5, 192(ra)<br> [0x80000658]:sw t6, 200(ra)<br> [0x8000065c]:sw t5, 208(ra)<br> [0x80000660]:sw a7, 216(ra)<br> |
|  19|[0x800036a8]<br>0x00000000<br> [0x800036c0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0x707d21f5c40de and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000694]:fadd.d t5, t3, s10, dyn<br> [0x80000698]:csrrs a7, fcsr, zero<br> [0x8000069c]:sw t5, 224(ra)<br> [0x800006a0]:sw t6, 232(ra)<br> [0x800006a4]:sw t5, 240(ra)<br> [0x800006a8]:sw a7, 248(ra)<br> |
|  20|[0x800036c8]<br>0x00000000<br> [0x800036e0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xa65214b23e38e and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800006dc]:fadd.d t5, t3, s10, dyn<br> [0x800006e0]:csrrs a7, fcsr, zero<br> [0x800006e4]:sw t5, 256(ra)<br> [0x800006e8]:sw t6, 264(ra)<br> [0x800006ec]:sw t5, 272(ra)<br> [0x800006f0]:sw a7, 280(ra)<br> |
|  21|[0x800036e8]<br>0x9E9FDFA1<br> [0x80003700]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x7f5 and fm1 == 0x2b954e52a4bff and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xfa980f38509ed and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x8000072c]:fadd.d t5, t3, s10, dyn<br> [0x80000730]:csrrs a7, fcsr, zero<br> [0x80000734]:sw t5, 288(ra)<br> [0x80000738]:sw t6, 296(ra)<br> [0x8000073c]:sw t5, 304(ra)<br> [0x80000740]:sw a7, 312(ra)<br> |
|  22|[0x80003708]<br>0x00000000<br> [0x80003720]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xbc366e555215f and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000774]:fadd.d t5, t3, s10, dyn<br> [0x80000778]:csrrs a7, fcsr, zero<br> [0x8000077c]:sw t5, 320(ra)<br> [0x80000780]:sw t6, 328(ra)<br> [0x80000784]:sw t5, 336(ra)<br> [0x80000788]:sw a7, 344(ra)<br> |
|  23|[0x80003728]<br>0x00000000<br> [0x80003740]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fd and fm1 == 0x4d025f5a10f55 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800007bc]:fadd.d t5, t3, s10, dyn<br> [0x800007c0]:csrrs a7, fcsr, zero<br> [0x800007c4]:sw t5, 352(ra)<br> [0x800007c8]:sw t6, 360(ra)<br> [0x800007cc]:sw t5, 368(ra)<br> [0x800007d0]:sw a7, 376(ra)<br> |
|  24|[0x80003748]<br>0x48E30657<br> [0x80003760]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x7fc and fm1 == 0x874e2eeac1c13 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x84645048e0d5c and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x8000080c]:fadd.d t5, t3, s10, dyn<br> [0x80000810]:csrrs a7, fcsr, zero<br> [0x80000814]:sw t5, 384(ra)<br> [0x80000818]:sw t6, 392(ra)<br> [0x8000081c]:sw t5, 400(ra)<br> [0x80000820]:sw a7, 408(ra)<br> |
|  25|[0x80003768]<br>0x00000000<br> [0x80003780]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fc and fm1 == 0xe8af77cda8053 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000854]:fadd.d t5, t3, s10, dyn<br> [0x80000858]:csrrs a7, fcsr, zero<br> [0x8000085c]:sw t5, 416(ra)<br> [0x80000860]:sw t6, 424(ra)<br> [0x80000864]:sw t5, 432(ra)<br> [0x80000868]:sw a7, 440(ra)<br> |
|  26|[0x80003788]<br>0x00000000<br> [0x800037a0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0x9b3a56e2c058e and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x8000089c]:fadd.d t5, t3, s10, dyn<br> [0x800008a0]:csrrs a7, fcsr, zero<br> [0x800008a4]:sw t5, 448(ra)<br> [0x800008a8]:sw t6, 456(ra)<br> [0x800008ac]:sw t5, 464(ra)<br> [0x800008b0]:sw a7, 472(ra)<br> |
|  27|[0x800037a8]<br>0x4F002DC1<br> [0x800037c0]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x7fb and fm1 == 0x49818dfc8788f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x9a1cc86f24be5 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800008ec]:fadd.d t5, t3, s10, dyn<br> [0x800008f0]:csrrs a7, fcsr, zero<br> [0x800008f4]:sw t5, 480(ra)<br> [0x800008f8]:sw t6, 488(ra)<br> [0x800008fc]:sw t5, 496(ra)<br> [0x80000900]:sw a7, 504(ra)<br> |
|  28|[0x800037c8]<br>0x00000000<br> [0x800037e0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0x0410cbbfdec45 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000934]:fadd.d t5, t3, s10, dyn<br> [0x80000938]:csrrs a7, fcsr, zero<br> [0x8000093c]:sw t5, 512(ra)<br> [0x80000940]:sw t6, 520(ra)<br> [0x80000944]:sw t5, 528(ra)<br> [0x80000948]:sw a7, 536(ra)<br> |
|  29|[0x800037e8]<br>0x352A13AB<br> [0x80003800]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x7fb and fm1 == 0xbeb3709a573b7 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x52162165ec222 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000984]:fadd.d t5, t3, s10, dyn<br> [0x80000988]:csrrs a7, fcsr, zero<br> [0x8000098c]:sw t5, 544(ra)<br> [0x80000990]:sw t6, 552(ra)<br> [0x80000994]:sw t5, 560(ra)<br> [0x80000998]:sw a7, 568(ra)<br> |
|  30|[0x80003808]<br>0x00000000<br> [0x80003820]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0x69c26ac7fce60 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x800009cc]:fadd.d t5, t3, s10, dyn<br> [0x800009d0]:csrrs a7, fcsr, zero<br> [0x800009d4]:sw t5, 576(ra)<br> [0x800009d8]:sw t6, 584(ra)<br> [0x800009dc]:sw t5, 592(ra)<br> [0x800009e0]:sw a7, 600(ra)<br> |
|  31|[0x80003828]<br>0x00000000<br> [0x80003840]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xa101ccfb0623a and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000a14]:fadd.d t5, t3, s10, dyn<br> [0x80000a18]:csrrs a7, fcsr, zero<br> [0x80000a1c]:sw t5, 608(ra)<br> [0x80000a20]:sw t6, 616(ra)<br> [0x80000a24]:sw t5, 624(ra)<br> [0x80000a28]:sw a7, 632(ra)<br> |
|  32|[0x80003848]<br>0x00000000<br> [0x80003860]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xed7c3ef329d04 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                              |[0x80000a5c]:fadd.d t5, t3, s10, dyn<br> [0x80000a60]:csrrs a7, fcsr, zero<br> [0x80000a64]:sw t5, 640(ra)<br> [0x80000a68]:sw t6, 648(ra)<br> [0x80000a6c]:sw t5, 656(ra)<br> [0x80000a70]:sw a7, 664(ra)<br> |
