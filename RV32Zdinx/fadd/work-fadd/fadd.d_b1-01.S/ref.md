
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x8000d4e0')]      |
| SIG_REGION                | [('0x80011610', '0x80013a40', '2316 words')]      |
| COV_LABELS                | fadd.d_b1      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-fadd/fadd.d_b1-01.S/ref.S    |
| Total Number of coverpoints| 627     |
| Total Coverpoints Hit     | 627      |
| Total Signature Updates   | 1804      |
| STAT1                     | 451      |
| STAT2                     | 0      |
| STAT3                     | 126     |
| STAT4                     | 902     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x8000950c]:fadd.d t5, t3, s10, dyn
[0x80009510]:csrrs a7, fcsr, zero
[0x80009514]:sw t5, 912(ra)
[0x80009518]:sw t6, 920(ra)
[0x8000951c]:sw t5, 928(ra)
[0x80009520]:sw a7, 936(ra)
[0x80009524]:lui a4, 1
[0x80009528]:addi a4, a4, 2048
[0x8000952c]:add a6, a6, a4
[0x80009530]:lw t3, 464(a6)
[0x80009534]:sub a6, a6, a4
[0x80009538]:lui a4, 1
[0x8000953c]:addi a4, a4, 2048
[0x80009540]:add a6, a6, a4
[0x80009544]:lw t4, 468(a6)
[0x80009548]:sub a6, a6, a4
[0x8000954c]:lui a4, 1
[0x80009550]:addi a4, a4, 2048
[0x80009554]:add a6, a6, a4
[0x80009558]:lw s10, 472(a6)
[0x8000955c]:sub a6, a6, a4
[0x80009560]:lui a4, 1
[0x80009564]:addi a4, a4, 2048
[0x80009568]:add a6, a6, a4
[0x8000956c]:lw s11, 476(a6)
[0x80009570]:sub a6, a6, a4
[0x80009574]:addi t3, zero, 0
[0x80009578]:lui t4, 1048448
[0x8000957c]:addi s10, zero, 0
[0x80009580]:lui s11, 524160
[0x80009584]:addi a4, zero, 0
[0x80009588]:csrrw zero, fcsr, a4
[0x8000958c]:fadd.d t5, t3, s10, dyn
[0x80009590]:csrrs a7, fcsr, zero

[0x8000958c]:fadd.d t5, t3, s10, dyn
[0x80009590]:csrrs a7, fcsr, zero
[0x80009594]:sw t5, 944(ra)
[0x80009598]:sw t6, 952(ra)
[0x8000959c]:sw t5, 960(ra)
[0x800095a0]:sw a7, 968(ra)
[0x800095a4]:lui a4, 1
[0x800095a8]:addi a4, a4, 2048
[0x800095ac]:add a6, a6, a4
[0x800095b0]:lw t3, 480(a6)
[0x800095b4]:sub a6, a6, a4
[0x800095b8]:lui a4, 1
[0x800095bc]:addi a4, a4, 2048
[0x800095c0]:add a6, a6, a4
[0x800095c4]:lw t4, 484(a6)
[0x800095c8]:sub a6, a6, a4
[0x800095cc]:lui a4, 1
[0x800095d0]:addi a4, a4, 2048
[0x800095d4]:add a6, a6, a4
[0x800095d8]:lw s10, 488(a6)
[0x800095dc]:sub a6, a6, a4
[0x800095e0]:lui a4, 1
[0x800095e4]:addi a4, a4, 2048
[0x800095e8]:add a6, a6, a4
[0x800095ec]:lw s11, 492(a6)
[0x800095f0]:sub a6, a6, a4
[0x800095f4]:addi t3, zero, 0
[0x800095f8]:lui t4, 1048448
[0x800095fc]:addi s10, zero, 0
[0x80009600]:lui s11, 1048448
[0x80009604]:addi a4, zero, 0
[0x80009608]:csrrw zero, fcsr, a4
[0x8000960c]:fadd.d t5, t3, s10, dyn
[0x80009610]:csrrs a7, fcsr, zero

[0x8000960c]:fadd.d t5, t3, s10, dyn
[0x80009610]:csrrs a7, fcsr, zero
[0x80009614]:sw t5, 976(ra)
[0x80009618]:sw t6, 984(ra)
[0x8000961c]:sw t5, 992(ra)
[0x80009620]:sw a7, 1000(ra)
[0x80009624]:lui a4, 1
[0x80009628]:addi a4, a4, 2048
[0x8000962c]:add a6, a6, a4
[0x80009630]:lw t3, 496(a6)
[0x80009634]:sub a6, a6, a4
[0x80009638]:lui a4, 1
[0x8000963c]:addi a4, a4, 2048
[0x80009640]:add a6, a6, a4
[0x80009644]:lw t4, 500(a6)
[0x80009648]:sub a6, a6, a4
[0x8000964c]:lui a4, 1
[0x80009650]:addi a4, a4, 2048
[0x80009654]:add a6, a6, a4
[0x80009658]:lw s10, 504(a6)
[0x8000965c]:sub a6, a6, a4
[0x80009660]:lui a4, 1
[0x80009664]:addi a4, a4, 2048
[0x80009668]:add a6, a6, a4
[0x8000966c]:lw s11, 508(a6)
[0x80009670]:sub a6, a6, a4
[0x80009674]:addi t3, zero, 0
[0x80009678]:lui t4, 1048448
[0x8000967c]:addi s10, zero, 1
[0x80009680]:lui s11, 524160
[0x80009684]:addi a4, zero, 0
[0x80009688]:csrrw zero, fcsr, a4
[0x8000968c]:fadd.d t5, t3, s10, dyn
[0x80009690]:csrrs a7, fcsr, zero

[0x8000968c]:fadd.d t5, t3, s10, dyn
[0x80009690]:csrrs a7, fcsr, zero
[0x80009694]:sw t5, 1008(ra)
[0x80009698]:sw t6, 1016(ra)
[0x8000969c]:sw t5, 1024(ra)
[0x800096a0]:sw a7, 1032(ra)
[0x800096a4]:lui a4, 1
[0x800096a8]:addi a4, a4, 2048
[0x800096ac]:add a6, a6, a4
[0x800096b0]:lw t3, 512(a6)
[0x800096b4]:sub a6, a6, a4
[0x800096b8]:lui a4, 1
[0x800096bc]:addi a4, a4, 2048
[0x800096c0]:add a6, a6, a4
[0x800096c4]:lw t4, 516(a6)
[0x800096c8]:sub a6, a6, a4
[0x800096cc]:lui a4, 1
[0x800096d0]:addi a4, a4, 2048
[0x800096d4]:add a6, a6, a4
[0x800096d8]:lw s10, 520(a6)
[0x800096dc]:sub a6, a6, a4
[0x800096e0]:lui a4, 1
[0x800096e4]:addi a4, a4, 2048
[0x800096e8]:add a6, a6, a4
[0x800096ec]:lw s11, 524(a6)
[0x800096f0]:sub a6, a6, a4
[0x800096f4]:addi t3, zero, 0
[0x800096f8]:lui t4, 1048448
[0x800096fc]:addi s10, zero, 1
[0x80009700]:lui s11, 1048448
[0x80009704]:addi a4, zero, 0
[0x80009708]:csrrw zero, fcsr, a4
[0x8000970c]:fadd.d t5, t3, s10, dyn
[0x80009710]:csrrs a7, fcsr, zero

[0x8000970c]:fadd.d t5, t3, s10, dyn
[0x80009710]:csrrs a7, fcsr, zero
[0x80009714]:sw t5, 1040(ra)
[0x80009718]:sw t6, 1048(ra)
[0x8000971c]:sw t5, 1056(ra)
[0x80009720]:sw a7, 1064(ra)
[0x80009724]:lui a4, 1
[0x80009728]:addi a4, a4, 2048
[0x8000972c]:add a6, a6, a4
[0x80009730]:lw t3, 528(a6)
[0x80009734]:sub a6, a6, a4
[0x80009738]:lui a4, 1
[0x8000973c]:addi a4, a4, 2048
[0x80009740]:add a6, a6, a4
[0x80009744]:lw t4, 532(a6)
[0x80009748]:sub a6, a6, a4
[0x8000974c]:lui a4, 1
[0x80009750]:addi a4, a4, 2048
[0x80009754]:add a6, a6, a4
[0x80009758]:lw s10, 536(a6)
[0x8000975c]:sub a6, a6, a4
[0x80009760]:lui a4, 1
[0x80009764]:addi a4, a4, 2048
[0x80009768]:add a6, a6, a4
[0x8000976c]:lw s11, 540(a6)
[0x80009770]:sub a6, a6, a4
[0x80009774]:addi t3, zero, 0
[0x80009778]:lui t4, 1048448
[0x8000977c]:addi s10, zero, 1
[0x80009780]:lui s11, 524032
[0x80009784]:addi a4, zero, 0
[0x80009788]:csrrw zero, fcsr, a4
[0x8000978c]:fadd.d t5, t3, s10, dyn
[0x80009790]:csrrs a7, fcsr, zero

[0x8000978c]:fadd.d t5, t3, s10, dyn
[0x80009790]:csrrs a7, fcsr, zero
[0x80009794]:sw t5, 1072(ra)
[0x80009798]:sw t6, 1080(ra)
[0x8000979c]:sw t5, 1088(ra)
[0x800097a0]:sw a7, 1096(ra)
[0x800097a4]:lui a4, 1
[0x800097a8]:addi a4, a4, 2048
[0x800097ac]:add a6, a6, a4
[0x800097b0]:lw t3, 544(a6)
[0x800097b4]:sub a6, a6, a4
[0x800097b8]:lui a4, 1
[0x800097bc]:addi a4, a4, 2048
[0x800097c0]:add a6, a6, a4
[0x800097c4]:lw t4, 548(a6)
[0x800097c8]:sub a6, a6, a4
[0x800097cc]:lui a4, 1
[0x800097d0]:addi a4, a4, 2048
[0x800097d4]:add a6, a6, a4
[0x800097d8]:lw s10, 552(a6)
[0x800097dc]:sub a6, a6, a4
[0x800097e0]:lui a4, 1
[0x800097e4]:addi a4, a4, 2048
[0x800097e8]:add a6, a6, a4
[0x800097ec]:lw s11, 556(a6)
[0x800097f0]:sub a6, a6, a4
[0x800097f4]:addi t3, zero, 0
[0x800097f8]:lui t4, 1048448
[0x800097fc]:addi s10, zero, 1
[0x80009800]:lui s11, 1048320
[0x80009804]:addi a4, zero, 0
[0x80009808]:csrrw zero, fcsr, a4
[0x8000980c]:fadd.d t5, t3, s10, dyn
[0x80009810]:csrrs a7, fcsr, zero

[0x8000980c]:fadd.d t5, t3, s10, dyn
[0x80009810]:csrrs a7, fcsr, zero
[0x80009814]:sw t5, 1104(ra)
[0x80009818]:sw t6, 1112(ra)
[0x8000981c]:sw t5, 1120(ra)
[0x80009820]:sw a7, 1128(ra)
[0x80009824]:lui a4, 1
[0x80009828]:addi a4, a4, 2048
[0x8000982c]:add a6, a6, a4
[0x80009830]:lw t3, 560(a6)
[0x80009834]:sub a6, a6, a4
[0x80009838]:lui a4, 1
[0x8000983c]:addi a4, a4, 2048
[0x80009840]:add a6, a6, a4
[0x80009844]:lw t4, 564(a6)
[0x80009848]:sub a6, a6, a4
[0x8000984c]:lui a4, 1
[0x80009850]:addi a4, a4, 2048
[0x80009854]:add a6, a6, a4
[0x80009858]:lw s10, 568(a6)
[0x8000985c]:sub a6, a6, a4
[0x80009860]:lui a4, 1
[0x80009864]:addi a4, a4, 2048
[0x80009868]:add a6, a6, a4
[0x8000986c]:lw s11, 572(a6)
[0x80009870]:sub a6, a6, a4
[0x80009874]:addi t3, zero, 0
[0x80009878]:lui t4, 1048448
[0x8000987c]:addi s10, zero, 0
[0x80009880]:lui s11, 261888
[0x80009884]:addi a4, zero, 0
[0x80009888]:csrrw zero, fcsr, a4
[0x8000988c]:fadd.d t5, t3, s10, dyn
[0x80009890]:csrrs a7, fcsr, zero

[0x8000988c]:fadd.d t5, t3, s10, dyn
[0x80009890]:csrrs a7, fcsr, zero
[0x80009894]:sw t5, 1136(ra)
[0x80009898]:sw t6, 1144(ra)
[0x8000989c]:sw t5, 1152(ra)
[0x800098a0]:sw a7, 1160(ra)
[0x800098a4]:lui a4, 1
[0x800098a8]:addi a4, a4, 2048
[0x800098ac]:add a6, a6, a4
[0x800098b0]:lw t3, 576(a6)
[0x800098b4]:sub a6, a6, a4
[0x800098b8]:lui a4, 1
[0x800098bc]:addi a4, a4, 2048
[0x800098c0]:add a6, a6, a4
[0x800098c4]:lw t4, 580(a6)
[0x800098c8]:sub a6, a6, a4
[0x800098cc]:lui a4, 1
[0x800098d0]:addi a4, a4, 2048
[0x800098d4]:add a6, a6, a4
[0x800098d8]:lw s10, 584(a6)
[0x800098dc]:sub a6, a6, a4
[0x800098e0]:lui a4, 1
[0x800098e4]:addi a4, a4, 2048
[0x800098e8]:add a6, a6, a4
[0x800098ec]:lw s11, 588(a6)
[0x800098f0]:sub a6, a6, a4
[0x800098f4]:addi t3, zero, 0
[0x800098f8]:lui t4, 1048448
[0x800098fc]:addi s10, zero, 0
[0x80009900]:lui s11, 784384
[0x80009904]:addi a4, zero, 0
[0x80009908]:csrrw zero, fcsr, a4
[0x8000990c]:fadd.d t5, t3, s10, dyn
[0x80009910]:csrrs a7, fcsr, zero

[0x8000990c]:fadd.d t5, t3, s10, dyn
[0x80009910]:csrrs a7, fcsr, zero
[0x80009914]:sw t5, 1168(ra)
[0x80009918]:sw t6, 1176(ra)
[0x8000991c]:sw t5, 1184(ra)
[0x80009920]:sw a7, 1192(ra)
[0x80009924]:lui a4, 1
[0x80009928]:addi a4, a4, 2048
[0x8000992c]:add a6, a6, a4
[0x80009930]:lw t3, 592(a6)
[0x80009934]:sub a6, a6, a4
[0x80009938]:lui a4, 1
[0x8000993c]:addi a4, a4, 2048
[0x80009940]:add a6, a6, a4
[0x80009944]:lw t4, 596(a6)
[0x80009948]:sub a6, a6, a4
[0x8000994c]:lui a4, 1
[0x80009950]:addi a4, a4, 2048
[0x80009954]:add a6, a6, a4
[0x80009958]:lw s10, 600(a6)
[0x8000995c]:sub a6, a6, a4
[0x80009960]:lui a4, 1
[0x80009964]:addi a4, a4, 2048
[0x80009968]:add a6, a6, a4
[0x8000996c]:lw s11, 604(a6)
[0x80009970]:sub a6, a6, a4
[0x80009974]:addi t3, zero, 1
[0x80009978]:lui t4, 524160
[0x8000997c]:addi s10, zero, 0
[0x80009980]:addi s11, zero, 0
[0x80009984]:addi a4, zero, 0
[0x80009988]:csrrw zero, fcsr, a4
[0x8000998c]:fadd.d t5, t3, s10, dyn
[0x80009990]:csrrs a7, fcsr, zero

[0x8000998c]:fadd.d t5, t3, s10, dyn
[0x80009990]:csrrs a7, fcsr, zero
[0x80009994]:sw t5, 1200(ra)
[0x80009998]:sw t6, 1208(ra)
[0x8000999c]:sw t5, 1216(ra)
[0x800099a0]:sw a7, 1224(ra)
[0x800099a4]:lui a4, 1
[0x800099a8]:addi a4, a4, 2048
[0x800099ac]:add a6, a6, a4
[0x800099b0]:lw t3, 608(a6)
[0x800099b4]:sub a6, a6, a4
[0x800099b8]:lui a4, 1
[0x800099bc]:addi a4, a4, 2048
[0x800099c0]:add a6, a6, a4
[0x800099c4]:lw t4, 612(a6)
[0x800099c8]:sub a6, a6, a4
[0x800099cc]:lui a4, 1
[0x800099d0]:addi a4, a4, 2048
[0x800099d4]:add a6, a6, a4
[0x800099d8]:lw s10, 616(a6)
[0x800099dc]:sub a6, a6, a4
[0x800099e0]:lui a4, 1
[0x800099e4]:addi a4, a4, 2048
[0x800099e8]:add a6, a6, a4
[0x800099ec]:lw s11, 620(a6)
[0x800099f0]:sub a6, a6, a4
[0x800099f4]:addi t3, zero, 1
[0x800099f8]:lui t4, 524160
[0x800099fc]:addi s10, zero, 0
[0x80009a00]:lui s11, 524288
[0x80009a04]:addi a4, zero, 0
[0x80009a08]:csrrw zero, fcsr, a4
[0x80009a0c]:fadd.d t5, t3, s10, dyn
[0x80009a10]:csrrs a7, fcsr, zero

[0x80009a0c]:fadd.d t5, t3, s10, dyn
[0x80009a10]:csrrs a7, fcsr, zero
[0x80009a14]:sw t5, 1232(ra)
[0x80009a18]:sw t6, 1240(ra)
[0x80009a1c]:sw t5, 1248(ra)
[0x80009a20]:sw a7, 1256(ra)
[0x80009a24]:lui a4, 1
[0x80009a28]:addi a4, a4, 2048
[0x80009a2c]:add a6, a6, a4
[0x80009a30]:lw t3, 624(a6)
[0x80009a34]:sub a6, a6, a4
[0x80009a38]:lui a4, 1
[0x80009a3c]:addi a4, a4, 2048
[0x80009a40]:add a6, a6, a4
[0x80009a44]:lw t4, 628(a6)
[0x80009a48]:sub a6, a6, a4
[0x80009a4c]:lui a4, 1
[0x80009a50]:addi a4, a4, 2048
[0x80009a54]:add a6, a6, a4
[0x80009a58]:lw s10, 632(a6)
[0x80009a5c]:sub a6, a6, a4
[0x80009a60]:lui a4, 1
[0x80009a64]:addi a4, a4, 2048
[0x80009a68]:add a6, a6, a4
[0x80009a6c]:lw s11, 636(a6)
[0x80009a70]:sub a6, a6, a4
[0x80009a74]:addi t3, zero, 1
[0x80009a78]:lui t4, 524160
[0x80009a7c]:addi s10, zero, 1
[0x80009a80]:addi s11, zero, 0
[0x80009a84]:addi a4, zero, 0
[0x80009a88]:csrrw zero, fcsr, a4
[0x80009a8c]:fadd.d t5, t3, s10, dyn
[0x80009a90]:csrrs a7, fcsr, zero

[0x80009a8c]:fadd.d t5, t3, s10, dyn
[0x80009a90]:csrrs a7, fcsr, zero
[0x80009a94]:sw t5, 1264(ra)
[0x80009a98]:sw t6, 1272(ra)
[0x80009a9c]:sw t5, 1280(ra)
[0x80009aa0]:sw a7, 1288(ra)
[0x80009aa4]:lui a4, 1
[0x80009aa8]:addi a4, a4, 2048
[0x80009aac]:add a6, a6, a4
[0x80009ab0]:lw t3, 640(a6)
[0x80009ab4]:sub a6, a6, a4
[0x80009ab8]:lui a4, 1
[0x80009abc]:addi a4, a4, 2048
[0x80009ac0]:add a6, a6, a4
[0x80009ac4]:lw t4, 644(a6)
[0x80009ac8]:sub a6, a6, a4
[0x80009acc]:lui a4, 1
[0x80009ad0]:addi a4, a4, 2048
[0x80009ad4]:add a6, a6, a4
[0x80009ad8]:lw s10, 648(a6)
[0x80009adc]:sub a6, a6, a4
[0x80009ae0]:lui a4, 1
[0x80009ae4]:addi a4, a4, 2048
[0x80009ae8]:add a6, a6, a4
[0x80009aec]:lw s11, 652(a6)
[0x80009af0]:sub a6, a6, a4
[0x80009af4]:addi t3, zero, 1
[0x80009af8]:lui t4, 524160
[0x80009afc]:addi s10, zero, 1
[0x80009b00]:lui s11, 524288
[0x80009b04]:addi a4, zero, 0
[0x80009b08]:csrrw zero, fcsr, a4
[0x80009b0c]:fadd.d t5, t3, s10, dyn
[0x80009b10]:csrrs a7, fcsr, zero

[0x80009b0c]:fadd.d t5, t3, s10, dyn
[0x80009b10]:csrrs a7, fcsr, zero
[0x80009b14]:sw t5, 1296(ra)
[0x80009b18]:sw t6, 1304(ra)
[0x80009b1c]:sw t5, 1312(ra)
[0x80009b20]:sw a7, 1320(ra)
[0x80009b24]:lui a4, 1
[0x80009b28]:addi a4, a4, 2048
[0x80009b2c]:add a6, a6, a4
[0x80009b30]:lw t3, 656(a6)
[0x80009b34]:sub a6, a6, a4
[0x80009b38]:lui a4, 1
[0x80009b3c]:addi a4, a4, 2048
[0x80009b40]:add a6, a6, a4
[0x80009b44]:lw t4, 660(a6)
[0x80009b48]:sub a6, a6, a4
[0x80009b4c]:lui a4, 1
[0x80009b50]:addi a4, a4, 2048
[0x80009b54]:add a6, a6, a4
[0x80009b58]:lw s10, 664(a6)
[0x80009b5c]:sub a6, a6, a4
[0x80009b60]:lui a4, 1
[0x80009b64]:addi a4, a4, 2048
[0x80009b68]:add a6, a6, a4
[0x80009b6c]:lw s11, 668(a6)
[0x80009b70]:sub a6, a6, a4
[0x80009b74]:addi t3, zero, 1
[0x80009b78]:lui t4, 524160
[0x80009b7c]:addi s10, zero, 2
[0x80009b80]:addi s11, zero, 0
[0x80009b84]:addi a4, zero, 0
[0x80009b88]:csrrw zero, fcsr, a4
[0x80009b8c]:fadd.d t5, t3, s10, dyn
[0x80009b90]:csrrs a7, fcsr, zero

[0x80009b8c]:fadd.d t5, t3, s10, dyn
[0x80009b90]:csrrs a7, fcsr, zero
[0x80009b94]:sw t5, 1328(ra)
[0x80009b98]:sw t6, 1336(ra)
[0x80009b9c]:sw t5, 1344(ra)
[0x80009ba0]:sw a7, 1352(ra)
[0x80009ba4]:lui a4, 1
[0x80009ba8]:addi a4, a4, 2048
[0x80009bac]:add a6, a6, a4
[0x80009bb0]:lw t3, 672(a6)
[0x80009bb4]:sub a6, a6, a4
[0x80009bb8]:lui a4, 1
[0x80009bbc]:addi a4, a4, 2048
[0x80009bc0]:add a6, a6, a4
[0x80009bc4]:lw t4, 676(a6)
[0x80009bc8]:sub a6, a6, a4
[0x80009bcc]:lui a4, 1
[0x80009bd0]:addi a4, a4, 2048
[0x80009bd4]:add a6, a6, a4
[0x80009bd8]:lw s10, 680(a6)
[0x80009bdc]:sub a6, a6, a4
[0x80009be0]:lui a4, 1
[0x80009be4]:addi a4, a4, 2048
[0x80009be8]:add a6, a6, a4
[0x80009bec]:lw s11, 684(a6)
[0x80009bf0]:sub a6, a6, a4
[0x80009bf4]:addi t3, zero, 1
[0x80009bf8]:lui t4, 524160
[0x80009bfc]:addi s10, zero, 2
[0x80009c00]:lui s11, 524288
[0x80009c04]:addi a4, zero, 0
[0x80009c08]:csrrw zero, fcsr, a4
[0x80009c0c]:fadd.d t5, t3, s10, dyn
[0x80009c10]:csrrs a7, fcsr, zero

[0x80009c0c]:fadd.d t5, t3, s10, dyn
[0x80009c10]:csrrs a7, fcsr, zero
[0x80009c14]:sw t5, 1360(ra)
[0x80009c18]:sw t6, 1368(ra)
[0x80009c1c]:sw t5, 1376(ra)
[0x80009c20]:sw a7, 1384(ra)
[0x80009c24]:lui a4, 1
[0x80009c28]:addi a4, a4, 2048
[0x80009c2c]:add a6, a6, a4
[0x80009c30]:lw t3, 688(a6)
[0x80009c34]:sub a6, a6, a4
[0x80009c38]:lui a4, 1
[0x80009c3c]:addi a4, a4, 2048
[0x80009c40]:add a6, a6, a4
[0x80009c44]:lw t4, 692(a6)
[0x80009c48]:sub a6, a6, a4
[0x80009c4c]:lui a4, 1
[0x80009c50]:addi a4, a4, 2048
[0x80009c54]:add a6, a6, a4
[0x80009c58]:lw s10, 696(a6)
[0x80009c5c]:sub a6, a6, a4
[0x80009c60]:lui a4, 1
[0x80009c64]:addi a4, a4, 2048
[0x80009c68]:add a6, a6, a4
[0x80009c6c]:lw s11, 700(a6)
[0x80009c70]:sub a6, a6, a4
[0x80009c74]:addi t3, zero, 1
[0x80009c78]:lui t4, 524160
[0x80009c7c]:addi s10, zero, 4095
[0x80009c80]:lui s11, 256
[0x80009c84]:addi s11, s11, 4095
[0x80009c88]:addi a4, zero, 0
[0x80009c8c]:csrrw zero, fcsr, a4
[0x80009c90]:fadd.d t5, t3, s10, dyn
[0x80009c94]:csrrs a7, fcsr, zero

[0x80009c90]:fadd.d t5, t3, s10, dyn
[0x80009c94]:csrrs a7, fcsr, zero
[0x80009c98]:sw t5, 1392(ra)
[0x80009c9c]:sw t6, 1400(ra)
[0x80009ca0]:sw t5, 1408(ra)
[0x80009ca4]:sw a7, 1416(ra)
[0x80009ca8]:lui a4, 1
[0x80009cac]:addi a4, a4, 2048
[0x80009cb0]:add a6, a6, a4
[0x80009cb4]:lw t3, 704(a6)
[0x80009cb8]:sub a6, a6, a4
[0x80009cbc]:lui a4, 1
[0x80009cc0]:addi a4, a4, 2048
[0x80009cc4]:add a6, a6, a4
[0x80009cc8]:lw t4, 708(a6)
[0x80009ccc]:sub a6, a6, a4
[0x80009cd0]:lui a4, 1
[0x80009cd4]:addi a4, a4, 2048
[0x80009cd8]:add a6, a6, a4
[0x80009cdc]:lw s10, 712(a6)
[0x80009ce0]:sub a6, a6, a4
[0x80009ce4]:lui a4, 1
[0x80009ce8]:addi a4, a4, 2048
[0x80009cec]:add a6, a6, a4
[0x80009cf0]:lw s11, 716(a6)
[0x80009cf4]:sub a6, a6, a4
[0x80009cf8]:addi t3, zero, 1
[0x80009cfc]:lui t4, 524160
[0x80009d00]:addi s10, zero, 4095
[0x80009d04]:lui s11, 524544
[0x80009d08]:addi s11, s11, 4095
[0x80009d0c]:addi a4, zero, 0
[0x80009d10]:csrrw zero, fcsr, a4
[0x80009d14]:fadd.d t5, t3, s10, dyn
[0x80009d18]:csrrs a7, fcsr, zero

[0x80009d14]:fadd.d t5, t3, s10, dyn
[0x80009d18]:csrrs a7, fcsr, zero
[0x80009d1c]:sw t5, 1424(ra)
[0x80009d20]:sw t6, 1432(ra)
[0x80009d24]:sw t5, 1440(ra)
[0x80009d28]:sw a7, 1448(ra)
[0x80009d2c]:lui a4, 1
[0x80009d30]:addi a4, a4, 2048
[0x80009d34]:add a6, a6, a4
[0x80009d38]:lw t3, 720(a6)
[0x80009d3c]:sub a6, a6, a4
[0x80009d40]:lui a4, 1
[0x80009d44]:addi a4, a4, 2048
[0x80009d48]:add a6, a6, a4
[0x80009d4c]:lw t4, 724(a6)
[0x80009d50]:sub a6, a6, a4
[0x80009d54]:lui a4, 1
[0x80009d58]:addi a4, a4, 2048
[0x80009d5c]:add a6, a6, a4
[0x80009d60]:lw s10, 728(a6)
[0x80009d64]:sub a6, a6, a4
[0x80009d68]:lui a4, 1
[0x80009d6c]:addi a4, a4, 2048
[0x80009d70]:add a6, a6, a4
[0x80009d74]:lw s11, 732(a6)
[0x80009d78]:sub a6, a6, a4
[0x80009d7c]:addi t3, zero, 1
[0x80009d80]:lui t4, 524160
[0x80009d84]:addi s10, zero, 0
[0x80009d88]:lui s11, 256
[0x80009d8c]:addi a4, zero, 0
[0x80009d90]:csrrw zero, fcsr, a4
[0x80009d94]:fadd.d t5, t3, s10, dyn
[0x80009d98]:csrrs a7, fcsr, zero

[0x80009d94]:fadd.d t5, t3, s10, dyn
[0x80009d98]:csrrs a7, fcsr, zero
[0x80009d9c]:sw t5, 1456(ra)
[0x80009da0]:sw t6, 1464(ra)
[0x80009da4]:sw t5, 1472(ra)
[0x80009da8]:sw a7, 1480(ra)
[0x80009dac]:lui a4, 1
[0x80009db0]:addi a4, a4, 2048
[0x80009db4]:add a6, a6, a4
[0x80009db8]:lw t3, 736(a6)
[0x80009dbc]:sub a6, a6, a4
[0x80009dc0]:lui a4, 1
[0x80009dc4]:addi a4, a4, 2048
[0x80009dc8]:add a6, a6, a4
[0x80009dcc]:lw t4, 740(a6)
[0x80009dd0]:sub a6, a6, a4
[0x80009dd4]:lui a4, 1
[0x80009dd8]:addi a4, a4, 2048
[0x80009ddc]:add a6, a6, a4
[0x80009de0]:lw s10, 744(a6)
[0x80009de4]:sub a6, a6, a4
[0x80009de8]:lui a4, 1
[0x80009dec]:addi a4, a4, 2048
[0x80009df0]:add a6, a6, a4
[0x80009df4]:lw s11, 748(a6)
[0x80009df8]:sub a6, a6, a4
[0x80009dfc]:addi t3, zero, 1
[0x80009e00]:lui t4, 524160
[0x80009e04]:addi s10, zero, 0
[0x80009e08]:lui s11, 524544
[0x80009e0c]:addi a4, zero, 0
[0x80009e10]:csrrw zero, fcsr, a4
[0x80009e14]:fadd.d t5, t3, s10, dyn
[0x80009e18]:csrrs a7, fcsr, zero

[0x80009e14]:fadd.d t5, t3, s10, dyn
[0x80009e18]:csrrs a7, fcsr, zero
[0x80009e1c]:sw t5, 1488(ra)
[0x80009e20]:sw t6, 1496(ra)
[0x80009e24]:sw t5, 1504(ra)
[0x80009e28]:sw a7, 1512(ra)
[0x80009e2c]:lui a4, 1
[0x80009e30]:addi a4, a4, 2048
[0x80009e34]:add a6, a6, a4
[0x80009e38]:lw t3, 752(a6)
[0x80009e3c]:sub a6, a6, a4
[0x80009e40]:lui a4, 1
[0x80009e44]:addi a4, a4, 2048
[0x80009e48]:add a6, a6, a4
[0x80009e4c]:lw t4, 756(a6)
[0x80009e50]:sub a6, a6, a4
[0x80009e54]:lui a4, 1
[0x80009e58]:addi a4, a4, 2048
[0x80009e5c]:add a6, a6, a4
[0x80009e60]:lw s10, 760(a6)
[0x80009e64]:sub a6, a6, a4
[0x80009e68]:lui a4, 1
[0x80009e6c]:addi a4, a4, 2048
[0x80009e70]:add a6, a6, a4
[0x80009e74]:lw s11, 764(a6)
[0x80009e78]:sub a6, a6, a4
[0x80009e7c]:addi t3, zero, 1
[0x80009e80]:lui t4, 524160
[0x80009e84]:addi s10, zero, 2
[0x80009e88]:lui s11, 256
[0x80009e8c]:addi a4, zero, 0
[0x80009e90]:csrrw zero, fcsr, a4
[0x80009e94]:fadd.d t5, t3, s10, dyn
[0x80009e98]:csrrs a7, fcsr, zero

[0x80009e94]:fadd.d t5, t3, s10, dyn
[0x80009e98]:csrrs a7, fcsr, zero
[0x80009e9c]:sw t5, 1520(ra)
[0x80009ea0]:sw t6, 1528(ra)
[0x80009ea4]:sw t5, 1536(ra)
[0x80009ea8]:sw a7, 1544(ra)
[0x80009eac]:lui a4, 1
[0x80009eb0]:addi a4, a4, 2048
[0x80009eb4]:add a6, a6, a4
[0x80009eb8]:lw t3, 768(a6)
[0x80009ebc]:sub a6, a6, a4
[0x80009ec0]:lui a4, 1
[0x80009ec4]:addi a4, a4, 2048
[0x80009ec8]:add a6, a6, a4
[0x80009ecc]:lw t4, 772(a6)
[0x80009ed0]:sub a6, a6, a4
[0x80009ed4]:lui a4, 1
[0x80009ed8]:addi a4, a4, 2048
[0x80009edc]:add a6, a6, a4
[0x80009ee0]:lw s10, 776(a6)
[0x80009ee4]:sub a6, a6, a4
[0x80009ee8]:lui a4, 1
[0x80009eec]:addi a4, a4, 2048
[0x80009ef0]:add a6, a6, a4
[0x80009ef4]:lw s11, 780(a6)
[0x80009ef8]:sub a6, a6, a4
[0x80009efc]:addi t3, zero, 1
[0x80009f00]:lui t4, 524160
[0x80009f04]:addi s10, zero, 2
[0x80009f08]:lui s11, 524544
[0x80009f0c]:addi a4, zero, 0
[0x80009f10]:csrrw zero, fcsr, a4
[0x80009f14]:fadd.d t5, t3, s10, dyn
[0x80009f18]:csrrs a7, fcsr, zero

[0x80009f14]:fadd.d t5, t3, s10, dyn
[0x80009f18]:csrrs a7, fcsr, zero
[0x80009f1c]:sw t5, 1552(ra)
[0x80009f20]:sw t6, 1560(ra)
[0x80009f24]:sw t5, 1568(ra)
[0x80009f28]:sw a7, 1576(ra)
[0x80009f2c]:lui a4, 1
[0x80009f30]:addi a4, a4, 2048
[0x80009f34]:add a6, a6, a4
[0x80009f38]:lw t3, 784(a6)
[0x80009f3c]:sub a6, a6, a4
[0x80009f40]:lui a4, 1
[0x80009f44]:addi a4, a4, 2048
[0x80009f48]:add a6, a6, a4
[0x80009f4c]:lw t4, 788(a6)
[0x80009f50]:sub a6, a6, a4
[0x80009f54]:lui a4, 1
[0x80009f58]:addi a4, a4, 2048
[0x80009f5c]:add a6, a6, a4
[0x80009f60]:lw s10, 792(a6)
[0x80009f64]:sub a6, a6, a4
[0x80009f68]:lui a4, 1
[0x80009f6c]:addi a4, a4, 2048
[0x80009f70]:add a6, a6, a4
[0x80009f74]:lw s11, 796(a6)
[0x80009f78]:sub a6, a6, a4
[0x80009f7c]:addi t3, zero, 1
[0x80009f80]:lui t4, 524160
[0x80009f84]:addi s10, zero, 4095
[0x80009f88]:lui s11, 524032
[0x80009f8c]:addi s11, s11, 4095
[0x80009f90]:addi a4, zero, 0
[0x80009f94]:csrrw zero, fcsr, a4
[0x80009f98]:fadd.d t5, t3, s10, dyn
[0x80009f9c]:csrrs a7, fcsr, zero

[0x80009f98]:fadd.d t5, t3, s10, dyn
[0x80009f9c]:csrrs a7, fcsr, zero
[0x80009fa0]:sw t5, 1584(ra)
[0x80009fa4]:sw t6, 1592(ra)
[0x80009fa8]:sw t5, 1600(ra)
[0x80009fac]:sw a7, 1608(ra)
[0x80009fb0]:lui a4, 1
[0x80009fb4]:addi a4, a4, 2048
[0x80009fb8]:add a6, a6, a4
[0x80009fbc]:lw t3, 800(a6)
[0x80009fc0]:sub a6, a6, a4
[0x80009fc4]:lui a4, 1
[0x80009fc8]:addi a4, a4, 2048
[0x80009fcc]:add a6, a6, a4
[0x80009fd0]:lw t4, 804(a6)
[0x80009fd4]:sub a6, a6, a4
[0x80009fd8]:lui a4, 1
[0x80009fdc]:addi a4, a4, 2048
[0x80009fe0]:add a6, a6, a4
[0x80009fe4]:lw s10, 808(a6)
[0x80009fe8]:sub a6, a6, a4
[0x80009fec]:lui a4, 1
[0x80009ff0]:addi a4, a4, 2048
[0x80009ff4]:add a6, a6, a4
[0x80009ff8]:lw s11, 812(a6)
[0x80009ffc]:sub a6, a6, a4
[0x8000a000]:addi t3, zero, 1
[0x8000a004]:lui t4, 524160
[0x8000a008]:addi s10, zero, 4095
[0x8000a00c]:lui s11, 1048320
[0x8000a010]:addi s11, s11, 4095
[0x8000a014]:addi a4, zero, 0
[0x8000a018]:csrrw zero, fcsr, a4
[0x8000a01c]:fadd.d t5, t3, s10, dyn
[0x8000a020]:csrrs a7, fcsr, zero

[0x8000a01c]:fadd.d t5, t3, s10, dyn
[0x8000a020]:csrrs a7, fcsr, zero
[0x8000a024]:sw t5, 1616(ra)
[0x8000a028]:sw t6, 1624(ra)
[0x8000a02c]:sw t5, 1632(ra)
[0x8000a030]:sw a7, 1640(ra)
[0x8000a034]:lui a4, 1
[0x8000a038]:addi a4, a4, 2048
[0x8000a03c]:add a6, a6, a4
[0x8000a040]:lw t3, 816(a6)
[0x8000a044]:sub a6, a6, a4
[0x8000a048]:lui a4, 1
[0x8000a04c]:addi a4, a4, 2048
[0x8000a050]:add a6, a6, a4
[0x8000a054]:lw t4, 820(a6)
[0x8000a058]:sub a6, a6, a4
[0x8000a05c]:lui a4, 1
[0x8000a060]:addi a4, a4, 2048
[0x8000a064]:add a6, a6, a4
[0x8000a068]:lw s10, 824(a6)
[0x8000a06c]:sub a6, a6, a4
[0x8000a070]:lui a4, 1
[0x8000a074]:addi a4, a4, 2048
[0x8000a078]:add a6, a6, a4
[0x8000a07c]:lw s11, 828(a6)
[0x8000a080]:sub a6, a6, a4
[0x8000a084]:addi t3, zero, 1
[0x8000a088]:lui t4, 524160
[0x8000a08c]:addi s10, zero, 0
[0x8000a090]:lui s11, 524032
[0x8000a094]:addi a4, zero, 0
[0x8000a098]:csrrw zero, fcsr, a4
[0x8000a09c]:fadd.d t5, t3, s10, dyn
[0x8000a0a0]:csrrs a7, fcsr, zero

[0x8000a09c]:fadd.d t5, t3, s10, dyn
[0x8000a0a0]:csrrs a7, fcsr, zero
[0x8000a0a4]:sw t5, 1648(ra)
[0x8000a0a8]:sw t6, 1656(ra)
[0x8000a0ac]:sw t5, 1664(ra)
[0x8000a0b0]:sw a7, 1672(ra)
[0x8000a0b4]:lui a4, 1
[0x8000a0b8]:addi a4, a4, 2048
[0x8000a0bc]:add a6, a6, a4
[0x8000a0c0]:lw t3, 832(a6)
[0x8000a0c4]:sub a6, a6, a4
[0x8000a0c8]:lui a4, 1
[0x8000a0cc]:addi a4, a4, 2048
[0x8000a0d0]:add a6, a6, a4
[0x8000a0d4]:lw t4, 836(a6)
[0x8000a0d8]:sub a6, a6, a4
[0x8000a0dc]:lui a4, 1
[0x8000a0e0]:addi a4, a4, 2048
[0x8000a0e4]:add a6, a6, a4
[0x8000a0e8]:lw s10, 840(a6)
[0x8000a0ec]:sub a6, a6, a4
[0x8000a0f0]:lui a4, 1
[0x8000a0f4]:addi a4, a4, 2048
[0x8000a0f8]:add a6, a6, a4
[0x8000a0fc]:lw s11, 844(a6)
[0x8000a100]:sub a6, a6, a4
[0x8000a104]:addi t3, zero, 1
[0x8000a108]:lui t4, 524160
[0x8000a10c]:addi s10, zero, 0
[0x8000a110]:lui s11, 1048320
[0x8000a114]:addi a4, zero, 0
[0x8000a118]:csrrw zero, fcsr, a4
[0x8000a11c]:fadd.d t5, t3, s10, dyn
[0x8000a120]:csrrs a7, fcsr, zero

[0x8000a11c]:fadd.d t5, t3, s10, dyn
[0x8000a120]:csrrs a7, fcsr, zero
[0x8000a124]:sw t5, 1680(ra)
[0x8000a128]:sw t6, 1688(ra)
[0x8000a12c]:sw t5, 1696(ra)
[0x8000a130]:sw a7, 1704(ra)
[0x8000a134]:lui a4, 1
[0x8000a138]:addi a4, a4, 2048
[0x8000a13c]:add a6, a6, a4
[0x8000a140]:lw t3, 848(a6)
[0x8000a144]:sub a6, a6, a4
[0x8000a148]:lui a4, 1
[0x8000a14c]:addi a4, a4, 2048
[0x8000a150]:add a6, a6, a4
[0x8000a154]:lw t4, 852(a6)
[0x8000a158]:sub a6, a6, a4
[0x8000a15c]:lui a4, 1
[0x8000a160]:addi a4, a4, 2048
[0x8000a164]:add a6, a6, a4
[0x8000a168]:lw s10, 856(a6)
[0x8000a16c]:sub a6, a6, a4
[0x8000a170]:lui a4, 1
[0x8000a174]:addi a4, a4, 2048
[0x8000a178]:add a6, a6, a4
[0x8000a17c]:lw s11, 860(a6)
[0x8000a180]:sub a6, a6, a4
[0x8000a184]:addi t3, zero, 1
[0x8000a188]:lui t4, 524160
[0x8000a18c]:addi s10, zero, 0
[0x8000a190]:lui s11, 524160
[0x8000a194]:addi a4, zero, 0
[0x8000a198]:csrrw zero, fcsr, a4
[0x8000a19c]:fadd.d t5, t3, s10, dyn
[0x8000a1a0]:csrrs a7, fcsr, zero

[0x8000a19c]:fadd.d t5, t3, s10, dyn
[0x8000a1a0]:csrrs a7, fcsr, zero
[0x8000a1a4]:sw t5, 1712(ra)
[0x8000a1a8]:sw t6, 1720(ra)
[0x8000a1ac]:sw t5, 1728(ra)
[0x8000a1b0]:sw a7, 1736(ra)
[0x8000a1b4]:lui a4, 1
[0x8000a1b8]:addi a4, a4, 2048
[0x8000a1bc]:add a6, a6, a4
[0x8000a1c0]:lw t3, 864(a6)
[0x8000a1c4]:sub a6, a6, a4
[0x8000a1c8]:lui a4, 1
[0x8000a1cc]:addi a4, a4, 2048
[0x8000a1d0]:add a6, a6, a4
[0x8000a1d4]:lw t4, 868(a6)
[0x8000a1d8]:sub a6, a6, a4
[0x8000a1dc]:lui a4, 1
[0x8000a1e0]:addi a4, a4, 2048
[0x8000a1e4]:add a6, a6, a4
[0x8000a1e8]:lw s10, 872(a6)
[0x8000a1ec]:sub a6, a6, a4
[0x8000a1f0]:lui a4, 1
[0x8000a1f4]:addi a4, a4, 2048
[0x8000a1f8]:add a6, a6, a4
[0x8000a1fc]:lw s11, 876(a6)
[0x8000a200]:sub a6, a6, a4
[0x8000a204]:addi t3, zero, 1
[0x8000a208]:lui t4, 524160
[0x8000a20c]:addi s10, zero, 0
[0x8000a210]:lui s11, 1048448
[0x8000a214]:addi a4, zero, 0
[0x8000a218]:csrrw zero, fcsr, a4
[0x8000a21c]:fadd.d t5, t3, s10, dyn
[0x8000a220]:csrrs a7, fcsr, zero

[0x8000a21c]:fadd.d t5, t3, s10, dyn
[0x8000a220]:csrrs a7, fcsr, zero
[0x8000a224]:sw t5, 1744(ra)
[0x8000a228]:sw t6, 1752(ra)
[0x8000a22c]:sw t5, 1760(ra)
[0x8000a230]:sw a7, 1768(ra)
[0x8000a234]:lui a4, 1
[0x8000a238]:addi a4, a4, 2048
[0x8000a23c]:add a6, a6, a4
[0x8000a240]:lw t3, 880(a6)
[0x8000a244]:sub a6, a6, a4
[0x8000a248]:lui a4, 1
[0x8000a24c]:addi a4, a4, 2048
[0x8000a250]:add a6, a6, a4
[0x8000a254]:lw t4, 884(a6)
[0x8000a258]:sub a6, a6, a4
[0x8000a25c]:lui a4, 1
[0x8000a260]:addi a4, a4, 2048
[0x8000a264]:add a6, a6, a4
[0x8000a268]:lw s10, 888(a6)
[0x8000a26c]:sub a6, a6, a4
[0x8000a270]:lui a4, 1
[0x8000a274]:addi a4, a4, 2048
[0x8000a278]:add a6, a6, a4
[0x8000a27c]:lw s11, 892(a6)
[0x8000a280]:sub a6, a6, a4
[0x8000a284]:addi t3, zero, 1
[0x8000a288]:lui t4, 524160
[0x8000a28c]:addi s10, zero, 1
[0x8000a290]:lui s11, 524160
[0x8000a294]:addi a4, zero, 0
[0x8000a298]:csrrw zero, fcsr, a4
[0x8000a29c]:fadd.d t5, t3, s10, dyn
[0x8000a2a0]:csrrs a7, fcsr, zero

[0x8000a29c]:fadd.d t5, t3, s10, dyn
[0x8000a2a0]:csrrs a7, fcsr, zero
[0x8000a2a4]:sw t5, 1776(ra)
[0x8000a2a8]:sw t6, 1784(ra)
[0x8000a2ac]:sw t5, 1792(ra)
[0x8000a2b0]:sw a7, 1800(ra)
[0x8000a2b4]:lui a4, 1
[0x8000a2b8]:addi a4, a4, 2048
[0x8000a2bc]:add a6, a6, a4
[0x8000a2c0]:lw t3, 896(a6)
[0x8000a2c4]:sub a6, a6, a4
[0x8000a2c8]:lui a4, 1
[0x8000a2cc]:addi a4, a4, 2048
[0x8000a2d0]:add a6, a6, a4
[0x8000a2d4]:lw t4, 900(a6)
[0x8000a2d8]:sub a6, a6, a4
[0x8000a2dc]:lui a4, 1
[0x8000a2e0]:addi a4, a4, 2048
[0x8000a2e4]:add a6, a6, a4
[0x8000a2e8]:lw s10, 904(a6)
[0x8000a2ec]:sub a6, a6, a4
[0x8000a2f0]:lui a4, 1
[0x8000a2f4]:addi a4, a4, 2048
[0x8000a2f8]:add a6, a6, a4
[0x8000a2fc]:lw s11, 908(a6)
[0x8000a300]:sub a6, a6, a4
[0x8000a304]:addi t3, zero, 1
[0x8000a308]:lui t4, 524160
[0x8000a30c]:addi s10, zero, 1
[0x8000a310]:lui s11, 1048448
[0x8000a314]:addi a4, zero, 0
[0x8000a318]:csrrw zero, fcsr, a4
[0x8000a31c]:fadd.d t5, t3, s10, dyn
[0x8000a320]:csrrs a7, fcsr, zero

[0x8000a31c]:fadd.d t5, t3, s10, dyn
[0x8000a320]:csrrs a7, fcsr, zero
[0x8000a324]:sw t5, 1808(ra)
[0x8000a328]:sw t6, 1816(ra)
[0x8000a32c]:sw t5, 1824(ra)
[0x8000a330]:sw a7, 1832(ra)
[0x8000a334]:lui a4, 1
[0x8000a338]:addi a4, a4, 2048
[0x8000a33c]:add a6, a6, a4
[0x8000a340]:lw t3, 912(a6)
[0x8000a344]:sub a6, a6, a4
[0x8000a348]:lui a4, 1
[0x8000a34c]:addi a4, a4, 2048
[0x8000a350]:add a6, a6, a4
[0x8000a354]:lw t4, 916(a6)
[0x8000a358]:sub a6, a6, a4
[0x8000a35c]:lui a4, 1
[0x8000a360]:addi a4, a4, 2048
[0x8000a364]:add a6, a6, a4
[0x8000a368]:lw s10, 920(a6)
[0x8000a36c]:sub a6, a6, a4
[0x8000a370]:lui a4, 1
[0x8000a374]:addi a4, a4, 2048
[0x8000a378]:add a6, a6, a4
[0x8000a37c]:lw s11, 924(a6)
[0x8000a380]:sub a6, a6, a4
[0x8000a384]:addi t3, zero, 1
[0x8000a388]:lui t4, 524160
[0x8000a38c]:addi s10, zero, 1
[0x8000a390]:lui s11, 524032
[0x8000a394]:addi a4, zero, 0
[0x8000a398]:csrrw zero, fcsr, a4
[0x8000a39c]:fadd.d t5, t3, s10, dyn
[0x8000a3a0]:csrrs a7, fcsr, zero

[0x8000a39c]:fadd.d t5, t3, s10, dyn
[0x8000a3a0]:csrrs a7, fcsr, zero
[0x8000a3a4]:sw t5, 1840(ra)
[0x8000a3a8]:sw t6, 1848(ra)
[0x8000a3ac]:sw t5, 1856(ra)
[0x8000a3b0]:sw a7, 1864(ra)
[0x8000a3b4]:lui a4, 1
[0x8000a3b8]:addi a4, a4, 2048
[0x8000a3bc]:add a6, a6, a4
[0x8000a3c0]:lw t3, 928(a6)
[0x8000a3c4]:sub a6, a6, a4
[0x8000a3c8]:lui a4, 1
[0x8000a3cc]:addi a4, a4, 2048
[0x8000a3d0]:add a6, a6, a4
[0x8000a3d4]:lw t4, 932(a6)
[0x8000a3d8]:sub a6, a6, a4
[0x8000a3dc]:lui a4, 1
[0x8000a3e0]:addi a4, a4, 2048
[0x8000a3e4]:add a6, a6, a4
[0x8000a3e8]:lw s10, 936(a6)
[0x8000a3ec]:sub a6, a6, a4
[0x8000a3f0]:lui a4, 1
[0x8000a3f4]:addi a4, a4, 2048
[0x8000a3f8]:add a6, a6, a4
[0x8000a3fc]:lw s11, 940(a6)
[0x8000a400]:sub a6, a6, a4
[0x8000a404]:addi t3, zero, 1
[0x8000a408]:lui t4, 524160
[0x8000a40c]:addi s10, zero, 1
[0x8000a410]:lui s11, 1048320
[0x8000a414]:addi a4, zero, 0
[0x8000a418]:csrrw zero, fcsr, a4
[0x8000a41c]:fadd.d t5, t3, s10, dyn
[0x8000a420]:csrrs a7, fcsr, zero

[0x8000a41c]:fadd.d t5, t3, s10, dyn
[0x8000a420]:csrrs a7, fcsr, zero
[0x8000a424]:sw t5, 1872(ra)
[0x8000a428]:sw t6, 1880(ra)
[0x8000a42c]:sw t5, 1888(ra)
[0x8000a430]:sw a7, 1896(ra)
[0x8000a434]:lui a4, 1
[0x8000a438]:addi a4, a4, 2048
[0x8000a43c]:add a6, a6, a4
[0x8000a440]:lw t3, 944(a6)
[0x8000a444]:sub a6, a6, a4
[0x8000a448]:lui a4, 1
[0x8000a44c]:addi a4, a4, 2048
[0x8000a450]:add a6, a6, a4
[0x8000a454]:lw t4, 948(a6)
[0x8000a458]:sub a6, a6, a4
[0x8000a45c]:lui a4, 1
[0x8000a460]:addi a4, a4, 2048
[0x8000a464]:add a6, a6, a4
[0x8000a468]:lw s10, 952(a6)
[0x8000a46c]:sub a6, a6, a4
[0x8000a470]:lui a4, 1
[0x8000a474]:addi a4, a4, 2048
[0x8000a478]:add a6, a6, a4
[0x8000a47c]:lw s11, 956(a6)
[0x8000a480]:sub a6, a6, a4
[0x8000a484]:addi t3, zero, 1
[0x8000a488]:lui t4, 524160
[0x8000a48c]:addi s10, zero, 0
[0x8000a490]:lui s11, 261888
[0x8000a494]:addi a4, zero, 0
[0x8000a498]:csrrw zero, fcsr, a4
[0x8000a49c]:fadd.d t5, t3, s10, dyn
[0x8000a4a0]:csrrs a7, fcsr, zero

[0x8000a49c]:fadd.d t5, t3, s10, dyn
[0x8000a4a0]:csrrs a7, fcsr, zero
[0x8000a4a4]:sw t5, 1904(ra)
[0x8000a4a8]:sw t6, 1912(ra)
[0x8000a4ac]:sw t5, 1920(ra)
[0x8000a4b0]:sw a7, 1928(ra)
[0x8000a4b4]:lui a4, 1
[0x8000a4b8]:addi a4, a4, 2048
[0x8000a4bc]:add a6, a6, a4
[0x8000a4c0]:lw t3, 960(a6)
[0x8000a4c4]:sub a6, a6, a4
[0x8000a4c8]:lui a4, 1
[0x8000a4cc]:addi a4, a4, 2048
[0x8000a4d0]:add a6, a6, a4
[0x8000a4d4]:lw t4, 964(a6)
[0x8000a4d8]:sub a6, a6, a4
[0x8000a4dc]:lui a4, 1
[0x8000a4e0]:addi a4, a4, 2048
[0x8000a4e4]:add a6, a6, a4
[0x8000a4e8]:lw s10, 968(a6)
[0x8000a4ec]:sub a6, a6, a4
[0x8000a4f0]:lui a4, 1
[0x8000a4f4]:addi a4, a4, 2048
[0x8000a4f8]:add a6, a6, a4
[0x8000a4fc]:lw s11, 972(a6)
[0x8000a500]:sub a6, a6, a4
[0x8000a504]:addi t3, zero, 1
[0x8000a508]:lui t4, 524160
[0x8000a50c]:addi s10, zero, 0
[0x8000a510]:lui s11, 784384
[0x8000a514]:addi a4, zero, 0
[0x8000a518]:csrrw zero, fcsr, a4
[0x8000a51c]:fadd.d t5, t3, s10, dyn
[0x8000a520]:csrrs a7, fcsr, zero

[0x8000a51c]:fadd.d t5, t3, s10, dyn
[0x8000a520]:csrrs a7, fcsr, zero
[0x8000a524]:sw t5, 1936(ra)
[0x8000a528]:sw t6, 1944(ra)
[0x8000a52c]:sw t5, 1952(ra)
[0x8000a530]:sw a7, 1960(ra)
[0x8000a534]:lui a4, 1
[0x8000a538]:addi a4, a4, 2048
[0x8000a53c]:add a6, a6, a4
[0x8000a540]:lw t3, 976(a6)
[0x8000a544]:sub a6, a6, a4
[0x8000a548]:lui a4, 1
[0x8000a54c]:addi a4, a4, 2048
[0x8000a550]:add a6, a6, a4
[0x8000a554]:lw t4, 980(a6)
[0x8000a558]:sub a6, a6, a4
[0x8000a55c]:lui a4, 1
[0x8000a560]:addi a4, a4, 2048
[0x8000a564]:add a6, a6, a4
[0x8000a568]:lw s10, 984(a6)
[0x8000a56c]:sub a6, a6, a4
[0x8000a570]:lui a4, 1
[0x8000a574]:addi a4, a4, 2048
[0x8000a578]:add a6, a6, a4
[0x8000a57c]:lw s11, 988(a6)
[0x8000a580]:sub a6, a6, a4
[0x8000a584]:addi t3, zero, 1
[0x8000a588]:lui t4, 1048448
[0x8000a58c]:addi s10, zero, 0
[0x8000a590]:addi s11, zero, 0
[0x8000a594]:addi a4, zero, 0
[0x8000a598]:csrrw zero, fcsr, a4
[0x8000a59c]:fadd.d t5, t3, s10, dyn
[0x8000a5a0]:csrrs a7, fcsr, zero

[0x8000a59c]:fadd.d t5, t3, s10, dyn
[0x8000a5a0]:csrrs a7, fcsr, zero
[0x8000a5a4]:sw t5, 1968(ra)
[0x8000a5a8]:sw t6, 1976(ra)
[0x8000a5ac]:sw t5, 1984(ra)
[0x8000a5b0]:sw a7, 1992(ra)
[0x8000a5b4]:lui a4, 1
[0x8000a5b8]:addi a4, a4, 2048
[0x8000a5bc]:add a6, a6, a4
[0x8000a5c0]:lw t3, 992(a6)
[0x8000a5c4]:sub a6, a6, a4
[0x8000a5c8]:lui a4, 1
[0x8000a5cc]:addi a4, a4, 2048
[0x8000a5d0]:add a6, a6, a4
[0x8000a5d4]:lw t4, 996(a6)
[0x8000a5d8]:sub a6, a6, a4
[0x8000a5dc]:lui a4, 1
[0x8000a5e0]:addi a4, a4, 2048
[0x8000a5e4]:add a6, a6, a4
[0x8000a5e8]:lw s10, 1000(a6)
[0x8000a5ec]:sub a6, a6, a4
[0x8000a5f0]:lui a4, 1
[0x8000a5f4]:addi a4, a4, 2048
[0x8000a5f8]:add a6, a6, a4
[0x8000a5fc]:lw s11, 1004(a6)
[0x8000a600]:sub a6, a6, a4
[0x8000a604]:addi t3, zero, 1
[0x8000a608]:lui t4, 1048448
[0x8000a60c]:addi s10, zero, 0
[0x8000a610]:lui s11, 524288
[0x8000a614]:addi a4, zero, 0
[0x8000a618]:csrrw zero, fcsr, a4
[0x8000a61c]:fadd.d t5, t3, s10, dyn
[0x8000a620]:csrrs a7, fcsr, zero

[0x8000a61c]:fadd.d t5, t3, s10, dyn
[0x8000a620]:csrrs a7, fcsr, zero
[0x8000a624]:sw t5, 2000(ra)
[0x8000a628]:sw t6, 2008(ra)
[0x8000a62c]:sw t5, 2016(ra)
[0x8000a630]:sw a7, 2024(ra)
[0x8000a634]:lui a4, 1
[0x8000a638]:addi a4, a4, 2048
[0x8000a63c]:add a6, a6, a4
[0x8000a640]:lw t3, 1008(a6)
[0x8000a644]:sub a6, a6, a4
[0x8000a648]:lui a4, 1
[0x8000a64c]:addi a4, a4, 2048
[0x8000a650]:add a6, a6, a4
[0x8000a654]:lw t4, 1012(a6)
[0x8000a658]:sub a6, a6, a4
[0x8000a65c]:lui a4, 1
[0x8000a660]:addi a4, a4, 2048
[0x8000a664]:add a6, a6, a4
[0x8000a668]:lw s10, 1016(a6)
[0x8000a66c]:sub a6, a6, a4
[0x8000a670]:lui a4, 1
[0x8000a674]:addi a4, a4, 2048
[0x8000a678]:add a6, a6, a4
[0x8000a67c]:lw s11, 1020(a6)
[0x8000a680]:sub a6, a6, a4
[0x8000a684]:addi t3, zero, 1
[0x8000a688]:lui t4, 1048448
[0x8000a68c]:addi s10, zero, 1
[0x8000a690]:addi s11, zero, 0
[0x8000a694]:addi a4, zero, 0
[0x8000a698]:csrrw zero, fcsr, a4
[0x8000a69c]:fadd.d t5, t3, s10, dyn
[0x8000a6a0]:csrrs a7, fcsr, zero

[0x8000a69c]:fadd.d t5, t3, s10, dyn
[0x8000a6a0]:csrrs a7, fcsr, zero
[0x8000a6a4]:sw t5, 2032(ra)
[0x8000a6a8]:sw t6, 2040(ra)
[0x8000a6ac]:addi ra, ra, 2040
[0x8000a6b0]:sw t5, 8(ra)
[0x8000a6b4]:sw a7, 16(ra)
[0x8000a6b8]:lui a4, 1
[0x8000a6bc]:addi a4, a4, 2048
[0x8000a6c0]:add a6, a6, a4
[0x8000a6c4]:lw t3, 1024(a6)
[0x8000a6c8]:sub a6, a6, a4
[0x8000a6cc]:lui a4, 1
[0x8000a6d0]:addi a4, a4, 2048
[0x8000a6d4]:add a6, a6, a4
[0x8000a6d8]:lw t4, 1028(a6)
[0x8000a6dc]:sub a6, a6, a4
[0x8000a6e0]:lui a4, 1
[0x8000a6e4]:addi a4, a4, 2048
[0x8000a6e8]:add a6, a6, a4
[0x8000a6ec]:lw s10, 1032(a6)
[0x8000a6f0]:sub a6, a6, a4
[0x8000a6f4]:lui a4, 1
[0x8000a6f8]:addi a4, a4, 2048
[0x8000a6fc]:add a6, a6, a4
[0x8000a700]:lw s11, 1036(a6)
[0x8000a704]:sub a6, a6, a4
[0x8000a708]:addi t3, zero, 1
[0x8000a70c]:lui t4, 1048448
[0x8000a710]:addi s10, zero, 1
[0x8000a714]:lui s11, 524288
[0x8000a718]:addi a4, zero, 0
[0x8000a71c]:csrrw zero, fcsr, a4
[0x8000a720]:fadd.d t5, t3, s10, dyn
[0x8000a724]:csrrs a7, fcsr, zero

[0x8000a720]:fadd.d t5, t3, s10, dyn
[0x8000a724]:csrrs a7, fcsr, zero
[0x8000a728]:sw t5, 24(ra)
[0x8000a72c]:sw t6, 32(ra)
[0x8000a730]:sw t5, 40(ra)
[0x8000a734]:sw a7, 48(ra)
[0x8000a738]:lui a4, 1
[0x8000a73c]:addi a4, a4, 2048
[0x8000a740]:add a6, a6, a4
[0x8000a744]:lw t3, 1040(a6)
[0x8000a748]:sub a6, a6, a4
[0x8000a74c]:lui a4, 1
[0x8000a750]:addi a4, a4, 2048
[0x8000a754]:add a6, a6, a4
[0x8000a758]:lw t4, 1044(a6)
[0x8000a75c]:sub a6, a6, a4
[0x8000a760]:lui a4, 1
[0x8000a764]:addi a4, a4, 2048
[0x8000a768]:add a6, a6, a4
[0x8000a76c]:lw s10, 1048(a6)
[0x8000a770]:sub a6, a6, a4
[0x8000a774]:lui a4, 1
[0x8000a778]:addi a4, a4, 2048
[0x8000a77c]:add a6, a6, a4
[0x8000a780]:lw s11, 1052(a6)
[0x8000a784]:sub a6, a6, a4
[0x8000a788]:addi t3, zero, 1
[0x8000a78c]:lui t4, 1048448
[0x8000a790]:addi s10, zero, 2
[0x8000a794]:addi s11, zero, 0
[0x8000a798]:addi a4, zero, 0
[0x8000a79c]:csrrw zero, fcsr, a4
[0x8000a7a0]:fadd.d t5, t3, s10, dyn
[0x8000a7a4]:csrrs a7, fcsr, zero

[0x8000a7a0]:fadd.d t5, t3, s10, dyn
[0x8000a7a4]:csrrs a7, fcsr, zero
[0x8000a7a8]:sw t5, 56(ra)
[0x8000a7ac]:sw t6, 64(ra)
[0x8000a7b0]:sw t5, 72(ra)
[0x8000a7b4]:sw a7, 80(ra)
[0x8000a7b8]:lui a4, 1
[0x8000a7bc]:addi a4, a4, 2048
[0x8000a7c0]:add a6, a6, a4
[0x8000a7c4]:lw t3, 1056(a6)
[0x8000a7c8]:sub a6, a6, a4
[0x8000a7cc]:lui a4, 1
[0x8000a7d0]:addi a4, a4, 2048
[0x8000a7d4]:add a6, a6, a4
[0x8000a7d8]:lw t4, 1060(a6)
[0x8000a7dc]:sub a6, a6, a4
[0x8000a7e0]:lui a4, 1
[0x8000a7e4]:addi a4, a4, 2048
[0x8000a7e8]:add a6, a6, a4
[0x8000a7ec]:lw s10, 1064(a6)
[0x8000a7f0]:sub a6, a6, a4
[0x8000a7f4]:lui a4, 1
[0x8000a7f8]:addi a4, a4, 2048
[0x8000a7fc]:add a6, a6, a4
[0x8000a800]:lw s11, 1068(a6)
[0x8000a804]:sub a6, a6, a4
[0x8000a808]:addi t3, zero, 1
[0x8000a80c]:lui t4, 1048448
[0x8000a810]:addi s10, zero, 2
[0x8000a814]:lui s11, 524288
[0x8000a818]:addi a4, zero, 0
[0x8000a81c]:csrrw zero, fcsr, a4
[0x8000a820]:fadd.d t5, t3, s10, dyn
[0x8000a824]:csrrs a7, fcsr, zero

[0x8000a820]:fadd.d t5, t3, s10, dyn
[0x8000a824]:csrrs a7, fcsr, zero
[0x8000a828]:sw t5, 88(ra)
[0x8000a82c]:sw t6, 96(ra)
[0x8000a830]:sw t5, 104(ra)
[0x8000a834]:sw a7, 112(ra)
[0x8000a838]:lui a4, 1
[0x8000a83c]:addi a4, a4, 2048
[0x8000a840]:add a6, a6, a4
[0x8000a844]:lw t3, 1072(a6)
[0x8000a848]:sub a6, a6, a4
[0x8000a84c]:lui a4, 1
[0x8000a850]:addi a4, a4, 2048
[0x8000a854]:add a6, a6, a4
[0x8000a858]:lw t4, 1076(a6)
[0x8000a85c]:sub a6, a6, a4
[0x8000a860]:lui a4, 1
[0x8000a864]:addi a4, a4, 2048
[0x8000a868]:add a6, a6, a4
[0x8000a86c]:lw s10, 1080(a6)
[0x8000a870]:sub a6, a6, a4
[0x8000a874]:lui a4, 1
[0x8000a878]:addi a4, a4, 2048
[0x8000a87c]:add a6, a6, a4
[0x8000a880]:lw s11, 1084(a6)
[0x8000a884]:sub a6, a6, a4
[0x8000a888]:addi t3, zero, 1
[0x8000a88c]:lui t4, 1048448
[0x8000a890]:addi s10, zero, 4095
[0x8000a894]:lui s11, 256
[0x8000a898]:addi s11, s11, 4095
[0x8000a89c]:addi a4, zero, 0
[0x8000a8a0]:csrrw zero, fcsr, a4
[0x8000a8a4]:fadd.d t5, t3, s10, dyn
[0x8000a8a8]:csrrs a7, fcsr, zero

[0x8000a8a4]:fadd.d t5, t3, s10, dyn
[0x8000a8a8]:csrrs a7, fcsr, zero
[0x8000a8ac]:sw t5, 120(ra)
[0x8000a8b0]:sw t6, 128(ra)
[0x8000a8b4]:sw t5, 136(ra)
[0x8000a8b8]:sw a7, 144(ra)
[0x8000a8bc]:lui a4, 1
[0x8000a8c0]:addi a4, a4, 2048
[0x8000a8c4]:add a6, a6, a4
[0x8000a8c8]:lw t3, 1088(a6)
[0x8000a8cc]:sub a6, a6, a4
[0x8000a8d0]:lui a4, 1
[0x8000a8d4]:addi a4, a4, 2048
[0x8000a8d8]:add a6, a6, a4
[0x8000a8dc]:lw t4, 1092(a6)
[0x8000a8e0]:sub a6, a6, a4
[0x8000a8e4]:lui a4, 1
[0x8000a8e8]:addi a4, a4, 2048
[0x8000a8ec]:add a6, a6, a4
[0x8000a8f0]:lw s10, 1096(a6)
[0x8000a8f4]:sub a6, a6, a4
[0x8000a8f8]:lui a4, 1
[0x8000a8fc]:addi a4, a4, 2048
[0x8000a900]:add a6, a6, a4
[0x8000a904]:lw s11, 1100(a6)
[0x8000a908]:sub a6, a6, a4
[0x8000a90c]:addi t3, zero, 1
[0x8000a910]:lui t4, 1048448
[0x8000a914]:addi s10, zero, 4095
[0x8000a918]:lui s11, 524544
[0x8000a91c]:addi s11, s11, 4095
[0x8000a920]:addi a4, zero, 0
[0x8000a924]:csrrw zero, fcsr, a4
[0x8000a928]:fadd.d t5, t3, s10, dyn
[0x8000a92c]:csrrs a7, fcsr, zero

[0x8000a928]:fadd.d t5, t3, s10, dyn
[0x8000a92c]:csrrs a7, fcsr, zero
[0x8000a930]:sw t5, 152(ra)
[0x8000a934]:sw t6, 160(ra)
[0x8000a938]:sw t5, 168(ra)
[0x8000a93c]:sw a7, 176(ra)
[0x8000a940]:lui a4, 1
[0x8000a944]:addi a4, a4, 2048
[0x8000a948]:add a6, a6, a4
[0x8000a94c]:lw t3, 1104(a6)
[0x8000a950]:sub a6, a6, a4
[0x8000a954]:lui a4, 1
[0x8000a958]:addi a4, a4, 2048
[0x8000a95c]:add a6, a6, a4
[0x8000a960]:lw t4, 1108(a6)
[0x8000a964]:sub a6, a6, a4
[0x8000a968]:lui a4, 1
[0x8000a96c]:addi a4, a4, 2048
[0x8000a970]:add a6, a6, a4
[0x8000a974]:lw s10, 1112(a6)
[0x8000a978]:sub a6, a6, a4
[0x8000a97c]:lui a4, 1
[0x8000a980]:addi a4, a4, 2048
[0x8000a984]:add a6, a6, a4
[0x8000a988]:lw s11, 1116(a6)
[0x8000a98c]:sub a6, a6, a4
[0x8000a990]:addi t3, zero, 1
[0x8000a994]:lui t4, 1048448
[0x8000a998]:addi s10, zero, 0
[0x8000a99c]:lui s11, 256
[0x8000a9a0]:addi a4, zero, 0
[0x8000a9a4]:csrrw zero, fcsr, a4
[0x8000a9a8]:fadd.d t5, t3, s10, dyn
[0x8000a9ac]:csrrs a7, fcsr, zero

[0x8000a9a8]:fadd.d t5, t3, s10, dyn
[0x8000a9ac]:csrrs a7, fcsr, zero
[0x8000a9b0]:sw t5, 184(ra)
[0x8000a9b4]:sw t6, 192(ra)
[0x8000a9b8]:sw t5, 200(ra)
[0x8000a9bc]:sw a7, 208(ra)
[0x8000a9c0]:lui a4, 1
[0x8000a9c4]:addi a4, a4, 2048
[0x8000a9c8]:add a6, a6, a4
[0x8000a9cc]:lw t3, 1120(a6)
[0x8000a9d0]:sub a6, a6, a4
[0x8000a9d4]:lui a4, 1
[0x8000a9d8]:addi a4, a4, 2048
[0x8000a9dc]:add a6, a6, a4
[0x8000a9e0]:lw t4, 1124(a6)
[0x8000a9e4]:sub a6, a6, a4
[0x8000a9e8]:lui a4, 1
[0x8000a9ec]:addi a4, a4, 2048
[0x8000a9f0]:add a6, a6, a4
[0x8000a9f4]:lw s10, 1128(a6)
[0x8000a9f8]:sub a6, a6, a4
[0x8000a9fc]:lui a4, 1
[0x8000aa00]:addi a4, a4, 2048
[0x8000aa04]:add a6, a6, a4
[0x8000aa08]:lw s11, 1132(a6)
[0x8000aa0c]:sub a6, a6, a4
[0x8000aa10]:addi t3, zero, 1
[0x8000aa14]:lui t4, 1048448
[0x8000aa18]:addi s10, zero, 0
[0x8000aa1c]:lui s11, 524544
[0x8000aa20]:addi a4, zero, 0
[0x8000aa24]:csrrw zero, fcsr, a4
[0x8000aa28]:fadd.d t5, t3, s10, dyn
[0x8000aa2c]:csrrs a7, fcsr, zero

[0x8000aa28]:fadd.d t5, t3, s10, dyn
[0x8000aa2c]:csrrs a7, fcsr, zero
[0x8000aa30]:sw t5, 216(ra)
[0x8000aa34]:sw t6, 224(ra)
[0x8000aa38]:sw t5, 232(ra)
[0x8000aa3c]:sw a7, 240(ra)
[0x8000aa40]:lui a4, 1
[0x8000aa44]:addi a4, a4, 2048
[0x8000aa48]:add a6, a6, a4
[0x8000aa4c]:lw t3, 1136(a6)
[0x8000aa50]:sub a6, a6, a4
[0x8000aa54]:lui a4, 1
[0x8000aa58]:addi a4, a4, 2048
[0x8000aa5c]:add a6, a6, a4
[0x8000aa60]:lw t4, 1140(a6)
[0x8000aa64]:sub a6, a6, a4
[0x8000aa68]:lui a4, 1
[0x8000aa6c]:addi a4, a4, 2048
[0x8000aa70]:add a6, a6, a4
[0x8000aa74]:lw s10, 1144(a6)
[0x8000aa78]:sub a6, a6, a4
[0x8000aa7c]:lui a4, 1
[0x8000aa80]:addi a4, a4, 2048
[0x8000aa84]:add a6, a6, a4
[0x8000aa88]:lw s11, 1148(a6)
[0x8000aa8c]:sub a6, a6, a4
[0x8000aa90]:addi t3, zero, 1
[0x8000aa94]:lui t4, 1048448
[0x8000aa98]:addi s10, zero, 2
[0x8000aa9c]:lui s11, 256
[0x8000aaa0]:addi a4, zero, 0
[0x8000aaa4]:csrrw zero, fcsr, a4
[0x8000aaa8]:fadd.d t5, t3, s10, dyn
[0x8000aaac]:csrrs a7, fcsr, zero

[0x8000aaa8]:fadd.d t5, t3, s10, dyn
[0x8000aaac]:csrrs a7, fcsr, zero
[0x8000aab0]:sw t5, 248(ra)
[0x8000aab4]:sw t6, 256(ra)
[0x8000aab8]:sw t5, 264(ra)
[0x8000aabc]:sw a7, 272(ra)
[0x8000aac0]:lui a4, 1
[0x8000aac4]:addi a4, a4, 2048
[0x8000aac8]:add a6, a6, a4
[0x8000aacc]:lw t3, 1152(a6)
[0x8000aad0]:sub a6, a6, a4
[0x8000aad4]:lui a4, 1
[0x8000aad8]:addi a4, a4, 2048
[0x8000aadc]:add a6, a6, a4
[0x8000aae0]:lw t4, 1156(a6)
[0x8000aae4]:sub a6, a6, a4
[0x8000aae8]:lui a4, 1
[0x8000aaec]:addi a4, a4, 2048
[0x8000aaf0]:add a6, a6, a4
[0x8000aaf4]:lw s10, 1160(a6)
[0x8000aaf8]:sub a6, a6, a4
[0x8000aafc]:lui a4, 1
[0x8000ab00]:addi a4, a4, 2048
[0x8000ab04]:add a6, a6, a4
[0x8000ab08]:lw s11, 1164(a6)
[0x8000ab0c]:sub a6, a6, a4
[0x8000ab10]:addi t3, zero, 1
[0x8000ab14]:lui t4, 1048448
[0x8000ab18]:addi s10, zero, 2
[0x8000ab1c]:lui s11, 524544
[0x8000ab20]:addi a4, zero, 0
[0x8000ab24]:csrrw zero, fcsr, a4
[0x8000ab28]:fadd.d t5, t3, s10, dyn
[0x8000ab2c]:csrrs a7, fcsr, zero

[0x8000ab28]:fadd.d t5, t3, s10, dyn
[0x8000ab2c]:csrrs a7, fcsr, zero
[0x8000ab30]:sw t5, 280(ra)
[0x8000ab34]:sw t6, 288(ra)
[0x8000ab38]:sw t5, 296(ra)
[0x8000ab3c]:sw a7, 304(ra)
[0x8000ab40]:lui a4, 1
[0x8000ab44]:addi a4, a4, 2048
[0x8000ab48]:add a6, a6, a4
[0x8000ab4c]:lw t3, 1168(a6)
[0x8000ab50]:sub a6, a6, a4
[0x8000ab54]:lui a4, 1
[0x8000ab58]:addi a4, a4, 2048
[0x8000ab5c]:add a6, a6, a4
[0x8000ab60]:lw t4, 1172(a6)
[0x8000ab64]:sub a6, a6, a4
[0x8000ab68]:lui a4, 1
[0x8000ab6c]:addi a4, a4, 2048
[0x8000ab70]:add a6, a6, a4
[0x8000ab74]:lw s10, 1176(a6)
[0x8000ab78]:sub a6, a6, a4
[0x8000ab7c]:lui a4, 1
[0x8000ab80]:addi a4, a4, 2048
[0x8000ab84]:add a6, a6, a4
[0x8000ab88]:lw s11, 1180(a6)
[0x8000ab8c]:sub a6, a6, a4
[0x8000ab90]:addi t3, zero, 1
[0x8000ab94]:lui t4, 1048448
[0x8000ab98]:addi s10, zero, 4095
[0x8000ab9c]:lui s11, 524032
[0x8000aba0]:addi s11, s11, 4095
[0x8000aba4]:addi a4, zero, 0
[0x8000aba8]:csrrw zero, fcsr, a4
[0x8000abac]:fadd.d t5, t3, s10, dyn
[0x8000abb0]:csrrs a7, fcsr, zero

[0x8000abac]:fadd.d t5, t3, s10, dyn
[0x8000abb0]:csrrs a7, fcsr, zero
[0x8000abb4]:sw t5, 312(ra)
[0x8000abb8]:sw t6, 320(ra)
[0x8000abbc]:sw t5, 328(ra)
[0x8000abc0]:sw a7, 336(ra)
[0x8000abc4]:lui a4, 1
[0x8000abc8]:addi a4, a4, 2048
[0x8000abcc]:add a6, a6, a4
[0x8000abd0]:lw t3, 1184(a6)
[0x8000abd4]:sub a6, a6, a4
[0x8000abd8]:lui a4, 1
[0x8000abdc]:addi a4, a4, 2048
[0x8000abe0]:add a6, a6, a4
[0x8000abe4]:lw t4, 1188(a6)
[0x8000abe8]:sub a6, a6, a4
[0x8000abec]:lui a4, 1
[0x8000abf0]:addi a4, a4, 2048
[0x8000abf4]:add a6, a6, a4
[0x8000abf8]:lw s10, 1192(a6)
[0x8000abfc]:sub a6, a6, a4
[0x8000ac00]:lui a4, 1
[0x8000ac04]:addi a4, a4, 2048
[0x8000ac08]:add a6, a6, a4
[0x8000ac0c]:lw s11, 1196(a6)
[0x8000ac10]:sub a6, a6, a4
[0x8000ac14]:addi t3, zero, 1
[0x8000ac18]:lui t4, 1048448
[0x8000ac1c]:addi s10, zero, 4095
[0x8000ac20]:lui s11, 1048320
[0x8000ac24]:addi s11, s11, 4095
[0x8000ac28]:addi a4, zero, 0
[0x8000ac2c]:csrrw zero, fcsr, a4
[0x8000ac30]:fadd.d t5, t3, s10, dyn
[0x8000ac34]:csrrs a7, fcsr, zero

[0x8000ac30]:fadd.d t5, t3, s10, dyn
[0x8000ac34]:csrrs a7, fcsr, zero
[0x8000ac38]:sw t5, 344(ra)
[0x8000ac3c]:sw t6, 352(ra)
[0x8000ac40]:sw t5, 360(ra)
[0x8000ac44]:sw a7, 368(ra)
[0x8000ac48]:lui a4, 1
[0x8000ac4c]:addi a4, a4, 2048
[0x8000ac50]:add a6, a6, a4
[0x8000ac54]:lw t3, 1200(a6)
[0x8000ac58]:sub a6, a6, a4
[0x8000ac5c]:lui a4, 1
[0x8000ac60]:addi a4, a4, 2048
[0x8000ac64]:add a6, a6, a4
[0x8000ac68]:lw t4, 1204(a6)
[0x8000ac6c]:sub a6, a6, a4
[0x8000ac70]:lui a4, 1
[0x8000ac74]:addi a4, a4, 2048
[0x8000ac78]:add a6, a6, a4
[0x8000ac7c]:lw s10, 1208(a6)
[0x8000ac80]:sub a6, a6, a4
[0x8000ac84]:lui a4, 1
[0x8000ac88]:addi a4, a4, 2048
[0x8000ac8c]:add a6, a6, a4
[0x8000ac90]:lw s11, 1212(a6)
[0x8000ac94]:sub a6, a6, a4
[0x8000ac98]:addi t3, zero, 1
[0x8000ac9c]:lui t4, 1048448
[0x8000aca0]:addi s10, zero, 0
[0x8000aca4]:lui s11, 524032
[0x8000aca8]:addi a4, zero, 0
[0x8000acac]:csrrw zero, fcsr, a4
[0x8000acb0]:fadd.d t5, t3, s10, dyn
[0x8000acb4]:csrrs a7, fcsr, zero

[0x8000acb0]:fadd.d t5, t3, s10, dyn
[0x8000acb4]:csrrs a7, fcsr, zero
[0x8000acb8]:sw t5, 376(ra)
[0x8000acbc]:sw t6, 384(ra)
[0x8000acc0]:sw t5, 392(ra)
[0x8000acc4]:sw a7, 400(ra)
[0x8000acc8]:lui a4, 1
[0x8000accc]:addi a4, a4, 2048
[0x8000acd0]:add a6, a6, a4
[0x8000acd4]:lw t3, 1216(a6)
[0x8000acd8]:sub a6, a6, a4
[0x8000acdc]:lui a4, 1
[0x8000ace0]:addi a4, a4, 2048
[0x8000ace4]:add a6, a6, a4
[0x8000ace8]:lw t4, 1220(a6)
[0x8000acec]:sub a6, a6, a4
[0x8000acf0]:lui a4, 1
[0x8000acf4]:addi a4, a4, 2048
[0x8000acf8]:add a6, a6, a4
[0x8000acfc]:lw s10, 1224(a6)
[0x8000ad00]:sub a6, a6, a4
[0x8000ad04]:lui a4, 1
[0x8000ad08]:addi a4, a4, 2048
[0x8000ad0c]:add a6, a6, a4
[0x8000ad10]:lw s11, 1228(a6)
[0x8000ad14]:sub a6, a6, a4
[0x8000ad18]:addi t3, zero, 1
[0x8000ad1c]:lui t4, 1048448
[0x8000ad20]:addi s10, zero, 0
[0x8000ad24]:lui s11, 1048320
[0x8000ad28]:addi a4, zero, 0
[0x8000ad2c]:csrrw zero, fcsr, a4
[0x8000ad30]:fadd.d t5, t3, s10, dyn
[0x8000ad34]:csrrs a7, fcsr, zero

[0x8000ad30]:fadd.d t5, t3, s10, dyn
[0x8000ad34]:csrrs a7, fcsr, zero
[0x8000ad38]:sw t5, 408(ra)
[0x8000ad3c]:sw t6, 416(ra)
[0x8000ad40]:sw t5, 424(ra)
[0x8000ad44]:sw a7, 432(ra)
[0x8000ad48]:lui a4, 1
[0x8000ad4c]:addi a4, a4, 2048
[0x8000ad50]:add a6, a6, a4
[0x8000ad54]:lw t3, 1232(a6)
[0x8000ad58]:sub a6, a6, a4
[0x8000ad5c]:lui a4, 1
[0x8000ad60]:addi a4, a4, 2048
[0x8000ad64]:add a6, a6, a4
[0x8000ad68]:lw t4, 1236(a6)
[0x8000ad6c]:sub a6, a6, a4
[0x8000ad70]:lui a4, 1
[0x8000ad74]:addi a4, a4, 2048
[0x8000ad78]:add a6, a6, a4
[0x8000ad7c]:lw s10, 1240(a6)
[0x8000ad80]:sub a6, a6, a4
[0x8000ad84]:lui a4, 1
[0x8000ad88]:addi a4, a4, 2048
[0x8000ad8c]:add a6, a6, a4
[0x8000ad90]:lw s11, 1244(a6)
[0x8000ad94]:sub a6, a6, a4
[0x8000ad98]:addi t3, zero, 1
[0x8000ad9c]:lui t4, 1048448
[0x8000ada0]:addi s10, zero, 0
[0x8000ada4]:lui s11, 524160
[0x8000ada8]:addi a4, zero, 0
[0x8000adac]:csrrw zero, fcsr, a4
[0x8000adb0]:fadd.d t5, t3, s10, dyn
[0x8000adb4]:csrrs a7, fcsr, zero

[0x8000adb0]:fadd.d t5, t3, s10, dyn
[0x8000adb4]:csrrs a7, fcsr, zero
[0x8000adb8]:sw t5, 440(ra)
[0x8000adbc]:sw t6, 448(ra)
[0x8000adc0]:sw t5, 456(ra)
[0x8000adc4]:sw a7, 464(ra)
[0x8000adc8]:lui a4, 1
[0x8000adcc]:addi a4, a4, 2048
[0x8000add0]:add a6, a6, a4
[0x8000add4]:lw t3, 1248(a6)
[0x8000add8]:sub a6, a6, a4
[0x8000addc]:lui a4, 1
[0x8000ade0]:addi a4, a4, 2048
[0x8000ade4]:add a6, a6, a4
[0x8000ade8]:lw t4, 1252(a6)
[0x8000adec]:sub a6, a6, a4
[0x8000adf0]:lui a4, 1
[0x8000adf4]:addi a4, a4, 2048
[0x8000adf8]:add a6, a6, a4
[0x8000adfc]:lw s10, 1256(a6)
[0x8000ae00]:sub a6, a6, a4
[0x8000ae04]:lui a4, 1
[0x8000ae08]:addi a4, a4, 2048
[0x8000ae0c]:add a6, a6, a4
[0x8000ae10]:lw s11, 1260(a6)
[0x8000ae14]:sub a6, a6, a4
[0x8000ae18]:addi t3, zero, 1
[0x8000ae1c]:lui t4, 1048448
[0x8000ae20]:addi s10, zero, 0
[0x8000ae24]:lui s11, 1048448
[0x8000ae28]:addi a4, zero, 0
[0x8000ae2c]:csrrw zero, fcsr, a4
[0x8000ae30]:fadd.d t5, t3, s10, dyn
[0x8000ae34]:csrrs a7, fcsr, zero

[0x8000ae30]:fadd.d t5, t3, s10, dyn
[0x8000ae34]:csrrs a7, fcsr, zero
[0x8000ae38]:sw t5, 472(ra)
[0x8000ae3c]:sw t6, 480(ra)
[0x8000ae40]:sw t5, 488(ra)
[0x8000ae44]:sw a7, 496(ra)
[0x8000ae48]:lui a4, 1
[0x8000ae4c]:addi a4, a4, 2048
[0x8000ae50]:add a6, a6, a4
[0x8000ae54]:lw t3, 1264(a6)
[0x8000ae58]:sub a6, a6, a4
[0x8000ae5c]:lui a4, 1
[0x8000ae60]:addi a4, a4, 2048
[0x8000ae64]:add a6, a6, a4
[0x8000ae68]:lw t4, 1268(a6)
[0x8000ae6c]:sub a6, a6, a4
[0x8000ae70]:lui a4, 1
[0x8000ae74]:addi a4, a4, 2048
[0x8000ae78]:add a6, a6, a4
[0x8000ae7c]:lw s10, 1272(a6)
[0x8000ae80]:sub a6, a6, a4
[0x8000ae84]:lui a4, 1
[0x8000ae88]:addi a4, a4, 2048
[0x8000ae8c]:add a6, a6, a4
[0x8000ae90]:lw s11, 1276(a6)
[0x8000ae94]:sub a6, a6, a4
[0x8000ae98]:addi t3, zero, 1
[0x8000ae9c]:lui t4, 1048448
[0x8000aea0]:addi s10, zero, 1
[0x8000aea4]:lui s11, 524160
[0x8000aea8]:addi a4, zero, 0
[0x8000aeac]:csrrw zero, fcsr, a4
[0x8000aeb0]:fadd.d t5, t3, s10, dyn
[0x8000aeb4]:csrrs a7, fcsr, zero

[0x8000aeb0]:fadd.d t5, t3, s10, dyn
[0x8000aeb4]:csrrs a7, fcsr, zero
[0x8000aeb8]:sw t5, 504(ra)
[0x8000aebc]:sw t6, 512(ra)
[0x8000aec0]:sw t5, 520(ra)
[0x8000aec4]:sw a7, 528(ra)
[0x8000aec8]:lui a4, 1
[0x8000aecc]:addi a4, a4, 2048
[0x8000aed0]:add a6, a6, a4
[0x8000aed4]:lw t3, 1280(a6)
[0x8000aed8]:sub a6, a6, a4
[0x8000aedc]:lui a4, 1
[0x8000aee0]:addi a4, a4, 2048
[0x8000aee4]:add a6, a6, a4
[0x8000aee8]:lw t4, 1284(a6)
[0x8000aeec]:sub a6, a6, a4
[0x8000aef0]:lui a4, 1
[0x8000aef4]:addi a4, a4, 2048
[0x8000aef8]:add a6, a6, a4
[0x8000aefc]:lw s10, 1288(a6)
[0x8000af00]:sub a6, a6, a4
[0x8000af04]:lui a4, 1
[0x8000af08]:addi a4, a4, 2048
[0x8000af0c]:add a6, a6, a4
[0x8000af10]:lw s11, 1292(a6)
[0x8000af14]:sub a6, a6, a4
[0x8000af18]:addi t3, zero, 1
[0x8000af1c]:lui t4, 1048448
[0x8000af20]:addi s10, zero, 1
[0x8000af24]:lui s11, 1048448
[0x8000af28]:addi a4, zero, 0
[0x8000af2c]:csrrw zero, fcsr, a4
[0x8000af30]:fadd.d t5, t3, s10, dyn
[0x8000af34]:csrrs a7, fcsr, zero

[0x8000af30]:fadd.d t5, t3, s10, dyn
[0x8000af34]:csrrs a7, fcsr, zero
[0x8000af38]:sw t5, 536(ra)
[0x8000af3c]:sw t6, 544(ra)
[0x8000af40]:sw t5, 552(ra)
[0x8000af44]:sw a7, 560(ra)
[0x8000af48]:lui a4, 1
[0x8000af4c]:addi a4, a4, 2048
[0x8000af50]:add a6, a6, a4
[0x8000af54]:lw t3, 1296(a6)
[0x8000af58]:sub a6, a6, a4
[0x8000af5c]:lui a4, 1
[0x8000af60]:addi a4, a4, 2048
[0x8000af64]:add a6, a6, a4
[0x8000af68]:lw t4, 1300(a6)
[0x8000af6c]:sub a6, a6, a4
[0x8000af70]:lui a4, 1
[0x8000af74]:addi a4, a4, 2048
[0x8000af78]:add a6, a6, a4
[0x8000af7c]:lw s10, 1304(a6)
[0x8000af80]:sub a6, a6, a4
[0x8000af84]:lui a4, 1
[0x8000af88]:addi a4, a4, 2048
[0x8000af8c]:add a6, a6, a4
[0x8000af90]:lw s11, 1308(a6)
[0x8000af94]:sub a6, a6, a4
[0x8000af98]:addi t3, zero, 1
[0x8000af9c]:lui t4, 1048448
[0x8000afa0]:addi s10, zero, 1
[0x8000afa4]:lui s11, 524032
[0x8000afa8]:addi a4, zero, 0
[0x8000afac]:csrrw zero, fcsr, a4
[0x8000afb0]:fadd.d t5, t3, s10, dyn
[0x8000afb4]:csrrs a7, fcsr, zero

[0x8000afb0]:fadd.d t5, t3, s10, dyn
[0x8000afb4]:csrrs a7, fcsr, zero
[0x8000afb8]:sw t5, 568(ra)
[0x8000afbc]:sw t6, 576(ra)
[0x8000afc0]:sw t5, 584(ra)
[0x8000afc4]:sw a7, 592(ra)
[0x8000afc8]:lui a4, 1
[0x8000afcc]:addi a4, a4, 2048
[0x8000afd0]:add a6, a6, a4
[0x8000afd4]:lw t3, 1312(a6)
[0x8000afd8]:sub a6, a6, a4
[0x8000afdc]:lui a4, 1
[0x8000afe0]:addi a4, a4, 2048
[0x8000afe4]:add a6, a6, a4
[0x8000afe8]:lw t4, 1316(a6)
[0x8000afec]:sub a6, a6, a4
[0x8000aff0]:lui a4, 1
[0x8000aff4]:addi a4, a4, 2048
[0x8000aff8]:add a6, a6, a4
[0x8000affc]:lw s10, 1320(a6)
[0x8000b000]:sub a6, a6, a4
[0x8000b004]:lui a4, 1
[0x8000b008]:addi a4, a4, 2048
[0x8000b00c]:add a6, a6, a4
[0x8000b010]:lw s11, 1324(a6)
[0x8000b014]:sub a6, a6, a4
[0x8000b018]:addi t3, zero, 1
[0x8000b01c]:lui t4, 1048448
[0x8000b020]:addi s10, zero, 1
[0x8000b024]:lui s11, 1048320
[0x8000b028]:addi a4, zero, 0
[0x8000b02c]:csrrw zero, fcsr, a4
[0x8000b030]:fadd.d t5, t3, s10, dyn
[0x8000b034]:csrrs a7, fcsr, zero

[0x8000b030]:fadd.d t5, t3, s10, dyn
[0x8000b034]:csrrs a7, fcsr, zero
[0x8000b038]:sw t5, 600(ra)
[0x8000b03c]:sw t6, 608(ra)
[0x8000b040]:sw t5, 616(ra)
[0x8000b044]:sw a7, 624(ra)
[0x8000b048]:lui a4, 1
[0x8000b04c]:addi a4, a4, 2048
[0x8000b050]:add a6, a6, a4
[0x8000b054]:lw t3, 1328(a6)
[0x8000b058]:sub a6, a6, a4
[0x8000b05c]:lui a4, 1
[0x8000b060]:addi a4, a4, 2048
[0x8000b064]:add a6, a6, a4
[0x8000b068]:lw t4, 1332(a6)
[0x8000b06c]:sub a6, a6, a4
[0x8000b070]:lui a4, 1
[0x8000b074]:addi a4, a4, 2048
[0x8000b078]:add a6, a6, a4
[0x8000b07c]:lw s10, 1336(a6)
[0x8000b080]:sub a6, a6, a4
[0x8000b084]:lui a4, 1
[0x8000b088]:addi a4, a4, 2048
[0x8000b08c]:add a6, a6, a4
[0x8000b090]:lw s11, 1340(a6)
[0x8000b094]:sub a6, a6, a4
[0x8000b098]:addi t3, zero, 1
[0x8000b09c]:lui t4, 1048448
[0x8000b0a0]:addi s10, zero, 0
[0x8000b0a4]:lui s11, 261888
[0x8000b0a8]:addi a4, zero, 0
[0x8000b0ac]:csrrw zero, fcsr, a4
[0x8000b0b0]:fadd.d t5, t3, s10, dyn
[0x8000b0b4]:csrrs a7, fcsr, zero

[0x8000b0b0]:fadd.d t5, t3, s10, dyn
[0x8000b0b4]:csrrs a7, fcsr, zero
[0x8000b0b8]:sw t5, 632(ra)
[0x8000b0bc]:sw t6, 640(ra)
[0x8000b0c0]:sw t5, 648(ra)
[0x8000b0c4]:sw a7, 656(ra)
[0x8000b0c8]:lui a4, 1
[0x8000b0cc]:addi a4, a4, 2048
[0x8000b0d0]:add a6, a6, a4
[0x8000b0d4]:lw t3, 1344(a6)
[0x8000b0d8]:sub a6, a6, a4
[0x8000b0dc]:lui a4, 1
[0x8000b0e0]:addi a4, a4, 2048
[0x8000b0e4]:add a6, a6, a4
[0x8000b0e8]:lw t4, 1348(a6)
[0x8000b0ec]:sub a6, a6, a4
[0x8000b0f0]:lui a4, 1
[0x8000b0f4]:addi a4, a4, 2048
[0x8000b0f8]:add a6, a6, a4
[0x8000b0fc]:lw s10, 1352(a6)
[0x8000b100]:sub a6, a6, a4
[0x8000b104]:lui a4, 1
[0x8000b108]:addi a4, a4, 2048
[0x8000b10c]:add a6, a6, a4
[0x8000b110]:lw s11, 1356(a6)
[0x8000b114]:sub a6, a6, a4
[0x8000b118]:addi t3, zero, 1
[0x8000b11c]:lui t4, 1048448
[0x8000b120]:addi s10, zero, 0
[0x8000b124]:lui s11, 784384
[0x8000b128]:addi a4, zero, 0
[0x8000b12c]:csrrw zero, fcsr, a4
[0x8000b130]:fadd.d t5, t3, s10, dyn
[0x8000b134]:csrrs a7, fcsr, zero

[0x8000b130]:fadd.d t5, t3, s10, dyn
[0x8000b134]:csrrs a7, fcsr, zero
[0x8000b138]:sw t5, 664(ra)
[0x8000b13c]:sw t6, 672(ra)
[0x8000b140]:sw t5, 680(ra)
[0x8000b144]:sw a7, 688(ra)
[0x8000b148]:lui a4, 1
[0x8000b14c]:addi a4, a4, 2048
[0x8000b150]:add a6, a6, a4
[0x8000b154]:lw t3, 1360(a6)
[0x8000b158]:sub a6, a6, a4
[0x8000b15c]:lui a4, 1
[0x8000b160]:addi a4, a4, 2048
[0x8000b164]:add a6, a6, a4
[0x8000b168]:lw t4, 1364(a6)
[0x8000b16c]:sub a6, a6, a4
[0x8000b170]:lui a4, 1
[0x8000b174]:addi a4, a4, 2048
[0x8000b178]:add a6, a6, a4
[0x8000b17c]:lw s10, 1368(a6)
[0x8000b180]:sub a6, a6, a4
[0x8000b184]:lui a4, 1
[0x8000b188]:addi a4, a4, 2048
[0x8000b18c]:add a6, a6, a4
[0x8000b190]:lw s11, 1372(a6)
[0x8000b194]:sub a6, a6, a4
[0x8000b198]:addi t3, zero, 1
[0x8000b19c]:lui t4, 524032
[0x8000b1a0]:addi s10, zero, 0
[0x8000b1a4]:addi s11, zero, 0
[0x8000b1a8]:addi a4, zero, 0
[0x8000b1ac]:csrrw zero, fcsr, a4
[0x8000b1b0]:fadd.d t5, t3, s10, dyn
[0x8000b1b4]:csrrs a7, fcsr, zero

[0x8000b1b0]:fadd.d t5, t3, s10, dyn
[0x8000b1b4]:csrrs a7, fcsr, zero
[0x8000b1b8]:sw t5, 696(ra)
[0x8000b1bc]:sw t6, 704(ra)
[0x8000b1c0]:sw t5, 712(ra)
[0x8000b1c4]:sw a7, 720(ra)
[0x8000b1c8]:lui a4, 1
[0x8000b1cc]:addi a4, a4, 2048
[0x8000b1d0]:add a6, a6, a4
[0x8000b1d4]:lw t3, 1376(a6)
[0x8000b1d8]:sub a6, a6, a4
[0x8000b1dc]:lui a4, 1
[0x8000b1e0]:addi a4, a4, 2048
[0x8000b1e4]:add a6, a6, a4
[0x8000b1e8]:lw t4, 1380(a6)
[0x8000b1ec]:sub a6, a6, a4
[0x8000b1f0]:lui a4, 1
[0x8000b1f4]:addi a4, a4, 2048
[0x8000b1f8]:add a6, a6, a4
[0x8000b1fc]:lw s10, 1384(a6)
[0x8000b200]:sub a6, a6, a4
[0x8000b204]:lui a4, 1
[0x8000b208]:addi a4, a4, 2048
[0x8000b20c]:add a6, a6, a4
[0x8000b210]:lw s11, 1388(a6)
[0x8000b214]:sub a6, a6, a4
[0x8000b218]:addi t3, zero, 1
[0x8000b21c]:lui t4, 524032
[0x8000b220]:addi s10, zero, 0
[0x8000b224]:lui s11, 524288
[0x8000b228]:addi a4, zero, 0
[0x8000b22c]:csrrw zero, fcsr, a4
[0x8000b230]:fadd.d t5, t3, s10, dyn
[0x8000b234]:csrrs a7, fcsr, zero

[0x8000b230]:fadd.d t5, t3, s10, dyn
[0x8000b234]:csrrs a7, fcsr, zero
[0x8000b238]:sw t5, 728(ra)
[0x8000b23c]:sw t6, 736(ra)
[0x8000b240]:sw t5, 744(ra)
[0x8000b244]:sw a7, 752(ra)
[0x8000b248]:lui a4, 1
[0x8000b24c]:addi a4, a4, 2048
[0x8000b250]:add a6, a6, a4
[0x8000b254]:lw t3, 1392(a6)
[0x8000b258]:sub a6, a6, a4
[0x8000b25c]:lui a4, 1
[0x8000b260]:addi a4, a4, 2048
[0x8000b264]:add a6, a6, a4
[0x8000b268]:lw t4, 1396(a6)
[0x8000b26c]:sub a6, a6, a4
[0x8000b270]:lui a4, 1
[0x8000b274]:addi a4, a4, 2048
[0x8000b278]:add a6, a6, a4
[0x8000b27c]:lw s10, 1400(a6)
[0x8000b280]:sub a6, a6, a4
[0x8000b284]:lui a4, 1
[0x8000b288]:addi a4, a4, 2048
[0x8000b28c]:add a6, a6, a4
[0x8000b290]:lw s11, 1404(a6)
[0x8000b294]:sub a6, a6, a4
[0x8000b298]:addi t3, zero, 1
[0x8000b29c]:lui t4, 524032
[0x8000b2a0]:addi s10, zero, 1
[0x8000b2a4]:addi s11, zero, 0
[0x8000b2a8]:addi a4, zero, 0
[0x8000b2ac]:csrrw zero, fcsr, a4
[0x8000b2b0]:fadd.d t5, t3, s10, dyn
[0x8000b2b4]:csrrs a7, fcsr, zero

[0x8000b2b0]:fadd.d t5, t3, s10, dyn
[0x8000b2b4]:csrrs a7, fcsr, zero
[0x8000b2b8]:sw t5, 760(ra)
[0x8000b2bc]:sw t6, 768(ra)
[0x8000b2c0]:sw t5, 776(ra)
[0x8000b2c4]:sw a7, 784(ra)
[0x8000b2c8]:lui a4, 1
[0x8000b2cc]:addi a4, a4, 2048
[0x8000b2d0]:add a6, a6, a4
[0x8000b2d4]:lw t3, 1408(a6)
[0x8000b2d8]:sub a6, a6, a4
[0x8000b2dc]:lui a4, 1
[0x8000b2e0]:addi a4, a4, 2048
[0x8000b2e4]:add a6, a6, a4
[0x8000b2e8]:lw t4, 1412(a6)
[0x8000b2ec]:sub a6, a6, a4
[0x8000b2f0]:lui a4, 1
[0x8000b2f4]:addi a4, a4, 2048
[0x8000b2f8]:add a6, a6, a4
[0x8000b2fc]:lw s10, 1416(a6)
[0x8000b300]:sub a6, a6, a4
[0x8000b304]:lui a4, 1
[0x8000b308]:addi a4, a4, 2048
[0x8000b30c]:add a6, a6, a4
[0x8000b310]:lw s11, 1420(a6)
[0x8000b314]:sub a6, a6, a4
[0x8000b318]:addi t3, zero, 1
[0x8000b31c]:lui t4, 524032
[0x8000b320]:addi s10, zero, 1
[0x8000b324]:lui s11, 524288
[0x8000b328]:addi a4, zero, 0
[0x8000b32c]:csrrw zero, fcsr, a4
[0x8000b330]:fadd.d t5, t3, s10, dyn
[0x8000b334]:csrrs a7, fcsr, zero

[0x8000b330]:fadd.d t5, t3, s10, dyn
[0x8000b334]:csrrs a7, fcsr, zero
[0x8000b338]:sw t5, 792(ra)
[0x8000b33c]:sw t6, 800(ra)
[0x8000b340]:sw t5, 808(ra)
[0x8000b344]:sw a7, 816(ra)
[0x8000b348]:lui a4, 1
[0x8000b34c]:addi a4, a4, 2048
[0x8000b350]:add a6, a6, a4
[0x8000b354]:lw t3, 1424(a6)
[0x8000b358]:sub a6, a6, a4
[0x8000b35c]:lui a4, 1
[0x8000b360]:addi a4, a4, 2048
[0x8000b364]:add a6, a6, a4
[0x8000b368]:lw t4, 1428(a6)
[0x8000b36c]:sub a6, a6, a4
[0x8000b370]:lui a4, 1
[0x8000b374]:addi a4, a4, 2048
[0x8000b378]:add a6, a6, a4
[0x8000b37c]:lw s10, 1432(a6)
[0x8000b380]:sub a6, a6, a4
[0x8000b384]:lui a4, 1
[0x8000b388]:addi a4, a4, 2048
[0x8000b38c]:add a6, a6, a4
[0x8000b390]:lw s11, 1436(a6)
[0x8000b394]:sub a6, a6, a4
[0x8000b398]:addi t3, zero, 1
[0x8000b39c]:lui t4, 524032
[0x8000b3a0]:addi s10, zero, 2
[0x8000b3a4]:addi s11, zero, 0
[0x8000b3a8]:addi a4, zero, 0
[0x8000b3ac]:csrrw zero, fcsr, a4
[0x8000b3b0]:fadd.d t5, t3, s10, dyn
[0x8000b3b4]:csrrs a7, fcsr, zero

[0x8000b3b0]:fadd.d t5, t3, s10, dyn
[0x8000b3b4]:csrrs a7, fcsr, zero
[0x8000b3b8]:sw t5, 824(ra)
[0x8000b3bc]:sw t6, 832(ra)
[0x8000b3c0]:sw t5, 840(ra)
[0x8000b3c4]:sw a7, 848(ra)
[0x8000b3c8]:lui a4, 1
[0x8000b3cc]:addi a4, a4, 2048
[0x8000b3d0]:add a6, a6, a4
[0x8000b3d4]:lw t3, 1440(a6)
[0x8000b3d8]:sub a6, a6, a4
[0x8000b3dc]:lui a4, 1
[0x8000b3e0]:addi a4, a4, 2048
[0x8000b3e4]:add a6, a6, a4
[0x8000b3e8]:lw t4, 1444(a6)
[0x8000b3ec]:sub a6, a6, a4
[0x8000b3f0]:lui a4, 1
[0x8000b3f4]:addi a4, a4, 2048
[0x8000b3f8]:add a6, a6, a4
[0x8000b3fc]:lw s10, 1448(a6)
[0x8000b400]:sub a6, a6, a4
[0x8000b404]:lui a4, 1
[0x8000b408]:addi a4, a4, 2048
[0x8000b40c]:add a6, a6, a4
[0x8000b410]:lw s11, 1452(a6)
[0x8000b414]:sub a6, a6, a4
[0x8000b418]:addi t3, zero, 1
[0x8000b41c]:lui t4, 524032
[0x8000b420]:addi s10, zero, 2
[0x8000b424]:lui s11, 524288
[0x8000b428]:addi a4, zero, 0
[0x8000b42c]:csrrw zero, fcsr, a4
[0x8000b430]:fadd.d t5, t3, s10, dyn
[0x8000b434]:csrrs a7, fcsr, zero

[0x8000b430]:fadd.d t5, t3, s10, dyn
[0x8000b434]:csrrs a7, fcsr, zero
[0x8000b438]:sw t5, 856(ra)
[0x8000b43c]:sw t6, 864(ra)
[0x8000b440]:sw t5, 872(ra)
[0x8000b444]:sw a7, 880(ra)
[0x8000b448]:lui a4, 1
[0x8000b44c]:addi a4, a4, 2048
[0x8000b450]:add a6, a6, a4
[0x8000b454]:lw t3, 1456(a6)
[0x8000b458]:sub a6, a6, a4
[0x8000b45c]:lui a4, 1
[0x8000b460]:addi a4, a4, 2048
[0x8000b464]:add a6, a6, a4
[0x8000b468]:lw t4, 1460(a6)
[0x8000b46c]:sub a6, a6, a4
[0x8000b470]:lui a4, 1
[0x8000b474]:addi a4, a4, 2048
[0x8000b478]:add a6, a6, a4
[0x8000b47c]:lw s10, 1464(a6)
[0x8000b480]:sub a6, a6, a4
[0x8000b484]:lui a4, 1
[0x8000b488]:addi a4, a4, 2048
[0x8000b48c]:add a6, a6, a4
[0x8000b490]:lw s11, 1468(a6)
[0x8000b494]:sub a6, a6, a4
[0x8000b498]:addi t3, zero, 1
[0x8000b49c]:lui t4, 524032
[0x8000b4a0]:addi s10, zero, 4095
[0x8000b4a4]:lui s11, 256
[0x8000b4a8]:addi s11, s11, 4095
[0x8000b4ac]:addi a4, zero, 0
[0x8000b4b0]:csrrw zero, fcsr, a4
[0x8000b4b4]:fadd.d t5, t3, s10, dyn
[0x8000b4b8]:csrrs a7, fcsr, zero

[0x8000b4b4]:fadd.d t5, t3, s10, dyn
[0x8000b4b8]:csrrs a7, fcsr, zero
[0x8000b4bc]:sw t5, 888(ra)
[0x8000b4c0]:sw t6, 896(ra)
[0x8000b4c4]:sw t5, 904(ra)
[0x8000b4c8]:sw a7, 912(ra)
[0x8000b4cc]:lui a4, 1
[0x8000b4d0]:addi a4, a4, 2048
[0x8000b4d4]:add a6, a6, a4
[0x8000b4d8]:lw t3, 1472(a6)
[0x8000b4dc]:sub a6, a6, a4
[0x8000b4e0]:lui a4, 1
[0x8000b4e4]:addi a4, a4, 2048
[0x8000b4e8]:add a6, a6, a4
[0x8000b4ec]:lw t4, 1476(a6)
[0x8000b4f0]:sub a6, a6, a4
[0x8000b4f4]:lui a4, 1
[0x8000b4f8]:addi a4, a4, 2048
[0x8000b4fc]:add a6, a6, a4
[0x8000b500]:lw s10, 1480(a6)
[0x8000b504]:sub a6, a6, a4
[0x8000b508]:lui a4, 1
[0x8000b50c]:addi a4, a4, 2048
[0x8000b510]:add a6, a6, a4
[0x8000b514]:lw s11, 1484(a6)
[0x8000b518]:sub a6, a6, a4
[0x8000b51c]:addi t3, zero, 1
[0x8000b520]:lui t4, 524032
[0x8000b524]:addi s10, zero, 4095
[0x8000b528]:lui s11, 524544
[0x8000b52c]:addi s11, s11, 4095
[0x8000b530]:addi a4, zero, 0
[0x8000b534]:csrrw zero, fcsr, a4
[0x8000b538]:fadd.d t5, t3, s10, dyn
[0x8000b53c]:csrrs a7, fcsr, zero

[0x8000b538]:fadd.d t5, t3, s10, dyn
[0x8000b53c]:csrrs a7, fcsr, zero
[0x8000b540]:sw t5, 920(ra)
[0x8000b544]:sw t6, 928(ra)
[0x8000b548]:sw t5, 936(ra)
[0x8000b54c]:sw a7, 944(ra)
[0x8000b550]:lui a4, 1
[0x8000b554]:addi a4, a4, 2048
[0x8000b558]:add a6, a6, a4
[0x8000b55c]:lw t3, 1488(a6)
[0x8000b560]:sub a6, a6, a4
[0x8000b564]:lui a4, 1
[0x8000b568]:addi a4, a4, 2048
[0x8000b56c]:add a6, a6, a4
[0x8000b570]:lw t4, 1492(a6)
[0x8000b574]:sub a6, a6, a4
[0x8000b578]:lui a4, 1
[0x8000b57c]:addi a4, a4, 2048
[0x8000b580]:add a6, a6, a4
[0x8000b584]:lw s10, 1496(a6)
[0x8000b588]:sub a6, a6, a4
[0x8000b58c]:lui a4, 1
[0x8000b590]:addi a4, a4, 2048
[0x8000b594]:add a6, a6, a4
[0x8000b598]:lw s11, 1500(a6)
[0x8000b59c]:sub a6, a6, a4
[0x8000b5a0]:addi t3, zero, 1
[0x8000b5a4]:lui t4, 524032
[0x8000b5a8]:addi s10, zero, 0
[0x8000b5ac]:lui s11, 256
[0x8000b5b0]:addi a4, zero, 0
[0x8000b5b4]:csrrw zero, fcsr, a4
[0x8000b5b8]:fadd.d t5, t3, s10, dyn
[0x8000b5bc]:csrrs a7, fcsr, zero

[0x8000b5b8]:fadd.d t5, t3, s10, dyn
[0x8000b5bc]:csrrs a7, fcsr, zero
[0x8000b5c0]:sw t5, 952(ra)
[0x8000b5c4]:sw t6, 960(ra)
[0x8000b5c8]:sw t5, 968(ra)
[0x8000b5cc]:sw a7, 976(ra)
[0x8000b5d0]:lui a4, 1
[0x8000b5d4]:addi a4, a4, 2048
[0x8000b5d8]:add a6, a6, a4
[0x8000b5dc]:lw t3, 1504(a6)
[0x8000b5e0]:sub a6, a6, a4
[0x8000b5e4]:lui a4, 1
[0x8000b5e8]:addi a4, a4, 2048
[0x8000b5ec]:add a6, a6, a4
[0x8000b5f0]:lw t4, 1508(a6)
[0x8000b5f4]:sub a6, a6, a4
[0x8000b5f8]:lui a4, 1
[0x8000b5fc]:addi a4, a4, 2048
[0x8000b600]:add a6, a6, a4
[0x8000b604]:lw s10, 1512(a6)
[0x8000b608]:sub a6, a6, a4
[0x8000b60c]:lui a4, 1
[0x8000b610]:addi a4, a4, 2048
[0x8000b614]:add a6, a6, a4
[0x8000b618]:lw s11, 1516(a6)
[0x8000b61c]:sub a6, a6, a4
[0x8000b620]:addi t3, zero, 1
[0x8000b624]:lui t4, 524032
[0x8000b628]:addi s10, zero, 0
[0x8000b62c]:lui s11, 524544
[0x8000b630]:addi a4, zero, 0
[0x8000b634]:csrrw zero, fcsr, a4
[0x8000b638]:fadd.d t5, t3, s10, dyn
[0x8000b63c]:csrrs a7, fcsr, zero

[0x8000b638]:fadd.d t5, t3, s10, dyn
[0x8000b63c]:csrrs a7, fcsr, zero
[0x8000b640]:sw t5, 984(ra)
[0x8000b644]:sw t6, 992(ra)
[0x8000b648]:sw t5, 1000(ra)
[0x8000b64c]:sw a7, 1008(ra)
[0x8000b650]:lui a4, 1
[0x8000b654]:addi a4, a4, 2048
[0x8000b658]:add a6, a6, a4
[0x8000b65c]:lw t3, 1520(a6)
[0x8000b660]:sub a6, a6, a4
[0x8000b664]:lui a4, 1
[0x8000b668]:addi a4, a4, 2048
[0x8000b66c]:add a6, a6, a4
[0x8000b670]:lw t4, 1524(a6)
[0x8000b674]:sub a6, a6, a4
[0x8000b678]:lui a4, 1
[0x8000b67c]:addi a4, a4, 2048
[0x8000b680]:add a6, a6, a4
[0x8000b684]:lw s10, 1528(a6)
[0x8000b688]:sub a6, a6, a4
[0x8000b68c]:lui a4, 1
[0x8000b690]:addi a4, a4, 2048
[0x8000b694]:add a6, a6, a4
[0x8000b698]:lw s11, 1532(a6)
[0x8000b69c]:sub a6, a6, a4
[0x8000b6a0]:addi t3, zero, 1
[0x8000b6a4]:lui t4, 524032
[0x8000b6a8]:addi s10, zero, 2
[0x8000b6ac]:lui s11, 256
[0x8000b6b0]:addi a4, zero, 0
[0x8000b6b4]:csrrw zero, fcsr, a4
[0x8000b6b8]:fadd.d t5, t3, s10, dyn
[0x8000b6bc]:csrrs a7, fcsr, zero

[0x8000b6b8]:fadd.d t5, t3, s10, dyn
[0x8000b6bc]:csrrs a7, fcsr, zero
[0x8000b6c0]:sw t5, 1016(ra)
[0x8000b6c4]:sw t6, 1024(ra)
[0x8000b6c8]:sw t5, 1032(ra)
[0x8000b6cc]:sw a7, 1040(ra)
[0x8000b6d0]:lui a4, 1
[0x8000b6d4]:addi a4, a4, 2048
[0x8000b6d8]:add a6, a6, a4
[0x8000b6dc]:lw t3, 1536(a6)
[0x8000b6e0]:sub a6, a6, a4
[0x8000b6e4]:lui a4, 1
[0x8000b6e8]:addi a4, a4, 2048
[0x8000b6ec]:add a6, a6, a4
[0x8000b6f0]:lw t4, 1540(a6)
[0x8000b6f4]:sub a6, a6, a4
[0x8000b6f8]:lui a4, 1
[0x8000b6fc]:addi a4, a4, 2048
[0x8000b700]:add a6, a6, a4
[0x8000b704]:lw s10, 1544(a6)
[0x8000b708]:sub a6, a6, a4
[0x8000b70c]:lui a4, 1
[0x8000b710]:addi a4, a4, 2048
[0x8000b714]:add a6, a6, a4
[0x8000b718]:lw s11, 1548(a6)
[0x8000b71c]:sub a6, a6, a4
[0x8000b720]:addi t3, zero, 1
[0x8000b724]:lui t4, 524032
[0x8000b728]:addi s10, zero, 2
[0x8000b72c]:lui s11, 524544
[0x8000b730]:addi a4, zero, 0
[0x8000b734]:csrrw zero, fcsr, a4
[0x8000b738]:fadd.d t5, t3, s10, dyn
[0x8000b73c]:csrrs a7, fcsr, zero

[0x8000b738]:fadd.d t5, t3, s10, dyn
[0x8000b73c]:csrrs a7, fcsr, zero
[0x8000b740]:sw t5, 1048(ra)
[0x8000b744]:sw t6, 1056(ra)
[0x8000b748]:sw t5, 1064(ra)
[0x8000b74c]:sw a7, 1072(ra)
[0x8000b750]:lui a4, 1
[0x8000b754]:addi a4, a4, 2048
[0x8000b758]:add a6, a6, a4
[0x8000b75c]:lw t3, 1552(a6)
[0x8000b760]:sub a6, a6, a4
[0x8000b764]:lui a4, 1
[0x8000b768]:addi a4, a4, 2048
[0x8000b76c]:add a6, a6, a4
[0x8000b770]:lw t4, 1556(a6)
[0x8000b774]:sub a6, a6, a4
[0x8000b778]:lui a4, 1
[0x8000b77c]:addi a4, a4, 2048
[0x8000b780]:add a6, a6, a4
[0x8000b784]:lw s10, 1560(a6)
[0x8000b788]:sub a6, a6, a4
[0x8000b78c]:lui a4, 1
[0x8000b790]:addi a4, a4, 2048
[0x8000b794]:add a6, a6, a4
[0x8000b798]:lw s11, 1564(a6)
[0x8000b79c]:sub a6, a6, a4
[0x8000b7a0]:addi t3, zero, 1
[0x8000b7a4]:lui t4, 524032
[0x8000b7a8]:addi s10, zero, 4095
[0x8000b7ac]:lui s11, 524032
[0x8000b7b0]:addi s11, s11, 4095
[0x8000b7b4]:addi a4, zero, 0
[0x8000b7b8]:csrrw zero, fcsr, a4
[0x8000b7bc]:fadd.d t5, t3, s10, dyn
[0x8000b7c0]:csrrs a7, fcsr, zero

[0x8000b7bc]:fadd.d t5, t3, s10, dyn
[0x8000b7c0]:csrrs a7, fcsr, zero
[0x8000b7c4]:sw t5, 1080(ra)
[0x8000b7c8]:sw t6, 1088(ra)
[0x8000b7cc]:sw t5, 1096(ra)
[0x8000b7d0]:sw a7, 1104(ra)
[0x8000b7d4]:lui a4, 1
[0x8000b7d8]:addi a4, a4, 2048
[0x8000b7dc]:add a6, a6, a4
[0x8000b7e0]:lw t3, 1568(a6)
[0x8000b7e4]:sub a6, a6, a4
[0x8000b7e8]:lui a4, 1
[0x8000b7ec]:addi a4, a4, 2048
[0x8000b7f0]:add a6, a6, a4
[0x8000b7f4]:lw t4, 1572(a6)
[0x8000b7f8]:sub a6, a6, a4
[0x8000b7fc]:lui a4, 1
[0x8000b800]:addi a4, a4, 2048
[0x8000b804]:add a6, a6, a4
[0x8000b808]:lw s10, 1576(a6)
[0x8000b80c]:sub a6, a6, a4
[0x8000b810]:lui a4, 1
[0x8000b814]:addi a4, a4, 2048
[0x8000b818]:add a6, a6, a4
[0x8000b81c]:lw s11, 1580(a6)
[0x8000b820]:sub a6, a6, a4
[0x8000b824]:addi t3, zero, 1
[0x8000b828]:lui t4, 524032
[0x8000b82c]:addi s10, zero, 4095
[0x8000b830]:lui s11, 1048320
[0x8000b834]:addi s11, s11, 4095
[0x8000b838]:addi a4, zero, 0
[0x8000b83c]:csrrw zero, fcsr, a4
[0x8000b840]:fadd.d t5, t3, s10, dyn
[0x8000b844]:csrrs a7, fcsr, zero

[0x8000b840]:fadd.d t5, t3, s10, dyn
[0x8000b844]:csrrs a7, fcsr, zero
[0x8000b848]:sw t5, 1112(ra)
[0x8000b84c]:sw t6, 1120(ra)
[0x8000b850]:sw t5, 1128(ra)
[0x8000b854]:sw a7, 1136(ra)
[0x8000b858]:lui a4, 1
[0x8000b85c]:addi a4, a4, 2048
[0x8000b860]:add a6, a6, a4
[0x8000b864]:lw t3, 1584(a6)
[0x8000b868]:sub a6, a6, a4
[0x8000b86c]:lui a4, 1
[0x8000b870]:addi a4, a4, 2048
[0x8000b874]:add a6, a6, a4
[0x8000b878]:lw t4, 1588(a6)
[0x8000b87c]:sub a6, a6, a4
[0x8000b880]:lui a4, 1
[0x8000b884]:addi a4, a4, 2048
[0x8000b888]:add a6, a6, a4
[0x8000b88c]:lw s10, 1592(a6)
[0x8000b890]:sub a6, a6, a4
[0x8000b894]:lui a4, 1
[0x8000b898]:addi a4, a4, 2048
[0x8000b89c]:add a6, a6, a4
[0x8000b8a0]:lw s11, 1596(a6)
[0x8000b8a4]:sub a6, a6, a4
[0x8000b8a8]:addi t3, zero, 1
[0x8000b8ac]:lui t4, 524032
[0x8000b8b0]:addi s10, zero, 0
[0x8000b8b4]:lui s11, 524032
[0x8000b8b8]:addi a4, zero, 0
[0x8000b8bc]:csrrw zero, fcsr, a4
[0x8000b8c0]:fadd.d t5, t3, s10, dyn
[0x8000b8c4]:csrrs a7, fcsr, zero

[0x8000b8c0]:fadd.d t5, t3, s10, dyn
[0x8000b8c4]:csrrs a7, fcsr, zero
[0x8000b8c8]:sw t5, 1144(ra)
[0x8000b8cc]:sw t6, 1152(ra)
[0x8000b8d0]:sw t5, 1160(ra)
[0x8000b8d4]:sw a7, 1168(ra)
[0x8000b8d8]:lui a4, 1
[0x8000b8dc]:addi a4, a4, 2048
[0x8000b8e0]:add a6, a6, a4
[0x8000b8e4]:lw t3, 1600(a6)
[0x8000b8e8]:sub a6, a6, a4
[0x8000b8ec]:lui a4, 1
[0x8000b8f0]:addi a4, a4, 2048
[0x8000b8f4]:add a6, a6, a4
[0x8000b8f8]:lw t4, 1604(a6)
[0x8000b8fc]:sub a6, a6, a4
[0x8000b900]:lui a4, 1
[0x8000b904]:addi a4, a4, 2048
[0x8000b908]:add a6, a6, a4
[0x8000b90c]:lw s10, 1608(a6)
[0x8000b910]:sub a6, a6, a4
[0x8000b914]:lui a4, 1
[0x8000b918]:addi a4, a4, 2048
[0x8000b91c]:add a6, a6, a4
[0x8000b920]:lw s11, 1612(a6)
[0x8000b924]:sub a6, a6, a4
[0x8000b928]:addi t3, zero, 1
[0x8000b92c]:lui t4, 524032
[0x8000b930]:addi s10, zero, 0
[0x8000b934]:lui s11, 1048320
[0x8000b938]:addi a4, zero, 0
[0x8000b93c]:csrrw zero, fcsr, a4
[0x8000b940]:fadd.d t5, t3, s10, dyn
[0x8000b944]:csrrs a7, fcsr, zero

[0x8000b940]:fadd.d t5, t3, s10, dyn
[0x8000b944]:csrrs a7, fcsr, zero
[0x8000b948]:sw t5, 1176(ra)
[0x8000b94c]:sw t6, 1184(ra)
[0x8000b950]:sw t5, 1192(ra)
[0x8000b954]:sw a7, 1200(ra)
[0x8000b958]:lui a4, 1
[0x8000b95c]:addi a4, a4, 2048
[0x8000b960]:add a6, a6, a4
[0x8000b964]:lw t3, 1616(a6)
[0x8000b968]:sub a6, a6, a4
[0x8000b96c]:lui a4, 1
[0x8000b970]:addi a4, a4, 2048
[0x8000b974]:add a6, a6, a4
[0x8000b978]:lw t4, 1620(a6)
[0x8000b97c]:sub a6, a6, a4
[0x8000b980]:lui a4, 1
[0x8000b984]:addi a4, a4, 2048
[0x8000b988]:add a6, a6, a4
[0x8000b98c]:lw s10, 1624(a6)
[0x8000b990]:sub a6, a6, a4
[0x8000b994]:lui a4, 1
[0x8000b998]:addi a4, a4, 2048
[0x8000b99c]:add a6, a6, a4
[0x8000b9a0]:lw s11, 1628(a6)
[0x8000b9a4]:sub a6, a6, a4
[0x8000b9a8]:addi t3, zero, 1
[0x8000b9ac]:lui t4, 524032
[0x8000b9b0]:addi s10, zero, 0
[0x8000b9b4]:lui s11, 524160
[0x8000b9b8]:addi a4, zero, 0
[0x8000b9bc]:csrrw zero, fcsr, a4
[0x8000b9c0]:fadd.d t5, t3, s10, dyn
[0x8000b9c4]:csrrs a7, fcsr, zero

[0x8000b9c0]:fadd.d t5, t3, s10, dyn
[0x8000b9c4]:csrrs a7, fcsr, zero
[0x8000b9c8]:sw t5, 1208(ra)
[0x8000b9cc]:sw t6, 1216(ra)
[0x8000b9d0]:sw t5, 1224(ra)
[0x8000b9d4]:sw a7, 1232(ra)
[0x8000b9d8]:lui a4, 1
[0x8000b9dc]:addi a4, a4, 2048
[0x8000b9e0]:add a6, a6, a4
[0x8000b9e4]:lw t3, 1632(a6)
[0x8000b9e8]:sub a6, a6, a4
[0x8000b9ec]:lui a4, 1
[0x8000b9f0]:addi a4, a4, 2048
[0x8000b9f4]:add a6, a6, a4
[0x8000b9f8]:lw t4, 1636(a6)
[0x8000b9fc]:sub a6, a6, a4
[0x8000ba00]:lui a4, 1
[0x8000ba04]:addi a4, a4, 2048
[0x8000ba08]:add a6, a6, a4
[0x8000ba0c]:lw s10, 1640(a6)
[0x8000ba10]:sub a6, a6, a4
[0x8000ba14]:lui a4, 1
[0x8000ba18]:addi a4, a4, 2048
[0x8000ba1c]:add a6, a6, a4
[0x8000ba20]:lw s11, 1644(a6)
[0x8000ba24]:sub a6, a6, a4
[0x8000ba28]:addi t3, zero, 1
[0x8000ba2c]:lui t4, 524032
[0x8000ba30]:addi s10, zero, 0
[0x8000ba34]:lui s11, 1048448
[0x8000ba38]:addi a4, zero, 0
[0x8000ba3c]:csrrw zero, fcsr, a4
[0x8000ba40]:fadd.d t5, t3, s10, dyn
[0x8000ba44]:csrrs a7, fcsr, zero

[0x8000ba40]:fadd.d t5, t3, s10, dyn
[0x8000ba44]:csrrs a7, fcsr, zero
[0x8000ba48]:sw t5, 1240(ra)
[0x8000ba4c]:sw t6, 1248(ra)
[0x8000ba50]:sw t5, 1256(ra)
[0x8000ba54]:sw a7, 1264(ra)
[0x8000ba58]:lui a4, 1
[0x8000ba5c]:addi a4, a4, 2048
[0x8000ba60]:add a6, a6, a4
[0x8000ba64]:lw t3, 1648(a6)
[0x8000ba68]:sub a6, a6, a4
[0x8000ba6c]:lui a4, 1
[0x8000ba70]:addi a4, a4, 2048
[0x8000ba74]:add a6, a6, a4
[0x8000ba78]:lw t4, 1652(a6)
[0x8000ba7c]:sub a6, a6, a4
[0x8000ba80]:lui a4, 1
[0x8000ba84]:addi a4, a4, 2048
[0x8000ba88]:add a6, a6, a4
[0x8000ba8c]:lw s10, 1656(a6)
[0x8000ba90]:sub a6, a6, a4
[0x8000ba94]:lui a4, 1
[0x8000ba98]:addi a4, a4, 2048
[0x8000ba9c]:add a6, a6, a4
[0x8000baa0]:lw s11, 1660(a6)
[0x8000baa4]:sub a6, a6, a4
[0x8000baa8]:addi t3, zero, 1
[0x8000baac]:lui t4, 524032
[0x8000bab0]:addi s10, zero, 1
[0x8000bab4]:lui s11, 524160
[0x8000bab8]:addi a4, zero, 0
[0x8000babc]:csrrw zero, fcsr, a4
[0x8000bac0]:fadd.d t5, t3, s10, dyn
[0x8000bac4]:csrrs a7, fcsr, zero

[0x8000bac0]:fadd.d t5, t3, s10, dyn
[0x8000bac4]:csrrs a7, fcsr, zero
[0x8000bac8]:sw t5, 1272(ra)
[0x8000bacc]:sw t6, 1280(ra)
[0x8000bad0]:sw t5, 1288(ra)
[0x8000bad4]:sw a7, 1296(ra)
[0x8000bad8]:lui a4, 1
[0x8000badc]:addi a4, a4, 2048
[0x8000bae0]:add a6, a6, a4
[0x8000bae4]:lw t3, 1664(a6)
[0x8000bae8]:sub a6, a6, a4
[0x8000baec]:lui a4, 1
[0x8000baf0]:addi a4, a4, 2048
[0x8000baf4]:add a6, a6, a4
[0x8000baf8]:lw t4, 1668(a6)
[0x8000bafc]:sub a6, a6, a4
[0x8000bb00]:lui a4, 1
[0x8000bb04]:addi a4, a4, 2048
[0x8000bb08]:add a6, a6, a4
[0x8000bb0c]:lw s10, 1672(a6)
[0x8000bb10]:sub a6, a6, a4
[0x8000bb14]:lui a4, 1
[0x8000bb18]:addi a4, a4, 2048
[0x8000bb1c]:add a6, a6, a4
[0x8000bb20]:lw s11, 1676(a6)
[0x8000bb24]:sub a6, a6, a4
[0x8000bb28]:addi t3, zero, 1
[0x8000bb2c]:lui t4, 524032
[0x8000bb30]:addi s10, zero, 1
[0x8000bb34]:lui s11, 1048448
[0x8000bb38]:addi a4, zero, 0
[0x8000bb3c]:csrrw zero, fcsr, a4
[0x8000bb40]:fadd.d t5, t3, s10, dyn
[0x8000bb44]:csrrs a7, fcsr, zero

[0x8000bb40]:fadd.d t5, t3, s10, dyn
[0x8000bb44]:csrrs a7, fcsr, zero
[0x8000bb48]:sw t5, 1304(ra)
[0x8000bb4c]:sw t6, 1312(ra)
[0x8000bb50]:sw t5, 1320(ra)
[0x8000bb54]:sw a7, 1328(ra)
[0x8000bb58]:lui a4, 1
[0x8000bb5c]:addi a4, a4, 2048
[0x8000bb60]:add a6, a6, a4
[0x8000bb64]:lw t3, 1680(a6)
[0x8000bb68]:sub a6, a6, a4
[0x8000bb6c]:lui a4, 1
[0x8000bb70]:addi a4, a4, 2048
[0x8000bb74]:add a6, a6, a4
[0x8000bb78]:lw t4, 1684(a6)
[0x8000bb7c]:sub a6, a6, a4
[0x8000bb80]:lui a4, 1
[0x8000bb84]:addi a4, a4, 2048
[0x8000bb88]:add a6, a6, a4
[0x8000bb8c]:lw s10, 1688(a6)
[0x8000bb90]:sub a6, a6, a4
[0x8000bb94]:lui a4, 1
[0x8000bb98]:addi a4, a4, 2048
[0x8000bb9c]:add a6, a6, a4
[0x8000bba0]:lw s11, 1692(a6)
[0x8000bba4]:sub a6, a6, a4
[0x8000bba8]:addi t3, zero, 1
[0x8000bbac]:lui t4, 524032
[0x8000bbb0]:addi s10, zero, 1
[0x8000bbb4]:lui s11, 524032
[0x8000bbb8]:addi a4, zero, 0
[0x8000bbbc]:csrrw zero, fcsr, a4
[0x8000bbc0]:fadd.d t5, t3, s10, dyn
[0x8000bbc4]:csrrs a7, fcsr, zero

[0x8000bbc0]:fadd.d t5, t3, s10, dyn
[0x8000bbc4]:csrrs a7, fcsr, zero
[0x8000bbc8]:sw t5, 1336(ra)
[0x8000bbcc]:sw t6, 1344(ra)
[0x8000bbd0]:sw t5, 1352(ra)
[0x8000bbd4]:sw a7, 1360(ra)
[0x8000bbd8]:lui a4, 1
[0x8000bbdc]:addi a4, a4, 2048
[0x8000bbe0]:add a6, a6, a4
[0x8000bbe4]:lw t3, 1696(a6)
[0x8000bbe8]:sub a6, a6, a4
[0x8000bbec]:lui a4, 1
[0x8000bbf0]:addi a4, a4, 2048
[0x8000bbf4]:add a6, a6, a4
[0x8000bbf8]:lw t4, 1700(a6)
[0x8000bbfc]:sub a6, a6, a4
[0x8000bc00]:lui a4, 1
[0x8000bc04]:addi a4, a4, 2048
[0x8000bc08]:add a6, a6, a4
[0x8000bc0c]:lw s10, 1704(a6)
[0x8000bc10]:sub a6, a6, a4
[0x8000bc14]:lui a4, 1
[0x8000bc18]:addi a4, a4, 2048
[0x8000bc1c]:add a6, a6, a4
[0x8000bc20]:lw s11, 1708(a6)
[0x8000bc24]:sub a6, a6, a4
[0x8000bc28]:addi t3, zero, 1
[0x8000bc2c]:lui t4, 524032
[0x8000bc30]:addi s10, zero, 1
[0x8000bc34]:lui s11, 1048320
[0x8000bc38]:addi a4, zero, 0
[0x8000bc3c]:csrrw zero, fcsr, a4
[0x8000bc40]:fadd.d t5, t3, s10, dyn
[0x8000bc44]:csrrs a7, fcsr, zero

[0x8000bc40]:fadd.d t5, t3, s10, dyn
[0x8000bc44]:csrrs a7, fcsr, zero
[0x8000bc48]:sw t5, 1368(ra)
[0x8000bc4c]:sw t6, 1376(ra)
[0x8000bc50]:sw t5, 1384(ra)
[0x8000bc54]:sw a7, 1392(ra)
[0x8000bc58]:lui a4, 1
[0x8000bc5c]:addi a4, a4, 2048
[0x8000bc60]:add a6, a6, a4
[0x8000bc64]:lw t3, 1712(a6)
[0x8000bc68]:sub a6, a6, a4
[0x8000bc6c]:lui a4, 1
[0x8000bc70]:addi a4, a4, 2048
[0x8000bc74]:add a6, a6, a4
[0x8000bc78]:lw t4, 1716(a6)
[0x8000bc7c]:sub a6, a6, a4
[0x8000bc80]:lui a4, 1
[0x8000bc84]:addi a4, a4, 2048
[0x8000bc88]:add a6, a6, a4
[0x8000bc8c]:lw s10, 1720(a6)
[0x8000bc90]:sub a6, a6, a4
[0x8000bc94]:lui a4, 1
[0x8000bc98]:addi a4, a4, 2048
[0x8000bc9c]:add a6, a6, a4
[0x8000bca0]:lw s11, 1724(a6)
[0x8000bca4]:sub a6, a6, a4
[0x8000bca8]:addi t3, zero, 1
[0x8000bcac]:lui t4, 524032
[0x8000bcb0]:addi s10, zero, 0
[0x8000bcb4]:lui s11, 261888
[0x8000bcb8]:addi a4, zero, 0
[0x8000bcbc]:csrrw zero, fcsr, a4
[0x8000bcc0]:fadd.d t5, t3, s10, dyn
[0x8000bcc4]:csrrs a7, fcsr, zero

[0x8000bcc0]:fadd.d t5, t3, s10, dyn
[0x8000bcc4]:csrrs a7, fcsr, zero
[0x8000bcc8]:sw t5, 1400(ra)
[0x8000bccc]:sw t6, 1408(ra)
[0x8000bcd0]:sw t5, 1416(ra)
[0x8000bcd4]:sw a7, 1424(ra)
[0x8000bcd8]:lui a4, 1
[0x8000bcdc]:addi a4, a4, 2048
[0x8000bce0]:add a6, a6, a4
[0x8000bce4]:lw t3, 1728(a6)
[0x8000bce8]:sub a6, a6, a4
[0x8000bcec]:lui a4, 1
[0x8000bcf0]:addi a4, a4, 2048
[0x8000bcf4]:add a6, a6, a4
[0x8000bcf8]:lw t4, 1732(a6)
[0x8000bcfc]:sub a6, a6, a4
[0x8000bd00]:lui a4, 1
[0x8000bd04]:addi a4, a4, 2048
[0x8000bd08]:add a6, a6, a4
[0x8000bd0c]:lw s10, 1736(a6)
[0x8000bd10]:sub a6, a6, a4
[0x8000bd14]:lui a4, 1
[0x8000bd18]:addi a4, a4, 2048
[0x8000bd1c]:add a6, a6, a4
[0x8000bd20]:lw s11, 1740(a6)
[0x8000bd24]:sub a6, a6, a4
[0x8000bd28]:addi t3, zero, 1
[0x8000bd2c]:lui t4, 524032
[0x8000bd30]:addi s10, zero, 0
[0x8000bd34]:lui s11, 784384
[0x8000bd38]:addi a4, zero, 0
[0x8000bd3c]:csrrw zero, fcsr, a4
[0x8000bd40]:fadd.d t5, t3, s10, dyn
[0x8000bd44]:csrrs a7, fcsr, zero

[0x8000bd40]:fadd.d t5, t3, s10, dyn
[0x8000bd44]:csrrs a7, fcsr, zero
[0x8000bd48]:sw t5, 1432(ra)
[0x8000bd4c]:sw t6, 1440(ra)
[0x8000bd50]:sw t5, 1448(ra)
[0x8000bd54]:sw a7, 1456(ra)
[0x8000bd58]:lui a4, 1
[0x8000bd5c]:addi a4, a4, 2048
[0x8000bd60]:add a6, a6, a4
[0x8000bd64]:lw t3, 1744(a6)
[0x8000bd68]:sub a6, a6, a4
[0x8000bd6c]:lui a4, 1
[0x8000bd70]:addi a4, a4, 2048
[0x8000bd74]:add a6, a6, a4
[0x8000bd78]:lw t4, 1748(a6)
[0x8000bd7c]:sub a6, a6, a4
[0x8000bd80]:lui a4, 1
[0x8000bd84]:addi a4, a4, 2048
[0x8000bd88]:add a6, a6, a4
[0x8000bd8c]:lw s10, 1752(a6)
[0x8000bd90]:sub a6, a6, a4
[0x8000bd94]:lui a4, 1
[0x8000bd98]:addi a4, a4, 2048
[0x8000bd9c]:add a6, a6, a4
[0x8000bda0]:lw s11, 1756(a6)
[0x8000bda4]:sub a6, a6, a4
[0x8000bda8]:addi t3, zero, 1
[0x8000bdac]:lui t4, 1048320
[0x8000bdb0]:addi s10, zero, 0
[0x8000bdb4]:addi s11, zero, 0
[0x8000bdb8]:addi a4, zero, 0
[0x8000bdbc]:csrrw zero, fcsr, a4
[0x8000bdc0]:fadd.d t5, t3, s10, dyn
[0x8000bdc4]:csrrs a7, fcsr, zero

[0x8000bdc0]:fadd.d t5, t3, s10, dyn
[0x8000bdc4]:csrrs a7, fcsr, zero
[0x8000bdc8]:sw t5, 1464(ra)
[0x8000bdcc]:sw t6, 1472(ra)
[0x8000bdd0]:sw t5, 1480(ra)
[0x8000bdd4]:sw a7, 1488(ra)
[0x8000bdd8]:lui a4, 1
[0x8000bddc]:addi a4, a4, 2048
[0x8000bde0]:add a6, a6, a4
[0x8000bde4]:lw t3, 1760(a6)
[0x8000bde8]:sub a6, a6, a4
[0x8000bdec]:lui a4, 1
[0x8000bdf0]:addi a4, a4, 2048
[0x8000bdf4]:add a6, a6, a4
[0x8000bdf8]:lw t4, 1764(a6)
[0x8000bdfc]:sub a6, a6, a4
[0x8000be00]:lui a4, 1
[0x8000be04]:addi a4, a4, 2048
[0x8000be08]:add a6, a6, a4
[0x8000be0c]:lw s10, 1768(a6)
[0x8000be10]:sub a6, a6, a4
[0x8000be14]:lui a4, 1
[0x8000be18]:addi a4, a4, 2048
[0x8000be1c]:add a6, a6, a4
[0x8000be20]:lw s11, 1772(a6)
[0x8000be24]:sub a6, a6, a4
[0x8000be28]:addi t3, zero, 1
[0x8000be2c]:lui t4, 1048320
[0x8000be30]:addi s10, zero, 0
[0x8000be34]:lui s11, 524288
[0x8000be38]:addi a4, zero, 0
[0x8000be3c]:csrrw zero, fcsr, a4
[0x8000be40]:fadd.d t5, t3, s10, dyn
[0x8000be44]:csrrs a7, fcsr, zero

[0x8000be40]:fadd.d t5, t3, s10, dyn
[0x8000be44]:csrrs a7, fcsr, zero
[0x8000be48]:sw t5, 1496(ra)
[0x8000be4c]:sw t6, 1504(ra)
[0x8000be50]:sw t5, 1512(ra)
[0x8000be54]:sw a7, 1520(ra)
[0x8000be58]:lui a4, 1
[0x8000be5c]:addi a4, a4, 2048
[0x8000be60]:add a6, a6, a4
[0x8000be64]:lw t3, 1776(a6)
[0x8000be68]:sub a6, a6, a4
[0x8000be6c]:lui a4, 1
[0x8000be70]:addi a4, a4, 2048
[0x8000be74]:add a6, a6, a4
[0x8000be78]:lw t4, 1780(a6)
[0x8000be7c]:sub a6, a6, a4
[0x8000be80]:lui a4, 1
[0x8000be84]:addi a4, a4, 2048
[0x8000be88]:add a6, a6, a4
[0x8000be8c]:lw s10, 1784(a6)
[0x8000be90]:sub a6, a6, a4
[0x8000be94]:lui a4, 1
[0x8000be98]:addi a4, a4, 2048
[0x8000be9c]:add a6, a6, a4
[0x8000bea0]:lw s11, 1788(a6)
[0x8000bea4]:sub a6, a6, a4
[0x8000bea8]:addi t3, zero, 1
[0x8000beac]:lui t4, 1048320
[0x8000beb0]:addi s10, zero, 1
[0x8000beb4]:addi s11, zero, 0
[0x8000beb8]:addi a4, zero, 0
[0x8000bebc]:csrrw zero, fcsr, a4
[0x8000bec0]:fadd.d t5, t3, s10, dyn
[0x8000bec4]:csrrs a7, fcsr, zero

[0x8000bec0]:fadd.d t5, t3, s10, dyn
[0x8000bec4]:csrrs a7, fcsr, zero
[0x8000bec8]:sw t5, 1528(ra)
[0x8000becc]:sw t6, 1536(ra)
[0x8000bed0]:sw t5, 1544(ra)
[0x8000bed4]:sw a7, 1552(ra)
[0x8000bed8]:lui a4, 1
[0x8000bedc]:addi a4, a4, 2048
[0x8000bee0]:add a6, a6, a4
[0x8000bee4]:lw t3, 1792(a6)
[0x8000bee8]:sub a6, a6, a4
[0x8000beec]:lui a4, 1
[0x8000bef0]:addi a4, a4, 2048
[0x8000bef4]:add a6, a6, a4
[0x8000bef8]:lw t4, 1796(a6)
[0x8000befc]:sub a6, a6, a4
[0x8000bf00]:lui a4, 1
[0x8000bf04]:addi a4, a4, 2048
[0x8000bf08]:add a6, a6, a4
[0x8000bf0c]:lw s10, 1800(a6)
[0x8000bf10]:sub a6, a6, a4
[0x8000bf14]:lui a4, 1
[0x8000bf18]:addi a4, a4, 2048
[0x8000bf1c]:add a6, a6, a4
[0x8000bf20]:lw s11, 1804(a6)
[0x8000bf24]:sub a6, a6, a4
[0x8000bf28]:addi t3, zero, 1
[0x8000bf2c]:lui t4, 1048320
[0x8000bf30]:addi s10, zero, 1
[0x8000bf34]:lui s11, 524288
[0x8000bf38]:addi a4, zero, 0
[0x8000bf3c]:csrrw zero, fcsr, a4
[0x8000bf40]:fadd.d t5, t3, s10, dyn
[0x8000bf44]:csrrs a7, fcsr, zero

[0x8000bf40]:fadd.d t5, t3, s10, dyn
[0x8000bf44]:csrrs a7, fcsr, zero
[0x8000bf48]:sw t5, 1560(ra)
[0x8000bf4c]:sw t6, 1568(ra)
[0x8000bf50]:sw t5, 1576(ra)
[0x8000bf54]:sw a7, 1584(ra)
[0x8000bf58]:lui a4, 1
[0x8000bf5c]:addi a4, a4, 2048
[0x8000bf60]:add a6, a6, a4
[0x8000bf64]:lw t3, 1808(a6)
[0x8000bf68]:sub a6, a6, a4
[0x8000bf6c]:lui a4, 1
[0x8000bf70]:addi a4, a4, 2048
[0x8000bf74]:add a6, a6, a4
[0x8000bf78]:lw t4, 1812(a6)
[0x8000bf7c]:sub a6, a6, a4
[0x8000bf80]:lui a4, 1
[0x8000bf84]:addi a4, a4, 2048
[0x8000bf88]:add a6, a6, a4
[0x8000bf8c]:lw s10, 1816(a6)
[0x8000bf90]:sub a6, a6, a4
[0x8000bf94]:lui a4, 1
[0x8000bf98]:addi a4, a4, 2048
[0x8000bf9c]:add a6, a6, a4
[0x8000bfa0]:lw s11, 1820(a6)
[0x8000bfa4]:sub a6, a6, a4
[0x8000bfa8]:addi t3, zero, 1
[0x8000bfac]:lui t4, 1048320
[0x8000bfb0]:addi s10, zero, 2
[0x8000bfb4]:addi s11, zero, 0
[0x8000bfb8]:addi a4, zero, 0
[0x8000bfbc]:csrrw zero, fcsr, a4
[0x8000bfc0]:fadd.d t5, t3, s10, dyn
[0x8000bfc4]:csrrs a7, fcsr, zero

[0x8000bfc0]:fadd.d t5, t3, s10, dyn
[0x8000bfc4]:csrrs a7, fcsr, zero
[0x8000bfc8]:sw t5, 1592(ra)
[0x8000bfcc]:sw t6, 1600(ra)
[0x8000bfd0]:sw t5, 1608(ra)
[0x8000bfd4]:sw a7, 1616(ra)
[0x8000bfd8]:lui a4, 1
[0x8000bfdc]:addi a4, a4, 2048
[0x8000bfe0]:add a6, a6, a4
[0x8000bfe4]:lw t3, 1824(a6)
[0x8000bfe8]:sub a6, a6, a4
[0x8000bfec]:lui a4, 1
[0x8000bff0]:addi a4, a4, 2048
[0x8000bff4]:add a6, a6, a4
[0x8000bff8]:lw t4, 1828(a6)
[0x8000bffc]:sub a6, a6, a4
[0x8000c000]:lui a4, 1
[0x8000c004]:addi a4, a4, 2048
[0x8000c008]:add a6, a6, a4
[0x8000c00c]:lw s10, 1832(a6)
[0x8000c010]:sub a6, a6, a4
[0x8000c014]:lui a4, 1
[0x8000c018]:addi a4, a4, 2048
[0x8000c01c]:add a6, a6, a4
[0x8000c020]:lw s11, 1836(a6)
[0x8000c024]:sub a6, a6, a4
[0x8000c028]:addi t3, zero, 1
[0x8000c02c]:lui t4, 1048320
[0x8000c030]:addi s10, zero, 2
[0x8000c034]:lui s11, 524288
[0x8000c038]:addi a4, zero, 0
[0x8000c03c]:csrrw zero, fcsr, a4
[0x8000c040]:fadd.d t5, t3, s10, dyn
[0x8000c044]:csrrs a7, fcsr, zero

[0x8000c040]:fadd.d t5, t3, s10, dyn
[0x8000c044]:csrrs a7, fcsr, zero
[0x8000c048]:sw t5, 1624(ra)
[0x8000c04c]:sw t6, 1632(ra)
[0x8000c050]:sw t5, 1640(ra)
[0x8000c054]:sw a7, 1648(ra)
[0x8000c058]:lui a4, 1
[0x8000c05c]:addi a4, a4, 2048
[0x8000c060]:add a6, a6, a4
[0x8000c064]:lw t3, 1840(a6)
[0x8000c068]:sub a6, a6, a4
[0x8000c06c]:lui a4, 1
[0x8000c070]:addi a4, a4, 2048
[0x8000c074]:add a6, a6, a4
[0x8000c078]:lw t4, 1844(a6)
[0x8000c07c]:sub a6, a6, a4
[0x8000c080]:lui a4, 1
[0x8000c084]:addi a4, a4, 2048
[0x8000c088]:add a6, a6, a4
[0x8000c08c]:lw s10, 1848(a6)
[0x8000c090]:sub a6, a6, a4
[0x8000c094]:lui a4, 1
[0x8000c098]:addi a4, a4, 2048
[0x8000c09c]:add a6, a6, a4
[0x8000c0a0]:lw s11, 1852(a6)
[0x8000c0a4]:sub a6, a6, a4
[0x8000c0a8]:addi t3, zero, 1
[0x8000c0ac]:lui t4, 1048320
[0x8000c0b0]:addi s10, zero, 4095
[0x8000c0b4]:lui s11, 256
[0x8000c0b8]:addi s11, s11, 4095
[0x8000c0bc]:addi a4, zero, 0
[0x8000c0c0]:csrrw zero, fcsr, a4
[0x8000c0c4]:fadd.d t5, t3, s10, dyn
[0x8000c0c8]:csrrs a7, fcsr, zero

[0x8000c0c4]:fadd.d t5, t3, s10, dyn
[0x8000c0c8]:csrrs a7, fcsr, zero
[0x8000c0cc]:sw t5, 1656(ra)
[0x8000c0d0]:sw t6, 1664(ra)
[0x8000c0d4]:sw t5, 1672(ra)
[0x8000c0d8]:sw a7, 1680(ra)
[0x8000c0dc]:lui a4, 1
[0x8000c0e0]:addi a4, a4, 2048
[0x8000c0e4]:add a6, a6, a4
[0x8000c0e8]:lw t3, 1856(a6)
[0x8000c0ec]:sub a6, a6, a4
[0x8000c0f0]:lui a4, 1
[0x8000c0f4]:addi a4, a4, 2048
[0x8000c0f8]:add a6, a6, a4
[0x8000c0fc]:lw t4, 1860(a6)
[0x8000c100]:sub a6, a6, a4
[0x8000c104]:lui a4, 1
[0x8000c108]:addi a4, a4, 2048
[0x8000c10c]:add a6, a6, a4
[0x8000c110]:lw s10, 1864(a6)
[0x8000c114]:sub a6, a6, a4
[0x8000c118]:lui a4, 1
[0x8000c11c]:addi a4, a4, 2048
[0x8000c120]:add a6, a6, a4
[0x8000c124]:lw s11, 1868(a6)
[0x8000c128]:sub a6, a6, a4
[0x8000c12c]:addi t3, zero, 1
[0x8000c130]:lui t4, 1048320
[0x8000c134]:addi s10, zero, 4095
[0x8000c138]:lui s11, 524544
[0x8000c13c]:addi s11, s11, 4095
[0x8000c140]:addi a4, zero, 0
[0x8000c144]:csrrw zero, fcsr, a4
[0x8000c148]:fadd.d t5, t3, s10, dyn
[0x8000c14c]:csrrs a7, fcsr, zero

[0x8000c148]:fadd.d t5, t3, s10, dyn
[0x8000c14c]:csrrs a7, fcsr, zero
[0x8000c150]:sw t5, 1688(ra)
[0x8000c154]:sw t6, 1696(ra)
[0x8000c158]:sw t5, 1704(ra)
[0x8000c15c]:sw a7, 1712(ra)
[0x8000c160]:lui a4, 1
[0x8000c164]:addi a4, a4, 2048
[0x8000c168]:add a6, a6, a4
[0x8000c16c]:lw t3, 1872(a6)
[0x8000c170]:sub a6, a6, a4
[0x8000c174]:lui a4, 1
[0x8000c178]:addi a4, a4, 2048
[0x8000c17c]:add a6, a6, a4
[0x8000c180]:lw t4, 1876(a6)
[0x8000c184]:sub a6, a6, a4
[0x8000c188]:lui a4, 1
[0x8000c18c]:addi a4, a4, 2048
[0x8000c190]:add a6, a6, a4
[0x8000c194]:lw s10, 1880(a6)
[0x8000c198]:sub a6, a6, a4
[0x8000c19c]:lui a4, 1
[0x8000c1a0]:addi a4, a4, 2048
[0x8000c1a4]:add a6, a6, a4
[0x8000c1a8]:lw s11, 1884(a6)
[0x8000c1ac]:sub a6, a6, a4
[0x8000c1b0]:addi t3, zero, 1
[0x8000c1b4]:lui t4, 1048320
[0x8000c1b8]:addi s10, zero, 0
[0x8000c1bc]:lui s11, 256
[0x8000c1c0]:addi a4, zero, 0
[0x8000c1c4]:csrrw zero, fcsr, a4
[0x8000c1c8]:fadd.d t5, t3, s10, dyn
[0x8000c1cc]:csrrs a7, fcsr, zero

[0x8000c1c8]:fadd.d t5, t3, s10, dyn
[0x8000c1cc]:csrrs a7, fcsr, zero
[0x8000c1d0]:sw t5, 1720(ra)
[0x8000c1d4]:sw t6, 1728(ra)
[0x8000c1d8]:sw t5, 1736(ra)
[0x8000c1dc]:sw a7, 1744(ra)
[0x8000c1e0]:lui a4, 1
[0x8000c1e4]:addi a4, a4, 2048
[0x8000c1e8]:add a6, a6, a4
[0x8000c1ec]:lw t3, 1888(a6)
[0x8000c1f0]:sub a6, a6, a4
[0x8000c1f4]:lui a4, 1
[0x8000c1f8]:addi a4, a4, 2048
[0x8000c1fc]:add a6, a6, a4
[0x8000c200]:lw t4, 1892(a6)
[0x8000c204]:sub a6, a6, a4
[0x8000c208]:lui a4, 1
[0x8000c20c]:addi a4, a4, 2048
[0x8000c210]:add a6, a6, a4
[0x8000c214]:lw s10, 1896(a6)
[0x8000c218]:sub a6, a6, a4
[0x8000c21c]:lui a4, 1
[0x8000c220]:addi a4, a4, 2048
[0x8000c224]:add a6, a6, a4
[0x8000c228]:lw s11, 1900(a6)
[0x8000c22c]:sub a6, a6, a4
[0x8000c230]:addi t3, zero, 1
[0x8000c234]:lui t4, 1048320
[0x8000c238]:addi s10, zero, 0
[0x8000c23c]:lui s11, 524544
[0x8000c240]:addi a4, zero, 0
[0x8000c244]:csrrw zero, fcsr, a4
[0x8000c248]:fadd.d t5, t3, s10, dyn
[0x8000c24c]:csrrs a7, fcsr, zero

[0x8000c248]:fadd.d t5, t3, s10, dyn
[0x8000c24c]:csrrs a7, fcsr, zero
[0x8000c250]:sw t5, 1752(ra)
[0x8000c254]:sw t6, 1760(ra)
[0x8000c258]:sw t5, 1768(ra)
[0x8000c25c]:sw a7, 1776(ra)
[0x8000c260]:lui a4, 1
[0x8000c264]:addi a4, a4, 2048
[0x8000c268]:add a6, a6, a4
[0x8000c26c]:lw t3, 1904(a6)
[0x8000c270]:sub a6, a6, a4
[0x8000c274]:lui a4, 1
[0x8000c278]:addi a4, a4, 2048
[0x8000c27c]:add a6, a6, a4
[0x8000c280]:lw t4, 1908(a6)
[0x8000c284]:sub a6, a6, a4
[0x8000c288]:lui a4, 1
[0x8000c28c]:addi a4, a4, 2048
[0x8000c290]:add a6, a6, a4
[0x8000c294]:lw s10, 1912(a6)
[0x8000c298]:sub a6, a6, a4
[0x8000c29c]:lui a4, 1
[0x8000c2a0]:addi a4, a4, 2048
[0x8000c2a4]:add a6, a6, a4
[0x8000c2a8]:lw s11, 1916(a6)
[0x8000c2ac]:sub a6, a6, a4
[0x8000c2b0]:addi t3, zero, 1
[0x8000c2b4]:lui t4, 1048320
[0x8000c2b8]:addi s10, zero, 2
[0x8000c2bc]:lui s11, 256
[0x8000c2c0]:addi a4, zero, 0
[0x8000c2c4]:csrrw zero, fcsr, a4
[0x8000c2c8]:fadd.d t5, t3, s10, dyn
[0x8000c2cc]:csrrs a7, fcsr, zero

[0x8000c2c8]:fadd.d t5, t3, s10, dyn
[0x8000c2cc]:csrrs a7, fcsr, zero
[0x8000c2d0]:sw t5, 1784(ra)
[0x8000c2d4]:sw t6, 1792(ra)
[0x8000c2d8]:sw t5, 1800(ra)
[0x8000c2dc]:sw a7, 1808(ra)
[0x8000c2e0]:lui a4, 1
[0x8000c2e4]:addi a4, a4, 2048
[0x8000c2e8]:add a6, a6, a4
[0x8000c2ec]:lw t3, 1920(a6)
[0x8000c2f0]:sub a6, a6, a4
[0x8000c2f4]:lui a4, 1
[0x8000c2f8]:addi a4, a4, 2048
[0x8000c2fc]:add a6, a6, a4
[0x8000c300]:lw t4, 1924(a6)
[0x8000c304]:sub a6, a6, a4
[0x8000c308]:lui a4, 1
[0x8000c30c]:addi a4, a4, 2048
[0x8000c310]:add a6, a6, a4
[0x8000c314]:lw s10, 1928(a6)
[0x8000c318]:sub a6, a6, a4
[0x8000c31c]:lui a4, 1
[0x8000c320]:addi a4, a4, 2048
[0x8000c324]:add a6, a6, a4
[0x8000c328]:lw s11, 1932(a6)
[0x8000c32c]:sub a6, a6, a4
[0x8000c330]:addi t3, zero, 1
[0x8000c334]:lui t4, 1048320
[0x8000c338]:addi s10, zero, 2
[0x8000c33c]:lui s11, 524544
[0x8000c340]:addi a4, zero, 0
[0x8000c344]:csrrw zero, fcsr, a4
[0x8000c348]:fadd.d t5, t3, s10, dyn
[0x8000c34c]:csrrs a7, fcsr, zero

[0x8000c348]:fadd.d t5, t3, s10, dyn
[0x8000c34c]:csrrs a7, fcsr, zero
[0x8000c350]:sw t5, 1816(ra)
[0x8000c354]:sw t6, 1824(ra)
[0x8000c358]:sw t5, 1832(ra)
[0x8000c35c]:sw a7, 1840(ra)
[0x8000c360]:lui a4, 1
[0x8000c364]:addi a4, a4, 2048
[0x8000c368]:add a6, a6, a4
[0x8000c36c]:lw t3, 1936(a6)
[0x8000c370]:sub a6, a6, a4
[0x8000c374]:lui a4, 1
[0x8000c378]:addi a4, a4, 2048
[0x8000c37c]:add a6, a6, a4
[0x8000c380]:lw t4, 1940(a6)
[0x8000c384]:sub a6, a6, a4
[0x8000c388]:lui a4, 1
[0x8000c38c]:addi a4, a4, 2048
[0x8000c390]:add a6, a6, a4
[0x8000c394]:lw s10, 1944(a6)
[0x8000c398]:sub a6, a6, a4
[0x8000c39c]:lui a4, 1
[0x8000c3a0]:addi a4, a4, 2048
[0x8000c3a4]:add a6, a6, a4
[0x8000c3a8]:lw s11, 1948(a6)
[0x8000c3ac]:sub a6, a6, a4
[0x8000c3b0]:addi t3, zero, 1
[0x8000c3b4]:lui t4, 1048320
[0x8000c3b8]:addi s10, zero, 4095
[0x8000c3bc]:lui s11, 524032
[0x8000c3c0]:addi s11, s11, 4095
[0x8000c3c4]:addi a4, zero, 0
[0x8000c3c8]:csrrw zero, fcsr, a4
[0x8000c3cc]:fadd.d t5, t3, s10, dyn
[0x8000c3d0]:csrrs a7, fcsr, zero

[0x8000c3cc]:fadd.d t5, t3, s10, dyn
[0x8000c3d0]:csrrs a7, fcsr, zero
[0x8000c3d4]:sw t5, 1848(ra)
[0x8000c3d8]:sw t6, 1856(ra)
[0x8000c3dc]:sw t5, 1864(ra)
[0x8000c3e0]:sw a7, 1872(ra)
[0x8000c3e4]:lui a4, 1
[0x8000c3e8]:addi a4, a4, 2048
[0x8000c3ec]:add a6, a6, a4
[0x8000c3f0]:lw t3, 1952(a6)
[0x8000c3f4]:sub a6, a6, a4
[0x8000c3f8]:lui a4, 1
[0x8000c3fc]:addi a4, a4, 2048
[0x8000c400]:add a6, a6, a4
[0x8000c404]:lw t4, 1956(a6)
[0x8000c408]:sub a6, a6, a4
[0x8000c40c]:lui a4, 1
[0x8000c410]:addi a4, a4, 2048
[0x8000c414]:add a6, a6, a4
[0x8000c418]:lw s10, 1960(a6)
[0x8000c41c]:sub a6, a6, a4
[0x8000c420]:lui a4, 1
[0x8000c424]:addi a4, a4, 2048
[0x8000c428]:add a6, a6, a4
[0x8000c42c]:lw s11, 1964(a6)
[0x8000c430]:sub a6, a6, a4
[0x8000c434]:addi t3, zero, 1
[0x8000c438]:lui t4, 1048320
[0x8000c43c]:addi s10, zero, 4095
[0x8000c440]:lui s11, 1048320
[0x8000c444]:addi s11, s11, 4095
[0x8000c448]:addi a4, zero, 0
[0x8000c44c]:csrrw zero, fcsr, a4
[0x8000c450]:fadd.d t5, t3, s10, dyn
[0x8000c454]:csrrs a7, fcsr, zero

[0x8000c450]:fadd.d t5, t3, s10, dyn
[0x8000c454]:csrrs a7, fcsr, zero
[0x8000c458]:sw t5, 1880(ra)
[0x8000c45c]:sw t6, 1888(ra)
[0x8000c460]:sw t5, 1896(ra)
[0x8000c464]:sw a7, 1904(ra)
[0x8000c468]:lui a4, 1
[0x8000c46c]:addi a4, a4, 2048
[0x8000c470]:add a6, a6, a4
[0x8000c474]:lw t3, 1968(a6)
[0x8000c478]:sub a6, a6, a4
[0x8000c47c]:lui a4, 1
[0x8000c480]:addi a4, a4, 2048
[0x8000c484]:add a6, a6, a4
[0x8000c488]:lw t4, 1972(a6)
[0x8000c48c]:sub a6, a6, a4
[0x8000c490]:lui a4, 1
[0x8000c494]:addi a4, a4, 2048
[0x8000c498]:add a6, a6, a4
[0x8000c49c]:lw s10, 1976(a6)
[0x8000c4a0]:sub a6, a6, a4
[0x8000c4a4]:lui a4, 1
[0x8000c4a8]:addi a4, a4, 2048
[0x8000c4ac]:add a6, a6, a4
[0x8000c4b0]:lw s11, 1980(a6)
[0x8000c4b4]:sub a6, a6, a4
[0x8000c4b8]:addi t3, zero, 1
[0x8000c4bc]:lui t4, 1048320
[0x8000c4c0]:addi s10, zero, 0
[0x8000c4c4]:lui s11, 524032
[0x8000c4c8]:addi a4, zero, 0
[0x8000c4cc]:csrrw zero, fcsr, a4
[0x8000c4d0]:fadd.d t5, t3, s10, dyn
[0x8000c4d4]:csrrs a7, fcsr, zero

[0x8000c4d0]:fadd.d t5, t3, s10, dyn
[0x8000c4d4]:csrrs a7, fcsr, zero
[0x8000c4d8]:sw t5, 1912(ra)
[0x8000c4dc]:sw t6, 1920(ra)
[0x8000c4e0]:sw t5, 1928(ra)
[0x8000c4e4]:sw a7, 1936(ra)
[0x8000c4e8]:lui a4, 1
[0x8000c4ec]:addi a4, a4, 2048
[0x8000c4f0]:add a6, a6, a4
[0x8000c4f4]:lw t3, 1984(a6)
[0x8000c4f8]:sub a6, a6, a4
[0x8000c4fc]:lui a4, 1
[0x8000c500]:addi a4, a4, 2048
[0x8000c504]:add a6, a6, a4
[0x8000c508]:lw t4, 1988(a6)
[0x8000c50c]:sub a6, a6, a4
[0x8000c510]:lui a4, 1
[0x8000c514]:addi a4, a4, 2048
[0x8000c518]:add a6, a6, a4
[0x8000c51c]:lw s10, 1992(a6)
[0x8000c520]:sub a6, a6, a4
[0x8000c524]:lui a4, 1
[0x8000c528]:addi a4, a4, 2048
[0x8000c52c]:add a6, a6, a4
[0x8000c530]:lw s11, 1996(a6)
[0x8000c534]:sub a6, a6, a4
[0x8000c538]:addi t3, zero, 1
[0x8000c53c]:lui t4, 1048320
[0x8000c540]:addi s10, zero, 0
[0x8000c544]:lui s11, 1048320
[0x8000c548]:addi a4, zero, 0
[0x8000c54c]:csrrw zero, fcsr, a4
[0x8000c550]:fadd.d t5, t3, s10, dyn
[0x8000c554]:csrrs a7, fcsr, zero

[0x8000c550]:fadd.d t5, t3, s10, dyn
[0x8000c554]:csrrs a7, fcsr, zero
[0x8000c558]:sw t5, 1944(ra)
[0x8000c55c]:sw t6, 1952(ra)
[0x8000c560]:sw t5, 1960(ra)
[0x8000c564]:sw a7, 1968(ra)
[0x8000c568]:lui a4, 1
[0x8000c56c]:addi a4, a4, 2048
[0x8000c570]:add a6, a6, a4
[0x8000c574]:lw t3, 2000(a6)
[0x8000c578]:sub a6, a6, a4
[0x8000c57c]:lui a4, 1
[0x8000c580]:addi a4, a4, 2048
[0x8000c584]:add a6, a6, a4
[0x8000c588]:lw t4, 2004(a6)
[0x8000c58c]:sub a6, a6, a4
[0x8000c590]:lui a4, 1
[0x8000c594]:addi a4, a4, 2048
[0x8000c598]:add a6, a6, a4
[0x8000c59c]:lw s10, 2008(a6)
[0x8000c5a0]:sub a6, a6, a4
[0x8000c5a4]:lui a4, 1
[0x8000c5a8]:addi a4, a4, 2048
[0x8000c5ac]:add a6, a6, a4
[0x8000c5b0]:lw s11, 2012(a6)
[0x8000c5b4]:sub a6, a6, a4
[0x8000c5b8]:addi t3, zero, 1
[0x8000c5bc]:lui t4, 1048320
[0x8000c5c0]:addi s10, zero, 0
[0x8000c5c4]:lui s11, 524160
[0x8000c5c8]:addi a4, zero, 0
[0x8000c5cc]:csrrw zero, fcsr, a4
[0x8000c5d0]:fadd.d t5, t3, s10, dyn
[0x8000c5d4]:csrrs a7, fcsr, zero

[0x8000c5d0]:fadd.d t5, t3, s10, dyn
[0x8000c5d4]:csrrs a7, fcsr, zero
[0x8000c5d8]:sw t5, 1976(ra)
[0x8000c5dc]:sw t6, 1984(ra)
[0x8000c5e0]:sw t5, 1992(ra)
[0x8000c5e4]:sw a7, 2000(ra)
[0x8000c5e8]:lui a4, 1
[0x8000c5ec]:addi a4, a4, 2048
[0x8000c5f0]:add a6, a6, a4
[0x8000c5f4]:lw t3, 2016(a6)
[0x8000c5f8]:sub a6, a6, a4
[0x8000c5fc]:lui a4, 1
[0x8000c600]:addi a4, a4, 2048
[0x8000c604]:add a6, a6, a4
[0x8000c608]:lw t4, 2020(a6)
[0x8000c60c]:sub a6, a6, a4
[0x8000c610]:lui a4, 1
[0x8000c614]:addi a4, a4, 2048
[0x8000c618]:add a6, a6, a4
[0x8000c61c]:lw s10, 2024(a6)
[0x8000c620]:sub a6, a6, a4
[0x8000c624]:lui a4, 1
[0x8000c628]:addi a4, a4, 2048
[0x8000c62c]:add a6, a6, a4
[0x8000c630]:lw s11, 2028(a6)
[0x8000c634]:sub a6, a6, a4
[0x8000c638]:addi t3, zero, 1
[0x8000c63c]:lui t4, 1048320
[0x8000c640]:addi s10, zero, 0
[0x8000c644]:lui s11, 1048448
[0x8000c648]:addi a4, zero, 0
[0x8000c64c]:csrrw zero, fcsr, a4
[0x8000c650]:fadd.d t5, t3, s10, dyn
[0x8000c654]:csrrs a7, fcsr, zero

[0x8000c650]:fadd.d t5, t3, s10, dyn
[0x8000c654]:csrrs a7, fcsr, zero
[0x8000c658]:sw t5, 2008(ra)
[0x8000c65c]:sw t6, 2016(ra)
[0x8000c660]:sw t5, 2024(ra)
[0x8000c664]:sw a7, 2032(ra)
[0x8000c668]:lui a4, 1
[0x8000c66c]:addi a4, a4, 2048
[0x8000c670]:add a6, a6, a4
[0x8000c674]:lw t3, 2032(a6)
[0x8000c678]:sub a6, a6, a4
[0x8000c67c]:lui a4, 1
[0x8000c680]:addi a4, a4, 2048
[0x8000c684]:add a6, a6, a4
[0x8000c688]:lw t4, 2036(a6)
[0x8000c68c]:sub a6, a6, a4
[0x8000c690]:lui a4, 1
[0x8000c694]:addi a4, a4, 2048
[0x8000c698]:add a6, a6, a4
[0x8000c69c]:lw s10, 2040(a6)
[0x8000c6a0]:sub a6, a6, a4
[0x8000c6a4]:lui a4, 1
[0x8000c6a8]:addi a4, a4, 2048
[0x8000c6ac]:add a6, a6, a4
[0x8000c6b0]:lw s11, 2044(a6)
[0x8000c6b4]:sub a6, a6, a4
[0x8000c6b8]:addi t3, zero, 1
[0x8000c6bc]:lui t4, 1048320
[0x8000c6c0]:addi s10, zero, 1
[0x8000c6c4]:lui s11, 524160
[0x8000c6c8]:addi a4, zero, 0
[0x8000c6cc]:csrrw zero, fcsr, a4
[0x8000c6d0]:fadd.d t5, t3, s10, dyn
[0x8000c6d4]:csrrs a7, fcsr, zero

[0x8000c6d0]:fadd.d t5, t3, s10, dyn
[0x8000c6d4]:csrrs a7, fcsr, zero
[0x8000c6d8]:sw t5, 2040(ra)
[0x8000c6dc]:addi ra, ra, 2040
[0x8000c6e0]:sw t6, 8(ra)
[0x8000c6e4]:sw t5, 16(ra)
[0x8000c6e8]:sw a7, 24(ra)
[0x8000c6ec]:auipc ra, 7
[0x8000c6f0]:addi ra, ra, 4060
[0x8000c6f4]:lw t3, 0(a6)
[0x8000c6f8]:lw t4, 4(a6)
[0x8000c6fc]:lw s10, 8(a6)
[0x8000c700]:lw s11, 12(a6)
[0x8000c704]:addi t3, zero, 1
[0x8000c708]:lui t4, 1048320
[0x8000c70c]:addi s10, zero, 1
[0x8000c710]:lui s11, 1048448
[0x8000c714]:addi a4, zero, 0
[0x8000c718]:csrrw zero, fcsr, a4
[0x8000c71c]:fadd.d t5, t3, s10, dyn
[0x8000c720]:csrrs a7, fcsr, zero

[0x8000ce2c]:fadd.d t5, t3, s10, dyn
[0x8000ce30]:csrrs a7, fcsr, zero
[0x8000ce34]:sw t5, 896(ra)
[0x8000ce38]:sw t6, 904(ra)
[0x8000ce3c]:sw t5, 912(ra)
[0x8000ce40]:sw a7, 920(ra)
[0x8000ce44]:lw t3, 464(a6)
[0x8000ce48]:lw t4, 468(a6)
[0x8000ce4c]:lw s10, 472(a6)
[0x8000ce50]:lw s11, 476(a6)
[0x8000ce54]:addi t3, zero, 0
[0x8000ce58]:lui t4, 784384
[0x8000ce5c]:addi s10, zero, 0
[0x8000ce60]:addi s11, zero, 0
[0x8000ce64]:addi a4, zero, 0
[0x8000ce68]:csrrw zero, fcsr, a4
[0x8000ce6c]:fadd.d t5, t3, s10, dyn
[0x8000ce70]:csrrs a7, fcsr, zero

[0x8000ce6c]:fadd.d t5, t3, s10, dyn
[0x8000ce70]:csrrs a7, fcsr, zero
[0x8000ce74]:sw t5, 928(ra)
[0x8000ce78]:sw t6, 936(ra)
[0x8000ce7c]:sw t5, 944(ra)
[0x8000ce80]:sw a7, 952(ra)
[0x8000ce84]:lw t3, 480(a6)
[0x8000ce88]:lw t4, 484(a6)
[0x8000ce8c]:lw s10, 488(a6)
[0x8000ce90]:lw s11, 492(a6)
[0x8000ce94]:addi t3, zero, 0
[0x8000ce98]:lui t4, 784384
[0x8000ce9c]:addi s10, zero, 0
[0x8000cea0]:lui s11, 524288
[0x8000cea4]:addi a4, zero, 0
[0x8000cea8]:csrrw zero, fcsr, a4
[0x8000ceac]:fadd.d t5, t3, s10, dyn
[0x8000ceb0]:csrrs a7, fcsr, zero

[0x8000ceac]:fadd.d t5, t3, s10, dyn
[0x8000ceb0]:csrrs a7, fcsr, zero
[0x8000ceb4]:sw t5, 960(ra)
[0x8000ceb8]:sw t6, 968(ra)
[0x8000cebc]:sw t5, 976(ra)
[0x8000cec0]:sw a7, 984(ra)
[0x8000cec4]:lw t3, 496(a6)
[0x8000cec8]:lw t4, 500(a6)
[0x8000cecc]:lw s10, 504(a6)
[0x8000ced0]:lw s11, 508(a6)
[0x8000ced4]:addi t3, zero, 0
[0x8000ced8]:lui t4, 784384
[0x8000cedc]:addi s10, zero, 1
[0x8000cee0]:addi s11, zero, 0
[0x8000cee4]:addi a4, zero, 0
[0x8000cee8]:csrrw zero, fcsr, a4
[0x8000ceec]:fadd.d t5, t3, s10, dyn
[0x8000cef0]:csrrs a7, fcsr, zero

[0x8000ceec]:fadd.d t5, t3, s10, dyn
[0x8000cef0]:csrrs a7, fcsr, zero
[0x8000cef4]:sw t5, 992(ra)
[0x8000cef8]:sw t6, 1000(ra)
[0x8000cefc]:sw t5, 1008(ra)
[0x8000cf00]:sw a7, 1016(ra)
[0x8000cf04]:lw t3, 512(a6)
[0x8000cf08]:lw t4, 516(a6)
[0x8000cf0c]:lw s10, 520(a6)
[0x8000cf10]:lw s11, 524(a6)
[0x8000cf14]:addi t3, zero, 0
[0x8000cf18]:lui t4, 784384
[0x8000cf1c]:addi s10, zero, 1
[0x8000cf20]:lui s11, 524288
[0x8000cf24]:addi a4, zero, 0
[0x8000cf28]:csrrw zero, fcsr, a4
[0x8000cf2c]:fadd.d t5, t3, s10, dyn
[0x8000cf30]:csrrs a7, fcsr, zero

[0x8000cf2c]:fadd.d t5, t3, s10, dyn
[0x8000cf30]:csrrs a7, fcsr, zero
[0x8000cf34]:sw t5, 1024(ra)
[0x8000cf38]:sw t6, 1032(ra)
[0x8000cf3c]:sw t5, 1040(ra)
[0x8000cf40]:sw a7, 1048(ra)
[0x8000cf44]:lw t3, 528(a6)
[0x8000cf48]:lw t4, 532(a6)
[0x8000cf4c]:lw s10, 536(a6)
[0x8000cf50]:lw s11, 540(a6)
[0x8000cf54]:addi t3, zero, 0
[0x8000cf58]:lui t4, 784384
[0x8000cf5c]:addi s10, zero, 2
[0x8000cf60]:addi s11, zero, 0
[0x8000cf64]:addi a4, zero, 0
[0x8000cf68]:csrrw zero, fcsr, a4
[0x8000cf6c]:fadd.d t5, t3, s10, dyn
[0x8000cf70]:csrrs a7, fcsr, zero

[0x8000cf6c]:fadd.d t5, t3, s10, dyn
[0x8000cf70]:csrrs a7, fcsr, zero
[0x8000cf74]:sw t5, 1056(ra)
[0x8000cf78]:sw t6, 1064(ra)
[0x8000cf7c]:sw t5, 1072(ra)
[0x8000cf80]:sw a7, 1080(ra)
[0x8000cf84]:lw t3, 544(a6)
[0x8000cf88]:lw t4, 548(a6)
[0x8000cf8c]:lw s10, 552(a6)
[0x8000cf90]:lw s11, 556(a6)
[0x8000cf94]:addi t3, zero, 0
[0x8000cf98]:lui t4, 784384
[0x8000cf9c]:addi s10, zero, 2
[0x8000cfa0]:lui s11, 524288
[0x8000cfa4]:addi a4, zero, 0
[0x8000cfa8]:csrrw zero, fcsr, a4
[0x8000cfac]:fadd.d t5, t3, s10, dyn
[0x8000cfb0]:csrrs a7, fcsr, zero

[0x8000cfac]:fadd.d t5, t3, s10, dyn
[0x8000cfb0]:csrrs a7, fcsr, zero
[0x8000cfb4]:sw t5, 1088(ra)
[0x8000cfb8]:sw t6, 1096(ra)
[0x8000cfbc]:sw t5, 1104(ra)
[0x8000cfc0]:sw a7, 1112(ra)
[0x8000cfc4]:lw t3, 560(a6)
[0x8000cfc8]:lw t4, 564(a6)
[0x8000cfcc]:lw s10, 568(a6)
[0x8000cfd0]:lw s11, 572(a6)
[0x8000cfd4]:addi t3, zero, 0
[0x8000cfd8]:lui t4, 784384
[0x8000cfdc]:addi s10, zero, 4095
[0x8000cfe0]:lui s11, 256
[0x8000cfe4]:addi s11, s11, 4095
[0x8000cfe8]:addi a4, zero, 0
[0x8000cfec]:csrrw zero, fcsr, a4
[0x8000cff0]:fadd.d t5, t3, s10, dyn
[0x8000cff4]:csrrs a7, fcsr, zero

[0x8000cff0]:fadd.d t5, t3, s10, dyn
[0x8000cff4]:csrrs a7, fcsr, zero
[0x8000cff8]:sw t5, 1120(ra)
[0x8000cffc]:sw t6, 1128(ra)
[0x8000d000]:sw t5, 1136(ra)
[0x8000d004]:sw a7, 1144(ra)
[0x8000d008]:lw t3, 576(a6)
[0x8000d00c]:lw t4, 580(a6)
[0x8000d010]:lw s10, 584(a6)
[0x8000d014]:lw s11, 588(a6)
[0x8000d018]:addi t3, zero, 0
[0x8000d01c]:lui t4, 784384
[0x8000d020]:addi s10, zero, 4095
[0x8000d024]:lui s11, 524544
[0x8000d028]:addi s11, s11, 4095
[0x8000d02c]:addi a4, zero, 0
[0x8000d030]:csrrw zero, fcsr, a4
[0x8000d034]:fadd.d t5, t3, s10, dyn
[0x8000d038]:csrrs a7, fcsr, zero

[0x8000d034]:fadd.d t5, t3, s10, dyn
[0x8000d038]:csrrs a7, fcsr, zero
[0x8000d03c]:sw t5, 1152(ra)
[0x8000d040]:sw t6, 1160(ra)
[0x8000d044]:sw t5, 1168(ra)
[0x8000d048]:sw a7, 1176(ra)
[0x8000d04c]:lw t3, 592(a6)
[0x8000d050]:lw t4, 596(a6)
[0x8000d054]:lw s10, 600(a6)
[0x8000d058]:lw s11, 604(a6)
[0x8000d05c]:addi t3, zero, 0
[0x8000d060]:lui t4, 784384
[0x8000d064]:addi s10, zero, 0
[0x8000d068]:lui s11, 256
[0x8000d06c]:addi a4, zero, 0
[0x8000d070]:csrrw zero, fcsr, a4
[0x8000d074]:fadd.d t5, t3, s10, dyn
[0x8000d078]:csrrs a7, fcsr, zero

[0x8000d074]:fadd.d t5, t3, s10, dyn
[0x8000d078]:csrrs a7, fcsr, zero
[0x8000d07c]:sw t5, 1184(ra)
[0x8000d080]:sw t6, 1192(ra)
[0x8000d084]:sw t5, 1200(ra)
[0x8000d088]:sw a7, 1208(ra)
[0x8000d08c]:lw t3, 608(a6)
[0x8000d090]:lw t4, 612(a6)
[0x8000d094]:lw s10, 616(a6)
[0x8000d098]:lw s11, 620(a6)
[0x8000d09c]:addi t3, zero, 0
[0x8000d0a0]:lui t4, 784384
[0x8000d0a4]:addi s10, zero, 0
[0x8000d0a8]:lui s11, 524544
[0x8000d0ac]:addi a4, zero, 0
[0x8000d0b0]:csrrw zero, fcsr, a4
[0x8000d0b4]:fadd.d t5, t3, s10, dyn
[0x8000d0b8]:csrrs a7, fcsr, zero

[0x8000d0b4]:fadd.d t5, t3, s10, dyn
[0x8000d0b8]:csrrs a7, fcsr, zero
[0x8000d0bc]:sw t5, 1216(ra)
[0x8000d0c0]:sw t6, 1224(ra)
[0x8000d0c4]:sw t5, 1232(ra)
[0x8000d0c8]:sw a7, 1240(ra)
[0x8000d0cc]:lw t3, 624(a6)
[0x8000d0d0]:lw t4, 628(a6)
[0x8000d0d4]:lw s10, 632(a6)
[0x8000d0d8]:lw s11, 636(a6)
[0x8000d0dc]:addi t3, zero, 0
[0x8000d0e0]:lui t4, 784384
[0x8000d0e4]:addi s10, zero, 2
[0x8000d0e8]:lui s11, 256
[0x8000d0ec]:addi a4, zero, 0
[0x8000d0f0]:csrrw zero, fcsr, a4
[0x8000d0f4]:fadd.d t5, t3, s10, dyn
[0x8000d0f8]:csrrs a7, fcsr, zero

[0x8000d0f4]:fadd.d t5, t3, s10, dyn
[0x8000d0f8]:csrrs a7, fcsr, zero
[0x8000d0fc]:sw t5, 1248(ra)
[0x8000d100]:sw t6, 1256(ra)
[0x8000d104]:sw t5, 1264(ra)
[0x8000d108]:sw a7, 1272(ra)
[0x8000d10c]:lw t3, 640(a6)
[0x8000d110]:lw t4, 644(a6)
[0x8000d114]:lw s10, 648(a6)
[0x8000d118]:lw s11, 652(a6)
[0x8000d11c]:addi t3, zero, 0
[0x8000d120]:lui t4, 784384
[0x8000d124]:addi s10, zero, 2
[0x8000d128]:lui s11, 524544
[0x8000d12c]:addi a4, zero, 0
[0x8000d130]:csrrw zero, fcsr, a4
[0x8000d134]:fadd.d t5, t3, s10, dyn
[0x8000d138]:csrrs a7, fcsr, zero

[0x8000d134]:fadd.d t5, t3, s10, dyn
[0x8000d138]:csrrs a7, fcsr, zero
[0x8000d13c]:sw t5, 1280(ra)
[0x8000d140]:sw t6, 1288(ra)
[0x8000d144]:sw t5, 1296(ra)
[0x8000d148]:sw a7, 1304(ra)
[0x8000d14c]:lw t3, 656(a6)
[0x8000d150]:lw t4, 660(a6)
[0x8000d154]:lw s10, 664(a6)
[0x8000d158]:lw s11, 668(a6)
[0x8000d15c]:addi t3, zero, 0
[0x8000d160]:lui t4, 784384
[0x8000d164]:addi s10, zero, 4095
[0x8000d168]:lui s11, 524032
[0x8000d16c]:addi s11, s11, 4095
[0x8000d170]:addi a4, zero, 0
[0x8000d174]:csrrw zero, fcsr, a4
[0x8000d178]:fadd.d t5, t3, s10, dyn
[0x8000d17c]:csrrs a7, fcsr, zero

[0x8000d178]:fadd.d t5, t3, s10, dyn
[0x8000d17c]:csrrs a7, fcsr, zero
[0x8000d180]:sw t5, 1312(ra)
[0x8000d184]:sw t6, 1320(ra)
[0x8000d188]:sw t5, 1328(ra)
[0x8000d18c]:sw a7, 1336(ra)
[0x8000d190]:lw t3, 672(a6)
[0x8000d194]:lw t4, 676(a6)
[0x8000d198]:lw s10, 680(a6)
[0x8000d19c]:lw s11, 684(a6)
[0x8000d1a0]:addi t3, zero, 0
[0x8000d1a4]:lui t4, 784384
[0x8000d1a8]:addi s10, zero, 4095
[0x8000d1ac]:lui s11, 1048320
[0x8000d1b0]:addi s11, s11, 4095
[0x8000d1b4]:addi a4, zero, 0
[0x8000d1b8]:csrrw zero, fcsr, a4
[0x8000d1bc]:fadd.d t5, t3, s10, dyn
[0x8000d1c0]:csrrs a7, fcsr, zero

[0x8000d1bc]:fadd.d t5, t3, s10, dyn
[0x8000d1c0]:csrrs a7, fcsr, zero
[0x8000d1c4]:sw t5, 1344(ra)
[0x8000d1c8]:sw t6, 1352(ra)
[0x8000d1cc]:sw t5, 1360(ra)
[0x8000d1d0]:sw a7, 1368(ra)
[0x8000d1d4]:lw t3, 688(a6)
[0x8000d1d8]:lw t4, 692(a6)
[0x8000d1dc]:lw s10, 696(a6)
[0x8000d1e0]:lw s11, 700(a6)
[0x8000d1e4]:addi t3, zero, 0
[0x8000d1e8]:lui t4, 784384
[0x8000d1ec]:addi s10, zero, 0
[0x8000d1f0]:lui s11, 524032
[0x8000d1f4]:addi a4, zero, 0
[0x8000d1f8]:csrrw zero, fcsr, a4
[0x8000d1fc]:fadd.d t5, t3, s10, dyn
[0x8000d200]:csrrs a7, fcsr, zero

[0x8000d1fc]:fadd.d t5, t3, s10, dyn
[0x8000d200]:csrrs a7, fcsr, zero
[0x8000d204]:sw t5, 1376(ra)
[0x8000d208]:sw t6, 1384(ra)
[0x8000d20c]:sw t5, 1392(ra)
[0x8000d210]:sw a7, 1400(ra)
[0x8000d214]:lw t3, 704(a6)
[0x8000d218]:lw t4, 708(a6)
[0x8000d21c]:lw s10, 712(a6)
[0x8000d220]:lw s11, 716(a6)
[0x8000d224]:addi t3, zero, 0
[0x8000d228]:lui t4, 784384
[0x8000d22c]:addi s10, zero, 0
[0x8000d230]:lui s11, 1048320
[0x8000d234]:addi a4, zero, 0
[0x8000d238]:csrrw zero, fcsr, a4
[0x8000d23c]:fadd.d t5, t3, s10, dyn
[0x8000d240]:csrrs a7, fcsr, zero

[0x8000d23c]:fadd.d t5, t3, s10, dyn
[0x8000d240]:csrrs a7, fcsr, zero
[0x8000d244]:sw t5, 1408(ra)
[0x8000d248]:sw t6, 1416(ra)
[0x8000d24c]:sw t5, 1424(ra)
[0x8000d250]:sw a7, 1432(ra)
[0x8000d254]:lw t3, 720(a6)
[0x8000d258]:lw t4, 724(a6)
[0x8000d25c]:lw s10, 728(a6)
[0x8000d260]:lw s11, 732(a6)
[0x8000d264]:addi t3, zero, 0
[0x8000d268]:lui t4, 784384
[0x8000d26c]:addi s10, zero, 0
[0x8000d270]:lui s11, 524160
[0x8000d274]:addi a4, zero, 0
[0x8000d278]:csrrw zero, fcsr, a4
[0x8000d27c]:fadd.d t5, t3, s10, dyn
[0x8000d280]:csrrs a7, fcsr, zero

[0x8000d27c]:fadd.d t5, t3, s10, dyn
[0x8000d280]:csrrs a7, fcsr, zero
[0x8000d284]:sw t5, 1440(ra)
[0x8000d288]:sw t6, 1448(ra)
[0x8000d28c]:sw t5, 1456(ra)
[0x8000d290]:sw a7, 1464(ra)
[0x8000d294]:lw t3, 736(a6)
[0x8000d298]:lw t4, 740(a6)
[0x8000d29c]:lw s10, 744(a6)
[0x8000d2a0]:lw s11, 748(a6)
[0x8000d2a4]:addi t3, zero, 0
[0x8000d2a8]:lui t4, 784384
[0x8000d2ac]:addi s10, zero, 0
[0x8000d2b0]:lui s11, 1048448
[0x8000d2b4]:addi a4, zero, 0
[0x8000d2b8]:csrrw zero, fcsr, a4
[0x8000d2bc]:fadd.d t5, t3, s10, dyn
[0x8000d2c0]:csrrs a7, fcsr, zero

[0x8000d2bc]:fadd.d t5, t3, s10, dyn
[0x8000d2c0]:csrrs a7, fcsr, zero
[0x8000d2c4]:sw t5, 1472(ra)
[0x8000d2c8]:sw t6, 1480(ra)
[0x8000d2cc]:sw t5, 1488(ra)
[0x8000d2d0]:sw a7, 1496(ra)
[0x8000d2d4]:lw t3, 752(a6)
[0x8000d2d8]:lw t4, 756(a6)
[0x8000d2dc]:lw s10, 760(a6)
[0x8000d2e0]:lw s11, 764(a6)
[0x8000d2e4]:addi t3, zero, 0
[0x8000d2e8]:lui t4, 784384
[0x8000d2ec]:addi s10, zero, 1
[0x8000d2f0]:lui s11, 524160
[0x8000d2f4]:addi a4, zero, 0
[0x8000d2f8]:csrrw zero, fcsr, a4
[0x8000d2fc]:fadd.d t5, t3, s10, dyn
[0x8000d300]:csrrs a7, fcsr, zero

[0x8000d2fc]:fadd.d t5, t3, s10, dyn
[0x8000d300]:csrrs a7, fcsr, zero
[0x8000d304]:sw t5, 1504(ra)
[0x8000d308]:sw t6, 1512(ra)
[0x8000d30c]:sw t5, 1520(ra)
[0x8000d310]:sw a7, 1528(ra)
[0x8000d314]:lw t3, 768(a6)
[0x8000d318]:lw t4, 772(a6)
[0x8000d31c]:lw s10, 776(a6)
[0x8000d320]:lw s11, 780(a6)
[0x8000d324]:addi t3, zero, 0
[0x8000d328]:lui t4, 784384
[0x8000d32c]:addi s10, zero, 1
[0x8000d330]:lui s11, 1048448
[0x8000d334]:addi a4, zero, 0
[0x8000d338]:csrrw zero, fcsr, a4
[0x8000d33c]:fadd.d t5, t3, s10, dyn
[0x8000d340]:csrrs a7, fcsr, zero

[0x8000d33c]:fadd.d t5, t3, s10, dyn
[0x8000d340]:csrrs a7, fcsr, zero
[0x8000d344]:sw t5, 1536(ra)
[0x8000d348]:sw t6, 1544(ra)
[0x8000d34c]:sw t5, 1552(ra)
[0x8000d350]:sw a7, 1560(ra)
[0x8000d354]:lw t3, 784(a6)
[0x8000d358]:lw t4, 788(a6)
[0x8000d35c]:lw s10, 792(a6)
[0x8000d360]:lw s11, 796(a6)
[0x8000d364]:addi t3, zero, 0
[0x8000d368]:lui t4, 784384
[0x8000d36c]:addi s10, zero, 1
[0x8000d370]:lui s11, 524032
[0x8000d374]:addi a4, zero, 0
[0x8000d378]:csrrw zero, fcsr, a4
[0x8000d37c]:fadd.d t5, t3, s10, dyn
[0x8000d380]:csrrs a7, fcsr, zero

[0x8000d37c]:fadd.d t5, t3, s10, dyn
[0x8000d380]:csrrs a7, fcsr, zero
[0x8000d384]:sw t5, 1568(ra)
[0x8000d388]:sw t6, 1576(ra)
[0x8000d38c]:sw t5, 1584(ra)
[0x8000d390]:sw a7, 1592(ra)
[0x8000d394]:lw t3, 800(a6)
[0x8000d398]:lw t4, 804(a6)
[0x8000d39c]:lw s10, 808(a6)
[0x8000d3a0]:lw s11, 812(a6)
[0x8000d3a4]:addi t3, zero, 0
[0x8000d3a8]:lui t4, 784384
[0x8000d3ac]:addi s10, zero, 1
[0x8000d3b0]:lui s11, 1048320
[0x8000d3b4]:addi a4, zero, 0
[0x8000d3b8]:csrrw zero, fcsr, a4
[0x8000d3bc]:fadd.d t5, t3, s10, dyn
[0x8000d3c0]:csrrs a7, fcsr, zero

[0x8000d3bc]:fadd.d t5, t3, s10, dyn
[0x8000d3c0]:csrrs a7, fcsr, zero
[0x8000d3c4]:sw t5, 1600(ra)
[0x8000d3c8]:sw t6, 1608(ra)
[0x8000d3cc]:sw t5, 1616(ra)
[0x8000d3d0]:sw a7, 1624(ra)
[0x8000d3d4]:lw t3, 816(a6)
[0x8000d3d8]:lw t4, 820(a6)
[0x8000d3dc]:lw s10, 824(a6)
[0x8000d3e0]:lw s11, 828(a6)
[0x8000d3e4]:addi t3, zero, 0
[0x8000d3e8]:lui t4, 784384
[0x8000d3ec]:addi s10, zero, 0
[0x8000d3f0]:lui s11, 261888
[0x8000d3f4]:addi a4, zero, 0
[0x8000d3f8]:csrrw zero, fcsr, a4
[0x8000d3fc]:fadd.d t5, t3, s10, dyn
[0x8000d400]:csrrs a7, fcsr, zero

[0x8000d3fc]:fadd.d t5, t3, s10, dyn
[0x8000d400]:csrrs a7, fcsr, zero
[0x8000d404]:sw t5, 1632(ra)
[0x8000d408]:sw t6, 1640(ra)
[0x8000d40c]:sw t5, 1648(ra)
[0x8000d410]:sw a7, 1656(ra)
[0x8000d414]:lw t3, 832(a6)
[0x8000d418]:lw t4, 836(a6)
[0x8000d41c]:lw s10, 840(a6)
[0x8000d420]:lw s11, 844(a6)
[0x8000d424]:addi t3, zero, 0
[0x8000d428]:lui t4, 784384
[0x8000d42c]:addi s10, zero, 0
[0x8000d430]:lui s11, 784384
[0x8000d434]:addi a4, zero, 0
[0x8000d438]:csrrw zero, fcsr, a4
[0x8000d43c]:fadd.d t5, t3, s10, dyn
[0x8000d440]:csrrs a7, fcsr, zero

[0x8000d43c]:fadd.d t5, t3, s10, dyn
[0x8000d440]:csrrs a7, fcsr, zero
[0x8000d444]:sw t5, 1664(ra)
[0x8000d448]:sw t6, 1672(ra)
[0x8000d44c]:sw t5, 1680(ra)
[0x8000d450]:sw a7, 1688(ra)
[0x8000d454]:lw t3, 848(a6)
[0x8000d458]:lw t4, 852(a6)
[0x8000d45c]:lw s10, 856(a6)
[0x8000d460]:lw s11, 860(a6)
[0x8000d464]:addi t3, zero, 0
[0x8000d468]:addi t4, zero, 0
[0x8000d46c]:addi s10, zero, 0
[0x8000d470]:addi s11, zero, 0
[0x8000d474]:addi a4, zero, 0
[0x8000d478]:csrrw zero, fcsr, a4
[0x8000d47c]:fadd.d t5, t3, s10, dyn
[0x8000d480]:csrrs a7, fcsr, zero

[0x8000d4bc]:fadd.d t5, t3, s10, dyn
[0x8000d4c0]:csrrs a7, fcsr, zero
[0x8000d4c4]:sw t5, 1728(ra)
[0x8000d4c8]:sw t6, 1736(ra)
[0x8000d4cc]:sw t5, 1744(ra)
[0x8000d4d0]:sw a7, 1752(ra)
[0x8000d4d4]:addi zero, zero, 0
[0x8000d4d8]:addi zero, zero, 0
[0x8000d4dc]:addi zero, zero, 0



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fadd.d', 'rs1 : x30', 'rs2 : x30', 'rd : x30', 'rs1 == rs2 == rd', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000013c]:fadd.d t5, t5, t5, dyn
	-[0x80000140]:csrrs tp, fcsr, zero
	-[0x80000144]:sw t5, 0(ra)
	-[0x80000148]:sw t6, 8(ra)
Current Store : [0x80000148] : sw t6, 8(ra) -- Store: [0x80011620]:0x00000000




Last Coverpoint : ['mnemonic : fadd.d', 'rs1 : x30', 'rs2 : x30', 'rd : x30', 'rs1 == rs2 == rd', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000013c]:fadd.d t5, t5, t5, dyn
	-[0x80000140]:csrrs tp, fcsr, zero
	-[0x80000144]:sw t5, 0(ra)
	-[0x80000148]:sw t6, 8(ra)
	-[0x8000014c]:sw t5, 16(ra)
Current Store : [0x8000014c] : sw t5, 16(ra) -- Store: [0x80011628]:0x00000000




Last Coverpoint : ['rs1 : x26', 'rs2 : x26', 'rd : x28', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x8000017c]:fadd.d t3, s10, s10, dyn
	-[0x80000180]:csrrs tp, fcsr, zero
	-[0x80000184]:sw t3, 32(ra)
	-[0x80000188]:sw t4, 40(ra)
Current Store : [0x80000188] : sw t4, 40(ra) -- Store: [0x80011640]:0xEEDBEADF




Last Coverpoint : ['rs1 : x26', 'rs2 : x26', 'rd : x28', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x8000017c]:fadd.d t3, s10, s10, dyn
	-[0x80000180]:csrrs tp, fcsr, zero
	-[0x80000184]:sw t3, 32(ra)
	-[0x80000188]:sw t4, 40(ra)
	-[0x8000018c]:sw t3, 48(ra)
Current Store : [0x8000018c] : sw t3, 48(ra) -- Store: [0x80011648]:0x00000000




Last Coverpoint : ['rs1 : x28', 'rs2 : x24', 'rd : x26', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001bc]:fadd.d s10, t3, s8, dyn
	-[0x800001c0]:csrrs tp, fcsr, zero
	-[0x800001c4]:sw s10, 64(ra)
	-[0x800001c8]:sw s11, 72(ra)
Current Store : [0x800001c8] : sw s11, 72(ra) -- Store: [0x80011660]:0x00000000




Last Coverpoint : ['rs1 : x28', 'rs2 : x24', 'rd : x26', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001bc]:fadd.d s10, t3, s8, dyn
	-[0x800001c0]:csrrs tp, fcsr, zero
	-[0x800001c4]:sw s10, 64(ra)
	-[0x800001c8]:sw s11, 72(ra)
	-[0x800001cc]:sw s10, 80(ra)
Current Store : [0x800001cc] : sw s10, 80(ra) -- Store: [0x80011668]:0x00000001




Last Coverpoint : ['rs1 : x24', 'rs2 : x28', 'rd : x24', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001fc]:fadd.d s8, s8, t3, dyn
	-[0x80000200]:csrrs tp, fcsr, zero
	-[0x80000204]:sw s8, 96(ra)
	-[0x80000208]:sw s9, 104(ra)
Current Store : [0x80000208] : sw s9, 104(ra) -- Store: [0x80011680]:0x00000000




Last Coverpoint : ['rs1 : x24', 'rs2 : x28', 'rd : x24', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001fc]:fadd.d s8, s8, t3, dyn
	-[0x80000200]:csrrs tp, fcsr, zero
	-[0x80000204]:sw s8, 96(ra)
	-[0x80000208]:sw s9, 104(ra)
	-[0x8000020c]:sw s8, 112(ra)
Current Store : [0x8000020c] : sw s8, 112(ra) -- Store: [0x80011688]:0x00000001




Last Coverpoint : ['rs1 : x20', 'rs2 : x22', 'rd : x22', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000023c]:fadd.d s6, s4, s6, dyn
	-[0x80000240]:csrrs tp, fcsr, zero
	-[0x80000244]:sw s6, 128(ra)
	-[0x80000248]:sw s7, 136(ra)
Current Store : [0x80000248] : sw s7, 136(ra) -- Store: [0x800116a0]:0x00000000




Last Coverpoint : ['rs1 : x20', 'rs2 : x22', 'rd : x22', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000023c]:fadd.d s6, s4, s6, dyn
	-[0x80000240]:csrrs tp, fcsr, zero
	-[0x80000244]:sw s6, 128(ra)
	-[0x80000248]:sw s7, 136(ra)
	-[0x8000024c]:sw s6, 144(ra)
Current Store : [0x8000024c] : sw s6, 144(ra) -- Store: [0x800116a8]:0x00000002




Last Coverpoint : ['rs1 : x22', 'rs2 : x18', 'rd : x20', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000027c]:fadd.d s4, s6, s2, dyn
	-[0x80000280]:csrrs tp, fcsr, zero
	-[0x80000284]:sw s4, 160(ra)
	-[0x80000288]:sw s5, 168(ra)
Current Store : [0x80000288] : sw s5, 168(ra) -- Store: [0x800116c0]:0x00000000




Last Coverpoint : ['rs1 : x22', 'rs2 : x18', 'rd : x20', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000027c]:fadd.d s4, s6, s2, dyn
	-[0x80000280]:csrrs tp, fcsr, zero
	-[0x80000284]:sw s4, 160(ra)
	-[0x80000288]:sw s5, 168(ra)
	-[0x8000028c]:sw s4, 176(ra)
Current Store : [0x8000028c] : sw s4, 176(ra) -- Store: [0x800116c8]:0x00000002




Last Coverpoint : ['rs1 : x16', 'rs2 : x20', 'rd : x18', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002c0]:fadd.d s2, a6, s4, dyn
	-[0x800002c4]:csrrs tp, fcsr, zero
	-[0x800002c8]:sw s2, 192(ra)
	-[0x800002cc]:sw s3, 200(ra)
Current Store : [0x800002cc] : sw s3, 200(ra) -- Store: [0x800116e0]:0x80000000




Last Coverpoint : ['rs1 : x16', 'rs2 : x20', 'rd : x18', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002c0]:fadd.d s2, a6, s4, dyn
	-[0x800002c4]:csrrs tp, fcsr, zero
	-[0x800002c8]:sw s2, 192(ra)
	-[0x800002cc]:sw s3, 200(ra)
	-[0x800002d0]:sw s2, 208(ra)
Current Store : [0x800002d0] : sw s2, 208(ra) -- Store: [0x800116e8]:0xFFFFFFFF




Last Coverpoint : ['rs1 : x18', 'rs2 : x14', 'rd : x16', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000304]:fadd.d a6, s2, a4, dyn
	-[0x80000308]:csrrs tp, fcsr, zero
	-[0x8000030c]:sw a6, 224(ra)
	-[0x80000310]:sw a7, 232(ra)
Current Store : [0x80000310] : sw a7, 232(ra) -- Store: [0x80011700]:0x00000000




Last Coverpoint : ['rs1 : x18', 'rs2 : x14', 'rd : x16', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000304]:fadd.d a6, s2, a4, dyn
	-[0x80000308]:csrrs tp, fcsr, zero
	-[0x8000030c]:sw a6, 224(ra)
	-[0x80000310]:sw a7, 232(ra)
	-[0x80000314]:sw a6, 240(ra)
Current Store : [0x80000314] : sw a6, 240(ra) -- Store: [0x80011708]:0xFFFFFFFF




Last Coverpoint : ['rs1 : x12', 'rs2 : x16', 'rd : x14', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000344]:fadd.d a4, a2, a6, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:sw a4, 256(ra)
	-[0x80000350]:sw a5, 264(ra)
Current Store : [0x80000350] : sw a5, 264(ra) -- Store: [0x80011720]:0x800FFFFF




Last Coverpoint : ['rs1 : x12', 'rs2 : x16', 'rd : x14', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000344]:fadd.d a4, a2, a6, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:sw a4, 256(ra)
	-[0x80000350]:sw a5, 264(ra)
	-[0x80000354]:sw a4, 272(ra)
Current Store : [0x80000354] : sw a4, 272(ra) -- Store: [0x80011728]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rs2 : x10', 'rd : x12', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000038c]:fadd.d a2, a4, a0, dyn
	-[0x80000390]:csrrs a7, fcsr, zero
	-[0x80000394]:sw a2, 288(ra)
	-[0x80000398]:sw a3, 296(ra)
Current Store : [0x80000398] : sw a3, 296(ra) -- Store: [0x80011740]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rs2 : x10', 'rd : x12', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000038c]:fadd.d a2, a4, a0, dyn
	-[0x80000390]:csrrs a7, fcsr, zero
	-[0x80000394]:sw a2, 288(ra)
	-[0x80000398]:sw a3, 296(ra)
	-[0x8000039c]:sw a2, 304(ra)
Current Store : [0x8000039c] : sw a2, 304(ra) -- Store: [0x80011748]:0x00000000




Last Coverpoint : ['rs1 : x8', 'rs2 : x12', 'rd : x10', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003cc]:fadd.d a0, fp, a2, dyn
	-[0x800003d0]:csrrs a7, fcsr, zero
	-[0x800003d4]:sw a0, 320(ra)
	-[0x800003d8]:sw a1, 328(ra)
Current Store : [0x800003d8] : sw a1, 328(ra) -- Store: [0x80011760]:0x80100000




Last Coverpoint : ['rs1 : x8', 'rs2 : x12', 'rd : x10', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003cc]:fadd.d a0, fp, a2, dyn
	-[0x800003d0]:csrrs a7, fcsr, zero
	-[0x800003d4]:sw a0, 320(ra)
	-[0x800003d8]:sw a1, 328(ra)
	-[0x800003dc]:sw a0, 336(ra)
Current Store : [0x800003dc] : sw a0, 336(ra) -- Store: [0x80011768]:0x00000002




Last Coverpoint : ['rs1 : x10', 'rs2 : x6', 'rd : x8', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000414]:fadd.d fp, a0, t1, dyn
	-[0x80000418]:csrrs a7, fcsr, zero
	-[0x8000041c]:sw fp, 0(ra)
	-[0x80000420]:sw s1, 8(ra)
Current Store : [0x80000420] : sw s1, 8(ra) -- Store: [0x800116d0]:0x00000000




Last Coverpoint : ['rs1 : x10', 'rs2 : x6', 'rd : x8', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000414]:fadd.d fp, a0, t1, dyn
	-[0x80000418]:csrrs a7, fcsr, zero
	-[0x8000041c]:sw fp, 0(ra)
	-[0x80000420]:sw s1, 8(ra)
	-[0x80000424]:sw fp, 16(ra)
Current Store : [0x80000424] : sw fp, 16(ra) -- Store: [0x800116d8]:0x00000002




Last Coverpoint : ['rs1 : x4', 'rs2 : x8', 'rd : x6', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000458]:fadd.d t1, tp, fp, dyn
	-[0x8000045c]:csrrs a7, fcsr, zero
	-[0x80000460]:sw t1, 32(ra)
	-[0x80000464]:sw t2, 40(ra)
Current Store : [0x80000464] : sw t2, 40(ra) -- Store: [0x800116f0]:0x80100000




Last Coverpoint : ['rs1 : x4', 'rs2 : x8', 'rd : x6', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000458]:fadd.d t1, tp, fp, dyn
	-[0x8000045c]:csrrs a7, fcsr, zero
	-[0x80000460]:sw t1, 32(ra)
	-[0x80000464]:sw t2, 40(ra)
	-[0x80000468]:sw t1, 48(ra)
Current Store : [0x80000468] : sw t1, 48(ra) -- Store: [0x800116f8]:0xFFFFFFFF




Last Coverpoint : ['rs1 : x6', 'rs2 : x2', 'rd : x4', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000049c]:fadd.d tp, t1, sp, dyn
	-[0x800004a0]:csrrs a7, fcsr, zero
	-[0x800004a4]:sw tp, 64(ra)
	-[0x800004a8]:sw t0, 72(ra)
Current Store : [0x800004a8] : sw t0, 72(ra) -- Store: [0x80011710]:0x00000000




Last Coverpoint : ['rs1 : x6', 'rs2 : x2', 'rd : x4', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000049c]:fadd.d tp, t1, sp, dyn
	-[0x800004a0]:csrrs a7, fcsr, zero
	-[0x800004a4]:sw tp, 64(ra)
	-[0x800004a8]:sw t0, 72(ra)
	-[0x800004ac]:sw tp, 80(ra)
Current Store : [0x800004ac] : sw tp, 80(ra) -- Store: [0x80011718]:0xFFFFFFFF




Last Coverpoint : ['rs1 : x2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004dc]:fadd.d t5, sp, t3, dyn
	-[0x800004e0]:csrrs a7, fcsr, zero
	-[0x800004e4]:sw t5, 96(ra)
	-[0x800004e8]:sw t6, 104(ra)
Current Store : [0x800004e8] : sw t6, 104(ra) -- Store: [0x80011730]:0x00000000




Last Coverpoint : ['rs1 : x2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004dc]:fadd.d t5, sp, t3, dyn
	-[0x800004e0]:csrrs a7, fcsr, zero
	-[0x800004e4]:sw t5, 96(ra)
	-[0x800004e8]:sw t6, 104(ra)
	-[0x800004ec]:sw t5, 112(ra)
Current Store : [0x800004ec] : sw t5, 112(ra) -- Store: [0x80011738]:0x00000000




Last Coverpoint : ['rs2 : x4', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000051c]:fadd.d t5, t3, tp, dyn
	-[0x80000520]:csrrs a7, fcsr, zero
	-[0x80000524]:sw t5, 128(ra)
	-[0x80000528]:sw t6, 136(ra)
Current Store : [0x80000528] : sw t6, 136(ra) -- Store: [0x80011750]:0x00000000




Last Coverpoint : ['rs2 : x4', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000051c]:fadd.d t5, t3, tp, dyn
	-[0x80000520]:csrrs a7, fcsr, zero
	-[0x80000524]:sw t5, 128(ra)
	-[0x80000528]:sw t6, 136(ra)
	-[0x8000052c]:sw t5, 144(ra)
Current Store : [0x8000052c] : sw t5, 144(ra) -- Store: [0x80011758]:0x00000000




Last Coverpoint : ['rd : x2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000055c]:fadd.d sp, t5, t3, dyn
	-[0x80000560]:csrrs a7, fcsr, zero
	-[0x80000564]:sw sp, 160(ra)
	-[0x80000568]:sw gp, 168(ra)
Current Store : [0x80000568] : sw gp, 168(ra) -- Store: [0x80011770]:0x00000000




Last Coverpoint : ['rd : x2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000055c]:fadd.d sp, t5, t3, dyn
	-[0x80000560]:csrrs a7, fcsr, zero
	-[0x80000564]:sw sp, 160(ra)
	-[0x80000568]:sw gp, 168(ra)
	-[0x8000056c]:sw sp, 176(ra)
Current Store : [0x8000056c] : sw sp, 176(ra) -- Store: [0x80011778]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000059c]:fadd.d t5, t3, s10, dyn
	-[0x800005a0]:csrrs a7, fcsr, zero
	-[0x800005a4]:sw t5, 192(ra)
	-[0x800005a8]:sw t6, 200(ra)
Current Store : [0x800005a8] : sw t6, 200(ra) -- Store: [0x80011790]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000059c]:fadd.d t5, t3, s10, dyn
	-[0x800005a0]:csrrs a7, fcsr, zero
	-[0x800005a4]:sw t5, 192(ra)
	-[0x800005a8]:sw t6, 200(ra)
	-[0x800005ac]:sw t5, 208(ra)
Current Store : [0x800005ac] : sw t5, 208(ra) -- Store: [0x80011798]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005dc]:fadd.d t5, t3, s10, dyn
	-[0x800005e0]:csrrs a7, fcsr, zero
	-[0x800005e4]:sw t5, 224(ra)
	-[0x800005e8]:sw t6, 232(ra)
Current Store : [0x800005e8] : sw t6, 232(ra) -- Store: [0x800117b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005dc]:fadd.d t5, t3, s10, dyn
	-[0x800005e0]:csrrs a7, fcsr, zero
	-[0x800005e4]:sw t5, 224(ra)
	-[0x800005e8]:sw t6, 232(ra)
	-[0x800005ec]:sw t5, 240(ra)
Current Store : [0x800005ec] : sw t5, 240(ra) -- Store: [0x800117b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000061c]:fadd.d t5, t3, s10, dyn
	-[0x80000620]:csrrs a7, fcsr, zero
	-[0x80000624]:sw t5, 256(ra)
	-[0x80000628]:sw t6, 264(ra)
Current Store : [0x80000628] : sw t6, 264(ra) -- Store: [0x800117d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000061c]:fadd.d t5, t3, s10, dyn
	-[0x80000620]:csrrs a7, fcsr, zero
	-[0x80000624]:sw t5, 256(ra)
	-[0x80000628]:sw t6, 264(ra)
	-[0x8000062c]:sw t5, 272(ra)
Current Store : [0x8000062c] : sw t5, 272(ra) -- Store: [0x800117d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000065c]:fadd.d t5, t3, s10, dyn
	-[0x80000660]:csrrs a7, fcsr, zero
	-[0x80000664]:sw t5, 288(ra)
	-[0x80000668]:sw t6, 296(ra)
Current Store : [0x80000668] : sw t6, 296(ra) -- Store: [0x800117f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000065c]:fadd.d t5, t3, s10, dyn
	-[0x80000660]:csrrs a7, fcsr, zero
	-[0x80000664]:sw t5, 288(ra)
	-[0x80000668]:sw t6, 296(ra)
	-[0x8000066c]:sw t5, 304(ra)
Current Store : [0x8000066c] : sw t5, 304(ra) -- Store: [0x800117f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000069c]:fadd.d t5, t3, s10, dyn
	-[0x800006a0]:csrrs a7, fcsr, zero
	-[0x800006a4]:sw t5, 320(ra)
	-[0x800006a8]:sw t6, 328(ra)
Current Store : [0x800006a8] : sw t6, 328(ra) -- Store: [0x80011810]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000069c]:fadd.d t5, t3, s10, dyn
	-[0x800006a0]:csrrs a7, fcsr, zero
	-[0x800006a4]:sw t5, 320(ra)
	-[0x800006a8]:sw t6, 328(ra)
	-[0x800006ac]:sw t5, 336(ra)
Current Store : [0x800006ac] : sw t5, 336(ra) -- Store: [0x80011818]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006dc]:fadd.d t5, t3, s10, dyn
	-[0x800006e0]:csrrs a7, fcsr, zero
	-[0x800006e4]:sw t5, 352(ra)
	-[0x800006e8]:sw t6, 360(ra)
Current Store : [0x800006e8] : sw t6, 360(ra) -- Store: [0x80011830]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006dc]:fadd.d t5, t3, s10, dyn
	-[0x800006e0]:csrrs a7, fcsr, zero
	-[0x800006e4]:sw t5, 352(ra)
	-[0x800006e8]:sw t6, 360(ra)
	-[0x800006ec]:sw t5, 368(ra)
Current Store : [0x800006ec] : sw t5, 368(ra) -- Store: [0x80011838]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000071c]:fadd.d t5, t3, s10, dyn
	-[0x80000720]:csrrs a7, fcsr, zero
	-[0x80000724]:sw t5, 384(ra)
	-[0x80000728]:sw t6, 392(ra)
Current Store : [0x80000728] : sw t6, 392(ra) -- Store: [0x80011850]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000071c]:fadd.d t5, t3, s10, dyn
	-[0x80000720]:csrrs a7, fcsr, zero
	-[0x80000724]:sw t5, 384(ra)
	-[0x80000728]:sw t6, 392(ra)
	-[0x8000072c]:sw t5, 400(ra)
Current Store : [0x8000072c] : sw t5, 400(ra) -- Store: [0x80011858]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000075c]:fadd.d t5, t3, s10, dyn
	-[0x80000760]:csrrs a7, fcsr, zero
	-[0x80000764]:sw t5, 416(ra)
	-[0x80000768]:sw t6, 424(ra)
Current Store : [0x80000768] : sw t6, 424(ra) -- Store: [0x80011870]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000075c]:fadd.d t5, t3, s10, dyn
	-[0x80000760]:csrrs a7, fcsr, zero
	-[0x80000764]:sw t5, 416(ra)
	-[0x80000768]:sw t6, 424(ra)
	-[0x8000076c]:sw t5, 432(ra)
Current Store : [0x8000076c] : sw t5, 432(ra) -- Store: [0x80011878]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000079c]:fadd.d t5, t3, s10, dyn
	-[0x800007a0]:csrrs a7, fcsr, zero
	-[0x800007a4]:sw t5, 448(ra)
	-[0x800007a8]:sw t6, 456(ra)
Current Store : [0x800007a8] : sw t6, 456(ra) -- Store: [0x80011890]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000079c]:fadd.d t5, t3, s10, dyn
	-[0x800007a0]:csrrs a7, fcsr, zero
	-[0x800007a4]:sw t5, 448(ra)
	-[0x800007a8]:sw t6, 456(ra)
	-[0x800007ac]:sw t5, 464(ra)
Current Store : [0x800007ac] : sw t5, 464(ra) -- Store: [0x80011898]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007dc]:fadd.d t5, t3, s10, dyn
	-[0x800007e0]:csrrs a7, fcsr, zero
	-[0x800007e4]:sw t5, 480(ra)
	-[0x800007e8]:sw t6, 488(ra)
Current Store : [0x800007e8] : sw t6, 488(ra) -- Store: [0x800118b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007dc]:fadd.d t5, t3, s10, dyn
	-[0x800007e0]:csrrs a7, fcsr, zero
	-[0x800007e4]:sw t5, 480(ra)
	-[0x800007e8]:sw t6, 488(ra)
	-[0x800007ec]:sw t5, 496(ra)
Current Store : [0x800007ec] : sw t5, 496(ra) -- Store: [0x800118b8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000081c]:fadd.d t5, t3, s10, dyn
	-[0x80000820]:csrrs a7, fcsr, zero
	-[0x80000824]:sw t5, 512(ra)
	-[0x80000828]:sw t6, 520(ra)
Current Store : [0x80000828] : sw t6, 520(ra) -- Store: [0x800118d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000081c]:fadd.d t5, t3, s10, dyn
	-[0x80000820]:csrrs a7, fcsr, zero
	-[0x80000824]:sw t5, 512(ra)
	-[0x80000828]:sw t6, 520(ra)
	-[0x8000082c]:sw t5, 528(ra)
Current Store : [0x8000082c] : sw t5, 528(ra) -- Store: [0x800118d8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000085c]:fadd.d t5, t3, s10, dyn
	-[0x80000860]:csrrs a7, fcsr, zero
	-[0x80000864]:sw t5, 544(ra)
	-[0x80000868]:sw t6, 552(ra)
Current Store : [0x80000868] : sw t6, 552(ra) -- Store: [0x800118f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000085c]:fadd.d t5, t3, s10, dyn
	-[0x80000860]:csrrs a7, fcsr, zero
	-[0x80000864]:sw t5, 544(ra)
	-[0x80000868]:sw t6, 552(ra)
	-[0x8000086c]:sw t5, 560(ra)
Current Store : [0x8000086c] : sw t5, 560(ra) -- Store: [0x800118f8]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000089c]:fadd.d t5, t3, s10, dyn
	-[0x800008a0]:csrrs a7, fcsr, zero
	-[0x800008a4]:sw t5, 576(ra)
	-[0x800008a8]:sw t6, 584(ra)
Current Store : [0x800008a8] : sw t6, 584(ra) -- Store: [0x80011910]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000089c]:fadd.d t5, t3, s10, dyn
	-[0x800008a0]:csrrs a7, fcsr, zero
	-[0x800008a4]:sw t5, 576(ra)
	-[0x800008a8]:sw t6, 584(ra)
	-[0x800008ac]:sw t5, 592(ra)
Current Store : [0x800008ac] : sw t5, 592(ra) -- Store: [0x80011918]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008e0]:fadd.d t5, t3, s10, dyn
	-[0x800008e4]:csrrs a7, fcsr, zero
	-[0x800008e8]:sw t5, 608(ra)
	-[0x800008ec]:sw t6, 616(ra)
Current Store : [0x800008ec] : sw t6, 616(ra) -- Store: [0x80011930]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008e0]:fadd.d t5, t3, s10, dyn
	-[0x800008e4]:csrrs a7, fcsr, zero
	-[0x800008e8]:sw t5, 608(ra)
	-[0x800008ec]:sw t6, 616(ra)
	-[0x800008f0]:sw t5, 624(ra)
Current Store : [0x800008f0] : sw t5, 624(ra) -- Store: [0x80011938]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000924]:fadd.d t5, t3, s10, dyn
	-[0x80000928]:csrrs a7, fcsr, zero
	-[0x8000092c]:sw t5, 640(ra)
	-[0x80000930]:sw t6, 648(ra)
Current Store : [0x80000930] : sw t6, 648(ra) -- Store: [0x80011950]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000924]:fadd.d t5, t3, s10, dyn
	-[0x80000928]:csrrs a7, fcsr, zero
	-[0x8000092c]:sw t5, 640(ra)
	-[0x80000930]:sw t6, 648(ra)
	-[0x80000934]:sw t5, 656(ra)
Current Store : [0x80000934] : sw t5, 656(ra) -- Store: [0x80011958]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000964]:fadd.d t5, t3, s10, dyn
	-[0x80000968]:csrrs a7, fcsr, zero
	-[0x8000096c]:sw t5, 672(ra)
	-[0x80000970]:sw t6, 680(ra)
Current Store : [0x80000970] : sw t6, 680(ra) -- Store: [0x80011970]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000964]:fadd.d t5, t3, s10, dyn
	-[0x80000968]:csrrs a7, fcsr, zero
	-[0x8000096c]:sw t5, 672(ra)
	-[0x80000970]:sw t6, 680(ra)
	-[0x80000974]:sw t5, 688(ra)
Current Store : [0x80000974] : sw t5, 688(ra) -- Store: [0x80011978]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009a4]:fadd.d t5, t3, s10, dyn
	-[0x800009a8]:csrrs a7, fcsr, zero
	-[0x800009ac]:sw t5, 704(ra)
	-[0x800009b0]:sw t6, 712(ra)
Current Store : [0x800009b0] : sw t6, 712(ra) -- Store: [0x80011990]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009a4]:fadd.d t5, t3, s10, dyn
	-[0x800009a8]:csrrs a7, fcsr, zero
	-[0x800009ac]:sw t5, 704(ra)
	-[0x800009b0]:sw t6, 712(ra)
	-[0x800009b4]:sw t5, 720(ra)
Current Store : [0x800009b4] : sw t5, 720(ra) -- Store: [0x80011998]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009e4]:fadd.d t5, t3, s10, dyn
	-[0x800009e8]:csrrs a7, fcsr, zero
	-[0x800009ec]:sw t5, 736(ra)
	-[0x800009f0]:sw t6, 744(ra)
Current Store : [0x800009f0] : sw t6, 744(ra) -- Store: [0x800119b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009e4]:fadd.d t5, t3, s10, dyn
	-[0x800009e8]:csrrs a7, fcsr, zero
	-[0x800009ec]:sw t5, 736(ra)
	-[0x800009f0]:sw t6, 744(ra)
	-[0x800009f4]:sw t5, 752(ra)
Current Store : [0x800009f4] : sw t5, 752(ra) -- Store: [0x800119b8]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a24]:fadd.d t5, t3, s10, dyn
	-[0x80000a28]:csrrs a7, fcsr, zero
	-[0x80000a2c]:sw t5, 768(ra)
	-[0x80000a30]:sw t6, 776(ra)
Current Store : [0x80000a30] : sw t6, 776(ra) -- Store: [0x800119d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a24]:fadd.d t5, t3, s10, dyn
	-[0x80000a28]:csrrs a7, fcsr, zero
	-[0x80000a2c]:sw t5, 768(ra)
	-[0x80000a30]:sw t6, 776(ra)
	-[0x80000a34]:sw t5, 784(ra)
Current Store : [0x80000a34] : sw t5, 784(ra) -- Store: [0x800119d8]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a68]:fadd.d t5, t3, s10, dyn
	-[0x80000a6c]:csrrs a7, fcsr, zero
	-[0x80000a70]:sw t5, 800(ra)
	-[0x80000a74]:sw t6, 808(ra)
Current Store : [0x80000a74] : sw t6, 808(ra) -- Store: [0x800119f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a68]:fadd.d t5, t3, s10, dyn
	-[0x80000a6c]:csrrs a7, fcsr, zero
	-[0x80000a70]:sw t5, 800(ra)
	-[0x80000a74]:sw t6, 808(ra)
	-[0x80000a78]:sw t5, 816(ra)
Current Store : [0x80000a78] : sw t5, 816(ra) -- Store: [0x800119f8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000aac]:fadd.d t5, t3, s10, dyn
	-[0x80000ab0]:csrrs a7, fcsr, zero
	-[0x80000ab4]:sw t5, 832(ra)
	-[0x80000ab8]:sw t6, 840(ra)
Current Store : [0x80000ab8] : sw t6, 840(ra) -- Store: [0x80011a10]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000aac]:fadd.d t5, t3, s10, dyn
	-[0x80000ab0]:csrrs a7, fcsr, zero
	-[0x80000ab4]:sw t5, 832(ra)
	-[0x80000ab8]:sw t6, 840(ra)
	-[0x80000abc]:sw t5, 848(ra)
Current Store : [0x80000abc] : sw t5, 848(ra) -- Store: [0x80011a18]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000aec]:fadd.d t5, t3, s10, dyn
	-[0x80000af0]:csrrs a7, fcsr, zero
	-[0x80000af4]:sw t5, 864(ra)
	-[0x80000af8]:sw t6, 872(ra)
Current Store : [0x80000af8] : sw t6, 872(ra) -- Store: [0x80011a30]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000aec]:fadd.d t5, t3, s10, dyn
	-[0x80000af0]:csrrs a7, fcsr, zero
	-[0x80000af4]:sw t5, 864(ra)
	-[0x80000af8]:sw t6, 872(ra)
	-[0x80000afc]:sw t5, 880(ra)
Current Store : [0x80000afc] : sw t5, 880(ra) -- Store: [0x80011a38]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b2c]:fadd.d t5, t3, s10, dyn
	-[0x80000b30]:csrrs a7, fcsr, zero
	-[0x80000b34]:sw t5, 896(ra)
	-[0x80000b38]:sw t6, 904(ra)
Current Store : [0x80000b38] : sw t6, 904(ra) -- Store: [0x80011a50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b2c]:fadd.d t5, t3, s10, dyn
	-[0x80000b30]:csrrs a7, fcsr, zero
	-[0x80000b34]:sw t5, 896(ra)
	-[0x80000b38]:sw t6, 904(ra)
	-[0x80000b3c]:sw t5, 912(ra)
Current Store : [0x80000b3c] : sw t5, 912(ra) -- Store: [0x80011a58]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b6c]:fadd.d t5, t3, s10, dyn
	-[0x80000b70]:csrrs a7, fcsr, zero
	-[0x80000b74]:sw t5, 928(ra)
	-[0x80000b78]:sw t6, 936(ra)
Current Store : [0x80000b78] : sw t6, 936(ra) -- Store: [0x80011a70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b6c]:fadd.d t5, t3, s10, dyn
	-[0x80000b70]:csrrs a7, fcsr, zero
	-[0x80000b74]:sw t5, 928(ra)
	-[0x80000b78]:sw t6, 936(ra)
	-[0x80000b7c]:sw t5, 944(ra)
Current Store : [0x80000b7c] : sw t5, 944(ra) -- Store: [0x80011a78]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bac]:fadd.d t5, t3, s10, dyn
	-[0x80000bb0]:csrrs a7, fcsr, zero
	-[0x80000bb4]:sw t5, 960(ra)
	-[0x80000bb8]:sw t6, 968(ra)
Current Store : [0x80000bb8] : sw t6, 968(ra) -- Store: [0x80011a90]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bac]:fadd.d t5, t3, s10, dyn
	-[0x80000bb0]:csrrs a7, fcsr, zero
	-[0x80000bb4]:sw t5, 960(ra)
	-[0x80000bb8]:sw t6, 968(ra)
	-[0x80000bbc]:sw t5, 976(ra)
Current Store : [0x80000bbc] : sw t5, 976(ra) -- Store: [0x80011a98]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bec]:fadd.d t5, t3, s10, dyn
	-[0x80000bf0]:csrrs a7, fcsr, zero
	-[0x80000bf4]:sw t5, 992(ra)
	-[0x80000bf8]:sw t6, 1000(ra)
Current Store : [0x80000bf8] : sw t6, 1000(ra) -- Store: [0x80011ab0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bec]:fadd.d t5, t3, s10, dyn
	-[0x80000bf0]:csrrs a7, fcsr, zero
	-[0x80000bf4]:sw t5, 992(ra)
	-[0x80000bf8]:sw t6, 1000(ra)
	-[0x80000bfc]:sw t5, 1008(ra)
Current Store : [0x80000bfc] : sw t5, 1008(ra) -- Store: [0x80011ab8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c2c]:fadd.d t5, t3, s10, dyn
	-[0x80000c30]:csrrs a7, fcsr, zero
	-[0x80000c34]:sw t5, 1024(ra)
	-[0x80000c38]:sw t6, 1032(ra)
Current Store : [0x80000c38] : sw t6, 1032(ra) -- Store: [0x80011ad0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c2c]:fadd.d t5, t3, s10, dyn
	-[0x80000c30]:csrrs a7, fcsr, zero
	-[0x80000c34]:sw t5, 1024(ra)
	-[0x80000c38]:sw t6, 1032(ra)
	-[0x80000c3c]:sw t5, 1040(ra)
Current Store : [0x80000c3c] : sw t5, 1040(ra) -- Store: [0x80011ad8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c6c]:fadd.d t5, t3, s10, dyn
	-[0x80000c70]:csrrs a7, fcsr, zero
	-[0x80000c74]:sw t5, 1056(ra)
	-[0x80000c78]:sw t6, 1064(ra)
Current Store : [0x80000c78] : sw t6, 1064(ra) -- Store: [0x80011af0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c6c]:fadd.d t5, t3, s10, dyn
	-[0x80000c70]:csrrs a7, fcsr, zero
	-[0x80000c74]:sw t5, 1056(ra)
	-[0x80000c78]:sw t6, 1064(ra)
	-[0x80000c7c]:sw t5, 1072(ra)
Current Store : [0x80000c7c] : sw t5, 1072(ra) -- Store: [0x80011af8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cac]:fadd.d t5, t3, s10, dyn
	-[0x80000cb0]:csrrs a7, fcsr, zero
	-[0x80000cb4]:sw t5, 1088(ra)
	-[0x80000cb8]:sw t6, 1096(ra)
Current Store : [0x80000cb8] : sw t6, 1096(ra) -- Store: [0x80011b10]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cac]:fadd.d t5, t3, s10, dyn
	-[0x80000cb0]:csrrs a7, fcsr, zero
	-[0x80000cb4]:sw t5, 1088(ra)
	-[0x80000cb8]:sw t6, 1096(ra)
	-[0x80000cbc]:sw t5, 1104(ra)
Current Store : [0x80000cbc] : sw t5, 1104(ra) -- Store: [0x80011b18]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cec]:fadd.d t5, t3, s10, dyn
	-[0x80000cf0]:csrrs a7, fcsr, zero
	-[0x80000cf4]:sw t5, 1120(ra)
	-[0x80000cf8]:sw t6, 1128(ra)
Current Store : [0x80000cf8] : sw t6, 1128(ra) -- Store: [0x80011b30]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cec]:fadd.d t5, t3, s10, dyn
	-[0x80000cf0]:csrrs a7, fcsr, zero
	-[0x80000cf4]:sw t5, 1120(ra)
	-[0x80000cf8]:sw t6, 1128(ra)
	-[0x80000cfc]:sw t5, 1136(ra)
Current Store : [0x80000cfc] : sw t5, 1136(ra) -- Store: [0x80011b38]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d2c]:fadd.d t5, t3, s10, dyn
	-[0x80000d30]:csrrs a7, fcsr, zero
	-[0x80000d34]:sw t5, 1152(ra)
	-[0x80000d38]:sw t6, 1160(ra)
Current Store : [0x80000d38] : sw t6, 1160(ra) -- Store: [0x80011b50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d2c]:fadd.d t5, t3, s10, dyn
	-[0x80000d30]:csrrs a7, fcsr, zero
	-[0x80000d34]:sw t5, 1152(ra)
	-[0x80000d38]:sw t6, 1160(ra)
	-[0x80000d3c]:sw t5, 1168(ra)
Current Store : [0x80000d3c] : sw t5, 1168(ra) -- Store: [0x80011b58]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d6c]:fadd.d t5, t3, s10, dyn
	-[0x80000d70]:csrrs a7, fcsr, zero
	-[0x80000d74]:sw t5, 1184(ra)
	-[0x80000d78]:sw t6, 1192(ra)
Current Store : [0x80000d78] : sw t6, 1192(ra) -- Store: [0x80011b70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d6c]:fadd.d t5, t3, s10, dyn
	-[0x80000d70]:csrrs a7, fcsr, zero
	-[0x80000d74]:sw t5, 1184(ra)
	-[0x80000d78]:sw t6, 1192(ra)
	-[0x80000d7c]:sw t5, 1200(ra)
Current Store : [0x80000d7c] : sw t5, 1200(ra) -- Store: [0x80011b78]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000dac]:fadd.d t5, t3, s10, dyn
	-[0x80000db0]:csrrs a7, fcsr, zero
	-[0x80000db4]:sw t5, 1216(ra)
	-[0x80000db8]:sw t6, 1224(ra)
Current Store : [0x80000db8] : sw t6, 1224(ra) -- Store: [0x80011b90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000dac]:fadd.d t5, t3, s10, dyn
	-[0x80000db0]:csrrs a7, fcsr, zero
	-[0x80000db4]:sw t5, 1216(ra)
	-[0x80000db8]:sw t6, 1224(ra)
	-[0x80000dbc]:sw t5, 1232(ra)
Current Store : [0x80000dbc] : sw t5, 1232(ra) -- Store: [0x80011b98]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000dec]:fadd.d t5, t3, s10, dyn
	-[0x80000df0]:csrrs a7, fcsr, zero
	-[0x80000df4]:sw t5, 1248(ra)
	-[0x80000df8]:sw t6, 1256(ra)
Current Store : [0x80000df8] : sw t6, 1256(ra) -- Store: [0x80011bb0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000dec]:fadd.d t5, t3, s10, dyn
	-[0x80000df0]:csrrs a7, fcsr, zero
	-[0x80000df4]:sw t5, 1248(ra)
	-[0x80000df8]:sw t6, 1256(ra)
	-[0x80000dfc]:sw t5, 1264(ra)
Current Store : [0x80000dfc] : sw t5, 1264(ra) -- Store: [0x80011bb8]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e2c]:fadd.d t5, t3, s10, dyn
	-[0x80000e30]:csrrs a7, fcsr, zero
	-[0x80000e34]:sw t5, 1280(ra)
	-[0x80000e38]:sw t6, 1288(ra)
Current Store : [0x80000e38] : sw t6, 1288(ra) -- Store: [0x80011bd0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e2c]:fadd.d t5, t3, s10, dyn
	-[0x80000e30]:csrrs a7, fcsr, zero
	-[0x80000e34]:sw t5, 1280(ra)
	-[0x80000e38]:sw t6, 1288(ra)
	-[0x80000e3c]:sw t5, 1296(ra)
Current Store : [0x80000e3c] : sw t5, 1296(ra) -- Store: [0x80011bd8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e6c]:fadd.d t5, t3, s10, dyn
	-[0x80000e70]:csrrs a7, fcsr, zero
	-[0x80000e74]:sw t5, 1312(ra)
	-[0x80000e78]:sw t6, 1320(ra)
Current Store : [0x80000e78] : sw t6, 1320(ra) -- Store: [0x80011bf0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e6c]:fadd.d t5, t3, s10, dyn
	-[0x80000e70]:csrrs a7, fcsr, zero
	-[0x80000e74]:sw t5, 1312(ra)
	-[0x80000e78]:sw t6, 1320(ra)
	-[0x80000e7c]:sw t5, 1328(ra)
Current Store : [0x80000e7c] : sw t5, 1328(ra) -- Store: [0x80011bf8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000eac]:fadd.d t5, t3, s10, dyn
	-[0x80000eb0]:csrrs a7, fcsr, zero
	-[0x80000eb4]:sw t5, 1344(ra)
	-[0x80000eb8]:sw t6, 1352(ra)
Current Store : [0x80000eb8] : sw t6, 1352(ra) -- Store: [0x80011c10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000eac]:fadd.d t5, t3, s10, dyn
	-[0x80000eb0]:csrrs a7, fcsr, zero
	-[0x80000eb4]:sw t5, 1344(ra)
	-[0x80000eb8]:sw t6, 1352(ra)
	-[0x80000ebc]:sw t5, 1360(ra)
Current Store : [0x80000ebc] : sw t5, 1360(ra) -- Store: [0x80011c18]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ef0]:fadd.d t5, t3, s10, dyn
	-[0x80000ef4]:csrrs a7, fcsr, zero
	-[0x80000ef8]:sw t5, 1376(ra)
	-[0x80000efc]:sw t6, 1384(ra)
Current Store : [0x80000efc] : sw t6, 1384(ra) -- Store: [0x80011c30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ef0]:fadd.d t5, t3, s10, dyn
	-[0x80000ef4]:csrrs a7, fcsr, zero
	-[0x80000ef8]:sw t5, 1376(ra)
	-[0x80000efc]:sw t6, 1384(ra)
	-[0x80000f00]:sw t5, 1392(ra)
Current Store : [0x80000f00] : sw t5, 1392(ra) -- Store: [0x80011c38]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f34]:fadd.d t5, t3, s10, dyn
	-[0x80000f38]:csrrs a7, fcsr, zero
	-[0x80000f3c]:sw t5, 1408(ra)
	-[0x80000f40]:sw t6, 1416(ra)
Current Store : [0x80000f40] : sw t6, 1416(ra) -- Store: [0x80011c50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f34]:fadd.d t5, t3, s10, dyn
	-[0x80000f38]:csrrs a7, fcsr, zero
	-[0x80000f3c]:sw t5, 1408(ra)
	-[0x80000f40]:sw t6, 1416(ra)
	-[0x80000f44]:sw t5, 1424(ra)
Current Store : [0x80000f44] : sw t5, 1424(ra) -- Store: [0x80011c58]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f74]:fadd.d t5, t3, s10, dyn
	-[0x80000f78]:csrrs a7, fcsr, zero
	-[0x80000f7c]:sw t5, 1440(ra)
	-[0x80000f80]:sw t6, 1448(ra)
Current Store : [0x80000f80] : sw t6, 1448(ra) -- Store: [0x80011c70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f74]:fadd.d t5, t3, s10, dyn
	-[0x80000f78]:csrrs a7, fcsr, zero
	-[0x80000f7c]:sw t5, 1440(ra)
	-[0x80000f80]:sw t6, 1448(ra)
	-[0x80000f84]:sw t5, 1456(ra)
Current Store : [0x80000f84] : sw t5, 1456(ra) -- Store: [0x80011c78]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fb4]:fadd.d t5, t3, s10, dyn
	-[0x80000fb8]:csrrs a7, fcsr, zero
	-[0x80000fbc]:sw t5, 1472(ra)
	-[0x80000fc0]:sw t6, 1480(ra)
Current Store : [0x80000fc0] : sw t6, 1480(ra) -- Store: [0x80011c90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fb4]:fadd.d t5, t3, s10, dyn
	-[0x80000fb8]:csrrs a7, fcsr, zero
	-[0x80000fbc]:sw t5, 1472(ra)
	-[0x80000fc0]:sw t6, 1480(ra)
	-[0x80000fc4]:sw t5, 1488(ra)
Current Store : [0x80000fc4] : sw t5, 1488(ra) -- Store: [0x80011c98]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ff4]:fadd.d t5, t3, s10, dyn
	-[0x80000ff8]:csrrs a7, fcsr, zero
	-[0x80000ffc]:sw t5, 1504(ra)
	-[0x80001000]:sw t6, 1512(ra)
Current Store : [0x80001000] : sw t6, 1512(ra) -- Store: [0x80011cb0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ff4]:fadd.d t5, t3, s10, dyn
	-[0x80000ff8]:csrrs a7, fcsr, zero
	-[0x80000ffc]:sw t5, 1504(ra)
	-[0x80001000]:sw t6, 1512(ra)
	-[0x80001004]:sw t5, 1520(ra)
Current Store : [0x80001004] : sw t5, 1520(ra) -- Store: [0x80011cb8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001034]:fadd.d t5, t3, s10, dyn
	-[0x80001038]:csrrs a7, fcsr, zero
	-[0x8000103c]:sw t5, 1536(ra)
	-[0x80001040]:sw t6, 1544(ra)
Current Store : [0x80001040] : sw t6, 1544(ra) -- Store: [0x80011cd0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001034]:fadd.d t5, t3, s10, dyn
	-[0x80001038]:csrrs a7, fcsr, zero
	-[0x8000103c]:sw t5, 1536(ra)
	-[0x80001040]:sw t6, 1544(ra)
	-[0x80001044]:sw t5, 1552(ra)
Current Store : [0x80001044] : sw t5, 1552(ra) -- Store: [0x80011cd8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001078]:fadd.d t5, t3, s10, dyn
	-[0x8000107c]:csrrs a7, fcsr, zero
	-[0x80001080]:sw t5, 1568(ra)
	-[0x80001084]:sw t6, 1576(ra)
Current Store : [0x80001084] : sw t6, 1576(ra) -- Store: [0x80011cf0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001078]:fadd.d t5, t3, s10, dyn
	-[0x8000107c]:csrrs a7, fcsr, zero
	-[0x80001080]:sw t5, 1568(ra)
	-[0x80001084]:sw t6, 1576(ra)
	-[0x80001088]:sw t5, 1584(ra)
Current Store : [0x80001088] : sw t5, 1584(ra) -- Store: [0x80011cf8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010bc]:fadd.d t5, t3, s10, dyn
	-[0x800010c0]:csrrs a7, fcsr, zero
	-[0x800010c4]:sw t5, 1600(ra)
	-[0x800010c8]:sw t6, 1608(ra)
Current Store : [0x800010c8] : sw t6, 1608(ra) -- Store: [0x80011d10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010bc]:fadd.d t5, t3, s10, dyn
	-[0x800010c0]:csrrs a7, fcsr, zero
	-[0x800010c4]:sw t5, 1600(ra)
	-[0x800010c8]:sw t6, 1608(ra)
	-[0x800010cc]:sw t5, 1616(ra)
Current Store : [0x800010cc] : sw t5, 1616(ra) -- Store: [0x80011d18]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010fc]:fadd.d t5, t3, s10, dyn
	-[0x80001100]:csrrs a7, fcsr, zero
	-[0x80001104]:sw t5, 1632(ra)
	-[0x80001108]:sw t6, 1640(ra)
Current Store : [0x80001108] : sw t6, 1640(ra) -- Store: [0x80011d30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010fc]:fadd.d t5, t3, s10, dyn
	-[0x80001100]:csrrs a7, fcsr, zero
	-[0x80001104]:sw t5, 1632(ra)
	-[0x80001108]:sw t6, 1640(ra)
	-[0x8000110c]:sw t5, 1648(ra)
Current Store : [0x8000110c] : sw t5, 1648(ra) -- Store: [0x80011d38]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000113c]:fadd.d t5, t3, s10, dyn
	-[0x80001140]:csrrs a7, fcsr, zero
	-[0x80001144]:sw t5, 1664(ra)
	-[0x80001148]:sw t6, 1672(ra)
Current Store : [0x80001148] : sw t6, 1672(ra) -- Store: [0x80011d50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000113c]:fadd.d t5, t3, s10, dyn
	-[0x80001140]:csrrs a7, fcsr, zero
	-[0x80001144]:sw t5, 1664(ra)
	-[0x80001148]:sw t6, 1672(ra)
	-[0x8000114c]:sw t5, 1680(ra)
Current Store : [0x8000114c] : sw t5, 1680(ra) -- Store: [0x80011d58]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000117c]:fadd.d t5, t3, s10, dyn
	-[0x80001180]:csrrs a7, fcsr, zero
	-[0x80001184]:sw t5, 1696(ra)
	-[0x80001188]:sw t6, 1704(ra)
Current Store : [0x80001188] : sw t6, 1704(ra) -- Store: [0x80011d70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000117c]:fadd.d t5, t3, s10, dyn
	-[0x80001180]:csrrs a7, fcsr, zero
	-[0x80001184]:sw t5, 1696(ra)
	-[0x80001188]:sw t6, 1704(ra)
	-[0x8000118c]:sw t5, 1712(ra)
Current Store : [0x8000118c] : sw t5, 1712(ra) -- Store: [0x80011d78]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011bc]:fadd.d t5, t3, s10, dyn
	-[0x800011c0]:csrrs a7, fcsr, zero
	-[0x800011c4]:sw t5, 1728(ra)
	-[0x800011c8]:sw t6, 1736(ra)
Current Store : [0x800011c8] : sw t6, 1736(ra) -- Store: [0x80011d90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011bc]:fadd.d t5, t3, s10, dyn
	-[0x800011c0]:csrrs a7, fcsr, zero
	-[0x800011c4]:sw t5, 1728(ra)
	-[0x800011c8]:sw t6, 1736(ra)
	-[0x800011cc]:sw t5, 1744(ra)
Current Store : [0x800011cc] : sw t5, 1744(ra) -- Store: [0x80011d98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011fc]:fadd.d t5, t3, s10, dyn
	-[0x80001200]:csrrs a7, fcsr, zero
	-[0x80001204]:sw t5, 1760(ra)
	-[0x80001208]:sw t6, 1768(ra)
Current Store : [0x80001208] : sw t6, 1768(ra) -- Store: [0x80011db0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011fc]:fadd.d t5, t3, s10, dyn
	-[0x80001200]:csrrs a7, fcsr, zero
	-[0x80001204]:sw t5, 1760(ra)
	-[0x80001208]:sw t6, 1768(ra)
	-[0x8000120c]:sw t5, 1776(ra)
Current Store : [0x8000120c] : sw t5, 1776(ra) -- Store: [0x80011db8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000123c]:fadd.d t5, t3, s10, dyn
	-[0x80001240]:csrrs a7, fcsr, zero
	-[0x80001244]:sw t5, 1792(ra)
	-[0x80001248]:sw t6, 1800(ra)
Current Store : [0x80001248] : sw t6, 1800(ra) -- Store: [0x80011dd0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000123c]:fadd.d t5, t3, s10, dyn
	-[0x80001240]:csrrs a7, fcsr, zero
	-[0x80001244]:sw t5, 1792(ra)
	-[0x80001248]:sw t6, 1800(ra)
	-[0x8000124c]:sw t5, 1808(ra)
Current Store : [0x8000124c] : sw t5, 1808(ra) -- Store: [0x80011dd8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000127c]:fadd.d t5, t3, s10, dyn
	-[0x80001280]:csrrs a7, fcsr, zero
	-[0x80001284]:sw t5, 1824(ra)
	-[0x80001288]:sw t6, 1832(ra)
Current Store : [0x80001288] : sw t6, 1832(ra) -- Store: [0x80011df0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000127c]:fadd.d t5, t3, s10, dyn
	-[0x80001280]:csrrs a7, fcsr, zero
	-[0x80001284]:sw t5, 1824(ra)
	-[0x80001288]:sw t6, 1832(ra)
	-[0x8000128c]:sw t5, 1840(ra)
Current Store : [0x8000128c] : sw t5, 1840(ra) -- Store: [0x80011df8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012bc]:fadd.d t5, t3, s10, dyn
	-[0x800012c0]:csrrs a7, fcsr, zero
	-[0x800012c4]:sw t5, 1856(ra)
	-[0x800012c8]:sw t6, 1864(ra)
Current Store : [0x800012c8] : sw t6, 1864(ra) -- Store: [0x80011e10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012bc]:fadd.d t5, t3, s10, dyn
	-[0x800012c0]:csrrs a7, fcsr, zero
	-[0x800012c4]:sw t5, 1856(ra)
	-[0x800012c8]:sw t6, 1864(ra)
	-[0x800012cc]:sw t5, 1872(ra)
Current Store : [0x800012cc] : sw t5, 1872(ra) -- Store: [0x80011e18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012fc]:fadd.d t5, t3, s10, dyn
	-[0x80001300]:csrrs a7, fcsr, zero
	-[0x80001304]:sw t5, 1888(ra)
	-[0x80001308]:sw t6, 1896(ra)
Current Store : [0x80001308] : sw t6, 1896(ra) -- Store: [0x80011e30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012fc]:fadd.d t5, t3, s10, dyn
	-[0x80001300]:csrrs a7, fcsr, zero
	-[0x80001304]:sw t5, 1888(ra)
	-[0x80001308]:sw t6, 1896(ra)
	-[0x8000130c]:sw t5, 1904(ra)
Current Store : [0x8000130c] : sw t5, 1904(ra) -- Store: [0x80011e38]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000133c]:fadd.d t5, t3, s10, dyn
	-[0x80001340]:csrrs a7, fcsr, zero
	-[0x80001344]:sw t5, 1920(ra)
	-[0x80001348]:sw t6, 1928(ra)
Current Store : [0x80001348] : sw t6, 1928(ra) -- Store: [0x80011e50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000133c]:fadd.d t5, t3, s10, dyn
	-[0x80001340]:csrrs a7, fcsr, zero
	-[0x80001344]:sw t5, 1920(ra)
	-[0x80001348]:sw t6, 1928(ra)
	-[0x8000134c]:sw t5, 1936(ra)
Current Store : [0x8000134c] : sw t5, 1936(ra) -- Store: [0x80011e58]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000137c]:fadd.d t5, t3, s10, dyn
	-[0x80001380]:csrrs a7, fcsr, zero
	-[0x80001384]:sw t5, 1952(ra)
	-[0x80001388]:sw t6, 1960(ra)
Current Store : [0x80001388] : sw t6, 1960(ra) -- Store: [0x80011e70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000137c]:fadd.d t5, t3, s10, dyn
	-[0x80001380]:csrrs a7, fcsr, zero
	-[0x80001384]:sw t5, 1952(ra)
	-[0x80001388]:sw t6, 1960(ra)
	-[0x8000138c]:sw t5, 1968(ra)
Current Store : [0x8000138c] : sw t5, 1968(ra) -- Store: [0x80011e78]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013bc]:fadd.d t5, t3, s10, dyn
	-[0x800013c0]:csrrs a7, fcsr, zero
	-[0x800013c4]:sw t5, 1984(ra)
	-[0x800013c8]:sw t6, 1992(ra)
Current Store : [0x800013c8] : sw t6, 1992(ra) -- Store: [0x80011e90]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013bc]:fadd.d t5, t3, s10, dyn
	-[0x800013c0]:csrrs a7, fcsr, zero
	-[0x800013c4]:sw t5, 1984(ra)
	-[0x800013c8]:sw t6, 1992(ra)
	-[0x800013cc]:sw t5, 2000(ra)
Current Store : [0x800013cc] : sw t5, 2000(ra) -- Store: [0x80011e98]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013fc]:fadd.d t5, t3, s10, dyn
	-[0x80001400]:csrrs a7, fcsr, zero
	-[0x80001404]:sw t5, 2016(ra)
	-[0x80001408]:sw t6, 2024(ra)
Current Store : [0x80001408] : sw t6, 2024(ra) -- Store: [0x80011eb0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013fc]:fadd.d t5, t3, s10, dyn
	-[0x80001400]:csrrs a7, fcsr, zero
	-[0x80001404]:sw t5, 2016(ra)
	-[0x80001408]:sw t6, 2024(ra)
	-[0x8000140c]:sw t5, 2032(ra)
Current Store : [0x8000140c] : sw t5, 2032(ra) -- Store: [0x80011eb8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000143c]:fadd.d t5, t3, s10, dyn
	-[0x80001440]:csrrs a7, fcsr, zero
	-[0x80001444]:addi ra, ra, 2040
	-[0x80001448]:sw t5, 8(ra)
	-[0x8000144c]:sw t6, 16(ra)
Current Store : [0x8000144c] : sw t6, 16(ra) -- Store: [0x80011ed0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000143c]:fadd.d t5, t3, s10, dyn
	-[0x80001440]:csrrs a7, fcsr, zero
	-[0x80001444]:addi ra, ra, 2040
	-[0x80001448]:sw t5, 8(ra)
	-[0x8000144c]:sw t6, 16(ra)
	-[0x80001450]:sw t5, 24(ra)
Current Store : [0x80001450] : sw t5, 24(ra) -- Store: [0x80011ed8]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001480]:fadd.d t5, t3, s10, dyn
	-[0x80001484]:csrrs a7, fcsr, zero
	-[0x80001488]:sw t5, 40(ra)
	-[0x8000148c]:sw t6, 48(ra)
Current Store : [0x8000148c] : sw t6, 48(ra) -- Store: [0x80011ef0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001480]:fadd.d t5, t3, s10, dyn
	-[0x80001484]:csrrs a7, fcsr, zero
	-[0x80001488]:sw t5, 40(ra)
	-[0x8000148c]:sw t6, 48(ra)
	-[0x80001490]:sw t5, 56(ra)
Current Store : [0x80001490] : sw t5, 56(ra) -- Store: [0x80011ef8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014c0]:fadd.d t5, t3, s10, dyn
	-[0x800014c4]:csrrs a7, fcsr, zero
	-[0x800014c8]:sw t5, 72(ra)
	-[0x800014cc]:sw t6, 80(ra)
Current Store : [0x800014cc] : sw t6, 80(ra) -- Store: [0x80011f10]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014c0]:fadd.d t5, t3, s10, dyn
	-[0x800014c4]:csrrs a7, fcsr, zero
	-[0x800014c8]:sw t5, 72(ra)
	-[0x800014cc]:sw t6, 80(ra)
	-[0x800014d0]:sw t5, 88(ra)
Current Store : [0x800014d0] : sw t5, 88(ra) -- Store: [0x80011f18]:0x00000003




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001504]:fadd.d t5, t3, s10, dyn
	-[0x80001508]:csrrs a7, fcsr, zero
	-[0x8000150c]:sw t5, 104(ra)
	-[0x80001510]:sw t6, 112(ra)
Current Store : [0x80001510] : sw t6, 112(ra) -- Store: [0x80011f30]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001504]:fadd.d t5, t3, s10, dyn
	-[0x80001508]:csrrs a7, fcsr, zero
	-[0x8000150c]:sw t5, 104(ra)
	-[0x80001510]:sw t6, 112(ra)
	-[0x80001514]:sw t5, 120(ra)
Current Store : [0x80001514] : sw t5, 120(ra) -- Store: [0x80011f38]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001548]:fadd.d t5, t3, s10, dyn
	-[0x8000154c]:csrrs a7, fcsr, zero
	-[0x80001550]:sw t5, 136(ra)
	-[0x80001554]:sw t6, 144(ra)
Current Store : [0x80001554] : sw t6, 144(ra) -- Store: [0x80011f50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001548]:fadd.d t5, t3, s10, dyn
	-[0x8000154c]:csrrs a7, fcsr, zero
	-[0x80001550]:sw t5, 136(ra)
	-[0x80001554]:sw t6, 144(ra)
	-[0x80001558]:sw t5, 152(ra)
Current Store : [0x80001558] : sw t5, 152(ra) -- Store: [0x80011f58]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001588]:fadd.d t5, t3, s10, dyn
	-[0x8000158c]:csrrs a7, fcsr, zero
	-[0x80001590]:sw t5, 168(ra)
	-[0x80001594]:sw t6, 176(ra)
Current Store : [0x80001594] : sw t6, 176(ra) -- Store: [0x80011f70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001588]:fadd.d t5, t3, s10, dyn
	-[0x8000158c]:csrrs a7, fcsr, zero
	-[0x80001590]:sw t5, 168(ra)
	-[0x80001594]:sw t6, 176(ra)
	-[0x80001598]:sw t5, 184(ra)
Current Store : [0x80001598] : sw t5, 184(ra) -- Store: [0x80011f78]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015c8]:fadd.d t5, t3, s10, dyn
	-[0x800015cc]:csrrs a7, fcsr, zero
	-[0x800015d0]:sw t5, 200(ra)
	-[0x800015d4]:sw t6, 208(ra)
Current Store : [0x800015d4] : sw t6, 208(ra) -- Store: [0x80011f90]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015c8]:fadd.d t5, t3, s10, dyn
	-[0x800015cc]:csrrs a7, fcsr, zero
	-[0x800015d0]:sw t5, 200(ra)
	-[0x800015d4]:sw t6, 208(ra)
	-[0x800015d8]:sw t5, 216(ra)
Current Store : [0x800015d8] : sw t5, 216(ra) -- Store: [0x80011f98]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001608]:fadd.d t5, t3, s10, dyn
	-[0x8000160c]:csrrs a7, fcsr, zero
	-[0x80001610]:sw t5, 232(ra)
	-[0x80001614]:sw t6, 240(ra)
Current Store : [0x80001614] : sw t6, 240(ra) -- Store: [0x80011fb0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001608]:fadd.d t5, t3, s10, dyn
	-[0x8000160c]:csrrs a7, fcsr, zero
	-[0x80001610]:sw t5, 232(ra)
	-[0x80001614]:sw t6, 240(ra)
	-[0x80001618]:sw t5, 248(ra)
Current Store : [0x80001618] : sw t5, 248(ra) -- Store: [0x80011fb8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001648]:fadd.d t5, t3, s10, dyn
	-[0x8000164c]:csrrs a7, fcsr, zero
	-[0x80001650]:sw t5, 264(ra)
	-[0x80001654]:sw t6, 272(ra)
Current Store : [0x80001654] : sw t6, 272(ra) -- Store: [0x80011fd0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001648]:fadd.d t5, t3, s10, dyn
	-[0x8000164c]:csrrs a7, fcsr, zero
	-[0x80001650]:sw t5, 264(ra)
	-[0x80001654]:sw t6, 272(ra)
	-[0x80001658]:sw t5, 280(ra)
Current Store : [0x80001658] : sw t5, 280(ra) -- Store: [0x80011fd8]:0x00000003




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000168c]:fadd.d t5, t3, s10, dyn
	-[0x80001690]:csrrs a7, fcsr, zero
	-[0x80001694]:sw t5, 296(ra)
	-[0x80001698]:sw t6, 304(ra)
Current Store : [0x80001698] : sw t6, 304(ra) -- Store: [0x80011ff0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000168c]:fadd.d t5, t3, s10, dyn
	-[0x80001690]:csrrs a7, fcsr, zero
	-[0x80001694]:sw t5, 296(ra)
	-[0x80001698]:sw t6, 304(ra)
	-[0x8000169c]:sw t5, 312(ra)
Current Store : [0x8000169c] : sw t5, 312(ra) -- Store: [0x80011ff8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016d0]:fadd.d t5, t3, s10, dyn
	-[0x800016d4]:csrrs a7, fcsr, zero
	-[0x800016d8]:sw t5, 328(ra)
	-[0x800016dc]:sw t6, 336(ra)
Current Store : [0x800016dc] : sw t6, 336(ra) -- Store: [0x80012010]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016d0]:fadd.d t5, t3, s10, dyn
	-[0x800016d4]:csrrs a7, fcsr, zero
	-[0x800016d8]:sw t5, 328(ra)
	-[0x800016dc]:sw t6, 336(ra)
	-[0x800016e0]:sw t5, 344(ra)
Current Store : [0x800016e0] : sw t5, 344(ra) -- Store: [0x80012018]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001710]:fadd.d t5, t3, s10, dyn
	-[0x80001714]:csrrs a7, fcsr, zero
	-[0x80001718]:sw t5, 360(ra)
	-[0x8000171c]:sw t6, 368(ra)
Current Store : [0x8000171c] : sw t6, 368(ra) -- Store: [0x80012030]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001710]:fadd.d t5, t3, s10, dyn
	-[0x80001714]:csrrs a7, fcsr, zero
	-[0x80001718]:sw t5, 360(ra)
	-[0x8000171c]:sw t6, 368(ra)
	-[0x80001720]:sw t5, 376(ra)
Current Store : [0x80001720] : sw t5, 376(ra) -- Store: [0x80012038]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001750]:fadd.d t5, t3, s10, dyn
	-[0x80001754]:csrrs a7, fcsr, zero
	-[0x80001758]:sw t5, 392(ra)
	-[0x8000175c]:sw t6, 400(ra)
Current Store : [0x8000175c] : sw t6, 400(ra) -- Store: [0x80012050]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001750]:fadd.d t5, t3, s10, dyn
	-[0x80001754]:csrrs a7, fcsr, zero
	-[0x80001758]:sw t5, 392(ra)
	-[0x8000175c]:sw t6, 400(ra)
	-[0x80001760]:sw t5, 408(ra)
Current Store : [0x80001760] : sw t5, 408(ra) -- Store: [0x80012058]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001790]:fadd.d t5, t3, s10, dyn
	-[0x80001794]:csrrs a7, fcsr, zero
	-[0x80001798]:sw t5, 424(ra)
	-[0x8000179c]:sw t6, 432(ra)
Current Store : [0x8000179c] : sw t6, 432(ra) -- Store: [0x80012070]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001790]:fadd.d t5, t3, s10, dyn
	-[0x80001794]:csrrs a7, fcsr, zero
	-[0x80001798]:sw t5, 424(ra)
	-[0x8000179c]:sw t6, 432(ra)
	-[0x800017a0]:sw t5, 440(ra)
Current Store : [0x800017a0] : sw t5, 440(ra) -- Store: [0x80012078]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017d0]:fadd.d t5, t3, s10, dyn
	-[0x800017d4]:csrrs a7, fcsr, zero
	-[0x800017d8]:sw t5, 456(ra)
	-[0x800017dc]:sw t6, 464(ra)
Current Store : [0x800017dc] : sw t6, 464(ra) -- Store: [0x80012090]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017d0]:fadd.d t5, t3, s10, dyn
	-[0x800017d4]:csrrs a7, fcsr, zero
	-[0x800017d8]:sw t5, 456(ra)
	-[0x800017dc]:sw t6, 464(ra)
	-[0x800017e0]:sw t5, 472(ra)
Current Store : [0x800017e0] : sw t5, 472(ra) -- Store: [0x80012098]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001810]:fadd.d t5, t3, s10, dyn
	-[0x80001814]:csrrs a7, fcsr, zero
	-[0x80001818]:sw t5, 488(ra)
	-[0x8000181c]:sw t6, 496(ra)
Current Store : [0x8000181c] : sw t6, 496(ra) -- Store: [0x800120b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001810]:fadd.d t5, t3, s10, dyn
	-[0x80001814]:csrrs a7, fcsr, zero
	-[0x80001818]:sw t5, 488(ra)
	-[0x8000181c]:sw t6, 496(ra)
	-[0x80001820]:sw t5, 504(ra)
Current Store : [0x80001820] : sw t5, 504(ra) -- Store: [0x800120b8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001850]:fadd.d t5, t3, s10, dyn
	-[0x80001854]:csrrs a7, fcsr, zero
	-[0x80001858]:sw t5, 520(ra)
	-[0x8000185c]:sw t6, 528(ra)
Current Store : [0x8000185c] : sw t6, 528(ra) -- Store: [0x800120d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001850]:fadd.d t5, t3, s10, dyn
	-[0x80001854]:csrrs a7, fcsr, zero
	-[0x80001858]:sw t5, 520(ra)
	-[0x8000185c]:sw t6, 528(ra)
	-[0x80001860]:sw t5, 536(ra)
Current Store : [0x80001860] : sw t5, 536(ra) -- Store: [0x800120d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001890]:fadd.d t5, t3, s10, dyn
	-[0x80001894]:csrrs a7, fcsr, zero
	-[0x80001898]:sw t5, 552(ra)
	-[0x8000189c]:sw t6, 560(ra)
Current Store : [0x8000189c] : sw t6, 560(ra) -- Store: [0x800120f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001890]:fadd.d t5, t3, s10, dyn
	-[0x80001894]:csrrs a7, fcsr, zero
	-[0x80001898]:sw t5, 552(ra)
	-[0x8000189c]:sw t6, 560(ra)
	-[0x800018a0]:sw t5, 568(ra)
Current Store : [0x800018a0] : sw t5, 568(ra) -- Store: [0x800120f8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018d0]:fadd.d t5, t3, s10, dyn
	-[0x800018d4]:csrrs a7, fcsr, zero
	-[0x800018d8]:sw t5, 584(ra)
	-[0x800018dc]:sw t6, 592(ra)
Current Store : [0x800018dc] : sw t6, 592(ra) -- Store: [0x80012110]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018d0]:fadd.d t5, t3, s10, dyn
	-[0x800018d4]:csrrs a7, fcsr, zero
	-[0x800018d8]:sw t5, 584(ra)
	-[0x800018dc]:sw t6, 592(ra)
	-[0x800018e0]:sw t5, 600(ra)
Current Store : [0x800018e0] : sw t5, 600(ra) -- Store: [0x80012118]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001910]:fadd.d t5, t3, s10, dyn
	-[0x80001914]:csrrs a7, fcsr, zero
	-[0x80001918]:sw t5, 616(ra)
	-[0x8000191c]:sw t6, 624(ra)
Current Store : [0x8000191c] : sw t6, 624(ra) -- Store: [0x80012130]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001910]:fadd.d t5, t3, s10, dyn
	-[0x80001914]:csrrs a7, fcsr, zero
	-[0x80001918]:sw t5, 616(ra)
	-[0x8000191c]:sw t6, 624(ra)
	-[0x80001920]:sw t5, 632(ra)
Current Store : [0x80001920] : sw t5, 632(ra) -- Store: [0x80012138]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001950]:fadd.d t5, t3, s10, dyn
	-[0x80001954]:csrrs a7, fcsr, zero
	-[0x80001958]:sw t5, 648(ra)
	-[0x8000195c]:sw t6, 656(ra)
Current Store : [0x8000195c] : sw t6, 656(ra) -- Store: [0x80012150]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001950]:fadd.d t5, t3, s10, dyn
	-[0x80001954]:csrrs a7, fcsr, zero
	-[0x80001958]:sw t5, 648(ra)
	-[0x8000195c]:sw t6, 656(ra)
	-[0x80001960]:sw t5, 664(ra)
Current Store : [0x80001960] : sw t5, 664(ra) -- Store: [0x80012158]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001990]:fadd.d t5, t3, s10, dyn
	-[0x80001994]:csrrs a7, fcsr, zero
	-[0x80001998]:sw t5, 680(ra)
	-[0x8000199c]:sw t6, 688(ra)
Current Store : [0x8000199c] : sw t6, 688(ra) -- Store: [0x80012170]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001990]:fadd.d t5, t3, s10, dyn
	-[0x80001994]:csrrs a7, fcsr, zero
	-[0x80001998]:sw t5, 680(ra)
	-[0x8000199c]:sw t6, 688(ra)
	-[0x800019a0]:sw t5, 696(ra)
Current Store : [0x800019a0] : sw t5, 696(ra) -- Store: [0x80012178]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800019d0]:fadd.d t5, t3, s10, dyn
	-[0x800019d4]:csrrs a7, fcsr, zero
	-[0x800019d8]:sw t5, 712(ra)
	-[0x800019dc]:sw t6, 720(ra)
Current Store : [0x800019dc] : sw t6, 720(ra) -- Store: [0x80012190]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800019d0]:fadd.d t5, t3, s10, dyn
	-[0x800019d4]:csrrs a7, fcsr, zero
	-[0x800019d8]:sw t5, 712(ra)
	-[0x800019dc]:sw t6, 720(ra)
	-[0x800019e0]:sw t5, 728(ra)
Current Store : [0x800019e0] : sw t5, 728(ra) -- Store: [0x80012198]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a10]:fadd.d t5, t3, s10, dyn
	-[0x80001a14]:csrrs a7, fcsr, zero
	-[0x80001a18]:sw t5, 744(ra)
	-[0x80001a1c]:sw t6, 752(ra)
Current Store : [0x80001a1c] : sw t6, 752(ra) -- Store: [0x800121b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a10]:fadd.d t5, t3, s10, dyn
	-[0x80001a14]:csrrs a7, fcsr, zero
	-[0x80001a18]:sw t5, 744(ra)
	-[0x80001a1c]:sw t6, 752(ra)
	-[0x80001a20]:sw t5, 760(ra)
Current Store : [0x80001a20] : sw t5, 760(ra) -- Store: [0x800121b8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a50]:fadd.d t5, t3, s10, dyn
	-[0x80001a54]:csrrs a7, fcsr, zero
	-[0x80001a58]:sw t5, 776(ra)
	-[0x80001a5c]:sw t6, 784(ra)
Current Store : [0x80001a5c] : sw t6, 784(ra) -- Store: [0x800121d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a50]:fadd.d t5, t3, s10, dyn
	-[0x80001a54]:csrrs a7, fcsr, zero
	-[0x80001a58]:sw t5, 776(ra)
	-[0x80001a5c]:sw t6, 784(ra)
	-[0x80001a60]:sw t5, 792(ra)
Current Store : [0x80001a60] : sw t5, 792(ra) -- Store: [0x800121d8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a90]:fadd.d t5, t3, s10, dyn
	-[0x80001a94]:csrrs a7, fcsr, zero
	-[0x80001a98]:sw t5, 808(ra)
	-[0x80001a9c]:sw t6, 816(ra)
Current Store : [0x80001a9c] : sw t6, 816(ra) -- Store: [0x800121f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a90]:fadd.d t5, t3, s10, dyn
	-[0x80001a94]:csrrs a7, fcsr, zero
	-[0x80001a98]:sw t5, 808(ra)
	-[0x80001a9c]:sw t6, 816(ra)
	-[0x80001aa0]:sw t5, 824(ra)
Current Store : [0x80001aa0] : sw t5, 824(ra) -- Store: [0x800121f8]:0x00000004




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ad0]:fadd.d t5, t3, s10, dyn
	-[0x80001ad4]:csrrs a7, fcsr, zero
	-[0x80001ad8]:sw t5, 840(ra)
	-[0x80001adc]:sw t6, 848(ra)
Current Store : [0x80001adc] : sw t6, 848(ra) -- Store: [0x80012210]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ad0]:fadd.d t5, t3, s10, dyn
	-[0x80001ad4]:csrrs a7, fcsr, zero
	-[0x80001ad8]:sw t5, 840(ra)
	-[0x80001adc]:sw t6, 848(ra)
	-[0x80001ae0]:sw t5, 856(ra)
Current Store : [0x80001ae0] : sw t5, 856(ra) -- Store: [0x80012218]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b14]:fadd.d t5, t3, s10, dyn
	-[0x80001b18]:csrrs a7, fcsr, zero
	-[0x80001b1c]:sw t5, 872(ra)
	-[0x80001b20]:sw t6, 880(ra)
Current Store : [0x80001b20] : sw t6, 880(ra) -- Store: [0x80012230]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b14]:fadd.d t5, t3, s10, dyn
	-[0x80001b18]:csrrs a7, fcsr, zero
	-[0x80001b1c]:sw t5, 872(ra)
	-[0x80001b20]:sw t6, 880(ra)
	-[0x80001b24]:sw t5, 888(ra)
Current Store : [0x80001b24] : sw t5, 888(ra) -- Store: [0x80012238]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b58]:fadd.d t5, t3, s10, dyn
	-[0x80001b5c]:csrrs a7, fcsr, zero
	-[0x80001b60]:sw t5, 904(ra)
	-[0x80001b64]:sw t6, 912(ra)
Current Store : [0x80001b64] : sw t6, 912(ra) -- Store: [0x80012250]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b58]:fadd.d t5, t3, s10, dyn
	-[0x80001b5c]:csrrs a7, fcsr, zero
	-[0x80001b60]:sw t5, 904(ra)
	-[0x80001b64]:sw t6, 912(ra)
	-[0x80001b68]:sw t5, 920(ra)
Current Store : [0x80001b68] : sw t5, 920(ra) -- Store: [0x80012258]:0xFFFFFFFD




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b98]:fadd.d t5, t3, s10, dyn
	-[0x80001b9c]:csrrs a7, fcsr, zero
	-[0x80001ba0]:sw t5, 936(ra)
	-[0x80001ba4]:sw t6, 944(ra)
Current Store : [0x80001ba4] : sw t6, 944(ra) -- Store: [0x80012270]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b98]:fadd.d t5, t3, s10, dyn
	-[0x80001b9c]:csrrs a7, fcsr, zero
	-[0x80001ba0]:sw t5, 936(ra)
	-[0x80001ba4]:sw t6, 944(ra)
	-[0x80001ba8]:sw t5, 952(ra)
Current Store : [0x80001ba8] : sw t5, 952(ra) -- Store: [0x80012278]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001bd8]:fadd.d t5, t3, s10, dyn
	-[0x80001bdc]:csrrs a7, fcsr, zero
	-[0x80001be0]:sw t5, 968(ra)
	-[0x80001be4]:sw t6, 976(ra)
Current Store : [0x80001be4] : sw t6, 976(ra) -- Store: [0x80012290]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001bd8]:fadd.d t5, t3, s10, dyn
	-[0x80001bdc]:csrrs a7, fcsr, zero
	-[0x80001be0]:sw t5, 968(ra)
	-[0x80001be4]:sw t6, 976(ra)
	-[0x80001be8]:sw t5, 984(ra)
Current Store : [0x80001be8] : sw t5, 984(ra) -- Store: [0x80012298]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c18]:fadd.d t5, t3, s10, dyn
	-[0x80001c1c]:csrrs a7, fcsr, zero
	-[0x80001c20]:sw t5, 1000(ra)
	-[0x80001c24]:sw t6, 1008(ra)
Current Store : [0x80001c24] : sw t6, 1008(ra) -- Store: [0x800122b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c18]:fadd.d t5, t3, s10, dyn
	-[0x80001c1c]:csrrs a7, fcsr, zero
	-[0x80001c20]:sw t5, 1000(ra)
	-[0x80001c24]:sw t6, 1008(ra)
	-[0x80001c28]:sw t5, 1016(ra)
Current Store : [0x80001c28] : sw t5, 1016(ra) -- Store: [0x800122b8]:0x00000004




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c58]:fadd.d t5, t3, s10, dyn
	-[0x80001c5c]:csrrs a7, fcsr, zero
	-[0x80001c60]:sw t5, 1032(ra)
	-[0x80001c64]:sw t6, 1040(ra)
Current Store : [0x80001c64] : sw t6, 1040(ra) -- Store: [0x800122d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c58]:fadd.d t5, t3, s10, dyn
	-[0x80001c5c]:csrrs a7, fcsr, zero
	-[0x80001c60]:sw t5, 1032(ra)
	-[0x80001c64]:sw t6, 1040(ra)
	-[0x80001c68]:sw t5, 1048(ra)
Current Store : [0x80001c68] : sw t5, 1048(ra) -- Store: [0x800122d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c9c]:fadd.d t5, t3, s10, dyn
	-[0x80001ca0]:csrrs a7, fcsr, zero
	-[0x80001ca4]:sw t5, 1064(ra)
	-[0x80001ca8]:sw t6, 1072(ra)
Current Store : [0x80001ca8] : sw t6, 1072(ra) -- Store: [0x800122f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c9c]:fadd.d t5, t3, s10, dyn
	-[0x80001ca0]:csrrs a7, fcsr, zero
	-[0x80001ca4]:sw t5, 1064(ra)
	-[0x80001ca8]:sw t6, 1072(ra)
	-[0x80001cac]:sw t5, 1080(ra)
Current Store : [0x80001cac] : sw t5, 1080(ra) -- Store: [0x800122f8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ce0]:fadd.d t5, t3, s10, dyn
	-[0x80001ce4]:csrrs a7, fcsr, zero
	-[0x80001ce8]:sw t5, 1096(ra)
	-[0x80001cec]:sw t6, 1104(ra)
Current Store : [0x80001cec] : sw t6, 1104(ra) -- Store: [0x80012310]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ce0]:fadd.d t5, t3, s10, dyn
	-[0x80001ce4]:csrrs a7, fcsr, zero
	-[0x80001ce8]:sw t5, 1096(ra)
	-[0x80001cec]:sw t6, 1104(ra)
	-[0x80001cf0]:sw t5, 1112(ra)
Current Store : [0x80001cf0] : sw t5, 1112(ra) -- Store: [0x80012318]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d20]:fadd.d t5, t3, s10, dyn
	-[0x80001d24]:csrrs a7, fcsr, zero
	-[0x80001d28]:sw t5, 1128(ra)
	-[0x80001d2c]:sw t6, 1136(ra)
Current Store : [0x80001d2c] : sw t6, 1136(ra) -- Store: [0x80012330]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d20]:fadd.d t5, t3, s10, dyn
	-[0x80001d24]:csrrs a7, fcsr, zero
	-[0x80001d28]:sw t5, 1128(ra)
	-[0x80001d2c]:sw t6, 1136(ra)
	-[0x80001d30]:sw t5, 1144(ra)
Current Store : [0x80001d30] : sw t5, 1144(ra) -- Store: [0x80012338]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d60]:fadd.d t5, t3, s10, dyn
	-[0x80001d64]:csrrs a7, fcsr, zero
	-[0x80001d68]:sw t5, 1160(ra)
	-[0x80001d6c]:sw t6, 1168(ra)
Current Store : [0x80001d6c] : sw t6, 1168(ra) -- Store: [0x80012350]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d60]:fadd.d t5, t3, s10, dyn
	-[0x80001d64]:csrrs a7, fcsr, zero
	-[0x80001d68]:sw t5, 1160(ra)
	-[0x80001d6c]:sw t6, 1168(ra)
	-[0x80001d70]:sw t5, 1176(ra)
Current Store : [0x80001d70] : sw t5, 1176(ra) -- Store: [0x80012358]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001da0]:fadd.d t5, t3, s10, dyn
	-[0x80001da4]:csrrs a7, fcsr, zero
	-[0x80001da8]:sw t5, 1192(ra)
	-[0x80001dac]:sw t6, 1200(ra)
Current Store : [0x80001dac] : sw t6, 1200(ra) -- Store: [0x80012370]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001da0]:fadd.d t5, t3, s10, dyn
	-[0x80001da4]:csrrs a7, fcsr, zero
	-[0x80001da8]:sw t5, 1192(ra)
	-[0x80001dac]:sw t6, 1200(ra)
	-[0x80001db0]:sw t5, 1208(ra)
Current Store : [0x80001db0] : sw t5, 1208(ra) -- Store: [0x80012378]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001de0]:fadd.d t5, t3, s10, dyn
	-[0x80001de4]:csrrs a7, fcsr, zero
	-[0x80001de8]:sw t5, 1224(ra)
	-[0x80001dec]:sw t6, 1232(ra)
Current Store : [0x80001dec] : sw t6, 1232(ra) -- Store: [0x80012390]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001de0]:fadd.d t5, t3, s10, dyn
	-[0x80001de4]:csrrs a7, fcsr, zero
	-[0x80001de8]:sw t5, 1224(ra)
	-[0x80001dec]:sw t6, 1232(ra)
	-[0x80001df0]:sw t5, 1240(ra)
Current Store : [0x80001df0] : sw t5, 1240(ra) -- Store: [0x80012398]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e20]:fadd.d t5, t3, s10, dyn
	-[0x80001e24]:csrrs a7, fcsr, zero
	-[0x80001e28]:sw t5, 1256(ra)
	-[0x80001e2c]:sw t6, 1264(ra)
Current Store : [0x80001e2c] : sw t6, 1264(ra) -- Store: [0x800123b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e20]:fadd.d t5, t3, s10, dyn
	-[0x80001e24]:csrrs a7, fcsr, zero
	-[0x80001e28]:sw t5, 1256(ra)
	-[0x80001e2c]:sw t6, 1264(ra)
	-[0x80001e30]:sw t5, 1272(ra)
Current Store : [0x80001e30] : sw t5, 1272(ra) -- Store: [0x800123b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e60]:fadd.d t5, t3, s10, dyn
	-[0x80001e64]:csrrs a7, fcsr, zero
	-[0x80001e68]:sw t5, 1288(ra)
	-[0x80001e6c]:sw t6, 1296(ra)
Current Store : [0x80001e6c] : sw t6, 1296(ra) -- Store: [0x800123d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e60]:fadd.d t5, t3, s10, dyn
	-[0x80001e64]:csrrs a7, fcsr, zero
	-[0x80001e68]:sw t5, 1288(ra)
	-[0x80001e6c]:sw t6, 1296(ra)
	-[0x80001e70]:sw t5, 1304(ra)
Current Store : [0x80001e70] : sw t5, 1304(ra) -- Store: [0x800123d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ea0]:fadd.d t5, t3, s10, dyn
	-[0x80001ea4]:csrrs a7, fcsr, zero
	-[0x80001ea8]:sw t5, 1320(ra)
	-[0x80001eac]:sw t6, 1328(ra)
Current Store : [0x80001eac] : sw t6, 1328(ra) -- Store: [0x800123f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ea0]:fadd.d t5, t3, s10, dyn
	-[0x80001ea4]:csrrs a7, fcsr, zero
	-[0x80001ea8]:sw t5, 1320(ra)
	-[0x80001eac]:sw t6, 1328(ra)
	-[0x80001eb0]:sw t5, 1336(ra)
Current Store : [0x80001eb0] : sw t5, 1336(ra) -- Store: [0x800123f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ee0]:fadd.d t5, t3, s10, dyn
	-[0x80001ee4]:csrrs a7, fcsr, zero
	-[0x80001ee8]:sw t5, 1352(ra)
	-[0x80001eec]:sw t6, 1360(ra)
Current Store : [0x80001eec] : sw t6, 1360(ra) -- Store: [0x80012410]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ee0]:fadd.d t5, t3, s10, dyn
	-[0x80001ee4]:csrrs a7, fcsr, zero
	-[0x80001ee8]:sw t5, 1352(ra)
	-[0x80001eec]:sw t6, 1360(ra)
	-[0x80001ef0]:sw t5, 1368(ra)
Current Store : [0x80001ef0] : sw t5, 1368(ra) -- Store: [0x80012418]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f20]:fadd.d t5, t3, s10, dyn
	-[0x80001f24]:csrrs a7, fcsr, zero
	-[0x80001f28]:sw t5, 1384(ra)
	-[0x80001f2c]:sw t6, 1392(ra)
Current Store : [0x80001f2c] : sw t6, 1392(ra) -- Store: [0x80012430]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f20]:fadd.d t5, t3, s10, dyn
	-[0x80001f24]:csrrs a7, fcsr, zero
	-[0x80001f28]:sw t5, 1384(ra)
	-[0x80001f2c]:sw t6, 1392(ra)
	-[0x80001f30]:sw t5, 1400(ra)
Current Store : [0x80001f30] : sw t5, 1400(ra) -- Store: [0x80012438]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f60]:fadd.d t5, t3, s10, dyn
	-[0x80001f64]:csrrs a7, fcsr, zero
	-[0x80001f68]:sw t5, 1416(ra)
	-[0x80001f6c]:sw t6, 1424(ra)
Current Store : [0x80001f6c] : sw t6, 1424(ra) -- Store: [0x80012450]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f60]:fadd.d t5, t3, s10, dyn
	-[0x80001f64]:csrrs a7, fcsr, zero
	-[0x80001f68]:sw t5, 1416(ra)
	-[0x80001f6c]:sw t6, 1424(ra)
	-[0x80001f70]:sw t5, 1432(ra)
Current Store : [0x80001f70] : sw t5, 1432(ra) -- Store: [0x80012458]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001fa0]:fadd.d t5, t3, s10, dyn
	-[0x80001fa4]:csrrs a7, fcsr, zero
	-[0x80001fa8]:sw t5, 1448(ra)
	-[0x80001fac]:sw t6, 1456(ra)
Current Store : [0x80001fac] : sw t6, 1456(ra) -- Store: [0x80012470]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001fa0]:fadd.d t5, t3, s10, dyn
	-[0x80001fa4]:csrrs a7, fcsr, zero
	-[0x80001fa8]:sw t5, 1448(ra)
	-[0x80001fac]:sw t6, 1456(ra)
	-[0x80001fb0]:sw t5, 1464(ra)
Current Store : [0x80001fb0] : sw t5, 1464(ra) -- Store: [0x80012478]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001fe0]:fadd.d t5, t3, s10, dyn
	-[0x80001fe4]:csrrs a7, fcsr, zero
	-[0x80001fe8]:sw t5, 1480(ra)
	-[0x80001fec]:sw t6, 1488(ra)
Current Store : [0x80001fec] : sw t6, 1488(ra) -- Store: [0x80012490]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001fe0]:fadd.d t5, t3, s10, dyn
	-[0x80001fe4]:csrrs a7, fcsr, zero
	-[0x80001fe8]:sw t5, 1480(ra)
	-[0x80001fec]:sw t6, 1488(ra)
	-[0x80001ff0]:sw t5, 1496(ra)
Current Store : [0x80001ff0] : sw t5, 1496(ra) -- Store: [0x80012498]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002020]:fadd.d t5, t3, s10, dyn
	-[0x80002024]:csrrs a7, fcsr, zero
	-[0x80002028]:sw t5, 1512(ra)
	-[0x8000202c]:sw t6, 1520(ra)
Current Store : [0x8000202c] : sw t6, 1520(ra) -- Store: [0x800124b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002020]:fadd.d t5, t3, s10, dyn
	-[0x80002024]:csrrs a7, fcsr, zero
	-[0x80002028]:sw t5, 1512(ra)
	-[0x8000202c]:sw t6, 1520(ra)
	-[0x80002030]:sw t5, 1528(ra)
Current Store : [0x80002030] : sw t5, 1528(ra) -- Store: [0x800124b8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002060]:fadd.d t5, t3, s10, dyn
	-[0x80002064]:csrrs a7, fcsr, zero
	-[0x80002068]:sw t5, 1544(ra)
	-[0x8000206c]:sw t6, 1552(ra)
Current Store : [0x8000206c] : sw t6, 1552(ra) -- Store: [0x800124d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002060]:fadd.d t5, t3, s10, dyn
	-[0x80002064]:csrrs a7, fcsr, zero
	-[0x80002068]:sw t5, 1544(ra)
	-[0x8000206c]:sw t6, 1552(ra)
	-[0x80002070]:sw t5, 1560(ra)
Current Store : [0x80002070] : sw t5, 1560(ra) -- Store: [0x800124d8]:0x00000003




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800020a0]:fadd.d t5, t3, s10, dyn
	-[0x800020a4]:csrrs a7, fcsr, zero
	-[0x800020a8]:sw t5, 1576(ra)
	-[0x800020ac]:sw t6, 1584(ra)
Current Store : [0x800020ac] : sw t6, 1584(ra) -- Store: [0x800124f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800020a0]:fadd.d t5, t3, s10, dyn
	-[0x800020a4]:csrrs a7, fcsr, zero
	-[0x800020a8]:sw t5, 1576(ra)
	-[0x800020ac]:sw t6, 1584(ra)
	-[0x800020b0]:sw t5, 1592(ra)
Current Store : [0x800020b0] : sw t5, 1592(ra) -- Store: [0x800124f8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800020e0]:fadd.d t5, t3, s10, dyn
	-[0x800020e4]:csrrs a7, fcsr, zero
	-[0x800020e8]:sw t5, 1608(ra)
	-[0x800020ec]:sw t6, 1616(ra)
Current Store : [0x800020ec] : sw t6, 1616(ra) -- Store: [0x80012510]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800020e0]:fadd.d t5, t3, s10, dyn
	-[0x800020e4]:csrrs a7, fcsr, zero
	-[0x800020e8]:sw t5, 1608(ra)
	-[0x800020ec]:sw t6, 1616(ra)
	-[0x800020f0]:sw t5, 1624(ra)
Current Store : [0x800020f0] : sw t5, 1624(ra) -- Store: [0x80012518]:0x00000004




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002124]:fadd.d t5, t3, s10, dyn
	-[0x80002128]:csrrs a7, fcsr, zero
	-[0x8000212c]:sw t5, 1640(ra)
	-[0x80002130]:sw t6, 1648(ra)
Current Store : [0x80002130] : sw t6, 1648(ra) -- Store: [0x80012530]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002124]:fadd.d t5, t3, s10, dyn
	-[0x80002128]:csrrs a7, fcsr, zero
	-[0x8000212c]:sw t5, 1640(ra)
	-[0x80002130]:sw t6, 1648(ra)
	-[0x80002134]:sw t5, 1656(ra)
Current Store : [0x80002134] : sw t5, 1656(ra) -- Store: [0x80012538]:0xFFFFFFFD




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002168]:fadd.d t5, t3, s10, dyn
	-[0x8000216c]:csrrs a7, fcsr, zero
	-[0x80002170]:sw t5, 1672(ra)
	-[0x80002174]:sw t6, 1680(ra)
Current Store : [0x80002174] : sw t6, 1680(ra) -- Store: [0x80012550]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002168]:fadd.d t5, t3, s10, dyn
	-[0x8000216c]:csrrs a7, fcsr, zero
	-[0x80002170]:sw t5, 1672(ra)
	-[0x80002174]:sw t6, 1680(ra)
	-[0x80002178]:sw t5, 1688(ra)
Current Store : [0x80002178] : sw t5, 1688(ra) -- Store: [0x80012558]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800021a8]:fadd.d t5, t3, s10, dyn
	-[0x800021ac]:csrrs a7, fcsr, zero
	-[0x800021b0]:sw t5, 1704(ra)
	-[0x800021b4]:sw t6, 1712(ra)
Current Store : [0x800021b4] : sw t6, 1712(ra) -- Store: [0x80012570]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800021a8]:fadd.d t5, t3, s10, dyn
	-[0x800021ac]:csrrs a7, fcsr, zero
	-[0x800021b0]:sw t5, 1704(ra)
	-[0x800021b4]:sw t6, 1712(ra)
	-[0x800021b8]:sw t5, 1720(ra)
Current Store : [0x800021b8] : sw t5, 1720(ra) -- Store: [0x80012578]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800021e8]:fadd.d t5, t3, s10, dyn
	-[0x800021ec]:csrrs a7, fcsr, zero
	-[0x800021f0]:sw t5, 1736(ra)
	-[0x800021f4]:sw t6, 1744(ra)
Current Store : [0x800021f4] : sw t6, 1744(ra) -- Store: [0x80012590]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800021e8]:fadd.d t5, t3, s10, dyn
	-[0x800021ec]:csrrs a7, fcsr, zero
	-[0x800021f0]:sw t5, 1736(ra)
	-[0x800021f4]:sw t6, 1744(ra)
	-[0x800021f8]:sw t5, 1752(ra)
Current Store : [0x800021f8] : sw t5, 1752(ra) -- Store: [0x80012598]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002228]:fadd.d t5, t3, s10, dyn
	-[0x8000222c]:csrrs a7, fcsr, zero
	-[0x80002230]:sw t5, 1768(ra)
	-[0x80002234]:sw t6, 1776(ra)
Current Store : [0x80002234] : sw t6, 1776(ra) -- Store: [0x800125b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002228]:fadd.d t5, t3, s10, dyn
	-[0x8000222c]:csrrs a7, fcsr, zero
	-[0x80002230]:sw t5, 1768(ra)
	-[0x80002234]:sw t6, 1776(ra)
	-[0x80002238]:sw t5, 1784(ra)
Current Store : [0x80002238] : sw t5, 1784(ra) -- Store: [0x800125b8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002268]:fadd.d t5, t3, s10, dyn
	-[0x8000226c]:csrrs a7, fcsr, zero
	-[0x80002270]:sw t5, 1800(ra)
	-[0x80002274]:sw t6, 1808(ra)
Current Store : [0x80002274] : sw t6, 1808(ra) -- Store: [0x800125d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002268]:fadd.d t5, t3, s10, dyn
	-[0x8000226c]:csrrs a7, fcsr, zero
	-[0x80002270]:sw t5, 1800(ra)
	-[0x80002274]:sw t6, 1808(ra)
	-[0x80002278]:sw t5, 1816(ra)
Current Store : [0x80002278] : sw t5, 1816(ra) -- Store: [0x800125d8]:0x00000004




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800022ac]:fadd.d t5, t3, s10, dyn
	-[0x800022b0]:csrrs a7, fcsr, zero
	-[0x800022b4]:sw t5, 1832(ra)
	-[0x800022b8]:sw t6, 1840(ra)
Current Store : [0x800022b8] : sw t6, 1840(ra) -- Store: [0x800125f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800022ac]:fadd.d t5, t3, s10, dyn
	-[0x800022b0]:csrrs a7, fcsr, zero
	-[0x800022b4]:sw t5, 1832(ra)
	-[0x800022b8]:sw t6, 1840(ra)
	-[0x800022bc]:sw t5, 1848(ra)
Current Store : [0x800022bc] : sw t5, 1848(ra) -- Store: [0x800125f8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800022f0]:fadd.d t5, t3, s10, dyn
	-[0x800022f4]:csrrs a7, fcsr, zero
	-[0x800022f8]:sw t5, 1864(ra)
	-[0x800022fc]:sw t6, 1872(ra)
Current Store : [0x800022fc] : sw t6, 1872(ra) -- Store: [0x80012610]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800022f0]:fadd.d t5, t3, s10, dyn
	-[0x800022f4]:csrrs a7, fcsr, zero
	-[0x800022f8]:sw t5, 1864(ra)
	-[0x800022fc]:sw t6, 1872(ra)
	-[0x80002300]:sw t5, 1880(ra)
Current Store : [0x80002300] : sw t5, 1880(ra) -- Store: [0x80012618]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002330]:fadd.d t5, t3, s10, dyn
	-[0x80002334]:csrrs a7, fcsr, zero
	-[0x80002338]:sw t5, 1896(ra)
	-[0x8000233c]:sw t6, 1904(ra)
Current Store : [0x8000233c] : sw t6, 1904(ra) -- Store: [0x80012630]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002330]:fadd.d t5, t3, s10, dyn
	-[0x80002334]:csrrs a7, fcsr, zero
	-[0x80002338]:sw t5, 1896(ra)
	-[0x8000233c]:sw t6, 1904(ra)
	-[0x80002340]:sw t5, 1912(ra)
Current Store : [0x80002340] : sw t5, 1912(ra) -- Store: [0x80012638]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002370]:fadd.d t5, t3, s10, dyn
	-[0x80002374]:csrrs a7, fcsr, zero
	-[0x80002378]:sw t5, 1928(ra)
	-[0x8000237c]:sw t6, 1936(ra)
Current Store : [0x8000237c] : sw t6, 1936(ra) -- Store: [0x80012650]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002370]:fadd.d t5, t3, s10, dyn
	-[0x80002374]:csrrs a7, fcsr, zero
	-[0x80002378]:sw t5, 1928(ra)
	-[0x8000237c]:sw t6, 1936(ra)
	-[0x80002380]:sw t5, 1944(ra)
Current Store : [0x80002380] : sw t5, 1944(ra) -- Store: [0x80012658]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800023b0]:fadd.d t5, t3, s10, dyn
	-[0x800023b4]:csrrs a7, fcsr, zero
	-[0x800023b8]:sw t5, 1960(ra)
	-[0x800023bc]:sw t6, 1968(ra)
Current Store : [0x800023bc] : sw t6, 1968(ra) -- Store: [0x80012670]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800023b0]:fadd.d t5, t3, s10, dyn
	-[0x800023b4]:csrrs a7, fcsr, zero
	-[0x800023b8]:sw t5, 1960(ra)
	-[0x800023bc]:sw t6, 1968(ra)
	-[0x800023c0]:sw t5, 1976(ra)
Current Store : [0x800023c0] : sw t5, 1976(ra) -- Store: [0x80012678]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800023f0]:fadd.d t5, t3, s10, dyn
	-[0x800023f4]:csrrs a7, fcsr, zero
	-[0x800023f8]:sw t5, 1992(ra)
	-[0x800023fc]:sw t6, 2000(ra)
Current Store : [0x800023fc] : sw t6, 2000(ra) -- Store: [0x80012690]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800023f0]:fadd.d t5, t3, s10, dyn
	-[0x800023f4]:csrrs a7, fcsr, zero
	-[0x800023f8]:sw t5, 1992(ra)
	-[0x800023fc]:sw t6, 2000(ra)
	-[0x80002400]:sw t5, 2008(ra)
Current Store : [0x80002400] : sw t5, 2008(ra) -- Store: [0x80012698]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002430]:fadd.d t5, t3, s10, dyn
	-[0x80002434]:csrrs a7, fcsr, zero
	-[0x80002438]:sw t5, 2024(ra)
	-[0x8000243c]:sw t6, 2032(ra)
Current Store : [0x8000243c] : sw t6, 2032(ra) -- Store: [0x800126b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002430]:fadd.d t5, t3, s10, dyn
	-[0x80002434]:csrrs a7, fcsr, zero
	-[0x80002438]:sw t5, 2024(ra)
	-[0x8000243c]:sw t6, 2032(ra)
	-[0x80002440]:sw t5, 2040(ra)
Current Store : [0x80002440] : sw t5, 2040(ra) -- Store: [0x800126b8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800024b4]:fadd.d t5, t3, s10, dyn
	-[0x800024b8]:csrrs a7, fcsr, zero
	-[0x800024bc]:sw t5, 16(ra)
	-[0x800024c0]:sw t6, 24(ra)
Current Store : [0x800024c0] : sw t6, 24(ra) -- Store: [0x800126d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800024b4]:fadd.d t5, t3, s10, dyn
	-[0x800024b8]:csrrs a7, fcsr, zero
	-[0x800024bc]:sw t5, 16(ra)
	-[0x800024c0]:sw t6, 24(ra)
	-[0x800024c4]:sw t5, 32(ra)
Current Store : [0x800024c4] : sw t5, 32(ra) -- Store: [0x800126d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002534]:fadd.d t5, t3, s10, dyn
	-[0x80002538]:csrrs a7, fcsr, zero
	-[0x8000253c]:sw t5, 48(ra)
	-[0x80002540]:sw t6, 56(ra)
Current Store : [0x80002540] : sw t6, 56(ra) -- Store: [0x800126f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002534]:fadd.d t5, t3, s10, dyn
	-[0x80002538]:csrrs a7, fcsr, zero
	-[0x8000253c]:sw t5, 48(ra)
	-[0x80002540]:sw t6, 56(ra)
	-[0x80002544]:sw t5, 64(ra)
Current Store : [0x80002544] : sw t5, 64(ra) -- Store: [0x800126f8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800025b4]:fadd.d t5, t3, s10, dyn
	-[0x800025b8]:csrrs a7, fcsr, zero
	-[0x800025bc]:sw t5, 80(ra)
	-[0x800025c0]:sw t6, 88(ra)
Current Store : [0x800025c0] : sw t6, 88(ra) -- Store: [0x80012710]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800025b4]:fadd.d t5, t3, s10, dyn
	-[0x800025b8]:csrrs a7, fcsr, zero
	-[0x800025bc]:sw t5, 80(ra)
	-[0x800025c0]:sw t6, 88(ra)
	-[0x800025c4]:sw t5, 96(ra)
Current Store : [0x800025c4] : sw t5, 96(ra) -- Store: [0x80012718]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002634]:fadd.d t5, t3, s10, dyn
	-[0x80002638]:csrrs a7, fcsr, zero
	-[0x8000263c]:sw t5, 112(ra)
	-[0x80002640]:sw t6, 120(ra)
Current Store : [0x80002640] : sw t6, 120(ra) -- Store: [0x80012730]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002634]:fadd.d t5, t3, s10, dyn
	-[0x80002638]:csrrs a7, fcsr, zero
	-[0x8000263c]:sw t5, 112(ra)
	-[0x80002640]:sw t6, 120(ra)
	-[0x80002644]:sw t5, 128(ra)
Current Store : [0x80002644] : sw t5, 128(ra) -- Store: [0x80012738]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800026b4]:fadd.d t5, t3, s10, dyn
	-[0x800026b8]:csrrs a7, fcsr, zero
	-[0x800026bc]:sw t5, 144(ra)
	-[0x800026c0]:sw t6, 152(ra)
Current Store : [0x800026c0] : sw t6, 152(ra) -- Store: [0x80012750]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800026b4]:fadd.d t5, t3, s10, dyn
	-[0x800026b8]:csrrs a7, fcsr, zero
	-[0x800026bc]:sw t5, 144(ra)
	-[0x800026c0]:sw t6, 152(ra)
	-[0x800026c4]:sw t5, 160(ra)
Current Store : [0x800026c4] : sw t5, 160(ra) -- Store: [0x80012758]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002738]:fadd.d t5, t3, s10, dyn
	-[0x8000273c]:csrrs a7, fcsr, zero
	-[0x80002740]:sw t5, 176(ra)
	-[0x80002744]:sw t6, 184(ra)
Current Store : [0x80002744] : sw t6, 184(ra) -- Store: [0x80012770]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002738]:fadd.d t5, t3, s10, dyn
	-[0x8000273c]:csrrs a7, fcsr, zero
	-[0x80002740]:sw t5, 176(ra)
	-[0x80002744]:sw t6, 184(ra)
	-[0x80002748]:sw t5, 192(ra)
Current Store : [0x80002748] : sw t5, 192(ra) -- Store: [0x80012778]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800027bc]:fadd.d t5, t3, s10, dyn
	-[0x800027c0]:csrrs a7, fcsr, zero
	-[0x800027c4]:sw t5, 208(ra)
	-[0x800027c8]:sw t6, 216(ra)
Current Store : [0x800027c8] : sw t6, 216(ra) -- Store: [0x80012790]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800027bc]:fadd.d t5, t3, s10, dyn
	-[0x800027c0]:csrrs a7, fcsr, zero
	-[0x800027c4]:sw t5, 208(ra)
	-[0x800027c8]:sw t6, 216(ra)
	-[0x800027cc]:sw t5, 224(ra)
Current Store : [0x800027cc] : sw t5, 224(ra) -- Store: [0x80012798]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002840]:fadd.d t5, t3, s10, dyn
	-[0x80002844]:csrrs a7, fcsr, zero
	-[0x80002848]:sw t5, 240(ra)
	-[0x8000284c]:sw t6, 248(ra)
Current Store : [0x8000284c] : sw t6, 248(ra) -- Store: [0x800127b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002840]:fadd.d t5, t3, s10, dyn
	-[0x80002844]:csrrs a7, fcsr, zero
	-[0x80002848]:sw t5, 240(ra)
	-[0x8000284c]:sw t6, 248(ra)
	-[0x80002850]:sw t5, 256(ra)
Current Store : [0x80002850] : sw t5, 256(ra) -- Store: [0x800127b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800028c4]:fadd.d t5, t3, s10, dyn
	-[0x800028c8]:csrrs a7, fcsr, zero
	-[0x800028cc]:sw t5, 272(ra)
	-[0x800028d0]:sw t6, 280(ra)
Current Store : [0x800028d0] : sw t6, 280(ra) -- Store: [0x800127d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800028c4]:fadd.d t5, t3, s10, dyn
	-[0x800028c8]:csrrs a7, fcsr, zero
	-[0x800028cc]:sw t5, 272(ra)
	-[0x800028d0]:sw t6, 280(ra)
	-[0x800028d4]:sw t5, 288(ra)
Current Store : [0x800028d4] : sw t5, 288(ra) -- Store: [0x800127d8]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002948]:fadd.d t5, t3, s10, dyn
	-[0x8000294c]:csrrs a7, fcsr, zero
	-[0x80002950]:sw t5, 304(ra)
	-[0x80002954]:sw t6, 312(ra)
Current Store : [0x80002954] : sw t6, 312(ra) -- Store: [0x800127f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002948]:fadd.d t5, t3, s10, dyn
	-[0x8000294c]:csrrs a7, fcsr, zero
	-[0x80002950]:sw t5, 304(ra)
	-[0x80002954]:sw t6, 312(ra)
	-[0x80002958]:sw t5, 320(ra)
Current Store : [0x80002958] : sw t5, 320(ra) -- Store: [0x800127f8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800029cc]:fadd.d t5, t3, s10, dyn
	-[0x800029d0]:csrrs a7, fcsr, zero
	-[0x800029d4]:sw t5, 336(ra)
	-[0x800029d8]:sw t6, 344(ra)
Current Store : [0x800029d8] : sw t6, 344(ra) -- Store: [0x80012810]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800029cc]:fadd.d t5, t3, s10, dyn
	-[0x800029d0]:csrrs a7, fcsr, zero
	-[0x800029d4]:sw t5, 336(ra)
	-[0x800029d8]:sw t6, 344(ra)
	-[0x800029dc]:sw t5, 352(ra)
Current Store : [0x800029dc] : sw t5, 352(ra) -- Store: [0x80012818]:0xFFFFFFFD




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002a54]:fadd.d t5, t3, s10, dyn
	-[0x80002a58]:csrrs a7, fcsr, zero
	-[0x80002a5c]:sw t5, 368(ra)
	-[0x80002a60]:sw t6, 376(ra)
Current Store : [0x80002a60] : sw t6, 376(ra) -- Store: [0x80012830]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002a54]:fadd.d t5, t3, s10, dyn
	-[0x80002a58]:csrrs a7, fcsr, zero
	-[0x80002a5c]:sw t5, 368(ra)
	-[0x80002a60]:sw t6, 376(ra)
	-[0x80002a64]:sw t5, 384(ra)
Current Store : [0x80002a64] : sw t5, 384(ra) -- Store: [0x80012838]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002adc]:fadd.d t5, t3, s10, dyn
	-[0x80002ae0]:csrrs a7, fcsr, zero
	-[0x80002ae4]:sw t5, 400(ra)
	-[0x80002ae8]:sw t6, 408(ra)
Current Store : [0x80002ae8] : sw t6, 408(ra) -- Store: [0x80012850]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002adc]:fadd.d t5, t3, s10, dyn
	-[0x80002ae0]:csrrs a7, fcsr, zero
	-[0x80002ae4]:sw t5, 400(ra)
	-[0x80002ae8]:sw t6, 408(ra)
	-[0x80002aec]:sw t5, 416(ra)
Current Store : [0x80002aec] : sw t5, 416(ra) -- Store: [0x80012858]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002b60]:fadd.d t5, t3, s10, dyn
	-[0x80002b64]:csrrs a7, fcsr, zero
	-[0x80002b68]:sw t5, 432(ra)
	-[0x80002b6c]:sw t6, 440(ra)
Current Store : [0x80002b6c] : sw t6, 440(ra) -- Store: [0x80012870]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002b60]:fadd.d t5, t3, s10, dyn
	-[0x80002b64]:csrrs a7, fcsr, zero
	-[0x80002b68]:sw t5, 432(ra)
	-[0x80002b6c]:sw t6, 440(ra)
	-[0x80002b70]:sw t5, 448(ra)
Current Store : [0x80002b70] : sw t5, 448(ra) -- Store: [0x80012878]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002be4]:fadd.d t5, t3, s10, dyn
	-[0x80002be8]:csrrs a7, fcsr, zero
	-[0x80002bec]:sw t5, 464(ra)
	-[0x80002bf0]:sw t6, 472(ra)
Current Store : [0x80002bf0] : sw t6, 472(ra) -- Store: [0x80012890]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002be4]:fadd.d t5, t3, s10, dyn
	-[0x80002be8]:csrrs a7, fcsr, zero
	-[0x80002bec]:sw t5, 464(ra)
	-[0x80002bf0]:sw t6, 472(ra)
	-[0x80002bf4]:sw t5, 480(ra)
Current Store : [0x80002bf4] : sw t5, 480(ra) -- Store: [0x80012898]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002c68]:fadd.d t5, t3, s10, dyn
	-[0x80002c6c]:csrrs a7, fcsr, zero
	-[0x80002c70]:sw t5, 496(ra)
	-[0x80002c74]:sw t6, 504(ra)
Current Store : [0x80002c74] : sw t6, 504(ra) -- Store: [0x800128b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002c68]:fadd.d t5, t3, s10, dyn
	-[0x80002c6c]:csrrs a7, fcsr, zero
	-[0x80002c70]:sw t5, 496(ra)
	-[0x80002c74]:sw t6, 504(ra)
	-[0x80002c78]:sw t5, 512(ra)
Current Store : [0x80002c78] : sw t5, 512(ra) -- Store: [0x800128b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002cec]:fadd.d t5, t3, s10, dyn
	-[0x80002cf0]:csrrs a7, fcsr, zero
	-[0x80002cf4]:sw t5, 528(ra)
	-[0x80002cf8]:sw t6, 536(ra)
Current Store : [0x80002cf8] : sw t6, 536(ra) -- Store: [0x800128d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002cec]:fadd.d t5, t3, s10, dyn
	-[0x80002cf0]:csrrs a7, fcsr, zero
	-[0x80002cf4]:sw t5, 528(ra)
	-[0x80002cf8]:sw t6, 536(ra)
	-[0x80002cfc]:sw t5, 544(ra)
Current Store : [0x80002cfc] : sw t5, 544(ra) -- Store: [0x800128d8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002d74]:fadd.d t5, t3, s10, dyn
	-[0x80002d78]:csrrs a7, fcsr, zero
	-[0x80002d7c]:sw t5, 560(ra)
	-[0x80002d80]:sw t6, 568(ra)
Current Store : [0x80002d80] : sw t6, 568(ra) -- Store: [0x800128f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002d74]:fadd.d t5, t3, s10, dyn
	-[0x80002d78]:csrrs a7, fcsr, zero
	-[0x80002d7c]:sw t5, 560(ra)
	-[0x80002d80]:sw t6, 568(ra)
	-[0x80002d84]:sw t5, 576(ra)
Current Store : [0x80002d84] : sw t5, 576(ra) -- Store: [0x800128f8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002dfc]:fadd.d t5, t3, s10, dyn
	-[0x80002e00]:csrrs a7, fcsr, zero
	-[0x80002e04]:sw t5, 592(ra)
	-[0x80002e08]:sw t6, 600(ra)
Current Store : [0x80002e08] : sw t6, 600(ra) -- Store: [0x80012910]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002dfc]:fadd.d t5, t3, s10, dyn
	-[0x80002e00]:csrrs a7, fcsr, zero
	-[0x80002e04]:sw t5, 592(ra)
	-[0x80002e08]:sw t6, 600(ra)
	-[0x80002e0c]:sw t5, 608(ra)
Current Store : [0x80002e0c] : sw t5, 608(ra) -- Store: [0x80012918]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002e80]:fadd.d t5, t3, s10, dyn
	-[0x80002e84]:csrrs a7, fcsr, zero
	-[0x80002e88]:sw t5, 624(ra)
	-[0x80002e8c]:sw t6, 632(ra)
Current Store : [0x80002e8c] : sw t6, 632(ra) -- Store: [0x80012930]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002e80]:fadd.d t5, t3, s10, dyn
	-[0x80002e84]:csrrs a7, fcsr, zero
	-[0x80002e88]:sw t5, 624(ra)
	-[0x80002e8c]:sw t6, 632(ra)
	-[0x80002e90]:sw t5, 640(ra)
Current Store : [0x80002e90] : sw t5, 640(ra) -- Store: [0x80012938]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002f04]:fadd.d t5, t3, s10, dyn
	-[0x80002f08]:csrrs a7, fcsr, zero
	-[0x80002f0c]:sw t5, 656(ra)
	-[0x80002f10]:sw t6, 664(ra)
Current Store : [0x80002f10] : sw t6, 664(ra) -- Store: [0x80012950]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002f04]:fadd.d t5, t3, s10, dyn
	-[0x80002f08]:csrrs a7, fcsr, zero
	-[0x80002f0c]:sw t5, 656(ra)
	-[0x80002f10]:sw t6, 664(ra)
	-[0x80002f14]:sw t5, 672(ra)
Current Store : [0x80002f14] : sw t5, 672(ra) -- Store: [0x80012958]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002f88]:fadd.d t5, t3, s10, dyn
	-[0x80002f8c]:csrrs a7, fcsr, zero
	-[0x80002f90]:sw t5, 688(ra)
	-[0x80002f94]:sw t6, 696(ra)
Current Store : [0x80002f94] : sw t6, 696(ra) -- Store: [0x80012970]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002f88]:fadd.d t5, t3, s10, dyn
	-[0x80002f8c]:csrrs a7, fcsr, zero
	-[0x80002f90]:sw t5, 688(ra)
	-[0x80002f94]:sw t6, 696(ra)
	-[0x80002f98]:sw t5, 704(ra)
Current Store : [0x80002f98] : sw t5, 704(ra) -- Store: [0x80012978]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000300c]:fadd.d t5, t3, s10, dyn
	-[0x80003010]:csrrs a7, fcsr, zero
	-[0x80003014]:sw t5, 720(ra)
	-[0x80003018]:sw t6, 728(ra)
Current Store : [0x80003018] : sw t6, 728(ra) -- Store: [0x80012990]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000300c]:fadd.d t5, t3, s10, dyn
	-[0x80003010]:csrrs a7, fcsr, zero
	-[0x80003014]:sw t5, 720(ra)
	-[0x80003018]:sw t6, 728(ra)
	-[0x8000301c]:sw t5, 736(ra)
Current Store : [0x8000301c] : sw t5, 736(ra) -- Store: [0x80012998]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003090]:fadd.d t5, t3, s10, dyn
	-[0x80003094]:csrrs a7, fcsr, zero
	-[0x80003098]:sw t5, 752(ra)
	-[0x8000309c]:sw t6, 760(ra)
Current Store : [0x8000309c] : sw t6, 760(ra) -- Store: [0x800129b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003090]:fadd.d t5, t3, s10, dyn
	-[0x80003094]:csrrs a7, fcsr, zero
	-[0x80003098]:sw t5, 752(ra)
	-[0x8000309c]:sw t6, 760(ra)
	-[0x800030a0]:sw t5, 768(ra)
Current Store : [0x800030a0] : sw t5, 768(ra) -- Store: [0x800129b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003114]:fadd.d t5, t3, s10, dyn
	-[0x80003118]:csrrs a7, fcsr, zero
	-[0x8000311c]:sw t5, 784(ra)
	-[0x80003120]:sw t6, 792(ra)
Current Store : [0x80003120] : sw t6, 792(ra) -- Store: [0x800129d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003114]:fadd.d t5, t3, s10, dyn
	-[0x80003118]:csrrs a7, fcsr, zero
	-[0x8000311c]:sw t5, 784(ra)
	-[0x80003120]:sw t6, 792(ra)
	-[0x80003124]:sw t5, 800(ra)
Current Store : [0x80003124] : sw t5, 800(ra) -- Store: [0x800129d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003198]:fadd.d t5, t3, s10, dyn
	-[0x8000319c]:csrrs a7, fcsr, zero
	-[0x800031a0]:sw t5, 816(ra)
	-[0x800031a4]:sw t6, 824(ra)
Current Store : [0x800031a4] : sw t6, 824(ra) -- Store: [0x800129f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003198]:fadd.d t5, t3, s10, dyn
	-[0x8000319c]:csrrs a7, fcsr, zero
	-[0x800031a0]:sw t5, 816(ra)
	-[0x800031a4]:sw t6, 824(ra)
	-[0x800031a8]:sw t5, 832(ra)
Current Store : [0x800031a8] : sw t5, 832(ra) -- Store: [0x800129f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000321c]:fadd.d t5, t3, s10, dyn
	-[0x80003220]:csrrs a7, fcsr, zero
	-[0x80003224]:sw t5, 848(ra)
	-[0x80003228]:sw t6, 856(ra)
Current Store : [0x80003228] : sw t6, 856(ra) -- Store: [0x80012a10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000321c]:fadd.d t5, t3, s10, dyn
	-[0x80003220]:csrrs a7, fcsr, zero
	-[0x80003224]:sw t5, 848(ra)
	-[0x80003228]:sw t6, 856(ra)
	-[0x8000322c]:sw t5, 864(ra)
Current Store : [0x8000322c] : sw t5, 864(ra) -- Store: [0x80012a18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800032a0]:fadd.d t5, t3, s10, dyn
	-[0x800032a4]:csrrs a7, fcsr, zero
	-[0x800032a8]:sw t5, 880(ra)
	-[0x800032ac]:sw t6, 888(ra)
Current Store : [0x800032ac] : sw t6, 888(ra) -- Store: [0x80012a30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800032a0]:fadd.d t5, t3, s10, dyn
	-[0x800032a4]:csrrs a7, fcsr, zero
	-[0x800032a8]:sw t5, 880(ra)
	-[0x800032ac]:sw t6, 888(ra)
	-[0x800032b0]:sw t5, 896(ra)
Current Store : [0x800032b0] : sw t5, 896(ra) -- Store: [0x80012a38]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003324]:fadd.d t5, t3, s10, dyn
	-[0x80003328]:csrrs a7, fcsr, zero
	-[0x8000332c]:sw t5, 912(ra)
	-[0x80003330]:sw t6, 920(ra)
Current Store : [0x80003330] : sw t6, 920(ra) -- Store: [0x80012a50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003324]:fadd.d t5, t3, s10, dyn
	-[0x80003328]:csrrs a7, fcsr, zero
	-[0x8000332c]:sw t5, 912(ra)
	-[0x80003330]:sw t6, 920(ra)
	-[0x80003334]:sw t5, 928(ra)
Current Store : [0x80003334] : sw t5, 928(ra) -- Store: [0x80012a58]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800033a8]:fadd.d t5, t3, s10, dyn
	-[0x800033ac]:csrrs a7, fcsr, zero
	-[0x800033b0]:sw t5, 944(ra)
	-[0x800033b4]:sw t6, 952(ra)
Current Store : [0x800033b4] : sw t6, 952(ra) -- Store: [0x80012a70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800033a8]:fadd.d t5, t3, s10, dyn
	-[0x800033ac]:csrrs a7, fcsr, zero
	-[0x800033b0]:sw t5, 944(ra)
	-[0x800033b4]:sw t6, 952(ra)
	-[0x800033b8]:sw t5, 960(ra)
Current Store : [0x800033b8] : sw t5, 960(ra) -- Store: [0x80012a78]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000342c]:fadd.d t5, t3, s10, dyn
	-[0x80003430]:csrrs a7, fcsr, zero
	-[0x80003434]:sw t5, 976(ra)
	-[0x80003438]:sw t6, 984(ra)
Current Store : [0x80003438] : sw t6, 984(ra) -- Store: [0x80012a90]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000342c]:fadd.d t5, t3, s10, dyn
	-[0x80003430]:csrrs a7, fcsr, zero
	-[0x80003434]:sw t5, 976(ra)
	-[0x80003438]:sw t6, 984(ra)
	-[0x8000343c]:sw t5, 992(ra)
Current Store : [0x8000343c] : sw t5, 992(ra) -- Store: [0x80012a98]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800034b0]:fadd.d t5, t3, s10, dyn
	-[0x800034b4]:csrrs a7, fcsr, zero
	-[0x800034b8]:sw t5, 1008(ra)
	-[0x800034bc]:sw t6, 1016(ra)
Current Store : [0x800034bc] : sw t6, 1016(ra) -- Store: [0x80012ab0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800034b0]:fadd.d t5, t3, s10, dyn
	-[0x800034b4]:csrrs a7, fcsr, zero
	-[0x800034b8]:sw t5, 1008(ra)
	-[0x800034bc]:sw t6, 1016(ra)
	-[0x800034c0]:sw t5, 1024(ra)
Current Store : [0x800034c0] : sw t5, 1024(ra) -- Store: [0x80012ab8]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003534]:fadd.d t5, t3, s10, dyn
	-[0x80003538]:csrrs a7, fcsr, zero
	-[0x8000353c]:sw t5, 1040(ra)
	-[0x80003540]:sw t6, 1048(ra)
Current Store : [0x80003540] : sw t6, 1048(ra) -- Store: [0x80012ad0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003534]:fadd.d t5, t3, s10, dyn
	-[0x80003538]:csrrs a7, fcsr, zero
	-[0x8000353c]:sw t5, 1040(ra)
	-[0x80003540]:sw t6, 1048(ra)
	-[0x80003544]:sw t5, 1056(ra)
Current Store : [0x80003544] : sw t5, 1056(ra) -- Store: [0x80012ad8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800035b8]:fadd.d t5, t3, s10, dyn
	-[0x800035bc]:csrrs a7, fcsr, zero
	-[0x800035c0]:sw t5, 1072(ra)
	-[0x800035c4]:sw t6, 1080(ra)
Current Store : [0x800035c4] : sw t6, 1080(ra) -- Store: [0x80012af0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800035b8]:fadd.d t5, t3, s10, dyn
	-[0x800035bc]:csrrs a7, fcsr, zero
	-[0x800035c0]:sw t5, 1072(ra)
	-[0x800035c4]:sw t6, 1080(ra)
	-[0x800035c8]:sw t5, 1088(ra)
Current Store : [0x800035c8] : sw t5, 1088(ra) -- Store: [0x80012af8]:0xFFFFFFFD




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000363c]:fadd.d t5, t3, s10, dyn
	-[0x80003640]:csrrs a7, fcsr, zero
	-[0x80003644]:sw t5, 1104(ra)
	-[0x80003648]:sw t6, 1112(ra)
Current Store : [0x80003648] : sw t6, 1112(ra) -- Store: [0x80012b10]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000363c]:fadd.d t5, t3, s10, dyn
	-[0x80003640]:csrrs a7, fcsr, zero
	-[0x80003644]:sw t5, 1104(ra)
	-[0x80003648]:sw t6, 1112(ra)
	-[0x8000364c]:sw t5, 1120(ra)
Current Store : [0x8000364c] : sw t5, 1120(ra) -- Store: [0x80012b18]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800036c4]:fadd.d t5, t3, s10, dyn
	-[0x800036c8]:csrrs a7, fcsr, zero
	-[0x800036cc]:sw t5, 1136(ra)
	-[0x800036d0]:sw t6, 1144(ra)
Current Store : [0x800036d0] : sw t6, 1144(ra) -- Store: [0x80012b30]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800036c4]:fadd.d t5, t3, s10, dyn
	-[0x800036c8]:csrrs a7, fcsr, zero
	-[0x800036cc]:sw t5, 1136(ra)
	-[0x800036d0]:sw t6, 1144(ra)
	-[0x800036d4]:sw t5, 1152(ra)
Current Store : [0x800036d4] : sw t5, 1152(ra) -- Store: [0x80012b38]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000374c]:fadd.d t5, t3, s10, dyn
	-[0x80003750]:csrrs a7, fcsr, zero
	-[0x80003754]:sw t5, 1168(ra)
	-[0x80003758]:sw t6, 1176(ra)
Current Store : [0x80003758] : sw t6, 1176(ra) -- Store: [0x80012b50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000374c]:fadd.d t5, t3, s10, dyn
	-[0x80003750]:csrrs a7, fcsr, zero
	-[0x80003754]:sw t5, 1168(ra)
	-[0x80003758]:sw t6, 1176(ra)
	-[0x8000375c]:sw t5, 1184(ra)
Current Store : [0x8000375c] : sw t5, 1184(ra) -- Store: [0x80012b58]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800037d0]:fadd.d t5, t3, s10, dyn
	-[0x800037d4]:csrrs a7, fcsr, zero
	-[0x800037d8]:sw t5, 1200(ra)
	-[0x800037dc]:sw t6, 1208(ra)
Current Store : [0x800037dc] : sw t6, 1208(ra) -- Store: [0x80012b70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800037d0]:fadd.d t5, t3, s10, dyn
	-[0x800037d4]:csrrs a7, fcsr, zero
	-[0x800037d8]:sw t5, 1200(ra)
	-[0x800037dc]:sw t6, 1208(ra)
	-[0x800037e0]:sw t5, 1216(ra)
Current Store : [0x800037e0] : sw t5, 1216(ra) -- Store: [0x80012b78]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003854]:fadd.d t5, t3, s10, dyn
	-[0x80003858]:csrrs a7, fcsr, zero
	-[0x8000385c]:sw t5, 1232(ra)
	-[0x80003860]:sw t6, 1240(ra)
Current Store : [0x80003860] : sw t6, 1240(ra) -- Store: [0x80012b90]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003854]:fadd.d t5, t3, s10, dyn
	-[0x80003858]:csrrs a7, fcsr, zero
	-[0x8000385c]:sw t5, 1232(ra)
	-[0x80003860]:sw t6, 1240(ra)
	-[0x80003864]:sw t5, 1248(ra)
Current Store : [0x80003864] : sw t5, 1248(ra) -- Store: [0x80012b98]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800038d8]:fadd.d t5, t3, s10, dyn
	-[0x800038dc]:csrrs a7, fcsr, zero
	-[0x800038e0]:sw t5, 1264(ra)
	-[0x800038e4]:sw t6, 1272(ra)
Current Store : [0x800038e4] : sw t6, 1272(ra) -- Store: [0x80012bb0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800038d8]:fadd.d t5, t3, s10, dyn
	-[0x800038dc]:csrrs a7, fcsr, zero
	-[0x800038e0]:sw t5, 1264(ra)
	-[0x800038e4]:sw t6, 1272(ra)
	-[0x800038e8]:sw t5, 1280(ra)
Current Store : [0x800038e8] : sw t5, 1280(ra) -- Store: [0x80012bb8]:0x00000003




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000395c]:fadd.d t5, t3, s10, dyn
	-[0x80003960]:csrrs a7, fcsr, zero
	-[0x80003964]:sw t5, 1296(ra)
	-[0x80003968]:sw t6, 1304(ra)
Current Store : [0x80003968] : sw t6, 1304(ra) -- Store: [0x80012bd0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000395c]:fadd.d t5, t3, s10, dyn
	-[0x80003960]:csrrs a7, fcsr, zero
	-[0x80003964]:sw t5, 1296(ra)
	-[0x80003968]:sw t6, 1304(ra)
	-[0x8000396c]:sw t5, 1312(ra)
Current Store : [0x8000396c] : sw t5, 1312(ra) -- Store: [0x80012bd8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800039e4]:fadd.d t5, t3, s10, dyn
	-[0x800039e8]:csrrs a7, fcsr, zero
	-[0x800039ec]:sw t5, 1328(ra)
	-[0x800039f0]:sw t6, 1336(ra)
Current Store : [0x800039f0] : sw t6, 1336(ra) -- Store: [0x80012bf0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800039e4]:fadd.d t5, t3, s10, dyn
	-[0x800039e8]:csrrs a7, fcsr, zero
	-[0x800039ec]:sw t5, 1328(ra)
	-[0x800039f0]:sw t6, 1336(ra)
	-[0x800039f4]:sw t5, 1344(ra)
Current Store : [0x800039f4] : sw t5, 1344(ra) -- Store: [0x80012bf8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003a6c]:fadd.d t5, t3, s10, dyn
	-[0x80003a70]:csrrs a7, fcsr, zero
	-[0x80003a74]:sw t5, 1360(ra)
	-[0x80003a78]:sw t6, 1368(ra)
Current Store : [0x80003a78] : sw t6, 1368(ra) -- Store: [0x80012c10]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003a6c]:fadd.d t5, t3, s10, dyn
	-[0x80003a70]:csrrs a7, fcsr, zero
	-[0x80003a74]:sw t5, 1360(ra)
	-[0x80003a78]:sw t6, 1368(ra)
	-[0x80003a7c]:sw t5, 1376(ra)
Current Store : [0x80003a7c] : sw t5, 1376(ra) -- Store: [0x80012c18]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003af0]:fadd.d t5, t3, s10, dyn
	-[0x80003af4]:csrrs a7, fcsr, zero
	-[0x80003af8]:sw t5, 1392(ra)
	-[0x80003afc]:sw t6, 1400(ra)
Current Store : [0x80003afc] : sw t6, 1400(ra) -- Store: [0x80012c30]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003af0]:fadd.d t5, t3, s10, dyn
	-[0x80003af4]:csrrs a7, fcsr, zero
	-[0x80003af8]:sw t5, 1392(ra)
	-[0x80003afc]:sw t6, 1400(ra)
	-[0x80003b00]:sw t5, 1408(ra)
Current Store : [0x80003b00] : sw t5, 1408(ra) -- Store: [0x80012c38]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003b74]:fadd.d t5, t3, s10, dyn
	-[0x80003b78]:csrrs a7, fcsr, zero
	-[0x80003b7c]:sw t5, 1424(ra)
	-[0x80003b80]:sw t6, 1432(ra)
Current Store : [0x80003b80] : sw t6, 1432(ra) -- Store: [0x80012c50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003b74]:fadd.d t5, t3, s10, dyn
	-[0x80003b78]:csrrs a7, fcsr, zero
	-[0x80003b7c]:sw t5, 1424(ra)
	-[0x80003b80]:sw t6, 1432(ra)
	-[0x80003b84]:sw t5, 1440(ra)
Current Store : [0x80003b84] : sw t5, 1440(ra) -- Store: [0x80012c58]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003bf8]:fadd.d t5, t3, s10, dyn
	-[0x80003bfc]:csrrs a7, fcsr, zero
	-[0x80003c00]:sw t5, 1456(ra)
	-[0x80003c04]:sw t6, 1464(ra)
Current Store : [0x80003c04] : sw t6, 1464(ra) -- Store: [0x80012c70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003bf8]:fadd.d t5, t3, s10, dyn
	-[0x80003bfc]:csrrs a7, fcsr, zero
	-[0x80003c00]:sw t5, 1456(ra)
	-[0x80003c04]:sw t6, 1464(ra)
	-[0x80003c08]:sw t5, 1472(ra)
Current Store : [0x80003c08] : sw t5, 1472(ra) -- Store: [0x80012c78]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003c7c]:fadd.d t5, t3, s10, dyn
	-[0x80003c80]:csrrs a7, fcsr, zero
	-[0x80003c84]:sw t5, 1488(ra)
	-[0x80003c88]:sw t6, 1496(ra)
Current Store : [0x80003c88] : sw t6, 1496(ra) -- Store: [0x80012c90]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003c7c]:fadd.d t5, t3, s10, dyn
	-[0x80003c80]:csrrs a7, fcsr, zero
	-[0x80003c84]:sw t5, 1488(ra)
	-[0x80003c88]:sw t6, 1496(ra)
	-[0x80003c8c]:sw t5, 1504(ra)
Current Store : [0x80003c8c] : sw t5, 1504(ra) -- Store: [0x80012c98]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003d00]:fadd.d t5, t3, s10, dyn
	-[0x80003d04]:csrrs a7, fcsr, zero
	-[0x80003d08]:sw t5, 1520(ra)
	-[0x80003d0c]:sw t6, 1528(ra)
Current Store : [0x80003d0c] : sw t6, 1528(ra) -- Store: [0x80012cb0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003d00]:fadd.d t5, t3, s10, dyn
	-[0x80003d04]:csrrs a7, fcsr, zero
	-[0x80003d08]:sw t5, 1520(ra)
	-[0x80003d0c]:sw t6, 1528(ra)
	-[0x80003d10]:sw t5, 1536(ra)
Current Store : [0x80003d10] : sw t5, 1536(ra) -- Store: [0x80012cb8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003d84]:fadd.d t5, t3, s10, dyn
	-[0x80003d88]:csrrs a7, fcsr, zero
	-[0x80003d8c]:sw t5, 1552(ra)
	-[0x80003d90]:sw t6, 1560(ra)
Current Store : [0x80003d90] : sw t6, 1560(ra) -- Store: [0x80012cd0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003d84]:fadd.d t5, t3, s10, dyn
	-[0x80003d88]:csrrs a7, fcsr, zero
	-[0x80003d8c]:sw t5, 1552(ra)
	-[0x80003d90]:sw t6, 1560(ra)
	-[0x80003d94]:sw t5, 1568(ra)
Current Store : [0x80003d94] : sw t5, 1568(ra) -- Store: [0x80012cd8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003e08]:fadd.d t5, t3, s10, dyn
	-[0x80003e0c]:csrrs a7, fcsr, zero
	-[0x80003e10]:sw t5, 1584(ra)
	-[0x80003e14]:sw t6, 1592(ra)
Current Store : [0x80003e14] : sw t6, 1592(ra) -- Store: [0x80012cf0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003e08]:fadd.d t5, t3, s10, dyn
	-[0x80003e0c]:csrrs a7, fcsr, zero
	-[0x80003e10]:sw t5, 1584(ra)
	-[0x80003e14]:sw t6, 1592(ra)
	-[0x80003e18]:sw t5, 1600(ra)
Current Store : [0x80003e18] : sw t5, 1600(ra) -- Store: [0x80012cf8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003e8c]:fadd.d t5, t3, s10, dyn
	-[0x80003e90]:csrrs a7, fcsr, zero
	-[0x80003e94]:sw t5, 1616(ra)
	-[0x80003e98]:sw t6, 1624(ra)
Current Store : [0x80003e98] : sw t6, 1624(ra) -- Store: [0x80012d10]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003e8c]:fadd.d t5, t3, s10, dyn
	-[0x80003e90]:csrrs a7, fcsr, zero
	-[0x80003e94]:sw t5, 1616(ra)
	-[0x80003e98]:sw t6, 1624(ra)
	-[0x80003e9c]:sw t5, 1632(ra)
Current Store : [0x80003e9c] : sw t5, 1632(ra) -- Store: [0x80012d18]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003f10]:fadd.d t5, t3, s10, dyn
	-[0x80003f14]:csrrs a7, fcsr, zero
	-[0x80003f18]:sw t5, 1648(ra)
	-[0x80003f1c]:sw t6, 1656(ra)
Current Store : [0x80003f1c] : sw t6, 1656(ra) -- Store: [0x80012d30]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003f10]:fadd.d t5, t3, s10, dyn
	-[0x80003f14]:csrrs a7, fcsr, zero
	-[0x80003f18]:sw t5, 1648(ra)
	-[0x80003f1c]:sw t6, 1656(ra)
	-[0x80003f20]:sw t5, 1664(ra)
Current Store : [0x80003f20] : sw t5, 1664(ra) -- Store: [0x80012d38]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003f94]:fadd.d t5, t3, s10, dyn
	-[0x80003f98]:csrrs a7, fcsr, zero
	-[0x80003f9c]:sw t5, 1680(ra)
	-[0x80003fa0]:sw t6, 1688(ra)
Current Store : [0x80003fa0] : sw t6, 1688(ra) -- Store: [0x80012d50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80003f94]:fadd.d t5, t3, s10, dyn
	-[0x80003f98]:csrrs a7, fcsr, zero
	-[0x80003f9c]:sw t5, 1680(ra)
	-[0x80003fa0]:sw t6, 1688(ra)
	-[0x80003fa4]:sw t5, 1696(ra)
Current Store : [0x80003fa4] : sw t5, 1696(ra) -- Store: [0x80012d58]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004014]:fadd.d t5, t3, s10, dyn
	-[0x80004018]:csrrs a7, fcsr, zero
	-[0x8000401c]:sw t5, 1712(ra)
	-[0x80004020]:sw t6, 1720(ra)
Current Store : [0x80004020] : sw t6, 1720(ra) -- Store: [0x80012d70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004014]:fadd.d t5, t3, s10, dyn
	-[0x80004018]:csrrs a7, fcsr, zero
	-[0x8000401c]:sw t5, 1712(ra)
	-[0x80004020]:sw t6, 1720(ra)
	-[0x80004024]:sw t5, 1728(ra)
Current Store : [0x80004024] : sw t5, 1728(ra) -- Store: [0x80012d78]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004094]:fadd.d t5, t3, s10, dyn
	-[0x80004098]:csrrs a7, fcsr, zero
	-[0x8000409c]:sw t5, 1744(ra)
	-[0x800040a0]:sw t6, 1752(ra)
Current Store : [0x800040a0] : sw t6, 1752(ra) -- Store: [0x80012d90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004094]:fadd.d t5, t3, s10, dyn
	-[0x80004098]:csrrs a7, fcsr, zero
	-[0x8000409c]:sw t5, 1744(ra)
	-[0x800040a0]:sw t6, 1752(ra)
	-[0x800040a4]:sw t5, 1760(ra)
Current Store : [0x800040a4] : sw t5, 1760(ra) -- Store: [0x80012d98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004114]:fadd.d t5, t3, s10, dyn
	-[0x80004118]:csrrs a7, fcsr, zero
	-[0x8000411c]:sw t5, 1776(ra)
	-[0x80004120]:sw t6, 1784(ra)
Current Store : [0x80004120] : sw t6, 1784(ra) -- Store: [0x80012db0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004114]:fadd.d t5, t3, s10, dyn
	-[0x80004118]:csrrs a7, fcsr, zero
	-[0x8000411c]:sw t5, 1776(ra)
	-[0x80004120]:sw t6, 1784(ra)
	-[0x80004124]:sw t5, 1792(ra)
Current Store : [0x80004124] : sw t5, 1792(ra) -- Store: [0x80012db8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004194]:fadd.d t5, t3, s10, dyn
	-[0x80004198]:csrrs a7, fcsr, zero
	-[0x8000419c]:sw t5, 1808(ra)
	-[0x800041a0]:sw t6, 1816(ra)
Current Store : [0x800041a0] : sw t6, 1816(ra) -- Store: [0x80012dd0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004194]:fadd.d t5, t3, s10, dyn
	-[0x80004198]:csrrs a7, fcsr, zero
	-[0x8000419c]:sw t5, 1808(ra)
	-[0x800041a0]:sw t6, 1816(ra)
	-[0x800041a4]:sw t5, 1824(ra)
Current Store : [0x800041a4] : sw t5, 1824(ra) -- Store: [0x80012dd8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004214]:fadd.d t5, t3, s10, dyn
	-[0x80004218]:csrrs a7, fcsr, zero
	-[0x8000421c]:sw t5, 1840(ra)
	-[0x80004220]:sw t6, 1848(ra)
Current Store : [0x80004220] : sw t6, 1848(ra) -- Store: [0x80012df0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004214]:fadd.d t5, t3, s10, dyn
	-[0x80004218]:csrrs a7, fcsr, zero
	-[0x8000421c]:sw t5, 1840(ra)
	-[0x80004220]:sw t6, 1848(ra)
	-[0x80004224]:sw t5, 1856(ra)
Current Store : [0x80004224] : sw t5, 1856(ra) -- Store: [0x80012df8]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004294]:fadd.d t5, t3, s10, dyn
	-[0x80004298]:csrrs a7, fcsr, zero
	-[0x8000429c]:sw t5, 1872(ra)
	-[0x800042a0]:sw t6, 1880(ra)
Current Store : [0x800042a0] : sw t6, 1880(ra) -- Store: [0x80012e10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004294]:fadd.d t5, t3, s10, dyn
	-[0x80004298]:csrrs a7, fcsr, zero
	-[0x8000429c]:sw t5, 1872(ra)
	-[0x800042a0]:sw t6, 1880(ra)
	-[0x800042a4]:sw t5, 1888(ra)
Current Store : [0x800042a4] : sw t5, 1888(ra) -- Store: [0x80012e18]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004318]:fadd.d t5, t3, s10, dyn
	-[0x8000431c]:csrrs a7, fcsr, zero
	-[0x80004320]:sw t5, 1904(ra)
	-[0x80004324]:sw t6, 1912(ra)
Current Store : [0x80004324] : sw t6, 1912(ra) -- Store: [0x80012e30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004318]:fadd.d t5, t3, s10, dyn
	-[0x8000431c]:csrrs a7, fcsr, zero
	-[0x80004320]:sw t5, 1904(ra)
	-[0x80004324]:sw t6, 1912(ra)
	-[0x80004328]:sw t5, 1920(ra)
Current Store : [0x80004328] : sw t5, 1920(ra) -- Store: [0x80012e38]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000439c]:fadd.d t5, t3, s10, dyn
	-[0x800043a0]:csrrs a7, fcsr, zero
	-[0x800043a4]:sw t5, 1936(ra)
	-[0x800043a8]:sw t6, 1944(ra)
Current Store : [0x800043a8] : sw t6, 1944(ra) -- Store: [0x80012e50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000439c]:fadd.d t5, t3, s10, dyn
	-[0x800043a0]:csrrs a7, fcsr, zero
	-[0x800043a4]:sw t5, 1936(ra)
	-[0x800043a8]:sw t6, 1944(ra)
	-[0x800043ac]:sw t5, 1952(ra)
Current Store : [0x800043ac] : sw t5, 1952(ra) -- Store: [0x80012e58]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000441c]:fadd.d t5, t3, s10, dyn
	-[0x80004420]:csrrs a7, fcsr, zero
	-[0x80004424]:sw t5, 1968(ra)
	-[0x80004428]:sw t6, 1976(ra)
Current Store : [0x80004428] : sw t6, 1976(ra) -- Store: [0x80012e70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000441c]:fadd.d t5, t3, s10, dyn
	-[0x80004420]:csrrs a7, fcsr, zero
	-[0x80004424]:sw t5, 1968(ra)
	-[0x80004428]:sw t6, 1976(ra)
	-[0x8000442c]:sw t5, 1984(ra)
Current Store : [0x8000442c] : sw t5, 1984(ra) -- Store: [0x80012e78]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000449c]:fadd.d t5, t3, s10, dyn
	-[0x800044a0]:csrrs a7, fcsr, zero
	-[0x800044a4]:sw t5, 2000(ra)
	-[0x800044a8]:sw t6, 2008(ra)
Current Store : [0x800044a8] : sw t6, 2008(ra) -- Store: [0x80012e90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000449c]:fadd.d t5, t3, s10, dyn
	-[0x800044a0]:csrrs a7, fcsr, zero
	-[0x800044a4]:sw t5, 2000(ra)
	-[0x800044a8]:sw t6, 2008(ra)
	-[0x800044ac]:sw t5, 2016(ra)
Current Store : [0x800044ac] : sw t5, 2016(ra) -- Store: [0x80012e98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000451c]:fadd.d t5, t3, s10, dyn
	-[0x80004520]:csrrs a7, fcsr, zero
	-[0x80004524]:sw t5, 2032(ra)
	-[0x80004528]:sw t6, 2040(ra)
Current Store : [0x80004528] : sw t6, 2040(ra) -- Store: [0x80012eb0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000451c]:fadd.d t5, t3, s10, dyn
	-[0x80004520]:csrrs a7, fcsr, zero
	-[0x80004524]:sw t5, 2032(ra)
	-[0x80004528]:sw t6, 2040(ra)
	-[0x8000452c]:addi ra, ra, 2040
	-[0x80004530]:sw t5, 8(ra)
Current Store : [0x80004530] : sw t5, 8(ra) -- Store: [0x80012eb8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800045a0]:fadd.d t5, t3, s10, dyn
	-[0x800045a4]:csrrs a7, fcsr, zero
	-[0x800045a8]:sw t5, 24(ra)
	-[0x800045ac]:sw t6, 32(ra)
Current Store : [0x800045ac] : sw t6, 32(ra) -- Store: [0x80012ed0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800045a0]:fadd.d t5, t3, s10, dyn
	-[0x800045a4]:csrrs a7, fcsr, zero
	-[0x800045a8]:sw t5, 24(ra)
	-[0x800045ac]:sw t6, 32(ra)
	-[0x800045b0]:sw t5, 40(ra)
Current Store : [0x800045b0] : sw t5, 40(ra) -- Store: [0x80012ed8]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004624]:fadd.d t5, t3, s10, dyn
	-[0x80004628]:csrrs a7, fcsr, zero
	-[0x8000462c]:sw t5, 56(ra)
	-[0x80004630]:sw t6, 64(ra)
Current Store : [0x80004630] : sw t6, 64(ra) -- Store: [0x80012ef0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004624]:fadd.d t5, t3, s10, dyn
	-[0x80004628]:csrrs a7, fcsr, zero
	-[0x8000462c]:sw t5, 56(ra)
	-[0x80004630]:sw t6, 64(ra)
	-[0x80004634]:sw t5, 72(ra)
Current Store : [0x80004634] : sw t5, 72(ra) -- Store: [0x80012ef8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800046a8]:fadd.d t5, t3, s10, dyn
	-[0x800046ac]:csrrs a7, fcsr, zero
	-[0x800046b0]:sw t5, 88(ra)
	-[0x800046b4]:sw t6, 96(ra)
Current Store : [0x800046b4] : sw t6, 96(ra) -- Store: [0x80012f10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800046a8]:fadd.d t5, t3, s10, dyn
	-[0x800046ac]:csrrs a7, fcsr, zero
	-[0x800046b0]:sw t5, 88(ra)
	-[0x800046b4]:sw t6, 96(ra)
	-[0x800046b8]:sw t5, 104(ra)
Current Store : [0x800046b8] : sw t5, 104(ra) -- Store: [0x80012f18]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004728]:fadd.d t5, t3, s10, dyn
	-[0x8000472c]:csrrs a7, fcsr, zero
	-[0x80004730]:sw t5, 120(ra)
	-[0x80004734]:sw t6, 128(ra)
Current Store : [0x80004734] : sw t6, 128(ra) -- Store: [0x80012f30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004728]:fadd.d t5, t3, s10, dyn
	-[0x8000472c]:csrrs a7, fcsr, zero
	-[0x80004730]:sw t5, 120(ra)
	-[0x80004734]:sw t6, 128(ra)
	-[0x80004738]:sw t5, 136(ra)
Current Store : [0x80004738] : sw t5, 136(ra) -- Store: [0x80012f38]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800047a8]:fadd.d t5, t3, s10, dyn
	-[0x800047ac]:csrrs a7, fcsr, zero
	-[0x800047b0]:sw t5, 152(ra)
	-[0x800047b4]:sw t6, 160(ra)
Current Store : [0x800047b4] : sw t6, 160(ra) -- Store: [0x80012f50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800047a8]:fadd.d t5, t3, s10, dyn
	-[0x800047ac]:csrrs a7, fcsr, zero
	-[0x800047b0]:sw t5, 152(ra)
	-[0x800047b4]:sw t6, 160(ra)
	-[0x800047b8]:sw t5, 168(ra)
Current Store : [0x800047b8] : sw t5, 168(ra) -- Store: [0x80012f58]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004828]:fadd.d t5, t3, s10, dyn
	-[0x8000482c]:csrrs a7, fcsr, zero
	-[0x80004830]:sw t5, 184(ra)
	-[0x80004834]:sw t6, 192(ra)
Current Store : [0x80004834] : sw t6, 192(ra) -- Store: [0x80012f70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004828]:fadd.d t5, t3, s10, dyn
	-[0x8000482c]:csrrs a7, fcsr, zero
	-[0x80004830]:sw t5, 184(ra)
	-[0x80004834]:sw t6, 192(ra)
	-[0x80004838]:sw t5, 200(ra)
Current Store : [0x80004838] : sw t5, 200(ra) -- Store: [0x80012f78]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800048a8]:fadd.d t5, t3, s10, dyn
	-[0x800048ac]:csrrs a7, fcsr, zero
	-[0x800048b0]:sw t5, 216(ra)
	-[0x800048b4]:sw t6, 224(ra)
Current Store : [0x800048b4] : sw t6, 224(ra) -- Store: [0x80012f90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800048a8]:fadd.d t5, t3, s10, dyn
	-[0x800048ac]:csrrs a7, fcsr, zero
	-[0x800048b0]:sw t5, 216(ra)
	-[0x800048b4]:sw t6, 224(ra)
	-[0x800048b8]:sw t5, 232(ra)
Current Store : [0x800048b8] : sw t5, 232(ra) -- Store: [0x80012f98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004928]:fadd.d t5, t3, s10, dyn
	-[0x8000492c]:csrrs a7, fcsr, zero
	-[0x80004930]:sw t5, 248(ra)
	-[0x80004934]:sw t6, 256(ra)
Current Store : [0x80004934] : sw t6, 256(ra) -- Store: [0x80012fb0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004928]:fadd.d t5, t3, s10, dyn
	-[0x8000492c]:csrrs a7, fcsr, zero
	-[0x80004930]:sw t5, 248(ra)
	-[0x80004934]:sw t6, 256(ra)
	-[0x80004938]:sw t5, 264(ra)
Current Store : [0x80004938] : sw t5, 264(ra) -- Store: [0x80012fb8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800049a8]:fadd.d t5, t3, s10, dyn
	-[0x800049ac]:csrrs a7, fcsr, zero
	-[0x800049b0]:sw t5, 280(ra)
	-[0x800049b4]:sw t6, 288(ra)
Current Store : [0x800049b4] : sw t6, 288(ra) -- Store: [0x80012fd0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800049a8]:fadd.d t5, t3, s10, dyn
	-[0x800049ac]:csrrs a7, fcsr, zero
	-[0x800049b0]:sw t5, 280(ra)
	-[0x800049b4]:sw t6, 288(ra)
	-[0x800049b8]:sw t5, 296(ra)
Current Store : [0x800049b8] : sw t5, 296(ra) -- Store: [0x80012fd8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004a28]:fadd.d t5, t3, s10, dyn
	-[0x80004a2c]:csrrs a7, fcsr, zero
	-[0x80004a30]:sw t5, 312(ra)
	-[0x80004a34]:sw t6, 320(ra)
Current Store : [0x80004a34] : sw t6, 320(ra) -- Store: [0x80012ff0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004a28]:fadd.d t5, t3, s10, dyn
	-[0x80004a2c]:csrrs a7, fcsr, zero
	-[0x80004a30]:sw t5, 312(ra)
	-[0x80004a34]:sw t6, 320(ra)
	-[0x80004a38]:sw t5, 328(ra)
Current Store : [0x80004a38] : sw t5, 328(ra) -- Store: [0x80012ff8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004aa8]:fadd.d t5, t3, s10, dyn
	-[0x80004aac]:csrrs a7, fcsr, zero
	-[0x80004ab0]:sw t5, 344(ra)
	-[0x80004ab4]:sw t6, 352(ra)
Current Store : [0x80004ab4] : sw t6, 352(ra) -- Store: [0x80013010]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004aa8]:fadd.d t5, t3, s10, dyn
	-[0x80004aac]:csrrs a7, fcsr, zero
	-[0x80004ab0]:sw t5, 344(ra)
	-[0x80004ab4]:sw t6, 352(ra)
	-[0x80004ab8]:sw t5, 360(ra)
Current Store : [0x80004ab8] : sw t5, 360(ra) -- Store: [0x80013018]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004b28]:fadd.d t5, t3, s10, dyn
	-[0x80004b2c]:csrrs a7, fcsr, zero
	-[0x80004b30]:sw t5, 376(ra)
	-[0x80004b34]:sw t6, 384(ra)
Current Store : [0x80004b34] : sw t6, 384(ra) -- Store: [0x80013030]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004b28]:fadd.d t5, t3, s10, dyn
	-[0x80004b2c]:csrrs a7, fcsr, zero
	-[0x80004b30]:sw t5, 376(ra)
	-[0x80004b34]:sw t6, 384(ra)
	-[0x80004b38]:sw t5, 392(ra)
Current Store : [0x80004b38] : sw t5, 392(ra) -- Store: [0x80013038]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004ba8]:fadd.d t5, t3, s10, dyn
	-[0x80004bac]:csrrs a7, fcsr, zero
	-[0x80004bb0]:sw t5, 408(ra)
	-[0x80004bb4]:sw t6, 416(ra)
Current Store : [0x80004bb4] : sw t6, 416(ra) -- Store: [0x80013050]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004ba8]:fadd.d t5, t3, s10, dyn
	-[0x80004bac]:csrrs a7, fcsr, zero
	-[0x80004bb0]:sw t5, 408(ra)
	-[0x80004bb4]:sw t6, 416(ra)
	-[0x80004bb8]:sw t5, 424(ra)
Current Store : [0x80004bb8] : sw t5, 424(ra) -- Store: [0x80013058]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004c28]:fadd.d t5, t3, s10, dyn
	-[0x80004c2c]:csrrs a7, fcsr, zero
	-[0x80004c30]:sw t5, 440(ra)
	-[0x80004c34]:sw t6, 448(ra)
Current Store : [0x80004c34] : sw t6, 448(ra) -- Store: [0x80013070]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004c28]:fadd.d t5, t3, s10, dyn
	-[0x80004c2c]:csrrs a7, fcsr, zero
	-[0x80004c30]:sw t5, 440(ra)
	-[0x80004c34]:sw t6, 448(ra)
	-[0x80004c38]:sw t5, 456(ra)
Current Store : [0x80004c38] : sw t5, 456(ra) -- Store: [0x80013078]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004ca8]:fadd.d t5, t3, s10, dyn
	-[0x80004cac]:csrrs a7, fcsr, zero
	-[0x80004cb0]:sw t5, 472(ra)
	-[0x80004cb4]:sw t6, 480(ra)
Current Store : [0x80004cb4] : sw t6, 480(ra) -- Store: [0x80013090]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004ca8]:fadd.d t5, t3, s10, dyn
	-[0x80004cac]:csrrs a7, fcsr, zero
	-[0x80004cb0]:sw t5, 472(ra)
	-[0x80004cb4]:sw t6, 480(ra)
	-[0x80004cb8]:sw t5, 488(ra)
Current Store : [0x80004cb8] : sw t5, 488(ra) -- Store: [0x80013098]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004d28]:fadd.d t5, t3, s10, dyn
	-[0x80004d2c]:csrrs a7, fcsr, zero
	-[0x80004d30]:sw t5, 504(ra)
	-[0x80004d34]:sw t6, 512(ra)
Current Store : [0x80004d34] : sw t6, 512(ra) -- Store: [0x800130b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004d28]:fadd.d t5, t3, s10, dyn
	-[0x80004d2c]:csrrs a7, fcsr, zero
	-[0x80004d30]:sw t5, 504(ra)
	-[0x80004d34]:sw t6, 512(ra)
	-[0x80004d38]:sw t5, 520(ra)
Current Store : [0x80004d38] : sw t5, 520(ra) -- Store: [0x800130b8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004da8]:fadd.d t5, t3, s10, dyn
	-[0x80004dac]:csrrs a7, fcsr, zero
	-[0x80004db0]:sw t5, 536(ra)
	-[0x80004db4]:sw t6, 544(ra)
Current Store : [0x80004db4] : sw t6, 544(ra) -- Store: [0x800130d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004da8]:fadd.d t5, t3, s10, dyn
	-[0x80004dac]:csrrs a7, fcsr, zero
	-[0x80004db0]:sw t5, 536(ra)
	-[0x80004db4]:sw t6, 544(ra)
	-[0x80004db8]:sw t5, 552(ra)
Current Store : [0x80004db8] : sw t5, 552(ra) -- Store: [0x800130d8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004e28]:fadd.d t5, t3, s10, dyn
	-[0x80004e2c]:csrrs a7, fcsr, zero
	-[0x80004e30]:sw t5, 568(ra)
	-[0x80004e34]:sw t6, 576(ra)
Current Store : [0x80004e34] : sw t6, 576(ra) -- Store: [0x800130f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004e28]:fadd.d t5, t3, s10, dyn
	-[0x80004e2c]:csrrs a7, fcsr, zero
	-[0x80004e30]:sw t5, 568(ra)
	-[0x80004e34]:sw t6, 576(ra)
	-[0x80004e38]:sw t5, 584(ra)
Current Store : [0x80004e38] : sw t5, 584(ra) -- Store: [0x800130f8]:0xFFFFFFFE




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004ea8]:fadd.d t5, t3, s10, dyn
	-[0x80004eac]:csrrs a7, fcsr, zero
	-[0x80004eb0]:sw t5, 600(ra)
	-[0x80004eb4]:sw t6, 608(ra)
Current Store : [0x80004eb4] : sw t6, 608(ra) -- Store: [0x80013110]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004ea8]:fadd.d t5, t3, s10, dyn
	-[0x80004eac]:csrrs a7, fcsr, zero
	-[0x80004eb0]:sw t5, 600(ra)
	-[0x80004eb4]:sw t6, 608(ra)
	-[0x80004eb8]:sw t5, 616(ra)
Current Store : [0x80004eb8] : sw t5, 616(ra) -- Store: [0x80013118]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004f2c]:fadd.d t5, t3, s10, dyn
	-[0x80004f30]:csrrs a7, fcsr, zero
	-[0x80004f34]:sw t5, 632(ra)
	-[0x80004f38]:sw t6, 640(ra)
Current Store : [0x80004f38] : sw t6, 640(ra) -- Store: [0x80013130]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004f2c]:fadd.d t5, t3, s10, dyn
	-[0x80004f30]:csrrs a7, fcsr, zero
	-[0x80004f34]:sw t5, 632(ra)
	-[0x80004f38]:sw t6, 640(ra)
	-[0x80004f3c]:sw t5, 648(ra)
Current Store : [0x80004f3c] : sw t5, 648(ra) -- Store: [0x80013138]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004fb0]:fadd.d t5, t3, s10, dyn
	-[0x80004fb4]:csrrs a7, fcsr, zero
	-[0x80004fb8]:sw t5, 664(ra)
	-[0x80004fbc]:sw t6, 672(ra)
Current Store : [0x80004fbc] : sw t6, 672(ra) -- Store: [0x80013150]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80004fb0]:fadd.d t5, t3, s10, dyn
	-[0x80004fb4]:csrrs a7, fcsr, zero
	-[0x80004fb8]:sw t5, 664(ra)
	-[0x80004fbc]:sw t6, 672(ra)
	-[0x80004fc0]:sw t5, 680(ra)
Current Store : [0x80004fc0] : sw t5, 680(ra) -- Store: [0x80013158]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005030]:fadd.d t5, t3, s10, dyn
	-[0x80005034]:csrrs a7, fcsr, zero
	-[0x80005038]:sw t5, 696(ra)
	-[0x8000503c]:sw t6, 704(ra)
Current Store : [0x8000503c] : sw t6, 704(ra) -- Store: [0x80013170]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005030]:fadd.d t5, t3, s10, dyn
	-[0x80005034]:csrrs a7, fcsr, zero
	-[0x80005038]:sw t5, 696(ra)
	-[0x8000503c]:sw t6, 704(ra)
	-[0x80005040]:sw t5, 712(ra)
Current Store : [0x80005040] : sw t5, 712(ra) -- Store: [0x80013178]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800050b0]:fadd.d t5, t3, s10, dyn
	-[0x800050b4]:csrrs a7, fcsr, zero
	-[0x800050b8]:sw t5, 728(ra)
	-[0x800050bc]:sw t6, 736(ra)
Current Store : [0x800050bc] : sw t6, 736(ra) -- Store: [0x80013190]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800050b0]:fadd.d t5, t3, s10, dyn
	-[0x800050b4]:csrrs a7, fcsr, zero
	-[0x800050b8]:sw t5, 728(ra)
	-[0x800050bc]:sw t6, 736(ra)
	-[0x800050c0]:sw t5, 744(ra)
Current Store : [0x800050c0] : sw t5, 744(ra) -- Store: [0x80013198]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005130]:fadd.d t5, t3, s10, dyn
	-[0x80005134]:csrrs a7, fcsr, zero
	-[0x80005138]:sw t5, 760(ra)
	-[0x8000513c]:sw t6, 768(ra)
Current Store : [0x8000513c] : sw t6, 768(ra) -- Store: [0x800131b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005130]:fadd.d t5, t3, s10, dyn
	-[0x80005134]:csrrs a7, fcsr, zero
	-[0x80005138]:sw t5, 760(ra)
	-[0x8000513c]:sw t6, 768(ra)
	-[0x80005140]:sw t5, 776(ra)
Current Store : [0x80005140] : sw t5, 776(ra) -- Store: [0x800131b8]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800051b0]:fadd.d t5, t3, s10, dyn
	-[0x800051b4]:csrrs a7, fcsr, zero
	-[0x800051b8]:sw t5, 792(ra)
	-[0x800051bc]:sw t6, 800(ra)
Current Store : [0x800051bc] : sw t6, 800(ra) -- Store: [0x800131d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800051b0]:fadd.d t5, t3, s10, dyn
	-[0x800051b4]:csrrs a7, fcsr, zero
	-[0x800051b8]:sw t5, 792(ra)
	-[0x800051bc]:sw t6, 800(ra)
	-[0x800051c0]:sw t5, 808(ra)
Current Store : [0x800051c0] : sw t5, 808(ra) -- Store: [0x800131d8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005234]:fadd.d t5, t3, s10, dyn
	-[0x80005238]:csrrs a7, fcsr, zero
	-[0x8000523c]:sw t5, 824(ra)
	-[0x80005240]:sw t6, 832(ra)
Current Store : [0x80005240] : sw t6, 832(ra) -- Store: [0x800131f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005234]:fadd.d t5, t3, s10, dyn
	-[0x80005238]:csrrs a7, fcsr, zero
	-[0x8000523c]:sw t5, 824(ra)
	-[0x80005240]:sw t6, 832(ra)
	-[0x80005244]:sw t5, 840(ra)
Current Store : [0x80005244] : sw t5, 840(ra) -- Store: [0x800131f8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800052b8]:fadd.d t5, t3, s10, dyn
	-[0x800052bc]:csrrs a7, fcsr, zero
	-[0x800052c0]:sw t5, 856(ra)
	-[0x800052c4]:sw t6, 864(ra)
Current Store : [0x800052c4] : sw t6, 864(ra) -- Store: [0x80013210]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800052b8]:fadd.d t5, t3, s10, dyn
	-[0x800052bc]:csrrs a7, fcsr, zero
	-[0x800052c0]:sw t5, 856(ra)
	-[0x800052c4]:sw t6, 864(ra)
	-[0x800052c8]:sw t5, 872(ra)
Current Store : [0x800052c8] : sw t5, 872(ra) -- Store: [0x80013218]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005338]:fadd.d t5, t3, s10, dyn
	-[0x8000533c]:csrrs a7, fcsr, zero
	-[0x80005340]:sw t5, 888(ra)
	-[0x80005344]:sw t6, 896(ra)
Current Store : [0x80005344] : sw t6, 896(ra) -- Store: [0x80013230]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005338]:fadd.d t5, t3, s10, dyn
	-[0x8000533c]:csrrs a7, fcsr, zero
	-[0x80005340]:sw t5, 888(ra)
	-[0x80005344]:sw t6, 896(ra)
	-[0x80005348]:sw t5, 904(ra)
Current Store : [0x80005348] : sw t5, 904(ra) -- Store: [0x80013238]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800053b8]:fadd.d t5, t3, s10, dyn
	-[0x800053bc]:csrrs a7, fcsr, zero
	-[0x800053c0]:sw t5, 920(ra)
	-[0x800053c4]:sw t6, 928(ra)
Current Store : [0x800053c4] : sw t6, 928(ra) -- Store: [0x80013250]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800053b8]:fadd.d t5, t3, s10, dyn
	-[0x800053bc]:csrrs a7, fcsr, zero
	-[0x800053c0]:sw t5, 920(ra)
	-[0x800053c4]:sw t6, 928(ra)
	-[0x800053c8]:sw t5, 936(ra)
Current Store : [0x800053c8] : sw t5, 936(ra) -- Store: [0x80013258]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005438]:fadd.d t5, t3, s10, dyn
	-[0x8000543c]:csrrs a7, fcsr, zero
	-[0x80005440]:sw t5, 952(ra)
	-[0x80005444]:sw t6, 960(ra)
Current Store : [0x80005444] : sw t6, 960(ra) -- Store: [0x80013270]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005438]:fadd.d t5, t3, s10, dyn
	-[0x8000543c]:csrrs a7, fcsr, zero
	-[0x80005440]:sw t5, 952(ra)
	-[0x80005444]:sw t6, 960(ra)
	-[0x80005448]:sw t5, 968(ra)
Current Store : [0x80005448] : sw t5, 968(ra) -- Store: [0x80013278]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800054b8]:fadd.d t5, t3, s10, dyn
	-[0x800054bc]:csrrs a7, fcsr, zero
	-[0x800054c0]:sw t5, 984(ra)
	-[0x800054c4]:sw t6, 992(ra)
Current Store : [0x800054c4] : sw t6, 992(ra) -- Store: [0x80013290]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800054b8]:fadd.d t5, t3, s10, dyn
	-[0x800054bc]:csrrs a7, fcsr, zero
	-[0x800054c0]:sw t5, 984(ra)
	-[0x800054c4]:sw t6, 992(ra)
	-[0x800054c8]:sw t5, 1000(ra)
Current Store : [0x800054c8] : sw t5, 1000(ra) -- Store: [0x80013298]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005538]:fadd.d t5, t3, s10, dyn
	-[0x8000553c]:csrrs a7, fcsr, zero
	-[0x80005540]:sw t5, 1016(ra)
	-[0x80005544]:sw t6, 1024(ra)
Current Store : [0x80005544] : sw t6, 1024(ra) -- Store: [0x800132b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005538]:fadd.d t5, t3, s10, dyn
	-[0x8000553c]:csrrs a7, fcsr, zero
	-[0x80005540]:sw t5, 1016(ra)
	-[0x80005544]:sw t6, 1024(ra)
	-[0x80005548]:sw t5, 1032(ra)
Current Store : [0x80005548] : sw t5, 1032(ra) -- Store: [0x800132b8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800055b8]:fadd.d t5, t3, s10, dyn
	-[0x800055bc]:csrrs a7, fcsr, zero
	-[0x800055c0]:sw t5, 1048(ra)
	-[0x800055c4]:sw t6, 1056(ra)
Current Store : [0x800055c4] : sw t6, 1056(ra) -- Store: [0x800132d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800055b8]:fadd.d t5, t3, s10, dyn
	-[0x800055bc]:csrrs a7, fcsr, zero
	-[0x800055c0]:sw t5, 1048(ra)
	-[0x800055c4]:sw t6, 1056(ra)
	-[0x800055c8]:sw t5, 1064(ra)
Current Store : [0x800055c8] : sw t5, 1064(ra) -- Store: [0x800132d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005638]:fadd.d t5, t3, s10, dyn
	-[0x8000563c]:csrrs a7, fcsr, zero
	-[0x80005640]:sw t5, 1080(ra)
	-[0x80005644]:sw t6, 1088(ra)
Current Store : [0x80005644] : sw t6, 1088(ra) -- Store: [0x800132f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005638]:fadd.d t5, t3, s10, dyn
	-[0x8000563c]:csrrs a7, fcsr, zero
	-[0x80005640]:sw t5, 1080(ra)
	-[0x80005644]:sw t6, 1088(ra)
	-[0x80005648]:sw t5, 1096(ra)
Current Store : [0x80005648] : sw t5, 1096(ra) -- Store: [0x800132f8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800056b8]:fadd.d t5, t3, s10, dyn
	-[0x800056bc]:csrrs a7, fcsr, zero
	-[0x800056c0]:sw t5, 1112(ra)
	-[0x800056c4]:sw t6, 1120(ra)
Current Store : [0x800056c4] : sw t6, 1120(ra) -- Store: [0x80013310]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800056b8]:fadd.d t5, t3, s10, dyn
	-[0x800056bc]:csrrs a7, fcsr, zero
	-[0x800056c0]:sw t5, 1112(ra)
	-[0x800056c4]:sw t6, 1120(ra)
	-[0x800056c8]:sw t5, 1128(ra)
Current Store : [0x800056c8] : sw t5, 1128(ra) -- Store: [0x80013318]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005738]:fadd.d t5, t3, s10, dyn
	-[0x8000573c]:csrrs a7, fcsr, zero
	-[0x80005740]:sw t5, 1144(ra)
	-[0x80005744]:sw t6, 1152(ra)
Current Store : [0x80005744] : sw t6, 1152(ra) -- Store: [0x80013330]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005738]:fadd.d t5, t3, s10, dyn
	-[0x8000573c]:csrrs a7, fcsr, zero
	-[0x80005740]:sw t5, 1144(ra)
	-[0x80005744]:sw t6, 1152(ra)
	-[0x80005748]:sw t5, 1160(ra)
Current Store : [0x80005748] : sw t5, 1160(ra) -- Store: [0x80013338]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800057b8]:fadd.d t5, t3, s10, dyn
	-[0x800057bc]:csrrs a7, fcsr, zero
	-[0x800057c0]:sw t5, 1176(ra)
	-[0x800057c4]:sw t6, 1184(ra)
Current Store : [0x800057c4] : sw t6, 1184(ra) -- Store: [0x80013350]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800057b8]:fadd.d t5, t3, s10, dyn
	-[0x800057bc]:csrrs a7, fcsr, zero
	-[0x800057c0]:sw t5, 1176(ra)
	-[0x800057c4]:sw t6, 1184(ra)
	-[0x800057c8]:sw t5, 1192(ra)
Current Store : [0x800057c8] : sw t5, 1192(ra) -- Store: [0x80013358]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005838]:fadd.d t5, t3, s10, dyn
	-[0x8000583c]:csrrs a7, fcsr, zero
	-[0x80005840]:sw t5, 1208(ra)
	-[0x80005844]:sw t6, 1216(ra)
Current Store : [0x80005844] : sw t6, 1216(ra) -- Store: [0x80013370]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005838]:fadd.d t5, t3, s10, dyn
	-[0x8000583c]:csrrs a7, fcsr, zero
	-[0x80005840]:sw t5, 1208(ra)
	-[0x80005844]:sw t6, 1216(ra)
	-[0x80005848]:sw t5, 1224(ra)
Current Store : [0x80005848] : sw t5, 1224(ra) -- Store: [0x80013378]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800058b8]:fadd.d t5, t3, s10, dyn
	-[0x800058bc]:csrrs a7, fcsr, zero
	-[0x800058c0]:sw t5, 1240(ra)
	-[0x800058c4]:sw t6, 1248(ra)
Current Store : [0x800058c4] : sw t6, 1248(ra) -- Store: [0x80013390]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800058b8]:fadd.d t5, t3, s10, dyn
	-[0x800058bc]:csrrs a7, fcsr, zero
	-[0x800058c0]:sw t5, 1240(ra)
	-[0x800058c4]:sw t6, 1248(ra)
	-[0x800058c8]:sw t5, 1256(ra)
Current Store : [0x800058c8] : sw t5, 1256(ra) -- Store: [0x80013398]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005938]:fadd.d t5, t3, s10, dyn
	-[0x8000593c]:csrrs a7, fcsr, zero
	-[0x80005940]:sw t5, 1272(ra)
	-[0x80005944]:sw t6, 1280(ra)
Current Store : [0x80005944] : sw t6, 1280(ra) -- Store: [0x800133b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005938]:fadd.d t5, t3, s10, dyn
	-[0x8000593c]:csrrs a7, fcsr, zero
	-[0x80005940]:sw t5, 1272(ra)
	-[0x80005944]:sw t6, 1280(ra)
	-[0x80005948]:sw t5, 1288(ra)
Current Store : [0x80005948] : sw t5, 1288(ra) -- Store: [0x800133b8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800059b8]:fadd.d t5, t3, s10, dyn
	-[0x800059bc]:csrrs a7, fcsr, zero
	-[0x800059c0]:sw t5, 1304(ra)
	-[0x800059c4]:sw t6, 1312(ra)
Current Store : [0x800059c4] : sw t6, 1312(ra) -- Store: [0x800133d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800059b8]:fadd.d t5, t3, s10, dyn
	-[0x800059bc]:csrrs a7, fcsr, zero
	-[0x800059c0]:sw t5, 1304(ra)
	-[0x800059c4]:sw t6, 1312(ra)
	-[0x800059c8]:sw t5, 1320(ra)
Current Store : [0x800059c8] : sw t5, 1320(ra) -- Store: [0x800133d8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005a38]:fadd.d t5, t3, s10, dyn
	-[0x80005a3c]:csrrs a7, fcsr, zero
	-[0x80005a40]:sw t5, 1336(ra)
	-[0x80005a44]:sw t6, 1344(ra)
Current Store : [0x80005a44] : sw t6, 1344(ra) -- Store: [0x800133f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005a38]:fadd.d t5, t3, s10, dyn
	-[0x80005a3c]:csrrs a7, fcsr, zero
	-[0x80005a40]:sw t5, 1336(ra)
	-[0x80005a44]:sw t6, 1344(ra)
	-[0x80005a48]:sw t5, 1352(ra)
Current Store : [0x80005a48] : sw t5, 1352(ra) -- Store: [0x800133f8]:0x00000004




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005ab8]:fadd.d t5, t3, s10, dyn
	-[0x80005abc]:csrrs a7, fcsr, zero
	-[0x80005ac0]:sw t5, 1368(ra)
	-[0x80005ac4]:sw t6, 1376(ra)
Current Store : [0x80005ac4] : sw t6, 1376(ra) -- Store: [0x80013410]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005ab8]:fadd.d t5, t3, s10, dyn
	-[0x80005abc]:csrrs a7, fcsr, zero
	-[0x80005ac0]:sw t5, 1368(ra)
	-[0x80005ac4]:sw t6, 1376(ra)
	-[0x80005ac8]:sw t5, 1384(ra)
Current Store : [0x80005ac8] : sw t5, 1384(ra) -- Store: [0x80013418]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005b3c]:fadd.d t5, t3, s10, dyn
	-[0x80005b40]:csrrs a7, fcsr, zero
	-[0x80005b44]:sw t5, 1400(ra)
	-[0x80005b48]:sw t6, 1408(ra)
Current Store : [0x80005b48] : sw t6, 1408(ra) -- Store: [0x80013430]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005b3c]:fadd.d t5, t3, s10, dyn
	-[0x80005b40]:csrrs a7, fcsr, zero
	-[0x80005b44]:sw t5, 1400(ra)
	-[0x80005b48]:sw t6, 1408(ra)
	-[0x80005b4c]:sw t5, 1416(ra)
Current Store : [0x80005b4c] : sw t5, 1416(ra) -- Store: [0x80013438]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005bc0]:fadd.d t5, t3, s10, dyn
	-[0x80005bc4]:csrrs a7, fcsr, zero
	-[0x80005bc8]:sw t5, 1432(ra)
	-[0x80005bcc]:sw t6, 1440(ra)
Current Store : [0x80005bcc] : sw t6, 1440(ra) -- Store: [0x80013450]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005bc0]:fadd.d t5, t3, s10, dyn
	-[0x80005bc4]:csrrs a7, fcsr, zero
	-[0x80005bc8]:sw t5, 1432(ra)
	-[0x80005bcc]:sw t6, 1440(ra)
	-[0x80005bd0]:sw t5, 1448(ra)
Current Store : [0x80005bd0] : sw t5, 1448(ra) -- Store: [0x80013458]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005c40]:fadd.d t5, t3, s10, dyn
	-[0x80005c44]:csrrs a7, fcsr, zero
	-[0x80005c48]:sw t5, 1464(ra)
	-[0x80005c4c]:sw t6, 1472(ra)
Current Store : [0x80005c4c] : sw t6, 1472(ra) -- Store: [0x80013470]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005c40]:fadd.d t5, t3, s10, dyn
	-[0x80005c44]:csrrs a7, fcsr, zero
	-[0x80005c48]:sw t5, 1464(ra)
	-[0x80005c4c]:sw t6, 1472(ra)
	-[0x80005c50]:sw t5, 1480(ra)
Current Store : [0x80005c50] : sw t5, 1480(ra) -- Store: [0x80013478]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005cc0]:fadd.d t5, t3, s10, dyn
	-[0x80005cc4]:csrrs a7, fcsr, zero
	-[0x80005cc8]:sw t5, 1496(ra)
	-[0x80005ccc]:sw t6, 1504(ra)
Current Store : [0x80005ccc] : sw t6, 1504(ra) -- Store: [0x80013490]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005cc0]:fadd.d t5, t3, s10, dyn
	-[0x80005cc4]:csrrs a7, fcsr, zero
	-[0x80005cc8]:sw t5, 1496(ra)
	-[0x80005ccc]:sw t6, 1504(ra)
	-[0x80005cd0]:sw t5, 1512(ra)
Current Store : [0x80005cd0] : sw t5, 1512(ra) -- Store: [0x80013498]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005d40]:fadd.d t5, t3, s10, dyn
	-[0x80005d44]:csrrs a7, fcsr, zero
	-[0x80005d48]:sw t5, 1528(ra)
	-[0x80005d4c]:sw t6, 1536(ra)
Current Store : [0x80005d4c] : sw t6, 1536(ra) -- Store: [0x800134b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005d40]:fadd.d t5, t3, s10, dyn
	-[0x80005d44]:csrrs a7, fcsr, zero
	-[0x80005d48]:sw t5, 1528(ra)
	-[0x80005d4c]:sw t6, 1536(ra)
	-[0x80005d50]:sw t5, 1544(ra)
Current Store : [0x80005d50] : sw t5, 1544(ra) -- Store: [0x800134b8]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005dc0]:fadd.d t5, t3, s10, dyn
	-[0x80005dc4]:csrrs a7, fcsr, zero
	-[0x80005dc8]:sw t5, 1560(ra)
	-[0x80005dcc]:sw t6, 1568(ra)
Current Store : [0x80005dcc] : sw t6, 1568(ra) -- Store: [0x800134d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005dc0]:fadd.d t5, t3, s10, dyn
	-[0x80005dc4]:csrrs a7, fcsr, zero
	-[0x80005dc8]:sw t5, 1560(ra)
	-[0x80005dcc]:sw t6, 1568(ra)
	-[0x80005dd0]:sw t5, 1576(ra)
Current Store : [0x80005dd0] : sw t5, 1576(ra) -- Store: [0x800134d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005e44]:fadd.d t5, t3, s10, dyn
	-[0x80005e48]:csrrs a7, fcsr, zero
	-[0x80005e4c]:sw t5, 1592(ra)
	-[0x80005e50]:sw t6, 1600(ra)
Current Store : [0x80005e50] : sw t6, 1600(ra) -- Store: [0x800134f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005e44]:fadd.d t5, t3, s10, dyn
	-[0x80005e48]:csrrs a7, fcsr, zero
	-[0x80005e4c]:sw t5, 1592(ra)
	-[0x80005e50]:sw t6, 1600(ra)
	-[0x80005e54]:sw t5, 1608(ra)
Current Store : [0x80005e54] : sw t5, 1608(ra) -- Store: [0x800134f8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005ec8]:fadd.d t5, t3, s10, dyn
	-[0x80005ecc]:csrrs a7, fcsr, zero
	-[0x80005ed0]:sw t5, 1624(ra)
	-[0x80005ed4]:sw t6, 1632(ra)
Current Store : [0x80005ed4] : sw t6, 1632(ra) -- Store: [0x80013510]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005ec8]:fadd.d t5, t3, s10, dyn
	-[0x80005ecc]:csrrs a7, fcsr, zero
	-[0x80005ed0]:sw t5, 1624(ra)
	-[0x80005ed4]:sw t6, 1632(ra)
	-[0x80005ed8]:sw t5, 1640(ra)
Current Store : [0x80005ed8] : sw t5, 1640(ra) -- Store: [0x80013518]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005f48]:fadd.d t5, t3, s10, dyn
	-[0x80005f4c]:csrrs a7, fcsr, zero
	-[0x80005f50]:sw t5, 1656(ra)
	-[0x80005f54]:sw t6, 1664(ra)
Current Store : [0x80005f54] : sw t6, 1664(ra) -- Store: [0x80013530]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005f48]:fadd.d t5, t3, s10, dyn
	-[0x80005f4c]:csrrs a7, fcsr, zero
	-[0x80005f50]:sw t5, 1656(ra)
	-[0x80005f54]:sw t6, 1664(ra)
	-[0x80005f58]:sw t5, 1672(ra)
Current Store : [0x80005f58] : sw t5, 1672(ra) -- Store: [0x80013538]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005fc8]:fadd.d t5, t3, s10, dyn
	-[0x80005fcc]:csrrs a7, fcsr, zero
	-[0x80005fd0]:sw t5, 1688(ra)
	-[0x80005fd4]:sw t6, 1696(ra)
Current Store : [0x80005fd4] : sw t6, 1696(ra) -- Store: [0x80013550]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80005fc8]:fadd.d t5, t3, s10, dyn
	-[0x80005fcc]:csrrs a7, fcsr, zero
	-[0x80005fd0]:sw t5, 1688(ra)
	-[0x80005fd4]:sw t6, 1696(ra)
	-[0x80005fd8]:sw t5, 1704(ra)
Current Store : [0x80005fd8] : sw t5, 1704(ra) -- Store: [0x80013558]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006048]:fadd.d t5, t3, s10, dyn
	-[0x8000604c]:csrrs a7, fcsr, zero
	-[0x80006050]:sw t5, 1720(ra)
	-[0x80006054]:sw t6, 1728(ra)
Current Store : [0x80006054] : sw t6, 1728(ra) -- Store: [0x80013570]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006048]:fadd.d t5, t3, s10, dyn
	-[0x8000604c]:csrrs a7, fcsr, zero
	-[0x80006050]:sw t5, 1720(ra)
	-[0x80006054]:sw t6, 1728(ra)
	-[0x80006058]:sw t5, 1736(ra)
Current Store : [0x80006058] : sw t5, 1736(ra) -- Store: [0x80013578]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800060c8]:fadd.d t5, t3, s10, dyn
	-[0x800060cc]:csrrs a7, fcsr, zero
	-[0x800060d0]:sw t5, 1752(ra)
	-[0x800060d4]:sw t6, 1760(ra)
Current Store : [0x800060d4] : sw t6, 1760(ra) -- Store: [0x80013590]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800060c8]:fadd.d t5, t3, s10, dyn
	-[0x800060cc]:csrrs a7, fcsr, zero
	-[0x800060d0]:sw t5, 1752(ra)
	-[0x800060d4]:sw t6, 1760(ra)
	-[0x800060d8]:sw t5, 1768(ra)
Current Store : [0x800060d8] : sw t5, 1768(ra) -- Store: [0x80013598]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006148]:fadd.d t5, t3, s10, dyn
	-[0x8000614c]:csrrs a7, fcsr, zero
	-[0x80006150]:sw t5, 1784(ra)
	-[0x80006154]:sw t6, 1792(ra)
Current Store : [0x80006154] : sw t6, 1792(ra) -- Store: [0x800135b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006148]:fadd.d t5, t3, s10, dyn
	-[0x8000614c]:csrrs a7, fcsr, zero
	-[0x80006150]:sw t5, 1784(ra)
	-[0x80006154]:sw t6, 1792(ra)
	-[0x80006158]:sw t5, 1800(ra)
Current Store : [0x80006158] : sw t5, 1800(ra) -- Store: [0x800135b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800061c8]:fadd.d t5, t3, s10, dyn
	-[0x800061cc]:csrrs a7, fcsr, zero
	-[0x800061d0]:sw t5, 1816(ra)
	-[0x800061d4]:sw t6, 1824(ra)
Current Store : [0x800061d4] : sw t6, 1824(ra) -- Store: [0x800135d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800061c8]:fadd.d t5, t3, s10, dyn
	-[0x800061cc]:csrrs a7, fcsr, zero
	-[0x800061d0]:sw t5, 1816(ra)
	-[0x800061d4]:sw t6, 1824(ra)
	-[0x800061d8]:sw t5, 1832(ra)
Current Store : [0x800061d8] : sw t5, 1832(ra) -- Store: [0x800135d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006248]:fadd.d t5, t3, s10, dyn
	-[0x8000624c]:csrrs a7, fcsr, zero
	-[0x80006250]:sw t5, 1848(ra)
	-[0x80006254]:sw t6, 1856(ra)
Current Store : [0x80006254] : sw t6, 1856(ra) -- Store: [0x800135f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006248]:fadd.d t5, t3, s10, dyn
	-[0x8000624c]:csrrs a7, fcsr, zero
	-[0x80006250]:sw t5, 1848(ra)
	-[0x80006254]:sw t6, 1856(ra)
	-[0x80006258]:sw t5, 1864(ra)
Current Store : [0x80006258] : sw t5, 1864(ra) -- Store: [0x800135f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800062c8]:fadd.d t5, t3, s10, dyn
	-[0x800062cc]:csrrs a7, fcsr, zero
	-[0x800062d0]:sw t5, 1880(ra)
	-[0x800062d4]:sw t6, 1888(ra)
Current Store : [0x800062d4] : sw t6, 1888(ra) -- Store: [0x80013610]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800062c8]:fadd.d t5, t3, s10, dyn
	-[0x800062cc]:csrrs a7, fcsr, zero
	-[0x800062d0]:sw t5, 1880(ra)
	-[0x800062d4]:sw t6, 1888(ra)
	-[0x800062d8]:sw t5, 1896(ra)
Current Store : [0x800062d8] : sw t5, 1896(ra) -- Store: [0x80013618]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006348]:fadd.d t5, t3, s10, dyn
	-[0x8000634c]:csrrs a7, fcsr, zero
	-[0x80006350]:sw t5, 1912(ra)
	-[0x80006354]:sw t6, 1920(ra)
Current Store : [0x80006354] : sw t6, 1920(ra) -- Store: [0x80013630]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006348]:fadd.d t5, t3, s10, dyn
	-[0x8000634c]:csrrs a7, fcsr, zero
	-[0x80006350]:sw t5, 1912(ra)
	-[0x80006354]:sw t6, 1920(ra)
	-[0x80006358]:sw t5, 1928(ra)
Current Store : [0x80006358] : sw t5, 1928(ra) -- Store: [0x80013638]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800063c8]:fadd.d t5, t3, s10, dyn
	-[0x800063cc]:csrrs a7, fcsr, zero
	-[0x800063d0]:sw t5, 1944(ra)
	-[0x800063d4]:sw t6, 1952(ra)
Current Store : [0x800063d4] : sw t6, 1952(ra) -- Store: [0x80013650]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800063c8]:fadd.d t5, t3, s10, dyn
	-[0x800063cc]:csrrs a7, fcsr, zero
	-[0x800063d0]:sw t5, 1944(ra)
	-[0x800063d4]:sw t6, 1952(ra)
	-[0x800063d8]:sw t5, 1960(ra)
Current Store : [0x800063d8] : sw t5, 1960(ra) -- Store: [0x80013658]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006448]:fadd.d t5, t3, s10, dyn
	-[0x8000644c]:csrrs a7, fcsr, zero
	-[0x80006450]:sw t5, 1976(ra)
	-[0x80006454]:sw t6, 1984(ra)
Current Store : [0x80006454] : sw t6, 1984(ra) -- Store: [0x80013670]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006448]:fadd.d t5, t3, s10, dyn
	-[0x8000644c]:csrrs a7, fcsr, zero
	-[0x80006450]:sw t5, 1976(ra)
	-[0x80006454]:sw t6, 1984(ra)
	-[0x80006458]:sw t5, 1992(ra)
Current Store : [0x80006458] : sw t5, 1992(ra) -- Store: [0x80013678]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800064c8]:fadd.d t5, t3, s10, dyn
	-[0x800064cc]:csrrs a7, fcsr, zero
	-[0x800064d0]:sw t5, 2008(ra)
	-[0x800064d4]:sw t6, 2016(ra)
Current Store : [0x800064d4] : sw t6, 2016(ra) -- Store: [0x80013690]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800064c8]:fadd.d t5, t3, s10, dyn
	-[0x800064cc]:csrrs a7, fcsr, zero
	-[0x800064d0]:sw t5, 2008(ra)
	-[0x800064d4]:sw t6, 2016(ra)
	-[0x800064d8]:sw t5, 2024(ra)
Current Store : [0x800064d8] : sw t5, 2024(ra) -- Store: [0x80013698]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006548]:fadd.d t5, t3, s10, dyn
	-[0x8000654c]:csrrs a7, fcsr, zero
	-[0x80006550]:sw t5, 2040(ra)
	-[0x80006554]:addi ra, ra, 2040
	-[0x80006558]:sw t6, 8(ra)
Current Store : [0x80006558] : sw t6, 8(ra) -- Store: [0x800136b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006548]:fadd.d t5, t3, s10, dyn
	-[0x8000654c]:csrrs a7, fcsr, zero
	-[0x80006550]:sw t5, 2040(ra)
	-[0x80006554]:addi ra, ra, 2040
	-[0x80006558]:sw t6, 8(ra)
	-[0x8000655c]:sw t5, 16(ra)
Current Store : [0x8000655c] : sw t5, 16(ra) -- Store: [0x800136b8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006594]:fadd.d t5, t3, s10, dyn
	-[0x80006598]:csrrs a7, fcsr, zero
	-[0x8000659c]:sw t5, 0(ra)
	-[0x800065a0]:sw t6, 8(ra)
Current Store : [0x800065a0] : sw t6, 8(ra) -- Store: [0x800126d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006594]:fadd.d t5, t3, s10, dyn
	-[0x80006598]:csrrs a7, fcsr, zero
	-[0x8000659c]:sw t5, 0(ra)
	-[0x800065a0]:sw t6, 8(ra)
	-[0x800065a4]:sw t5, 16(ra)
Current Store : [0x800065a4] : sw t5, 16(ra) -- Store: [0x800126d8]:0x00000003




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800065d4]:fadd.d t5, t3, s10, dyn
	-[0x800065d8]:csrrs a7, fcsr, zero
	-[0x800065dc]:sw t5, 32(ra)
	-[0x800065e0]:sw t6, 40(ra)
Current Store : [0x800065e0] : sw t6, 40(ra) -- Store: [0x800126f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800065d4]:fadd.d t5, t3, s10, dyn
	-[0x800065d8]:csrrs a7, fcsr, zero
	-[0x800065dc]:sw t5, 32(ra)
	-[0x800065e0]:sw t6, 40(ra)
	-[0x800065e4]:sw t5, 48(ra)
Current Store : [0x800065e4] : sw t5, 48(ra) -- Store: [0x800126f8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006614]:fadd.d t5, t3, s10, dyn
	-[0x80006618]:csrrs a7, fcsr, zero
	-[0x8000661c]:sw t5, 64(ra)
	-[0x80006620]:sw t6, 72(ra)
Current Store : [0x80006620] : sw t6, 72(ra) -- Store: [0x80012710]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006614]:fadd.d t5, t3, s10, dyn
	-[0x80006618]:csrrs a7, fcsr, zero
	-[0x8000661c]:sw t5, 64(ra)
	-[0x80006620]:sw t6, 72(ra)
	-[0x80006624]:sw t5, 80(ra)
Current Store : [0x80006624] : sw t5, 80(ra) -- Store: [0x80012718]:0x00000004




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006658]:fadd.d t5, t3, s10, dyn
	-[0x8000665c]:csrrs a7, fcsr, zero
	-[0x80006660]:sw t5, 96(ra)
	-[0x80006664]:sw t6, 104(ra)
Current Store : [0x80006664] : sw t6, 104(ra) -- Store: [0x80012730]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006658]:fadd.d t5, t3, s10, dyn
	-[0x8000665c]:csrrs a7, fcsr, zero
	-[0x80006660]:sw t5, 96(ra)
	-[0x80006664]:sw t6, 104(ra)
	-[0x80006668]:sw t5, 112(ra)
Current Store : [0x80006668] : sw t5, 112(ra) -- Store: [0x80012738]:0x00000003




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000669c]:fadd.d t5, t3, s10, dyn
	-[0x800066a0]:csrrs a7, fcsr, zero
	-[0x800066a4]:sw t5, 128(ra)
	-[0x800066a8]:sw t6, 136(ra)
Current Store : [0x800066a8] : sw t6, 136(ra) -- Store: [0x80012750]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000669c]:fadd.d t5, t3, s10, dyn
	-[0x800066a0]:csrrs a7, fcsr, zero
	-[0x800066a4]:sw t5, 128(ra)
	-[0x800066a8]:sw t6, 136(ra)
	-[0x800066ac]:sw t5, 144(ra)
Current Store : [0x800066ac] : sw t5, 144(ra) -- Store: [0x80012758]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800066dc]:fadd.d t5, t3, s10, dyn
	-[0x800066e0]:csrrs a7, fcsr, zero
	-[0x800066e4]:sw t5, 160(ra)
	-[0x800066e8]:sw t6, 168(ra)
Current Store : [0x800066e8] : sw t6, 168(ra) -- Store: [0x80012770]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800066dc]:fadd.d t5, t3, s10, dyn
	-[0x800066e0]:csrrs a7, fcsr, zero
	-[0x800066e4]:sw t5, 160(ra)
	-[0x800066e8]:sw t6, 168(ra)
	-[0x800066ec]:sw t5, 176(ra)
Current Store : [0x800066ec] : sw t5, 176(ra) -- Store: [0x80012778]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000671c]:fadd.d t5, t3, s10, dyn
	-[0x80006720]:csrrs a7, fcsr, zero
	-[0x80006724]:sw t5, 192(ra)
	-[0x80006728]:sw t6, 200(ra)
Current Store : [0x80006728] : sw t6, 200(ra) -- Store: [0x80012790]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000671c]:fadd.d t5, t3, s10, dyn
	-[0x80006720]:csrrs a7, fcsr, zero
	-[0x80006724]:sw t5, 192(ra)
	-[0x80006728]:sw t6, 200(ra)
	-[0x8000672c]:sw t5, 208(ra)
Current Store : [0x8000672c] : sw t5, 208(ra) -- Store: [0x80012798]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000675c]:fadd.d t5, t3, s10, dyn
	-[0x80006760]:csrrs a7, fcsr, zero
	-[0x80006764]:sw t5, 224(ra)
	-[0x80006768]:sw t6, 232(ra)
Current Store : [0x80006768] : sw t6, 232(ra) -- Store: [0x800127b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000675c]:fadd.d t5, t3, s10, dyn
	-[0x80006760]:csrrs a7, fcsr, zero
	-[0x80006764]:sw t5, 224(ra)
	-[0x80006768]:sw t6, 232(ra)
	-[0x8000676c]:sw t5, 240(ra)
Current Store : [0x8000676c] : sw t5, 240(ra) -- Store: [0x800127b8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000679c]:fadd.d t5, t3, s10, dyn
	-[0x800067a0]:csrrs a7, fcsr, zero
	-[0x800067a4]:sw t5, 256(ra)
	-[0x800067a8]:sw t6, 264(ra)
Current Store : [0x800067a8] : sw t6, 264(ra) -- Store: [0x800127d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000679c]:fadd.d t5, t3, s10, dyn
	-[0x800067a0]:csrrs a7, fcsr, zero
	-[0x800067a4]:sw t5, 256(ra)
	-[0x800067a8]:sw t6, 264(ra)
	-[0x800067ac]:sw t5, 272(ra)
Current Store : [0x800067ac] : sw t5, 272(ra) -- Store: [0x800127d8]:0x00000002




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800067e0]:fadd.d t5, t3, s10, dyn
	-[0x800067e4]:csrrs a7, fcsr, zero
	-[0x800067e8]:sw t5, 288(ra)
	-[0x800067ec]:sw t6, 296(ra)
Current Store : [0x800067ec] : sw t6, 296(ra) -- Store: [0x800127f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800067e0]:fadd.d t5, t3, s10, dyn
	-[0x800067e4]:csrrs a7, fcsr, zero
	-[0x800067e8]:sw t5, 288(ra)
	-[0x800067ec]:sw t6, 296(ra)
	-[0x800067f0]:sw t5, 304(ra)
Current Store : [0x800067f0] : sw t5, 304(ra) -- Store: [0x800127f8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006824]:fadd.d t5, t3, s10, dyn
	-[0x80006828]:csrrs a7, fcsr, zero
	-[0x8000682c]:sw t5, 320(ra)
	-[0x80006830]:sw t6, 328(ra)
Current Store : [0x80006830] : sw t6, 328(ra) -- Store: [0x80012810]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006824]:fadd.d t5, t3, s10, dyn
	-[0x80006828]:csrrs a7, fcsr, zero
	-[0x8000682c]:sw t5, 320(ra)
	-[0x80006830]:sw t6, 328(ra)
	-[0x80006834]:sw t5, 336(ra)
Current Store : [0x80006834] : sw t5, 336(ra) -- Store: [0x80012818]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006864]:fadd.d t5, t3, s10, dyn
	-[0x80006868]:csrrs a7, fcsr, zero
	-[0x8000686c]:sw t5, 352(ra)
	-[0x80006870]:sw t6, 360(ra)
Current Store : [0x80006870] : sw t6, 360(ra) -- Store: [0x80012830]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006864]:fadd.d t5, t3, s10, dyn
	-[0x80006868]:csrrs a7, fcsr, zero
	-[0x8000686c]:sw t5, 352(ra)
	-[0x80006870]:sw t6, 360(ra)
	-[0x80006874]:sw t5, 368(ra)
Current Store : [0x80006874] : sw t5, 368(ra) -- Store: [0x80012838]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800068a4]:fadd.d t5, t3, s10, dyn
	-[0x800068a8]:csrrs a7, fcsr, zero
	-[0x800068ac]:sw t5, 384(ra)
	-[0x800068b0]:sw t6, 392(ra)
Current Store : [0x800068b0] : sw t6, 392(ra) -- Store: [0x80012850]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800068a4]:fadd.d t5, t3, s10, dyn
	-[0x800068a8]:csrrs a7, fcsr, zero
	-[0x800068ac]:sw t5, 384(ra)
	-[0x800068b0]:sw t6, 392(ra)
	-[0x800068b4]:sw t5, 400(ra)
Current Store : [0x800068b4] : sw t5, 400(ra) -- Store: [0x80012858]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800068e4]:fadd.d t5, t3, s10, dyn
	-[0x800068e8]:csrrs a7, fcsr, zero
	-[0x800068ec]:sw t5, 416(ra)
	-[0x800068f0]:sw t6, 424(ra)
Current Store : [0x800068f0] : sw t6, 424(ra) -- Store: [0x80012870]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800068e4]:fadd.d t5, t3, s10, dyn
	-[0x800068e8]:csrrs a7, fcsr, zero
	-[0x800068ec]:sw t5, 416(ra)
	-[0x800068f0]:sw t6, 424(ra)
	-[0x800068f4]:sw t5, 432(ra)
Current Store : [0x800068f4] : sw t5, 432(ra) -- Store: [0x80012878]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006924]:fadd.d t5, t3, s10, dyn
	-[0x80006928]:csrrs a7, fcsr, zero
	-[0x8000692c]:sw t5, 448(ra)
	-[0x80006930]:sw t6, 456(ra)
Current Store : [0x80006930] : sw t6, 456(ra) -- Store: [0x80012890]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006924]:fadd.d t5, t3, s10, dyn
	-[0x80006928]:csrrs a7, fcsr, zero
	-[0x8000692c]:sw t5, 448(ra)
	-[0x80006930]:sw t6, 456(ra)
	-[0x80006934]:sw t5, 464(ra)
Current Store : [0x80006934] : sw t5, 464(ra) -- Store: [0x80012898]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006964]:fadd.d t5, t3, s10, dyn
	-[0x80006968]:csrrs a7, fcsr, zero
	-[0x8000696c]:sw t5, 480(ra)
	-[0x80006970]:sw t6, 488(ra)
Current Store : [0x80006970] : sw t6, 488(ra) -- Store: [0x800128b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006964]:fadd.d t5, t3, s10, dyn
	-[0x80006968]:csrrs a7, fcsr, zero
	-[0x8000696c]:sw t5, 480(ra)
	-[0x80006970]:sw t6, 488(ra)
	-[0x80006974]:sw t5, 496(ra)
Current Store : [0x80006974] : sw t5, 496(ra) -- Store: [0x800128b8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800069a4]:fadd.d t5, t3, s10, dyn
	-[0x800069a8]:csrrs a7, fcsr, zero
	-[0x800069ac]:sw t5, 512(ra)
	-[0x800069b0]:sw t6, 520(ra)
Current Store : [0x800069b0] : sw t6, 520(ra) -- Store: [0x800128d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800069a4]:fadd.d t5, t3, s10, dyn
	-[0x800069a8]:csrrs a7, fcsr, zero
	-[0x800069ac]:sw t5, 512(ra)
	-[0x800069b0]:sw t6, 520(ra)
	-[0x800069b4]:sw t5, 528(ra)
Current Store : [0x800069b4] : sw t5, 528(ra) -- Store: [0x800128d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800069e4]:fadd.d t5, t3, s10, dyn
	-[0x800069e8]:csrrs a7, fcsr, zero
	-[0x800069ec]:sw t5, 544(ra)
	-[0x800069f0]:sw t6, 552(ra)
Current Store : [0x800069f0] : sw t6, 552(ra) -- Store: [0x800128f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800069e4]:fadd.d t5, t3, s10, dyn
	-[0x800069e8]:csrrs a7, fcsr, zero
	-[0x800069ec]:sw t5, 544(ra)
	-[0x800069f0]:sw t6, 552(ra)
	-[0x800069f4]:sw t5, 560(ra)
Current Store : [0x800069f4] : sw t5, 560(ra) -- Store: [0x800128f8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006a24]:fadd.d t5, t3, s10, dyn
	-[0x80006a28]:csrrs a7, fcsr, zero
	-[0x80006a2c]:sw t5, 576(ra)
	-[0x80006a30]:sw t6, 584(ra)
Current Store : [0x80006a30] : sw t6, 584(ra) -- Store: [0x80012910]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006a24]:fadd.d t5, t3, s10, dyn
	-[0x80006a28]:csrrs a7, fcsr, zero
	-[0x80006a2c]:sw t5, 576(ra)
	-[0x80006a30]:sw t6, 584(ra)
	-[0x80006a34]:sw t5, 592(ra)
Current Store : [0x80006a34] : sw t5, 592(ra) -- Store: [0x80012918]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006a64]:fadd.d t5, t3, s10, dyn
	-[0x80006a68]:csrrs a7, fcsr, zero
	-[0x80006a6c]:sw t5, 608(ra)
	-[0x80006a70]:sw t6, 616(ra)
Current Store : [0x80006a70] : sw t6, 616(ra) -- Store: [0x80012930]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006a64]:fadd.d t5, t3, s10, dyn
	-[0x80006a68]:csrrs a7, fcsr, zero
	-[0x80006a6c]:sw t5, 608(ra)
	-[0x80006a70]:sw t6, 616(ra)
	-[0x80006a74]:sw t5, 624(ra)
Current Store : [0x80006a74] : sw t5, 624(ra) -- Store: [0x80012938]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006aa4]:fadd.d t5, t3, s10, dyn
	-[0x80006aa8]:csrrs a7, fcsr, zero
	-[0x80006aac]:sw t5, 640(ra)
	-[0x80006ab0]:sw t6, 648(ra)
Current Store : [0x80006ab0] : sw t6, 648(ra) -- Store: [0x80012950]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006aa4]:fadd.d t5, t3, s10, dyn
	-[0x80006aa8]:csrrs a7, fcsr, zero
	-[0x80006aac]:sw t5, 640(ra)
	-[0x80006ab0]:sw t6, 648(ra)
	-[0x80006ab4]:sw t5, 656(ra)
Current Store : [0x80006ab4] : sw t5, 656(ra) -- Store: [0x80012958]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006ae8]:fadd.d t5, t3, s10, dyn
	-[0x80006aec]:csrrs a7, fcsr, zero
	-[0x80006af0]:sw t5, 672(ra)
	-[0x80006af4]:sw t6, 680(ra)
Current Store : [0x80006af4] : sw t6, 680(ra) -- Store: [0x80012970]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006ae8]:fadd.d t5, t3, s10, dyn
	-[0x80006aec]:csrrs a7, fcsr, zero
	-[0x80006af0]:sw t5, 672(ra)
	-[0x80006af4]:sw t6, 680(ra)
	-[0x80006af8]:sw t5, 688(ra)
Current Store : [0x80006af8] : sw t5, 688(ra) -- Store: [0x80012978]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006b2c]:fadd.d t5, t3, s10, dyn
	-[0x80006b30]:csrrs a7, fcsr, zero
	-[0x80006b34]:sw t5, 704(ra)
	-[0x80006b38]:sw t6, 712(ra)
Current Store : [0x80006b38] : sw t6, 712(ra) -- Store: [0x80012990]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006b2c]:fadd.d t5, t3, s10, dyn
	-[0x80006b30]:csrrs a7, fcsr, zero
	-[0x80006b34]:sw t5, 704(ra)
	-[0x80006b38]:sw t6, 712(ra)
	-[0x80006b3c]:sw t5, 720(ra)
Current Store : [0x80006b3c] : sw t5, 720(ra) -- Store: [0x80012998]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006b70]:fadd.d t5, t3, s10, dyn
	-[0x80006b74]:csrrs a7, fcsr, zero
	-[0x80006b78]:sw t5, 736(ra)
	-[0x80006b7c]:sw t6, 744(ra)
Current Store : [0x80006b7c] : sw t6, 744(ra) -- Store: [0x800129b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006b70]:fadd.d t5, t3, s10, dyn
	-[0x80006b74]:csrrs a7, fcsr, zero
	-[0x80006b78]:sw t5, 736(ra)
	-[0x80006b7c]:sw t6, 744(ra)
	-[0x80006b80]:sw t5, 752(ra)
Current Store : [0x80006b80] : sw t5, 752(ra) -- Store: [0x800129b8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006bb4]:fadd.d t5, t3, s10, dyn
	-[0x80006bb8]:csrrs a7, fcsr, zero
	-[0x80006bbc]:sw t5, 768(ra)
	-[0x80006bc0]:sw t6, 776(ra)
Current Store : [0x80006bc0] : sw t6, 776(ra) -- Store: [0x800129d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006bb4]:fadd.d t5, t3, s10, dyn
	-[0x80006bb8]:csrrs a7, fcsr, zero
	-[0x80006bbc]:sw t5, 768(ra)
	-[0x80006bc0]:sw t6, 776(ra)
	-[0x80006bc4]:sw t5, 784(ra)
Current Store : [0x80006bc4] : sw t5, 784(ra) -- Store: [0x800129d8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006bf8]:fadd.d t5, t3, s10, dyn
	-[0x80006bfc]:csrrs a7, fcsr, zero
	-[0x80006c00]:sw t5, 800(ra)
	-[0x80006c04]:sw t6, 808(ra)
Current Store : [0x80006c04] : sw t6, 808(ra) -- Store: [0x800129f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006bf8]:fadd.d t5, t3, s10, dyn
	-[0x80006bfc]:csrrs a7, fcsr, zero
	-[0x80006c00]:sw t5, 800(ra)
	-[0x80006c04]:sw t6, 808(ra)
	-[0x80006c08]:sw t5, 816(ra)
Current Store : [0x80006c08] : sw t5, 816(ra) -- Store: [0x800129f8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006c3c]:fadd.d t5, t3, s10, dyn
	-[0x80006c40]:csrrs a7, fcsr, zero
	-[0x80006c44]:sw t5, 832(ra)
	-[0x80006c48]:sw t6, 840(ra)
Current Store : [0x80006c48] : sw t6, 840(ra) -- Store: [0x80012a10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006c3c]:fadd.d t5, t3, s10, dyn
	-[0x80006c40]:csrrs a7, fcsr, zero
	-[0x80006c44]:sw t5, 832(ra)
	-[0x80006c48]:sw t6, 840(ra)
	-[0x80006c4c]:sw t5, 848(ra)
Current Store : [0x80006c4c] : sw t5, 848(ra) -- Store: [0x80012a18]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006c84]:fadd.d t5, t3, s10, dyn
	-[0x80006c88]:csrrs a7, fcsr, zero
	-[0x80006c8c]:sw t5, 864(ra)
	-[0x80006c90]:sw t6, 872(ra)
Current Store : [0x80006c90] : sw t6, 872(ra) -- Store: [0x80012a30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006c84]:fadd.d t5, t3, s10, dyn
	-[0x80006c88]:csrrs a7, fcsr, zero
	-[0x80006c8c]:sw t5, 864(ra)
	-[0x80006c90]:sw t6, 872(ra)
	-[0x80006c94]:sw t5, 880(ra)
Current Store : [0x80006c94] : sw t5, 880(ra) -- Store: [0x80012a38]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006ccc]:fadd.d t5, t3, s10, dyn
	-[0x80006cd0]:csrrs a7, fcsr, zero
	-[0x80006cd4]:sw t5, 896(ra)
	-[0x80006cd8]:sw t6, 904(ra)
Current Store : [0x80006cd8] : sw t6, 904(ra) -- Store: [0x80012a50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006ccc]:fadd.d t5, t3, s10, dyn
	-[0x80006cd0]:csrrs a7, fcsr, zero
	-[0x80006cd4]:sw t5, 896(ra)
	-[0x80006cd8]:sw t6, 904(ra)
	-[0x80006cdc]:sw t5, 912(ra)
Current Store : [0x80006cdc] : sw t5, 912(ra) -- Store: [0x80012a58]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006d10]:fadd.d t5, t3, s10, dyn
	-[0x80006d14]:csrrs a7, fcsr, zero
	-[0x80006d18]:sw t5, 928(ra)
	-[0x80006d1c]:sw t6, 936(ra)
Current Store : [0x80006d1c] : sw t6, 936(ra) -- Store: [0x80012a70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006d10]:fadd.d t5, t3, s10, dyn
	-[0x80006d14]:csrrs a7, fcsr, zero
	-[0x80006d18]:sw t5, 928(ra)
	-[0x80006d1c]:sw t6, 936(ra)
	-[0x80006d20]:sw t5, 944(ra)
Current Store : [0x80006d20] : sw t5, 944(ra) -- Store: [0x80012a78]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006d54]:fadd.d t5, t3, s10, dyn
	-[0x80006d58]:csrrs a7, fcsr, zero
	-[0x80006d5c]:sw t5, 960(ra)
	-[0x80006d60]:sw t6, 968(ra)
Current Store : [0x80006d60] : sw t6, 968(ra) -- Store: [0x80012a90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006d54]:fadd.d t5, t3, s10, dyn
	-[0x80006d58]:csrrs a7, fcsr, zero
	-[0x80006d5c]:sw t5, 960(ra)
	-[0x80006d60]:sw t6, 968(ra)
	-[0x80006d64]:sw t5, 976(ra)
Current Store : [0x80006d64] : sw t5, 976(ra) -- Store: [0x80012a98]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006d98]:fadd.d t5, t3, s10, dyn
	-[0x80006d9c]:csrrs a7, fcsr, zero
	-[0x80006da0]:sw t5, 992(ra)
	-[0x80006da4]:sw t6, 1000(ra)
Current Store : [0x80006da4] : sw t6, 1000(ra) -- Store: [0x80012ab0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006d98]:fadd.d t5, t3, s10, dyn
	-[0x80006d9c]:csrrs a7, fcsr, zero
	-[0x80006da0]:sw t5, 992(ra)
	-[0x80006da4]:sw t6, 1000(ra)
	-[0x80006da8]:sw t5, 1008(ra)
Current Store : [0x80006da8] : sw t5, 1008(ra) -- Store: [0x80012ab8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006ddc]:fadd.d t5, t3, s10, dyn
	-[0x80006de0]:csrrs a7, fcsr, zero
	-[0x80006de4]:sw t5, 1024(ra)
	-[0x80006de8]:sw t6, 1032(ra)
Current Store : [0x80006de8] : sw t6, 1032(ra) -- Store: [0x80012ad0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006ddc]:fadd.d t5, t3, s10, dyn
	-[0x80006de0]:csrrs a7, fcsr, zero
	-[0x80006de4]:sw t5, 1024(ra)
	-[0x80006de8]:sw t6, 1032(ra)
	-[0x80006dec]:sw t5, 1040(ra)
Current Store : [0x80006dec] : sw t5, 1040(ra) -- Store: [0x80012ad8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006e24]:fadd.d t5, t3, s10, dyn
	-[0x80006e28]:csrrs a7, fcsr, zero
	-[0x80006e2c]:sw t5, 1056(ra)
	-[0x80006e30]:sw t6, 1064(ra)
Current Store : [0x80006e30] : sw t6, 1064(ra) -- Store: [0x80012af0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006e24]:fadd.d t5, t3, s10, dyn
	-[0x80006e28]:csrrs a7, fcsr, zero
	-[0x80006e2c]:sw t5, 1056(ra)
	-[0x80006e30]:sw t6, 1064(ra)
	-[0x80006e34]:sw t5, 1072(ra)
Current Store : [0x80006e34] : sw t5, 1072(ra) -- Store: [0x80012af8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006e6c]:fadd.d t5, t3, s10, dyn
	-[0x80006e70]:csrrs a7, fcsr, zero
	-[0x80006e74]:sw t5, 1088(ra)
	-[0x80006e78]:sw t6, 1096(ra)
Current Store : [0x80006e78] : sw t6, 1096(ra) -- Store: [0x80012b10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006e6c]:fadd.d t5, t3, s10, dyn
	-[0x80006e70]:csrrs a7, fcsr, zero
	-[0x80006e74]:sw t5, 1088(ra)
	-[0x80006e78]:sw t6, 1096(ra)
	-[0x80006e7c]:sw t5, 1104(ra)
Current Store : [0x80006e7c] : sw t5, 1104(ra) -- Store: [0x80012b18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006eb0]:fadd.d t5, t3, s10, dyn
	-[0x80006eb4]:csrrs a7, fcsr, zero
	-[0x80006eb8]:sw t5, 1120(ra)
	-[0x80006ebc]:sw t6, 1128(ra)
Current Store : [0x80006ebc] : sw t6, 1128(ra) -- Store: [0x80012b30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006eb0]:fadd.d t5, t3, s10, dyn
	-[0x80006eb4]:csrrs a7, fcsr, zero
	-[0x80006eb8]:sw t5, 1120(ra)
	-[0x80006ebc]:sw t6, 1128(ra)
	-[0x80006ec0]:sw t5, 1136(ra)
Current Store : [0x80006ec0] : sw t5, 1136(ra) -- Store: [0x80012b38]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006ef4]:fadd.d t5, t3, s10, dyn
	-[0x80006ef8]:csrrs a7, fcsr, zero
	-[0x80006efc]:sw t5, 1152(ra)
	-[0x80006f00]:sw t6, 1160(ra)
Current Store : [0x80006f00] : sw t6, 1160(ra) -- Store: [0x80012b50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006ef4]:fadd.d t5, t3, s10, dyn
	-[0x80006ef8]:csrrs a7, fcsr, zero
	-[0x80006efc]:sw t5, 1152(ra)
	-[0x80006f00]:sw t6, 1160(ra)
	-[0x80006f04]:sw t5, 1168(ra)
Current Store : [0x80006f04] : sw t5, 1168(ra) -- Store: [0x80012b58]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006f38]:fadd.d t5, t3, s10, dyn
	-[0x80006f3c]:csrrs a7, fcsr, zero
	-[0x80006f40]:sw t5, 1184(ra)
	-[0x80006f44]:sw t6, 1192(ra)
Current Store : [0x80006f44] : sw t6, 1192(ra) -- Store: [0x80012b70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006f38]:fadd.d t5, t3, s10, dyn
	-[0x80006f3c]:csrrs a7, fcsr, zero
	-[0x80006f40]:sw t5, 1184(ra)
	-[0x80006f44]:sw t6, 1192(ra)
	-[0x80006f48]:sw t5, 1200(ra)
Current Store : [0x80006f48] : sw t5, 1200(ra) -- Store: [0x80012b78]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006f7c]:fadd.d t5, t3, s10, dyn
	-[0x80006f80]:csrrs a7, fcsr, zero
	-[0x80006f84]:sw t5, 1216(ra)
	-[0x80006f88]:sw t6, 1224(ra)
Current Store : [0x80006f88] : sw t6, 1224(ra) -- Store: [0x80012b90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006f7c]:fadd.d t5, t3, s10, dyn
	-[0x80006f80]:csrrs a7, fcsr, zero
	-[0x80006f84]:sw t5, 1216(ra)
	-[0x80006f88]:sw t6, 1224(ra)
	-[0x80006f8c]:sw t5, 1232(ra)
Current Store : [0x80006f8c] : sw t5, 1232(ra) -- Store: [0x80012b98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006fc0]:fadd.d t5, t3, s10, dyn
	-[0x80006fc4]:csrrs a7, fcsr, zero
	-[0x80006fc8]:sw t5, 1248(ra)
	-[0x80006fcc]:sw t6, 1256(ra)
Current Store : [0x80006fcc] : sw t6, 1256(ra) -- Store: [0x80012bb0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80006fc0]:fadd.d t5, t3, s10, dyn
	-[0x80006fc4]:csrrs a7, fcsr, zero
	-[0x80006fc8]:sw t5, 1248(ra)
	-[0x80006fcc]:sw t6, 1256(ra)
	-[0x80006fd0]:sw t5, 1264(ra)
Current Store : [0x80006fd0] : sw t5, 1264(ra) -- Store: [0x80012bb8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007004]:fadd.d t5, t3, s10, dyn
	-[0x80007008]:csrrs a7, fcsr, zero
	-[0x8000700c]:sw t5, 1280(ra)
	-[0x80007010]:sw t6, 1288(ra)
Current Store : [0x80007010] : sw t6, 1288(ra) -- Store: [0x80012bd0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007004]:fadd.d t5, t3, s10, dyn
	-[0x80007008]:csrrs a7, fcsr, zero
	-[0x8000700c]:sw t5, 1280(ra)
	-[0x80007010]:sw t6, 1288(ra)
	-[0x80007014]:sw t5, 1296(ra)
Current Store : [0x80007014] : sw t5, 1296(ra) -- Store: [0x80012bd8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007048]:fadd.d t5, t3, s10, dyn
	-[0x8000704c]:csrrs a7, fcsr, zero
	-[0x80007050]:sw t5, 1312(ra)
	-[0x80007054]:sw t6, 1320(ra)
Current Store : [0x80007054] : sw t6, 1320(ra) -- Store: [0x80012bf0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007048]:fadd.d t5, t3, s10, dyn
	-[0x8000704c]:csrrs a7, fcsr, zero
	-[0x80007050]:sw t5, 1312(ra)
	-[0x80007054]:sw t6, 1320(ra)
	-[0x80007058]:sw t5, 1328(ra)
Current Store : [0x80007058] : sw t5, 1328(ra) -- Store: [0x80012bf8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000708c]:fadd.d t5, t3, s10, dyn
	-[0x80007090]:csrrs a7, fcsr, zero
	-[0x80007094]:sw t5, 1344(ra)
	-[0x80007098]:sw t6, 1352(ra)
Current Store : [0x80007098] : sw t6, 1352(ra) -- Store: [0x80012c10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000708c]:fadd.d t5, t3, s10, dyn
	-[0x80007090]:csrrs a7, fcsr, zero
	-[0x80007094]:sw t5, 1344(ra)
	-[0x80007098]:sw t6, 1352(ra)
	-[0x8000709c]:sw t5, 1360(ra)
Current Store : [0x8000709c] : sw t5, 1360(ra) -- Store: [0x80012c18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800070d0]:fadd.d t5, t3, s10, dyn
	-[0x800070d4]:csrrs a7, fcsr, zero
	-[0x800070d8]:sw t5, 1376(ra)
	-[0x800070dc]:sw t6, 1384(ra)
Current Store : [0x800070dc] : sw t6, 1384(ra) -- Store: [0x80012c30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800070d0]:fadd.d t5, t3, s10, dyn
	-[0x800070d4]:csrrs a7, fcsr, zero
	-[0x800070d8]:sw t5, 1376(ra)
	-[0x800070dc]:sw t6, 1384(ra)
	-[0x800070e0]:sw t5, 1392(ra)
Current Store : [0x800070e0] : sw t5, 1392(ra) -- Store: [0x80012c38]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007114]:fadd.d t5, t3, s10, dyn
	-[0x80007118]:csrrs a7, fcsr, zero
	-[0x8000711c]:sw t5, 1408(ra)
	-[0x80007120]:sw t6, 1416(ra)
Current Store : [0x80007120] : sw t6, 1416(ra) -- Store: [0x80012c50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007114]:fadd.d t5, t3, s10, dyn
	-[0x80007118]:csrrs a7, fcsr, zero
	-[0x8000711c]:sw t5, 1408(ra)
	-[0x80007120]:sw t6, 1416(ra)
	-[0x80007124]:sw t5, 1424(ra)
Current Store : [0x80007124] : sw t5, 1424(ra) -- Store: [0x80012c58]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007158]:fadd.d t5, t3, s10, dyn
	-[0x8000715c]:csrrs a7, fcsr, zero
	-[0x80007160]:sw t5, 1440(ra)
	-[0x80007164]:sw t6, 1448(ra)
Current Store : [0x80007164] : sw t6, 1448(ra) -- Store: [0x80012c70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007158]:fadd.d t5, t3, s10, dyn
	-[0x8000715c]:csrrs a7, fcsr, zero
	-[0x80007160]:sw t5, 1440(ra)
	-[0x80007164]:sw t6, 1448(ra)
	-[0x80007168]:sw t5, 1456(ra)
Current Store : [0x80007168] : sw t5, 1456(ra) -- Store: [0x80012c78]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000719c]:fadd.d t5, t3, s10, dyn
	-[0x800071a0]:csrrs a7, fcsr, zero
	-[0x800071a4]:sw t5, 1472(ra)
	-[0x800071a8]:sw t6, 1480(ra)
Current Store : [0x800071a8] : sw t6, 1480(ra) -- Store: [0x80012c90]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000719c]:fadd.d t5, t3, s10, dyn
	-[0x800071a0]:csrrs a7, fcsr, zero
	-[0x800071a4]:sw t5, 1472(ra)
	-[0x800071a8]:sw t6, 1480(ra)
	-[0x800071ac]:sw t5, 1488(ra)
Current Store : [0x800071ac] : sw t5, 1488(ra) -- Store: [0x80012c98]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800071e0]:fadd.d t5, t3, s10, dyn
	-[0x800071e4]:csrrs a7, fcsr, zero
	-[0x800071e8]:sw t5, 1504(ra)
	-[0x800071ec]:sw t6, 1512(ra)
Current Store : [0x800071ec] : sw t6, 1512(ra) -- Store: [0x80012cb0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800071e0]:fadd.d t5, t3, s10, dyn
	-[0x800071e4]:csrrs a7, fcsr, zero
	-[0x800071e8]:sw t5, 1504(ra)
	-[0x800071ec]:sw t6, 1512(ra)
	-[0x800071f0]:sw t5, 1520(ra)
Current Store : [0x800071f0] : sw t5, 1520(ra) -- Store: [0x80012cb8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007224]:fadd.d t5, t3, s10, dyn
	-[0x80007228]:csrrs a7, fcsr, zero
	-[0x8000722c]:sw t5, 1536(ra)
	-[0x80007230]:sw t6, 1544(ra)
Current Store : [0x80007230] : sw t6, 1544(ra) -- Store: [0x80012cd0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007224]:fadd.d t5, t3, s10, dyn
	-[0x80007228]:csrrs a7, fcsr, zero
	-[0x8000722c]:sw t5, 1536(ra)
	-[0x80007230]:sw t6, 1544(ra)
	-[0x80007234]:sw t5, 1552(ra)
Current Store : [0x80007234] : sw t5, 1552(ra) -- Store: [0x80012cd8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007268]:fadd.d t5, t3, s10, dyn
	-[0x8000726c]:csrrs a7, fcsr, zero
	-[0x80007270]:sw t5, 1568(ra)
	-[0x80007274]:sw t6, 1576(ra)
Current Store : [0x80007274] : sw t6, 1576(ra) -- Store: [0x80012cf0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007268]:fadd.d t5, t3, s10, dyn
	-[0x8000726c]:csrrs a7, fcsr, zero
	-[0x80007270]:sw t5, 1568(ra)
	-[0x80007274]:sw t6, 1576(ra)
	-[0x80007278]:sw t5, 1584(ra)
Current Store : [0x80007278] : sw t5, 1584(ra) -- Store: [0x80012cf8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800072ac]:fadd.d t5, t3, s10, dyn
	-[0x800072b0]:csrrs a7, fcsr, zero
	-[0x800072b4]:sw t5, 1600(ra)
	-[0x800072b8]:sw t6, 1608(ra)
Current Store : [0x800072b8] : sw t6, 1608(ra) -- Store: [0x80012d10]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800072ac]:fadd.d t5, t3, s10, dyn
	-[0x800072b0]:csrrs a7, fcsr, zero
	-[0x800072b4]:sw t5, 1600(ra)
	-[0x800072b8]:sw t6, 1608(ra)
	-[0x800072bc]:sw t5, 1616(ra)
Current Store : [0x800072bc] : sw t5, 1616(ra) -- Store: [0x80012d18]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800072f4]:fadd.d t5, t3, s10, dyn
	-[0x800072f8]:csrrs a7, fcsr, zero
	-[0x800072fc]:sw t5, 1632(ra)
	-[0x80007300]:sw t6, 1640(ra)
Current Store : [0x80007300] : sw t6, 1640(ra) -- Store: [0x80012d30]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800072f4]:fadd.d t5, t3, s10, dyn
	-[0x800072f8]:csrrs a7, fcsr, zero
	-[0x800072fc]:sw t5, 1632(ra)
	-[0x80007300]:sw t6, 1640(ra)
	-[0x80007304]:sw t5, 1648(ra)
Current Store : [0x80007304] : sw t5, 1648(ra) -- Store: [0x80012d38]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000733c]:fadd.d t5, t3, s10, dyn
	-[0x80007340]:csrrs a7, fcsr, zero
	-[0x80007344]:sw t5, 1664(ra)
	-[0x80007348]:sw t6, 1672(ra)
Current Store : [0x80007348] : sw t6, 1672(ra) -- Store: [0x80012d50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000733c]:fadd.d t5, t3, s10, dyn
	-[0x80007340]:csrrs a7, fcsr, zero
	-[0x80007344]:sw t5, 1664(ra)
	-[0x80007348]:sw t6, 1672(ra)
	-[0x8000734c]:sw t5, 1680(ra)
Current Store : [0x8000734c] : sw t5, 1680(ra) -- Store: [0x80012d58]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007380]:fadd.d t5, t3, s10, dyn
	-[0x80007384]:csrrs a7, fcsr, zero
	-[0x80007388]:sw t5, 1696(ra)
	-[0x8000738c]:sw t6, 1704(ra)
Current Store : [0x8000738c] : sw t6, 1704(ra) -- Store: [0x80012d70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007380]:fadd.d t5, t3, s10, dyn
	-[0x80007384]:csrrs a7, fcsr, zero
	-[0x80007388]:sw t5, 1696(ra)
	-[0x8000738c]:sw t6, 1704(ra)
	-[0x80007390]:sw t5, 1712(ra)
Current Store : [0x80007390] : sw t5, 1712(ra) -- Store: [0x80012d78]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800073c4]:fadd.d t5, t3, s10, dyn
	-[0x800073c8]:csrrs a7, fcsr, zero
	-[0x800073cc]:sw t5, 1728(ra)
	-[0x800073d0]:sw t6, 1736(ra)
Current Store : [0x800073d0] : sw t6, 1736(ra) -- Store: [0x80012d90]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800073c4]:fadd.d t5, t3, s10, dyn
	-[0x800073c8]:csrrs a7, fcsr, zero
	-[0x800073cc]:sw t5, 1728(ra)
	-[0x800073d0]:sw t6, 1736(ra)
	-[0x800073d4]:sw t5, 1744(ra)
Current Store : [0x800073d4] : sw t5, 1744(ra) -- Store: [0x80012d98]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007408]:fadd.d t5, t3, s10, dyn
	-[0x8000740c]:csrrs a7, fcsr, zero
	-[0x80007410]:sw t5, 1760(ra)
	-[0x80007414]:sw t6, 1768(ra)
Current Store : [0x80007414] : sw t6, 1768(ra) -- Store: [0x80012db0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007408]:fadd.d t5, t3, s10, dyn
	-[0x8000740c]:csrrs a7, fcsr, zero
	-[0x80007410]:sw t5, 1760(ra)
	-[0x80007414]:sw t6, 1768(ra)
	-[0x80007418]:sw t5, 1776(ra)
Current Store : [0x80007418] : sw t5, 1776(ra) -- Store: [0x80012db8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000744c]:fadd.d t5, t3, s10, dyn
	-[0x80007450]:csrrs a7, fcsr, zero
	-[0x80007454]:sw t5, 1792(ra)
	-[0x80007458]:sw t6, 1800(ra)
Current Store : [0x80007458] : sw t6, 1800(ra) -- Store: [0x80012dd0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000744c]:fadd.d t5, t3, s10, dyn
	-[0x80007450]:csrrs a7, fcsr, zero
	-[0x80007454]:sw t5, 1792(ra)
	-[0x80007458]:sw t6, 1800(ra)
	-[0x8000745c]:sw t5, 1808(ra)
Current Store : [0x8000745c] : sw t5, 1808(ra) -- Store: [0x80012dd8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007494]:fadd.d t5, t3, s10, dyn
	-[0x80007498]:csrrs a7, fcsr, zero
	-[0x8000749c]:sw t5, 1824(ra)
	-[0x800074a0]:sw t6, 1832(ra)
Current Store : [0x800074a0] : sw t6, 1832(ra) -- Store: [0x80012df0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007494]:fadd.d t5, t3, s10, dyn
	-[0x80007498]:csrrs a7, fcsr, zero
	-[0x8000749c]:sw t5, 1824(ra)
	-[0x800074a0]:sw t6, 1832(ra)
	-[0x800074a4]:sw t5, 1840(ra)
Current Store : [0x800074a4] : sw t5, 1840(ra) -- Store: [0x80012df8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800074dc]:fadd.d t5, t3, s10, dyn
	-[0x800074e0]:csrrs a7, fcsr, zero
	-[0x800074e4]:sw t5, 1856(ra)
	-[0x800074e8]:sw t6, 1864(ra)
Current Store : [0x800074e8] : sw t6, 1864(ra) -- Store: [0x80012e10]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800074dc]:fadd.d t5, t3, s10, dyn
	-[0x800074e0]:csrrs a7, fcsr, zero
	-[0x800074e4]:sw t5, 1856(ra)
	-[0x800074e8]:sw t6, 1864(ra)
	-[0x800074ec]:sw t5, 1872(ra)
Current Store : [0x800074ec] : sw t5, 1872(ra) -- Store: [0x80012e18]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007520]:fadd.d t5, t3, s10, dyn
	-[0x80007524]:csrrs a7, fcsr, zero
	-[0x80007528]:sw t5, 1888(ra)
	-[0x8000752c]:sw t6, 1896(ra)
Current Store : [0x8000752c] : sw t6, 1896(ra) -- Store: [0x80012e30]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007520]:fadd.d t5, t3, s10, dyn
	-[0x80007524]:csrrs a7, fcsr, zero
	-[0x80007528]:sw t5, 1888(ra)
	-[0x8000752c]:sw t6, 1896(ra)
	-[0x80007530]:sw t5, 1904(ra)
Current Store : [0x80007530] : sw t5, 1904(ra) -- Store: [0x80012e38]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007564]:fadd.d t5, t3, s10, dyn
	-[0x80007568]:csrrs a7, fcsr, zero
	-[0x8000756c]:sw t5, 1920(ra)
	-[0x80007570]:sw t6, 1928(ra)
Current Store : [0x80007570] : sw t6, 1928(ra) -- Store: [0x80012e50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007564]:fadd.d t5, t3, s10, dyn
	-[0x80007568]:csrrs a7, fcsr, zero
	-[0x8000756c]:sw t5, 1920(ra)
	-[0x80007570]:sw t6, 1928(ra)
	-[0x80007574]:sw t5, 1936(ra)
Current Store : [0x80007574] : sw t5, 1936(ra) -- Store: [0x80012e58]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800075a8]:fadd.d t5, t3, s10, dyn
	-[0x800075ac]:csrrs a7, fcsr, zero
	-[0x800075b0]:sw t5, 1952(ra)
	-[0x800075b4]:sw t6, 1960(ra)
Current Store : [0x800075b4] : sw t6, 1960(ra) -- Store: [0x80012e70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800075a8]:fadd.d t5, t3, s10, dyn
	-[0x800075ac]:csrrs a7, fcsr, zero
	-[0x800075b0]:sw t5, 1952(ra)
	-[0x800075b4]:sw t6, 1960(ra)
	-[0x800075b8]:sw t5, 1968(ra)
Current Store : [0x800075b8] : sw t5, 1968(ra) -- Store: [0x80012e78]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800075ec]:fadd.d t5, t3, s10, dyn
	-[0x800075f0]:csrrs a7, fcsr, zero
	-[0x800075f4]:sw t5, 1984(ra)
	-[0x800075f8]:sw t6, 1992(ra)
Current Store : [0x800075f8] : sw t6, 1992(ra) -- Store: [0x80012e90]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800075ec]:fadd.d t5, t3, s10, dyn
	-[0x800075f0]:csrrs a7, fcsr, zero
	-[0x800075f4]:sw t5, 1984(ra)
	-[0x800075f8]:sw t6, 1992(ra)
	-[0x800075fc]:sw t5, 2000(ra)
Current Store : [0x800075fc] : sw t5, 2000(ra) -- Store: [0x80012e98]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007630]:fadd.d t5, t3, s10, dyn
	-[0x80007634]:csrrs a7, fcsr, zero
	-[0x80007638]:sw t5, 2016(ra)
	-[0x8000763c]:sw t6, 2024(ra)
Current Store : [0x8000763c] : sw t6, 2024(ra) -- Store: [0x80012eb0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007630]:fadd.d t5, t3, s10, dyn
	-[0x80007634]:csrrs a7, fcsr, zero
	-[0x80007638]:sw t5, 2016(ra)
	-[0x8000763c]:sw t6, 2024(ra)
	-[0x80007640]:sw t5, 2032(ra)
Current Store : [0x80007640] : sw t5, 2032(ra) -- Store: [0x80012eb8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007674]:fadd.d t5, t3, s10, dyn
	-[0x80007678]:csrrs a7, fcsr, zero
	-[0x8000767c]:addi ra, ra, 2040
	-[0x80007680]:sw t5, 8(ra)
	-[0x80007684]:sw t6, 16(ra)
Current Store : [0x80007684] : sw t6, 16(ra) -- Store: [0x80012ed0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007674]:fadd.d t5, t3, s10, dyn
	-[0x80007678]:csrrs a7, fcsr, zero
	-[0x8000767c]:addi ra, ra, 2040
	-[0x80007680]:sw t5, 8(ra)
	-[0x80007684]:sw t6, 16(ra)
	-[0x80007688]:sw t5, 24(ra)
Current Store : [0x80007688] : sw t5, 24(ra) -- Store: [0x80012ed8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800076bc]:fadd.d t5, t3, s10, dyn
	-[0x800076c0]:csrrs a7, fcsr, zero
	-[0x800076c4]:sw t5, 40(ra)
	-[0x800076c8]:sw t6, 48(ra)
Current Store : [0x800076c8] : sw t6, 48(ra) -- Store: [0x80012ef0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800076bc]:fadd.d t5, t3, s10, dyn
	-[0x800076c0]:csrrs a7, fcsr, zero
	-[0x800076c4]:sw t5, 40(ra)
	-[0x800076c8]:sw t6, 48(ra)
	-[0x800076cc]:sw t5, 56(ra)
Current Store : [0x800076cc] : sw t5, 56(ra) -- Store: [0x80012ef8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007700]:fadd.d t5, t3, s10, dyn
	-[0x80007704]:csrrs a7, fcsr, zero
	-[0x80007708]:sw t5, 72(ra)
	-[0x8000770c]:sw t6, 80(ra)
Current Store : [0x8000770c] : sw t6, 80(ra) -- Store: [0x80012f10]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007700]:fadd.d t5, t3, s10, dyn
	-[0x80007704]:csrrs a7, fcsr, zero
	-[0x80007708]:sw t5, 72(ra)
	-[0x8000770c]:sw t6, 80(ra)
	-[0x80007710]:sw t5, 88(ra)
Current Store : [0x80007710] : sw t5, 88(ra) -- Store: [0x80012f18]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007744]:fadd.d t5, t3, s10, dyn
	-[0x80007748]:csrrs a7, fcsr, zero
	-[0x8000774c]:sw t5, 104(ra)
	-[0x80007750]:sw t6, 112(ra)
Current Store : [0x80007750] : sw t6, 112(ra) -- Store: [0x80012f30]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007744]:fadd.d t5, t3, s10, dyn
	-[0x80007748]:csrrs a7, fcsr, zero
	-[0x8000774c]:sw t5, 104(ra)
	-[0x80007750]:sw t6, 112(ra)
	-[0x80007754]:sw t5, 120(ra)
Current Store : [0x80007754] : sw t5, 120(ra) -- Store: [0x80012f38]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007788]:fadd.d t5, t3, s10, dyn
	-[0x8000778c]:csrrs a7, fcsr, zero
	-[0x80007790]:sw t5, 136(ra)
	-[0x80007794]:sw t6, 144(ra)
Current Store : [0x80007794] : sw t6, 144(ra) -- Store: [0x80012f50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007788]:fadd.d t5, t3, s10, dyn
	-[0x8000778c]:csrrs a7, fcsr, zero
	-[0x80007790]:sw t5, 136(ra)
	-[0x80007794]:sw t6, 144(ra)
	-[0x80007798]:sw t5, 152(ra)
Current Store : [0x80007798] : sw t5, 152(ra) -- Store: [0x80012f58]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800077c8]:fadd.d t5, t3, s10, dyn
	-[0x800077cc]:csrrs a7, fcsr, zero
	-[0x800077d0]:sw t5, 168(ra)
	-[0x800077d4]:sw t6, 176(ra)
Current Store : [0x800077d4] : sw t6, 176(ra) -- Store: [0x80012f70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800077c8]:fadd.d t5, t3, s10, dyn
	-[0x800077cc]:csrrs a7, fcsr, zero
	-[0x800077d0]:sw t5, 168(ra)
	-[0x800077d4]:sw t6, 176(ra)
	-[0x800077d8]:sw t5, 184(ra)
Current Store : [0x800077d8] : sw t5, 184(ra) -- Store: [0x80012f78]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007808]:fadd.d t5, t3, s10, dyn
	-[0x8000780c]:csrrs a7, fcsr, zero
	-[0x80007810]:sw t5, 200(ra)
	-[0x80007814]:sw t6, 208(ra)
Current Store : [0x80007814] : sw t6, 208(ra) -- Store: [0x80012f90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007808]:fadd.d t5, t3, s10, dyn
	-[0x8000780c]:csrrs a7, fcsr, zero
	-[0x80007810]:sw t5, 200(ra)
	-[0x80007814]:sw t6, 208(ra)
	-[0x80007818]:sw t5, 216(ra)
Current Store : [0x80007818] : sw t5, 216(ra) -- Store: [0x80012f98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007848]:fadd.d t5, t3, s10, dyn
	-[0x8000784c]:csrrs a7, fcsr, zero
	-[0x80007850]:sw t5, 232(ra)
	-[0x80007854]:sw t6, 240(ra)
Current Store : [0x80007854] : sw t6, 240(ra) -- Store: [0x80012fb0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007848]:fadd.d t5, t3, s10, dyn
	-[0x8000784c]:csrrs a7, fcsr, zero
	-[0x80007850]:sw t5, 232(ra)
	-[0x80007854]:sw t6, 240(ra)
	-[0x80007858]:sw t5, 248(ra)
Current Store : [0x80007858] : sw t5, 248(ra) -- Store: [0x80012fb8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007888]:fadd.d t5, t3, s10, dyn
	-[0x8000788c]:csrrs a7, fcsr, zero
	-[0x80007890]:sw t5, 264(ra)
	-[0x80007894]:sw t6, 272(ra)
Current Store : [0x80007894] : sw t6, 272(ra) -- Store: [0x80012fd0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007888]:fadd.d t5, t3, s10, dyn
	-[0x8000788c]:csrrs a7, fcsr, zero
	-[0x80007890]:sw t5, 264(ra)
	-[0x80007894]:sw t6, 272(ra)
	-[0x80007898]:sw t5, 280(ra)
Current Store : [0x80007898] : sw t5, 280(ra) -- Store: [0x80012fd8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800078c8]:fadd.d t5, t3, s10, dyn
	-[0x800078cc]:csrrs a7, fcsr, zero
	-[0x800078d0]:sw t5, 296(ra)
	-[0x800078d4]:sw t6, 304(ra)
Current Store : [0x800078d4] : sw t6, 304(ra) -- Store: [0x80012ff0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800078c8]:fadd.d t5, t3, s10, dyn
	-[0x800078cc]:csrrs a7, fcsr, zero
	-[0x800078d0]:sw t5, 296(ra)
	-[0x800078d4]:sw t6, 304(ra)
	-[0x800078d8]:sw t5, 312(ra)
Current Store : [0x800078d8] : sw t5, 312(ra) -- Store: [0x80012ff8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007908]:fadd.d t5, t3, s10, dyn
	-[0x8000790c]:csrrs a7, fcsr, zero
	-[0x80007910]:sw t5, 328(ra)
	-[0x80007914]:sw t6, 336(ra)
Current Store : [0x80007914] : sw t6, 336(ra) -- Store: [0x80013010]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007908]:fadd.d t5, t3, s10, dyn
	-[0x8000790c]:csrrs a7, fcsr, zero
	-[0x80007910]:sw t5, 328(ra)
	-[0x80007914]:sw t6, 336(ra)
	-[0x80007918]:sw t5, 344(ra)
Current Store : [0x80007918] : sw t5, 344(ra) -- Store: [0x80013018]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000794c]:fadd.d t5, t3, s10, dyn
	-[0x80007950]:csrrs a7, fcsr, zero
	-[0x80007954]:sw t5, 360(ra)
	-[0x80007958]:sw t6, 368(ra)
Current Store : [0x80007958] : sw t6, 368(ra) -- Store: [0x80013030]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000794c]:fadd.d t5, t3, s10, dyn
	-[0x80007950]:csrrs a7, fcsr, zero
	-[0x80007954]:sw t5, 360(ra)
	-[0x80007958]:sw t6, 368(ra)
	-[0x8000795c]:sw t5, 376(ra)
Current Store : [0x8000795c] : sw t5, 376(ra) -- Store: [0x80013038]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007990]:fadd.d t5, t3, s10, dyn
	-[0x80007994]:csrrs a7, fcsr, zero
	-[0x80007998]:sw t5, 392(ra)
	-[0x8000799c]:sw t6, 400(ra)
Current Store : [0x8000799c] : sw t6, 400(ra) -- Store: [0x80013050]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007990]:fadd.d t5, t3, s10, dyn
	-[0x80007994]:csrrs a7, fcsr, zero
	-[0x80007998]:sw t5, 392(ra)
	-[0x8000799c]:sw t6, 400(ra)
	-[0x800079a0]:sw t5, 408(ra)
Current Store : [0x800079a0] : sw t5, 408(ra) -- Store: [0x80013058]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800079d0]:fadd.d t5, t3, s10, dyn
	-[0x800079d4]:csrrs a7, fcsr, zero
	-[0x800079d8]:sw t5, 424(ra)
	-[0x800079dc]:sw t6, 432(ra)
Current Store : [0x800079dc] : sw t6, 432(ra) -- Store: [0x80013070]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800079d0]:fadd.d t5, t3, s10, dyn
	-[0x800079d4]:csrrs a7, fcsr, zero
	-[0x800079d8]:sw t5, 424(ra)
	-[0x800079dc]:sw t6, 432(ra)
	-[0x800079e0]:sw t5, 440(ra)
Current Store : [0x800079e0] : sw t5, 440(ra) -- Store: [0x80013078]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007a10]:fadd.d t5, t3, s10, dyn
	-[0x80007a14]:csrrs a7, fcsr, zero
	-[0x80007a18]:sw t5, 456(ra)
	-[0x80007a1c]:sw t6, 464(ra)
Current Store : [0x80007a1c] : sw t6, 464(ra) -- Store: [0x80013090]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007a10]:fadd.d t5, t3, s10, dyn
	-[0x80007a14]:csrrs a7, fcsr, zero
	-[0x80007a18]:sw t5, 456(ra)
	-[0x80007a1c]:sw t6, 464(ra)
	-[0x80007a20]:sw t5, 472(ra)
Current Store : [0x80007a20] : sw t5, 472(ra) -- Store: [0x80013098]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007a50]:fadd.d t5, t3, s10, dyn
	-[0x80007a54]:csrrs a7, fcsr, zero
	-[0x80007a58]:sw t5, 488(ra)
	-[0x80007a5c]:sw t6, 496(ra)
Current Store : [0x80007a5c] : sw t6, 496(ra) -- Store: [0x800130b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007a50]:fadd.d t5, t3, s10, dyn
	-[0x80007a54]:csrrs a7, fcsr, zero
	-[0x80007a58]:sw t5, 488(ra)
	-[0x80007a5c]:sw t6, 496(ra)
	-[0x80007a60]:sw t5, 504(ra)
Current Store : [0x80007a60] : sw t5, 504(ra) -- Store: [0x800130b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007a90]:fadd.d t5, t3, s10, dyn
	-[0x80007a94]:csrrs a7, fcsr, zero
	-[0x80007a98]:sw t5, 520(ra)
	-[0x80007a9c]:sw t6, 528(ra)
Current Store : [0x80007a9c] : sw t6, 528(ra) -- Store: [0x800130d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007a90]:fadd.d t5, t3, s10, dyn
	-[0x80007a94]:csrrs a7, fcsr, zero
	-[0x80007a98]:sw t5, 520(ra)
	-[0x80007a9c]:sw t6, 528(ra)
	-[0x80007aa0]:sw t5, 536(ra)
Current Store : [0x80007aa0] : sw t5, 536(ra) -- Store: [0x800130d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007ad4]:fadd.d t5, t3, s10, dyn
	-[0x80007ad8]:csrrs a7, fcsr, zero
	-[0x80007adc]:sw t5, 552(ra)
	-[0x80007ae0]:sw t6, 560(ra)
Current Store : [0x80007ae0] : sw t6, 560(ra) -- Store: [0x800130f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007ad4]:fadd.d t5, t3, s10, dyn
	-[0x80007ad8]:csrrs a7, fcsr, zero
	-[0x80007adc]:sw t5, 552(ra)
	-[0x80007ae0]:sw t6, 560(ra)
	-[0x80007ae4]:sw t5, 568(ra)
Current Store : [0x80007ae4] : sw t5, 568(ra) -- Store: [0x800130f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007b18]:fadd.d t5, t3, s10, dyn
	-[0x80007b1c]:csrrs a7, fcsr, zero
	-[0x80007b20]:sw t5, 584(ra)
	-[0x80007b24]:sw t6, 592(ra)
Current Store : [0x80007b24] : sw t6, 592(ra) -- Store: [0x80013110]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007b18]:fadd.d t5, t3, s10, dyn
	-[0x80007b1c]:csrrs a7, fcsr, zero
	-[0x80007b20]:sw t5, 584(ra)
	-[0x80007b24]:sw t6, 592(ra)
	-[0x80007b28]:sw t5, 600(ra)
Current Store : [0x80007b28] : sw t5, 600(ra) -- Store: [0x80013118]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007b58]:fadd.d t5, t3, s10, dyn
	-[0x80007b5c]:csrrs a7, fcsr, zero
	-[0x80007b60]:sw t5, 616(ra)
	-[0x80007b64]:sw t6, 624(ra)
Current Store : [0x80007b64] : sw t6, 624(ra) -- Store: [0x80013130]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007b58]:fadd.d t5, t3, s10, dyn
	-[0x80007b5c]:csrrs a7, fcsr, zero
	-[0x80007b60]:sw t5, 616(ra)
	-[0x80007b64]:sw t6, 624(ra)
	-[0x80007b68]:sw t5, 632(ra)
Current Store : [0x80007b68] : sw t5, 632(ra) -- Store: [0x80013138]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007b98]:fadd.d t5, t3, s10, dyn
	-[0x80007b9c]:csrrs a7, fcsr, zero
	-[0x80007ba0]:sw t5, 648(ra)
	-[0x80007ba4]:sw t6, 656(ra)
Current Store : [0x80007ba4] : sw t6, 656(ra) -- Store: [0x80013150]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007b98]:fadd.d t5, t3, s10, dyn
	-[0x80007b9c]:csrrs a7, fcsr, zero
	-[0x80007ba0]:sw t5, 648(ra)
	-[0x80007ba4]:sw t6, 656(ra)
	-[0x80007ba8]:sw t5, 664(ra)
Current Store : [0x80007ba8] : sw t5, 664(ra) -- Store: [0x80013158]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007bd8]:fadd.d t5, t3, s10, dyn
	-[0x80007bdc]:csrrs a7, fcsr, zero
	-[0x80007be0]:sw t5, 680(ra)
	-[0x80007be4]:sw t6, 688(ra)
Current Store : [0x80007be4] : sw t6, 688(ra) -- Store: [0x80013170]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007bd8]:fadd.d t5, t3, s10, dyn
	-[0x80007bdc]:csrrs a7, fcsr, zero
	-[0x80007be0]:sw t5, 680(ra)
	-[0x80007be4]:sw t6, 688(ra)
	-[0x80007be8]:sw t5, 696(ra)
Current Store : [0x80007be8] : sw t5, 696(ra) -- Store: [0x80013178]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007c18]:fadd.d t5, t3, s10, dyn
	-[0x80007c1c]:csrrs a7, fcsr, zero
	-[0x80007c20]:sw t5, 712(ra)
	-[0x80007c24]:sw t6, 720(ra)
Current Store : [0x80007c24] : sw t6, 720(ra) -- Store: [0x80013190]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007c18]:fadd.d t5, t3, s10, dyn
	-[0x80007c1c]:csrrs a7, fcsr, zero
	-[0x80007c20]:sw t5, 712(ra)
	-[0x80007c24]:sw t6, 720(ra)
	-[0x80007c28]:sw t5, 728(ra)
Current Store : [0x80007c28] : sw t5, 728(ra) -- Store: [0x80013198]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007c58]:fadd.d t5, t3, s10, dyn
	-[0x80007c5c]:csrrs a7, fcsr, zero
	-[0x80007c60]:sw t5, 744(ra)
	-[0x80007c64]:sw t6, 752(ra)
Current Store : [0x80007c64] : sw t6, 752(ra) -- Store: [0x800131b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007c58]:fadd.d t5, t3, s10, dyn
	-[0x80007c5c]:csrrs a7, fcsr, zero
	-[0x80007c60]:sw t5, 744(ra)
	-[0x80007c64]:sw t6, 752(ra)
	-[0x80007c68]:sw t5, 760(ra)
Current Store : [0x80007c68] : sw t5, 760(ra) -- Store: [0x800131b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007c98]:fadd.d t5, t3, s10, dyn
	-[0x80007c9c]:csrrs a7, fcsr, zero
	-[0x80007ca0]:sw t5, 776(ra)
	-[0x80007ca4]:sw t6, 784(ra)
Current Store : [0x80007ca4] : sw t6, 784(ra) -- Store: [0x800131d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007c98]:fadd.d t5, t3, s10, dyn
	-[0x80007c9c]:csrrs a7, fcsr, zero
	-[0x80007ca0]:sw t5, 776(ra)
	-[0x80007ca4]:sw t6, 784(ra)
	-[0x80007ca8]:sw t5, 792(ra)
Current Store : [0x80007ca8] : sw t5, 792(ra) -- Store: [0x800131d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007cd8]:fadd.d t5, t3, s10, dyn
	-[0x80007cdc]:csrrs a7, fcsr, zero
	-[0x80007ce0]:sw t5, 808(ra)
	-[0x80007ce4]:sw t6, 816(ra)
Current Store : [0x80007ce4] : sw t6, 816(ra) -- Store: [0x800131f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007cd8]:fadd.d t5, t3, s10, dyn
	-[0x80007cdc]:csrrs a7, fcsr, zero
	-[0x80007ce0]:sw t5, 808(ra)
	-[0x80007ce4]:sw t6, 816(ra)
	-[0x80007ce8]:sw t5, 824(ra)
Current Store : [0x80007ce8] : sw t5, 824(ra) -- Store: [0x800131f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007d18]:fadd.d t5, t3, s10, dyn
	-[0x80007d1c]:csrrs a7, fcsr, zero
	-[0x80007d20]:sw t5, 840(ra)
	-[0x80007d24]:sw t6, 848(ra)
Current Store : [0x80007d24] : sw t6, 848(ra) -- Store: [0x80013210]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007d18]:fadd.d t5, t3, s10, dyn
	-[0x80007d1c]:csrrs a7, fcsr, zero
	-[0x80007d20]:sw t5, 840(ra)
	-[0x80007d24]:sw t6, 848(ra)
	-[0x80007d28]:sw t5, 856(ra)
Current Store : [0x80007d28] : sw t5, 856(ra) -- Store: [0x80013218]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007d58]:fadd.d t5, t3, s10, dyn
	-[0x80007d5c]:csrrs a7, fcsr, zero
	-[0x80007d60]:sw t5, 872(ra)
	-[0x80007d64]:sw t6, 880(ra)
Current Store : [0x80007d64] : sw t6, 880(ra) -- Store: [0x80013230]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007d58]:fadd.d t5, t3, s10, dyn
	-[0x80007d5c]:csrrs a7, fcsr, zero
	-[0x80007d60]:sw t5, 872(ra)
	-[0x80007d64]:sw t6, 880(ra)
	-[0x80007d68]:sw t5, 888(ra)
Current Store : [0x80007d68] : sw t5, 888(ra) -- Store: [0x80013238]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007d98]:fadd.d t5, t3, s10, dyn
	-[0x80007d9c]:csrrs a7, fcsr, zero
	-[0x80007da0]:sw t5, 904(ra)
	-[0x80007da4]:sw t6, 912(ra)
Current Store : [0x80007da4] : sw t6, 912(ra) -- Store: [0x80013250]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007d98]:fadd.d t5, t3, s10, dyn
	-[0x80007d9c]:csrrs a7, fcsr, zero
	-[0x80007da0]:sw t5, 904(ra)
	-[0x80007da4]:sw t6, 912(ra)
	-[0x80007da8]:sw t5, 920(ra)
Current Store : [0x80007da8] : sw t5, 920(ra) -- Store: [0x80013258]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007dd8]:fadd.d t5, t3, s10, dyn
	-[0x80007ddc]:csrrs a7, fcsr, zero
	-[0x80007de0]:sw t5, 936(ra)
	-[0x80007de4]:sw t6, 944(ra)
Current Store : [0x80007de4] : sw t6, 944(ra) -- Store: [0x80013270]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007dd8]:fadd.d t5, t3, s10, dyn
	-[0x80007ddc]:csrrs a7, fcsr, zero
	-[0x80007de0]:sw t5, 936(ra)
	-[0x80007de4]:sw t6, 944(ra)
	-[0x80007de8]:sw t5, 952(ra)
Current Store : [0x80007de8] : sw t5, 952(ra) -- Store: [0x80013278]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007e18]:fadd.d t5, t3, s10, dyn
	-[0x80007e1c]:csrrs a7, fcsr, zero
	-[0x80007e20]:sw t5, 968(ra)
	-[0x80007e24]:sw t6, 976(ra)
Current Store : [0x80007e24] : sw t6, 976(ra) -- Store: [0x80013290]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007e18]:fadd.d t5, t3, s10, dyn
	-[0x80007e1c]:csrrs a7, fcsr, zero
	-[0x80007e20]:sw t5, 968(ra)
	-[0x80007e24]:sw t6, 976(ra)
	-[0x80007e28]:sw t5, 984(ra)
Current Store : [0x80007e28] : sw t5, 984(ra) -- Store: [0x80013298]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007e58]:fadd.d t5, t3, s10, dyn
	-[0x80007e5c]:csrrs a7, fcsr, zero
	-[0x80007e60]:sw t5, 1000(ra)
	-[0x80007e64]:sw t6, 1008(ra)
Current Store : [0x80007e64] : sw t6, 1008(ra) -- Store: [0x800132b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007e58]:fadd.d t5, t3, s10, dyn
	-[0x80007e5c]:csrrs a7, fcsr, zero
	-[0x80007e60]:sw t5, 1000(ra)
	-[0x80007e64]:sw t6, 1008(ra)
	-[0x80007e68]:sw t5, 1016(ra)
Current Store : [0x80007e68] : sw t5, 1016(ra) -- Store: [0x800132b8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007e98]:fadd.d t5, t3, s10, dyn
	-[0x80007e9c]:csrrs a7, fcsr, zero
	-[0x80007ea0]:sw t5, 1032(ra)
	-[0x80007ea4]:sw t6, 1040(ra)
Current Store : [0x80007ea4] : sw t6, 1040(ra) -- Store: [0x800132d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007e98]:fadd.d t5, t3, s10, dyn
	-[0x80007e9c]:csrrs a7, fcsr, zero
	-[0x80007ea0]:sw t5, 1032(ra)
	-[0x80007ea4]:sw t6, 1040(ra)
	-[0x80007ea8]:sw t5, 1048(ra)
Current Store : [0x80007ea8] : sw t5, 1048(ra) -- Store: [0x800132d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007ed8]:fadd.d t5, t3, s10, dyn
	-[0x80007edc]:csrrs a7, fcsr, zero
	-[0x80007ee0]:sw t5, 1064(ra)
	-[0x80007ee4]:sw t6, 1072(ra)
Current Store : [0x80007ee4] : sw t6, 1072(ra) -- Store: [0x800132f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007ed8]:fadd.d t5, t3, s10, dyn
	-[0x80007edc]:csrrs a7, fcsr, zero
	-[0x80007ee0]:sw t5, 1064(ra)
	-[0x80007ee4]:sw t6, 1072(ra)
	-[0x80007ee8]:sw t5, 1080(ra)
Current Store : [0x80007ee8] : sw t5, 1080(ra) -- Store: [0x800132f8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007f18]:fadd.d t5, t3, s10, dyn
	-[0x80007f1c]:csrrs a7, fcsr, zero
	-[0x80007f20]:sw t5, 1096(ra)
	-[0x80007f24]:sw t6, 1104(ra)
Current Store : [0x80007f24] : sw t6, 1104(ra) -- Store: [0x80013310]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007f18]:fadd.d t5, t3, s10, dyn
	-[0x80007f1c]:csrrs a7, fcsr, zero
	-[0x80007f20]:sw t5, 1096(ra)
	-[0x80007f24]:sw t6, 1104(ra)
	-[0x80007f28]:sw t5, 1112(ra)
Current Store : [0x80007f28] : sw t5, 1112(ra) -- Store: [0x80013318]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007f5c]:fadd.d t5, t3, s10, dyn
	-[0x80007f60]:csrrs a7, fcsr, zero
	-[0x80007f64]:sw t5, 1128(ra)
	-[0x80007f68]:sw t6, 1136(ra)
Current Store : [0x80007f68] : sw t6, 1136(ra) -- Store: [0x80013330]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007f5c]:fadd.d t5, t3, s10, dyn
	-[0x80007f60]:csrrs a7, fcsr, zero
	-[0x80007f64]:sw t5, 1128(ra)
	-[0x80007f68]:sw t6, 1136(ra)
	-[0x80007f6c]:sw t5, 1144(ra)
Current Store : [0x80007f6c] : sw t5, 1144(ra) -- Store: [0x80013338]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007fa0]:fadd.d t5, t3, s10, dyn
	-[0x80007fa4]:csrrs a7, fcsr, zero
	-[0x80007fa8]:sw t5, 1160(ra)
	-[0x80007fac]:sw t6, 1168(ra)
Current Store : [0x80007fac] : sw t6, 1168(ra) -- Store: [0x80013350]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007fa0]:fadd.d t5, t3, s10, dyn
	-[0x80007fa4]:csrrs a7, fcsr, zero
	-[0x80007fa8]:sw t5, 1160(ra)
	-[0x80007fac]:sw t6, 1168(ra)
	-[0x80007fb0]:sw t5, 1176(ra)
Current Store : [0x80007fb0] : sw t5, 1176(ra) -- Store: [0x80013358]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007fe0]:fadd.d t5, t3, s10, dyn
	-[0x80007fe4]:csrrs a7, fcsr, zero
	-[0x80007fe8]:sw t5, 1192(ra)
	-[0x80007fec]:sw t6, 1200(ra)
Current Store : [0x80007fec] : sw t6, 1200(ra) -- Store: [0x80013370]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80007fe0]:fadd.d t5, t3, s10, dyn
	-[0x80007fe4]:csrrs a7, fcsr, zero
	-[0x80007fe8]:sw t5, 1192(ra)
	-[0x80007fec]:sw t6, 1200(ra)
	-[0x80007ff0]:sw t5, 1208(ra)
Current Store : [0x80007ff0] : sw t5, 1208(ra) -- Store: [0x80013378]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008020]:fadd.d t5, t3, s10, dyn
	-[0x80008024]:csrrs a7, fcsr, zero
	-[0x80008028]:sw t5, 1224(ra)
	-[0x8000802c]:sw t6, 1232(ra)
Current Store : [0x8000802c] : sw t6, 1232(ra) -- Store: [0x80013390]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008020]:fadd.d t5, t3, s10, dyn
	-[0x80008024]:csrrs a7, fcsr, zero
	-[0x80008028]:sw t5, 1224(ra)
	-[0x8000802c]:sw t6, 1232(ra)
	-[0x80008030]:sw t5, 1240(ra)
Current Store : [0x80008030] : sw t5, 1240(ra) -- Store: [0x80013398]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008060]:fadd.d t5, t3, s10, dyn
	-[0x80008064]:csrrs a7, fcsr, zero
	-[0x80008068]:sw t5, 1256(ra)
	-[0x8000806c]:sw t6, 1264(ra)
Current Store : [0x8000806c] : sw t6, 1264(ra) -- Store: [0x800133b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008060]:fadd.d t5, t3, s10, dyn
	-[0x80008064]:csrrs a7, fcsr, zero
	-[0x80008068]:sw t5, 1256(ra)
	-[0x8000806c]:sw t6, 1264(ra)
	-[0x80008070]:sw t5, 1272(ra)
Current Store : [0x80008070] : sw t5, 1272(ra) -- Store: [0x800133b8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800080a0]:fadd.d t5, t3, s10, dyn
	-[0x800080a4]:csrrs a7, fcsr, zero
	-[0x800080a8]:sw t5, 1288(ra)
	-[0x800080ac]:sw t6, 1296(ra)
Current Store : [0x800080ac] : sw t6, 1296(ra) -- Store: [0x800133d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800080a0]:fadd.d t5, t3, s10, dyn
	-[0x800080a4]:csrrs a7, fcsr, zero
	-[0x800080a8]:sw t5, 1288(ra)
	-[0x800080ac]:sw t6, 1296(ra)
	-[0x800080b0]:sw t5, 1304(ra)
Current Store : [0x800080b0] : sw t5, 1304(ra) -- Store: [0x800133d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800080e4]:fadd.d t5, t3, s10, dyn
	-[0x800080e8]:csrrs a7, fcsr, zero
	-[0x800080ec]:sw t5, 1320(ra)
	-[0x800080f0]:sw t6, 1328(ra)
Current Store : [0x800080f0] : sw t6, 1328(ra) -- Store: [0x800133f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800080e4]:fadd.d t5, t3, s10, dyn
	-[0x800080e8]:csrrs a7, fcsr, zero
	-[0x800080ec]:sw t5, 1320(ra)
	-[0x800080f0]:sw t6, 1328(ra)
	-[0x800080f4]:sw t5, 1336(ra)
Current Store : [0x800080f4] : sw t5, 1336(ra) -- Store: [0x800133f8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008128]:fadd.d t5, t3, s10, dyn
	-[0x8000812c]:csrrs a7, fcsr, zero
	-[0x80008130]:sw t5, 1352(ra)
	-[0x80008134]:sw t6, 1360(ra)
Current Store : [0x80008134] : sw t6, 1360(ra) -- Store: [0x80013410]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008128]:fadd.d t5, t3, s10, dyn
	-[0x8000812c]:csrrs a7, fcsr, zero
	-[0x80008130]:sw t5, 1352(ra)
	-[0x80008134]:sw t6, 1360(ra)
	-[0x80008138]:sw t5, 1368(ra)
Current Store : [0x80008138] : sw t5, 1368(ra) -- Store: [0x80013418]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008168]:fadd.d t5, t3, s10, dyn
	-[0x8000816c]:csrrs a7, fcsr, zero
	-[0x80008170]:sw t5, 1384(ra)
	-[0x80008174]:sw t6, 1392(ra)
Current Store : [0x80008174] : sw t6, 1392(ra) -- Store: [0x80013430]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008168]:fadd.d t5, t3, s10, dyn
	-[0x8000816c]:csrrs a7, fcsr, zero
	-[0x80008170]:sw t5, 1384(ra)
	-[0x80008174]:sw t6, 1392(ra)
	-[0x80008178]:sw t5, 1400(ra)
Current Store : [0x80008178] : sw t5, 1400(ra) -- Store: [0x80013438]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800081a8]:fadd.d t5, t3, s10, dyn
	-[0x800081ac]:csrrs a7, fcsr, zero
	-[0x800081b0]:sw t5, 1416(ra)
	-[0x800081b4]:sw t6, 1424(ra)
Current Store : [0x800081b4] : sw t6, 1424(ra) -- Store: [0x80013450]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800081a8]:fadd.d t5, t3, s10, dyn
	-[0x800081ac]:csrrs a7, fcsr, zero
	-[0x800081b0]:sw t5, 1416(ra)
	-[0x800081b4]:sw t6, 1424(ra)
	-[0x800081b8]:sw t5, 1432(ra)
Current Store : [0x800081b8] : sw t5, 1432(ra) -- Store: [0x80013458]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800081e8]:fadd.d t5, t3, s10, dyn
	-[0x800081ec]:csrrs a7, fcsr, zero
	-[0x800081f0]:sw t5, 1448(ra)
	-[0x800081f4]:sw t6, 1456(ra)
Current Store : [0x800081f4] : sw t6, 1456(ra) -- Store: [0x80013470]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800081e8]:fadd.d t5, t3, s10, dyn
	-[0x800081ec]:csrrs a7, fcsr, zero
	-[0x800081f0]:sw t5, 1448(ra)
	-[0x800081f4]:sw t6, 1456(ra)
	-[0x800081f8]:sw t5, 1464(ra)
Current Store : [0x800081f8] : sw t5, 1464(ra) -- Store: [0x80013478]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008228]:fadd.d t5, t3, s10, dyn
	-[0x8000822c]:csrrs a7, fcsr, zero
	-[0x80008230]:sw t5, 1480(ra)
	-[0x80008234]:sw t6, 1488(ra)
Current Store : [0x80008234] : sw t6, 1488(ra) -- Store: [0x80013490]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008228]:fadd.d t5, t3, s10, dyn
	-[0x8000822c]:csrrs a7, fcsr, zero
	-[0x80008230]:sw t5, 1480(ra)
	-[0x80008234]:sw t6, 1488(ra)
	-[0x80008238]:sw t5, 1496(ra)
Current Store : [0x80008238] : sw t5, 1496(ra) -- Store: [0x80013498]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008268]:fadd.d t5, t3, s10, dyn
	-[0x8000826c]:csrrs a7, fcsr, zero
	-[0x80008270]:sw t5, 1512(ra)
	-[0x80008274]:sw t6, 1520(ra)
Current Store : [0x80008274] : sw t6, 1520(ra) -- Store: [0x800134b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008268]:fadd.d t5, t3, s10, dyn
	-[0x8000826c]:csrrs a7, fcsr, zero
	-[0x80008270]:sw t5, 1512(ra)
	-[0x80008274]:sw t6, 1520(ra)
	-[0x80008278]:sw t5, 1528(ra)
Current Store : [0x80008278] : sw t5, 1528(ra) -- Store: [0x800134b8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800082a8]:fadd.d t5, t3, s10, dyn
	-[0x800082ac]:csrrs a7, fcsr, zero
	-[0x800082b0]:sw t5, 1544(ra)
	-[0x800082b4]:sw t6, 1552(ra)
Current Store : [0x800082b4] : sw t6, 1552(ra) -- Store: [0x800134d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800082a8]:fadd.d t5, t3, s10, dyn
	-[0x800082ac]:csrrs a7, fcsr, zero
	-[0x800082b0]:sw t5, 1544(ra)
	-[0x800082b4]:sw t6, 1552(ra)
	-[0x800082b8]:sw t5, 1560(ra)
Current Store : [0x800082b8] : sw t5, 1560(ra) -- Store: [0x800134d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800082e8]:fadd.d t5, t3, s10, dyn
	-[0x800082ec]:csrrs a7, fcsr, zero
	-[0x800082f0]:sw t5, 1576(ra)
	-[0x800082f4]:sw t6, 1584(ra)
Current Store : [0x800082f4] : sw t6, 1584(ra) -- Store: [0x800134f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800082e8]:fadd.d t5, t3, s10, dyn
	-[0x800082ec]:csrrs a7, fcsr, zero
	-[0x800082f0]:sw t5, 1576(ra)
	-[0x800082f4]:sw t6, 1584(ra)
	-[0x800082f8]:sw t5, 1592(ra)
Current Store : [0x800082f8] : sw t5, 1592(ra) -- Store: [0x800134f8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008328]:fadd.d t5, t3, s10, dyn
	-[0x8000832c]:csrrs a7, fcsr, zero
	-[0x80008330]:sw t5, 1608(ra)
	-[0x80008334]:sw t6, 1616(ra)
Current Store : [0x80008334] : sw t6, 1616(ra) -- Store: [0x80013510]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008328]:fadd.d t5, t3, s10, dyn
	-[0x8000832c]:csrrs a7, fcsr, zero
	-[0x80008330]:sw t5, 1608(ra)
	-[0x80008334]:sw t6, 1616(ra)
	-[0x80008338]:sw t5, 1624(ra)
Current Store : [0x80008338] : sw t5, 1624(ra) -- Store: [0x80013518]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008368]:fadd.d t5, t3, s10, dyn
	-[0x8000836c]:csrrs a7, fcsr, zero
	-[0x80008370]:sw t5, 1640(ra)
	-[0x80008374]:sw t6, 1648(ra)
Current Store : [0x80008374] : sw t6, 1648(ra) -- Store: [0x80013530]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008368]:fadd.d t5, t3, s10, dyn
	-[0x8000836c]:csrrs a7, fcsr, zero
	-[0x80008370]:sw t5, 1640(ra)
	-[0x80008374]:sw t6, 1648(ra)
	-[0x80008378]:sw t5, 1656(ra)
Current Store : [0x80008378] : sw t5, 1656(ra) -- Store: [0x80013538]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800083a8]:fadd.d t5, t3, s10, dyn
	-[0x800083ac]:csrrs a7, fcsr, zero
	-[0x800083b0]:sw t5, 1672(ra)
	-[0x800083b4]:sw t6, 1680(ra)
Current Store : [0x800083b4] : sw t6, 1680(ra) -- Store: [0x80013550]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800083a8]:fadd.d t5, t3, s10, dyn
	-[0x800083ac]:csrrs a7, fcsr, zero
	-[0x800083b0]:sw t5, 1672(ra)
	-[0x800083b4]:sw t6, 1680(ra)
	-[0x800083b8]:sw t5, 1688(ra)
Current Store : [0x800083b8] : sw t5, 1688(ra) -- Store: [0x80013558]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800083e8]:fadd.d t5, t3, s10, dyn
	-[0x800083ec]:csrrs a7, fcsr, zero
	-[0x800083f0]:sw t5, 1704(ra)
	-[0x800083f4]:sw t6, 1712(ra)
Current Store : [0x800083f4] : sw t6, 1712(ra) -- Store: [0x80013570]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800083e8]:fadd.d t5, t3, s10, dyn
	-[0x800083ec]:csrrs a7, fcsr, zero
	-[0x800083f0]:sw t5, 1704(ra)
	-[0x800083f4]:sw t6, 1712(ra)
	-[0x800083f8]:sw t5, 1720(ra)
Current Store : [0x800083f8] : sw t5, 1720(ra) -- Store: [0x80013578]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008428]:fadd.d t5, t3, s10, dyn
	-[0x8000842c]:csrrs a7, fcsr, zero
	-[0x80008430]:sw t5, 1736(ra)
	-[0x80008434]:sw t6, 1744(ra)
Current Store : [0x80008434] : sw t6, 1744(ra) -- Store: [0x80013590]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008428]:fadd.d t5, t3, s10, dyn
	-[0x8000842c]:csrrs a7, fcsr, zero
	-[0x80008430]:sw t5, 1736(ra)
	-[0x80008434]:sw t6, 1744(ra)
	-[0x80008438]:sw t5, 1752(ra)
Current Store : [0x80008438] : sw t5, 1752(ra) -- Store: [0x80013598]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008468]:fadd.d t5, t3, s10, dyn
	-[0x8000846c]:csrrs a7, fcsr, zero
	-[0x80008470]:sw t5, 1768(ra)
	-[0x80008474]:sw t6, 1776(ra)
Current Store : [0x80008474] : sw t6, 1776(ra) -- Store: [0x800135b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008468]:fadd.d t5, t3, s10, dyn
	-[0x8000846c]:csrrs a7, fcsr, zero
	-[0x80008470]:sw t5, 1768(ra)
	-[0x80008474]:sw t6, 1776(ra)
	-[0x80008478]:sw t5, 1784(ra)
Current Store : [0x80008478] : sw t5, 1784(ra) -- Store: [0x800135b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800084a8]:fadd.d t5, t3, s10, dyn
	-[0x800084ac]:csrrs a7, fcsr, zero
	-[0x800084b0]:sw t5, 1800(ra)
	-[0x800084b4]:sw t6, 1808(ra)
Current Store : [0x800084b4] : sw t6, 1808(ra) -- Store: [0x800135d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800084a8]:fadd.d t5, t3, s10, dyn
	-[0x800084ac]:csrrs a7, fcsr, zero
	-[0x800084b0]:sw t5, 1800(ra)
	-[0x800084b4]:sw t6, 1808(ra)
	-[0x800084b8]:sw t5, 1816(ra)
Current Store : [0x800084b8] : sw t5, 1816(ra) -- Store: [0x800135d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800084e8]:fadd.d t5, t3, s10, dyn
	-[0x800084ec]:csrrs a7, fcsr, zero
	-[0x800084f0]:sw t5, 1832(ra)
	-[0x800084f4]:sw t6, 1840(ra)
Current Store : [0x800084f4] : sw t6, 1840(ra) -- Store: [0x800135f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800084e8]:fadd.d t5, t3, s10, dyn
	-[0x800084ec]:csrrs a7, fcsr, zero
	-[0x800084f0]:sw t5, 1832(ra)
	-[0x800084f4]:sw t6, 1840(ra)
	-[0x800084f8]:sw t5, 1848(ra)
Current Store : [0x800084f8] : sw t5, 1848(ra) -- Store: [0x800135f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008528]:fadd.d t5, t3, s10, dyn
	-[0x8000852c]:csrrs a7, fcsr, zero
	-[0x80008530]:sw t5, 1864(ra)
	-[0x80008534]:sw t6, 1872(ra)
Current Store : [0x80008534] : sw t6, 1872(ra) -- Store: [0x80013610]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008528]:fadd.d t5, t3, s10, dyn
	-[0x8000852c]:csrrs a7, fcsr, zero
	-[0x80008530]:sw t5, 1864(ra)
	-[0x80008534]:sw t6, 1872(ra)
	-[0x80008538]:sw t5, 1880(ra)
Current Store : [0x80008538] : sw t5, 1880(ra) -- Store: [0x80013618]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000856c]:fadd.d t5, t3, s10, dyn
	-[0x80008570]:csrrs a7, fcsr, zero
	-[0x80008574]:sw t5, 1896(ra)
	-[0x80008578]:sw t6, 1904(ra)
Current Store : [0x80008578] : sw t6, 1904(ra) -- Store: [0x80013630]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000856c]:fadd.d t5, t3, s10, dyn
	-[0x80008570]:csrrs a7, fcsr, zero
	-[0x80008574]:sw t5, 1896(ra)
	-[0x80008578]:sw t6, 1904(ra)
	-[0x8000857c]:sw t5, 1912(ra)
Current Store : [0x8000857c] : sw t5, 1912(ra) -- Store: [0x80013638]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800085b0]:fadd.d t5, t3, s10, dyn
	-[0x800085b4]:csrrs a7, fcsr, zero
	-[0x800085b8]:sw t5, 1928(ra)
	-[0x800085bc]:sw t6, 1936(ra)
Current Store : [0x800085bc] : sw t6, 1936(ra) -- Store: [0x80013650]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800085b0]:fadd.d t5, t3, s10, dyn
	-[0x800085b4]:csrrs a7, fcsr, zero
	-[0x800085b8]:sw t5, 1928(ra)
	-[0x800085bc]:sw t6, 1936(ra)
	-[0x800085c0]:sw t5, 1944(ra)
Current Store : [0x800085c0] : sw t5, 1944(ra) -- Store: [0x80013658]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800085f0]:fadd.d t5, t3, s10, dyn
	-[0x800085f4]:csrrs a7, fcsr, zero
	-[0x800085f8]:sw t5, 1960(ra)
	-[0x800085fc]:sw t6, 1968(ra)
Current Store : [0x800085fc] : sw t6, 1968(ra) -- Store: [0x80013670]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800085f0]:fadd.d t5, t3, s10, dyn
	-[0x800085f4]:csrrs a7, fcsr, zero
	-[0x800085f8]:sw t5, 1960(ra)
	-[0x800085fc]:sw t6, 1968(ra)
	-[0x80008600]:sw t5, 1976(ra)
Current Store : [0x80008600] : sw t5, 1976(ra) -- Store: [0x80013678]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008630]:fadd.d t5, t3, s10, dyn
	-[0x80008634]:csrrs a7, fcsr, zero
	-[0x80008638]:sw t5, 1992(ra)
	-[0x8000863c]:sw t6, 2000(ra)
Current Store : [0x8000863c] : sw t6, 2000(ra) -- Store: [0x80013690]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008630]:fadd.d t5, t3, s10, dyn
	-[0x80008634]:csrrs a7, fcsr, zero
	-[0x80008638]:sw t5, 1992(ra)
	-[0x8000863c]:sw t6, 2000(ra)
	-[0x80008640]:sw t5, 2008(ra)
Current Store : [0x80008640] : sw t5, 2008(ra) -- Store: [0x80013698]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008670]:fadd.d t5, t3, s10, dyn
	-[0x80008674]:csrrs a7, fcsr, zero
	-[0x80008678]:sw t5, 2024(ra)
	-[0x8000867c]:sw t6, 2032(ra)
Current Store : [0x8000867c] : sw t6, 2032(ra) -- Store: [0x800136b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008670]:fadd.d t5, t3, s10, dyn
	-[0x80008674]:csrrs a7, fcsr, zero
	-[0x80008678]:sw t5, 2024(ra)
	-[0x8000867c]:sw t6, 2032(ra)
	-[0x80008680]:sw t5, 2040(ra)
Current Store : [0x80008680] : sw t5, 2040(ra) -- Store: [0x800136b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800086f4]:fadd.d t5, t3, s10, dyn
	-[0x800086f8]:csrrs a7, fcsr, zero
	-[0x800086fc]:sw t5, 16(ra)
	-[0x80008700]:sw t6, 24(ra)
Current Store : [0x80008700] : sw t6, 24(ra) -- Store: [0x800136d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800086f4]:fadd.d t5, t3, s10, dyn
	-[0x800086f8]:csrrs a7, fcsr, zero
	-[0x800086fc]:sw t5, 16(ra)
	-[0x80008700]:sw t6, 24(ra)
	-[0x80008704]:sw t5, 32(ra)
Current Store : [0x80008704] : sw t5, 32(ra) -- Store: [0x800136d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008778]:fadd.d t5, t3, s10, dyn
	-[0x8000877c]:csrrs a7, fcsr, zero
	-[0x80008780]:sw t5, 48(ra)
	-[0x80008784]:sw t6, 56(ra)
Current Store : [0x80008784] : sw t6, 56(ra) -- Store: [0x800136f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008778]:fadd.d t5, t3, s10, dyn
	-[0x8000877c]:csrrs a7, fcsr, zero
	-[0x80008780]:sw t5, 48(ra)
	-[0x80008784]:sw t6, 56(ra)
	-[0x80008788]:sw t5, 64(ra)
Current Store : [0x80008788] : sw t5, 64(ra) -- Store: [0x800136f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800087fc]:fadd.d t5, t3, s10, dyn
	-[0x80008800]:csrrs a7, fcsr, zero
	-[0x80008804]:sw t5, 80(ra)
	-[0x80008808]:sw t6, 88(ra)
Current Store : [0x80008808] : sw t6, 88(ra) -- Store: [0x80013710]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800087fc]:fadd.d t5, t3, s10, dyn
	-[0x80008800]:csrrs a7, fcsr, zero
	-[0x80008804]:sw t5, 80(ra)
	-[0x80008808]:sw t6, 88(ra)
	-[0x8000880c]:sw t5, 96(ra)
Current Store : [0x8000880c] : sw t5, 96(ra) -- Store: [0x80013718]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000887c]:fadd.d t5, t3, s10, dyn
	-[0x80008880]:csrrs a7, fcsr, zero
	-[0x80008884]:sw t5, 112(ra)
	-[0x80008888]:sw t6, 120(ra)
Current Store : [0x80008888] : sw t6, 120(ra) -- Store: [0x80013730]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000887c]:fadd.d t5, t3, s10, dyn
	-[0x80008880]:csrrs a7, fcsr, zero
	-[0x80008884]:sw t5, 112(ra)
	-[0x80008888]:sw t6, 120(ra)
	-[0x8000888c]:sw t5, 128(ra)
Current Store : [0x8000888c] : sw t5, 128(ra) -- Store: [0x80013738]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800088fc]:fadd.d t5, t3, s10, dyn
	-[0x80008900]:csrrs a7, fcsr, zero
	-[0x80008904]:sw t5, 144(ra)
	-[0x80008908]:sw t6, 152(ra)
Current Store : [0x80008908] : sw t6, 152(ra) -- Store: [0x80013750]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800088fc]:fadd.d t5, t3, s10, dyn
	-[0x80008900]:csrrs a7, fcsr, zero
	-[0x80008904]:sw t5, 144(ra)
	-[0x80008908]:sw t6, 152(ra)
	-[0x8000890c]:sw t5, 160(ra)
Current Store : [0x8000890c] : sw t5, 160(ra) -- Store: [0x80013758]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000897c]:fadd.d t5, t3, s10, dyn
	-[0x80008980]:csrrs a7, fcsr, zero
	-[0x80008984]:sw t5, 176(ra)
	-[0x80008988]:sw t6, 184(ra)
Current Store : [0x80008988] : sw t6, 184(ra) -- Store: [0x80013770]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000897c]:fadd.d t5, t3, s10, dyn
	-[0x80008980]:csrrs a7, fcsr, zero
	-[0x80008984]:sw t5, 176(ra)
	-[0x80008988]:sw t6, 184(ra)
	-[0x8000898c]:sw t5, 192(ra)
Current Store : [0x8000898c] : sw t5, 192(ra) -- Store: [0x80013778]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800089fc]:fadd.d t5, t3, s10, dyn
	-[0x80008a00]:csrrs a7, fcsr, zero
	-[0x80008a04]:sw t5, 208(ra)
	-[0x80008a08]:sw t6, 216(ra)
Current Store : [0x80008a08] : sw t6, 216(ra) -- Store: [0x80013790]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800089fc]:fadd.d t5, t3, s10, dyn
	-[0x80008a00]:csrrs a7, fcsr, zero
	-[0x80008a04]:sw t5, 208(ra)
	-[0x80008a08]:sw t6, 216(ra)
	-[0x80008a0c]:sw t5, 224(ra)
Current Store : [0x80008a0c] : sw t5, 224(ra) -- Store: [0x80013798]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008a7c]:fadd.d t5, t3, s10, dyn
	-[0x80008a80]:csrrs a7, fcsr, zero
	-[0x80008a84]:sw t5, 240(ra)
	-[0x80008a88]:sw t6, 248(ra)
Current Store : [0x80008a88] : sw t6, 248(ra) -- Store: [0x800137b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008a7c]:fadd.d t5, t3, s10, dyn
	-[0x80008a80]:csrrs a7, fcsr, zero
	-[0x80008a84]:sw t5, 240(ra)
	-[0x80008a88]:sw t6, 248(ra)
	-[0x80008a8c]:sw t5, 256(ra)
Current Store : [0x80008a8c] : sw t5, 256(ra) -- Store: [0x800137b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008afc]:fadd.d t5, t3, s10, dyn
	-[0x80008b00]:csrrs a7, fcsr, zero
	-[0x80008b04]:sw t5, 272(ra)
	-[0x80008b08]:sw t6, 280(ra)
Current Store : [0x80008b08] : sw t6, 280(ra) -- Store: [0x800137d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008afc]:fadd.d t5, t3, s10, dyn
	-[0x80008b00]:csrrs a7, fcsr, zero
	-[0x80008b04]:sw t5, 272(ra)
	-[0x80008b08]:sw t6, 280(ra)
	-[0x80008b0c]:sw t5, 288(ra)
Current Store : [0x80008b0c] : sw t5, 288(ra) -- Store: [0x800137d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008b7c]:fadd.d t5, t3, s10, dyn
	-[0x80008b80]:csrrs a7, fcsr, zero
	-[0x80008b84]:sw t5, 304(ra)
	-[0x80008b88]:sw t6, 312(ra)
Current Store : [0x80008b88] : sw t6, 312(ra) -- Store: [0x800137f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008b7c]:fadd.d t5, t3, s10, dyn
	-[0x80008b80]:csrrs a7, fcsr, zero
	-[0x80008b84]:sw t5, 304(ra)
	-[0x80008b88]:sw t6, 312(ra)
	-[0x80008b8c]:sw t5, 320(ra)
Current Store : [0x80008b8c] : sw t5, 320(ra) -- Store: [0x800137f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008bfc]:fadd.d t5, t3, s10, dyn
	-[0x80008c00]:csrrs a7, fcsr, zero
	-[0x80008c04]:sw t5, 336(ra)
	-[0x80008c08]:sw t6, 344(ra)
Current Store : [0x80008c08] : sw t6, 344(ra) -- Store: [0x80013810]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008bfc]:fadd.d t5, t3, s10, dyn
	-[0x80008c00]:csrrs a7, fcsr, zero
	-[0x80008c04]:sw t5, 336(ra)
	-[0x80008c08]:sw t6, 344(ra)
	-[0x80008c0c]:sw t5, 352(ra)
Current Store : [0x80008c0c] : sw t5, 352(ra) -- Store: [0x80013818]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008c7c]:fadd.d t5, t3, s10, dyn
	-[0x80008c80]:csrrs a7, fcsr, zero
	-[0x80008c84]:sw t5, 368(ra)
	-[0x80008c88]:sw t6, 376(ra)
Current Store : [0x80008c88] : sw t6, 376(ra) -- Store: [0x80013830]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008c7c]:fadd.d t5, t3, s10, dyn
	-[0x80008c80]:csrrs a7, fcsr, zero
	-[0x80008c84]:sw t5, 368(ra)
	-[0x80008c88]:sw t6, 376(ra)
	-[0x80008c8c]:sw t5, 384(ra)
Current Store : [0x80008c8c] : sw t5, 384(ra) -- Store: [0x80013838]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008cfc]:fadd.d t5, t3, s10, dyn
	-[0x80008d00]:csrrs a7, fcsr, zero
	-[0x80008d04]:sw t5, 400(ra)
	-[0x80008d08]:sw t6, 408(ra)
Current Store : [0x80008d08] : sw t6, 408(ra) -- Store: [0x80013850]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008cfc]:fadd.d t5, t3, s10, dyn
	-[0x80008d00]:csrrs a7, fcsr, zero
	-[0x80008d04]:sw t5, 400(ra)
	-[0x80008d08]:sw t6, 408(ra)
	-[0x80008d0c]:sw t5, 416(ra)
Current Store : [0x80008d0c] : sw t5, 416(ra) -- Store: [0x80013858]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008d7c]:fadd.d t5, t3, s10, dyn
	-[0x80008d80]:csrrs a7, fcsr, zero
	-[0x80008d84]:sw t5, 432(ra)
	-[0x80008d88]:sw t6, 440(ra)
Current Store : [0x80008d88] : sw t6, 440(ra) -- Store: [0x80013870]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008d7c]:fadd.d t5, t3, s10, dyn
	-[0x80008d80]:csrrs a7, fcsr, zero
	-[0x80008d84]:sw t5, 432(ra)
	-[0x80008d88]:sw t6, 440(ra)
	-[0x80008d8c]:sw t5, 448(ra)
Current Store : [0x80008d8c] : sw t5, 448(ra) -- Store: [0x80013878]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008dfc]:fadd.d t5, t3, s10, dyn
	-[0x80008e00]:csrrs a7, fcsr, zero
	-[0x80008e04]:sw t5, 464(ra)
	-[0x80008e08]:sw t6, 472(ra)
Current Store : [0x80008e08] : sw t6, 472(ra) -- Store: [0x80013890]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008dfc]:fadd.d t5, t3, s10, dyn
	-[0x80008e00]:csrrs a7, fcsr, zero
	-[0x80008e04]:sw t5, 464(ra)
	-[0x80008e08]:sw t6, 472(ra)
	-[0x80008e0c]:sw t5, 480(ra)
Current Store : [0x80008e0c] : sw t5, 480(ra) -- Store: [0x80013898]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008e7c]:fadd.d t5, t3, s10, dyn
	-[0x80008e80]:csrrs a7, fcsr, zero
	-[0x80008e84]:sw t5, 496(ra)
	-[0x80008e88]:sw t6, 504(ra)
Current Store : [0x80008e88] : sw t6, 504(ra) -- Store: [0x800138b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008e7c]:fadd.d t5, t3, s10, dyn
	-[0x80008e80]:csrrs a7, fcsr, zero
	-[0x80008e84]:sw t5, 496(ra)
	-[0x80008e88]:sw t6, 504(ra)
	-[0x80008e8c]:sw t5, 512(ra)
Current Store : [0x80008e8c] : sw t5, 512(ra) -- Store: [0x800138b8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008efc]:fadd.d t5, t3, s10, dyn
	-[0x80008f00]:csrrs a7, fcsr, zero
	-[0x80008f04]:sw t5, 528(ra)
	-[0x80008f08]:sw t6, 536(ra)
Current Store : [0x80008f08] : sw t6, 536(ra) -- Store: [0x800138d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008efc]:fadd.d t5, t3, s10, dyn
	-[0x80008f00]:csrrs a7, fcsr, zero
	-[0x80008f04]:sw t5, 528(ra)
	-[0x80008f08]:sw t6, 536(ra)
	-[0x80008f0c]:sw t5, 544(ra)
Current Store : [0x80008f0c] : sw t5, 544(ra) -- Store: [0x800138d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008f7c]:fadd.d t5, t3, s10, dyn
	-[0x80008f80]:csrrs a7, fcsr, zero
	-[0x80008f84]:sw t5, 560(ra)
	-[0x80008f88]:sw t6, 568(ra)
Current Store : [0x80008f88] : sw t6, 568(ra) -- Store: [0x800138f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008f7c]:fadd.d t5, t3, s10, dyn
	-[0x80008f80]:csrrs a7, fcsr, zero
	-[0x80008f84]:sw t5, 560(ra)
	-[0x80008f88]:sw t6, 568(ra)
	-[0x80008f8c]:sw t5, 576(ra)
Current Store : [0x80008f8c] : sw t5, 576(ra) -- Store: [0x800138f8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008ffc]:fadd.d t5, t3, s10, dyn
	-[0x80009000]:csrrs a7, fcsr, zero
	-[0x80009004]:sw t5, 592(ra)
	-[0x80009008]:sw t6, 600(ra)
Current Store : [0x80009008] : sw t6, 600(ra) -- Store: [0x80013910]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80008ffc]:fadd.d t5, t3, s10, dyn
	-[0x80009000]:csrrs a7, fcsr, zero
	-[0x80009004]:sw t5, 592(ra)
	-[0x80009008]:sw t6, 600(ra)
	-[0x8000900c]:sw t5, 608(ra)
Current Store : [0x8000900c] : sw t5, 608(ra) -- Store: [0x80013918]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009080]:fadd.d t5, t3, s10, dyn
	-[0x80009084]:csrrs a7, fcsr, zero
	-[0x80009088]:sw t5, 624(ra)
	-[0x8000908c]:sw t6, 632(ra)
Current Store : [0x8000908c] : sw t6, 632(ra) -- Store: [0x80013930]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009080]:fadd.d t5, t3, s10, dyn
	-[0x80009084]:csrrs a7, fcsr, zero
	-[0x80009088]:sw t5, 624(ra)
	-[0x8000908c]:sw t6, 632(ra)
	-[0x80009090]:sw t5, 640(ra)
Current Store : [0x80009090] : sw t5, 640(ra) -- Store: [0x80013938]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009104]:fadd.d t5, t3, s10, dyn
	-[0x80009108]:csrrs a7, fcsr, zero
	-[0x8000910c]:sw t5, 656(ra)
	-[0x80009110]:sw t6, 664(ra)
Current Store : [0x80009110] : sw t6, 664(ra) -- Store: [0x80013950]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009104]:fadd.d t5, t3, s10, dyn
	-[0x80009108]:csrrs a7, fcsr, zero
	-[0x8000910c]:sw t5, 656(ra)
	-[0x80009110]:sw t6, 664(ra)
	-[0x80009114]:sw t5, 672(ra)
Current Store : [0x80009114] : sw t5, 672(ra) -- Store: [0x80013958]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009184]:fadd.d t5, t3, s10, dyn
	-[0x80009188]:csrrs a7, fcsr, zero
	-[0x8000918c]:sw t5, 688(ra)
	-[0x80009190]:sw t6, 696(ra)
Current Store : [0x80009190] : sw t6, 696(ra) -- Store: [0x80013970]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009184]:fadd.d t5, t3, s10, dyn
	-[0x80009188]:csrrs a7, fcsr, zero
	-[0x8000918c]:sw t5, 688(ra)
	-[0x80009190]:sw t6, 696(ra)
	-[0x80009194]:sw t5, 704(ra)
Current Store : [0x80009194] : sw t5, 704(ra) -- Store: [0x80013978]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009204]:fadd.d t5, t3, s10, dyn
	-[0x80009208]:csrrs a7, fcsr, zero
	-[0x8000920c]:sw t5, 720(ra)
	-[0x80009210]:sw t6, 728(ra)
Current Store : [0x80009210] : sw t6, 728(ra) -- Store: [0x80013990]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009204]:fadd.d t5, t3, s10, dyn
	-[0x80009208]:csrrs a7, fcsr, zero
	-[0x8000920c]:sw t5, 720(ra)
	-[0x80009210]:sw t6, 728(ra)
	-[0x80009214]:sw t5, 736(ra)
Current Store : [0x80009214] : sw t5, 736(ra) -- Store: [0x80013998]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009284]:fadd.d t5, t3, s10, dyn
	-[0x80009288]:csrrs a7, fcsr, zero
	-[0x8000928c]:sw t5, 752(ra)
	-[0x80009290]:sw t6, 760(ra)
Current Store : [0x80009290] : sw t6, 760(ra) -- Store: [0x800139b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009284]:fadd.d t5, t3, s10, dyn
	-[0x80009288]:csrrs a7, fcsr, zero
	-[0x8000928c]:sw t5, 752(ra)
	-[0x80009290]:sw t6, 760(ra)
	-[0x80009294]:sw t5, 768(ra)
Current Store : [0x80009294] : sw t5, 768(ra) -- Store: [0x800139b8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009304]:fadd.d t5, t3, s10, dyn
	-[0x80009308]:csrrs a7, fcsr, zero
	-[0x8000930c]:sw t5, 784(ra)
	-[0x80009310]:sw t6, 792(ra)
Current Store : [0x80009310] : sw t6, 792(ra) -- Store: [0x800139d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009304]:fadd.d t5, t3, s10, dyn
	-[0x80009308]:csrrs a7, fcsr, zero
	-[0x8000930c]:sw t5, 784(ra)
	-[0x80009310]:sw t6, 792(ra)
	-[0x80009314]:sw t5, 800(ra)
Current Store : [0x80009314] : sw t5, 800(ra) -- Store: [0x800139d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009388]:fadd.d t5, t3, s10, dyn
	-[0x8000938c]:csrrs a7, fcsr, zero
	-[0x80009390]:sw t5, 816(ra)
	-[0x80009394]:sw t6, 824(ra)
Current Store : [0x80009394] : sw t6, 824(ra) -- Store: [0x800139f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80009388]:fadd.d t5, t3, s10, dyn
	-[0x8000938c]:csrrs a7, fcsr, zero
	-[0x80009390]:sw t5, 816(ra)
	-[0x80009394]:sw t6, 824(ra)
	-[0x80009398]:sw t5, 832(ra)
Current Store : [0x80009398] : sw t5, 832(ra) -- Store: [0x800139f8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000940c]:fadd.d t5, t3, s10, dyn
	-[0x80009410]:csrrs a7, fcsr, zero
	-[0x80009414]:sw t5, 848(ra)
	-[0x80009418]:sw t6, 856(ra)
Current Store : [0x80009418] : sw t6, 856(ra) -- Store: [0x80013a10]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000940c]:fadd.d t5, t3, s10, dyn
	-[0x80009410]:csrrs a7, fcsr, zero
	-[0x80009414]:sw t5, 848(ra)
	-[0x80009418]:sw t6, 856(ra)
	-[0x8000941c]:sw t5, 864(ra)
Current Store : [0x8000941c] : sw t5, 864(ra) -- Store: [0x80013a18]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000948c]:fadd.d t5, t3, s10, dyn
	-[0x80009490]:csrrs a7, fcsr, zero
	-[0x80009494]:sw t5, 880(ra)
	-[0x80009498]:sw t6, 888(ra)
Current Store : [0x80009498] : sw t6, 888(ra) -- Store: [0x80013a30]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000948c]:fadd.d t5, t3, s10, dyn
	-[0x80009490]:csrrs a7, fcsr, zero
	-[0x80009494]:sw t5, 880(ra)
	-[0x80009498]:sw t6, 888(ra)
	-[0x8000949c]:sw t5, 896(ra)
Current Store : [0x8000949c] : sw t5, 896(ra) -- Store: [0x80013a38]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c71c]:fadd.d t5, t3, s10, dyn
	-[0x8000c720]:csrrs a7, fcsr, zero
	-[0x8000c724]:sw t5, 0(ra)
	-[0x8000c728]:sw t6, 8(ra)
Current Store : [0x8000c728] : sw t6, 8(ra) -- Store: [0x800136d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c71c]:fadd.d t5, t3, s10, dyn
	-[0x8000c720]:csrrs a7, fcsr, zero
	-[0x8000c724]:sw t5, 0(ra)
	-[0x8000c728]:sw t6, 8(ra)
	-[0x8000c72c]:sw t5, 16(ra)
Current Store : [0x8000c72c] : sw t5, 16(ra) -- Store: [0x800136d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c75c]:fadd.d t5, t3, s10, dyn
	-[0x8000c760]:csrrs a7, fcsr, zero
	-[0x8000c764]:sw t5, 32(ra)
	-[0x8000c768]:sw t6, 40(ra)
Current Store : [0x8000c768] : sw t6, 40(ra) -- Store: [0x800136f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c75c]:fadd.d t5, t3, s10, dyn
	-[0x8000c760]:csrrs a7, fcsr, zero
	-[0x8000c764]:sw t5, 32(ra)
	-[0x8000c768]:sw t6, 40(ra)
	-[0x8000c76c]:sw t5, 48(ra)
Current Store : [0x8000c76c] : sw t5, 48(ra) -- Store: [0x800136f8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c79c]:fadd.d t5, t3, s10, dyn
	-[0x8000c7a0]:csrrs a7, fcsr, zero
	-[0x8000c7a4]:sw t5, 64(ra)
	-[0x8000c7a8]:sw t6, 72(ra)
Current Store : [0x8000c7a8] : sw t6, 72(ra) -- Store: [0x80013710]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c79c]:fadd.d t5, t3, s10, dyn
	-[0x8000c7a0]:csrrs a7, fcsr, zero
	-[0x8000c7a4]:sw t5, 64(ra)
	-[0x8000c7a8]:sw t6, 72(ra)
	-[0x8000c7ac]:sw t5, 80(ra)
Current Store : [0x8000c7ac] : sw t5, 80(ra) -- Store: [0x80013718]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c7dc]:fadd.d t5, t3, s10, dyn
	-[0x8000c7e0]:csrrs a7, fcsr, zero
	-[0x8000c7e4]:sw t5, 96(ra)
	-[0x8000c7e8]:sw t6, 104(ra)
Current Store : [0x8000c7e8] : sw t6, 104(ra) -- Store: [0x80013730]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c7dc]:fadd.d t5, t3, s10, dyn
	-[0x8000c7e0]:csrrs a7, fcsr, zero
	-[0x8000c7e4]:sw t5, 96(ra)
	-[0x8000c7e8]:sw t6, 104(ra)
	-[0x8000c7ec]:sw t5, 112(ra)
Current Store : [0x8000c7ec] : sw t5, 112(ra) -- Store: [0x80013738]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c81c]:fadd.d t5, t3, s10, dyn
	-[0x8000c820]:csrrs a7, fcsr, zero
	-[0x8000c824]:sw t5, 128(ra)
	-[0x8000c828]:sw t6, 136(ra)
Current Store : [0x8000c828] : sw t6, 136(ra) -- Store: [0x80013750]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c81c]:fadd.d t5, t3, s10, dyn
	-[0x8000c820]:csrrs a7, fcsr, zero
	-[0x8000c824]:sw t5, 128(ra)
	-[0x8000c828]:sw t6, 136(ra)
	-[0x8000c82c]:sw t5, 144(ra)
Current Store : [0x8000c82c] : sw t5, 144(ra) -- Store: [0x80013758]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c85c]:fadd.d t5, t3, s10, dyn
	-[0x8000c860]:csrrs a7, fcsr, zero
	-[0x8000c864]:sw t5, 160(ra)
	-[0x8000c868]:sw t6, 168(ra)
Current Store : [0x8000c868] : sw t6, 168(ra) -- Store: [0x80013770]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c85c]:fadd.d t5, t3, s10, dyn
	-[0x8000c860]:csrrs a7, fcsr, zero
	-[0x8000c864]:sw t5, 160(ra)
	-[0x8000c868]:sw t6, 168(ra)
	-[0x8000c86c]:sw t5, 176(ra)
Current Store : [0x8000c86c] : sw t5, 176(ra) -- Store: [0x80013778]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c89c]:fadd.d t5, t3, s10, dyn
	-[0x8000c8a0]:csrrs a7, fcsr, zero
	-[0x8000c8a4]:sw t5, 192(ra)
	-[0x8000c8a8]:sw t6, 200(ra)
Current Store : [0x8000c8a8] : sw t6, 200(ra) -- Store: [0x80013790]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c89c]:fadd.d t5, t3, s10, dyn
	-[0x8000c8a0]:csrrs a7, fcsr, zero
	-[0x8000c8a4]:sw t5, 192(ra)
	-[0x8000c8a8]:sw t6, 200(ra)
	-[0x8000c8ac]:sw t5, 208(ra)
Current Store : [0x8000c8ac] : sw t5, 208(ra) -- Store: [0x80013798]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c8dc]:fadd.d t5, t3, s10, dyn
	-[0x8000c8e0]:csrrs a7, fcsr, zero
	-[0x8000c8e4]:sw t5, 224(ra)
	-[0x8000c8e8]:sw t6, 232(ra)
Current Store : [0x8000c8e8] : sw t6, 232(ra) -- Store: [0x800137b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c8dc]:fadd.d t5, t3, s10, dyn
	-[0x8000c8e0]:csrrs a7, fcsr, zero
	-[0x8000c8e4]:sw t5, 224(ra)
	-[0x8000c8e8]:sw t6, 232(ra)
	-[0x8000c8ec]:sw t5, 240(ra)
Current Store : [0x8000c8ec] : sw t5, 240(ra) -- Store: [0x800137b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c91c]:fadd.d t5, t3, s10, dyn
	-[0x8000c920]:csrrs a7, fcsr, zero
	-[0x8000c924]:sw t5, 256(ra)
	-[0x8000c928]:sw t6, 264(ra)
Current Store : [0x8000c928] : sw t6, 264(ra) -- Store: [0x800137d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c91c]:fadd.d t5, t3, s10, dyn
	-[0x8000c920]:csrrs a7, fcsr, zero
	-[0x8000c924]:sw t5, 256(ra)
	-[0x8000c928]:sw t6, 264(ra)
	-[0x8000c92c]:sw t5, 272(ra)
Current Store : [0x8000c92c] : sw t5, 272(ra) -- Store: [0x800137d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c95c]:fadd.d t5, t3, s10, dyn
	-[0x8000c960]:csrrs a7, fcsr, zero
	-[0x8000c964]:sw t5, 288(ra)
	-[0x8000c968]:sw t6, 296(ra)
Current Store : [0x8000c968] : sw t6, 296(ra) -- Store: [0x800137f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c95c]:fadd.d t5, t3, s10, dyn
	-[0x8000c960]:csrrs a7, fcsr, zero
	-[0x8000c964]:sw t5, 288(ra)
	-[0x8000c968]:sw t6, 296(ra)
	-[0x8000c96c]:sw t5, 304(ra)
Current Store : [0x8000c96c] : sw t5, 304(ra) -- Store: [0x800137f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c99c]:fadd.d t5, t3, s10, dyn
	-[0x8000c9a0]:csrrs a7, fcsr, zero
	-[0x8000c9a4]:sw t5, 320(ra)
	-[0x8000c9a8]:sw t6, 328(ra)
Current Store : [0x8000c9a8] : sw t6, 328(ra) -- Store: [0x80013810]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c99c]:fadd.d t5, t3, s10, dyn
	-[0x8000c9a0]:csrrs a7, fcsr, zero
	-[0x8000c9a4]:sw t5, 320(ra)
	-[0x8000c9a8]:sw t6, 328(ra)
	-[0x8000c9ac]:sw t5, 336(ra)
Current Store : [0x8000c9ac] : sw t5, 336(ra) -- Store: [0x80013818]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c9e0]:fadd.d t5, t3, s10, dyn
	-[0x8000c9e4]:csrrs a7, fcsr, zero
	-[0x8000c9e8]:sw t5, 352(ra)
	-[0x8000c9ec]:sw t6, 360(ra)
Current Store : [0x8000c9ec] : sw t6, 360(ra) -- Store: [0x80013830]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000c9e0]:fadd.d t5, t3, s10, dyn
	-[0x8000c9e4]:csrrs a7, fcsr, zero
	-[0x8000c9e8]:sw t5, 352(ra)
	-[0x8000c9ec]:sw t6, 360(ra)
	-[0x8000c9f0]:sw t5, 368(ra)
Current Store : [0x8000c9f0] : sw t5, 368(ra) -- Store: [0x80013838]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ca24]:fadd.d t5, t3, s10, dyn
	-[0x8000ca28]:csrrs a7, fcsr, zero
	-[0x8000ca2c]:sw t5, 384(ra)
	-[0x8000ca30]:sw t6, 392(ra)
Current Store : [0x8000ca30] : sw t6, 392(ra) -- Store: [0x80013850]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ca24]:fadd.d t5, t3, s10, dyn
	-[0x8000ca28]:csrrs a7, fcsr, zero
	-[0x8000ca2c]:sw t5, 384(ra)
	-[0x8000ca30]:sw t6, 392(ra)
	-[0x8000ca34]:sw t5, 400(ra)
Current Store : [0x8000ca34] : sw t5, 400(ra) -- Store: [0x80013858]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ca64]:fadd.d t5, t3, s10, dyn
	-[0x8000ca68]:csrrs a7, fcsr, zero
	-[0x8000ca6c]:sw t5, 416(ra)
	-[0x8000ca70]:sw t6, 424(ra)
Current Store : [0x8000ca70] : sw t6, 424(ra) -- Store: [0x80013870]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ca64]:fadd.d t5, t3, s10, dyn
	-[0x8000ca68]:csrrs a7, fcsr, zero
	-[0x8000ca6c]:sw t5, 416(ra)
	-[0x8000ca70]:sw t6, 424(ra)
	-[0x8000ca74]:sw t5, 432(ra)
Current Store : [0x8000ca74] : sw t5, 432(ra) -- Store: [0x80013878]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000caa4]:fadd.d t5, t3, s10, dyn
	-[0x8000caa8]:csrrs a7, fcsr, zero
	-[0x8000caac]:sw t5, 448(ra)
	-[0x8000cab0]:sw t6, 456(ra)
Current Store : [0x8000cab0] : sw t6, 456(ra) -- Store: [0x80013890]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000caa4]:fadd.d t5, t3, s10, dyn
	-[0x8000caa8]:csrrs a7, fcsr, zero
	-[0x8000caac]:sw t5, 448(ra)
	-[0x8000cab0]:sw t6, 456(ra)
	-[0x8000cab4]:sw t5, 464(ra)
Current Store : [0x8000cab4] : sw t5, 464(ra) -- Store: [0x80013898]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cae4]:fadd.d t5, t3, s10, dyn
	-[0x8000cae8]:csrrs a7, fcsr, zero
	-[0x8000caec]:sw t5, 480(ra)
	-[0x8000caf0]:sw t6, 488(ra)
Current Store : [0x8000caf0] : sw t6, 488(ra) -- Store: [0x800138b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cae4]:fadd.d t5, t3, s10, dyn
	-[0x8000cae8]:csrrs a7, fcsr, zero
	-[0x8000caec]:sw t5, 480(ra)
	-[0x8000caf0]:sw t6, 488(ra)
	-[0x8000caf4]:sw t5, 496(ra)
Current Store : [0x8000caf4] : sw t5, 496(ra) -- Store: [0x800138b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cb24]:fadd.d t5, t3, s10, dyn
	-[0x8000cb28]:csrrs a7, fcsr, zero
	-[0x8000cb2c]:sw t5, 512(ra)
	-[0x8000cb30]:sw t6, 520(ra)
Current Store : [0x8000cb30] : sw t6, 520(ra) -- Store: [0x800138d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cb24]:fadd.d t5, t3, s10, dyn
	-[0x8000cb28]:csrrs a7, fcsr, zero
	-[0x8000cb2c]:sw t5, 512(ra)
	-[0x8000cb30]:sw t6, 520(ra)
	-[0x8000cb34]:sw t5, 528(ra)
Current Store : [0x8000cb34] : sw t5, 528(ra) -- Store: [0x800138d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cb68]:fadd.d t5, t3, s10, dyn
	-[0x8000cb6c]:csrrs a7, fcsr, zero
	-[0x8000cb70]:sw t5, 544(ra)
	-[0x8000cb74]:sw t6, 552(ra)
Current Store : [0x8000cb74] : sw t6, 552(ra) -- Store: [0x800138f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cb68]:fadd.d t5, t3, s10, dyn
	-[0x8000cb6c]:csrrs a7, fcsr, zero
	-[0x8000cb70]:sw t5, 544(ra)
	-[0x8000cb74]:sw t6, 552(ra)
	-[0x8000cb78]:sw t5, 560(ra)
Current Store : [0x8000cb78] : sw t5, 560(ra) -- Store: [0x800138f8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cbac]:fadd.d t5, t3, s10, dyn
	-[0x8000cbb0]:csrrs a7, fcsr, zero
	-[0x8000cbb4]:sw t5, 576(ra)
	-[0x8000cbb8]:sw t6, 584(ra)
Current Store : [0x8000cbb8] : sw t6, 584(ra) -- Store: [0x80013910]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cbac]:fadd.d t5, t3, s10, dyn
	-[0x8000cbb0]:csrrs a7, fcsr, zero
	-[0x8000cbb4]:sw t5, 576(ra)
	-[0x8000cbb8]:sw t6, 584(ra)
	-[0x8000cbbc]:sw t5, 592(ra)
Current Store : [0x8000cbbc] : sw t5, 592(ra) -- Store: [0x80013918]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cbec]:fadd.d t5, t3, s10, dyn
	-[0x8000cbf0]:csrrs a7, fcsr, zero
	-[0x8000cbf4]:sw t5, 608(ra)
	-[0x8000cbf8]:sw t6, 616(ra)
Current Store : [0x8000cbf8] : sw t6, 616(ra) -- Store: [0x80013930]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cbec]:fadd.d t5, t3, s10, dyn
	-[0x8000cbf0]:csrrs a7, fcsr, zero
	-[0x8000cbf4]:sw t5, 608(ra)
	-[0x8000cbf8]:sw t6, 616(ra)
	-[0x8000cbfc]:sw t5, 624(ra)
Current Store : [0x8000cbfc] : sw t5, 624(ra) -- Store: [0x80013938]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cc2c]:fadd.d t5, t3, s10, dyn
	-[0x8000cc30]:csrrs a7, fcsr, zero
	-[0x8000cc34]:sw t5, 640(ra)
	-[0x8000cc38]:sw t6, 648(ra)
Current Store : [0x8000cc38] : sw t6, 648(ra) -- Store: [0x80013950]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cc2c]:fadd.d t5, t3, s10, dyn
	-[0x8000cc30]:csrrs a7, fcsr, zero
	-[0x8000cc34]:sw t5, 640(ra)
	-[0x8000cc38]:sw t6, 648(ra)
	-[0x8000cc3c]:sw t5, 656(ra)
Current Store : [0x8000cc3c] : sw t5, 656(ra) -- Store: [0x80013958]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cc6c]:fadd.d t5, t3, s10, dyn
	-[0x8000cc70]:csrrs a7, fcsr, zero
	-[0x8000cc74]:sw t5, 672(ra)
	-[0x8000cc78]:sw t6, 680(ra)
Current Store : [0x8000cc78] : sw t6, 680(ra) -- Store: [0x80013970]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cc6c]:fadd.d t5, t3, s10, dyn
	-[0x8000cc70]:csrrs a7, fcsr, zero
	-[0x8000cc74]:sw t5, 672(ra)
	-[0x8000cc78]:sw t6, 680(ra)
	-[0x8000cc7c]:sw t5, 688(ra)
Current Store : [0x8000cc7c] : sw t5, 688(ra) -- Store: [0x80013978]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ccac]:fadd.d t5, t3, s10, dyn
	-[0x8000ccb0]:csrrs a7, fcsr, zero
	-[0x8000ccb4]:sw t5, 704(ra)
	-[0x8000ccb8]:sw t6, 712(ra)
Current Store : [0x8000ccb8] : sw t6, 712(ra) -- Store: [0x80013990]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ccac]:fadd.d t5, t3, s10, dyn
	-[0x8000ccb0]:csrrs a7, fcsr, zero
	-[0x8000ccb4]:sw t5, 704(ra)
	-[0x8000ccb8]:sw t6, 712(ra)
	-[0x8000ccbc]:sw t5, 720(ra)
Current Store : [0x8000ccbc] : sw t5, 720(ra) -- Store: [0x80013998]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ccec]:fadd.d t5, t3, s10, dyn
	-[0x8000ccf0]:csrrs a7, fcsr, zero
	-[0x8000ccf4]:sw t5, 736(ra)
	-[0x8000ccf8]:sw t6, 744(ra)
Current Store : [0x8000ccf8] : sw t6, 744(ra) -- Store: [0x800139b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000ccec]:fadd.d t5, t3, s10, dyn
	-[0x8000ccf0]:csrrs a7, fcsr, zero
	-[0x8000ccf4]:sw t5, 736(ra)
	-[0x8000ccf8]:sw t6, 744(ra)
	-[0x8000ccfc]:sw t5, 752(ra)
Current Store : [0x8000ccfc] : sw t5, 752(ra) -- Store: [0x800139b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cd2c]:fadd.d t5, t3, s10, dyn
	-[0x8000cd30]:csrrs a7, fcsr, zero
	-[0x8000cd34]:sw t5, 768(ra)
	-[0x8000cd38]:sw t6, 776(ra)
Current Store : [0x8000cd38] : sw t6, 776(ra) -- Store: [0x800139d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cd2c]:fadd.d t5, t3, s10, dyn
	-[0x8000cd30]:csrrs a7, fcsr, zero
	-[0x8000cd34]:sw t5, 768(ra)
	-[0x8000cd38]:sw t6, 776(ra)
	-[0x8000cd3c]:sw t5, 784(ra)
Current Store : [0x8000cd3c] : sw t5, 784(ra) -- Store: [0x800139d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cd6c]:fadd.d t5, t3, s10, dyn
	-[0x8000cd70]:csrrs a7, fcsr, zero
	-[0x8000cd74]:sw t5, 800(ra)
	-[0x8000cd78]:sw t6, 808(ra)
Current Store : [0x8000cd78] : sw t6, 808(ra) -- Store: [0x800139f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cd6c]:fadd.d t5, t3, s10, dyn
	-[0x8000cd70]:csrrs a7, fcsr, zero
	-[0x8000cd74]:sw t5, 800(ra)
	-[0x8000cd78]:sw t6, 808(ra)
	-[0x8000cd7c]:sw t5, 816(ra)
Current Store : [0x8000cd7c] : sw t5, 816(ra) -- Store: [0x800139f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cdac]:fadd.d t5, t3, s10, dyn
	-[0x8000cdb0]:csrrs a7, fcsr, zero
	-[0x8000cdb4]:sw t5, 832(ra)
	-[0x8000cdb8]:sw t6, 840(ra)
Current Store : [0x8000cdb8] : sw t6, 840(ra) -- Store: [0x80013a10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cdac]:fadd.d t5, t3, s10, dyn
	-[0x8000cdb0]:csrrs a7, fcsr, zero
	-[0x8000cdb4]:sw t5, 832(ra)
	-[0x8000cdb8]:sw t6, 840(ra)
	-[0x8000cdbc]:sw t5, 848(ra)
Current Store : [0x8000cdbc] : sw t5, 848(ra) -- Store: [0x80013a18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cdec]:fadd.d t5, t3, s10, dyn
	-[0x8000cdf0]:csrrs a7, fcsr, zero
	-[0x8000cdf4]:sw t5, 864(ra)
	-[0x8000cdf8]:sw t6, 872(ra)
Current Store : [0x8000cdf8] : sw t6, 872(ra) -- Store: [0x80013a30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000cdec]:fadd.d t5, t3, s10, dyn
	-[0x8000cdf0]:csrrs a7, fcsr, zero
	-[0x8000cdf4]:sw t5, 864(ra)
	-[0x8000cdf8]:sw t6, 872(ra)
	-[0x8000cdfc]:sw t5, 880(ra)
Current Store : [0x8000cdfc] : sw t5, 880(ra) -- Store: [0x80013a38]:0x00000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                          signature                           |                                                                                                                        coverpoints                                                                                                                         |                                                                                                                       code                                                                                                                        |
|---:|--------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80011618]<br>0x00000000<br> [0x80011630]<br>0x00000000<br> |- mnemonic : fadd.d<br> - rs1 : x30<br> - rs2 : x30<br> - rd : x30<br> - rs1 == rs2 == rd<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br> |[0x8000013c]:fadd.d t5, t5, t5, dyn<br> [0x80000140]:csrrs tp, fcsr, zero<br> [0x80000144]:sw t5, 0(ra)<br> [0x80000148]:sw t6, 8(ra)<br> [0x8000014c]:sw t5, 16(ra)<br> [0x80000150]:sw tp, 24(ra)<br>                                            |
|   2|[0x80011638]<br>0x00000000<br> [0x80011650]<br>0x00000000<br> |- rs1 : x26<br> - rs2 : x26<br> - rd : x28<br> - rs1 == rs2 != rd<br>                                                                                                                                                                                       |[0x8000017c]:fadd.d t3, s10, s10, dyn<br> [0x80000180]:csrrs tp, fcsr, zero<br> [0x80000184]:sw t3, 32(ra)<br> [0x80000188]:sw t4, 40(ra)<br> [0x8000018c]:sw t3, 48(ra)<br> [0x80000190]:sw tp, 56(ra)<br>                                        |
|   3|[0x80011658]<br>0x00000001<br> [0x80011670]<br>0x00000000<br> |- rs1 : x28<br> - rs2 : x24<br> - rd : x26<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>  |[0x800001bc]:fadd.d s10, t3, s8, dyn<br> [0x800001c0]:csrrs tp, fcsr, zero<br> [0x800001c4]:sw s10, 64(ra)<br> [0x800001c8]:sw s11, 72(ra)<br> [0x800001cc]:sw s10, 80(ra)<br> [0x800001d0]:sw tp, 88(ra)<br>                                      |
|   4|[0x80011678]<br>0x00000001<br> [0x80011690]<br>0x00000000<br> |- rs1 : x24<br> - rs2 : x28<br> - rd : x24<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                         |[0x800001fc]:fadd.d s8, s8, t3, dyn<br> [0x80000200]:csrrs tp, fcsr, zero<br> [0x80000204]:sw s8, 96(ra)<br> [0x80000208]:sw s9, 104(ra)<br> [0x8000020c]:sw s8, 112(ra)<br> [0x80000210]:sw tp, 120(ra)<br>                                       |
|   5|[0x80011698]<br>0x00000002<br> [0x800116b0]<br>0x00000000<br> |- rs1 : x20<br> - rs2 : x22<br> - rd : x22<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                         |[0x8000023c]:fadd.d s6, s4, s6, dyn<br> [0x80000240]:csrrs tp, fcsr, zero<br> [0x80000244]:sw s6, 128(ra)<br> [0x80000248]:sw s7, 136(ra)<br> [0x8000024c]:sw s6, 144(ra)<br> [0x80000250]:sw tp, 152(ra)<br>                                      |
|   6|[0x800116b8]<br>0x00000002<br> [0x800116d0]<br>0x00000000<br> |- rs1 : x22<br> - rs2 : x18<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x8000027c]:fadd.d s4, s6, s2, dyn<br> [0x80000280]:csrrs tp, fcsr, zero<br> [0x80000284]:sw s4, 160(ra)<br> [0x80000288]:sw s5, 168(ra)<br> [0x8000028c]:sw s4, 176(ra)<br> [0x80000290]:sw tp, 184(ra)<br>                                      |
|   7|[0x800116d8]<br>0xFFFFFFFF<br> [0x800116f0]<br>0x00000000<br> |- rs1 : x16<br> - rs2 : x20<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x800002c0]:fadd.d s2, a6, s4, dyn<br> [0x800002c4]:csrrs tp, fcsr, zero<br> [0x800002c8]:sw s2, 192(ra)<br> [0x800002cc]:sw s3, 200(ra)<br> [0x800002d0]:sw s2, 208(ra)<br> [0x800002d4]:sw tp, 216(ra)<br>                                      |
|   8|[0x800116f8]<br>0xFFFFFFFF<br> [0x80011710]<br>0x00000000<br> |- rs1 : x18<br> - rs2 : x14<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000304]:fadd.d a6, s2, a4, dyn<br> [0x80000308]:csrrs tp, fcsr, zero<br> [0x8000030c]:sw a6, 224(ra)<br> [0x80000310]:sw a7, 232(ra)<br> [0x80000314]:sw a6, 240(ra)<br> [0x80000318]:sw tp, 248(ra)<br>                                      |
|   9|[0x80011718]<br>0x00000000<br> [0x80011730]<br>0x00000000<br> |- rs1 : x12<br> - rs2 : x16<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x80000344]:fadd.d a4, a2, a6, dyn<br> [0x80000348]:csrrs tp, fcsr, zero<br> [0x8000034c]:sw a4, 256(ra)<br> [0x80000350]:sw a5, 264(ra)<br> [0x80000354]:sw a4, 272(ra)<br> [0x80000358]:sw tp, 280(ra)<br>                                      |
|  10|[0x80011738]<br>0x00000000<br> [0x80011750]<br>0x00000000<br> |- rs1 : x14<br> - rs2 : x10<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                |[0x8000038c]:fadd.d a2, a4, a0, dyn<br> [0x80000390]:csrrs a7, fcsr, zero<br> [0x80000394]:sw a2, 288(ra)<br> [0x80000398]:sw a3, 296(ra)<br> [0x8000039c]:sw a2, 304(ra)<br> [0x800003a0]:sw a7, 312(ra)<br>                                      |
|  11|[0x80011758]<br>0x00000002<br> [0x80011770]<br>0x00000000<br> |- rs1 : x8<br> - rs2 : x12<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                 |[0x800003cc]:fadd.d a0, fp, a2, dyn<br> [0x800003d0]:csrrs a7, fcsr, zero<br> [0x800003d4]:sw a0, 320(ra)<br> [0x800003d8]:sw a1, 328(ra)<br> [0x800003dc]:sw a0, 336(ra)<br> [0x800003e0]:sw a7, 344(ra)<br>                                      |
|  12|[0x800116c8]<br>0x00000002<br> [0x800116e0]<br>0x00000000<br> |- rs1 : x10<br> - rs2 : x6<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                  |[0x80000414]:fadd.d fp, a0, t1, dyn<br> [0x80000418]:csrrs a7, fcsr, zero<br> [0x8000041c]:sw fp, 0(ra)<br> [0x80000420]:sw s1, 8(ra)<br> [0x80000424]:sw fp, 16(ra)<br> [0x80000428]:sw a7, 24(ra)<br>                                            |
|  13|[0x800116e8]<br>0xFFFFFFFF<br> [0x80011700]<br>0x00000000<br> |- rs1 : x4<br> - rs2 : x8<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                   |[0x80000458]:fadd.d t1, tp, fp, dyn<br> [0x8000045c]:csrrs a7, fcsr, zero<br> [0x80000460]:sw t1, 32(ra)<br> [0x80000464]:sw t2, 40(ra)<br> [0x80000468]:sw t1, 48(ra)<br> [0x8000046c]:sw a7, 56(ra)<br>                                          |
|  14|[0x80011708]<br>0xFFFFFFFF<br> [0x80011720]<br>0x00000000<br> |- rs1 : x6<br> - rs2 : x2<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                   |[0x8000049c]:fadd.d tp, t1, sp, dyn<br> [0x800004a0]:csrrs a7, fcsr, zero<br> [0x800004a4]:sw tp, 64(ra)<br> [0x800004a8]:sw t0, 72(ra)<br> [0x800004ac]:sw tp, 80(ra)<br> [0x800004b0]:sw a7, 88(ra)<br>                                          |
|  15|[0x80011728]<br>0x00000000<br> [0x80011740]<br>0x00000000<br> |- rs1 : x2<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                |[0x800004dc]:fadd.d t5, sp, t3, dyn<br> [0x800004e0]:csrrs a7, fcsr, zero<br> [0x800004e4]:sw t5, 96(ra)<br> [0x800004e8]:sw t6, 104(ra)<br> [0x800004ec]:sw t5, 112(ra)<br> [0x800004f0]:sw a7, 120(ra)<br>                                       |
|  16|[0x80011748]<br>0x00000000<br> [0x80011760]<br>0x00000000<br> |- rs2 : x4<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                |[0x8000051c]:fadd.d t5, t3, tp, dyn<br> [0x80000520]:csrrs a7, fcsr, zero<br> [0x80000524]:sw t5, 128(ra)<br> [0x80000528]:sw t6, 136(ra)<br> [0x8000052c]:sw t5, 144(ra)<br> [0x80000530]:sw a7, 152(ra)<br>                                      |
|  17|[0x80011768]<br>0x00000000<br> [0x80011780]<br>0x00000000<br> |- rd : x2<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                 |[0x8000055c]:fadd.d sp, t5, t3, dyn<br> [0x80000560]:csrrs a7, fcsr, zero<br> [0x80000564]:sw sp, 160(ra)<br> [0x80000568]:sw gp, 168(ra)<br> [0x8000056c]:sw sp, 176(ra)<br> [0x80000570]:sw a7, 184(ra)<br>                                      |
|  18|[0x80011788]<br>0x00000000<br> [0x800117a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000059c]:fadd.d t5, t3, s10, dyn<br> [0x800005a0]:csrrs a7, fcsr, zero<br> [0x800005a4]:sw t5, 192(ra)<br> [0x800005a8]:sw t6, 200(ra)<br> [0x800005ac]:sw t5, 208(ra)<br> [0x800005b0]:sw a7, 216(ra)<br>                                     |
|  19|[0x800117a8]<br>0x00000000<br> [0x800117c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800005dc]:fadd.d t5, t3, s10, dyn<br> [0x800005e0]:csrrs a7, fcsr, zero<br> [0x800005e4]:sw t5, 224(ra)<br> [0x800005e8]:sw t6, 232(ra)<br> [0x800005ec]:sw t5, 240(ra)<br> [0x800005f0]:sw a7, 248(ra)<br>                                     |
|  20|[0x800117c8]<br>0x00000000<br> [0x800117e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000061c]:fadd.d t5, t3, s10, dyn<br> [0x80000620]:csrrs a7, fcsr, zero<br> [0x80000624]:sw t5, 256(ra)<br> [0x80000628]:sw t6, 264(ra)<br> [0x8000062c]:sw t5, 272(ra)<br> [0x80000630]:sw a7, 280(ra)<br>                                     |
|  21|[0x800117e8]<br>0x00000000<br> [0x80011800]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000065c]:fadd.d t5, t3, s10, dyn<br> [0x80000660]:csrrs a7, fcsr, zero<br> [0x80000664]:sw t5, 288(ra)<br> [0x80000668]:sw t6, 296(ra)<br> [0x8000066c]:sw t5, 304(ra)<br> [0x80000670]:sw a7, 312(ra)<br>                                     |
|  22|[0x80011808]<br>0x00000000<br> [0x80011820]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000069c]:fadd.d t5, t3, s10, dyn<br> [0x800006a0]:csrrs a7, fcsr, zero<br> [0x800006a4]:sw t5, 320(ra)<br> [0x800006a8]:sw t6, 328(ra)<br> [0x800006ac]:sw t5, 336(ra)<br> [0x800006b0]:sw a7, 344(ra)<br>                                     |
|  23|[0x80011828]<br>0x00000000<br> [0x80011840]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800006dc]:fadd.d t5, t3, s10, dyn<br> [0x800006e0]:csrrs a7, fcsr, zero<br> [0x800006e4]:sw t5, 352(ra)<br> [0x800006e8]:sw t6, 360(ra)<br> [0x800006ec]:sw t5, 368(ra)<br> [0x800006f0]:sw a7, 376(ra)<br>                                     |
|  24|[0x80011848]<br>0x00000000<br> [0x80011860]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000071c]:fadd.d t5, t3, s10, dyn<br> [0x80000720]:csrrs a7, fcsr, zero<br> [0x80000724]:sw t5, 384(ra)<br> [0x80000728]:sw t6, 392(ra)<br> [0x8000072c]:sw t5, 400(ra)<br> [0x80000730]:sw a7, 408(ra)<br>                                     |
|  25|[0x80011868]<br>0x00000000<br> [0x80011880]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000075c]:fadd.d t5, t3, s10, dyn<br> [0x80000760]:csrrs a7, fcsr, zero<br> [0x80000764]:sw t5, 416(ra)<br> [0x80000768]:sw t6, 424(ra)<br> [0x8000076c]:sw t5, 432(ra)<br> [0x80000770]:sw a7, 440(ra)<br>                                     |
|  26|[0x80011888]<br>0x00000000<br> [0x800118a0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000079c]:fadd.d t5, t3, s10, dyn<br> [0x800007a0]:csrrs a7, fcsr, zero<br> [0x800007a4]:sw t5, 448(ra)<br> [0x800007a8]:sw t6, 456(ra)<br> [0x800007ac]:sw t5, 464(ra)<br> [0x800007b0]:sw a7, 472(ra)<br>                                     |
|  27|[0x800118a8]<br>0x00000001<br> [0x800118c0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800007dc]:fadd.d t5, t3, s10, dyn<br> [0x800007e0]:csrrs a7, fcsr, zero<br> [0x800007e4]:sw t5, 480(ra)<br> [0x800007e8]:sw t6, 488(ra)<br> [0x800007ec]:sw t5, 496(ra)<br> [0x800007f0]:sw a7, 504(ra)<br>                                     |
|  28|[0x800118c8]<br>0x00000001<br> [0x800118e0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000081c]:fadd.d t5, t3, s10, dyn<br> [0x80000820]:csrrs a7, fcsr, zero<br> [0x80000824]:sw t5, 512(ra)<br> [0x80000828]:sw t6, 520(ra)<br> [0x8000082c]:sw t5, 528(ra)<br> [0x80000830]:sw a7, 536(ra)<br>                                     |
|  29|[0x800118e8]<br>0x00000002<br> [0x80011900]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000085c]:fadd.d t5, t3, s10, dyn<br> [0x80000860]:csrrs a7, fcsr, zero<br> [0x80000864]:sw t5, 544(ra)<br> [0x80000868]:sw t6, 552(ra)<br> [0x8000086c]:sw t5, 560(ra)<br> [0x80000870]:sw a7, 568(ra)<br>                                     |
|  30|[0x80011908]<br>0x00000002<br> [0x80011920]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000089c]:fadd.d t5, t3, s10, dyn<br> [0x800008a0]:csrrs a7, fcsr, zero<br> [0x800008a4]:sw t5, 576(ra)<br> [0x800008a8]:sw t6, 584(ra)<br> [0x800008ac]:sw t5, 592(ra)<br> [0x800008b0]:sw a7, 600(ra)<br>                                     |
|  31|[0x80011928]<br>0xFFFFFFFF<br> [0x80011940]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800008e0]:fadd.d t5, t3, s10, dyn<br> [0x800008e4]:csrrs a7, fcsr, zero<br> [0x800008e8]:sw t5, 608(ra)<br> [0x800008ec]:sw t6, 616(ra)<br> [0x800008f0]:sw t5, 624(ra)<br> [0x800008f4]:sw a7, 632(ra)<br>                                     |
|  32|[0x80011948]<br>0xFFFFFFFF<br> [0x80011960]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000924]:fadd.d t5, t3, s10, dyn<br> [0x80000928]:csrrs a7, fcsr, zero<br> [0x8000092c]:sw t5, 640(ra)<br> [0x80000930]:sw t6, 648(ra)<br> [0x80000934]:sw t5, 656(ra)<br> [0x80000938]:sw a7, 664(ra)<br>                                     |
|  33|[0x80011968]<br>0x00000000<br> [0x80011980]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000964]:fadd.d t5, t3, s10, dyn<br> [0x80000968]:csrrs a7, fcsr, zero<br> [0x8000096c]:sw t5, 672(ra)<br> [0x80000970]:sw t6, 680(ra)<br> [0x80000974]:sw t5, 688(ra)<br> [0x80000978]:sw a7, 696(ra)<br>                                     |
|  34|[0x80011988]<br>0x00000000<br> [0x800119a0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800009a4]:fadd.d t5, t3, s10, dyn<br> [0x800009a8]:csrrs a7, fcsr, zero<br> [0x800009ac]:sw t5, 704(ra)<br> [0x800009b0]:sw t6, 712(ra)<br> [0x800009b4]:sw t5, 720(ra)<br> [0x800009b8]:sw a7, 728(ra)<br>                                     |
|  35|[0x800119a8]<br>0x00000002<br> [0x800119c0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800009e4]:fadd.d t5, t3, s10, dyn<br> [0x800009e8]:csrrs a7, fcsr, zero<br> [0x800009ec]:sw t5, 736(ra)<br> [0x800009f0]:sw t6, 744(ra)<br> [0x800009f4]:sw t5, 752(ra)<br> [0x800009f8]:sw a7, 760(ra)<br>                                     |
|  36|[0x800119c8]<br>0x00000002<br> [0x800119e0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000a24]:fadd.d t5, t3, s10, dyn<br> [0x80000a28]:csrrs a7, fcsr, zero<br> [0x80000a2c]:sw t5, 768(ra)<br> [0x80000a30]:sw t6, 776(ra)<br> [0x80000a34]:sw t5, 784(ra)<br> [0x80000a38]:sw a7, 792(ra)<br>                                     |
|  37|[0x800119e8]<br>0xFFFFFFFF<br> [0x80011a00]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000a68]:fadd.d t5, t3, s10, dyn<br> [0x80000a6c]:csrrs a7, fcsr, zero<br> [0x80000a70]:sw t5, 800(ra)<br> [0x80000a74]:sw t6, 808(ra)<br> [0x80000a78]:sw t5, 816(ra)<br> [0x80000a7c]:sw a7, 824(ra)<br>                                     |
|  38|[0x80011a08]<br>0xFFFFFFFF<br> [0x80011a20]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000aac]:fadd.d t5, t3, s10, dyn<br> [0x80000ab0]:csrrs a7, fcsr, zero<br> [0x80000ab4]:sw t5, 832(ra)<br> [0x80000ab8]:sw t6, 840(ra)<br> [0x80000abc]:sw t5, 848(ra)<br> [0x80000ac0]:sw a7, 856(ra)<br>                                     |
|  39|[0x80011a28]<br>0x00000000<br> [0x80011a40]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000aec]:fadd.d t5, t3, s10, dyn<br> [0x80000af0]:csrrs a7, fcsr, zero<br> [0x80000af4]:sw t5, 864(ra)<br> [0x80000af8]:sw t6, 872(ra)<br> [0x80000afc]:sw t5, 880(ra)<br> [0x80000b00]:sw a7, 888(ra)<br>                                     |
|  40|[0x80011a48]<br>0x00000000<br> [0x80011a60]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000b2c]:fadd.d t5, t3, s10, dyn<br> [0x80000b30]:csrrs a7, fcsr, zero<br> [0x80000b34]:sw t5, 896(ra)<br> [0x80000b38]:sw t6, 904(ra)<br> [0x80000b3c]:sw t5, 912(ra)<br> [0x80000b40]:sw a7, 920(ra)<br>                                     |
|  41|[0x80011a68]<br>0x00000000<br> [0x80011a80]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000b6c]:fadd.d t5, t3, s10, dyn<br> [0x80000b70]:csrrs a7, fcsr, zero<br> [0x80000b74]:sw t5, 928(ra)<br> [0x80000b78]:sw t6, 936(ra)<br> [0x80000b7c]:sw t5, 944(ra)<br> [0x80000b80]:sw a7, 952(ra)<br>                                     |
|  42|[0x80011a88]<br>0x00000000<br> [0x80011aa0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000bac]:fadd.d t5, t3, s10, dyn<br> [0x80000bb0]:csrrs a7, fcsr, zero<br> [0x80000bb4]:sw t5, 960(ra)<br> [0x80000bb8]:sw t6, 968(ra)<br> [0x80000bbc]:sw t5, 976(ra)<br> [0x80000bc0]:sw a7, 984(ra)<br>                                     |
|  43|[0x80011aa8]<br>0x00000000<br> [0x80011ac0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000bec]:fadd.d t5, t3, s10, dyn<br> [0x80000bf0]:csrrs a7, fcsr, zero<br> [0x80000bf4]:sw t5, 992(ra)<br> [0x80000bf8]:sw t6, 1000(ra)<br> [0x80000bfc]:sw t5, 1008(ra)<br> [0x80000c00]:sw a7, 1016(ra)<br>                                  |
|  44|[0x80011ac8]<br>0x00000000<br> [0x80011ae0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000c2c]:fadd.d t5, t3, s10, dyn<br> [0x80000c30]:csrrs a7, fcsr, zero<br> [0x80000c34]:sw t5, 1024(ra)<br> [0x80000c38]:sw t6, 1032(ra)<br> [0x80000c3c]:sw t5, 1040(ra)<br> [0x80000c40]:sw a7, 1048(ra)<br>                                 |
|  45|[0x80011ae8]<br>0x00000000<br> [0x80011b00]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000c6c]:fadd.d t5, t3, s10, dyn<br> [0x80000c70]:csrrs a7, fcsr, zero<br> [0x80000c74]:sw t5, 1056(ra)<br> [0x80000c78]:sw t6, 1064(ra)<br> [0x80000c7c]:sw t5, 1072(ra)<br> [0x80000c80]:sw a7, 1080(ra)<br>                                 |
|  46|[0x80011b08]<br>0x00000000<br> [0x80011b20]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000cac]:fadd.d t5, t3, s10, dyn<br> [0x80000cb0]:csrrs a7, fcsr, zero<br> [0x80000cb4]:sw t5, 1088(ra)<br> [0x80000cb8]:sw t6, 1096(ra)<br> [0x80000cbc]:sw t5, 1104(ra)<br> [0x80000cc0]:sw a7, 1112(ra)<br>                                 |
|  47|[0x80011b28]<br>0x00000000<br> [0x80011b40]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000cec]:fadd.d t5, t3, s10, dyn<br> [0x80000cf0]:csrrs a7, fcsr, zero<br> [0x80000cf4]:sw t5, 1120(ra)<br> [0x80000cf8]:sw t6, 1128(ra)<br> [0x80000cfc]:sw t5, 1136(ra)<br> [0x80000d00]:sw a7, 1144(ra)<br>                                 |
|  48|[0x80011b48]<br>0x00000000<br> [0x80011b60]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000d2c]:fadd.d t5, t3, s10, dyn<br> [0x80000d30]:csrrs a7, fcsr, zero<br> [0x80000d34]:sw t5, 1152(ra)<br> [0x80000d38]:sw t6, 1160(ra)<br> [0x80000d3c]:sw t5, 1168(ra)<br> [0x80000d40]:sw a7, 1176(ra)<br>                                 |
|  49|[0x80011b68]<br>0x00000001<br> [0x80011b80]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000d6c]:fadd.d t5, t3, s10, dyn<br> [0x80000d70]:csrrs a7, fcsr, zero<br> [0x80000d74]:sw t5, 1184(ra)<br> [0x80000d78]:sw t6, 1192(ra)<br> [0x80000d7c]:sw t5, 1200(ra)<br> [0x80000d80]:sw a7, 1208(ra)<br>                                 |
|  50|[0x80011b88]<br>0x00000001<br> [0x80011ba0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000dac]:fadd.d t5, t3, s10, dyn<br> [0x80000db0]:csrrs a7, fcsr, zero<br> [0x80000db4]:sw t5, 1216(ra)<br> [0x80000db8]:sw t6, 1224(ra)<br> [0x80000dbc]:sw t5, 1232(ra)<br> [0x80000dc0]:sw a7, 1240(ra)<br>                                 |
|  51|[0x80011ba8]<br>0x00000002<br> [0x80011bc0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000dec]:fadd.d t5, t3, s10, dyn<br> [0x80000df0]:csrrs a7, fcsr, zero<br> [0x80000df4]:sw t5, 1248(ra)<br> [0x80000df8]:sw t6, 1256(ra)<br> [0x80000dfc]:sw t5, 1264(ra)<br> [0x80000e00]:sw a7, 1272(ra)<br>                                 |
|  52|[0x80011bc8]<br>0x00000000<br> [0x80011be0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000e2c]:fadd.d t5, t3, s10, dyn<br> [0x80000e30]:csrrs a7, fcsr, zero<br> [0x80000e34]:sw t5, 1280(ra)<br> [0x80000e38]:sw t6, 1288(ra)<br> [0x80000e3c]:sw t5, 1296(ra)<br> [0x80000e40]:sw a7, 1304(ra)<br>                                 |
|  53|[0x80011be8]<br>0x00000003<br> [0x80011c00]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000e6c]:fadd.d t5, t3, s10, dyn<br> [0x80000e70]:csrrs a7, fcsr, zero<br> [0x80000e74]:sw t5, 1312(ra)<br> [0x80000e78]:sw t6, 1320(ra)<br> [0x80000e7c]:sw t5, 1328(ra)<br> [0x80000e80]:sw a7, 1336(ra)<br>                                 |
|  54|[0x80011c08]<br>0x00000001<br> [0x80011c20]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000eac]:fadd.d t5, t3, s10, dyn<br> [0x80000eb0]:csrrs a7, fcsr, zero<br> [0x80000eb4]:sw t5, 1344(ra)<br> [0x80000eb8]:sw t6, 1352(ra)<br> [0x80000ebc]:sw t5, 1360(ra)<br> [0x80000ec0]:sw a7, 1368(ra)<br>                                 |
|  55|[0x80011c28]<br>0x00000000<br> [0x80011c40]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000ef0]:fadd.d t5, t3, s10, dyn<br> [0x80000ef4]:csrrs a7, fcsr, zero<br> [0x80000ef8]:sw t5, 1376(ra)<br> [0x80000efc]:sw t6, 1384(ra)<br> [0x80000f00]:sw t5, 1392(ra)<br> [0x80000f04]:sw a7, 1400(ra)<br>                                 |
|  56|[0x80011c48]<br>0xFFFFFFFE<br> [0x80011c60]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000f34]:fadd.d t5, t3, s10, dyn<br> [0x80000f38]:csrrs a7, fcsr, zero<br> [0x80000f3c]:sw t5, 1408(ra)<br> [0x80000f40]:sw t6, 1416(ra)<br> [0x80000f44]:sw t5, 1424(ra)<br> [0x80000f48]:sw a7, 1432(ra)<br>                                 |
|  57|[0x80011c68]<br>0x00000001<br> [0x80011c80]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000f74]:fadd.d t5, t3, s10, dyn<br> [0x80000f78]:csrrs a7, fcsr, zero<br> [0x80000f7c]:sw t5, 1440(ra)<br> [0x80000f80]:sw t6, 1448(ra)<br> [0x80000f84]:sw t5, 1456(ra)<br> [0x80000f88]:sw a7, 1464(ra)<br>                                 |
|  58|[0x80011c88]<br>0xFFFFFFFF<br> [0x80011ca0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000fb4]:fadd.d t5, t3, s10, dyn<br> [0x80000fb8]:csrrs a7, fcsr, zero<br> [0x80000fbc]:sw t5, 1472(ra)<br> [0x80000fc0]:sw t6, 1480(ra)<br> [0x80000fc4]:sw t5, 1488(ra)<br> [0x80000fc8]:sw a7, 1496(ra)<br>                                 |
|  59|[0x80011ca8]<br>0x00000003<br> [0x80011cc0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80000ff4]:fadd.d t5, t3, s10, dyn<br> [0x80000ff8]:csrrs a7, fcsr, zero<br> [0x80000ffc]:sw t5, 1504(ra)<br> [0x80001000]:sw t6, 1512(ra)<br> [0x80001004]:sw t5, 1520(ra)<br> [0x80001008]:sw a7, 1528(ra)<br>                                 |
|  60|[0x80011cc8]<br>0x00000001<br> [0x80011ce0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001034]:fadd.d t5, t3, s10, dyn<br> [0x80001038]:csrrs a7, fcsr, zero<br> [0x8000103c]:sw t5, 1536(ra)<br> [0x80001040]:sw t6, 1544(ra)<br> [0x80001044]:sw t5, 1552(ra)<br> [0x80001048]:sw a7, 1560(ra)<br>                                 |
|  61|[0x80011ce8]<br>0xFFFFFFFF<br> [0x80011d00]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001078]:fadd.d t5, t3, s10, dyn<br> [0x8000107c]:csrrs a7, fcsr, zero<br> [0x80001080]:sw t5, 1568(ra)<br> [0x80001084]:sw t6, 1576(ra)<br> [0x80001088]:sw t5, 1584(ra)<br> [0x8000108c]:sw a7, 1592(ra)<br>                                 |
|  62|[0x80011d08]<br>0xFFFFFFFF<br> [0x80011d20]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800010bc]:fadd.d t5, t3, s10, dyn<br> [0x800010c0]:csrrs a7, fcsr, zero<br> [0x800010c4]:sw t5, 1600(ra)<br> [0x800010c8]:sw t6, 1608(ra)<br> [0x800010cc]:sw t5, 1616(ra)<br> [0x800010d0]:sw a7, 1624(ra)<br>                                 |
|  63|[0x80011d28]<br>0x00000000<br> [0x80011d40]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800010fc]:fadd.d t5, t3, s10, dyn<br> [0x80001100]:csrrs a7, fcsr, zero<br> [0x80001104]:sw t5, 1632(ra)<br> [0x80001108]:sw t6, 1640(ra)<br> [0x8000110c]:sw t5, 1648(ra)<br> [0x80001110]:sw a7, 1656(ra)<br>                                 |
|  64|[0x80011d48]<br>0x00000000<br> [0x80011d60]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000113c]:fadd.d t5, t3, s10, dyn<br> [0x80001140]:csrrs a7, fcsr, zero<br> [0x80001144]:sw t5, 1664(ra)<br> [0x80001148]:sw t6, 1672(ra)<br> [0x8000114c]:sw t5, 1680(ra)<br> [0x80001150]:sw a7, 1688(ra)<br>                                 |
|  65|[0x80011d68]<br>0x00000000<br> [0x80011d80]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000117c]:fadd.d t5, t3, s10, dyn<br> [0x80001180]:csrrs a7, fcsr, zero<br> [0x80001184]:sw t5, 1696(ra)<br> [0x80001188]:sw t6, 1704(ra)<br> [0x8000118c]:sw t5, 1712(ra)<br> [0x80001190]:sw a7, 1720(ra)<br>                                 |
|  66|[0x80011d88]<br>0x00000000<br> [0x80011da0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800011bc]:fadd.d t5, t3, s10, dyn<br> [0x800011c0]:csrrs a7, fcsr, zero<br> [0x800011c4]:sw t5, 1728(ra)<br> [0x800011c8]:sw t6, 1736(ra)<br> [0x800011cc]:sw t5, 1744(ra)<br> [0x800011d0]:sw a7, 1752(ra)<br>                                 |
|  67|[0x80011da8]<br>0x00000000<br> [0x80011dc0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800011fc]:fadd.d t5, t3, s10, dyn<br> [0x80001200]:csrrs a7, fcsr, zero<br> [0x80001204]:sw t5, 1760(ra)<br> [0x80001208]:sw t6, 1768(ra)<br> [0x8000120c]:sw t5, 1776(ra)<br> [0x80001210]:sw a7, 1784(ra)<br>                                 |
|  68|[0x80011dc8]<br>0x00000000<br> [0x80011de0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000123c]:fadd.d t5, t3, s10, dyn<br> [0x80001240]:csrrs a7, fcsr, zero<br> [0x80001244]:sw t5, 1792(ra)<br> [0x80001248]:sw t6, 1800(ra)<br> [0x8000124c]:sw t5, 1808(ra)<br> [0x80001250]:sw a7, 1816(ra)<br>                                 |
|  69|[0x80011de8]<br>0x00000000<br> [0x80011e00]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000127c]:fadd.d t5, t3, s10, dyn<br> [0x80001280]:csrrs a7, fcsr, zero<br> [0x80001284]:sw t5, 1824(ra)<br> [0x80001288]:sw t6, 1832(ra)<br> [0x8000128c]:sw t5, 1840(ra)<br> [0x80001290]:sw a7, 1848(ra)<br>                                 |
|  70|[0x80011e08]<br>0x00000000<br> [0x80011e20]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800012bc]:fadd.d t5, t3, s10, dyn<br> [0x800012c0]:csrrs a7, fcsr, zero<br> [0x800012c4]:sw t5, 1856(ra)<br> [0x800012c8]:sw t6, 1864(ra)<br> [0x800012cc]:sw t5, 1872(ra)<br> [0x800012d0]:sw a7, 1880(ra)<br>                                 |
|  71|[0x80011e28]<br>0x00000000<br> [0x80011e40]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800012fc]:fadd.d t5, t3, s10, dyn<br> [0x80001300]:csrrs a7, fcsr, zero<br> [0x80001304]:sw t5, 1888(ra)<br> [0x80001308]:sw t6, 1896(ra)<br> [0x8000130c]:sw t5, 1904(ra)<br> [0x80001310]:sw a7, 1912(ra)<br>                                 |
|  72|[0x80011e48]<br>0x00000000<br> [0x80011e60]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000133c]:fadd.d t5, t3, s10, dyn<br> [0x80001340]:csrrs a7, fcsr, zero<br> [0x80001344]:sw t5, 1920(ra)<br> [0x80001348]:sw t6, 1928(ra)<br> [0x8000134c]:sw t5, 1936(ra)<br> [0x80001350]:sw a7, 1944(ra)<br>                                 |
|  73|[0x80011e68]<br>0x00000001<br> [0x80011e80]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000137c]:fadd.d t5, t3, s10, dyn<br> [0x80001380]:csrrs a7, fcsr, zero<br> [0x80001384]:sw t5, 1952(ra)<br> [0x80001388]:sw t6, 1960(ra)<br> [0x8000138c]:sw t5, 1968(ra)<br> [0x80001390]:sw a7, 1976(ra)<br>                                 |
|  74|[0x80011e88]<br>0x00000001<br> [0x80011ea0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800013bc]:fadd.d t5, t3, s10, dyn<br> [0x800013c0]:csrrs a7, fcsr, zero<br> [0x800013c4]:sw t5, 1984(ra)<br> [0x800013c8]:sw t6, 1992(ra)<br> [0x800013cc]:sw t5, 2000(ra)<br> [0x800013d0]:sw a7, 2008(ra)<br>                                 |
|  75|[0x80011ea8]<br>0x00000000<br> [0x80011ec0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800013fc]:fadd.d t5, t3, s10, dyn<br> [0x80001400]:csrrs a7, fcsr, zero<br> [0x80001404]:sw t5, 2016(ra)<br> [0x80001408]:sw t6, 2024(ra)<br> [0x8000140c]:sw t5, 2032(ra)<br> [0x80001410]:sw a7, 2040(ra)<br>                                 |
|  76|[0x80011ec8]<br>0x00000002<br> [0x80011ee0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000143c]:fadd.d t5, t3, s10, dyn<br> [0x80001440]:csrrs a7, fcsr, zero<br> [0x80001444]:addi ra, ra, 2040<br> [0x80001448]:sw t5, 8(ra)<br> [0x8000144c]:sw t6, 16(ra)<br> [0x80001450]:sw t5, 24(ra)<br> [0x80001454]:sw a7, 32(ra)<br>       |
|  77|[0x80011ee8]<br>0x00000001<br> [0x80011f00]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001480]:fadd.d t5, t3, s10, dyn<br> [0x80001484]:csrrs a7, fcsr, zero<br> [0x80001488]:sw t5, 40(ra)<br> [0x8000148c]:sw t6, 48(ra)<br> [0x80001490]:sw t5, 56(ra)<br> [0x80001494]:sw a7, 64(ra)<br>                                         |
|  78|[0x80011f08]<br>0x00000003<br> [0x80011f20]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800014c0]:fadd.d t5, t3, s10, dyn<br> [0x800014c4]:csrrs a7, fcsr, zero<br> [0x800014c8]:sw t5, 72(ra)<br> [0x800014cc]:sw t6, 80(ra)<br> [0x800014d0]:sw t5, 88(ra)<br> [0x800014d4]:sw a7, 96(ra)<br>                                         |
|  79|[0x80011f28]<br>0xFFFFFFFE<br> [0x80011f40]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001504]:fadd.d t5, t3, s10, dyn<br> [0x80001508]:csrrs a7, fcsr, zero<br> [0x8000150c]:sw t5, 104(ra)<br> [0x80001510]:sw t6, 112(ra)<br> [0x80001514]:sw t5, 120(ra)<br> [0x80001518]:sw a7, 128(ra)<br>                                     |
|  80|[0x80011f48]<br>0x00000000<br> [0x80011f60]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001548]:fadd.d t5, t3, s10, dyn<br> [0x8000154c]:csrrs a7, fcsr, zero<br> [0x80001550]:sw t5, 136(ra)<br> [0x80001554]:sw t6, 144(ra)<br> [0x80001558]:sw t5, 152(ra)<br> [0x8000155c]:sw a7, 160(ra)<br>                                     |
|  81|[0x80011f68]<br>0xFFFFFFFF<br> [0x80011f80]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001588]:fadd.d t5, t3, s10, dyn<br> [0x8000158c]:csrrs a7, fcsr, zero<br> [0x80001590]:sw t5, 168(ra)<br> [0x80001594]:sw t6, 176(ra)<br> [0x80001598]:sw t5, 184(ra)<br> [0x8000159c]:sw a7, 192(ra)<br>                                     |
|  82|[0x80011f88]<br>0x00000001<br> [0x80011fa0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800015c8]:fadd.d t5, t3, s10, dyn<br> [0x800015cc]:csrrs a7, fcsr, zero<br> [0x800015d0]:sw t5, 200(ra)<br> [0x800015d4]:sw t6, 208(ra)<br> [0x800015d8]:sw t5, 216(ra)<br> [0x800015dc]:sw a7, 224(ra)<br>                                     |
|  83|[0x80011fa8]<br>0x00000001<br> [0x80011fc0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001608]:fadd.d t5, t3, s10, dyn<br> [0x8000160c]:csrrs a7, fcsr, zero<br> [0x80001610]:sw t5, 232(ra)<br> [0x80001614]:sw t6, 240(ra)<br> [0x80001618]:sw t5, 248(ra)<br> [0x8000161c]:sw a7, 256(ra)<br>                                     |
|  84|[0x80011fc8]<br>0x00000003<br> [0x80011fe0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001648]:fadd.d t5, t3, s10, dyn<br> [0x8000164c]:csrrs a7, fcsr, zero<br> [0x80001650]:sw t5, 264(ra)<br> [0x80001654]:sw t6, 272(ra)<br> [0x80001658]:sw t5, 280(ra)<br> [0x8000165c]:sw a7, 288(ra)<br>                                     |
|  85|[0x80011fe8]<br>0xFFFFFFFF<br> [0x80012000]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000168c]:fadd.d t5, t3, s10, dyn<br> [0x80001690]:csrrs a7, fcsr, zero<br> [0x80001694]:sw t5, 296(ra)<br> [0x80001698]:sw t6, 304(ra)<br> [0x8000169c]:sw t5, 312(ra)<br> [0x800016a0]:sw a7, 320(ra)<br>                                     |
|  86|[0x80012008]<br>0xFFFFFFFF<br> [0x80012020]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800016d0]:fadd.d t5, t3, s10, dyn<br> [0x800016d4]:csrrs a7, fcsr, zero<br> [0x800016d8]:sw t5, 328(ra)<br> [0x800016dc]:sw t6, 336(ra)<br> [0x800016e0]:sw t5, 344(ra)<br> [0x800016e4]:sw a7, 352(ra)<br>                                     |
|  87|[0x80012028]<br>0x00000000<br> [0x80012040]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001710]:fadd.d t5, t3, s10, dyn<br> [0x80001714]:csrrs a7, fcsr, zero<br> [0x80001718]:sw t5, 360(ra)<br> [0x8000171c]:sw t6, 368(ra)<br> [0x80001720]:sw t5, 376(ra)<br> [0x80001724]:sw a7, 384(ra)<br>                                     |
|  88|[0x80012048]<br>0x00000000<br> [0x80012060]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001750]:fadd.d t5, t3, s10, dyn<br> [0x80001754]:csrrs a7, fcsr, zero<br> [0x80001758]:sw t5, 392(ra)<br> [0x8000175c]:sw t6, 400(ra)<br> [0x80001760]:sw t5, 408(ra)<br> [0x80001764]:sw a7, 416(ra)<br>                                     |
|  89|[0x80012068]<br>0x00000000<br> [0x80012080]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001790]:fadd.d t5, t3, s10, dyn<br> [0x80001794]:csrrs a7, fcsr, zero<br> [0x80001798]:sw t5, 424(ra)<br> [0x8000179c]:sw t6, 432(ra)<br> [0x800017a0]:sw t5, 440(ra)<br> [0x800017a4]:sw a7, 448(ra)<br>                                     |
|  90|[0x80012088]<br>0x00000000<br> [0x800120a0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800017d0]:fadd.d t5, t3, s10, dyn<br> [0x800017d4]:csrrs a7, fcsr, zero<br> [0x800017d8]:sw t5, 456(ra)<br> [0x800017dc]:sw t6, 464(ra)<br> [0x800017e0]:sw t5, 472(ra)<br> [0x800017e4]:sw a7, 480(ra)<br>                                     |
|  91|[0x800120a8]<br>0x00000000<br> [0x800120c0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001810]:fadd.d t5, t3, s10, dyn<br> [0x80001814]:csrrs a7, fcsr, zero<br> [0x80001818]:sw t5, 488(ra)<br> [0x8000181c]:sw t6, 496(ra)<br> [0x80001820]:sw t5, 504(ra)<br> [0x80001824]:sw a7, 512(ra)<br>                                     |
|  92|[0x800120c8]<br>0x00000000<br> [0x800120e0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001850]:fadd.d t5, t3, s10, dyn<br> [0x80001854]:csrrs a7, fcsr, zero<br> [0x80001858]:sw t5, 520(ra)<br> [0x8000185c]:sw t6, 528(ra)<br> [0x80001860]:sw t5, 536(ra)<br> [0x80001864]:sw a7, 544(ra)<br>                                     |
|  93|[0x800120e8]<br>0x00000000<br> [0x80012100]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001890]:fadd.d t5, t3, s10, dyn<br> [0x80001894]:csrrs a7, fcsr, zero<br> [0x80001898]:sw t5, 552(ra)<br> [0x8000189c]:sw t6, 560(ra)<br> [0x800018a0]:sw t5, 568(ra)<br> [0x800018a4]:sw a7, 576(ra)<br>                                     |
|  94|[0x80012108]<br>0x00000000<br> [0x80012120]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800018d0]:fadd.d t5, t3, s10, dyn<br> [0x800018d4]:csrrs a7, fcsr, zero<br> [0x800018d8]:sw t5, 584(ra)<br> [0x800018dc]:sw t6, 592(ra)<br> [0x800018e0]:sw t5, 600(ra)<br> [0x800018e4]:sw a7, 608(ra)<br>                                     |
|  95|[0x80012128]<br>0x00000000<br> [0x80012140]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001910]:fadd.d t5, t3, s10, dyn<br> [0x80001914]:csrrs a7, fcsr, zero<br> [0x80001918]:sw t5, 616(ra)<br> [0x8000191c]:sw t6, 624(ra)<br> [0x80001920]:sw t5, 632(ra)<br> [0x80001924]:sw a7, 640(ra)<br>                                     |
|  96|[0x80012148]<br>0x00000000<br> [0x80012160]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001950]:fadd.d t5, t3, s10, dyn<br> [0x80001954]:csrrs a7, fcsr, zero<br> [0x80001958]:sw t5, 648(ra)<br> [0x8000195c]:sw t6, 656(ra)<br> [0x80001960]:sw t5, 664(ra)<br> [0x80001964]:sw a7, 672(ra)<br>                                     |
|  97|[0x80012168]<br>0x00000002<br> [0x80012180]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001990]:fadd.d t5, t3, s10, dyn<br> [0x80001994]:csrrs a7, fcsr, zero<br> [0x80001998]:sw t5, 680(ra)<br> [0x8000199c]:sw t6, 688(ra)<br> [0x800019a0]:sw t5, 696(ra)<br> [0x800019a4]:sw a7, 704(ra)<br>                                     |
|  98|[0x80012188]<br>0x00000002<br> [0x800121a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800019d0]:fadd.d t5, t3, s10, dyn<br> [0x800019d4]:csrrs a7, fcsr, zero<br> [0x800019d8]:sw t5, 712(ra)<br> [0x800019dc]:sw t6, 720(ra)<br> [0x800019e0]:sw t5, 728(ra)<br> [0x800019e4]:sw a7, 736(ra)<br>                                     |
|  99|[0x800121a8]<br>0x00000003<br> [0x800121c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001a10]:fadd.d t5, t3, s10, dyn<br> [0x80001a14]:csrrs a7, fcsr, zero<br> [0x80001a18]:sw t5, 744(ra)<br> [0x80001a1c]:sw t6, 752(ra)<br> [0x80001a20]:sw t5, 760(ra)<br> [0x80001a24]:sw a7, 768(ra)<br>                                     |
| 100|[0x800121c8]<br>0x00000001<br> [0x800121e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001a50]:fadd.d t5, t3, s10, dyn<br> [0x80001a54]:csrrs a7, fcsr, zero<br> [0x80001a58]:sw t5, 776(ra)<br> [0x80001a5c]:sw t6, 784(ra)<br> [0x80001a60]:sw t5, 792(ra)<br> [0x80001a64]:sw a7, 800(ra)<br>                                     |
| 101|[0x800121e8]<br>0x00000004<br> [0x80012200]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001a90]:fadd.d t5, t3, s10, dyn<br> [0x80001a94]:csrrs a7, fcsr, zero<br> [0x80001a98]:sw t5, 808(ra)<br> [0x80001a9c]:sw t6, 816(ra)<br> [0x80001aa0]:sw t5, 824(ra)<br> [0x80001aa4]:sw a7, 832(ra)<br>                                     |
| 102|[0x80012208]<br>0x00000000<br> [0x80012220]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001ad0]:fadd.d t5, t3, s10, dyn<br> [0x80001ad4]:csrrs a7, fcsr, zero<br> [0x80001ad8]:sw t5, 840(ra)<br> [0x80001adc]:sw t6, 848(ra)<br> [0x80001ae0]:sw t5, 856(ra)<br> [0x80001ae4]:sw a7, 864(ra)<br>                                     |
| 103|[0x80012228]<br>0x00000001<br> [0x80012240]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001b14]:fadd.d t5, t3, s10, dyn<br> [0x80001b18]:csrrs a7, fcsr, zero<br> [0x80001b1c]:sw t5, 872(ra)<br> [0x80001b20]:sw t6, 880(ra)<br> [0x80001b24]:sw t5, 888(ra)<br> [0x80001b28]:sw a7, 896(ra)<br>                                     |
| 104|[0x80012248]<br>0xFFFFFFFD<br> [0x80012260]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001b58]:fadd.d t5, t3, s10, dyn<br> [0x80001b5c]:csrrs a7, fcsr, zero<br> [0x80001b60]:sw t5, 904(ra)<br> [0x80001b64]:sw t6, 912(ra)<br> [0x80001b68]:sw t5, 920(ra)<br> [0x80001b6c]:sw a7, 928(ra)<br>                                     |
| 105|[0x80012268]<br>0x00000002<br> [0x80012280]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001b98]:fadd.d t5, t3, s10, dyn<br> [0x80001b9c]:csrrs a7, fcsr, zero<br> [0x80001ba0]:sw t5, 936(ra)<br> [0x80001ba4]:sw t6, 944(ra)<br> [0x80001ba8]:sw t5, 952(ra)<br> [0x80001bac]:sw a7, 960(ra)<br>                                     |
| 106|[0x80012288]<br>0xFFFFFFFE<br> [0x800122a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001bd8]:fadd.d t5, t3, s10, dyn<br> [0x80001bdc]:csrrs a7, fcsr, zero<br> [0x80001be0]:sw t5, 968(ra)<br> [0x80001be4]:sw t6, 976(ra)<br> [0x80001be8]:sw t5, 984(ra)<br> [0x80001bec]:sw a7, 992(ra)<br>                                     |
| 107|[0x800122a8]<br>0x00000004<br> [0x800122c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001c18]:fadd.d t5, t3, s10, dyn<br> [0x80001c1c]:csrrs a7, fcsr, zero<br> [0x80001c20]:sw t5, 1000(ra)<br> [0x80001c24]:sw t6, 1008(ra)<br> [0x80001c28]:sw t5, 1016(ra)<br> [0x80001c2c]:sw a7, 1024(ra)<br>                                 |
| 108|[0x800122c8]<br>0x00000000<br> [0x800122e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001c58]:fadd.d t5, t3, s10, dyn<br> [0x80001c5c]:csrrs a7, fcsr, zero<br> [0x80001c60]:sw t5, 1032(ra)<br> [0x80001c64]:sw t6, 1040(ra)<br> [0x80001c68]:sw t5, 1048(ra)<br> [0x80001c6c]:sw a7, 1056(ra)<br>                                 |
| 109|[0x800122e8]<br>0xFFFFFFFF<br> [0x80012300]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001c9c]:fadd.d t5, t3, s10, dyn<br> [0x80001ca0]:csrrs a7, fcsr, zero<br> [0x80001ca4]:sw t5, 1064(ra)<br> [0x80001ca8]:sw t6, 1072(ra)<br> [0x80001cac]:sw t5, 1080(ra)<br> [0x80001cb0]:sw a7, 1088(ra)<br>                                 |
| 110|[0x80012308]<br>0xFFFFFFFF<br> [0x80012320]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001ce0]:fadd.d t5, t3, s10, dyn<br> [0x80001ce4]:csrrs a7, fcsr, zero<br> [0x80001ce8]:sw t5, 1096(ra)<br> [0x80001cec]:sw t6, 1104(ra)<br> [0x80001cf0]:sw t5, 1112(ra)<br> [0x80001cf4]:sw a7, 1120(ra)<br>                                 |
| 111|[0x80012328]<br>0x00000000<br> [0x80012340]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001d20]:fadd.d t5, t3, s10, dyn<br> [0x80001d24]:csrrs a7, fcsr, zero<br> [0x80001d28]:sw t5, 1128(ra)<br> [0x80001d2c]:sw t6, 1136(ra)<br> [0x80001d30]:sw t5, 1144(ra)<br> [0x80001d34]:sw a7, 1152(ra)<br>                                 |
| 112|[0x80012348]<br>0x00000000<br> [0x80012360]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001d60]:fadd.d t5, t3, s10, dyn<br> [0x80001d64]:csrrs a7, fcsr, zero<br> [0x80001d68]:sw t5, 1160(ra)<br> [0x80001d6c]:sw t6, 1168(ra)<br> [0x80001d70]:sw t5, 1176(ra)<br> [0x80001d74]:sw a7, 1184(ra)<br>                                 |
| 113|[0x80012368]<br>0x00000000<br> [0x80012380]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001da0]:fadd.d t5, t3, s10, dyn<br> [0x80001da4]:csrrs a7, fcsr, zero<br> [0x80001da8]:sw t5, 1192(ra)<br> [0x80001dac]:sw t6, 1200(ra)<br> [0x80001db0]:sw t5, 1208(ra)<br> [0x80001db4]:sw a7, 1216(ra)<br>                                 |
| 114|[0x80012388]<br>0x00000000<br> [0x800123a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001de0]:fadd.d t5, t3, s10, dyn<br> [0x80001de4]:csrrs a7, fcsr, zero<br> [0x80001de8]:sw t5, 1224(ra)<br> [0x80001dec]:sw t6, 1232(ra)<br> [0x80001df0]:sw t5, 1240(ra)<br> [0x80001df4]:sw a7, 1248(ra)<br>                                 |
| 115|[0x800123a8]<br>0x00000000<br> [0x800123c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001e20]:fadd.d t5, t3, s10, dyn<br> [0x80001e24]:csrrs a7, fcsr, zero<br> [0x80001e28]:sw t5, 1256(ra)<br> [0x80001e2c]:sw t6, 1264(ra)<br> [0x80001e30]:sw t5, 1272(ra)<br> [0x80001e34]:sw a7, 1280(ra)<br>                                 |
| 116|[0x800123c8]<br>0x00000000<br> [0x800123e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001e60]:fadd.d t5, t3, s10, dyn<br> [0x80001e64]:csrrs a7, fcsr, zero<br> [0x80001e68]:sw t5, 1288(ra)<br> [0x80001e6c]:sw t6, 1296(ra)<br> [0x80001e70]:sw t5, 1304(ra)<br> [0x80001e74]:sw a7, 1312(ra)<br>                                 |
| 117|[0x800123e8]<br>0x00000000<br> [0x80012400]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001ea0]:fadd.d t5, t3, s10, dyn<br> [0x80001ea4]:csrrs a7, fcsr, zero<br> [0x80001ea8]:sw t5, 1320(ra)<br> [0x80001eac]:sw t6, 1328(ra)<br> [0x80001eb0]:sw t5, 1336(ra)<br> [0x80001eb4]:sw a7, 1344(ra)<br>                                 |
| 118|[0x80012408]<br>0x00000000<br> [0x80012420]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001ee0]:fadd.d t5, t3, s10, dyn<br> [0x80001ee4]:csrrs a7, fcsr, zero<br> [0x80001ee8]:sw t5, 1352(ra)<br> [0x80001eec]:sw t6, 1360(ra)<br> [0x80001ef0]:sw t5, 1368(ra)<br> [0x80001ef4]:sw a7, 1376(ra)<br>                                 |
| 119|[0x80012428]<br>0x00000000<br> [0x80012440]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001f20]:fadd.d t5, t3, s10, dyn<br> [0x80001f24]:csrrs a7, fcsr, zero<br> [0x80001f28]:sw t5, 1384(ra)<br> [0x80001f2c]:sw t6, 1392(ra)<br> [0x80001f30]:sw t5, 1400(ra)<br> [0x80001f34]:sw a7, 1408(ra)<br>                                 |
| 120|[0x80012448]<br>0x00000000<br> [0x80012460]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001f60]:fadd.d t5, t3, s10, dyn<br> [0x80001f64]:csrrs a7, fcsr, zero<br> [0x80001f68]:sw t5, 1416(ra)<br> [0x80001f6c]:sw t6, 1424(ra)<br> [0x80001f70]:sw t5, 1432(ra)<br> [0x80001f74]:sw a7, 1440(ra)<br>                                 |
| 121|[0x80012468]<br>0x00000002<br> [0x80012480]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001fa0]:fadd.d t5, t3, s10, dyn<br> [0x80001fa4]:csrrs a7, fcsr, zero<br> [0x80001fa8]:sw t5, 1448(ra)<br> [0x80001fac]:sw t6, 1456(ra)<br> [0x80001fb0]:sw t5, 1464(ra)<br> [0x80001fb4]:sw a7, 1472(ra)<br>                                 |
| 122|[0x80012488]<br>0x00000002<br> [0x800124a0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80001fe0]:fadd.d t5, t3, s10, dyn<br> [0x80001fe4]:csrrs a7, fcsr, zero<br> [0x80001fe8]:sw t5, 1480(ra)<br> [0x80001fec]:sw t6, 1488(ra)<br> [0x80001ff0]:sw t5, 1496(ra)<br> [0x80001ff4]:sw a7, 1504(ra)<br>                                 |
| 123|[0x800124a8]<br>0x00000001<br> [0x800124c0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002020]:fadd.d t5, t3, s10, dyn<br> [0x80002024]:csrrs a7, fcsr, zero<br> [0x80002028]:sw t5, 1512(ra)<br> [0x8000202c]:sw t6, 1520(ra)<br> [0x80002030]:sw t5, 1528(ra)<br> [0x80002034]:sw a7, 1536(ra)<br>                                 |
| 124|[0x800124c8]<br>0x00000003<br> [0x800124e0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002060]:fadd.d t5, t3, s10, dyn<br> [0x80002064]:csrrs a7, fcsr, zero<br> [0x80002068]:sw t5, 1544(ra)<br> [0x8000206c]:sw t6, 1552(ra)<br> [0x80002070]:sw t5, 1560(ra)<br> [0x80002074]:sw a7, 1568(ra)<br>                                 |
| 125|[0x800124e8]<br>0x00000000<br> [0x80012500]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800020a0]:fadd.d t5, t3, s10, dyn<br> [0x800020a4]:csrrs a7, fcsr, zero<br> [0x800020a8]:sw t5, 1576(ra)<br> [0x800020ac]:sw t6, 1584(ra)<br> [0x800020b0]:sw t5, 1592(ra)<br> [0x800020b4]:sw a7, 1600(ra)<br>                                 |
| 126|[0x80012508]<br>0x00000004<br> [0x80012520]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800020e0]:fadd.d t5, t3, s10, dyn<br> [0x800020e4]:csrrs a7, fcsr, zero<br> [0x800020e8]:sw t5, 1608(ra)<br> [0x800020ec]:sw t6, 1616(ra)<br> [0x800020f0]:sw t5, 1624(ra)<br> [0x800020f4]:sw a7, 1632(ra)<br>                                 |
| 127|[0x80012528]<br>0xFFFFFFFD<br> [0x80012540]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002124]:fadd.d t5, t3, s10, dyn<br> [0x80002128]:csrrs a7, fcsr, zero<br> [0x8000212c]:sw t5, 1640(ra)<br> [0x80002130]:sw t6, 1648(ra)<br> [0x80002134]:sw t5, 1656(ra)<br> [0x80002138]:sw a7, 1664(ra)<br>                                 |
| 128|[0x80012548]<br>0x00000001<br> [0x80012560]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002168]:fadd.d t5, t3, s10, dyn<br> [0x8000216c]:csrrs a7, fcsr, zero<br> [0x80002170]:sw t5, 1672(ra)<br> [0x80002174]:sw t6, 1680(ra)<br> [0x80002178]:sw t5, 1688(ra)<br> [0x8000217c]:sw a7, 1696(ra)<br>                                 |
| 129|[0x80012568]<br>0xFFFFFFFE<br> [0x80012580]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800021a8]:fadd.d t5, t3, s10, dyn<br> [0x800021ac]:csrrs a7, fcsr, zero<br> [0x800021b0]:sw t5, 1704(ra)<br> [0x800021b4]:sw t6, 1712(ra)<br> [0x800021b8]:sw t5, 1720(ra)<br> [0x800021bc]:sw a7, 1728(ra)<br>                                 |
| 130|[0x80012588]<br>0x00000002<br> [0x800125a0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800021e8]:fadd.d t5, t3, s10, dyn<br> [0x800021ec]:csrrs a7, fcsr, zero<br> [0x800021f0]:sw t5, 1736(ra)<br> [0x800021f4]:sw t6, 1744(ra)<br> [0x800021f8]:sw t5, 1752(ra)<br> [0x800021fc]:sw a7, 1760(ra)<br>                                 |
| 131|[0x800125a8]<br>0x00000000<br> [0x800125c0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002228]:fadd.d t5, t3, s10, dyn<br> [0x8000222c]:csrrs a7, fcsr, zero<br> [0x80002230]:sw t5, 1768(ra)<br> [0x80002234]:sw t6, 1776(ra)<br> [0x80002238]:sw t5, 1784(ra)<br> [0x8000223c]:sw a7, 1792(ra)<br>                                 |
| 132|[0x800125c8]<br>0x00000004<br> [0x800125e0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002268]:fadd.d t5, t3, s10, dyn<br> [0x8000226c]:csrrs a7, fcsr, zero<br> [0x80002270]:sw t5, 1800(ra)<br> [0x80002274]:sw t6, 1808(ra)<br> [0x80002278]:sw t5, 1816(ra)<br> [0x8000227c]:sw a7, 1824(ra)<br>                                 |
| 133|[0x800125e8]<br>0xFFFFFFFF<br> [0x80012600]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800022ac]:fadd.d t5, t3, s10, dyn<br> [0x800022b0]:csrrs a7, fcsr, zero<br> [0x800022b4]:sw t5, 1832(ra)<br> [0x800022b8]:sw t6, 1840(ra)<br> [0x800022bc]:sw t5, 1848(ra)<br> [0x800022c0]:sw a7, 1856(ra)<br>                                 |
| 134|[0x80012608]<br>0xFFFFFFFF<br> [0x80012620]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800022f0]:fadd.d t5, t3, s10, dyn<br> [0x800022f4]:csrrs a7, fcsr, zero<br> [0x800022f8]:sw t5, 1864(ra)<br> [0x800022fc]:sw t6, 1872(ra)<br> [0x80002300]:sw t5, 1880(ra)<br> [0x80002304]:sw a7, 1888(ra)<br>                                 |
| 135|[0x80012628]<br>0x00000000<br> [0x80012640]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002330]:fadd.d t5, t3, s10, dyn<br> [0x80002334]:csrrs a7, fcsr, zero<br> [0x80002338]:sw t5, 1896(ra)<br> [0x8000233c]:sw t6, 1904(ra)<br> [0x80002340]:sw t5, 1912(ra)<br> [0x80002344]:sw a7, 1920(ra)<br>                                 |
| 136|[0x80012648]<br>0x00000000<br> [0x80012660]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002370]:fadd.d t5, t3, s10, dyn<br> [0x80002374]:csrrs a7, fcsr, zero<br> [0x80002378]:sw t5, 1928(ra)<br> [0x8000237c]:sw t6, 1936(ra)<br> [0x80002380]:sw t5, 1944(ra)<br> [0x80002384]:sw a7, 1952(ra)<br>                                 |
| 137|[0x80012668]<br>0x00000000<br> [0x80012680]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800023b0]:fadd.d t5, t3, s10, dyn<br> [0x800023b4]:csrrs a7, fcsr, zero<br> [0x800023b8]:sw t5, 1960(ra)<br> [0x800023bc]:sw t6, 1968(ra)<br> [0x800023c0]:sw t5, 1976(ra)<br> [0x800023c4]:sw a7, 1984(ra)<br>                                 |
| 138|[0x80012688]<br>0x00000000<br> [0x800126a0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800023f0]:fadd.d t5, t3, s10, dyn<br> [0x800023f4]:csrrs a7, fcsr, zero<br> [0x800023f8]:sw t5, 1992(ra)<br> [0x800023fc]:sw t6, 2000(ra)<br> [0x80002400]:sw t5, 2008(ra)<br> [0x80002404]:sw a7, 2016(ra)<br>                                 |
| 139|[0x800126a8]<br>0x00000000<br> [0x800126c0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002430]:fadd.d t5, t3, s10, dyn<br> [0x80002434]:csrrs a7, fcsr, zero<br> [0x80002438]:sw t5, 2024(ra)<br> [0x8000243c]:sw t6, 2032(ra)<br> [0x80002440]:sw t5, 2040(ra)<br> [0x80002444]:addi ra, ra, 2040<br> [0x80002448]:sw a7, 8(ra)<br> |
| 140|[0x800126c8]<br>0x00000000<br> [0x800126e0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800024b4]:fadd.d t5, t3, s10, dyn<br> [0x800024b8]:csrrs a7, fcsr, zero<br> [0x800024bc]:sw t5, 16(ra)<br> [0x800024c0]:sw t6, 24(ra)<br> [0x800024c4]:sw t5, 32(ra)<br> [0x800024c8]:sw a7, 40(ra)<br>                                         |
| 141|[0x800126e8]<br>0x00000000<br> [0x80012700]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002534]:fadd.d t5, t3, s10, dyn<br> [0x80002538]:csrrs a7, fcsr, zero<br> [0x8000253c]:sw t5, 48(ra)<br> [0x80002540]:sw t6, 56(ra)<br> [0x80002544]:sw t5, 64(ra)<br> [0x80002548]:sw a7, 72(ra)<br>                                         |
| 142|[0x80012708]<br>0x00000000<br> [0x80012720]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800025b4]:fadd.d t5, t3, s10, dyn<br> [0x800025b8]:csrrs a7, fcsr, zero<br> [0x800025bc]:sw t5, 80(ra)<br> [0x800025c0]:sw t6, 88(ra)<br> [0x800025c4]:sw t5, 96(ra)<br> [0x800025c8]:sw a7, 104(ra)<br>                                        |
| 143|[0x80012728]<br>0x00000000<br> [0x80012740]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002634]:fadd.d t5, t3, s10, dyn<br> [0x80002638]:csrrs a7, fcsr, zero<br> [0x8000263c]:sw t5, 112(ra)<br> [0x80002640]:sw t6, 120(ra)<br> [0x80002644]:sw t5, 128(ra)<br> [0x80002648]:sw a7, 136(ra)<br>                                     |
| 144|[0x80012748]<br>0x00000000<br> [0x80012760]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800026b4]:fadd.d t5, t3, s10, dyn<br> [0x800026b8]:csrrs a7, fcsr, zero<br> [0x800026bc]:sw t5, 144(ra)<br> [0x800026c0]:sw t6, 152(ra)<br> [0x800026c4]:sw t5, 160(ra)<br> [0x800026c8]:sw a7, 168(ra)<br>                                     |
| 145|[0x80012768]<br>0xFFFFFFFF<br> [0x80012780]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002738]:fadd.d t5, t3, s10, dyn<br> [0x8000273c]:csrrs a7, fcsr, zero<br> [0x80002740]:sw t5, 176(ra)<br> [0x80002744]:sw t6, 184(ra)<br> [0x80002748]:sw t5, 192(ra)<br> [0x8000274c]:sw a7, 200(ra)<br>                                     |
| 146|[0x80012788]<br>0xFFFFFFFF<br> [0x800127a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800027bc]:fadd.d t5, t3, s10, dyn<br> [0x800027c0]:csrrs a7, fcsr, zero<br> [0x800027c4]:sw t5, 208(ra)<br> [0x800027c8]:sw t6, 216(ra)<br> [0x800027cc]:sw t5, 224(ra)<br> [0x800027d0]:sw a7, 232(ra)<br>                                     |
| 147|[0x800127a8]<br>0x00000000<br> [0x800127c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002840]:fadd.d t5, t3, s10, dyn<br> [0x80002844]:csrrs a7, fcsr, zero<br> [0x80002848]:sw t5, 240(ra)<br> [0x8000284c]:sw t6, 248(ra)<br> [0x80002850]:sw t5, 256(ra)<br> [0x80002854]:sw a7, 264(ra)<br>                                     |
| 148|[0x800127c8]<br>0xFFFFFFFE<br> [0x800127e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800028c4]:fadd.d t5, t3, s10, dyn<br> [0x800028c8]:csrrs a7, fcsr, zero<br> [0x800028cc]:sw t5, 272(ra)<br> [0x800028d0]:sw t6, 280(ra)<br> [0x800028d4]:sw t5, 288(ra)<br> [0x800028d8]:sw a7, 296(ra)<br>                                     |
| 149|[0x800127e8]<br>0x00000001<br> [0x80012800]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002948]:fadd.d t5, t3, s10, dyn<br> [0x8000294c]:csrrs a7, fcsr, zero<br> [0x80002950]:sw t5, 304(ra)<br> [0x80002954]:sw t6, 312(ra)<br> [0x80002958]:sw t5, 320(ra)<br> [0x8000295c]:sw a7, 328(ra)<br>                                     |
| 150|[0x80012808]<br>0xFFFFFFFD<br> [0x80012820]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800029cc]:fadd.d t5, t3, s10, dyn<br> [0x800029d0]:csrrs a7, fcsr, zero<br> [0x800029d4]:sw t5, 336(ra)<br> [0x800029d8]:sw t6, 344(ra)<br> [0x800029dc]:sw t5, 352(ra)<br> [0x800029e0]:sw a7, 360(ra)<br>                                     |
| 151|[0x80012828]<br>0xFFFFFFFE<br> [0x80012840]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002a54]:fadd.d t5, t3, s10, dyn<br> [0x80002a58]:csrrs a7, fcsr, zero<br> [0x80002a5c]:sw t5, 368(ra)<br> [0x80002a60]:sw t6, 376(ra)<br> [0x80002a64]:sw t5, 384(ra)<br> [0x80002a68]:sw a7, 392(ra)<br>                                     |
| 152|[0x80012848]<br>0x00000000<br> [0x80012860]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002adc]:fadd.d t5, t3, s10, dyn<br> [0x80002ae0]:csrrs a7, fcsr, zero<br> [0x80002ae4]:sw t5, 400(ra)<br> [0x80002ae8]:sw t6, 408(ra)<br> [0x80002aec]:sw t5, 416(ra)<br> [0x80002af0]:sw a7, 424(ra)<br>                                     |
| 153|[0x80012868]<br>0xFFFFFFFF<br> [0x80012880]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002b60]:fadd.d t5, t3, s10, dyn<br> [0x80002b64]:csrrs a7, fcsr, zero<br> [0x80002b68]:sw t5, 432(ra)<br> [0x80002b6c]:sw t6, 440(ra)<br> [0x80002b70]:sw t5, 448(ra)<br> [0x80002b74]:sw a7, 456(ra)<br>                                     |
| 154|[0x80012888]<br>0x00000001<br> [0x800128a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002be4]:fadd.d t5, t3, s10, dyn<br> [0x80002be8]:csrrs a7, fcsr, zero<br> [0x80002bec]:sw t5, 464(ra)<br> [0x80002bf0]:sw t6, 472(ra)<br> [0x80002bf4]:sw t5, 480(ra)<br> [0x80002bf8]:sw a7, 488(ra)<br>                                     |
| 155|[0x800128a8]<br>0x00000000<br> [0x800128c0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002c68]:fadd.d t5, t3, s10, dyn<br> [0x80002c6c]:csrrs a7, fcsr, zero<br> [0x80002c70]:sw t5, 496(ra)<br> [0x80002c74]:sw t6, 504(ra)<br> [0x80002c78]:sw t5, 512(ra)<br> [0x80002c7c]:sw a7, 520(ra)<br>                                     |
| 156|[0x800128c8]<br>0x00000003<br> [0x800128e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002cec]:fadd.d t5, t3, s10, dyn<br> [0x80002cf0]:csrrs a7, fcsr, zero<br> [0x80002cf4]:sw t5, 528(ra)<br> [0x80002cf8]:sw t6, 536(ra)<br> [0x80002cfc]:sw t5, 544(ra)<br> [0x80002d00]:sw a7, 552(ra)<br>                                     |
| 157|[0x800128e8]<br>0xFFFFFFFF<br> [0x80012900]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002d74]:fadd.d t5, t3, s10, dyn<br> [0x80002d78]:csrrs a7, fcsr, zero<br> [0x80002d7c]:sw t5, 560(ra)<br> [0x80002d80]:sw t6, 568(ra)<br> [0x80002d84]:sw t5, 576(ra)<br> [0x80002d88]:sw a7, 584(ra)<br>                                     |
| 158|[0x80012908]<br>0xFFFFFFFF<br> [0x80012920]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002dfc]:fadd.d t5, t3, s10, dyn<br> [0x80002e00]:csrrs a7, fcsr, zero<br> [0x80002e04]:sw t5, 592(ra)<br> [0x80002e08]:sw t6, 600(ra)<br> [0x80002e0c]:sw t5, 608(ra)<br> [0x80002e10]:sw a7, 616(ra)<br>                                     |
| 159|[0x80012928]<br>0x00000000<br> [0x80012940]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002e80]:fadd.d t5, t3, s10, dyn<br> [0x80002e84]:csrrs a7, fcsr, zero<br> [0x80002e88]:sw t5, 624(ra)<br> [0x80002e8c]:sw t6, 632(ra)<br> [0x80002e90]:sw t5, 640(ra)<br> [0x80002e94]:sw a7, 648(ra)<br>                                     |
| 160|[0x80012948]<br>0x00000000<br> [0x80012960]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002f04]:fadd.d t5, t3, s10, dyn<br> [0x80002f08]:csrrs a7, fcsr, zero<br> [0x80002f0c]:sw t5, 656(ra)<br> [0x80002f10]:sw t6, 664(ra)<br> [0x80002f14]:sw t5, 672(ra)<br> [0x80002f18]:sw a7, 680(ra)<br>                                     |
| 161|[0x80012968]<br>0x00000000<br> [0x80012980]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80002f88]:fadd.d t5, t3, s10, dyn<br> [0x80002f8c]:csrrs a7, fcsr, zero<br> [0x80002f90]:sw t5, 688(ra)<br> [0x80002f94]:sw t6, 696(ra)<br> [0x80002f98]:sw t5, 704(ra)<br> [0x80002f9c]:sw a7, 712(ra)<br>                                     |
| 162|[0x80012988]<br>0x00000000<br> [0x800129a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000300c]:fadd.d t5, t3, s10, dyn<br> [0x80003010]:csrrs a7, fcsr, zero<br> [0x80003014]:sw t5, 720(ra)<br> [0x80003018]:sw t6, 728(ra)<br> [0x8000301c]:sw t5, 736(ra)<br> [0x80003020]:sw a7, 744(ra)<br>                                     |
| 163|[0x800129a8]<br>0x00000000<br> [0x800129c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80003090]:fadd.d t5, t3, s10, dyn<br> [0x80003094]:csrrs a7, fcsr, zero<br> [0x80003098]:sw t5, 752(ra)<br> [0x8000309c]:sw t6, 760(ra)<br> [0x800030a0]:sw t5, 768(ra)<br> [0x800030a4]:sw a7, 776(ra)<br>                                     |
| 164|[0x800129c8]<br>0x00000000<br> [0x800129e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80003114]:fadd.d t5, t3, s10, dyn<br> [0x80003118]:csrrs a7, fcsr, zero<br> [0x8000311c]:sw t5, 784(ra)<br> [0x80003120]:sw t6, 792(ra)<br> [0x80003124]:sw t5, 800(ra)<br> [0x80003128]:sw a7, 808(ra)<br>                                     |
| 165|[0x800129e8]<br>0x00000000<br> [0x80012a00]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80003198]:fadd.d t5, t3, s10, dyn<br> [0x8000319c]:csrrs a7, fcsr, zero<br> [0x800031a0]:sw t5, 816(ra)<br> [0x800031a4]:sw t6, 824(ra)<br> [0x800031a8]:sw t5, 832(ra)<br> [0x800031ac]:sw a7, 840(ra)<br>                                     |
| 166|[0x80012a08]<br>0x00000000<br> [0x80012a20]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000321c]:fadd.d t5, t3, s10, dyn<br> [0x80003220]:csrrs a7, fcsr, zero<br> [0x80003224]:sw t5, 848(ra)<br> [0x80003228]:sw t6, 856(ra)<br> [0x8000322c]:sw t5, 864(ra)<br> [0x80003230]:sw a7, 872(ra)<br>                                     |
| 167|[0x80012a28]<br>0x00000000<br> [0x80012a40]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800032a0]:fadd.d t5, t3, s10, dyn<br> [0x800032a4]:csrrs a7, fcsr, zero<br> [0x800032a8]:sw t5, 880(ra)<br> [0x800032ac]:sw t6, 888(ra)<br> [0x800032b0]:sw t5, 896(ra)<br> [0x800032b4]:sw a7, 904(ra)<br>                                     |
| 168|[0x80012a48]<br>0x00000000<br> [0x80012a60]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80003324]:fadd.d t5, t3, s10, dyn<br> [0x80003328]:csrrs a7, fcsr, zero<br> [0x8000332c]:sw t5, 912(ra)<br> [0x80003330]:sw t6, 920(ra)<br> [0x80003334]:sw t5, 928(ra)<br> [0x80003338]:sw a7, 936(ra)<br>                                     |
| 169|[0x80012a68]<br>0xFFFFFFFF<br> [0x80012a80]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800033a8]:fadd.d t5, t3, s10, dyn<br> [0x800033ac]:csrrs a7, fcsr, zero<br> [0x800033b0]:sw t5, 944(ra)<br> [0x800033b4]:sw t6, 952(ra)<br> [0x800033b8]:sw t5, 960(ra)<br> [0x800033bc]:sw a7, 968(ra)<br>                                     |
| 170|[0x80012a88]<br>0xFFFFFFFF<br> [0x80012aa0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000342c]:fadd.d t5, t3, s10, dyn<br> [0x80003430]:csrrs a7, fcsr, zero<br> [0x80003434]:sw t5, 976(ra)<br> [0x80003438]:sw t6, 984(ra)<br> [0x8000343c]:sw t5, 992(ra)<br> [0x80003440]:sw a7, 1000(ra)<br>                                    |
| 171|[0x80012aa8]<br>0xFFFFFFFE<br> [0x80012ac0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800034b0]:fadd.d t5, t3, s10, dyn<br> [0x800034b4]:csrrs a7, fcsr, zero<br> [0x800034b8]:sw t5, 1008(ra)<br> [0x800034bc]:sw t6, 1016(ra)<br> [0x800034c0]:sw t5, 1024(ra)<br> [0x800034c4]:sw a7, 1032(ra)<br>                                 |
| 172|[0x80012ac8]<br>0x00000000<br> [0x80012ae0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80003534]:fadd.d t5, t3, s10, dyn<br> [0x80003538]:csrrs a7, fcsr, zero<br> [0x8000353c]:sw t5, 1040(ra)<br> [0x80003540]:sw t6, 1048(ra)<br> [0x80003544]:sw t5, 1056(ra)<br> [0x80003548]:sw a7, 1064(ra)<br>                                 |
| 173|[0x80012ae8]<br>0xFFFFFFFD<br> [0x80012b00]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800035b8]:fadd.d t5, t3, s10, dyn<br> [0x800035bc]:csrrs a7, fcsr, zero<br> [0x800035c0]:sw t5, 1072(ra)<br> [0x800035c4]:sw t6, 1080(ra)<br> [0x800035c8]:sw t5, 1088(ra)<br> [0x800035cc]:sw a7, 1096(ra)<br>                                 |
| 174|[0x80012b08]<br>0x00000001<br> [0x80012b20]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000363c]:fadd.d t5, t3, s10, dyn<br> [0x80003640]:csrrs a7, fcsr, zero<br> [0x80003644]:sw t5, 1104(ra)<br> [0x80003648]:sw t6, 1112(ra)<br> [0x8000364c]:sw t5, 1120(ra)<br> [0x80003650]:sw a7, 1128(ra)<br>                                 |
| 175|[0x80012b28]<br>0x00000000<br> [0x80012b40]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800036c4]:fadd.d t5, t3, s10, dyn<br> [0x800036c8]:csrrs a7, fcsr, zero<br> [0x800036cc]:sw t5, 1136(ra)<br> [0x800036d0]:sw t6, 1144(ra)<br> [0x800036d4]:sw t5, 1152(ra)<br> [0x800036d8]:sw a7, 1160(ra)<br>                                 |
| 176|[0x80012b48]<br>0xFFFFFFFE<br> [0x80012b60]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000374c]:fadd.d t5, t3, s10, dyn<br> [0x80003750]:csrrs a7, fcsr, zero<br> [0x80003754]:sw t5, 1168(ra)<br> [0x80003758]:sw t6, 1176(ra)<br> [0x8000375c]:sw t5, 1184(ra)<br> [0x80003760]:sw a7, 1192(ra)<br>                                 |
| 177|[0x80012b68]<br>0x00000001<br> [0x80012b80]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800037d0]:fadd.d t5, t3, s10, dyn<br> [0x800037d4]:csrrs a7, fcsr, zero<br> [0x800037d8]:sw t5, 1200(ra)<br> [0x800037dc]:sw t6, 1208(ra)<br> [0x800037e0]:sw t5, 1216(ra)<br> [0x800037e4]:sw a7, 1224(ra)<br>                                 |
| 178|[0x80012b88]<br>0xFFFFFFFF<br> [0x80012ba0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80003854]:fadd.d t5, t3, s10, dyn<br> [0x80003858]:csrrs a7, fcsr, zero<br> [0x8000385c]:sw t5, 1232(ra)<br> [0x80003860]:sw t6, 1240(ra)<br> [0x80003864]:sw t5, 1248(ra)<br> [0x80003868]:sw a7, 1256(ra)<br>                                 |
| 179|[0x80012ba8]<br>0x00000003<br> [0x80012bc0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800038d8]:fadd.d t5, t3, s10, dyn<br> [0x800038dc]:csrrs a7, fcsr, zero<br> [0x800038e0]:sw t5, 1264(ra)<br> [0x800038e4]:sw t6, 1272(ra)<br> [0x800038e8]:sw t5, 1280(ra)<br> [0x800038ec]:sw a7, 1288(ra)<br>                                 |
| 180|[0x80012bc8]<br>0x00000000<br> [0x80012be0]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000395c]:fadd.d t5, t3, s10, dyn<br> [0x80003960]:csrrs a7, fcsr, zero<br> [0x80003964]:sw t5, 1296(ra)<br> [0x80003968]:sw t6, 1304(ra)<br> [0x8000396c]:sw t5, 1312(ra)<br> [0x80003970]:sw a7, 1320(ra)<br>                                 |
| 181|[0x80012be8]<br>0xFFFFFFFF<br> [0x80012c00]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800039e4]:fadd.d t5, t3, s10, dyn<br> [0x800039e8]:csrrs a7, fcsr, zero<br> [0x800039ec]:sw t5, 1328(ra)<br> [0x800039f0]:sw t6, 1336(ra)<br> [0x800039f4]:sw t5, 1344(ra)<br> [0x800039f8]:sw a7, 1352(ra)<br>                                 |
| 182|[0x80012c08]<br>0xFFFFFFFF<br> [0x80012c20]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80003a6c]:fadd.d t5, t3, s10, dyn<br> [0x80003a70]:csrrs a7, fcsr, zero<br> [0x80003a74]:sw t5, 1360(ra)<br> [0x80003a78]:sw t6, 1368(ra)<br> [0x80003a7c]:sw t5, 1376(ra)<br> [0x80003a80]:sw a7, 1384(ra)<br>                                 |
| 183|[0x80012c28]<br>0x00000000<br> [0x80012c40]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80003af0]:fadd.d t5, t3, s10, dyn<br> [0x80003af4]:csrrs a7, fcsr, zero<br> [0x80003af8]:sw t5, 1392(ra)<br> [0x80003afc]:sw t6, 1400(ra)<br> [0x80003b00]:sw t5, 1408(ra)<br> [0x80003b04]:sw a7, 1416(ra)<br>                                 |
| 184|[0x80012c48]<br>0x00000000<br> [0x80012c60]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80003b74]:fadd.d t5, t3, s10, dyn<br> [0x80003b78]:csrrs a7, fcsr, zero<br> [0x80003b7c]:sw t5, 1424(ra)<br> [0x80003b80]:sw t6, 1432(ra)<br> [0x80003b84]:sw t5, 1440(ra)<br> [0x80003b88]:sw a7, 1448(ra)<br>                                 |
| 185|[0x80012c68]<br>0x00000000<br> [0x80012c80]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80003bf8]:fadd.d t5, t3, s10, dyn<br> [0x80003bfc]:csrrs a7, fcsr, zero<br> [0x80003c00]:sw t5, 1456(ra)<br> [0x80003c04]:sw t6, 1464(ra)<br> [0x80003c08]:sw t5, 1472(ra)<br> [0x80003c0c]:sw a7, 1480(ra)<br>                                 |
| 186|[0x80012c88]<br>0x00000000<br> [0x80012ca0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80003c7c]:fadd.d t5, t3, s10, dyn<br> [0x80003c80]:csrrs a7, fcsr, zero<br> [0x80003c84]:sw t5, 1488(ra)<br> [0x80003c88]:sw t6, 1496(ra)<br> [0x80003c8c]:sw t5, 1504(ra)<br> [0x80003c90]:sw a7, 1512(ra)<br>                                 |
| 187|[0x80012ca8]<br>0x00000000<br> [0x80012cc0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80003d00]:fadd.d t5, t3, s10, dyn<br> [0x80003d04]:csrrs a7, fcsr, zero<br> [0x80003d08]:sw t5, 1520(ra)<br> [0x80003d0c]:sw t6, 1528(ra)<br> [0x80003d10]:sw t5, 1536(ra)<br> [0x80003d14]:sw a7, 1544(ra)<br>                                 |
| 188|[0x80012cc8]<br>0x00000000<br> [0x80012ce0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80003d84]:fadd.d t5, t3, s10, dyn<br> [0x80003d88]:csrrs a7, fcsr, zero<br> [0x80003d8c]:sw t5, 1552(ra)<br> [0x80003d90]:sw t6, 1560(ra)<br> [0x80003d94]:sw t5, 1568(ra)<br> [0x80003d98]:sw a7, 1576(ra)<br>                                 |
| 189|[0x80012ce8]<br>0x00000000<br> [0x80012d00]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80003e08]:fadd.d t5, t3, s10, dyn<br> [0x80003e0c]:csrrs a7, fcsr, zero<br> [0x80003e10]:sw t5, 1584(ra)<br> [0x80003e14]:sw t6, 1592(ra)<br> [0x80003e18]:sw t5, 1600(ra)<br> [0x80003e1c]:sw a7, 1608(ra)<br>                                 |
| 190|[0x80012d08]<br>0x00000000<br> [0x80012d20]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80003e8c]:fadd.d t5, t3, s10, dyn<br> [0x80003e90]:csrrs a7, fcsr, zero<br> [0x80003e94]:sw t5, 1616(ra)<br> [0x80003e98]:sw t6, 1624(ra)<br> [0x80003e9c]:sw t5, 1632(ra)<br> [0x80003ea0]:sw a7, 1640(ra)<br>                                 |
| 191|[0x80012d28]<br>0x00000000<br> [0x80012d40]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80003f10]:fadd.d t5, t3, s10, dyn<br> [0x80003f14]:csrrs a7, fcsr, zero<br> [0x80003f18]:sw t5, 1648(ra)<br> [0x80003f1c]:sw t6, 1656(ra)<br> [0x80003f20]:sw t5, 1664(ra)<br> [0x80003f24]:sw a7, 1672(ra)<br>                                 |
| 192|[0x80012d48]<br>0x00000000<br> [0x80012d60]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80003f94]:fadd.d t5, t3, s10, dyn<br> [0x80003f98]:csrrs a7, fcsr, zero<br> [0x80003f9c]:sw t5, 1680(ra)<br> [0x80003fa0]:sw t6, 1688(ra)<br> [0x80003fa4]:sw t5, 1696(ra)<br> [0x80003fa8]:sw a7, 1704(ra)<br>                                 |
| 193|[0x80012d68]<br>0x00000000<br> [0x80012d80]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004014]:fadd.d t5, t3, s10, dyn<br> [0x80004018]:csrrs a7, fcsr, zero<br> [0x8000401c]:sw t5, 1712(ra)<br> [0x80004020]:sw t6, 1720(ra)<br> [0x80004024]:sw t5, 1728(ra)<br> [0x80004028]:sw a7, 1736(ra)<br>                                 |
| 194|[0x80012d88]<br>0x00000000<br> [0x80012da0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004094]:fadd.d t5, t3, s10, dyn<br> [0x80004098]:csrrs a7, fcsr, zero<br> [0x8000409c]:sw t5, 1744(ra)<br> [0x800040a0]:sw t6, 1752(ra)<br> [0x800040a4]:sw t5, 1760(ra)<br> [0x800040a8]:sw a7, 1768(ra)<br>                                 |
| 195|[0x80012da8]<br>0x00000001<br> [0x80012dc0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004114]:fadd.d t5, t3, s10, dyn<br> [0x80004118]:csrrs a7, fcsr, zero<br> [0x8000411c]:sw t5, 1776(ra)<br> [0x80004120]:sw t6, 1784(ra)<br> [0x80004124]:sw t5, 1792(ra)<br> [0x80004128]:sw a7, 1800(ra)<br>                                 |
| 196|[0x80012dc8]<br>0xFFFFFFFF<br> [0x80012de0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004194]:fadd.d t5, t3, s10, dyn<br> [0x80004198]:csrrs a7, fcsr, zero<br> [0x8000419c]:sw t5, 1808(ra)<br> [0x800041a0]:sw t6, 1816(ra)<br> [0x800041a4]:sw t5, 1824(ra)<br> [0x800041a8]:sw a7, 1832(ra)<br>                                 |
| 197|[0x80012de8]<br>0x00000002<br> [0x80012e00]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004214]:fadd.d t5, t3, s10, dyn<br> [0x80004218]:csrrs a7, fcsr, zero<br> [0x8000421c]:sw t5, 1840(ra)<br> [0x80004220]:sw t6, 1848(ra)<br> [0x80004224]:sw t5, 1856(ra)<br> [0x80004228]:sw a7, 1864(ra)<br>                                 |
| 198|[0x80012e08]<br>0xFFFFFFFE<br> [0x80012e20]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004294]:fadd.d t5, t3, s10, dyn<br> [0x80004298]:csrrs a7, fcsr, zero<br> [0x8000429c]:sw t5, 1872(ra)<br> [0x800042a0]:sw t6, 1880(ra)<br> [0x800042a4]:sw t5, 1888(ra)<br> [0x800042a8]:sw a7, 1896(ra)<br>                                 |
| 199|[0x80012e28]<br>0xFFFFFFFF<br> [0x80012e40]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004318]:fadd.d t5, t3, s10, dyn<br> [0x8000431c]:csrrs a7, fcsr, zero<br> [0x80004320]:sw t5, 1904(ra)<br> [0x80004324]:sw t6, 1912(ra)<br> [0x80004328]:sw t5, 1920(ra)<br> [0x8000432c]:sw a7, 1928(ra)<br>                                 |
| 200|[0x80012e48]<br>0x00000001<br> [0x80012e60]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000439c]:fadd.d t5, t3, s10, dyn<br> [0x800043a0]:csrrs a7, fcsr, zero<br> [0x800043a4]:sw t5, 1936(ra)<br> [0x800043a8]:sw t6, 1944(ra)<br> [0x800043ac]:sw t5, 1952(ra)<br> [0x800043b0]:sw a7, 1960(ra)<br>                                 |
| 201|[0x80012e68]<br>0x00000000<br> [0x80012e80]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000441c]:fadd.d t5, t3, s10, dyn<br> [0x80004420]:csrrs a7, fcsr, zero<br> [0x80004424]:sw t5, 1968(ra)<br> [0x80004428]:sw t6, 1976(ra)<br> [0x8000442c]:sw t5, 1984(ra)<br> [0x80004430]:sw a7, 1992(ra)<br>                                 |
| 202|[0x80012e88]<br>0x00000000<br> [0x80012ea0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000449c]:fadd.d t5, t3, s10, dyn<br> [0x800044a0]:csrrs a7, fcsr, zero<br> [0x800044a4]:sw t5, 2000(ra)<br> [0x800044a8]:sw t6, 2008(ra)<br> [0x800044ac]:sw t5, 2016(ra)<br> [0x800044b0]:sw a7, 2024(ra)<br>                                 |
| 203|[0x80012ea8]<br>0x00000001<br> [0x80012ec0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000451c]:fadd.d t5, t3, s10, dyn<br> [0x80004520]:csrrs a7, fcsr, zero<br> [0x80004524]:sw t5, 2032(ra)<br> [0x80004528]:sw t6, 2040(ra)<br> [0x8000452c]:addi ra, ra, 2040<br> [0x80004530]:sw t5, 8(ra)<br> [0x80004534]:sw a7, 16(ra)<br>   |
| 204|[0x80012ec8]<br>0x00000002<br> [0x80012ee0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800045a0]:fadd.d t5, t3, s10, dyn<br> [0x800045a4]:csrrs a7, fcsr, zero<br> [0x800045a8]:sw t5, 24(ra)<br> [0x800045ac]:sw t6, 32(ra)<br> [0x800045b0]:sw t5, 40(ra)<br> [0x800045b4]:sw a7, 48(ra)<br>                                         |
| 205|[0x80012ee8]<br>0xFFFFFFFF<br> [0x80012f00]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004624]:fadd.d t5, t3, s10, dyn<br> [0x80004628]:csrrs a7, fcsr, zero<br> [0x8000462c]:sw t5, 56(ra)<br> [0x80004630]:sw t6, 64(ra)<br> [0x80004634]:sw t5, 72(ra)<br> [0x80004638]:sw a7, 80(ra)<br>                                         |
| 206|[0x80012f08]<br>0xFFFFFFFF<br> [0x80012f20]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800046a8]:fadd.d t5, t3, s10, dyn<br> [0x800046ac]:csrrs a7, fcsr, zero<br> [0x800046b0]:sw t5, 88(ra)<br> [0x800046b4]:sw t6, 96(ra)<br> [0x800046b8]:sw t5, 104(ra)<br> [0x800046bc]:sw a7, 112(ra)<br>                                       |
| 207|[0x80012f28]<br>0x00000000<br> [0x80012f40]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004728]:fadd.d t5, t3, s10, dyn<br> [0x8000472c]:csrrs a7, fcsr, zero<br> [0x80004730]:sw t5, 120(ra)<br> [0x80004734]:sw t6, 128(ra)<br> [0x80004738]:sw t5, 136(ra)<br> [0x8000473c]:sw a7, 144(ra)<br>                                     |
| 208|[0x80012f48]<br>0x00000000<br> [0x80012f60]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800047a8]:fadd.d t5, t3, s10, dyn<br> [0x800047ac]:csrrs a7, fcsr, zero<br> [0x800047b0]:sw t5, 152(ra)<br> [0x800047b4]:sw t6, 160(ra)<br> [0x800047b8]:sw t5, 168(ra)<br> [0x800047bc]:sw a7, 176(ra)<br>                                     |
| 209|[0x80012f68]<br>0x00000000<br> [0x80012f80]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004828]:fadd.d t5, t3, s10, dyn<br> [0x8000482c]:csrrs a7, fcsr, zero<br> [0x80004830]:sw t5, 184(ra)<br> [0x80004834]:sw t6, 192(ra)<br> [0x80004838]:sw t5, 200(ra)<br> [0x8000483c]:sw a7, 208(ra)<br>                                     |
| 210|[0x80012f88]<br>0x00000000<br> [0x80012fa0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800048a8]:fadd.d t5, t3, s10, dyn<br> [0x800048ac]:csrrs a7, fcsr, zero<br> [0x800048b0]:sw t5, 216(ra)<br> [0x800048b4]:sw t6, 224(ra)<br> [0x800048b8]:sw t5, 232(ra)<br> [0x800048bc]:sw a7, 240(ra)<br>                                     |
| 211|[0x80012fa8]<br>0x00000000<br> [0x80012fc0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004928]:fadd.d t5, t3, s10, dyn<br> [0x8000492c]:csrrs a7, fcsr, zero<br> [0x80004930]:sw t5, 248(ra)<br> [0x80004934]:sw t6, 256(ra)<br> [0x80004938]:sw t5, 264(ra)<br> [0x8000493c]:sw a7, 272(ra)<br>                                     |
| 212|[0x80012fc8]<br>0x00000000<br> [0x80012fe0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800049a8]:fadd.d t5, t3, s10, dyn<br> [0x800049ac]:csrrs a7, fcsr, zero<br> [0x800049b0]:sw t5, 280(ra)<br> [0x800049b4]:sw t6, 288(ra)<br> [0x800049b8]:sw t5, 296(ra)<br> [0x800049bc]:sw a7, 304(ra)<br>                                     |
| 213|[0x80012fe8]<br>0x00000000<br> [0x80013000]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004a28]:fadd.d t5, t3, s10, dyn<br> [0x80004a2c]:csrrs a7, fcsr, zero<br> [0x80004a30]:sw t5, 312(ra)<br> [0x80004a34]:sw t6, 320(ra)<br> [0x80004a38]:sw t5, 328(ra)<br> [0x80004a3c]:sw a7, 336(ra)<br>                                     |
| 214|[0x80013008]<br>0x00000000<br> [0x80013020]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004aa8]:fadd.d t5, t3, s10, dyn<br> [0x80004aac]:csrrs a7, fcsr, zero<br> [0x80004ab0]:sw t5, 344(ra)<br> [0x80004ab4]:sw t6, 352(ra)<br> [0x80004ab8]:sw t5, 360(ra)<br> [0x80004abc]:sw a7, 368(ra)<br>                                     |
| 215|[0x80013028]<br>0x00000000<br> [0x80013040]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004b28]:fadd.d t5, t3, s10, dyn<br> [0x80004b2c]:csrrs a7, fcsr, zero<br> [0x80004b30]:sw t5, 376(ra)<br> [0x80004b34]:sw t6, 384(ra)<br> [0x80004b38]:sw t5, 392(ra)<br> [0x80004b3c]:sw a7, 400(ra)<br>                                     |
| 216|[0x80013048]<br>0x00000000<br> [0x80013060]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004ba8]:fadd.d t5, t3, s10, dyn<br> [0x80004bac]:csrrs a7, fcsr, zero<br> [0x80004bb0]:sw t5, 408(ra)<br> [0x80004bb4]:sw t6, 416(ra)<br> [0x80004bb8]:sw t5, 424(ra)<br> [0x80004bbc]:sw a7, 432(ra)<br>                                     |
| 217|[0x80013068]<br>0x00000000<br> [0x80013080]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004c28]:fadd.d t5, t3, s10, dyn<br> [0x80004c2c]:csrrs a7, fcsr, zero<br> [0x80004c30]:sw t5, 440(ra)<br> [0x80004c34]:sw t6, 448(ra)<br> [0x80004c38]:sw t5, 456(ra)<br> [0x80004c3c]:sw a7, 464(ra)<br>                                     |
| 218|[0x80013088]<br>0x00000000<br> [0x800130a0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004ca8]:fadd.d t5, t3, s10, dyn<br> [0x80004cac]:csrrs a7, fcsr, zero<br> [0x80004cb0]:sw t5, 472(ra)<br> [0x80004cb4]:sw t6, 480(ra)<br> [0x80004cb8]:sw t5, 488(ra)<br> [0x80004cbc]:sw a7, 496(ra)<br>                                     |
| 219|[0x800130a8]<br>0xFFFFFFFF<br> [0x800130c0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004d28]:fadd.d t5, t3, s10, dyn<br> [0x80004d2c]:csrrs a7, fcsr, zero<br> [0x80004d30]:sw t5, 504(ra)<br> [0x80004d34]:sw t6, 512(ra)<br> [0x80004d38]:sw t5, 520(ra)<br> [0x80004d3c]:sw a7, 528(ra)<br>                                     |
| 220|[0x800130c8]<br>0x00000001<br> [0x800130e0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004da8]:fadd.d t5, t3, s10, dyn<br> [0x80004dac]:csrrs a7, fcsr, zero<br> [0x80004db0]:sw t5, 536(ra)<br> [0x80004db4]:sw t6, 544(ra)<br> [0x80004db8]:sw t5, 552(ra)<br> [0x80004dbc]:sw a7, 560(ra)<br>                                     |
| 221|[0x800130e8]<br>0xFFFFFFFE<br> [0x80013100]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004e28]:fadd.d t5, t3, s10, dyn<br> [0x80004e2c]:csrrs a7, fcsr, zero<br> [0x80004e30]:sw t5, 568(ra)<br> [0x80004e34]:sw t6, 576(ra)<br> [0x80004e38]:sw t5, 584(ra)<br> [0x80004e3c]:sw a7, 592(ra)<br>                                     |
| 222|[0x80013108]<br>0x00000002<br> [0x80013120]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004ea8]:fadd.d t5, t3, s10, dyn<br> [0x80004eac]:csrrs a7, fcsr, zero<br> [0x80004eb0]:sw t5, 600(ra)<br> [0x80004eb4]:sw t6, 608(ra)<br> [0x80004eb8]:sw t5, 616(ra)<br> [0x80004ebc]:sw a7, 624(ra)<br>                                     |
| 223|[0x80013128]<br>0x00000001<br> [0x80013140]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004f2c]:fadd.d t5, t3, s10, dyn<br> [0x80004f30]:csrrs a7, fcsr, zero<br> [0x80004f34]:sw t5, 632(ra)<br> [0x80004f38]:sw t6, 640(ra)<br> [0x80004f3c]:sw t5, 648(ra)<br> [0x80004f40]:sw a7, 656(ra)<br>                                     |
| 224|[0x80013148]<br>0xFFFFFFFF<br> [0x80013160]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80004fb0]:fadd.d t5, t3, s10, dyn<br> [0x80004fb4]:csrrs a7, fcsr, zero<br> [0x80004fb8]:sw t5, 664(ra)<br> [0x80004fbc]:sw t6, 672(ra)<br> [0x80004fc0]:sw t5, 680(ra)<br> [0x80004fc4]:sw a7, 688(ra)<br>                                     |
| 225|[0x80013168]<br>0x00000000<br> [0x80013180]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80005030]:fadd.d t5, t3, s10, dyn<br> [0x80005034]:csrrs a7, fcsr, zero<br> [0x80005038]:sw t5, 696(ra)<br> [0x8000503c]:sw t6, 704(ra)<br> [0x80005040]:sw t5, 712(ra)<br> [0x80005044]:sw a7, 720(ra)<br>                                     |
| 226|[0x80013188]<br>0x00000000<br> [0x800131a0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800050b0]:fadd.d t5, t3, s10, dyn<br> [0x800050b4]:csrrs a7, fcsr, zero<br> [0x800050b8]:sw t5, 728(ra)<br> [0x800050bc]:sw t6, 736(ra)<br> [0x800050c0]:sw t5, 744(ra)<br> [0x800050c4]:sw a7, 752(ra)<br>                                     |
| 227|[0x800131a8]<br>0x00000002<br> [0x800131c0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80005130]:fadd.d t5, t3, s10, dyn<br> [0x80005134]:csrrs a7, fcsr, zero<br> [0x80005138]:sw t5, 760(ra)<br> [0x8000513c]:sw t6, 768(ra)<br> [0x80005140]:sw t5, 776(ra)<br> [0x80005144]:sw a7, 784(ra)<br>                                     |
| 228|[0x800131c8]<br>0x00000001<br> [0x800131e0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800051b0]:fadd.d t5, t3, s10, dyn<br> [0x800051b4]:csrrs a7, fcsr, zero<br> [0x800051b8]:sw t5, 792(ra)<br> [0x800051bc]:sw t6, 800(ra)<br> [0x800051c0]:sw t5, 808(ra)<br> [0x800051c4]:sw a7, 816(ra)<br>                                     |
| 229|[0x800131e8]<br>0xFFFFFFFF<br> [0x80013200]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80005234]:fadd.d t5, t3, s10, dyn<br> [0x80005238]:csrrs a7, fcsr, zero<br> [0x8000523c]:sw t5, 824(ra)<br> [0x80005240]:sw t6, 832(ra)<br> [0x80005244]:sw t5, 840(ra)<br> [0x80005248]:sw a7, 848(ra)<br>                                     |
| 230|[0x80013208]<br>0xFFFFFFFF<br> [0x80013220]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800052b8]:fadd.d t5, t3, s10, dyn<br> [0x800052bc]:csrrs a7, fcsr, zero<br> [0x800052c0]:sw t5, 856(ra)<br> [0x800052c4]:sw t6, 864(ra)<br> [0x800052c8]:sw t5, 872(ra)<br> [0x800052cc]:sw a7, 880(ra)<br>                                     |
| 231|[0x80013228]<br>0x00000000<br> [0x80013240]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80005338]:fadd.d t5, t3, s10, dyn<br> [0x8000533c]:csrrs a7, fcsr, zero<br> [0x80005340]:sw t5, 888(ra)<br> [0x80005344]:sw t6, 896(ra)<br> [0x80005348]:sw t5, 904(ra)<br> [0x8000534c]:sw a7, 912(ra)<br>                                     |
| 232|[0x80013248]<br>0x00000000<br> [0x80013260]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800053b8]:fadd.d t5, t3, s10, dyn<br> [0x800053bc]:csrrs a7, fcsr, zero<br> [0x800053c0]:sw t5, 920(ra)<br> [0x800053c4]:sw t6, 928(ra)<br> [0x800053c8]:sw t5, 936(ra)<br> [0x800053cc]:sw a7, 944(ra)<br>                                     |
| 233|[0x80013268]<br>0x00000000<br> [0x80013280]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80005438]:fadd.d t5, t3, s10, dyn<br> [0x8000543c]:csrrs a7, fcsr, zero<br> [0x80005440]:sw t5, 952(ra)<br> [0x80005444]:sw t6, 960(ra)<br> [0x80005448]:sw t5, 968(ra)<br> [0x8000544c]:sw a7, 976(ra)<br>                                     |
| 234|[0x80013288]<br>0x00000000<br> [0x800132a0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800054b8]:fadd.d t5, t3, s10, dyn<br> [0x800054bc]:csrrs a7, fcsr, zero<br> [0x800054c0]:sw t5, 984(ra)<br> [0x800054c4]:sw t6, 992(ra)<br> [0x800054c8]:sw t5, 1000(ra)<br> [0x800054cc]:sw a7, 1008(ra)<br>                                   |
| 235|[0x800132a8]<br>0x00000000<br> [0x800132c0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80005538]:fadd.d t5, t3, s10, dyn<br> [0x8000553c]:csrrs a7, fcsr, zero<br> [0x80005540]:sw t5, 1016(ra)<br> [0x80005544]:sw t6, 1024(ra)<br> [0x80005548]:sw t5, 1032(ra)<br> [0x8000554c]:sw a7, 1040(ra)<br>                                 |
| 236|[0x800132c8]<br>0x00000000<br> [0x800132e0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800055b8]:fadd.d t5, t3, s10, dyn<br> [0x800055bc]:csrrs a7, fcsr, zero<br> [0x800055c0]:sw t5, 1048(ra)<br> [0x800055c4]:sw t6, 1056(ra)<br> [0x800055c8]:sw t5, 1064(ra)<br> [0x800055cc]:sw a7, 1072(ra)<br>                                 |
| 237|[0x800132e8]<br>0x00000000<br> [0x80013300]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80005638]:fadd.d t5, t3, s10, dyn<br> [0x8000563c]:csrrs a7, fcsr, zero<br> [0x80005640]:sw t5, 1080(ra)<br> [0x80005644]:sw t6, 1088(ra)<br> [0x80005648]:sw t5, 1096(ra)<br> [0x8000564c]:sw a7, 1104(ra)<br>                                 |
| 238|[0x80013308]<br>0x00000000<br> [0x80013320]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800056b8]:fadd.d t5, t3, s10, dyn<br> [0x800056bc]:csrrs a7, fcsr, zero<br> [0x800056c0]:sw t5, 1112(ra)<br> [0x800056c4]:sw t6, 1120(ra)<br> [0x800056c8]:sw t5, 1128(ra)<br> [0x800056cc]:sw a7, 1136(ra)<br>                                 |
| 239|[0x80013328]<br>0x00000000<br> [0x80013340]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80005738]:fadd.d t5, t3, s10, dyn<br> [0x8000573c]:csrrs a7, fcsr, zero<br> [0x80005740]:sw t5, 1144(ra)<br> [0x80005744]:sw t6, 1152(ra)<br> [0x80005748]:sw t5, 1160(ra)<br> [0x8000574c]:sw a7, 1168(ra)<br>                                 |
| 240|[0x80013348]<br>0x00000000<br> [0x80013360]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800057b8]:fadd.d t5, t3, s10, dyn<br> [0x800057bc]:csrrs a7, fcsr, zero<br> [0x800057c0]:sw t5, 1176(ra)<br> [0x800057c4]:sw t6, 1184(ra)<br> [0x800057c8]:sw t5, 1192(ra)<br> [0x800057cc]:sw a7, 1200(ra)<br>                                 |
| 241|[0x80013368]<br>0x00000002<br> [0x80013380]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80005838]:fadd.d t5, t3, s10, dyn<br> [0x8000583c]:csrrs a7, fcsr, zero<br> [0x80005840]:sw t5, 1208(ra)<br> [0x80005844]:sw t6, 1216(ra)<br> [0x80005848]:sw t5, 1224(ra)<br> [0x8000584c]:sw a7, 1232(ra)<br>                                 |
| 242|[0x80013388]<br>0x00000002<br> [0x800133a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800058b8]:fadd.d t5, t3, s10, dyn<br> [0x800058bc]:csrrs a7, fcsr, zero<br> [0x800058c0]:sw t5, 1240(ra)<br> [0x800058c4]:sw t6, 1248(ra)<br> [0x800058c8]:sw t5, 1256(ra)<br> [0x800058cc]:sw a7, 1264(ra)<br>                                 |
| 243|[0x800133a8]<br>0x00000003<br> [0x800133c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80005938]:fadd.d t5, t3, s10, dyn<br> [0x8000593c]:csrrs a7, fcsr, zero<br> [0x80005940]:sw t5, 1272(ra)<br> [0x80005944]:sw t6, 1280(ra)<br> [0x80005948]:sw t5, 1288(ra)<br> [0x8000594c]:sw a7, 1296(ra)<br>                                 |
| 244|[0x800133c8]<br>0x00000001<br> [0x800133e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800059b8]:fadd.d t5, t3, s10, dyn<br> [0x800059bc]:csrrs a7, fcsr, zero<br> [0x800059c0]:sw t5, 1304(ra)<br> [0x800059c4]:sw t6, 1312(ra)<br> [0x800059c8]:sw t5, 1320(ra)<br> [0x800059cc]:sw a7, 1328(ra)<br>                                 |
| 245|[0x800133e8]<br>0x00000004<br> [0x80013400]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80005a38]:fadd.d t5, t3, s10, dyn<br> [0x80005a3c]:csrrs a7, fcsr, zero<br> [0x80005a40]:sw t5, 1336(ra)<br> [0x80005a44]:sw t6, 1344(ra)<br> [0x80005a48]:sw t5, 1352(ra)<br> [0x80005a4c]:sw a7, 1360(ra)<br>                                 |
| 246|[0x80013408]<br>0x00000000<br> [0x80013420]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80005ab8]:fadd.d t5, t3, s10, dyn<br> [0x80005abc]:csrrs a7, fcsr, zero<br> [0x80005ac0]:sw t5, 1368(ra)<br> [0x80005ac4]:sw t6, 1376(ra)<br> [0x80005ac8]:sw t5, 1384(ra)<br> [0x80005acc]:sw a7, 1392(ra)<br>                                 |
| 247|[0x80013428]<br>0x00000000<br> [0x80013440]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80005b3c]:fadd.d t5, t3, s10, dyn<br> [0x80005b40]:csrrs a7, fcsr, zero<br> [0x80005b44]:sw t5, 1400(ra)<br> [0x80005b48]:sw t6, 1408(ra)<br> [0x80005b4c]:sw t5, 1416(ra)<br> [0x80005b50]:sw a7, 1424(ra)<br>                                 |
| 248|[0x80013448]<br>0x00000003<br> [0x80013460]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80005bc0]:fadd.d t5, t3, s10, dyn<br> [0x80005bc4]:csrrs a7, fcsr, zero<br> [0x80005bc8]:sw t5, 1432(ra)<br> [0x80005bcc]:sw t6, 1440(ra)<br> [0x80005bd0]:sw t5, 1448(ra)<br> [0x80005bd4]:sw a7, 1456(ra)<br>                                 |
| 249|[0x80013468]<br>0x00000001<br> [0x80013480]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80005c40]:fadd.d t5, t3, s10, dyn<br> [0x80005c44]:csrrs a7, fcsr, zero<br> [0x80005c48]:sw t5, 1464(ra)<br> [0x80005c4c]:sw t6, 1472(ra)<br> [0x80005c50]:sw t5, 1480(ra)<br> [0x80005c54]:sw a7, 1488(ra)<br>                                 |
| 250|[0x80013488]<br>0x00000002<br> [0x800134a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80005cc0]:fadd.d t5, t3, s10, dyn<br> [0x80005cc4]:csrrs a7, fcsr, zero<br> [0x80005cc8]:sw t5, 1496(ra)<br> [0x80005ccc]:sw t6, 1504(ra)<br> [0x80005cd0]:sw t5, 1512(ra)<br> [0x80005cd4]:sw a7, 1520(ra)<br>                                 |
| 251|[0x800134a8]<br>0x00000002<br> [0x800134c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80005d40]:fadd.d t5, t3, s10, dyn<br> [0x80005d44]:csrrs a7, fcsr, zero<br> [0x80005d48]:sw t5, 1528(ra)<br> [0x80005d4c]:sw t6, 1536(ra)<br> [0x80005d50]:sw t5, 1544(ra)<br> [0x80005d54]:sw a7, 1552(ra)<br>                                 |
| 252|[0x800134c8]<br>0x00000000<br> [0x800134e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80005dc0]:fadd.d t5, t3, s10, dyn<br> [0x80005dc4]:csrrs a7, fcsr, zero<br> [0x80005dc8]:sw t5, 1560(ra)<br> [0x80005dcc]:sw t6, 1568(ra)<br> [0x80005dd0]:sw t5, 1576(ra)<br> [0x80005dd4]:sw a7, 1584(ra)<br>                                 |
| 253|[0x800134e8]<br>0xFFFFFFFF<br> [0x80013500]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80005e44]:fadd.d t5, t3, s10, dyn<br> [0x80005e48]:csrrs a7, fcsr, zero<br> [0x80005e4c]:sw t5, 1592(ra)<br> [0x80005e50]:sw t6, 1600(ra)<br> [0x80005e54]:sw t5, 1608(ra)<br> [0x80005e58]:sw a7, 1616(ra)<br>                                 |
| 254|[0x80013508]<br>0xFFFFFFFF<br> [0x80013520]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80005ec8]:fadd.d t5, t3, s10, dyn<br> [0x80005ecc]:csrrs a7, fcsr, zero<br> [0x80005ed0]:sw t5, 1624(ra)<br> [0x80005ed4]:sw t6, 1632(ra)<br> [0x80005ed8]:sw t5, 1640(ra)<br> [0x80005edc]:sw a7, 1648(ra)<br>                                 |
| 255|[0x80013528]<br>0x00000000<br> [0x80013540]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80005f48]:fadd.d t5, t3, s10, dyn<br> [0x80005f4c]:csrrs a7, fcsr, zero<br> [0x80005f50]:sw t5, 1656(ra)<br> [0x80005f54]:sw t6, 1664(ra)<br> [0x80005f58]:sw t5, 1672(ra)<br> [0x80005f5c]:sw a7, 1680(ra)<br>                                 |
| 256|[0x80013548]<br>0x00000000<br> [0x80013560]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80005fc8]:fadd.d t5, t3, s10, dyn<br> [0x80005fcc]:csrrs a7, fcsr, zero<br> [0x80005fd0]:sw t5, 1688(ra)<br> [0x80005fd4]:sw t6, 1696(ra)<br> [0x80005fd8]:sw t5, 1704(ra)<br> [0x80005fdc]:sw a7, 1712(ra)<br>                                 |
| 257|[0x80013568]<br>0x00000000<br> [0x80013580]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006048]:fadd.d t5, t3, s10, dyn<br> [0x8000604c]:csrrs a7, fcsr, zero<br> [0x80006050]:sw t5, 1720(ra)<br> [0x80006054]:sw t6, 1728(ra)<br> [0x80006058]:sw t5, 1736(ra)<br> [0x8000605c]:sw a7, 1744(ra)<br>                                 |
| 258|[0x80013588]<br>0x00000000<br> [0x800135a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800060c8]:fadd.d t5, t3, s10, dyn<br> [0x800060cc]:csrrs a7, fcsr, zero<br> [0x800060d0]:sw t5, 1752(ra)<br> [0x800060d4]:sw t6, 1760(ra)<br> [0x800060d8]:sw t5, 1768(ra)<br> [0x800060dc]:sw a7, 1776(ra)<br>                                 |
| 259|[0x800135a8]<br>0x00000000<br> [0x800135c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006148]:fadd.d t5, t3, s10, dyn<br> [0x8000614c]:csrrs a7, fcsr, zero<br> [0x80006150]:sw t5, 1784(ra)<br> [0x80006154]:sw t6, 1792(ra)<br> [0x80006158]:sw t5, 1800(ra)<br> [0x8000615c]:sw a7, 1808(ra)<br>                                 |
| 260|[0x800135c8]<br>0x00000000<br> [0x800135e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800061c8]:fadd.d t5, t3, s10, dyn<br> [0x800061cc]:csrrs a7, fcsr, zero<br> [0x800061d0]:sw t5, 1816(ra)<br> [0x800061d4]:sw t6, 1824(ra)<br> [0x800061d8]:sw t5, 1832(ra)<br> [0x800061dc]:sw a7, 1840(ra)<br>                                 |
| 261|[0x800135e8]<br>0x00000000<br> [0x80013600]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006248]:fadd.d t5, t3, s10, dyn<br> [0x8000624c]:csrrs a7, fcsr, zero<br> [0x80006250]:sw t5, 1848(ra)<br> [0x80006254]:sw t6, 1856(ra)<br> [0x80006258]:sw t5, 1864(ra)<br> [0x8000625c]:sw a7, 1872(ra)<br>                                 |
| 262|[0x80013608]<br>0x00000000<br> [0x80013620]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800062c8]:fadd.d t5, t3, s10, dyn<br> [0x800062cc]:csrrs a7, fcsr, zero<br> [0x800062d0]:sw t5, 1880(ra)<br> [0x800062d4]:sw t6, 1888(ra)<br> [0x800062d8]:sw t5, 1896(ra)<br> [0x800062dc]:sw a7, 1904(ra)<br>                                 |
| 263|[0x80013628]<br>0x00000000<br> [0x80013640]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006348]:fadd.d t5, t3, s10, dyn<br> [0x8000634c]:csrrs a7, fcsr, zero<br> [0x80006350]:sw t5, 1912(ra)<br> [0x80006354]:sw t6, 1920(ra)<br> [0x80006358]:sw t5, 1928(ra)<br> [0x8000635c]:sw a7, 1936(ra)<br>                                 |
| 264|[0x80013648]<br>0x00000000<br> [0x80013660]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800063c8]:fadd.d t5, t3, s10, dyn<br> [0x800063cc]:csrrs a7, fcsr, zero<br> [0x800063d0]:sw t5, 1944(ra)<br> [0x800063d4]:sw t6, 1952(ra)<br> [0x800063d8]:sw t5, 1960(ra)<br> [0x800063dc]:sw a7, 1968(ra)<br>                                 |
| 265|[0x80013668]<br>0x00000002<br> [0x80013680]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006448]:fadd.d t5, t3, s10, dyn<br> [0x8000644c]:csrrs a7, fcsr, zero<br> [0x80006450]:sw t5, 1976(ra)<br> [0x80006454]:sw t6, 1984(ra)<br> [0x80006458]:sw t5, 1992(ra)<br> [0x8000645c]:sw a7, 2000(ra)<br>                                 |
| 266|[0x80013688]<br>0x00000002<br> [0x800136a0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800064c8]:fadd.d t5, t3, s10, dyn<br> [0x800064cc]:csrrs a7, fcsr, zero<br> [0x800064d0]:sw t5, 2008(ra)<br> [0x800064d4]:sw t6, 2016(ra)<br> [0x800064d8]:sw t5, 2024(ra)<br> [0x800064dc]:sw a7, 2032(ra)<br>                                 |
| 267|[0x800136a8]<br>0x00000001<br> [0x800136c0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006548]:fadd.d t5, t3, s10, dyn<br> [0x8000654c]:csrrs a7, fcsr, zero<br> [0x80006550]:sw t5, 2040(ra)<br> [0x80006554]:addi ra, ra, 2040<br> [0x80006558]:sw t6, 8(ra)<br> [0x8000655c]:sw t5, 16(ra)<br> [0x80006560]:sw a7, 24(ra)<br>     |
| 268|[0x800126c8]<br>0x00000003<br> [0x800126e0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006594]:fadd.d t5, t3, s10, dyn<br> [0x80006598]:csrrs a7, fcsr, zero<br> [0x8000659c]:sw t5, 0(ra)<br> [0x800065a0]:sw t6, 8(ra)<br> [0x800065a4]:sw t5, 16(ra)<br> [0x800065a8]:sw a7, 24(ra)<br>                                           |
| 269|[0x800126e8]<br>0x00000000<br> [0x80012700]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800065d4]:fadd.d t5, t3, s10, dyn<br> [0x800065d8]:csrrs a7, fcsr, zero<br> [0x800065dc]:sw t5, 32(ra)<br> [0x800065e0]:sw t6, 40(ra)<br> [0x800065e4]:sw t5, 48(ra)<br> [0x800065e8]:sw a7, 56(ra)<br>                                         |
| 270|[0x80012708]<br>0x00000004<br> [0x80012720]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006614]:fadd.d t5, t3, s10, dyn<br> [0x80006618]:csrrs a7, fcsr, zero<br> [0x8000661c]:sw t5, 64(ra)<br> [0x80006620]:sw t6, 72(ra)<br> [0x80006624]:sw t5, 80(ra)<br> [0x80006628]:sw a7, 88(ra)<br>                                         |
| 271|[0x80012728]<br>0x00000003<br> [0x80012740]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006658]:fadd.d t5, t3, s10, dyn<br> [0x8000665c]:csrrs a7, fcsr, zero<br> [0x80006660]:sw t5, 96(ra)<br> [0x80006664]:sw t6, 104(ra)<br> [0x80006668]:sw t5, 112(ra)<br> [0x8000666c]:sw a7, 120(ra)<br>                                      |
| 272|[0x80012748]<br>0x00000000<br> [0x80012760]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000669c]:fadd.d t5, t3, s10, dyn<br> [0x800066a0]:csrrs a7, fcsr, zero<br> [0x800066a4]:sw t5, 128(ra)<br> [0x800066a8]:sw t6, 136(ra)<br> [0x800066ac]:sw t5, 144(ra)<br> [0x800066b0]:sw a7, 152(ra)<br>                                     |
| 273|[0x80012768]<br>0x00000002<br> [0x80012780]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800066dc]:fadd.d t5, t3, s10, dyn<br> [0x800066e0]:csrrs a7, fcsr, zero<br> [0x800066e4]:sw t5, 160(ra)<br> [0x800066e8]:sw t6, 168(ra)<br> [0x800066ec]:sw t5, 176(ra)<br> [0x800066f0]:sw a7, 184(ra)<br>                                     |
| 274|[0x80012788]<br>0x00000001<br> [0x800127a0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000671c]:fadd.d t5, t3, s10, dyn<br> [0x80006720]:csrrs a7, fcsr, zero<br> [0x80006724]:sw t5, 192(ra)<br> [0x80006728]:sw t6, 200(ra)<br> [0x8000672c]:sw t5, 208(ra)<br> [0x80006730]:sw a7, 216(ra)<br>                                     |
| 275|[0x800127a8]<br>0x00000000<br> [0x800127c0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000675c]:fadd.d t5, t3, s10, dyn<br> [0x80006760]:csrrs a7, fcsr, zero<br> [0x80006764]:sw t5, 224(ra)<br> [0x80006768]:sw t6, 232(ra)<br> [0x8000676c]:sw t5, 240(ra)<br> [0x80006770]:sw a7, 248(ra)<br>                                     |
| 276|[0x800127c8]<br>0x00000002<br> [0x800127e0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000679c]:fadd.d t5, t3, s10, dyn<br> [0x800067a0]:csrrs a7, fcsr, zero<br> [0x800067a4]:sw t5, 256(ra)<br> [0x800067a8]:sw t6, 264(ra)<br> [0x800067ac]:sw t5, 272(ra)<br> [0x800067b0]:sw a7, 280(ra)<br>                                     |
| 277|[0x800127e8]<br>0xFFFFFFFF<br> [0x80012800]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800067e0]:fadd.d t5, t3, s10, dyn<br> [0x800067e4]:csrrs a7, fcsr, zero<br> [0x800067e8]:sw t5, 288(ra)<br> [0x800067ec]:sw t6, 296(ra)<br> [0x800067f0]:sw t5, 304(ra)<br> [0x800067f4]:sw a7, 312(ra)<br>                                     |
| 278|[0x80012808]<br>0xFFFFFFFF<br> [0x80012820]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006824]:fadd.d t5, t3, s10, dyn<br> [0x80006828]:csrrs a7, fcsr, zero<br> [0x8000682c]:sw t5, 320(ra)<br> [0x80006830]:sw t6, 328(ra)<br> [0x80006834]:sw t5, 336(ra)<br> [0x80006838]:sw a7, 344(ra)<br>                                     |
| 279|[0x80012828]<br>0x00000000<br> [0x80012840]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006864]:fadd.d t5, t3, s10, dyn<br> [0x80006868]:csrrs a7, fcsr, zero<br> [0x8000686c]:sw t5, 352(ra)<br> [0x80006870]:sw t6, 360(ra)<br> [0x80006874]:sw t5, 368(ra)<br> [0x80006878]:sw a7, 376(ra)<br>                                     |
| 280|[0x80012848]<br>0x00000000<br> [0x80012860]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800068a4]:fadd.d t5, t3, s10, dyn<br> [0x800068a8]:csrrs a7, fcsr, zero<br> [0x800068ac]:sw t5, 384(ra)<br> [0x800068b0]:sw t6, 392(ra)<br> [0x800068b4]:sw t5, 400(ra)<br> [0x800068b8]:sw a7, 408(ra)<br>                                     |
| 281|[0x80012868]<br>0x00000000<br> [0x80012880]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800068e4]:fadd.d t5, t3, s10, dyn<br> [0x800068e8]:csrrs a7, fcsr, zero<br> [0x800068ec]:sw t5, 416(ra)<br> [0x800068f0]:sw t6, 424(ra)<br> [0x800068f4]:sw t5, 432(ra)<br> [0x800068f8]:sw a7, 440(ra)<br>                                     |
| 282|[0x80012888]<br>0x00000000<br> [0x800128a0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006924]:fadd.d t5, t3, s10, dyn<br> [0x80006928]:csrrs a7, fcsr, zero<br> [0x8000692c]:sw t5, 448(ra)<br> [0x80006930]:sw t6, 456(ra)<br> [0x80006934]:sw t5, 464(ra)<br> [0x80006938]:sw a7, 472(ra)<br>                                     |
| 283|[0x800128a8]<br>0x00000000<br> [0x800128c0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006964]:fadd.d t5, t3, s10, dyn<br> [0x80006968]:csrrs a7, fcsr, zero<br> [0x8000696c]:sw t5, 480(ra)<br> [0x80006970]:sw t6, 488(ra)<br> [0x80006974]:sw t5, 496(ra)<br> [0x80006978]:sw a7, 504(ra)<br>                                     |
| 284|[0x800128c8]<br>0x00000000<br> [0x800128e0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800069a4]:fadd.d t5, t3, s10, dyn<br> [0x800069a8]:csrrs a7, fcsr, zero<br> [0x800069ac]:sw t5, 512(ra)<br> [0x800069b0]:sw t6, 520(ra)<br> [0x800069b4]:sw t5, 528(ra)<br> [0x800069b8]:sw a7, 536(ra)<br>                                     |
| 285|[0x800128e8]<br>0x00000000<br> [0x80012900]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800069e4]:fadd.d t5, t3, s10, dyn<br> [0x800069e8]:csrrs a7, fcsr, zero<br> [0x800069ec]:sw t5, 544(ra)<br> [0x800069f0]:sw t6, 552(ra)<br> [0x800069f4]:sw t5, 560(ra)<br> [0x800069f8]:sw a7, 568(ra)<br>                                     |
| 286|[0x80012908]<br>0x00000000<br> [0x80012920]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006a24]:fadd.d t5, t3, s10, dyn<br> [0x80006a28]:csrrs a7, fcsr, zero<br> [0x80006a2c]:sw t5, 576(ra)<br> [0x80006a30]:sw t6, 584(ra)<br> [0x80006a34]:sw t5, 592(ra)<br> [0x80006a38]:sw a7, 600(ra)<br>                                     |
| 287|[0x80012928]<br>0x00000000<br> [0x80012940]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006a64]:fadd.d t5, t3, s10, dyn<br> [0x80006a68]:csrrs a7, fcsr, zero<br> [0x80006a6c]:sw t5, 608(ra)<br> [0x80006a70]:sw t6, 616(ra)<br> [0x80006a74]:sw t5, 624(ra)<br> [0x80006a78]:sw a7, 632(ra)<br>                                     |
| 288|[0x80012948]<br>0x00000000<br> [0x80012960]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006aa4]:fadd.d t5, t3, s10, dyn<br> [0x80006aa8]:csrrs a7, fcsr, zero<br> [0x80006aac]:sw t5, 640(ra)<br> [0x80006ab0]:sw t6, 648(ra)<br> [0x80006ab4]:sw t5, 656(ra)<br> [0x80006ab8]:sw a7, 664(ra)<br>                                     |
| 289|[0x80012968]<br>0xFFFFFFFF<br> [0x80012980]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006ae8]:fadd.d t5, t3, s10, dyn<br> [0x80006aec]:csrrs a7, fcsr, zero<br> [0x80006af0]:sw t5, 672(ra)<br> [0x80006af4]:sw t6, 680(ra)<br> [0x80006af8]:sw t5, 688(ra)<br> [0x80006afc]:sw a7, 696(ra)<br>                                     |
| 290|[0x80012988]<br>0xFFFFFFFF<br> [0x800129a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006b2c]:fadd.d t5, t3, s10, dyn<br> [0x80006b30]:csrrs a7, fcsr, zero<br> [0x80006b34]:sw t5, 704(ra)<br> [0x80006b38]:sw t6, 712(ra)<br> [0x80006b3c]:sw t5, 720(ra)<br> [0x80006b40]:sw a7, 728(ra)<br>                                     |
| 291|[0x800129a8]<br>0xFFFFFFFF<br> [0x800129c0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006b70]:fadd.d t5, t3, s10, dyn<br> [0x80006b74]:csrrs a7, fcsr, zero<br> [0x80006b78]:sw t5, 736(ra)<br> [0x80006b7c]:sw t6, 744(ra)<br> [0x80006b80]:sw t5, 752(ra)<br> [0x80006b84]:sw a7, 760(ra)<br>                                     |
| 292|[0x800129c8]<br>0xFFFFFFFF<br> [0x800129e0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006bb4]:fadd.d t5, t3, s10, dyn<br> [0x80006bb8]:csrrs a7, fcsr, zero<br> [0x80006bbc]:sw t5, 768(ra)<br> [0x80006bc0]:sw t6, 776(ra)<br> [0x80006bc4]:sw t5, 784(ra)<br> [0x80006bc8]:sw a7, 792(ra)<br>                                     |
| 293|[0x800129e8]<br>0xFFFFFFFF<br> [0x80012a00]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006bf8]:fadd.d t5, t3, s10, dyn<br> [0x80006bfc]:csrrs a7, fcsr, zero<br> [0x80006c00]:sw t5, 800(ra)<br> [0x80006c04]:sw t6, 808(ra)<br> [0x80006c08]:sw t5, 816(ra)<br> [0x80006c0c]:sw a7, 824(ra)<br>                                     |
| 294|[0x80012a08]<br>0xFFFFFFFF<br> [0x80012a20]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006c3c]:fadd.d t5, t3, s10, dyn<br> [0x80006c40]:csrrs a7, fcsr, zero<br> [0x80006c44]:sw t5, 832(ra)<br> [0x80006c48]:sw t6, 840(ra)<br> [0x80006c4c]:sw t5, 848(ra)<br> [0x80006c50]:sw a7, 856(ra)<br>                                     |
| 295|[0x80012a28]<br>0xFFFFFFFF<br> [0x80012a40]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006c84]:fadd.d t5, t3, s10, dyn<br> [0x80006c88]:csrrs a7, fcsr, zero<br> [0x80006c8c]:sw t5, 864(ra)<br> [0x80006c90]:sw t6, 872(ra)<br> [0x80006c94]:sw t5, 880(ra)<br> [0x80006c98]:sw a7, 888(ra)<br>                                     |
| 296|[0x80012a48]<br>0xFFFFFFFF<br> [0x80012a60]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006ccc]:fadd.d t5, t3, s10, dyn<br> [0x80006cd0]:csrrs a7, fcsr, zero<br> [0x80006cd4]:sw t5, 896(ra)<br> [0x80006cd8]:sw t6, 904(ra)<br> [0x80006cdc]:sw t5, 912(ra)<br> [0x80006ce0]:sw a7, 920(ra)<br>                                     |
| 297|[0x80012a68]<br>0xFFFFFFFF<br> [0x80012a80]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006d10]:fadd.d t5, t3, s10, dyn<br> [0x80006d14]:csrrs a7, fcsr, zero<br> [0x80006d18]:sw t5, 928(ra)<br> [0x80006d1c]:sw t6, 936(ra)<br> [0x80006d20]:sw t5, 944(ra)<br> [0x80006d24]:sw a7, 952(ra)<br>                                     |
| 298|[0x80012a88]<br>0xFFFFFFFF<br> [0x80012aa0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006d54]:fadd.d t5, t3, s10, dyn<br> [0x80006d58]:csrrs a7, fcsr, zero<br> [0x80006d5c]:sw t5, 960(ra)<br> [0x80006d60]:sw t6, 968(ra)<br> [0x80006d64]:sw t5, 976(ra)<br> [0x80006d68]:sw a7, 984(ra)<br>                                     |
| 299|[0x80012aa8]<br>0xFFFFFFFF<br> [0x80012ac0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006d98]:fadd.d t5, t3, s10, dyn<br> [0x80006d9c]:csrrs a7, fcsr, zero<br> [0x80006da0]:sw t5, 992(ra)<br> [0x80006da4]:sw t6, 1000(ra)<br> [0x80006da8]:sw t5, 1008(ra)<br> [0x80006dac]:sw a7, 1016(ra)<br>                                  |
| 300|[0x80012ac8]<br>0xFFFFFFFF<br> [0x80012ae0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006ddc]:fadd.d t5, t3, s10, dyn<br> [0x80006de0]:csrrs a7, fcsr, zero<br> [0x80006de4]:sw t5, 1024(ra)<br> [0x80006de8]:sw t6, 1032(ra)<br> [0x80006dec]:sw t5, 1040(ra)<br> [0x80006df0]:sw a7, 1048(ra)<br>                                 |
| 301|[0x80012ae8]<br>0x00000000<br> [0x80012b00]<br>0x00000005<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006e24]:fadd.d t5, t3, s10, dyn<br> [0x80006e28]:csrrs a7, fcsr, zero<br> [0x80006e2c]:sw t5, 1056(ra)<br> [0x80006e30]:sw t6, 1064(ra)<br> [0x80006e34]:sw t5, 1072(ra)<br> [0x80006e38]:sw a7, 1080(ra)<br>                                 |
| 302|[0x80012b08]<br>0x00000000<br> [0x80012b20]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006e6c]:fadd.d t5, t3, s10, dyn<br> [0x80006e70]:csrrs a7, fcsr, zero<br> [0x80006e74]:sw t5, 1088(ra)<br> [0x80006e78]:sw t6, 1096(ra)<br> [0x80006e7c]:sw t5, 1104(ra)<br> [0x80006e80]:sw a7, 1112(ra)<br>                                 |
| 303|[0x80012b28]<br>0x00000000<br> [0x80012b40]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006eb0]:fadd.d t5, t3, s10, dyn<br> [0x80006eb4]:csrrs a7, fcsr, zero<br> [0x80006eb8]:sw t5, 1120(ra)<br> [0x80006ebc]:sw t6, 1128(ra)<br> [0x80006ec0]:sw t5, 1136(ra)<br> [0x80006ec4]:sw a7, 1144(ra)<br>                                 |
| 304|[0x80012b48]<br>0x00000000<br> [0x80012b60]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006ef4]:fadd.d t5, t3, s10, dyn<br> [0x80006ef8]:csrrs a7, fcsr, zero<br> [0x80006efc]:sw t5, 1152(ra)<br> [0x80006f00]:sw t6, 1160(ra)<br> [0x80006f04]:sw t5, 1168(ra)<br> [0x80006f08]:sw a7, 1176(ra)<br>                                 |
| 305|[0x80012b68]<br>0x00000000<br> [0x80012b80]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006f38]:fadd.d t5, t3, s10, dyn<br> [0x80006f3c]:csrrs a7, fcsr, zero<br> [0x80006f40]:sw t5, 1184(ra)<br> [0x80006f44]:sw t6, 1192(ra)<br> [0x80006f48]:sw t5, 1200(ra)<br> [0x80006f4c]:sw a7, 1208(ra)<br>                                 |
| 306|[0x80012b88]<br>0x00000000<br> [0x80012ba0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006f7c]:fadd.d t5, t3, s10, dyn<br> [0x80006f80]:csrrs a7, fcsr, zero<br> [0x80006f84]:sw t5, 1216(ra)<br> [0x80006f88]:sw t6, 1224(ra)<br> [0x80006f8c]:sw t5, 1232(ra)<br> [0x80006f90]:sw a7, 1240(ra)<br>                                 |
| 307|[0x80012ba8]<br>0x00000000<br> [0x80012bc0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80006fc0]:fadd.d t5, t3, s10, dyn<br> [0x80006fc4]:csrrs a7, fcsr, zero<br> [0x80006fc8]:sw t5, 1248(ra)<br> [0x80006fcc]:sw t6, 1256(ra)<br> [0x80006fd0]:sw t5, 1264(ra)<br> [0x80006fd4]:sw a7, 1272(ra)<br>                                 |
| 308|[0x80012bc8]<br>0x00000000<br> [0x80012be0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007004]:fadd.d t5, t3, s10, dyn<br> [0x80007008]:csrrs a7, fcsr, zero<br> [0x8000700c]:sw t5, 1280(ra)<br> [0x80007010]:sw t6, 1288(ra)<br> [0x80007014]:sw t5, 1296(ra)<br> [0x80007018]:sw a7, 1304(ra)<br>                                 |
| 309|[0x80012be8]<br>0x00000000<br> [0x80012c00]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007048]:fadd.d t5, t3, s10, dyn<br> [0x8000704c]:csrrs a7, fcsr, zero<br> [0x80007050]:sw t5, 1312(ra)<br> [0x80007054]:sw t6, 1320(ra)<br> [0x80007058]:sw t5, 1328(ra)<br> [0x8000705c]:sw a7, 1336(ra)<br>                                 |
| 310|[0x80012c08]<br>0x00000000<br> [0x80012c20]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000708c]:fadd.d t5, t3, s10, dyn<br> [0x80007090]:csrrs a7, fcsr, zero<br> [0x80007094]:sw t5, 1344(ra)<br> [0x80007098]:sw t6, 1352(ra)<br> [0x8000709c]:sw t5, 1360(ra)<br> [0x800070a0]:sw a7, 1368(ra)<br>                                 |
| 311|[0x80012c28]<br>0xFFFFFFFF<br> [0x80012c40]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800070d0]:fadd.d t5, t3, s10, dyn<br> [0x800070d4]:csrrs a7, fcsr, zero<br> [0x800070d8]:sw t5, 1376(ra)<br> [0x800070dc]:sw t6, 1384(ra)<br> [0x800070e0]:sw t5, 1392(ra)<br> [0x800070e4]:sw a7, 1400(ra)<br>                                 |
| 312|[0x80012c48]<br>0xFFFFFFFF<br> [0x80012c60]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007114]:fadd.d t5, t3, s10, dyn<br> [0x80007118]:csrrs a7, fcsr, zero<br> [0x8000711c]:sw t5, 1408(ra)<br> [0x80007120]:sw t6, 1416(ra)<br> [0x80007124]:sw t5, 1424(ra)<br> [0x80007128]:sw a7, 1432(ra)<br>                                 |
| 313|[0x80012c68]<br>0xFFFFFFFF<br> [0x80012c80]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007158]:fadd.d t5, t3, s10, dyn<br> [0x8000715c]:csrrs a7, fcsr, zero<br> [0x80007160]:sw t5, 1440(ra)<br> [0x80007164]:sw t6, 1448(ra)<br> [0x80007168]:sw t5, 1456(ra)<br> [0x8000716c]:sw a7, 1464(ra)<br>                                 |
| 314|[0x80012c88]<br>0xFFFFFFFF<br> [0x80012ca0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000719c]:fadd.d t5, t3, s10, dyn<br> [0x800071a0]:csrrs a7, fcsr, zero<br> [0x800071a4]:sw t5, 1472(ra)<br> [0x800071a8]:sw t6, 1480(ra)<br> [0x800071ac]:sw t5, 1488(ra)<br> [0x800071b0]:sw a7, 1496(ra)<br>                                 |
| 315|[0x80012ca8]<br>0xFFFFFFFF<br> [0x80012cc0]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800071e0]:fadd.d t5, t3, s10, dyn<br> [0x800071e4]:csrrs a7, fcsr, zero<br> [0x800071e8]:sw t5, 1504(ra)<br> [0x800071ec]:sw t6, 1512(ra)<br> [0x800071f0]:sw t5, 1520(ra)<br> [0x800071f4]:sw a7, 1528(ra)<br>                                 |
| 316|[0x80012cc8]<br>0xFFFFFFFF<br> [0x80012ce0]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007224]:fadd.d t5, t3, s10, dyn<br> [0x80007228]:csrrs a7, fcsr, zero<br> [0x8000722c]:sw t5, 1536(ra)<br> [0x80007230]:sw t6, 1544(ra)<br> [0x80007234]:sw t5, 1552(ra)<br> [0x80007238]:sw a7, 1560(ra)<br>                                 |
| 317|[0x80012ce8]<br>0xFFFFFFFF<br> [0x80012d00]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007268]:fadd.d t5, t3, s10, dyn<br> [0x8000726c]:csrrs a7, fcsr, zero<br> [0x80007270]:sw t5, 1568(ra)<br> [0x80007274]:sw t6, 1576(ra)<br> [0x80007278]:sw t5, 1584(ra)<br> [0x8000727c]:sw a7, 1592(ra)<br>                                 |
| 318|[0x80012d08]<br>0xFFFFFFFF<br> [0x80012d20]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800072ac]:fadd.d t5, t3, s10, dyn<br> [0x800072b0]:csrrs a7, fcsr, zero<br> [0x800072b4]:sw t5, 1600(ra)<br> [0x800072b8]:sw t6, 1608(ra)<br> [0x800072bc]:sw t5, 1616(ra)<br> [0x800072c0]:sw a7, 1624(ra)<br>                                 |
| 319|[0x80012d28]<br>0xFFFFFFFF<br> [0x80012d40]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800072f4]:fadd.d t5, t3, s10, dyn<br> [0x800072f8]:csrrs a7, fcsr, zero<br> [0x800072fc]:sw t5, 1632(ra)<br> [0x80007300]:sw t6, 1640(ra)<br> [0x80007304]:sw t5, 1648(ra)<br> [0x80007308]:sw a7, 1656(ra)<br>                                 |
| 320|[0x80012d48]<br>0xFFFFFFFF<br> [0x80012d60]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000733c]:fadd.d t5, t3, s10, dyn<br> [0x80007340]:csrrs a7, fcsr, zero<br> [0x80007344]:sw t5, 1664(ra)<br> [0x80007348]:sw t6, 1672(ra)<br> [0x8000734c]:sw t5, 1680(ra)<br> [0x80007350]:sw a7, 1688(ra)<br>                                 |
| 321|[0x80012d68]<br>0xFFFFFFFF<br> [0x80012d80]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007380]:fadd.d t5, t3, s10, dyn<br> [0x80007384]:csrrs a7, fcsr, zero<br> [0x80007388]:sw t5, 1696(ra)<br> [0x8000738c]:sw t6, 1704(ra)<br> [0x80007390]:sw t5, 1712(ra)<br> [0x80007394]:sw a7, 1720(ra)<br>                                 |
| 322|[0x80012d88]<br>0xFFFFFFFF<br> [0x80012da0]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800073c4]:fadd.d t5, t3, s10, dyn<br> [0x800073c8]:csrrs a7, fcsr, zero<br> [0x800073cc]:sw t5, 1728(ra)<br> [0x800073d0]:sw t6, 1736(ra)<br> [0x800073d4]:sw t5, 1744(ra)<br> [0x800073d8]:sw a7, 1752(ra)<br>                                 |
| 323|[0x80012da8]<br>0xFFFFFFFF<br> [0x80012dc0]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007408]:fadd.d t5, t3, s10, dyn<br> [0x8000740c]:csrrs a7, fcsr, zero<br> [0x80007410]:sw t5, 1760(ra)<br> [0x80007414]:sw t6, 1768(ra)<br> [0x80007418]:sw t5, 1776(ra)<br> [0x8000741c]:sw a7, 1784(ra)<br>                                 |
| 324|[0x80012dc8]<br>0xFFFFFFFF<br> [0x80012de0]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000744c]:fadd.d t5, t3, s10, dyn<br> [0x80007450]:csrrs a7, fcsr, zero<br> [0x80007454]:sw t5, 1792(ra)<br> [0x80007458]:sw t6, 1800(ra)<br> [0x8000745c]:sw t5, 1808(ra)<br> [0x80007460]:sw a7, 1816(ra)<br>                                 |
| 325|[0x80012de8]<br>0x00000000<br> [0x80012e00]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007494]:fadd.d t5, t3, s10, dyn<br> [0x80007498]:csrrs a7, fcsr, zero<br> [0x8000749c]:sw t5, 1824(ra)<br> [0x800074a0]:sw t6, 1832(ra)<br> [0x800074a4]:sw t5, 1840(ra)<br> [0x800074a8]:sw a7, 1848(ra)<br>                                 |
| 326|[0x80012e08]<br>0x00000000<br> [0x80012e20]<br>0x00000005<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800074dc]:fadd.d t5, t3, s10, dyn<br> [0x800074e0]:csrrs a7, fcsr, zero<br> [0x800074e4]:sw t5, 1856(ra)<br> [0x800074e8]:sw t6, 1864(ra)<br> [0x800074ec]:sw t5, 1872(ra)<br> [0x800074f0]:sw a7, 1880(ra)<br>                                 |
| 327|[0x80012e28]<br>0x00000000<br> [0x80012e40]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007520]:fadd.d t5, t3, s10, dyn<br> [0x80007524]:csrrs a7, fcsr, zero<br> [0x80007528]:sw t5, 1888(ra)<br> [0x8000752c]:sw t6, 1896(ra)<br> [0x80007530]:sw t5, 1904(ra)<br> [0x80007534]:sw a7, 1912(ra)<br>                                 |
| 328|[0x80012e48]<br>0x00000000<br> [0x80012e60]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007564]:fadd.d t5, t3, s10, dyn<br> [0x80007568]:csrrs a7, fcsr, zero<br> [0x8000756c]:sw t5, 1920(ra)<br> [0x80007570]:sw t6, 1928(ra)<br> [0x80007574]:sw t5, 1936(ra)<br> [0x80007578]:sw a7, 1944(ra)<br>                                 |
| 329|[0x80012e68]<br>0x00000000<br> [0x80012e80]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800075a8]:fadd.d t5, t3, s10, dyn<br> [0x800075ac]:csrrs a7, fcsr, zero<br> [0x800075b0]:sw t5, 1952(ra)<br> [0x800075b4]:sw t6, 1960(ra)<br> [0x800075b8]:sw t5, 1968(ra)<br> [0x800075bc]:sw a7, 1976(ra)<br>                                 |
| 330|[0x80012e88]<br>0x00000000<br> [0x80012ea0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800075ec]:fadd.d t5, t3, s10, dyn<br> [0x800075f0]:csrrs a7, fcsr, zero<br> [0x800075f4]:sw t5, 1984(ra)<br> [0x800075f8]:sw t6, 1992(ra)<br> [0x800075fc]:sw t5, 2000(ra)<br> [0x80007600]:sw a7, 2008(ra)<br>                                 |
| 331|[0x80012ea8]<br>0x00000000<br> [0x80012ec0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007630]:fadd.d t5, t3, s10, dyn<br> [0x80007634]:csrrs a7, fcsr, zero<br> [0x80007638]:sw t5, 2016(ra)<br> [0x8000763c]:sw t6, 2024(ra)<br> [0x80007640]:sw t5, 2032(ra)<br> [0x80007644]:sw a7, 2040(ra)<br>                                 |
| 332|[0x80012ec8]<br>0x00000000<br> [0x80012ee0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007674]:fadd.d t5, t3, s10, dyn<br> [0x80007678]:csrrs a7, fcsr, zero<br> [0x8000767c]:addi ra, ra, 2040<br> [0x80007680]:sw t5, 8(ra)<br> [0x80007684]:sw t6, 16(ra)<br> [0x80007688]:sw t5, 24(ra)<br> [0x8000768c]:sw a7, 32(ra)<br>       |
| 333|[0x80012ee8]<br>0x00000000<br> [0x80012f00]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800076bc]:fadd.d t5, t3, s10, dyn<br> [0x800076c0]:csrrs a7, fcsr, zero<br> [0x800076c4]:sw t5, 40(ra)<br> [0x800076c8]:sw t6, 48(ra)<br> [0x800076cc]:sw t5, 56(ra)<br> [0x800076d0]:sw a7, 64(ra)<br>                                         |
| 334|[0x80012f08]<br>0x00000000<br> [0x80012f20]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007700]:fadd.d t5, t3, s10, dyn<br> [0x80007704]:csrrs a7, fcsr, zero<br> [0x80007708]:sw t5, 72(ra)<br> [0x8000770c]:sw t6, 80(ra)<br> [0x80007710]:sw t5, 88(ra)<br> [0x80007714]:sw a7, 96(ra)<br>                                         |
| 335|[0x80012f28]<br>0xFFFFFFFF<br> [0x80012f40]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007744]:fadd.d t5, t3, s10, dyn<br> [0x80007748]:csrrs a7, fcsr, zero<br> [0x8000774c]:sw t5, 104(ra)<br> [0x80007750]:sw t6, 112(ra)<br> [0x80007754]:sw t5, 120(ra)<br> [0x80007758]:sw a7, 128(ra)<br>                                     |
| 336|[0x80012f48]<br>0xFFFFFFFF<br> [0x80012f60]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007788]:fadd.d t5, t3, s10, dyn<br> [0x8000778c]:csrrs a7, fcsr, zero<br> [0x80007790]:sw t5, 136(ra)<br> [0x80007794]:sw t6, 144(ra)<br> [0x80007798]:sw t5, 152(ra)<br> [0x8000779c]:sw a7, 160(ra)<br>                                     |
| 337|[0x80012f68]<br>0x00000000<br> [0x80012f80]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800077c8]:fadd.d t5, t3, s10, dyn<br> [0x800077cc]:csrrs a7, fcsr, zero<br> [0x800077d0]:sw t5, 168(ra)<br> [0x800077d4]:sw t6, 176(ra)<br> [0x800077d8]:sw t5, 184(ra)<br> [0x800077dc]:sw a7, 192(ra)<br>                                     |
| 338|[0x80012f88]<br>0x00000000<br> [0x80012fa0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007808]:fadd.d t5, t3, s10, dyn<br> [0x8000780c]:csrrs a7, fcsr, zero<br> [0x80007810]:sw t5, 200(ra)<br> [0x80007814]:sw t6, 208(ra)<br> [0x80007818]:sw t5, 216(ra)<br> [0x8000781c]:sw a7, 224(ra)<br>                                     |
| 339|[0x80012fa8]<br>0x00000000<br> [0x80012fc0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007848]:fadd.d t5, t3, s10, dyn<br> [0x8000784c]:csrrs a7, fcsr, zero<br> [0x80007850]:sw t5, 232(ra)<br> [0x80007854]:sw t6, 240(ra)<br> [0x80007858]:sw t5, 248(ra)<br> [0x8000785c]:sw a7, 256(ra)<br>                                     |
| 340|[0x80012fc8]<br>0x00000000<br> [0x80012fe0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007888]:fadd.d t5, t3, s10, dyn<br> [0x8000788c]:csrrs a7, fcsr, zero<br> [0x80007890]:sw t5, 264(ra)<br> [0x80007894]:sw t6, 272(ra)<br> [0x80007898]:sw t5, 280(ra)<br> [0x8000789c]:sw a7, 288(ra)<br>                                     |
| 341|[0x80012fe8]<br>0x00000000<br> [0x80013000]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800078c8]:fadd.d t5, t3, s10, dyn<br> [0x800078cc]:csrrs a7, fcsr, zero<br> [0x800078d0]:sw t5, 296(ra)<br> [0x800078d4]:sw t6, 304(ra)<br> [0x800078d8]:sw t5, 312(ra)<br> [0x800078dc]:sw a7, 320(ra)<br>                                     |
| 342|[0x80013008]<br>0x00000000<br> [0x80013020]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007908]:fadd.d t5, t3, s10, dyn<br> [0x8000790c]:csrrs a7, fcsr, zero<br> [0x80007910]:sw t5, 328(ra)<br> [0x80007914]:sw t6, 336(ra)<br> [0x80007918]:sw t5, 344(ra)<br> [0x8000791c]:sw a7, 352(ra)<br>                                     |
| 343|[0x80013028]<br>0x00000000<br> [0x80013040]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000794c]:fadd.d t5, t3, s10, dyn<br> [0x80007950]:csrrs a7, fcsr, zero<br> [0x80007954]:sw t5, 360(ra)<br> [0x80007958]:sw t6, 368(ra)<br> [0x8000795c]:sw t5, 376(ra)<br> [0x80007960]:sw a7, 384(ra)<br>                                     |
| 344|[0x80013048]<br>0x00000000<br> [0x80013060]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007990]:fadd.d t5, t3, s10, dyn<br> [0x80007994]:csrrs a7, fcsr, zero<br> [0x80007998]:sw t5, 392(ra)<br> [0x8000799c]:sw t6, 400(ra)<br> [0x800079a0]:sw t5, 408(ra)<br> [0x800079a4]:sw a7, 416(ra)<br>                                     |
| 345|[0x80013068]<br>0x00000000<br> [0x80013080]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800079d0]:fadd.d t5, t3, s10, dyn<br> [0x800079d4]:csrrs a7, fcsr, zero<br> [0x800079d8]:sw t5, 424(ra)<br> [0x800079dc]:sw t6, 432(ra)<br> [0x800079e0]:sw t5, 440(ra)<br> [0x800079e4]:sw a7, 448(ra)<br>                                     |
| 346|[0x80013088]<br>0x00000000<br> [0x800130a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007a10]:fadd.d t5, t3, s10, dyn<br> [0x80007a14]:csrrs a7, fcsr, zero<br> [0x80007a18]:sw t5, 456(ra)<br> [0x80007a1c]:sw t6, 464(ra)<br> [0x80007a20]:sw t5, 472(ra)<br> [0x80007a24]:sw a7, 480(ra)<br>                                     |
| 347|[0x800130a8]<br>0x00000000<br> [0x800130c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007a50]:fadd.d t5, t3, s10, dyn<br> [0x80007a54]:csrrs a7, fcsr, zero<br> [0x80007a58]:sw t5, 488(ra)<br> [0x80007a5c]:sw t6, 496(ra)<br> [0x80007a60]:sw t5, 504(ra)<br> [0x80007a64]:sw a7, 512(ra)<br>                                     |
| 348|[0x800130c8]<br>0x00000000<br> [0x800130e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007a90]:fadd.d t5, t3, s10, dyn<br> [0x80007a94]:csrrs a7, fcsr, zero<br> [0x80007a98]:sw t5, 520(ra)<br> [0x80007a9c]:sw t6, 528(ra)<br> [0x80007aa0]:sw t5, 536(ra)<br> [0x80007aa4]:sw a7, 544(ra)<br>                                     |
| 349|[0x800130e8]<br>0x00000000<br> [0x80013100]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007ad4]:fadd.d t5, t3, s10, dyn<br> [0x80007ad8]:csrrs a7, fcsr, zero<br> [0x80007adc]:sw t5, 552(ra)<br> [0x80007ae0]:sw t6, 560(ra)<br> [0x80007ae4]:sw t5, 568(ra)<br> [0x80007ae8]:sw a7, 576(ra)<br>                                     |
| 350|[0x80013108]<br>0x00000000<br> [0x80013120]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007b18]:fadd.d t5, t3, s10, dyn<br> [0x80007b1c]:csrrs a7, fcsr, zero<br> [0x80007b20]:sw t5, 584(ra)<br> [0x80007b24]:sw t6, 592(ra)<br> [0x80007b28]:sw t5, 600(ra)<br> [0x80007b2c]:sw a7, 608(ra)<br>                                     |
| 351|[0x80013128]<br>0x00000000<br> [0x80013140]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007b58]:fadd.d t5, t3, s10, dyn<br> [0x80007b5c]:csrrs a7, fcsr, zero<br> [0x80007b60]:sw t5, 616(ra)<br> [0x80007b64]:sw t6, 624(ra)<br> [0x80007b68]:sw t5, 632(ra)<br> [0x80007b6c]:sw a7, 640(ra)<br>                                     |
| 352|[0x80013148]<br>0x00000000<br> [0x80013160]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007b98]:fadd.d t5, t3, s10, dyn<br> [0x80007b9c]:csrrs a7, fcsr, zero<br> [0x80007ba0]:sw t5, 648(ra)<br> [0x80007ba4]:sw t6, 656(ra)<br> [0x80007ba8]:sw t5, 664(ra)<br> [0x80007bac]:sw a7, 672(ra)<br>                                     |
| 353|[0x80013168]<br>0x00000000<br> [0x80013180]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007bd8]:fadd.d t5, t3, s10, dyn<br> [0x80007bdc]:csrrs a7, fcsr, zero<br> [0x80007be0]:sw t5, 680(ra)<br> [0x80007be4]:sw t6, 688(ra)<br> [0x80007be8]:sw t5, 696(ra)<br> [0x80007bec]:sw a7, 704(ra)<br>                                     |
| 354|[0x80013188]<br>0x00000000<br> [0x800131a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007c18]:fadd.d t5, t3, s10, dyn<br> [0x80007c1c]:csrrs a7, fcsr, zero<br> [0x80007c20]:sw t5, 712(ra)<br> [0x80007c24]:sw t6, 720(ra)<br> [0x80007c28]:sw t5, 728(ra)<br> [0x80007c2c]:sw a7, 736(ra)<br>                                     |
| 355|[0x800131a8]<br>0x00000000<br> [0x800131c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007c58]:fadd.d t5, t3, s10, dyn<br> [0x80007c5c]:csrrs a7, fcsr, zero<br> [0x80007c60]:sw t5, 744(ra)<br> [0x80007c64]:sw t6, 752(ra)<br> [0x80007c68]:sw t5, 760(ra)<br> [0x80007c6c]:sw a7, 768(ra)<br>                                     |
| 356|[0x800131c8]<br>0x00000000<br> [0x800131e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007c98]:fadd.d t5, t3, s10, dyn<br> [0x80007c9c]:csrrs a7, fcsr, zero<br> [0x80007ca0]:sw t5, 776(ra)<br> [0x80007ca4]:sw t6, 784(ra)<br> [0x80007ca8]:sw t5, 792(ra)<br> [0x80007cac]:sw a7, 800(ra)<br>                                     |
| 357|[0x800131e8]<br>0x00000000<br> [0x80013200]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007cd8]:fadd.d t5, t3, s10, dyn<br> [0x80007cdc]:csrrs a7, fcsr, zero<br> [0x80007ce0]:sw t5, 808(ra)<br> [0x80007ce4]:sw t6, 816(ra)<br> [0x80007ce8]:sw t5, 824(ra)<br> [0x80007cec]:sw a7, 832(ra)<br>                                     |
| 358|[0x80013208]<br>0x00000000<br> [0x80013220]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007d18]:fadd.d t5, t3, s10, dyn<br> [0x80007d1c]:csrrs a7, fcsr, zero<br> [0x80007d20]:sw t5, 840(ra)<br> [0x80007d24]:sw t6, 848(ra)<br> [0x80007d28]:sw t5, 856(ra)<br> [0x80007d2c]:sw a7, 864(ra)<br>                                     |
| 359|[0x80013228]<br>0x00000000<br> [0x80013240]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007d58]:fadd.d t5, t3, s10, dyn<br> [0x80007d5c]:csrrs a7, fcsr, zero<br> [0x80007d60]:sw t5, 872(ra)<br> [0x80007d64]:sw t6, 880(ra)<br> [0x80007d68]:sw t5, 888(ra)<br> [0x80007d6c]:sw a7, 896(ra)<br>                                     |
| 360|[0x80013248]<br>0x00000000<br> [0x80013260]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007d98]:fadd.d t5, t3, s10, dyn<br> [0x80007d9c]:csrrs a7, fcsr, zero<br> [0x80007da0]:sw t5, 904(ra)<br> [0x80007da4]:sw t6, 912(ra)<br> [0x80007da8]:sw t5, 920(ra)<br> [0x80007dac]:sw a7, 928(ra)<br>                                     |
| 361|[0x80013268]<br>0x00000000<br> [0x80013280]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007dd8]:fadd.d t5, t3, s10, dyn<br> [0x80007ddc]:csrrs a7, fcsr, zero<br> [0x80007de0]:sw t5, 936(ra)<br> [0x80007de4]:sw t6, 944(ra)<br> [0x80007de8]:sw t5, 952(ra)<br> [0x80007dec]:sw a7, 960(ra)<br>                                     |
| 362|[0x80013288]<br>0x00000000<br> [0x800132a0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007e18]:fadd.d t5, t3, s10, dyn<br> [0x80007e1c]:csrrs a7, fcsr, zero<br> [0x80007e20]:sw t5, 968(ra)<br> [0x80007e24]:sw t6, 976(ra)<br> [0x80007e28]:sw t5, 984(ra)<br> [0x80007e2c]:sw a7, 992(ra)<br>                                     |
| 363|[0x800132a8]<br>0x00000000<br> [0x800132c0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007e58]:fadd.d t5, t3, s10, dyn<br> [0x80007e5c]:csrrs a7, fcsr, zero<br> [0x80007e60]:sw t5, 1000(ra)<br> [0x80007e64]:sw t6, 1008(ra)<br> [0x80007e68]:sw t5, 1016(ra)<br> [0x80007e6c]:sw a7, 1024(ra)<br>                                 |
| 364|[0x800132c8]<br>0x00000000<br> [0x800132e0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007e98]:fadd.d t5, t3, s10, dyn<br> [0x80007e9c]:csrrs a7, fcsr, zero<br> [0x80007ea0]:sw t5, 1032(ra)<br> [0x80007ea4]:sw t6, 1040(ra)<br> [0x80007ea8]:sw t5, 1048(ra)<br> [0x80007eac]:sw a7, 1056(ra)<br>                                 |
| 365|[0x800132e8]<br>0x00000000<br> [0x80013300]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007ed8]:fadd.d t5, t3, s10, dyn<br> [0x80007edc]:csrrs a7, fcsr, zero<br> [0x80007ee0]:sw t5, 1064(ra)<br> [0x80007ee4]:sw t6, 1072(ra)<br> [0x80007ee8]:sw t5, 1080(ra)<br> [0x80007eec]:sw a7, 1088(ra)<br>                                 |
| 366|[0x80013308]<br>0x00000000<br> [0x80013320]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007f18]:fadd.d t5, t3, s10, dyn<br> [0x80007f1c]:csrrs a7, fcsr, zero<br> [0x80007f20]:sw t5, 1096(ra)<br> [0x80007f24]:sw t6, 1104(ra)<br> [0x80007f28]:sw t5, 1112(ra)<br> [0x80007f2c]:sw a7, 1120(ra)<br>                                 |
| 367|[0x80013328]<br>0x00000000<br> [0x80013340]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007f5c]:fadd.d t5, t3, s10, dyn<br> [0x80007f60]:csrrs a7, fcsr, zero<br> [0x80007f64]:sw t5, 1128(ra)<br> [0x80007f68]:sw t6, 1136(ra)<br> [0x80007f6c]:sw t5, 1144(ra)<br> [0x80007f70]:sw a7, 1152(ra)<br>                                 |
| 368|[0x80013348]<br>0x00000000<br> [0x80013360]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007fa0]:fadd.d t5, t3, s10, dyn<br> [0x80007fa4]:csrrs a7, fcsr, zero<br> [0x80007fa8]:sw t5, 1160(ra)<br> [0x80007fac]:sw t6, 1168(ra)<br> [0x80007fb0]:sw t5, 1176(ra)<br> [0x80007fb4]:sw a7, 1184(ra)<br>                                 |
| 369|[0x80013368]<br>0x00000000<br> [0x80013380]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80007fe0]:fadd.d t5, t3, s10, dyn<br> [0x80007fe4]:csrrs a7, fcsr, zero<br> [0x80007fe8]:sw t5, 1192(ra)<br> [0x80007fec]:sw t6, 1200(ra)<br> [0x80007ff0]:sw t5, 1208(ra)<br> [0x80007ff4]:sw a7, 1216(ra)<br>                                 |
| 370|[0x80013388]<br>0x00000000<br> [0x800133a0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008020]:fadd.d t5, t3, s10, dyn<br> [0x80008024]:csrrs a7, fcsr, zero<br> [0x80008028]:sw t5, 1224(ra)<br> [0x8000802c]:sw t6, 1232(ra)<br> [0x80008030]:sw t5, 1240(ra)<br> [0x80008034]:sw a7, 1248(ra)<br>                                 |
| 371|[0x800133a8]<br>0x00000000<br> [0x800133c0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008060]:fadd.d t5, t3, s10, dyn<br> [0x80008064]:csrrs a7, fcsr, zero<br> [0x80008068]:sw t5, 1256(ra)<br> [0x8000806c]:sw t6, 1264(ra)<br> [0x80008070]:sw t5, 1272(ra)<br> [0x80008074]:sw a7, 1280(ra)<br>                                 |
| 372|[0x800133c8]<br>0x00000000<br> [0x800133e0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800080a0]:fadd.d t5, t3, s10, dyn<br> [0x800080a4]:csrrs a7, fcsr, zero<br> [0x800080a8]:sw t5, 1288(ra)<br> [0x800080ac]:sw t6, 1296(ra)<br> [0x800080b0]:sw t5, 1304(ra)<br> [0x800080b4]:sw a7, 1312(ra)<br>                                 |
| 373|[0x800133e8]<br>0x00000000<br> [0x80013400]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800080e4]:fadd.d t5, t3, s10, dyn<br> [0x800080e8]:csrrs a7, fcsr, zero<br> [0x800080ec]:sw t5, 1320(ra)<br> [0x800080f0]:sw t6, 1328(ra)<br> [0x800080f4]:sw t5, 1336(ra)<br> [0x800080f8]:sw a7, 1344(ra)<br>                                 |
| 374|[0x80013408]<br>0x00000000<br> [0x80013420]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008128]:fadd.d t5, t3, s10, dyn<br> [0x8000812c]:csrrs a7, fcsr, zero<br> [0x80008130]:sw t5, 1352(ra)<br> [0x80008134]:sw t6, 1360(ra)<br> [0x80008138]:sw t5, 1368(ra)<br> [0x8000813c]:sw a7, 1376(ra)<br>                                 |
| 375|[0x80013428]<br>0x00000000<br> [0x80013440]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008168]:fadd.d t5, t3, s10, dyn<br> [0x8000816c]:csrrs a7, fcsr, zero<br> [0x80008170]:sw t5, 1384(ra)<br> [0x80008174]:sw t6, 1392(ra)<br> [0x80008178]:sw t5, 1400(ra)<br> [0x8000817c]:sw a7, 1408(ra)<br>                                 |
| 376|[0x80013448]<br>0x00000000<br> [0x80013460]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800081a8]:fadd.d t5, t3, s10, dyn<br> [0x800081ac]:csrrs a7, fcsr, zero<br> [0x800081b0]:sw t5, 1416(ra)<br> [0x800081b4]:sw t6, 1424(ra)<br> [0x800081b8]:sw t5, 1432(ra)<br> [0x800081bc]:sw a7, 1440(ra)<br>                                 |
| 377|[0x80013468]<br>0x00000000<br> [0x80013480]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800081e8]:fadd.d t5, t3, s10, dyn<br> [0x800081ec]:csrrs a7, fcsr, zero<br> [0x800081f0]:sw t5, 1448(ra)<br> [0x800081f4]:sw t6, 1456(ra)<br> [0x800081f8]:sw t5, 1464(ra)<br> [0x800081fc]:sw a7, 1472(ra)<br>                                 |
| 378|[0x80013488]<br>0x00000000<br> [0x800134a0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008228]:fadd.d t5, t3, s10, dyn<br> [0x8000822c]:csrrs a7, fcsr, zero<br> [0x80008230]:sw t5, 1480(ra)<br> [0x80008234]:sw t6, 1488(ra)<br> [0x80008238]:sw t5, 1496(ra)<br> [0x8000823c]:sw a7, 1504(ra)<br>                                 |
| 379|[0x800134a8]<br>0x00000000<br> [0x800134c0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008268]:fadd.d t5, t3, s10, dyn<br> [0x8000826c]:csrrs a7, fcsr, zero<br> [0x80008270]:sw t5, 1512(ra)<br> [0x80008274]:sw t6, 1520(ra)<br> [0x80008278]:sw t5, 1528(ra)<br> [0x8000827c]:sw a7, 1536(ra)<br>                                 |
| 380|[0x800134c8]<br>0x00000000<br> [0x800134e0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800082a8]:fadd.d t5, t3, s10, dyn<br> [0x800082ac]:csrrs a7, fcsr, zero<br> [0x800082b0]:sw t5, 1544(ra)<br> [0x800082b4]:sw t6, 1552(ra)<br> [0x800082b8]:sw t5, 1560(ra)<br> [0x800082bc]:sw a7, 1568(ra)<br>                                 |
| 381|[0x800134e8]<br>0x00000000<br> [0x80013500]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800082e8]:fadd.d t5, t3, s10, dyn<br> [0x800082ec]:csrrs a7, fcsr, zero<br> [0x800082f0]:sw t5, 1576(ra)<br> [0x800082f4]:sw t6, 1584(ra)<br> [0x800082f8]:sw t5, 1592(ra)<br> [0x800082fc]:sw a7, 1600(ra)<br>                                 |
| 382|[0x80013508]<br>0x00000000<br> [0x80013520]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008328]:fadd.d t5, t3, s10, dyn<br> [0x8000832c]:csrrs a7, fcsr, zero<br> [0x80008330]:sw t5, 1608(ra)<br> [0x80008334]:sw t6, 1616(ra)<br> [0x80008338]:sw t5, 1624(ra)<br> [0x8000833c]:sw a7, 1632(ra)<br>                                 |
| 383|[0x80013528]<br>0x00000000<br> [0x80013540]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008368]:fadd.d t5, t3, s10, dyn<br> [0x8000836c]:csrrs a7, fcsr, zero<br> [0x80008370]:sw t5, 1640(ra)<br> [0x80008374]:sw t6, 1648(ra)<br> [0x80008378]:sw t5, 1656(ra)<br> [0x8000837c]:sw a7, 1664(ra)<br>                                 |
| 384|[0x80013548]<br>0x00000000<br> [0x80013560]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800083a8]:fadd.d t5, t3, s10, dyn<br> [0x800083ac]:csrrs a7, fcsr, zero<br> [0x800083b0]:sw t5, 1672(ra)<br> [0x800083b4]:sw t6, 1680(ra)<br> [0x800083b8]:sw t5, 1688(ra)<br> [0x800083bc]:sw a7, 1696(ra)<br>                                 |
| 385|[0x80013568]<br>0x00000000<br> [0x80013580]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800083e8]:fadd.d t5, t3, s10, dyn<br> [0x800083ec]:csrrs a7, fcsr, zero<br> [0x800083f0]:sw t5, 1704(ra)<br> [0x800083f4]:sw t6, 1712(ra)<br> [0x800083f8]:sw t5, 1720(ra)<br> [0x800083fc]:sw a7, 1728(ra)<br>                                 |
| 386|[0x80013588]<br>0x00000000<br> [0x800135a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008428]:fadd.d t5, t3, s10, dyn<br> [0x8000842c]:csrrs a7, fcsr, zero<br> [0x80008430]:sw t5, 1736(ra)<br> [0x80008434]:sw t6, 1744(ra)<br> [0x80008438]:sw t5, 1752(ra)<br> [0x8000843c]:sw a7, 1760(ra)<br>                                 |
| 387|[0x800135a8]<br>0x00000000<br> [0x800135c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008468]:fadd.d t5, t3, s10, dyn<br> [0x8000846c]:csrrs a7, fcsr, zero<br> [0x80008470]:sw t5, 1768(ra)<br> [0x80008474]:sw t6, 1776(ra)<br> [0x80008478]:sw t5, 1784(ra)<br> [0x8000847c]:sw a7, 1792(ra)<br>                                 |
| 388|[0x800135c8]<br>0x00000000<br> [0x800135e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800084a8]:fadd.d t5, t3, s10, dyn<br> [0x800084ac]:csrrs a7, fcsr, zero<br> [0x800084b0]:sw t5, 1800(ra)<br> [0x800084b4]:sw t6, 1808(ra)<br> [0x800084b8]:sw t5, 1816(ra)<br> [0x800084bc]:sw a7, 1824(ra)<br>                                 |
| 389|[0x800135e8]<br>0x00000000<br> [0x80013600]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800084e8]:fadd.d t5, t3, s10, dyn<br> [0x800084ec]:csrrs a7, fcsr, zero<br> [0x800084f0]:sw t5, 1832(ra)<br> [0x800084f4]:sw t6, 1840(ra)<br> [0x800084f8]:sw t5, 1848(ra)<br> [0x800084fc]:sw a7, 1856(ra)<br>                                 |
| 390|[0x80013608]<br>0x00000000<br> [0x80013620]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008528]:fadd.d t5, t3, s10, dyn<br> [0x8000852c]:csrrs a7, fcsr, zero<br> [0x80008530]:sw t5, 1864(ra)<br> [0x80008534]:sw t6, 1872(ra)<br> [0x80008538]:sw t5, 1880(ra)<br> [0x8000853c]:sw a7, 1888(ra)<br>                                 |
| 391|[0x80013628]<br>0x00000000<br> [0x80013640]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000856c]:fadd.d t5, t3, s10, dyn<br> [0x80008570]:csrrs a7, fcsr, zero<br> [0x80008574]:sw t5, 1896(ra)<br> [0x80008578]:sw t6, 1904(ra)<br> [0x8000857c]:sw t5, 1912(ra)<br> [0x80008580]:sw a7, 1920(ra)<br>                                 |
| 392|[0x80013648]<br>0x00000000<br> [0x80013660]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800085b0]:fadd.d t5, t3, s10, dyn<br> [0x800085b4]:csrrs a7, fcsr, zero<br> [0x800085b8]:sw t5, 1928(ra)<br> [0x800085bc]:sw t6, 1936(ra)<br> [0x800085c0]:sw t5, 1944(ra)<br> [0x800085c4]:sw a7, 1952(ra)<br>                                 |
| 393|[0x80013668]<br>0x00000000<br> [0x80013680]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800085f0]:fadd.d t5, t3, s10, dyn<br> [0x800085f4]:csrrs a7, fcsr, zero<br> [0x800085f8]:sw t5, 1960(ra)<br> [0x800085fc]:sw t6, 1968(ra)<br> [0x80008600]:sw t5, 1976(ra)<br> [0x80008604]:sw a7, 1984(ra)<br>                                 |
| 394|[0x80013688]<br>0x00000000<br> [0x800136a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008630]:fadd.d t5, t3, s10, dyn<br> [0x80008634]:csrrs a7, fcsr, zero<br> [0x80008638]:sw t5, 1992(ra)<br> [0x8000863c]:sw t6, 2000(ra)<br> [0x80008640]:sw t5, 2008(ra)<br> [0x80008644]:sw a7, 2016(ra)<br>                                 |
| 395|[0x800136a8]<br>0x00000000<br> [0x800136c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008670]:fadd.d t5, t3, s10, dyn<br> [0x80008674]:csrrs a7, fcsr, zero<br> [0x80008678]:sw t5, 2024(ra)<br> [0x8000867c]:sw t6, 2032(ra)<br> [0x80008680]:sw t5, 2040(ra)<br> [0x80008684]:addi ra, ra, 2040<br> [0x80008688]:sw a7, 8(ra)<br> |
| 396|[0x800136c8]<br>0x00000000<br> [0x800136e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800086f4]:fadd.d t5, t3, s10, dyn<br> [0x800086f8]:csrrs a7, fcsr, zero<br> [0x800086fc]:sw t5, 16(ra)<br> [0x80008700]:sw t6, 24(ra)<br> [0x80008704]:sw t5, 32(ra)<br> [0x80008708]:sw a7, 40(ra)<br>                                         |
| 397|[0x800136e8]<br>0x00000000<br> [0x80013700]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008778]:fadd.d t5, t3, s10, dyn<br> [0x8000877c]:csrrs a7, fcsr, zero<br> [0x80008780]:sw t5, 48(ra)<br> [0x80008784]:sw t6, 56(ra)<br> [0x80008788]:sw t5, 64(ra)<br> [0x8000878c]:sw a7, 72(ra)<br>                                         |
| 398|[0x80013708]<br>0x00000000<br> [0x80013720]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800087fc]:fadd.d t5, t3, s10, dyn<br> [0x80008800]:csrrs a7, fcsr, zero<br> [0x80008804]:sw t5, 80(ra)<br> [0x80008808]:sw t6, 88(ra)<br> [0x8000880c]:sw t5, 96(ra)<br> [0x80008810]:sw a7, 104(ra)<br>                                        |
| 399|[0x80013728]<br>0x00000000<br> [0x80013740]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000887c]:fadd.d t5, t3, s10, dyn<br> [0x80008880]:csrrs a7, fcsr, zero<br> [0x80008884]:sw t5, 112(ra)<br> [0x80008888]:sw t6, 120(ra)<br> [0x8000888c]:sw t5, 128(ra)<br> [0x80008890]:sw a7, 136(ra)<br>                                     |
| 400|[0x80013748]<br>0x00000000<br> [0x80013760]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800088fc]:fadd.d t5, t3, s10, dyn<br> [0x80008900]:csrrs a7, fcsr, zero<br> [0x80008904]:sw t5, 144(ra)<br> [0x80008908]:sw t6, 152(ra)<br> [0x8000890c]:sw t5, 160(ra)<br> [0x80008910]:sw a7, 168(ra)<br>                                     |
| 401|[0x80013768]<br>0x00000000<br> [0x80013780]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000897c]:fadd.d t5, t3, s10, dyn<br> [0x80008980]:csrrs a7, fcsr, zero<br> [0x80008984]:sw t5, 176(ra)<br> [0x80008988]:sw t6, 184(ra)<br> [0x8000898c]:sw t5, 192(ra)<br> [0x80008990]:sw a7, 200(ra)<br>                                     |
| 402|[0x80013788]<br>0x00000000<br> [0x800137a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x800089fc]:fadd.d t5, t3, s10, dyn<br> [0x80008a00]:csrrs a7, fcsr, zero<br> [0x80008a04]:sw t5, 208(ra)<br> [0x80008a08]:sw t6, 216(ra)<br> [0x80008a0c]:sw t5, 224(ra)<br> [0x80008a10]:sw a7, 232(ra)<br>                                     |
| 403|[0x800137a8]<br>0x00000000<br> [0x800137c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008a7c]:fadd.d t5, t3, s10, dyn<br> [0x80008a80]:csrrs a7, fcsr, zero<br> [0x80008a84]:sw t5, 240(ra)<br> [0x80008a88]:sw t6, 248(ra)<br> [0x80008a8c]:sw t5, 256(ra)<br> [0x80008a90]:sw a7, 264(ra)<br>                                     |
| 404|[0x800137c8]<br>0x00000000<br> [0x800137e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008afc]:fadd.d t5, t3, s10, dyn<br> [0x80008b00]:csrrs a7, fcsr, zero<br> [0x80008b04]:sw t5, 272(ra)<br> [0x80008b08]:sw t6, 280(ra)<br> [0x80008b0c]:sw t5, 288(ra)<br> [0x80008b10]:sw a7, 296(ra)<br>                                     |
| 405|[0x800137e8]<br>0x00000000<br> [0x80013800]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008b7c]:fadd.d t5, t3, s10, dyn<br> [0x80008b80]:csrrs a7, fcsr, zero<br> [0x80008b84]:sw t5, 304(ra)<br> [0x80008b88]:sw t6, 312(ra)<br> [0x80008b8c]:sw t5, 320(ra)<br> [0x80008b90]:sw a7, 328(ra)<br>                                     |
| 406|[0x80013808]<br>0x00000000<br> [0x80013820]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008bfc]:fadd.d t5, t3, s10, dyn<br> [0x80008c00]:csrrs a7, fcsr, zero<br> [0x80008c04]:sw t5, 336(ra)<br> [0x80008c08]:sw t6, 344(ra)<br> [0x80008c0c]:sw t5, 352(ra)<br> [0x80008c10]:sw a7, 360(ra)<br>                                     |
| 407|[0x80013828]<br>0x00000000<br> [0x80013840]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008c7c]:fadd.d t5, t3, s10, dyn<br> [0x80008c80]:csrrs a7, fcsr, zero<br> [0x80008c84]:sw t5, 368(ra)<br> [0x80008c88]:sw t6, 376(ra)<br> [0x80008c8c]:sw t5, 384(ra)<br> [0x80008c90]:sw a7, 392(ra)<br>                                     |
| 408|[0x80013848]<br>0x00000000<br> [0x80013860]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008cfc]:fadd.d t5, t3, s10, dyn<br> [0x80008d00]:csrrs a7, fcsr, zero<br> [0x80008d04]:sw t5, 400(ra)<br> [0x80008d08]:sw t6, 408(ra)<br> [0x80008d0c]:sw t5, 416(ra)<br> [0x80008d10]:sw a7, 424(ra)<br>                                     |
| 409|[0x80013868]<br>0x00000000<br> [0x80013880]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008d7c]:fadd.d t5, t3, s10, dyn<br> [0x80008d80]:csrrs a7, fcsr, zero<br> [0x80008d84]:sw t5, 432(ra)<br> [0x80008d88]:sw t6, 440(ra)<br> [0x80008d8c]:sw t5, 448(ra)<br> [0x80008d90]:sw a7, 456(ra)<br>                                     |
| 410|[0x80013888]<br>0x00000000<br> [0x800138a0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008dfc]:fadd.d t5, t3, s10, dyn<br> [0x80008e00]:csrrs a7, fcsr, zero<br> [0x80008e04]:sw t5, 464(ra)<br> [0x80008e08]:sw t6, 472(ra)<br> [0x80008e0c]:sw t5, 480(ra)<br> [0x80008e10]:sw a7, 488(ra)<br>                                     |
| 411|[0x800138a8]<br>0x00000000<br> [0x800138c0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008e7c]:fadd.d t5, t3, s10, dyn<br> [0x80008e80]:csrrs a7, fcsr, zero<br> [0x80008e84]:sw t5, 496(ra)<br> [0x80008e88]:sw t6, 504(ra)<br> [0x80008e8c]:sw t5, 512(ra)<br> [0x80008e90]:sw a7, 520(ra)<br>                                     |
| 412|[0x800138c8]<br>0x00000000<br> [0x800138e0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008efc]:fadd.d t5, t3, s10, dyn<br> [0x80008f00]:csrrs a7, fcsr, zero<br> [0x80008f04]:sw t5, 528(ra)<br> [0x80008f08]:sw t6, 536(ra)<br> [0x80008f0c]:sw t5, 544(ra)<br> [0x80008f10]:sw a7, 552(ra)<br>                                     |
| 413|[0x800138e8]<br>0x00000000<br> [0x80013900]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008f7c]:fadd.d t5, t3, s10, dyn<br> [0x80008f80]:csrrs a7, fcsr, zero<br> [0x80008f84]:sw t5, 560(ra)<br> [0x80008f88]:sw t6, 568(ra)<br> [0x80008f8c]:sw t5, 576(ra)<br> [0x80008f90]:sw a7, 584(ra)<br>                                     |
| 414|[0x80013908]<br>0x00000000<br> [0x80013920]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80008ffc]:fadd.d t5, t3, s10, dyn<br> [0x80009000]:csrrs a7, fcsr, zero<br> [0x80009004]:sw t5, 592(ra)<br> [0x80009008]:sw t6, 600(ra)<br> [0x8000900c]:sw t5, 608(ra)<br> [0x80009010]:sw a7, 616(ra)<br>                                     |
| 415|[0x80013928]<br>0x00000000<br> [0x80013940]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80009080]:fadd.d t5, t3, s10, dyn<br> [0x80009084]:csrrs a7, fcsr, zero<br> [0x80009088]:sw t5, 624(ra)<br> [0x8000908c]:sw t6, 632(ra)<br> [0x80009090]:sw t5, 640(ra)<br> [0x80009094]:sw a7, 648(ra)<br>                                     |
| 416|[0x80013948]<br>0x00000000<br> [0x80013960]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80009104]:fadd.d t5, t3, s10, dyn<br> [0x80009108]:csrrs a7, fcsr, zero<br> [0x8000910c]:sw t5, 656(ra)<br> [0x80009110]:sw t6, 664(ra)<br> [0x80009114]:sw t5, 672(ra)<br> [0x80009118]:sw a7, 680(ra)<br>                                     |
| 417|[0x80013968]<br>0x00000000<br> [0x80013980]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80009184]:fadd.d t5, t3, s10, dyn<br> [0x80009188]:csrrs a7, fcsr, zero<br> [0x8000918c]:sw t5, 688(ra)<br> [0x80009190]:sw t6, 696(ra)<br> [0x80009194]:sw t5, 704(ra)<br> [0x80009198]:sw a7, 712(ra)<br>                                     |
| 418|[0x80013988]<br>0x00000000<br> [0x800139a0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80009204]:fadd.d t5, t3, s10, dyn<br> [0x80009208]:csrrs a7, fcsr, zero<br> [0x8000920c]:sw t5, 720(ra)<br> [0x80009210]:sw t6, 728(ra)<br> [0x80009214]:sw t5, 736(ra)<br> [0x80009218]:sw a7, 744(ra)<br>                                     |
| 419|[0x800139a8]<br>0x00000000<br> [0x800139c0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80009284]:fadd.d t5, t3, s10, dyn<br> [0x80009288]:csrrs a7, fcsr, zero<br> [0x8000928c]:sw t5, 752(ra)<br> [0x80009290]:sw t6, 760(ra)<br> [0x80009294]:sw t5, 768(ra)<br> [0x80009298]:sw a7, 776(ra)<br>                                     |
| 420|[0x800139c8]<br>0x00000000<br> [0x800139e0]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80009304]:fadd.d t5, t3, s10, dyn<br> [0x80009308]:csrrs a7, fcsr, zero<br> [0x8000930c]:sw t5, 784(ra)<br> [0x80009310]:sw t6, 792(ra)<br> [0x80009314]:sw t5, 800(ra)<br> [0x80009318]:sw a7, 808(ra)<br>                                     |
| 421|[0x800139e8]<br>0x00000000<br> [0x80013a00]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x80009388]:fadd.d t5, t3, s10, dyn<br> [0x8000938c]:csrrs a7, fcsr, zero<br> [0x80009390]:sw t5, 816(ra)<br> [0x80009394]:sw t6, 824(ra)<br> [0x80009398]:sw t5, 832(ra)<br> [0x8000939c]:sw a7, 840(ra)<br>                                     |
| 422|[0x80013a08]<br>0x00000000<br> [0x80013a20]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000940c]:fadd.d t5, t3, s10, dyn<br> [0x80009410]:csrrs a7, fcsr, zero<br> [0x80009414]:sw t5, 848(ra)<br> [0x80009418]:sw t6, 856(ra)<br> [0x8000941c]:sw t5, 864(ra)<br> [0x80009420]:sw a7, 872(ra)<br>                                     |
| 423|[0x80013a28]<br>0x00000000<br> [0x80013a40]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000948c]:fadd.d t5, t3, s10, dyn<br> [0x80009490]:csrrs a7, fcsr, zero<br> [0x80009494]:sw t5, 880(ra)<br> [0x80009498]:sw t6, 888(ra)<br> [0x8000949c]:sw t5, 896(ra)<br> [0x800094a0]:sw a7, 904(ra)<br>                                     |
| 424|[0x800136c8]<br>0x00000000<br> [0x800136e0]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000c71c]:fadd.d t5, t3, s10, dyn<br> [0x8000c720]:csrrs a7, fcsr, zero<br> [0x8000c724]:sw t5, 0(ra)<br> [0x8000c728]:sw t6, 8(ra)<br> [0x8000c72c]:sw t5, 16(ra)<br> [0x8000c730]:sw a7, 24(ra)<br>                                           |
| 425|[0x800136e8]<br>0x00000000<br> [0x80013700]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000c75c]:fadd.d t5, t3, s10, dyn<br> [0x8000c760]:csrrs a7, fcsr, zero<br> [0x8000c764]:sw t5, 32(ra)<br> [0x8000c768]:sw t6, 40(ra)<br> [0x8000c76c]:sw t5, 48(ra)<br> [0x8000c770]:sw a7, 56(ra)<br>                                         |
| 426|[0x80013708]<br>0x00000000<br> [0x80013720]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000c79c]:fadd.d t5, t3, s10, dyn<br> [0x8000c7a0]:csrrs a7, fcsr, zero<br> [0x8000c7a4]:sw t5, 64(ra)<br> [0x8000c7a8]:sw t6, 72(ra)<br> [0x8000c7ac]:sw t5, 80(ra)<br> [0x8000c7b0]:sw a7, 88(ra)<br>                                         |
| 427|[0x80013728]<br>0x00000000<br> [0x80013740]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000c7dc]:fadd.d t5, t3, s10, dyn<br> [0x8000c7e0]:csrrs a7, fcsr, zero<br> [0x8000c7e4]:sw t5, 96(ra)<br> [0x8000c7e8]:sw t6, 104(ra)<br> [0x8000c7ec]:sw t5, 112(ra)<br> [0x8000c7f0]:sw a7, 120(ra)<br>                                      |
| 428|[0x80013748]<br>0x00000000<br> [0x80013760]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000c81c]:fadd.d t5, t3, s10, dyn<br> [0x8000c820]:csrrs a7, fcsr, zero<br> [0x8000c824]:sw t5, 128(ra)<br> [0x8000c828]:sw t6, 136(ra)<br> [0x8000c82c]:sw t5, 144(ra)<br> [0x8000c830]:sw a7, 152(ra)<br>                                     |
| 429|[0x80013768]<br>0x00000000<br> [0x80013780]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000c85c]:fadd.d t5, t3, s10, dyn<br> [0x8000c860]:csrrs a7, fcsr, zero<br> [0x8000c864]:sw t5, 160(ra)<br> [0x8000c868]:sw t6, 168(ra)<br> [0x8000c86c]:sw t5, 176(ra)<br> [0x8000c870]:sw a7, 184(ra)<br>                                     |
| 430|[0x80013788]<br>0x00000000<br> [0x800137a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000c89c]:fadd.d t5, t3, s10, dyn<br> [0x8000c8a0]:csrrs a7, fcsr, zero<br> [0x8000c8a4]:sw t5, 192(ra)<br> [0x8000c8a8]:sw t6, 200(ra)<br> [0x8000c8ac]:sw t5, 208(ra)<br> [0x8000c8b0]:sw a7, 216(ra)<br>                                     |
| 431|[0x800137a8]<br>0x00000000<br> [0x800137c0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000c8dc]:fadd.d t5, t3, s10, dyn<br> [0x8000c8e0]:csrrs a7, fcsr, zero<br> [0x8000c8e4]:sw t5, 224(ra)<br> [0x8000c8e8]:sw t6, 232(ra)<br> [0x8000c8ec]:sw t5, 240(ra)<br> [0x8000c8f0]:sw a7, 248(ra)<br>                                     |
| 432|[0x800137c8]<br>0x00000000<br> [0x800137e0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000c91c]:fadd.d t5, t3, s10, dyn<br> [0x8000c920]:csrrs a7, fcsr, zero<br> [0x8000c924]:sw t5, 256(ra)<br> [0x8000c928]:sw t6, 264(ra)<br> [0x8000c92c]:sw t5, 272(ra)<br> [0x8000c930]:sw a7, 280(ra)<br>                                     |
| 433|[0x800137e8]<br>0x00000000<br> [0x80013800]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000c95c]:fadd.d t5, t3, s10, dyn<br> [0x8000c960]:csrrs a7, fcsr, zero<br> [0x8000c964]:sw t5, 288(ra)<br> [0x8000c968]:sw t6, 296(ra)<br> [0x8000c96c]:sw t5, 304(ra)<br> [0x8000c970]:sw a7, 312(ra)<br>                                     |
| 434|[0x80013808]<br>0x00000000<br> [0x80013820]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000c99c]:fadd.d t5, t3, s10, dyn<br> [0x8000c9a0]:csrrs a7, fcsr, zero<br> [0x8000c9a4]:sw t5, 320(ra)<br> [0x8000c9a8]:sw t6, 328(ra)<br> [0x8000c9ac]:sw t5, 336(ra)<br> [0x8000c9b0]:sw a7, 344(ra)<br>                                     |
| 435|[0x80013828]<br>0x00000000<br> [0x80013840]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000c9e0]:fadd.d t5, t3, s10, dyn<br> [0x8000c9e4]:csrrs a7, fcsr, zero<br> [0x8000c9e8]:sw t5, 352(ra)<br> [0x8000c9ec]:sw t6, 360(ra)<br> [0x8000c9f0]:sw t5, 368(ra)<br> [0x8000c9f4]:sw a7, 376(ra)<br>                                     |
| 436|[0x80013848]<br>0x00000000<br> [0x80013860]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000ca24]:fadd.d t5, t3, s10, dyn<br> [0x8000ca28]:csrrs a7, fcsr, zero<br> [0x8000ca2c]:sw t5, 384(ra)<br> [0x8000ca30]:sw t6, 392(ra)<br> [0x8000ca34]:sw t5, 400(ra)<br> [0x8000ca38]:sw a7, 408(ra)<br>                                     |
| 437|[0x80013868]<br>0x00000000<br> [0x80013880]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000ca64]:fadd.d t5, t3, s10, dyn<br> [0x8000ca68]:csrrs a7, fcsr, zero<br> [0x8000ca6c]:sw t5, 416(ra)<br> [0x8000ca70]:sw t6, 424(ra)<br> [0x8000ca74]:sw t5, 432(ra)<br> [0x8000ca78]:sw a7, 440(ra)<br>                                     |
| 438|[0x80013888]<br>0x00000000<br> [0x800138a0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000caa4]:fadd.d t5, t3, s10, dyn<br> [0x8000caa8]:csrrs a7, fcsr, zero<br> [0x8000caac]:sw t5, 448(ra)<br> [0x8000cab0]:sw t6, 456(ra)<br> [0x8000cab4]:sw t5, 464(ra)<br> [0x8000cab8]:sw a7, 472(ra)<br>                                     |
| 439|[0x800138a8]<br>0x00000000<br> [0x800138c0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000cae4]:fadd.d t5, t3, s10, dyn<br> [0x8000cae8]:csrrs a7, fcsr, zero<br> [0x8000caec]:sw t5, 480(ra)<br> [0x8000caf0]:sw t6, 488(ra)<br> [0x8000caf4]:sw t5, 496(ra)<br> [0x8000caf8]:sw a7, 504(ra)<br>                                     |
| 440|[0x800138c8]<br>0x00000000<br> [0x800138e0]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000cb24]:fadd.d t5, t3, s10, dyn<br> [0x8000cb28]:csrrs a7, fcsr, zero<br> [0x8000cb2c]:sw t5, 512(ra)<br> [0x8000cb30]:sw t6, 520(ra)<br> [0x8000cb34]:sw t5, 528(ra)<br> [0x8000cb38]:sw a7, 536(ra)<br>                                     |
| 441|[0x800138e8]<br>0xFFFFFFFF<br> [0x80013900]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000cb68]:fadd.d t5, t3, s10, dyn<br> [0x8000cb6c]:csrrs a7, fcsr, zero<br> [0x8000cb70]:sw t5, 544(ra)<br> [0x8000cb74]:sw t6, 552(ra)<br> [0x8000cb78]:sw t5, 560(ra)<br> [0x8000cb7c]:sw a7, 568(ra)<br>                                     |
| 442|[0x80013908]<br>0xFFFFFFFF<br> [0x80013920]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000cbac]:fadd.d t5, t3, s10, dyn<br> [0x8000cbb0]:csrrs a7, fcsr, zero<br> [0x8000cbb4]:sw t5, 576(ra)<br> [0x8000cbb8]:sw t6, 584(ra)<br> [0x8000cbbc]:sw t5, 592(ra)<br> [0x8000cbc0]:sw a7, 600(ra)<br>                                     |
| 443|[0x80013928]<br>0x00000000<br> [0x80013940]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000cbec]:fadd.d t5, t3, s10, dyn<br> [0x8000cbf0]:csrrs a7, fcsr, zero<br> [0x8000cbf4]:sw t5, 608(ra)<br> [0x8000cbf8]:sw t6, 616(ra)<br> [0x8000cbfc]:sw t5, 624(ra)<br> [0x8000cc00]:sw a7, 632(ra)<br>                                     |
| 444|[0x80013948]<br>0x00000000<br> [0x80013960]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000cc2c]:fadd.d t5, t3, s10, dyn<br> [0x8000cc30]:csrrs a7, fcsr, zero<br> [0x8000cc34]:sw t5, 640(ra)<br> [0x8000cc38]:sw t6, 648(ra)<br> [0x8000cc3c]:sw t5, 656(ra)<br> [0x8000cc40]:sw a7, 664(ra)<br>                                     |
| 445|[0x80013968]<br>0x00000000<br> [0x80013980]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000cc6c]:fadd.d t5, t3, s10, dyn<br> [0x8000cc70]:csrrs a7, fcsr, zero<br> [0x8000cc74]:sw t5, 672(ra)<br> [0x8000cc78]:sw t6, 680(ra)<br> [0x8000cc7c]:sw t5, 688(ra)<br> [0x8000cc80]:sw a7, 696(ra)<br>                                     |
| 446|[0x80013988]<br>0x00000000<br> [0x800139a0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000ccac]:fadd.d t5, t3, s10, dyn<br> [0x8000ccb0]:csrrs a7, fcsr, zero<br> [0x8000ccb4]:sw t5, 704(ra)<br> [0x8000ccb8]:sw t6, 712(ra)<br> [0x8000ccbc]:sw t5, 720(ra)<br> [0x8000ccc0]:sw a7, 728(ra)<br>                                     |
| 447|[0x800139a8]<br>0x00000000<br> [0x800139c0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000ccec]:fadd.d t5, t3, s10, dyn<br> [0x8000ccf0]:csrrs a7, fcsr, zero<br> [0x8000ccf4]:sw t5, 736(ra)<br> [0x8000ccf8]:sw t6, 744(ra)<br> [0x8000ccfc]:sw t5, 752(ra)<br> [0x8000cd00]:sw a7, 760(ra)<br>                                     |
| 448|[0x800139c8]<br>0x00000000<br> [0x800139e0]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000cd2c]:fadd.d t5, t3, s10, dyn<br> [0x8000cd30]:csrrs a7, fcsr, zero<br> [0x8000cd34]:sw t5, 768(ra)<br> [0x8000cd38]:sw t6, 776(ra)<br> [0x8000cd3c]:sw t5, 784(ra)<br> [0x8000cd40]:sw a7, 792(ra)<br>                                     |
| 449|[0x800139e8]<br>0x00000000<br> [0x80013a00]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000cd6c]:fadd.d t5, t3, s10, dyn<br> [0x8000cd70]:csrrs a7, fcsr, zero<br> [0x8000cd74]:sw t5, 800(ra)<br> [0x8000cd78]:sw t6, 808(ra)<br> [0x8000cd7c]:sw t5, 816(ra)<br> [0x8000cd80]:sw a7, 824(ra)<br>                                     |
| 450|[0x80013a08]<br>0x00000000<br> [0x80013a20]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000cdac]:fadd.d t5, t3, s10, dyn<br> [0x8000cdb0]:csrrs a7, fcsr, zero<br> [0x8000cdb4]:sw t5, 832(ra)<br> [0x8000cdb8]:sw t6, 840(ra)<br> [0x8000cdbc]:sw t5, 848(ra)<br> [0x8000cdc0]:sw a7, 856(ra)<br>                                     |
| 451|[0x80013a28]<br>0x00000000<br> [0x80013a40]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and  fcsr == 0x0 and rm_val == 7   #nosat<br>                                                                                               |[0x8000cdec]:fadd.d t5, t3, s10, dyn<br> [0x8000cdf0]:csrrs a7, fcsr, zero<br> [0x8000cdf4]:sw t5, 864(ra)<br> [0x8000cdf8]:sw t6, 872(ra)<br> [0x8000cdfc]:sw t5, 880(ra)<br> [0x8000ce00]:sw a7, 888(ra)<br>                                     |
